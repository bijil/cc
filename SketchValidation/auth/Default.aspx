﻿<%@ Page Title="Sketch Validation - CourthouseUSA - Login" Language="VB" MasterPageFile="~/App_MasterPages/SecurityPage.master"
	AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
	<meta name="viewport" content="user-scalable=no,initial-scale=1.2,maximum-scale=1.2" />
	<meta name="apple-mobile-web-app-capable" content="yes">
	<script type="text/javascript">
		$(function () {
			$(':text').keypress(function (e) {
				if (e.which == 13) {
					$(':password').focus();
					$(':password').select();
				}
			});

			$(':password').keypress(function (e) {
				if (e.which == 13) {
					var href = $('.login-button').attr('href').replace('javascript:', '');
					eval(href);
				}

			});
			$(':text').focus();
			$(':text').select();
		});
		
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<div class="error" runat="server" id="divError" visible="false">
	</div>
	<asp:Login runat="server" ID="Login">
		<LayoutTemplate>
			<div class="login-inputs">
				<div class="input-box">
					<asp:TextBox ID="UserName" runat="server" CssClass="text txt-username" watermark="Your Login ID" />
				</div>
				<div class="input-box">
					<asp:TextBox ID="Password" TextMode="Password" runat="server" CssClass="text txt-password" watermark="Password" />
				</div>
			</div>
			<div style="text-align: center;">
				<asp:LinkButton runat="server" CssClass="login-button" ID="Login" CommandName="Login"><span>Login</span></asp:LinkButton>
			</div>
		</LayoutTemplate>
	</asp:Login>
</asp:Content>
<asp:Content ContentPlaceHolderID="Footer" runat="server">
	<div>
		<strong>
			<asp:Label runat="server" ID="lblOrgName" /></strong>
	</div>
		<script type="text/javascript">

			var theForm = document.forms['form1'];
			if (!theForm) {
				theForm = document.form1;
			}
			function __doPostBack(eventTarget, eventArgument) {
				if (theForm === undefined) {
					theForm = document.forms['form1'];
					if (!theForm) {
						theForm = document.form1;
					}

					alert(theForm);
				}
				if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
					theForm.__EVENTTARGET.value = eventTarget;
					theForm.__EVENTARGUMENT.value = eventArgument;
					theForm.submit();
				}
			}
	</script>
</asp:Content>
