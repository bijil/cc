﻿var tileSize = 256;

var countyMapOptions = {
	getTileUrl: function (coord, zoom) {
		var x = coord.x;
		var y = coord.y;
		var lng = ((x * tileSize) - (tileSize * (Math.pow(2, zoom - 1)))) / ((tileSize * (Math.pow(2, zoom))) / 360);
		var ex = ((y * tileSize) - (tileSize * (Math.pow(2, zoom - 1)))) / ((-tileSize * Math.pow(2, zoom)) / (2 * Math.PI));
		var lat = ((2 * Math.atan(Math.exp(ex))) - (Math.PI / 2)) / (Math.PI / 180);
		if (lng > 180) lng -= 360;
		if (lng < -180) lng += 360;
		if (lat < -90) lat = -90;
		if (lat > 90) lat = 90;
		return "/mapsvc/tile/?lat=" + lat + "&lng=" + lng + '&zoom=' + zoom;
		//return "http://localhost:8808/?ll=" + lat + "," + lng + '&z=' + zoom;
	},
	tileSize: new google.maps.Size(tileSize, tileSize),
	maxZoom: 20,
	minZoom: 12,
	zoom:19,
	name: "County Imagery"
};

var countyMapType = new google.maps.ImageMapType(countyMapOptions);
var measure;
function loadSearchMap() {
	var mapOptions = {
		zoom: 18,
		maxZoom: 20,
		minZoom: 6,
		center: new google.maps.LatLng(41.68953, -83.576147),
		tilt: 0,
		streetViewControl: false,
		mapTypeControlOptions: {
			mapTypeIds: ["CountyImagery", google.maps.MapTypeId.HYBRID]
		}
	};
	var mc = document.getElementById('search-map');
	map = new google.maps.Map(mc, mapOptions);

	google.maps.event.addListener(map, 'bounds_changed', (function () {
	    window.setTimeout('showResultsOnMap();', 500);
	}));

    map.mapTypes.set('CountyImagery', countyMapType);
	//map.setMapTypeId('CountyImagery');
	map.setMapTypeId('hybrid');

	google.maps.event.addListener(map, 'zoom_changed', (function () {
//	    if ((map.getZoom() > 17) && (activeParcel != null)) {
//	        map.setMapTypeId('CountyImagery');
//	    } else {
//	        map.setMapTypeId('hybrid');
//	    }
	}));

	var measureDiv = document.createElement('div');
	measure = new MeasurementScale(measureDiv, map);
	measureDiv.index = 1;
	map.controls[google.maps.ControlPosition.TOP_LEFT].push(measureDiv);

	google.maps.event.addListener(map, 'click', (function (e) {
	    if (measure.on) {
	        if (measure.start == null) {
	            measure.start = e.latLng;
	        } else {
	            if (measure.end == null) {
	                measure.end = e.latLng;
	            } else {
	                measure.start = e.latLng;
	                measure.end = null;
	            }
	        }

	        measure.updateGraphics();
	        e.stop();
	    }
	}));

	
//	google.maps.event.addListener(parcelPolygon, 'click', (function (e) {
//	    if (measure.on) {
//	        if (measure.start == null) {
//	            measure.start = e.latLng;
//	        } else {
//	            if (measure.end == null) {
//	                measure.end = e.latLng;
//	            } else {
//	                measure.start = e.latLng;
//	                measure.end = null;
//	            }
//	        }

//	        measure.updateGraphics();
//	        e.stop();
//	    }
//	}));
	
}

function MeasurementScale(div, map) {
    var ms = this;
    this.control = div;
    this.on = false;
    this.start = null;
    this.end = null;

    var mA, mB, line;
    mA = new google.maps.Marker();
    mB = new google.maps.Marker();
    line = new google.maps.Polyline({strokeColor: 'Yellow'});

    this.clear = function () {
        this.start = null;
        this.end = null;
        this.updateGraphics();
        label.innerHTML = '0.0ft';
    }

    this.updateGraphics = function () {
        mA.setMap(null);
        mB.setMap(null);
        line.setMap(null);

        if (ms.start != null) {
            mA.setPosition(ms.start);
            mA.setMap(map);
        }

        if (ms.end != null) {
            mB.setPosition(ms.end);
            mB.setMap(map);
        }

        if ((ms.start != null) && (ms.end != null)) {
            line.setPath([ms.start, ms.end]);
            line.setMap(map);
            label.innerHTML = Math.round(ms.distance() * 10) / 10 + 'ft';
        }
    }

    div.className = 'measurement-tool measure-off';

    var button = document.createElement('span');
    button.className = 'button';
    div.appendChild(button);

    var label = document.createElement('span');
    label.className = 'distance';
    div.appendChild(label);
    label.innerHTML = '0.0ft'

    google.maps.event.addDomListener(button, 'click', function (e) {
        if (ms.on) {
            map.setOptions({ draggableCursor: null });
            div.className = 'measurement-tool measure-off';
            ms.clear();
        } else {
            map.setOptions({ draggableCursor: 'crosshair' });
            div.className = 'measurement-tool measure-on';
            ms.clear();
        }

        ms.on = !ms.on;
        e.preventDefault();
    });

    this.distance = function () {
        if ((ms.start != null) && (ms.end != null)) {
            return google.maps.geometry.spherical.computeDistanceBetween(ms.start, ms.end) * 3.28084;
        } else {
            return 0;
        }
    }

    //this.__defineGetter__("distance", );
    
}