﻿Imports Microsoft.Reporting.WebForms

Partial Class reports
	Inherits System.Web.UI.Page

	Sub LoadReports()
        ddlReport.FillFromSql("SELECT Id, Name FROM Reports WHERE RoleId = 8 AND FreeReport = 1")
    End Sub

	Protected Sub btnGenerate_Click(sender As Object, e As System.EventArgs) Handles btnGenerate.Click
		If ddlReport.SelectedValue <> "" Then
			Dim reportId As Integer = ddlReport.SelectedValue
			Dim r As DataRow = t_("SELECT * FROM Reports WHERE Id = " & reportId)
			viewer.LocalReport.DataSources.Clear()
			If r("DataSetName") IsNot DBNull.Value Then
				Dim dsname As String = r.GetString("DataSetName")
				Dim dsource As String = r.GetString("DataSource")
				Dim rs As New ReportDataSource(dsname, d_(dsource))
				viewer.LocalReport.DataSources.Add(rs)

				Dim rpath As String = r.GetString("ReportPath")
				viewer.LocalReport.ReportPath = rpath
                viewer.LocalReport.Refresh()

                If viewer.LocalReport.GetParameters("ReportTitle") IsNot Nothing Then
                    Dim param As New ReportParameter
                    param.Name = "ReportTitle"
                    param.Values.Add(r.GetString("ReportTitle"))
                    viewer.LocalReport.SetParameters(param)
                End If

			End If
		End If
		'Dim ds As New ReportDataSource("DataSet1", d_("SELECT * FROM reports_ProductivityTotals"))
		'viewer.LocalReport.DataSources.Add(ds)
		'viewer.LocalReport.ReportPath = "reports/ProductivityTotalsReviewer.rdlc"
		''viewer.LocalReport.Refresh()
	End Sub

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
			LoadReports()
		End If
	End Sub

	Protected Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
		RunScript("setScreenLayout();")
	End Sub
End Class
