﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/CAMASV.master"
    AutoEventWireup="false" CodeFile="sv.aspx.vb" Inherits="SV" %>

<asp:Content runat="server" ContentPlaceHolderID="head">
<link rel="Stylesheet" href="/App_Static/slider/winclassic.css" type="text/css" />
    <script type="text/javascript" src="/App_Static/slider/range.js"></script>
    <script type="text/javascript" src="/App_Static/slider/timer.js"></script>
    <script type="text/javascript" src="/App_Static/slider/slider.js"></script>
    <script type="text/javascript" src="/App_Static/js/map.js?12345"></script>
    <script type="text/javascript" src="/App_Static/js/search.js"></script>
    <script type="text/javascript" src="/App_Static/js/parcel.js?t=1236"></script>
    <script type="text/javascript" src="/App_Static/js/commands.js"></script>
    <script type="text/javascript" src="/App_Static/js/sketch.js?t=1235"></script>
    <script type="text/javascript">
        var iResize, iRotate;
//        function setPageFunctions() {
//            //iResize = new Slider(document.getElementById("sketch-resize"), document.getElementById("sketch-resize-input"));
//        }
        
    </script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <style type="text/css">
        #rblStatus label:before
        {
            content: " ";
            display: inline-block;
            width: 12px;
            height: 12px;
            margin-right: 8px;
            border: 1px solid #333;
        }
    </style>
    <asp:Repeater runat="server" ID="rpFlagStyles">
        <HeaderTemplate>
            <style type="text/css">
        </HeaderTemplate>
        <ItemTemplate>
            label[for="rblStatus_<%# Container.ItemIndex%>"]:before 
            { 
                background:<%# Eval("ColorCode")%>;
            }
        </ItemTemplate>
        <FooterTemplate>
            </style>
            </FooterTemplate> </asp:Repeater>
            <section class="screen" id="sketch-validation">
                <div class="search-panel">
                    <table>
                        <tr>
                            <td>
                                <span>Parcel ID:</span>
                            </td>
                            <td>
                                <input type="text" autocorrect="off" autocapitalize="off" maxlength="20" class="src-parcel src-field" />
                            </td>
                            <td>
                                <span>Neighborhood:</span>
                            </td>
                            <td>
                                <input type="text" autocorrect="off" autocapitalize="off" maxlength="20" class="src-nbhd src-field" />
                            </td>
                            <td>
                                <span>Flag Status:</span>
                            </td>
                            <td>
                                <select class="src-flag src-field">
                                    <option value="">All</option>
                                    <option value="1">Field Inspection</option>
                                    <option value="2">Data Entry</option>
                                    <option value="3">Further Review</option>
                                    <option value="4">Complete</option>
                                    <option value="5">Quality Control</option>
                                </select>
                            </td>
                            <td>
                                <span>Reviewed By:</span>
                            </td>
                            <td class="last-item">
                                <asp:DropDownList runat="server" ID="ddlUsers" CssClass="src-user src-field">
                                </asp:DropDownList>
                            </td>
                            <td class="search-buttons">
                                <button class="cmd-search">
                                    Search</button>
                                <button class="cmd-cancel">
                                    Clear</button>
                                <button onclick="showResultsOnMap();return false;">
                                    Refresh</button>
                            </td>
                        </tr>
                    </table>
                </div>
                <table class="map-table">
                    <tr>
                        <td class="mapframe">
                            <div id="search-map">
                            </div>
                        </td>
                        <td class="parcelframe">
                            <div class="toolbar">
                                <table>
                                    <tr>
                                        <td>
                                            <button style="color: Red; font-weight: bold;" onclick="closeParcel();return false;">
                                                r</button>
                                        </td>
                                        <td>
                                            <button class="cmd-prev">
                                                7</button>
                                        </td>
                                        <td>
                                            <button class="cmd-next">
                                                8</button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <table class="parcel-frame-table">
                                <tr>
                                    <td class="divider">
                                    </td>
                                    <td class="sketchframe">
                                        <div class="title-line">
                                            Parcel Details <span class="data-view-switch"><a class="a-view-sketch" href="#">View
                                                Sketch</a> <a class="a-view-oby" href="#">View Outbuildings</a> </span>
                                        </div>
                                        <div class="parcel-header-data">
                                            <div>
                                                Loading ...</div>
                                        </div>
                                        <div class="view-sketch">
                                            <canvas style="width: 396px; height: 380px; border: 1px solid #BBB" id="sketchvas">
                                            </canvas>
                                            <div class="sketch-tools ui-widget-header">
                                                <table style="width: 100%; border-spacing: 0px;">
                                                    <tr>
                                                        <td>
                                                            <span class="sketch-tool-value">Zoom: <span class="sketch-zoom-value">100%</span></span>&nbsp;
                                                            <input type="range" min="0.5" max="2.0" value="1.0" step="0.01" class="sketch-resize sketch-tool"
                                                                onchange="paintSketch();" />
                                                            <%--<div class="slider" id="sketch-resize" style="display:inline-block;">
                                                                <input class="slider-input" id="sketch-resize-input" name="sketch-resize-input" value="50" />
                                                            </div>--%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span class="sketch-tool-value">Rotate: <span class="sketch-rotate-value">0&deg;</span></span>&nbsp;
                                                            <input type="range" min="-180" max="180" value="0" step="1" class="sketch-rotate sketch-tool"
                                                                onchange="paintSketch();" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="view-oby" style="display: none;">
                                        </div>
                                        <div class="navigation">
                                        </div>
                                    </td>
                                    <td class="divider">
                                    </td>
                                    <td class="dataframe">
                                        <div class="title-line">
                                            Sketch Validation & Review:
                                        </div>
                                        <div class="review-form">
                                            <div class="review-save">
                                                <button class="cmd-savereview">
                                                    Save Review</button>
                                                <button class="cmd-reset">
                                                    Reset</button>
                                            </div>
                                            <div>
                                                Notes:</div>
                                            <textarea style="width: 90%; height: 120px;" class="review-note"></textarea>
                                            <asp:RadioButtonList runat="server" ID="rblStatus" Width="100%" RepeatColumns="2"
                                                RepeatDirection="Horizontal" ClientIDMode="Static">
                                                <asp:ListItem Text="Field Inspection" Value="1" />
                                                <asp:ListItem Text="Data Entry" Value="2" />
                                                <asp:ListItem Text="Further Review" Value="3" />
                                                <asp:ListItem Text="Completed" Value="4" />
                                                <asp:ListItem Text="QC Passed" Value="5" />
                                            </asp:RadioButtonList>
                                            <%--<div class="sv-qc-check">
                                        <asp:CheckBox runat="server" ID="chkQC" Text="Quality Control Passed" Font-Bold="true" />
                                    </div>--%>
                                            <div class="otherflags" id="divOtherFlags" runat="server">
                                                <div class="info">
                                                    <b>Other flags:</b>
                                                    <br />
                                                    Click or slide the button to change state.</div>
                                                <table class="flag-fields">
                                                    <asp:Repeater runat="server" ID='rptOtherFlags'>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td>
                                                                    <%#Eval("Name") %>
                                                                </td>
                                                                <td style="width: 50px;">
                                                                    <input type="checkbox" flag='<%# Eval("Id") %>' style="width: 40px;" />
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </table>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </section>
            <section class="screen" id="searchresults">
            </section>
</asp:Content> 