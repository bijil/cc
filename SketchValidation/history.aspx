﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/CAMASV.master"
    AutoEventWireup="false" CodeFile="history.aspx.vb" Inherits="history" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function setPageLayout(w, h) {
            $('.screen').height(h);
            $('.contentpage').height(h - $('.page-title').height()-10);
            $('.contentpage').css({ 'overflow-y': 'auto' });
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <section class="screen" id="history">
		<div class="title-line page-title">
			Audit Trail</div>
            <div class="contentpage">
            		<div class="historypage">
			<asp:UpdatePanel runat="server" ID="upHistory">
				<ContentTemplate>
					<p>
						<asp:Button runat="server" ID="btnRefreshHistory" ClientIDMode="Static" Text=" Refresh History "
							Style="padding: 4px 15px;" />
					</p>
					<p>
						Showing latest
						<asp:Label runat="server" ID="lblAuditCount" Text="0" />
						items from history</p>
					<asp:Repeater runat="server" ID="rpAudit">
						<ItemTemplate>
							<span>
								<%# ShowDate()%></span>
							<div class="h-item">
								<span class="h-time">
									<%# CDate(Eval("ReviewTime")).ToString("h:mm tt")%></span> <span class="h-user">
										<%# Profile.GetProfile(Eval("LoginID")).FullName%></span> <span class="h-kv1"><a
											href='sv.aspx#<%# Eval("ParcelId") %>' >
											<%# Eval("KeyValue1")%></a></span> <span class="h-parcel">
												<%# Eval("StreetAddress")%></span> <span class="h-desc">
													<%# Eval("Description")%></span>
							</div>
						</ItemTemplate>
					</asp:Repeater>
				</ContentTemplate>
			</asp:UpdatePanel>
		</div>
        </div>
	</section>
</asp:Content>
