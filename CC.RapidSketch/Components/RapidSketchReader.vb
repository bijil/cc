﻿Imports RapidSketch
Imports RapidSketch.UOMs


Public Class RapidSketchReader

    Public Shared Function ConvertLineToVectorString(area As Area, std As UOM)
        Dim vectorString As String = ""

        Dim startingLine As Line = area.StartingLine
        Dim startPoint As Point = startingLine.Start
        If startPoint.Y > 0 Then
            vectorString += "U" + std.GetLengthInUnits(startPoint.Y) + " "
        ElseIf startPoint.Y < 0 Then
            vectorString += "D" + std.GetLengthInUnits(-startPoint.Y) + " "
        End If

        If startPoint.X > 0 Then
            vectorString += "R" + std.GetLengthInUnits(startPoint.X) + " "
        ElseIf startPoint.X < 0 Then
            vectorString += "L" + std.GetLengthInUnits(-startPoint.X) + " "
        End If

        vectorString += "S "

        For Each line As Line In area.Lines
            Dim lineLength As String = std.GetLengthInUnits(line.Length)
            Dim arcHeight As String = std.GetLengthInUnits(line.ArcHeight)



            If line.Length > 0 Then
                If line.ArcHeight <> 0 Then
                    vectorString += "A" + arcHeight + "/"
                End If

                If line.IsVertical AndAlso line.IsUp Then
                    vectorString += "U" + lineLength
                ElseIf line.IsVertical AndAlso line.IsDown Then
                    vectorString += "D" + lineLength
                ElseIf line.IsHorizontal AndAlso line.IsLeft Then
                    vectorString += "L" + lineLength
                ElseIf line.IsHorizontal AndAlso line.IsRight() Then
                    vectorString += "R" + lineLength
                Else
                    Dim vLength As String = std.GetLengthInUnits(Math.Abs(Math.Cos(line.AngleAsRadians) * line.Length))
                    Dim hLength As String = std.GetLengthInUnits(Math.Abs(Math.Sin(line.AngleAsRadians) * line.Length))
                    If line.IsUp Then
                        vectorString += "U" + vLength + "/"
                    ElseIf line.IsDown Then
                        vectorString += "D" + vLength + "/"
                    End If
                    If line.IsLeft Then
                        vectorString += "L" + hLength
                    ElseIf line.IsRight Then
                        vectorString += "R" + hLength
                    End If
                End If
            End If

            vectorString += " "
        Next


        Return vectorString
    End Function

End Class
