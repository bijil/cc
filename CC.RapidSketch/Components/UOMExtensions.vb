﻿Imports System.Runtime.CompilerServices
Imports RapidSketch
Imports RapidSketch.UOMs

Module UOMExtensions

    <Extension()> Public Function GetLengthInUnits(source As UOM, pixelLength As Single) As String
        Return (pixelLength / source.PixelsPerUnit).ToString("0.##")
    End Function

End Module
