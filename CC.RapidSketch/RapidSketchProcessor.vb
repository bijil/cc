﻿Imports CAMACloud.BusinessLogic.Sketching
Imports System.Runtime.Serialization.Formatters.Binary
Imports RapidSketch

Public Class RapidSketchProcessor
    Implements ISketchProcessor

    Public Function ReadSketch(stream As System.IO.Stream) As System.Collections.Generic.List(Of BusinessLogic.Sketching.SketchVector) Implements BusinessLogic.Sketching.ISketchProcessor.ReadSketch
        Dim formatter As New BinaryFormatter
        Dim sk As Sketch = formatter.Deserialize(stream)

        Dim vectors As New List(Of SketchVector)
        Dim ord As Integer = 0
        For Each a As Area In sk.AllAreas
            ord += 1
            vectors.Add(New SketchVector With {.Ordinal = ord, .Label = a.Name, .ReferenceId = a.ReferenceId, .VectorString = RapidSketchReader.ConvertLineToVectorString(a, sk.UOM)})
        Next
        Return vectors
    End Function

    Public Function UpdateSketch(stream As System.IO.Stream, vectors As System.Collections.Generic.List(Of BusinessLogic.Sketching.SketchVector)) As System.IO.Stream Implements BusinessLogic.Sketching.ISketchProcessor.UpdateSketch
        Return stream
    End Function
End Class
