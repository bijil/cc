﻿Public Class SyncHelper

    Private Shared sqlParcel As String
    Private Shared sqlMapPoints As String = "DROP TABLE MapPoints; CREATE TABLE MapPoints (Id INTEGER, ParcelId INTEGER, Point, Index INTEGER);"
    Private Shared sqlImages As String = "DROP TABLE Images; CREATE TABLE IF NOT EXISTS Images (Id INTEGER, ParcelId INTEGER, Image, Type INTEGER, LocalId, Accepted INTEGER INTEGER NOT NULL DEFAULT 0, Synced INTEGER NOT NULL DEFAULT 0);"
    Private Shared sqlComparables As String = "DROP TABLE Comparables; CREATE TABLE IF NOT EXISTS Comparables (Id INTEGER, ParcelId INTEGER, C1 INTEGER, C2 INTEGER, C3 INTEGER, C4 INTEGER, C5 INTEGER);"
    Private Shared sqlStreets As String
    Private Shared sqlNbhdStats As String

    Private Shared sqlParcelChanges As String = "CREATE TABLE IF NOT EXISTS ParcelChanges (Id INTEGER PRIMARY KEY, ParcelId INTGER, AuxRowId INTEGER, ParentAuxRowId, Field, FieldId INTEGER, OldValue, NewValue, ChangedTime, Latitude, Longitude, Synced);"
    Private Shared sqlCompletedParcels As String = "DROP TABLE CompletedParcels; CREATE TABLE IF NOT EXISTS CompletedParcels (Id INTEGER PRIMARY KEY, ParcelId INTEGER, SelectedAppraisalType, ChangedTime, Latitude, Longitude, Synced);"

    Private Shared sqlViewSubjectParcels As String = "DROP VIEW SubjectParcels; CREATE VIEW SubjectParcels AS SELECT p.Id, n.*, p.*, n.Number AS NeighborhoodNo, n.Number AS NBHDNO, p.Id AS SubjectParcelId FROM Parcel p INNER JOIN Neighborhood n ON p.NeighborhoodId = n.Id WHERE IsComparable = 0;"
    Private Shared sqlViewPendingParcels As String = "DROP VIEW PendingParcels; CREATE VIEW PendingParcels AS SELECT p.Id, n.*, p.*, n.Number AS NeighborhoodNo, n.Number AS NBHDNO, p.Id AS SubjectParcelId FROM Parcel p INNER JOIN Neighborhood n ON p.NeighborhoodId = n.Id WHERE Reviewed = 'false' AND IsComparable = 0;"


    Public Shared Function GetPreparationStatements() As Dictionary(Of String, String)


        Return Nothing
    End Function


    Private Shared Function getParcelSql() As String
        For Each dr As DataRow In Database.Tenant.GetDataTable("SELECT Name, DataType, AssignedName, DefaultValue, SourceTable FROM DataSourceField WHERE CategoryId IS NOT NULL ORDER BY Name").Rows
            Dim fieldName As String = dr.GetString("Name")
        Next
        Return Nothing
    End Function

End Class
