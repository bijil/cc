﻿Imports System.Web.Security
Imports CAMACloud.Security
Imports CAMACloud.Exceptions

Public Class AuthenticationProcessor
    Inherits RequestProcessorBase

    Public Overrides Sub ProcessRequest()

        If Not CAMACloud.Security.DeviceLicense.ValidateRequest(HttpContext.Current.ApplicationInstance) Then
            Dim calAuthPage As String = ApplicationSettings.CALAuthorityPageUrl
            JSON.Write(New With {.LoginStatus = "423", .RedirectURL = calAuthPage})
            Return
        End If
        Dim uName As String
        If Path <> "reset" And Path <> "resetdevice" Then
            uName = Database.System.GetStringValue("SELECT au.UserName FROM  aspnet_Users au INNER JOIN aspnet_Applications a On a.ApplicationId=au.ApplicationId WHERE  a.ApplicationName='" & Membership.ApplicationName & "' And au.UserName='" & Request("username").ToString() & "'")
        End If
        Select Case Path
            Case "login"
                Dim userName As String = Request("username")
                Dim password As String = Request("password")

                If Membership.ValidateUser(userName, password) Then
                    FormsAuthentication.Authenticate(userName, password)
                    Dim authCookie As HttpCookie = FormsAuthentication.GetAuthCookie(userName, True)
                    authCookie.Domain = FormsAuthentication.CookieDomain
                    authCookie.Expires = Now.AddHours(12)
                    Response.Cookies.Add(authCookie)
                    CAMASession.RegisterUserOnDeviceLicense(uName)
                    Dim UserType As DataRow = Database.Tenant.GetTopRow("IF exists(SELECT * FROM Application where BPPEnabled =1) SELECT CASE IsBPPAppraiser WHEN  1 then 'BPP' ELSE 'RP' END as IsBPPAppraiser,BPPRealDataReadOnly,CASE  WHEN BPPDownloadType = 1 then '1' ELSE '0' END  as BPPDownloadType   from UserSettings where loginId = '" + uName + "' ")
                    If (Not UserType Is Nothing) Then
                        JSON.Write(New With {.status = "OK", .username = uName, .usertype = UserType.GetString("IsBPPAppraiser"), .RealDataReadOnly = UserType.GetBoolean("BPPRealDataReadOnly"), .BPPDownloadType = UserType.GetInteger("BPPDownloadType")})
                    Else
                        JSON.Write(New With {.status = "OK", .username = uName})
                    End If

                    UserAuditTrail.CreateEvent(uName, UserAuditTrail.ApplicationType.MobileAssessor, UserAuditTrail.EventType.Login, "User logged in from mobile.")
                Else
                    Try
                        Throw New Exception("Login Failed.")
                    Catch ex As Exception
                        ErrorMailer.ReportException("Login", ex, HttpContext.Current.Request)
                    End Try
                    JSON.Error("Invalid credentials for " + HttpContext.Current.GetCAMASession.OrganizationName + ". Log in attempt failed. ")
                End If
            Case "logout"
                FormsAuthentication.SignOut()
                JSON.Write(New With {.LoginStatus = "401"})
                UserAuditTrail.CreateEvent(uName, UserAuditTrail.ApplicationType.MobileAssessor, UserAuditTrail.EventType.Logout, "User logged out of mobile.")
                Return
            Case "deviceauth"
                Dim license As String = Request("license")
                Dim otp As String = Request("otp")

                Try
                    DeviceLicense.Authenticate(license, otp, "", "", HttpContext.Current)
                    CAMASession.RegisterUserOnDeviceLicense("!DSynClient")
                    JSON.OK()
                Catch lex As InvalidLicenseException
                    ErrorMailer.ReportException("license", lex, Request)
                    JSON.Error(lex.Message)
                Catch ex As Exception
                    ErrorMailer.ReportException("license-unknown", ex, Request)
                    JSON.Error(ex.Message)
                End Try
            Case "reset"
                Try
                    DeviceLicense.ClearLicenseCache(HttpContext.Current)
                    HttpContext.Current.Session.Clear()
                    HttpContext.Current.Session.Abandon()
                    JSON.OK()
                Catch ex As Exception
                    JSON.Error(ex)
                End Try
            Case "resetdevice"
                Try
                    DeviceLicense.ClearLicenseCache(HttpContext.Current)
                    HttpContext.Current.Response.Cookies.Clear()
                    HttpContext.Current.Session.Clear()
                    HttpContext.Current.Session.Abandon()
                    JSON.OK()
                Catch ex As Exception
                    JSON.Error(ex)
                End Try
        End Select
    End Sub
End Class
