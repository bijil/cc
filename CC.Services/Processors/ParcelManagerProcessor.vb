﻿
Imports System.Text.RegularExpressions

Public Class ParcelManagerProcessor
    Inherits RequestProcessorBase
    Public ReadOnly Property EnableNewPriorities As Boolean
        Get
            Dim EnableNewPriority = False
            Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM ClientSettings WHERE Name = 'EnableNewPriorities' AND Value = 1")
            If dr IsNot Nothing Then
                EnableNewPriority = True
            End If
            Return EnableNewPriority
        End Get
    End Property

    Public Overrides Sub ProcessRequest()
        Select Case Path
            Case "verifyahg"
                Dim newName As String = Request("name")
                Dim prioId As String = Request("prioId")
                Dim oldName As String = Request("oldName")
                Dim parcelCount As Integer = 0
                Try
                    Dim nbhdExists As Boolean = Neighborhood.DoesGroupExist(Database.Tenant, newName, parcelCount)
                    Dim message As String = ""
                    If nbhdExists Then
                        message = "A group named " + newName + " already exists with " & parcelCount & " parcels."
                    Else
                        message = "Group with name " + newName + " do not exist."
                        Database.Tenant.Execute("UPDATE PriorityListItems SET GROUPNAME ={0},OldName ={2} WHERE PriorityListId = {1} AND GROUPNAME ={2}".SqlFormatString(newName, prioId, oldName))
                    End If
                    JSON.Write(New With {.status = "OK", .exists = nbhdExists, .parcels = parcelCount, .message = message})
                Catch ex As Exception
                    JSON.Error(ex.Message)
                End Try
            Case "getcomparablesmappoints"
                Dim Ids As String = Request("pids")
                JSON.SendTable(Database.Tenant, "EXEC cse.ComparablesMapPoints '" & Ids & "'")
            Case "appraiserlocator"
                JSON.SendTable(Database.Tenant, "select * from usertrackingcurrent where loginId <>  'admin'  and latitude is not null  and longitude is not null")
            Case "getmappoints"
                Dim pageindex As Integer = Request("pageindex")
                Dim startindex As Integer
                Dim endindex As Integer
                startindex = (pageindex - 1) * 10000 + 1
                endindex = pageindex * 10000
                Dim ds As DataSet = Database.Tenant.GetDataSet("select a.* from( select cc.*,ROW_NUMBER() OVER(ORDER BY cc.ParcelId) Rownum from cc_MergeParcelMapPoints  cc) a where Rownum between '" & startindex.ToString() & "' AND '" & endindex.ToString() & "'")
                JSON.SendTable(ds.Tables(0))
            Case "getparcels"
                Dim neighborhoodid As String = Request("number")
                JSON.SendTable(Database.Tenant, "select P.Priority,P.KeyValue1,PM.Latitude,PM.Longitude from ParcelMapPoints as PM join Parcel as P on PM.ParcelId = P.Id join Neighborhood as N on P.NeighborhoodId = N.Id where N.Number ='" + neighborhoodid + "'")
            Case "getparcelid"
                Dim bounds As String = Request("bounds")
                Dim latLng As String() = bounds.Split("$$")
                Dim lat As String()
                Dim lng As String()
                Dim subquery As String = ""
                Dim coordinates As String()
                For Each item As String In latLng
                    If item <> Nothing AndAlso item <> "" Then
                        coordinates = item.Split("|")
                        lat = coordinates(0).Split(",")
                        lng = coordinates(1).Split(",")
                        subquery += " ((Pm.Latitude Between " + lat(0) + " and " + lat(1) + ") and ( Pm.Longitude between " + lng(0) + " and " + lng(1) + ")) OR"
                    End If
                Next
                If subquery.Length > 0 Then
                    subquery = subquery.Substring(0, subquery.Length - 2) + " order by P.KeyValue1"
                    JSON.SendTable(Database.Tenant, "SELECT DISTINCT top 5000 P.KeyValue1,P.Priority FROM ParcelMapPoints pm  JOIN Parcel p ON pm.ParcelId = p.Id JOIN Neighborhood n ON n.Id = p.NeighborhoodId WHERE" + subquery)
                Else
                    JSON.Error("Invalid points")
                End If

            Case "getadhocneighborhood"
                JSON.SendTable(Database.Tenant, "select Number from Neighborhood where IsAdhoc ='1'")
            Case "adhoccreation"
                Dim ParcelId As String = Request("ParcelId")
                Dim Priority As String = Request("Priority")
                Dim AdHocNumber As String = Request("AdHocNumber")
                Dim Status As String = Request("Status")
                Dim LoginID As String = Request("LoginId")
                Database.Tenant.Execute("EXEC cc_ParcelPriorityChange '" & ParcelId & "','" & Priority & "','" & AdHocNumber & "','" & Status & "','" & LoginID & "'")
                JSON.OK(ParcelId)

            Case "getadhocneighborhood"
                JSON.SendTable(Database.Tenant, "select Id,Number from Neighborhood where IsAdhoc =1")
            Case "mappointscount"
                Dim mappoints As String = Database.Tenant.GetStringValue("EXEC cc_GetParcels")
                JSON.WriteString("{""PointCount"":" + mappoints + "}")
            Case "setmapboundary"
                JSON.SendTable(Database.Tenant, "select MAX(Latitude) as Maxlat,MAX(Longitude) as Maxlng,MIN(Latitude) as Minlat,MIN(Longitude) as Minlng  from ParcelMapPoints")
            'select MAX(Latitude) as Maxlat,MAX(Longitude) as Maxlng,MIN(Latitude) as Minlat,MIN(Longitude) as Minlng  from ParcelMapPoints
            Case "getparcelcount"
                Dim bounds As String() = Request("bounds").ToString().Split(",")
                JSON.SendTable(Database.Tenant, "SELECT count( DISTINCT(P.KeyValue1)) as count,case P.Priority when '0' then 'Normal'  when'1' then 'High' when '2' then 'Urgent' end Priority  FROM ParcelMapPoints pm  JOIN Parcel p ON pm.ParcelId = p.Id JOIN Neighborhood n ON n.Id = p.NeighborhoodId WHERE ((Pm.Latitude Between " + bounds(0) + " and " + bounds(1) + ") and ( Pm.Longitude between " + bounds(2) + " and " + bounds(3) + " )) group by P.Priority")
            Case "viewlist"
                Dim Name As String = Request("Name")
                'Dim KeyFields As String = Request("keyfields")
                'Dim keyField1 As String = KeyFields.Split(","c)(0)
                Dim TableId As String = Request("prioTId")
                Dim fields As String = ""
                Dim t As Integer
                If Database.Tenant.Application.ShowKeyValue1 = True Then
                    t = 1
                Else
                    t = 2
                End If
                For i = t To 10
                    If Database.Tenant.Application.KeyField(i).IsNotEmpty Then
                        If fields <> "" Then
                            fields += ","
                        End If
                        'Database.System.GetStringValue("select keyvalue"+i.ToString()+ "From Application")
                        fields += "temp.KEYVALUE" + i.ToString() + " AS " + Database.Tenant.Application.KeyField(i)

                    End If
                Next
                If Database.Tenant.Application.ShowAlternateField = True Then
                    If fields <> "" Then
                        fields += ","
                    End If
                    fields += "temp.Alternatekeyfieldvalue AS " + Database.Tenant.Application.AlternateKeyfield
                End If


                Select Case Name
                    Case "NotApproved"
                        JSON.SendTable(Database.Tenant, "SELECT " + fields + ",p.Priority ,[dbo].[GetLocalDate](ReviewDate) AS [Review Date],ReviewedBy AS [Reviewed By],[dbo].[GetLocalDate](QCDate) As [ QC Checked On] ,QCBy As [QC Checked By] FROM Parcel p INNER JOIN PriorityListItems temp On p.id=temp.CC_ParcelId WHERE temp.isInvalid = 0 AND temp.isNotApproved=1 AND temp.PriorityListId = " + TableId.ToString())
                    Case "NotSyncedBack"
                        JSON.SendTable(Database.Tenant, "SELECT " + fields + ",p.Priority ,[dbo].[GetLocalDate](ReviewDate) AS [Review Date],ReviewedBy AS [Reviewed By],[dbo].[GetLocalDate](QCDate) As [ QC Checked On] ,QCBy As [QC Checked By] FROM Parcel p INNER JOIN PriorityListItems temp On p.id=temp.CC_ParcelId WHERE temp.isInvalid = 0 AND temp.isNotSyncedBack =1 AND temp.PriorityListId = " + TableId.ToString())
                    Case "Assigned"
                        Dim ca As New ClientApplication(Database.Tenant)
                        Dim neighborhoodNumberField As String = ca.NeighborhoodField
                        JSON.SendTable(Database.Tenant, "SELECT n.id NbhdId, n.Number AS " + neighborhoodNumberField + " ,n.AssignedTo AS Assigned, COUNT(p.KeyValue1) AS [Count of Properties] FROM Parcel p INNER JOIN PriorityListItems temp On p.id=temp.CC_ParcelId INNER JOIN Neighborhood n ON p.Neighborhoodid=n.id WHERE temp.isInvalid = 0 AND temp.isAssigned =1 AND temp.PriorityListId = " + TableId.ToString() + " GROUP BY n.id,Number,n.AssignedTo")
                    Case "WorkedInFieldLast30days"
                        JSON.SendTable(Database.Tenant, "SELECT " + fields + ",p.Priority,[dbo].[GetLocalDate](temp.LastReviewDate) AS [Last Review Date],temp.LastReviewedBy AS [Last Reviewed By],[dbo].[GetLocalDate](temp.LastQCDate) AS  [Last QC Checked On],temp.LastQCBy  As [Last QC Checked By] FROM PriorityListItems temp INNER JOIN Parcel p On p.id=temp.CC_ParcelId  WHERE temp.isWorkedInFieldLast30days=1 AND temp.isInvalid = 0 AND isDuplicated=0 AND temp.PriorityListId = " + TableId.ToString())
                    Case "Invalid"
                        JSON.SendTable(Database.Tenant, "SELECT " + fields + ",PRIORITY,MESSAGE FROM   PriorityListItems temp  WHERE isInvalid = 1 AND PriorityListId = " + TableId.ToString())
                    Case "Duplicated"
                        JSON.SendTable(Database.Tenant, "SELECT " + fields + ",PRIORITY,MESSAGE FROM   PriorityListItems temp WHERE isDuplicated = 1 AND PriorityListId = " + TableId.ToString())
                End Select
            Case "priority"
                _priority()
            Case "priorityupload"
                _priorityUpload()
            Case "viewinqc"
                _viewInQc()
            Case "removenbhd"
                _removeNBHD()
            Case "prioupload"
                _prioUpload()
            Case "updateclient"
                _updateClient()
            Case "priobackup"
                _priobackup()
        End Select
    End Sub
    Private Sub _priority()
        Try
            Dim isAdho As Boolean = False
            isAdho = Request("adhoc")
            Dim Tid As Integer
            Dim reload As Boolean = Request("reload")
            Dim priority As String = Request("priority")
            Tid = Request("prioId")
            Dim backUpId As String = ""
            Dim grpwithAss As String = Request("grpwithAss")
            Dim grpCodeWithAssn As String() = Nothing
            If grpwithAss <> "" Then
                grpCodeWithAssn = grpwithAss.Split(New Char() {","c})
            End If
            Dim GROUPNAME As String()
            If isAdho Then
                GROUPNAME = grpCodeWithAssn(0).Split(New String() {"@!*!*$#@"}, StringSplitOptions.None)
                Database.Tenant.Execute("UPDATE PriorityListItems SET GROUPNAME = {0},AssignTo = {1} WHERE PriorityListId = {2}".SqlFormatString(GROUPNAME(0), GROUPNAME(1), Tid))
            End If

            If priority <> "" Then
                Database.Tenant.Execute("UPDATE p SET p.PRIORITY = {0} FROM PriorityListItems p INNER JOIN priotityFileRetain pf ON pf.KEYVALUE1 = p.KEYVALUE1 WHERE p.PriorityListId ={1} AND pf.PriorityListId ={1} AND  LTRIM(RTRIM(pf.PRIORITY)) = ''".SqlFormatString(priority, Tid))
            End If
            Dim removeNbhdIds As String = Request("selectedNbhd")
            Dim counts As Integer
            If removeNbhdIds <> "" Then
                counts = Database.Tenant.GetIntegerValue("EXEC SP_VerifyNbhdGrpForPriorityList @Flagg ={0},@NbhdId = {1},@PrioId ={2}".SqlFormatString(1, removeNbhdIds, Tid))
            Else
                counts = 0
            End If

            Dim keyfields As New List(Of String)
            Dim joinCondition As String = ""
            Dim whereCondition As String = ""
            Dim tcount As Integer = 0
            Dim n As Integer
            If Database.Tenant.Application.ShowKeyValue1 = True Then
                n = 1
            Else
                n = 2
            End If
            For i = n To 10
                If Database.Tenant.Application.KeyField(i).IsNotEmpty Then
                    If joinCondition <> "" Then
                        joinCondition += " AND "
                        whereCondition += " OR "
                    End If
                    joinCondition += "p.KeyValue" + i.ToString() + "=tmp.KEYVALUE" + i.ToString()
                    whereCondition += "p.KeyValue" + i.ToString() + " IS NULL"
                    keyfields.Add(Database.Tenant.Application.KeyField(i))
                End If
            Next
            If Database.Tenant.Application.ShowAlternateField = True Then
                keyfields.Add(Database.Tenant.Application.AlternateKeyfield)
            End If

            Dim drParcelCount As DataRow = Database.Tenant.GetTopRow("EXEC cc_ParcelPriorityVerification @joinCondition = {0},@pTableId ={1}".SqlFormat(False, joinCondition, Tid))
            Dim parcel_Count As Integer = 0
            Dim newCode As New List(Of String)()
            Dim newgrp As String = ""

            Dim dt As DataTable = Database.Tenant.GetDataTable("EXEC SP_VerifyNbhdGrpForPriorityList @Flagg = {0}, @PrioId = {1}".SqlFormatString(2, Tid))
            If dt.Rows.Count > 0 Then
                For Each dr As DataRow In dt.Rows
                    newCode.Add(dr.Item(0).ToString())
                Next
                'newCode.Add(dt.AsEnumerable.ToList().ToString())
            End If

            'Database.Tenant.Execute("UPDATE  PriorityListBackup SET TotalParcels={0},Urgent={1},High={2},Normal={3},Invalid={4},IsResetStatus={5},IsAdhoc={6},GroupCode={7},AssignTo={8} WHERE id={9}".SqlFormatString(drParcelCount("TotalParcel").ToString(), drParcelCount("Urgent").ToString(), drParcelCount("High").ToString(), drParcelCount("Normal").ToString(), drParcelCount("Invalid").ToString(), IIf(chkDoNotReset.Checked, 1, 0), IIf(hdnAdHoc.Value = "1", 1, 0), txtGroupNumber.Text, ddlAssignTo.SelectedValue.ToString(), hfBackupId.Value))
            Dim jstr As String
            Dim isNotSyncedBackCount As Integer = 0
            Dim isAssignedCount As Integer = 0
            Dim groupNames As String = ""
            Dim CAMASystem As String = ""
            If drParcelCount("TotalParcel") > 0 Then
                isNotSyncedBackCount = drParcelCount("isNotSyncedBack")
                isAssignedCount = drParcelCount("isAssigned")
            End If
            If isNotSyncedBackCount > 0 Then
                CAMASystem = Database.System.GetStringValue("Select c.Name FROM Organization o INNER JOIN CAMASystem c On o.CAMASystem= c.Id WHERE o.id = " & HttpContext.Current.GetCAMASession().OrganizationId)
            End If
            If isAssignedCount > 0 Then
                Dim dtNotApprovedParcels As DataTable = Database.Tenant.GetDataTable("Select LTRIM(RTRIM( N.Number)) As CC_GROUP FROM PriorityListItems tmp INNER JOIN Parcel p On p.id=tmp.CC_ParcelId  INNER JOIN Neighborhood n On p.Neighborhoodid=n.id  WHERE tmp.isInvalid=0 and isDuplicated=0 And isAssigned=1 And n.AssignedTo Is Not NULL AND tmp.PriorityListId ={0} GROUP BY N.Number, n.AssignedTo".SqlFormatString(Tid))
                groupNames = String.Join(",", dtNotApprovedParcels.Rows.OfType(Of DataRow)().[Select](Function(r) r(0).ToString()))
            End If

            Dim bppEnabled As String = Database.Tenant.GetStringValue("SELECT BPPEnabled FROM Application")
            Dim bppCount As Integer = 0
            If bppEnabled = "True" Then
                bppCount = Database.Tenant.GetStringValue("SELECT COUNT(*) FROM Parcel WHERE Id IN (SELECT DISTINCT p.ParentParcelID FROM PriorityListItems pl JOIN Parcel p On pl.CC_ParcelId = p.Id WHERE pl.PriorityListId = " + Tid.ToString() + " AND p.IsPersonalProperty = 1 And p.ParentParcelID Is Not NULL AND p.ParentParcelID NOT IN (SELECT CC_ParcelId FROM PriorityListItems WHERE PriorityListId = " + Tid.ToString() + ")) And Priority NOT IN (1,2);")
            End If

            groupNames =groupNames.Replace("\","\b")
            jstr = JSON.GetRowAsJSON(drParcelCount).TrimEnd("}")
            jstr += ",""AdhcAssgn"":" + JSON.GetListAsJson(newCode)
            jstr += ",""CAMASystem"": """ + CAMASystem.ToString() + """"
            jstr += ",""AssignedGroupNames"": """ + groupNames + """"
            jstr += ",""prioId"": """ + Tid.ToString() + """"
            'jstr += ",""backUpId"": """ + backUpId + """"
            jstr += ",""isAdho"": """ + isAdho.ToString() + """"
            jstr += ",""counts"": """ + counts.ToString() + """"
            jstr += ",""BppCount"": """ + bppCount.ToString() + """"
            jstr += "}"
            JSON.WriteString(jstr)

            'If Not IsDBNull(drParcelCount) Then
            '    'Dim isNotSyncedBackCount As Integer = IsDBNull(drParcelCount("isNotSyncedBack")
            '    'Dim isAssignedCount As Integer = drParcelCount("isAssigned")

            '    If isNotSyncedBackCount > 0 Then
            '        CAMASystem = Database.System.GetStringValue("Select c.Name FROM Organization o INNER JOIN CAMASystem c On o.CAMASystem= c.Id WHERE o.id = " & HttpContext.Current.GetCAMASession().OrganizationId)
            '    End If
            '    If isAssignedCount > 0 Then
            '        Dim dtNotApprovedParcels As DataTable = Database.Tenant.GetDataTable("Select  N.Number As CC_GROUP FROM PriorityListItems tmp INNER JOIN Parcel p On p.id=tmp.CC_ParcelId  INNER JOIN Neighborhood n On p.Neighborhoodid=n.id  WHERE tmp.isInvalid=0 and isDuplicated=0 And isAssigned=1 And n.AssignedTo Is Not NULL AND tmp.PriorityListId ={0} GROUP BY N.Number, n.AssignedTo".SqlFormatString(Tid))
            '        groupNames = String.Join(",", dtNotApprovedParcels.Rows.OfType(Of DataRow)().[Select](Function(r) r(0).ToString()))
            '    End If
            '    'Dim ntable As DataTable = Database.Tenant.GetDataTable("select * from PriorityListItems where PriorityListId ={0}".SqlFormatString(Tid))
            '    jstr = JSON.GetRowAsJSON(drParcelCount).TrimEnd("}")
            '    jstr += ",""AdhcAssgn"":" + JSON.GetListAsJson(newCode)
            '    jstr += ",""CAMASystem"": """ + CAMASystem.ToString() + """"
            '    jstr += ",""AssignedGroupNames"": """ + groupNames + """"
            '    jstr += ",""prioId"": """ + Tid.ToString() + """"
            '    'jstr += ",""backUpId"": """ + backUpId + """"
            '    jstr += ",""isAdho"": """ + isAdho.ToString() + """"
            '    jstr += ",""counts"": """ + counts.ToString() + """"
            '    jstr += ",""ParcelExist"": true "
            '    jstr += "}"
            '    JSON.WriteString(jstr)
            'Else
            '    jstr = "{""counts"": """ + counts.ToString() + """"
            '    jstr += ",""ParcelExist"": false"
            '    jstr += ",""isAdho"": """ + isAdho.ToString()
            '    jstr += "}"
            'End If


        Catch ex As Exception
            JSON.Error(ex.Message)
        End Try

    End Sub

    Public Function TakePriorityListBackup(fileName As String, login As String) As String
        Dim content As String = ""
        Dim LoginId = login
        Dim ipAddress As String = ""
        If HttpContext.Current IsNot Nothing Then
            ipAddress = HttpContext.Current.Request.ClientIPAddress
        End If
        Dim changeSql As String = "INSERT INTO PriorityListBackup (fileName,FileContent,IPAddress,LoginId) VALUES ({0}, {1}, {2}, {3}); SELECT CAST(@@IDENTITY AS INT) As NewId;"
        'hfBackupId.Value = Database.Tenant.GetIntegerValue(changeSql.SqlFormat(True, "AdhocFile.csv", content, ipAddress, LoginId))
        Dim t As Integer = Database.Tenant.GetIntegerValue(changeSql.SqlFormat(True, "AdhocFile.csv", content, ipAddress, LoginId))
        Return t.ToString()
    End Function
    Public Function GenerateCSV(Optional multigrp As Boolean = False, Optional prioId As Integer = 0) As String
        Dim csv As String = String.Empty
        Dim multiNbhd = 0
        Dim ColumnHeader As New List(Of String)()
        For i = 1 To 10
            If Database.Tenant.Application.KeyField(i).IsNotEmpty Then
                ColumnHeader.Add("KEYVALUE" + i.ToString())
            End If
        Next
        If Database.Tenant.Application.ShowKeyValue1 = False Then
            ColumnHeader.Remove("KEYVALUE1")
        End If
        If Database.Tenant.Application.ShowAlternateField = True Then
            ColumnHeader.Add("Alternatekeyfieldvalue")
        End If
        ColumnHeader.Add("PRIORITY")
        ColumnHeader.Add("MESSAGE")
        If multigrp = True Then
            ColumnHeader.Add("GROUPNAME")
            multiNbhd = 1
        End If
        Dim sql = "EXEC ConvertTableToCSVFormat {0}, {1},{2},{3}".SqlFormat(True, "PriorityListItems", String.Join(",", ColumnHeader), prioId, multiNbhd)
        csv = Database.Tenant.GetStringValue(sql)
        Return csv
    End Function
    Private Sub _priorityUpload()
        Try
            If (Database.Tenant.GetIntegerValue("IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME ='PriorityListItems') SELECT 1 ELSE SELECT 0") = 1) Then
                Dim prioId = Request("prioId")
                Database.Tenant.Execute("DELETE FROM PriorityListItems WHERE isInvalid = 1 OR isDuplicated=1 AND PriorityListId ={0}".SqlFormatString(prioId))
                Dim removeApprovedParcels = Request("removeApprovedParcels")
                Dim removeNotSyncedBackParcels = Request("removeNotSyncedBackParcels")
                Dim removeAssignedParcels = Request("removeAssignedParcels")
                Dim CreateNbhd = Request("CreateNbhd")
                Dim DonotReset = Request("DonotReset")
                Dim appendGrp = Request("appendGrp")
                Dim backupId = Request("backupId")
                Dim grpString = Request("grpString")
                Dim createAdh = Request("createAdh")
                Dim priorityType = Request("priorityType")
                Dim Duplicated = Request("Duplicated")
                Dim High = Request("High")
                Dim Invalid = Request("Invalid")
                Dim Normal = Request("Normal")
                Dim Urgent = Request("Urgent")
                Dim Medium = 0
                Dim Critical = 0
                Dim Proximity = 0
                If EnableNewPriorities Then
                    Medium = Request("Medium")
                    Critical = Request("Critical")
                    Proximity = Request("Proximity")
                End If
                Dim TotalParcel = Request("TotalParcel")
                Dim updateBpp As String = Request("updateBpp")
                Dim removeWorkedInFieldLast30daysParcels = Request("removeWorkedInFieldLast30daysParcels")
                If removeApprovedParcels Then
                    Database.Tenant.Execute("DELETE FROM PriorityListItems WHERE isInvalid = 0 AND isDuplicated=0 AND isNotApproved=1 AND PriorityListId ={0}".SqlFormatString(prioId))
                End If
                If removeNotSyncedBackParcels Then
                    Database.Tenant.Execute("DELETE FROM PriorityListItems WHERE isInvalid = 0 AND isDuplicated=0 AND isNotSyncedBack=1 AND PriorityListId ={0}".SqlFormatString(prioId))
                End If
                If removeAssignedParcels Then
                    Database.Tenant.Execute("DELETE FROM PriorityListItems WHERE  isInvalid = 0 AND isDuplicated=0 AND isAssigned=1 AND PriorityListId = {0}".SqlFormatString(prioId))
                End If

                If removeWorkedInFieldLast30daysParcels Then
                    Database.Tenant.Execute("DELETE FROM PriorityListItems WHERE isInvalid = 0 AND isDuplicated=0 AND isWorkedInFieldLast30days=1 AND PriorityListId ={0}".SqlFormatString(prioId))
                End If
                Dim colName As String = ""
                Dim joinCondition As String = ""
                Dim nbhdId As Integer = -1
                Dim neighborhoodIds(20) As Integer
                Dim alertCount As Integer = 0
                Dim priorityCount As Integer = 0
                Dim revokeCount As Integer = 0
                Dim HCount As Integer = 0
                Dim UCount As Integer = 0
                Dim CCount As Integer = 0
                Dim group As String = ""
                Dim assgn As New List(Of String)
                Dim multiGrp As Boolean = False
                Dim ca As New ClientApplication(Database.Tenant)
                Dim neighborhoodNumberField As String = ca.NeighborhoodField
                Dim dtTemp As DataTable = Database.Tenant.GetDataTable("SELECT * FROM PriorityListItems WHERE PriorityListId ={0}".SqlFormatString(prioId))
                If dtTemp.Rows.Count > 0 Then
                    For i = 1 To 10
                        If Database.Tenant.Application.KeyField(i).IsNotEmpty Then
                            If joinCondition <> "" Then
                                joinCondition += " AND "
                            End If
                            joinCondition += "p.KeyValue" + i.ToString() + "=tmp.KeyValue" + i.ToString()
                        End If
                    Next

                    If (dtTemp.Columns.Contains("MESSAGE") AndAlso dtTemp.Columns.Contains("PRIORITY")) Then
                        If (CreateNbhd = "true" Or createAdh = "True") Then
                            If joinCondition <> "" Then
                                Database.Tenant.Execute("EXEC SP_NBHDUpdateForPriorityList @PrioId ={0},@flag = 1".SqlFormatString(prioId))
                            Else
                                Database.Tenant.Execute("UPDATE  PriorityListBackup Set Error={0} WHERE id={1}".SqlFormatString("Invalid parcel key field configuration.", backupId))
                                Throw New Exception("Invalid parcel key field configuration.")
                            End If
                            Dim grpCodeWithAssn As String() = grpString.Split(New Char() {","c})
                            If grpCodeWithAssn.Count > 1 Then
                                multiGrp = True
                            End If
                            Dim GROUPNAME As String()
                            For n = 0 To grpCodeWithAssn.Length - 1
                                GROUPNAME = grpCodeWithAssn(n).Split(New String() {"@!*!*$#@"}, StringSplitOptions.None)
                                If group <> "" Then
                                    group += ","
                                End If
                                group += GROUPNAME(0)
                                If GROUPNAME.Length > 1 Then
                                    assgn.Add(GROUPNAME(1))
                                End If

                            Next
                        End If
                    End If

                    If DonotReset Then
                        Database.Tenant.Execute("UPDATE p Set p.Priority =tmp.Priority, p.ParcelAlert =(Case tmp.Priority When 0 Then NULL Else Case When tmp.MESSAGE='' THEN NULL ELSE tmp.MESSAGE END END),p.Committed = 0,p.Mps=1, p.DownloadStatus = 0 FROM Parcel p INNER JOIN PriorityListItems tmp ON tmp.CC_ParcelId = p.Id WHERE tmp.PriorityListId ={0}".SqlFormatString(prioId))
                    Else
                        Database.Tenant.Execute("UPDATE p SET p.Reviewed=0,p.ReviewedBy=NULL,p.QCDate=NULL,p.QC=NULL,p.QCBy=NULL,p.ReviewDate=NULL,p.Priority =tmp.Priority,p.Mps=1, p.ParcelAlert =(CASE tmp.Priority WHEN 0 THEN NULL ELSE CASE WHEN tmp.MESSAGE='' THEN NULL ELSE tmp.MESSAGE END END),p.Committed=0, p.FailedOnDownSync = 0, p.FailedOnDownSyncStatus = 0, p.DownSyncRejectReason = NULL, p.DownloadStatus = 0 FROM Parcel p INNER JOIN PriorityListItems tmp ON tmp.CC_ParcelId = p.Id WHERE  tmp.PriorityListId ={0}".SqlFormatString(prioId))
                        Database.Tenant.Execute("UPDATE ParcelChanges SET QCApproved=0,QCChecked=0 WHERE ParcelId IN (SELECT CC_ParcelId FROM PriorityListItems WHERE PriorityListId = {0})".SqlFormatString(prioId))
                    End If
                    If createAdh Then

                        Dim AdhocPriority As String = priorityType
                        If AdhocPriority = "0" Or AdhocPriority = "" Then
                            Database.Tenant.Execute("INSERT INTO ParcelAuditTrail (ParcelId, LoginID, EventType, Description,ApplicationType) SELECT p.Id, " + UserName.ToSqlValue + ", 0, 'Parcel moved to group ' + n.Number  + ' via Priority List upload','Desktop' FROM Parcel p INNER JOIN  PriorityListItems tmp ON tmp.CC_ParcelId = p.Id INNER JOIN Neighborhood n on tmp.GroupName=n.Number WHERE tmp.PriorityListId ={0}".SqlFormatString(prioId))
                        Else
                            Dim Priority As String = ""
                            If EnableNewPriorities Then
                                Select Case AdhocPriority
                                    Case "1"
                                        Priority = "Normal"
                                    Case "2"
                                        Priority = "Medium"
                                    Case "3"
                                        Priority = "High"
                                    Case "4"
                                        Priority = "Urgent"
                                    Case "5"
                                        Priority = "Critical"
                                End Select
                            Else
                                Select Case AdhocPriority
                                    Case "1"
                                        Priority = "High"
                                    Case "2"
                                        Priority = "Urgent"
                                End Select
                            End If
                            Database.Tenant.Execute("INSERT INTO ParcelAuditTrail (ParcelId, LoginID, EventType, Description,ApplicationType) SELECT p.Id, " + UserName.ToSqlValue + ", 0, 'Parcel moved to group ' + n.Number  + ' and priority changed to " + Priority + " via Priority List upload','Desktop' FROM Parcel p INNER JOIN PriorityListItems tmp ON tmp.CC_ParcelId = p.Id INNER JOIN Neighborhood n on tmp.GroupName=n.Number WHERE tmp.PriorityListId ={0}".SqlFormatString(prioId))
                        End If
                    Else
                        If EnableNewPriorities Then
                            Database.Tenant.Execute("INSERT INTO ParcelAuditTrail (ParcelId, LoginID, EventType, Description,ApplicationType) SELECT p.Id, " + UserName.ToSqlValue + ", 0, 'Parcel moved to group '+ n.Number +' and priority changed to ' + CASE p.Priority WHEN 0 THEN 'Proximity' WHEN 1 THEN 'Normal' WHEN 2 THEN 'Medium' WHEN 3 THEN 'High' WHEN 4 THEN 'Urgent' WHEN 5 THEN 'Critical' ELSE 'Proximity' END + ' via Priority List upload','Desktop' FROM Parcel p INNER JOIN PriorityListItems tmp ON tmp.CC_ParcelId = p.Id INNER JOIN Neighborhood n on tmp.GroupName=n.Number WHERE tmp.PriorityListId ={0}".SqlFormatString(prioId))
                        Else
                            Database.Tenant.Execute("INSERT INTO ParcelAuditTrail (ParcelId, LoginID, EventType, Description,ApplicationType) SELECT p.Id, " + UserName.ToSqlValue + ", 0, 'Parcel moved to group '+ n.Number +' and priority changed to ' + CASE p.Priority WHEN 0 THEN 'Normal'  WHEN 1 THEN 'High' WHEN 2 THEN 'Urgent' ELSE 'Normal' END + ' via Priority List upload','Desktop' FROM Parcel p INNER JOIN PriorityListItems tmp ON tmp.CC_ParcelId = p.Id INNER JOIN Neighborhood n on tmp.GroupName=n.Number WHERE tmp.PriorityListId ={0}".SqlFormatString(prioId))
                            'Database.Tenant.Execute("INSERT INTO ParcelAuditTrail (ParcelId, LoginID, EventType, Description, ApplicationType) SELECT p.Id, " + UserName.ToSqlValue + ", 0, 'Parcel moved ' + CASE  WHEN n.Number IS NOT NULL THEN 'to group ' + n.Number  ELSE 'without assigning to any group' END +  ' and priority changed to ' + CASE p.Priority WHEN 0 THEN 'Normal' WHEN 1 THEN 'High'  WHEN 2 THEN 'Urgent' ELSE 'Normal' END + ' via Priority List upload', 'Desktop' FROM Parcel p INNER JOIN PriorityListItems tmp ON tmp.CC_ParcelId = p.Id LEFT JOIN Neighborhood n ON tmp.GroupName = n.Number WHERE tmp.PriorityListId = {0}".SqlFormatString(prioId))

                        End If
                        Database.Tenant.Execute("INSERT INTO ParcelAuditTrail (ParcelId, LoginID, EventType, Description,ApplicationType) SELECT p.Id, " + UserName.ToSqlValue + ", 0, 'Alert from Office Changed to: '+ tmp.message,'Desktop'  FROM Parcel p INNER JOIN PriorityListItems tmp ON tmp.CC_ParcelId = p.Id  WHERE tmp.PriorityListId ={0}".SqlFormatString(prioId))
                    End If

                    Dim dr As DataRow
                    If EnableNewPriorities Then
                        dr = Database.Tenant.GetTopRow("SELECT SUM(Case tmp.Priority WHEN 0 THEN 1 ELSE 0 END ) As RevokeCount, SUM(Case tmp.Priority WHEN 1 THEN 1 ELSE 0 END ) As PriorityCount, SUM(Case tmp.Priority WHEN 2 THEN 1 ELSE 0 END ) As AlertCount, SUM(Case tmp.Priority WHEN 3 THEN 1 ELSE 0 END ) As HCount, SUM(Case tmp.Priority WHEN 4 THEN 1 ELSE 0 END ) As UCount, SUM(Case tmp.Priority WHEN 5 THEN 1 ELSE 0 END ) As CCount FROM PriorityListItems tmp INNER JOIN Parcel p ON tmp.CC_ParcelId = p.Id WHERE tmp.PriorityListId ={0}".SqlFormatString(prioId))
                    Else
                        dr = Database.Tenant.GetTopRow("SELECT SUM(Case tmp.Priority WHEN 0 THEN 1 ELSE 0 END ) As RevokeCount, SUM(Case tmp.Priority WHEN 1 THEN 1 ELSE 0 END )As PriorityCount,SUM(Case tmp.Priority WHEN 2 THEN 1 ELSE 0 END )As AlertCount FROM PriorityListItems tmp INNER JOIN Parcel p ON tmp.CC_ParcelId = p.Id WHERE tmp.PriorityListId ={0}".SqlFormatString(prioId))
                    End If

                    If IsNothing(dr) = False Then
                        revokeCount = dr.GetInteger("RevokeCount")
                        priorityCount = dr.GetInteger("PriorityCount")
                        alertCount = dr.GetInteger("AlertCount")
                        If EnableNewPriorities Then
                            HCount = dr.GetInteger("HCount")
                            UCount = dr.GetInteger("UCount")
                            CCount = dr.GetInteger("CCount")
                        End If
                    End If
                    Database.Tenant.Execute("UPDATE p SET p.NeighborhoodId = n.Id FROM Parcel p INNER JOIN PriorityListItems tmp ON tmp.CC_ParcelId = p.Id INNER JOIN Neighborhood n on n.Number = tmp.GroupName WHERE  tmp.PriorityListId ={0}".SqlFormatString(prioId))
                    Database.Tenant.Execute("EXEC cc_UpdateNeighborhoodStatistics NULL, 1")
                    Dim content As String = GenerateCSV(multiGrp, prioId)
                    Database.Tenant.Execute("UPDATE  PriorityListBackup SET FileContent={0},Status={1},Error=NULL WHERE id={2}".SqlFormatString(content, 1, backupId))
                    Database.Tenant.Execute("UPDATE PriorityList SET Status ='Success' WHERE Id ={0}".SqlFormatString(prioId))
                End If
                If EnableNewPriorities Then
                    Database.Tenant.Execute("UPDATE  PriorityListBackup SET TotalParcels = {0}, Urgent = {1}, High = {2}, Normal = {3}, Proximity = {9}, Critical = {10}, Medium = {11}, GroupCode = {4}, AssignTo = {5}, Invalid = {7}, IsResetStatus = {8} WHERE id = {6}".SqlFormatString(String.Concat((alertCount + priorityCount + revokeCount + HCount + UCount + CCount).ToString, "/", TotalParcel), String.Concat(UCount, "/", Urgent), String.Concat(HCount, "/", High), String.Concat(priorityCount, "/", Normal), group, String.Join(",", assgn.ToArray()), backupId, Invalid, DonotReset, String.Concat(revokeCount, "/", Proximity), String.Concat(CCount, "/", Critical), String.Concat(alertCount, "/", Medium)))
                Else
                    Database.Tenant.Execute("UPDATE  PriorityListBackup SET TotalParcels = {0}, Urgent = {1}, High = {2}, Normal = {3}, GroupCode = {4}, AssignTo = {5}, Invalid = {7}, IsResetStatus = {8} WHERE id = {6}".SqlFormatString(String.Concat((alertCount + priorityCount + revokeCount).ToString, "/", TotalParcel), String.Concat(alertCount, "/", Urgent), String.Concat(priorityCount, "/", High), String.Concat(revokeCount, "/", Normal), group, String.Join(",", assgn.ToArray()), backupId, Invalid, DonotReset))
                End If

                Dim txtNeighborhoodName As String = CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName()
                Dim resultString As String = ""
                Dim result As New List(Of String)()
                If (CreateNbhd = "true" Or createAdh = "True") Then
                    Database.Tenant.Execute("UPDATE  PriorityListBackup SET IsAdhoc = 1 WHERE id={0}".SqlFormatString(backupId))
                    Dim grpTable As DataTable = Database.Tenant.GetDataTable("select GROUPNAME,assignTo,count(*) as NumerOfParcels from prioritylistitems where PriorityListId ={0} group by GROUPNAME,assignTo".SqlFormatString(prioId))
                    If grpTable.Rows.Count > 0 Then
                        For Each r As DataRow In grpTable.Rows
                            NotificationMailer.SendNotification(IIf(IsDBNull(r("assignTo")), "", r("assignTo")), txtNeighborhoodName + " assigned", txtNeighborhoodName + " " + r("GROUPNAME") + " assigned to you.", True)
                            Dim rstring As String = ""
                            rstring += r("GROUPNAME") + ":"
                            rstring += IIf(IsDBNull(r("assignTo")), "", r("assignTo")) + ":"
                            rstring += r("NumerOfParcels").ToString()
                            result.Add(rstring)
                        Next
                        If EnableNewPriorities Then
                            If grpTable.Rows.Count = 1 Then
                                resultString += (alertCount + priorityCount + revokeCount + HCount + UCount + CCount).ToString + " parcels moved to group " + group + IIf(assgn.Count = 0, ".", " and assigned to " + String.Join(",", assgn.ToArray()) + ".")
                            Else
                                resultString += (alertCount + priorityCount + revokeCount + HCount + UCount + CCount).ToString + " parcels moved to group " + group
                            End If
                        Else
                            If grpTable.Rows.Count = 1 Then
                                resultString += (alertCount + priorityCount + revokeCount).ToString + " parcels moved to group " + group + IIf(assgn.Count = 0, ".", " and assigned to " + String.Join(",", assgn.ToArray()) + ".")
                            Else
                                resultString += (alertCount + priorityCount + revokeCount).ToString + " parcels moved to group " + group
                            End If
                        End If
                    End If
                Else
                    resultString = (alertCount + priorityCount + revokeCount + HCount + UCount + CCount).ToString + " parcels updated successfully."
                End If
                Dim AffectedParcelCount As String
                Dim AffectedUrgentCount As String
                Dim AffectedHighCount As String
                Dim AffectedNormalCount As String
                Dim AffectedCriticalCount As String = ""
                Dim AffectedMeidumCount As String = ""
                Dim AffectedProximityCount As String = ""
                If EnableNewPriorities Then
                    AffectedParcelCount = String.Concat((alertCount + priorityCount + revokeCount + HCount + UCount + CCount).ToString, "/<font color='green'>", TotalParcel)
                    AffectedUrgentCount = String.Concat(UCount, "/<font color='green'>", Urgent)
                    AffectedHighCount = String.Concat(HCount, "/<font color='green'>", High)
                    AffectedNormalCount = String.Concat(priorityCount, "/<font color='green'>", Normal)
                    AffectedCriticalCount = String.Concat(CCount, "/<font color='green'>", Critical)
                    AffectedMeidumCount = String.Concat(alertCount, "/<font color='green'>", Medium)
                    AffectedProximityCount = String.Concat(revokeCount, "/<font color='green'>", Proximity)
                Else
                    AffectedParcelCount = String.Concat((alertCount + priorityCount + revokeCount).ToString, "/<font color='green'>", TotalParcel)
                    AffectedUrgentCount = String.Concat(alertCount, "/<font color='green'>", Urgent)
                    AffectedHighCount = String.Concat(priorityCount, "/<font color='green'>", High)
                    AffectedNormalCount = String.Concat(revokeCount, "/<font color='green'>", Normal)
                End If

                If Database.Tenant.GetStringValue("IF OBJECT_ID('job_UpdateNeighborhoodProfile', 'P') IS NOT NULL BEGIN SELECT 'true' END ELSE BEGIN SELECT 'false' END") = "true" Then
                    Try
                        Database.Tenant.Execute("EXEC [job_UpdateNeighborhoodProfile]")
                    Catch ex As Exception
                        Database.Tenant.Execute("UPDATE  PriorityListBackup SET Error={0} WHERE id={1}".SqlFormatString("Error on Update NeighborhoodProfile.Group profile not updated.", backupId))
                        'Return
                        'Throw
                    End Try
                End If
                If EnableNewPriorities Then
                    If updateBpp <> "0" Then
                        If (updateBpp = "6") Then
                            Dim qu1 As String = "INSERT INTO ParcelAuditTrail (ParcelId, LoginID, EventType, Description,ApplicationType) SELECT DISTINCT Id, " + UserName.ToSqlValue + ", 0, 'Related RP parcel priority changed to ' + pcl.PRIORITY + ' via  Priority List upload','Desktop' FROM Parcel pc JOIN (Select DISTINCT p.ParentParcelID, pl.PRIORITY FROM PriorityListItems pl Join Parcel p On pl.CC_ParcelId = p.Id WHERE pl.PriorityListId = " + prioId.ToString() + " AND p.IsPersonalProperty = 1 AND p.ParentParcelID Is Not NULL AND p.ParentParcelID NOT IN (SELECT CC_ParcelId FROM PriorityListItems WHERE PriorityListId = " + prioId.ToString() + ")) pcl ON pc.Id = pcl.ParentParcelID WHERE pc.Priority Not IN (1,2) AND pc.priority <> pcl.Priority"
                            Dim qu2 As String = "UPDATE pc SET Priority = pcl.PRIORITY FROM Parcel pc JOIN (Select DISTINCT p.ParentParcelID, pl.PRIORITY FROM PriorityListItems pl JOIN Parcel p On pl.CC_ParcelId = p.Id WHERE pl.PriorityListId = " + prioId.ToString() + " AND p.IsPersonalProperty = 1 AND p.ParentParcelID Is Not NULL AND p.ParentParcelID NOT IN (SELECT CC_ParcelId FROM PriorityListItems WHERE PriorityListId = " + prioId.ToString() + ")) pcl ON pc.Id = pcl.ParentParcelID  WHERE pc.Priority Not IN (1,2,3,4,5) AND  pc.priority <> pcl.Priority"
                            Database.Tenant.Execute(qu1)
                            Database.Tenant.Execute(qu2)
                        Else
                            Dim pty As String = ""
                            Select Case updateBpp
                                Case "1"
                                    pty = "Normal"
                                Case "2"
                                    pty = "Medium"
                                Case "3"
                                    pty = "High"
                                Case "4"
                                    pty = "Urgent"
                                Case "5"
                                    pty = "Critical"
                            End Select
                            Database.Tenant.Execute("INSERT INTO ParcelAuditTrail (ParcelId, LoginID, EventType, Description,ApplicationType) SELECT DISTINCT Id, " + UserName.ToSqlValue + ", 0, 'Related RP parcel priority changed to " + pty + " via  Priority List upload','Desktop' FROM Parcel WHERE Id In (Select DISTINCT p.ParentParcelID FROM PriorityListItems pl Join Parcel p On pl.CC_ParcelId = p.Id WHERE pl.PriorityListId = " + prioId.ToString() + " AND p.IsPersonalProperty = 1 AND p.ParentParcelID Is Not NULL AND p.ParentParcelID NOT IN (SELECT CC_ParcelId FROM PriorityListItems WHERE PriorityListId = " + prioId.ToString() + ")) And Priority Not IN (1,2,3,4,5)")
                            Database.Tenant.Execute("UPDATE Parcel SET Priority = " + updateBpp + " WHERE Id IN (Select DISTINCT p.ParentParcelID FROM PriorityListItems pl JOIN Parcel p On pl.CC_ParcelId = p.Id WHERE pl.PriorityListId = " + prioId.ToString() + " AND p.IsPersonalProperty = 1 AND p.ParentParcelID Is Not NULL AND p.ParentParcelID NOT IN (SELECT CC_ParcelId FROM PriorityListItems WHERE PriorityListId = " + prioId.ToString() + ")) And Priority Not IN (1,2,3,4,5)")
                        End If
                    End If
                Else
                    If updateBpp <> "0" Then
                        If (updateBpp = "3") Then
                            Dim qu1 As String = "INSERT INTO ParcelAuditTrail (ParcelId, LoginID, EventType, Description,ApplicationType) SELECT DISTINCT Id, " + UserName.ToSqlValue + ", 0, 'Related RP parcel priority changed to ' + pcl.PRIORITY + ' via  Priority List upload','Desktop' FROM Parcel pc JOIN (Select DISTINCT p.ParentParcelID, pl.PRIORITY FROM PriorityListItems pl Join Parcel p On pl.CC_ParcelId = p.Id WHERE pl.PriorityListId = " + prioId.ToString() + " AND p.IsPersonalProperty = 1 AND p.ParentParcelID Is Not NULL AND p.ParentParcelID NOT IN (SELECT CC_ParcelId FROM PriorityListItems WHERE PriorityListId = " + prioId.ToString() + ")) pcl ON pc.Id = pcl.ParentParcelID WHERE pc.Priority Not IN (1,2) AND pc.priority <> pcl.Priority"
                            Dim qu2 As String = "UPDATE pc SET Priority = pcl.PRIORITY FROM Parcel pc JOIN (Select DISTINCT p.ParentParcelID, pl.PRIORITY FROM PriorityListItems pl JOIN Parcel p On pl.CC_ParcelId = p.Id WHERE pl.PriorityListId = " + prioId.ToString() + " AND p.IsPersonalProperty = 1 AND p.ParentParcelID Is Not NULL AND p.ParentParcelID NOT IN (SELECT CC_ParcelId FROM PriorityListItems WHERE PriorityListId = " + prioId.ToString() + ")) pcl ON pc.Id = pcl.ParentParcelID  WHERE pc.Priority Not IN (1,2) AND  pc.priority <> pcl.Priority"
                            Database.Tenant.Execute(qu1)
                            Database.Tenant.Execute(qu2)
                        Else
                            Dim pty As String = IIf(updateBpp = "2", "Urgent", "High")
                            Database.Tenant.Execute("INSERT INTO ParcelAuditTrail (ParcelId, LoginID, EventType, Description,ApplicationType) SELECT DISTINCT Id, " + UserName.ToSqlValue + ", 0, 'Related RP parcel priority changed to " + pty + " via  Priority List upload','Desktop' FROM Parcel WHERE Id In (Select DISTINCT p.ParentParcelID FROM PriorityListItems pl Join Parcel p On pl.CC_ParcelId = p.Id WHERE pl.PriorityListId = " + prioId.ToString() + " AND p.IsPersonalProperty = 1 AND p.ParentParcelID Is Not NULL AND p.ParentParcelID NOT IN (SELECT CC_ParcelId FROM PriorityListItems WHERE PriorityListId = " + prioId.ToString() + ")) And Priority Not IN (1,2)")
                            Database.Tenant.Execute("UPDATE Parcel SET Priority = " + updateBpp + " WHERE Id IN (Select DISTINCT p.ParentParcelID FROM PriorityListItems pl JOIN Parcel p On pl.CC_ParcelId = p.Id WHERE pl.PriorityListId = " + prioId.ToString() + " AND p.IsPersonalProperty = 1 AND p.ParentParcelID Is Not NULL AND p.ParentParcelID NOT IN (SELECT CC_ParcelId FROM PriorityListItems WHERE PriorityListId = " + prioId.ToString() + ")) And Priority Not IN (1,2)")
                        End If
                    End If
                End If


                Database.Tenant.Execute("DELETE FROM PriorityListItems WHERE PriorityListId ={0}".SqlFormatString(prioId))
                Database.Tenant.Execute("DELETE FROM priotityFileRetain WHERE PriorityListId ={0}".SqlFormatString(prioId))
                Dim jstr As String = "{""AffectedParcelCount"": """ + AffectedParcelCount + """"
                jstr += ",""AffectedUrgentCount"": """ + AffectedUrgentCount + """"
                jstr += ",""AffectedHighCount"": """ + AffectedHighCount + """"
                jstr += ",""AffectedNormalCount"": """ + AffectedNormalCount + """"
                If EnableNewPriorities Then
                    jstr += ",""AffectedMediumCount"": """ + AffectedMeidumCount + """"
                    jstr += ",""AffectedProximityCount"": """ + AffectedProximityCount + """"
                    jstr += ",""AffectedCriticalCount"": """ + AffectedCriticalCount + """"
                End If
                jstr += ",""resultString"": """ + resultString + """"
                jstr += ",""multiGrp"": """ + multiGrp.ToString() + """"
                jstr += ",""Result"":" + JSON.GetListAsJson(result)
                jstr += "}"
                JSON.WriteString(jstr)
            Else
                    JSON.Error("Error Occurred")
            End If
        Catch ex As Exception
            JSON.Error(ex.Message)
        End Try

    End Sub


    Private Sub _viewInQc()
        Try
            Dim Id = Request("prioId")
            Dim selectedNbhd = Request("selectedNbhd")
            Database.Tenant.GetIntegerValue("UPDATE PriorityListItems SET isViewInQCNbhds=0 WHERE PriorityListId ={0}".SqlFormatString(Id))
            Database.Tenant.Execute("UPDATE PriorityListItems SET isViewInQCNbhds=1 WHERE NbhdId in (" & selectedNbhd & ") AND PriorityListId = {0}".SqlFormatString(Id))
            JSON.OK()
        Catch ex As Exception
            JSON.Error(ex.Message)
        End Try

    End Sub

    Private Sub _removeNBHD()
        Try
            Dim Id = Request("prioId")
            Dim removeNbhdIds As String = Request("selectedNbhd")
            Dim counts = Database.Tenant.GetIntegerValue("SELECT Count(*) FROM PriorityListItems WHERE NbhdId in (" & removeNbhdIds & ") AND isDuplicated=0 AND PriorityListId ={0}".SqlFormatString(Id))
            JSON.WriteString("{""counts"":" + counts + "}")
        Catch ex As Exception
            JSON.Error(ex.Message)
        End Try

    End Sub

    Private Sub _prioUpload()
        Try
            Dim backUpId As String = ""
            Dim user = Request("user")
            Dim grpwithAss As String = Request("grpwithAss")
            Dim grpCodeWithAssn As String() = Nothing
            If grpwithAss <> "" Then
                grpCodeWithAssn = grpwithAss.Split(New Char() {","c})
            End If
            Dim Tid As Integer
            Tid = Database.Tenant.GetIntegerValue("INSERT INTO PriorityList (UserName,Status) VALUES('" + user + "','Pending')Select @@IDENTITY ")
            Dim dt As New DataTable

            Dim pattern As String = "^\s*""?|""?\s*$"
            Dim rgx As New Regex(pattern)
            Dim rgx1 As New Regex(",(?=(?:[^""]*""[^""]*"")*[^""]*$)")
            Dim rgx2 As New Regex("\n(?=(?:[^""]*""[^""]*"")*[^""]*$)")
            Dim Fields As String()
            Dim Lines As String = Request("List")


            Dim Row As DataRow
            Dim header As String()
            Dim multiNbhd As Integer = 0

            Dim Les As String() = rgx2.Split(Lines).Where(Function(x) Not String.IsNullOrWhiteSpace(x)).ToArray()
            header = rgx1.Split(Les(0))

            For i As Integer = 0 To header.Length - 1
                dt.Columns.Add(rgx.Replace(header(i).ToUpper(), ""), GetType(String))
                If header(i).Replace(vbCr, "").ToUpper() = "GROUPNAME" Then
                    multiNbhd = 1
                End If
            Next
            For i As Integer = 1 To Les.GetLength(0) - 1
                If Les(i).Trim.Length = 0 Then
                    Continue For
                End If
                Fields = rgx1.Split(Les(i))
                Row = dt.NewRow()
                If Fields.Length < header.Length Then
                    Throw New Exception("Your list could not be uploaded because " + header(Fields.Length).ToString() + " column is missing in the " + IIf(i > 3, i.ToString() + "th", IIf(i > 1, IIf(i = 2, i.ToString() + "nd", i.ToString() + "rd"), i.ToString() + "st")) + " row in the list Or an invalid line break is present in the list.")
                End If
                If Fields.Length > header.Length Then ' field and header length should same
                    Throw New Exception("There are values in the Priority column in your list that require correction. Please update all values in the Priority column To 0, 1, 2, 3, 4 or 5 And Then Try again.")
                End If
                For j As Integer = 0 To Fields.Length - 1
                    Dim fval = Fields(j)
                    Try
                        If (fval IsNot Nothing AndAlso fval <> "" AndAlso fval.ToString().StartsWith("'")) Then
                            fval = fval.ToString().Remove(0, 1)
                            If (IsNumeric(fval)) AndAlso (((fval.ToString().Substring(0, 1) = "0") AndAlso (fval.ToString().Length > 1)) Or (fval.ToString().Length > 15)) Then
                                Fields(j) = fval
                            End If
                        End If
                    Catch ex As Exception
                    End Try
                    Row(j) = rgx.Replace(Fields(j), "")
                Next
                dt.Rows.Add(Row)
            Next
            Dim t As Integer
            If Database.Tenant.Application.ShowKeyValue1 = True Then
                t = 1
            Else
                t = 2
            End If
            For i = t To 10
                If Database.Tenant.Application.KeyField(i).IsNotEmpty Then
                    Dim s = Database.Tenant.Application.KeyField(i)
                    Dim ts = dt.Columns(s).ColumnName
                    dt.Columns(Database.Tenant.Application.KeyField(i)).ColumnName = "KEYVALUE" + i.ToString()
                End If
            Next
            If Database.Tenant.Application.ShowAlternateField = True Then
                dt.Columns(Database.Tenant.Application.AlternateKeyfield).ColumnName = "Alternatekeyfieldvalue"
            End If
            dt.Columns.Add("PriorityListId")
            For Each ro As DataRow In dt.Rows
                ro("PriorityListId") = Tid
            Next
            If (multiNbhd = 0) Then
                dt.Columns.Add("GROUPNAME")
            End If
            dt.Columns.Add("AssignTo")
            Dim GROUPNAME As String()
            Dim groupnameOnly As String = ""
            Dim grpWithAppraiser As New DataTable
            grpWithAppraiser.Columns.Add("GROUPNAME")
            grpWithAppraiser.Columns.Add("Appraiser")

            If multiNbhd = 1 Then
                If grpCodeWithAssn IsNot Nothing Then
                    For j = 0 To grpCodeWithAssn.Length - 1
                        GROUPNAME = grpCodeWithAssn(j).Split(New String() {"@!*!*$#@"}, StringSplitOptions.None)
                        Dim Rows As DataRow = grpWithAppraiser.NewRow()
                        For i = 0 To GROUPNAME.Length - 1
                            Rows(i) = rgx.Replace(GROUPNAME(i), "")
                        Next

                        For Each ro As DataRow In dt.Rows
                            Try
                                Dim grpName = ro("GROUPNAME").Replace(Environment.NewLine, "")
                                If grpName.Replace(vbLf, "") = rgx.Replace(GROUPNAME(0), "") Then
                                    ro("AssignTo") = rgx.Replace(GROUPNAME(1), "")
                                End If
                            Catch ex As Exception
                                Throw New Exception("Group name is missing in some rows in the list.")
                            End Try


                        Next
                        grpWithAppraiser.Rows.Add(Rows)
                    Next
                End If

            Else
                If grpCodeWithAssn IsNot Nothing Then
                    GROUPNAME = grpCodeWithAssn(0).Split(New String() {"@!*!*$#@"}, StringSplitOptions.None)
                    For Each ro As DataRow In dt.Rows
                        ro("AssignTo") = GROUPNAME(1)
                        ro("GROUPNAME") = GROUPNAME(0)
                    Next
                End If
            End If
            Database.Tenant.LoadTableFromDataTable("priotityFileRetain", dt)
            Database.Tenant.LoadTableFromDataTable("PriorityListItems", dt)
            Dim noprioandLongMsgTable As DataSet = Database.Tenant.ExecuteWithDatatableAndParams("SP_PriorityListKeyandMSg", grpWithAppraiser, Nothing, Tid)
            Dim dt1 As DataTable = noprioandLongMsgTable.Tables(0)
            Dim dt2 As DataTable = noprioandLongMsgTable.Tables(1)
            Dim dt3 As DataTable = noprioandLongMsgTable.Tables(2)
            Dim dt4 As DataTable = noprioandLongMsgTable.Tables(3)
            Dim longMsg As Boolean = False
            Dim longMsgKey As String = "null"
            Dim noPriority As Integer
            If dt1.Rows.Count > 1 Then
                longMsg = True
            ElseIf dt1.Rows.Count = 1 Then
                longMsg = True
                longMsgKey = dt1.Rows(0)("KEYFIELD")
            End If
            If dt2.Rows(0)(0) > 0 Then
                noPriority = dt2.Rows(0)(0)
            End If
            If dt3.Rows(0)(0) > 0 Then
                Throw New Exception("Your Ad-Hoc Assignment Group name exceeds the length allowed 50 characters. Please shorten the name to be 50 characters Or less.")
            End If
            If dt4.Rows(0)(0) > 0 Then
                If EnableNewPriorities Then
                    Throw New Exception("There are values in the Priority column in your list that require correction. Please update all values in the Priority column To 0, 1, 2, 3, 4 or 5 And Then Try again.")
                Else
                    Throw New Exception("There are values in the Priority column in your list that require correction. Please update all values in the Priority column To 0, 1, Or 2, And Then Try again.")
                End If
            End If

            Dim jstr As String = "{""prioId"": " + Tid.ToString()
            jstr += ",""longMsg"": """ + longMsg.ToString() + """"
            jstr += ",""longMsgKey"": """ + longMsgKey + """"
            jstr += ",""noPriority"": " + noPriority.ToString()
            jstr += "}"
            JSON.WriteString(jstr)
        Catch ex As Exception
            JSON.Error(ex.Message)
        End Try

    End Sub

    Private Sub _updateClient()
        Try
            Dim CSquery As String = "IF ((SELECT COUNT(*) FROM ClientSettings WHERE Name = 'DbUpdatedTime') = 0) INSERT INTO ClientSettings VALUES('DbUpdatedTime',(SELECT cast(Datediff(s, '1970-01-01', GETUTCDATE()) AS BIGINT)*1000)) ELSE UPDATE ClientSettings SET Value = (SELECT cast(Datediff(s, '1970-01-01', GETUTCDATE()) AS BIGINT)*1000) WHERE Name = 'DbUpdatedTime'"
            Database.Tenant.Execute(CSquery)
            JSON.OK()
        Catch ex As Exception
            JSON.Error(ex.Message)
        End Try

    End Sub

    Private Sub _priobackup()
        Try
            Dim isAdho As Boolean = False
            isAdho = Request("adhoc")
            Dim prioId = Request("prioId")
            Dim fileName = Request("fileName")
            Dim user = Request("user")
            Dim backupId = TakePriorityListBackup(fileName, user)
            JSON.WriteString("{""backupId"":" + backupId + "}")
        Catch ex As Exception
            JSON.Error(ex.Message)
        End Try

    End Sub

End Class
