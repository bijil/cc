﻿Public Class SketchViewProcessor
    Inherits RequestProcessorBase


    Public Overrides Sub ProcessRequest()
        Select Case Path
            Case "searchparcelids"
                _getParcelIds()
            Case "getparcelfromid"
                _getParcelDetails()
            Case "getclientsettings"
                _loadClientSettings()
            Case "loadfiltervalue"
                _loadFilterValue()
            Case "reviewsketch"
                _reviewsketch()
        End Select
    End Sub

    Private Sub _getParcelIds()
        Dim parcelno As String = Request("parcelno")
        Dim assnGroup As String = Request("assnGroup")
        Dim reviewedFrom As String = Request("reviewedFrom")
        Dim reviewedTo As String = Request("reviewedTo")
		Dim reviwedBy As String = Request("reviewedBy")
		Dim dtrStatus As String = Request("dtrStatus")
        Dim includeReviewedParcel As String = Request("includeReviewedparcels")
        Dim fieldQuery As String = ""
		Dim sketchFields As String = Request("sketchFields")

		'      Try
		'          Dim dtSketchSettings As DataTable = Database.Tenant.GetDataTable("SELECT * FROM ClientSettings WHERE Name IN ('SketchVectorTable', 'SketchVectorCommandField')")
		'          Dim svTable As String() = dtSketchSettings.Rows(0)(1).ToString().Split("/")
		'          Dim svField As String() = dtSketchSettings.Rows(1)(1).ToString().Split("/")
		'          Dim dataFields As DataTable = Database.Tenant.GetDataTable("SELECT Id, SourceTable FROM DataSourceField WHERE Name IN ('" & svField(0) & "','" & svField(1) & "')")
		'          For Each dfield In dataFields.Rows
		'              For i = 0 To svTable.Length - 1
		'                  If (dfield(1).ToString() = svTable(i)) Then
		'                      field += "'" + dfield(0).ToString() + "',"
		'                  End If
		'              Next
		'          Next
		'          If field.Length > 0 Then
		'              field = field.Remove(field.Length - 1)
		'          End If
		'      Catch

		'End Try

        If sketchFields IsNot Nothing AndAlso sketchFields <> "" Then
            sketchFields = "'" + String.Join("','", sketchFields.Split(",")) + "'"
            fieldQuery = "SELECT Id FROM DataSourceField WHERE SourceTable + ':' + Name IN (" + sketchFields + ")"
        End If

		Dim query As String = "SELECT DISTINCT TOP 200 p.Id, p.KeyValue1, p.StreetAddress, n.Number NbhdNo, CASE WHEN p.SketchViewReviewed = 1 THEN 'b' ELSE '' END As SVRClass, p.SketchViewReviewed FROM Parcel p INNER JOIN Neighborhood n ON p.NeighborhoodId = n.Id INNER JOIN ParcelChanges pc ON p.Id = pc.ParcelId"
		Dim condition As String = "p.Reviewed = 1 AND p.QC = 1 AND"
        If fieldQuery <> "" Then
            condition += " pc.FieldId IN (" & fieldQuery & ") AND"
        End If
        If parcelno <> "" Then
            condition += " p.KeyValue1 = '" & parcelno & "' AND"
        End If
        If assnGroup <> "all" Then
            condition += " n.Id = '" & assnGroup & "' AND"
        End If
        If reviwedBy <> "all" Then
            condition += " p.ReviewedBy = '" & reviwedBy & "' AND"
        End If
        If reviewedFrom <> "" Then
            condition += " p.ReviewDate >= '" & reviewedFrom & "' AND"
        End If
        If reviewedTo <> "" Then
            condition += " p.ReviewDate <= '" & reviewedTo & "' AND"
		End If
        If dtrStatus <> "all" And dtrStatus <> Nothing Then
			condition += " p.DTRStatus = " + dtrStatus.ToSqlValue + " AND"
        End If
        If includeReviewedParcel = "false" Then
			condition += " (p.SketchViewReviewed = 0 OR p.SketchViewReviewed IS NULL)"
        Else
            If condition.EndsWith("AND") Then
                condition = condition.Remove(condition.Length - 3)
            End If
        End If
        If condition <> "" Then
            condition = " WHERE " + condition
            query += condition
        End If
		JSON.SendTable(Database.Tenant, query)
    End Sub

    Private Sub _getParcelDetails()
        Dim sParcelId As String = Request("pid")
        Dim parcelId As Integer
        If Integer.TryParse(sParcelId, parcelId) Then

			Dim parcelSql = <sql>
SELECT 
	p.Id,
	KeyValue1 as ParcelID,
	KeyValue1,
	KeyValue2,
	SketchViewReviewed, 
	SketchViewReviewedBy,
	CONVERT(varchar(10), dbo.GetLocalDate(p.SketchViewReviewedOn), 101) As SketchViewReviewedOn, 
	StreetAddress, 
	n.Number As AssignmentGroup,
	st.Name +  ' - ' + st.Description As DTRStatus,
	CONCAT(us.FirstName + ' ', LTRIM(us.MiddleName + ' '), us.LastName) As ReviewedUser,
	CONVERT(varchar(10), dbo.GetLocalDate(p.ReviewDate), 101) As ReviewedDate,
	CONCAT(usq.FirstName + ' ', LTRIM(usq.MiddleName + ' '), usq.LastName) As QCUser
	FROM Parcel p  
	LEFT OUTER JOIN parceldata pd on p.id=pd.CC_parcelId 
	LEFT OUTER JOIN Neighborhood n ON p.NeighborhoodId = n.Id 
	LEFT OUTER JOIN DTR_StatusType st ON p.DTRStatus = st.Name
	LEFT OUTER JOIN UserSettings us ON p.ReviewedBy = us.LoginId
	LEFT OUTER JOIN UserSettings usq ON p.QCBy = usq.LoginId
	WHERE p.Id = {0}
</sql>

			Dim parcelData As DataTable = Database.Tenant.GetDataTable(parcelSql.Value.SqlFormat(False, parcelId))
            
			If parcelData.Rows.Count = 0 Then
				JSON.Error("Parcel not found")
				Return
			End If

			Dim jstr As String = JSON.GetRowAsJSON(parcelData.Rows(0)).TrimEnd("}")

			Dim parcelchanges As DataTable = Database.Tenant.GetDataTable("SELECT pc.*, fc.Id As CategoryId, f.Name As FieldName, f.SourceTable, dbo.GetLocalDate(pc.ChangedTime) as ChangedTimeNew, dbo.GetLocalDate(pc.ChangedTime) as LocalChangedTime FROM ParcelChanges pc LEFT OUTER JOIN DataSourceField f ON pc.FieldId = f.Id LEFT OUTER JOIN FieldCategory fc ON f.CategoryId = fc.Id WHERE ParcelId = " + sParcelId + " ORDER BY ChangedTime")
			jstr += ",""ParcelChanges"":" + JSON.GetTableAsJSON(parcelchanges)
			Dim relatedTableNames As New List(Of String)
			If Database.Tenant.Application.IsAPISyncModel Then
				Dim primaryTable As String = Database.Tenant.Application.ParcelTable
				relatedTableNames = GetRelatedTables(Database.Tenant)
			Else
				For Each dr As DataRow In Database.Tenant.GetDataTable("SELECT Name FROM DataSourceTable WHERE ImportType = 1").Rows
					relatedTableNames.Add(dr.GetString("Name"))
				Next
			End If

			Dim tables As New List(Of String)
			Dim missingTables As New List(Of String)
			Dim auxFetchSql As String = ""
			For Each tableName In relatedTableNames
				Dim sqlTableName As String = "XT_" + tableName
				If Database.Tenant.DoesTableExists(sqlTableName) Then
					Dim sortFormula = FieldCategory.GetSortExpression(tableName)
					'Dim tableData As String = JSON.GetTableAsJSON(Database.Tenant, "SELECT * FROM " + sqlTableName + " WHERE (CC_Deleted = 0 OR CC_Deleted IS NULL) AND CC_ParcelId = " & parcelId)
					Dim auxSql As String = "SELECT * FROM " + sqlTableName + " WHERE " + " CC_ParcelId = " & parcelId & "" & sortFormula + ";"
					auxFetchSql += auxSql
					'Dim tableData As String = JSON.GetTableAsJSON(Database.Tenant, auxSql)
					tables.Add(tableName)
					'jstr += ",""" + tableName + """:" + tableData
				Else
					missingTables.Add(tableName)
					'jstr += ",""" + tableName + """: []"
				End If
			Next

			If auxFetchSql.Trim <> "" Then
				Dim auxDataSet As DataSet = Database.Tenant.GetDataSet(auxFetchSql)
				For i As Integer = 0 To tables.Count - 1
					jstr += ",""" + tables(i) + """:" + JSON.GetTableAsJSON(auxDataSet.Tables(i))
				Next
			End If

			For Each mt As String In missingTables
				jstr += ",""" + mt + """: []"
			Next

            jstr += "}"
            JSON.WriteString(jstr)
        Else
            JSON.Error("Parcel not found")
        End If
    End Sub

    Private Sub _loadClientSettings()
        Dim dtClientSettings As DataTable = Database.Tenant.GetDataTable("SELECT * FROM ClientSettings")
        Dim jstr As String = ""
        If dtClientSettings.Rows.Count = 0 Then
            JSON.Error("Client settings not found")
            Return
        End If
        JSON.SendTable(dtClientSettings)
    End Sub
    Public Function GetRelatedTables(ByVal db As Database) As List(Of String)
        Dim tables As New Dictionary(Of String, Integer)
        _getConnectedTables(db, tables, db.Application.ParcelTable)
        Return tables.Where(Function(x) x.Value = 1 Or x.Value = 2).Select(Function(x) x.Key).ToList
    End Function

    Private Sub _getConnectedTables(ByVal db As Database, ByVal tables As Dictionary(Of String, Integer), ByVal tableName As String)
        For Each connectTableName In db.GetDataTable("SELECT t.Name, r.Relationship FROM DataSourceRelationships r INNER JOIN DataSourceTable t ON r.TableId = t.Id INNER JOIN DataSourceTable p ON r.ParentTableId = p.Id WHERE p.Name = '{0}' ORDER BY Relationship".FormatString(tableName)).Rows.OfType(Of DataRow).Select(Function(x) New With {.TableName = x.GetString("Name"), .Relationship = x.GetInteger("Relationship")}).ToArray
            If Not tables.ContainsKey(connectTableName.TableName) Then
                tables.Add(connectTableName.TableName, connectTableName.Relationship)
            End If
            _getConnectedTables(db, tables, connectTableName.TableName)
        Next
    End Sub

    Private Sub _loadFilterValue()
		Dim ds As DataSet = Database.Tenant.GetDataSet("SELECT Id, Number Name FROM Neighborhood; SELECT LoginId Id, FirstName + COALESCE(' ' + LastName, '') As Name FROM USERSETTINGS; SELECT Name As Id, CONCAT(Name, ' - ',  Description) As Name FROM DTR_StatusType ORDER BY Name ")
        Dim neighborhood As DataTable = ds.Tables(0)
		Dim userSettings As DataTable = ds.Tables(1)
		Dim dtrStatusTypes As DataTable = ds.Tables(2)
        Dim jstr As String = "{""Neighborhood"": " + JSON.GetTableAsJSON(neighborhood)
		jstr += ",""UserSettings"":" + JSON.GetTableAsJSON(userSettings)
		jstr += ",""DTRStatusTypes"":" + JSON.GetTableAsJSON(dtrStatusTypes)
        jstr += "}"
        JSON.WriteString(jstr)
    End Sub

    Private Sub _reviewsketch()
        Dim ParcelId As String = Request("ParcelId")
        Dim sketchViewReviewed As String = Request("sketchViewReviewed")
        If (sketchViewReviewed = "true") Then
			Database.Tenant.Execute("UPDATE Parcel SET SketchViewReviewed = 0,SketchViewReviewedBy = NULL,SketchViewReviewedOn = NULL WHERE Id='" & ParcelId & "'")
        Else
			Database.Tenant.Execute("UPDATE Parcel SET SketchViewReviewed = 1,SketchViewReviewedBy = '" & UserName & "',SketchViewReviewedOn =" & Date.UtcNow.ToSqlValue & " WHERE Id='" & ParcelId & "'")
        End If
        JSON.OK()
    End Sub

End Class
