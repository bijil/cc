﻿Imports System.Net
Imports System.Web.Configuration
Imports System.Configuration
Imports System.IO
Imports CAMACloud.Routing
Imports Newtonsoft.Json

Public Class MobileAssessorProcessor
    Inherits RequestProcessorBase


    Public ReadOnly Property SqlNbhdForSp As String
        Get
            If Request("nbhd") <> Nothing Then
                Return Request("nbhd").ToString()
            End If
            Return ""
        End Get
    End Property
    Public ReadOnly Property EnableNewPriorities As Boolean
        Get
            Dim EnableNewPriority = False
            Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM ClientSettings WHERE Name = 'EnableNewPriorities' AND Value = 1")
            If dr IsNot Nothing Then
                EnableNewPriority = True
            End If
            Return EnableNewPriority
        End Get
    End Property

    Public ReadOnly Property SqlNbhdForQuery As String
        Get
            Dim tempNbhd As String = Request("nbhd").ToString()
            tempNbhd = tempNbhd.Replace("||", "','")
            Return tempNbhd
        End Get
    End Property

    Public ReadOnly Property NeighborhoodId As String
        Get
            Dim tempNbhd As String = ""
            Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT Id FROM Neighborhood WHERE Number IN ('" & SqlNbhdForQuery & "')")
            For Each dr As DataRow In dt.Rows
                tempNbhd += (dr(0).ToString() + ",")
            Next
            If tempNbhd.Length > 0 Then
                tempNbhd = "'" + tempNbhd.Trim().Remove(tempNbhd.Length - 1) + "'"
                tempNbhd = tempNbhd.Replace(",", "','")
            End If
            Return tempNbhd
        End Get
    End Property
    Public ReadOnly Property NeighborhoodName As String
        Get
            Dim tempNbhd As String = Database.Tenant.GetStringValue("SELECT STRING_AGG(Name, ',')  FROM Neighborhood WHERE Number IN ('" & SqlNbhdForQuery & "')")
            Return tempNbhd
        End Get
    End Property

    Public ReadOnly Property NeighborhoodIdArray As Integer()
        Get
            Dim tempNbhd As Integer() = New Integer(0) {}
            Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT Id FROM Neighborhood WHERE Number IN ('" & SqlNbhdForQuery & "')")
            For Each dr As DataRow In dt.Rows
                tempNbhd(tempNbhd.Length - 1) = Convert.ToInt32(dr(0).ToString)
                Array.Resize(tempNbhd, tempNbhd.Length + 1)
            Next
            Array.Resize(tempNbhd, tempNbhd.Length - 1)
            Return tempNbhd
        End Get
    End Property

    Public cacheList As New List(Of String)
    Public basePath As String = ""
    Public excludePaths As New List(Of String)
    Public maxModDate As Date = #1/1/2010#

    Public Overrides Sub ProcessRequest()

        Try
            If Path <> "getcachefiles" AndAlso Path <> "getcacheversion" Then
                CAMACloud.Security.DeviceLicense.ValidateRequest(HttpContext.Current.ApplicationInstance)
            End If
        Catch ex As Exception
            Dim calAuthPage As String = ApplicationSettings.CALAuthorityPageUrl
            JSON.Write(New With {.LoginStatus = "423", .RedirectURL = calAuthPage})
            Return
        End Try
        If Path = "licenseagreement" Then
            JSON.SendTable(Database.System, "select AgreementContent from Vendor where Id=(select VendorId from Organization where id={0}) ".FormatString(HttpContext.Current.GetCAMASession.OrganizationId), allowReplace:=False)
            Return
        End If

        If Path = "recorderrorlog" Then
            _recordErrorLog()
            Return
        End If

        If Path = "geterrorlog" Then
            _geterrorlog()
            Return
        End If

        If Path = "clearerrorlog" Then
            Database.Tenant.Execute("DELETE FROM ApplicationLogHistory")
            JSON.OK()
            Return
        End If

        If Request("HIVCSKL897") <> "^GHIIj88JJHg6fj" And Path <> "getcachefiles" And Path <> "getcacheversion" Then
            If Not Request.IsAuthenticated Then
                JSON.Write(New With {.LoginStatus = "401"})
                Try
                    Throw New Exception("Unauthenticated connection attempt from mobile.")
                Catch ex As Exception
                    ' ErrorMailer.ReportException("MA-Connect", ex, Request)
                End Try
                Return
            Else
                'Debug.Print("REQUEST IS AUTHENTICATED")
            End If
        End If

        Dim isSyncEvent As Boolean = False
        Try
            Select Case Path
                Case "downloadphoto"
                    _getphotofromID()
                Case "check"
                    JSON.OK()
                Case "tquery"
                    _downloadTableAsJSONFromRequest()
                Case "fieldcatagoryupdation"
                    _categoryUpdation()
                'BEGIN Update CAMA Data Related JRQs 
                Case "parquery"
                    isSyncEvent = True
                    _queryParcels()
                Case "myparcels"
                    isSyncEvent = True
                    _getParcelsPage()
                Case "parcelsubtables"
                    isSyncEvent = True
                    _getAuxiliariesPage()
                Case "lookupdatatables"
                    _getLookupDataTables()
                Case "getparcelchanges"
                    isSyncEvent = True
                    _getParcelChangesPage()
                Case "parcelmap"
                    isSyncEvent = True
                    _getMapPoints()
                Case "parcelimages"
                    isSyncEvent = True
                    _getImages()
                Case "parcelsketches"
                    isSyncEvent = True
                    _getParcelSketches()
                Case "parcelsketchvectors"
                    isSyncEvent = True
                    _getParcelSketchVectors()
                Case "comparables"
                    isSyncEvent = True
                    _getParcelsComparables()
                Case "parcellinks"
                    isSyncEvent = True
                    _getParcelsLinks()
                Case "getnbhds"
                    _getNbhds()
                '                Case "testworker"
                '                	_testWorker()
                Case "nbhdstats"
                    isSyncEvent = True
                    If NeighborhoodId.Length > 0 Then
                        Dim query As String = "UPDATE Neighborhood SET LastVisitedDate = {0} WHERE Number IN ('" & SqlNbhdForQuery & "')"
                        DB.Execute(query.SqlFormat(False, Date.UtcNow)) 'need to recheck this
                        'Neighborhood.SetLastVisitedDate(DB, NeighborhoodId) 'need to recheck this
                        query = "DELETE FROM UserTrackingCurrent  WHERE LoginId = {0};".SqlFormatString(SqlUserName.Replace("'", ""))
                        For Each n As Integer In NeighborhoodIdArray
                            query += "INSERT INTO UserTrackingCurrent (LoginId, NbhdId) VALUES ({0},{1}); ".SqlFormat(False, SqlUserName.Replace("'", ""), n)
                        Next
                        DB.Execute(query)
                    End If
                    JSON.SendTable(Database.Tenant, "EXEC ccma_GetNeighborhoodStats " + SqlNbhdForSp.ToSqlValue + ", " + SqlUserName)
                Case "rplinkedbpps"
                    isSyncEvent = True
                    _getrplinkedbpps()
                Case "parcelgispoints"
                    isSyncEvent = True
                    _getParcelGISPoints()
                'END Update CAMA Data Related JRQs
                Case "agreenbhd"
                    _agreeNbhd()
                Case "parcelchanges"
                    _recordParcelChanges()
                Case "gisupdate"
                    _collectTrackingInfo()
                Case "route"
                    isSyncEvent = True
                    _calculateRouteUsingMapQuest()
                Case "route_new"
                    isSyncEvent = True
                    _calculateRouteUsingMapQuest_new()
                Case "photosync"
                    _syncImage()
                Case "synccomplete"
                    _syncComplete()
                Case "getforceupdate"
                    _getForceUpdation()
                Case "updateforceupdate"
                    _updateForceUpdate()
                Case "testparcelstosort"
                    JSON.SendTable(Database.Tenant, "EXEC temp_ParcelsToSort")
                Case "getcachefiles"
                    _getCacheFiles()
                Case "getcacheversion"
                    _getCacheVersion()
                Case "createnbhddownloadlog"
                    _createNbhdDownloadLog()
                Case "inputtypes"
                    _getGlobalInputTypes()
                    '            	Case "initiatedb"
                    '					JSON.SendTable(Database.Tenant, "EXEC [ccma_GetTableDetails] '" + Request("type").ToString() + "'")
                Case Else
                    JSON.Error("Invalid Request Exception")
            End Select

            If isSyncEvent Then
                Dim pageNo As Integer = 0
                Integer.TryParse(Request("page"), pageNo)
                _registerSyncEvent(Path, "", pageNo)
            End If
        Catch ex As Exception
            ErrorMailer.ReportException(UserName, ex, Request)
            JSON.Error(ex)
        End Try

    End Sub

    '    Private Sub _testWorker()
    '    	Dim values As String()=Request("val").Split(",")
    '    	Dim result As Integer= Convert.ToInt32(values(0))+Convert.ToInt32(values(1))
    '    	JSON.Write(New With {.result = "Result: " + result.ToString() + "", .status = "OK"})
    '    End Sub

    Private Sub _downloadTableAsJSONFromRequest()
        Dim tableName As String = Request("t")
        Select Case tableName
            Case "FieldCategory"
                JSON.SendTable(FieldCategory.GetFieldCategories)
            Case "CategorySettings"
                Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT * FROM FieldCategoryProperties")
                MobileSyncDownloadProcessor.AddBlankRowIfNoRows(dt)
                JSON.SendTable(dt)
            Case "Field"
                JSON.SendTable(FieldCategory.GetFields, allowReplace:=False)
            Case "FieldSettings"
                Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT * FROM DataSourceFieldProperties")
                MobileSyncDownloadProcessor.AddBlankRowIfNoRows(dt)
                JSON.SendTable(dt)
            Case "FieldInputType"
                JSON.SendTable(Database.System, "SELECT * FROM GlobalFieldInputTypes")
            Case "Neighborhood"
                JSON.SendTable(Database.Tenant, "SELECT * FROM Neighborhood n LEFT OUTER JOIN NeighborhoodData nd ON n.Id = nd.NbhdId WHERE Number In ('" & SqlNbhdForQuery & "')")
            Case "LookupValue"
                'JSON.SendTable(Database.Tenant, "SELECT Id, LookupName AS Source, IdValue AS Value, NameValue AS Name, DescValue AS Description, Ordinal FROM dbo.ParcelDataLookup")
                _getLookupValues(Coalesce(Request("p"), 0))
            Case "ImportSettings", "ClientSettings"
                JSON.SendTable(Database.Tenant, "SELECT Name,Value FROM ClientSettings UNION SELECT 'ShowKeyValue1' as Name, CONVERT(varchar(5),ShowKeyValue1) as Value from Application UNION SELECT 'ShowAlternateField' as Name, CONVERT(varchar(5),ShowAlternateField) as Value from Application UNION SELECT 'AlternateKey' as Name, Alternatekeyfieldvalue as Value from Application")
            Case "SketchSettings"
                Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT * FROM SketchSettings")
                MobileSyncDownloadProcessor.AddBlankRowIfNoRows(dt)
                JSON.SendTable(dt)
            Case "ScreenMenu"
                JSON.SendTable(Database.Tenant, "SELECT * FROM ScreenMenu WHERE Show = 1")
            Case "Streets"
                JSON.SendTable(Database.Tenant, "SELECT DISTINCT p.StreetName + COALESCE(' ' + p.StreetNameSuffix, '') As Id, p.StreetName Name, COALESCE(' ' + p.StreetNameSuffix, '') Suffix FROM Parcel p LEFT OUTER JOIN Neighborhood n ON p.NeighborhoodId = n.Id WHERE n.Number IN ('{0}') ORDER BY p.StreetName, 3".FormatString(SqlNbhdForQuery))
            Case "ClientValidation"
                Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT * FROM ClientValidation")
                MobileSyncDownloadProcessor.AddBlankRowIfNoRows(dt)
                JSON.SendTable(dt)
            Case "TableKeys"
                JSON.SendTable(Database.Tenant, "SELECT f.Name,t.Name as SourceTable FROM DataSourceField f INNER JOIN  DataSourceKeys k ON f.Id = k.FieldId INNER JOIN  DataSourceRelationships r ON k.RelationshipId = r.Id INNER JOIN DataSourceTable t ON r.TableId = t.Id WHERE isPrimaryKey=1 ")
            Case "AggregateFieldSettings"
                JSON.SendTable(MobileSyncDownloadProcessor.AggregateFieldSettings())
            Case "FieldAlertTypes"
                JSON.SendTable(Database.Tenant, "SELECT * FROM FieldAlertTypes ORDER BY Name")
            Case "ParentChildTable"
                JSON.SendTable(DataSource.GetParentChildTable)
            Case "StreetMapPoints"
                _getStreetMapPoints()
            Case "heatmap"
                Dim dt As DataTable = MobileSyncDownloadProcessor.heatmapFields()
                JSON.SendTable(dt)
            Case "heatmaplookup"
                Dim dt As DataTable = MobileSyncDownloadProcessor.heatmapLookup()
                JSON.SendTable(dt)
        End Select
        _registerSyncEvent(Path, tableName, 0)
    End Sub

    Private Sub _getLookupValues(page As Integer, Optional pageSize As Integer = 2000)
        If page = 0 Then
            pageSize = 20000
        End If
        Dim si As Integer = (page - 1) * pageSize
        Dim ei As Integer = page * pageSize - 1
        Dim sql As String = "SELECT Id, LookupName AS Source, IdValue AS Value, NameValue AS Name, DescValue AS Description,ColorCode as Color, Ordinal,value as NumericValue, AdditionalValue1, AdditionalValue2 FROM dbo.ParcelDataLookup ORDER BY Id OFFSET " & si & " ROWS FETCH NEXT " & pageSize & " ROWS ONLY"
        Dim dt As DataTable = Database.Tenant.GetDataTable(sql)
        If page = 1 Then
            MobileSyncDownloadProcessor.AddBlankRowIfNoRows(dt)
        End If
        Dim EnableBlankValue As String = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name = 'EnableBlankValue'")
        JSON.SendTable(dt, allowReplace:=False)
    End Sub
    Private Sub _getphotofromID()
        Dim path As String = Database.Tenant.GetStringValue("select path from ParcelImages where id={0}".SqlFormatString(Request("pid")))
        '   JSON.WriteString(MobileSyncDownloadProcessor.ConvertPhotoToBase64(path))
        '  JSON.OK()
        JSON.Write(New With {.value = "" + MobileSyncDownloadProcessor.ConvertPhotoToBase64(path) + "", .status = "OK"})
    End Sub
    Private Sub _queryParcels()
        JSON.SendDataRow(Database.Tenant, "EXEC ccma_PrepareParcelsForSync " + SqlUserName + ", " + SqlNbhdForSp.ToSqlValue + ", " + Request("downloadLimit").ToString())
    End Sub
    Private Sub _categoryUpdation()
        Dim data = Request("data")
        _recordDeviceSyncStatus(data)
        Dim value As String = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name = 'LastSchemaUpdatedCounter'")
        JSON.OK(value)
    End Sub

    Private Sub _getForceUpdation()
        Dim lastForceCounter As String = Request("lastForceCounter")
        Dim dt As DataTable = Database.System.GetDataTable("SELECT f.UpdateId, p.UpdateType FROM ForceUpdateApplicationStatus f JOIN ForceUpdateApplication p on p.Id = f.UpdateId WHERE f.orgId = " + HttpContext.Current.GetCAMASession.OrganizationId.ToString() + " AND f.UpdateId > " + lastForceCounter + " ORDER BY f.UpdateId")
        JSON.SendTable(dt, allowReplace:=False)
    End Sub

    Private Sub _updateForceUpdate()
        Dim lastForceCounter As String = Request("lastForceCounter")
        Dim type = Request("type")
        Dim mxId As Integer = 0
        Dim updateIds As String = ""
        Try
            Dim dt As DataTable = Database.System.GetDataTable("SELECT f.UpdateId, p.UpdateType FROM ForceUpdateApplicationStatus f JOIN ForceUpdateApplication p on p.Id = f.UpdateId WHERE f.orgId = " + HttpContext.Current.GetCAMASession.OrganizationId.ToString() + " AND f.UpdateId > " + lastForceCounter + " ORDER BY f.UpdateId")

            For Each dr As DataRow In dt.Rows
                If type = 1 Then
                    mxId = dr.GetInteger("UpdateId")
                    updateIds = updateIds + dr.GetString("UpdateId") + ","
                ElseIf type = 2 AndAlso dr.GetString("UpdateType") = "Update Data & Assignment Groups" Then
                    mxId = dr.GetInteger("UpdateId")
                    updateIds = updateIds + dr.GetString("UpdateId") + ","
                ElseIf type = 3 AndAlso dr.GetString("UpdateType") = "Update Application/Update System Data" Then
                    mxId = dr.GetInteger("UpdateId")
                    updateIds = updateIds + dr.GetString("UpdateId") + ","
                End If
            Next
            If (mxId > 0) And updateIds <> "" Then
                updateIds = updateIds.Remove(updateIds.Length - 1)
                Database.System.GetDataTable("UPDATE ForceUpdateApplicationStatus SET [Status] = 1, [UpdateDate] = GETUTCDATE() WHERE orgId = " + HttpContext.Current.GetCAMASession.OrganizationId.ToString() + " AND UpdateId In (" + updateIds + ") ")
            End If
            JSON.OK(mxId)
        Catch ex As Exception
            JSON.Error(ex.Message)
        End Try

    End Sub

    Private Sub _getParcelsPage()
        Dim nbhdStatsId As Integer = 0
        If Request("nbhdStatsId") IsNot Nothing Then
            nbhdStatsId = Request("nbhdStatsId")
        End If
        JSON.SendTable(Database.Tenant, "EXEC ccma_SyncParcels @LoginID = {0}, @Nbhd = {1}, @Page = {2}, @NbhdStatsId = {3}".SqlFormatString(UserName, Request("nbhd"), Request("page"), nbhdStatsId))
    End Sub
    Private Sub _getParcelsComparables()
        JSON.SendTable(Database.Tenant, "EXEC [ccma_SyncComparables] @LoginID = {0}, @Nbhd = {1}, @Page = {2}".SqlFormatString(UserName, Request("nbhd"), Request("page")))
    End Sub

    Private Sub _getParcelsLinks()
        JSON.SendTable(Database.Tenant, "EXEC [ccma_SyncParcelLinks] @LoginID = {0}, @Nbhd = {1}, @Page = {2}".SqlFormatString(UserName, Request("nbhd"), Request("page")))
    End Sub

    Private Sub _getParcelSketches()
        JSON.SendTable(Database.Tenant, "EXEC [ccma_SyncParcelSketches] @LoginID = {0}, @Nbhd = {1}, @Page = {2}".SqlFormatString(UserName, Request("nbhd"), Request("page")))
    End Sub

    Private Sub _getParcelSketchVectors()
        JSON.SendTable(Database.Tenant, "EXEC [ccma_SyncParcelSketchVectors] @LoginID = {0}, @Nbhd = {1}, @Page = {2}".SqlFormatString(UserName, Request("nbhd"), Request("page")))
    End Sub

    Private Sub _getAuxiliariesPage()
        JSON.StartTick()
        Dim objectCount As Integer = 0
        Dim items As New List(Of String)
        Dim relatedTableNames As New List(Of String)
        If Database.Tenant.Application.IsAPISyncModel Then
            Dim primaryTable As String = Database.Tenant.Application.ParcelTable
            relatedTableNames = GetRelatedTables(Database.Tenant)
        Else
            For Each dr As DataRow In Database.Tenant.GetDataTable("SELECT Name FROM DataSourceTable WHERE ImportType = 1").Rows
                relatedTableNames.Add(dr.GetString("Name"))
            Next
        End If

        For Each tableName In relatedTableNames
            Dim sqlTableName As String = "XT_" + tableName
            If Database.Tenant.DoesTableExists(sqlTableName) Then
                'Dim tableData As String = JSON.GetTableAsJSON( Database.Tenant, "EXEC ccma_SyncAuxiliary {0}, {1}, {2}, {3}".SqlFormatString(UserName, Request("nbhd"), tableName, Request("page")))
                Dim tableData As String = JSON.GetTableAsJSON(MobileSyncDownloadProcessor.SyncAuxiliary(UserName, SqlNbhdForQuery, tableName, Request("page")))
                objectCount += JSON.ObjectsAffected
                If tableData <> "[]" Then
                    items.Add(tableData.TrimStart("[").TrimEnd("]"))
                End If
            End If
        Next
        Dim js As String = "[" + String.Join(", ", items.ToArray) + "]"
        JSON.WriteString(js)
        JSON.EndTick()
        JSON.SetObjectCount(objectCount)
    End Sub

    Public Function GetRelatedTables(ByVal db As Database) As List(Of String)
        Dim tables As New Dictionary(Of String, Integer)
        _getConnectedTables(db, tables, db.Application.ParcelTable)
        Return tables.Where(Function(x) x.Value = 1 Or x.Value = 2).Select(Function(x) x.Key).ToList
    End Function

    Private Sub _getConnectedTables(ByVal db As Database, ByVal tables As Dictionary(Of String, Integer), ByVal tableName As String)
        For Each connectTableName In db.GetDataTable("SELECT t.Name, r.Relationship FROM DataSourceRelationships r INNER JOIN DataSourceTable t ON r.TableId = t.Id INNER JOIN DataSourceTable p ON r.ParentTableId = p.Id WHERE p.Name = '{0}' ORDER BY Relationship".FormatString(tableName)).Rows.OfType(Of DataRow).Select(Function(x) New With {.TableName = x.GetString("Name"), .Relationship = x.GetInteger("Relationship")}).ToArray
        If Not tables.ContainsKey(connectTableName.TableName) Then
                tables.Add(connectTableName.TableName, connectTableName.Relationship)
            End If
            _getConnectedTables(db, tables, connectTableName.TableName)
        Next
    End Sub

    Private Sub _getParcelChangesPage()
        Dim dt As DataTable = MobileSyncDownloadProcessor.SyncParcelChanges(UserName, Request("nbhd"), Request("page"))
        JSON.SendTable(dt)
    End Sub

    Private Sub _getMapPoints()
        Dim dt As DataTable = MobileSyncDownloadProcessor.SyncParcelMapPoints(UserName, SqlNbhdForSp, Request("page"))
        JSON.SendTable(dt)
    End Sub
    Private Sub _getStreetMapPoints()
        Dim dt As DataTable = MobileSyncDownloadProcessor.syncStreetMapPoints(NeighborhoodId.Replace("','", ","))
        JSON.SendTable(dt)
    End Sub
    Private Sub _getrplinkedbpps()
        Dim dt As DataTable = MobileSyncDownloadProcessor.rplinkedbpps(Request("nbhd"))
        JSON.SendTable(dt)
    End Sub
    Private Sub _getParcelGISPoints()
        Dim dt As DataTable = MobileSyncDownloadProcessor.parcelGISPoints(UserName, SqlNbhdForSp, Request("page"))
        JSON.SendTable(dt)
    End Sub
    Private Sub _getImages()
        Dim isBPPAppraiser As Boolean = Database.Tenant.GetIntegerValue("SELECT IsBPPAppraiser  from UserSettings where loginId ={0} ".SqlFormatString(UserName))
        Dim BPPType = Database.Tenant.GetStringValue("SELECT value from ClientSettings WHERE name= 'PersonalPropertyDownloadType'")
        Dim dt As DataTable = MobileSyncDownloadProcessor.SyncParcelImages(UserName, SqlNbhdForQuery, Request("page"), Request("imageLimit"))
        If (isBPPAppraiser = True) Then
            MobileSyncDownloadProcessor.SyncParcelImages(UserName, SqlNbhdForQuery, Request("page"), Request("imageLimit"), True, dt)
        End If
        JSON.SendTable(dt)
    End Sub

    Private Sub _recordParcelChanges()

        Dim lockName As String = HttpContext.Current.GetCAMASession.TenantKey + "~" + UserName
        SyncLock lockName
            Dim data As String = Request("dat")
            Dim lat As String = Request("lat")
            Dim lon As String = Request("lon")

            MobileSyncUploadProcessor.EnqueueParcelChangeData(UserName, data, lat, lon)

            'If data.IsNotEmpty Then
            '    Dim batchId As Integer = 0
            '    Dim latitude, longitude As Single
            '    Dim batchData As String = data
            '    Single.TryParse(lat, latitude)
            '    Single.TryParse(lon, longitude)

            '    Dim batchSql As String = "INSERT INTO UserParcelChangeBatch (LoginId, Latitude, Longitude, BatchData) VALUES ({0}, {1}, {2}, {3}); SELECT CAST(@@IDENTITY AS INT) As NewId;"
            '    batchId = Database.Tenant.GetIntegerValue(batchSql.FormatString(SqlUserName, latitude, longitude, batchData.ToSqlValue))

            '    data = data.Trim
            '    Dim lines As String() = data.Split(vbLf)

            '    Dim splitLines As New List(Of String)

            '    For Each line As String In lines
            '        Dim v = line.Trim.Split("|")
            '        Dim isLine As Boolean = True
            '        If v.Length = 5 Or v.Length = 6 Or v.Length = 7 Then
            '            Try
            '                Dim timestamp As Long = CLng(v(0))
            '            Catch ex As Exception
            '                isLine = False
            '            End Try
            '        Else
            '            isLine = False
            '            Try
            '                Dim timestamp As Long = CLng(v(0))
            '                isLine = True
            '            Catch ex As Exception
            '                isLine = False
            '            End Try
            '        End If
            '        If isLine Then
            '            splitLines.Add(line)
            '        Else
            '            Dim lastLine = splitLines(splitLines.Count - 1)
            '            lastLine += vbNewLine + line
            '            splitLines(splitLines.Count - 1) = lastLine
            '        End If
            '    Next

            '    MobileSyncUploadProcessor.ProcessParcelChangeData(batchId, UserName, splitLines.ToArray, lat, lon)
            'Else
            '    MobileSyncUploadProcessor.ProcessParcelChangeBatches(UserName, False)
            'End If

            'Database.Tenant.Execute("EXEC cc_UpdateNeighborhoodStatistics " & NeighborhoodId)
            JSON.OK()
        End SyncLock
    End Sub

    Private Sub _collectTrackingInfo()
        'Try
        If Request("lastUpdated").IsNotEmpty AndAlso Request("lastUpdated") <> "null" Then
            Dim lastUpdated As Long = Request("lastUpdated")
            Dim lastLat As Decimal = Request("lastLat")
            Dim lastLng As Decimal = Request("lastLng")
            Dim lastAcc As Integer = Request("lastAcc")
            Dim user As String = UserName

            e_("EXEC ccma_UserTrackingUpdate {0}, {1}, {2}, {3}, {4}", True, UserName, lastUpdated, lastLat, lastLng, lastAcc)
        End If
        If Request("geo").Trim.IsNotEmpty Then
            Dim pointStr As String = Request("geo").Trim
            Dim lines As String() = pointStr.Split("|")
            For Each line In lines
                Dim v = line.Trim.Split(",")
                Dim timestamp As Long = CLng(v(0))
                Dim lat As Decimal = CDec(v(1))
                Dim lng As Decimal = CDec(v(2))
                Dim acc As Integer = CInt(v(3))
                e_("EXEC ccma_UserTrackingAdd {0}, {1}, {2}, {3}, {4}", True, UserName, timestamp, lat, lng, acc)
            Next
        End If
        JSON.OK()

    End Sub

    Private Sub _agreeNbhd()
        Dim nbhd As String = Request("nbhd")
        Dim agree As String = Request("agree")
        If Boolean.Parse(agree) Then

        End If
        e_("UPDATE Neighborhood SET ProfileReasonable = {1} WHERE Number IN ('{0}')", SqlNbhdForQuery, Boolean.Parse(agree).GetHashCode)
        JSON.OK()
    End Sub
    Private Sub _calculateRouteUsingMapQuest_new()
        Dim parcels As String = Request("parcels")
        Dim Location_Latitude As String = Request("currentLocation_Latitude")
        Dim Location_Longitude As String = Request("currentLocation_Longitude")
        'Dim address = HttpContext.Current.GetCAMASession.OrganizationAddress
        Dim routeType As Integer = Request("routeMethod")
        Dim cs = HttpContext.Current.GetCAMASession
        Dim currentLocation = New Location With {.Latitude = Location_Latitude, .Longitude = Location_Longitude, .State = cs.State, .County = cs.County, .Country = cs.Country}
        Dim routes = RouteProcessor.CalculateRoute(UserName, DB, currentLocation, parcels.Split(","c).[Select](Function(n) Integer.Parse(n)).ToArray(), routeType)
        Dim jsonstr As String = "{""status"":""" + routes.success.ToString() + """, ""Sequence"":["
        For Each p In routes.locationSequence
            jsonstr += "{ ""ParcelId"":" + p.Key.ToString() + ",""Index"":" + p.Value.ToString() + "},"
        Next
        jsonstr = jsonstr.Trim().Remove(jsonstr.Length - 1)
        jsonstr += "]}"
        JSON.WriteString(jsonstr)
    End Sub
    Private Sub _calculateRouteUsingMapQuest()
        'Dim url = "http://www.mapquestapi.com/directions/v2/" + "optimizedroute" + "?key=Fmjtd%7Cluua2168n5%2C8s%3Do5-h4bsl"
        'Dim url = "http://www.mapquestapi.com/directions/v2/" + "optimizedroute" + "?key=N6EsTYbsLA85FgdCAHxGcQugePV3ER4h"
        Dim url = "http://www.mapquestapi.com/directions/v2/" + "optimizedroute" + "?key=TbrubRuJmy19ytFhyA1iGbEpg9c9P3A0"
        Dim data As String = Request("json")
        Dim wc As New WebClient
        Try
            Dim result As String = wc.UploadString(url, data)
            JSON.WriteString(result)
        Catch ex As WebException
            Dim reader As New StreamReader(ex.Response.GetResponseStream())
            Dim outputContent As String = reader.ReadToEnd()

            JSON.WriteString(outputContent)
        End Try

    End Sub

    Private Sub _registerSyncEvent(requestType As String, tableName As String, pageNo As Integer)
        Dim sql As String = "", tempSql As String = ""
        tempSql = "INSERT INTO SyncLog (LoginID, IPAddress, Neighborhood, RequestType, TableName, PageNo, ProcessTime, RowsReturned, DataSize, Success, ErrorMessage) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10});"
        tempSql = tempSql.FormatString(SqlUserName, Request.ClientIPAddress.ToSqlValue, SqlNbhdForSp.ToSqlValue, requestType.ToSqlValue, tableName.ToSqlValue(True), pageNo, JSON.ProcessTime, JSON.ObjectsAffected, JSON.DataSize, JSON.Success.GetHashCode, JSON.ErrorMessage.ToSqlValue(True))
        sql += tempSql
        tempSql = ""
        Database.Tenant.Execute(sql)

    End Sub
    Private Sub _createNbhdDownloadLog()
        Dim newId As Integer
        If NeighborhoodId.Length > 0 Then
            Dim squery As String = "INSERT INTO NeighborhoodDownloadStats([EventDate], [LoginId], [NbhdList], [Status]) VALUES (GETUTCDATE(), {0}, {1}, 0); SELECT CAST(@@IDENTITY  AS INT);"
            newId = Database.Tenant.GetIntegerValue(squery.SqlFormat(True, UserName, NeighborhoodName))
        End If
        JSON.Write(New With {.status = "OK", .newId = newId})
    End Sub
    Private Sub _syncImage()
        Try

            Dim pid As String = Request("parcelid")
            Dim pkey As String '= Request("keyvalue")
            Dim type As String = Request("Type")
            Dim dataUrl As String = Request("photo")
            Dim ClientROWUID As String = Nothing
            If String.IsNullOrEmpty(pid) OrElse String.IsNullOrEmpty(dataUrl) OrElse String.IsNullOrEmpty(Request("timestamp")) Then
                'Try
                '    Throw New Exception("Invalid photo upload request from MA")
                'Catch ex As Exception
                '    ErrorMailer.ReportException("MA-Photo-Sync", ex, Request)
                'End Try
                JSON.OK()
                Return
            End If
            If IsNumeric(pid) Then
                If (pid < 0) Then
                    ClientROWUID = pid
                    pid = Database.Tenant.GetIntegerValue("SELECT TOP 1 CC_ParcelID FROM ParcelData WHERE ClientROWUID={0}".SqlFormatString(pid.ToString()))
                End If
            End If

            Dim timeStamp As Long = Request("timestamp")
            Dim localId As String = Request("localid")

            Dim existingRow = Database.Tenant.GetTopRow("SELECT Id, Path FROM ParcelImages WHERE LocalId = " & localId.ToSqlValue)
            If (localId <> "" And existingRow IsNot Nothing) Then
                Dim exImgId As Integer = GetInteger(existingRow, "Id")
                Dim desc As String = "LocalId (" + localId + ") already exists for the image (" + exImgId.ToString() + ")"
                Database.Tenant.Execute("INSERT INTO AdditionalLogs(ParcelId, LoginId, Description) VALUES({0}, {1}, {2})".SqlFormatString(pid, UserName, desc))
                JSON.Write(New With {.status = "OK", .imageId = exImgId, .s3Path = GetString(existingRow, "Path"), .localId = localId})
                Return
            End If

            pkey = Database.Tenant.GetStringValue("SELECT KeyValue1 FROM Parcel WHERE Id = " & pid)

            Dim application As Integer = IIf(type = "", 1, type)
            Dim base64 As String = dataUrl.Substring(dataUrl.IndexOf(";base64,") + 8)
            Dim imageType As String = dataUrl.Substring(0, dataUrl.IndexOf(";base64,")).Trim(";").Split(":")(1).Split("/")(1)
            base64 = base64.Replace("_", "/").Replace("-", "+").Replace(" ", "+")
            If imageType = "jpeg" Then
                imageType = "jpg"
            End If
            Dim fileName As String = pid + Now.ToString("-yyyyMMdd-HHmmssfff") + "." + imageType
            Dim s3 As New S3FileManager()

            Dim bytes() As Byte = System.Convert.FromBase64String(base64)
            Dim ms As New MemoryStream(bytes)
            Dim s3Path As String = HttpContext.Current.GetCAMASession.TenantKey + "/pc/"

            For Each count In New Integer() {2, 3, 3, 3}
                If pkey.Length > 0 Then
                    Dim part As String = pkey.Substring(0, Math.Min(count, pkey.Length))
                    pkey = pkey.Substring(part.Length)
                    If part(part.Length - 1) = "." Or part(part.Length - 1) = " " Or part(part.Length - 1) = "-" Then
                        part = part.Remove(part.Length - 1, 1)
                    End If
                    s3Path += part + "/"
                End If
            Next
            s3Path += fileName
            s3.UploadFile(ms, s3Path)
            Dim eventDate As Date = #1/1/1970#
            If timeStamp.ToString.Length > 13 Then
                timeStamp = Long.Parse(timeStamp.ToString.Substring(0, 13))
            End If
            eventDate = eventDate.AddMilliseconds(timeStamp)
            Dim sql = "EXEC cc_Parcel_AddPhoto  {0}, {1}, 0, {2}, {3}, {4}, {5}, {6},@AppType = 'MA'".SqlFormat(True, UserName, pid, s3Path, application, eventDate, localId, ClientROWUID)

            'Dim imageId As Integer = Database.Tenant.GetIntegerValue("INSERT INTO ParcelImages (ParcelId, IsSketch, Storage, Path) VALUES ({0}, 0, 1, {1});SELECT CAST(@@IDENTITY AS INT) As NewId".SqlFormat(True, pid, s3Path))
            Dim imageId As Integer = Database.Tenant.GetIntegerValue(sql)
            'If ClientROWUID IsNot Nothing Then
            '	Database.Tenant.Execute("UPDATE ParcelImages SET LocalId={0},NewBPP=1,BppClientROWUID={2} WHERE Id={1}".SqlFormatString(localId, imageId,ClientROWUID))	
            'Else
            '	Database.Tenant.Execute("UPDATE ParcelImages SET LocalId={0} WHERE Id={1}".SqlFormatString(localId, imageId))	            	
            'End If
            MobileSyncUploadProcessor.insertNewRecordDetails(Database.Tenant, pid, imageId, "Images", localId, eventDate, UserName)
            UserAuditTrail.CreateEvent(UserName, UserAuditTrail.ApplicationType.MobileAssessor, UserAuditTrail.EventType.UploadPhoto, "Uploaded photo - " + localId + " (" & imageId & ")")

            JSON.Write(New With {.status = "OK", .imageId = imageId, .s3Path = s3Path, .localId = localId})
            'JSON.OK(s3Path)
        Catch ex As Exception
            'ErrorMailer.ReportException("MA-Photo-Sync", ex, Request)
            'JSON.Error(ex)
            Dim localId As String = Request("localid")
            Dim plId As String = Request("parcelid")
            Dim BppClientROWUID As String = ""
            If IsNumeric(plId) Then
                If (plId < 0) Then
                    BppClientROWUID = plId
                    plId = Database.Tenant.GetIntegerValue("SELECT TOP 1 CC_ParcelID FROM ParcelData WHERE ClientROWUID = {0}".SqlFormatString(plId.ToString()))
                End If
            End If
            Dim errDesc As String = "MaPhotoSyncError: LocalId (" + localId + ") - " + ex.Message
            Dim sql As String = "INSERT INTO PendingPhotos(parcelid, [Type], photo, [timestamp], LoginId, LocalId, ErrorMessage, BppClientROWUID) VALUES({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7})Select @@IDENTITY".SqlFormatStringWithNulls(plId, Request("Type"), Request("photo"), Request("timestamp"), UserName, localId, ex.Message, BppClientROWUID)
            Dim Rowuid = Database.Tenant.GetIntegerValue(sql)
            Database.Tenant.Execute("INSERT INTO AdditionalLogs(ParcelId, LoginId, Description) VALUES({0}, {1}, {2})".SqlFormatString(plId, UserName, errDesc))
            JSON.Write(New With {.status = "OK", .imageId = Rowuid, .s3Path = "", .localId = ""})
        End Try
    End Sub

    Private Sub _syncComplete()
        Try
            Dim type As String = Request("type")
            Dim action As String = "User updated mobile data."
            Dim NbhdStatsId = Request("nbhdStatsId")
            Select Case type
                Case "System"
                    action = "User downloaded system data."
                Case "CAMA"
                    action = "User downloaded CAMA data for neighborhood - "
                Case "All"
                    action = "User downloaded all data - "
            End Select
            UserAuditTrail.CreateEvent(UserName, UserAuditTrail.ApplicationType.MobileAssessor, UserAuditTrail.EventType.DownloadData, action)
            'Database.Tenant.Execute("EXEC ccma_SyncUserComplete @LoginID = {0}, @Nbhd = {1}", SqlUserName, SqlNbhd)
            Try
                If NbhdStatsId IsNot Nothing Then
                    Dim squery = ""
                    If EnableNewPriorities Then
                        squery = "UPDATE n SET n.Status = 1, n.Urgent = p.Urgent, n.High = p.High, n.Normal = p.Normal, n.TotalCount = p.TotalCount, n.Critical = p.Critical, n.Medium = p.Medium, n.Proximity = p.Proximity FROM NeighborhoodDownloadStats n JOIN (SELECT COUNT(*) As TotalCount, SUM(CASE WHEN Priority='3' THEN 1 ELSE 0 END) High, SUM(CASE WHEN Priority='4' THEN 1 ELSE 0 END) Urgent, SUM(CASE WHEN Priority='1' THEN 1 ELSE 0 END) Normal, SUM(CASE WHEN Priority='5' THEN 1 ELSE 0 END) Critical, SUM(CASE WHEN Priority='2' THEN 1 ELSE 0 END) Medium, SUM(CASE WHEN Priority='0' THEN 1 ELSE 0 END) Proximity, " + NbhdStatsId.ToString() + " As NbhdStatsId FROM NeighborhoodDownloadparcelList WHERE NbhdStatsId = " + NbhdStatsId.ToString() + ") p ON n.Id = p.NbhdStatsId WHERE n.Id = " + NbhdStatsId.ToString()
                    Else
                        squery = "UPDATE n SET n.Status = 1, n.Urgent = p.Urgent, n.High = p.High, n.Normal = p.Normal, n.TotalCount = p.TotalCount FROM NeighborhoodDownloadStats n JOIN (SELECT COUNT(*) As TotalCount, SUM(CASE WHEN Priority='1' THEN 1 ELSE 0 END) High, SUM(CASE WHEN Priority='2' THEN 1 ELSE 0 END) Urgent, SUM(CASE WHEN Priority='0' THEN 1 ELSE 0 END) Normal, " + NbhdStatsId.ToString() + " As NbhdStatsId FROM NeighborhoodDownloadparcelList WHERE NbhdStatsId = " + NbhdStatsId.ToString() + ") p ON n.Id = p.NbhdStatsId WHERE n.Id = " + NbhdStatsId.ToString()
                    End If

                    squery += "; UPDATE Parcel SET [DownloadStatus] = 1 WHERE Id IN (SELECT PId FROM NeighborhoodDownloadparcelList WHERE NbhdStatsId = " + NbhdStatsId.ToString() + ")"
                    squery += "; INSERT INTO ParcelAuditTrail (ParcelId, LoginId, EventType, Description,ApplicationType) SELECT  pId, '" + UserName + "', 0, 'Parcel downloaded to field device in Assignment Group '+ Nbhd,'MA'  FROM NeighborhoodDownloadparcelList where NbhdStatsId = " + NbhdStatsId.ToString()
                    Database.Tenant.Execute(squery)
                End If
            Catch ex As Exception

            End Try

            JSON.OK()
        Catch ex As Exception
            JSON.OK()
        End Try
    End Sub

    Private Sub _getGlobalInputTypes()
        Dim dt As DataTable = Database.System.GetDataTable("SELECT * FROM InputTypes")
        Dim jsonString As String = ""
        For Each dr As DataRow In dt.Rows
            Dim i As Integer = dr("cctype")
            If jsonString <> "" Then
                jsonString += ","
            Else

            End If
            jsonString += vbNewLine + vbTab + i.ToString + " : " + JSON.GetRowAsJSON(dr)
        Next
        JSON.WriteString("{" + jsonString + vbNewLine + "}")
    End Sub

    Private Sub _getLookupDataTables()
        JSON.StartTick()
        Dim objectCount As Integer = 0
        Dim items As New List(Of String)
        Dim lookupTableNames As New List(Of String)
        Dim tableIndex As String = Request("tableIndex").ToString()
        Dim pageIndex As String = Convert.ToInt32(Request("pageIndex").ToString())
        '        For Each tableName In DB.GetDataTable("SELECT t.Name FROM DataSourceTable t LEFT OUTER JOIN DataSourceRelationships r ON t.Id = r.TableId AND r.Relationship IS NOT NULL WHERE NOT (t.Name LIKE '`_%' ESCAPE '`') AND t.Name NOT IN (SELECT ParcelTable FROM Application UNION SELECT NeighborhoodTable FROM Application UNION SELECT '_photo_meta_data') GROUP BY t.Name HAVING COUNT(r.TableId) = 0").Rows.OfType(Of DataRow).Select(Function(x) x.GetString("Name")).ToArray
        '            lookupTableNames.Add(tableName)
        '        Next
        If pageIndex <> 0 Then
            pageIndex = pageIndex * 2000
        End If
        Dim tableName As String = DB.GetStringValue("SELECT Name FROM DataSourceTable WHERE ImportType = 5 ORDER BY Name OFFSET " + tableIndex + " ROWS FETCH NEXT 1 ROWS ONLY")

        If tableName = Nothing Then
            JSON.WriteString("{""status"": ""completed""}")
        Else
            Dim sortFormula As String = FieldCategory.GetSortExpression(tableName, "xt.")
            Dim sql As String = "SELECT '" + tableName + "' As DataSourceName, xt.* FROM LT_" + tableName + " xt ORDER BY DataSourceName OFFSET " + pageIndex + " ROWS FETCH NEXT 2000 ROWS ONLY"
            Dim dt As DataTable = Database.Tenant.GetDataTable(sql)
            MobileSyncDownloadProcessor.AddBlankRowIfNoRows(dt, "DataSourceName", tableName)
            JSON.SendTable(dt)
        End If

        '        For Each tableName In lookupTableNames
        '            Dim sqlTableName As String = "LT_" + tableName
        '            If Database.Tenant.DoesTableExists(sqlTableName) Then
        '                Dim tableData As String = JSON.GetTableAsJSON()
        '                objectCount += JSON.ObjectsAffected
        '                If tableData <> "[]" And tableData.Length > 0 Then
        '                    items.Add(tableData.TrimStart("[").TrimEnd("]"))
        '                End If
        '            End If
        '        Next
        '        Dim js As String = "[" + String.Join(", ", items.ToArray) + "]"
        '        JSON.WriteString(js)
        '        JSON.EndTick()
        '        JSON.SetObjectCount(objectCount)
    End Sub

    Private Sub _getNbhds()
        Dim dtNbhd As DataTable = Database.Tenant.GetDataTable("EXEC ccma_GetNeighborhoods " + SqlUserName)
        Dim dtClientSettings As DataTable = Database.Tenant.GetDataTable("SELECT * FROM ClientSettings WHERE Name IN ('ParcelDownloadLimit', 'EnableMultipleNbhd', 'EnableWalkingMode', 'DefaultImageDownloadOption')")
        Dim downloadLimit As String = ""
        Dim multipleNbhd As String = ""
        Dim EnableWalkMode As String = ""
        Dim DefaultImageDownloadOption As String = "1"

        For Each dr As DataRow In dtClientSettings.Rows
            If dr(0).ToString() = "ParcelDownloadLimit" Then
                downloadLimit = dr(1).ToString()
            ElseIf dr(0).ToString() = "EnableMultipleNbhd" Then
                multipleNbhd = dr(1).ToString()
            ElseIf dr(0).ToString() = "EnableWalkingMode" Then
                EnableWalkMode = dr(1).ToString()
            ElseIf dr(0).ToString() = "DefaultImageDownloadOption" Then
                DefaultImageDownloadOption = dr(1).ToString()
            End If
        Next
        downloadLimit = IIf(downloadLimit = "" And multipleNbhd = "1", "1200", IIf(downloadLimit = "", "0", downloadLimit))
        multipleNbhd = IIf(multipleNbhd = "", multipleNbhd = "0", multipleNbhd)
        EnableWalkMode = IIf(EnableWalkMode = "", EnableWalkMode = "0", EnableWalkMode)
        Dim jstr As String = "{""Neighborhoods"": " + JSON.GetTableAsJSON(dtNbhd).TrimEnd("}")
        jstr += ",""ParcelDownloadLimit"": """ + downloadLimit + """"
        jstr += ",""EnableMultipleNbhd"": """ + multipleNbhd + """"
        jstr += ",""EnableWalkingMode"": """ + EnableWalkMode + """"
        jstr += ",""DefaultImageDownloadOption"": """ + DefaultImageDownloadOption + """"
        jstr += ",""NeighborhoodName"": """ + Neighborhood.getNeighborhoodName() + """"
        jstr += "}"
        JSON.WriteString(jstr)
        'JSON.SendTable(Database.Tenant, "EXEC ccma_GetNeighborhoods " + SqlUserName)
        UserAuditTrail.CreateEvent(UserName, UserAuditTrail.ApplicationType.MobileAssessor, UserAuditTrail.EventType.MobileTask, "Downloaded group/nbhd list.")
    End Sub
    Private Sub _recordDeviceSyncStatus(data As String)
        Dim arr = data.Split("|")
        If (arr.Count = 7) Then
            Dim eventDate As Date = #1/1/1970#
            If arr(5).ToString.Length > 13 Then
                arr(5) = Long.Parse(arr(5).ToString.Substring(0, 13))
            End If
            If (arr(5) = "") Then
                eventDate = DateTime.UtcNow
            Else
                eventDate = eventDate.AddMilliseconds(arr(5))
            End If

            Database.System.Execute("if exists( select * from DeviceSyncStatus where MachineKey={0}) UPDATE DeviceSyncStatus SET LoginId={1},PendingPCIs={2},PendingImages={3},ErrorMessage={4},lastSyncTime={5},Url={6},UpdatedTime=getutcdate(),UserAgent={7},OrganizationId={8}  WHERE MachineKey={0} else INSERT INTO DeviceSyncStatus(MachineKey,LoginId,PendingPCIs,PendingImages,ErrorMessage,lastSyncTime,Url,UserAgent,OrganizationId) VALUES({0},{1},{2},{3},{4},{5},{6},{7},{8})".SqlFormat(True, arr(0), arr(1), arr(3), arr(2), arr(4), eventDate, arr(6), Request.UserAgent, HttpContext.Current.GetCAMASession.OrganizationId))

        End If
    End Sub
    Private Sub _recordErrorLog()
        Dim values As String() = Request("message").ToString().Split("|")
        If Request("message") <> "check" Then
            Dim insertQuery As String = "INSERT INTO ApplicationLogHistory(Application, LoginId, ParcelId, CurrentScreenId, LogText, LogTime,  LogType, Version) VALUES('MA', '" + values(0) + "', " + IIf(values(1) = "null", values(1), "'" + values(1) + "'") + "," + IIf(values(2) = "", "null", "'" + values(2) + "'") + ", '" + values(3) + "', '" + values(4) + "', '" + values(5) + "', '" + values(6) + "');"
            Database.Tenant.Execute(insertQuery)
        End If
        JSON.OK()
    End Sub

    Private Sub _geterrorlog()
        Dim query As String = "SELECT * FROM ApplicationLogHistory"
        Dim type As String = Request("type")
        If type <> Nothing AndAlso type <> "all" Then
            If type = "latest" Then
                query += " WHERE Id > " + Request("id").ToString()
            Else
                query += " WHERE LogType = '" + type + "'"
            End If
        End If

        Dim dt = Database.Tenant.GetDataTable(query)

        If type = "eval" Then
            Dim dt1 As DataTable = Database.Tenant.GetDataTable("SELECT * FROM ApplicationLogHistory WHERE LogType = 'eval'")
            Database.Tenant.Execute("DELETE FROM ApplicationLogHistory WHERE LogType = 'eval'")
        End If

        JSON.SendTable(dt)
    End Sub

    Private Sub _getCacheFiles()
        Dim resp As Object
        basePath = HttpContext.Current.Server.MapPath("~/")
        excludePaths.AddRange({HttpContext.Current.Server.MapPath("~/logs").TrimEnd("/"), HttpContext.Current.Server.MapPath("~/static/js/unused").TrimEnd("/"), HttpContext.Current.Server.MapPath("~/static/js/worker").TrimEnd("/")})
        cacheList.AddRange({"/", "/applogo-logo-ma-app-top-left.png", "/applogo-logo-ma-app-top-right.png", "/applogo-logo-ma-app-splash.png"})
        ListAllCacheFiles(HttpContext.Current.Server.MapPath("~/static"))
        Dim info As New FileInfo("/rwi-worker.js")
        Dim relPath As String = "/rwi-worker.js" & "?" & info.LastWriteTimeUtc.Ticks
        cacheList.Add(relPath)
        resp = New With {.CacheFiles = JsonConvert.SerializeObject(cacheList)}
        Response.Clear()
        Response.ContentType = "application/json"
        Response.Write(JSONResponse.Serialize(resp))
    End Sub

    Public Sub ListAllCacheFiles(path As String)
        For Each fn In Directory.GetFiles(path)
            Dim ext = IO.Path.GetExtension(fn).TrimStart(".").ToLower
            Dim relPath As String = ""
            Dim info As New FileInfo(fn)
            If ext = "css" Then
                relPath = fn.Replace(basePath, "\").Replace("\", "/") & "?" & info.LastWriteTimeUtc.Ticks
            ElseIf ext = "png" Or ext = "jpg" Or ext = "gif" Or ext = "bmp" Then
                relPath = fn.Replace(basePath, "\").Replace("\", "/")
            ElseIf ext = "js" Then
                relPath = fn.Replace(basePath, "\").Replace("\", "/") + IIf(True, "?" & info.LastWriteTimeUtc.Ticks, "")
            End If
            If relPath <> "" Then
                cacheList.Add(relPath)
            End If
        Next

        For Each dn In Directory.GetDirectories(path)
            If Not excludePaths.Contains(dn.TrimEnd("/")) Then
                ListAllCacheFiles(dn)
            End If
        Next
    End Sub

    Private Sub _getCacheVersion()
        Dim maxDate As Date = #1/1/2010#
        Dim cacheVersion As String = ""
        If ClientSettings.CustomizationDate > maxDate Then
            maxDate = ClientSettings.CustomizationDate
        End If

        If ClientSettings.LastClientTemplateUpdateDate > maxDate Then
            maxDate = ClientSettings.LastClientTemplateUpdateDate
        End If

        If Data.Database.Tenant.Application.SchemaVersionDate > maxDate Then
            maxDate = Data.Database.Tenant.Application.SchemaVersionDate
        End If

        Dim buildDate As Date = Date.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings("AppBuildTime"))
        If buildDate > maxDate Then
            maxDate = buildDate
        End If
        Dim s = HttpContext.Current.Server
        excludePaths.AddRange({s.MapPath("~/logs").TrimEnd("/"), s.MapPath("~/static/js/unused").TrimEnd("/"), s.MapPath("~/static/js/worker").TrimEnd("/")})
        GetLastModifiedDates(s.MapPath("~/static"))
        Dim modDate As Date = Date.Parse(IO.File.GetLastWriteTime(s.MapPath("~/rwi-worker.js")))
        If modDate > maxModDate Then
            maxModDate = modDate
        End If
        modDate = Date.Parse(IO.File.GetLastWriteTime(s.MapPath("~/Default.aspx")))
        If modDate > maxModDate Then
            maxModDate = modDate
        End If

        If maxModDate > maxDate Then
            maxDate = maxModDate
        End If

        Dim version As Long = (maxDate.Ticks - #3/3/2013#.Ticks) / (10000 * 1000)
        cacheVersion = version.ToString()
        JSON.WriteString(cacheVersion)
    End Sub

    Public Sub GetLastModifiedDates(path As String)
        For Each fn In Directory.GetFiles(path)
            Dim ext = IO.Path.GetExtension(fn).TrimStart(".").ToLower
            If ext = "css" Or ext = "js" Then
                Dim modDate As Date = Date.Parse(IO.File.GetLastWriteTime(fn))
                If modDate > maxModDate Then
                    maxModDate = modDate
                End If
            End If
        Next

        For Each dn In Directory.GetDirectories(path)
            If Not excludePaths.Contains(dn.TrimEnd("/")) Then
                GetLastModifiedDates(dn)
            End If
        Next
    End Sub

End Class
