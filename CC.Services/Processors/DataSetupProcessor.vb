﻿Imports System.Text.RegularExpressions

Public Class DataSetupProcessor
    Inherits RequestProcessorBase

	Public Overrides Sub ProcessRequest()
        Select Case Path
            Case "saveeditfield"
                Dim fieldId As String = Request("FieldId")
                Dim label As String = Request("label")
                Dim datatype As String = Request("datatype")
                Dim lookuptable As String = Request("lookup")
                CSEFieldCategory.saveeditfield(fieldId, label, datatype, lookuptable,UserName.ToString())
                JSON.OK()
            Case "addcsefieldtocategory"
                Dim fieldId As String = Request("FieldId")
                Dim catId As String = Request("CatId")
                ' Dim tableId As String = Coalesce(Request("TableId"), -1)
                Try
                    Dim cseFID As Integer = CSEFieldCategory.AddFieldToCategory(catId, fieldId, "label",UserName.ToString())
                    JSON.WriteString(cseFID.ToString)
                Catch ex As Exception
                    JSON.Error(ex)
                End Try
            Case "removecsefield"
                Dim fieldId As String = Request("FieldId")
                Dim catId As String = Request("CatId")
                CSEFieldCategory.RemoveField(fieldId, catId,UserName.ToString())
                JSON.OK()
            Case "movecsecategory"
                Dim newIndex As String = Request("NewIndex")
                Dim catId As String = Request("CatId")
                CSEFieldCategory.MoveCategory(catId, newIndex)
                JSON.OK()
            Case "movecsefield"
                Dim newIndex As String = Request("NewIndex")
                Dim fieldId As String = Request("FieldId")
                CSEFieldCategory.MoveField(fieldId, newIndex)
                JSON.OK()
            Case "renamecsecategory"
                Dim catId As String = Request("CatId")
                Dim newName As String = Request("NewName")
                CSEFieldCategory.Rename(catId, newName,UserName.ToString())
                JSON.OK()
            Case "removecsecategory"
                Dim catId As String = Request("CatId")
                CSEFieldCategory.Delete(catId,UserName.ToString())
                JSON.OK()
            Case "setimporttype"
                Dim tableName As String = Request("TableName")
                Dim importType As String = Request("ImportType")
                DataSource.ChangeImportType(tableName, Integer.Parse(importType))
                JSON.OK()
            Case "donotimport"
                Dim tableName As String = Request("TableName")
                DataSource.MarkTableAsDNI(tableName)
                JSON.OK()
            Case "addfieldtocategory"    
            	Dim fname As String = Request("Fieldname")
                Dim fieldId As String = Request("FieldId")
                Dim catId As String = Request("CatId")
                Dim tableId As String = Coalesce(Request("TableId"), -1)
                Dim cname As String = Request("Catname")                
                Try
                	FieldCategory.AddFieldToCategory(catId, fieldId, tableId)
                	Dim name As String= UserName
               	    Dim Description As String = "Field "+fname+" has been added to "+cname+" Category."
                    SettingsAuditTrail.insertSettingsAuditTrail(UserName.ToString(),Description)
                    JSON.OK()
                Catch ex As Exception
                    JSON.Error(ex)
                End Try
            Case "addfieldtoqrtab"
                Dim fieldId As String = Request("FieldId")
                Try
                    FieldCategory.AddFieldToQRTab(fieldId)
                    JSON.OK()
                Catch ex As Exception
                    JSON.Error(ex)
                End Try
            Case "removefieldfromqrtab"
                Dim fieldId As String = Request("FieldId")
                Try
                    FieldCategory.RemoveFieldFromQRTab(fieldId)
                    JSON.OK()
                Catch ex As Exception
                    JSON.Error(ex)
                End Try
            Case "moveqrfield"
                Dim newIndex As String = Request("NewIndex")
                Dim fieldId As String = Request("FieldId")
                FieldCategory.MoveQRField(fieldId, newIndex)
                JSON.OK()

            Case "removefield"
            	Dim cname As string = Request("Catname")
            	Dim fname As String = Request("Fieldname")
                Dim fieldId As String = Request("FieldId")
                Dim catId As String = Request("CatId")
                FieldCategory.RemoveField(fieldId, catId)
                Dim name As String= UserName
                Dim Description As String = "Field "+fname+" has been deleted from "+cname+" Category."
                SettingsAuditTrail.insertSettingsAuditTrail(UserName.ToString(),Description)
                JSON.OK()
            Case "removemultiplefields"
                Dim cname As String = Request("CatName")
                Dim fname As String = Request("FieldNames")
                Dim fieldId As String = Request("FieldId")
                Dim catId As String = Request("CatId")
                FieldCategory.RemoveMultipleField(fieldId, catId)
                Dim name As String= UserName
                Dim Description As String = "Fields " + fname + " has been deleted from " + cname + " Category."
                SettingsAuditTrail.insertSettingsAuditTrail(UserName.ToString(), Description)
                JSON.OK()
            Case "movecategory"
                Dim newIndex As String = Request("NewIndex")
                Dim catId As String = Request("CatId")
                FieldCategory.MoveCategory(catId, newIndex)
                JSON.OK()
            Case "movefield"
            	Dim Fieldslist As String = Request("Fieldslist")
                Dim newIndex As String = Request("NewIndex")
                Dim fieldId As String = Request("FieldId")
                Dim catid As Integer = db.GetIntegerValue("SELECT CategoryId FROM DataSourceField WHERE Id = {0}".FormatString(fieldId))
                DB.Execute("EXEC cc_ReorderCategoryFields @CategoryId = {0}, @Fieldslist = {1} ".SqlFormat(True, catid,Fieldslist))
                JSON.OK()
            Case "renamecategory"
                Dim catId As String = Request("CatId")
                Dim newName As String = Request("NewName")
                Dim oldName As String = Request("OldName")
                FieldCategory.Rename(catId, newName)
                Dim Description As String = "Field Category name changed from "+oldName+" to "+newName+"."
                SettingsAuditTrail.insertSettingsAuditTrail(UserName.ToString(),Description)
                JSON.OK()
                
            Case "executequery"
                Dim tableSet As String = Request("tables")
                Dim query As String = Request("query")
                Dim tables As String() = Regex.Split(tableSet, "\,")
                Dim targetTblName As String
                Dim tblName As String()
                For Each table As String In tables
                	tblName = table.Split(" ")
                	targetTblName = Database.Tenant.GetStringValue("SELECT CC_TargetTable FROM DataSourceTable WHERE Name = '" + tblName(1) + "'")
                	query = Regex.Replace(query, table, tblName(0) + " " + targetTblName, RegexOptions.IgnoreCase)
                	query = query.Replace(tblName(1)+".", targetTblName+".")
                Next
                Dim dt As DataTable = New DataTable()
                Try
                	dt = Database.Tenant.GetDataTable(query)
                Catch ex As Exception
                	JSON.Error(ex.Message)
                	Throw
                End Try
                JSON.SendTable(dt)
                
            Case "removecategory"
            	Dim catId As String = Request("CatId")
            	Dim catName As String = Request("cname")
            	FieldCategory.Delete(catId)
            	Dim name As String= UserName
                Dim Description As String = "Category "+catName+" has been deleted from Field Category."
                SettingsAuditTrail.insertSettingsAuditTrail(UserName.ToString(),Description)
                JSON.OK()
            Case "updatefield"
                Dim fieldId As String = Request("FieldId")
                Dim col As String = Request("ColumnName")
                Dim value As String = Request("Value")
                Dim type As String = Request("Type")
                DataSource.UpdateFieldProperty(fieldId, col, type, value, Request("LoginId"))
                Database.Tenant.Execute("if exists(SELECT * FROM ClientSettings WHERE Name = 'LastSchemaUpdatedTime') UPDATE ClientSettings SET Value = CONVERT(NVARCHAR(24),GETUTCDATE(),121) WHERE Name = 'LastSchemaUpdatedTime' else INSERT INTO ClientSettings(name,Value) Values('LastSchemaUpdatedTime',CONVERT(NVARCHAR(24),GETUTCDATE(),121))")
                Database.Tenant.Execute("if exists(SELECT * FROM ClientSettings WHERE Name = 'LastSchemaUpdatedCounter') UPDATE ClientSettings SET Value = Value + 1 WHERE Name = 'LastSchemaUpdatedCounter' else INSERT INTO ClientSettings(name,Value) Values('LastSchemaUpdatedCounter','1')")
                JSON.OK()
            Case "updatetable"
                Dim tableId As String = Request("TableId")
                Dim col As String = Request("ColumnName")
                Dim value As String = Request("Value")
                DataSource.UpdateTableProperty(tableId, col, value)
                JSON.OK()
            Case "movecomparable"
                Dim newIndex As String = Request("NewIndex")
                Dim cfId As String = Request("CFID")
                ComparableReport.MoveField(cfId, newIndex)
                JSON.OK()
            Case "moveadefield"
                Dim configId As String = Request("ConfigId")
                Dim stageId As String = Request("StageId")
                Dim fId As String = Request("SID")
                Dim newIndex As String = Request("NewIndex")
                AutoDataEntryConfig.MoveStep(configId, stageId, fId, newIndex)
                JSON.OK()

                ' add start with NPF client setting table & checking 
            Case "addtonpf"
                Dim fieldid As String = Request("fieldId")
                ' Dim fieldName As String = Database.Tenant.GetStringValue("select Name from DataSourceField where id=" + fieldid)

                If Database.Tenant.GetDataTable("select Name from ClientSettings where Name like 'NPF%' and Value Like '" + fieldid + "'").Rows.Count > 0 Then
                    'do nothing
                Else
                    Dim totalNpf = Database.Tenant.GetIntegerValue("select count(Name) from ClientSettings where Name like 'NPF%'")
                    If totalNpf > 5 Then
                        ' do nothing
                    Else
                        Dim combinedString = "NPF" & (totalNpf + 1).ToString()
                        e_("INSERT INTO ClientSettings VALUES('{0}','{1}')".FormatString(combinedString, fieldid))
                    End If
                End If
                JSON.OK()
            Case "deletefromnpf"
                Dim fieldid As String = Request("fieldId")
                'Dim fieldName As String = Database.Tenant.GetStringValue("select Name from DataSourceField where id=" + fieldid)

                e_("Delete  from ClientSettings WHERE value='{0}' and Name LIKE 'NPF%'".FormatString(fieldid))
                JSON.OK()

            Case "addafn"
                Dim agrFn As String = Request("afn")

                If Database.Tenant.GetDataTable("select Name from ClientSettings where Name like 'AFN%' and Value Like '" + agrFn + "'").Rows.Count > 0 Then
                    'do nothing
                Else
                    Dim totalAfn = Database.Tenant.GetIntegerValue("select count(Name) from ClientSettings where Name like 'AFN%'")

                    Dim combinedString = "AFN" & (totalAfn + 1).ToString()
                    e_("INSERT INTO ClientSettings VALUES('{0}','{1}')".FormatString(combinedString, agrFn))

                End If
                JSON.OK()
            Case "deleteafn"
                Dim agrFn As String = Request("afn")
                e_("Delete  from ClientSettings WHERE value='{0}' and Name LIKE 'AFN%'".FormatString(agrFn))
                JSON.OK()

                '6/9/2014
            Case "fillafn"
                JSON.SendTable(Database.Tenant.GetDataTable("select Value from ClientSettings where Name like 'AFN%'"))
                '6/10/2015
            Case "updaterelation"
                Dim tableId As String = Request("TableId")
                Dim tableName As String = Request("SourceTable")
                Dim fieldName As String = Request("FieldName")
                Dim cbStatus As String = Request("CheckBoxStatus")
                Dim msg As String = SetPrimarykey(tableId, tableName, fieldName, cbStatus)
                If(cbStatus = 1) Then
                	Dim Description As String = ""+fieldName+" has been set as primary key for the table "+tableName+""
        			SettingsAuditTrail.insertSettingsAuditTrail(UserName.ToString(),Description)
                End If
                If(cbStatus = 0) Then
                	Dim Description As String = "The primary key "+fieldName+" of table "+tableName+" has been removed"
        			SettingsAuditTrail.insertSettingsAuditTrail(UserName.ToString(),Description)
                End If
                JSON.OK(msg)
            Case "showcolumns"
                Dim tableName As String = Request("TableName")
                Dim sql As String
                sql = "SELECT Name FROM DataSourceField  WHERE TableId in (SELECT id FROM DataSourceTable where name={0})".SqlFormatString(tableName)
                JSON.SendTable(Database.Tenant, sql)
            Case "toggledisable"

                Dim tableName As String = Request("TableName") 
                Dim id As String = Request("Id")

                If tableName = "ClientCustomAction" Then
                   Dim dr As DataRow = db.GetTopRow("SELECT Action,case when cca.Enabled=1 then 'Disabled' else 'Enabled' end as EnOrDis FROM ClientCustomAction cca WHERE Id ={0}".SqlFormatString(id))
     			   Dim Action As String = dr("Action").ToString
                    Dim EnOrDis As String = dr("EnOrDis").ToString
              
                    Dim Description As String = "Custom Action "+Action.ToString+" has been "+EnOrDis.ToString()+"."
                   SettingsAuditTrail.insertSettingsAuditTrail(UserName.ToString(),Description)
                End If

                Database.Tenant.Execute("UPDATE " + tableName + " SET Enabled= (SELECT ~Enabled FROM " + tableName + " WHERE Id = {0}) WHERE Id = {0}".SqlFormatString(id))
                Database.Tenant.Execute("if exists(SELECT * FROM ClientSettings WHERE Name = 'LastSchemaUpdatedTime') UPDATE ClientSettings SET Value = CONVERT(NVARCHAR(24),GETUTCDATE(),121) WHERE Name = 'LastSchemaUpdatedTime' else INSERT INTO ClientSettings(name,Value) Values('LastSchemaUpdatedTime',CONVERT(NVARCHAR(24),GETUTCDATE(),121))")
                Database.Tenant.Execute("if exists(SELECT * FROM ClientSettings WHERE Name = 'LastSchemaUpdatedCounter') UPDATE ClientSettings SET Value = Value + 1 WHERE Name = 'LastSchemaUpdatedCounter' else INSERT INTO ClientSettings(name,Value) Values('LastSchemaUpdatedCounter','1')")

                If tableName = "ClientValidation" Then
                    Dim ds As DataRow = DB.GetTopRow("SELECT Name,case when cca.Enabled=1 then 'Disabled' else 'Enabled' end as EnOrDis FROM ClientValidation cca WHERE Id ={0}".SqlFormatString(id))
                    Dim Name As String = ds("Name").ToString
                    Dim status As String = ds("EnOrDis").ToString

                    Dim DescriptionEnablestatus As String = "Data Validation Rule " + Name.ToString + " has been Enabled."
                    Dim DescriptionDisablestatus As String = "Data Validation Rule " + Name.ToString + " has been Disabled. "
                    If status = "Enabled" Then
                        SettingsAuditTrail.insertSettingsAuditTrail(UserName.ToString(), DescriptionDisablestatus)
                    Else
                        SettingsAuditTrail.insertSettingsAuditTrail(UserName.ToString(), DescriptionEnablestatus)
                    End If
                End If

                JSON.OK()

            Case "getparcellist"
                Dim tableNamelist As String = Request("TableNames")
                Dim tableNames As String() = tableNamelist.Split(New Char() {","c})
                Dim i as integer =1
                Dim Sql As String = "SELECT top 100 p.keyvalue1 AS ParcelId FROM  Parcel p "
                For Each tableName In tableNames
                    Sql += " INNER JOIN " + tableName + " x" + i.ToString() + " On p.id= x" + i.ToString() + ".CC_parcelId "
                    i += 1
                Next
                Sql += " WHERE  p.keyvalue1 !='-99' GROUP BY  p.keyvalue1  ORDER BY p.keyvalue1 ASC"
                JSON.SendTable(Database.Tenant, Sql)
            Case "getinfocontent"
            	Dim fieldId As String = Request("id")
            	Dim infoContent As String = Database.Tenant.GetStringValue("SELECT InfoContent FROM DataSourceField WHERE Id = " + fieldId)
            	JSON.OK(infoContent)
            Case "saveinfocontent"
            	Dim fieldId As String = Request("id")
            	Dim infoContent As String = Request("infoContent").ToString()
            	infoContent = infoContent.Replace("'","''").Replace("&lt;","<").Replace("&gt;",">")
            	Dim query As String = "UPDATE DataSourceField SET InfoContent = "
            	query += IIf(infoContent.Trim() = "", "NULL","'" + infoContent + "'") + " WHERE Id = " + fieldId
            	Database.Tenant.Execute(query)
            	JSON.OK()
            Case "viewvalidationdata"
            	Dim id As String = Request("Id")
            	Dim sql As String
            	sql = "SELECT Name,Condition,ErrorMessage,SourceTable,ValidateFirstOnly,Note from  ClientValidation where Id={0}".SqlFormatString(id)
            	JSON.SendTable(Database.Tenant, sql)
            Case "updatedtrcolorcode"
                Dim dId As String = Request("dId")
                Dim color As String = Request("color")
                Dim query As String = "Update DTR_StatusType set GIS_Flag_Color = '" + color + "' where Id = " + dId
                Database.Tenant.Execute(query)
                JSON.OK()
            Case "updateddtrstatustype"
                Dim Id As String = Request("Id")
                Dim column As String = Request("Column")
                Dim value As String = Request("Value")
                Dim query As String = "Update DTR_StatusType set " + column + " = " + value + " where Id = " + Id
                Database.Tenant.Execute(query)
                JSON.OK()
            Case "updateworkflowstatustype"
                Dim Id As String = Request("Id")
                Dim column As String = Request("Column")
                Dim value As String = Request("Value")
                Dim query As String = "Update ParcelWorkFlowStatusType set " + column + " = " + value + " where Id = " + Id
                Database.Tenant.Execute(query)
                JSON.OK()
            Case "updatefilterfields"
            	Dim Id As String = Request("Id")
                Dim value As String = Request("Value")
                Dim Name As String = Request("Name")
                Dim query As String
                If Name.Contains("agg_") Then
                    query = "UPDATE AggregateFieldSettings SET showInSearchFilter = '" & value & "' WHERE ROWUID = '" & Id & "'"
                Else
                    query = "UPDATE DataSourceField SET IncludeInSearchFilter = '" & value & "' WHERE Id = '" & Id & "'"
                End If
                Database.Tenant.Execute(query)
                JSON.OK()
            Case "loadfieldcategory"
             	Dim sql As String
             	sql = Request("Query")
             	JSON.SendTable(Database.Tenant, sql)
        End Select


    End Sub

    Public Function SetPrimarykey(tableId As Integer, tableName As String, fieldName As String, cbStatus As Boolean) As String
        Dim sql As String
        Dim Msg As String
        If cbStatus = True Then
            sql = "EXEC ccad_AddPrimaryKey {0},{1}".SqlFormatString(tableName, fieldName)
            Msg = Database.Tenant.GetStringValue(sql)

        Else
            sql = "EXEC ccad_RemovePrimaryKey {0},{1}".SqlFormatString(tableName, fieldName)
            Msg = Database.Tenant.GetStringValue(sql)
        End If
        Return Msg
    End Function
End Class
