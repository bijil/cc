﻿Imports CAMACloud.WebService.ShapeFileFactory
Imports System.Text.RegularExpressions

Public Class SketchValidationProcessor
    Inherits RequestProcessorBase


    Public Overrides Sub ProcessRequest()
        Select Case Path
            Case "lookupdatatables"
                _getLookupDataTables()
            Case "search"
                Dim fieldName As String = Request("fieldName")
                Dim fieldValue As String = Request("fieldValue").Replace("'", ControlChars.Quote)
                Dim flag As String = Request("flag")
                Dim user As String = Request("user")
                Dim searchstrng As String = Request("searchstrng")

                Dim dtparcels As DataSet = Database.Tenant.GetDataSet(String.Format("EXEC sv_SearchParcelsNew @User = {0}, @Flag = {1}, @dateChange = {2},@searchstrng ={3}", user.ToSqlValue(True), flag.ToSqlValue(True), "1", searchstrng.ToSqlValue(True)))

                Dim resp As Object
                resp = New With {
                .Parcels = dtparcels.Tables(0),
                .TotalNoGisParcels = dtparcels.Tables(1),
                 .NeighborhoodPoints = Nothing,
                 .HasNeighborhood = False,
                 .HasParcels = False,
                 .TotalParcels = 0
                }
                If fieldName = "1" Then
                    Dim nid As Integer = Database.Tenant.GetIntegerValue("SELECT Id FROM Neighborhood WHERE Number = '" + fieldValue + "'")
                    If nid > 0 Then
                        resp = New With {
                            .Parcels = dtparcels.Tables(0),
                            .TotalNoGisParcels = dtparcels.Tables(1),
                            .NeighborhoodPoints = Database.Tenant.GetDataTable("SELECT Latitude, Longitude, Ordinal FROM NeighborhoodMapPoints WHERE NbhdId = " & nid & " ORDER BY Ordinal"),
                            .HasNeighborhood = False,
                            .HasParcels = False,
                            .TotalParcels = 0
                        }
                    End If
                End If
                resp.HasNeighborhood = resp.NeighborhoodPoints IsNot Nothing AndAlso resp.NeighborhoodPoints.Rows.Count > 0
                resp.HasParcels = dtparcels.Tables(0).Rows.Count > 0
                resp.TotalParcels = dtparcels.Tables(0).Rows.Count
                JSON.Write(resp)
            Case "bulkedit"
                Dim fieldName As String = Request("fieldName")
                Dim fieldValue As String = Request("fieldValue").Replace("'", ControlChars.Quote)
                Dim flag As String = Request("flag")
                Dim user As String = Request("user")
                Dim searchstrng As String = Request("searchstrng")
                Dim flagStatus As String = Request("flagstatus")
                Dim bulkedits As String = Request("bulkedits")
                If bulkedits = 1 Then
                    Dim dtsbulkedit As DataSet = Database.Tenant.GetDataSet(String.Format("EXEC sketchvalidationBulk @User = {0}, @Flag = {1}, @dateChange = {2},@searchstrng ={3} ,@bulkedit={4} ,@flagstatus={5},@UserName={6}", user.ToSqlValue(True), flag.ToSqlValue, "1", searchstrng.ToSqlValue(True), "1", flagStatus.ToSqlValue(True), UserName.ToSqlValue(True)))
                End If
                'Dim dtparcels As DataSet = Database.Tenant.GetDataSet(String.Format("EXEC sketchvalidationBulk @User = {0}, @Flag = {1}, @dateChange = {2},@searchstrng ={3},@bulkedit={4}", user.ToSqlValue(True), flag.ToSqlValue(True), "1", searchstrng.ToSqlValue(True), "0"))
                Dim dtparcelsbulkedit As DataSet = Database.Tenant.GetDataSet(String.Format("EXEC sketchvalidationBulk @User = {0}, @Flag = {1}, @dateChange = {2},@searchstrng ={3} ,@bulkedit={4} ,@flagstatus={5},@UserName={6}", user.ToSqlValue(True), flag.ToSqlValue, "1", searchstrng.ToSqlValue(True), "0", flagStatus.ToSqlValue(True), UserName.ToSqlValue(True)))


                Dim resp As Object
                resp = New With {
                            .Parcels = dtparcelsbulkedit.Tables(0),
                            .TotalNoGisParcels = dtparcelsbulkedit.Tables(1),
                            .NeighborhoodPoints = Nothing,
                            .HasNeighborhood = False,
                            .HasParcels = False,
                            .TotalParcels = 0,
                            .username = UserName}

                resp.HasNeighborhood = resp.NeighborhoodPoints IsNot Nothing AndAlso resp.NeighborhoodPoints.Rows.Count > 0
                resp.HasParcels = dtparcelsbulkedit.Tables(0).Rows.Count > 0
                resp.TotalParcels = dtparcelsbulkedit.Tables(0).Rows.Count
                JSON.Write(resp)
            Case "getparcel"
                Dim p As String = Request("parcel")
                Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM Parcel WHERE KeyValue1 = '" + p + "'")

                Dim pid As String = dr.GetString("Id")
                Dim data As DataRow = Database.Tenant.GetTopRow("SELECT * FROM ParcelData WHERE CC_ParcelId = " + pid)
                Dim map As DataTable = Database.Tenant.GetDataTable("SELECT Latitude, Longitude, Ordinal FROM ParcelMapPoints WHERE ParcelId = " + pid + " ORDER BY Ordinal")
                Dim PositiondataTable As DataTable = Database.Tenant.GetDataTable("SELECT * FROM SketchData WHERE parcelId = " + pid)
                Dim resp As Object
                If PositiondataTable.Rows.Count < 1 Then
                    If dr("SketchAnchorLatitude") Is DBNull.Value Then
                        resp = New With {.dataExist = False, .CCId = pid, .ParcelId = p, .SketchVector = data("Sketch").ToString(), .Points = map, .Latitude = 0.0F, .Longitude = 0.0F}
                    Else
                        resp = New With {.dataExist = True, .CCId = pid, .ParcelId = p, .SketchVector = data.GetString("Sketch"), .Points = map, .Latitude = 0.0F, .Longitude = 0.0F, .degree = dr.GetString("SketchRotation"), .sketchZoom = dr.GetString("SketchZoom"), .mapZoom = dr.GetString("MapZoom"), .lat = dr.GetString("SketchAnchorLatitude"), .lng = dr.GetString("SketchAnchorLongitude")}
                    End If

                Else
                    Dim Positiondata As DataRow = PositiondataTable.Rows(0)
                    resp = New With {.dataExist = True, .CCId = pid, .ParcelId = p, .SketchVector = data.GetString("Sketch"), .Points = map, .Latitude = 0.0F, .Longitude = 0.0F, .degree = Positiondata.GetString("Degree"), .sketchZoom = Positiondata.GetString("SketchZoom"), .mapZoom = Positiondata.GetString("MapZoom"), .lat = Positiondata.GetString("CenterPointLat"), .lng = Positiondata.GetString("CenterPointLng")}
                End If

                Response.Clear()
                Response.ContentType = "application/json"
                Response.Write(JSONResponse.Serialize(resp))
            Case "getparcelfromid"
                'Dim p As String = Request("pid")
                'If p = "" Then
                '    p = "0"
                'End If
                'Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT p.*, dbo.GetLocalDate(SketchReviewedDate) As SketchReviewedOn, COALESCE(sf.ColorCode, '#FF0000') As ColorCode FROM Parcel p LEFT OUTER JOIN SketchStatusFlags sf ON p.SketchFlag = sf.Id WHERE p.Id = '" + p + "'")
                'GetParcelData(dr)
                Dim sketchOnly = Request("quick") = "1"
                _getParcelDetails(sketchOnly)
            Case "updatesketch"
                Dim sql As String
                Dim p As String = Request("parcel")
                Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM Parcel WHERE KeyValue1 = '" + p + "'")
                Dim pid As String = dr.GetString("Id")
                Dim exist As String = Request("exist")
                If exist Then
                    sql = "UPDATE SketchData SET Degree={0},sketchSize={1},sketchZoom={2},MapZoom={3},CenterPointLat={4},CenterPointLng={5} WHERE parcelId={6}".FormatString(Request("degree"), Request("canvasSize"), Request("sketchZoom"), Request("mapZoom"), Request("lat"), Request("lng"), pid)
                Else
                    sql = "INSERT INTO SketchData VALUES({0},{1},{2},{3},{4},{5},{6})".FormatString(pid, Request("degree"), Request("canvasSize"), Request("sketchZoom"), Request("mapZoom"), Request("lat"), Request("lng"))
                End If
                Database.Tenant.Execute(sql)
                Response.Clear()
                Response.ContentType = "text/plain"
                Response.Write("success")
            Case "saveForm"
                Dim sql As String
                Dim p As String = Request("parcel")
                Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM Parcel WHERE KeyValue1 = '" + p + "'")
                Dim pid As String = dr.GetString("Id")
                Dim exist As String = Request("exist")
                Dim result As String = "unknown value"
                If exist Then
                    sql = "UPDATE SketchData SET Comment={0}, FlagChecked={1},IsValid={2} WHERE parcelId={3}".FormatString(Request("comment"), Request("flag"), Request("valid"), pid)
                    result = "success"
                    Database.Tenant.Execute(sql)
                Else
                    result = "error"
                End If

                Response.Clear()
                Response.ContentType = "text/plain"
                Response.Write("error")
            Case "savereview"
                Dim pid As String = Request("pid")
                Dim flags As String = Request("flag")
                Dim oFlags As String
                Dim result As String
                Dim notes As String = Request("notes")
                Dim szoom As String = Request("szoom")
                Dim mzoom As String = Request("mzoom")
                Dim rotate As String = Request("rotate")
                Dim status As String = Request("status")
                Dim salat As String = Request("salat")
                Dim salng As String = Request("salng")
                Dim SketchData As String = Request("sketchData")
                Dim description As String = Request("Description")
                Dim sketchLatLng As String = Request("sketchLatLng")
                If flags = "" Then
                    flags = "99999"
                Else
                    Dim flag = flags.Split(",")
                    For i As Integer = 0 To flag.Length - 1
                        oFlags = Database.Tenant.GetStringValue("select name from SketchReviewFlags where Id ={0}".SqlFormatString(flag(i)))
                        result += oFlags + ","
                    Next
                End If
                Dim sql As String = "UPDATE Parcel SET SketchReviewNote = {1}, SketchZoom = {2}, SketchRotation = {3}, MapZoom = {4}, SketchReviewedBy = {5}, SketchReviewedDate = GETUTCDATE(), SketchAnchorLatitude = {6}, SketchAnchorLongitude = {7}, SketchData={8}, sketchLatLng={9}, NewSketchData = {10} WHERE Id = {0}"
                Database.Tenant.Execute(sql.SqlFormat(True, pid, notes, szoom, rotate, mzoom, UserName, salat, salng, SketchData, sketchLatLng, SketchData))
                Database.Tenant.Execute("UPDATE Parcel SET SketchFlag = {1} WHERE Id = {0}".SqlFormat(True, pid, status))
                Dim Sketchflg As String = Database.Tenant.GetStringValue("SELECT SketchFlag from Parcel where Id = " & pid)
                Dim flagSql As String = "DELETE FROM ParcelSketchFlag WHERE ParcelId = {0} AND FlagId NOT IN ({1}); INSERT INTO ParcelSketchFlag SELECT {0}, Id FROM SketchReviewFlags WHERE Id IN ({1}) AND Id NOT IN (SELECT FlagId FROM ParcelSketchFlag WHERE ParcelId = {0});"
                Database.Tenant.Execute(flagSql.FormatString(pid, flags))

                Dim desc As String = "Sketch reviewed. "
                If description = 0 Then
                    desc = "Sketch reset by " + UserName
                End If
                If status <> "" Then
                    Dim flagName As String = Database.Tenant.GetStringValue("SELECT Name FROM SketchStatusFlags WHERE Id = " & status)
                    desc += "Flagged as " + flagName + ". "
                End If
                If notes.Length > 0 Then
                    desc += "Notes : " + notes + " added. "
                End If
                If oFlags <> "" Then
                    result = result.Substring(0, result.Length - 1)
                    desc += "Other Flags : " + result + " has added."
                End If

                Dim geoReferenceInsertSql As String = "DELETE FROM SV_GIS_Vectors WHERE CC_ParcelId = " & pid & vbNewLine

                If status = "4" Or status = "5" Then
                    Dim pgData As String = Request("polygonArray")
                    Dim pmp = Newtonsoft.Json.JsonConvert.DeserializeObject(Of ParcelMultiPolygon)(pgData)

                    Dim vi As Integer = 0
                    For Each psk In pmp.polygonArray
                        For Each vector In psk.vectors
                            vi += 1
                            Dim sqlVector = "INSERT INTO SV_GIS_Vectors (CC_ParcelId, VectorNumber, Label, Points) VALUES ({0}, {1}, {2}, {3}); ".SqlFormat(False, pid, vi, vector.label, vector.ToLatLngString)
                            geoReferenceInsertSql += sqlVector + vbNewLine
                        Next
                    Next

                    Database.Tenant.Execute(geoReferenceInsertSql)
                    Database.Tenant.Execute("UPDATE Parcel SET CC_GeoreferencedDate = GETUTCDATE(), CC_GeoReferencedBy = " + UserName.ToSqlValue + " WHERE Id = " + pid)
                Else
                    Database.Tenant.Execute(geoReferenceInsertSql)
                    Database.Tenant.Execute("UPDATE Parcel SET CC_GeoreferencedDate = NULL, CC_GeoReferencedBy = NULL WHERE Id = " + pid)
                End If

                Database.Tenant.Execute("INSERT INTO ParcelAuditTrail (ParcelId, LoginID, EventType, Description,ApplicationType) VALUES ({0}, {1}, {2}, {3},'SV')".SqlFormat(True, pid, UserName, 10, desc))
                JSON.OK()
            Case "loadtables"
                Dim resp As Object
                Dim Svtable As String = Database.Tenant.GetStringValue("SELECT  value from clientsettings where name='sv_lookup_tables'")
                Dim Fields As DataTable = FieldCategory.GetFields(Svtable)
                Dim t = (From row In Fields.AsEnumerable() Where row.Field(Of String)("LookupTable") <> "" And row.Field(Of String)("LookupTable") <> "$QUERY$" Select row.Field(Of String)("LookupTable")).Distinct().ToList()
                Dim k As String = String.Join("','", t)
                Fields = FieldCategory.GetFields()
                Dim datafieldsettings As DataTable = Database.Tenant.GetDataTable("SELECT * FROM DataSourceFieldProperties")	
                Dim sketchSettings As DataTable = Database.Tenant.GetDataTable("SELECT * FROM SketchSettings")
                Dim Sketch_Config = Database.Tenant.GetStringValue("SELECT Value FROM clientsettings WHERE Name = 'SketchConfig'")
                If Sketch_Config Is Nothing Or Sketch_Config = "" Then
                	Sketch_Config = Database.Tenant.GetStringValue("SELECT Value FROM SketchSettings WHERE Name = 'SketchConfig'")
                End If
                If Sketch_Config = "ProValNew" Or Sketch_Config = "ProValNewFYL" Then
                    k += "','ob_codes_imp_type"
                End If
                Dim Lookup As DataTable = Database.Tenant.GetDataTable("SELECT  Id, LookupName AS Source, IdValue AS Value, NameValue AS Name, DescValue AS Description, Ordinal, ColorCode as Color, value as NumericValue, AdditionalValue1, AdditionalValue2 FROM dbo.ParcelDataLookup") '+ IIf(k = "", "", " Where LookupName in('" + k + "')"))
                Dim tableList = FieldCategory.GetFieldCategories
                Dim clientSettings = Database.Tenant.GetDataTable("SELECT * FROM clientsettings UNION SELECT 'ShowKeyValue1' as Name, CONVERT(varchar(5),ShowKeyValue1) as Value from Application UNION SELECT 'ShowAlternateField' as Name, CONVERT(varchar(5),ShowAlternateField) as Value from Application")
                Dim tableKeys = Database.Tenant.GetDataTable(" SELECT f.Name,t.Name as SourceTable FROM DataSourceField f INNER JOIN  DataSourceKeys k ON f.Id = k.FieldId INNER JOIN  DataSourceRelationships r ON k.RelationshipId = r.Id INNER JOIN DataSourceTable t ON r.TableId = t.Id WHERE isPrimaryKey=1 ")
                Dim sk_Lookup, Outbuliding_Lookup, Default_Exterior_Cover, sketch_codes, sketch_labels, detail_Lookup, addons_Lookup, floor_lookup, patriot_Lookup, lafayette_Lookup, farragut_Lookup, sigmaCuyahoga_Lookup, WrightAddn_Lookup, WrightCom_Lookup, WrightComType_Lookup

                If Sketch_Config = "MVP" Then
                    Dim _skLookup = Database.Tenant.GetDataTable("SELECT s.*,c.tbl_element,c.field_2,c.CodesToSysType FROM LT_sketch_codes s join LT_codes_table c on s.tbl_element_desc != 'N/A' and s.tbl_element_desc=c.tbl_element_desc and s.field_1=c.field_1 ORDER BY s.Ordinal")
                    Dim _Outbuliding_Lookup = Database.Tenant.GetDataTable("SELECT label_code, label_short, mdescr, label_text FROM LT_imp_labels i JOIN LT_t_systype t ON i.label_text = t.msysTypeId ORDER BY label_description")
                    Dim _Default_Exterior_Cover = Database.Tenant.GetDataTable("SELECT * FROM LT_codes_table WHERE tbl_type_code = 'extcover'")
                    Dim _sketch_codes = Database.Tenant.GetDataTable("SELECT * FROM LT_sketch_codes")
                    Dim _sketch_labels = Database.Tenant.GetDataTable("SELECT * FROM LT_sketch_labels")
                    sk_Lookup = JSON.GetTableAsJSON(_skLookup)
                    Outbuliding_Lookup = JSON.GetTableAsJSON(_Outbuliding_Lookup)
                    Default_Exterior_Cover = JSON.GetTableAsJSON(_Default_Exterior_Cover)
                    sketch_codes = JSON.GetTableAsJSON(_sketch_codes)
                    sketch_labels = JSON.GetTableAsJSON(_sketch_labels)
                End If
                If Sketch_Config = "ProValNew" Or Sketch_Config = "ProValNewFYL" Then
                    Dim _skLookup = Database.Tenant.GetDataTable("SELECT s.*,c.tbl_element,c.field_2 FROM LT_sketch_codes s join LT_codes_table c on s.tbl_element_desc=c.tbl_element_desc and s.field_1=c.field_1 ORDER BY s.Ordinal")
                    Dim _Outbuliding_Lookup = Database.Tenant.GetDataTable("SELECT (label_code) Code,label_description,UPPER(label_short) label_short FROM LT_imp_labels WHERE (label_code like 'O%') OR (label_code like 'S%') ORDER BY label_description ASC")
                    Dim _Default_Exterior_Cover = Database.Tenant.GetDataTable("SELECT tbl_element, tbl_element_desc, tbl_type_code FROM LT_codes_table WHERE tbl_type_code = 'extcover' or tbl_type_code = 'MSExtWall' or tbl_type_code = 'extframe' or tbl_type_code = 'const'")
                    Dim _sketch_codes = Database.Tenant.GetDataTable("SELECT * FROM LT_sketch_codes")
                    sk_Lookup = JSON.GetTableAsJSON(_skLookup)
                    Outbuliding_Lookup = JSON.GetTableAsJSON(_Outbuliding_Lookup)
                    Default_Exterior_Cover = JSON.GetTableAsJSON(_Default_Exterior_Cover)
                    sketch_codes = JSON.GetTableAsJSON(_sketch_codes)
                End If
                If Sketch_Config = "FTC" Or Sketch_Config = "ApexJson" Then
                    Dim _detail_Lookup = Database.Tenant.GetDataTable("SELECT DETAILCALCULATIONTYPE, IMPDETAILTYPEID, IMPDETAILDESCRIPTION, IMPDETAILTYPE, APEXLINKFLAG, ACTIVEFLAG, APEXAREACODE FROM LT_TLKPIMPSDETAILTYPE ORDER BY SORTORDER")
                    Dim _addons_Lookup = Database.Tenant.GetDataTable("SELECT ADDONFILTERTYPE, ADDONDESCRIPTION, IMPDETAILTYPE, JURISDICTIONID, APEXLINKFLAG, ACTIVEFLAG, ADDONCODE FROM LT_TLKPIMPSADDONS ORDER BY SORTORDER")
                    Dim _floor_lookup = Database.Tenant.GetDataTable("SELECT IMPSFLOORDESCRIPTION, APEXLINKFLAG, ACTIVEFLAG FROM LT_TLKPIMPSFLOOR ORDER BY SORTORDER")
                    detail_Lookup = JSON.GetTableAsJSON(_detail_Lookup)
                    addons_Lookup = JSON.GetTableAsJSON(_addons_Lookup)
                    floor_lookup = JSON.GetTableAsJSON(_floor_lookup)
                End If
                If Sketch_Config = "Lafayette" Then
                	Dim _lafayette_Lookup = Database.Tenant.GetDataTable("SELECT * FROM LT_lu_sar")
                	lafayette_Lookup = JSON.GetTableAsJSON(_lafayette_Lookup)
                End If
                If Sketch_Config = "Patriot" Then
                	Dim _patriot_Lookup = Database.Tenant.GetDataTable("SELECT * FROM LT_CCV_xrSubArea")
                	patriot_Lookup = JSON.GetTableAsJSON(_patriot_Lookup)
                End If
                If Sketch_Config = "FarragutJson" Then
                    Dim _farragut_Lookup = Database.Tenant.GetDataTable("SELECT * FROM LT_SKETCH_CODES_LOOKUP")
                    farragut_Lookup = JSON.GetTableAsJSON(_farragut_Lookup)
                End If
                If Sketch_Config = "SigmaCuyahoga" Then
                    Dim _sigmaCuyahoga_Lookup = Database.Tenant.GetDataTable("SELECT CODE, DESCRIPTION [Desc] FROM LT_VALUE_LIST WHERE TABLE_NAME = 'RES_AMENITY' AND COLUMN_NAME = 'AMENITY_TYPE'")
                    sigmaCuyahoga_Lookup = JSON.GetTableAsJSON(_sigmaCuyahoga_Lookup)
                End If
                If Sketch_Config = "WrightJson" Then
                    Dim _WrightAddn_Lookup = Database.Tenant.GetDataTable("SELECT p.[CODE] Id, CONCAT(p.[CODE], ' - ', p.[NAME]) Name, p.[DESCR], p.[NAME] LName, f.[BASEAREA], f.[FIRST], f.[LOWER], f.[SECOND], f.[THIRD], t.TBLE FROM LT_CCV_APEX_CODES_APEX_CODES_AUD p JOIN LT_CCV_APEX_IAS_TAB_REF_APEX_IAS_TAB_REF_AUD t ON p.CODE = t.CODE JOIN LT_CCV_APEX_ADDN_APEX_ADDN_AUD f ON p.CODE = f.CODE WHERE t.TBLE = 'ADDN' AND p.AUD_OP='I' AND f.AUD_OP='I'")
                    WrightAddn_Lookup = JSON.GetTableAsJSON(_WrightAddn_Lookup)
                    Dim _WrightCom_Lookup = Database.Tenant.GetDataTable("SELECT p.[CODE] Id, CONCAT(p.[CODE], ' - ', p.[NAME]) Name, p.[DESCR], p.[NAME] LName, t.TBLE FROM LT_CCV_APEX_CODES_APEX_CODES_AUD p JOIN LT_CCV_APEX_IAS_TAB_REF_APEX_IAS_TAB_REF_AUD t ON p.CODE = t.CODE WHERE t.TBLE IN ('COMINTEXT', 'COMFEAT') AND p.AUD_OP='I'")
                    WrightCom_Lookup = JSON.GetTableAsJSON(_WrightCom_Lookup)
                    Dim _WrightComType_Lookup = Database.Tenant.GetDataTable("SELECT [CODE] Id, [USETYPE] FROM LT_CCV_APEX_COMINTEXT_APEX_COMINTEXT_AUD WHERE USETYPE IS NOT NULL")
                    WrightComType_Lookup = JSON.GetTableAsJSON(_WrightComType_Lookup)
                End If
                Dim DTR_Tasks = Database.Tenant.GetDataTable("select * from DTR_Tasks")
                Dim DTR_StatusType = Database.Tenant.GetDataTable("select * from DTR_StatusType")
                resp = New With {.tableKeys = tableKeys, .lookup = Lookup, .fields = Fields, .parentChild = DataSource.GetParentChildTable, .fieldCategories = tableList, .clientSettings = clientSettings, .sketchSettings = sketchSettings, .sk_Lookup = sk_Lookup, .Outbuliding_Lookup = Outbuliding_Lookup, .Default_Exterior_Cover = Default_Exterior_Cover, .sketch_codes = sketch_codes, .sketch_labels = sketch_labels, .detail_Lookup = detail_Lookup, .addons_Lookup = addons_Lookup, .floor_lookup = floor_lookup, .lafayette_Lookup = lafayette_Lookup, .patriot_Lookup = patriot_Lookup, .farragut_Lookup = farragut_Lookup, .sigmaCuyahoga_Lookup = sigmaCuyahoga_Lookup, .datafieldsettings = datafieldsettings, .DTR_StatusType = DTR_StatusType, .DTR_Tasks = DTR_Tasks, .WrightAddn_Lookup = WrightAddn_Lookup, .WrightCom_Lookup = WrightCom_Lookup, .WrightComType_Lookup = WrightComType_Lookup}
                Response.Clear()
                Response.ContentType = "application/json"
                Response.Write(JSONResponse.Serialize(resp))
            Case "exportkml"
                Try
                    Dim polyArray As String = Request("polyArray")
                    Dim keyValue1 = Request("KeyValue1")
                    Dim StreetAddress = Request("StreetAddress")
                    Dim selectedSketch = Request("selectedSketch")
                    Dim polyArrays As String() = polyArray.Split("$,")
                    Dim doc = New XDocument(New XDeclaration("1.0", "utf-8", True))
                    Dim kmlElmnt = <kml xmlns="http://www.opengis.net/kml/2.2"></kml>
                    Dim docElmnt = <Document></Document>
                    Dim stElmnt As XElement = <Style id="transRedPoly"><LineStyle><width>1.5</width></LineStyle><PolyStyle><color>7d0000ff</color></PolyStyle></Style>
                    docElmnt.Add(stElmnt)
                    Dim decElmnt As XElement = <description></description>
                    decElmnt.Value = keyValue1
                    Dim nameElmnt As XElement = <name></name>
                    nameElmnt.Value = keyValue1
                    docElmnt.Add(nameElmnt)
                    docElmnt.Add(decElmnt)
                    For Each polyPoints In polyArrays
                        If polyPoints(0) = "," Then
                            polyPoints = polyPoints.Remove(0, 1)
                        End If
                        Dim plmkElmnt = <Placemark><styleUrl>#transRedPoly</styleUrl></Placemark>
                        Dim polyElmnt = <Polygon><extrude>1</extrude><altitudeMode>relativeToGround</altitudeMode></Polygon>
                        Dim outELmnt = <outerBoundaryIs></outerBoundaryIs>
                        Dim linElmnt = <LinearRing></LinearRing>
                        Dim cordElmnt As XElement = <coordinates></coordinates>
                        cordElmnt.Value = polyPoints
                        linElmnt.Add(cordElmnt)
                        outELmnt.Add(linElmnt)
                        polyElmnt.Add(outELmnt)
                        plmkElmnt.Add(polyElmnt)
                        docElmnt.Add(plmkElmnt)
                    Next
                    kmlElmnt.Add(docElmnt)
                    doc.Add(kmlElmnt)
                    Dim exportName = keyValue1 + ".kml"
                    Dim contentXML = doc.ToString(SaveOptions.None)
                    Response.WriteAsAttachment(contentXML, exportName)
                Catch ex As Exception
                    Throw New Exception(ex.StackTrace)
                End Try
            Case "exportshp"
                Dim polyArray As String = Request("polyArray")
                Dim keyValue1 = Request("KeyValue1")
                Dim StreetAddress = Request("StreetAddress")
                Dim selectedSketch = Request("selectedSketch")
                Dim polyArrays As String() = polyArray.Split("$,")

                Dim shp As New GISShapeFile()
                shp.KeyValue = keyValue1

                Dim sketch As New GISShapeGroup
                For Each polyPoints In polyArrays
                    If polyPoints(0) = "," Then
                        polyPoints = polyPoints.Remove(0, 1)
                    End If
                    Dim vector As New GISShape
                    Dim nodes = polyPoints.Split(vbLf)
                    For Each node In nodes
                        If (node <> "") Then
                            Dim coord = node.Split(",").ToArray
                            vector.AddVertex(coord(0), coord(1))
                        End If
                    Next
                    sketch.AddShape(vector)
                Next
                shp.AddSketch(sketch)

                Dim ms As New IO.MemoryStream
                shp.Save(ms)
                Dim shpBase64 = Convert.ToBase64String(ms.ToArray)

                Response.Clear()
                Response.ContentType = "text/plain"
                Response.Write(shpBase64)
                'Response.AppendHeader("Content-Disposition", "attachment; filename=""" & keyValue1 & ".zip")
                'shp.Save(Response.OutputStream)
                Response.Flush()
                HttpContext.Current.ApplicationInstance.CompleteRequest()

        End Select
    End Sub
    Private Sub _getLookupDataTables()
        JSON.StartTick()
        Dim objectCount As Integer = 0
        Dim items As New List(Of String)
        Dim lookupTableNames As New List(Of String)

        For Each tableName In DB.GetDataTable("SELECT t.Name FROM DataSourceTable t LEFT OUTER JOIN DataSourceRelationships r ON t.Id = r.TableId AND r.Relationship IS NOT NULL WHERE NOT (t.Name LIKE '`_%' ESCAPE '`') AND t.Name NOT IN (SELECT ParcelTable FROM Application UNION SELECT NeighborhoodTable FROM Application UNION SELECT '_photo_meta_data') GROUP BY t.Name HAVING COUNT(r.TableId) = 0").Rows.OfType(Of DataRow).Select(Function(x) x.GetString("Name")).ToArray
            lookupTableNames.Add(tableName)
        Next

        For Each tableName In lookupTableNames
            Dim sqlTableName As String = "LT_" + tableName
            If Database.Tenant.DoesTableExists(sqlTableName) Then
                Dim tableData As String = JSON.GetTableAsJSON(MobileSyncDownloadProcessor.SyncLookupDataTable(UserName, Request("nbhd"), tableName))
                objectCount += JSON.ObjectsAffected
                If tableData <> "[]" Then
                    items.Add(tableData.TrimStart("[").TrimEnd("]"))
                End If
            End If
        Next
        Dim js As String = "[" + String.Join(", ", items.ToArray) + "]"
        JSON.WriteString(js)
        JSON.EndTick()
        JSON.SetObjectCount(objectCount)
    End Sub
    Private Sub _getParcelDetails(Optional sketchOnly As Boolean = False)
        Dim sParcelId As String = Request("pid")
        Dim parcelId As Integer
        If Integer.TryParse(sParcelId, parcelId) Then
            '   Dim pds As DataSet = Database.Tenant.GetDataSet("EXEC qc_GetParcelData " & parcelId)
            Dim parcelData As DataTable = Database.Tenant.GetDataTable("SELECT p.Id,keyvalue1 as ParcelID,KeyValue1,KeyValue2,KeyValue3, StreetAddress,'0.0F' as Latitude,'0.0F' as Longitude  ,p.ParcelAlert as ParcelAlert, SketchReviewedBy as ReviewedBy,SketchReviewedDate as ReviewdDate,COALESCE(SketchZoom, 1.0) As sketchZoom,COALESCE(SketchRotation, 0) As SketchRotate, COALESCE(MapZoom, 0) As MapZoom,SketchReviewNote, dbo.GetLocalDate(SketchReviewedDate) As SketchReviewedOn,SketchFlag, COALESCE(sf.ColorCode, '#FF0000') As ColorCode,SketchAnchorLatitude as lat,SketchAnchorLongitude as lng, SketchData, sketchLatLng, NewSketchData, nb.Number As CC_NBDH, pd.* FROM Parcel p  LEFT OUTER JOIN parceldata pd on p.id=pd.CC_parcelId LEFT OUTER JOIN SketchStatusFlags sf ON p.SketchFlag = sf.Id LEFT OUTER JOIN Neighborhood nb ON p.NeighborhoodId = nb.Id WHERE p.Id = '" + sParcelId + "'")
            Dim alternateField = Database.Tenant.GetStringValue("SELECT Alternatekeyfieldvalue from application")
            Dim alternateFieldvalue
            If alternateField <> "" Then
                alternateFieldvalue = Database.Tenant.GetStringValue(" SELECT " + alternateField + " from ParcelData pd  inner join parcel p on p.id = pd.CC_ParcelId where p.id = '" + sParcelId + "'")
            End If
            ' Dim images As DataTable = pds.Tables(1)
            Dim mapPoints As DataTable = Database.Tenant.GetDataTable("SELECT Latitude, Longitude, Ordinal, RecordNumber FROM ParcelMapPoints WHERE ParcelId = " + sParcelId + " ORDER BY Ordinal")
            If mapPoints.Rows.Count = 0 Then
                Dim Bpp As String = Database.Tenant.GetStringValue("SELECT BPPEnabled FROM Application")
                If Bpp = "True" Then
                    Dim BppParent As String = Database.Tenant.GetStringValue("select ParentParcelID from Parcel WHERE Id = " + sParcelId)
                    If BppParent <> "" Then
                        mapPoints = Database.Tenant.GetDataTable("SELECT Latitude, Longitude, Ordinal, RecordNumber FROM ParcelMapPoints WHERE ParcelId = " + BppParent + " ORDER BY Ordinal")
                    End If
                End If
            End If
            Dim flags As DataTable = Database.Tenant.GetDataTable("SELECT sf.Id, CAST(CASE WHEN pf.Id IS NULL THEN 0 ELSE 1 END AS BIT) As Value FROM SketchReviewFlags sf LEFT OUTER JOIN ParcelSketchFlag pf ON pf.FlagId = sf.Id AND pf.ParcelId = " + sParcelId)
            Dim settings As String = Database.Tenant.GetStringValue("select value from clientsettings where name='SketchConfig' ")
            If settings Is Nothing Or settings = "" Then
            	settings = Database.Tenant.GetStringValue("SELECT Value FROM SketchSettings WHERE Name = 'SketchConfig'")
            End If
            Dim sketchstatusflags As DataTable = Database.Tenant.GetDataTable("SELECT * FROM sketchstatusflags")
            Dim sketchSettings As DataTable = Database.Tenant.GetDataTable("SELECT * FROM SketchSettings")
            If parcelData.Rows.Count = 0 Then
                JSON.Error("Parcel not found")
                Return
            End If
            ''Dim ParcelAlert As String = Database.Tenant.GetStringValue("Select ParcelAlert from Parcel where Id = " + sParcelId)
            Dim PositiondataTable As DataTable = Database.Tenant.GetDataTable("SELECT * FROM SketchData WHERE parcelId = " + sParcelId)
            Dim firstphotoPath As String = Database.Tenant.GetStringValue("IF EXISTS(SELECT  * FROM parcelimages where ParcelId =  " + sParcelId + " and IsPrimary = 1) SELECT top 1 Path FROM parcelimages where ParcelId =  " + sParcelId + " and IsPrimary = 1 ELSE SELECT top 1 Path FROM parcelimages where ParcelId =  " + sParcelId + "")
            Dim firstPhoto As String = ""
            If Not sketchOnly Then
                firstPhoto = MobileSyncDownloadProcessor.ConvertPhotoToBase64(firstphotoPath)
            End If
            
            Dim _sktTablesArray()  As String
			If sketchOnly Then
				Dim _sktConfigTables =  New Dictionary(Of String, String()) 
				Try
		            _sktConfigTables =  VbSketchConfig._sketchConfigTables
		            _sktTablesArray = _sktConfigTables.Item(settings)
				Catch ex As Exception
				End Try	
			End If
			
            Dim parcelChanges As DataTable
            'If Not sketchOnly Then
            parcelChanges = Database.Tenant.GetDataTable("SELECT pc.*, fc.Id As CategoryId,f.Name As FieldName,f.SourceTable, dbo.GetLocalDate(pc.ChangedTime) as ChangedTimeNew, dbo.GetLocalDate(pc.ChangedTime) as LocalChangedTime FROM ParcelChanges pc LEFT OUTER JOIN DataSourceField f ON pc.FieldId = f.Id LEFT OUTER JOIN FieldCategory fc ON f.CategoryId = fc.Id WHERE ParcelId = " + sParcelId + " ORDER BY ChangedTime ")
            'End If
            Dim DataExist As Boolean = True
            If PositiondataTable.Rows.Count < 1 And parcelData.Rows(0)("lat") Is DBNull.Value And parcelData.Rows(0)("NewSketchData") Is DBNull.Value Then
                DataExist = False
            End If

            Dim relatedTableNames As New List(Of String)
            If Database.Tenant.Application.IsAPISyncModel Then
                Dim primaryTable As String = Database.Tenant.Application.ParcelTable
                relatedTableNames = GetRelatedTables(Database.Tenant)

                If sketchOnly Then
                    'Dim sketchTables = Database.Tenant.GetDataTable("SELECT DISTINCT t.Name FROM DataSourceField f INNER JOIN DataSourceTable t ON f.TableId = t.Id WHERE f.Name = 'CC_SKETCH'").ToRows.Select(Function(x) x.GetString("Name")).ToList
                    'relatedTableNames = relatedTableNames.Intersect(_sktTablesArray).ToList
                    If _sktTablesArray Is Nothing Then
                    	_sktTablesArray = {}
                    End If                   	
                    relatedTableNames = _sktTablesArray.ToList
                End If
            Else
                For Each dr As DataRow In Database.Tenant.GetDataTable("SELECT Name FROM DataSourceTable WHERE ImportType = 1").Rows
                    relatedTableNames.Add(dr.GetString("Name"))
                Next
            End If

            Dim tables As New List(Of String)
            Dim missingTables As New List(Of String)
            Dim auxFetchSql As String = ""
            For Each tableName In relatedTableNames
                Dim sqlTableName As String = "XT_" + tableName
                If Database.Tenant.DoesTableExists(sqlTableName) Then
                    Dim sortFormula = FieldCategory.GetSortExpression(tableName)
                    'Dim tableData As String = JSON.GetTableAsJSON(Database.Tenant, "SELECT * FROM " + sqlTableName + " WHERE (CC_Deleted = 0 OR CC_Deleted IS NULL) AND CC_ParcelId = " & parcelId)
                    Dim auxSql As String = ""
                    If sketchOnly Then
                    	auxSql = "SELECT * FROM " + sqlTableName + " WHERE (CC_Deleted = 0 OR CC_Deleted IS NULL) AND " + " CC_ParcelId = " & parcelId & "" & sortFormula + ";"
                    Else
                    	auxSql = "SELECT * FROM " + sqlTableName + " WHERE " + " CC_ParcelId = " & parcelId & "" & sortFormula + ";"
                    End If
                    auxFetchSql += auxSql
                    'Dim tableData As String = JSON.GetTableAsJSON(Database.Tenant, auxSql)
                    tables.Add(tableName)
                    'jstr += ",""" + tableName + """:" + tableData
                Else
                    missingTables.Add(tableName)
                    'jstr += ",""" + tableName + """: []"
                End If
            Next

            Dim tempjstr As String = ""

            If auxFetchSql.Trim <> "" Then
                Dim auxDataSet As DataSet = Database.Tenant.GetDataSet(auxFetchSql)
                For i As Integer = 0 To tables.Count - 1
                    tempjstr += ",""" + tables(i) + """:" + JSON.GetTableAsJSON(auxDataSet.Tables(i))
                Next
                If Not sketchOnly Then
                    Dim aggregateFieldSettings As DataTable = Database.Tenant.GetDataTable("SELECT * FROM AggregateFieldSettings ORDER BY TableName")
                    Dim minAccountLevel As Integer = Integer.MaxValue
                    Dim maxAccountLevel As Integer = Integer.MinValue
                    For Each dr As DataRow In aggregateFieldSettings.Rows
                        Dim tableName As String = dr(1).ToString()
                        Dim colName As String = dr(2).ToString()
                        Dim functName As String = dr(3).ToString()
                        Dim fieldName As String
                        Dim uniqueName As String = dr(6).ToString()
                        If uniqueName <> "" Then
                            fieldName = uniqueName
                        Else
                            fieldName = functName + "_" + tableName + "_" + colName
                        End If
                        Dim Condition As String = Regex.Replace(dr(5).ToString(), Chr(34), Chr(39))
                        Dim fieldId As String = Database.Tenant.GetStringValue("SELECT Id FROM DataSourceField WHERE Name = '" + colName + "' AND SourceTable = '" + tableName + "'")
                        Dim pcDataRow As DataRow()
                        If fieldId <> Nothing Or fieldId <> "" Then
                            pcDataRow = parcelChanges.Select("FieldId = '" + fieldId + "'")
                        End If
                        Dim value As String = Nothing
                        Dim medianArray() As Double
                        If tableName <> "Parcel" Then
                            For i As Integer = 0 To tables.Count - 1
                                If tableName = tables(i) Then
                                    Dim dt As DataTable = auxDataSet.Tables(i)
                                    If dt.Rows.Count > 0 Then
                                        For Each pcRow As DataRow In pcDataRow
                                            Dim j As Integer = 0
                                            For Each dtRow As DataRow In dt.Rows
                                                If pcRow("AuxROWUID").ToString() = dtRow("ROWUID").ToString() Then
                                                    dt.Rows(j)(colName) = IIf(IsDBNull(pcRow("NewValue")), DBNull.Value, pcRow("NewValue").ToString())
                                                End If
                                                j += 1
                                            Next
                                        Next
                                        Try
                                            Select Case functName
                                                Case "FIRST"
                                                    value = dt.Rows(0)(colName).ToString()
                                                Case "LAST"
                                                    value = dt.Rows(dt.Rows.Count - 1)(colName).ToString()
                                                Case "COUNT"
                                                	value = dt.Rows.Count.ToString()
                                                Case "MEDIAN"
                                                	Dim m As Integer = 0
	                                            	Dim medianPush As Boolean = False
	                                            	ReDim medianArray(0)
	                                            	For Each dtRow As DataRow In dt.Rows
	                                            	 	If Not dtRow("CC_deleted") Then
	                                            	 		ReDim Preserve medianArray(m)
			                                            	medianArray(m) = IIf(IsDBNull(dtRow(colName)), -9999, CDbl(dtRow(colName)))
			                                            	medianPush = True
			                                            	m += 1
	                                            	 	End If
	                                            	Next
	                                            	Array.Sort(medianArray)
	                                            	Dim mLength As Integer = medianArray.Length()
	                                            	If medianPush Then
	                                            		If mLength Mod 2 = 0 Then
		                                            		value = ( medianArray((mLength / 2) - 1) + medianArray(((mLength + 2) / 2) - 1) ) / 2
		                                            	Else
		                                            		value = medianArray(((mLength + 1) / 2) - 1)
		                                            	End If
		                                            	If value = -9999 Then
		                                            		value = Nothing
		                                            	End If
	                                            	End If
                                            		'value = Database.Tenant.GetStringValue("SELECT AVG(1 * " + colName + ") FROM ( SELECT " + colName + ", c  = COUNT(*) OVER (), rn = ROW_NUMBER() OVER (ORDER BY " + colName + ") FROM XT_" + tableName + " WHERE CC_ParcelId = " + parcelId.ToString() + " AND CC_deleted <> 1 " + IIf(Condition <> "", "And " + Condition +" ", "") +" ) AS x WHERE rn IN ((c + 1)/2, (c + 2)/2) ")
                                                Case "MAX", "MIN", "SUM", "AVG"
                                                    value = IIf(Not IsDBNull(dt.Compute(functName + "([" + colName + "])", Condition)), dt.Compute(functName + "([" + colName + "])", Condition).ToString(), "")
                                            End Select
                                        Catch ex As Exception

                                        End Try
                                    End If
                                    Exit For
                                End If
                            Next
                        Else
                            value = IIf(Not IsDBNull(parcelData.Rows(0)(colName)), IIf(functName = "COUNT", 1, parcelData.Rows(0)(colName).ToString()), "")
                        End If

                        Dim col As DataColumn = New DataColumn(fieldName)
                        parcelData.Columns.Add(col)
                        parcelData.Rows(0)(fieldName) = value
                    Next
                End If

            End If
            
            Dim jstr As String = JSON.GetRowAsJSON(parcelData.Rows(0)).TrimEnd("}")
            jstr += ", ""Positiondata"": " + JSON.GetTableAsJSON(PositiondataTable)
            jstr += ",""_mapPoints"":" + JSON.GetTableAsJSON(mapPoints)
            jstr += ",""Flags"":" + JSON.GetTableAsJSON(flags)
            jstr += ",""ParcelChanges"":" + JSON.GetTableAsJSON(parcelChanges)
            jstr += ",""sketchSettings"":" + JSON.GetTableAsJSON(sketchSettings)
            jstr += ",""sketchstatusflags"":" + JSON.GetTableAsJSON(sketchstatusflags)
            jstr += ",""sketchconfig"":""" + settings + """"
            jstr += ",""dataExist"":" + IIf(DataExist, "true", "false")
            jstr += ",""FirstPhoto"":""" + firstPhoto + """"
            ''jstr += ",""ParcelAlert"":""" + ParcelAlert + """"
            jstr += ",""alternateField"":""" + alternateField + """"
            If alternateFieldvalue IsNot Nothing AndAlso alternateFieldvalue <> "" Then
                jstr += ",""alternateFieldvalue"":""" + alternateFieldvalue + """"
            End If
            jstr += tempjstr

            For Each mt As String In missingTables
                jstr += ",""" + mt + """: []"
            Next

            jstr += "}"
            JSON.WriteString(jstr)
        Else
            JSON.Error("Parcel not found")
        End If
    End Sub
    Public Function GetRelatedTables(ByVal db As Database) As List(Of String)
        Dim tables As New Dictionary(Of String, Integer)
        _getConnectedTables(db, tables, db.Application.ParcelTable)
        Return tables.Where(Function(x) x.Value = 1 Or x.Value = 2).Select(Function(x) x.Key).ToList
    End Function

    Private Sub _getConnectedTables(ByVal db As Database, ByVal tables As Dictionary(Of String, Integer), ByVal tableName As String)
        For Each connectTableName In db.GetDataTable("SELECT t.Name, r.Relationship FROM DataSourceRelationships r INNER JOIN DataSourceTable t ON r.TableId = t.Id INNER JOIN DataSourceTable p ON r.ParentTableId = p.Id WHERE p.Name = '{0}' ORDER BY Relationship".FormatString(tableName)).Rows.OfType(Of DataRow).Select(Function(x) New With {.TableName = x.GetString("Name"), .Relationship = x.GetInteger("Relationship")}).ToArray
            If Not tables.ContainsKey(connectTableName.TableName) Then
                tables.Add(connectTableName.TableName, connectTableName.Relationship)
            End If
            _getConnectedTables(db, tables, connectTableName.TableName)
        Next
    End Sub

    Public Sub GetParcelData(dr As DataRow)
        Dim pid As String = dr.GetString("Id")
        Dim kv As String = dr.GetString("KeyValue1")
        Dim map As DataTable = Database.Tenant.GetDataTable("SELECT Latitude, Longitude, Ordinal FROM ParcelMapPoints WHERE ParcelId = " + pid + " ORDER BY Ordinal")

        Dim resp As Object
        resp = New With {
         .ID = pid,
         .ParcelID = kv,
         .StreetAddress = dr.GetString("StreetAddress"),
         .KeyValue1 = dr.GetString("KeyValue1"),
         .KeyValue2 = dr.GetString("KeyValue2"),
         .KeyValue3 = dr.GetString("KeyValue3"),
         .SketchUrl = "http://camacloud.appspot.com/parcelsketch/?assrno=" + dr.GetString("KeyValue1") + "&subpar=" + dr.GetString("KeyValue2"),
         .Flags = Database.Tenant.GetDataTable("SELECT sf.Id, CAST(CASE WHEN pf.Id IS NULL THEN 0 ELSE 1 END AS BIT) As Value FROM SketchReviewFlags sf LEFT OUTER JOIN ParcelSketchFlag pf ON pf.FlagId = sf.Id AND pf.ParcelId = " + pid),
         .Latitude = 0.0F,
         .Longitude = 0.0F,
         .ReviewedBy = dr.GetString("SketchReviewedBy"),
         .ReviewedDate = dr.GetString("SketchReviewedDate"),
         .ColorCode = dr.GetString("ColorCode"),
         .SketchZoom = dr.GetSingleWithDefault("SketchZoom", 1.0),
         .SketchRotate = dr.GetIntegerWithDefault("SketchRotation", 0),
         .MapZoom = dr.GetIntegerWithDefault("MapZoom", 0),
         .SketchReviewNote = dr.GetString("SketchReviewNote"),
         .SketchFlag = dr.GetInteger("SketchFlag"),
        ._mapPoints = Database.Tenant.GetDataTable("SELECT Latitude, Longitude, Ordinal, RecordNumber FROM ParcelMapPoints WHERE ParcelId = " + pid + " ORDER BY Ordinal"),
         .OutBuildings = DirectCast(Nothing, DataTable)
        }

        If Database.Tenant.DoesTableExists("XT_REDBA_OUTBUILD") Then
            Dim qoby = <sql>SELECT 
	l1.NameValue As OBUSE,
	l2.DescValue As OBCOND,
	l3.NameValue As OBWALL,
	OBYEAR,
	REPLACE(CONVERT(varchar, CAST(OBAREA AS money), 1), '.00', '') AS OBAREA
FROM XT_REDBA_OUTBUILD xt
LEFT OUTER JOIN ParcelDataLookup l1 ON (xt.OBUSE = l1.IdValue AND l1.LookupName = 'REDBA_OBUSE_LV')
LEFT OUTER JOIN ParcelDataLookup l2 ON (xt.OBCOND = l2.IdValue AND l2.LookupName = 'REDBA_OBCOND_LV')
LEFT OUTER JOIN ParcelDataLookup l3 ON (xt.OBWALL = l3.IdValue AND l3.LookupName = 'REDBA_OBWALL_LV')
WHERE CC_ParcelId = {0}</sql>
            resp.OutBuildings = Database.Tenant.GetDataTable(qoby.Value.FormatString(pid))
        Else
            resp.OutBuildings = Database.Tenant.GetDataTable("SELECT 'SHEDUTIL' As OBUSE,	'Average' As OBCOND, 'WOODVIN' As OBWALL, 1970 AS OBYEAR, 80 AS OBAREA")
        End If
        'If PositiondataTable.Rows.Count < 1 Then
        '	resp = New With {.dataExist = False, .CCId = pid, .ParcelId = kv, .SketchVector = data("Sketch").ToString(), .Points = map, .Latitude = 0.0F, .Longitude = 0.0F}
        'Else
        '	Dim Positiondata As DataRow = PositiondataTable.Rows(0)
        '	resp = New With {.dataExist = True, .CCId = pid, .ParcelId = kv, .SketchVector = data.GetString("Sketch"), .Points = map, .Latitude = 0.0F, .Longitude = 0.0F, .degree = Positiondata.GetString("Degree"), .sketchZoom = Positiondata.GetString("SketchZoom"), .mapZoom = Positiondata.GetString("MapZoom"), .lat = Positiondata.GetString("CenterPointLat"), .lng = Positiondata.GetString("CenterPointLng")}
        'End If

        JSON.Write(resp)
    End Sub
End Class

Public Class ParcelMultiPolygon
    Public Property parcelId As Integer
    Public Property keyValue1 As String
    Public Property streetAddress As String

    Public Property polygonArray As PolygonSketch()

    Public Class PolygonSketch
        Public Property selectedSketch As String
        Public Property vectors As PolygonVector()

    End Class

    'Public Class SketchPolygon
    '    Public Property polyPoints As PolygonVector()
    'End Class

    Public Class PolygonVector
        Public Property label As String
        Public Property points As String

        Public Function ToLatLngPoints() As List(Of LatLng)
            Dim points As New List(Of LatLng)
            For Each line In Me.points.Split(vbLf)
                line = line.Trim
                If line <> "" Then
                    Dim lp = line.Split(",")
                    Dim ll As New LatLng With {.Latitude = lp(1), .Longitude = lp(0)}
                    points.Add(ll)
                End If
            Next
            Return points
        End Function

        Public Function ToLatLngString() As String
            Dim lls As New List(Of String)
            For Each ll In ToLatLngPoints()
                lls.Add(String.Format("{0},{1}", ll.Latitude, ll.Longitude))
            Next
            Return String.Join(";", lls.ToArray)
        End Function
    End Class

    Public Class LatLng
        Public Property Latitude As String
        Public Property Longitude As String
    End Class
End Class

Public Class VbSketchConfig
	Public Shared _sketchConfigTables = New Dictionary(Of String, String()) From {
		{ "PACS80", { "imprv", "imprv_detail", "imprv_sketch_note" } },
    	{ "RapidSketch2", { "imprv", "ccv_property_val_profile", "imprv_detail", "imprv_sketch_note" } },
    	{ "PACS802", { "imprv", "imprv_detail", "imprv_sketch_note" } },
    	{ "RapidSketch", { "imprv", "imprv_detail", "imprv_sketch_note" } },
    	{ "RapidSketch_TA", { "imprv", "imprv_detail", "imprv_sketch_note" } },
    	{ "RapidSketch2_NFYL", { "imprv", "imprv_detail", "imprv_sketch_note" } },
    	{ "WinGap", { "REPROP", "SKETCH_RES", "COMMIMP", "SKETCH_COMM", "CCV_MOBILE", "SKETCH_MH", "CCV_MOBILE_PREBILL", "SKETCH_MH_PREBILL" } },
    	{ "Sigma", { "residence" } }, { "Stark", { "BUILDING" } }, { "FTC", { "IMPAPEXOBJECT" } }, { "Apex", { "Building_Sketch" } },
    	{ "Bristol", { "CCV_BLDG_CONSTR" } }, { "Vision6_5", { "CCV_BLDG_CONSTR" } }, { "Hennepin", { "CCV_BLDG_CONSTR" } },
    	{ "Vision7_5XML", { "CCV_BLDG_CONSTR" } }, { "Vision7_5XMLNew", { "CCV_BLDG_CONSTR" } }, 
    	{ "Vision8XML", { "CCV_BLDG_CONSTR" } }, { "Vision8XMLNew", { "CCV_BLDG_CONSTR" } }, { "ApexCart", { "CAMBL" } },
    	{ "CustomApex", { "prs_residential" } }, { "Baldwin", { "CCV_BLDG_MASTER" } }, { "Delta", { "CCV_BLDG_MASTER" } }, { "StLouis", {} },
    	{ "LexurSVG", { "BuildingSection", "Building_SketchNotes", "Dwelling", "Dwelling_SketchNotes" } },
    	{ "LexurSVGWilliams", { "Building", "Building_SketchNotes", "Dwelling", "Dwelling_SketchNotes" } },
    	{ "WarrenOH", { "MAF42", "CCV_56SKCM", "MAF43", "CCV_56SKRS" } },
    	{ "Fulton", { "DWELDAT", "ADDN", "COMDAT" ,"COMINTEXT" } },
    	{ "LucasDTR", { "DWELDAT", "ADDN", "COMDAT", "COMINTEXT", "COMFEAT", "OBY" } },
    	{ "ClarkKY", { "tblResidential", "tblCommercial", "tblMobileHome" ,"tblFarm" } },
    	{ "Brillion", { "CommercialBuildings", "MobileHomes", "ResidentialBuildings" ,"Sketches" } }, { "PnA", { "bldg" } },
    	{ "ProValNew", { "ccv_extensions", "sktsegment", "SktOutbuilding" ,"SktNote", "ccv_sktvector_outbuilding", "SktSegmentLabel", "SktVector", "ccv_improvements_dwellings", "ccv_improvements_comm_bldgs", "ccv_improvements" ,"SktHeader" } },
    	{ "ProValNewFYL", { "ccv_extensions", "ccv_SktSegment", "ccv_SktOutbuilding" ,"ccv_SktNote", "ccv_SktVector_outbuilding", "ccv_SktSegmentLabel", "ccv_SktVector", "ccv_improvements_dwellings", "ccv_improvements_comm_bldgs", "ccv_improvements" ,"ccv_SktHeader" } },
    	{ "CustomCAMA", { "ccv_bld_mstr_bld" } }, { "Lafayette", { "ccv_bld_mstr_bld" } }, { "Patriot", { "CCV_Sketch" } },
    	{ "LucasSV", { "ADDN_SV", "COMINTEXTFEAT_SV" } }, { "CustomCAMATraverse", { "ccv_bld_mstr_bld" } },
    	{ "WinGapRS", { "REPROP", "SKETCH_RES", "COMMIMP", "SKETCH_COMM", "CCV_MOBILE", "SKETCH_MH" , "CCV_MOBILE_PREBILL", "SKETCH_MH_PREBILL" } },
    	{ "MFCD", { "CAM_DWEL", "CAM_SKETCH", "CAM_OUTB", "CAM_OUTSK" } }, { "MVP", { "CC_Card" } }, 
    	{ "ApexJson", { "ccv_bld_mstr_bld" } }, { "ApexJsonARP", {} },{ "MA_Lite", {} },
    	{ "HennepinJson", { "CCV_BLDG_CONSTR" } }, { "DeltaJson", { "CCV_BLDG_MASTER" } }, { "MCISJson", {} },
    	{ "ApexCartJson", { "CAMBL" } }, { "LafayetteJson", { "ccv_bld_mstr_bld" } }, { "CustomCAMAJson", { "ccv_bld_mstr_bld" } },
    	{ "PVD_Manage", { "Buildings" } }, { "ARPJson", { "ccv_pvadata_characteristics", "CCV_pvadata_OI" } }, { "FarragutJson", { "CCV_RES_BUILDING", "CCV_COM_SECTION_BUILDING" } },
    	{ "T2Json", {} }
	}
End Class

