﻿Public Class TrackingProcessor
	Inherits RequestProcessorBase

	Public Overrides Sub ProcessRequest()
		Select Case Path
			Case "historypoints"
				Dim loginId As String = Request("keys")
				Dim ts1 As String = Request("ts1")
				Dim ts2 As String = Request("ts2")
				Dim mac As Integer = Request("mac")

				Dim results = JSON.GetTableAsJSON(Database.Tenant, "EXEC tracking_GetHistory {0}, {1}, {2}".SqlFormatString(loginId, ts1, ts2))
				Dim parcels = JSON.GetTableAsJSON(Database.Tenant, "EXEC [tracking_GetParcelHistory] {0}, {1}, {2}, {3}".SqlFormatString(loginId, ts1, ts2, mac))


				Dim js As String = "{""Results"": " + results + ", ""Parcels"": " + parcels + "}"
				JSON.WriteString(js)
		End Select
	End Sub

End Class
