﻿Public Class DebugProcessor
    Inherits RequestProcessorBase

    Public Overrides Sub ProcessRequest()
        Select Case Path
            Case "remotedebug"
                Debug.Print(Request("message"))
                JSON.OK()
        End Select
    End Sub

End Class
