﻿Imports System.Net
Imports System.Web.Configuration
Imports System.Configuration
Imports System.IO
Imports CAMACloud.Routing
Imports System.Threading
Imports System.Threading.Tasks
Imports System.Data.SqlClient
Public Class AdminProcessor
    Inherits RequestProcessorBase
    Public Overrides Sub ProcessRequest()
        Try
            Select Case Path
                Case "getcounty"
                    Getcounty()
                Case "getadvancedsettings"
                    GetAdvancedSettings()
                Case "saveadvancedsettings"
                    SaveAdvancedSettings()
                Case "clearsettingsaudittrail"
                    ClearSettingsAuditTrail()
                Case "getvendordetails"
                    GetVendorDetails()
                Case "addcamasystem"
                    AddCamaSystem()
                Case "savecamasystem"
                	SaveCamaSystem()
                Case "oldbackup"
                	OldBackUp()
                Case "newbackup"
                    NewBackUp()
                Case "deletecamasystem"
                    DeleteCamaSystem()
            End Select
        Catch ex As Exception

        End Try
    End Sub
    Private Sub threadFunction(sql As Object)
    	Dim s As String = DirectCast(sql, String)
    	 DbFunction(s)
    End Sub
    
    Private Sub OldBackUp()
    	Dim SourceDBHost As String
    	Dim drSourceDBHost As DataRow
    	drSourceDBHost = Database.System.GetTopRow("SELECT * FROM [dbo].[Organization] where Id ={0}".SqlFormat(True,Request("SourDB")))
    	SourceDBHost = drSourceDBHost.GetString("DBHost")
    	Dim k As String = Request("opVariable")
    	If k = 1 Then
    		
    	Dim DestinationDBHost As String
    	Dim sourceDB As String
    	Dim destinationDB As String
    	
    	Dim serverName As String
    	
    	
    	Dim drDestinationDBHost As DataRow
    	Dim sql As String
    	
    	drSourceDBHost = Database.System.GetTopRow("SELECT * FROM [dbo].[Organization] where Id ={0}".SqlFormat(True,Request("SourDB")))
    	SourceDBHost = drSourceDBHost.GetString("DBHost")
    	sourceDB = drSourceDBHost.GetString("DBName")
    	serverName = Database.System.GetStringValue("SELECT NAME FROM DatabaseServer WHERE Host ='"& SourceDBHost & "'")
    	drDestinationDBHost = Database.System.GetTopRow("SELECT * FROM [dbo].[TestingEnvironments] where Id ={0}".SqlFormat(True,Request("DesDB")))
    	DestinationDBHost = drDestinationDBHost.GetString("DBHost")
    	destinationDB = drDestinationDBHost.GetString("DBName")
    	sql = "EXEC [Sp_SampleRestoration] @destinationDbId = {0},@sourceDbId = {1},@ZipFile = {2},@HostId = {3},@Request = {4},@OldBackUp = {5}".SqlFormat(True,Request("DesDB"),Request("SourDB"),Request("ZipFileName"),SourceDBHost,Request("Requested"),1)
		Dim newThread As New System.Threading.Thread(AddressOf threadFunction)
    		newThread.Start(sql )
    	End If
		
		Try
			Dim sq As String = String.Format("SELECT * FROM TestingEnvironments WHERE Id ={0}".SqlFormat(True,Request("DesDB")))
			Dim testingDb As DataRow = Database.System.GetTopRow(sq)
			Dim opstatus As String = testingDb.GetString("OperationStatus")
			Dim restorestatus As String = testingDb.GetString("RestoreStatus")
			Dim errorstatus As String = testingDb.GetString("ErrorStatus")
			If errorstatus <> "1" Then
	        If opstatus = "100" Then
	        	Dim dropQuery As String = String.Format("EXEC [" & SourceDBHost & "].[Master].[dbo].[sp_JobStatus] @DropTable = 1")
	        	Database.System.Execute(dropQuery)
	        	Database.System.Execute("update TestingEnvironments set OperationStatus = 0 where id ={0}".SqlFormatString(Request("DesDB")))
				JSON.OK("101,completed")
    			Else 
					Dim sqlstatus As String = String.Format("EXEC [" & SourceDBHost & "].[Master].[dbo].[sp_JobStatus] @restore = 1")
					Dim statusDr As DataRow = Database.System.GetTopRow(sqlstatus)
	                If statusDr IsNot Nothing Then
	                	Dim percent As String = statusDr.GetString("PercentComplete")
	                    If percent <> "100" Then
	                    	Database.System.Execute("update TestingEnvironments set upstatus ={0} where id ={1}".SqlFormatString(percent,Request("DesDB")))
	                    	JSON.OK(percent+",notcompleted")
	                    Else If percent = "100" Then 
	                    	JSON.OK(percent+",notcompleted")
	                    	End If
	                Else If restorestatus = 100  Then 
	                	JSON.OK("100,notcompleted")
	                Else 
	                	JSON.OK("0,notcompleted")
	                End If
    			End If
			Else If errorstatus = "1"
				Dim errorMessage As String = testingDb.GetString("ErrorMessage")
				Database.System.Execute("update TestingEnvironments set ErrorStatus = 0 where id ={0}".SqlFormatString(Request("DesDB")))
				JSON.OK("0,error,"+errorMessage)
			End If
	    	Catch ex As Exception
	            	 JSON.Error(ex.Message)
	            	 Exit Sub 
	            	  
	    	End Try
    End Sub
    Public Function DbFunction(sqlQuery As String)
    	Dim dt As New DataTable
    	Dim connectionString As String = ConfigurationManager.ConnectionStrings("ControlDB").ConnectionString
    	''"Server=10.0.0.100;Database=CCMAIN;User Id=sa;Password=Cama@2014;"
        'Dim connectionString As String = "Server=TN01VMW7005;Database=CCMAIN;User Id=sa;Password=123;"
        Dim con As New SqlConnection(connectionString)
        Dim cmd As New SqlCommand
        If con.State = ConnectionState.Closed Then
            con.Open()
        End If
        cmd = New SqlCommand(sqlQuery, con)
        cmd.CommandTimeout = 12000
        Try
            Dim c As Integer = cmd.ExecuteNonQuery
            Return c
        Catch ex As Exception
            Dim msg As String = "Error on executing command - [" & sqlQuery & "]. Error Message is - " & ex.Message
        Finally
            con.Close()
        End Try
    End Function
    Private Sub NewBackUp()
    	Dim SourceDBHost As String
    	Dim sourceDB As String
    	Dim serverName As String
    	Dim drSourceDBHost As DataRow
    	Dim sql As String
    	drSourceDBHost = Database.System.GetTopRow("SELECT * FROM [dbo].[Organization] where Id ={0}".SqlFormat(True,Request("SourDB")))
    	SourceDBHost = drSourceDBHost.GetString("DBHost")
    	sourceDB = drSourceDBHost.GetString("DBName")
    	Dim k As String = Request("opVariable")
    	If k = 1 Then
	    	serverName = Database.System.GetStringValue("SELECT NAME FROM DatabaseServer WHERE Host ='"& SourceDBHost & "'")
	    	sql = "EXEC [Sp_SampleRestoration] @destinationDbId = {0},@sourceDbId = {1},@HostId = {2},@NewBackUp = 1,@Request = {3}".SqlFormat(True,Request("DesDB"),Request("SourDB"),SourceDBHost,Request("RequestedBy"))
	    	Dim newThread As New System.Threading.Thread(AddressOf threadFunction)
	    	newThread.Start(sql )
    	End If
    	Try
    		Dim sq As String = String.Format("SELECT * FROM TestingEnvironments WHERE Id ={0}".SqlFormat(True,Request("DesDB")))
			Dim testingDb As DataRow = Database.System.GetTopRow(sq)
			Dim opstatus As String = testingDb.GetString("OperationStatus")
			Dim backupstatus As String = testingDb.GetString("BackupStatus")
			Dim restorestatus As String = testingDb.GetString("RestoreStatus")
			Dim errorstatus As String = testingDb.GetString("ErrorStatus")
			If errorstatus <> "1" Then
				If opstatus = "100" Then
					Dim dropQuery As String = String.Format("EXEC [" & SourceDBHost & "].[Master].[dbo].[sp_JobStatus] @DropTable = 1")
					Database.System.Execute(dropQuery)
					Database.System.Execute("update TestingEnvironments set OperationStatus = 0 where id ={0}".SqlFormatString(Request("DesDB")))
					JSON.OK("101,completed")
	        	Else If opstatus = "50" Then
		        	Dim sqlstatus As String = "EXEC [" & SourceDBHost & "].[Master].[dbo].[sp_JobStatus] @restore ={0}".SqlFormat(True,1)
	    	 		Dim statusDr As DataRow = Database.System.GetTopRow(sqlstatus)
		                If statusDr IsNot Nothing Then
		                	Dim percent As String = statusDr.GetString("PercentComplete")
		                    If percent <> "100" Then
		                    	JSON.OK(percent+",restoreotcompleted")
		                     Else If percent = "100" Then 
		                     	JSON.OK(percent+",restoreotcompleted")
		                     End If 
				   		Else
		                			JSON.OK("0,restoreotcompleted")
				   		End If
	        	Else
	        	
	    			Dim sqlstatus As String = "EXEC [" & SourceDBHost & "].[Master].[dbo].[sp_JobStatus] @backup ={0}".SqlFormat(True,1)
	    	 		Dim statusDr As DataRow = Database.System.GetTopRow(sqlstatus)
		                If statusDr IsNot Nothing Then
		                	Dim percent As String = statusDr.GetString("PercentComplete")
		                    If percent <> "100" Then
		                    	JSON.OK(percent+",notcompleted")
		                    Else If percent = "100" Then 
		                    	JSON.OK(percent+",notcompleted")
		                    End If 
				   		Else If backupstatus = 100
				   			JSON.OK("100,notcompleted")
				   		Else
		                	JSON.OK("0,notcompleted")
				   		End If
	        	End If
	        Else If errorstatus = "1"
				Dim errorMessage As String = testingDb.GetString("ErrorMessage")
				Database.System.Execute("update TestingEnvironments set ErrorStatus = 0 where id ={0}".SqlFormatString(Request("DesDB")))
				JSON.OK("0,error,"+errorMessage)
			End If
	    	Catch ex As Exception
	            	 JSON.Error(ex.Message)
	            	 Exit Sub
	        End Try
    	
    End Sub
    
   
    Private Sub Getcounty()
        JSON.SendTable(Database.System, "SELECT Name, Id from Organization WHERE Name like '" & Request("pre").ToString() & "%' Order by Name")
    End Sub
    Private Sub GetAdvancedSettings()
        Try
            Dim o As DataRow = Database.System.GetTopRow("SELECT o.Id,o.DBHost,o.DBName, os.EnableAdvancedMap,os.EnableXMLAuditTrail,os.PictometryInMA,os.DistoEnabled,os.PCITrackingEnabled,os.FYLEnabled,os.BPPEnabled, os.EnableInternalView, os.PRCReport FROM Organization o INNER JOIN OrganizationSettings os ON o.id=os.OrganizationId WHERE o.Id = " & Request("orgId").ToString())
            Dim id = o.GetString("Id")

            'Data from ClientSettings.
            Dim sql As String = String.Format("SELECT * FROM [{0}].[{1}].[dbo].ClientSettings", o.GetString("DBHost"), o.GetString("DBName"))
            Dim dtClient As DataTable = Database.System.GetDataTable(sql)

            Dim sqlQuery As String
            Dim jstr As String = "{"

            ''MaxPhotosPerParcel
            Dim MaxPhotoParcelCount As DataRow = (From column In dtClient.Rows Where column("Name") = "MaxPhotosPerParcel").FirstOrDefault()
            If IsNothing(MaxPhotoParcelCount) Then
                sqlQuery = String.Format("INSERT INTO [{0}].[{1}].[dbo].ClientSettings(Name,Value) VALUES('MaxPhotosPerParcel',0)", o.GetString("DBHost"), o.GetString("DBName"))
                Database.System.Execute(sqlQuery)
                dtClient = Database.System.GetDataTable(sql)
                MaxPhotoParcelCount = (From column In dtClient.Rows Where column("Name") = "MaxPhotosPerParcel").FirstOrDefault()
            End If

            jstr += """MaxPhotosPerParcel"":""" + IIf(Convert.ToInt32(MaxPhotoParcelCount("Value")).ToString("N0") = 0, "0", Convert.ToInt32(MaxPhotoParcelCount("Value")).ToString("N0")) + ""","

            ''EnableAdvancedMap
            Dim isUseOSM As DataRow = (From column In dtClient.Rows Where column("Name") = "UseOSM").FirstOrDefault()
            If (o.GetBoolean("EnableAdvancedMap")) Then
                If IsNothing(isUseOSM) Then
                    sqlQuery = String.Format("INSERT INTO [{0}].[{1}].[dbo].ClientSettings(Name,Value) VALUES('UseOSM',0)", o.GetString("DBHost"), o.GetString("DBName"))
                    Database.System.Execute(sqlQuery)
                End If
            End If
            jstr += """EnableAdvancedMap"":""" + IIf(o.GetBoolean("EnableAdvancedMap"), "true", "false") + ""","

            ''UseOSM
            jstr += """UseOSM"":""" + IIf(Not IsNothing(isUseOSM) AndAlso isUseOSM("Value").ToString() = "1", "true", "false") + ""","

            ''PictometryInMA
            Dim EnableEagleViewInMA As DataRow = (From column In dtClient.Rows Where column("Name") = "EnableEagleViewInMA").FirstOrDefault()
            If (o.GetBoolean("EnableAdvancedMap") AndAlso Not IsNothing(isUseOSM) AndAlso isUseOSM("Value").ToString() = "1" AndAlso o.GetBoolean("PictometryInMA")) Then 'added Not IsNothing(isUseOSM) to check isUseOsm exists CC_1659'
                If IsNothing(EnableEagleViewInMA) Then
                    sqlQuery = String.Format("INSERT INTO [{0}].[{1}].[dbo].ClientSettings(Name,Value) VALUES('EnableEagleViewInMA',0)", o.GetString("DBHost"), o.GetString("DBName"))
                    Database.System.Execute(sqlQuery)
                End If
            End If
            jstr += """EagleViewInMA"":""" + IIf(o.GetBoolean("PictometryInMA"), "true", "false") + ""","

            ''EnablePictometryInMA
            jstr += """EnableEagleViewInMA"":""" + IIf(Not IsNothing(EnableEagleViewInMA) AndAlso EnableEagleViewInMA("Value").ToString() = "1", "true", "false") + ""","

            ''DistoEnabled
            Dim isEnableDisto As DataRow = (From column In dtClient.Rows Where column("Name") = "EnableDisto").FirstOrDefault()
            If (o.GetBoolean("DistoEnabled")) Then
                If IsNothing(isEnableDisto) Then
                    sqlQuery = String.Format("INSERT INTO [{0}].[{1}].[dbo].ClientSettings(Name,Value) VALUES('EnableDisto',0)", o.GetString("DBHost"), o.GetString("DBName"))
                    Database.System.Execute(sqlQuery)
                End If
            End If
            jstr += """DistoEnabled"":""" + IIf(o.GetBoolean("DistoEnabled"), "true", "false") + ""","

            ''EnableDisto
            jstr += """EnableDisto"":""" + IIf(Not IsNothing(isEnableDisto) AndAlso isEnableDisto("Value").ToString() = "1", "true", "false") + ""","


            Dim isEnableFYL As DataRow = (From column In dtClient.Rows Where column("Name") = "FutureYearEnabled").FirstOrDefault()
            If (o.GetBoolean("FYLEnabled")) Then
                If IsNothing(isEnableFYL) Then
                    sqlQuery = String.Format("INSERT INTO [{0}].[{1}].[dbo].ClientSettings(Name,Value) VALUES('FutureYearEnabled',0)", o.GetString("DBHost"), o.GetString("DBName"))
                    Database.System.Execute(sqlQuery)
                End If
            End If
            jstr += """FYLEnabled"":""" + IIf(o.GetBoolean("FYLEnabled"), "true", "false") + ""","


            jstr += """FutureYearEnabled"":""" + IIf(Not IsNothing(isEnableFYL) AndAlso isEnableFYL("Value").ToString() = "1", "true", "false") + ""","

            ''bppEnabled
            jstr += """bppEnabled"":""" + IIf(o.GetBoolean("BPPEnabled"), "true", "false") + ""","

            ''EnableInternalView
            jstr += """EnableInternalView"":""" + IIf(o.GetBoolean("EnableInternalView"), "true", "false") + ""","

            jstr += """PRCReport"":""" + IIf(o.GetBoolean("PRCReport"), "true", "false") + ""","

            ''PCITrackingEnabled
            Dim isEnableFieldTracking As DataRow = (From column In dtClient.Rows Where column("Name") = "EnableFieldTracking").FirstOrDefault()
            If (o.GetBoolean("PCITrackingEnabled")) Then
                If IsNothing(isEnableFieldTracking) Then
                    sqlQuery = String.Format("INSERT INTO [{0}].[{1}].[dbo].ClientSettings(Name,Value) VALUES('EnableFieldTracking',0)", o.GetString("DBHost"), o.GetString("DBName"))
                    Database.System.Execute(sqlQuery)
                End If
            End If
            jstr += """PCITrackingEnabled"":""" + IIf(o.GetBoolean("PCITrackingEnabled"), "true", "false") + ""","

            ''EnableFieldTracking
            jstr += """EnableFieldTracking"":""" + IIf(Not IsNothing(isEnableFieldTracking) AndAlso isEnableFieldTracking("Value").ToString() = "1", "true", "false") + ""","

            ''vendor-cds Email
            Dim ApplicationName As String = IIf(id > 0 And id <= 8, String.Concat("org00", Convert.ToString(id)), String.Concat("org", Convert.ToString(id)))
            sql = String.Format("SELECT m.Email FROM aspnet_Membership m inner join aspnet_Applications a on m.ApplicationId=a.ApplicationId inner join aspnet_Users u on m.UserId=u.UserId where u.LoweredUserName='vendor-cds' and a.LoweredApplicationName={0}".SqlFormatString(ApplicationName))
            Dim vendor_cdsEmail As String = Database.System.GetStringValue(sql)
            jstr += """VendorCDSEmail"":""" + vendor_cdsEmail + ""","

            ''EnableXMLAuditTrail
            jstr += """EnableXMLAuditTrail"":""" + IIf(o.GetBoolean("EnableXMLAuditTrail"), "true", "false") + ""","

            jstr = jstr.Remove(jstr.LastIndexOf(","))
            jstr += "}"

            JSON.WriteString(jstr)
        Catch ex As Exception
            JSON.Error(ex.Message)
        End Try
    End Sub
    Private Sub SaveAdvancedSettings()
        Try
            Dim orgId As String = Request("orgId").ToString()
            Dim maxPhotosParcel As String = Request("maxPhotosParcel").ToString()
            Dim vendorCDSMail As String = Request("vendorCDSMail").ToString()
            Dim enableAdvancedMap As String = Request("enableAdvancedMap").ToString()
            Dim pictometryInMA As String = Request("PictometryInMA").ToString()
            Dim distoEnabled As String = Request("distoEnabled").ToString()
            Dim fylEnabled As String = Request("FYLEnabled").ToString()
            Dim bppEnabled = Request("bppEnabled")
            Dim pciTrackingEnabled As String = Request("pciTrackingEnabled").ToString()
            Dim enableXMLAuditTrail As String = Request("enableXMLAuditTrail").ToString()
            Dim cbInternalView As String = Request("cbInternalView").ToString()
            Dim PRCReport As String = Request("PRCReport").ToString()
            Dim sqlOrg As String = ""
            Dim connStr As String = Database.GetConnectionStringFromOrganization(orgId)
            Dim sql As String = "UPDATE ClientSettings SET Value=" & maxPhotosParcel & " WHERE Name='MaxPhotosPerParcel'; "

            Dim UserNm As String = Request("UserNm").ToString()
            Dim sqlAdvDesc As String = ""
            Dim photoCnt As String = "NULL"
            Dim VendorEmail As String = "NULL"
            Dim a As DataRow = Database.System.GetTopRow(" SELECT * FROM OrganizationSettings WHERE OrganizationId = " & orgId)
            Dim AdvenableAdvancedMap = If(a.GetBoolean("enableAdvancedMap") = True, "1", "0")
            Dim AdvpictometryInMA = If(a.GetBoolean("pictometryInMA") = True, "1", "0")
            Dim AdvdistoEnabled = If(a.GetBoolean("distoEnabled") = True, "1", "0")
            Dim AdvpciTrackingEnabled = If(a.GetBoolean("pciTrackingEnabled") = True, "1", "0")
            Dim AdvenableXMLAuditTrail = If(a.GetBoolean("enableXMLAuditTrail") = True, "1", "0")
            Dim AdvfylEnabled = If(a.GetBoolean("fylEnabled") = True, "1", "0")
            Dim AdvbppEnabled = If(a.GetBoolean("bppEnabled") = True, "1", "0")
            Dim AdvEnableInternalView = If(a.GetBoolean("EnableInternalView") = True, "1", "0")
            Dim AdvPRCReport = If(a.GetBoolean("PRCReport") = True, "1", "0")

            If (enableAdvancedMap = "1") Then
                sql += " IF Not EXISTS(SELECT * FROM ClientSettings WHERE Name='UseOSM') INSERT INTO ClientSettings(Name, Value) VALUES('UseOSM',0); "
            Else
                sql += " DELETE FROM ClientSettings WHERE Name in('UseOSM','EnableEagleViewInMA');"
                sqlOrg += "UPDATE OrganizationSettings SET PictometryInMA=0  WHERE OrganizationId= " & orgId & " ;"
            End If

            Dim o As DataRow = Database.System.GetTopRow("SELECT o.Id,o.DBHost,o.DBName FROM Organization o INNER JOIN OrganizationSettings os ON o.id=os.OrganizationId WHERE o.Id = " & Request("orgId").ToString())
            Dim s As String = String.Format("SELECT Value FROM [{0}].[{1}].[dbo].ClientSettings WHERE Name='UseOSM'", o.GetString("DBHost"), o.GetString("DBName"))
            Dim isUseOSM As String = Database.System.GetStringValue(s)

            If (isUseOSM = "" Or isUseOSM = "0") Then
                sql += " DELETE FROM ClientSettings WHERE Name in('EnableEagleViewInMA');"
                sqlOrg += "UPDATE OrganizationSettings SET PictometryInMA=0  WHERE OrganizationId= " & orgId & " ;"
            End If

            If (pictometryInMA = "1") Then
                sql += " IF NOT EXISTS(SELECT * FROM ClientSettings WHERE Name='EnableEagleViewInMA') INSERT INTO ClientSettings(Name, Value) VALUES('EnableEagleViewInMA',0) ; "
            Else
                sql += " DELETE FROM ClientSettings WHERE Name='EnableEagleViewInMA'; "
            End If

            If (distoEnabled = "1") Then
                sql += " IF NOT EXISTS(SELECT * FROM ClientSettings WHERE Name='EnableDisto') INSERT INTO ClientSettings(Name, Value) VALUES('EnableDisto',0) ; "
            Else
                sql += " DELETE FROM ClientSettings WHERE Name='EnableDisto'; "
            End If

            If (fylEnabled = "1") Then
                sql += " IF NOT EXISTS(SELECT * FROM ClientSettings WHERE Name='FutureYearEnabled') INSERT INTO ClientSettings(Name, Value) VALUES('FutureYearEnabled',1) ; "
            Else
                sql += " DELETE FROM ClientSettings WHERE Name ='FutureYearEnabled'; "
            End If

            If (fylEnabled = "1") Then
                sql += " IF NOT EXISTS(SELECT * FROM ClientSettings WHERE Name='CurrentYearValue') INSERT INTO ClientSettings(Name, Value) VALUES('CurrentYearValue',YEAR(GETDATE())) ; "
            Else
                sql += " DELETE FROM ClientSettings WHERE Name = 'CurrentYearValue'; "
            End If

            If (pciTrackingEnabled = "1") Then
                sql += " IF NOT EXISTS(SELECT * FROM ClientSettings WHERE Name='EnableFieldTracking') INSERT INTO ClientSettings(Name, Value) VALUES('EnableFieldTracking',1) ; "
            Else
                sql += " DELETE FROM ClientSettings WHERE Name='EnableFieldTracking'; "
            End If

            If (bppEnabled = "0") Then
                sql += " IF EXISTS(SELECT * FROM ClientSettings WHERE Name='EnablePersonalProperty') DELETE FROM ClientSettings WHERE Name = 'EnablePersonalProperty'; "
                sql += " IF EXISTS(select * from UserSettings where IsBPPAppraiser = 1) update UserSettings set IsBPPAppraiser = 0 where IsBPPAppraiser = 1; "
            End If

            Dim userName As String = "vendor-cds"
            sql += " UPDATE UserSettings SET Email='" & vendorCDSMail & "' WHERE loginid='" & userName & "'; "
            sql += " UPDATE Application SET BPPEnabled = '" & bppEnabled & "'; "
            Dim ApplicationName As String = IIf(orgId > 0 And orgId <= 8, String.Concat("org00", orgId), String.Concat("org", Convert.ToString(orgId)))
            sqlOrg += String.Format("UPDATE m SET m.Email ={0}, m.LoweredEmail={0} FROM aspnet_Membership m inner join aspnet_Applications a on m.ApplicationId=a.ApplicationId inner join aspnet_Users u on m.UserId=u.UserId where u.LoweredUserName='vendor-cds' and a.LoweredApplicationName={1}".SqlFormatString(vendorCDSMail, ApplicationName))
            sqlOrg += String.Format("UPDATE OrganizationSettings SET EnableAdvancedMap= " & enableAdvancedMap & ", PictometryInMA=" & pictometryInMA & " ,EnableXMLAuditTrail= " & enableXMLAuditTrail & ", distoEnabled=" & distoEnabled & " , FYLEnabled=" & fylEnabled & " , PCITrackingEnabled=" & pciTrackingEnabled & " , BPPEnabled=" & bppEnabled & ", EnableInternalView = " & cbInternalView & ", PRCReport = " & PRCReport & " WHERE OrganizationId= " & orgId)
            Database.System.Execute(sqlOrg)

            Dim org As DataRow = Database.System.GetTopRow("SELECT Name,County,State,City FROM Organization WHERE Id = " & orgId)
            Dim CountyNm As String = org.GetString("Name").ToSqlValue()
            Dim p As String = String.Format("SELECT Value FROM [{0}].[{1}].[dbo].ClientSettings WHERE Name='MaxPhotosPerParcel'", o.GetString("DBHost"), o.GetString("DBName"))
            photoCnt = Database.System.GetStringValue(p)
            Dim e As String = String.Format("SELECT Email FROM [{0}].[{1}].[dbo].UserSettings WHERE loginid ='" & userName & "'", o.GetString("DBHost"), o.GetString("DBName"))
            VendorEmail = Database.System.GetStringValue(e)

            If photoCnt <> maxPhotosParcel Then
                sqlAdvDesc += "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserNm & "'," & CountyNm & ", 'Max Photos/Parcel of Advanced Settings is updated from " & photoCnt & " to " & maxPhotosParcel & "' )"
            End If
            If vendorCDSMail <> VendorEmail Then
                sqlAdvDesc += "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserNm & "'," & CountyNm & ", 'Vendor-CDS Email of Advanced Settings is updated from " & VendorEmail & " to " & vendorCDSMail & "' )"
            End If

            If AdvenableAdvancedMap <> enableAdvancedMap Then
                sqlAdvDesc += "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserNm & "'," & CountyNm & ", 'Advanced Map of Advanced Settings is " & If((enableAdvancedMap = 1), "enabled", "disabled") & "' )"
            End If
            If AdvpictometryInMA <> pictometryInMA Then
                sqlAdvDesc += "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserNm & "'," & CountyNm & ", 'Pictometry In MA of Advanced Settings is " & If((pictometryInMA = 1), "enabled", "disabled") & "' )"
            End If
            If AdvdistoEnabled <> distoEnabled Then
                sqlAdvDesc += "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserNm & "'," & CountyNm & ", 'Disto of Advanced Settings is " & If((distoEnabled = 1), "enabled", "disabled") & "' )"
            End If
            If AdvpciTrackingEnabled <> pciTrackingEnabled Then
                sqlAdvDesc += "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserNm & "'," & CountyNm & ", 'PCI Tracking of Advanced Settings is " & If((pciTrackingEnabled = 1), "enabled", "disabled") & "' )"
            End If
            If AdvenableXMLAuditTrail <> enableXMLAuditTrail Then
                sqlAdvDesc += "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserNm & "'," & CountyNm & ", 'XML Audit Trail of Advanced Settings is " & If((enableXMLAuditTrail = 1), "enabled", "disabled") & "' )"
            End If
            If AdvfylEnabled <> fylEnabled Then
                sqlAdvDesc += "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserNm & "'," & CountyNm & ", 'FYL of Advanced Settings is " & If((fylEnabled = 1), "enabled", "disabled") & "' )"
            End If
            If AdvbppEnabled <> bppEnabled Then
                sqlAdvDesc += "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserNm & "'," & CountyNm & ", 'BPP of Advanced Settings is " & If((bppEnabled = 1), "enabled", "disabled") & "' )"
            End If

            If AdvEnableInternalView <> cbInternalView Then
                sqlAdvDesc += "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserNm & "'," & CountyNm & ", 'Internal View of Advanced Settings is " & If((cbInternalView = 1), "enabled", "disabled") & "' )"
            End If

            If AdvPRCReport <> PRCReport Then
                sqlAdvDesc += "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserNm & "'," & CountyNm & ", 'PRC Report View of Advanced Settings is " & If((PRCReport = 1), "enabled", "disabled") & "' )"
            End If

            Try
                Database.System.Execute(sqlAdvDesc)
                Dim conn As New SqlClient.SqlConnection(connStr)
                conn.Open()
                Dim cmd As New SqlClient.SqlCommand(sql, conn)
                cmd.ExecuteNonQuery()
                conn.Close()
            Catch sqlex As SqlClient.SqlException
            Catch ex As Exception
            End Try
            JSON.OK()
        Catch ex As Exception
            JSON.Error(ex.Message)
        End Try
    End Sub
    Private Sub ClearSettingsAuditTrail()
        Dim connStr As String = Database.GetConnectionStringFromOrganization(Request("orgId").ToString())
        Dim sql As String = "DELETE FROM SettingsAuditTrail"
        Try
            Dim conn As New SqlClient.SqlConnection(connStr)
            conn.Open()
            Dim cmd As New SqlClient.SqlCommand(sql, conn)
            cmd.ExecuteNonQuery()
            conn.Close()
            JSON.OK()
        Catch ex As Exception
            JSON.Error(ex.Message)
        End Try
    End Sub
    Private Sub GetVendorDetails()
        JSON.SendTable(Database.System, "SELECT Id,Name from Vendor Order by Id")
    End Sub
    Private Sub AddCamaSystem()
        Try
            Dim VendorId As String = Request("vendorId").ToString()
            Dim CamaSystem As String = Request("camaSystem").ToString()
            If Database.System.GetIntegerValue("SELECT COUNT(*) FROM CAMASystem WHERE  VendorId={0} AND Name={1} ".SqlFormatString(VendorId, CamaSystem.Trim())) > 0 Then
                JSON.OK("The CAMASystem name already exists.")
                Return
            Else
                Database.System.Execute("INSERT INTO CAMASystem (VendorId, Name) VALUES ({0}, {1}) SELECT @@IDENTITY".SqlFormatString(VendorId, CamaSystem.Trim()))
                JSON.OK()
            End If
        Catch ex As Exception
            JSON.Error(ex.Message)
        End Try
    End Sub


    Private Sub SaveCamaSystem()
        Try
            Dim CamaId As String = Request("CamaId").ToString()
            Dim CamaSystem As String = Request("camaSystem").ToString()
            If Database.System.GetIntegerValue("SELECT COUNT(*) FROM CAMASystem WHERE  Id={0} AND Name={1} ".SqlFormatString(CamaId, CamaSystem.Trim())) > 0 Then
                JSON.OK("The Cama System name already exists.")
                Return
            Else
                Database.System.Execute("UPDATE CAMASystem SET Name = {0} WHERE ID = {1}".SqlFormatString(CamaSystem.Trim(), CamaId))
                JSON.OK()
            End If
        Catch ex As Exception
            JSON.Error(ex.Message)
        End Try
    End Sub

    Private Sub DeleteCamaSystem()
        Try
            Dim CamaId As String = Request("CamaId").ToString()
            Database.System.Execute("DELETE CAMASystem WHERE ID = {0}".SqlFormatString(CamaId))
            JSON.OK()

        Catch ex As Exception
            JSON.Error(ex.Message)
        End Try

    End Sub

End Class
