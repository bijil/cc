﻿Public Class MRAProcessor     
	Inherits RequestProcessorBase
	
	Public Overrides Sub ProcessRequest()
		Dim residualsData As String 
			Select Case Path
				Case "graphicalrecords"
					Dim pageSize As Integer  = Request("page")
					Dim i As Integer
					Integer.TryParse(Request("Page"), i)
					JSON.SendTable(Database.Tenant,"SELECT  Id, keyvalue1,StreetAddress FROM Parcel ORDER BY Id OFFSET {0} ROWS FETCH NEXT 1000 ROWS only".SqlFormat(False, i*1000))
					
				Case "residualrecords"
					Dim fId,c,tempID As Integer
					Integer.TryParse(Request("fieldId"), fId)
					Integer.TryParse(Request("count"), c)
					Integer.TryParse(Request("tempID"), tempID)
					Dim dr As DataRow =Database .Tenant.GetTopRow("Select name,dbo.cc_findTargetTable(sourcetable) SourceTable From datasourceField Where id= " & fId)
					'Dim query = String.Format("SELECT TOP {0} X.{1} fieldValue ,M.Residual,M.parcelid from {2} X join MRA_ResidualOutput M  on X.CC_parcelid=M.Parcelid order by M.Parcelid",c,dr.GetString ("name"),dr.GetString ("SourceTable")
'					Dim ds As DataSet = Database.Tenant.GetDataSet("EXEC MRA_GraphviewRecords @FieldName={0},@SourceTable={1},@pCount={2},@tempID={3}".SqlFormatString (dr.GetString ("name"),dr.GetString ("SourceTable"),c,tempID) )
'					Dim residuals As DataTable = ds.Tables (0)
'					Dim maxvalue As DataTable = ds.Tables (1)
'					residualsData  = "{""ResidualsRecords"":"+ JSON.GetTableAsJSON( residuals) + ",""MaxValue"":" + JSON.GetTableAsJSON(maxvalue) + "}"
'					JSON.WriteString(residualsData)
					JSON.SendTable(Database.Tenant,"EXEC MRA_GraphviewRecords @FieldName={0},@SourceTable={1},@pCount={2},@tempID={3}".SqlFormatString (dr.GetString ("name"),dr.GetString ("SourceTable"),c,tempID) )
				Case "selectedrecords"
					Dim fId,c,tempID As Integer
					Dim StartX,StartY,EndX,EndY As Double
					Dim jobType As String  = Request("jobType")
					Double.TryParse(Request("startX"),StartX)
					Double.TryParse(Request("startY"), StartY)
					Double.TryParse(Request("endX"), EndX)
					Double.TryParse(Request("endY"),EndY)
					Integer.TryParse(Request("fieldId"), fId)
					Integer.TryParse(Request("count"), c)
					Integer.TryParse(Request("tempID"), tempID)
					Dim dr As DataRow =Database .Tenant.GetTopRow("Select name,dbo.cc_findTargetTable(sourcetable) SourceTable From datasourceField Where id= " & fId) 
					JSON.SendTable(Database.Tenant,"EXEC MRA_GraphviewRecords @FieldName={0},@SourceTable={1},@pCount={2},@tempID={3},@StartX={4},@StartY={5},@EndX={6}, @EndY={7}, @jobType={8}".SqlFormatString (dr.GetString ("name"),dr.GetString ("SourceTable"),c,tempID,StartX,StartY,EndX,EndY,jobType) )				
			End Select
	
			End Sub
	
	
End Class
