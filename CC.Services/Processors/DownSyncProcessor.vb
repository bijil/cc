﻿Imports CAMACloud.Security

Public Class DownSyncProcessor
    Inherits RequestProcessorBase

    Public Overrides Sub ProcessRequest()

        If Request.IsLocal AndAlso Request("bypasslicense") = "true" Then
        Else
            If Not DeviceLicense.ValidateRequest(Request.Cookies, HttpContext.Current) Then
                JSON.Write(New With {.licenseFailed = True, .status = "Access Denied", .message = "Application has been denied access to CAMA Cloud due to invalid license."})
                Return
            End If
        End If

        Select Case Path
            Case "check"
                JSON.OK()
            Case "adecgtype"
                _getConfigType()

            Case "tablespec"
                JSON.SendTableAsStandardResponse(Database.Tenant, "SELECT Id, ImportType, Name, KeyField1, KeyField2, KeyField3, AuxiliaryKeyField1, AuxiliaryKeyField2 FROM DataSourceTable", False)
            Case "adeconfigs"
                JSON.SendTableAsStandardResponse(Database.Tenant, "SELECT * FROM ADE_Config", False)
            Case "adestages"
                JSON.SendTableAsStandardResponse(Database.Tenant, "SELECT * FROM ADE_Stage", False)
            Case "adesteps"
                _getADESteps()
            Case "adeallsteps"
                _getADEAllSteps()
            Case "querychanges"
                _queryChanges()
            Case "pulldata"
                _pullData()
            Case "test"
                Dim null As Boolean = False
                If Request("allowNull") = "true" Then
                    null = True
                End If
                JSON.SendTableAsStandardResponse(Database.Tenant, "SELECT TOP 1 * FROM Parcel", null)
        End Select
    End Sub

    Sub _queryChanges()
        Dim type As Integer = 0
        Dim startDate, endDate, lastDate As Date
        Integer.TryParse(Request("Type"), type)
        Date.TryParse(Request("StartDate"), startDate)
        Date.TryParse(Request("EndDate"), endDate)
        Date.TryParse(Request("LastDateTime"), lastDate)

        Dim config = New With {
                .Type = type,
                .StartDate = startDate,
                .EndDate = endDate,
                .LastDate = lastDate
            }
        Dim sql As String = "EXEC DSY_PrepareParcelQueue {0}, {1}, {2}, {3}".SqlFormat(True, type, startDate, endDate, lastDate)
		JSON.SendRowObjectAsStandardResponse(t_(sql), False)
    End Sub
    Sub _getConfigType()
        Dim configId As Integer = 0
        Integer.TryParse(Request("Id"), configId)
        Dim sql As String = "SELECT * FROM ADE_Config WHERE Id={0}".SqlFormatString(configId)
        JSON.SendRowObjectAsStandardResponse(t_(sql), False)
    End Sub

    Private Sub _pullData()
        Dim jobId, pageCount, pageSize, page As Integer
        Integer.TryParse(Request("JobId"), jobId)
        Integer.TryParse(Request("PageCount"), pageCount)
        Integer.TryParse(Request("PageSize"), pageSize)
        Integer.TryParse(Request("Page"), page)

        Dim sql As String = "EXEC DSY_PullData {0}, {1}, {2}, {3}".SqlFormat(False, jobId, pageCount, pageSize, page)
		JSON.SendTableAsStandardResponse(d_(sql), False)
    End Sub

    Private Sub _getADESteps()
        Dim configId, stageId As Integer
        Integer.TryParse(Request("ConfigId"), configId)
        Integer.TryParse(Request("StageId"), stageId)
		JSON.SendTableAsStandardResponse(Database.Tenant, "SELECT * FROM ADE_Step WHERE Enabled = 1 AND ConfigId = {0} AND StageId = {1} ORDER BY Ordinal".FormatString(configId, stageId), False)
	End Sub

	Private Sub _getADEAllSteps()
		Dim configId As Integer
		Integer.TryParse(Request("ConfigId"), configId)
		JSON.SendTableAsStandardResponse(Database.Tenant, "SELECT * FROM ADE_Step WHERE Enabled = 1 AND ConfigId = {0} ORDER BY Ordinal".FormatString(configId), False)
	End Sub

End Class
