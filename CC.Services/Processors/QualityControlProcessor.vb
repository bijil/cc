﻿Imports System.Net
Imports System.Web.Configuration
Imports System.Configuration
Imports System.IO
Imports Microsoft.VisualBasic
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates
Imports System.Web.Security
Imports System.Text.RegularExpressions
Imports System.Web.UI.WebControls
Imports System.Text

Public Class QualityControlProcessor
    Inherits RequestProcessorBase

    Public ReadOnly Property EnableNewPriorities As Boolean
        Get
            Dim EnableNewPriority = False
            Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM ClientSettings WHERE Name = 'EnableNewPriorities' AND Value = 1")
            If dr IsNot Nothing Then
                EnableNewPriority = True
            End If
            Return EnableNewPriority
        End Get
    End Property

    Public ReadOnly Property EnabledInternalView As Integer
        Get
            Dim EnableInternalView = 0
            Dim dr As DataRow = Database.System.GetTopRow("SELECT * FROM OrganizationSettings WHERE EnableInternalView = 1 AND OrganizationId = " + HttpContext.Current.GetCAMASession().OrganizationId.ToString())
            If dr IsNot Nothing Then
                EnableInternalView = 1
            End If
            Return EnableInternalView
        End Get
    End Property

    Private Function isDTRCall() As Boolean
        Return Request("__dtr") = "1"
    End Function
    Private Function isBPPParcel() As Boolean
        Dim childCount As Integer = Database.Tenant.GetIntegerValue("SELECT count(*) FROM Parcel Where ParentParcelID={0} ".SqlFormatString(Request("ParcelId")))
        If childCount > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Overrides Sub ProcessRequest()

        If Not Request.IsAuthenticated Then
            JSON.Write(New With {.LoginStatus = "401"})
            Return
        Else
            'Debug.Print("REQUEST IS AUTHENTICATED")
        End If

        Try
            Dim isSyncEvent As Boolean = False


            Select Case Path
                Case "lookupdatatables"
                    _getLookupDataTables()
                'Case "getparcelchanges"
                '    isSyncEvent = True
                '    _getParcelChangesPage()

                'Case "parcelimages"
                '    isSyncEvent = True
                '    _getImages()
                Case "tquery"
                    _downloadTableAsJSONFromRequest()
                Case "check"
                    JSON.OK()
                Case "parcel"
                    _getParcelDetails()
                Case "searchresultfields"
                    _getSearchResultFields()
                Case "setsearchresultview"
                    _setSearchResultView()
                Case "search"
                    _searchParcels()
                Case "getofflinetables"
                    _getOfflineTables()
                Case "deleteimage"
                    _deletePhoto()
                Case "recoverimage"
                    _recoverPhoto()
                Case "setasmainimage"
                    _setAsMainImage()
                Case "updateprimaryphoto"
                    _updatePrimaryPhoto()
                Case "saveparcelchanges"
                    _saveParcelChanges()
                Case "qcset"
                    _setQC()
                Case "parcelupdates"
                    _getParcelDetailUpdates()
                Case "bulkjob"
                    _performBulkAction()
                Case "savesearchquery"
                    _saveSearchQuery()
                Case "savecustomquery"
                    _saveCustomQuery()
                Case "getcustomquery"
                    _getCustomQueryData()
                Case "getsearchqueries"
                    _getSearchQueries()
                Case "deletesearchquery"
                    _deleteSearchQuery()
                Case "getdetailsonreject"
                    _getdetailsonreject()
                Case "savesketch"
                    _saveSketchChanges()
                Case "loadvalidations"
                    _loadValidations()
                Case "loaddatasourcefields"
                    _loadDatasourceFields()
                Case "softreject"
                    _doSoftReject()
                Case "fcload"
                    _loadFieldCategories()
                Case "clientsettingsload"
                    _loadClientSettings()
                'Case "deleteauxrecord"
                '    _deleteAuxRecord()
                Case "reviewparcel"
                    _reviewparcel()
                Case "modifygridcolumns"
                    _setgridcolumns()
                Case "qcvisualstats"
                    _qcVisualStats()
                Case "updateadhocpriority"
                    _updateAdhocPriority()
                Case "lastupdatedtime"
                    _lastUpdatedTime()
                Case "lastupdatedcounter"
                    _lastupdatedCounter()
                Case "qccolumnswap"
                    _qccolumnswap()
                Case "updateforcestatus"
                    _updateForceStatus()
                Case "commitchange"
                    _doCommitChanges()
                Case "uploadhtmlfile"
                    _uploadHtmlFile()
                Case "downloadhtmlfile"
                    _downloadHtmlFile()
                Case "userbppsettings"
                    Dim UserType As DataRow = Database.Tenant.GetTopRow("IF exists(SELECT * FROM Application where BPPEnabled =1) SELECT CASE IsBPPAppraiser WHEN  1 then 'BPP' ELSE 'RP' END as IsBPPAppraiser,BPPRealDataReadOnly,CASE  WHEN BPPDownloadType = 1 then '1' ELSE '0' END  as BPPDownloadType   from UserSettings where loginId = '" + UserName + "' ")
                    If (Not UserType Is Nothing) Then
                        JSON.Write(New With {.status = "OK", .username = UserName, .usertype = UserType.GetString("IsBPPAppraiser"), .RealDataReadOnly = UserType.GetBoolean("BPPRealDataReadOnly"), .BPPDownloadType = UserType.GetInteger("BPPDownloadType")})
                    End If
                Case Else
                    JSON.Error("Invalid Request Exception")

            End Select
        Catch ex As Exception
            JSON.Error(ex)
        End Try

    End Sub
    Private Sub _getLookupDataTables()
        JSON.StartTick()
        Dim objectCount As Integer = 0
        Dim items As New List(Of String)
        Dim lookupTableNames As New List(Of String)
        Dim tableIndex As String = Request("tableIndex").ToString()
        Dim pageIndex As String = Convert.ToInt32(Request("pageIndex").ToString())

        '        For Each tableName In DB.GetDataTable("SELECT t.Name FROM DataSourceTable t LEFT OUTER JOIN DataSourceRelationships r ON t.Id = r.TableId AND r.Relationship IS NOT NULL WHERE NOT (t.Name LIKE '`_%' ESCAPE '`') AND t.Name NOT IN (SELECT ParcelTable FROM Application UNION SELECT NeighborhoodTable FROM Application UNION SELECT '_photo_meta_data') GROUP BY t.Name HAVING COUNT(r.TableId) = 0").Rows.OfType(Of DataRow).Select(Function(x) x.GetString("Name")).ToArray
        '            lookupTableNames.Add(tableName)
        '        Next

        If pageIndex <> 0 Then
            pageIndex = pageIndex * 2000
        End If
        Dim tableName As String = DB.GetStringValue("SELECT Name FROM DataSourceTable WHERE ImportType = 5 ORDER BY Name OFFSET " + tableIndex + " ROWS FETCH NEXT 1 ROWS ONLY")

        If tableName = Nothing Then
            JSON.WriteString("{""status"": ""completed""}")
        Else
            Dim sortFormula As String = FieldCategory.GetSortExpression(tableName, "xt.")
            Dim sql As String = "SELECT '" + tableName + "' As DataSourceName, xt.* FROM LT_" + tableName + " xt ORDER BY DataSourceName OFFSET " + pageIndex + " ROWS FETCH NEXT 2000 ROWS ONLY"
            Dim dt As DataTable = Database.Tenant.GetDataTable(sql)
            MobileSyncDownloadProcessor.AddBlankRowIfNoRows(dt, "DataSourceName", tableName)
            JSON.SendTable(dt)
        End If

        '        For Each tableName In lookupTableNames
        '            Dim sqlTableName As String = "LT_" + tableName
        '            If Database.Tenant.DoesTableExists(sqlTableName) Then
        '                Dim tableData As String = JSON.GetTableAsJSON(MobileSyncDownloadProcessor.SyncLookupDataTable(UserName, Request("nbhd"), tableName))
        '                objectCount += JSON.ObjectsAffected
        '                If tableData <> "[]" Then
        '                    items.Add(tableData.TrimStart("[").TrimEnd("]"))
        '                End If
        '            End If
        '        Next
        '        Dim js As String = "[" + String.Join(", ", items.ToArray) + "]"
        '        JSON.WriteString(js)
        '        JSON.EndTick()
        '        JSON.SetObjectCount(objectCount)
    End Sub
    Private Sub _getParcelChangesPage()
        Dim dt As DataTable = MobileSyncDownloadProcessor.SyncParcelChanges(UserName, Request("nbhd"), Request("page"))
        JSON.SendTable(dt)
    End Sub
    Private Sub _getImages()
        Dim dt As DataTable = MobileSyncDownloadProcessor.SyncParcelImages(UserName, "'" + Request("nbhd") + "'", Request("page"))
        JSON.SendTable(dt)
    End Sub
    Private Sub _downloadTableAsJSONFromRequest()
        Dim tableName As String = Request("t")
        Select Case tableName
            Case "FieldCategory"
                JSON.SendTable(FieldCategory.GetFieldCategories)
            Case "CategorySettings"
                Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT * FROM FieldCategoryProperties")
                MobileSyncDownloadProcessor.AddBlankRowIfNoRows(dt)
                JSON.SendTable(dt)
            Case "Field"
                JSON.SendTable(FieldCategory.GetFields, allowReplace:=False)
            Case "FieldSettings"
                Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT * FROM DataSourceFieldProperties")
                MobileSyncDownloadProcessor.AddBlankRowIfNoRows(dt)
                JSON.SendTable(dt)
            Case "Neighborhood"
                JSON.SendTable(Database.Tenant, "SELECT * FROM Neighborhood n LEFT OUTER JOIN NeighborhoodData nd ON n.Id = nd.NbhdId  ")
            Case "LookupValue"
                JSON.SendTable(Database.Tenant, "SELECT  Id, LookupName AS Source, IdValue AS Value, NameValue AS Name, DescValue AS Description, Ordinal, ColorCode as Color, value as NumericValue, AdditionalValue1, AdditionalValue2 FROM dbo.ParcelDataLookup", allowReplace:=False)
            Case "ImportSettings", "ClientSettings"
                JSON.SendTable(Database.Tenant, "SELECT * FROM ClientSettings")
            Case "SketchSettings"
                Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT * FROM SketchSettings")
                MobileSyncDownloadProcessor.AddBlankRowIfNoRows(dt)
                JSON.SendTable(dt)
            Case "ParentChildTable"
                JSON.SendTable(DataSource.GetParentChildTable)
            Case "ClientValidation"
                Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT * FROM ClientValidation")
                MobileSyncDownloadProcessor.AddBlankRowIfNoRows(dt)
                JSON.SendTable(dt)
            Case "TableKeys"
                JSON.SendTable(Database.Tenant, "SELECT f.Name,t.Name as SourceTable FROM DataSourceField f INNER JOIN  DataSourceKeys k ON f.Id = k.FieldId INNER JOIN  DataSourceRelationships r ON k.RelationshipId = r.Id INNER JOIN DataSourceTable t ON r.TableId = t.Id WHERE isPrimaryKey=1 ")
            Case "FieldAlertTypes"
                JSON.SendTable(Database.Tenant, "SELECT * FROM FieldAlertTypes")
            Case "ParentChildTable"
                JSON.SendTable(DataSource.GetParentChildTable)
            Case "AggregateFieldSettings"
                JSON.SendTable(QCProcessor.AggregateFieldSettings())
        End Select
        ' _registerSyncEvent(Path, tableName, 0)
    End Sub
    Public Sub _reviewparcel()
        Dim editAction As DataTable = Database.Tenant.GetDataTable("SELECT * FROM DataSourceTableAction WHERE Action='E'")
        Dim _appType As String = Request("appType")
        Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT DBO.GETLOCALDATE(GETUTCDATE()) As LocalDate")
        If editAction.Rows.Count > 0 Then
            QCProcessor.EditTableAction(Database.Tenant, Request("ParcelId"), Request("Reviewed"), UserName, dr.GetDate("LocalDate"))
        End If
        QCProcessor.EditDescendentTableAction(Database.Tenant, Request("ParcelId"), Request("Reviewed"), UserName, dr.GetDate("LocalDate"))
        If (_appType = "DTR") Then
            Database.Tenant.Execute("EXEC qc_SetAsReviewed @parcelId={0},@loginId={1},@Reviewed={2}, @DTR = 1, @AppTyp = 'DTR'".SqlFormatString(Request("ParcelId"), UserName, Request("Reviewed")))
        ElseIf (_appType = "QC") Then
            Database.Tenant.Execute("EXEC qc_SetAsReviewed @parcelId={0},@loginId={1},@Reviewed={2},@DTR = 0,@AppTyp= 'QC'".SqlFormatString(Request("ParcelId"), UserName, Request("Reviewed")))
        Else
            Database.Tenant.Execute("EXEC qc_SetAsReviewed @parcelId={0},@loginId={1},@Reviewed={2},@AppTyp = Null".SqlFormatString(Request("ParcelId"), UserName, Request("Reviewed")))
        End If
        JSON.OK()
    End Sub

    Public Sub _doSoftReject()
        Dim parcelId = Request("ParcelId")
        If isDTRCall() Then
            Database.Tenant.Execute("EXEC qc_SoftReject @parcelId ={0},@UserName ={1},@DTR = 1".SqlFormatString(parcelId, UserName))
        Else
            Database.Tenant.Execute("EXEC qc_SoftReject @parcelId ={0},@UserName ={1}".SqlFormatString(parcelId, UserName))
        End If
        If ClientSettings.PropertyValue("SendMailonParcelRejection") = 1 Then
            Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT p.KeyValue1, us.LoginId, p.ParcelAlert FROM UserSettings us INNER JOIN Neighborhood nbh ON us.LoginId = nbh.AssignedTo INNER JOIN Parcel p ON nbh.Id = p.NeighborhoodId WHERE p.Id = {0}".SqlFormatString(parcelId))
            If (dt.Rows.Count > 0) Then
                Dim keyValue As String = dt.Rows(0)("KeyValue1").ToString()
                Dim loginId As String = dt.Rows(0)("LoginId").ToString()
                Dim parcelAlert As String = dt.Rows(0)("ParcelAlert").ToString()
                Dim subject As String = "Parcel Soft Rejected"
                Dim body As String
                If (parcelAlert Is Nothing Or parcelAlert = "") Then
                    body = keyValue + " has been Soft Rejected. Update Data & Assignment Groups to download the property again."
                Else
                    body = keyValue + " has been Soft Rejected with notes: " + parcelAlert + ". Update Data & Assignment Groups to download the property again."
                End If
                NotificationMailer.SendNotification(loginId, subject, body)
            End If
        End If
        JSON.OK()
    End Sub

    Public Sub _doCommitChanges()
        Dim ParcelID = Request("ParcelID")
        Database.Tenant.Execute("EXEC qc_CommitChangeParcel @parcelId ={0},@UserName ={1}".SqlFormatString(ParcelID, UserName))
        Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT p.KeyValue1, us.LoginId, p.ParcelAlert FROM UserSettings us INNER JOIN Neighborhood nbh ON us.LoginId = nbh.AssignedTo INNER JOIN Parcel p ON nbh.Id = p.NeighborhoodId WHERE p.Id = {0}".SqlFormatString(ParcelID))
        If (dt.Rows.Count > 0) Then
            Dim keyValue As String = dt.Rows(0)("KeyValue1").ToString()
            Dim loginId As String = dt.Rows(0)("LoginId").ToString()
            Dim parcelAlert As String = dt.Rows(0)("ParcelAlert").ToString()
            Dim subject As String = "Parcel Commited"
            Dim body As String
            If (parcelAlert Is Nothing Or parcelAlert = "") Then
                body = keyValue + " has been Commited. Update Data & Assignment Groups to download the property again."
            Else
                body = keyValue + " has been Commited with notes: " + parcelAlert + ". Update Data & Assignment Groups to download the property again."
            End If
            NotificationMailer.SendNotification(loginId, subject, body)
        End If
        JSON.OK()
    End Sub

    Public Sub _uploadHtmlFile()
        Dim htmlContent = Request("htmlContent")
        Dim pid As String = Request("parcelId")
        Dim Path As String = "PrcBackup/" + HttpContext.Current.GetCAMASession.TenantKey + "/" + pid.ToString
        Try
            Dim insertSql = "INSERT INTO PrcBackup (LoginId, ParcelId, UploadTime) VALUES({0}, {1}, GETDATE()); SELECT CAST(@@IDENTITY AS INT);".SqlFormat(True, UserName, pid)
            Dim newId = DB.GetIntegerValue(insertSql)
            Path = Path + "/" + newId.ToString() + ".html"
            Dim s3 As New S3FileManager()
            s3.UploadHtmlFile(htmlContent, Path)
            DB.Execute("UPDATE PrcBackup SET Path='" + Path + "' WHERE Id=" + newId.ToString())
            Dim dt As DataTable = DB.GetDataTable("SELECT Id,Path FROM PrcBackup WHERE ParcelId=" + pid + " ORDER BY Id DESC")
            If dt.Rows.Count > 20 Then
                Dim dsql As String = "DELETE FROM PrcBackup WHERE Id IN ("
                For i As Integer = 20 To dt.Rows.Count - 1
                    Dim row As DataRow = dt.Rows(i)
                    s3.DeleteFile(row.GetString("Path"))
                    dsql += row.GetString("Id") + ","
                Next
                dsql = dsql.Substring(0, dsql.Length - 1)
                dsql += ")"
                DB.Execute(dsql)
            End If
            JSON.OK()
        Catch ex As Exception
            JSON.Error(ex)
        End Try
    End Sub

    Public Sub _downloadHtmlFile()
        Dim r = System.Web.HttpContext.Current.Response
        Dim PrcId = Request("prcId")
        Try
            Dim Path As String = DB.GetStringValue("SELECT Path FROM PrcBackup WHERE Id=" + PrcId.ToString())
            Dim s3 As New S3FileManager()
            Dim html As String = s3.DownloadHtmlFile(Path, "PrcHtml", r)
            r.ContentType = "text/html"
            r.Write(html)
        Catch ex As Exception
            r.Write(ex.Message)
        End Try
    End Sub

    Private Sub _getParcelDetails()
        Dim sParcelId As String = Request("ParcelId")
        Dim parcelId As Integer
        Dim query As String = ""
        Dim dtr As Integer = 0
        Dim tableNo As Integer = 7
        If isDTRCall() Then
            dtr = 1
        End If
        If Integer.TryParse(sParcelId, parcelId) Then
            query = "EXEC qc_GetParcelData @ParcelId ={0},@dtr ={1}, @InternalView = {2}".FormatString(parcelId, dtr, EnabledInternalView)
            Dim pds As DataSet = Database.Tenant.GetDataSet(query)

            Dim parcelData As DataTable = pds.Tables(0)
            Dim images As DataTable = pds.Tables(1)
            Dim mapPoints As DataTable = pds.Tables(2)
            Dim sketch As DataTable = pds.Tables(3)
            Dim reviewLog As DataTable = pds.Tables(4)
            Dim auditTrail As DataTable = pds.Tables(5)
            Dim changes As DataTable = pds.Tables(6)
            Dim settings As DataTable = pds.Tables(7)
            Dim WorkFlowTypes As DataTable = New DataTable()
            If ClientSettings.PropertyValue("DTRWorkflowNew") = "1" AndAlso dtr = 1 Then
                WorkFlowTypes = Database.Tenant.GetDataTable("SELECT p.StatusCode as code, CONCAT(p.StatusCode, ' - ', t.Description) As WorkFlowType FROM ParcelWorkFlowStatus p JOIN ParcelWorkFlowStatusType t ON p.StatusCode = t.Code WHERE p.parcelId = " + parcelId.ToString())
            End If
            Dim AdjacentParcels As DataTable
            Dim BPPChildParcels As DataTable
            If isDTRCall() Then
                tableNo = tableNo + 1
                AdjacentParcels = pds.Tables(tableNo)
            End If
            tableNo = tableNo + 1
            BPPChildParcels = pds.Tables(tableNo)
            tableNo = tableNo + 1
            Dim gisPoints As DataTable = pds.Tables(tableNo)
            '' Dim parcelData As DataTable = pds.Tables(0)
            ''Dim parcelData As DataTable = Database.Tenant.GetDataTable("SELECT p.*, dbo.GetLocalDate(p.QCDate) as QCDateNew,pd.* FROM Parcel p LEFT OUTER JOIN ParcelData pd ON p.Id = pd.CC_ParcelId LEFT OUTER JOIN Neighborhood n ON p.NeighborhoodId = n.Id  LEFT OUTER JOIN NeighborhoodData nd ON nd.NbhdId = n.Id  WHERE p.Id = {0}".SqlFormatString(parcelId))
            'Dim parcelData As DataTable = Database.Tenant.GetDataTable("SELECT p.*, dbo.GetLocalDate(p.QCDate) as QCDateNew,pd.*,fal.Name as FieldAlertName,sat.Description as SelectedAppraisalType_New  FROM Parcel p LEFT OUTER JOIN ParcelData pd ON p.Id = pd.CC_ParcelId LEFT OUTER JOIN Neighborhood n ON p.NeighborhoodId = n.Id  LEFT OUTER JOIN NeighborhoodData nd ON nd.NbhdId = n.Id LEFT OUTER JOIN FieldAlertTypes fal ON fal.Id=p.FieldAlertType LEFT OUTER JOIN AppraisalType sat ON sat.Id=p.SelectedAppraisalType  WHERE p.Id = {0}".SqlFormatString(parcelId))
            ''Dim images As DataTable = pds.Tables(1)
            'Dim images As DataTable = Database.Tenant.GetDataTable("SELECT *, '/imgsvr/' + Path As ImagePath FROM ParcelImages WHERE ParcelId = {0}".SqlFormatString(parcelId))
            ''Dim mapPoints As DataTable = pds.Tables(2)
            'Dim mapPoints As DataTable = Database.Tenant.GetDataTable("SELECT * FROM ParcelMapPoints WHERE ParcelId = {0}".SqlFormatString(parcelId))
            '' Dim sketch As DataTable = pds.Tables(3)
            'Dim sketch As DataTable = Database.Tenant.GetDataTable(" SELECT * FROM ParcelSketch WHERE ParcelId ={0}".SqlFormatString(parcelId))
            '' Dim reviewLog As DataTable = pds.Tables(4)
            'Dim reviewLog As DataTable = Database.Tenant.GetDataTable("SELECT * FROM ParcelReviewLog WHERE ParcelId = {0}".SqlFormatString(parcelId))
            ''Dim auditTrail As DataTable = pds.Tables(5)
            'Dim auditTrail As DataTable = Database.Tenant.GetDataTable("SELECT p.*,dbo. GetLocalDate(p.EventTime) as EventTimeNew FROM ParcelAuditTrail p WHERE ParcelId= {0} ORDER BY EventTime".SqlFormatString(parcelId))
            '' Dim changes As DataTable = pds.Tables(6)
            'Dim changes As DataTable = Database.Tenant.GetDataTable("  SELECT  pc.*, fc.Id As CategoryId, f.Name As FieldName, f.SourceTable,dbo.GetLocalDate(pc.ChangedTime) as ChangedTimeNew  FROM  ParcelChanges pc  LEFT OUTER JOIN DataSourceField f ON pc.FieldId = f.Id  LEFT OUTER JOIN FieldCategory fc ON f.CategoryId = fc.Id  WHERE ParcelId = {0} ORDER BY ChangedTime".SqlFormatString(parcelId))
            '' Dim Settings As DataTable = pds.Tables(7)
            'Dim Settings As DataTable = Database.Tenant.GetDataTable(" select * from ClientSettings")

            If parcelData.Rows.Count = 0 Then
                JSON.Error("Parcel not found")
                Return
            End If

            Dim qc As Boolean = parcelData.Rows(0).GetBoolean("QC")

            Dim downSyncFailed As Boolean = parcelData.Rows(0).GetBoolean("FailedOnDownSync")

            Dim relatedTableNames As New List(Of String)
            If Database.Tenant.Application.IsAPISyncModel Then
                Dim primaryTable As String = Database.Tenant.Application.ParcelTable
                relatedTableNames = GetRelatedTables(Database.Tenant)
            Else
                For Each dr As DataRow In Database.Tenant.GetDataTable("SELECT Name FROM DataSourceTable WHERE ImportType = 1").Rows
                    relatedTableNames.Add(dr.GetString("Name"))
                Next
            End If

            Dim tables As New List(Of String)
            Dim missingTables As New List(Of String)
            Dim auxFetchSql As String = ""
            For Each tableName In relatedTableNames
                Dim sqlTableName As String = "XT_" + tableName
                If Database.Tenant.DoesTableExists(sqlTableName) Then
                    Dim sortFormula = FieldCategory.GetSortExpression(tableName)
                    If (sortFormula = "") Then
                        sortFormula = " ORDER BY ROWUID ASC "
                    End If
                    'Dim tableData As String = JSON.GetTableAsJSON(Database.Tenant, "SELECT * FROM " + sqlTableName + " WHERE (CC_Deleted = 0 OR CC_Deleted IS NULL) AND CC_ParcelId = " & parcelId)
                    Dim auxSql As String = "SELECT * FROM " + sqlTableName + " WHERE " + IIf(qc And Not downSyncFailed And Not isDTRCall(), "(CC_Deleted = 0 OR CC_Deleted IS NULL) AND", "") + " CC_ParcelId = " & parcelId & "" & sortFormula + ";"
                    auxFetchSql += auxSql
                    'Dim tableData As String = JSON.GetTableAsJSON(Database.Tenant, auxSql)
                    tables.Add(tableName)
                    'jstr += ",""" + tableName + """:" + tableData
                Else
                    missingTables.Add(tableName)
                    'jstr += ",""" + tableName + """: []"
                End If
            Next
            Dim tempjstr As String = ""
            If auxFetchSql.Trim <> "" Then
                Dim auxDataSet As DataSet = Database.Tenant.GetDataSet(auxFetchSql)
                For i As Integer = 0 To tables.Count - 1
                    tempjstr += ",""" + tables(i) + """:" + JSON.GetTableAsJSON(auxDataSet.Tables(i))
                Next
                Dim aggregateFieldSettings As DataTable = Database.Tenant.GetDataTable("SELECT * FROM AggregateFieldSettings ORDER BY TableName")
                Dim minAccountLevel As Integer = Integer.MaxValue
                Dim maxAccountLevel As Integer = Integer.MinValue
                For Each dr As DataRow In aggregateFieldSettings.Rows
                    Dim tableName As String = dr(1).ToString()
                    Dim colName As String = dr(2).ToString()
                    Dim functName As String = dr(3).ToString()
                    Dim fieldName As String
                    Dim uniqueName As String = dr(6).ToString()
                    If uniqueName <> "" Then
                        fieldName = uniqueName
                    Else
                        fieldName = functName + "_" + tableName + "_" + colName
                    End If
                    Dim Condition As String = Regex.Replace(dr(5).ToString(), Chr(34), Chr(39))
                    Dim fieldId As String = Database.Tenant.GetStringValue("SELECT Id FROM DataSourceField WHERE Name = '" + colName + "' AND SourceTable = '" + tableName + "'")
                    Dim pcDataRow As DataRow()
                    If fieldId <> Nothing Or fieldId <> "" Then
                        pcDataRow = changes.Select("FieldId = '" + fieldId + "'")
                    End If
                    Dim value As String = Nothing
                    Dim medianArray() As Double
                    If tableName <> "Parcel" Then
                        For i As Integer = 0 To tables.Count - 1
                            If tableName = tables(i) Then
                                Dim dt As DataTable = auxDataSet.Tables(i)
                                If dt.Rows.Count > 0 Then
                                    For Each pcRow As DataRow In pcDataRow
                                        Dim j As Integer = 0
                                        For Each dtRow As DataRow In dt.Rows
                                            If pcRow("AuxROWUID").ToString() = dtRow("ROWUID").ToString() Then
                                                dt.Rows(j)(colName) = IIf(IsDBNull(pcRow("NewValue")), DBNull.Value, pcRow("NewValue").ToString())
                                            End If
                                            j += 1
                                        Next
                                    Next
                                    Try
                                        Select Case functName
                                            Case "FIRST"
                                                value = dt.Rows(0)(colName).ToString()
                                            Case "LAST"
                                                value = dt.Rows(dt.Rows.Count - 1)(colName).ToString()
                                            Case "COUNT"
                                                value = dt.Rows.Count.ToString()
                                            Case "MEDIAN"
                                                Dim m As Integer = 0
                                                Dim medianPush As Boolean = False
                                                ReDim medianArray(0)
                                                For Each dtRow As DataRow In dt.Rows
                                                    If Not dtRow("CC_deleted") Then
                                                        ReDim Preserve medianArray(m)
                                                        medianArray(m) = IIf(IsDBNull(dtRow(colName)), -9999, CDbl(dtRow(colName)))
                                                        medianPush = True
                                                        m += 1
                                                    End If
                                                Next
                                                Array.Sort(medianArray)
                                                Dim mLength As Integer = medianArray.Length()
                                                If medianPush Then
                                                    If mLength Mod 2 = 0 Then
                                                        value = (medianArray((mLength / 2) - 1) + medianArray(((mLength + 2) / 2) - 1)) / 2
                                                    Else
                                                        value = medianArray(((mLength + 1) / 2) - 1)
                                                    End If
                                                    If value = -9999 Then
                                                        value = Nothing
                                                    End If
                                                End If
                                                'value = Database.Tenant.GetStringValue("SELECT AVG(1 * " + colName + ") FROM ( SELECT " + colName + ", c  = COUNT(*) OVER (), rn = ROW_NUMBER() OVER (ORDER BY " + colName + ") FROM XT_" + tableName + " WHERE CC_ParcelId = " + parcelId.ToString() + " AND CC_deleted <> 1 " + IIf(Condition <> "", "AND " + Condition +" ", "") +" ) AS x WHERE rn IN ((c + 1)/2, (c + 2)/2) ")
                                            Case "MAX", "MIN", "SUM", "AVG"
                                                value = IIf(Not IsDBNull(dt.Compute(functName + "([" + colName + "])", Condition)), dt.Compute(functName + "([" + colName + "])", Condition).ToString(), "")
                                        End Select
                                    Catch ex As Exception

                                    End Try
                                End If
                                Exit For
                            End If
                        Next
                    Else
                        value = IIf(Not IsDBNull(parcelData.Rows(0)(colName)), IIf(functName = "COUNT", 1, parcelData.Rows(0)(colName).ToString()), "")
                    End If

                    Dim col As DataColumn = New DataColumn(fieldName)
                    parcelData.Columns.Add(col)
                    parcelData.Rows(0)(fieldName) = value
                Next
            End If

            Dim prcBkpTbl As DataTable = Database.Tenant.GetDataTable("SELECT Id,LoginId,CONVERT(VARCHAR(20),dbo.GetLocalDate(UploadTime), 22) UploadTime,ParcelId,Path FROM PrcBackup WHERE Path IS NOT NULL AND ParcelId=" & parcelId.ToString())

            Dim countOfAudit As Integer = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM ParcelAuditTrail WHERE ParcelId = " + parcelId.ToString() + " AND EventTime < DATEADD(YEAR, -3, GETDATE())")

            Dim jstr As String = JSON.GetRowAsJSON(parcelData.Rows(0)).TrimEnd("}")
            jstr += ",""Images"":" + JSON.GetTableAsJSON(images)
            jstr += ",""MapPoints"":" + JSON.GetTableAsJSON(mapPoints)
            jstr += ",""ParcelSketchData"":" + JSON.GetTableAsJSON(sketch)
            jstr += ",""ReviewLog"":" + JSON.GetTableAsJSON(reviewLog)
            jstr += ",""AuditTrail"":" + JSON.GetTableAsJSON(auditTrail)
            jstr += ",""ParcelChanges"":" + JSON.GetTableAsJSON(changes)
            jstr += ",""ClientSettings"":" + JSON.GetTableAsJSON(settings)
            jstr += ",""ParcelGISPoints"":" + JSON.GetTableAsJSON(gisPoints)
            jstr += ",""PrcBackup"":" + JSON.GetTableAsJSON(prcBkpTbl)
            jstr += ",""WorkFlowTypes"":" + JSON.GetTableAsJSON(WorkFlowTypes)
            jstr += ",""countOfAudit"":" + countOfAudit.ToString()
            If isDTRCall() Then
                jstr += ",""AdjacentParcels"":" + JSON.GetTableAsJSON(AdjacentParcels)
            End If
            If isBPPParcel() Then
                jstr += ",""ChildParcels"":" + JSON.GetTableAsJSON(BPPChildParcels)
            End If
            jstr += tempjstr
            For Each mt As String In missingTables
                jstr += ",""" + mt + """: []"
            Next

            jstr += "}"
            JSON.WriteString(jstr)
        Else
            JSON.Error("Parcel not found")
        End If
    End Sub


    Public Function GetRelatedTables(ByVal db As Database) As List(Of String)
        Dim tables As New Dictionary(Of String, Integer)
        _getConnectedTables(db, tables, db.Application.ParcelTable)
        Return tables.Where(Function(x) x.Value = 1 Or x.Value = 2).Select(Function(x) x.Key).ToList
    End Function

    Private Sub _getConnectedTables(ByVal db As Database, ByVal tables As Dictionary(Of String, Integer), ByVal tableName As String)
        For Each connectTableName In db.GetDataTable("SELECT t.Name, r.Relationship FROM DataSourceRelationships r INNER JOIN DataSourceTable t ON r.TableId = t.Id INNER JOIN DataSourceTable p ON r.ParentTableId = p.Id WHERE p.Name = '{0}' ORDER BY Relationship".FormatString(tableName)).Rows.OfType(Of DataRow).Select(Function(x) New With {.TableName = x.GetString("Name"), .Relationship = x.GetInteger("Relationship")}).ToArray
            If Not tables.ContainsKey(connectTableName.TableName) Then
                tables.Add(connectTableName.TableName, connectTableName.Relationship)
            End If
            _getConnectedTables(db, tables, connectTableName.TableName)
        Next
    End Sub

    Private Sub _getParcelDetailUpdates()
        Dim sParcelId As String = Request("ParcelId")
        Dim parcelId As Integer
        If Integer.TryParse(sParcelId, parcelId) Then
            Dim dtr As Integer = 0
            If isDTRCall() Then
                dtr = 1
            End If
            Dim query = "EXEC qc_GetParcelDataUpdates @ParcelId ={0},@dtr ={1}, @InternalView = {2}".FormatString(parcelId, dtr, EnabledInternalView)
            Dim pds As DataSet = Database.Tenant.GetDataSet(query)

            Dim auditTrail As DataTable = pds.Tables(0)
            Dim changes As DataTable = pds.Tables(1)
            Dim parcel As DataRow = pds.Tables(2).Rows(0)
            Dim pImages As DataTable = pds.Tables(3)
            Dim prcBkpTbl As DataTable = Database.Tenant.GetDataTable("SELECT Id,LoginId,FORMAT(dbo.GetLocalDate(UploadTime), 'MM-dd-yy HH:mm:ss tt') UploadTime,ParcelId,Path FROM PrcBackup WHERE Path IS NOT NULL AND ParcelId=" & parcelId.ToString())
            Dim countOfAudit As Integer = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM ParcelAuditTrail WHERE ParcelId = " + parcelId.ToString() + " AND EventTime < DATEADD(YEAR, -3, GETDATE())")
            Dim selectedAppraisalType As String = Database.Tenant.GetTopRow(String.Format("select SelectedAppraisalType from parcel where Id={0}", parcelId)).GetString("SelectedAppraisalType")

            If String.IsNullOrEmpty(selectedAppraisalType) Then
                selectedAppraisalType = Nothing
            End If

            Dim jstr As String = "{""ParcelId"": " + sParcelId
            jstr += ",""AuditTrail"":" + JSON.GetTableAsJSON(auditTrail)
            jstr += ",""ParcelChanges"":" + JSON.GetTableAsJSON(changes)
            jstr += ",""Images"":" + JSON.GetTableAsJSON(pImages)
            jstr += ",""PrcBackup"":" + JSON.GetTableAsJSON(prcBkpTbl)
            jstr += ",""Reviewed"":" + IIf(parcel.GetBoolean("Reviewed"), "true", "false")
            jstr += ",""Priority"":" + parcel.GetString("Priority")
            jstr += ",""QC"":" + IIf(parcel.GetBoolean("QC"), "true", "false")
            jstr += ",""QCDate"":" + JSON.ObjectToJSON(GetType(Date), parcel.GetDate("QCDate"))
            jstr += ",""LocalQCDate"":" + JSON.ObjectToJSON(GetType(Date), parcel.GetDate("LocalQCDate"))
            jstr += ",""LocalReviewDate"":" + JSON.ObjectToJSON(GetType(Date), parcel.GetDate("LocalReviewDate"))
            jstr += ",""countOfAudit"":" + countOfAudit.ToString()
            If selectedAppraisalType Is Nothing Then
                jstr += ",""SelectedAppraisalType"":null"
            Else
                jstr += ",""SelectedAppraisalType"":" + selectedAppraisalType.ToString()
            End If

            'jstr += ",""SelectedAppraisalType"":" + selectedAppraisalType.ToString()
            jstr += ",""status"": ""OK"""
            jstr += "}"
            JSON.WriteString(jstr)
        Else
            JSON.Error("Parcel not found")
        End If
    End Sub

    Private Sub _getdetailsonreject()
        Dim sParcelId As String = Request("ParcelId")
        Dim parcelId As Integer
        If Integer.TryParse(sParcelId, parcelId) Then
            Dim pds As DataSet = Database.Tenant.GetDataSet("SELECT Priority,ParcelAlertType  FROM Parcel WHERE Id ={0}".SqlFormatString(parcelId))

            Dim changes As DataTable = pds.Tables(0)

            Dim jstr As String = "{""ParcelId"": " + sParcelId
            jstr += ",""ParcelChanges"":" + JSON.GetTableAsJSON(changes)
            jstr += "}"
            JSON.WriteString(jstr)
        Else
            JSON.Error("Parcel not found")
        End If
    End Sub

    Private Sub _searchParcels()

        Dim page As Integer = 1
        Dim pagesize As Integer = 25
        Dim dtrTaskId As Integer = -1
        Dim randomPercent As Integer = 100
        Integer.TryParse(Request("page"), page)
        Integer.TryParse(Request("pagesize"), pagesize)
        If Not Integer.TryParse(Request("random"), randomPercent) Then
            randomPercent = 100
        End If
        If Request("dtrfilter") IsNot Nothing AndAlso Request("dtrfilter") <> "" Then
            Integer.TryParse(Request("dtrfilter"), dtrTaskId)
        End If
        HttpContext.Current.Session("ptid") = Request("PrioTableId")
        Dim searchQuery As String = Request("query")
        Dim operation As String = Request("Operation")
        Dim exportType As String = Request("export")
        If Request("equery") <> "" Then
            Dim sq As String = Text.Encoding.Default.GetString(Convert.FromBase64String(Request("equery").Replace("_", "+")))
            searchQuery = sq.Replace("Â", "")
        End If

        If Request("export") <> "" And Request("export") <> "createAdhoc" Then
            Dim query As String = ""
            If searchQuery IsNot Nothing Then
                If searchQuery.Contains("RedirectedPriorityListParcels") Then
                    query = "EXEC qc_SearchParcels @Page = {0}, @PageSize = {1}, @Query = {2}, @SortName = {3}, @SortOrder = {4}, @RandomPercent = {5}, @DTRTaskId = {6},@pTableId = {7},@Operation={8}".FormatString(1, 75000, searchQuery.ToSqlValue(True), Request("sortname").ToSqlValue(True), Request("sortorder").ToSqlValue(True), randomPercent, dtrTaskId, HttpContext.Current.Session("ptid"), operation.ToSqlValue(True))
                ElseIf Request("CustomSearch") = "true" Then
                    Dim joinstr, Condstr As String
                    Dim SplitedQuery As String() = searchQuery.Split("^")
                    joinstr = SplitedQuery(0)
                    Condstr = SplitedQuery(1)
                    query = "EXEC qc_CustomSearchParcels @Page = {0}, @PageSize = {1}, @JoinQuery = {2}, @CondQuery = {3}, @SortName = {4}, @SortOrder = {5}, @RandomPercent = {6}, @DTRTaskId = {7}".FormatString(1, 75000, joinstr.ToSqlValue(True), Condstr.ToSqlValue(True), Request("sortname").ToSqlValue(True), Request("sortorder").ToSqlValue(True), randomPercent, dtrTaskId)
                Else
                    query = "EXEC qc_SearchParcels @Page = {0}, @PageSize = {1}, @Query = {2}, @SortName = {3}, @SortOrder = {4}, @RandomPercent = {5}, @DTRTaskId = {6},@Operation={7}, @randomTrackerId = {8}".FormatString(1, 75000, searchQuery.ToSqlValue(True), Request("sortname").ToSqlValue(True), Request("sortorder").ToSqlValue(True), randomPercent, dtrTaskId, operation.ToSqlValue(True), Request("randomTrackerId").ToSqlValue(True))
                End If
            Else
                query = "EXEC qc_SearchParcels @Page = {0}, @PageSize = {1}, @Query = {2}, @SortName = {3}, @SortOrder = {4}, @RandomPercent = {5}, @DTRTaskId = {6},@Operation={7}, @randomTrackerId = {8}".FormatString(1, 75000, searchQuery.ToSqlValue(True), Request("sortname").ToSqlValue(True), Request("sortorder").ToSqlValue(True), randomPercent, dtrTaskId, operation.ToSqlValue(True), Request("randomTrackerId").ToSqlValue(True))
            End If
            Dim ds As DataSet = Database.Tenant.GetDataSet(query)
            Dim dt As DataTable = ds.Tables(0)
            Dim fileName As String = ""
            If exportType = "csv" Then
                fileName = UserName + " query " + Now.ToString("yyyy-MM-dd") + ".csv"
                Response.Clear()
                Response.ContentType = "text/csv"
                Response.AddHeader("Content-Disposition", "attachment;filename=" + fileName)
            End If
            Dim grd As GridView = New GridView()
            grd.AutoGenerateColumns = False
            If exportType = "xls" Then
                Response.Clear()
                Response.Buffer = True
                Response.ClearContent()
                Response.ClearHeaders()
                fileName = UserName + " query " + Now.ToString("yyyy-MM-dd") + ".xls"
                Response.AddHeader("Content-Disposition", "attachment;filename=" + fileName)
                Response.Charset = ""
                Response.ContentType = "application/vnd.ms-excel"
            End If
            If exportType = "xlsx" Then
                fileName = UserName + " query " + Now.ToString("yyyy-MM-dd") + ".xlsx"
                Response.Clear()
                Response.Buffer = True
                Response.ClearContent()
                Response.ClearHeaders()
                Response.AddHeader("Content-Disposition", "attachment;filename=" + fileName)
                Response.Charset = ""
                Response.ContentType = "application/vnd.openxml.formats-officedocument.spreadsheetml.sheet"
            End If
            Dim reset As Integer = 0
            Dim dtr As Integer = 0
            If dtrTaskId <> -1 Then
                dtr = 1
            End If

            ' Dim fields As DataTable = Database.Tenant.GetDataTable("EXEC [qc_GetSearchFieldSettings] @LoginId={0}, @Reset={1},@DTR={2}".SqlFormatString("NULL", reset, dtr))

            'For Each dr As DataRow In fields.Rows
            '    If line <> "" Then line += ","
            '    line += dr.GetString("display")
            '    fieldNames.Add(dr.GetString("name"))
            'Next

            Dim resultFields As String = Request("searchfields").Replace("~~", "#")

            Dim line As String = ""
            Dim Headline As String = ""
            Dim fieldNames As New List(Of String)
            Dim regex As New Regex("\((([(^)]+))\)")
            Dim testt As Integer
            For Each field In resultFields.Split("|")
                If line <> "" Then line += ","

                ' Extract values inside parentheses as a single unit
                Dim match As Match = regex.Match(field)
                Dim insideParentheses As String = If(match.Success, match.Groups(1).Value, "")

                ' Remove parentheses and split the remaining field
                Dim remainingField As String = If(match.Success, field.Replace(match.Value, ""), field)

                Dim fieldParts As String() = remainingField.Split(",")

                Dim dataField As String = fieldParts(0).ToString()
                Dim columnName As String
                Dim width As String
                testt = fieldParts.Length
                If fieldParts.Length >= 4 Then
                    Dim sb As New StringBuilder()
                    For i As Integer = 1 To Math.Min(15, fieldParts.Length - 2)
                        sb.Append(fieldParts(i))
                        If i < Math.Min(15, fieldParts.Length - 2) Then
                            sb.Append(",")
                        End If
                    Next
                    Dim combinedColumn As String = $"""{sb.ToString()}"""
                    columnName = combinedColumn
                    width = fieldParts(testt - 1)
                ElseIf fieldParts.Length = 3 Then
                    columnName = fieldParts(1).ToString()
                    width = fieldParts(2).ToString()
                Else
                    ' Handle invalid input here if needed
                    Throw New ArgumentException("Invalid format for remainingField")
                End If
                width = If(width = "" Or width = "0", "50", width)

                ' Dim remainingField As String = If(match.Success, field.Replace(match.Value, ""), field)
                ' Dim dataField As String = remainingField.Split(",")(0).ToString()
                'Dim columnName As String = remainingField.Split(",")(1).ToString()
                'Dim width As String = remainingField.Split(",")(2).ToString()
                'width = If(width = "" Or width = "0", "50", width)
                ' Add the extracted field names to the list
                fieldNames.Add(dataField)

                ' Concatenate the column name with the values inside parentheses
                line += columnName & insideParentheses

                Dim gridField As BoundField = New BoundField()
                gridField.DataField = dataField

                ' Include parentheses in the HeaderText
                gridField.HeaderText = columnName & If(insideParentheses <> "", " (" & insideParentheses & ")", "")
                Headline += columnName & If(insideParentheses <> "", " (" & insideParentheses & ")", "")

                ' Check if there is another element, then add a comma
                If resultFields.Split("|").Length > fieldNames.Count Then
                    Headline += ","
                End If
                gridField.ItemStyle.Width = CType(width, Integer)
                grd.Columns.Add(gridField)
            Next


            If exportType = "csv" Then
                Response.Write(Headline + vbNewLine)
                For Each dr As DataRow In dt.Rows
                    Headline = ""
                    For Each fname In fieldNames
                        If Headline <> "" Then Headline += ","
                        Dim dataValue As String = dr.GetString(fname).Replace(vbCr, "").Replace(vbLf, " ").Replace(vbTab, " ")
                        If ((dataValue <> "") AndAlso (IsNumeric(dataValue)) AndAlso (((dataValue.Substring(0, 1) = "0") AndAlso (dataValue.Length > 1)) Or (dataValue.Length > 15))) Then
                            Headline += """'" + dataValue + """"
                        Else
                            Headline += """" + dataValue + """"
                        End If
                    Next
                    Response.Write(Headline + vbNewLine)
                Next
            Else
                grd.DataSource = dt
                grd.DataBind()
                Dim excelStream = ExcelGenerator.ExportGrid(grd, dt, "QC-Export", "QC-Export")
                excelStream.CopyTo(Response.OutputStream)
            End If


        ElseIf Request("export") = "createAdhoc" Then
            Dim downSynCheck As Integer = 0
            If (Request("checkDownSync") <> Nothing) Then
                downSynCheck = 1
            End If
            Dim query As String
            If Request("CustomSearch") = "true" Then
                Dim joinstr, Condstr As String
                Dim SplitedQuery As String() = searchQuery.Split("^")
                joinstr = SplitedQuery(0)
                Condstr = SplitedQuery(1)
                query = "EXEC qc_CustomSearchParcels @Page = {0}, @PageSize = {1}, @JoinQuery = {2}, @CondQuery = {3}, @SortName = {4}, @SortOrder = {5}, @RandomPercent = {6}, @DownSyncCheck = {7}, @DTRTaskId = {8}".FormatString(page, pagesize, joinstr.ToSqlValue(True), Condstr.ToSqlValue(True), Request("sortname").ToSqlValue(True), Request("sortorder").ToSqlValue(True), randomPercent, downSynCheck, dtrTaskId)
            Else
                query = "EXEC qc_SearchParcels @Page = {0}, @PageSize = {1}, @Query = {2}, @SortName = {3}, @SortOrder = {4}, @RandomPercent = {5}, @DownSyncCheck = {6}, @DTRTaskId = {7},@Operation={8}, @randomTrackerId = {9}".FormatString(page, pagesize, searchQuery.ToSqlValue(True), Request("sortname").ToSqlValue(True), Request("sortorder").ToSqlValue(True), randomPercent, downSynCheck, dtrTaskId, operation.ToSqlValue(True), Request("randomTrackerId").ToSqlValue(True))
            End If
            Dim ds As DataSet = Database.Tenant.GetDataSet(query)

            Dim keyValues As New List(Of String)
            Dim keyfields As New List(Of String)

            For i = 1 To 10
                If Database.Tenant.Application.KeyField(i).IsNotEmpty Then
                    keyValues.Add("KEYVALUE" + i.ToString())
                    'keyfields.Add("KeyValue" + i.ToString() + ":" + Database.Tenant.Application.KeyField(i))
                End If
            Next
            If Database.Tenant.Application.ShowKeyValue1 = False Then
                keyValues.Remove("KEYVALUE1")
            End If
            If Database.Tenant.Application.ShowAlternateField = True Then
                keyValues.Add("Alternatekeyfieldvalue")
            End If

            keyValues.Add("PRIORITY")
            keyValues.Add("ParcelAlert")

            Dim dt As DataTable = New DataView(ds.Tables(0)).ToTable(False, keyValues.ToArray())

            '            For Each dc As DataColumn In dt.Columns
            '                For Each kf In keyfields
            '                    If dc.ToString() = kf.Split(":"c)(0) Then
            '                        dt.Columns(kf.Split(":"c)(0)).ColumnName = kf.Split(":"c)(1)
            '                        Exit For
            '                    End If
            '                Next
            '            Next
            For Each col As DataColumn In dt.Columns
                If col.ColumnName.ToString() <> "Alternatekeyfieldvalue" Then
                    col.ColumnName = col.ColumnName.ToString().ToUpper()
                End If
            Next
            dt.Columns("ParcelAlert").ColumnName = "MESSAGE"
            Database.Tenant.LoadTableFromDataTable("PriorityListItems", dt, 1)
            Dim result As String = "0"
            If EnableNewPriorities Then
                result = Database.Tenant.GetStringValue("SELECT Count(*) FROM PriorityListItems WHERE Priority  IN(1,2,3,4,5) AND PriorityListId =" + HttpContext.Current.Session("ptid").ToString())
            Else
                result = Database.Tenant.GetStringValue("SELECT Count(*) FROM PriorityListItems WHERE Priority  IN(1,2) AND PriorityListId =" + HttpContext.Current.Session("ptid").ToString())
            End If
            JSON.OK(result)
        Else
            Dim result = New With {.rows = DirectCast(Nothing, DataTable), .page = 0, .total = 0, .query = "", .downSyncCheck = DirectCast(Nothing, DataTable), .dbName = "", .TrackerId = 0}
            Dim downSynCheck As Integer = 0
            If (Request("checkDownSync") <> Nothing) Then
                downSynCheck = 1
            End If
            If searchQuery.Contains("RedirectedPriorityListParcels") Then
                Dim query As String = "EXEC qc_SearchParcels @Page = {0}, @PageSize = {1}, @Query = {2}, @SortName = {3}, @SortOrder = {4}, @RandomPercent = {5}, @DownSyncCheck = {6}, @DTRTaskId = {7},@pTableId = {8},@Operation={9}".FormatString(page, pagesize, searchQuery.ToSqlValue(True), Request("sortname").ToSqlValue(True), Request("sortorder").ToSqlValue(True), randomPercent, downSynCheck, dtrTaskId, HttpContext.Current.Session("ptid"), operation.ToSqlValue(True))
                Dim ds As DataSet = Database.Tenant.GetDataSet(query)
                Dim stats As DataRow = ds.Tables(1).Rows(0)

                If Request("checkDownSync") = "true" Then
                    result.downSyncCheck = ds.Tables(2)
                    result.dbName = HttpContext.Current.GetCAMASession.OrganizationName.ToString()
                End If
                result.rows = ds.Tables(0)
                result.page = stats.GetInteger("Page")
                result.total = stats.GetInteger("Records")
                JSON.Write(result)
            ElseIf Request("CustomSearch") = "true" Then
                Dim joinstr, Condstr As String
                Dim SplitedQuery As String() = searchQuery.Split("^")
                joinstr = SplitedQuery(0)
                Condstr = SplitedQuery(1)
                Dim query As String = "EXEC qc_CustomSearchParcels @Page = {0}, @PageSize = {1}, @JoinQuery = {2}, @CondQuery = {3}, @SortName = {4}, @SortOrder = {5}, @RandomPercent = {6}, @DownSyncCheck = {7}, @DTRTaskId = {8}".FormatString(page, pagesize, joinstr.ToSqlValue(True), Condstr.ToSqlValue(True), Request("sortname").ToSqlValue(True), Request("sortorder").ToSqlValue(True), randomPercent, downSynCheck, dtrTaskId)
                Dim ds As DataSet = Database.Tenant.GetDataSet(query)
                Dim stats As DataRow = ds.Tables(1).Rows(0)
                If Request("checkDownSync") = "true" Then
                    result.downSyncCheck = ds.Tables(2)
                    result.dbName = HttpContext.Current.GetCAMASession.OrganizationName.ToString()
                End If
                result.rows = ds.Tables(0)
                result.page = stats.GetInteger("Page")
                result.total = stats.GetInteger("Records")
                JSON.Write(result)
            Else
                Dim query As String = "EXEC qc_SearchParcels @Page = {0}, @PageSize = {1}, @Query = {2}, @SortName = {3}, @SortOrder = {4}, @RandomPercent = {5}, @DownSyncCheck = {6}, @DTRTaskId = {7},@Operation={8}, @randomTrackerId = {9}".FormatString(page, pagesize, searchQuery.ToSqlValue(True), Request("sortname").ToSqlValue(True), Request("sortorder").ToSqlValue(True), randomPercent, downSynCheck, dtrTaskId, operation.ToSqlValue(True), Request("randomTrackerId").ToSqlValue(True))
                Dim ds As DataSet = Database.Tenant.GetDataSet(query)
                Dim stats As DataRow = ds.Tables(1).Rows(0)

                If Request("checkDownSync") = "true" Then
                    result.downSyncCheck = ds.Tables(2)
                    result.dbName = HttpContext.Current.GetCAMASession.OrganizationName.ToString()
                End If

                result.rows = ds.Tables(0)
                result.page = stats.GetInteger("Page")
                result.total = stats.GetInteger("Records")
                result.TrackerId = stats.GetInteger("TrackerId")
                JSON.Write(result)
            End If

        End If
    End Sub

    Private Sub _getSearchResultFields()
        'JSON.SendTable(Database.Tenant, "EXEC qc_GetSearchFieldSettings " + SqlUserName + If(isDTRCall, ", @DTR = 1", ""))
        Dim key_value As String = Database.Tenant.GetStringValue("SELECT KeyField1 FROM Application")
        Dim _camaSystemName As String
        Try
            _camaSystemName = Database.System.GetStringValue("Select c.Name FROM Organization o INNER JOIN CAMASystem c On o.CAMASystem= c.Id WHERE o.id = " & HttpContext.Current.GetCAMASession().OrganizationId)
        Catch ex As Exception
        End Try
        If _camaSystemName Is Nothing Or _camaSystemName = "" Then
            _camaSystemName = "0"
        End If
        Dim Nquery = "EXEC qc_GetSearchFieldSettings " + SqlUserName + If(isDTRCall(), ", @DTR = 1", "") + ",@camaSystem = " + _camaSystemName.ToSqlValue(True)
        Dim Ndt As DataTable = Database.Tenant.GetDataTable(Nquery)
        'To replace the display label with keyvalue1
        For Each dt As DataRow In Ndt.Rows
            If dt("name").ToString = "KeyValue1" Then
                dt.SetField("display", key_value)
            End If
        Next
        JSON.SendTable(Ndt)
    End Sub

    Private Sub _setSearchResultView()
        Dim settingsId As Integer = Request("SettingsId")
        Dim propertyName As String = Request("Property")
        Dim propertyValue As String = Request("Value")

        e_("EXEC qc_SetGridViewFieldSetting 'qc-parcel-search', {0}, {1}, {2}, {3}", False, UserName, settingsId, propertyName, propertyValue)
        JSON.OK()
    End Sub
    Private Sub _qccolumnswap()
        Dim source As String = Request("SettingsId")
        Dim propertyName As String = Request("Property")

        e_("EXEC qc_SetGridViewFieldSetting @GridView = 'qc-parcel-search',@orderChange = 1, @LoginId = {0}, @Property = {1},@source = {2}", False, UserName, propertyName, source)
        JSON.OK()
    End Sub

    Private Sub _getOfflineTables()
        Dim dtrParam As String = ""
        If Roles.IsUserInRole("DTR") Then
            dtrParam += ", @DTR = 1"
        End If
        Dim isDtr = Request("dtr")
        Dim pds As DataSet = Database.Tenant.GetDataSet("EXEC qc_GetOfflineTables " + SqlUserName + dtrParam)
        Dim assgnTable As DataTable = If(pds.Tables.Count > 12, pds.Tables(12), New DataTable())
        Dim selAppTypeTable As DataTable = If(pds.Tables.Count > 13, pds.Tables(13), New DataTable())
        Dim failedOnDownSyncStatus As DataTable = If(pds.Tables.Count > 14, pds.Tables(14), New DataTable())
        Dim dt As DataTable = Database.System.GetDataTable("select * from ForceUpdateApplicationStatus f join ForceUpdateApplication p on p.Id = f.UpdateId where f.orgId = " + HttpContext.Current.GetCAMASession.OrganizationId.ToString() + " and p.UpdateType='Update Application/Update System Data' and " + IIf(isDtr = "1", "f.dtrstatus", "f.qcstatus") + " = 0")
        Dim SketchFlag As DataTable = If(pds.Tables.Count > 15, pds.Tables(15), New DataTable())
        Dim ParcelWorkFlow As DataTable = New DataTable()
        If ClientSettings.PropertyValue("DTRWorkflowNew") = "1" AndAlso isDtr = 1 Then
            ParcelWorkFlow = Database.Tenant.GetDataTable("SELECT * FROM ParcelWorkFlowStatusType ORDER BY Id")
        End If
        Dim DTRStatusTypeAll As DataTable = Database.Tenant.GetDataTable("SELECT * FROM DTR_StatusType ORDER BY Id")
        Dim tables = New With {
           .SearchFields = pds.Tables(0),
           .Neighborhoods = pds.Tables(1),
           .Users = pds.Tables(2),
           .Priority = pds.Tables(3),
           .YesNo = pds.Tables(4),
           .FieldAlertTypes = pds.Tables(5),
           .QCType = pds.Tables(6),
           .GridColumns = pds.Tables(7),
           .DTRStatus = pds.Tables(8),
           .DownSyncRejectReason = pds.Tables(9),
           .DbStatus = pds.Tables(10),
           .AggrFields = pds.Tables(11),
           .AssignedTo = assgnTable,
           .SelectedAppraisalTypes = selAppTypeTable,
           .ForceUpdateApplicationStatus = dt,
           .FailedOnDownSyncStatus = failedOnDownSyncStatus,
           .SketchFlags = SketchFlag,
           .ParcelWorkFlow = ParcelWorkFlow,
           .DTRStatusTypeAll = DTRStatusTypeAll
        }
        JSON.Write(tables)
    End Sub

    Private Sub _deletePhoto()

        Dim imageid() As String = Request("ImageId").Split("$")
        For Each imid As String In imageid
            Dim Id As Integer = 0
            If Integer.TryParse(imid, Id) Then
                Photodelete(Id)
            End If
        Next
        Dim newPrimary As String = Request("newPrimary")
        If newPrimary <> "" Then
            e_("Update ParcelImages SET IsPrimary = 1 WHERE Id = " & newPrimary)
        End If
        JSON.OK()
    End Sub

    Private Sub _recoverPhoto()
        Dim imageid As String = Request("ImageId")
        Dim metaField As String = Request("MetaField")
        Dim isPrimary As String = Request("isPrimary")
        Dim drI As DataRow = Database.Tenant.GetTopRow("SELECT * FROM ParcelImages WHERE Id = " & imageid)
        If drI IsNot Nothing Then
            Dim metaQuery As String = ""
            If Not String.IsNullOrEmpty(metaField) Then
                metaQuery = ", " & metaField & " = 'true' "
            End If
            If isPrimary = "1" Then
                metaQuery = metaQuery + ", IsPrimary = 1 "
            End If
            e_("UPDATE ParcelImages SET CC_Deleted = 0" & metaQuery & " WHERE Id = {0}", True, imageid)
            Dim _appTypes As String = IIf(isDTRCall() = True, "DTR", "QC")
            ParcelAuditStream.CreateEvent(DB, Date.UtcNow, drI("ParcelId").ToString, UserName, 2, _appTypes & ": Photo #" & imageid & " recovered", _appTypes)
            JSON.OK()
        Else
            JSON.Error("Image with the corresponding ImageId didn't found.")
        End If
    End Sub

    Private Sub _setAsMainImage()
        Dim _appTypes As String = IIf(isDTRCall() = True, "DTR", "QC")
        Dim imageid As String = Request("ImageId")
        Dim pId As String = Request("ParcelId")
        Dim str As String = "UPDATE ParcelImages SET IsPrimary = 0 WHERE ParcelId = " + pId + " AND IsPrimary = 1"
        str += "; UPDATE ParcelImages SET IsPrimary = 1 WHERE Id = " + imageid
        str += "; INSERT INTO ParcelAuditTrail (ParcelId, LoginID, EventType, Description,ApplicationType) VALUES (" + pId + ", '" + UserName + "', 0, 'Primary photo of the parcel changed to #" + imageid + ".','" + _appTypes + "')"
        e_(str)
        Dim dt As DataTable = Database.Tenant.GetDataTable("Select p.Id, p.EventDate, p.EventTime, p.ParcelId, CONCAT(ISNULL(us.FirstName, p.LoginID), ' ',us.LastName) AS LoginID,p.EventType, REPLACE(p.Description, concat('by ',p.LoginID), CONCAT('by ',us.firstName,' ',us.LastName)) AS Description, p.CorrespondingChangeId, Format(dbo.GetLocalDate(p.EventTime),'yyyy-MM-dd hh:mm tt') AS EventTimeNew, Format(dbo.GetLocalDate(p.EventTime),'yyyy-MM-dd hh:mm tt') AS LocalEventTime From ParcelAuditTrail p Left Join UserSettings us On  us.LoginId = p.LoginID WHERE   ParcelId = " + pId + " ORDER BY p.Id")

        Dim jstr As String = "{""ParcelId"": " + pId
        jstr += ",""AuditTrail"":" + JSON.GetTableAsJSON(dt)
        jstr += ",""status"": ""OK"""
        jstr += "}"
        JSON.WriteString(jstr)
    End Sub

    Private Sub _updatePrimaryPhoto()
        Dim imageid As String = Request("ImageId")
        e_("UPDATE ParcelImages SET IsPrimary = 1 WHERE Id = {0}", True, imageid)
        JSON.OK()
    End Sub

    Private Sub Photodelete(imageId As Integer)
        Dim _appTypes As String = IIf(isDTRCall() = True, "DTR", "QC")
        Dim drI As DataRow = Database.Tenant.GetTopRow("SELECT * FROM ParcelImages WHERE Id = " & imageId)
        If drI IsNot Nothing Then
            If Not ClientSettings.PropertyValue("PhotoDeleteAndRecover") = "1" Then
                Dim s3 As New S3FileManager()
                s3.DeleteFile(drI("Path"))
            End If
            Dim UpdatePushLink As String = Request("UpdatePushLink")
            e_("EXEC cc_Parcel_DeletePhoto {0}, {1}, 0, {2},{3}", True, UserName, imageId, UpdatePushLink, _appTypes)
        End If
    End Sub


    Sub _setQC()
        Dim parcelId As Integer = 0
        Dim qc As Integer = 0
        If Integer.TryParse(Request("ParcelId"), parcelId) Then
            If Integer.TryParse(Request("QC"), qc) Then
                If qc = 0 Then
                    'call recovery functions
                    Dim childTablesSql As String = "SELECT pc.AuxROWUID, d.SourceTable, pc.Action FROM ParcelChanges pc join DataSourceField d on pc.Fieldid = d.Id WHERE pc.Action IN('new','delete','futurecopy') AND pc.AuxROWUID is not null and pc.ParcelId ={0} GROUP BY pc.AuxROWUID, d.SourceTable, pc.Action"
                    For Each c As DataRow In DB.GetDataTable(childTablesSql.SqlFormatString(parcelId)).Rows
                        Dim AuxROWUID As String = c.GetString("AuxROWUID")

                        Dim SourceTable As String = c.GetString("SourceTable")
                        'Dim sourcetablesSql As String = "    SELECT  SourceTable FROM DataSourceField WHERE Id = {0}"

                        'For Each s As DataRow In DB.GetDataTable(sourcetablesSql.SqlFormatString(FieldId)).Rows

                        __recoverChildRecords(DB, AuxROWUID, SourceTable, parcelId)

                        'Next
                    Next
                ElseIf qc = -2 Then
                    Database.Tenant.Execute("UPDATE ParcelData Set cc_deleted = 1 where cc_parcelId = {0}".SqlFormatString(parcelId))
                    Dim KeyValue As String = Database.Tenant.GetStringValue("SELECT KeyValue1 from parcel where Id = {0}".SqlFormatString(parcelId))
                    Dim pdDetails As DataRow = DB.GetTopRow("SELECT ClientROWUID,ROWUID from parceldata where CC_ParcelId = {0}".SqlFormatString(parcelId))
                    If KeyValue = "0" Then
                        KeyValue = pdDetails.GetString("ClientROWUID")
                    End If
                    Dim parceltable = DB.GetStringValue("select top 1 parceltable from Application ")
                    Dim DFID = DB.GetIntegerValue("SELECT MIN(Id) FROM DataSourceField WHERE SourceTable ={0}".SqlFormatString(parceltable))
                    _processParcelDataChange(DB, UserName, Date.UtcNow, parcelId, pdDetails.GetString("ROWUID"), DFID, Nothing, Nothing, 0, 0, "delete")
                    Database.Tenant.Execute("INSERT INTO ParcelActionLog(ParcelID,KeyValue1,Action,ActionBy,ApplicationType,[UpdatedTime]) VALUES({0},{1},{2},{3},{4},{5})".SqlFormatString(parcelId, KeyValue, 1, UserName, "QC", Date.UtcNow))
                    ParcelAuditStream.CreateEvent(DB, Date.UtcNow, parcelId, UserName, 1, "QC: Property deleted", "QC")
                ElseIf qc = -3 Then
                    Database.Tenant.Execute("UPDATE ParcelData Set cc_deleted = 0 where cc_parcelId = {0}".SqlFormatString(parcelId))
                    Dim KeyValue As String = Database.Tenant.GetStringValue("SELECT KeyValue1 from parcel where Id = {0}".SqlFormatString(parcelId))
                    Dim pdDetails As DataRow = DB.GetTopRow("SELECT ClientROWUID,ROWUID from parceldata where CC_ParcelId = {0}".SqlFormatString(parcelId))
                    If KeyValue = "0" Then
                        KeyValue = pdDetails.GetString("ClientROWUID")
                    End If
                    Database.Tenant.Execute("DELETE FROM ParcelChanges where Action = 'delete' and AuxROWUID = {1} and ParcelId = {0}".SqlFormatString(parcelId, Coalesce(pdDetails.GetString("ROWUID"), "")))
                    Database.Tenant.Execute("INSERT INTO ParcelActionLog(ParcelID,KeyValue1,Action,ActionBy,ApplicationType,[UpdatedTime]) VALUES({0},{1},{2},{3},{4},{5})".SqlFormatString(parcelId, KeyValue, 2, UserName, "QC", Date.UtcNow))
                    ParcelAuditStream.CreateEvent(DB, Date.UtcNow, parcelId, UserName, 2, "QC: Deleted property recovered", "QC")
                End If
                If qc > -2 Then
                    If isDTRCall() Then
                        e_("EXEC qc_Parcel_Set @LoginId = {0}, @ParcelId = {1}, @QC = {2}, @DTR = 1".SqlFormat(False, UserName, parcelId, qc))
                    Else
                        e_("EXEC qc_Parcel_Set {0}, {1}, {2}".SqlFormat(False, UserName, parcelId, qc))
                    End If
                End If
                JSON.OK()
            End If
        End If
    End Sub

    Private Sub _saveParcelChanges()

        Dim eventDate As Date = Date.Now
        Dim latitude As Single = "34.445"
        Dim longitude As Single = "56.7889"
        Dim parcelId As Integer = 0
        Dim dataRec As DataRow = Nothing
        Dim newRowid As Long = 0
        Dim _appType As String = Request("appType")
        Dim _appTypes As String
        Dim prioId As String = Request("prioId")
        If Integer.TryParse(Request("ParcelId"), parcelId) Then
            If (_appType = "SV") Then
                _appTypes = "SV"
            ElseIf ((isDTRCall() = True) Or _appType = "DTR") Then
                _appTypes = "DTR"
            Else
                _appTypes = "QC"
            End If
            Dim priority As String = Request("Priority")
            Dim alertMessage As String = Request("AlertMessage")
            If priority <> "" Then
                e_("EXEC cc_Parcel_SetPriority {0}, {1}, {2}, {3},{4},{5}", True, UserName, parcelId, priority, alertMessage, IIf(prioId = "", 0, prioId), _appTypes)
            End If
            Dim appraisal_type = Request("AppraisalType")
            Dim current_appraisal_type = Database.Tenant.GetTopRow("select SelectedAppraisalType from parcel where Id={0}".SqlFormatString(parcelId)).GetString("SelectedAppraisalType")

            Dim dtrStatus = Request("DTRStatus")
            Dim currentDTRStatus = Database.Tenant.GetTopRow("select DTRStatus from parcel where Id={0}".SqlFormatString(parcelId)).GetString("DTRStatus")
            Dim eventDescription = ""
            Dim updateColumns = ""
            Dim changedValue As String = ""

            If appraisal_type <> current_appraisal_type Then

                If (appraisal_type = "") Then
                    updateColumns = "SelectedAppraisalType=null"
                Else
                    changedValue = Database.Tenant.GetTopRow("select Code from AppraisalType where Id={0}".SqlFormatString(appraisal_type)).GetString("Code")
                    updateColumns = "SelectedAppraisalType={0}".SqlFormatString(appraisal_type)
                End If
                Database.Tenant.Execute("UPDATE Parcel SET " + updateColumns + " WHERE Id={0}".SqlFormatString(parcelId))
                If (_appTypes = "QC") Then
                    ParcelAuditStream.CreateFlagEvent(DB, eventDate, parcelId, UserName, "[QC] Appraisal Type changed to:" + changedValue, _appTypes)
                ElseIf (_appTypes = "DTR") Then
                    ParcelAuditStream.CreateFlagEvent(DB, eventDate, parcelId, UserName, "[DTR] Appraisal Type changed to:" + changedValue, _appTypes)
                End If

                Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT GETUTCDATE() As UtcDate")
                Database.Tenant.Execute("EXEC cc_doCustomChangeAction {0},{1},{2},{3},{4},@AppType = {5}".SqlFormat(True, parcelId, UserName, "AppraisalMethod", changedValue, dr.GetDate("UtcDate"), _appTypes))

            End If

            If _appTypes = "DTR" AndAlso dtrStatus <> currentDTRStatus Then

                If (dtrStatus = "") Then
                    updateColumns = "DTRStatus = null, DTR_Last_Status_Change_Date = GETUTCDATE(), DTR_Last_Status_Change_User = '" + UserName + "'"
                Else
                    changedValue = Database.Tenant.GetTopRow("select Description from DTR_StatusType where Code={0}".SqlFormatString(dtrStatus)).GetString("Description")
                    updateColumns = "DTRStatus={0}, DTR_Last_Status_Change_Date = GETUTCDATE(), DTR_Last_Status_Change_User = {1}".SqlFormatString(dtrStatus, UserName)
                End If
                Database.Tenant.Execute("UPDATE Parcel SET " + updateColumns + " WHERE Id={0}".SqlFormatString(parcelId))
                Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT GETUTCDATE() As UtcDate")

                Database.Tenant.Execute("EXEC cc_doCustomChangeAction {0},{1},{2},{3},{4},@AppType = {5}".SqlFormat(True, parcelId, UserName, "DTRStatus", changedValue, dr.GetDate("UtcDate"), _appTypes))

                ParcelAuditStream.CreateFlagEvent(DB, eventDate, parcelId, UserName, "Desk Review Status changed to:" + changedValue, _appTypes)
            End If


            Dim changeData = Request("Data")
            If changeData IsNot Nothing Then
                Dim lines = changeData.Split(vbLf)
                Dim dlines As New List(Of String)
                Dim lastLine As String = ""
                For Each line In lines
                    If line.Trim <> "" Then
                        If line.StartsWith("^^^") Then
                            If lastLine <> "" Then
                                dlines.Add(lastLine)
                            End If
                            lastLine = line
                        Else
                            lastLine = lastLine + vbNewLine + line
                        End If
                    End If
                Next
                If lastLine <> "" Then
                    dlines.Add(lastLine)
                End If
                For Each line In dlines
                    line = line.Substring(3)
                    Dim words As String() = line.Split("|".ToCharArray, 8)
                    Dim xParcelId, changeId, ParentROWUID, auxROWUID As Integer, qcValue, action As String, fieldId As Long, CC_YearStatus = ""
                    auxROWUID = 0
                    fieldId = 0
                    changeId = 0
                    ParentROWUID = 0
                    Integer.TryParse(words(0), xParcelId)
                    Integer.TryParse(words(1), auxROWUID)
                    Long.TryParse(words(2), fieldId)
                    Integer.TryParse(words(3), changeId)
                    Integer.TryParse(words(6), ParentROWUID)
                    qcValue = words(4)
                    If words.Length > 7 Then
                        CC_YearStatus = words(7)
                    End If

                    Dim field As DataRow = DB.GetTopRow("SELECT * FROM DataSourceField WHERE Id = " & fieldId)
                    If (field IsNot Nothing) Then
                        qcValue = DB.GetStringValue("EXEC CC_ProcessAndFillColumnValue {0},{1}".SqlFormat(True, field("name"), qcValue))
                    End If
                    action = words(5)
                    If action = "new" Then
                        _insertNewRecord(DB, xParcelId, UserName, words(1), fieldId, qcValue, eventDate, latitude, longitude, CC_YearStatus, _appTypes)
                    ElseIf action = "audit" Then
                        ParcelAuditStream.CreateFlagEvent(DB, eventDate, parcelId, UserName, _appTypes + ": " + qcValue, _appTypes)
                    ElseIf action = "vcp" Then
                        _insertVisionRecord(DB, xParcelId, UserName, words(1), words(2), qcValue, eventDate, latitude, longitude, CC_YearStatus, _appTypes, words(3))
                    ElseIf action = "delete" Then
                        _deleteAuxRecord(xParcelId, qcValue, words(1), eventDate, _appTypes)
                    Else
                        Dim tableName As String = DB.GetStringValue("SELECT SourceTable FROM DataSourceField WHERE Id = " & fieldId)
                        tableName = DataSource.FindTargetTable(DB, tableName)
                        dataRec = DB.GetTopRow("SELECT * FROM " + tableName + " WHERE ClientROWUID = " & words(1))
                        If dataRec IsNot Nothing Then
                            If auxROWUID = 0 Then
                                auxROWUID = dataRec.GetInteger("ROWUID")
                            End If
                            If ParentROWUID = 0 Then
                                ParentROWUID = dataRec.GetInteger("ParentROWUID")
                            End If
                        Else
                            newRowid = auxROWUID
                        End If
                        Dim mapToSourceTable As String = field.GetString("MapToSourceTable")
                        If mapToSourceTable IsNot Nothing AndAlso mapToSourceTable <> String.Empty Then
                            Dim newValueRowuid As String
                            Dim TableandRowuid() As String
                            TableandRowuid = qcValue.Split("{")
                            If TableandRowuid.Length > 1 Then
                                newValueRowuid = TableandRowuid(1).Split("}")(0)
                            Else
                                newValueRowuid = qcValue
                            End If
                            If IsNumeric(newValueRowuid) AndAlso CLng(newValueRowuid) < 0 Then
                                Dim targetTables() As String
                                targetTables = mapToSourceTable.Split(";")
                                If targetTables.Length > 1 Then
                                    For Each targetsTable As String In targetTables
                                        Dim targetTable As String = DataSource.FindTargetTable(DB, targetsTable)
                                        If targetTable IsNot Nothing AndAlso targetTable <> String.Empty Then
                                            Dim sqlMapping As String = "SELECT ROWUID FROM " + targetTable + " WHERE ClientROWUID = " + newValueRowuid
                                            Dim replacedValue As String = DB.GetIntegerValue(sqlMapping)
                                            If replacedValue <> Nothing AndAlso replacedValue <> "0" AndAlso replacedValue.Length > 0 Then
                                                qcValue = "&%" + targetsTable + "{" + replacedValue + "}&%"
                                                Exit For
                                            End If
                                        End If
                                    Next
                                Else
                                    Dim targetsTable = targetTables(0)
                                    Dim targetTable As String = DataSource.FindTargetTable(DB, targetsTable)
                                    If targetTable IsNot Nothing AndAlso targetTable <> String.Empty Then
                                        Dim sqlMapping As String = "SELECT ROWUID FROM " + targetTable + " WHERE ClientROWUID = " + newValueRowuid
                                        Dim replacedValue As String = DB.GetIntegerValue(sqlMapping)
                                        qcValue = replacedValue
                                    End If
                                End If
                            End If
                        End If

                        e_("EXEC qc_ProcessParcelChange {0}, {1}, {2}, {3}, {4}, {5},{6},{7},{8}", True, UserName, parcelId, auxROWUID, fieldId, changeId, qcValue, ParentROWUID, isDTRCall(), _appTypes)
                    End If
                Next
                Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT GETUTCDATE() As UtcDate")
                If _appTypes = "DTR" Then
                    Database.Tenant.Execute("EXEC cc_doCustomChangeAction {0},{1},{2},{3},{4},0,0,0,@AppType='DTR'".SqlFormat(True, parcelId, UserName, "saveparcelchanges", DBNull.Value, dr.GetDate("UtcDate")))
                ElseIf _appTypes = "QC" Then
                    Database.Tenant.Execute("EXEC cc_doCustomChangeAction {0},{1},{2},{3},{4},0,0,0,@AppType='QC'".SqlFormat(True, parcelId, UserName, "saveparcelchanges", DBNull.Value, dr.GetDate("UtcDate")))
                End If

            End If

            Dim recoveryData = Request("recoveryData")
            If recoveryData IsNot Nothing Then
                Dim rLines = recoveryData.Split(vbLf)
                Dim rDlines As New List(Of String)
                Dim rLastLine As String = ""
                For Each rline In rLines
                    If rline.Trim <> "" Then
                        If rline.StartsWith("^^^") Then
                            If rLastLine <> "" Then
                                rDlines.Add(rLastLine)
                            End If
                            rLastLine = rline
                        Else
                            rLastLine = rLastLine + vbNewLine + rline
                        End If
                    End If
                Next
                If rLastLine <> "" Then
                    rDlines.Add(rLastLine)
                End If
                Dim auxRecordNum As Integer = -9999
                Dim sTable As String = ""
                For Each rline In rDlines
                    rline = rline.Substring(3)
                    Dim rwords As String() = rline.Split("|".ToCharArray, 5)
                    Dim auxROWUID, changeId As Integer, targetTable As String, recoverChildRecord As Boolean
                    auxROWUID = 0
                    targetTable = _findTargetTable(Database.Tenant, rwords(0))
                    Integer.TryParse(rwords(1), auxROWUID)
                    Integer.TryParse(rwords(2), changeId)
                    Integer.TryParse(rwords(3), recoverChildRecord)
                    If auxRecordNum = auxROWUID And targetTable = sTable Then
                        _recoverDeletedAux(auxROWUID, targetTable, changeId, parcelId, UserName, eventDate, rwords(0), False, recoverChildRecord)
                    Else
                        _recoverDeletedAux(auxROWUID, targetTable, changeId, parcelId, UserName, eventDate, rwords(0), True, recoverChildRecord)
                    End If
                    auxRecordNum = auxROWUID
                    sTable = targetTable
                Next
            End If
            Dim PhotoEdits = Request("activeParcelPhotoEditsdata")
            If PhotoEdits IsNot Nothing Then
                Dim rLines = PhotoEdits.Split(vbLf)
                Dim rDlines As New List(Of String)
                Dim rLastLine As String = ""
                For Each rline In rLines
                    If rline.Trim <> "" Then
                        If rline.StartsWith("^^^") Then
                            If rLastLine <> "" Then
                                rDlines.Add(rLastLine)
                            End If
                            rLastLine = rline
                        Else
                            rLastLine = rLastLine + vbNewLine + rline
                        End If
                    End If
                Next
                If rLastLine <> "" Then
                    rDlines.Add(rLastLine)
                End If
                For Each rline In rDlines
                    rline = rline.Substring(3)
                    Dim rwords As String() = rline.Split("|".ToCharArray, 5)
                    Dim photoid As Integer, DownSynced As Boolean, metaField As String, metaValue As String
                    '  auxROWUID = 0
                    'targetTable = _findTargetTable(Database.Tenant, rwords(0))
                    Integer.TryParse(rwords(0), parcelId)
                    Integer.TryParse(rwords(1), photoid)
                    If rwords.Length > 3 Then
                        metaField = rwords(2)
                        metaValue = rwords(3)
                        _UpdatePhotoMetaData(parcelId, photoid, metaField, metaValue, UserName)
                    Else
                        DownSynced = rwords(2)
                        _UpdatePhotoDownSynced(parcelId, photoid, DownSynced, UserName)
                    End If
                Next
            End If
            Dim deletedWorkflow As String = Request("deletedWorkflow")
            If deletedWorkflow IsNot Nothing Then
                Dim workflowcode As String = deletedWorkflow
                _DeleteDTRWorkflow(parcelId, workflowcode)
            End If
        End If

        If ParcelEventLog.Enabled Then
            ParcelEventLog.CreateFlagEvent(Database.Tenant, ParcelEventLog.EventType.QCModified, ParcelEventLog.ApplicationType.Console, parcelId, UserName)
        End If
        Database.Tenant.Execute("UPDATE Parcel SET Committed=0, StatusFlag = NULL WHERE Id={0}".SqlFormatString(parcelId))
        JSON.OK()
    End Sub

    Private Sub _performBulkAction()
        Try
            Dim priopage As Boolean = False
            If Request("pripage") Then
                priopage = True
            End If
            Dim result As DataTable = Database.Tenant.GetDataTable("EXEC qc_ExecBulkJob {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8},{9},{10},{11},{12},{13},{14}".SqlFormat(True, UserName, Request("query"), Request("operation"), Coalesce(Request("value1"), ""), Coalesce(Request("value2"), ""), Coalesce(Request("value3"), ""), Coalesce(Request("dtrfilter"), ""), Coalesce(Request("page"), ""), Coalesce(Request("pagesize"), ""), Request("PrioTableId"), Request("Method"), Coalesce(Request("CustomSearch"), ""), priopage, IIf(isDTRCall(), 1, 0), Coalesce(Request("clearAlertFlag"), "0")), 3000)
            If result.Rows.Count > 0 AndAlso result.Rows(0)("Records") <> Nothing Then
                JSON.Write(New With {.Record = result.Rows(0)("Records")})
            Else
                JSON.Write(New With {.Record = 0})
            End If
        Catch ex As Exception
            JSON.Error(ex)
        End Try

    End Sub

    Private Sub _saveSearchQuery()
        Dim availableQueries As Integer = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM QC_SavedQueries WHERE LoginId = " + SqlUserName)
        If availableQueries >= 25 Then
            JSON.Error("You have reached the maximum saved queries limit. You may delete some of the existing queries to create room for new.")
            Return
        End If

        Dim qid As Integer = Request.GetIntegerOrInvalid("QueryId", 0)
        Dim name As String = Request("Name")
        Dim op As String = Request("Operator")
        ' Dim filterData As String = Request("FilterData")
        Dim filterData As String = Request("FilterData")
        Dim duplicates As Integer = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM QC_SavedQueries WHERE LoginId = {0} AND QueryName = {1} AND Id <> {2}".SqlFormatString(UserName, name, qid))
        If duplicates > 0 Then
            JSON.Error("Another query exists with the same name. Please enter a different name.")
            Return
        End If
        Dim filters = filterData.Split("^").Select(Function(x)
                                                       Dim sp = x.Split("|")
                                                       Return New With {
                                                            .Label = sp(0),
                                                            .Field = sp(1),
                                                            .Operator = sp(2),
                                                            .Value1 = sp(3),
                                                            .Value2 = sp(4)
                                                       }
                                                   End Function)
        Dim filterSql As String = "INSERT INTO QC_QueryFilters (QueryId, Label, Field, Operator, Value1, Value2) VALUES ({0}, {1}, {2}, {3}, {4}, {5});"
        If qid = 0 Then
            qid = Database.Tenant.GetIdentityFromInsert("INSERT INTO QC_SavedQueries (LoginId, QueryName,IsCustomSearch,MainOperator) VALUES ({0}, {1}, {2}, {3})".SqlFormatString(UserName, name, 0, op))
            For Each f In filters
                e_(filterSql, False, qid, f.Label, f.Field, f.Operator, f.Value1, f.Value2)
            Next
        Else
            e_("UPDATE QC_SavedQueries SET QueryName = {1} WHERE Id = {0}", False, qid, name)
        End If
        JSON.OK()
    End Sub

    Private Sub _saveCustomQuery()
        Dim availableQueries As Integer = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM QC_SavedQueries WHERE LoginId = " + SqlUserName)
        If availableQueries >= 25 Then
            JSON.Error("You have reached the maximum saved queries limit. You may delete some of the existing queries to create room for new.")
            Return
        End If

        Dim qid As Integer = Request.GetIntegerOrInvalid("QueryId", 0)
        Dim name As String = Request("Name")
        Dim filterData As String = Request("FilterData")

        'Dim JoinData As String = Request("JoinStr")
        'Dim ConditionData As String = Request("CondStr")
        Dim duplicates As Integer = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM QC_SavedQueries WHERE LoginId = {0} AND QueryName = {1} AND Id <> {2}".SqlFormatString(UserName, name, qid))
        If duplicates > 0 Then
            JSON.Error("Another query exists with the same name. Please enter a different name.")
            Return
        End If
        Dim filters As String() = filterData.Split("^")

        Dim filterSql As String = "INSERT INTO QC_QueryFilters (QueryId, JoinString, ConditionString,FilterData) VALUES ({0}, {1}, {2},{3});"
        If qid = 0 Then
            qid = Database.Tenant.GetIdentityFromInsert("INSERT INTO QC_SavedQueries (LoginId, QueryName, IsCustomSearch) VALUES ({0}, {1}, {2})".SqlFormatString(UserName, name, 1))
            e_(filterSql, False, qid, filters(0), filters(1), filters(2))
            JSON.Write(New With {.status = "OK", .Id = qid})
        Else
            e_("UPDATE QC_SavedQueries SET QueryName = {1} WHERE Id = {0}", False, qid, name)
            e_("UPDATE QC_QueryFilters SET JoinString = {0},ConditionString = {1},FilterData = {2} WHERE QueryId = {3}", False, filters(0), filters(1), filters(2), qid)
            JSON.OK()
        End If
    End Sub

    Private Sub _getCustomQueryData()
        Dim qid As String = Request("QueryId")
        Dim id As Integer
        If Integer.TryParse(qid, id) Then
            Dim dt As DataTable = Database.Tenant.GetDataTable("select JoinString,ConditionString,FilterData from QC_QueryFilters where QueryId = " & id)
            JSON.Write(New With {.status = "OK", .joinStr = dt(0).GetString("JoinString"), .Condition = dt(0).GetString("ConditionString"), .filter = dt(0).GetString("FilterData")})
        Else
            JSON.Error("Custom query not found.")
        End If
    End Sub


    Private Sub _getSearchQueries()
        Dim qs As DataTable = Database.Tenant.GetDataTable("SELECT * FROM QC_SavedQueries WHERE LoginId = " + SqlUserName)
        Dim queries = qs.AsEnumerable.Select(Function(x) New SearchQuery With {.Id = x.GetInteger("Id"), .Name = x.GetString("QueryName"), .IsCustom = x.GetBoolean("IsCustomSearch"), .MainOp = x.GetString("MainOperator")}).ToArray

        For Each q In queries
            Dim ff = d_("SELECT * FROM QC_QueryFilters WHERE QueryId = " & q.Id & " ORDER BY Id")
            q.Filters = ff.AsEnumerable.Select(Function(x) New SearchFilter With {
                                       .Label = x.GetString("Label"),
                                       .Field = x.GetString("Field"),
                                       .Operator = x.GetString("Operator"),
                                       .Value1 = x.GetString("Value1"),
                                       .Value2 = x.GetString("Value2")
                                   }).ToArray
        Next

        JSON.Write(New With {.status = "OK", .data = queries.ToArray})
    End Sub

    Private Sub _deleteSearchQuery()
        Dim qid As Integer = Request.GetIntegerOrInvalid("QueryId", 0)
        e_("DELETE FROM QC_QueryFilters WHERE QueryId = " & qid)
        e_("DELETE FROM QC_SavedQueries WHERE Id = " & qid)
        JSON.OK()
    End Sub

    Private Sub _saveSketchChanges()

        Dim latitude As Single = "34.445"
        Dim longitude As Single = "56.7889"
        Dim parcelId As Integer = 0
        Dim eventDate As Date = Date.UtcNow
        Dim _appType As String = Request("appType")
        Dim _appTypes As String
        If Integer.TryParse(Request("ParcelId"), parcelId) Then
            Dim sketchData = Request("Data")

            If (_appType = "sv") Then
                _appTypes = "SV"
            ElseIf (isDTRCall() = True) Then
                _appTypes = "DTR"
            Else
                _appTypes = "QC"
            End If

            If sketchData IsNot Nothing Then
                Dim datas = sketchData.Split("#")
                For Each data As String In datas
                    If data.Trim <> "" Then
                        Dim lines = data.Split(vbLf)
                        For Each line As String In lines
                            If line.Trim <> "" Then
                                Dim parts As String() = line.Split("|")
                                If parts.Length = 5 Then
                                    Dim rowUID As String = parts(0)
                                    Dim parent As String = parts(1)
                                    Dim fieldIds As String = System.Text.Encoding.Default.GetString(Convert.FromBase64String(parts(2)))
                                    Dim fieldData As String = System.Text.Encoding.Default.GetString(Convert.FromBase64String(parts(3)))
                                    Dim CC_YearStatus As String = parts(4)
                                    Dim errorMessage As String = ""
                                    If parent.Contains("/") Then
                                        Dim fieldParts = fieldIds.Split("|")
                                        Dim sketchVectorTable As String
                                        Dim fields As DataTable = DB.GetDataTable("SELECT * FROM DataSourceField WHERE Id IN " + String.Join(", ", fieldParts).Wrap("()"))
                                        sketchVectorTable = fields(0).GetString("SourceTable")
                                        Dim parcelRowuid As Long = DB.GetIntegerValue("SELECT ROWUID FROM ParcelData WHERE CC_ParcelId = " + parcelId.ToString)
                                        parent = parent.Split("/")(0).ToString
                                        'If parent.Contains("-") Then
                                        '    parent = parent.Remove(0, parent.IndexOf("-") + 1)
                                        'End If
                                        If (parent = parcelRowuid) Then
                                            parent = "0"
                                        End If
                                        _insertNewRecord(DB, parcelId, UserName, rowUID, parent, sketchVectorTable, DateTime.UtcNow, latitude, longitude, CC_YearStatus, _appTypes)
                                    End If
                                    SketchDataProcessor.UpdateSketch(Database.Tenant, parcelId, UserName, rowUID, fieldIds, fieldData, DateTime.UtcNow, 0, 0, errorMessage, "", _appTypes)
                                    ParcelAuditStream.CreateFlagEvent(DB, eventDate, parcelId, UserName, _appTypes + ": Sketch vector updated for Aux Record #" + rowUID.ToString() + ".", _appTypes)
                                End If

                            End If
                        Next
                    End If
                Next
                'Dim dlines As New List(Of String)
                'Dim lastLine As String = ""
                'For Each line In lines
                '    If line.Trim <> "" Then
                '        If line.StartsWith("^^^") Then
                '            If lastLine <> "" Then
                '                dlines.Add(lastLine)
                '            End If
                '            lastLine = line
                '        Else
                '            lastLine = lastLine + vbNewLine + line
                '        End If
                '    End If
                'Next
                'If lastLine <> "" Then
                '    dlines.Add(lastLine)
                'End If
                'For Each line In dlines
                '    line = line.Substring(3)
                '    Dim words As String() = line.Split("|".ToCharArray, 4)
                '    Dim area, label, vector As String, uid As Integer
                '    uid = 0
                '    area = words(0)
                '    label = words(1)
                '    Integer.TryParse(words(2), uid)
                '    vector = words(3)

                '    'function is here
                '    _updateSketchArea(Database.Tenant, parcelId, UserName, uid, area, DateTime.UtcNow, 0, 0)
                '    _updateSketchCommand(Database.Tenant, parcelId, UserName, uid, vector, DateTime.UtcNow, 0, 0)
                'Next
            End If
            Dim cc_error As String = Request("CC_Error")
            Dim isCC_Error As Boolean = IIf(Request("isCC_Error").ToString() = "1", True, False)
            If cc_error <> "" And cc_error <> Nothing Then
                Database.Tenant.Execute("UPDATE Parcel SET CC_Error = '" & cc_error & "' WHERE Id = " & parcelId)
            ElseIf isCC_Error Then
                Database.Tenant.Execute("UPDATE Parcel SET CC_Error = NULL WHERE Id = " & parcelId)
            End If
            Dim auditTrailEntry As String = Request("auditTrailEntry")
            If auditTrailEntry <> Nothing AndAlso auditTrailEntry <> "" Then
                ParcelAuditStream.CreateFlagEvent(DB, eventDate, parcelId, UserName, auditTrailEntry, _appTypes)
            End If

        End If
        JSON.OK()

    End Sub


    Private Sub _updateSketchArea(db As Database, parcelId As Integer, loginId As String, rowUID As Long, sketchArea As String, eventDate As Date, latitude As Single, longitude As Single)
        Dim sql As String
        Dim lockType As String = "ROWLOCK"
        Dim changeAction As String = "edit"
        Dim fieldName As String = IIf(ClientSettings.PropertyValue("SketchAreaField") IsNot Nothing, ClientSettings.PropertyValue("SketchAreaField"), ProcessParcelDataChange.SketchSettingsPropertyValue("SketchAreaField"))
        If fieldName = "" Then
            Return
        End If
        Dim sketchVectorTable = IIf(ClientSettings.PropertyValue("SketchVectorTable") IsNot Nothing, ClientSettings.PropertyValue("SketchVectorTable"), ProcessParcelDataChange.SketchSettingsPropertyValue("SketchVectorTable"))
        Dim sketchTargetTable = _findTargetTable(db, sketchVectorTable)
        Dim oldValue As String = "", newValue As String = ""
        Dim fieldId As Integer = db.GetIntegerValue("SELECT Id FROM DataSourceField WHERE Name = " + fieldName.ToSqlValue + " AND SourceTable = '" + sketchVectorTable + "'")
        Dim parentRowUID As String

        If rowUID < 0 Then
            rowUID = db.GetStringValue("SELECT ROWUID FROM  " + sketchTargetTable + "  WHERE ClientROWUID = " + rowUID.ToString)
            changeAction = "new"
        End If

        If rowUID > 0 Then
            oldValue = db.GetStringValue("SELECT " + fieldName + " FROM  " + sketchTargetTable + "  WHERE ROWUID = " + rowUID.ToString)
            parentRowUID = db.GetStringValue("SELECT ParentROWUID FROM  " + sketchTargetTable + "  WHERE ROWUID = " + rowUID.ToString)
            sql = "UPDATE  " + sketchTargetTable + " SET " + fieldName + " = " + sketchArea.ToSqlValue + " WHERE ROWUID = " + rowUID.ToString
        Else
            Throw New Exception("Attempt to update sketch data to a non-existent record.")
        End If
        Try
            db.Execute(sql)

            Dim changeSql As String = "INSERT INTO ParcelChanges (ParcelId, FieldId, Action, ReviewedBy, OriginalValue, NewValue, ChangedTime, Latitude, Longitude, AuxROWUID, ParentAuxROWUID) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}); SELECT CAST(@@IDENTITY AS INT) As NewId;"
            Dim changeId As Integer = db.GetIntegerValue(changeSql.SqlFormat(True, parcelId, fieldId, changeAction, loginId, oldValue, sketchArea, eventDate, latitude, longitude, rowUID, parentRowUID))
            db.Execute("EXEC processFieldChangesTracking {0},{1},{2},{3}".SqlFormat(True, parcelId, fieldId, changeId, sketchArea))
            'Return "Sketch vector updated."
        Catch ex As Exception
            ErrorMailer.ReportException("MA-Sync", ex, System.Web.HttpContext.Current.Request)

            'Return "Sketch vector update failed."
        End Try
    End Sub

    Private Sub _updateSketchCommand(db As Database, parcelId As Integer, loginId As String, rowUID As Long, sketchVector As String, eventDate As Date, latitude As Single, longitude As Single)
        Dim lockType As String = "ROWLOCK"
        Dim sql As String
        Dim changeAction As String = "edit"
        Dim fieldName As String = IIf(ClientSettings.PropertyValue("SketchVectorCommandField") IsNot Nothing, ClientSettings.PropertyValue("SketchVectorCommandField"), ProcessParcelDataChange.SketchSettingsPropertyValue("SketchVectorCommandField"))
        Dim sketchVectorTable = IIf(ClientSettings.PropertyValue("SketchVectorTable") IsNot Nothing, ClientSettings.PropertyValue("SketchVectorTable"), ProcessParcelDataChange.SketchSettingsPropertyValue("SketchVectorTable"))
        Dim sketchVectorTargetTable = _findTargetTable(db, sketchVectorTable)
        Dim oldValue As String = "", newValue As String = ""
        Dim fieldId As Integer = db.GetIntegerValue("SELECT Id FROM DataSourceField WHERE Name = " + fieldName.ToSqlValue + " AND SourceTable = '" + sketchVectorTable + "'")
        Dim parentRowUID As String

        If rowUID < 0 Then
            rowUID = db.GetStringValue("SELECT ROWUID FROM  " + sketchVectorTargetTable + "  WHERE ClientROWUID = " + rowUID.ToString)
            changeAction = "new"
        End If

        If rowUID > 0 Then
            oldValue = db.GetStringValue("SELECT " + fieldName + " FROM  " + sketchVectorTargetTable + "  WHERE ROWUID = " + rowUID.ToString)
            parentRowUID = db.GetStringValue("SELECT ParentROWUID FROM  " + sketchVectorTargetTable + "  WHERE ROWUID = " + rowUID.ToString)
            sql = "UPDATE  " + sketchVectorTargetTable + " SET " + fieldName + " = " + sketchVector.ToSqlValue + " WHERE ROWUID = " + rowUID.ToString
        Else
            Throw New Exception("Attempt to update sketch data to a non-existent record.")
        End If
        Try
            db.Execute(sql)

            Dim changeSql As String = "INSERT INTO ParcelChanges (ParcelId, FieldId, Action, ReviewedBy, OriginalValue, NewValue, ChangedTime, Latitude, Longitude, AuxROWUID, ParentAuxROWUID) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}); SELECT CAST(@@IDENTITY AS INT) As NewId;"
            Dim changeId As Integer = db.GetIntegerValue(changeSql.SqlFormat(True, parcelId, fieldId, changeAction, loginId, oldValue, sketchVector, eventDate, latitude, longitude, rowUID, parentRowUID))
            db.Execute("EXEC processFieldChangesTracking {0},{1},{2},{3}".SqlFormat(True, parcelId, fieldId, changeId, sketchVector))
        Catch ex As Exception
            ErrorMailer.ReportException("MA-Sync", ex, System.Web.HttpContext.Current.Request)
        End Try
    End Sub

    Public Shared Function _findTargetTable(ByVal db As Database, ByVal tableName As String, Optional ByRef targetTableOrginalName As String = "") As String
        Dim sql As String = "SELECT t2.Name FROM DataSourceRelationships r INNER JOIN DataSourceTable t1 ON r.TableId = t1.Id AND r.Relationship = 0 INNER JOIN DataSourceTable t2 ON r.ParentTableId = t2.Id WHERE t1.Name = {0}"
        Dim parentTable As String = "", targetTable As String = tableName
        While True
            parentTable = db.GetStringValue(sql.SqlFormatString(targetTable))
            If parentTable = "" Then
                Exit While
            Else
                targetTable = parentTable
            End If
        End While

        targetTableOrginalName = targetTable

        If targetTable.Trim.ToLower = db.Application.ParcelTable.Trim.ToLower Then
            Return "ParcelData"
        ElseIf targetTable.Trim.ToLower = db.Application.ParcelTable.Trim.ToLower Then
            Return "NeighborhoodData"
        ElseIf targetTable <> String.Empty Then
            Return "XT_" + targetTable
        Else
            Return String.Empty
        End If
    End Function

    Private Sub _loadDatasourceFields()

        Try

            Dim DFields As DataTable = Database.Tenant.GetDataTable("select Id,Name,IsRequired,SourceTable from DataSourceField where  CategoryId in(select id from FieldCategory)")
            If (DFields.Rows.Count > 0) Then
                Dim jstr As String = JSON.GetTableAsJSON(DFields)
                JSON.WriteString(jstr)
            Else
                JSON.Error("There is no Datasource fields")
            End If

        Catch ex As Exception
            ' write Exception code here
            JSON.Error(ex.Message)
        End Try
    End Sub

    Private Sub _loadValidations()

        Try

            Dim Validation As DataTable = Database.Tenant.GetDataTable("SELECT * FROM ClientValidation")
            If (Validation.Rows.Count > 0) Then
                Dim jstr As String = JSON.GetTableAsJSON(Validation)
                JSON.WriteString(jstr)
            Else
                JSON.Error("There is no Active validations")
            End If

        Catch ex As Exception
            ' write Exception code here
            JSON.Error(ex.Message)
        End Try
    End Sub

    Private Sub _updateForceStatus()

        Try
            Dim isDtr = Request("dtr")
            Dim updateIds As String = ""
            Dim dt As DataTable = Database.System.GetDataTable("select f.* from ForceUpdateApplicationStatus f join ForceUpdateApplication p on p.Id = f.UpdateId where f.orgId = " + HttpContext.Current.GetCAMASession.OrganizationId.ToString() + " and p.UpdateType='Update Application/Update System Data' and " + IIf(isDtr = "1", "f.dtrstatus", "f.qcstatus") + " = 0")
            If (dt.Rows.Count > 0) Then
                For Each dr As DataRow In dt.Rows
                    updateIds = updateIds + dr.GetString("UpdateId") + ","
                Next
                updateIds = updateIds.Remove(updateIds.Length - 1)
                Database.System.Execute("UPDATE ForceUpdateApplicationStatus SET  " + IIf(isDtr = "1", "dtrstatus", "qcstatus") + " = 1, " + IIf(isDtr = "1", "DTRUpdateDate", "QCUpdateDate") + " = GETUTCDATE() where orgId = " + HttpContext.Current.GetCAMASession.OrganizationId.ToString() + " and UpdateId in (" + updateIds + ")")
            End If
            JSON.OK()
        Catch ex As Exception
            JSON.Error(ex.Message)
        End Try
    End Sub

    Private Sub _recoverDeletedAux(rowID As Integer, tableName As String, changeId As Integer, parcelId As Integer, username As String, eventDate As Date, sourceTable As String, auditReq As Boolean, recoverChildRec As Boolean)
        Dim _appTypes As String = IIf(isDTRCall() = True, "DTR", "QC")
        Try
            If recoverChildRec = True Then
                __recoverChildRecords(DB, rowID, sourceTable, parcelId)
            End If
            Database.Tenant.Execute("UPDATE " + tableName + " SET CC_Deleted=0 WHERE ROWUID={0}".SqlFormatString(rowID))
            Database.Tenant.Execute("DELETE FROM ParcelChanges WHERE Id={0}".SqlFormatString(changeId))
            Database.Tenant.Execute("DELETE pc FROM ParcelChanges pc LEFT OUTER JOIN DataSourceField ds on ds.Id=pc.FieldId INNER JOIN DataSourceTableAction da ON da.tableid=ds.TableId AND da.fieldName=ds.Name WHERE parcelid={0} AND da.Action ='D' AND pc.auxrowuid= {1} AND ds.SourceTable = {2}".SqlFormatString(parcelId, rowID, sourceTable))
            If auditReq Then
                If (_appTypes = "DTR") Then
                    ParcelAuditStream.CreateFlagEvent(DB, eventDate, parcelId, username, "1 deleted record, Aux Record #" + rowID.ToString() + ",  recovered by DTR in " + sourceTable, _appTypes)
                Else
                    ParcelAuditStream.CreateFlagEvent(DB, eventDate, parcelId, username, "1 deleted record, Aux Record #" + rowID.ToString() + ",  recovered by QC in " + sourceTable, _appTypes)
                End If
            End If
            Return
        Catch ex As Exception
            JSON.Error(ex.Message)
        End Try
    End Sub

    Private Sub _UpdatePhotoDownSynced(parcelID As Integer, PhotoID As Integer, DownSynced As Boolean, LoginId As String)
        Try
            Dim eventDescription As String
            Dim refId As String = Database.Tenant.GetStringValue("SELECT ReferenceId FROM ParcelImages WHERE Id=" + PhotoID.ToString())
            If IsNothing(refId) Or refId = "" Then
                eventDescription = "(Photo " + PhotoID.ToString() + " has been flagged for re-download)"
            Else
                eventDescription = "Photo " + PhotoID.ToString() + " ( Ref# " + refId + ") has been flagged for re-download."
            End If
            Database.Tenant.Execute("INSERT INTO ParcelAuditTrail (EventDate, EventTime, ParcelId, LoginID, EventType, Description) SELECT GETUTCDATE(), GETUTCDATE(), ParcelId, NULL, 0,{0} As PhotoCount FROM ParcelImages WHERE parcelID={1} AND id={2}".SqlFormatString(eventDescription, parcelID, PhotoID))
            Database.Tenant.Execute("UPDATE ParcelImages SET DownSynced={0} WHERE parcelID={1} AND id={2}".SqlFormatString(DownSynced, parcelID, PhotoID))
            Return
        Catch ex As Exception
            JSON.Error(ex.Message)
        End Try
    End Sub

    Private Sub _UpdatePhotoMetaData(parcelID As Integer, PhotoID As Integer, metaFieldLabel As String, metaValue As String, LoginId As String)
        Try
            Dim dr As DataRow = DB.GetTopRow("SELECT * FROM ParcelImages where Id={0}".SqlFormatString(PhotoID))
            If dr Is Nothing Then
                Exit Sub
            End If
            Dim DownSynced = dr.GetString("downsynced")
            If dr IsNot Nothing Then
                Dim sql As String = ""
                Dim filter = "Id = {0} AND parcelId = {1}".SqlFormatString(PhotoID, parcelID)
                If DB.GetIntegerValue("SELECT COUNT(*) FROM ClientSettings WHERE Name = 'PhotoNoDownsyncOnFlagging' AND (Value = '1' OR LOWER(Value) = 'true')") > 0 And (DownSynced = "1" Or DownSynced.ToLower = "true") Then
                    sql = "UPDATE ParcelImages SET UploadedBy = " + LoginId.ToSqlValue + ", " + metaFieldLabel + " = " + metaValue.ToSqlValue + " WHERE " + filter
                    If metaValue = "null" Then
                        sql = "UPDATE ParcelImages SET UploadedBy = " + LoginId.ToSqlValue + ", " + metaFieldLabel + " =  NULL WHERE " + filter
                    End If
                Else
                    sql = "UPDATE ParcelImages SET UploadedBy = " + LoginId.ToSqlValue + ", " + metaFieldLabel + " = " + metaValue.ToSqlValue + ", DownSynced = 0 WHERE " + filter
                    If metaValue = "null" Then
                        sql = "UPDATE ParcelImages SET UploadedBy = " + LoginId.ToSqlValue + ", " + metaFieldLabel + " =  NULL , DownSynced = 0 WHERE " + filter
                    End If
                End If
                DB.Execute(sql)
                Dim _appTypes As String = IIf(isDTRCall() = True, "DTR", "QC")
                Dim ndr As DataRow = DB.GetTopRow("SELECT Name, DoNotIncludeInAuditTrail FROM DataSourceField WHERE AssignedName ={0} ".SqlFormatString(metaFieldLabel.Replace("MetaData", "PhotoMetaField")))
                Dim metaFieldName = ndr.GetString("Name")
                Dim DoNotAuditTrail As Integer = IIf(ndr.GetString("DoNotIncludeInAuditTrail") = "True", 1, 0)
                Dim eventDescription As String
                If (_appTypes = "DTR") Then
                    eventDescription = "DTR: Data Modified - Photo." + metaFieldName + " Changed to " + metaValue + " for image #" + PhotoID.ToString() + "."
                    If metaValue = "null" Then
                        eventDescription = "DTR: Data Modified - Photo." + metaFieldName + " Changed to  for image #" + PhotoID.ToString() + "."
                    End If
                Else
                    eventDescription = "QC: Data Modified - Photo." + metaFieldName + " Changed to " + metaValue + " for image #" + PhotoID.ToString() + "."
                    If metaValue = "null" Then
                        eventDescription = "QC: Data Modified - Photo." + metaFieldName + " Changed to  for image #" + PhotoID.ToString() + "."
                    End If
                End If
                Database.Tenant.Execute("INSERT INTO ParcelAuditTrail (EventDate, EventTime, ParcelId, LoginID, EventType, Description, ApplicationType, DoNotInsertInAuditTrail) SELECT GETUTCDATE(), GETUTCDATE(), ParcelId, {0}, 0, {1}, {4}, {5} FROM ParcelImages WHERE parcelID={2} AND id={3}".SqlFormatString(LoginId, eventDescription, parcelID, PhotoID, _appTypes, DoNotAuditTrail))
            End If
            Return
        Catch ex As Exception
            JSON.Error(ex.Message)
        End Try
    End Sub

    Private Sub _loadFieldCategories()

        Try

            Dim fc As DataTable = BusinessLogic.FieldCategory.GetFieldCategories
            If (fc.Rows.Count > 0) Then
                Dim jstr As String = JSON.GetTableAsJSON(fc)
                JSON.WriteString(jstr)
            Else
                JSON.Error("There is no fieldCategories")
            End If

        Catch ex As Exception
            ' write Exception code here
            JSON.Error(ex.Message)
        End Try
    End Sub
    Private Sub _loadClientSettings()

        Try

            Dim settings As DataTable = Database.Tenant.GetDataTable("SELECT * FROM ClientSettings")
            If (settings.Rows.Count > 0) Then
                Dim jstr As String = JSON.GetTableAsJSON(settings)
                JSON.WriteString(jstr)
            Else
                JSON.Error("There is no Client Settings")
            End If

        Catch ex As Exception
            ' write Exception code here
            JSON.Error(ex.Message)
        End Try
    End Sub

    Private Sub _lastUpdatedTime()
        Dim value As String = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name = 'LastSchemaUpdatedTime'")
        JSON.OK(value)
    End Sub

    Private Sub _lastupdatedCounter()
        Dim value As String = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name = 'LastSchemaUpdatedCounter'")
        JSON.OK(value)
    End Sub

    Private Sub _deleteAuxRecord(parcelId As Integer, sourceTable As String, auxRowUID As Long, eventDate As DateTime, Optional ByVal _appTypes As String = "")
        'Dim parcelId = Request("ParcelId")
        'Dim sourceTable = Request("SourceTable")
        'Dim auxROWUID = Request("ROWUID")
        'vineesh
        'If auxRowUID = "NULL" Or auxRowUID = "" Then
        '    auxRowUID = 0
        'End If
        If (_appTypes = "") Then
            _appTypes = IIf(isDTRCall() = True, "DTR", "QC")
        End If

        Try
            QCProcessor.DeleteRecord(Database.Tenant, parcelId, UserName, auxRowUID, sourceTable, 0, 0, eventDate, _appTypes)
            ' JSON.OK()
        Catch ex As Exception
            JSON.Error(ex)
        End Try

    End Sub

    Private Sub _setgridcolumns()
        Dim words As String = Request("line")
        Database.Tenant.Execute("EXEC qc_SetGridColumns @line={0},@DTR = {1}".SqlFormatString(words, isDTRCall()))
    End Sub

    Private Sub _insertNewRecord(db As Database, parcelId As Integer, loginId As String, clientROWUID As Long, clientParentROWUID As String, tableName As String, eventDate As Date, latitude As Single, longitude As Single, Optional ByVal CC_YearStatus As String = "", Optional ByVal _appTypes As String = "")
        'If clientROWUID = "NULL" Or clientROWUID = "" Then
        '    clientROWUID = 0
        'End If
        Dim insertFields = ""
        Dim selectFields = ""
        Dim newId As Integer
        Dim sourceTable = tableName
        Dim parentTableName As String = ""
        'Dim IFID = db.GetIntegerValue("SELECT MIN(Id) FROM DataSourceField WHERE SourceTable ={0}".SqlFormatString(tableName))
        Dim IFID = db.GetIntegerValue("SELECT MIN(df.Id) FROM DataSourceField df LEFT OUTER JOIN DataSourceKeys dk ON df.Id = dk.FieldId WHERE SourceTable={0} AND IsPrimaryKey IS NULL AND IsReferenceKey IS NULL AND ( ExclusiveForSS = 0 OR ExclusiveForSS IS NULL) ".SqlFormatString(tableName))
        'If no Then non PK field Is there While creating a record.generate PCI for the first PK field in order to avoid downsync failure
        If IFID = 0 Then
            IFID = db.GetIntegerValue("SELECT MIN(Id) FROM DataSourceField  WHERE SourceTable={0} AND ( ExclusiveForSS = 0 OR ExclusiveForSS IS NULL) ".SqlFormatString(tableName))
        End If
        Dim tableId As Integer = db.GetIntegerValueOrInvalid("SELECT Id FROM DataSourceTable WHERE Name = " + tableName.ToSqlValue)
        Dim fieldName = db.GetStringValue("SELECT Name FROM DataSourceField WHERE id ={0}".SqlFormatString(IFID))

        If (_appTypes = "") Then
            _appTypes = IIf(isDTRCall() = True, "DTR", "QC")
        End If

        Dim getSql = "SELECT pt.Name, tf.Name As TableField, pf.Name As ReferenceField FROM DataSourceRelationships r LEFT OUTER JOIN DataSourceKeys k ON k.RelationshipId = r.Id LEFT OUTER JOIN DataSourceField tf ON k.FieldId = tf.Id LEFT OUTER JOIN DataSourceField pf ON k.ReferenceFieldId = pf.Id 	LEFT OUTER JOIN DataSourceTable tt ON r.TableId = tt.Id LEFT OUTER JOIN DataSourceTable pt ON r.ParentTableId = pt.Id WHERE tt.Name ={0} AND Relationship IS NOT NULL AND Relationship <> 0 AND k.IsReferenceKey = 1".SqlFormatString(tableName)
        Dim dt As DataTable = db.GetDataTable(getSql)

        For Each dr As DataRow In dt.Rows
            sourceTable = dr.GetString("Name")
            insertFields = insertFields + "," + dr.GetString("TableField")
            selectFields = selectFields + "," + dr.GetString("ReferenceField")
        Next

        Dim tempParentTable = sourceTable
        Dim coParentTable = DataSource.FindTargetTable(db, tempParentTable)

        'Protection against ghost records:
        'While syncing changes from MA, if internet connection is lost so that the response is lost, MA will resend the same data - If there were
        'inserts, they would re-execute.
        Dim checkSql As String = "SELECT COUNT(*) FROM XT_" + tableName + " WHERE ClientROWUID = '" + clientROWUID.ToString() + "'"
        If db.GetIntegerValue(checkSql) > 0 Then
            Exit Sub
        End If

        Dim parentROWUID As Integer = 0
        If clientParentROWUID = 0 Then
            Dim insertSql = "INSERT INTO XT_" + tableName + "   (CC_ParcelId, ClientROWUID, CC_YearStatus " + insertFields + " ) SELECT '" + parcelId.ToString + "','" + clientROWUID.ToString() + "','" + CC_YearStatus + "' " + selectFields + "  FROM   " + coParentTable + " WHERE CC_ParcelId ={0}; SELECT CAST(@@IDENTITY  AS INT);".SqlFormatString(parcelId)
            newId = db.GetIntegerValue(insertSql)
        Else
            Dim dtValues = db.GetDataTable("SELECT p.Name,r.Id 	FROM DataSourceRelationships r INNER JOIN DataSourceTable t ON r.TableId = t.Id INNER JOIN DataSourceTable p ON r.ParentTableId = p.Id 	WHERE t.Name = {0}".SqlFormatString(tableName))
            parentTableName = dtValues.Rows(0).GetString("Name")
            If clientParentROWUID > 0 Then
                parentROWUID = clientParentROWUID
            Else
                If (dtValues.Rows.Count > 0) Then
                    Dim relationshipId = dtValues.Rows(0).GetInteger("Id")
                    parentROWUID = db.GetIntegerValue("SELECT ROWUID FROM XT_" + parentTableName + " WHERE ClientROWUID={0}".SqlFormatString(clientParentROWUID.ToString()))
                End If
            End If

            If parentROWUID > 0 Then
                Dim insertString = "INSERT INTO XT_" + tableName + "    (CC_ParcelId, ParentROWUID, ClientROWUID, ClientParentROWUID, CC_YearStatus " + insertFields + ")  SELECT '" + parcelId.ToString() + "','" + parentROWUID.ToString() + "','" + clientROWUID.ToString() + "','" + clientParentROWUID.ToString() + "','" + CC_YearStatus + "' " + selectFields + " FROM " + coParentTable + "  WHERE ROWUID={0};SELECT CAST(@@IDENTITY  AS INT)".SqlFormatString(parentROWUID.ToString())
                newId = db.GetIntegerValue(insertString)
            End If
        End If
        If newId > 0 Then
            If ParcelEventLog.Enabled Then
                ParcelEventLog.CreateTableEvent(db, ParcelEventLog.EventType.RowAdded, ParcelEventLog.ApplicationType.Console, parcelId, tableId, newId)
            End If
            MobileSyncUploadProcessor.insertNewRecordDetails(db, parcelId, newId, tableName, clientROWUID, eventDate, loginId)
            'Dim newValue = ""  ---   code changed to make same as MA
            'Dim selectSql = "SELECT  " + fieldName + "  FROM  XT_" + tableName + " WHERE ROWUID={0}".SqlFormatString(newId)
            'newValue = db.GetStringValue(selectSql)
            Dim isQCRes As DataRow = db.GetTopRow("SELECT top 1 QC FROM Parcel WHERE Id = {0}".SqlFormat(True, parcelId)) 'Updating qcapproved and qc checked values for correctly showing change colour in QC. If parcel is approved and record created from sketch it's value is 0 not chnaged to 1. So it shows in red colour not in green
            Dim isQC As Integer = If(Not IsDBNull(isQCRes("QC")) AndAlso isQCRes("QC") = True, 1, 0)
            Dim sql As String = "INSERT INTO ParcelChanges  (ParcelId, FieldId, Action, ReviewedBy, OriginalValue, NewValue, ChangedTime, Latitude, Longitude, AuxROWUID, ParentAuxROWUID, QCChecked, QCApproved) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}); SELECT CAST(@@IDENTITY AS INT) As NewId;"
            Dim changeId As Integer = db.GetIntegerValue(sql.SqlFormat(True, parcelId, IFID, "new", loginId, "", "", eventDate, latitude, longitude, newId, parentROWUID.ToString.TrimStart("0"), isQC, isQC))
            db.Execute("EXEC processFieldChangesTracking {0},{1},{2},{3}".SqlFormat(True, parcelId, IFID, changeId, ""))
            '_processParcelDataChange(loginId, eventDate, parcelId, newId, IFID, Nothing, Nothing, latitude, longitude, "new")
            Dim autonumberfields As DataTable = db.GetDataTable("select * from datasourcefield where sourcetable={0} and autonumber=1".SqlFormatString(tableName))
            Dim sibling As DataTable
            If autonumberfields.Rows.Count > 0 Then
                If clientParentROWUID = 0 Then
                    sibling = db.GetDataTable("select * from XT_" + tableName + " where CC_ParcelId=" + SqlFormatString(parcelId))
                Else
                    If (parentTableName <> "") Then
                        sibling = db.GetDataTable("EXEC cc_GetChildROWUIDs {0}, {1}, {2},{3}".SqlFormatString(parentTableName, tableName, parentROWUID, 1))
                    End If
                End If
            End If
            For Each dr As DataRow In autonumberfields.Rows
                Dim value As Integer
                If sibling.Rows.Count = 0 Then
                    value = 0
                Else
                    value = IIf(IsDBNull(sibling.Compute("MAX(" + dr.GetString("name") + ")", "")), 0, sibling.Compute("MAX(" + dr.GetString("name") + ")", ""))
                End If
                value += 1
                db.Execute("UPDATE XT_" + tableName + "   SET " + dr.GetString("name").ToString() + " = " + value.ToString() + " WHERE ROWUID = {0}".SqlFormatString(newId.ToString()))
                Dim sql2 As String = "INSERT INTO ParcelChanges  (ParcelId, FieldId, Action, ReviewedBy, OriginalValue, NewValue, ChangedTime, Latitude, Longitude, AuxROWUID, ParentAuxROWUID, QCChecked, QCApproved) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}); SELECT CAST(@@IDENTITY AS INT) As NewId;"
                Dim changeId2 As Integer = db.GetIntegerValue(sql2.SqlFormat(True, parcelId, dr.GetString("Id"), "new", loginId, "", value, eventDate, latitude, longitude, newId, parentROWUID.ToString.TrimStart("0"), isQC, isQC))
                db.Execute("EXEC processFieldChangesTracking {0},{1},{2},{3}".SqlFormat(True, parcelId, dr.GetString("Id"), changeId, value))
            Next
            Dim ParentfieldsInsert = db.GetStringValue("SELECT IncludeParentFieldsOnInsert FROM fieldcategory WHERE sourceTableName ={0} AND IncludeParentFieldsOnInsert is Not Null".SqlFormatString(tableName))
            Dim fieldsarray() As String = ParentfieldsInsert.Split(",")
            If ParentfieldsInsert <> "" And (fieldsarray.Length > 0 And clientParentROWUID <> 0) Then
                Dim parentROW As DataRow = db.GetTopRow("SELECT * FROM XT_" + parentTableName + " WHERE ROWUID={0}".SqlFormatString(parentROWUID.ToString()))
                For Each field As String In fieldsarray
                    Dim FID As Integer = db.GetIntegerValue("SELECT Id from DataSourceField WHERE Name={0} AND SourceTable={1}".SqlFormatString(field.ToString(), tableName))
                    changeId = db.GetIntegerValue(sql.SqlFormat(True, parcelId, FID, "new", loginId, "", parentROW.GetString(field), eventDate, latitude, longitude, newId, parentROWUID.ToString.TrimStart("0"), isQC, isQC))
                Next
            End If

            ParcelAuditStream.CreateFlagEvent(db, eventDate, parcelId, loginId, _appTypes + ":New Aux record " + newId.ToString() + " inserted to " + tableName, _appTypes)
            db.Execute("EXEC cc_InsertDefaulltPCIs {0},{1},{2},{3},{4},{5},{6},{7},{8},{9}".SqlFormat(True, parcelId, loginId, "new", eventDate, latitude, longitude, newId, parentROWUID.ToString.TrimStart("0"), tableName, _appTypes))
        Else
            Throw New Exception("Failed to insert new record in " + tableName)

        End If
    End Sub

    Private Sub _insertVisionRecord(db As Database, parcelId As Integer, loginId As String, clientROWUID As Long, clientParentROWUID As String, tableName As String, eventDate As Date, latitude As Single, longitude As Single, Optional ByVal CC_YearStatus As String = "", Optional ByVal _appTypes As String = "", Optional ByVal sourceROWUID As String = "")
        Dim des As String = MobileSyncUploadProcessor._improvementCopy(db, parcelId, parcelId, sourceROWUID, loginId, clientROWUID, clientParentROWUID, IIf(CC_YearStatus.Trim <> "", tableName + "$" + CC_YearStatus, tableName), eventDate, latitude, longitude, "", "visionCopy")
        ParcelAuditStream.CreateFlagEvent(db, eventDate, parcelId, loginId, _appTypes + ":" + des, _appTypes)
    End Sub

    Private Shared Function _deleteRecord(db As Database, parcelId As Integer, loginId As String, clientROWUID As Long, tableName As String, eventDate As Date, latitude As Single, longitude As Single)

        Dim DFID = db.GetIntegerValue("SELECT MIN(Id) FROM DataSourceField WHERE SourceTable ={0}".SqlFormatString(tableName))
        Dim tableId As Integer = db.GetIntegerValueOrInvalid("SELECT Id FROM DataSourceTable WHERE Name = " + tableName.ToSqlValue)
        If clientROWUID < 0 Then
            clientROWUID = db.GetIntegerValue("SELECT ROWUID FROM XT_" + tableName + " WHERE ClientROWUID ={0} ".SqlFormatString(clientROWUID.ToString()))
        End If

        If clientROWUID > 0 Then
            __deleteChildRecords(db, clientROWUID, tableName)
            db.Execute("UPDATE XT_" + tableName + "   SET CC_Deleted = 1 WHERE ROWUID = {0}".SqlFormatString(clientROWUID.ToString()))
            _processParcelDataChange(db, loginId, eventDate, parcelId, clientROWUID, DFID, Nothing, Nothing, latitude, longitude, "delete")
            If ParcelEventLog.Enabled Then
                ParcelEventLog.CreateTableEvent(db, ParcelEventLog.EventType.RowDeleted, ParcelEventLog.ApplicationType.Console, parcelId, tableId, clientROWUID)
            End If
            Return "1 aux record " + clientROWUID.ToString + " deleted from " + tableName
        Else
            Return ""
        End If

    End Function
    Private Shared Function _deleteChildRecordsOnly(db As Database, parcelId As Integer, loginId As String, clientROWUID As Long, tableName As String, childTableNameFilter As String, eventDate As Date, latitude As Single, longitude As Single)
        Dim countString As String = ""

        If clientROWUID < 0 Then
            clientROWUID = db.GetIntegerValue("SELECT ROWUID FROM XT_" + tableName + " WHERE ClientROWUID ={0} ".SqlFormatString(clientROWUID.ToString()))
        End If

        If clientROWUID > 0 Then
            Dim childTablesSql As String = "SELECT DISTINCT tt.Name FROM DataSourceRelationships r LEFT OUTER JOIN DataSourceTable pt ON r.ParentTableId = pt.Id LEFT OUTER JOIN DataSourceTable tt ON r.TableId = tt.Id WHERE r.Relationship = 2 AND pt.Name = {0}"
            For Each c As DataRow In db.GetDataTable(childTablesSql.SqlFormatString(tableName)).Rows
                Dim n As Integer = 0
                Dim childTableName As String = c.GetString("Name")
                If childTableNameFilter = "*" Or childTableNameFilter = "" Or childTableNameFilter Is Nothing Then
                ElseIf childTableNameFilter <> childTableName Then
                    Continue For
                End If

                Dim DFID = db.GetIntegerValue("SELECT MIN(Id) FROM DataSourceField WHERE SourceTable ={0}".SqlFormatString(childTableName))
                Dim tableId As Integer = db.GetIntegerValueOrInvalid("SELECT Id FROM DataSourceTable WHERE Name = " + childTableName.ToSqlValue)

                For Each cr As DataRow In db.GetDataTable("EXEC cc_GetChildROWUIDs {0}, {1}, {2}".SqlFormatString(tableName, childTableName, clientROWUID)).Rows
                    Dim childRowUID As Integer = cr.GetInteger("ROWUID")
                    __deleteChildRecords(db, childRowUID, childTableName)
                    db.Execute("UPDATE XT_" + childTableName + "   SET CC_Deleted = 1 WHERE ROWUID = {0}".SqlFormatString(childRowUID.ToString()))
                    If ParcelEventLog.Enabled Then
                        ParcelEventLog.CreateTableEvent(db, ParcelEventLog.EventType.RowDeleted, ParcelEventLog.ApplicationType.Console, parcelId, tableId, childRowUID)
                    End If
                    _processParcelDataChange(db, loginId, eventDate, parcelId, childRowUID, DFID, Nothing, Nothing, latitude, longitude, "delete")
                    n += 1
                Next
                If n > 0 Then
                    countString += n & " record(s) deleted from " + childTableName + "; "
                End If
            Next
            Return countString
        Else
            Return ""
        End If
    End Function

    Private Shared Sub __deleteChildRecords(db As Database, parentRowUID As Long, tableName As String)
        Dim childTablesSql As String = "SELECT DISTINCT tt.Name FROM DataSourceRelationships r LEFT OUTER JOIN DataSourceTable pt ON r.ParentTableId = pt.Id LEFT OUTER JOIN DataSourceTable tt ON r.TableId = tt.Id WHERE r.Relationship = 2 AND pt.Name = {0}"
        For Each c As DataRow In db.GetDataTable(childTablesSql.SqlFormatString(tableName)).Rows
            Dim childTableName As String = c.GetString("Name")
            For Each cr As DataRow In db.GetDataTable("EXEC cc_GetChildROWUIDs {0}, {1}, {2}".SqlFormatString(tableName, childTableName, parentRowUID)).Rows
                Dim childRowUID As Integer = cr.GetInteger("ROWUID")
                __deleteChildRecords(db, childRowUID, childTableName)
                db.Execute("UPDATE XT_" + childTableName + "   SET CC_Deleted = 1 WHERE ROWUID = {0}".SqlFormatString(childRowUID.ToString()))
            Next
        Next
    End Sub
    Private Shared Function IsDtrApp() As Boolean
        Return HttpContext.Current.Request("__dtr") = "1"
    End Function
    Private Shared Sub _processParcelDataChange(db As Database, loginId As String, eventDate As DateTime, parcelId As Integer, auxRowUID As Long, fieldId As Integer, oldValue As String, newValue As String, latitude As Single, longitude As Single, Optional ByVal action As String = "edit")
        Dim tableName As String = db.GetStringValue("SELECT SourceTable FROM DataSourceField WHERE Id = " & fieldId)
        tableName = DataSource.FindTargetTable(db, tableName)
        Dim parentAuxRowUID As Integer = 0
        Dim ar As DataRow = Nothing
        If auxRowUID < 0 Then
            ar = db.GetTopRow("SELECT * FROM " + tableName + " WHERE ClientROWUID = " & auxRowUID)
            If ar IsNot Nothing Then
                auxRowUID = ar.GetInteger("ROWUID")
            End If
            action = "new"
        ElseIf auxRowUID > 0 Then
            ar = db.GetTopRow("SELECT * FROM " + tableName + " WHERE ROWUID = " & auxRowUID)
        End If
        If ar IsNot Nothing Then
            parentAuxRowUID = ar.GetInteger("ParentROWUID")
        End If
        Dim _appTypes As String = IIf(IsDtrApp() = True, "DTR", "QC")
        Dim sql As String = "INSERT INTO ParcelChanges (ParcelId, FieldId, Action, ReviewedBy, OriginalValue, NewValue, ChangedTime, Latitude, Longitude, AuxROWUID, ParentAuxROWUID) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}); SELECT CAST(@@IDENTITY AS INT) As NewId;"
        Dim changeId As Integer = db.GetIntegerValue(sql.SqlFormat(True, parcelId, fieldId, action, loginId, oldValue, newValue, eventDate, latitude, longitude, IIf(auxRowUID = 0, "", auxRowUID.ToString), IIf(parentAuxRowUID = 0, "", parentAuxRowUID.ToString)))
        db.Execute("EXEC processFieldChangesTracking {0},{1},{2},{3}".SqlFormat(True, parcelId, fieldId, changeId, newValue))
        ParcelAuditStream.CreateDataChangeEvent(db, eventDate, parcelId, auxRowUID, loginId, fieldId, newValue, changeId, _appTypes)

        Parcel.ElevatePriorityIfNormal(db, parcelId, loginId)
        Parcel.ResetReviewStatus(db, parcelId, loginId)
    End Sub

    Private Shared Sub __recoverChildRecords(db As Database, parentRowUID As Long, tableName As String, parcelid As String)
        Dim childTablesSql As String = "SELECT DISTINCT tt.Name FROM DataSourceRelationships r LEFT OUTER JOIN DataSourceTable pt ON r.ParentTableId = pt.Id LEFT OUTER JOIN DataSourceTable tt ON r.TableId = tt.Id WHERE r.Relationship = 2 AND pt.Name = {0}"
        For Each c As DataRow In db.GetDataTable(childTablesSql.SqlFormatString(tableName)).Rows
            Dim childTableName As String = c.GetString("Name")
            For Each cr As DataRow In db.GetDataTable("EXEC cc_GetChildROWUIDs {0}, {1}, {2}".SqlFormatString(tableName, childTableName, parentRowUID)).Rows
                Dim childRowUID As Integer = cr.GetInteger("ROWUID")
                __recoverChildRecords(db, childRowUID, childTableName, parcelid)
                db.Execute("UPDATE XT_" + childTableName + "   SET CC_Deleted = 0 WHERE ROWUID = {0}".SqlFormatString(childRowUID.ToString()))
                Database.Tenant.Execute("DELETE pc FROM ParcelChanges pc LEFT OUTER JOIN DataSourceField ds on ds.Id=pc.FieldId INNER JOIN DataSourceTableAction da ON da.tableid=ds.TableId AND da.fieldName=ds.Name WHERE parcelid={0} AND da.Action ='D' AND pc.auxrowuid= {1} AND ds.SourceTable = {2}".SqlFormatString(parcelid, childRowUID, childTableName))
                Database.Tenant.Execute("DELETE pc FROM ParcelChanges pc LEFT OUTER JOIN DataSourceField ds on ds.Id=pc.FieldId WHERE parcelid={0} AND pc.auxrowuid= {1} AND pc.Action = 'delete' AND ds.SourceTable = {2}".SqlFormatString(parcelid, childRowUID, childTableName))  'only delete delete PCI when recovering FD_8652
            Next
        Next
    End Sub
    Private Sub _qcVisualStats()
        Try
            Dim dtAWC As DataTable = Database.Tenant.GetDataTable("EXEC qc_VisualStats {0}".SqlFormat(True, "1"))
            Dim jstr As String = "{"
            If (dtAWC.Rows.Count > 0) Then
                For Each dr As DataRow In dtAWC.Rows
                    jstr += """" + dr.Item("Item") + """:""" + dr.Item("Value") + ""","
                Next
            End If
            jstr = jstr.Remove(jstr.LastIndexOf(","))
            jstr += "}"
            JSON.WriteString(jstr)

        Catch ex As Exception
            JSON.Error(ex.Message)
        End Try
    End Sub

    Private Sub _updateAdhocPriority()
        Dim priority = Request("priority")
        Try
            Database.Tenant.Execute("UPDATE [dbo].[PriorityListItems] SET Priority={0} WHERE PriorityListId ={1}".SqlFormatString(priority, HttpContext.Current.Session("ptid")))
            JSON.OK()
        Catch ex As Exception
            JSON.Error(ex)
        End Try
    End Sub

    Private Sub _DeleteDTRWorkflow(ByVal parcelId As Integer, ByVal workflowcode As String)
        Try
            Dim DTRWorkflowNew = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM ClientSettings WHERE Name = 'DTRWorkflowNew' AND Value = 1")
            If DTRWorkflowNew > 0 Then
                Dim LoginId As String = UserName
                Dim codesArray As String() = workflowcode.Split(New Char() {"|"c}, StringSplitOptions.RemoveEmptyEntries)
                Dim WFDescription As String = "Removed the WorkFlow : "
                For Each code As String In codesArray
                    Dim codeDesc As String = Database.Tenant.GetStringValue("SELECT PWT.Description FROM ParcelWorkFlowStatusType PWT JOIN ParcelWorkFlowStatus PWS ON PWT.Code = PWS.StatusCode WHERE PWS.ParcelId = {0} AND PWS.StatusCode = {1}".SqlFormatString(parcelId, code))
                    Database.Tenant.Execute("INSERT INTO ParcelAuditTrail (EventDate, EventTime, ParcelId, LoginID, EventType, Description, ApplicationType) VALUES (GETUTCDATE(), GETUTCDATE(), '" & parcelId & "', '" & LoginId & "', 0,'" & WFDescription & code & "-" & codeDesc & " by " & LoginId & " ','DTR')".SqlFormatString)
                    Database.Tenant.Execute("DELETE FROM ParcelWorkFlowStatus WHERE ParcelId = {0} AND StatusCode = {1}".SqlFormatString(parcelId, code))
                Next
                JSON.OK()
            End If
        Catch ex As Exception
            JSON.Error(ex)
        End Try

    End Sub

End Class


Public Class SearchQuery
    Public Property Id As Integer
    Public Property Name As String
    Public Property IsCustom As Boolean
    Public Property MainOp As String
    Public Property Filters As Array
End Class

Public Class SearchFilter
    Public Property Label As String
    Public Property Field As String
    Public Property [Operator] As String
    Public Property Value1 As String
    Public Property Value2 As String
End Class