﻿Imports System.IO
Imports DotSpatial.Data
Imports DotSpatial.Projections
Imports DotSpatial.Topology
Imports ICSharpCode.SharpZipLib

Namespace ShapeFileFactory

    Public Class GISShapeFile

        Public Property KeyField As String = "Parcel"
        Public Property KeyValue As String
        'Public ReadOnly Property Shapes As New List(Of GISShape)

        'Public Sub AddShape(v As GISShape)
        '    Shapes.Add(v)
        'End Sub

        'Public Function ToPolygons() As List(Of Polygon)
        '    Return Shapes.Select(Function(x) x.ToPolygon).ToList
        'End Function

        'Public Function ToMultiPolygon() As MultiPolygon
        '    Return New MultiPolygon(ToPolygons.ToArray)
        'End Function

        Public ReadOnly Property Sketches As New List(Of GISShapeGroup)
        Public Sub AddSketch(v As GISShapeGroup)
            Sketches.Add(v)
        End Sub

        Sub Save(strm As Stream, Optional dt As DataTable = Nothing)

            Dim tempFileName As String = Guid.NewGuid.ToString

            Dim tempPath As String = Path.Combine(Path.GetTempPath, "shp-converter")
            If Not IO.Directory.Exists(tempPath) Then
                IO.Directory.CreateDirectory(tempPath)
            End If
            Dim tempFile As String = Path.Combine(tempPath, tempFileName + ".shp")

            Dim polyfile As New FeatureSet(FeatureType.Polygon)
            polyfile.Projection = ProjectionInfo.FromEpsgCode(4326)
            polyfile.CoordinateType = CoordinateType.Regular
            For Each sg In Me.Sketches
                polyfile.AddFeature(sg.ToMultiPolygon)
            Next

            polyfile.InvalidateVertices()
            polyfile.UpdateExtent()

            If dt Is Nothing Then
                dt = New DataTable
                dt.Columns.Add(KeyField)
                Dim dr As DataRow = dt.NewRow
                dr(KeyField) = KeyValue
                dt.Rows.Add(dr)
            End If
            polyfile.SetAttributes(0, dt)

            polyfile.SaveAs(tempFile, True)

            Dim zipOutput As New Zip.ZipOutputStream(strm)

            For Each fileName In Directory.GetFiles(tempPath, tempFileName + ".*")
                Dim ext As String = Path.GetExtension(fileName)
                Dim zipName As String = KeyValue + ext
                Dim ze As New Zip.ZipEntry(zipName)
                zipOutput.PutNextEntry(ze)
                Dim inputFile As New IO.FileStream(fileName, FileMode.Open)
                inputFile.CopyTo(zipOutput)
                zipOutput.CloseEntry()
                inputFile.Close()
                File.Delete(fileName)
            Next
            zipOutput.Close()
            strm.Close()
        End Sub

    End Class

    Public Class GISShapeGroup
        Public ReadOnly Property Shapes As New List(Of GISShape)

        Public Sub AddShape(v As GISShape)
            Shapes.Add(v)
        End Sub

        Public Function ToPolygons() As List(Of Polygon)
            Return Shapes.Select(Function(x) x.ToPolygon).ToList
        End Function

        Public Function ToMultiPolygon() As MultiPolygon
            Return New MultiPolygon(ToPolygons.ToArray)
        End Function

    End Class

    Public Class GISShape

        Public ReadOnly Property Vertices As New List(Of GISShapeVectex)

        Public Sub AddVertex(latitude As Double, longitude As Double)
            Vertices.Add(New GISShapeVectex() With {.Latitude = latitude, .Longitude = longitude})
        End Sub

        Public Function ToCoordinates() As List(Of Coordinate)
            Return Vertices.Select(Function(x) x.ToCoordinate).ToList
        End Function

        Public Function ToPolygon() As Polygon
            Return New Polygon(ToCoordinates)
        End Function

    End Class

    Public Class GISShapeVectex

        Public Property Latitude As Double
        Public Property Longitude As Double

        Public Function ToCoordinate() As Coordinate
            Return New Coordinate(Latitude, Longitude)
        End Function

    End Class

End Namespace
