﻿Public Class JSONDataHandler
	Inherits WebHandlerBase

    Protected Overrides Sub assignRequestProcessor(ByRef processor As RequestProcessorBase, ByRef handled As Boolean)

        If Path.StartsWith("datasetup/") Then
            processor = New DataSetupProcessor()
            processor.SetRoot("datasetup/")

        ElseIf Path.StartsWith("maauth/") Then
            processor = New AuthenticationProcessor
            processor.SetRoot("maauth/")

        ElseIf Path.StartsWith("mobileassessor/") Then
            processor = New MobileAssessorProcessor
            processor.SetRoot("mobileassessor/")

        ElseIf Path.StartsWith("tracking/") Then
            processor = New TrackingProcessor
            processor.SetRoot("tracking/")

        ElseIf Path.StartsWith("sv/") Then
            processor = New SketchValidationProcessor
            processor.SetRoot("sv/")

        ElseIf Path.StartsWith("quality/") Then
            processor = New QualityControlProcessor
            processor.SetRoot("quality/")

        ElseIf Path.StartsWith("debug/") Then
            processor = New DebugProcessor
            processor.SetRoot("debug/")

        ElseIf Path.StartsWith("dtr/") Then
            processor = New DesktopReviewProcessor
            processor.SetRoot("dtr/")

        ElseIf Path.StartsWith("parcels/") Then
            processor = New ParcelManagerProcessor
            processor.SetRoot("parcels/")

        ElseIf Path.StartsWith("sketchview") Then
            processor = New SketchViewProcessor
            processor.SetRoot("sketchview/")
            
       ElseIf Path.StartsWith("mra/") Then
            processor = New MRAProcessor
            processor.SetRoot("mra/")
            
        ElseIf Path.StartsWith("admin/")
            processor = New AdminProcessor
            processor.SetRoot("admin/")

        ElseIf Path.StartsWith("test/") Then
            JSON.Error("There is an error")
            handled = True	
        End If
    End Sub
End Class
