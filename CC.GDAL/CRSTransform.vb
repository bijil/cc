﻿Imports Microsoft.VisualBasic
Imports ProjNet
Imports ProjNet.CoordinateSystems
Imports ProjNet.CoordinateSystems.Transformations

Public Class CRSTransform

	Dim wgs84wktxml As XElement = <wkt>GEOGCS["WGS 84",
    DATUM["WGS_1984",
        SPHEROID["WGS 84",6378137,298.257223563,
            AUTHORITY["EPSG","7030"]],
        AUTHORITY["EPSG","6326"]],
    PRIMEM["Greenwich",0,
        AUTHORITY["EPSG","8901"]],
    UNIT["degree",0.01745329251994328,
        AUTHORITY["EPSG","9122"]],
    AUTHORITY["EPSG","4326"]]</wkt>

	Dim xmlwkt As XElement = <wkt>PROJCS["NAD83(HARN) / Ohio North",
  GEOGCS["NAD83(HARN)",
     DATUM["NAD83_High_Accuracy_Regional_Network",
        SPHEROID["GRS 1980",6378137,298.2572221010002,
           AUTHORITY["EPSG","7019"]],
        AUTHORITY["EPSG","6152"]],
     PRIMEM["Greenwich",0],
     UNIT["degree",0.0174532925199433],
     AUTHORITY["EPSG","4152"]],
  PROJECTION["Lambert_Conformal_Conic_2SP"],
  PARAMETER["standard_parallel_1",41.7],
  PARAMETER["standard_parallel_2",40.43333333333333],
  PARAMETER["latitude_of_origin",39.66666666666666],
  PARAMETER["central_meridian",-82.5],
  PARAMETER["false_easting",1968500],
  PARAMETER["false_northing",0],
  UNIT["US survey foot",0.3048006096012192,
     AUTHORITY["EPSG","9003"]],
  AUTHORITY["EPSG","2834"]]</wkt>

	Public Function TransformToNAD83HARN(lat As Double, lng As Double) As UTMPoint
		Dim cf As New CoordinateSystemFactory()
		Dim wgs84 As CoordinateSystem = cf.CreateFromWkt(wgs84wktxml.Value)
		Dim nad83 As CoordinateSystem = cf.CreateFromWkt(xmlwkt.Value)

		Dim t As New CoordinateTransformationFactory
		Dim trans As CoordinateTransformation = t.CreateFromCoordinateSystems(wgs84, nad83)

		Dim points = trans.MathTransform().Transform(New Double() {lng, lat})
		Return New UTMPoint With {.Easting = points(0), .Northing = points(1)}
	End Function


End Class

Public Class UTMPoint
	Public Northing As Double
	Public Easting As Double
End Class