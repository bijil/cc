﻿Imports System.Drawing
Imports System.Threading

Public Class MapServer

    Dim pool As New MapPool(4)

    Public Sub Initialize()

    End Sub

    Public Sub Test()

        Dim lat1, lng1 As Double
        lat1 = 41.650597166341406
        lng1 = -83.482017517089844

        Dim ct As New CRSTransform
        Dim utm As UTMPoint = ct.TransformToNAD83HARN(lat1, lng1)

        Dim d As Integer = 512 / 1.378
        Dim x1, y1, x2, y2 As Long
        x1 = utm.Easting
        y1 = utm.Northing
        x2 = x1 + d
        y2 = y1 + d

        For i As Integer = 0 To 100
            Dim th As New Thread(New ParameterizedThreadStart(Sub(c As Integer)
                                                                  Dim t As MapTiler = pool.GetTiler
                                                                  t.Render(x1 + d * c, y1, d, c)
                                                                  Console.WriteLine("{0}:   {1:ss.ffffff}   {2}", t.Index.ToString(), Now, c)
                                                              End Sub))
            th.Start(i)
        Next

    End Sub

    Shared Sub RenderMap(ByRef map As SharpMap.Map, x1 As Long, y1 As Long, d As Integer, i As Integer)
        Dim x2, y2 As Long
        x2 = x1 + d
        y2 = y1 + d
        Dim env As New GeoAPI.Geometries.Envelope(x1 + d * i, x2 + d * i, y1, y2)
        map.ZoomToBox(env)
        Dim img As Bitmap = map.GetMap()
        img.Save("D:\CC_GIS_Temp\Sample_" & i & ".jpg")
    End Sub

    Shared Sub Tick()
        Console.WriteLine(Now.ToString("ss.ffffff"))
    End Sub

End Class

Public Class MapPool
    Dim lock As New Object
    Dim _mapPool As New Collections.Queue

    Public Sub New(capacity As Integer)
        For i As Integer = 0 To capacity - 1
            Dim m As New MapTiler(i)
            _mapPool.Enqueue(m)
        Next
    End Sub

    Public Function GetTiler() As MapTiler
        SyncLock lock
            Dim t As MapTiler = _mapPool.Dequeue
            _mapPool.Enqueue(t)
            Return t
        End SyncLock
    End Function
End Class

Public Class MapTiler

    Private map As SharpMap.Map
    Private busy As Boolean = False
    Private lock As New Object
    Private _index As Integer

    Public Sub New(index As Integer)
        Me._index = index
        map = New SharpMap.Map(New Size(256, 256))
        Dim layer As New SharpMap.Layers.GdalRasterLayer("LUCAS", "D:\SID Store\Lucas 2011\NAD83\LUCAS_NAD83_RGB_100x_" & index & ".sid")
        map.Layers.Add(layer)
    End Sub

    Public Sub Render(x1 As Long, y1 As Long, d As Integer, i As Integer)
        SyncLock lock
            Dim x2, y2 As Long
            x2 = x1 + d
            y2 = y1 + d
            Dim env As New GeoAPI.Geometries.Envelope(x1 + d * i, x2 + d * i, y1, y2)

            map.ZoomToBox(env)
            Dim img As Bitmap = map.GetMap()
            img.Save("D:\CC_GIS_Temp\Sample_" & i & ".jpg")
        End SyncLock
    End Sub

    Public ReadOnly Property Index As Integer
        Get
            Return _index
        End Get
    End Property

End Class


