﻿Imports System.Web
Imports System.Web.SessionState
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Diagnostics


Public Class MapRequestHander
	Implements IHttpHandler, IRequiresSessionState

	Public ReadOnly Property IsReusable As Boolean Implements System.Web.IHttpHandler.IsReusable
		Get
			Return True
		End Get
	End Property

	Public Sub ProcessRequest(context As System.Web.HttpContext) Implements System.Web.IHttpHandler.ProcessRequest

		Dim lat As Double
		Dim lng As Double
		Dim zoom As Integer
		Try
			lat = Double.Parse(context.Request("lat"))
			lng = Double.Parse(context.Request("lng"))
			zoom = Integer.Parse(context.Request("zoom"))
		Catch ex As Exception

		End Try

		Dim ct As New CRSTransform
		Dim utm As UTMPoint = ct.TransformToNAD83HARN(lat, lng)

		Dim tempFile As String = My.Computer.FileSystem.GetTempFileName
		tempFile = IO.Path.ChangeExtension(tempFile, ".jpg")

		Dim scale As Integer = 20 - zoom
		Dim scaleCorrection As Single = 1.378
		Dim w As Long = 256 * Math.Pow(2, scale - 1)

		Dim fileCreated As Boolean = False
        Try
            Dim sid As String = "D:\SID Store\Lucas 2011\NAD83\LUCAS_NAD83_RGB_100x.sid"
            If IO.File.Exists(sid) Then
                Dim pi As New ProcessStartInfo
                pi.FileName = "C:\LizardTech\mrsidgeodecode.exe"
                Dim arguments As String = "-coord geo -input """ + sid + """ -output """ + tempFile + """ -ulxy {0} {1} -wh " + w.ToString() + " " + w.ToString() + " -s " & scale
                pi.Arguments = String.Format(arguments, utm.Easting, utm.Northing)
                pi.CreateNoWindow = True
                pi.WindowStyle = ProcessWindowStyle.Hidden
                pi.RedirectStandardError = True
                pi.UseShellExecute = False
                Dim pr As New Process()
                pr.StartInfo = pi
                pr.Start()
                pr.WaitForExit()
                fileCreated = True
            Else
                SendImage(context, GetErrorImage, "SID Not Found")
                Return
            End If

        Catch ex As Exception
            SendImage(context, GetErrorImage, ex.Message)
            Return
        End Try

		If fileCreated Then
			Dim f As New IO.FileStream(tempFile, IO.FileMode.OpenOrCreate)

			Dim extract As New Bitmap(f)

			Dim tile As New Bitmap(256, 256)
			Dim gr As Graphics = Graphics.FromImage(tile)
			gr.ScaleTransform(scaleCorrection, scaleCorrection)
			gr.DrawImage(extract, 0, 0)
			gr.Save()

			SendImage(context, tile)
			f.Close()
			IO.File.Delete(tempFile)
		End If
	End Sub

	Sub InlineProcessor()
		'Dim map As New SharpMap.Map(New Size(256, 256))

		'Try
		'	Dim layer As New SharpMap.Layers.GdalRasterLayer("LUCAS", "D:\t.sid")
		'	map.Layers.Add(layer)
		'	Dim coords As New GeoAPI.Geometries.Coordinate(40, -81)
		'	Dim coord2 As New GeoAPI.Geometries.Coordinate(43, -80)

		'	Dim c As New ProjNet.CoordinateSystems.Transformations.GeometryTransform
		'	Dim env As New GeoAPI.Geometries.Envelope(1597500, 1607500, 687500, 697500)

		'	map.ZoomToBox(env)
		'	map.GetMap().Save(context.Response.OutputStream, ImageFormat.Png)
		'Catch ex As Exception

		'End Try
	End Sub

	Function GetErrorImage() As Image
		Dim bmp As New Bitmap(256, 256)
		Dim gr As Graphics = Graphics.FromImage(bmp)
		gr.DrawLine(Pens.Red, 0, 0, 255, 255)
		gr.DrawLine(Pens.Red, 0, 255, 255, 0)
		gr.Save()
		Return bmp
	End Function

	Sub SendImage(context As HttpContext, image As Image, Optional otherinfo As String = "")
		context.Response.Clear()
		context.Response.Expires = 15
		context.Response.ContentType = "image/png"
		If otherinfo <> "" Then
			context.Response.AddHeader("OtherInfo", otherinfo)
		End If
		image.Save(context.Response.OutputStream, ImageFormat.Png)
	End Sub

End Class
