﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ClientDashboard.ascx.vb"
	Inherits="App_Controls_app_ClientDashboard" %>
<style type="text/css">
	.vtop
	{
		border-spacing: 0px;
	}
	
	.vtop td
	{
		vertical-align: top;
	}
	
	.vtop td:first-child
	{
		width: 160px;
	}
</style>
<h1 style="font-size: 32pt; margin-bottom: 5px;">
	<%= o.GetString("Name")%></h1>
<h1 style="display: none;">
	Welcome,
	<%= Profile.FullName%></h1>
<table style="border-spacing: 0px; width: 100%;">
	<tr>
		<td style="width: 50%; vertical-align: top;">
			<h2>
				Welcome,
				<%= Profile.FullName%></h2>
			<div style="margin-bottom: 10px;">
				<strong>Location</strong> :
				<%= o.GetString("City")%>
				<%= o.GetString("County")%>
				<%= o.GetString("State")%>
			</div>
			<table class="vtop">
				<tr>
					<td>Your last login: </td>
					<td>
						<%= Membership.GetUser.LastLoginDate%> +00:00
					</td>
				</tr>
			</table>
		</td>
		<td style="width: 50%; vertical-align: top;">
			<h2>
				Cloud Data Statistics</h2>
			<table class="vtop">
				<tr>
					<td>Total Parcels: </td>
					<td>
						<%= Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM Parcel").ToString("N0")%>
					</td>
				</tr>
				<tr>
					<td>Total Neighborhoods: </td>
					<td>
						<%= Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM Neighborhood").ToString("N0")%>
					</td>
				</tr>
				<tr>
					<td>Total Groups: </td>
					<td>
						<%= Database.Tenant.GetIntegerValue("SELECT COUNT(DISTINCT GroupNo) FROM Neighborhood").ToString("N0")%>
					</td>
				</tr>
			</table>
			<h2>
				User Pool</h2>
			<table class="vtop">
				<tr>
					<td>Total Users: </td>
					<td>
						<%= Membership.GetAllUsers.Count%>
					</td>
				</tr>
				<tr>
					<td>Online Users: </td>
					<td>
						<%= Membership.GetNumberOfUsersOnline%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
