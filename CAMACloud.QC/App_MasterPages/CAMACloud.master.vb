﻿
Partial Class App_MasterPages_CAMACloud
	Inherits System.Web.UI.MasterPage


	Protected Sub LoginStatus_LoggedOut(sender As Object, e As System.EventArgs) Handles LoginStatus.LoggedOut
		Response.Redirect("~/")
	End Sub

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Dim allowedRole As String = ConfigurationManager.AppSettings("AllowedRole")
			If allowedRole <> "" Then
				If Not Roles.IsUserInRole(allowedRole) Then
					Response.Redirect("~/web/403.aspx")
				End If
			End If
		End If
	End Sub
End Class

