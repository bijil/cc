﻿var map;


function initMap() {
	var options = {
		zoom: 16,
		center: new google.maps.LatLng(41.655471, -83.534546),
		mapTypeId: google.maps.MapTypeId.HYBRID,
		minZoom: 8
	};
	var mc = document.getElementById('map');
	map = new google.maps.Map(mc, options);
	navigator.geolocation.getCurrentPosition(function (position) {
		map.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
	});
}

function setPageDimensions(cheight) {
	$('#map').height(cheight - 2);
	$('.points-display').height(cheight - 426);
	$('.points-display').css({'overflow': 'auto'});
}

$(function () {
	initMap();
});