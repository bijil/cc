﻿function sigmaSketch(color,vector, target, showLabels, showLengths) {
	var ox, oy;
	ox = 0; oy = 0;
	var x, y;
	var max, may, mix, miy;
	max = 0; may = 0; mix = 0; miy = 0;

	var scale = 1;
	var fontSize;

	var canvas = document.getElementById(target);
	var ctx;

	if (!canvas.getContext) {
		alert('Incompatible browser!');
		return;
	}

	ctx = canvas.getContext("2d");

	canvas.width = canvas.width;

	var i;

	var parts = vector.split(',');
	for (i in parts) {
		var part = parts[i];
		if (/(.*?):(.*?)/.test(part)) {
			var hi = part.split(':');
			var head = hi[0];
			var info = hi[1];
			drawLines(ctx, info, true, true);
		}
	}

	var sHeight = may - miy;
	var sWidth = max - mix;
	if (sHeight * 1.1 > canvas.height) {
		scale = canvas.height / sHeight * 0.9;
	}

	var maxHeight = canvas.height * 0.9;
	var maxWidth = canvas.width * 0.9;
	var aspectRatio = parseFloat(sWidth) / parseFloat(sHeight);
	var width, height;

	if ((sWidth < maxWidth) && (sHeight < maxHeight)) {
		scale = 1;
	} else if (aspectRatio > 1) {
		width = maxWidth;
		height = (width / aspectRatio);
		if (height > maxHeight) {
			height = maxHeight;
			width = (height * aspectRatio);
		}
	} else {
		height = maxHeight;
		width = (height * aspectRatio);
		if (width > maxWidth) {
			width = maxWidth;
			height = (width / aspectRatio);
		}
	}

	scale = width / sWidth;

	ctx.lineWidth = 2 / scale;
	ctx.strokeStyle = color;
	fontSize = parseInt(12 / scale);
	if (fontSize < 8)
		fontSize = 8;
	if (fontSize > 18)
		fontSize = 18;
	ctx.font = 'normal ' + fontSize + 'pt Arial';
	ctx.scale(scale, scale);
	oy = (canvas.height / scale - (sHeight)) / 2 - miy;
	ox = (canvas.width / scale - (sWidth)) / 2 - mix;
	for (i in parts) {
		var part = parts[i];
		if (/(.*?):(.*?)/.test(part)) {
			var hi = part.split(':');
			var head = hi[0];
			var info = hi[1];
			if(showLabels)
				drawLabel(ctx, head);
			drawSketch(ctx, info, showLengths);
		}
	}

	function drawLabel(ctx, head) {
		var posString, labels;
		var r1 = /(.*?)\[(.*?)\]/;
		var parts = r1.exec(head);
		labels = parts[1].replace(/\{(.*?)\}/g, '');
		posString = parts[2];
		drawLines(ctx, posString);
		ctx.fillText(labels, x, y);

	}
	function drawSketch(ctx, vector, showLengths) {
		drawLines(ctx, vector, false, false, showLengths);
	}

	function drawLines(ctx, lineString, penOff, detectBounds, showLengths) {
		var penOn = false;
		x = ox; y = oy;
		var cx, cy;
		var ins = lineString.match(/[S]|[UDLR][0-9]*/g);
		ctx.beginPath();
		ctx.moveTo(x, y);
		for (var i in ins) {
			cx = x;
			cy = y;
			var cmd = ins[i];
			var distance = 0;
			if (cmd == "S") {
				penOn = true;
			}
			else {
				var cp = cmd.match(/[UDLR]|[0-9]*/g);
				var dir = cp[0];
				distance = cp[1];
				var dis = parseInt(distance) * 12;
				switch (dir) {
					case "U":
						y -= dis;
						break;
					case "D":
						y += dis;
						break;
					case "L":
						x -= dis;
						break;
					case "R":
						x += dis;
						break;
				}
			}
			if (!penOff && penOn) {
				if (showLengths) {
					if (distance > 0)
						ctx.fillText(distance, (x + cx) / 2, (y + cy) / 2);
				}
				ctx.moveTo(cx, cy);
				ctx.lineTo(x, y);
				ctx.stroke();
			} else {
				ctx.moveTo(x, y);
			}

			if (detectBounds) {
				if (x < mix) mix = x;
				if (x > max) max = x;
				if (y < miy) miy = y;
				if (y > may) may = y;
			}

		}
		ctx.stroke();
	}

	return canvas.toDataURL();
}