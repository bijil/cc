﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/Qualitycontrol.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftContent" Runat="Server">
         <div id="accordion" name="qc">
        <h3>
            <a>APPRISAL</a></h3>
        <div id="search" class="group-menu">
            <fieldset>
                <legend>Search Criteria:</legend>
                <label>
                    Select Appraiser:</label><br />
                <select class="agents" id="s-quick-agent">
                    <option>Loading ...</option>
                </select>
                <div class="boxed" style="width: 90%">
                    <div style="width: 50%;">
                        <label>
                            Nbhd#</label>
                        <input maxlength="6" id="s-nbhd" />
                    </div>
                </div>     Year Built <br />
                <div class="boxed" style="width: 90%">
             
                    <div style="width: 50%;">
                        <label>
                          From:</label>
                        <input type="number" min="1900" max="2012" id="s-year-from" />
                    </div>
                    <div style="width: 50%;">
                        <label>
                             To:</label>
                        <input type="number" min="1900" max="2012" id="s-year-to" />
                    </div>
                </div>
                <label>
                    Random Selection (10% - 100%):</label>
                <input type="range" max="100" min="10" step="10" value="10" id="s-random" />
                <div class="buttonbar">
                    <button onclick="doSearch(1);">
                        Search</button>
                </div>
            </fieldset>
        </div>
        <h3>
            <a>LATEST</a></h3>
        <div id="latest" class="group-menu">
            <fieldset>
                <legend>Select Agent:</legend>
                <select class="agents" id="s-latest-agent">
                    <option>Loading ...</option>
                </select>
                <div class="buttonbar">
                    <button onclick="doSearch(2);">
                        Search</button>
                </div>
            </fieldset>
        </div>
        <h3>
            <a>NBHD</a></h3>
        <div id="nbhd-search" class="group-menu">
            <fieldset>
                <legend>Search Criteria:</legend>
                <div class="boxed" style="width: 90%">
                    <label>
                        Nbhd#</label>
                    <input maxlength='10' id="nbhd-nbhd" />
                    <br />
                    <label>
                        Status:</label><br />
                    <select id="nbhd-type">
                        <option value="0">All</option>
                        <option value="1">Assigned</option>
                        <option value="2">Unassigned</option>
                        <option value="3">Completed</option>
                        <option value="4">Incomplete</option>
                        <option value="5">Modified</option>
                        <!--                                        <option value="6">With Comments</option>-->
                    </select>
                </div>
                Year Built <br />
                <div class="boxed" style="width: 90%">
                    <div style="width: 50%;">
                        <label>
                            From:</label>
                        <input type="number" min="1900" max="2012" id="nbhd-year-from" />
                    </div>
                    <div style="width: 50%;">
                        <label>
                           To:</label>
                        <input type="number" min="1900" max="2012" id="nbhd-year-to" />
                    </div>
                </div>
                <div class="boxed" style="width: 90%">
                    <label>
                        TLA From:</label>
                    <input type="number" id="nbhd-tla-from" />
                    <br />
                    <label>
                        TLA To:</label>
                    <input type="number" id="nbhd-tla-to" />
                </div>
                <div>
                    <div class="boxed" style="width: 90%">
                        <div style="width: 70%;">
                            <label>
                                Field Flag:</label>
                            <select id="nbhd-field-flag">
                                <option value="">All</option>
                                <option value="1">New Construction</option>
                                <option value="2">Demolition</option>
                                <option value="3">Data Discrepancy</option>
                                <option value="999">Any Flag</option>
                            </select>
                        </div>
                        <div style="width: 30%;">
                        </div>
                    </div>
                </div>
                <label>
                    Random Selection (10% - 100%):</label>
                <input type="range" max="100" min="10" step="10" value="10" id="nbhd-random" />
                <div class="buttonbar">
                    <button onclick="doSearch(4);">
                        Search</button>
                </div>
            </fieldset>
        </div>
        <h3>
            <a>PARCEL</a></h3>
        <div id="parcel-search" class="group-menu">
            <fieldset>
                <legend>Search Criteria:</legend>
                <div class="boxed" style="width: 90%">
                    <label>
                        Assessor#</label>
                    <input maxlength='10' id="s-assr" />
                    <br />
                    <label>
                        Parcel#</label>
                    <input maxlength="8" id="s-parcel" />
                </div>
                <div class="buttonbar">
                    <button onclick="doSearch(3);">
                        Search</button>
                </div>
            </fieldset>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">

</asp:Content>

