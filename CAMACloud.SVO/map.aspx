﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="CAMACloud.SVO.sketchv_map" CodeBehind="map.aspx.vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="/App_Static/css/measure.css" />
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=geometry&sensor=false&region=US"></script>
    <script type="text/javascript" src="/App_Static/jslib/jquery.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/jquery-ui-1.8.20.custom.min.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/jquery.rotate.js"></script>
    <script type="text/javascript" src="/App_Static/js/sigmasketch.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="App_Static/js/14-ccsketcheditor.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="/App_Static/jslib/underscore-min.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/base64.js"></script>
    <cc:JavaScript runat="server" IncludeFolder="/App_Static/js/sketchlib/" />
    <script type="text/javascript">
        var Hammer = false;
        var activeParcel = {}
        var map;
        var mapZoom;
        var drag = false;
        var overlay;
        var polygonCenter;
        var degree;
        var scale;
        var RotationDegree;
        var activeParcel;
        var Parcelid;
        var ParcelDataExist = false;
        var parcelPolygon = new google.maps.Polygon();
        var ParcelCenter;
        var alreadyDone;
        var parcelLoaded = false;
        var sketchRenderer = {};
        var clientSettings = {};
        var adjustHandle = null;
        var adjustHandleOrigin = null;
        var adjusting = false;

        var ccma = {
            Sketching: {
                SketchFormatter: null,
                Config: null
            }
        };
        $(function () {
            setPageDimensions();

            initMap(function () {
                window.parent.loadEssentialTables(function () {
                    if (window.parent.processHash) window.parent.processHash();
                })
            });
            sketchRenderer = new CCSketchEditor('.ccse-img', CAMACloud.Sketching.Formatters.TASketch);
            $('#sketch').mouseenter(function () {
                top.$('.sketch-big').show();
            }).mouseleave(function () {
                top.$('.sketch-big').hide()
            });
        });

        $(window).resize(setPageDimensions);
        $(document).keydown(function (e) {
            if (window.parent.currentInput)
                return true
            var key = e.charCode || e.keyCode;
            window.parent.keybordShortCuts(key, e.shiftKey)
            if (key == 38 || key == 40 || key == 37 || key == 39) { window.parent.paintSketch(); e.preventDefault(); return false; }
        });

        function initMap(callback) {
            var options = {
                zoom: 20,
                zoomControl: true,
			 	fullscreenControl: false,	
                center: new google.maps.LatLng(41.655471, -83.534546),
                mapTypeId: google.maps.MapTypeId.HYBRID,
                minZoom: 17,
                tilt: 0,
                keyboardShortcuts: false,
				gestureHandling: 'greedy'
            };
            var mc = document.getElementById('map');
            map = new google.maps.Map(mc, options);
            map.setZoom(20);
            mapZoom = 20;
            overlay = new google.maps.OverlayView();
            overlay.draw = function () { };
            overlay.setMap(map);

            google.maps.event.addListener(map, 'drag', function () {
                setPositionOverlayIfDataExist();
            });
            google.maps.event.addListener(map, 'dragend', function () {
                window.setTimeout(setPositionOverlayIfDataExist, 500);
            });
            google.maps.event.addListener(map, 'zoom_changed', function (e) {
                var zoomLevel = map.getZoom();
                sketchZoom = activeParcel.sketchZoom;
                if (mapZoom > zoomLevel) {
                    zoomSketch(sketchZoom / 2, null, true);
                } else if (mapZoom < zoomLevel) {
                    zoomSketch(sketchZoom * 2, null, true);
                }
                window.setTimeout(setPositionOverlayIfDataExist, 350);
                mapZoom = zoomLevel;
            });

            var measure;
            var measureDiv = document.createElement('div');
            measure = new MeasurementScale(measureDiv, map);
            measureDiv.index = 1;
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(measureDiv);

            var rzIcon = '/App_Static/cursors/rotatezoom.png';

            adjustHandle = new google.maps.Marker({
                position: map.getCenter(),
                icon: rzIcon,
                draggable: true,
                map: map
            });


            var startDegree = null;
            google.maps.event.addListener(adjustHandle, 'dragstart', (function (e) {
                startDegree = degree;
                adjusting = true;
            }));

            google.maps.event.addListener(adjustHandle, 'drag', (function (e) {
                var ah = LatLngToPoint(e.latLng);
                var c = adjustHandleOrigin;
                var dist = Math.round(Math.sqrt((ah.x - c.x) * (ah.x - c.x) + (ah.y - c.y) * (ah.y - c.y)));
                var zoom = Math.round((activeParcel.zoomSketch || 0.6) * dist / 200 * 100) / 100;
                zoomSketch(zoom);

                var rotateDegree = Math.round(Math.atan2(ah.y - c.y, ah.x - c.x) * 180 / Math.PI) + 135 + startDegree;
                if (rotateDegree > 180) rotateDegree = rotateDegree - 360;
                if (rotateDegree < -180) rotateDegree = rotateDegree + 360;
                rotateSketch(rotateDegree);

            }));

            google.maps.event.addListener(adjustHandle, 'dragend', (function (e) {
                startDegree = null;
                adjusting = false;
                //map.setOptions({ draggableCursor: 'default' });
                //adjustHandle.setVisible(true);
            }));

            google.maps.event.addListener(map, 'click', (function (e) {
                if (measure.on) {
                    if (measure.start == null) {
                        measure.start = e.latLng;
                    } else {
                        if (measure.end == null) {
                            measure.end = e.latLng;
                        } else {
                            measure.start = e.latLng;
                            measure.end = null;
                        }
                    }

                    measure.updateGraphics();
                    $('.sv-logo', window.parent.document).click().focus();
                    e.stop();
                }
            }));
            google.maps.event.addListenerOnce(map, 'idle', function () {
                adjustHandleOrigin = LatLngToPoint(adjustHandle.position)
                if (callback) callback();
                adjusting = false;
            });
        }

        function openParcel(callback) {
            activeParcel = parent.activeParcel
            parcelLoaded = false;
            alreadyDone = true;
            ParcelDataExist = false;
            degree = 0;
            currentOverlayPosition = {};
            parcelId = activeParcel.KeyValue1;
            clientSettings["SketchConfig"] = activeParcel.sketchconfig
            clientSettings["SketchFormat"] = activeParcel.sketchformat
            //   getParcelMapAndVector(parcelId, function (parcel) {

            var points = [];
            var bounds = new google.maps.LatLngBounds();
            for (var i = 0; i < activeParcel._mapPoints.length; i++) {
                lat = activeParcel._mapPoints[i].Latitude;
                lng = activeParcel._mapPoints[i].Longitude;
                latlng = new google.maps.LatLng(lat, lng);
                points.push(latlng);
                bounds.extend(latlng);
            }
            polygonCenter = bounds.getCenter();
            activeParcel["LatLngPoints"] = points;
            activeParcel["Bounds"] = bounds;
            var parcel = activeParcel;
            var polyOptions = {
                path: parcel.LatLngPoints,
                strokeColor: "#FF0000",
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: "#FF0000",
                fillOpacity: 0.1,
                visible: true,
                clickable: false
            }
            if (parcelPolygon == null) {
                parcelPolygon = new google.maps.Polygon(polyOptions);
            }
            else {
                if (parcelPolygon.setOptions)
                    parcelPolygon.setOptions(polyOptions);
            }
            parcelPolygon.setMap(map);
            google.maps.event.trigger(map, 'resize');
            drawParcel(0, 0, parcel.SketchVector, parcel.dataExist ? 'white' : 'yellow', function () {
                if (parcel.dataExist) {
                    // currentOverlayPosition = new google.maps.LatLng(parcel.lat, parcel.lng);
                    setOverLayPosition(parcel.lat, parcel.lng, parcel.sketchZoom, parcel.SketchRotate, parcel.SketchData);
                    ParcelDataExist = true;
                    setPositionOverlayIfDataExist();
                    scale = parseFloat(sketchOB.sketchZoom);
                    RotationDegree = parseFloat(sketchOB.sketchRotation);
                    map.setCenter(bounds.getCenter());
                    map.setZoom(parseInt(parcel.MapZoom));
                    zoomSketch(sketchOB.sketchZoom);
                    rotateSketch(parseFloat(sketchOB.sketchRotation));

                }
                else {
                    setOverLayPosition(polygonCenter.lat(), polygonCenter.lng(), 0.6, 0);
                    setPositionOverlayIfDataExist();
                    map.setCenter(bounds.getCenter());
                    map.setZoom(20);
                    zoomSketch(0.6);
                    rotateSketch(0);
                }

                if (callback) callback(parcel);
            });

            //  });

        }
        function setOverLayPosition(lat, lng, zoom, rotation, sketchData) {
            if (sketchData && sketchData != "") {
                currentOverlayPosition = $.parseJSON(sketchData);
                for (var key in currentOverlayPosition) {
                    for (var key2 in currentOverlayPosition[key]) {
                        currentOverlayPosition[key][key2].position = new google.maps.LatLng(currentOverlayPosition[key][key2].position.lat, currentOverlayPosition[key][key2].position.lng);
                    }
                }
                return;
            }
            var sk = sketchRenderer.sketches;
            sk.forEach(function (s, i) {
                currentOverlayPosition["sketch-" + i] = {};
                for (var key in s.sections) {
                    if (currentOverlayPosition["sketch-" + i][key] === undefined)
                        currentOverlayPosition["sketch-" + i][key] = { "position": new google.maps.LatLng(lat, lng), "sketchZoom": zoom, "sketchRotation": rotation }
                }
            })
        }
        function getParcelMapAndVector(parcelid, callback) {
            Parcelid = parcelid;
            $.ajax({
                url: '/sv/getparcel.jrq',
                method: 'POST',
                data: {
                    parcel: parcelid
                },
                success: function (resp) {
                    var points = [];
                    var bounds = new google.maps.LatLngBounds();
                    for (var i = 0; i < resp.Points.length; i++) {
                        lat = resp.Points[i].Latitude;
                        lng = resp.Points[i].Longitude;
                        latlng = new google.maps.LatLng(lat, lng);
                        points.push(latlng);
                        bounds.extend(latlng);
                    }
                    polygonCenter = bounds.getCenter();
                    resp["LatLngPoints"] = points;
                    resp["Bounds"] = bounds;

                    if (callback) callback(resp);
                }
            });
        }

        function drawParcel(latitude, longitude, vector, color, callback) {
            previewSketch(color, 1, function (sketchUrl) {
                $('#sketch').attr('src', sketchUrl);
                $(".overlay").draggable({ containment: "#map", scroll: false });
                $("#map").droppable();
                if (callback) callback();
            });

        }

        function LatLngToPoint(latlng) {
            return overlay.getProjection().fromLatLngToContainerPixel(latlng);
        }
        function PointToLatLng(point) {
            return overlay.getProjection().fromContainerPixelToLatLng(point)
        }
        function moveOverlayBy(topMove, leftMove) {
            var top = parseInt($('.overlay').css("top").replace(/[^-\d\.]/g, ''))
            var left = parseInt($('.overlay').css("left").replace(/[^-\d\.]/g, ''))
            $('.overlay').css({
                'top': top + topMove + 'px',
                'left': left + leftMove + 'px',
                'z-index': '1000'
            });
            setCurrentOverlapPos();
        }
        var sketchOB = null;
        function setPositionOverlayIfDataExist(multisketch) {
            var position, type;
            if (currentOverlayPosition && selectedSection) {
                sketchOB = currentOverlayPosition["sketch-" + selectedSketch][selectedSection]
                position = LatLngToPoint(sketchOB.position);
            }
                //else if (ParcelDataExist) {
                //    position = LatLngToPoint(ParcelCenter);
                //}
            else {
                position = LatLngToPoint(polygonCenter);
            }
            var x = parseFloat(position.x);
            var y = parseFloat(position.y);
            var mapH = parseFloat($('#map').height());
            var divW = parseFloat($('#resizable-wrapper').css("width")) / 2;
            var divH = parseFloat($('#resizable-wrapper').css("height")) / 2;
            y = y - mapH - divH;
            x = (x - divW);

            $('.overlay').css({
                'top': y + 'px',
                'left': x + 'px',
                'z-index': '1000'
            });

            ParcelCenter = PointToLatLng(new google.maps.Point(position.x, position.y));

            if (!adjusting) {
                var ahp = PointToLatLng(new google.maps.Point(position.x - 150, position.y - 150));
                adjustHandle.setPosition(ahp);
                adjustHandleOrigin = LatLngToPoint(ParcelCenter)
            }
            if (multisketch && sketchOB) {
                zoomSketch(sketchOB.sketchZoom);
                rotateSketch(sketchOB.sketchRotation);
            }
        }

        function setPageDimensions() {
            $('#map').height($(window).height());
            $('#map').width($(window).width());
            try {
                setPositionOverlayIfDataExist();
            }
            catch (err) {
            }
        }

        function zoomSketch(zoom, noSketchpostionChange, changeAll) {
            scale = zoom;
            $('#sketch').width(parseInt($('.ccse-img').attr('width')) * zoom);
            $('#sketch').height(parseInt($('.ccse-img').attr('height')) * zoom);
            $('#resizable-wrapper').width(parseInt($('.ccse-img').attr('width')) * zoom);
            $('#resizable-wrapper').height(parseInt($('.ccse-img').attr('height')) * zoom);
            if (!noSketchpostionChange) setPositionOverlayIfDataExist();
            $('.sketch-zoom-value', window.parent.document).html(parseInt(parseFloat(zoom).toFixed(2) * 100) + '%');
            $('.sketch-resize', window.parent.document).val(zoom);
            activeParcel.sketchZoom = zoom;
            if (selectedSection) currentOverlayPosition["sketch-" + selectedSketch][selectedSection].sketchZoom = zoom;
            if (!changeAll) return
            var sk = sketchRenderer.sketches;
            sk.forEach(function (s, i) {
                for (var key in s.sections) {
                    if (currentOverlayPosition["sketch-" + i][key]) currentOverlayPosition["sketch-" + i][key].sketchZoom = zoom
                }
            })

        }

        function rotateSketch(deg) {
            degree = deg;
            $('#sketch').rotate({ animateTo: deg });
            $('.sketch-rotate', window.parent.document).val(degree);
            $('.sketch-rotate-value', window.parent.document).html(degree + '&deg;');
            if (selectedSection) currentOverlayPosition["sketch-" + selectedSketch][selectedSection].sketchRotation = degree;
        }

        function saveForm(comment, flagChecked, validChecked) {
            $.ajax({
                url: '/sv/saveForm.jrq',
                method: 'POST',
                data: {
                    parcel: Parcelid,
                    exist: ParcelDataExist,
                    comment: comment,
                    flag: flagChecked,
                    valid: validChecked
                },
                success: function (response) { }
            });
        }


        function getParcelCenter(doNotSetParcelCenter) {
            var map_width = parseFloat($("#map").css("width"));
            var map_height = parseFloat($("#map").css("height"));
            var left = parseFloat($("#resizable-wrapper").css("left"));
            var top = parseFloat($("#resizable-wrapper").css("top"));
            var x = left + parseFloat($('#sketch').css("width")) / 2;
            var y = map_height + top + parseFloat($('#sketch').css("height")) / 2;
            if (doNotSetParcelCenter)
                return PointToLatLng(new google.maps.Point(x, y));
            ParcelCenter = PointToLatLng(new google.maps.Point(x, y));
            return ParcelCenter;
        }

        function AnchorSketch() {

            var map_width = parseFloat($("#map").css("width"));
            var map_height = parseFloat($("#map").css("height"));
            var left = parseFloat($("#resizable-wrapper").css("left"));
            var top = parseFloat($("#resizable-wrapper").css("top"));
            var x = left + parseFloat($('#sketch').css("width")) / 2;
            var y = map_height + top + parseFloat($('#sketch').css("height")) / 2;
            var canvasSize = parseFloat($('.ccse-img').attr('width'));
            var sketZoom = parseFloat($('#sketch').css("width")) / parseFloat($('.ccse-img').attr('width'));
            var mapZoom = map.zoom;
            ParcelCenter = PointToLatLng(new google.maps.Point(x, y));

            $.ajax({
                url: '/sv/updatesketch.jrq',
                method: 'POST',
                data: {
                    parcel: Parcelid,
                    exist: ParcelDataExist,
                    degree: degree,
                    canvasSize: canvasSize,
                    sketchZoom: sketZoom,
                    mapZoom: mapZoom,
                    lat: ParcelCenter.lat(),
                    lng: ParcelCenter.lng()


                },
                success: function (resp) {
                    new Messi('Sketch anchored.', { autoclose: 700 });
                }
            });
        }

        function MeasurementScale(div, map) {
            var ms = this;
            this.control = div;
            this.on = false;
            this.start = null;
            this.end = null;

            var mA, mB, line;
            mA = new google.maps.Marker();
            mB = new google.maps.Marker();
            line = new google.maps.Polyline({ strokeColor: 'Yellow' });

            this.clear = function () {
                this.start = null;
                this.end = null;
                this.updateGraphics();
                label.innerHTML = '0.0ft';
            }

            this.updateGraphics = function () {
                mA.setMap(null);
                mB.setMap(null);
                line.setMap(null);

                if (ms.start != null) {
                    mA.setPosition(ms.start);
                    mA.setMap(map);
                }

                if (ms.end != null) {
                    mB.setPosition(ms.end);
                    mB.setMap(map);
                }

                if ((ms.start != null) && (ms.end != null)) {
                    line.setPath([ms.start, ms.end]);
                    line.setMap(map);
                    label.innerHTML = Math.round(ms.distance() * 10) / 10 + 'ft';
                }
            }

            div.className = 'measurement-tool measure-off';

            var button = document.createElement('span');
            button.className = 'button';
            div.appendChild(button);

            var label = document.createElement('span');
            label.className = 'distance';
            div.appendChild(label);
            label.innerHTML = '0.0ft'

            google.maps.event.addDomListener(button, 'click', function (e) {
                if (ms.on) {
                    map.setOptions({ draggableCursor: null });
                    div.className = 'measurement-tool measure-off';
                    ms.clear();
                } else {
                    map.setOptions({ draggableCursor: 'crosshair' });
                    div.className = 'measurement-tool measure-on';
                    ms.clear();
                }

                ms.on = !ms.on;
                e.preventDefault();
            });

            this.distance = function () {
                if ((ms.start != null) && (ms.end != null)) {
                    return google.maps.geometry.spherical.computeDistanceBetween(ms.start, ms.end) * 3.28084;
                } else {
                    return 0;
                }
            }

            //this.__defineGetter__("distance", );

        }
        var currentOverlayPosition = {};
        function setCurrentOverlapPos() {
            currentOverlayPosition["sketch-" + selectedSketch][selectedSection].position = getParcelCenter(true)
        }
    </script>
    <style type="text/css">
        .overlay {
            width: 150px;
            height: 150px;
            position: relative;
            z-index: 1000;
            cursor: pointer;
        }
    </style>
</head>
<body style="overflow: hidden; background: #777; margin: 0px;">
    <form id="form1" runat="server">
        <div id="map">
        </div>
        <div class="overlay" id="resizable-wrapper" ondrag="setCurrentOverlapPos()">
            <img id="sketch" />
        </div>
        <div style="display: none;">
            <canvas height="300" width="300" class="ccse-img">
        </canvas>
        </div>
    </form>
</body>
</html>
