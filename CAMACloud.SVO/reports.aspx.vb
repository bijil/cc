﻿Imports Microsoft.Reporting.WebForms

Partial Class reports
    Inherits System.Web.UI.Page
    Public org As DataRow
    Sub LoadReportsHome()
        Dim xsql = <sql>
                SELECT ar.Id,  Name + ' Reports' As GroupTitle
                FROM AppRoles ar
                INNER JOIN OrganizationRoles orr ON ar.Id = orr.RoleId
                WHERE orr.OrganizationId = {0} AND (RoleId IN (SELECT RoleId FROM Reports WHERE RoleId = 8))
                AND ar.ASPRoleName IN ({1})
                ORDER BY Ordinal
            </sql>
        Dim sqlRoles As String = Roles.GetRolesForUser().Select(Function(x) x.ToSqlValue).Aggregate(Function(x, y) x + "," + y)
        Dim sql As String = xsql.Value.FormatString(HttpContext.Current.GetCAMASession.OrganizationId, sqlRoles)
        rptGroups.DataSource = Database.System.GetDataTable(sql)
        rptGroups.DataBind()
    End Sub
    Sub LoadReportSelection()
        Dim response As XElement = XElement.Load(Server.MapPath("reports/config/Reports.xml"))
        Dim temp1 = From uu In response.Descendants("Reports") Select Name = uu.Element("Name").Value, ID = uu.Element("Id").Value
        ddlReport.DataSource = temp1.ToList
        ddlReport.DataTextField = "Name"
        ddlReport.DataValueField = "ID"
        ddlReport.DataBind()
        ddlReport.Items.Insert(0, New ListItem("-- Select --", ""))
    End Sub
    Protected Sub btnGenerate_Click(sender As Object, e As System.EventArgs) Handles btnGenerate.Click
        If ddlReport.SelectedValue <> "" Then
            LoadReport(ddlReport.SelectedValue)
            reportsHome.Visible = False
        Else
            viewer.Visible = False
        End If
    End Sub
    Private Function GetValidationType(ByVal type As String) As Integer
        Dim value As Integer
        If type = "=" Then
            value = 0
        ElseIf type = ">" Then
            value = 2
        ElseIf type = "<" Then
            value = 4
        ElseIf type = ">=" Then
            value = 3
        ElseIf type = "<=" Then
            value = 5
        Else : value = 1
        End If
        Return value
    End Function
    Sub LoadReport(reportId As Integer)
        Dim ShowOnGridFooterList As New ArrayList
        ShowOnGridFooterList.Add("Completed")

        Dim response As XElement = XElement.Load(Server.MapPath("reports/config/Reports.xml"))
        Dim reportPath, dataSetName, dataSource, reportType As String
        Dim reportTitle As String = Nothing


        Dim temp = From uu In response.Elements("Reports") From yy In uu.Elements("Id") Where yy.Value = reportId.ToString Select uu
        ' For Each pp As XElement In temp.Elements
        reportTitle = ""
        reportPath = temp.Elements("ReportPath").Value
        dataSetName = temp.Elements("DataSetName").Value
        dataSource = temp.Elements("DataSource").Value
        reportTitle = temp.Elements("ReportTitle").Value
        reportType = temp.Elements("ReportType").Value
        Dim reportData As DataTable
        Dim parameters As New NameValueCollection
        Dim params As XElement = XElement.Load(Server.MapPath("reports/config/ReportParameters.xml"))
        Dim temp2 = From uu In params.Elements("ReportParameters") From yy In uu.Elements("ReportId") Where yy.Value = reportId.ToString Select uu
        For Each xe As XElement In temp2
            Dim name As String = xe.Element("Name").Value 'pr.GetString("Name")
            Dim inputType As Integer = xe.Element("InputType").Value ' pr.GetIntegerWithDefault("InputType", 0)
            Dim required As String = xe.Element("Required").Value ' pr.GetBoolean("Required")
            Dim value As String = ""
            Dim fc As WebControl = ph.FindControl("report_filter_" + name)
            Select Case inputType
                Case 0
                    value = CType(fc, TextBox).Text
                Case 1
                    value = CType(fc, DropDownList).SelectedValue
                Case 2
                    value = CType(fc, TextBox).Text
                Case 3
                    value = CType(fc, CheckBox).Checked.GetHashCode
                Case 4
                    value = CType(fc, RadioButtonList).SelectedValue
                Case 6
                    'For Flag Based reports
                    value = xe.Element("Value").Value
            End Select
            If value = "" Then
                parameters.Add("@" + name, Nothing)
            Else
                parameters.Add("@" + name, value)
            End If
        Next
        Me.viewer.KeepSessionAlive = False
        Me.viewer.AsyncRendering = False
        reportData = Database.Tenant.GetDataTable(dataSource, CommandType.Text, parameters, 3000)
        If (reportData.Rows.Count <= 0) Then
            viewer.Visible = False
            pnl_nofile.Visible = True
            Exit Sub
        End If
        viewer.Reset()
        viewer.LocalReport.ReportPath = reportPath
        ' viewer.LocalReport.Refresh()
        Try
            If reportId > 23 Then
                Dim param As New ReportParameter("ReportTitle", reportTitle)
                viewer.LocalReport.SetParameters(param)
            End If
        Catch ex As Exception

        End Try
        If dataSetName = "" Or IsNothing(dataSetName) Then
            For Each ds In viewer.LocalReport.GetDataSourceNames
                dataSetName = ds
            Next
        End If
        viewer.LocalReport.DataSources.Clear()
        Dim rs As New ReportDataSource(dataSetName, reportData)
        viewer.LocalReport.DataSources.Add(rs)
        'viewer.LocalReport.Refresh()

        viewer.Visible = True
        pnl_nofile.Visible = False
    End Sub
    Protected Sub ddlReport_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlReport.SelectedIndexChanged
        '-- This is handled in Page_PreInit, since the controls are needed to be added dynamically on the page.
        '-- By doing so, the control values are maintained through postback cycles.
    End Sub
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If ddlReport.SelectedIndex > 0 And HasFilters.Value = "1" Then
            reportParams.Visible = True
            lblReportParameters.Visible = True
        Else
            reportParams.Visible = False
            lblReportParameters.Visible = False
        End If
        If Not IsPostBack Then
            LoadReportsHome()
            viewer.Visible = False
            pnl_nofile.Visible = False
        End If

    End Sub

    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles Me.Init
        If Not IsPostBack Then
            LoadReportSelection()
            ph.Controls.Clear()
            reportParams.Visible = False
            lblReportParameters.Visible = False
            pnl_nofile.Visible = False
        Else
            reportParams.Visible = True
            lblReportParameters.Visible = True
        End If
        Dim srid As String = Request("ctl00$MainContent$ddlReport")
        If srid Is Nothing OrElse srid = "" Then
            ph.Controls.Clear()
            ph.Controls.Add(New LiteralControl("No parameters"))
            reportParams.Visible = False
            lblReportParameters.Visible = False
        Else
            ph.Controls.Clear()
            ph.EnableViewState = "false"
            reportParams.Visible = True
            lblReportParameters.Visible = True
            Dim rid As Integer = srid
            Dim reportType As String
            Dim response As XElement = XElement.Load(Server.MapPath("reports/config/Reports.xml"))
            Dim temp = From uu In response.Elements("Reports") From yy In uu.Elements("Id") Where yy.Value = rid.ToString Select uu
            reportType = temp.Elements("ReportType").Value
            'Dim params As DataTable = Database.System.GetDataTable("SELECT * FROM ReportParameters WHERE ReportId = " & rid)
            Dim params As XElement = XElement.Load(Server.MapPath("reports/config/ReportParameters.xml"))
            Dim temp2 = From uu In params.Elements("ReportParameters") From yy In uu.Elements("ReportId") Where yy.Value = rid.ToString Select uu
            For Each xe As XElement In temp2
                Dim name As String = xe.Element("Name")
                Dim label As String = xe.Element("Label")
                Dim inputType As Integer = xe.Element("InputType")
                Dim source As String = xe.Element("Source")
                Dim required As String = xe.Element("Required")
                Dim Validationfield As String = xe.Element("Validationfield")
                Dim ValidationOperator As String = xe.Element("ValidationOperator")
                Dim ip As Panel = GetInputControl(name, label, inputType, source, Validationfield, ValidationOperator, required, rid)
                ph.Controls.Add(ip)
            Next
            If temp2.Count = 0 Then
                ph.Controls.Add(New LiteralControl("No parameters"))
                HasFilters.Value = 0
            Else
                HasFilters.Value = 1
            End If
        End If
    End Sub
    Function GetInputControl(name As String, label As String, type As Integer, source As String, ByVal Validationfield As String, ByVal ValidationOperator As String, Optional mandatory As Boolean = False, Optional reportId As String = Nothing) As Panel
        Dim container As New Panel
        Dim labelContainer As New Panel
        Dim labelSpan As New Label
        labelSpan.Text = label
        labelContainer.Controls.Add(labelSpan)
        labelContainer.CssClass = "b"
        container.Controls.Add(labelContainer)

        Dim c As WebControl = New TextBox
        Select Case type
            Case 0
                c = New TextBox
                c.Width = New Unit(214)
            Case 1
                c = New DropDownList
                CType(c, DropDownList).FillFromSqlWithDatabase(Database.Tenant, source, True)
                c.Width = New Unit(220)
            Case 2
                c = New TextBox
                c.Attributes.Add("type", "date")
                c.Width = New Unit(214)
                Dim validator As String = "dateValidate(LeftContent_report_filter_" + name + ")"
                c.Attributes.Add("onkeyup", validator)
            Case 3
                c = New CheckBox
                CType(c, CheckBox).Text = label
                labelContainer.Visible = False
                c.Width = New Unit(220)
                mandatory = False
            Case 4
                c = New RadioButtonList
                CType(c, RadioButtonList).Items.Add("Date Ascending")
                CType(c, RadioButtonList).Items.Add("Date Descending")
                c.Width = New Unit(220)
            Case 5
                Dim dt As DataTable = Database.Tenant.GetDataTable(source)
                If dt.Rows.Count > 0 Then
                    c = New Panel
                    c.Attributes("style") = [String].Format("width:218px; height:87px; overflow:auto; z-index:55;")
                    c.Attributes.Add("class", "panelControls")
                    Dim gv = New GridView
                    gv = New GridView
                    gv.Attributes("style") = [String].Format("border-color: #c1c1c1 !important;")
                    gv.ID = "gv_" + name
                    Dim bfieldName As String
                    Dim bHeaderText As String
                    CType(gv, GridView).Columns.Clear()
                    CType(gv, GridView).AutoGenerateColumns = False
                    Dim col As New TemplateField
                    col.ItemTemplate = New MyTemplate()
                    col.HeaderStyle.CssClass = "hidden"
                    col.ItemStyle.CssClass = "rowstyle"
                    CType(gv, GridView).Columns.Add(col)
                    For Each column As DataColumn In dt.Columns
                        bfieldName = column.ColumnName
                        bHeaderText = column.ColumnName
                        Dim bfield As New BoundField()
                        bfield.HeaderText = bHeaderText
                        bfield.DataField = bfieldName
                        bfield.ItemStyle.CssClass = "rowstyle"
                        bfield.HeaderStyle.CssClass = "hidden"
                        If (column.ColumnName <> "Value") Then
                            bfield.ItemStyle.CssClass = "hidden"
                            bfield.FooterStyle.CssClass = "hidden"
                        End If
                        CType(gv, GridView).Columns.Add(bfield)
                    Next
                    CType(gv, GridView).AllowPaging = False
                    CType(gv, GridView).DataSource = Nothing
                    CType(gv, GridView).DataSource = dt
                    CType(gv, GridView).DataBind()
                    c.Controls.Add(CType(gv, GridView))
                Else
                    c = New Panel
                    c.Attributes("style") = [String].Format("width:218px; height:30px;")
                    Dim lbl = New Label
                    lbl = New Label
                    lbl.Attributes("style") = [String].Format("color: red; float: left;padding-top: 10px !important;")
                    lbl.ID = "lbl_" + name
                    lbl.Text = "No value for this selection"
                    c.Controls.Add(CType(lbl, Label))
                End If
                viewer.LocalReport.Refresh()
            Case 6
                'For Flag Based reports
                c = New TextBox
                c.Width = New Unit(214)
                c.Visible = False

        End Select
        c.ID = "report_filter_" + name
        Dim inputContainer As New Panel
        inputContainer.Controls.Add(c)

        If mandatory Then
            inputContainer.Controls.Add(New LiteralControl(" "))
            Dim rfv As New RequiredFieldValidator
            rfv.ControlToValidate = c.ID
            rfv.ValidationGroup = "reportquery"
            inputContainer.Controls.Add(rfv)
        End If
        If Validationfield <> "" Then
            inputContainer.Controls.Add(New LiteralControl(" "))
            Dim cfv As New CompareValidator
            cfv.ControlToValidate = c.ID
            cfv.Operator = GetValidationType(ValidationOperator)
            cfv.ControlToCompare = "report_filter_" + Validationfield
            cfv.ValidationGroup = "reportquery"
            cfv.ErrorMessage = "Please check dates"
            cfv.ForeColor = Drawing.Color.Red
            inputContainer.Controls.Add(cfv)
        End If

        container.Controls.Add(inputContainer)

        container.CssClass = "report-filter-container"
        Return container
    End Function
    Protected Sub rptGroups_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptGroups.ItemDataBound
        Dim roleId As Integer = CType(e.Item.FindControl("RID"), HiddenField).Value
        Dim rptReports As Repeater = e.Item.FindControl("rptReports")
        Dim response As XElement = XElement.Load(Server.MapPath("reports/config/Reports.xml"))
        Dim temp1 = From uu In response.Descendants("Reports") Where uu.Element("RoleId") = "8" Select Name = uu.Element("Name").Value, ID = uu.Element("Id").Value
        rptReports.DataSource = temp1.ToList
        rptReports.DataBind()
    End Sub
    'Protected Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
    '    RunScript("setScreenDimensions();")
    'End Sub

End Class
Public Class MyTemplate
    Implements ITemplate
    Dim itemcount As Integer = 0
    Sub InstantiateIn(ByVal container As Control) _
          Implements ITemplate.InstantiateIn
        Dim cb As New CheckBox
        cb.Checked = False
        container.Controls.Add(cb)
    End Sub
End Class
