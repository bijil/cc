﻿
Partial Class _sitedefault
	Inherits System.Web.UI.Page

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
		If Roles.IsUserInRole("SVAdmin") Or Roles.IsUserInRole("DataSetup") Then
			Response.Redirect("~/svadmin/users.aspx")
		ElseIf Roles.IsUserInRole("SVReviewer") Then
			Response.Redirect("~/sv.aspx")
		ElseIf Roles.IsUserInRole("SVReport") Then
			Response.Redirect("~/reports.aspx")
		Else
			Response.Redirect("~/sv.aspx")
		End If
	End Sub
End Class
