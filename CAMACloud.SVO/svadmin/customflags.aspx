﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/SVAdmin.master" AutoEventWireup="false" Inherits="CAMACloud.SVO.svadmin_customflags" Codebehind="customflags.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Styles" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="LeftPanel" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2>Sketch Review - User-defined Flags</h2>
    <p>
        New Flag: 
        <asp:TextBox runat="server" ID="txtFlagName" Width="100px" MaxLength="20" />
        <asp:Button runat="server" ID="btnAdd" Text="  Add  " />
    </p>
    <asp:GridView runat="server" ID="grid">
        <Columns>
            <asp:BoundField DataField="Name" HeaderText="Flag" ItemStyle-Width="200px" />
            <asp:TemplateField>
                <ItemStyle Width="70px" />
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="lbDelete" Text="Delete" CommandName="DeleteFlag" CommandArgument='<%# Eval("Id") %>' />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>

