﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/SVAdmin.master"
	AutoEventWireup="false" Inherits="CAMACloud.SVO.svadmin_users" Codebehind="users.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Styles" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="Server">
	<script type="text/javascript">

		function showPopup(title) {
			$('.edit-popup').dialog({
				modal: true,
				width: 820,
				resizable: false,
				title: title,
				open: function (type, data) {
					$(this).parent().appendTo("form");
				}
			});
		}

		function hidePopup() {
			$('.edit-popup').dialog('close');
		}

		function setPageLayout(w, h) {
		    $('.screen').height(h); 
		    $('.admin-container-cell').height(h - $('.page-title').height() - 10);
		    $('.screen').css({ 'overflow-y': 'auto' });
		    $('.admin-container-cell').css({ 'overflow-y': 'auto' });
		}

	</script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="LeftPanel" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainContent" runat="Server">
	<h1>
		Manage Users
	</h1>
	<asp:UpdatePanel runat="server" ID="uplGrid">
		<ContentTemplate>
			<div class="link-panel">
				<asp:LinkButton runat="server" ID="lbNewUser" Text="Add New User" OnClientClick="showPopup('Create new CAMA user');" />
			</div>
			<asp:GridView ID="gdvUserDetails" runat="server" AutoGenerateColumns="false" AllowPaging="true"  PageSize="50">
				<Columns>
					<asp:TemplateField>
						<ItemStyle Width="25px" />
						<ItemTemplate>
							<%# Container.DataItemIndex + 1 %>.
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Full Name">
						<ItemStyle Width="250px" />
						<ItemTemplate>
							<%# Eval("Comment")%>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField HeaderText="Login Id" DataField="UserName" ItemStyle-Width="120px">
						<ItemStyle Width="120px"></ItemStyle>
					</asp:BoundField>
					<asp:TemplateField HeaderText="Last Login">
						<ItemStyle Width="100px" />
						<ItemTemplate>
							<%# Eval("LastLoginDate", "{0:MMM d, yyyy}")%>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Last Login IP" ItemStyle-Width="120px" Visible="false">
						<ItemTemplate>
							
						</ItemTemplate>
						<ItemStyle Width="120px"></ItemStyle>
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemStyle Width="90px" />
						<ItemTemplate>
							<asp:LinkButton ID="LinkButton1" runat="server" CommandName="EditUser" Text="Edit"
								CommandArgument='<%# Eval("UserName") %>' OnClientClick="showPopup('Edit User Properties')" />
							<asp:LinkButton ID="lbtnDeleteUser" Text="Delete" runat="server" CommandName="DeleteUser"
								CommandArgument='<%# Eval("UserName")%>' CausesValidation="false"></asp:LinkButton>
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
			</asp:GridView>
		</ContentTemplate>
	</asp:UpdatePanel>
	<div class="edit-popup" style="display: none;">
		<asp:UpdatePanel ID="uplEdit" runat="server">
			<ContentTemplate>
				<asp:Panel runat="server" ID="pnlUserEdit">
					<asp:HiddenField ID="hdnUserId" runat="server" />
					<table class="user-edit-table">
						<tr>
							<td style="width: 8px;"></td>
							<td>
								<h3>
									User Properties</h3>
								<table class="user-edit-form">
									<tr>
										<td>Full Name:</td>
										<td>
											<asp:TextBox ID="txtFullName" runat="server" autocomplete="off"></asp:TextBox><asp:RequiredFieldValidator
												ID="FullNameValidator" runat="server" ForeColor="Red" ErrorMessage="*" ControlToValidate="txtFullName"
												ValidationGroup="UserProp"></asp:RequiredFieldValidator>
										</td>
									</tr>
									<tr>
										<td>Email:</td>
										<td>
											<asp:TextBox ID="txtUserMail" runat="server" autocomplete="off"></asp:TextBox><asp:RequiredFieldValidator
												ID="RequiredFieldValidator1" runat="server" ForeColor="Red" ErrorMessage="*"
												ControlToValidate="txtUserMail" Display="Dynamic" ValidationGroup="UserProp"></asp:RequiredFieldValidator>
											<asp:RegularExpressionValidator ID="rgeEmailValidator" runat="server" ControlToValidate="txtUserMail"
												ErrorMessage="Invalid Email!" ForeColor="#D20B0E" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
												Display="Dynamic" ValidationGroup="UserProp"></asp:RegularExpressionValidator>
										</td>
									</tr>
									<tr>
										<td>Login ID:</td>
										<td>
											<asp:TextBox ID="txtLoginId" runat="server" autocomplete="off"></asp:TextBox><asp:RequiredFieldValidator
												ID="UserNameRequiredFieldValidator" runat="server" ForeColor="Red" ErrorMessage="*"
												ControlToValidate="txtLoginId" Display="Dynamic" ValidationGroup="UserProp"></asp:RequiredFieldValidator>
										</td>
									</tr>
									<tr>
										<td>Password:</td>
										<td>
											<asp:TextBox ID="txtPassword" TextMode="Password" runat="server" autocomplete="off"></asp:TextBox><asp:RequiredFieldValidator
												ID="rfvPassword" runat="server" ForeColor="Red" ErrorMessage="*" ControlToValidate="txtPassword"
												Display="Dynamic" ValidationGroup="UserProp"></asp:RequiredFieldValidator>
										</td>
									</tr>
									<tr>
										<td>Confirm Password:</td>
										<td>
											<asp:TextBox ID="txtPassword2" TextMode="Password" runat="server" autocomplete="off"></asp:TextBox>
											<asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassword"
												ControlToValidate="txtPassword2" ErrorMessage="Mismatch!" ForeColor="#D20B0E"
												ValidationGroup="UserProp"></asp:CompareValidator>
										</td>
									</tr>
								</table>
								<div style="margin-top: 10px; margin-bottom: 15px;">
									<asp:Button runat="server" ID="btnSaveUser" Text="Save User" ValidationGroup="UserProp" />
									<asp:Button runat="server" ID="btnCancel" Text="Cancel" />
								</div>
							</td>
							<td class="v-split" style="width: 12px;"></td>
							<td style="width: 300px;vertical-align:top;">
								<h3>
									Select Roles</h3>
								<asp:CheckBoxList ID="cblRoles" runat="server">
								</asp:CheckBoxList>
							</td>
						</tr>
					</table>
				</asp:Panel>
			</ContentTemplate>
		</asp:UpdatePanel>
	</div>
</asp:Content>
