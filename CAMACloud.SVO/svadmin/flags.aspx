﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/SVAdmin.master"
	AutoEventWireup="false" Inherits="CAMACloud.SVO.svadmin_flags" Codebehind="flags.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Styles" runat="Server">
	<link rel="Stylesheet" href="/App_Static/css/colorPicker.css" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="Server">
	<script type="text/javascript" src="/App_Static/jslib/jquery.colorPicker.js"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="Server">
	<script type="text/javascript">
		function adminPageFunctions() {
			$('.colorpicker').colorPicker();
		}
	</script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="LeftPanel" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainContent" runat="Server">
	<h2>
		Sketch Status Flags - Color Codes</h2>
	<asp:GridView runat="server" ID="grid" AutoGenerateColumns="false" BorderStyle="None"
		GridLines="None" ShowHeader="false" SkinID="NoStyles">
		<RowStyle Height="30px" />
		<Columns>
			<asp:BoundField DataField="Name" HeaderText="Flag Name" ItemStyle-Width="200px" />
			<asp:TemplateField>
				<ItemTemplate>
					<asp:HiddenField runat="server" ID="FID" Value='<%# Eval("Id") %>' />
					<asp:TextBox runat="server" ID="ColorCode" Text='<%# Eval("ColorCode") %>' CssClass="colorpicker" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</asp:GridView>
	<div style="margin-top: 20px;">
		<asp:Button runat="server" ID="btnSave" Text=" Update Changes " Font-Bold="true" />
		<asp:Button runat="server" ID="btnCancel" Text=" Cancel " />
	</div>
</asp:Content>
