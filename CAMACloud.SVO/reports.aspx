﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/CAMASV.master"
	AutoEventWireup="false" Inherits="CAMACloud.SVO.reports" Codebehind="reports.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Styles" runat="Server">
	<link rel="Stylesheet" href="/App_Static/css/reports.css?<%=Now.Ticks%>" type="text/css" />
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="head">
	<script type="text/javascript">
		var rlayout;
		var sht;
		function setPageLayout(w, h) {
			sht = h;
			setTimeout('setReportHeight()', 500);
			$('.report-container-cell').height(h - $('.page-title').height());
		}
		function openReport(rid) {
		    $('#ddlReport').val(rid);
		    $('#btnRefresh').click();
		}
		function setReportHeight() {
			rlayout = $('#ctl00_MainContent_viewer_fixedTable > tbody > tr')[4];
			$(rlayout).attr('class', 'report-view-row');
			var h = sht; 
			var rh = h - $('.page-title').height() - 29 - 11;
			$('.report-view-row > td').height(rh);
		}

		function setPageFunctions() {
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<section class="screen" id="reports">
		<div class="title-line page-title">
			Sketch Validation Reports</div>
		<table class="report-container">
			<tr>
				<td class="report-container-cell report-selection-frame">
					<div class="report-selection">
                        <div class="report-control-head"> Select Report:</div>  &nbsp;
						<div>
                             <asp:Button runat="server" ID="btnRefresh" ClientIDMode="Static" Text="Refresh" style='display:none;'/></div>
						<div>
							<asp:DropDownList runat="server" ID="ddlReport" AutoPostBack="true" ClientIDMode="Static">
							</asp:DropDownList>
                             <asp:RequiredFieldValidator runat="server" ID="rfv_ddlReport" ControlToValidate="ddlReport" ErrorMessage="*" ForeColor="Red" SetFocusOnError="True" ValidationGroup="reportquery" />
						</div>
                         <div class="report-control-head" runat="server" id="lblReportParameters"> Report Parameters:</div> 
                         <asp:Panel runat="server" ID="reportParams" class="div_reportSelect">
                          <asp:HiddenField runat="server" ID="HasFilters" />
                           &nbsp;
                          <asp:PlaceHolder runat="server" ID="ph"></asp:PlaceHolder> <br />
                         <asp:Label ID="Label1" runat="server" Text="" ForeColor="Red"></asp:Label>
                         </asp:Panel>
						<div>
							<asp:Button runat="server" ID="btnGenerate" Text="Generate Report" OnClick="btnGenerate_Click" />
						</div>
					</div>
				</td>
				<td class="report-container-cell report-frame">
                    <asp:Panel runat="server" ID="reportsHome">
                        <div style="padding:15px;padding-top:0px;">
                             <asp:Repeater runat="server" ID="rptGroups">
                                <ItemTemplate>
                                <asp:HiddenField runat="server" ID="RID" Value='<%# Eval("Id") %>' />
                                <div class="clear"></div>
                                <asp:Repeater runat="server" ID="rptReports">
                                       <ItemTemplate>
                                           <a class="report-item" OnClick='openReport(<%# Eval("Id") %>)'>
                                             <div class="icon"></div>
                                             <span class="text"><%# Eval("Name")%></span>
                                           </a>                                         
                                       </ItemTemplate>
                               </asp:Repeater>
                               <div class="clear"></div> 
                              </ItemTemplate>
                            </asp:Repeater>
                        </div>     
                    </asp:Panel>
					<reports:ReportViewer ID="viewer" runat="server" Width="100%" Height="90%" ShowPrintButton="true" ShowZoomControl="true"
						  KeepSessionAlive="false" CssClass="report-viewer" ExportContentDisposition="AlwaysAttachment"  
						ClientIDMode="Static">
					</reports:ReportViewer>
                     <asp:Panel runat="server" ID="pnl_nofile" Visible="false">                 
                              <div style="padding:15px;padding-top:15px;">
                                  <div class="report-viewer">Sorry, it appears there is no data matching your criteria. Please adjust your search parameters and try again.</div>
                              </div>     
                     </asp:Panel>
				</td>
			</tr>
		</table>
	</section>
</asp:Content>
