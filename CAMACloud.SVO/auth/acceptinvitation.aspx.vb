﻿Imports CAMACloud.Data

Partial Class auth_acceptinvitation
	Inherits System.Web.UI.Page

	Public ReadOnly Property GroupId As Integer
		Get
			If Request("uid") Is Nothing Then
				Response.Redirect("~/")
				Return -1
			End If
			Return Database.System.GetIntegerValue("SELECT Id FROM InvitationGroup WHERE UID = " + Request("uid").ToSqlValue)
		End Get
	End Property

	Sub ShowError(message As String)
		pError.InnerHtml = message
		pError.Visible = True
	End Sub

	Protected Sub lbActivate_Click(sender As Object, e As System.EventArgs) Handles lbActivate.Click
		Try
			Dim dr As DataRow = Database.System.GetTopRow("SELECT * FROM InviteeLicenses WHERE InvitationGroupId = " & GroupId & " AND Email = " + txtEmail.Text.ToSqlValue)
			If dr Is Nothing Then
				ShowError("The email address you have given is not an invited one. Please use the exact email address in which you have recieved your invitation.")
				Return
			End If
			If dr("DeviceLicenseId") IsNot DBNull.Value Then
				ShowError("You have already accepted this invitation on another machine.")
				Return
			End If


			Dim maxDateSql = <sql>
                            SELECT DATEDIFF(d, GETUTCDATE(), MIN(Date)) FROM
                            (
                            SELECT DATEADD(d,ExpirationDays,GETUTCDATE()) AS Date FROM InvitationGroup WHERE Id = {0}
                            UNION
                            SELECT EndDate FROM InvitationGroup WHERE Id = {0}
                            ) Dates
                    </sql>

			Dim edays As Integer = Database.System.GetIntegerValue(maxDateSql.Value.SqlFormat(False, GroupId))
			If edays < 1 Then
				ShowError("The validity period of invitation has expired.")
				Return
			End If

			Dim licenseId As Integer = GenerateLicense(edays)

			Dim sql As String = "UPDATE InviteeLicenses SET Name = {1}, Phone = {2}, Jurisdiction = {3}, State = {4}, DeviceLicenseId = {5}, AcceptedDate = GETUTCDATE() WHERE Email = {0} AND InvitationGroupId = " & GroupId
			sql = sql.SqlFormat(True, txtEmail, txtName, txtPhone, txtJurisdiction, txtState, licenseId)

			Database.System.Execute(sql)

			Response.Redirect("~/")
		Catch ex As Exception
			ErrorMailer.ReportException("Demo Invitation", ex, Request)
			ShowError("An unexpected error Occurred.")
		End Try
	End Sub

	Protected Function GenerateLicense(expirationDays As Integer) As Integer
		Dim orgId As Integer = 3

		Dim license As String = RandomString(16)
		Dim otp As Integer = RandomNumber()
		Dim sql As String = String.Format("INSERT INTO DeviceLicense (OrganizationId,LicenseKey,OTP,CreatedDate, ExpirationDays) VALUES({0},'{1}',{2},'{3}', {4});SELECT @@IDENTITY;", orgId, license, otp, Date.Now, expirationDays)
		Dim licenseId As Integer = Database.System.GetIntegerValue(sql)

		CAMACloud.Security.DeviceLicense.Authenticate(license, otp, "invite@camacloud.com", Guid.NewGuid.ToString, HttpContext.Current)

		Return licenseId
	End Function

	Private Function RandomNumber() As Integer
		Dim random As New Random()
		Dim otp = random.Next(100000, 999999)
		Return otp
	End Function

	Private Function RandomString(size As Integer) As String
		Dim builder As New StringBuilder()
		Dim licenseKey As String
		Dim random As New Random()
		Dim ch As Char
		Dim i As Integer
		For i = 0 To size - 1
			ch = Convert.ToChar(Convert.ToInt32((25 * random.NextDouble() + 65)))
			builder.Append(ch)
		Next
		licenseKey = builder.ToString()
		Dim sql As String = String.Format("SELECT count(*) FROM DeviceLicense WHERE LicenseKey = '{0}'", licenseKey)
		If (Database.System.GetIntegerValue(sql) > 0) Then
			licenseKey = RandomString(16)
		End If

		Return licenseKey
	End Function

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Try
				If CAMACloud.Security.DeviceLicense.ValidateRequest(Request.Cookies, HttpContext.Current) Then
					Response.Redirect("~/")
				End If
			Catch ex As Exception

			End Try

		End If
	End Sub
End Class
