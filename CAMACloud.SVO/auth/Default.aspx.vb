﻿Imports CAMACloud
Partial Class _Default
    Inherits System.Web.UI.Page

    'Protected Sub Login_Authenticate(sender As Object, e As System.Web.UI.WebControls.AuthenticateEventArgs) Handles Login.Authenticate
    '	Throw New Exception(Membership.ApplicationName)
    'End Sub

    Protected Sub Login_LoggedIn(sender As Object, e As System.EventArgs) Handles Login.LoggedIn
        CAMASession.RegisterUserOnDeviceLicense(Login.UserName)
        Dim returnUrl As String = FormsAuthentication.DefaultUrl
        If Request("ReturnUrl") <> "" Then
            returnUrl += Request("ReturnUrl").TrimStart("/")
        End If
        Response.Redirect(returnUrl)
    End Sub

    Protected Sub Login_LoginError(sender As Object, e As System.EventArgs) Handles Login.LoginError
        divError.InnerText = "Your login attempt failed. Please try again."
        divError.Visible = True
        ErrorMailer.ReportException(Login.UserName, New Exception("Login attempt failed"), Request)

    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            lblOrgName.Text = HttpContext.Current.GetCAMASession.OrganizationName
            FormsAuthentication.SignOut()
            CloudUserProvider.VerifyOrganizationUsers()
        End If
    End Sub

    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles Me.PreInit
        If Request.UserAgent IsNot Nothing Then
            If Request.UserAgent.Contains("iPad") Then
                Me.ClientTarget = "uplevel"
            End If
        End If
    End Sub
End Class
