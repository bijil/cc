﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/SecurityPage.master"
	AutoEventWireup="false" Inherits="CAMACloud.SVO.auth_acceptinvitation" Codebehind="acceptinvitation.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        html
        {
            position:absolute !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<p>
		Hello Guest,<br />
		Thank you for accepting our invitation to try Data Cloud Solution's on-demand SketchValidation application.  Please complete this form to access a live trial version of this application.</p>
	
	<p class="error" runat="server" visible="false" id="pError">
		You have entered an invalid OTP.</p>
	<div class="login-inputs">
		<div class="input-box">
			<asp:TextBox ID="txtEmail" runat="server" CssClass="text" watermark="Your email address" />
			<asp:RequiredFieldValidator runat="server" ControlToValidate="txtEmail" ErrorMessage="*" ValidationGroup="AcceptInvite" />
		</div>
		<div class="input-box">
			<asp:TextBox ID="txtName" runat="server" CssClass="text" watermark="Your name" />
			<asp:RequiredFieldValidator runat="server" ControlToValidate="txtName" ErrorMessage="*" ValidationGroup="AcceptInvite" />
		</div>
		<div class="input-box">
			<asp:TextBox ID="txtJurisdiction" runat="server" CssClass="text" watermark="Jurisdiction" />
			<asp:RequiredFieldValidator runat="server" ControlToValidate="txtJurisdiction" ErrorMessage="*" ValidationGroup="AcceptInvite" />
		</div>
		<div class="input-box">
			<asp:TextBox ID="txtState" runat="server" CssClass="text" watermark="State" />
			<asp:RequiredFieldValidator runat="server" ControlToValidate="txtState" ErrorMessage="*" ValidationGroup="AcceptInvite" />
		</div>
		<div class="input-box">
			<asp:TextBox ID="txtPhone" runat="server" CssClass="text" watermark="Phone/mobile number" />
			<asp:RequiredFieldValidator runat="server" ControlToValidate="txtPhone" ErrorMessage="*" ValidationGroup="AcceptInvite" />
		</div>

	</div>
	<div style="text-align: center;">
		<asp:LinkButton runat="server" CssClass="login-button" ID="lbActivate" ValidationGroup="AcceptInvite"><span>Accept Invitation</span></asp:LinkButton>
	</div>
	<p>
		<b>Important Note:</b> This is a single-device licensed invitation; per authorized e-mail address.  You cannot sign-in on another machine once you have accepted your invitation on this machine.</p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="Server">
</asp:Content>
