﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/CAMASV.master"
    AutoEventWireup="false" Inherits="CAMACloud.SVO.SV" CodeBehind="sv.aspx.vb" %>
<asp:Content runat="server" ContentPlaceHolderID="head">
    <link rel="Stylesheet" href="/App_Static/slider/winclassic.css" type="text/css" />
    <asp:Literal runat="server" Text="<style type='text/css'>" />
    <asp:Repeater runat="server" ID="rpFlagStyles">
        <itemtemplate>
            label[for="rblStatus_<%# Container.ItemIndex%>"]:before {
                    background: <%# Eval("ColorCode")%>;
                }
        </itemtemplate>
    </asp:Repeater>
    <asp:Literal runat="server" Text="</style>" />
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="MainContent">
      <script type="text/javascript">
          function setPageLayout(w, h) {     
              $('.screen').height(h);
              $('.screen').css({ 'overflow-y': 'auto' });
          }
    </script>
    <style type="text/css">

          #rblStatus label:before {
            content: " ";
            display: inline-block;
            width: 12px;
            height: 12px;
            margin-right: 8px;
            border: 1px solid #333;
        }
        .async_wait  {
                 background: black;
                 opacity: 0.75 ;
                 top: 0%;
                 width: 100%;
                 height: 100%;
                 bottom:0%;
                 position: absolute;
                 display: none;
         }
         .mask {
                 display: none;
                 height: 700px;
                 position: absolute;
                 top: 0;
                 left: 0;
                 background-color: black;
                 width: 300px;
                 z-index: 100;
                 opacity: 0.75;
         }

    </style>

     <section class="screen" id="sketch-validation">
         <div class="async_wait" style="overflow: hidden; cursor:progress; font-family:Verdana; font-size:12px; padding: 15px; z-index: 22;display:none; top: 139.5px; left: 6px; text-align:center;">            
             <div class="process" style="margin-top:143px;">
                 <span><img src="/App_Static/css/images/comploader-old.gif" /></span>
                 <div style="text-align:center;color:white;font:bold;font-size:14px;">Please wait ...</div> </div>
              <div class="mask" style="display: none;">
             </div>
         </div>
        <div class="search-panel">
            <table>
                <tr>
                    <td>
                        <span>Parcel ID:</span>
                    </td>
                    <td>
                        <input type="text" autocorrect="off" autocapitalize="off" maxlength="20" class="src-parcel src-field" />
                    </td>
                    <td>
                        <span>Neighborhood:</span>
                    </td>
                    <td>
                        <input type="text" autocorrect="off" autocapitalize="off" maxlength="20" class="src-nbhd src-field" />
                    </td>
                    <td>
                        <span>Flag Status:</span>
                    </td>
                    <td>
                        <select class="src-flag src-field">
                            <option value="">All</option>
                            <option value="1">Field Inspection</option>
                            <option value="2">Data Entry</option>
                            <option value="3">Further Review</option>
                            <option value="4">Complete</option>
                            <option value="5">Quality Control</option>
                        </select>
                    </td>
                    <td>
                        <span>Reviewed By:</span>
                    </td>
                    <td class="last-item">
                        <asp:DropDownList runat="server" ID="ddlUsers" CssClass="src-user src-field" AppendDataBoundItems="true">
                            <asp:ListItem Text=" " Value=""></asp:ListItem>
                            <asp:ListItem Text="None" Value="None"></asp:ListItem>
                            <asp:ListItem Text="All" Value="All"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td class="search-buttons">
                        <button class="cmd-search">
                            Search</button>
                        <button class="cmd-cancel">
                            Clear</button>
                        <button onclick="showResultsOnMap();return false;">
                            Refresh</button>
                    </td>
                    <td>
                        <div class="toolbar" style="display: none">
                            <table>
                                <tr>
                                    <td>
                                        <button style="color: Red; font-weight: bold; width: 28px;" onclick="closeParcel();return false;" accesskey="c">
                                           &#935;</button></td>
                                    <td>
                                        <button class="cmd-prev" accesskey="p">
                                            &#9668;</button>
                                    </td>
                                    <td>
                                        <button class="cmd-next" accesskey="n">
                                           &#9658;</button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="searchframe">
            <div id="search-map">
            </div>
        </div>
        <div class="resultframe">
            <table class="map-table">
                <asp:HiddenField ID="hdnUser" runat="Server" Value="" />
                <tr>
                    <td class="mapframe">
                        <iframe width="100%" frameborder="0" src="map.aspx?t=12345" id="mapFrame"></iframe>
                    </td>
                    <td class="parcelframe" style='position:relative'>
                        <div class="sketchload-div">
                            Sketches<select class="sketch-select" onchange="previewSketch()"></select>Sections<select onchange="previewSketch()" class="section-select"></select>

                        </div>
                        <table class="parcel-frame-table">
                            <tr>
                                <td class="divider"></td>
                                <td class="dataframe">
                                    <div class="sketch-tools ui-widget-header">
                                        <table style="width: 100%; border-spacing: 0px;">
                                            <tr>
                                                <td>
                                                    <span class="sketch-tool-value">Zoom: <span class="sketch-zoom-value">100%</span></span>&nbsp;
                                                        <input type="range" min="0.25" max="2.5" value="1.0" step="0.01" class="sketch-resize sketch-tool"
                                                            onchange="paintSketch();" />
                                                    <%--<div class="slider" id="sketch-resize" style="display: inline-block;">
                                                            <input class="slider-input" id="sketch-resize-input" name="sketch-resize-input" value="50" />
                                                        </div>--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="sketch-tool-value">Rotate: <span class="sketch-rotate-value">0&deg;</span></span>&nbsp;
                                                        <input type="range" min="-180" max="180" value="0" step="1" class="sketch-rotate sketch-tool"
                                                            onchange="paintSketch();" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="title-line">
                                        Sketch Validation & Review:
                                    </div>
                                    <div class="parcel-header-data">
                                        <div>
                                            Loading ...
                                        </div>
                                    </div>
                                    <div class="aux-data"></div>
                                    <div class="review-form" style="padding-top: 3px">
                                        <div class="review-save">
                                            <button class="cmd-savereview">
                                                Save Review</button>
                                            <button class="cmd-reset">
                                                Reset</button>
                                        </div>
                                        <div>
                                            Notes:
                                        </div>
                                        <textarea style="width: 100%; height: 50px;" class="review-note"></textarea>
                                        <asp:RadioButtonList runat="server" ID="rblStatus" Width="100%" RepeatColumns="3"
                                            RepeatDirection="Horizontal" ClientIDMode="Static">
                                            <asp:ListItem Text="Field Inspection" Value="1" />
                                            <asp:ListItem Text="Data Entry" Value="2" />
                                            <asp:ListItem Text="Further Review" Value="3" />
                                            <asp:ListItem Text="Completed" Value="4" />
                                            <asp:ListItem Text="QC Passed" Value="5" />
                                        </asp:RadioButtonList>
                                        <%--<div class="sv-qc-check">
                                        <asp:CheckBox runat="server" ID="chkQC" Text="Quality Control Passed" Font-Bold="true" />
                                    </div>--%>
                                        <div class="otherflags" id="divOtherFlags" runat="server">
                                            <div class="info">
                                                <b>Other flags:</b>

                                                Click or slide the button to change state.
                                            </div>
                                            <table class="flag-fields">
                                                <asp:Repeater runat="server" ID='rptOtherFlags'>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                                <%#Eval("Name") %>
                                                            </td>
                                                            <td style="width: 50px;">
                                                                <input type="checkbox" flag='<%# Eval("Id") %>' style="width: 40px;" />
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <div class="sketch-big" style="position: absolute; top: 0; height: 435px; width: 440px; display: none; background-repeat: no-repeat; background-color: white; background-position: center;"></div>
                    </td>
                </tr>
            </table>
        </div>
    </section>
    <section class="screen" id="searchresults">
    </section>

    <div class="parcel-header-template" style="display: none;">
        <div class="street-address">
            ${StreetAddress}
        </div>
        <table>
            <tr>
                <td>Parcel ID: <b>${KeyValue1}</b></td>
                <td>Neighborhood: <b>${NBHD}</b></td>
            </tr>
        </table>
        <div runat="server" id="sv_template">
        </div>
    </div>
    <script type="text/javascript" src="/App_Static/slider/range.js"></script>
    <script type="text/javascript" src="/App_Static/slider/timer.js"></script>
    <script type="text/javascript" src="/App_Static/slider/slider.js"></script>
    <script type="text/javascript" src="/App_Static/js/map.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="/App_Static/js/search.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="/App_Static/js/parcel.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="/App_Static/js/commands.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="/App_Static/js/sketch.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript">
        var iResize, iRotate;
        //        function setPageFunctions() {
        //            //iResize = new Slider(document.getElementById("sketch-resize"), document.getElementById("sketch-resize-input"));
        //        }
        var userAccess = document.getElementById("<%=hdnUser.ClientID%>").value;
    </script>
</asp:Content>
