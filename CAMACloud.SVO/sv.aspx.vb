﻿
Partial Class SV
    Inherits System.Web.UI.Page

    Sub LoadUsers()
        Dim cUsers = Membership.GetAllUsers
        Dim removeList As New List(Of String)
        For Each u As MembershipUser In cUsers
            If Not Roles.IsUserInRole(u.UserName, "SVAdmin") AndAlso Not Roles.IsUserInRole(u.UserName, "SVReviewer") Then
                removeList.Add(u.UserName)
            End If
        Next
        For Each un In removeList
            cUsers.Remove(un)
        Next
        ddlUsers.DataSource = cUsers
        ddlUsers.DataTextField = "UserName"
        ddlUsers.DataValueField = "UserName"
        ddlUsers.DataBind()
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadUsers()
            hdnUser.Value = "0"
            If Roles.IsUserInRole(UserName(), "SVQC") Or Roles.IsUserInRole(UserName(), "SVAdmin") Then
                rblStatus.Items(4).Enabled = True
                hdnUser.Value = "1"
            Else
                rblStatus.Items(4).Enabled = False
                hdnUser.Value = "0"
            End If

            rpFlagStyles.DataSource = Database.Tenant.GetDataTable("SELECT * FROM SketchStatusFlags ORDER BY Id")
            rpFlagStyles.DataBind()

            rptOtherFlags.DataSource = Database.Tenant.GetDataTable("SELECT * FROM SketchReviewFlags ORDER BY Id")
            rptOtherFlags.DataBind()
            sv_template.InnerHtml = Database.Tenant.GetStringValue("SELECT templateContent FROM ClientTemplates where Name='sv-auxdata'")

        End If
    End Sub
End Class
