﻿function setPageLayout(w, h) {

}
function navigateParcels(type) {
    if (parcels == undefined) {
        alert('Navigate option not available. Run any query to enable list navigation.');
        return false;
    }
    try {
        if (type == 'next') {
            if (parcelindex < parcels.length - 1) {
                parcelindex++;
                openParcel(parcels[parcelindex].ID);
            }
        }
        else {
            if (parcelindex > 0) {
                parcelindex--;
                openParcel(parcels[parcelindex].ID);
            }
        }
    }
    catch (e) {
        alert(e);
    }
}
function setPageFunctions() {
}
$(window).bind('resize', setScreenLayout);
function keybordShortCuts(key, shiftKey) {
    if (shiftKey) {
        if (key == 38)
            mapFrame.contentWindow.moveOverlayBy(-1, 0)

        else if (key == 40)
            mapFrame.contentWindow.moveOverlayBy(1, 0)

        else if (key == 39)
            mapFrame.contentWindow.moveOverlayBy(0, 1)

        else if (key == 37)
            mapFrame.contentWindow.moveOverlayBy(0, -1)

    }
    else {
        if (!shiftKey && key == 38) {
            $('.sketch-resize').val(parseFloat($('.sketch-resize').val()) + .01);
        }
        else if (!shiftKey && key == 40) {
            $('.sketch-resize').val(parseFloat($('.sketch-resize').val()) - .01);
        }
        else if (!shiftKey && key == 37) {
            $('.sketch-rotate').val(parseFloat($('.sketch-rotate').val()) - 1);
        }
        else if (!shiftKey && key == 39) {
            $('.sketch-rotate').val(parseFloat($('.sketch-rotate').val()) + 1);
        }
        else if (!shiftKey && key == 188) {
            navigateParcels('prev')
        }
        else if (!shiftKey && key == 190) {
            navigateParcels('next')
        }
        else if (!shiftKey && key == 13) {
            saveReview();
        }

    }
}
$(document).keydown(function (e) {
    if (currentInput)
        return true
    var key = e.charCode || e.keyCode;
    keybordShortCuts(key, e.shiftKey)
    if (key == 38 || key == 40 || key == 37 || key == 39) { paintSketch(); e.preventDefault(); return false; }
});

function setScreenLayout() {
    document.body.scrollTop = 0;
    document.body.scrollLeft = 0;

    var mb = 5 + 5 + 2; 			//body margin & border
    var wH = $(window).height(); //window height
    var wW = $(window).width(); 	//window width
    $(document.body).height(wH - mb);
    $(document.body).width(wW - mb);

    var h = wH - ($('header').height() + $('footer').height() + $('menu').height() + mb);
    var w = wW - mb;
    $('.wrapper').height(h);
    $('.wrapper').width(w);

    setPageLayout(w, h);

}

$(function () {
    setScreenLayout();
    $('input').blur(setScreenLayout);
    $('header').bind('click', setScreenLayout);
    $('footer').bind('click', setScreenLayout);
    setPageFunctions();

    if (!theForm) {
        theForm = document.dcs;
    }
});

$(document).bind('touchmove', function (e) {
    e.preventDefault();
});

function showHistory() {
    $('#btnRefreshHistory').click();
    $('.screen').hide();
    $('#history').show();
}

function showSV() {
    $('.screen').hide();
    $('#sketch-validation').show();
}

function showReports() {
    $('.screen').hide();
    $('#reports').show();
}

function openSV(pid) {
    showSV();
    openParcel(pid);
    return false;
}

function processHash() {
    if (window.location.hash != '') {
        var param = window.location.hash.replace('#', '');
        if (parseInt(param) != NaN) {
            openSV(param);
        }
    }
}