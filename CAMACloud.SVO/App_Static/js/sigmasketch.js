﻿function sigmaSketch(color, vector, target, showLabels, showLengths) {
    var ox, oy;
    ox = 0; oy = 0;
    var x, y;
    var max, may, mix, miy;
    max = 0; may = 0; mix = 0; miy = 0;

    var scale = 1;
    var fontSize;

    var canvas = document.getElementById(target);
    var ctx;

    if (!canvas.getContext) {
        alert('Incompatible browser!');
        return;
    }

    ctx = canvas.getContext("2d");

    canvas.width = canvas.width;

    var i;

    var parts = vector.split(',');
    for (i in parts) {
        var part = parts[i];
        if (/(.*?):(.*?)/.test(part)) {
            var hi = part.split(':');
            var head = hi[0];
            var info = hi[1];
            drawLines(ctx, info, true, true);
        }
    }

    var sHeight = may - miy;
    var sWidth = max - mix;
    if (sHeight * 1.1 > canvas.height) {
        scale = canvas.height / sHeight * 0.9;
    }

    var maxHeight = canvas.height * 0.9;
    var maxWidth = canvas.width * 0.9;
    var aspectRatio = parseFloat(sWidth) / parseFloat(sHeight);
    var width, height;

    if ((sWidth < maxWidth) && (sHeight < maxHeight)) {
        scale = 1;
    } else if (aspectRatio > 1) {
        width = maxWidth;
        height = (width / aspectRatio);
        if (height > maxHeight) {
            height = maxHeight;
            width = (height * aspectRatio);
        }
    } else {
        height = maxHeight;
        width = (height * aspectRatio);
        if (width > maxWidth) {
            width = maxWidth;
            height = (width / aspectRatio);
        }
    }

    scale = width / sWidth;

    ctx.lineWidth = 2 / scale;
    ctx.strokeStyle = color;
    fontSize = parseInt(12 / scale);
    if (fontSize < 8)
        fontSize = 8;
    if (fontSize > 18)
        fontSize = 18;
    ctx.font = 'normal ' + fontSize + 'pt Arial';
    ctx.scale(scale, scale);
    oy = (canvas.height / scale - (sHeight)) / 2 - miy;
    ox = (canvas.width / scale - (sWidth)) / 2 - mix;
    for (i in parts) {
        var part = parts[i];
        if (/(.*?):(.*?)/.test(part)) {
            var hi = part.split(':');
            var head = hi[0];
            var info = hi[1];
            if (showLabels)
                drawLabel(ctx, head);
            drawSketch(ctx, info, showLengths);
        }
    }

    function drawLabel(ctx, head) {
        var posString, labels;
        var r1 = /(.*?)\[(.*?)\]/;
        var parts = r1.exec(head);
        labels = parts[1].replace(/\{(.*?)\}/g, '');
        posString = parts[2];
        drawLines(ctx, posString);
        ctx.fillText(labels, x, y);

    }
    function drawSketch(ctx, vector, showLengths) {
        drawLines(ctx, vector, false, false, showLengths);
    }

    function drawLines(ctx, lineString, penOff, detectBounds, showLengths) {
        var penOn = false;
        x = ox; y = oy;
        var cx, cy;
        var ins = lineString.match(/[S]|[UDLR][0-9]*/g);
        ctx.beginPath();
        ctx.moveTo(x, y);
        for (var i in ins) {
            cx = x;
            cy = y;
            var cmd = ins[i];
            var distance = 0;
            if (cmd == "S") {
                penOn = true;
            }
            else {
                var cp = cmd.match(/[UDLR]|[0-9]*/g);
                var dir = cp[0];
                distance = cp[1];
                var dis = parseInt(distance) * 12;
                switch (dir) {
                    case "U":
                        y -= dis;
                        break;
                    case "D":
                        y += dis;
                        break;
                    case "L":
                        x -= dis;
                        break;
                    case "R":
                        x += dis;
                        break;
                }
            }
            if (!penOff && penOn) {
                if (showLengths) {
                    if (distance > 0)
                        ctx.fillText(distance, (x + cx) / 2, (y + cy) / 2);
                }
                ctx.moveTo(cx, cy);
                ctx.lineTo(x, y);
                ctx.stroke();
            } else {
                ctx.moveTo(x, y);
            }

            if (detectBounds) {
                if (x < mix) mix = x;
                if (x > max) max = x;
                if (y < miy) miy = y;
                if (y > may) may = y;
            }

        }
        ctx.stroke();
    }

    return canvas.toDataURL();
}
function getSketchSegments() {
    if (clientSettings["SketchFormat"])
        ccma.Sketching.SketchFormatter = eval('CAMACloud.Sketching.Formatters.' + clientSettings["SketchFormat"]);
    ccma.Sketching.Config = CAMACloud.Sketching.Configs.GetConfigFromSettings();
    if (!ccma.Sketching.SketchFormatter)
        if (ccma.Sketching.Config.formatter)
            ccma.Sketching.SketchFormatter = eval('CAMACloud.Sketching.Formatters.' + ccma.Sketching.Config.formatter);
    var sketchedited = false;
    var processor = ccma.Sketching.SketchFormatter;
    var config = ccma.Sketching.Config;

    if (!processor)
        processor = CAMACloud.Sketching.Formatters.TASketch;
    var sketches = [];
    for (var ski in config.sources) {
        var sksource = config.sources[ski];
        var keyPrefix = sksource.Key ? (sksource.Key + '-') : '';
        var sketchTable = sksource.SketchSource.Table ? eval('activeParcel.' + sksource.SketchSource.Table) : [activeParcel];
        sketchTable = sketchTable && sketchTable.filter ? sketchTable.filter(function (x) { return x.CC_Deleted != true }) : sketchTable;
        // var sketchTableOriginal = sksource.SketchSource.Table ? eval('activeParcel.Original.' + sksource.SketchSource.Table) : [activeParcel.Original];
        
        // var sketchVectorTableOrginal = sksource.VectorSource.Table ? eval('activeParcel.Original.' + sksource.VectorSource.Table) : [activeParcel.Original];
        // var notesTable = sksource.NotesSource && sksource.NotesSource.Table ? eval('activeParcel.' + sksource.NotesSource.Table) : [];
        var sketchKeyField = sksource.SketchSource.KeyField || 'ROWUID';
        for (var i in sketchTable) {
            var pi = sketchTable[i];
            //  var orgTable = sketchTableOriginal[i];
            var sketchKey = keyPrefix + pi[sketchKeyField];
            var labelField = sksource.SketchSource.LabelField.split('/');
            var labelFieldValue = labelField[1] ? (eval('pi.' + labelField[0]) || '???') + '/' + (eval('pi.' + labelField[1]) || '???') : eval('pi.' + labelField[0]) || '???';
            var sketch = {
                uid: sketchKey.toString(),
                sid: pi[sketchKeyField],
                label: (sksource.SketchLabelPrefix ? (sksource.SketchLabelPrefix + ' ') : '') + (labelFieldValue) + (eval('pi.' + sksource.SketchSource.KeyFields[0]) ? (' [' + eval('pi.' + sksource.SketchSource.KeyFields[0]) + ']') : ''),
                sketches: [],
                notes: [],
                config: sksource,
                parentRow: sksource.SketchSource.Table ? pi : null
            }

            for (var vs in sksource.VectorSource) {
            	var vectorSource = sksource.VectorSource[vs];
        		var sketchVectorKeyField = vectorSource.KeyField || 'ROWUID';
            	var sketchVectorTable = vectorSource.Table ? eval('activeParcel.' + vectorSource.Table) : [activeParcel];
        		sketchVectorTable = sketchVectorTable && sketchVectorTable.filter ? sketchVectorTable.filter(function (x) { return x.CC_Deleted != true }) : sketchVectorTable;
	            if (sksource.SketchSource.Table == vectorSource.Table) {
	                var fullVector = eval('pi.' + vectorSource.CommandField);
	                //    var fullVectorOrg = orgTable ? eval('orgTable.' + sksource.VectorSource.CommandField) : null;
	                if (!processor) {
	                    processor = CAMACloud.Sketching.Formatters.Sigma;
	                }
	                var parts = processor.getParts(fullVector);
	                //  var orgParts = fullVectorOrg ? processor.getParts(fullVectorOrg) : null;
	                var pcount = 0;
	                for (var x in parts) {
	                    pcount++;
	                    var part = parts[x];
	                    // var orgPart = orgParts ? orgParts[x] : null;
	                    //var isChanged = true;
	                    //   if (orgPart)
	                    //      isChanged = part.vector != orgPart.vector;
	                    //   var isChanged = part.vector != orgPart.vector;
	                    //  if (isChanged)
	                    //     sketchedited = true;
	                    var sk = {
	                        uid: sketchKey.toString() + "/" + pcount,
	                        label: part.label,
	                        labelPosition: part.labelPosition,
	                        referenceIds: part.referenceIds,
	                        isChanged: false,
	                        vector: part.vector
	                    }
	
	                    var hideSketch = (sksource.HideNullSketches && !sk.vector) || false;
	                    if (!hideSketch) sketch.sketches.push(sk);
	                }
	
	            } else {
	
	                //var filteredDetails = sketchVectorTable.filter(function (ivx) { if (ivx.ParentROWUID) return (ivx.ParentROWUID == pi.ROWUID); else return (eval('ivx.' + sksource.VectorSource.ConnectingFields[0]) == eval('pi.' + sksource.SketchSource.KeyFields[0])); });
	                var filteredDetails = sketchVectorTable.filter(function (ivx) { if (ivx.ParentROWUID) return (ivx.ParentROWUID == pi.ROWUID); else if (pi && pi.constructor.name == "Parcel") { return (ivx.CC_ParcelId == pi.Id); } else return (eval('ivx.' + vectorSource.ConnectingFields[0]) == eval('pi.' + sksource.SketchSource.KeyFields[0])); });
	                //var filteredDetailsOriginal = sketchVectorTableOrginal.filter(function (ivx) { if (ivx.ParentROWUID) return (ivx.ParentROWUID == pi.ROWUID); else if (pi && pi.constructor.name == "Parcel") { return (ivx.CC_ParcelId == pi.Id); } else return (eval('ivx.' + sksource.VectorSource.ConnectingFields[0]) == eval('pi.' + sksource.SketchSource.KeyFields[0])); });
	                if (clientSettings.SketchConfig == 'Wyandott') {
	                	var sks = WyandottVectorDefinition(filteredDetails, isChanged);
	                	sks.forEach(function(sk){ sketch.sketches.push(sk); });
	                }
	                else
		                for (var x in filteredDetails) {
		                    var id = filteredDetails[x];
		                    // var org = filteredDetailsOriginal[x]
		
		                    var vectorKey = id[sketchVectorKeyField];
		                    var isChanged = false;
		                    //if (org)
		                    //    isChanged = eval('id.' + sksource.VectorSource.CommandField) != eval('org.' + sksource.VectorSource.CommandField) || eval('id.' + sksource.VectorSource.LabelField) != eval('org.' + sksource.VectorSource.LabelField);
		                    if (isChanged)
		                        sketchedited = true;
		                    var sk = {
		                        uid: vectorKey.toString(),
		                        label: eval('id.' + vectorSource.LabelField),
		                        labelPosition: eval('id.' + vectorSource.LabelCommandField),
		                        vector: eval('id.' + vectorSource.CommandField),
		                        isChanged: isChanged,
		                        section: id[sksource.sectionFilterField]
		                    }
		
		                    if (vectorSource.IdField) {
		                        sk.rowId = eval('id.' + vectorSource.IdField);
		                    }
		
		                    if (processor.decodeSketch) {
		                        sk.vector = processor.decodeSketch(sk.vector);
		                    }
		
		                    var hideSketch = (sksource.HideNullSketches && !sk.vector) || false;
		                    if (!hideSketch) sketch.sketches.push(sk);
		                }
	            }
			}
            //if (notesTable && notesTable.length > 0) {
            //    var cf = sksource.NotesSource.ConnectingFields;
            //    var filterCondition = "1==1";
            //    cf.forEach(function (f) {
            //        filterCondition += " && ivx." + f + " == pi." + f
            //    });
            //    var filteredNotes = notesTable.filter(function (ivx) { if (ivx.ParentROWUID) return (ivx.ParentROWUID == pi.ROWUID); else return (eval(filterCondition)) });
            //    for (var x in filteredNotes) {
            //        var note = filteredNotes[x];
            //        var n = {
            //            uid: note[sksource.NotesSource.KeyField || 'ROWUID'],
            //            text: note[sksource.NotesSource.TextField],
            //            x: parseInt(note[sksource.NotesSource.PositionXField]) * (sksource.NotesSource.ScaleFactor || 1),
            //            y: parseInt(note[sksource.NotesSource.PositionYField]) * (sksource.NotesSource.ScaleFactor || 1),
            //            lx: parseInt(note[sksource.NotesSource.LineXField]) * (sksource.NotesSource.ScaleFactor || 1),
            //            ly: parseInt(note[sksource.NotesSource.LineYField]) * (sksource.NotesSource.ScaleFactor || 1)
            //        }
            //        sketch.notes.push(n);
            //    }
            //}

            if (sketch.config.AllowSegmentAddition || sketch.sketches.length > 0) {
                sketches.push(sketch);
            }

        }
    }

    return sketches;
}
var clr = 'white';
var selectedSketch = null, selectedSection = null;
function previewSketch(colorCode, type, callback) {
    if (!colorCode) colorCode = clr; clr = colorCode;
    sketchRenderer.scale = 1
    sketchRenderer.resetOrigin();
    var sketches
    if (type == 1) sketches = getSketchSegments();
    sketchRenderer.formatter = ccma.Sketching.SketchFormatter
    sketchRenderer.showOrigin = false;
    sketchRenderer.viewOnly = true;
    if (type == 1) {
        sketchRenderer.open(sketches, null, true, colorCode, 'white');
        top.$('.sketch-select,.section-select').empty().append('<option></option>').empty()
    }
    var sk = sketchRenderer.sketches
    if (type == 1)
        sk.forEach(function (s, i) {
            if (s.vectors.length > 0)
                top.$('.sketch-select').append($('<option>', { value: i, text: s.label }));
        })
    selectedSketch = top.$('.sketch-select').val()
    if (selectedSketch > -1 && type == 1 && selectedSketch)
        for (var key in sk[selectedSketch].sections) {
            top.$('.section-select').append($('<option>', { value: key, text: key }));
        }
    selectedSection = top.$('.section-select').val();
    var selectedSectionIndex = selectedSketch && sk[selectedSketch].sections ? sk[selectedSketch].sections[selectedSection] : null;
    var ab = selectedSectionIndex ? sectionbound(selectedSectionIndex) : { xmax: 0, ymax: 0, xmin: 0, ymin: 0 };
    var scale = 1
    var sHeight = (ab.ymax - ab.ymin) * 10;
    var sWidth = (ab.xmax - ab.xmin) * 10;
    if (sHeight * 1.1 > 300) {
        scale = 3000 / sHeight * 0.9;
    }

    var maxHeight = 300 * 0.9;
    var maxWidth = 300 * 0.9;
    var aspectRatio = parseFloat(sWidth) / parseFloat(sHeight);
    var width, height, url, url2;

    if ((sWidth < maxWidth) && (sHeight < maxHeight)) {
        scale = 1;
        width = maxWidth;
    } else if (aspectRatio > 1) {
        width = maxWidth;
        height = (width / aspectRatio);
        if (height > maxHeight) {
            height = maxHeight;
            width = (height * aspectRatio);
        }
    } else {
        height = maxHeight;
        width = (height * aspectRatio);
        if (width > maxWidth) {
            width = maxWidth;
            height = (width / aspectRatio);
        }
    }

    scale = parseFloat((width / sWidth).toFixed(2))
    sketchRenderer.scale = scale
    sketchRenderer.resizeCanvas(300, 300);
    sketchRenderer.pan(sketchRenderer.formatter.originPosition == "topRight" ? -ab.xmax * scale - 45 : -ab.xmin * 10 * scale - 20, sketchRenderer.formatter.originPosition == "topRight" ? -ab.ymax * scale - 45 : -ab.ymin * 10 * scale - 20);
    sketchRenderer.clearCanvas();
    //sketchRenderer.renderAll();
    if (selectedSketch && selectedSection && selectedSketch > -1 && selectedSection > -1) {
        sk[selectedSketch].sections[selectedSection].forEach(function (vt) {
            vt.render();
        })
    } else
        url = ""
    // if ($('.ccse-img-container .ccse-img').height() < 10000 && $('.ccse-img-container .ccse-img').width() < 10000)
    url = sketchRenderer.toDataURL();
    // $('.ccse-img-container').hide();
    if (type != 1)
        $('#sketch').attr('src', url);
    activeParcel.SketchPreview = url;
    if (!type) setPositionOverlayIfDataExist(true);
    
	sketchRenderer.labelColorCode='black'
	sketchRenderer.colorCode='black'
	sketchRenderer.clearCanvas();
	
	if (selectedSketch && selectedSection && selectedSketch > -1 && selectedSection > -1) 
        sk[selectedSketch].sections[selectedSection].forEach(function (vt) { vt.render(true); })
    else url2 = ""
    
    url2 = sketchRenderer.toDataURL();	
	top.$('.sketch-big').css({ 'background-image': 'url(' + url2 + ')' });
    if (callback) callback(url);
    //activeParcelData.SketchPreview = url;
    //$('.sketch-preview').css({ 'background-image': 'url(' + url + ')' });
}

function sectionbound(sk) {
    var x1 = 0, x2 = 0, y1 = 0, y2 = 0;
    for (x in sk) {
        var b = new PointX(0, 0);
        var v = sk[x];
        var sn = v.startNode;
        while (sn != null) {
            x1 = Math.min(x1, isNaN(sn.p.x) ? 0 : sn.p.x);
            x2 = Math.max(x2, isNaN(sn.p.x) ? 0 : sn.p.x);
            y1 = Math.min(y1, isNaN(sn.p.y) ? 0 : sn.p.y);
            y2 = Math.max(y2, isNaN(sn.p.y) ? 0 : sn.p.y);

            if (sn.isArc) {
                var mp = sn.midpoint;
                x1 = Math.min(x1, isNaN(mp.x) ? 0 : mp.x);
                x2 = Math.max(x2, isNaN(mp.x) ? 0 : mp.x);
                y1 = Math.min(y1, isNaN(mp.y) ? 0 : mp.y);
                y2 = Math.max(y2, isNaN(mp.y) ? 0 : mp.y);
            }

            sn = sn.nextNode;
        }
    }
    return { xmax: x2, ymax: y2, xmin: x1, ymin: y1 };
}

var WyandottVectorDefinition = function (data, isChanged) {
	var vector = '', vectorStart = true, sketches = [], vectorSource = ccma.Sketching.Config.sources[0].VectorSource[0], vectorCounter = 0, startPos = '';
	
	var convertToSketch = function(d, type, isAngled, isInverted) {
		var updownData = isInverted? parseInt(d['CASKLDU' + type]) * -1: parseInt(d['CASKLDU' + type]);
		var leftrightData = isInverted? parseInt(d['CASKLRL' + type]) * -1: parseInt(d['CASKLRL' + type]);
		
		return ' ' + (leftrightData > 0? 'R' + leftrightData: 'L' + leftrightData * -1) + (isAngled? '/': ' ') + (updownData > 0? 'D' + updownData: 'U' + updownData * -1);
	}
	
	data.map(function(d){ if (d.CASKLRECNUM) d.CASKLRECNUM = parseInt(d.CASKLRECNUM); })
	
	data = _.sortBy(data, 'CASKLRECNUM')
	data.forEach(function(d){
		++vectorCounter;
		if (d.CASKLDIAG1 != '+' && d.CASKLDIAG1 != '-') {
			if (vectorStart) {
				startPos = convertToSketch(d, 1);
				vector += ':' + startPos + ' S '
				vectorStart = false;
			}
			else vector += convertToSketch(d, 1);
			vector += convertToSketch(d, 2);
		}
		else {
			vector += convertToSketch(d, 1, true);
			vector += convertToSketch(d, 2);			
		}
		if (d.CASKLSQFT != '0') {
		
			if (vectorCounter == 1) vector += convertToSketch(d, 2, false, true);
			
	     	sketches.push({
		        uid: (d.CASKLPARCEL? d.CASKLPARCEL.toString(): ''),
		        label:  d.CASKLCONS,//[{ Field: 'CASKLCONS', Value: d.CASKLCONS, Description: null, IsEdited: isChanged, hiddenFromEdit: vectorSource.false, hiddenFromShow: false, lookup: null, Caption: vectorSource.labelCaption, ShowCurrentValueOnly: vectorSource.ShowCurrentValueOnly, lookUpQuery: null, splitByDelimiter: null, Target: vectorSource.LabelTarget, UseLookUpNameAsValue: null, IsLargeLookup: false }],
		        vector: d.CASKLCONS + '[-1,-1,' + startPos + ']' + vector,
		        labelPosition: null,
		        isChanged: isChanged,
		        vectorConfig: vectorSource
		    });
			vectorStart = true;
			vector = '';
			startPos = '';
			vectorCounter = 0;
	    }
	});
	console.log(sketches);
	return sketches;
}
//function viewAllSections() {
//    map.setOptions({ draggable: false, zoomControl: false, scrollwheel: false, disableDoubleClickZoom: true }); $('.sketch-tool').attr('disabled', 'disabled');
//    $('#map').after('<div class="overlay1" id="resizable-wrapper" ondrag="setCurrentOverlapPos()"> <img id="sketch1" /></div>')
//    $('#sketch1').attr('src', activeParcel.SketchPreview);
//}