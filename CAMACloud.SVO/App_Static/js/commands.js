﻿$(function () {

    $('.cmd-search').click(function () {
        doSearch();
        return false;
    });

    $('.cmd-cancel').click(function () {
        $('.src-field').val('');
        clearSearch();
        return false;
    });

    $('.cmd-reset').click(function () {
        if (activeParcel != null) {
            openParcel(activeParcel.Id);
        }
        return false;
    });

    $('.cmd-savereview').click(function () {
        saveReview();
        return false;
    });

    $('.cmd-next').click(function () {
        navigateParcels('next')
        return false;
    });

    $('.cmd-prev').click(function () {
        navigateParcels('prev')
        return false;
    });
});
function saveReview() {

    var data = {
        pid: activeParcel.Id,
        notes: $('.review-note').val(),
        flag: '',
        szoom: mapFrame.contentWindow.scale,
        mzoom: mapFrame.contentWindow.mapZoom,
        rotate: mapFrame.contentWindow.degree,
        salat: mapFrame.contentWindow.getParcelCenter().lat(),
        salng: mapFrame.contentWindow.getParcelCenter().lng(),
        status: '',
        sketchData: JSON.stringify(mapFrame.contentWindow.currentOverlayPosition)
    };

    $('.flag-fields :checkbox').each(function () {
        if (this.checked) {
            if (data['flag'] != '') {
                data['flag'] += ',';
            }
            data['flag'] += $(this).attr('flag');
        }
    });

    var fstat = $('#rblStatus input:checked').val();
    if ((fstat == undefined) || (fstat == null)) {
        fstat = '';
    }
    data['status'] = fstat;

    $.ajax({
        url: '/sv/savereview.jrq',
        data: data,
        dataType: 'json',
        success: function (resp) {
            if (activeParcel != null) {
                openParcel(activeParcel.Id);
            }
        },
        error: function (resp) { }
    });


}