﻿var activeParcel;
var activeParcelId = 99988;
var lookup = {}
var polyOptions = {
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillOpacity: 0.1
}

var parcelPolygon = new google.maps.Polygon(polyOptions);



function Parcel(p) {
    var points = [];
    var bounds = new google.maps.LatLngBounds();

    this.ID = 0;
    this.KeyValue1 = "";
    this.KeyValue2 = "";
    this.KeyValue3 = "";
    this.StreetAddress = "";
    this.SketchUrl = "";
    this.Latitude = 0.0;
    this.Longitude = 0.0;
    this.ReviewedBy = "";
    this.ReviewedDate = "";
    this.ColorCode = "";
    this.OutBuildings = [];

    this.CopyFrom = function (obj) {
        for (key in obj) {

            eval('this["' + key + '"] = obj["' + key + '"];');

        }
    };

    this.Points = function () { return points; }
    this.Bounds = function () { return bounds; }
    this.Center = function () { return bounds.getCenter(); }

    this.ShowOnMap = function (locate) {
        if (locate == null)
            locate = true;
        if (map) {
            var polyOptions = {
                path: this.Points(),
                strokeColor: this.ColorCode,
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: this.ColorCode,
                fillOpacity: 0.1,
                visible: true,
                clickable: false
            }
            if (parcelPolygon == null) {
                //parcelPolygon = new google.maps.Polygon(polyOptions);
            }
            else {
                if (parcelPolygon.setOptions)
                    parcelPolygon.setOptions(polyOptions);
            }
            parcelPolygon.setMap(map);
            map.setMapTypeId('CountyImagery');
            if (locate == true)
                this.LocateOnMap();
        }
    }

    this.LocateOnMap = function () {
        map.setCenter(this.Center());
        map.fitBounds(this.Bounds());
    }

    this.CopyFrom(p);
    for (x in p._mapPoints) {
        var pt = p._mapPoints[x];
        latlng = new google.maps.LatLng(pt.Latitude, pt.Longitude);
        points.push(latlng);
        bounds.extend(latlng);
    }
}

function openParcel(pid) {
    console.error('Opening parcel')
    $('.searchframe').hide();
    $('.async_wait').show();

    $.ajax({
        url: '/sv/getparcelfromid.jrq',
        data: { pid: pid },
        dataType: 'json',
        success: function (resp) {
            activeParcel = new Parcel(resp);
            $('.resultframe').show();
            if (mapFrame.contentWindow.openParcel) {
                mapFrame.contentWindow.openParcel(function () {
                    var z = mapFrame.contentWindow.scale;
                    var d = mapFrame.contentWindow.degree;
                    if (z) $('.sketch-resize').val(z);
                    if (d) $('.sketch-zoom-value').html(parseInt(parseFloat(z).toFixed(2) * 100) + '%');
                    if (parcelindex == 0) $('.cmd-prev').attr('disabled', 'disabled')
                    else $('.cmd-prev').removeAttr('disabled')
                    if (parcels && parcelindex + 1 == parcels.length)
                        $('.cmd-next').attr('disabled', 'disabled')
                    else $('.cmd-next').removeAttr('disabled')
                    $('.toolbar').show()
                    mapFrame.contentWindow.setPositionOverlayIfDataExist()
                });
            } 
            window.setTimeout('mapFrame.contentWindow.openParcel();', 100);
            $('.async_wait').hide();
            $('.parcel-header-data').html($('.parcel-header-template').html());
            $('.parcel-header-data').fillTemplate([activeParcel]);
            $('.review-note').val(resp.SketchReviewNote);
            if (resp.MapZoom != 0) {
                mapFrame.contentWindow.map.setZoom(resp.MapZoom);
                map.setZoom(resp.MapZoom);
                mapFrame.contentWindow.mapZoom = resp.MapZoom;
            }
            //            $('.parcelframe').show('fast', function () {
            //                google.maps.event.trigger(map, 'resize');
            //                activeParcel.ShowOnMap();
            //                loadSketch(function () {
            //                    $('.sketch-resize').val(resp.SketchZoom);
            //                    $('.sketch-rotate').val(resp.SketchRotate);
            //                });
            //                if (resp.MapZoom != 0) {
            //                    map.setZoom(resp.MapZoom);
            //                }
            //            });
            $('#rblStatus input').each(function () {
                this.checked = false;
            });
            var rbl = $('#rblStatus input[value="' + resp.SketchFlag + '"]');
            if (rbl.length > 0) {
                rbl[0].checked = true;
            }

            for (x in resp.Flags) {
                var id = resp.Flags[x].Id;
                $('.flag-fields input[flag="' + id + '"]')[0].checked = resp.Flags[x].Value;
                $('.flag-fields input[flag="' + id + '"]').iphoneStyle('refresh');
            }

            if (activeParcel.OutBuildings.length == 0) {
                $('.data-view-switch').hide();
                $('.view-sketch').show();
                $('.view-oby').hide();
            } else {
                $('.data-view-switch').show();
                $('.view-sketch').show();
                $('.view-oby').hide();
                $('.a-view-sketch').hide();
                $('.a-view-oby').show();
            }

            for (x in parcels) {
                if (parcels[x].ID.toString() == activeParcel.Id) {
                    parcels[x].ColorCode = activeParcel.ColorCode;
                }
            }


            for (x in markers) {
                var m = markers[x];
                if (m.ParcelId.toString() == activeParcel.Id) {
                    activeMarker = m;
                    var icon = activeMarker.icon;
                    activeIcon = icon;
                    icon.fillColor = activeParcel.ColorCode;
                    icon.strokeColor = activeParcel.ColorCode;
                    activeMarker.setIcon(icon);
                    activeMarker.setMap(map);
                }
            }
            if (activeParcel.Points().length == 0) {
                alert('No map points available for this parcel');
            }
            if (userAccess == "0" && (activeParcel.ReviewdDate != null || activeParcel.ReviewdDate != ''))
                $("#rblStatus #rblStatus_4").attr('disabled', true);
            else
                $("#rblStatus #rblStatus_4").attr('disabled', false);
        },
        error: function (resp) { }
    });
}

var activeMarker;
var activeIcon;

function closeParcel() {
    if (parcelPolygon != null) {
        parcelPolygon.setMap(null);
    }
    activeParcel = null;
    $('.resultframe').hide();
    $('.searchframe').show();
    $('.toolbar').hide()
    //    $('.parcelframe').hide('fast', function () {
    //        map.setMapTypeId('hybrid');
    //        google.maps.event.trigger(map, 'resize');
    //    });
}


var currentSketch;
var picH, picW, cH, cW;
var drwg;
var canvas;
var picAngle = 0;

function loadSketch(precall) {
    canvas = document.getElementById('sketchvas');
    drwg = canvas.getContext('2d');

    $('.sketch-resize').val(1.0);
    $('.sketch-rotate').val(0);
    cleanCanvas();
    currentSketch = null;
    $('.sketch-tool').attr('disabled', 'disabled');

    if (activeParcel.SketchUrl != null) {
        //var sketchUrl = activeParcel.SketchUrl;
        //sketchUrl = 'http://camacloud.appspot.com/parcelsketch/?assrno=9999821991&subpar=*'
        currentSketch = new Image();
        currentSketch.src = activeParcel.SketchUrl;
        currentSketch.onload = function () {
            if (precall) precall();
            paintSketch();
        }
        currentSketch.onerror = function () {
            alert('There is no sketch available for this parcel.');
            currentSketch = null;
            return;
        }

        $('.sketch-tool').removeAttr('disabled');
    } else {
        console.log('Sketch not loaded.');
        cleanCanvas();
        currentSketch = null;
        $('.sketch-tool').attr('disabled', 'disabled');
    }
}

function cleanCanvas() {
    var sketchvas = document.getElementById('sketchvas');
    sketchvas.height = $('#sketchvas').height();
    sketchvas.width = $('#sketchvas').width();
}

function paintSketch() {
    var zoom = $('.sketch-resize').val();
    var angle = $('.sketch-rotate').val();
    $('.sketch-zoom-value').html(parseInt(parseFloat(zoom).toFixed(2) * 100) + '%');
    $('.sketch-rotate-value').html(angle + '&deg;');

    if (mapFrame) {
        mapFrame.contentWindow.zoomSketch(zoom, true);
        mapFrame.contentWindow.rotateSketch(parseFloat(angle));
    }
}
//$(function () {
//    deferredAction = function () {

//    }
//    if (window.location.hash != '') {
//        var param = window.location.hash.replace('#', '');
//        if (parseInt(param) != NaN) {
//            openSV(param);
//        }
//    }
//})


//function paintSketch() {
//    cleanCanvas();
//    if (currentSketch != null) {
//        var zoom = $('.sketch-resize').val();
//        var angle = $('.sketch-rotate').val();
//        $('.sketch-zoom-value').html(parseInt(parseFloat(zoom).toFixed(2) * 100) + '%');
//        $('.sketch-rotate-value').html(angle + '&deg;');

//        picH = currentSketch.height;
//        picW = currentSketch.width;
//        cH = picH * zoom; cW = picW * zoom;


//        picAngle = angle * 0.0174532925199432957;

//        drwg.translate(canvas.width / 2, canvas.height / 2);
//        drwg.rotate(picAngle);
//        drwg.drawImage(currentSketch, -cW / 2, -cH / 2, cW, cH);

//        $('.sketch-tool').removeAttr('disabled');
//    }
//    else {
//        var zoom = $('.sketch-resize').val();
//        var angle = $('.sketch-rotate').val();
//        $('.sketch-zoom-value').html(parseInt(parseFloat(zoom).toFixed(2) * 100) + '%');
//        $('.sketch-rotate-value').html(angle + '&deg;');
//    }
//}

$('input:text,textarea').focus(function (e) {
    currentInput = this;
}).blur(function () { currentInput = null; })

function loadEssentialTables(callback) {
    $.ajax({
        url: '/sv/loadtables.jrq',
        data: {},
        dataType: 'json',
        success: function (resp) {
            var lv = resp.lookup
            for (var x in lv) {
                var f = lv[x].Source;
                var v = lv[x].Value.trim();
                var n = lv[x].Name;
                var d = lv[x].Description;
                var o = parseInt(lv[x].Ordinal);
                var t = lv[x].SortType;
                if (lookup[f] === undefined) {
                    lookup[f] = {};
                }
                if (lookup[f][v] === undefined)
                    lookup[f][v] = { Name: n, Description: d, Ordinal: o, SortType: t, Id: v };
            }
            if (callback) callback()
        },
        error: function (resp) { }
    });
}
function evalLookup(sourse, value) {
    if (lookup[sourse] && lookup[sourse][value])
        return lookup[sourse][value].Name
    else if (value)
        return value
    else return ""
}
function previewSketch() { mapFrame.contentWindow.previewSketch(); }
//function viewAllSections() { mapFrame.contentWindow.viewAllSections() }