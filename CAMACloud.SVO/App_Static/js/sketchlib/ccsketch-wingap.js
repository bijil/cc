﻿
CAMACloud.Sketching.Formatters.WinGap = {
    name: "WinGap for Georgia",
    saveAllSegments: false,
    allowSegmentAddition: false,
    allowSegmentDeletion: false,
    preventSavingOpenSegments: false,
    allowSegmentAdditionOnMidsketch: true,
    originPosition: "topRight",
    nodeToString: function (n, e) {
        var str = "";
        str = sRound(n.p.x) + ',' + sRound(-n.p.y);
        return str;
    },
    vectorToString: function (v) {
        var str = "";
        if (v.startNode != null) {
            str += v.startNode.vectorString();
            var nn = v.startNode.nextNode;
            while (nn != null) {
                str += ";" + nn.vectorString();
                nn = nn.nextNode;
            }
            str += ';'
        }
        return str;
    },
    vectorToLabelCommand: function (v) {
        //var str = "";
        //var sx = 0, sy = 0, n = 0;
        //if (v.startNode != null) {
        //    var nn = v.startNode.nextNode;
        //    while (nn != null) {
        //        sx += sRound(nn.p.x);
        //        sy += sRound(nn.p.y);
        //        n++;
        //        nn = nn.nextNode;
        //    }
        //    str += ';'
        //}
        //if (n == 0) {
        //    if (v.startNode) {
        //        str = v.startNode.p.x + ',' + (-v.startNode.p.y) + ',' + v.name.trim() + ',0;'
        //    } else {
        //        str = "";
        //    }
        //    return str;
        //}
        if (!v.labelPosition)
            return "";
        var cx = v.labelPosition;
        //var cy = sRound(sy / n);
        str = cx.x + ',' + (-cx.y) + ',' + v.name.trim() + ',0;'
        return str;
    },
    vectorToDimensionCommand: function (v) {
        var str = "";
        var ln;
        if (v.startNode != null) {
            ln = v.startNode;
            var nn = v.startNode.nextNode;
            while (nn != null) {
                var mx = sRound(ln.p.x + nn.p.x) / 2;
                var my = -sRound(ln.p.y + nn.p.y) / 2;
                var l = nn.length;
                var dim = mx + ',' + my + ',' + parseFloat(l.toFixed(0)) + ',0;'
                str += dim;
                ln = nn;
                nn = nn.nextNode;
            }
        }
        return str;
    },
    labelPositionFromString: function (editor, l) {
        if (!l)
            return null
        var parts = l.split(',');
        return new PointX(parseFloat(parts[0]), -(parseFloat(parts[1])))
    },
    vectorFromString: function (editor, s) {
        if (s == null || s == '') {
            var nv = new Vector(editor);
            nv.isClosed = false;
            nv.label = "Blank"
            nv.uid = -1
            return nv;
        }

        var o = editor.origin;
        var p = o.copy();

        var v = new Vector(editor);

        var ins = s.split(';');
        var isStart = true;
        var hasStarted = false;
        var lastIns = null;
        for (var i in ins) {
            if (ins[i] == '') continue;
            var pxy = ins[i].split(",");
            if (lastIns == ins[i]) continue;
            if (pxy.length != 2) continue;

            var px = parseFloat(pxy[0]);
            var py = parseFloat(pxy[1]);

            var p = new PointX(px, -py);

            if (isStart) {
                v.start(p.copy(), true);
                hasStarted = true;
                isStart = false;
            } else if (hasStarted) {
                var q = p.copy(); //.alignToGrid(editor);
                if (!v.startNode.overlaps(q))
                    v.connect(p.copy(), true);
                else
                    v.terminateNode(v.startNode);
            }

            lastIns = ins[i];
        }

        //WinGap: Allow auto-closing of vectors, if they aren't closed by default.
        if (v.startNode)
            if (!v.startNode.overlaps(v.endNode.p.copy())) {
                v.terminateNode(v.startNode);
            }
        v.isClosed = true;
        return v;

    },
    open: function (editor, data) {
        editor.vectors = [];
        editor.sketches = [];
        for (var x in data) {
            var sketch = new Sketch(editor);
            var s = data[x];
            sketch.parentRow = s.parentRow;
            sketch.uid = s.uid;
            sketch.label = s.label;
            sketch.vectors = [];
            sketch.config = s.config || {};
            sketch.sid = s.sid;
            var counter = 0;
            for (var i in s.sketches) {
                counter += 1;
                var sk = s.sketches[i];
                var v = this.vectorFromString(editor, sk.vector);
                v.sketch = sketch;
                v.uid = sk.uid;
                v.index = counter;
                v.name = sk.label;
                v.label = '[' + counter + '] ' + sk.label;
                v.vectorString = sk.vector;
                v.isChanged = sk.isChanged;
                v.labelPosition = this.labelPositionFromString(editor, sk.labelPosition);
                editor.vectors.pop();
                sketch.vectors.push(v);
            }
            sketch.isModified = false;
            editor.loadNotesForSketch(sketch, s.notes);
            editor.sketches.push(sketch);
        }
    }
}
