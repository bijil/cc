﻿CAMACloud.Sketching.Configs = {};

CAMACloud.Sketching.Configs.GetConfigFromSettings = function () {
    if (clientSettings["SketchConfig"] != null) {
        var cfg = eval('CAMACloud.Sketching.Configs.' + clientSettings["SketchConfig"]);
        if (cfg) return cfg;
    }
    var config = { sources: [] };
    var source = {
        SketchSource: { Table: null, LabelField: null, CommandField: null, KeyFields: [] },
        VectorSource: [{ Table: null, LabelField: null, CommandField: null, AreaField: null, PerimeterField: null, ConnectingFields: [] }]
    }

    for (var x in clientSettings) {
        switch (x) {
            case "SketchLabel": source.SketchSource.LabelField = clientSettings[x]; break;
            case "SketchCommandField": source.SketchSource.CommandField = clientSettings[x]; break;
            case "SketchKeyField": source.SketchSource.KeyFields = clientSettings[x] ? clientSettings[x].split(',') : []; break;
            case "SketchTable": source.SketchSource.Table = clientSettings[x]; break;
            case "SketchVectorTable": source.VectorSource[0].Table = clientSettings[x]; break;
            case "SketchVectorLabelField": source.VectorSource[0].LabelField = clientSettings[x]; break;
            case "SketchVectorCommandField": source.VectorSource[0].CommandField = clientSettings[x]; break;
            case "SketchPerimeter": source.VectorSource[0].PerimeterField = clientSettings[x]; break;
            case "SketchAreaField": source.VectorSource[0].AreaField = clientSettings[x]; break;
            case "SketchVectorConnectingField": source.VectorSource[0].ConnectingFields = clientSettings[x] ? clientSettings[x].split(',') : []; break;
        }
    }

    config.sources.push(source);

    return config;

}

CAMACloud.Sketching.Configs.PACS80 = {
    formatter: 'TASketch',
    sources: [
        {
            SketchSource: { Table: "imprv", LabelField: "imprv_state_cd", CommandField: null, KeyFields: ["imprv_id"] },
            VectorSource: [{ Table: "imprv_detail", LabelField: "imprv_det_type_cd", CommandField: "sketch_cmds", ConnectingFields: ["imprv_id"], AreaField: "sketch_area", PerimeterField: null }],
            NotesSource: { Table: "imprv_sketch_note", TextField: "NoteText", PositionXField: "xLocation", PositionYField: "yLocation", LineXField: "xLine", LineYField: "yLine", ConnectingFields: ["imprv_id"], ScaleFactor: 0.01 }
        }
    ]
}

CAMACloud.Sketching.Configs.RapidSketch = {
    formatter: 'RapidSketch',
    sources: [
        {
            Name: "imprv_detail_sketch",
            Key: "PID",
            SketchLabelPrefix: "Det",
            AllowTransferIn: true,
            AllowMultiSegmentAddDelete: true,
            SketchSource: { Table: "imprv", LabelField: "imprv_state_cd", CommandField: null, KeyFields: ["imprv_id"] },
            VectorSource: [{ Table: "imprv_detail", IdField: "imprv_det_id", LabelField: "imprv_det_type_cd", CommandField: "custom_imprv_detail_sketch", ConnectingFields: ["imprv_id"], AreaField: 'sketch_area', PerimeterField: 'perimeter' }],
            NotesSource: { Table: "imprv_sketch_note", TextField: "NoteText", PositionXField: "xLocation", PositionYField: "yLocation", ConnectingFields: ["imprv_id"], ScaleFactor: 0.01 }
        },
        {
            Name: "imprv_sketch",
            Key: "PI",
            SketchLabelPrefix: "Imp",
            HideNullSketches: true,
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
            AllowTransferOut: true,
            SaveAllSegments: true,
            SketchLabelLookup: null,
            SketchLabelLookupX: 'imprv_det_type',
            SketchSource: { Table: "imprv", LabelField: "imprv_state_cd", CommandField: "custom_imprv_sketch", KeyFields: ["imprv_id"] },
            VectorSource: [{ Table: "imprv", IdField: null, LabelField: "imprv_id", CommandField: "custom_imprv_sketch", ConnectingFields: ["imprv_id"], AreaField: null, PerimeterField: null }]
        },
        {
            Name: "property_sketch",
            Key: "P",
            SketchLabelPrefix: "Prop",
            HideNullSketches: true,
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
            AllowTransferOut: true,
            SaveAllSegments: true,
            SketchLabelLookup: null,
            SketchLabelLookupX: 'imprv_det_type',
            SketchSource: { Table: null, LabelField: "prop_id", CommandField: "custom_property_sketch", KeyFields: ["prop_id"] },
            VectorSource: [{ Table: null, IdField: null, LabelField: "prop_id", CommandField: "custom_property_sketch", ConnectingFields: ["prop_id"], AreaField: null, PerimeterField: null }]
        }
    ]
}

CAMACloud.Sketching.Configs.WinGap = {
    formatter: 'WinGap',
    sources: [
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            ClearSegmentOnDeletion: true,
            DeleteBlankSegmentsOnSave: true,
            SketchLabelPrefix: "RES ",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
            SketchLabelLookup: 'IMPLABEL_RES',
            SketchSource: { Table: "REPROP", LabelField: "REPROPKEY", CommandField: null, KeyFields: ["REPROPKEY"] },
            VectorSource: [{ Table: "SKETCH_RES", LabelField: "IMPKEY", CommandField: "VERTICES", ConnectingFields: ["REPROPKEY"], AreaField: "AREA", PerimeterField: "PERIMETER", LabelCommandField: "IMPLABEL", DimensionCommandField: "LABELS" }]
        },
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            ClearSegmentOnDeletion: true,
            DeleteBlankSegmentsOnSave: true,
            SketchLabelPrefix: "COM ",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
            SketchLabelLookup: 'IMPLABEL_COMM',
            SketchSource: { Table: "COMMIMP", LabelField: "IMPROV_NO/SECTION_NO", CommandField: null, KeyFields: ["COMMKEY"] },
            VectorSource: [{ Table: "SKETCH_COMM", LabelField: "IMPKEY", CommandField: "VERTICES", ConnectingFields: ["COMMKEY"], AreaField: "AREA", PerimeterField: "PERIMETER", LabelCommandField: "IMPLABEL", DimensionCommandField: "LABELS" }]
        }
    ]
}

CAMACloud.Sketching.Configs.Sigma = {
    formatter: "Sigma2",
    sources: [
        {
            AllowSegmentAddition: true,
            SketchSource: { Table: "residence", LabelField: "bldg_style", CommandField: "bldg_style", KeyFields: ["recid1"] },
            VectorSource: [{ Table: "residence", LabelField: "bldg_style", CommandField: "sketch", AreaField: null, PerimeterField: null, ConnectingFields: ["recid1"] }],
            LabelAreaFields: {
                "LM": "main_fn_area",
                "LU": "uppr_fn_area",
                "FA": "addl_fn_area",
                "UNF": "unfin_area",
                "B": "bsmt_area",
                "BF": "fn_bsmt_area",
                "S": "att_storg_sf",
                "AC": "carport_sqft",
                "AG": "att_gar_sqft",
                "BIG": "bltin_garage",
                "BZ": "breezeway_sf",
                "CP": "cov_porch_sf",
                "EP": "enc_porch_sf",
                "OP": "opn_porch_sf",
                "SP": "scr_porch_sf",
                "WD": "wood_deck_sf",
                "GP": "gls_porch_sf",
                "X": "attic_area",
                "PAT": "patio_sf",
                "COV": "cover_sf",
                "FAT": "fin_attic_ar"


            },
            AreaSummaryFields: {
                "tot_sqf_l_area": ["LM", "LU"]
            }
        }
    ]
}


CAMACloud.Sketching.Configs.Apex = {
    formatter: 'Apex',
    sources: [
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            AllowLabelEdit: true,
            SketchSource: { Table: 'Building_Sketch', LabelField: "BUILDING_NUMBER", CommandField: null, KeyFields: ["BUILDING_NUMBER"] },
            VectorSource: [{ Table: "Building_Sketch", LabelField: "BUILDING_NUMBER", CommandField: "SKETCH", ConnectingFields: ["BUILDING_NUMBER"], AreaField: null, PerimeterField: null }]
        }
    ]
}

CAMACloud.Sketching.Configs.CustomApex = {
    formatter: 'Apex',
    sources: [
    {
        AllowSegmentAddition: true,
        AllowSegmentDeletion: true,
        InsertDataOnAddition: true,
        AllowLabelEdit: true,
        SketchSource: { Table: 'prs_residential', LabelField: "property_key", CommandField: null, KeyFields: ["property_key"] },
        VectorSource: [{ Table: "prs_residential", LabelField: "structure_id", CommandField: "sketch", ConnectingFields: ["property_key"], AreaField: null, PerimeterField: null }]
    }
    ]
}


CAMACloud.Sketching.Configs.LexurSVG = {
    formatter: 'CCSketch',
    sources: [
    {
        AllowSegmentAddition: true,
        AllowSegmentDeletion: true,
        InsertDataOnAddition: true,
        DoNotAllowOpenSegments: false,
        AllowLabelEdit: true,
        SketchLabelPrefix: "COM ",
        SketchSource: { Table: 'Building', LabelField: "CardNumber", CommandField: null, KeyFields: ["ROWUID"] },
        VectorSource: [{ Table: "Building", LabelField: null, CommandField: "SKETCH", ConnectingFields: ["ROWUID"], AreaField: null, PerimeterField: null }],
        NotesSource: { Table: "Building_SketchNotes", TextField: "SKETCHNOTE", PositionXField: "SKETCHNOTE_X", PositionYField: "SKETCHNOTE_Y", ConnectingFields: ["PropertyNumber", "CardNumber", "BuildingSectionId"], ScaleFactor: 0.01 }
    },
    {
        AllowSegmentAddition: true,
        AllowSegmentDeletion: true,
        InsertDataOnAddition: true,
        DoNotAllowOpenSegments: false,
        AllowLabelEdit: true,
        SketchLabelPrefix: "RES ",
        SketchSource: { Table: 'Dwelling', LabelField: "CardNumber", CommandField: null, KeyFields: ["ROWUID"] },
        VectorSource: [{ Table: "Dwelling", LabelField: null, CommandField: "SKETCH", ConnectingFields: ["ROWUID"], AreaField: null, PerimeterField: null }],
        NotesSource: { Table: "Dwelling_SketchNotes", TextField: "SKETCHNOTE", PositionXField: "SKETCHNOTE_X", PositionYField: "SKETCHNOTE_Y", ConnectingFields: ["PropertyNumber", "CardNumber"], ScaleFactor: 0.01 }
    }
    ]
}
CAMACloud.Sketching.Configs.WarrenOH = {
    formatter: 'WarrenOH',
    sources: [
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            ClearSegmentOnDeletion: true,
            DeleteBlankSegmentsOnSave: true,
            SketchLabelPrefix: "COM ",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
            SketchLabelLookup: 'Sketch_VL',
            SketchSource: { Table: "MAF42", LabelField: "BLDGID", CommandField: null, KeyFields: ["ACCT"] },
            VectorSource: [{ Table: "CCV_56SKCM", LabelField: "SKLABL", CommandField: "SKVCTR", ConnectingFields: ["ACCT"], AreaField: "SKSQFT", PerimeterField: null, LabelCommandField: null, DimensionCommandField: null }]
        },
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            ClearSegmentOnDeletion: true,
            DeleteBlankSegmentsOnSave: true,
            SketchLabelPrefix: "RES ",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
            SketchLabelLookup: 'Sketch_VL',
            SketchSource: { Table: "MAF43", LabelField: "ACCT", CommandField: null, KeyFields: ["ACCT"] },
            VectorSource: [{ Table: "CCV_56SKRS", LabelField: "SKLABL", CommandField: "SKVCTR", ConnectingFields: ["ACCT"], AreaField: "SKSQFT", PerimeterField: null, LabelCommandField: null, DimensionCommandField: null }]
        }
    ]
}
CAMACloud.Sketching.Configs.Cuyahoga = {
    formatter: 'Sigma',
    sources: [
    {
        AllowSegmentAddition: true,
        AllowSegmentDeletion: true,
        InsertDataOnAddition: true,
        AllowLabelEdit: true,
        SketchSource: { Table: null, LabelField: "PARCEL_ID", CommandField: "SKETCH", KeyFields: ["PARCEL_ID"] },
        VectorSource: [{ Table: null, IdField: null, LabelField: "PARCEL_ID", CommandField: "SKETCH", ConnectingFields: ["PARCEL_ID"], AreaField: null, PerimeterField: null }]
    }
    ]
}


CAMACloud.Sketching.Configs.Lucas = {
    formatter: 'Lucas',
    sources: [
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            ClearSegmentOnDeletion: true,
            DeleteBlankSegmentsOnSave: true,
            SketchLabelPrefix: "COM ",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
            SketchLabelLookup: 'Sketch_VL',
            sectionFilterField: 'CARD',
            SketchSource: { Table: null, LabelField: "IASW_ID", CommandField: null, KeyFields: ["PARID"] },
            VectorSource: [{ Table: "ADDN_SV", LabelField: "IASW_ID", CommandField: "VECT", ConnectingFields: ["PARID"], AreaField: "AREA", PerimeterField: null, LabelCommandField: null, DimensionCommandField: null }]
        },
        {
            AllowSegmentAddition: true,
            AllowSegmentDeletion: true,
            ClearSegmentOnDeletion: true,
            DeleteBlankSegmentsOnSave: true,
            SketchLabelPrefix: "RES ",
            AllowLabelEdit: true,
            InsertDataOnAddition: true,
            SketchLabelLookup: 'Sketch_VL',
            sectionFilterField: 'CARD',
            SketchSource: { Table: null, LabelField: "PARID", CommandField: null, KeyFields: ["PARID"] },
            VectorSource: [{ Table: "COMINTEXTFEAT_SV", LabelField: "SF", CommandField: "VECT", ConnectingFields: ["PARID"], AreaField: "AREA", PerimeterField: null, LabelCommandField: null, DimensionCommandField: null }]
        }
    ]
}

CAMACloud.Sketching.Configs.Wyandott = {
    formatter: 'CCSketch',
    sources: [{
        AllowSegmentAddition: true,
        AllowSegmentDeletion: true,
        InsertDataOnAddition: true,
        AllowLabelEdit: true,
        SketchSource: { Table: null, LabelField: 'CASKLPARCEL', CommandField: null, KeyFields: ["CASKLPARCEL"] },
        VectorSource: [{ Table: 'Sketches', LabelField: "CASKLCONS", CommandField: null, ConnectingFields: ["CASKLPARCEL"], AreaField: null, PerimeterField: null, LabelTarget: "code" }]
    }]
}

