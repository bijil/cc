﻿Imports CAMACloud.Exceptions
Imports CAMACloud.Data
Imports System.Data
Imports CAMACloud
Imports System.Web.UI.WebControls
Imports System.Net.Mail
Imports System.Web
'Munir
Namespace Security

    Public Class DeviceLicense

        '
        '	Device Access Keys - MACID, MACAUTH
        '

        Private Shared _syncLock As String = "SYNCLOCK"
        Private Shared _licenseCache As New Hashtable
        Private Shared _lastCleanupTime As DateTime = DateTime.Now

		Private _ts As Integer
        Private _id, _orgId As Integer
        Private _machineKey, _authKey, _otp As String
        Private _isVendor, _isSuper As Boolean
        Private _cachedTime As DateTime
        Private _email As String, _nick As String

        Private Sub New()
            _cachedTime = Now
            ' This class cannot be instantiated from any other place.
        End Sub

        Private Sub New(dr As DataRow)
            _id = dr("Id")
            _machineKey = dr.GetString("MachineKey")
            _authKey = dr.GetString("SessionKey")
            _orgId = dr.GetIntegerWithDefault("OrganizationId", -1)
            _isVendor = dr.GetBoolean("IsVendor")
            _isSuper = dr.GetBoolean("IsSuper")
            _email = dr.GetString("RegisteredEmail")
            _nick = dr.GetString("RegisteredNick")
            _cachedTime = Now

            If Not dr.GetBoolean("IsEnabled") Then
                Throw New Exceptions.InvalidLicenseException(Exceptions.LicenseErrorType.LicenseDisabled)
            End If


        End Sub

        Public Sub VerifyUserInfo()
            If _email.Trim = "" Or _nick.Trim = "" Then
                Throw New Exceptions.LicenseMissingInfoException(HttpContext.Current)
            End If
        End Sub

        Public ReadOnly Property Id As Integer
            Get
                Return _id
            End Get
        End Property

        Public ReadOnly Property OrganizationId As Integer
            Get
                Return _orgId
            End Get
        End Property

        Public ReadOnly Property IsVendor As Boolean
            Get
                Return _isVendor
            End Get
        End Property

        Public ReadOnly Property IsSuper As Boolean
            Get
                Return _isSuper
            End Get
        End Property

        Public ReadOnly Property RegisteredEmail As String
            Get
                Return _email
            End Get
        End Property

        Public ReadOnly Property RegisteredNickName As String
            Get
                Return _nick
            End Get
        End Property

        Public ReadOnly Property IsCacheExpired As Boolean
            Get
                If (Now - _cachedTime).TotalSeconds > 60 Then
                    Return True
                Else
                    Return False
                End If
            End Get
        End Property

        Private Sub RegenerateAuthKey()
            _authKey = Guid.NewGuid.ToString.Replace("-", "").ToLower
            Database.System.UpdateField("DeviceLicense", "SessionKey", _authKey, "Id = " & _id)
        End Sub

        Private Sub GenerateMacId(context As HttpContext)
            Randomize()

            _machineKey = Guid.NewGuid.ToString.Replace("-", "").ToLower + Guid.NewGuid.ToString.Replace("-", "").ToLower
            _authKey = Guid.NewGuid.ToString.Replace("-", "").ToLower

            '"TGWGZTRNQNSKKZTZ"
            Database.System.UpdateField("DeviceLicense", "SessionKey", _authKey, "Id = " & _id)
            Database.System.UpdateField("DeviceLicense", "MachineKey", _machineKey, "Id = " & _id)


            _otp = GenerateNewOTP()
            Database.System.UpdateField("DeviceLicense", "OTP", _otp, "Id = " & _id)
            Database.System.Execute("UPDATE DeviceLicense SET InUse = 1, UsageCounter = COALESCE(UsageCounter,0) + 1 WHERE Id = " + _id.ToString())
            Database.System.Execute("UPDATE DeviceLicense SET FirstUsageDate = GETUTCDATE(), LastUsageDate = GETUTCDATE() WHERE UsageCounter = 1 AND Id = " + _id.ToString())
            Database.System.Execute("UPDATE DeviceLicense SET LastUsageDate = GETUTCDATE() WHERE UsageCounter > 1 AND Id = " + _id.ToString())

            Dim expirationDays As Integer = Database.System.GetIntegerValue("SELECT COALESCE(ExpirationDays,360) FROM DeviceLicense WHERE Id = " + _id.ToString())

            Dim auth As New HttpCookie("MACAUTH")
            auth.Domain = ApplicationSettings.AuthenticationDomain
            auth.Expires = Now.AddMinutes(20)
            auth.Value = _authKey
            context.Response.Cookies.Add(auth)
            context.Response.AddHeader("CC-MACAUTH", _authKey)

            Dim macid As New HttpCookie("MACID")
            macid.Domain = ApplicationSettings.AuthenticationDomain
            macid.Expires = Now.AddDays(expirationDays)
            macid.Value = _machineKey
            context.Response.Cookies.Add(macid)
            context.Response.AddHeader("CC-MACID", _machineKey)


        End Sub

        Public Shared Function GetLicense() As DeviceLicense
            Dim cookies = HttpContext.Current.Request.Cookies
            Dim context = HttpContext.Current
            'Database.System.Execute("UPDATE DeviceLicense SET EmailedTo= 'GetLicense' WHERE Id=7710")
            If (Not (context.Request.Cookies("MACID") Is Nothing)) Then
                If (context.GetKeyValue("MACID") <> "") Then
                    'Database.System.Execute("UPDATE DeviceLicense SET EmailedTo = '" + context.GetKeyValue("MACID") + "' WHERE Id=1")
                    'Database.System.Execute("UPDATE DeviceLicense SET RegisteredMobile = '9001' where MachineKey='" + context.GetKeyValue("MACID") + "'")
                    Dim drCC As DataRow = Database.System.GetTopRow("SELECT * FROM DeviceLicense WHERE MachineKey = '" + context.GetKeyValue("MACID") + "'")

                    If (drCC IsNot Nothing) Then
                        If (drCC.GetString("ExpirationDays") = "") Then
                            'Database.System.Execute("UPDATE DeviceLicense SET RegisteredMobile = '9002' WHERE MachineKey = '" + context.GetKeyValue("MACID") + "'")
                            context.Request.Cookies("MACID").Expires = Now.AddYears(1)
                        End If
                    End If
                End If
            End If
            If context.HasKeys("MACAUTH") Then
                Dim _macAuth As String = context.GetKeyValue("MACAUTH")
                If Not _licenseCache.ContainsKey(_macAuth) Then
                    Dim dl As DeviceLicense = GetLicenseFromMacAuth(HttpContext.Current, _macAuth)
                    SyncLock _syncLock
                        _licenseCache.Add(_macAuth, dl)
                    End SyncLock
                End If
                Return _licenseCache(_macAuth)
            ElseIf context.HasKeys("MACID") Then
                Dim _macId As String = context.GetKeyValue("MACID")
                Return CreateAndCacheMacAuthKey(HttpContext.Current, _macId)
            End If
            Return Nothing
        End Function

        Public Shared Function ValidateRequest(app As HttpApplication) As Boolean

            Dim request As HttpRequest = app.Request
            Dim response As HttpResponse = app.Response
            Dim session As HttpSessionState = HttpContext.Current.Session
            Dim cookies As HttpCookieCollection = app.Request.Cookies
            Dim _macId As String = HttpContext.Current.GetKeyValue("MACID")
            If Configuration.WebConfigurationManager.AppSettings("ApplicationCode") = "MA" Then
                If (request("macid") <> "") Then
                    CreateMacIdCookie(app.Context, request("macid"), request.UserAgent, _macId)
                End If
            End If

            If (Now - _lastCleanupTime).TotalMinutes > 20 Then
                Dim xx = "1"
                SyncLock xx
                    If (Now - _lastCleanupTime).TotalMinutes > 20 Then
                        Dim keys As New List(Of String)
                        For Each key In _licenseCache.Keys
                            If CType(_licenseCache(key), DeviceLicense).IsCacheExpired Then
                                keys.Add(key)
                            End If
                        Next
                        For Each key In keys
                            _licenseCache.Remove(key)
                        Next
                        _lastCleanupTime = DateTime.Now
                    End If
                End SyncLock
            End If

            ' Return True Or (Regex.IsMatch(request.UserAgent, "OS 11_")

            If Not ClientSettings.IsLicenseProtected Then
                Return True
            End If

            If ValidateRequest(cookies, app.Context) Then

                Dim drDL As DataRow = Database.System.GetTopRow("SELECT * FROM DeviceLicense WHERE MachineKey = '" + _macId + "'")
                Dim hostName As String = app.Request.Url.Host
                Dim hostParts() As String = hostName.Split(".")
                Dim isSuper As Boolean = drDL.GetBoolean("IsSuper")
                Dim isVendor As Boolean = drDL.GetBoolean("IsVendor")
                Dim isSketchPro As Boolean = drDL.GetBoolean("EnabledSketchPro")
                If Not isSuper And Not isVendor And Configuration.WebConfigurationManager.AppSettings("ApplicationCode") = "MA" Then
                    Dim LicenceorgId As Integer = drDL.GetInteger("OrganizationId")
                    Dim orgId = Database.System.GetIntegerValueOrInvalid("SELECT Id FROM Organization WHERE MAHostKey = {0}".SqlFormatString(hostParts(0)))
                    If orgId = -1 Then
                        Throw New Exceptions.InvalidUrlException(app.Context)
                    End If
                    If LicenceorgId <> orgId Then
                        Throw New Exceptions.UnlicensedAccessException(app.Context)
                    End If
                End If
                If isVendor And Configuration.WebConfigurationManager.AppSettings("ApplicationCode") = "MA" Then
                    Dim LicenceVendorId As Integer = drDL.GetInteger("VendorId")
                    Dim vendorId = Database.System.GetIntegerValueOrInvalid("SELECT VendorId FROM Organization WHERE MAHostKey = {0}".SqlFormatString(hostParts(0)))
                    If vendorId = -1 Then
                        Throw New Exceptions.InvalidUrlException(app.Context)
                    End If
                    Dim supportVendorId = Database.System.GetIntegerValueOrInvalid("SELECT SupportVendorId FROM Organization WHERE MAHostKey = {0}".SqlFormatString(hostParts(0)))
                    If LicenceVendorId <> vendorId AndAlso LicenceVendorId <> supportVendorId Then
                        Throw New Exceptions.UnlicensedAccessException(app.Context)
                    End If
                End If
                If drDL IsNot Nothing Then
                    Dim deviceFilter As String = drDL.GetString("DeviceFilter")
                    If hostName.Contains("sketcher.camacloud.com") Then
                        Dim browserSupport As Boolean = IIf((request.BrowserMatches(AGENT_CHROME) Or request.BrowserMatches(" Safari/") Or request.BrowserMatches(AGENT_IPAD)), True, False)
                        If (((isSuper Or deviceFilter = "SketchPro") And Not browserSupport) Or ((deviceFilter = "Chrome" And isSketchPro) And Not request.BrowserMatches(AGENT_CHROME)) Or ((deviceFilter = "iPad" And isSketchPro) And Not request.BrowserMatches(AGENT_IPAD))) Then
                            Throw New Exceptions.UnlicensedApplicationAccessException(app.Context)
                        ElseIf ((deviceFilter = "Chrome" Or deviceFilter = "iPad") And Not isSketchPro) Then
                            Throw New Exceptions.UnlicensedAccessException(app.Context)
                        ElseIf ({"MAChrome", "MACommon"}.Contains(deviceFilter) Or deviceFilter = "") Then
                            Throw New Exceptions.UnlicensedAccessException(app.Context)
                        End If
                    Else
                        Select Case deviceFilter
                            Case "iPad"
                                If Configuration.WebConfigurationManager.AppSettings("ApplicationCode") <> "MA" Or Not request.BrowserMatches(AGENT_IPAD) Then
                                    Throw New Exceptions.UnlicensedApplicationAccessException(app.Context)
                                End If
                            Case "Chrome"
                                If Configuration.WebConfigurationManager.AppSettings("ApplicationCode") = "MA" Then
                                    Throw New Exceptions.UnlicensedApplicationAccessException(app.Context)
                                End If
                            Case "MAChrome"
                                If Configuration.WebConfigurationManager.AppSettings("ApplicationCode") <> "MA" Or Not request.BrowserMatches(AGENT_CHROME) Then
                                    Throw New Exceptions.UnlicensedApplicationAccessException(app.Context)
                                End If
                            Case "MACommon"
                                If Configuration.WebConfigurationManager.AppSettings("ApplicationCode") <> "MA" Then
                                    Throw New Exceptions.UnlicensedApplicationAccessException(app.Context)
                                End If
                            Case "SketchPro"
                                If Not hostName.Contains("sketcher.camacloud.com") Then
                                    Throw New Exceptions.UnlicensedApplicationAccessException(app.Context)
                                End If
                            Case Else
                        End Select
                    End If
                End If
                Return True
            Else
                Throw New Exceptions.UnlicensedAccessException(app.Context)
            End If
        End Function

        Public Shared Sub ClearLicenseCache(context As HttpContext)
            Try
                If context.HasKeys("MACAUTH") Then
                    Dim _macAuth As String = context.GetKeyValue("MACAUTH")

                    If _licenseCache.ContainsKey(_macAuth) Then
                        _licenseCache.Remove(_macAuth)
                    Else
                        _licenseCache.Clear()
                    End If
                End If
            Catch ex As Exception
                Throw New Exception("Error on clearing license cache", ex)
            End Try
        End Sub

        Public Shared Function ValidateRequest(cookies As HttpCookieCollection, context As HttpContext) As Boolean
            Dim dl As DeviceLicense
            If context.HasKeys("MACID") AndAlso context.HasKeys("MACAUTH") Then
                Dim _macId As String = context.GetKeyValue("MACID")
                SyncLock _macId
                    Dim _macAuth As String = context.GetKeyValue("MACAUTH")

                    If _licenseCache.ContainsKey(_macAuth) Then
                        dl = _licenseCache(_macAuth)
                        'The License is available in the application cache, and requires no further checking.
                        If dl.IsCacheExpired Then
                            'License cache expired, need to re-evaluate
                            _licenseCache.Remove(_macAuth)
                            dl = Nothing
                        Else
                            dl.VerifyUserInfo()
                            Return True
                        End If
                    End If

                    'Check in database if the MACAUTH exists, and if exists, add to app cache.
                    dl = GetLicenseFromMacAuth(context, _macAuth)
                    dl.VerifyUserInfo()
                    If Not _licenseCache.ContainsKey(_macAuth) Then
                        SyncLock _syncLock
                            _licenseCache.Add(_macAuth, dl)
                        End SyncLock
                    End If
                    Return True
                End SyncLock
            End If

            If context.HasKeys("MACID") Then
                dl = CreateAndCacheMacAuthKey(context, context.GetKeyValue("MACID"))
                dl.VerifyUserInfo()
                Return True

            End If
            Return False
        End Function

        Private Shared Function GetLicenseFromMacAuth(context As HttpContext, macAuth As String) As DeviceLicense
            Dim drMA As DataRow = Database.System.GetTopRow("SELECT * FROM DeviceLicense WHERE SessionKey = '" + macAuth + "'")
            If drMA IsNot Nothing Then
                Dim dl As New DeviceLicense(drMA)
                Return dl
            Else
                Dim cookies As HttpCookieCollection = context.Request.Cookies
                If context.HasKeys("MACID") Then
                    Return CreateAndCacheMacAuthKey(context, context.GetKeyValue("MACID"))
                Else
                    Throw New Exceptions.LicenseBreachException(context)
                End If

            End If
        End Function

        Private Shared Function CreateAndCacheMacAuthKey(context As HttpContext, macId As String) As DeviceLicense
            Dim drMA As DataRow = Database.System.GetTopRow("SELECT * FROM DeviceLicense WHERE MachineKey = '" + macId + "'")
            If drMA IsNot Nothing Then
                Dim dl As New DeviceLicense(drMA)
                dl.RegenerateAuthKey()

                'If (drMA("ExpirationDays").ToString() = "") Then
                '    context.Request.Cookies("MACID").Expires = Now.AddYears(1)
                'End If

                Dim macIdCookie = context.Request.Cookies("MACID")
                macIdCookie.Expires = Now.AddYears(1)
                macIdCookie.Domain = ApplicationSettings.AuthenticationDomain
                context.Response.Cookies.Add(macIdCookie)

                Dim auth As New HttpCookie("MACAUTH")
                auth.Domain = ApplicationSettings.AuthenticationDomain
                auth.Expires = Now.AddMinutes(20)
                'auth.Expires = Now.AddDays(1)
                auth.Value = dl._authKey
                context.Response.Cookies.Add(auth)
                Return dl
            Else
                Throw New Exceptions.LicenseBreachException(context, macId)
            End If
        End Function
        Public Shared Function CreateMacIdCookie(context As HttpContext, macId As String, ua As String, ByRef _macId As String)
            If (macId <> "" And HttpContext.Current.Request.Cookies("MACID") Is Nothing And (ua.Contains("OS 11") Or ua.Contains("OS 12"))) Then
                Dim drLi As DataRow = Database.System.GetTopRow("Select * FROM DeviceLicense WHERE MachineKey = '" + macId + "'")
                If drLi Is Nothing Then
                Else
                    Dim macid_cookie As New HttpCookie("MACID")
                    macid_cookie.Domain = ApplicationSettings.AuthenticationDomain
                    macid_cookie.Expires = Now.AddYears(1)
                    'auth.Expires = Now.AddDays(1)
                    macid_cookie.Value = macId
                    context.Response.Cookies.Add(macid_cookie)
                    _macId = macId
                End If
            End If

        End Function
        Public Shared Sub Authenticate(licenseKey As String, otp As String, email As String, nick As String, context As HttpContext, Optional returnHost As String = "")
            licenseKey = licenseKey.Trim.Replace(" ", "").Replace("-", "")
            Dim drLi As DataRow = Database.System.GetTopRow("SELECT * FROM DeviceLicense WHERE LicenseKey = '" + licenseKey + "'")
            If drLi Is Nothing Then
                Throw New Exceptions.InvalidLicenseException(Exceptions.LicenseErrorType.InvalidLicenseKey)
            End If
            If Not drLi.GetBoolean("IsEnabled") Then
                RegisterInvalidAttempt(licenseKey)
                Throw New Exceptions.InvalidLicenseException(Exceptions.LicenseErrorType.LicenseDisabled)
            End If
            If drLi.GetBoolean("InUse") Then
                RegisterInvalidAttempt(licenseKey)
                Throw New Exceptions.InvalidLicenseException(Exceptions.LicenseErrorType.LicenseInUse)
            End If
            If drLi.GetString("OTP") <> otp Then
                RegisterInvalidAttempt(licenseKey)
                Throw New Exceptions.InvalidLicenseException(Exceptions.LicenseErrorType.InvalidOTP)
            End If
            Dim deviceFilter As String = drLi.GetString("DeviceFilter")
            Dim userAgent As String = context.Request.UserAgent
            Dim isSuper As Integer = drLi.GetBoolean("IsSuper")
            Dim isSketchPro As Boolean = drLi.GetBoolean("EnabledSketchPro")

            'If returnHost.Contains("sketcher.camacloud.com") Then
            '    If ((isSuper Or deviceFilter = "SketchPro" Or (deviceFilter = "Chrome" And isSketchPro)) And Not context.Request.BrowserMatches(AGENT_CHROME)) Then
            '        RegisterInvalidAttempt(licenseKey)
            '        Throw New Exceptions.InvalidLicenseException(Exceptions.LicenseErrorType.InvalidTarget)
            '    ElseIf (deviceFilter = "Chrome" And Not isSketchPro) Then
            '        RegisterInvalidAttempt(licenseKey)
            '        Throw New Exceptions.InvalidLicenseException(Exceptions.LicenseErrorType.InvalidTarget)
            '    ElseIf ({"iPad", "MAChrome", "MACommon"}.Contains(deviceFilter)) Then
            '        RegisterInvalidAttempt(licenseKey)
            '        Throw New Exceptions.InvalidLicenseException(Exceptions.LicenseErrorType.InvalidTarget)
            '    End If
            'Else
            Select Case deviceFilter
                Case "iPad"
                    If Not (context.Request.BrowserMatches(AGENT_IPAD)) Then
                        RegisterInvalidAttempt(licenseKey)
                        Throw New Exceptions.InvalidLicenseException(Exceptions.LicenseErrorType.InvalidTarget)
                    End If
                Case "Chrome"
                    If Not context.Request.BrowserMatches(AGENT_CHROME) Then
                        RegisterInvalidAttempt(licenseKey)
                        Throw New Exceptions.InvalidLicenseException(Exceptions.LicenseErrorType.InvalidConsoleTarget)
                    End If
                Case "MAChrome"
                    If Not context.Request.BrowserMatches(AGENT_CHROME) Or context.Request.BrowserMatches(AGENT_IPAD) Then
                        RegisterInvalidAttempt(licenseKey)
                        Throw New Exceptions.InvalidLicenseException(Exceptions.LicenseErrorType.InvalidTarget)
                    End If
                Case "MACommon"
                    If Not (context.Request.BrowserMatches(AGENT_CHROME) Or context.Request.BrowserMatches(AGENT_IPAD)) Then
                        RegisterInvalidAttempt(licenseKey)
                        Throw New Exceptions.InvalidLicenseException(Exceptions.LicenseErrorType.InvalidTarget)
                    End If
                Case "SketchPro"
                    If Not (context.Request.BrowserMatches(AGENT_CHROME) Or context.Request.BrowserMatches(" Safari/") Or context.Request.BrowserMatches(AGENT_IPAD)) Then
                        RegisterInvalidAttempt(licenseKey)
                        Throw New Exceptions.InvalidLicenseException(Exceptions.LicenseErrorType.InvalidTarget)
                    End If
                Case Else
            End Select

            Dim nickTest As Integer = 0
            Dim orgId As Integer = drLi.GetIntegerWithDefault("OrganizationId", -1)
            Dim vendorId As Integer = drLi.GetIntegerWithDefault("VendorId", -1)
            If isSuper Then
                nickTest = Database.System.GetIntegerValue("SELECT COUNT(*) FROM DeviceLicense WHERE IsSuper = 1 AND LicenseKey <> {0} AND RegisteredNick = {1}".SqlFormatString(licenseKey, nick))
            ElseIf vendorId <> -1 Then
                nickTest = Database.System.GetIntegerValue("SELECT COUNT(*) FROM DeviceLicense WHERE VendorId = {0} AND LicenseKey <> {1} AND RegisteredNick = {2}".SqlFormatString(vendorId, licenseKey, nick))
            ElseIf orgId <> -1 Then
                nickTest = Database.System.GetIntegerValue("SELECT COUNT(*) FROM DeviceLicense WHERE IsSuper = 0 AND IsVendor = 0 AND OrganizationId = {0} AND LicenseKey <> {1} AND RegisteredNick = {2}".SqlFormatString(orgId, licenseKey, nick))
            Else
                nickTest = 0
            End If
            If nickTest > 0 Then
                Throw New Exceptions.InvalidLicenseException(Exceptions.LicenseErrorType.InvalidIdentifiers)
            End If
            Dim dl As New DeviceLicense(drLi)
            dl.RegisterUserInformation(email, "", nick)
            dl.GenerateMacId(context)
        End Sub

        Public Sub RegisterUserInformation(email As String, mobile As String, nick As String)
            Dim drLi As DataRow = Database.System.GetTopRow("SELECT * FROM DeviceLicense WHERE ID = '" & _id & "'")
            If drLi Is Nothing Then
                Exit Sub
            End If
            Dim licenseKey As String = drLi.GetString("LicenseKey")
            Dim nickTest As Integer = 0
            Dim orgId As Integer = drLi.GetIntegerWithDefault("OrganizationId", -1)
            Dim vendorId As Integer = drLi.GetIntegerWithDefault("VendorId", -1)
            Dim isSuper As Integer = drLi.GetBoolean("IsSuper")
            If isSuper Then
                nickTest = Database.System.GetIntegerValue("SELECT COUNT(*) FROM DeviceLicense WHERE IsSuper = 1 AND LicenseKey <> {0} AND RegisteredNick = {1}".SqlFormatString(licenseKey, nick))
            ElseIf vendorId <> -1 Then
                nickTest = Database.System.GetIntegerValue("SELECT COUNT(*) FROM DeviceLicense WHERE VendorId = {0} AND LicenseKey <> {1} AND RegisteredNick = {2}".SqlFormatString(vendorId, licenseKey, nick))
            ElseIf orgId <> -1 Then
                nickTest = Database.System.GetIntegerValue("SELECT COUNT(*) FROM DeviceLicense WHERE IsSuper = 0 AND IsVendor = 0 AND OrganizationId = {0} AND LicenseKey <> {1} AND RegisteredNick = {2}".SqlFormatString(orgId, licenseKey, nick))
            Else
                nickTest = 0
            End If
            If nickTest > 0 Then
                Throw New Exceptions.InvalidLicenseException(Exceptions.LicenseErrorType.InvalidIdentifiers)
            End If

            Database.System.Execute("UPDATE DeviceLicense SET RegisteredEmail = {1}, RegisteredMobile = {2}, RegisteredNick = {3} WHERE Id = {0}".SqlFormatString(_id, email, mobile, nick))
            Database.System.Execute("INSERT INTO LicenseAudittrail SELECT OrganizationId,NULL,GETUTCDATE() AS EventTime,DeviceFilter,LicenseKey,'" + HttpContext.Current.Request.ClientIPAddress + "' AS IPAddress,'License Key installed' AS Comment FROM DeviceLicense where id = {0}".SqlFormatString(_id) )
        End Sub

        Public Shared Sub RegisterInvalidAttempt(licenseKey As String)
            Database.System.Execute("UPDATE DeviceLicense SET LastUsedLoginId = NULL, LastUsedIPAddress = {2}, InvalidAttempts = InvalidAttempts + 1 WHERE LicenseKey = {0}".SqlFormatString(licenseKey, "", HttpContext.Current.Request.ClientIPAddress))
        End Sub

        Public Shared Sub ClearLicenseAuthCookies()
            Dim auth1 As New HttpCookie("MACAUTH")
            auth1.Domain = ApplicationSettings.AuthenticationDomain
            auth1.Expires = Now.AddYears(-20)
            auth1.Value = ""
            HttpContext.Current.Response.Cookies.Add(auth1)

            'Dim auth As New HttpCookie("MACAUTH")
            'auth.Domain = ApplicationSettings.AuthenticationDomain
            'auth.Expires = Now.AddYears(-20)
            'auth.Value = ""
            'HttpContext.Current.Response.Cookies.Add(auth)
        End Sub

        'Private Shared Sub writetoFile(text As String)
        '    If System.IO.File.Exists("c:\test.txt") = False Then

        '        File.Create("c:\test.txt").Dispose()
        '    End If
        '    Dim objWriter As New System.IO.StreamWriter("c:\test.txt", True)
        '    objWriter.Write(text)
        '    objWriter.WriteLine()
        '    objWriter.WriteLine("################### " & DateTime.Now & "################### ")
        '    objWriter.Close()
        'End Sub

        Public Shared Function GenerateNewOTP() As String
            Dim random As New Random()
            Dim otp = random.Next(100000, 999999)
            ''''Okay to Repeat
            Return otp.ToString()
        End Function


        Public Shared Function GenerateNewLicenseKey(size As Integer) As String
            Dim builder As New Text.StringBuilder()
            Dim licenseKey As String
            Dim random As New Random()
            Dim ch As Char
            Dim i As Integer
            For i = 0 To size - 1
                ch = Convert.ToChar(Convert.ToInt32((25 * random.NextDouble() + 65)))
                builder.Append(ch)
            Next
            licenseKey = builder.ToString()
            Dim sql As String = String.Format("SELECT count(*) FROM DeviceLicense WHERE LicenseKey = '{0}'", licenseKey)
            If (Database.System.GetIntegerValue(sql) > 0) Then
                licenseKey = GenerateNewLicenseKey(size)
            End If

            Return licenseKey
        End Function

        Public Shared Function GenerateScode(num As Long, Optional base As Integer = 26) As String
            Dim charRef As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            Dim q As Long = 0, r As Long = 0
            q = num
            Dim out As String = ""
            While q > 0
                r = q Mod base
                q = q \ base
                out = charRef(r) + out
            End While
            Return out
        End Function


        Public Shared Sub SendLicenseEmail(page As Page, grid As GridView, email As String, organizationName As String, Optional isVendor As Boolean = False)
            If email.Trim = "" Then
                Return
            End If
            Try
                Dim ta As New MailAddress(email)
            Catch ex As Exception
                page.Alert("Invalid email address.")
                Return
            End Try

            If organizationName Is Nothing Then
                organizationName = "Master Access"
            End If

            If organizationName <> "" And isVendor Then
                organizationName += " (Master Access)"
            End If

            Dim mail As New MailMessage
            mail.From = New MailAddress(ClientSettings.DefaultSender, "Access@CAMACloud")
            mail.To.Add(email)
            mail.Bcc.Add("consultsarath+cclicense@gmail.com")
            mail.Subject = Now.ToString("yyyyMMdd\HHHmmss") + " : CAMA Cloud License Keys for " + organizationName
            mail.Body = composeLicenseEmail(grid, organizationName)
            mail.IsBodyHtml = True

            For Each gr As GridViewRow In grid.Rows
                If gr.GetChecked("chkSel") Then
                    Database.System.Execute("UPDATE DeviceLicense SET EmailedTo = " + email.ToSqlValue + " WHERE Id = " + gr.GetHiddenValue("LID"))
                End If
            Next

            AWSMailer.SendMail(mail)
            page.Alert("An email has been sent to " + email + " with the selected license keys and their OTPs.")
        End Sub

        Private Shared Function composeLicenseEmail(grid As GridView, organizationName As String) As String
            Dim mailContent As XElement = <html>
                                              <body>
                                                  <div style="font-family:Arial;width:600px;">
                                                      <h1 style="border-bottom:1px solid #075297;color:#075297;">CAMA Cloud - Access License</h1>
                                                      <h2></h2>
                                                      <p style="font-size:10pt;">Greetings from CAMA Cloud,</p>
                                                      <p style="font-size:10pt;">The following license keys can be used to access CAMA Cloud applications for <b>Organization</b>.</p>
                                                      <table style="font-size:10pt;font-family:Arial;">
                                                          <thead>
                                                              <tr>
                                                                  <td style='font-weight:bold;width:200px;'>License Key</td>
                                                                  <td style='font-weight:bold;width:80px;'>OTP</td>
                                                                  <td style='font-weight:bold;width:90px;'>Device</td>
                                                              </tr>
                                                          </thead>
                                                          <tbody>

                                                          </tbody>
                                                      </table>
                                                      <p style="font-size:10pt;">The OTP or One-Time-Password can be used only for authenticating a new device/browser. This implies that each license/OTP pair is valid to be used on one target device only.</p>
                                                      <p style="font-size:10pt;">Sincererly, <br/>CAMA Cloud</p>
                                                      <hr/>
                                                      <p style="font-size:8pt;margin-top:0px;">
                                                          <a href="http://www.camacloud.com/">CAMA Cloud<sup style="font-size:6pt;">SM</sup></a> powered by <a href="http://www.datacloudsolutions.net/">Data Cloud Solutions, LLC.</a>
                                                      </p>
                                                  </div>
                                              </body>
                                          </html>

            Dim dataFormat As XElement = <tr>
                                             <td></td>
                                             <td></td>
                                             <td></td>
                                         </tr>


            mailContent...<body>...<div>...<p>(1)...<b>.Value = organizationName
            mailContent...<body>...<div>...<h2>.Value = organizationName
            For Each gr As GridViewRow In grid.Rows
                If gr.GetChecked("chkSel") Then
                    Dim dataLine As XElement = XElement.Parse(dataFormat.ToString)
                    dataLine...<td>(0).Value = gr.GetHiddenValue("LKEY")
                    dataLine...<td>(1).Value = gr.GetHiddenValue("OTP")
                    dataLine...<td>(2).Value = gr.GetHiddenValue("DEVICE")
                    mailContent...<body>...<div>...<table>...<tbody>(0).Add(dataLine)
                End If
            Next

            Return mailContent.ToString
        End Function

        Private Shared Function newPara(line As String) As String
            Return "<p>" + line + "</p>"
        End Function


    End Class




End Namespace
