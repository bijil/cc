﻿Imports CAMACloud.Data
Namespace Authentication
    Public Class ApplicationAccess

        Public Shared Function ConnectToClientDatabase(accessKey As String, passKey As String, MACAddress As String) As Database
            Dim org As DataRow = Database.System.GetTopRow("SELECT OrganizationId,ServerMACAddress FROM ApplicationAccess WHERE  IsEnabled=1 AND  AccessKey = {0} AND PassKey = {1}".SqlFormatString(accessKey, passKey))
            If org Is Nothing Then
                Throw New Exceptions.AuthenticationFailedException
            End If
            Dim organizationId As Integer = org.GetInteger("OrganizationId")
            Dim DBMACAddress As String = org.GetString("ServerMACAddress")
            If (DBMACAddress = "" And MACAddress <> "") Then
                Database.System.Execute("update ApplicationAccess set ServerMACAddress={2} where AccessKey = {0} AND PassKey = {1}".SqlFormatString(accessKey, passKey, MACAddress))
            ElseIf (MACAddress = "" And DBMACAddress <> "") Or (DBMACAddress <> "" AndAlso MACAddress <> DBMACAddress)
                Throw New Exceptions.BadRequestException("Authentication failed! The machine you are trying to connect does not match records. Please contact support.", Nothing)
            End If
            Dim db = New Database(organizationId, Database.GetConnectionStringFromOrganization(organizationId))
            db.RefreshName()
            Return db
        End Function

    End Class
End Namespace

