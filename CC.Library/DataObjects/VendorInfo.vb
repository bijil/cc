﻿Imports CAMACloud.Data
Imports System.Configuration

Public Class VendorInfo

    Public Shared _id As Integer

    Public ReadOnly Property ID As Integer
        Get
            Return _id
        End Get
    End Property

    Public Name As String
    Public CommonName As String
    Public Website As String
    Public Info As String
    Public ShowVendorInfo As Boolean = False
    Public VendorLogoPath As String

    Public Sub New()
    End Sub

    Public Sub New(id As Integer)
        _id = id
    End Sub

    Public Shared Property MAVendorLogo As String
        Get
            Dim logo = PropertyValue("MAVendorLogo")
            If String.IsNullOrEmpty(logo) Then
                ' Return "/applogo/ccma-small.png"
                Return "/App_Static/images/ccma-small.png"

            Else
                Return logo
            End If
        End Get
        Set(value As String)
            PropertyValue("MAVendorLogo") = value
        End Set
    End Property

    Public Shared Property PropertyValue(path As String) As String
        Get
            Try
                Dim dr As DataRow = Database.System.GetTopRow("SELECT VendorLogoPath FROM Vendor WHERE id = " + _id.ToString.ToSqlValue)
                If dr Is Nothing Then
                    Return Nothing
                End If
                Return dr.GetString("VendorLogoPath")
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(VendorLogoPath As String)
            Dim dr As DataRow
            Try
                dr = Database.System.GetTopRow("SELECT VendorLogoPath FROM Vendor WHERE id = " + _id.ToString.ToSqlValue)
            Catch ex As Exceptions.MissingTableException
                dr = Nothing
            End Try
            Database.System.UpdateField("Vendor", "VendorLogoPath", VendorLogoPath, "id = " + _id.ToString.ToSqlValue)
        End Set
    End Property
End Class
