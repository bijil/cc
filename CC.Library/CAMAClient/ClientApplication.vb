﻿Imports CAMACloud.Data

Public Enum ApplicationStatus As Integer
    Empty = 0
    SchemaCreated = 1
    DataLoaded = 2
    Active = 3
    Locked = 9
End Enum

Public Class ClientApplication

    Private _db As Database

    Public Sub New(targetDatabase As Database)
        _db = targetDatabase
    End Sub

    Public Property PropertyValue(name As String) As Object
        Get
            Try
                Dim value = _db.GetTopRow("SELECT [" + name + "] FROM Application")(0)
                If value Is DBNull.Value Then
                    Return Nothing
                End If
                Return value
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As Object)
            Dim dbValue = value
            If TypeOf value Is String Then
                dbValue = CStr(value).ToSqlValue.Trim("'")
            ElseIf TypeOf value Is Date Then
                dbValue = CDate(value).ToSqlValue.Trim("'")
            ElseIf TypeOf value Is Boolean Then
                dbValue = CBool(value).GetHashCode
            End If
            _db.UpdateField("Application", name, dbValue, "1 = 1")
        End Set
    End Property
    
    Public ReadOnly Property PropertyLabelValue(name As String) As Object
        Get
            Try
                Dim value = _db.GetTopRow("SELECT [" + name + "] FROM Application")(0)
                If value Is DBNull.Value Then
                    Return Nothing
                End If
                value = _db.GetTopRow("select DisplayLabel from DataSourceField df inner join DataSourceTable dt on df.TableId = dt.Id where dt.CC_TargetTable = 'ParcelData' and df.Name = {0}".SqlFormatString(value))(0) 
                If value Is DBNull.Value Then
                    Return Nothing
                End If
                Return value
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
    End Property


    Public Property ApplicationStatus As ApplicationStatus
        Get
            Return Coalesce(PropertyValue("ApplicationStatus"), ApplicationStatus.Empty)
        End Get
        Set(value As ApplicationStatus)
            PropertyValue("ApplicationStatus") = value.GetHashCode
        End Set
    End Property

    Public Property SchemaVersion As Integer
        Get
            Return Coalesce(PropertyValue("SchemaVersion"), 1)
        End Get
        Set(value As Integer)
            PropertyValue("SchemaVersion") = value
        End Set
    End Property


    Public ReadOnly Property SchemaVersionDate As DateTime
        Get
            Dim lastSchemaUpdatedDate As DateTime = Coalesce(PropertyValue("LastSchemaUpdate"), DateTime.MinValue)
            If lastSchemaUpdatedDate = DateTime.MinValue Then
                Dim dt As Date = Date.UtcNow
                PropertyValue("LastSchemaUpdate") = dt
                Return dt
            Else
                Return lastSchemaUpdatedDate
            End If
        End Get
    End Property

    Public ReadOnly Property NewSchemaVersion As Integer
        Get
            PropertyValue("SchemaVersion") = Coalesce(PropertyValue("SchemaVersion"), 0) + 1
            PropertyValue("LastSchemaUpdate") = Date.UtcNow
            Return PropertyValue("SchemaVersion")
        End Get
    End Property

    Public Property ParcelTable As String
        Get
            Return PropertyValue("ParcelTable")
        End Get
        Set(value As String)
            PropertyValue("ParcelTable") = value
        End Set
    End Property

    Public Property NeighborhoodTable As String
        Get
            Return PropertyValue("NeighborhoodTable")
        End Get
        Set(value As String)
            PropertyValue("NeighborhoodTable") = value
        End Set
    End Property

    Public Property NeighborhoodField As String
        Get
            Return PropertyValue("NeighborhoodField")
        End Get
        Set(value As String)
            PropertyValue("NeighborhoodField") = value
        End Set
    End Property


    Public Property NeighborhoodNameField As String
        Get
            Return PropertyValue("NeighborhoodNameField")
        End Get
        Set(value As String)
            PropertyValue("NeighborhoodNameField") = value
        End Set
    End Property

    Public Property NeighborhoodNumberField As String
        Get
            Return PropertyValue("NeighborhoodNumberField")
        End Get
        Set(value As String)
            PropertyValue("NeighborhoodNumberField") = value
        End Set
    End Property

    Public Property KeyField(index As Integer) As String
        Get
            If index < 1 And index > 10 Then
                Throw New Exception("An invalid key field entry is attempted on the Application settings.")
            End If
            Return PropertyValue("KeyField" & index)
        End Get
        Set(value As String)
            PropertyValue("KeyField" & index) = value
        End Set
    End Property

    Public Property StreetAddressTable As String
        Get
            Return PropertyValue("StreetAddressTable")
        End Get
        Set(value As String)
            PropertyValue("StreetAddressTable") = value
        End Set
    End Property

    Public Property StreetAddressField As String
        Get
            Return PropertyValue("StreetAddressField")
        End Get
        Set(value As String)
            PropertyValue("StreetAddressField") = value
        End Set
    End Property

    Public Property StreetAddressFilter As String
        Get
            Return PropertyValue("StreetAddressFilter")
        End Get
        Set(value As String)
            PropertyValue("StreetAddressFilter") = value
        End Set
    End Property

    Public ReadOnly Property DataModel As String
        Get
            Return PropertyValue("DataModel")
        End Get
    End Property

    Public ReadOnly Property IsAPISyncModel As Boolean
        Get
            Dim model As String = DataModel
            If model Is Nothing OrElse model.Trim = String.Empty Then
                Return False
            End If
            If model = "TASYNC" Or model = "APISYNC" Then
                Return True
            End If
            Return False
        End Get
    End Property

    Public ReadOnly Property AlternateKeyfield As String
        Get
            Return PropertyValue("Alternatekeyfieldvalue")
        End Get
    End Property
    Public ReadOnly Property AlternateKeyLabelfield As String
        Get
            Return PropertyLabelValue("Alternatekeyfieldvalue")
        End Get
    End Property
    Public ReadOnly Property ShowKeyValue1 As Boolean
        Get
            Return PropertyValue("ShowKeyValue1")
        End Get
    End Property
    Public ReadOnly Property ShowAlternateField As Boolean
        Get
            Return PropertyValue("ShowAlternateField")
        End Get
    End Property

End Class
