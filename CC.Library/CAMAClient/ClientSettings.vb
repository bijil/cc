﻿Imports CAMACloud.Data
Imports System.Configuration

Public Class ClientSettings

    Private Shared Sub _createSettingsTable()
        Database.Tenant.Execute("CREATE TABLE ClientSettings (Name VARCHAR(50), Value NVARCHAR(500));")
    End Sub

    Public Shared Property PropertyValue(name As String) As String
        Get
            'If OverrideTenantSettings = True Then
            '    Return Nothing
            'End If
            Try
                Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT Value FROM ClientSettings WHERE Name = '" + name + "'")
                If dr Is Nothing Then
                    Return Nothing
                End If
                Return dr.GetString("Value")
            Catch mtx As Exceptions.MissingTableException
                _createSettingsTable()
                Return ""
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As String)
            Dim dr As DataRow
            Try
                dr = Database.Tenant.GetTopRow("SELECT Value FROM ClientSettings WHERE Name = '" + name + "'")
            Catch ex As Exceptions.MissingTableException
                _createSettingsTable()
                dr = Nothing
            End Try
            If dr Is Nothing Then
                Database.Tenant.Execute("INSERT INTO ClientSettings (Name, Value) VALUES (" & name.ToSqlValue & ", " + value.ToSqlValue + ")")
            Else
                Database.Tenant.UpdateField("ClientSettings", "Value", value, "Name = " + name.ToSqlValue)
            End If
        End Set
    End Property

    Public Shared Sub ClientSettUpdate(Name As String, Value As String)
        Dim DtStreetAdressTab As DataTable = Database.Tenant.GetDataTable("SELECT Value FROM ClientSettings WHERE Name={0}".SqlFormatString(Name))
        If DtStreetAdressTab.Rows.Count > 0 Then
            Database.Tenant.Execute("UPDATE ClientSettings SET Value={0} WHERE Name={1} ".SqlFormatString(Value, Name))
        Else
            Database.Tenant.Execute("INSERT INTO ClientSettings (Value,Name) VALUES({0},{1}) ".SqlFormatString(Value, Name))
        End If
    End Sub

    Public Shared Sub Delete(Name As String)
        Database.Tenant.Execute("DELETE ClientSettings WHERE Name={0} ".SqlFormatString(Name))
    End Sub

    Public Shared Property OrganizationId As Integer
        Get
            Dim oid = PropertyValue("OrganizationId")
            If String.IsNullOrEmpty(oid) Then
                oid = HttpContext.Current.GetCAMASession.OrganizationId
                PropertyValue("OrganizationId") = oid
            End If
            Return oid
        End Get
        Set(value As Integer)
            PropertyValue("OrganizationId") = value
        End Set
    End Property
    
    Public Shared ReadOnly Property DisableLastSyncStatuses As Boolean
        Get
            If PropertyValue("DisableLastSyncStatuses") IsNot Nothing AndAlso (PropertyValue("DisableLastSyncStatuses").ToString.ToLower = "true" OrElse PropertyValue("DisableLastSyncStatuses").ToString = "1") Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property

    Public Shared ReadOnly Property OverrideTenantSettings As Boolean
        Get
            Dim value As String = ConfigurationManager.AppSettings("OverrideTenantSettings")
            Dim bValue As Boolean = False
            Boolean.TryParse(value, bValue)
            Return bValue
        End Get
    End Property

    Public Shared ReadOnly Property CAMACloudApplicationName As String
        Get
            Dim value As String = ConfigurationManager.AppSettings("CAMACloudApplicationName")
            Return value
        End Get
    End Property
    Public Shared ReadOnly Property TimeZone As String
        Get
            Return PropertyValue("TimeZone")
        End Get
    End Property
    Public Shared Property MobileTitle As String
        Get
            Return PropertyValue("MobileTitle")
        End Get
        Set(value As String)
            PropertyValue("MobileTitle") = value
        End Set
    End Property

    Public Shared Property DesktopTitle As String
        Get
            Return PropertyValue("DesktopTitle")
        End Get
        Set(value As String)
            PropertyValue("DesktopTitle") = value
        End Set
    End Property


    Public Shared Property CAMADataSource As String
        Get
            Return PropertyValue("CAMADataSource")
        End Get
        Set(value As String)
            PropertyValue("CAMADataSource") = value
        End Set
    End Property

    Public Shared Property PrimaryTable As String
        Get
            Return PropertyValue("PrimaryTable")
        End Get
        Set(value As String)
            PropertyValue("PrimaryTable") = value
        End Set
    End Property

    Public Shared Property CommonKeyField1 As String
        Get
            Return PropertyValue("CommonKeyField1")
        End Get
        Set(value As String)
            PropertyValue("CommonKeyField1") = value
        End Set
    End Property

    Public Shared Property CommonKeyField2 As String
        Get
            Return PropertyValue("CommonKeyField2")
        End Get
        Set(value As String)
            PropertyValue("CommonKeyField2") = value
        End Set
    End Property

    Public Shared Property CommonKeyField3 As String
        Get
            Return PropertyValue("CommonKeyField3")
        End Get
        Set(value As String)
            PropertyValue("CommonKeyField3") = value
        End Set
    End Property

    Public Shared Property AWSS3Bucket As String
        Get
            Return Coalesce(PropertyValue("AWSS3Bucket"), ConfigurationManager.AppSettings("AWSS3Bucket"), "storage.camacloud.com")
        End Get
        Set(value As String)
            PropertyValue("AWSS3Bucket") = value
        End Set
    End Property

    Public Shared Property AWSRegion As String
        Get
            Return Coalesce(PropertyValue("AWSRegion"), ConfigurationManager.AppSettings("AWSRegion"), "us-east-1")
        End Get
        Set(value As String)
            PropertyValue("AWSRegion") = value
        End Set
    End Property

    Public Shared Property AWSAccessKey As String
        Get
            Return Coalesce(PropertyValue("AWSAccessKey"), ConfigurationManager.AppSettings("AWSAccessKey"))
        End Get
        Set(value As String)
            PropertyValue("AWSAccessKey") = value
        End Set
    End Property

    Public Shared Property AWSSecretKey As String
        Get
            Return Coalesce(PropertyValue("AWSSecretKey"), ConfigurationManager.AppSettings("AWSSecretKey"))
        End Get
        Set(value As String)
            PropertyValue("AWSSecretKey") = value
        End Set
    End Property

    Public Shared Property NeighborhoodTable As String
        Get
            Return PropertyValue("NeighborhoodTable")
        End Get
        Set(value As String)
            PropertyValue("NeighborhoodTable") = value
        End Set
    End Property

    Public Shared Property NeighborhoodField As String
        Get
            Return PropertyValue("NeighborhoodField")
        End Get
        Set(value As String)
            PropertyValue("NeighborhoodField") = value
        End Set
    End Property

    Public Shared Property NeighborhoodGroup As String
        Get
            Return PropertyValue("NeighborhoodGroup")
        End Get
        Set(value As String)
            PropertyValue("NeighborhoodGroup") = value
        End Set
    End Property

    Public Shared Property StreetAddressTable As String
        Get
            Return PropertyValue("StreetAddressTable")
        End Get
        Set(value As String)
            PropertyValue("StreetAddressTable") = value
        End Set
    End Property

    Public Shared Property StreetAddressFormula As String
        Get
            Return PropertyValue("StreetAddressFormula")
        End Get
        Set(value As String)
            PropertyValue("StreetAddressFormula") = value
        End Set
    End Property

    Public Shared Property ComparablesSource As String
        Get
            Return PropertyValue("ComparablesSource")
        End Get
        Set(value As String)
            PropertyValue("ComparablesSource") = value
        End Set
    End Property

    Public Shared Property ComparablesSubjectField As String
        Get
            Return PropertyValue("ComparablesSubjectField")
        End Get
        Set(value As String)
            PropertyValue("ComparablesSubjectField") = value
        End Set
    End Property

    Public Shared Property ComparablesKeyFields As String
        Get
            Return PropertyValue("ComparablesKeyFields")
        End Get
        Set(value As String)
            PropertyValue("ComparablesKeyFields") = value
        End Set
    End Property

    Public Shared ReadOnly Property EnableAdhocAssignment As Boolean
        Get
            If PropertyValue("EnableAdhocAssignment") IsNot Nothing AndAlso (PropertyValue("EnableAdhocAssignment").ToString.ToLower = "true" OrElse PropertyValue("EnableAdhocAssignment").ToString = "1") Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property


    Public Shared Property NeighborhoodDisplayLabelFullName As String
        Get
            If PropertyValue("NeighborhoodDisplayLabelFullName") = "" AndAlso PropertyValue("NeighborhoodDisplayLabelFullName") Is Nothing Then
                Return "Neighborhood"
            Else
                Return PropertyValue("NeighborhoodDisplayLabelFullName")
            End If

        End Get
        Set(value As String)
            PropertyValue("NeighborhoodDisplayLabelFullName") = value
        End Set
    End Property

    Public Shared Property NeighborhoodDisplayLabelShortName As String
        Get
            If PropertyValue("NeighborhoodDisplayLabelShortName") = "" AndAlso PropertyValue("NeighborhoodDisplayLabelShortName") Is Nothing Then
                Return "Nbhd"
            Else
                Return PropertyValue("NeighborhoodDisplayLabelShortName")
            End If
        End Get
        Set(value As String)
            PropertyValue("NeighborhoodDisplayLabelShortName") = value
        End Set
    End Property

    Public Shared ReadOnly Property IsLicenseProtected As Boolean
        Get
            Return Boolean.Parse(Coalesce(ConfigurationManager.AppSettings("Licensed"), "True"))
        End Get
    End Property

    Public Shared Property MobileAssessorTopLeftLogo As String
        Get
            Dim logo = PropertyValue("MobileAssessorTopLeftLogo")
            If String.IsNullOrEmpty(logo) Then
                Return "assets/applogo/logo-ma-app-top-left.png"
            Else
                Return logo
            End If
        End Get
        Set(value As String)
            PropertyValue("MobileAssessorTopLeftLogo") = value
        End Set
    End Property

    Public Shared Property MobileAssessorTopRightLogo As String
        Get
            Dim logo = PropertyValue("MobileAssessorTopRightLogo")
            If String.IsNullOrEmpty(logo) Then
                Return "assets/applogo/logo-ma-app-top-right.png"
            Else
                Return logo
            End If
        End Get
        Set(value As String)
            PropertyValue("MobileAssessorTopRightLogo") = value
        End Set
    End Property
    Public Shared Property MobileAssessorSplash As String
        Get
            Dim logo = PropertyValue("MobileAssessorSplash")
            If String.IsNullOrEmpty(logo) Then
                Return "assets/applogo/ma-splash-image.png"
            Else
                Return logo
            End If
        End Get
        Set(value As String)
            PropertyValue("MobileAssessorsplash") = value
        End Set
    End Property
    Public Shared Property ConsoleMain As String
        Get
            Dim logo = PropertyValue("ConsoleMain")
            If String.IsNullOrEmpty(logo) Then
                Return "assets/applogo/console-main.png"
            Else
                Return logo
            End If
        End Get
        Set(value As String)
            PropertyValue("ConsoleMain") = value
        End Set
    End Property

    Public Shared Property CustomizationDate As Date
        Get
            Dim tickString = PropertyValue("CustomizationDate")
            If tickString = "" Then
                Return #1/1/2013#
            Else
                Dim ticks As Long = 0
                Long.TryParse(tickString, ticks)
                Return New Date(ticks)
            End If
        End Get
        Set(value As Date)
            PropertyValue("CustomizationDate") = value.Ticks
        End Set
    End Property

    Public Shared Property LastClientTemplateUpdateDate As Date
        Get
            Dim tickString = PropertyValue("LastClientTemplateUpdateDate")
            If tickString = "" Then
                Return #1/1/2013#
            Else
                Dim ticks As Long = 0
                Long.TryParse(tickString, ticks)
                Return New Date(ticks)
            End If
        End Get
        Set(value As Date)
            PropertyValue("LastClientTemplateUpdateDate") = value.Ticks
        End Set
    End Property

    Public Shared ReadOnly Property ContentTemplate(name As String) As String
        Get
            Try
                Dim dr As DataRow = Data.Database.Tenant.GetTopRow("SELECT * FROM ClientTemplates WHERE Name = " + name.ToSqlValue)
                If dr Is Nothing Then
                    Return ""
                End If
                Dim template As String = dr.GetString("TemplateContent")
                Return template
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Public Shared ReadOnly Property DefaultSender As String
        Get
            Return Coalesce(ConfigurationManager.AppSettings("DefaultSender"), "CAMACloud<camacloud@datacloudsolutions.net>")
        End Get
    End Property

    Public Shared ReadOnly Property ErrorLogRoot As String
        Get
            Return Coalesce(ConfigurationManager.AppSettings("ErrorLogRoot"), "C:\CAMACloud Errors\")
        End Get
    End Property

    Public Shared ReadOnly Property ErrorMailSender As String
        Get
            Return Coalesce(ConfigurationManager.AppSettings("ErrorMailSender"), DefaultSender)
        End Get
    End Property

    Public Shared ReadOnly Property ErrorMailRecipients As String
        Get
            Return Coalesce(ConfigurationManager.AppSettings("ErrorMailRecipients"), "camacloud-bugs@techneurons.com")
        End Get
    End Property

    Public Shared ReadOnly Property NotificationSender As String
        Get
            Return Coalesce(ConfigurationManager.AppSettings("NotificationSender"), DefaultSender)
        End Get
    End Property

    Public Shared ReadOnly Property ForceSSL As Boolean
        Get
            Return Boolean.Parse(Coalesce(ConfigurationManager.AppSettings("ForceSSL"), "False"))
        End Get
    End Property

    Public Shared Sub AlterDateFunctions(Name As String, Value As String, db As Database)
        Dim Time As String
        Dim Dts As String

        Dim ExistingValue As String = db.GetStringValue("SELECT Value FROM ClientSettings WHERE Name={0}".SqlFormatString(IIf(Name = "TimeZone", "DST", "TimeZone")))

        If Name = "TimeZone" Then
            Time = Value
            Dts = ExistingValue
        End If

        If Name = "DST" Then
            Time = ExistingValue
            Dts = Value
        End If
        Dim sql As String = "Exec cc_SetTimeZone {0},{1}".SqlFormatString(Time, Dts)
        db.Execute(sql)
    End Sub
End Class


