﻿Imports CAMACloud.Data

Public Class ClientOrganization

    Public Shared Function HasModule(organizationId As Integer, moduleName As String) As Boolean
        Return Database.System.GetIntegerValue("SELECT COUNT(*) FROM AppRoles ar INNER JOIN OrganizationRoles orr ON orr.RoleId = ar.Id WHERE OrganizationId = {0} AND ASPRoleName = {1}".SqlFormat(False, organizationId, moduleName)) > 0
    End Function
    
    Public Shared Property PropertyValue(name As String,orgId As String ) As Object
        Get
            Try
                Dim value =  Database.System.GetTopRow("SELECT [" + name + "] FROM Organization where Id= {0}".SqlFormatString(orgId ))(0)
                If value Is DBNull.Value Then
                    Return Nothing
                End If
                Return value
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As Object)
            Dim dbValue = value
            If TypeOf value Is String Then
                dbValue = CStr(value).ToSqlValue.Trim("'")
            ElseIf TypeOf value Is Date Then
                dbValue = CDate(value).ToSqlValue.Trim("'")
            ElseIf TypeOf value Is Boolean Then
                dbValue = CBool(value).GetHashCode
            End If
             Database.Tenant.UpdateField("Organization", name, dbValue, "1 = 1")
        End Set
    End Property

End Class
