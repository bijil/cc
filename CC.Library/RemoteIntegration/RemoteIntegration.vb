﻿Imports System.ServiceModel

Namespace RemoteIntegration

    Public Enum ServiceResponseCode
        OK = 200
        [Error] = 500
        NotFound = 404
        AuthenticationFailed = 401
        Fobidden = 403
        BadInput = 400
        NotImplemented = 999
    End Enum


    Public Class ServiceResponseStatus
        Private _code As ServiceResponseCode
        Private _description As String

        Public Sub New(Optional code As ServiceResponseCode = ServiceResponseCode.OK)
            _code = code
            Select Case _code
                Case ServiceResponseCode.OK
                    _description = "OK"
                Case ServiceResponseCode.Error
                    _description = "An error occurred while processing your request. Please check messages for more info if available."
                Case ServiceResponseCode.AuthenticationFailed
                    _description = "Authentication failed! Please verify your credentials."
                Case ServiceResponseCode.BadInput
                    _description = "Bad input parameters!"
                Case ServiceResponseCode.Fobidden
                    _description = "Access forbidden!"
                Case ServiceResponseCode.NotFound
                    _description = "The content you are trying to access does not exist."
                Case ServiceResponseCode.NotImplemented
                    _description = "An error occurred while processing your request. Please check messages for more info if available."
            End Select
        End Sub

        Public ReadOnly Property StatusCode As String
            Get
                Return _code.GetHashCode.ToString
            End Get
        End Property

        Public ReadOnly Property Description As String
            Get
                Return _description
            End Get
        End Property
    End Class

End Namespace