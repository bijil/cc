﻿Public Module ErrorEvents

    'Category-User API Events
    Public ReadOnly ERR_UsernameExists As New EventType("UserAccount", 1400, "Username already exist.Please try with another username.")
    Public ReadOnly ERR_UsernameDoesNotExists As New EventType("UserAccount", 1401, "Username didn't exist.Please try with valid username.")
    Public ReadOnly ERR_InvalidUsername As New EventType("UserAccount", 1402, "Username could not be blank.")
    Public ReadOnly ERR_PasswordNotValid As New EventType("UserAccount", 1403, "Password must have minimum 3 characters.")
    Public ReadOnly ERR_PasswordEmpty As New EventType("UserAccount", 1404, "Incorrect password or password is missing. Please correct your entries and try again.")
    Public ReadOnly ERR_InvalidStatus As New EventType("UserAccount", 1405, "Invalid status parameter. Options are modify, delete, unlock,lock.")
    Public ReadOnly ERR_InvalidRoleNames As New EventType("UserAccount", 1406, "Invalid values in ""roles"" .Please correct your entries and try again.")
    Public ReadOnly ERR_InvalidEmailAddress As New EventType("UserAccount", 1407, "Invalid email. Please correct your email address and try again.")
    Public ReadOnly ERR_EmptyEmailAddress As New EventType("UserAccount", 1408, "Email is missing. Please correct your entries and try again.")

End Module

