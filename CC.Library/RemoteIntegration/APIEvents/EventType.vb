﻿Public Class EventType

    Private _code As Integer
    Private _message As String
    Private _eventType As String

    Public ReadOnly Property Code As Integer
        Get
            Return _code
        End Get
    End Property

    Public ReadOnly Property Message As String
        Get
            Return _message
        End Get
    End Property

    Public ReadOnly Property EventType As String
        Get
            Return _eventType
        End Get
    End Property

    Public Sub New(EventType As String, code As Integer, message As String)
        _eventType = EventType
        _code = code
        _message = message
    End Sub
End Class
