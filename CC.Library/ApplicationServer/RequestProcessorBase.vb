﻿Imports System.Web.SessionState
Imports CAMACloud.Data
Imports System.Web.Security

Public MustInherit Class RequestProcessorBase

	Private _context As HttpContext
	Private _request As HttpRequest, _response As HttpResponse, _server As HttpServerUtility, _session As HttpSessionState
	Private _camaSession As CAMASession
	Private _path As String, _root As String
	Private _handler As WebHandlerBase


	Protected ReadOnly Property Request As HttpRequest
		Get
			Return _request
		End Get
	End Property

	Protected ReadOnly Property Response As HttpResponse
		Get
			Return _response
		End Get
	End Property

	Protected ReadOnly Property Session As HttpSessionState
		Get
			Return _session
		End Get
	End Property

	Protected ReadOnly Property Server As HttpServerUtility
		Get
			Return _server
		End Get
	End Property

	Protected ReadOnly Property CAMASession As CAMASession
		Get
			Return _camaSession
		End Get
	End Property

	Protected ReadOnly Property Path As String
		Get
			Return _path
		End Get
	End Property


	Public ReadOnly Property JSON As JSONResponse
		Get
			Return _handler.JSON
		End Get
	End Property


	Public ReadOnly Property DB As Database
		Get
			Return Database.Tenant
		End Get
	End Property

	Public ReadOnly Property UserName As String
		Get
			'Return Membership.GetUser().UserName
			 Return Database.System.GetStringValue("SELECT au.UserName FROM  aspnet_Users au INNER JOIN aspnet_Applications a On a.ApplicationId=au.ApplicationId WHERE  a.ApplicationName='" & Membership.ApplicationName & "' And au.UserName='" &  Membership.GetUser().ToString() & "'")
		End Get
	End Property

	Public ReadOnly Property SqlUserName As String
		Get
			Return Membership.GetUser().UserName.ToSqlValue
		End Get
	End Property

	Public WriteOnly Property Handler As WebHandlerBase
		Set(value As WebHandlerBase)
			_handler = value
			_request = value.Request
			_response = value.Response
			_server = value.Server
			_session = value.Session
			_camaSession = value.CAMASession
			_context = value.Context
			If _root <> String.Empty Then
				_path = _request.Path.ToLower.Replace(_root, "").Replace(".jrq", "").Replace(".xrq", "").Trim("/").Trim
			End If

		End Set
	End Property

	Public Sub SetRoot(root As String)
		_root = root
		If _request IsNot Nothing Then
			_path = _request.Path.ToLower.Replace(root, "").Replace(".jrq", "").Replace(".xrq", "").Trim("/").Trim
		End If
	End Sub

	Public Sub New()

	End Sub

	Public Sub New(root As String)
		SetRoot(root)
	End Sub

	Public Sub New(handler As WebHandlerBase, root As String)
		Me.Handler = handler
		SetRoot(root)
	End Sub

	Public MustOverride Sub ProcessRequest()

End Class

