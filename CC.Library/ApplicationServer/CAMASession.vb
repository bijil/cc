﻿Imports System.Runtime.CompilerServices
Imports System.Configuration
Imports CAMACloud.Data
Imports System.Web.Security


Public Class CAMASession


    Public Shared ReservedHostKeys As String() = {"mobileassessor", "client", "console", "auth", "sv", "admin", "overlaysv", "qc", "sketchvalidation", "ma-preview"}

    Private _sessionKey As String
    Private _context As HttpContext

    Private _organizationId As Integer = -1
    Private _country As String
    Private _county As String
    Private _state As String

    Sub New(context As HttpContext)
        _context = context
    End Sub

    Public ReadOnly Property SessionKey As String
        Get
            If Not _context.Request.Cookies.AllKeys.Contains("MACAUTH") Then
                Return ""
            End If
            Return _context.Request.Cookies.Get("MACAUTH").Value
        End Get
    End Property

    Public ReadOnly Property MachineKey As String
        Get
            If _context.Request.Cookies.Get("MACID") Is Nothing Then
                Return ""
            End If
            Return _context.Request.Cookies.Get("MACID").Value
        End Get
    End Property

    Private Function _getOrganizationId() As Integer
        Try



            Dim hostName As String = _context.Request.Url.Host
            Dim hostPort As Integer = _context.Request.Url.Port

            If hostName = "localhost" Then
                If hostPort >= 50000 And hostPort <= 55000 Then
                    Return hostPort - 50000
                End If
                If hostPort >= 60000 And hostPort <= 65000 Then
                    Return hostPort - 60000
                End If
            End If


            Dim hostParts() As String = hostName.Split(".")

            Dim ip As Net.IPAddress = New Net.IPAddress(1)
            If Net.IPAddress.TryParse(hostName, ip) Then
                If hostPort >= 50000 And hostPort <= 55000 Then
                    Return hostPort - 50000
                End If
                If hostPort >= 60000 And hostPort <= 65000 Then
                    Return hostPort - 60000
                End If
                Throw New Exception("Looks like an IP address. Connecting to developer mode.")
            End If

            If hostParts.Length > 4 Then
                Throw New Exception("More number of parts in URL than expected - " + hostName)
            End If

            Dim hasHostKey As Boolean = False
            If ReservedHostKeys.Contains(hostParts(0).ToLower) Then
                hasHostKey = False
            Else
                hasHostKey = True
            End If
            'If hostParts(0) = "mobileassessor" Or hostParts(0) = "console" Or hostParts(0) = "client" Or hostParts(0) = "auth" Then
            '    hasHostKey = False
            'Else
            '    hasHostKey = True
            'End If


            Dim orgId As Integer = -1
            Dim isVendor As Boolean = False

            If Not hasHostKey Then

                Dim lic = Security.DeviceLicense.GetLicense()
                If lic IsNot Nothing Then
                    If Not hasHostKey Then
                        orgId = lic.OrganizationId
                    End If
                    isVendor = lic.IsVendor
                    If orgId <> -1 Then
                        Return orgId
                    End If

                    orgId = CAMACloud.Data.Database.System.GetIntegerValueOrInvalid("SELECT OrganizationId FROM DomainLicense WHERE DeviceLicenseId = " & lic.Id & " AND Domain = '" + _context.Request.Url.Host + "'")
                ElseIf Not ClientSettings.IsLicenseProtected Then
                    Return Coalesce(ConfigurationManager.AppSettings("OrganizationId"), 1)
                End If
            End If


            If orgId <> -1 Then
                Return orgId
            End If

            Dim ccHost As CAMACloudHost = CAMACloudHost.GetWorkingHost(isVendor)
            If ccHost.Stage <> HostStage.Unknown Then
                If Not Integer.TryParse(ccHost.HostKey, orgId) Then
                    orgId = Database.System.GetIntegerValueOrInvalid("SELECT Id FROM Organization WHERE MAHostKey = {0}".SqlFormatString(ccHost.HostKey))
                End If
            End If

            If orgId <> -1 Then
                Return orgId
            End If

            If ccHost.Stage = HostStage.LocalHost Or ccHost.Stage = HostStage.LocalServer Then
                Return Coalesce(ConfigurationManager.AppSettings("OrganizationId"), 1)
            End If




            'If orgId = -1 Then
            '    Throw New Exception
            'End If

            Return orgId
        Catch ex As Exception
            'If Not HttpContext.Current.Request.IsLocal Then
            '    ErrorMailer.ReportException("Org-Identification", ex, _context.Request)
            'End If
            'Dim key = ConfigurationManager.OpenMachineConfiguration.AppSettings.Settings("OrganizationId")
            'If key IsNot Nothing Then
            '    Return key.Value
            'Else
            '    Return Coalesce(ConfigurationManager.AppSettings("OrganizationId"), 1)
            'End If
            '  ErrorMailer.ReportException("Org-Identification", ex, _context.Request, True)
            Return Coalesce(ConfigurationManager.AppSettings("OrganizationId"), 1)
        End Try
    End Function

    Public ReadOnly Property OrganizationId As Integer
        Get
            If _organizationId = -1 Then
                _organizationId = _getOrganizationId()
            End If
            Return _organizationId
        End Get
    End Property

    Public ReadOnly Property OrganizationName As String
        Get
            Return CAMACloud.Data.Database.System.GetStringValue("SELECT Name FROM Organization WHERE Id = " & OrganizationId)
        End Get
    End Property

    Public ReadOnly Property OrganizationAddress As String
        Get
            Return CAMACloud.Data.Database.System.GetStringValue("SELECT Name +','+ State FROM Organization WHERE Id = " & OrganizationId)
        End Get
    End Property

    Public ReadOnly Property Country As String
        Get
            If _country Is Nothing Then
                If ConfigurationManager.AppSettings.AllKeys.Contains("Country") Then
                    _country = ConfigurationManager.AppSettings("Country")
                Else
                    _country = "USA"
                End If
            End If
            Return _country
        End Get
    End Property

    Public ReadOnly Property County As String
        Get
            If _county Is Nothing Then
                _county = CAMACloud.Data.Database.System.GetStringValue("SELECT County FROM Organization WHERE Id = " & OrganizationId)
            End If
            Return _county
        End Get
    End Property

    Public ReadOnly Property State As String
        Get
            If _state Is Nothing Then
                _state = CAMACloud.Data.Database.System.GetStringValue("SELECT State FROM Organization WHERE Id = " & OrganizationId)
            End If
            Return _state
        End Get
    End Property

    Public ReadOnly Property OrganizationCodeName As String
        Get
            Return CAMACloud.Data.Database.System.GetStringValue("SELECT DBName FROM Organization WHERE Id = " & OrganizationId).ToLower.Replace("_", "-")
        End Get
    End Property
    Public ReadOnly Property IsTestingCounty As String
        Get
            Return CAMACloud.Data.Database.System.GetStringValue("select CASE WHEN COUNT(*) > 0 THEN 'true' ELSE 'false' END  FROM TestingEnvironments WHERE org_Id =  " & OrganizationId)
        End Get
    End Property
    Public ReadOnly Property OrganizationCountyName As String
        Get
            Return CAMACloud.Data.Database.System.GetStringValue("SELECT County FROM Organization WHERE Id = " & OrganizationId).ToLower.Replace("_", "-")
        End Get
    End Property

    Public ReadOnly Property OrganizationCharId As String
        Get
            Return BaseCharMap(OrganizationId)
        End Get
    End Property

    Public ReadOnly Property TenantKey As String
        Get
            Return "org" & OrganizationId.ToString.PadLeft(3, "0")
        End Get
    End Property
    Public ReadOnly Property VendorId As String
        Get
            Return "Vendor" & VendorInfo.ID.ToString.PadLeft(3, "0")
        End Get
    End Property
    Public Property DeviceValidated As Boolean = False

    Public Shared Sub RegisterUserOnDeviceLicense(userName As String)
        Database.System.Execute("UPDATE DeviceLicense SET LastUsedLoginId = {1}, LastUsedIPAddress = {2}, LastAccessTime = GETDATE() WHERE MachineKey = {0}".SqlFormatString(HttpContext.Current.GetCAMASession.MachineKey, userName, HttpContext.Current.Request.ClientIPAddress))
    End Sub

    Public Function VendorInfo() As VendorInfo
        Dim vi As VendorInfo
        If HttpContext.Current.Session("VendorInfo") Is Nothing Then
            vi = New VendorInfo
            Dim dr As DataRow = Database.System.GetTopRow("SELECT * FROM Vendor WHERE Id = (SELECT VendorId FROM Organization WHERE Id = " & OrganizationId & ")")

            vi.Name = dr.GetString("Name")
            vi.Website = dr.GetString("Website")
            vi.Info = dr.GetString("VendorInfo")
            vi.ShowVendorInfo = dr.GetBoolean("ShowVendorInfo")
            vi.CommonName = dr.GetString("CommonName")
            vi.VendorLogoPath = dr.GetString("VendorLogoPath")
            HttpContext.Current.Session.Add("VendorInfo", vi)
        Else
            vi = HttpContext.Current.Session("VendorInfo")
        End If
        Return vi
    End Function

    Public Function AdminVendorInfo() As VendorInfo
        Dim vi As VendorInfo
        If HttpContext.Current.Session("AdminVendorInfo") Is Nothing Then

            Dim dr As DataRow = Database.System.GetTopRow("SELECT * FROM Vendor WHERE Id = (SELECT VendorId FROM DeviceLicense WHERE IsVendor = 1 AND MachineKey = " & MachineKey.ToSqlValue & ")")
            vi = New VendorInfo(dr.GetInteger("ID"))
            vi.Name = dr.GetString("Name")
            vi.Website = dr.GetString("Website")
            vi.Info = dr.GetString("VendorInfo")
            vi.ShowVendorInfo = dr.GetBoolean("ShowVendorInfo")
            vi.CommonName = dr.GetString("CommonName")

            HttpContext.Current.Session.Add("AdminVendorInfo", vi)
        Else
            vi = HttpContext.Current.Session("AdminVendorInfo")
        End If
        Return vi
    End Function

    Public Shared Sub Logout(context As HttpContext)
        FormsAuthentication.SignOut()

        Dim ccHost As CAMACloudHost = CAMACloudHost.GetWorkingHost()
        Dim fac As New HttpCookie(FormsAuthentication.FormsCookieName, "")
        fac.Domain = ccHost.HostRoot
        fac.Expires = DateTime.Now.AddYears(-1)
        context.Response.Cookies.Add(fac)

        Dim ans As New HttpCookie("ASP.NET_SessionId", "")
        ans.Expires = DateTime.Now.AddYears(-1)
        context.Response.Cookies.Add(ans)

If context.Session IsNot Nothing Then
			Dim userName  = Membership.GetUser()
			Dim userId  = userName.ProviderUserKey.ToString()
            context.Session.RemoveAll()
            context.Session.Abandon()
            
            Dim sql As String = "IF EXISTS(SELECT * FROM LoginDetails WHERE UserId='" + userId + "' and ApplicationId = 2 ) UPDATE LoginDetails SET LogOut = GETUTCDATE() WHERE UserId ='" + userId + "' and ApplicationId = 2;"
            Database.System.Execute(sql)
        End If

        context.Response.Redirect("~/")
    End Sub

End Class
