﻿Imports System.Web.SessionState
Imports System.Web

Imports CAMACloud.Security
Imports System.Web.Security
Imports CAMACloud.Data
Imports System.Text.RegularExpressions

Public Module BrowserAuthentication
    Public Const AGENT_CHROME = ") Chrome/"
    Public Const AGENT_IPAD_IOS = "iPad"
    Public Const AGENT_IPAD_MACOS = "Mac OS"
    Public Const AGENT_IPHONE = "iPhone"
    Public Const AGENT_CFNETWORK = "CFNetwork"
    Public Const AGENT_IE10 = "Trident"
    Public ReadOnly ALLOWED_AGENTS() As String = New String() {AGENT_CHROME, AGENT_IPAD_IOS, AGENT_IPAD_MACOS, AGENT_CFNETWORK, AGENT_IE10}
    Public ReadOnly AGENT_IPAD() As String = New String() {AGENT_IPAD_IOS, AGENT_IPAD_MACOS, AGENT_CFNETWORK}
End Module


Public Class CAMACloudApplication
    Implements IHttpModule, IRequiresSessionState



    Private _noLicense As Boolean = False

    Public Sub Dispose() Implements System.Web.IHttpModule.Dispose

    End Sub

    Public Sub Init(application As System.Web.HttpApplication) Implements System.Web.IHttpModule.Init
        AddHandler application.BeginRequest, AddressOf OnBeginRequest
        Dim ssm As SessionStateModule = application.Modules("Session")
        AddHandler ssm.Start, AddressOf OnSessionStart
        AddHandler application.Error, AddressOf OnError
        AddHandler application.PreRequestHandlerExecute, AddressOf OnPreRequestHandlerExecute
        AddHandler application.EndRequest, AddressOf OnEndRequest
        AddHandler application.LogRequest, AddressOf OnRequestCompleted

    End Sub


    Private Sub OnBeginRequest(application As HttpApplication, e As EventArgs)

        CAMACloudRequestLogger.Default.LogBeginRequest(application.Request)
        Try
            'Debug.Print(Request.Path + vbTab + vbTab + vbTab + vbTab + Request.UserAgent)
            Dim request = application.Request
            Dim userAgent As String = ""
            If request.UserAgent IsNot Nothing Then
                userAgent = request.UserAgent
            End If
            If userAgent.Contains("VBH8JF7DXHI") Then
                Exit Sub
            End If

            Dim UA_ = Function(str As String) As Boolean
                          Return userAgent.Contains(str)
                      End Function



            If request.Headers("X-Forwarded-Proto") IsNot Nothing AndAlso request.Headers("X-Forwarded-Proto").ToLower = "http" Then
                application.Response.Redirect("https://" + request.ServerVariables("HTTP_HOST") + HttpContext.Current.Request.RawUrl)
                Return
            End If

            'If Not HttpContext.Current.Request.IsSecureConnection AndAlso Request.IsLocal Then
            '    Dim urlWithHTTPS = "https://" + Request.ServerVariables("HTTP_HOST") + HttpContext.Current.Request.RawUrl
            '    Response.Redirect(urlWithHTTPS)
            '    Return
            'End If

            'If Request.Headers("X-Forwarded-Proto") IsNot Nothing AndAlso Request.Headers("X-Forwarded-Proto").ToLower = "http" AndAlso Request.IsLocal Then
            '    Dim secureUrl = Request.Url.ToString.Replace("http://", "https://")
            '    application.Response.Redirect(secureUrl)
            '    Return
            'End If

            'Enable default.aspx of MobileAssessor to pass through licensing, so as to show installation page for iOS WebApp.

            'If (Request.Path.ToLower = "/" Or Request.Path.ToLower = "/default.aspx") AndAlso (userAgent.Contains("iPad") OrElse userAgent.Contains("iPhone")) AndAlso Regex.IsMatch(userAgent, "Safari") Then
            '    If Configuration.WebConfigurationManager.AppSettings("ApplicationCode") = "MA" Then
            '        Return
            '    End If
            'End If


            If request.Path.ToLower = "/healthcheck.html" Or request.Path.ToLower = "/blank.html" Or request.Path.ToLower = "/install.aspx" Then
                Return
            End If
            'Authentication and management urls (JSON services) do not need pre-authentication of licenses.
            If Not ApplicationSettings.PreValidateLicense Then
                Return
            End If

            'Cross check license with allowed browsers
            Dim allowedBrowsersConfig As String = Configuration.WebConfigurationManager.AppSettings("AllowedBrowsers")
            Dim allowed As Boolean = False
            If allowedBrowsersConfig <> "" Then
                Dim allowedBrowsers() As String = allowedBrowsersConfig.Split(",")
                For Each browser As String In allowedBrowsers
                    Select Case browser.ToLower
                        Case "chrome"
                            If request.BrowserMatches(AGENT_CHROME) Then
                                allowed = True
                            End If
                        Case "ipad"
                            If request.BrowserMatches(AGENT_IPAD) Then
                                allowed = True
                            End If
                        Case "ie10-11"
                            If request.BrowserMatches(AGENT_IE10) Then
                                allowed = True
                            End If
                    End Select
                Next
            End If
            'Block unauthorized user agents
            'CFNetwork - Special user agent for iOS Homescreen icon refresh
            If Not allowed Then
                If Not (request.BrowserMatches(ALLOWED_AGENTS)) Then
                    application.Response.Redirect("http://www.camacloud.com/unsupported-browser.htm")
                    Exit Sub
                    'ElseIf userAgent.Contains("Edg") Then 'userAgent.Contains("Edge") FD_9234  in newversion of edge user agent in Edg form note Edge , contains changed from Edge to Edg
                    '    application.Response.Redirect("http://www.camacloud.com/unsupported-browser.htm")
                    '    Exit Sub
                End If
            End If
            If Configuration.WebConfigurationManager.AppSettings("ApplicationCode") = "MA" AndAlso request.BrowserMatches(AGENT_IPAD) AndAlso Regex.IsMatch(userAgent, "Safari") Then
                Return
            End If
            _noLicense = True
            Try
                DeviceLicense.ValidateRequest(application)
                _noLicense = False
            Catch ex As Exceptions.LicenseBreachException
                'ErrorMailer.ReportException("License-Breach", ex, Request)
                Dim calauthority As String = ApplicationSettings.CALAuthorityPageUrl
                application.Response.Redirect(calauthority)
            Catch ex2 As Exceptions.UnlicensedAccessException
                'ErrorMailer.ReportException("New-Device-Access", ex2, Request)
                Dim calauthority As String = ApplicationSettings.CALAuthorityPageUrl
                application.Response.Redirect(calauthority)
            Catch ex3 As Exceptions.LicenseMissingInfoException
                'ErrorMailer.ReportException("License-Failure", ex3, Request)
                Dim userInfoPage As String = ApplicationSettings.CALUserInfoPageUrl
                application.Response.Redirect(userInfoPage)
            Catch ex4 As Exceptions.UnlicensedApplicationAccessException
                Dim calauthority As String = ApplicationSettings.UnLicensedUrl
                application.Response.Redirect(calauthority)
            Catch ex5 As Exceptions.InvalidUrlException
                application.Response.Redirect("http://auth.camacloud.com/info/noclient.aspx?wrongmaurl=true")
            Catch ex As Exception
                'ErrorMailer.ReportException("License-Failure", ex, Request)
                application.Response.Write("<script type='text/javascript'>")
                application.Response.Write("alert('Application load failed.');")
                application.Response.Write("<" + "/script>")
                'application.Response.StatusCode = 403
                'application.Response.StatusDescription = "License Validation failed - " + ex.Message
                application.CompleteRequest()
                Exit Sub
            End Try
        Catch exBR As Exception

        End Try



    End Sub

    Private Sub OnSessionStart(sender As Object, e As EventArgs)
        Try
            CloudUserProvider.VerifyOrganizationUsers()
        Catch ex As Exception
            CAMACloudRequestLogger.Default.LogError("SessionStart", ex, True)
        End Try
    End Sub

    Private Sub OnError(sender As Object, e As EventArgs)
        Try
            ErrorMailer.ReportException("ApplicationError", HttpContext.Current.Error, HttpContext.Current.Request)
        Catch ex As Exception
            CAMACloudRequestLogger.Default.LogError("App.OnError", ex, True)
            ErrorMailer.GenerateErrorLog("app-crash", HttpContext.Current.Error)
            Throw HttpContext.Current.Error
        End Try
    End Sub

    Private Sub OnPreRequestHandlerExecute(sender As Object, e As EventArgs)
        'If Request.Headers("Accept-Encoding").ToLower().Contains("gzip") Then
        '    Response.Filter = New System.IO.Compression.GZipStream(Response.Filter, System.IO.Compression.CompressionMode.Compress)
        '    Response.AppendHeader("Content-Encoding", "gzip")
        'End If
    End Sub

    Private Sub OnEndRequest(application As HttpApplication, e As EventArgs)

        CAMACloudRequestLogger.Default.LogEndRequest(application.Request)

        If _noLicense Then
            Exit Sub
        End If
        If Configuration.WebConfigurationManager.AppSettings("CustomAuthentication") = "True" Or Configuration.WebConfigurationManager.AppSettings("SkipAuthentication") = "True" Then
            'Skip authentication redirection 
        Else
            If Not application.Request.IsAuthenticated Then
                Try
                    If Not UrlAuthorizationModule.CheckUrlAccessForPrincipal(application.Request.Path, HttpContext.Current.User, application.Request.HttpMethod) Then
                        application.Response.Redirect(ApplicationSettings.AuthenticationURL)
                    End If
                Catch ex As ArgumentNullException
                    Exit Sub
                End Try
            End If
        End If
    End Sub

    Private Sub OnRequestCompleted(application As HttpApplication, e As EventArgs)
        CAMACloudRequestLogger.Default.LogCompletion(application.Request)
    End Sub

End Class

Public Class CAMACloudRequestLogger

    Public Shared RequestLoggingEnabled As Boolean = False
    Public Shared CCLogsOnly As Boolean = True

    Private Shared _instanceCounter As Integer = 0
    Private _instanceId As Integer

    Private _logPath As String
    Private _logFile As String
    Private _instanceName As String

    Private _requestCounter As Integer = 0

    Public Sub New()

        Dim appCode = If(Configuration.WebConfigurationManager.AppSettings("ApplicationCode"), "CC")
        Dim configEnabled = Configuration.WebConfigurationManager.AppSettings("RequestLogging.Enabled")
        Dim configStateDate = Configuration.WebConfigurationManager.AppSettings("RequestLogging.StartDate")
        If configEnabled IsNot Nothing AndAlso configEnabled = "true" Then
            Dim reDate As Date = Date.MinValue
            Date.TryParse(configStateDate, reDate)
            If reDate > Date.MinValue AndAlso reDate < Now AndAlso (Now - reDate).TotalDays < 3 Then
                RequestLoggingEnabled = True
            End If
        End If

            _instanceCounter += 1
        _instanceId = _instanceCounter
        _instanceName = "app_" + Now.ToString("HHmmss") & "_" & _instanceId

        If RequestLoggingEnabled Then
            _logPath = "C:\CAMACloud Logs\" + appCode + "\" + Now.ToString("yyMM") + "\" + Now.ToString("dd")
            If Not IO.Directory.Exists(_logPath) Then
                IO.Directory.CreateDirectory(_logPath)
            End If

            _logFile = IO.Path.Combine(_logPath, _instanceName & ".txt")
        End If

    End Sub

    Private Shared _logger As CAMACloudRequestLogger
    Public Shared ReadOnly Property [Default] As CAMACloudRequestLogger
        Get
            If _logger Is Nothing Then
                _logger = New CAMACloudRequestLogger
            End If
            Return _logger
        End Get
    End Property

    Private Function shouldNotLog() As Boolean
        Try
            shouldNotLog = Not (RequestLoggingEnabled AndAlso HttpContext.Current.Request.Url.Host.EndsWith(".camacloud.com"))
        Catch ex As Exception
            log(ex.Message)
            shouldNotLog = False
        End Try
    End Function

    Private Sub log(message As String)
        SyncLock _logFile
            IO.File.AppendAllText(_logFile, message + vbNewLine)
        End SyncLock


    End Sub

    Public Sub LogBeginRequest(request As HttpRequest)
        If shouldNotLog() Then
            Return
        End If

        _requestCounter += 1
        Dim requestNumber = _requestCounter
        request.RequestContext.HttpContext.Items("RequestNo") = requestNumber

        Dim logInfo As String = String.Format("{0} BEGIN {1} {2} {3}", requestNumber, Now.ToString("HH:mm:ss"), request.Url.Host, request.Url.PathAndQuery)
        log(logInfo)
    End Sub

    Public Sub LogEndRequest(request As HttpRequest)
        If shouldNotLog() Then
            Return
        End If

        Dim requestNumber = request.RequestContext.HttpContext.Items("RequestNo")
        Dim response = request.RequestContext.HttpContext.Response

        Dim logInfo As String = String.Format("     {0} END {1} - {2}", requestNumber, Now.ToString("HH:mm:ss"), response.StatusCode)
        log(logInfo)
    End Sub

    Public Sub LogFailure(request As HttpRequest)
        If shouldNotLog() Then
            Return
        End If

        Dim requestNumber = request.RequestContext.HttpContext.Items("RequestNo")
        Dim response = request.RequestContext.HttpContext.Response

        Dim logInfo As String = String.Format("     {0} FAIL {1} {2} {3} " + vbNewLine + "{4}", requestNumber, Now.ToString("HH:mm:ss"), request.Url.Host, request.Url.PathAndQuery, request.RequestContext.HttpContext.Error)
        log(logInfo)
    End Sub

    Public Sub LogCompletion(request As HttpRequest)
        If shouldNotLog() Then
            Return
        End If
        'If request IsNot Nothing Then
        '    Dim requestNumber = request.RequestContext.HttpContext.Items("RequestNo")
        '    Dim response = request.RequestContext.HttpContext.Response

        '    Dim logInfo As String = String.Format("     {0} COMPLETED ", requestNumber)
        '    log(logInfo)
        'End If

    End Sub

    Public Sub LogError(context As String, ex As Exception, Optional detailed As Boolean = False)
        If shouldNotLog() Then
            Return
        End If

        LogError(context, ex.Message, If(detailed, ex.StackTrace, Nothing))
    End Sub

    Public Sub LogError(context As String, errorMessage As String, Optional stackTrace As String = Nothing)
        If shouldNotLog() Then
            Return
        End If

        Dim requestNumber = 0
        If HttpContext.Current IsNot Nothing Then
            requestNumber = HttpContext.Current.Items("RequestNo")
        End If


        Dim logInfo As String = String.Format("     {0} ERROR! {1} {2}", requestNumber, context, errorMessage)
        If stackTrace IsNot Nothing Then
            logInfo += vbNewLine + stackTrace
        End If
        logInfo += vbNewLine
        log(logInfo)
    End Sub


End Class

