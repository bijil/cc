﻿Imports System.Web.SessionState

Public MustInherit Class WebHandlerBase
    Implements IHttpHandler, IRequiresSessionState

	Private _context As HttpContext
	Private _request As HttpRequest, _response As HttpResponse, _server As HttpServerUtility, _session As HttpSessionState
	Private _camaSession As CAMASession
	Private _path As String
	Private _jsonResp As JSONResponse

	Public ReadOnly Property Request As HttpRequest
		Get
			Return _request
		End Get
	End Property

	Public ReadOnly Property Response As HttpResponse
		Get
			Return _response
		End Get
	End Property

	Public ReadOnly Property Session As HttpSessionState
		Get
			Return _session
		End Get
	End Property

	Public ReadOnly Property Server As HttpServerUtility
		Get
			Return _server
		End Get
	End Property

	Public ReadOnly Property CAMASession As CAMASession
		Get
			Return _camaSession
		End Get
	End Property

	Public ReadOnly Property Path As String
		Get
			Return _path
		End Get
	End Property

	Public ReadOnly Property Context As HttpContext
		Get
			Return _context
		End Get
	End Property

	Public ReadOnly Property IsReusable As Boolean Implements System.Web.IHttpHandler.IsReusable
		Get
			Return False
		End Get
	End Property

	Public ReadOnly Property JSON As JSONResponse
		Get
			Return _jsonResp
		End Get
	End Property

	Public Sub ProcessRequest(context As System.Web.HttpContext) Implements System.Web.IHttpHandler.ProcessRequest
		_request = context.Request
		_response = context.Response
		_server = context.Server
		_session = context.Session
		_camaSession = context.GetCAMASession
		_context = context
		_path = _request.Path.ToLower.Replace(".jrq", "").Replace(".xrq", "").Trim("/").Trim
		_jsonResp = New JSONResponse(context)

		_response.AddHeader("Access-Control-Allow-Origin", "*")

		Dim proc As RequestProcessorBase = Nothing
		Dim handled As Boolean = False

		assignRequestProcessor(proc, handled)

		If proc IsNot Nothing Then
			proc.Handler = Me
			Try
				proc.ProcessRequest()
			Catch ex As Exception
				If Request("debug") = "true" Then
					Throw New Exception(ex.StackTrace)
				End If
				JSON.Error(ex)
				Return
			End Try
		Else
			If Not handled Then
				JSON.Error("Invalid request path")
			End If
		End If
	End Sub

	Protected MustOverride Sub assignRequestProcessor(ByRef processor As RequestProcessorBase, ByRef handled As Boolean)

End Class
