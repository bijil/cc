﻿Imports CAMACloud.Data
Imports System.Net
Imports CAMACloud.Security


Public Enum HostStage
    Unknown = -1
    LocalHost = 0
    LocalServer = 1
    Alpha = 2
    Beta = 3
    ReleaseCandidate = 4
    Production = 9
    Undetermined = 99
End Enum

Public Enum HostApplication
    Unknown = -1
    Console = 0
    MobileAssessor = 1
    API = 2
End Enum


Public Class CAMACloudHost

    Shared alphaStages As String() = {"alpha", "beta1"}
    Shared betaStages As String() = {"beta", "beta2", "pacsbeta", "demo", "ca-beta"}
    Shared rcStages As String() = {"pacsdemo", "qtest"}

    Private _application As HostApplication
    Private _hostRoot As String
    Private _stage As HostStage
    Private _hostKey As String
    Private _organizationId As Integer = -1

    Public ReadOnly Property Application As HostApplication
        Get
            Return _application
        End Get
    End Property

    Public ReadOnly Property Stage As HostStage
        Get
            Return _stage
        End Get
    End Property

    Public ReadOnly Property HostRoot As String
        Get
            Return _hostRoot
        End Get
    End Property

    Public ReadOnly Property HostKey As String
        Get
            Return _hostKey
        End Get
    End Property

    Public ReadOnly Property OrganizationId As Integer
        Get
            If Not Integer.TryParse(_hostKey, _organizationId) Then
                _organizationId = Database.System.GetIntegerValueOrInvalid("SELECT Id FROM Organization WHERE MAHostKey = {0}".SqlFormatString(_hostKey))
            End If
            Return _organizationId
        End Get
    End Property

    Public Sub New()
        _application = HostApplication.Unknown
        _hostKey = ""
        _hostRoot = ".camacloud.com"
        _stage = HostStage.Unknown
    End Sub

    Public Shared Function GetWorkingHost(Optional isVendor As Boolean = False) As CAMACloudHost

        Dim host As New CAMACloudHost

        Dim currentHost As String = HttpContext.Current.Request.Url.Host
        Dim currentPort As Integer = HttpContext.Current.Request.Url.Port
        Dim scheme As String = HttpContext.Current.Request.Url.Scheme

        Dim vendorFilter As String = ""
        Dim vendorInfo As VendorInfo
        Try
            vendorInfo = HttpContext.Current.GetCAMASession.AdminVendorInfo
            vendorFilter = IIf(isVendor, "VendorId = " & vendorInfo.ID & " AND ", "")
        Catch ex As Exception
            vendorFilter = ""
        End Try


        Dim hostRoot = "." + String.Join(".", currentHost.Split(".").Skip(1))
        Dim stage As String = hostRoot.Replace(".camacloud.com", "").Trim(".")

        host._application = HostApplication.Unknown
        host._hostRoot = hostRoot
        host._hostKey = ""

        If alphaStages.Contains(stage) Then
            host._stage = HostStage.Alpha
        ElseIf betaStages.Contains(stage) Then
            host._stage = HostStage.Beta
        ElseIf rcStages.Contains(stage) Then
            host._stage = HostStage.ReleaseCandidate
        ElseIf currentHost = "localhost" Then
            host._stage = HostStage.LocalHost
        Else
            host._stage = HostStage.Production
        End If

        'If currentHost.EndsWith(".beta2.camacloud.com") Then
        '    host._hostRoot = ".beta2.camacloud.com"
        '    host._stage = HostStage.Beta
        'ElseIf currentHost.EndsWith(".beta.camacloud.com") Then
        '    host._hostRoot = ".beta.camacloud.com"
        '    host._stage = HostStage.Beta
        'ElseIf currentHost.EndsWith(".pacsbeta.camacloud.com") Then
        '    host._hostRoot = ".pacsbeta.camacloud.com"
        '    host._stage = HostStage.Beta
        'ElseIf currentHost.EndsWith(".pacsdemo.camacloud.com") Then
        '    host._hostRoot = ".pacsdemo.camacloud.com"
        '    host._stage = HostStage.ReleaseCandidate
        'ElseIf currentHost.EndsWith(".demo.camacloud.com") Then
        '    host._hostRoot = ".demo.camacloud.com"
        '    host._stage = HostStage.Beta
        'ElseIf currentHost.EndsWith(".qtest.camacloud.com") Then
        '    host._hostRoot = ".qtest.camacloud.com"
        '    host._stage = HostStage.Beta

        'ElseIf currentHost.EndsWith(".alpha.camacloud.com") Then
        '    host._hostRoot = ".alpha.camacloud.com"
        '    host._stage = HostStage.Alpha
        'ElseIf currentHost.EndsWith(".beta1.camacloud.com") Then
        '    host._hostRoot = ".beta1.camacloud.com"
        '    host._stage = HostStage.Alpha
        'ElseIf currentHost.EndsWith(".devx.camacloud.com") Then
        '    host._hostRoot = ".devx.camacloud.com"
        '    host._stage = HostStage.LocalServer
        'ElseIf currentHost.EndsWith(".camacloud.com") Then   'This includes all else, .pacs, .vgsi and any custom sub-root-domain
        '    host._hostRoot = ".camacloud.com"
        '    host._stage = HostStage.Production
        'ElseIf currentHost.EndsWith("localhost") Then
        '    host._hostRoot = "localhost"
        '    host._stage = HostStage.LocalHost
        'End If

        If host._stage = HostStage.LocalHost Then
            Select Case currentPort
                Case 300
                    host._application = HostApplication.MobileAssessor
                Case 301
                    host._application = HostApplication.Console
                Case Else
                    If currentPort >= 50000 And currentPort <= 55000 Then
                        host._application = HostApplication.MobileAssessor
                    ElseIf currentPort >= 60000 And currentPort <= 65000 Then
                        host._application = HostApplication.Console
                    Else
                        host._application = HostApplication.Unknown
                    End If
            End Select
            Return host
        End If

        Dim c As IPAddress = IPAddress.Loopback
        If IPAddress.TryParse(currentHost, c) Then
            host._stage = HostStage.LocalServer
            host._hostRoot = currentHost
            Select Case currentPort
                Case 300
                    host._application = HostApplication.MobileAssessor
                Case 301
                    host._application = HostApplication.Console
                Case Else
                    If currentPort >= 50000 And currentPort <= 55000 Then
                        host._application = HostApplication.MobileAssessor
                    ElseIf currentPort >= 60000 And currentPort <= 65000 Then
                        host._application = HostApplication.Console
                    Else
                        host._application = HostApplication.Unknown
                    End If

            End Select
            Return host
        End If

        Dim applicationHostName As String = currentHost.Replace(host._hostRoot, "")
        Dim appHostParts() As String = applicationHostName.Split(".")
        If appHostParts.Count > 3 Then
            Throw New Exception("Invalid host name")
        End If

        If appHostParts.Length = 1 Then
            Select Case appHostParts(0)
                Case "mobileassessor"
                    host._application = HostApplication.MobileAssessor
                Case "ma-preview"
                    host._application = HostApplication.MobileAssessor
                Case "console", "client"
                    host._application = HostApplication.Console
                Case "api"
                    host._application = HostApplication.API
                Case "pacs", "pacsbeta", "pacsdemo"
                    host._application = HostApplication.MobileAssessor
                Case Else
                    host._hostKey = appHostParts(0)
                    If Database.System.GetIntegerValue("SELECT COUNT(*) FROM Organization WHERE " + vendorFilter + " MAHostKey = {0}".SqlFormatString(host._hostKey)) = 0 Then
                        host._hostKey = ""
                        host._stage = HostStage.Unknown
                        Return host
                    Else
                        host._application = HostApplication.MobileAssessor
                    End If
            End Select
            Return host
        End If

        If appHostParts.Length = 2 Then
            host._hostKey = appHostParts(0)
            Dim appName = appHostParts(1)

            Select Case appName
                Case "mobileassessor"
                    host._application = HostApplication.MobileAssessor
                Case "console", "client"
                    host._application = HostApplication.Console
                Case "api"
                    host._application = HostApplication.API
                Case "pacs", "pacsbeta"
                    host._application = HostApplication.MobileAssessor
                Case Else
                    host._application = HostApplication.Unknown
            End Select

            If Database.System.GetIntegerValue("SELECT COUNT(*) FROM Organization WHERE  " + vendorFilter + "  MAHostKey = {0}".SqlFormatString(host._hostKey)) = 0 Then
                host._hostKey = ""
                host._stage = HostStage.Unknown
                Return host
            End If

            Return host
        End If

        host._application = HostApplication.Unknown
        host._stage = HostStage.Unknown
        Return host
    End Function

End Class

