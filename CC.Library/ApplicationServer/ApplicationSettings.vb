﻿Imports CAMACloud.Data, System.Configuration
Imports System.Web.Security

Public Class ApplicationSettings

#Region "Property Value"

    Private Shared Sub _createSettingsTable()
        Database.System.Execute("CREATE TABLE ApplicationSettings (Name VARCHAR(50), Value NVARCHAR(500));")
    End Sub

    Public Shared Property PropertyValue(name As String) As String
        Get
            Try
                Dim dr As DataRow = Database.System.GetTopRow("SELECT Value FROM ApplicationSettings WHERE Name = '" + name + "'")
                If dr Is Nothing Then
                    Return Nothing
                End If
                Return dr.GetString("Value")
            Catch mtx As Exceptions.MissingTableException
                _createSettingsTable()
                Return ""
            Catch ex As Exception
                Return Nothing
            End Try
        End Get
        Set(value As String)
            Dim dr As DataRow
            Try
                dr = Database.System.GetTopRow("SELECT Value FROM ApplicationSettings WHERE Name = '" + name + "'")
            Catch ex As Exceptions.MissingTableException
                _createSettingsTable()
                dr = Nothing
            End Try
            If dr Is Nothing Then
                Database.System.Execute("INSERT INTO ApplicationSettings (Name, Value) VALUES (" & name.ToSqlValue & ", " + value.ToSqlValue + ")")
            Else
                Database.System.UpdateField("ApplicationSettings", "Value", value, "Name = " + name.ToSqlValue)
            End If
        End Set
    End Property

#End Region

    Public Shared Property ApplicationUpdateDate As Date
        Get
            Dim tickString = PropertyValue("ApplicationUpdateDate")
            If tickString = "" Then
                Return #1/1/2013#
            Else
                Dim ticks As Long = 0
                Long.TryParse(tickString, ticks)
                Return New Date(ticks)
            End If
        End Get
        Set(value As Date)
            PropertyValue("ApplicationUpdateDate") = value.Ticks
        End Set
    End Property

    Public Shared ReadOnly Property LogDatabaseConnectionString As String
        Get
            Dim logdbConnStr = PropertyValue("LogDBConnectionString")
            If logdbConnStr = "" Then
                logdbConnStr = "Server=10.0.0.10;Database=CCLOG2;User Id=sa;Password=Cama@2022"
                PropertyValue("LogDBConnectionString") = logdbConnStr
            End If
            Return logdbConnStr
        End Get
    End Property

    Public Shared ReadOnly Property CALAuthority As String
        Get
            Dim cauth As String = Coalesce(ConfigurationManager.AppSettings("CALAuthority"), "")
            If cauth = "" Then
                If HttpContext.Current IsNot Nothing AndAlso HttpContext.Current.Request IsNot Nothing Then
                    Dim ccHost As CAMACloudHost = CAMACloudHost.GetWorkingHost
                    Select Case ccHost.Stage
                        Case HostStage.LocalHost
                            cauth = "http://localhost:309/"
                        Case HostStage.LocalServer
                            cauth = "http://" + ccHost.HostRoot + ":309/"
                        Case HostStage.Undetermined
                            cauth = "http://" + ccHost.HostRoot + ":309/"
                        Case Else
                            cauth = "https://auth" + ccHost.HostRoot + "/"
                    End Select
                Else
                    cauth = "https://auth.camacloud.com/"
                End If
            End If
            Return cauth
        End Get
    End Property

    Public Shared ReadOnly Property UnLicensedUrl As String
        Get
            Return CALAuthority + "info/noclient.aspx?license=false"
        End Get
    End Property
    Public Shared ReadOnly Property MaintenanceURL As String
        Get
            Dim maintenanceDuration As String = ""
            Dim dr As DataRow = Database.System.GetTopRow("SELECT Maintenance,MaintenanceDuration FROM Organization WHERE Id = " & HttpContext.Current.GetCAMASession.OrganizationId)
            If dr Is Nothing Then
                'New vendor/admin license where OrganizationId is null
                Return "False"
            End If
            If (dr("Maintenance") = "True") Then
                maintenanceDuration = dr("MaintenanceDuration").ToString()
                If (maintenanceDuration = "NULL" Or maintenanceDuration = "") Then
                    maintenanceDuration = "60"
                End If
                Return CALAuthority + "ExceptionDisplay.aspx?duration=" & maintenanceDuration
            Else
                Return "False"
            End If
        End Get
    End Property

    Public Shared ReadOnly Property AuthenticationURL As String
        Get
            If HttpContext.Current IsNot Nothing AndAlso HttpContext.Current.Request IsNot Nothing Then
                Dim hostName As String = HttpContext.Current.Request.Url.Host
                Dim port As String = HttpContext.Current.Request.Url.Port
                Dim scheme As String = HttpContext.Current.Request.Url.Scheme
                If port <> "80" Then
                    hostName += ":" + port
                End If
                Return CALAuthority + "?return=" + hostName + "&s=" & scheme.StartsWith("https").GetHashCode
            Else
                Return CALAuthority
            End If
        End Get
    End Property

    Public Shared ReadOnly Property CALAuthorityPageUrl As String
        Get
            If HttpContext.Current IsNot Nothing AndAlso HttpContext.Current.Request IsNot Nothing Then
                Dim hostName As String = HttpContext.Current.Request.Url.Host
                Dim port As String = HttpContext.Current.Request.Url.Port
                Dim scheme As String = HttpContext.Current.Request.Url.Scheme
                If port <> "80" AndAlso port.StartsWith("30") Then
                    hostName += ":" + port
                End If
                Return CALAuthority + "cal.aspx?return=" + hostName + "&s=" & scheme.StartsWith("https").GetHashCode
            Else
                Return CALAuthority + "cal.aspx"
            End If
        End Get
    End Property


    Public Shared ReadOnly Property CALUserInfoPageUrl As String
        Get
            If HttpContext.Current IsNot Nothing AndAlso HttpContext.Current.Request IsNot Nothing Then
                Return GetCALUserInfoPageUrl(HttpContext.Current)
            Else
                Return CALAuthority + "info/nick.aspx"
            End If
        End Get
    End Property

    Public Shared Function GetCALUserInfoPageUrl(context As HttpContext) As String
        Dim hostName As String = context.Request.Url.Host
        Dim port As String = context.Request.Url.Port
        Dim scheme As String = context.Request.Url.Scheme
        If port <> "80" Then
            hostName += ":" + port
        End If
        Return CALAuthority + "info/nick.aspx?return=" + hostName + "&s=" & scheme.StartsWith("https").GetHashCode
    End Function

    Public Shared ReadOnly Property AuthenticationDomain As String
        Get
            Dim hostName As String = HttpContext.Current.Request.Url.Host

            If hostName.EndsWith(".devx.camacloud.com") Then
                Return ".devx.camacloud.com"
            ElseIf hostName.EndsWith(".camacloud.com") Then
                Return ".camacloud.com"
            Else
                Return FormsAuthentication.CookieDomain
            End If


        End Get
    End Property

    Public Shared ReadOnly Property PreValidateLicense As Boolean
        Get
            Dim value As String = ConfigurationManager.AppSettings("PreValidateLicense")
            Dim bValue As Boolean = True
            If Boolean.TryParse(value, bValue) Then
                Return bValue
            Else
                Return True
            End If
        End Get
    End Property

End Class
