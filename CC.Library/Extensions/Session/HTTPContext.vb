﻿Imports System.Runtime.CompilerServices

Public Module HttpContextExtensions

	<Extension()> Public Function GetCAMASession(source As HttpContext) As CAMASession
		Try
			Dim cs As CAMASession = source.Session("CAMASESSION")
			If cs Is Nothing Then
				cs = New CAMASession(source)
				source.Session.Add("CAMASESSION", cs)
			End If
			Return cs
		Catch ex As Exception
			Return Nothing
		End Try
    End Function

    '<Extension()> Public Function CloudApplication(source As HttpContext) As CAMACloudApplication
    '    Try
    '        'Return CType(source.ApplicationInstance, CAMACloudApplication)
    '    Catch ex As Exception
    '        Return Nothing
    '    End Try
    'End Function

    <Extension()> Public Function HasKeys(source As HttpContext, keyName As String) As Boolean
        Dim cookies = source.Request.Cookies
        Return cookies.AllKeys.Contains(keyName) OrElse source.Request.Headers(keyName) <> ""
    End Function

    <Extension()> Public Function GetKeyValue(source As HttpContext, keyName As String) As String
        Dim cookies = source.Request.Cookies
        If cookies.AllKeys.Contains(keyName) Then
            Return cookies.Get(keyName).Value
        Else
            Return source.Request.Headers(keyName)
        End If
    End Function


End Module
