﻿Imports Microsoft.VisualBasic
Imports System.Runtime.CompilerServices
Imports System.Web
Imports CAMACloud.Data

Public Module PageExtensions

    <Extension()> Public Sub RunScript(ByVal source As Web.UI.Page, ByVal script As String)
        ScriptManager.RegisterStartupScript(source, GetType(String), Guid.NewGuid.ToString, script, True)
    End Sub

    <Extension()> Public Sub Alert(ByVal source As Web.UI.Page, ByVal message As String)
		message = message.Replace(vbNewLine, "\n").Replace(vbTab, "\t")
        ScriptManager.RegisterClientScriptBlock(source, GetType(Page), Guid.NewGuid.ToString, "alert('" & message.Replace("'", "\'") & "');", True)
    End Sub

    <Extension()> Public Sub AlertAndRedirect(ByVal source As Web.UI.Page, ByVal message As String, ByVal newLocation As String)
        If newLocation.StartsWith("~") Then
            newLocation = source.ResolveClientUrl(newLocation)
        End If
        ScriptManager.RegisterClientScriptBlock(source, GetType(Page), Guid.NewGuid.ToString, "alert('" & message.Replace("'", "\'") & "'); window.location = '" & newLocation.Replace("'", "\'") & "';", True)
    End Sub

    <Extension()> Public Function TimeEval(ByVal source As Web.UI.Page, ByVal o As Object, Optional ByVal nullString As String = "") As String
        If o Is DBNull.Value Then
            Return nullString
        End If
        Dim time As TimeSpan = o
        Return time.ToLocalString
    End Function

    <Extension()> Public Function UserName(source As Web.UI.Page) As String
        Return source.User.Identity.Name
    End Function

    <Extension()> Public Function UserDisplayName(source As Web.UI.Page) As String
        Dim loginId As String = source.User.Identity.Name
        Dim fullName As String = Database.Tenant.GetStringValue("SELECT COALESCE(FirstName, '') + COALESCE(' ' + LastName, '') FROM UserSettings WHERE LoginId = {0}".SqlFormatString(loginId))
        If fullName = "" Then
            Return loginId
        Else
            Return fullName
        End If
    End Function
End Module
