﻿Imports Microsoft.VisualBasic
Imports System.Runtime.CompilerServices

Public Module TimeSpanExtension

	<Extension()> Public Function ToLocalString(ByVal source As TimeSpan) As String
		If source = Nothing Then
			Return "--:--"
		End If
		Dim t = IIf((source.Hours Mod 12) = 0, 12, source.Hours Mod 12) & ":" + Right("0" & source.Minutes, 2) & " " + IIf(source.Hours < 12, "am", "pm")
		'Code changed above: AM/PM turned to am/pm
		Return t
	End Function

End Module
