﻿Imports Microsoft.VisualBasic
Imports System.Runtime.CompilerServices
Imports System.Web.UI.HtmlControls

Public Module MasterPageExtensions

	<Extension()> Public Sub SetPageTitle(ByVal source As Web.UI.MasterPage, ByVal PageTitleControl As HtmlContainerControl)
		Dim childTitle As String = source.Page.Title
		If Not String.IsNullOrEmpty(childTitle) Then
			source.Page.Title = childTitle + " : CAMA Cloud"
			PageTitleControl.InnerHtml = childTitle
			PageTitleControl.Visible = True
		Else
			source.Page.Title = "CAMA Cloud"
			PageTitleControl.Visible = False
		End If
	End Sub
End Module
