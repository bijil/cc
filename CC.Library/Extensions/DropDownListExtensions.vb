﻿Imports Microsoft.VisualBasic
Imports System.Runtime.CompilerServices
Imports System.Web.UI.WebControls
Imports CAMACloud.Data

Public Module DropDownListExtensions

	<Extension()> Public Sub AddItem(ByVal source As ListItemCollection, ByVal text As String, ByVal value As String)
		source.Add(New ListItem(text, value))
	End Sub

	<Extension()> Public Sub AddItem(ByVal source As DropDownList, ByVal text As String, ByVal value As String)
		source.Items.Add(New ListItem(text, value))
	End Sub

	<Extension()> Public Sub InsertItem(ByVal source As DropDownList, ByVal text As String, ByVal value As String, index As Integer)
		source.Items.Insert(index, New ListItem(text, value))
	End Sub
    <Extension()> Public Sub FillFromSql(ByVal source As DropDownList, ByVal sql As String, Optional showSelect As Boolean = False, Optional selectText As String = "-- Select --", Optional selectedValue As String = "")
        Dim dt As DataTable = Database.Tenant.GetDataTable(sql)
        source.FillFromTable(dt, showSelect, selectText, selectedValue)
    End Sub

    <Extension()> Public Sub FillFromSqlWithDatabase(ByVal source As DropDownList, ByVal db As Database, ByVal sql As String, Optional showSelect As Boolean = False, Optional selectText As String = "-- Select --", Optional selectedValue As String = "")
        Dim dt As DataTable = db.GetDataTable(sql)
        source.FillFromTable(dt, showSelect, selectText, selectedValue)
    End Sub
	<Extension()> Public Sub FillFromTable(ByVal source As DropDownList, ByVal dt As DataTable, Optional showSelect As Boolean = False, Optional selectText As String = "-- Select --", Optional selectedValue As String = "")
		source.Items.Clear()
		source.ClearSelection()
		source.DataSource = dt
		If dt.Columns.Count = 1 Then
			source.DataTextField = dt.Columns(0).ColumnName
			source.DataValueField = dt.Columns(0).ColumnName
		Else
			source.DataTextField = dt.Columns(1).ColumnName
			source.DataValueField = dt.Columns(0).ColumnName
		End If
		Try
			source.DataBind()
		Catch aex As ArgumentOutOfRangeException

		Catch ex As Exception
			Throw ex
		End Try


		If showSelect Then
			source.Items.Insert(0, New ListItem(selectText, ""))
		End If
		Try
			source.SelectedValue = selectedValue
		Catch ex As Exception
			'Just leave it, if the text is not available, then let it show first value.
			'Or decide later.
		End Try
	End Sub

	<Extension()> Public Sub FillNumbers(source As DropDownList, minValue As Integer, maxValue As Integer, Optional stepValue As Integer = 1, Optional textFormat As String = "{0}", Optional textOffset As Integer = 0, Optional showSelect As Boolean = False, Optional selectText As String = "--", Optional selectedValue As String = "")
		source.Items.Clear()
		For i As Integer = minValue To maxValue Step stepValue
			source.Items.Add(New ListItem(textFormat.FormatString(i + textOffset), i))
		Next
		If showSelect Then
			source.Items.Insert(0, New ListItem(selectText, ""))
		End If
		Try
			source.SelectedValue = selectedValue
		Catch ex As Exception
			'Just leave it, if the text is not available, then let it show first value.
			'Or decide later.
		End Try
	End Sub

End Module
