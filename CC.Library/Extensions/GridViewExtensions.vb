﻿Imports Microsoft.VisualBasic
Imports System.Runtime.CompilerServices
Imports System.Web.UI.WebControls

Public Module GridViewExtensions

	'Extensions to System.Web.UI.WebControls.GridViewUpdateEventArgs
	<Extension()> Public Function GetRow(ByVal source As System.Web.UI.WebControls.GridViewUpdateEventArgs, ByVal parent As GridView) As GridViewRow
		Return parent.Rows(source.RowIndex)
	End Function

	<Extension()> Public Function GetControl(Of T As WebControl)(ByVal source As GridViewUpdateEventArgs, ByVal parent As GridView, ByVal id As String) As T
		Return CType(source.GetRow(parent).FindControl(id), T)
	End Function

	<Extension()> Public Function GetTextValue(ByVal source As GridViewUpdateEventArgs, ByVal parent As GridView, ByVal id As String) As String
		Return CType(source.GetRow(parent).FindControl(id), TextBox).Text
	End Function

	<Extension()> Public Function GetSelection(ByVal source As GridViewUpdateEventArgs, ByVal parent As GridView, ByVal id As String) As String
		Return CType(source.GetRow(parent).FindControl(id), DropDownList).SelectedValue
	End Function

	<Extension()> Public Function GetHiddenValue(ByVal source As GridViewUpdateEventArgs, ByVal parent As GridView, ByVal id As String) As String
		Return CType(source.GetRow(parent).FindControl(id), HiddenField).Value
	End Function

	<Extension()> Public Function GetChecked(ByVal source As GridViewUpdateEventArgs, ByVal parent As GridView, ByVal id As String) As Boolean
		Return CType(source.GetRow(parent).FindControl(id), CheckBox).Checked
	End Function


	'Extensions to System.Web.UI.WebControls.GridViewRow

	<Extension()> Public Function GetControl(Of T As WebControl)(ByVal source As GridViewRow, ByVal id As String) As T
		Return CType(source.FindControl(id), T)
	End Function

	<Extension()> Public Function GetTextValue(ByVal source As GridViewRow, ByVal id As String) As String
		Return CType(source.FindControl(id), TextBox).Text
	End Function

	<Extension()> Public Function GetSelection(ByVal source As GridViewRow, ByVal id As String) As String
		Return CType(source.FindControl(id), DropDownList).SelectedValue
	End Function

	<Extension()> Public Function GetHiddenValue(ByVal source As GridViewRow, ByVal id As String) As String
		Return CType(source.FindControl(id), HiddenField).Value
	End Function

	<Extension()> Public Function GetChecked(ByVal source As GridViewRow, ByVal id As String) As Boolean
		Return CType(source.FindControl(id), CheckBox).Checked
	End Function


	'Extensions to System.Web.UI.WebControls.GridViewCommandEventArgs
	<Extension()> Public Function CommandRow(ByVal source As GridViewCommandEventArgs) As GridViewRow
		Dim p As Object = source.CommandSource.Parent
		While p.GetType IsNot GetType(GridViewRow)
			p = p.Parent
		End While
		Return p
	End Function

	<Extension()> Public Function GetControl(Of T As WebControl)(ByVal source As GridViewCommandEventArgs, ByVal parent As GridView, ByVal id As String) As T
		Return source.CommandRow.GetControl(Of T)(id)
	End Function

	<Extension()> Public Function GetTextValue(ByVal source As GridViewCommandEventArgs, ByVal id As String) As String
		Return source.CommandRow.GetControl(Of TextBox)(id).Text
	End Function

	<Extension()> Public Function GetSelection(ByVal source As GridViewCommandEventArgs, ByVal id As String) As String
		Return CType(source.CommandRow.FindControl(id), DropDownList).SelectedValue
	End Function

	<Extension()> Public Function GetHiddenValue(ByVal source As GridViewCommandEventArgs, ByVal id As String) As String
		Return CType(source.CommandRow.FindControl(id), HiddenField).Value
	End Function

    <Extension()> Public Function GetChecked(ByVal source As GridViewCommandEventArgs, ByVal id As String) As Boolean
        Return CType(source.CommandRow.FindControl(id), CheckBox).Checked
    End Function

    <Extension()> Public Sub ExportAsExcel(ByVal source As GridView, ByVal table As DataTable, title As String, fileName As String, Optional sheetName As String = "data")
        Dim excelStream = ExcelGenerator.ExportGrid(source, table, title, sheetName)
        Dim r = source.Page.Response
        r.Clear()
        r.BufferOutput = True
        r.ContentType = "application/vnd.openxml.formats-officedocument.spreadsheetml.sheet"
        r.AddHeader("Content-Disposition", "attachment;filename=" & fileName & ".xlsx")
        excelStream.CopyTo(r.OutputStream)
        r.End()
    End Sub

End Module
