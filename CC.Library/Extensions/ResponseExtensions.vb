﻿Imports Microsoft.VisualBasic
Imports System.Runtime.CompilerServices

Public Module ResponseExtensions

    <Extension()> Public Sub SendAsAttachment(ByVal source As HttpResponse, ByVal filePath As String, Optional ByVal downloadName As String = Nothing, Optional ByVal deleteAfterDownload As Boolean = False)
        If Not IO.File.Exists(filePath) Then
            Throw New IO.FileNotFoundException("Requested file does not exist.", filePath)
        End If
        source.Clear()
        Dim filename As String = IO.Path.GetFileName(filePath)
        If downloadName IsNot Nothing Then
            filename = downloadName
        End If
        source.ContentType = "application/octet-stream"
        source.AppendHeader("Content-Disposition", "attachment; filename=" + filename)
        source.TransmitFile(filePath)
        source.Flush()

        If deleteAfterDownload Then
            IO.File.Delete(filePath)
        End If
        source.End()
        source.Close()
    End Sub


    <Extension()> Public Sub WriteAsAttachment(ByVal source As HttpResponse, ByVal fileContent As String, ByVal downloadName As String, Optional ByVal contentType As String = "application/octet-stream")
    	source.Clear()
    	source.ContentType = contentType
        source.AppendHeader("Content-Disposition", "attachment; filename=""" & downloadName & "")
        source.Write(fileContent)
        source.Flush()
        source.End()
    End Sub

    <Extension()> Public Sub WriteLine(source As HttpResponse, Optional line As String = "")
        source.Write(line + vbNewLine)
    End Sub

End Module
