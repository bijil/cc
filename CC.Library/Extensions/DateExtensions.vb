﻿Imports Microsoft.VisualBasic
Imports System.Runtime.CompilerServices

Public Module DateExtensions

    <Extension()> Public Function ToSqlValue(ByVal source As DateTime, Optional ByVal allowNull As Boolean = False) As String
        If source = Nothing AndAlso allowNull = True Then
            Return "null"
        End If
        If source = Date.MinValue Then
            Return "null"
        End If
        Return "'" + source.ToString("yyyy-MM-dd HH:mm:ss") + "'"
    End Function


End Module
