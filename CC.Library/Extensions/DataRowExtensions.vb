﻿Imports Microsoft.VisualBasic
Imports System.Runtime.CompilerServices
Imports System.Reflection

Public Module DataRowExtensions


    '<Extension()> Public Function _s(ByVal source As DataRow, ByVal fieldName As String) As String
    '    Dim o As Object = source(fieldName)
    '    If o Is DBNull.Value Then
    '        Return ""
    '    Else
    '        Return o.ToString
    '    End If
    'End Function

    <Extension()> Public Function GetString(ByVal source As DataRow, ByVal fieldName As String) As String
        Dim o As Object = source(fieldName)
        If o Is DBNull.Value Then
            Return ""
        Else
            Return o.ToString
        End If
    End Function

    <Extension()> Public Function GetInteger(ByVal source As DataRow, ByVal fieldName As String, Optional nullValue As Integer = 0) As Integer
        Dim o As Object = source(fieldName)
        If o Is DBNull.Value Then
            Return nullValue
        Else
            Return o
        End If
    End Function


    <Extension()> Public Function GetIntegerWithDefault(ByVal source As DataRow, ByVal fieldName As String, Optional defaultValue As Integer = 0) As Integer
        Dim o As Object = source(fieldName)
        If o Is DBNull.Value Then
            Return defaultValue
        Else
            Return o
        End If
    End Function



    <Extension()> Public Function GetSingleWithDefault(ByVal source As DataRow, ByVal fieldName As String, Optional defaultValue As Single = 0.0F) As Single
        Dim o As Object = source(fieldName)
        Dim s As Single
        If o Is DBNull.Value Then
            Return defaultValue
        ElseIf Single.TryParse(o, s) Then
            Return s
        Else
            Return defaultValue
        End If
    End Function

    <Extension()> Public Function GetBoolean(ByVal source As DataRow, ByVal fieldName As String) As Boolean
        Dim o As Object = source(fieldName)
        If o Is DBNull.Value Then
            Return False
        Else
            Return o
        End If
    End Function


    <Extension()> Public Function GetDate(ByVal source As DataRow, ByVal fieldName As String) As Date
        Dim o As Object = source(fieldName)
        If o Is DBNull.Value Then
            Return Nothing
        Else
            Return o
        End If
    End Function

    <Extension()> Public Function GetTimeSpan(ByVal source As DataRow, ByVal fieldName As String) As TimeSpan
        Dim o As Object = source(fieldName)
        If o Is DBNull.Value Then
            Return Nothing
        Else
            Return o
        End If
    End Function

    <Extension()> Public Function [Get](ByVal source As DataRow, ByVal nameOrIndex As Object) As Object
        Dim o As Object = source(nameOrIndex)
        If o Is DBNull.Value Then
            Return Nothing
        Else
            Return o
        End If
    End Function

    <Extension()> Public Function ConvertToList(Of T As New)(ByVal source As DataTable) As List(Of T)
        Dim props = GetType(T).GetProperties()
        Dim list As New List(Of T)

        Dim propNames = props.Select(Function(x) x.Name).ToList
        Dim columns = source.Columns.Cast(Of DataColumn).Select(Function(x) x.ColumnName).ToList

        Dim matchingNames = propNames.Select(Function(x) x.ToLower).Intersect(columns.Select(Function(x) x.ToLower)).ToList

        For Each dr As DataRow In source.Rows
            Dim o As New T
            For Each prop In props
                If matchingNames.Contains(prop.Name.ToLower) Then
                    Dim propType = prop.PropertyType
                    Dim propValue = dr.Get(prop.Name)
                    If propValue IsNot Nothing Then
                        Dim targetType As Type
                        If propType.IsGenericType AndAlso propType.GetGenericTypeDefinition().Equals(GetType(Nullable(Of))) Then
                            targetType = Nullable.GetUnderlyingType(propType)
                        Else
                            targetType = propType
                        End If
                        prop.SetValue(o, Convert.ChangeType(dr.Get(prop.Name), targetType))
                    End If

                End If
            Next
            list.Add(o)
        Next

        Return list
    End Function

    <Extension()> Public Function ToRows(ByVal source As DataTable) As IEnumerable(Of DataRow)
        Return source.Rows.Cast(Of System.Data.DataRow)
    End Function

    <Extension()> Public Function ToKeyValueList(ByVal source As DataRow) As Dictionary(Of String, Object)
        Dim items As New Dictionary(Of String, Object)
        For Each dc As DataColumn In source.Table.Columns
            items.Add(dc.ColumnName, source.Get(dc.ColumnName))
        Next
        Return items
    End Function

    <Extension()> Public Function ConvertToList(ByVal source As DataTable) As JSONTable
        Dim list As New JSONTable
        For Each dr As DataRow In source.Rows
            list.Add(dr.ToKeyValueList)
        Next
        Return list
    End Function

End Module

Public Class JSONTable
    Inherits List(Of Dictionary(Of String, Object))

End Class

Public Class JSONRow
    Inherits Dictionary(Of String, Object)
End Class