﻿Imports Microsoft.VisualBasic
Imports System.Runtime.CompilerServices

Public Module RequestExtensions

    <Extension()> Public Function GetIntegerOrInvalid(source As HttpRequest, name As String, Optional invalidValue As Integer = -1)
        Dim v As Integer = 0
        If Not Integer.TryParse(source(name), v) Then
            v = invalidValue
        End If
        Return v
    End Function

    <Extension()> Public Function ClientIPAddress(source As HttpRequest) As String
        Dim awsForwardedIPHeader As String = "X-Forwarded-For"
        If Not source.Headers.AllKeys.Contains(awsForwardedIPHeader) Then
            Return source.UserHostAddress
        Else
            Return source.Headers(awsForwardedIPHeader).Split(",")(0)
        End If
	End Function

    <Extension()> Public Function BrowserMatches(source As HttpRequest, str As String) As Boolean
        Return source.UserAgent.Contains(str)
    End Function

    <Extension()> Public Function BrowserMatches(source As HttpRequest, str() As String) As Boolean
        For Each s In str
            If source.UserAgent.Contains(s) Then
                Return True
            End If
        Next
        Return False
    End Function

    <Extension()> Public Function IsAjaxRequest(source As HttpRequest) As Boolean
        Return source.Headers("X-Requested-With") = "XMLHttpRequest"
    End Function

End Module
