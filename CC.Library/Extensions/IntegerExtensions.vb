﻿Imports Microsoft.VisualBasic
Imports System.Runtime.CompilerServices

Public Module IntegerExtensions

    <Extension()> Public Function NullIfZero(ByVal source As Integer) As String
        If source = 0 Then
            Return "null"
        Else
            Return source
        End If
    End Function

    <Extension()> Public Function NullIf(ByVal source As Integer, ByVal check As Integer) As String
        If source = check Then
            Return "null"
        Else
            Return source
        End If
    End Function

End Module
