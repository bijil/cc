﻿Namespace Exceptions

	Public Enum LicenseErrorType
		InvalidLicenseKey
		InvalidOTP
		LicenseInUse
        LicenseDisabled
        InvalidTarget
        InvalidConsoleTarget
		InvalidIdentifiers
	End Enum

	Public Class UnlicensedAccessException
		Inherits Exception
		Public Sub New(context As HttpContext)
			MyBase.New("Your browser is not licensed to access this application.")


		End Sub
	End Class
	Public Class UnlicensedApplicationAccessException
		Inherits Exception

		Public Sub New(context As HttpContext)
			MyBase.New("Your browser is not licensed to access this application.")
		End Sub
	End Class

	Public Class LicenseBreachException
		Inherits Exception

		Public Sub New(context As HttpContext, Optional macId As String = "", Optional macAuth As String = "")
			MyBase.New("Your browser has used incorrect license parameters. " + macId + IIf(macAuth <> "", ", " + macAuth, ""))


		End Sub
	End Class


	Public Class LicenseMissingInfoException
		Inherits Exception

		Public Sub New(context As HttpContext)
			MyBase.New("Your license key does not hold sufficient user data.")


		End Sub
	End Class

	Public Class InvalidLicenseException
		Inherits Exception

		Dim _errorType As LicenseErrorType

		Public ReadOnly Property ErrorType As LicenseErrorType
			Get
				Return _errorType
			End Get
		End Property

		Public Sub New(errorType As LicenseErrorType, Optional context As HttpContext = Nothing)
			MyBase.New(TranslateErrorType(errorType))

			_errorType = errorType

		End Sub

		Private Shared Function TranslateErrorType(errorType As LicenseErrorType) As String
			Select Case errorType
				Case LicenseErrorType.InvalidLicenseKey
					Return "The license key you have entered is invalid."
				Case LicenseErrorType.InvalidOTP
					Return "You have entered an incorrect password. Please try again."
				Case LicenseErrorType.LicenseDisabled
					Return "The license which are attempting to use is currently disabled."
				Case LicenseErrorType.LicenseInUse
					Return "You are attempting to use a License Key which is already installed on another machine."
				Case LicenseErrorType.InvalidTarget
					Return "You are attempting to use a License Key which is intented to be used on another type of device."
				Case LicenseErrorType.InvalidConsoleTarget
					Return "You are attempting to use a License Key which is intended to be used on another type of device or browser. The Admin Console must be accessed through Google Chrome."
				Case LicenseErrorType.InvalidIdentifiers
					Return "The nickname you have entered is already registered on another device."
			End Select
			Return "Unknown License Error"
		End Function

	End Class

	Public Class InvalidUrlException
		Inherits Exception
		Public Sub New(context As HttpContext)
			MyBase.New("You entered url is invalid.")
		End Sub


	End Class
End Namespace
