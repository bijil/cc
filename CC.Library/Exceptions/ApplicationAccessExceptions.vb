﻿Namespace Exceptions

    Public Class AuthenticationFailedException
        Inherits Exception

        Public Sub New()
            MyBase.New("Authentication failed.")
        End Sub

    End Class

End Namespace
