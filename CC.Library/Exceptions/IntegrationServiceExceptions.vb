﻿Namespace Exceptions

    Public MustInherit Class ServiceExceptionBase
        Inherits Exception

        Public ServiceData As Object

        Protected _isHandled As Boolean = False

        Protected _statusCode As String
        Protected _reportException As Boolean = True

        Public ReadOnly Property StatusCode As String
            Get
                Return _statusCode
            End Get
        End Property

        Public ReadOnly Property ErrorMailRequired As Boolean
            Get
                Return _reportException
            End Get
        End Property

        Public Sub New(statusCode As String)
            MyBase.New("Generic Service Error")
            Me._statusCode = statusCode
        End Sub

        Public Sub New(statusCode As String, description As String)
            MyBase.New(description)
            Me._statusCode = statusCode
        End Sub

        Public Sub New(statusCode As String, description As String, innerException As Exception)
            MyBase.New(description, innerException)
            Me._statusCode = statusCode
        End Sub

    End Class

    Public Class RequestAbortException
        Inherits ServiceExceptionBase

        Public Sub New()
            MyBase.New("500", "Service is aborted due to an error on processing the request.")
        End Sub

        Public Sub New(innerException As Exception)
            MyBase.New("500", "Service is aborted due to an error on processing the request.", innerException)
        End Sub

        Public Sub New(message As String)
            MyBase.New("500", message)
        End Sub

    End Class

    Public Class BadRequestException
        Inherits ServiceExceptionBase

        Public Sub New()
            MyBase.New("400", "Bad Request - One or more of required parameters are either unacceptable or missing.")
            _isHandled = True
        End Sub

        Public Sub New(innerException As Exception, Optional reportException As Boolean = True)
            MyBase.New("400", "Bad Request - One or more of required parameters are either unacceptable or missing.", innerException)
            _isHandled = True
            _reportException = reportException
        End Sub

        Public Sub New(message As String, innerException As Exception, Optional reportException As Boolean = True)
            MyBase.New("400", message, innerException)
            _reportException = reportException
        End Sub

    End Class

    Public Class PendingTransferException
        Inherits ServiceExceptionBase

        Private _pendingJobIds As New List(Of Integer)
        Private _pendingJobTypes As New List(Of String)
        Public ReadOnly Property PendingJobIds As List(Of Integer)
            Get
                Return _pendingJobIds
            End Get
        End Property

        Public ReadOnly Property PendingJobTypes As List(Of String)
            Get
                Return _pendingJobTypes
            End Get
        End Property

        Public ReadOnly Property PendingJobTypeString As String
            Get
                Return String.Join(",", _pendingJobTypes.ToArray)
            End Get
        End Property

        Public Sub New(pendingJobIds As List(Of Integer), pendingJobTypes As List(Of String), Optional reportException As Boolean = True)
            MyBase.New("400", "Another transfer job is in progress. A new job cannot be initiated until the previous one is committed or cancelled.")
            _pendingJobIds = pendingJobIds
            _pendingJobTypes = pendingJobTypes
            _isHandled = True
            _reportException = reportException
        End Sub

    End Class

    Public Class InvalidAuthException
        Inherits ServiceExceptionBase

        Public Sub New()
            MyBase.New("401", "Authentication failed. Access is denied.")
            _isHandled = True
        End Sub

        Public Sub New(innerException As Exception)
            MyBase.New("401", "Authentication failed. Access is denied.", innerException)
            _isHandled = True
        End Sub

        Public Sub New(message As String)
            MyBase.New("401", message)
        End Sub

    End Class

    Public Class DataSchemaException
        Inherits ServiceExceptionBase

        Public Sub New()
            MyBase.New("500", "Invalid schema caused an error while processing the data structures.")
        End Sub

        Public Sub New(innerException As Exception)
            MyBase.New("500", "Invalid schema caused an error while processing the data structures.", innerException)
        End Sub
    End Class


    Public Class DataException
        Inherits ServiceExceptionBase

        Public Sub New()
            MyBase.New("500", "An error occurred while processing the request on the database.")
        End Sub

        Public Sub New(innerException As Exception)
            MyBase.New("500", "An error occurred while processing the request on the database.", innerException)

        End Sub

        Public Sub New(message As String)
            MyBase.New("500", message)
        End Sub

    End Class


    Public Class ServiceFailure
        Inherits ServiceExceptionBase

        Public Sub New()
            MyBase.New("500", "An error occurred while processing the request.")
        End Sub

        Public Sub New(innerException As Exception)
            MyBase.New("500", "An error occurred while processing the request.", innerException)

        End Sub

        Public Sub New(message As String)
            MyBase.New("500", message)
        End Sub

    End Class

End Namespace