﻿Imports System.Data.SqlClient

Namespace Exceptions

	Public Class MissingTableException
		Inherits Exception

		Private _tableName As String

		Public ReadOnly Property TableName As String
			Get
				Return _tableName
			End Get
		End Property

		Public ReadOnly Property LoweredTableName As String
			Get
				Return _tableName.ToLower
			End Get
		End Property

		Public Sub New(context As HttpContext, base As Exception)
			MyBase.New("Table not found - " + getMissingTableName(base.Message))

			_tableName = getMissingTableName(base.Message)
		End Sub

		Public Sub New(context As HttpContext, tableName As String)
			MyBase.New("Table not found - " + tableName)

			_tableName = tableName
		End Sub

		Private Shared Function getMissingTableName(errorMessage As String) As String
			Return errorMessage.Replace("Invalid object name", "").Trim.Trim(".").Trim("'")
		End Function
	End Class

    Public Class EnvironmentMaintenanceException
        Inherits Exception

        Private _durationInMinutes As Integer

        Public Sub New(duration As Integer)
            MyBase.New("Account has been closed temporarily for maintenance. All data services will reinstantiate after " & duration & " minutes.")
            _durationInMinutes = duration
        End Sub

        Public ReadOnly Property DurationInMinutes As Integer
            Get
                Return _durationInMinutes
            End Get
        End Property

    End Class
End Namespace