﻿Imports Microsoft.VisualBasic
Imports System.Net.Security
Imports System.IO
Imports System.Net
Imports Amazon.S3
Imports Amazon.S3.Model
Imports Amazon.S3.Transfer
Imports System.Security.Cryptography.X509Certificates

Public Class S3FileManager

    Dim _client As AmazonS3Client
    Dim _bucketName As String = ClientSettings.AWSS3Bucket

    Dim fileInProgress As String = ""

    Public Property UploadResponses As List(Of Amazon.S3.Model.UploadPartResponse)
        Get
            If HttpContext.Current.Session("UPRS") Is Nothing Then
                HttpContext.Current.Session("UPRS") = New List(Of Amazon.S3.Model.UploadPartResponse)
            End If
            Return HttpContext.Current.Session("UPRS")
        End Get
        Set(ByVal value As List(Of Amazon.S3.Model.UploadPartResponse))
            HttpContext.Current.Session("UPRS") = value
        End Set
    End Property

    Public Property UploadID As String
        Get
            Return Coalesce(HttpContext.Current.Session("UID"), "")
        End Get
        Set(ByVal value As String)
            HttpContext.Current.Session("UID") = value
        End Set
    End Property

    Public Property PartNumber As Integer
        Get
            Return Coalesce(HttpContext.Current.Session("PNO"), 1)
        End Get
        Set(ByVal value As Integer)
            HttpContext.Current.Session("PNO") = value
        End Set
    End Property

    Public Sub New()
        Dim regionEndPoint = Amazon.RegionEndpoint.GetBySystemName(ClientSettings.AWSRegion)
        Dim accessKey As String = ClientSettings.AWSAccessKey
        Dim secretKey As String = ClientSettings.AWSSecretKey
        _client = New AmazonS3Client(accessKey, secretKey, regionEndPoint)

        ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf TrustAllCertificateValidation)


    End Sub

    Public Function GetFile(ByVal filePath As String) As MemoryStream
        Dim ms As New MemoryStream
        Dim req As New GetObjectRequest()
        req.BucketName = _bucketName
        req.Key = filePath
        Dim resp As GetObjectResponse = _client.GetObject(req)
        'GetObjectResponse response = client.GetObject(request)
        resp.ResponseStream.CopyTo(ms)
        resp.ResponseStream.Close()
        Return (ms)
    End Function

    Public Function GetDownloadURL(ByVal filePath As String) As String
        Dim req As New GetPreSignedUrlRequest
        req.BucketName = _bucketName
        req.Key = filePath
        req.Expires = Now.AddHours(24)
        Return _client.GetPreSignedURL(req)
    End Function

    Public Sub UploadFile(ByVal stream As Stream, ByVal path As String)
        Try
            Dim req As New PutObjectRequest()
            req.BucketName = _bucketName
            req.Key = path
            req.InputStream = New MemoryStream
            stream.Seek(0, SeekOrigin.Begin)
            stream.CopyTo(req.InputStream)
            stream.Close()
            Dim resp As PutObjectResponse = _client.PutObject(req)
        Catch s3ex As AmazonS3Exception
            Throw s3ex
        Catch ex As Exception
            Throw New Exception("Upload to cloud storage failed; " + ex.Message)
        End Try
    End Sub

    Public Sub UploadHtmlFile(ByVal htmlContent As String, ByVal path As String)
        Try
            Dim req As New PutObjectRequest()
            req.BucketName = _bucketName
            req.Key = path
            req.ContentBody = htmlContent
            Dim resp As PutObjectResponse = _client.PutObject(req)
        Catch s3ex As AmazonS3Exception
            Throw s3ex
        Catch ex As Exception
            Throw New Exception("Upload to cloud storage failed; " + ex.Message)
        End Try
    End Sub

    Public Function DownloadHtmlFile(ByVal filePath As String, ByVal filename As String, ByVal response As HttpResponse) As String
        Dim req As New GetObjectRequest()
        req.BucketName = _bucketName
        req.Key = filePath
        Using resp As GetObjectResponse = _client.GetObject(req)
            Using streamReader As New System.IO.StreamReader(resp.ResponseStream)
                Dim htmlContent As String = streamReader.ReadToEnd()
                Return htmlContent
            End Using
        End Using
        'response.End()
    End Function

    Public Function DirectoryExists(ByVal path As String) As Boolean
        If Not path.EndsWith("/") Then
            path = path + "/"
        End If
        Dim req As New GetObjectRequest()
        req.BucketName = _bucketName
        req.Key = path
        Try
            Dim resp As GetObjectResponse = _client.GetObject(req)
        Catch ex As AmazonS3Exception
            Return False
        End Try
        Return True
    End Function

    Public Sub CreateDirectory(ByVal path As String)
        If Not path.EndsWith("/") Then
            path = path + "/"
        End If
        Dim req As New PutObjectRequest()
        req.BucketName = _bucketName
        req.Key = path
        req.InputStream = New MemoryStream
        Dim resp As PutObjectResponse = _client.PutObject(req)
    End Sub

    Public Function FileExists(ByVal path As String) As Boolean
        Dim req As New GetObjectRequest()
        req.BucketName = _bucketName
        req.Key = path
        Try
            Dim resp As GetObjectResponse = _client.GetObject(req)
        Catch ex As AmazonS3Exception
            Return False
        End Try
        Return True
    End Function

    Public Function DeleteFile(ByVal path As String) As Boolean
        Dim req As New DeleteObjectRequest
        req.BucketName = _bucketName
        req.Key = path
        Try
            Dim resp As DeleteObjectResponse = _client.DeleteObject(req)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function DeleteMultipleFile(ByVal dt As DataTable) As String
        Dim errorList As String = ""
        For Each dr As DataRow In dt.Rows
            Dim s3Path As String = dr.GetString("Path")
            Dim imgId As Integer = dr.GetInteger("Id")
            Dim req As New DeleteObjectRequest
            req.BucketName = _bucketName
            req.Key = s3Path
            Try
                Dim resp As DeleteObjectResponse = _client.DeleteObject(req)
            Catch ex As Exception
                errorList = IIf(errorList = "", errorList + imgId.ToString(), errorList + ", " + imgId.ToString())
            End Try
        Next
        Return errorList
    End Function

    'Public Sub InitiateMultiUpload(ByVal path As String)
    '    Dim req As New InitiateMultipartUploadRequest
    '    req.BucketName = _bucketName
    '    req.Key = path

    '    Dim resp As InitiateMultipartUploadResponse = _client.InitiateMultipartUpload(req)
    '    UploadID = resp.UploadId
    '    PartNumber = 1
    '    UploadResponses.Clear()
    'End Sub

    'Public Sub CompleteMultiUpload(ByVal path As String)

    '    Dim req As New CompleteMultipartUploadRequest()
    '    req.AddPartETags(UploadResponses)
    '    '        req = req.WithPartETags(UploadResponses)
    '    req.BucketName = _bucketName
    '    req.Key = path
    '    req.UploadId = UploadID

    '    Dim resp As CompleteMultipartUploadResponse = _client.CompleteMultipartUpload(req)
    'End Sub

    'Public Sub UploadPartFile(ByVal stream As Stream, ByVal path As String, ByVal chunk As Integer, ByVal chunkSize As Long)
    '    Dim req As New UploadPartRequest
    '    req.BucketName = _bucketName
    '    req.Key = path
    '    req.UploadId = UploadID
    '    req.PartNumber = PartNumber
    '    req.PartSize = 5242880
    '    req.InputStream = stream
    '    Dim res As UploadPartResponse = _client.UploadPart(req)
    '    UploadResponses.Add(res)
    '    PartNumber += 1
    'End Sub

    'Public Sub TransferFileToS3(ByVal stream As Stream, ByVal s3Path As String, Optional createProgressLog As Boolean = False)
    '    Dim config As New TransferUtilityConfig()
    '    '        config.DefaultTimeout = 1200000
    '    Dim t As New TransferUtility(_client, config)
    '    t.Upload(stream, _bucketName, s3Path)

    'End Sub

    Public Sub TransferFile(ByVal filePath As String, ByVal s3Path As String, Optional createProgressLog As Boolean = False)
        Dim config As New TransferUtilityConfig()
        'config.NumberOfUploadThreads = 1
        config.ConcurrentServiceRequests = 16
        config.MinSizeBeforePartUpload = 1024 * 1024 * 16

        Dim t As New TransferUtility(_client, config)
        Dim req As New TransferUtilityUploadRequest()
        req.FilePath = filePath
        req.BucketName = _bucketName
        req.Key = s3Path
        req.PartSize = (1024 * 1024 * 50)
        'req.Timeout = (60 * 1000 * 10)
        AddHandler req.UploadProgressEvent, AddressOf s3_UploadProgress

        fileInProgress = filePath
        t.Upload(req)
    End Sub

    Public Sub DownloadFile(ByVal filePath As String, ByVal filename As String, ByVal response As HttpResponse)
        Dim req As New GetObjectRequest()
        req.BucketName = _bucketName
        req.Key = filePath
        Dim resp As GetObjectResponse = _client.GetObject(req)
        response.Clear()
        response.ContentType = "application/octet-stream"
        If Not String.IsNullOrEmpty(filename) Then
            response.AddHeader("Content-Disposition", "attachment;filename=" & filename)
        End If
        resp.ResponseStream.CopyTo(response.OutputStream)
        'response.End()
    End Sub

    Public Sub DownloadImage(ByVal filePath As String, ByVal response As HttpResponse)
        Dim req As New GetObjectRequest()
        req.BucketName = _bucketName
        req.Key = filePath
        Dim ext As String = System.IO.Path.GetExtension(filePath)

        Dim extTag As String = ext.TrimStart(".").ToLower
        Dim mimeType As String

        Select Case extTag
            Case "jpg", "jpeg"
                mimeType = "image/jpeg"
            Case "gif"
                mimeType = "image/gif"
            Case "png"
                mimeType = "image/png"
            Case Else
                mimeType = "application/octet-stream"
        End Select
        Dim resp As GetObjectResponse
        Try
            resp = _client.GetObject(req)
        Catch exA3 As AmazonS3Exception
            Try
                If Char.IsLower(ext.Chars(0)) Then
                    ext = ext.ToUpper
                Else
                    ext = ext.ToLower
                End If
                req.Key = System.IO.Path.ChangeExtension(filePath, ext)
                resp = _client.GetObject(req)
            Catch ex As Exception
                Throw New Exception("Requested file does not exist of S3 Storage - (" + exA3.Message + ") " + filePath + " from bucket " + _bucketName)
            End Try
        End Try

        response.Clear()
        response.ContentType = mimeType
        resp.ResponseStream.CopyTo(response.OutputStream)
        HttpContext.Current.ApplicationInstance.CompleteRequest()
    End Sub


    Public Async Function DownloadImageAsync(ByVal filePath As String, ByVal response As HttpResponse, Optional retryWithExtCaseInversion As Boolean = True) As Threading.Tasks.Task
        Dim req As New GetObjectRequest()
        req.BucketName = _bucketName
        req.Key = filePath
        Dim ext As String = System.IO.Path.GetExtension(filePath)

        Dim extTag As String = ext.TrimStart(".").ToLower
        Dim mimeType As String

        Select Case extTag
            Case "jpg", "jpeg"
                mimeType = "image/jpeg"
            Case "gif"
                mimeType = "image/gif"
            Case "png"
                mimeType = "image/png"
            Case Else
                mimeType = "application/octet-stream"
        End Select

        Dim resp As GetObjectResponse

        Dim successful As Boolean = False, errorMessage As String = "", originalRequestedPath As String = filePath
        Try
            resp = Await _client.GetObjectAsync(req)
            successful = True
        Catch ex As Exception
            errorMessage = ex.Message
        End Try

        If Not successful AndAlso retryWithExtCaseInversion Then
            Try
                If Char.IsLower(ext.Chars(0)) Then
                    ext = ext.ToUpper
                Else
                    ext = ext.ToLower
                End If
                req.Key = System.IO.Path.ChangeExtension(filePath, ext)
                resp = Await _client.GetObjectAsync(req)
                successful = True
            Catch ex As Exception
                errorMessage = ex.Message
            End Try
        End If

        If Not successful Then
            errorMessage = errorMessage + " - " + originalRequestedPath
            CAMACloudRequestLogger.Default.LogError("AWS.S3", errorMessage)
            Throw New Exception("Requested file does not exist in S3 Storage - " & filePath)
        End If

        response.Clear()
        response.ContentType = mimeType
        resp.ResponseStream.CopyTo(response.OutputStream)
        HttpContext.Current.ApplicationInstance.CompleteRequest()


        'response.End()
    End Function

    Private Function TrustAllCertificateValidation(ByVal sender As Object, ByVal cert As X509Certificate, ByVal chain As X509Chain, ByVal errors As SslPolicyErrors) As Boolean
        Return True
    End Function

    Private Sub s3_UploadProgress(sender As Object, e As UploadProgressArgs)

        '''SBS->NEN  - Where does this get saved? I have commented this.

        'Dim fs As New StreamWriter(Path.ChangeExtension(fileInProgress, ".progress.txt"), True)
        'fs.WriteLine("{0}: TX: {1}			{2}% completed.", Now.ToString("HH:mm:ss"), e.TransferredBytes, e.PercentDone)
        'fs.Close()
    End Sub

    Public Shared Function GetBrokenPath(pkey As String) As String
        Dim s3Path As String = ""
        Dim breakUp() As Integer
        If pkey.Length <= 8 Then
            pkey = pkey.PadLeft(8, "0").ReverseString
            breakUp = New Integer() {2, 3, 3}
        ElseIf pkey.Length <= 15 Then
            pkey = pkey.PadLeft(15, "0").ReverseString
            breakUp = New Integer() {4, 4, 4, 3}
        Else
            pkey = pkey.PadLeft(20, "0").ReverseString
            breakUp = New Integer() {4, 4, 5, 4, 3}
        End If
        For Each count In breakUp
            If pkey.Length > 0 Then
                Dim part As String = pkey.Substring(0, Math.Min(count, pkey.Length))
                pkey = pkey.Substring(part.Length)
                s3Path += part + "/"
            End If
        Next
        Return s3Path
    End Function
End Class
