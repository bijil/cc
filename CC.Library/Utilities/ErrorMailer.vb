﻿Imports System.Net.Mail
Imports System.Collections.Specialized
Imports System.Configuration
Imports System.ServiceModel.Web
Imports System.ServiceModel
Imports System.ServiceModel.Channels
Imports System.Net
Imports CAMACloud.Data

Public Class ErrorMailer

    Public Shared Function ReportException(ByVal tag As String, ByVal ex As Exception, ByVal request As HttpRequest, Optional skipOrganization As Boolean = False) As Integer
        'If request.IsLocal Then
        '    Exit Sub
        'End If
        Dim errorId As Integer = 0, sendErrorReport As Boolean = False, repeatCount As Integer = 0, errorHtml As String

        errorHtml = ex.Message

        Try
            parseException(ex)
            errorHtml = ErrorMailer.ComposeHTMLException(ex, request, skipOrganization).ToString



            Dim referrer As String = ""
            If request.UrlReferrer IsNot Nothing Then
                referrer = request.UrlReferrer.OriginalString
            End If

            Try
                ErrorLogger.RegisterError(tag, request.Url.Host, request.Url.PathAndQuery, ex.Message, ex.StackTrace, referrer, request.UserHostAddress, request.UserAgent, errorHtml, errorId, sendErrorReport, repeatCount)
            Catch regerror As Exception
                sendErrorReport = True
            End Try


            If sendErrorReport Then
                sendExceptionMail(errorId, repeatCount, tag, ex, errorHtml)
            End If
        Catch exOuter As Exception
            Dim oem = exOuter.Message + vbNewLine + exOuter.StackTrace

            If exOuter.InnerException IsNot Nothing Then
                oem = exOuter.InnerException.Message + vbNewLine + exOuter.InnerException.StackTrace
            End If

            generateErrorLogFile("error-crash", errorHtml, False)
            generateErrorLogFile("error-crash-detail", oem, True)
        End Try


        Return errorId
    End Function


    Public Shared Sub ReportWCFException(tag As String, ByVal ex As Exception, ByVal context As OperationContext, ByVal request As HttpRequest, accessKey As String)
        Try
            parseException(ex)
            Dim errorHtml As String = ErrorMailer.ComposeWCFException(ex, context, request, accessKey).ToString
            If request.Url.Host.ToLower = "localhost" Then
                generateErrorLogFile("API-Dev", errorHtml)
            Else
                Dim errorId As Integer = 0, sendErrorReport As Boolean = False, repeatCount As Integer = 0

                Dim ipAddress As String = request.ServerVariables("REMOTE_ADDR")

                If request.Headers.AllKeys.Contains("X-Forwarded-For") Then
                    ipAddress = request.Headers("X-Forwarded-For")
                End If

                ErrorLogger.RegisterError(tag, request.Url.Host, request.Url.PathAndQuery, ex.Message, ex.StackTrace, "", ipAddress, "API-Client", errorHtml, errorId, sendErrorReport, repeatCount)

                If sendErrorReport Then
                    sendExceptionMail(errorId, repeatCount, tag, ex, errorHtml)
                End If

            End If
        Catch failed As Exception
            generateErrorLogFile(tag, ex.Message + vbNewLine + vbNewLine + ex.StackTrace + vbNewLine + vbNewLine + failed.Message, True)
        End Try
    End Sub

    Private Shared Sub parseException(ByRef ex As Exception)
        If TypeOf ex Is HttpUnhandledException Then
            ex = ex.InnerException
        End If
        If ex Is Nothing Then
            ex = New Exception("Unknown or blank exception")
        End If
        If TypeOf ex Is Threading.ThreadAbortException Then
            ex = New Exception("Thread aborted")
        End If
    End Sub

    Private Shared Sub sendExceptionMail(errorId As Integer, repeatCount As Integer, tag As String, ex As Exception, errorHtml As String)

        Dim subjectHeader As String = ""
        If errorId > 0 Then
            subjectHeader = "[CCERR-" + errorId.ToString
            If repeatCount > 1 Then
                subjectHeader += "-R" & repeatCount
            End If
            subjectHeader += "] "
        Else
            subjectHeader = Now.ToString("yyyyMMdd\HHHmmss")
        End If

        Dim subject As String = ex.Message
        If subject.Length > 80 Then
            subject = subject.Substring(0, 77) + "..."
        End If
        subject = subject.Replace(vbCrLf, " ").Replace(vbLf, " ").Replace(vbCr, " ").Replace("<", "?").Replace(">", "?")

        generateErrorLogFile(tag, errorHtml)

        Try
            Dim mail As New MailMessage
            mail.From = New MailAddress(ClientSettings.ErrorMailSender)
            mail.To.Add(New MailAddress(ClientSettings.ErrorMailRecipients))
            mail.Subject = subjectHeader + " : " + tag + " - " + subject
            mail.Body = errorHtml
            mail.IsBodyHtml = True
            AWSMailer.SendMail(mail)
        Catch ex1 As Exception
            generateErrorLogFile(tag, ex.Message + vbNewLine + vbNewLine + ex.StackTrace, True)
        End Try
    End Sub

    Public Shared Sub GenerateErrorLog(tag As String, ex As Exception)
        Dim oem = ex.Message + vbNewLine + ex.StackTrace

        If ex.InnerException IsNot Nothing Then
            oem = ex.InnerException.Message + vbNewLine + ex.InnerException.StackTrace
        End If
        generateErrorLogFile(tag, oem, True)
    End Sub

    Private Shared Sub generateErrorLogFile(tag As String, errorHtml As String, Optional isText As Boolean = False)
        Dim errorRoot As String = ClientSettings.ErrorLogRoot
        Dim errorPath As String = errorRoot + Now.ToString("yyyy-MM") + "\" + Now.ToString("dd\HHH") + "\" + tag + "-" + Now.ToString("mm-ss-fff")
        Dim logFile As String
        If isText Then
            logFile = errorPath + ".error.txt"
        Else
            logFile = errorPath + ".error.htm"
        End If


        If Not Directory.Exists(Path.GetDirectoryName(logFile)) Then
            Directory.CreateDirectory(Path.GetDirectoryName(logFile))
        End If

        Dim f As New IO.StreamWriter(logFile, False)
        f.WriteLine(errorHtml)
        f.Close()
    End Sub

    Public Shared Function ComposeHTMLException(ByVal ex As Exception, ByVal request As HttpRequest, Optional skipOrganization As Boolean = False) As XElement
        Dim html As XElement = <html><head><title>Exception Report</title></head><body></body></html>
        Dim heading As XElement = <h1 style="font-family:Segoe UI,Tahoma,Trebuchet MS,Arial">Detailed Error Message</h1>

        Dim tableFormat As XElement = <table style="font-family:Arial;font-size:9pt;width:100%;border-collapse:collapse;border-color:#CFCFCF;" cellspacing="0" cellpadding="5" border="1"></table>
        Dim rowFormat As XElement = <tr><td style="width:160px;vertical-align:top;background-color:Maroon;color:White;font-weight:bold;"></td><td style="vertical-align:top;"></td></tr>

        Dim formFormat As XElement = <table style="font-family:Arial;font-size:9pt;width:100%;border-collapse:collapse;border-color:#CFCFCF;" cellspacing="0" cellpadding="5" border="1"></table>
        Dim formRow As XElement = <tr><td style="width:250px;vertical-align:top;background-color:Maroon;color:White;font-weight:bold;"></td><td style="vertical-align:top;"></td></tr>


        Dim stackTrace As String = ""
        Dim iX As Exception = ex
        While iX IsNot Nothing
            stackTrace += "<b>[" + HttpUtility.HtmlEncode(iX.GetType.FullName) + "] " + HttpUtility.HtmlEncode(iX.Message) + "</b>" + vbNewLine + HttpUtility.HtmlEncode(iX.StackTrace) + vbNewLine + vbNewLine
            iX = iX.InnerException
        End While
        stackTrace = stackTrace.Trim.Replace(vbNewLine, "<br/>")


        Dim params As New NameValueCollection

        params.Add("Error Message", ex.Message)
        params.Add("Stack Trace", stackTrace)
        params.Add("Host Name", request.Url.Host)
        params.Add("Path and Query", request.Url.PathAndQuery)
        If request.UrlReferrer IsNot Nothing Then
            params.Add("Referrer", request.UrlReferrer.OriginalString)
            'Else
            '    params.Add("Referrer", "-- direct access --")
        End If

        params.Add("User IP Address", request.ClientIPAddress)
        params.Add("User Agent String", request.ServerVariables("HTTP_USER_AGENT"))
        'params.Add("Browser Name", SessionControl.GetBrowserName(request.ServerVariables("HTTP_USER_AGENT")))
        params.Add("JavaScript", IIf(request.Browser.EcmaScriptVersion.Major >= 1, "Enabled", "Disabled"))
        params.Add("Cookies", IIf(request.Browser.Cookies, "Enabled", "Disabled"))
        params.Add("Server Time-Stamp", Date.UtcNow.ToLongDateString + " " + Date.UtcNow.ToLongTimeString)

        Dim firstTable As XElement = XElement.Parse(tableFormat.ToString)
        For i As Integer = 0 To params.Count - 1
            Dim key As String = params.AllKeys(i)
            Dim s As XElement = XElement.Parse(rowFormat.ToString)
            s...<td>.Value = key
            Dim value As String = Coalesce(params(i), "").ToString
            Dim valueNode As XElement = s.Descendants.Skip(1).First
            Dim x As XElement
            Try
                If key = "Stack Trace" Then
                    x = XElement.Parse("<pre style='white-space: pre-wrap;word-break:keep-all;'>" + value.Trim + "</pre>")
                Else
                    'x = XElement.Parse("<div>" + value.Replace("&", "&amp;").Replace(">", "&gt;").Replace("<", "&lt;") + "</div>")
                    x = XElement.Parse("<div>" + value + "</div>")
                End If

            Catch exw As Exception
                x = XElement.Parse("<div>" + HttpUtility.HtmlEncode(value) + "</div>")
            End Try
            valueNode.Add(x)
            '<b>Exception of type 'System.Web.HttpUnhandledException' was thrown.</b><br/>   at System.Web.UI.Page.HandleError(Exception e)<br/>   at System.Web.UI.Page.ProcessRequestMain(Boolean includeStagesBeforeAsyncPoint, Boolean includeStagesAfterAsyncPoint)<br/>   at System.Web.UI.Page.ProcessRequest(Boolean includeStagesBeforeAsyncPoint, Boolean includeStagesAfterAsyncPoint)<br/>   at System.Web.UI.Page.ProcessRequest()<br/>   at System.Web.UI.Page.ProcessRequest(HttpContext context)<br/>   at ASP.admin_default_aspx.ProcessRequest(HttpContext context)<br/>   at System.Web.HttpApplication.CallHandlerExecutionStep.System.Web.HttpApplication.IExecutionStep.Execute()<br/>   at System.Web.HttpApplication.ExecuteStep(IExecutionStep step, Boolean& completedSynchronously)<br/><br/><b>Arithmetic operation resulted in an overflow.</b><br/>   at admin_Default.lbSimulateError_Click(Object sender, EventArgs e) in D:\Websites\ConsultSarath.com\admin\Default.aspx.vb:line 15<br/>   at System.Web.UI.WebControls.LinkButton.OnClick(EventArgs e)<br/>   at System.Web.UI.WebControls.LinkButton.RaisePostBackEvent(String eventArgument)<br/>   at System.Web.UI.Page.RaisePostBackEvent(IPostBackEventHandler sourceControl, String eventArgument)<br/>   at System.Web.UI.Page.ProcessRequestMain(Boolean includeStagesBeforeAsyncPoint, Boolean includeStagesAfterAsyncPoint)
            firstTable.Add(s)
        Next

        Dim body As XElement = html.Descendants.Skip(2).First
        heading.Value = "Server Error - " + request.Url.DnsSafeHost.Replace("www.", "").ToUpper
        If Not skipOrganization Then
            If HttpContext.Current IsNot Nothing AndAlso HttpContext.Current.GetCAMASession() IsNot Nothing Then
                heading.Value += " - " + HttpContext.Current.GetCAMASession.OrganizationName
            End If
        Else
            heading.Value += " - UNDETERMINED LICENSE"
        End If
        
        body.Add(heading)
        body.Add(firstTable)

        If request.Form.Count > 0 Then
            Dim hasFormElements As Boolean = False
            Dim formTable As XElement = XElement.Parse(formFormat.ToString)
            For Each key As String In request.Form.AllKeys
                If Not key.StartsWith("__") Then
                    Dim s As XElement = XElement.Parse(formRow.ToString)
                    s...<td>.Value = key
                    Dim value As String = Coalesce(request.Form(key), "").ToString
                    Dim valueNode As XElement = s.Descendants.Skip(1).First
                    Dim x As XElement = XElement.Parse("<div>" + value + "</div>")
                    valueNode.Add(x)
                    formTable.Add(s)
                    hasFormElements = True
                End If
            Next
            If hasFormElements Then
                body.Add(<h2 style="font-family:Tahoma,Trebuchet MS,Arial">FORM Contents</h2>)
                body.Add(formTable)
            End If
        End If

        If request.Files.Count > 0 Then
            Dim index As Integer = 0
            Dim fileTable As XElement = XElement.Parse(formFormat.ToString)
            For Each f In request.Files.AllKeys
                Dim file As HttpPostedFile = request.Files(f)
                index += 1
                Dim s1 As XElement = XElement.Parse(formRow.ToString)
                s1...<td>.Value = String.Format("File Name({0}):", index)
                s1.Descendants.Skip(1).First.Value = File.FileName
                fileTable.Add(s1)

                Dim s2 As XElement = XElement.Parse(formRow.ToString)
                s2...<td>.Value = String.Format("Content Type({0}):", index)
                s2.Descendants.Skip(1).First.Value = File.ContentType
                fileTable.Add(s1)

                Dim s3 As XElement = XElement.Parse(formRow.ToString)
                s3...<td>.Value = String.Format("File Length({0}):", index)
                s3.Descendants.Skip(1).First.Value = File.ContentLength
                fileTable.Add(s3)
            Next

            body.Add(<h2 style="font-family:Tahoma,Trebuchet MS,Arial">Uploaded Files</h2>)
            body.Add(fileTable)
        End If

        If request.Cookies.Count > 0 Then
            body.Add(<h2 style="font-family:Tahoma,Trebuchet MS,Arial">Cookies</h2>)

            Dim index As Integer = 0

            For Each cName As String In request.Cookies.AllKeys
                index += 1
                Dim cookieHeader As XElement = XElement.Parse(tableFormat.ToString)

                Dim c As HttpCookie = request.Cookies(index - 1)
                Dim s1 As XElement = XElement.Parse(rowFormat.ToString)
                s1...<td>.Value = String.Format("Cookie Name({0}):", index)
                s1.Descendants.Skip(1).First.Value = c.Name
                cookieHeader.Add(s1)

                If c.Domain <> Nothing Then
                    Dim s2 As XElement = XElement.Parse(rowFormat.ToString)
                    s2...<td>.Value = String.Format("Domain({0}):", index)
                    s2.Descendants.Skip(1).First.Value = c.Domain
                    cookieHeader.Add(s1)
                End If

                If c.Expires <> DateTime.MinValue AndAlso c.Expires <> Nothing Then
                    Dim s3 As XElement = XElement.Parse(rowFormat.ToString)
                    s3...<td>.Value = String.Format("Expires({0}):", index)
                    s3.Descendants.Skip(1).First.Value = c.Expires.ToString("yyyy-MM-dd HH:mm:ss")
                    cookieHeader.Add(s3)
                End If


                Dim s4 As XElement = XElement.Parse(rowFormat.ToString)

                If c.Value.Contains("&") Or c.Value.Contains("=") Then
                    s4...<td>.Value = "VALUES"
                Else
                    s4...<td>.Value = "Value"
                    Dim v As String = c.Value
                    If Not v.Contains(" ") And v.Length > 80 Then
                        Dim full As String = v
                        v = ""
                        For l As Integer = 1 To full.Length Step 80
                            v += Mid(full, l, 80) + " "
                        Next
                    End If
                    s4.Descendants.Skip(1).First.Value = v
                End If

                cookieHeader.Add(s4)

                For Each key In c.Values.AllKeys
                    If key <> Nothing Then
                        Dim s5 As XElement = XElement.Parse(rowFormat.ToString)
                        s5...<td>.Value = key
                        s5.Descendants.Skip(1).First.Value = c.Values(key)
                        cookieHeader.Add(s5)
                    End If
                Next

                body.Add(cookieHeader)
                body.Add(<br/>)
            Next


        End If

        Return html

    End Function

    Public Shared Function ComposeWCFException(ByVal ex As Exception, context As OperationContext, request As HttpRequest, accessKey As String) As XElement
        Dim html As XElement = <html><head><title>Exception Report</title></head><body></body></html>
        Dim heading As XElement = <h1 style="font-family:Segoe UI,Tahoma,Trebuchet MS,Arial">Detailed Error Message</h1>

        Dim tableFormat As XElement = <table style="font-family:Arial;font-size:9pt;width:100%;border-collapse:collapse;border-color:#CFCFCF;" cellspacing="0" cellpadding="5" border="1"></table>
        Dim rowFormat As XElement = <tr><td style="width:160px;vertical-align:top;background-color:Maroon;color:White;font-weight:bold;"></td><td style="vertical-align:top;"></td></tr>

        Dim formFormat As XElement = <table style="font-family:Arial;font-size:9pt;width:100%;border-collapse:collapse;border-color:#CFCFCF;" cellspacing="0" cellpadding="5" border="1"></table>
        Dim formRow As XElement = <tr><td style="width:250px;vertical-align:top;background-color:Maroon;color:White;font-weight:bold;"></td><td style="vertical-align:top;"></td></tr>


        Dim stackTrace As String = ""
        Dim iX As Exception = ex
        While iX IsNot Nothing
            stackTrace += "<b>[" + HttpUtility.HtmlEncode(iX.GetType.FullName) + "] " + HttpUtility.HtmlEncode(iX.Message) + "</b>" + vbNewLine + HttpUtility.HtmlEncode(iX.StackTrace) + vbNewLine + vbNewLine
            iX = iX.InnerException
        End While
        stackTrace = stackTrace.Trim.Replace(vbNewLine, "<br/>")


        'Dim prop As MessageProperties = context.IncomingMessageProperties
        'Dim endpoint As RemoteEndpointMessageProperty = TryCast(prop(RemoteEndpointMessageProperty.Name), RemoteEndpointMessageProperty)
        'Dim httpRequest As HttpRequestMessageProperty = TryCast(prop(HttpRequestMessageProperty.Name), HttpRequestMessageProperty)


        Dim params As New NameValueCollection

        Dim accountName As String = ""
        If accessKey <> "" Then
            accountName = Database.System.GetStringValue("SELECT Name + ' [' + CAST(Id AS VARCHAR(10)) + ']' FROM Organization WHERE Id = (SELECT OrganizationId FROM ApplicationAccess WHERE AccessKey = {0})".SqlFormatString(accessKey))
        End If

        Try
            params.Add("Error Message", ex.Message)
            params.Add("Stack Trace", stackTrace)
            params.Add("Host Name", request.Url.Host)
            params.Add("Access Key", accessKey)
            params.Add("Path and Query", request.Url.PathAndQuery)
            params.Add("Client IP Address", request.ServerVariables("REMOTE_ADDR"))
            params.Add("Client Port", request.ServerVariables("REMOTE_PORT"))
            If request.UserAgent IsNot Nothing Then
                params("User Agent") = request.UserAgent
            End If
            If request.Headers.AllKeys.Contains("X-Forwarded-For") Then
                params("Client IP Address") = request.Headers("X-Forwarded-For")
            End If

        Catch exa As Exception

        End Try



        ''params.Add("User Agent String", prop.)
        params.Add("Server Time-Stamp", Date.UtcNow.ToLongDateString + " " + Date.UtcNow.ToLongTimeString)

        Dim firstTable As XElement = XElement.Parse(tableFormat.ToString)
        For i As Integer = 0 To params.Count - 1
            Dim key As String = params.AllKeys(i)
            Dim s As XElement = XElement.Parse(rowFormat.ToString)
            s...<td>.Value = key
            Dim value As String = Coalesce(params(i), "").ToString
            Dim valueNode As XElement = s.Descendants.Skip(1).First
            Dim x As XElement
            Try
                If key = "Stack Trace" Then
                    x = XElement.Parse("<pre style='white-space: pre-wrap;word-break:keep-all;'>" + value.Trim + "</pre>")
                Else
                    x = XElement.Parse("<div>" + value + "</div>")
                End If
            Catch exw As Exception
                x = XElement.Parse("<div>" + HttpUtility.HtmlEncode(value) + "</div>")
            End Try
            valueNode.Add(x)
            '<b>Exception of type 'System.Web.HttpUnhandledException' was thrown.</b><br/>   at System.Web.UI.Page.HandleError(Exception e)<br/>   at System.Web.UI.Page.ProcessRequestMain(Boolean includeStagesBeforeAsyncPoint, Boolean includeStagesAfterAsyncPoint)<br/>   at System.Web.UI.Page.ProcessRequest(Boolean includeStagesBeforeAsyncPoint, Boolean includeStagesAfterAsyncPoint)<br/>   at System.Web.UI.Page.ProcessRequest()<br/>   at System.Web.UI.Page.ProcessRequest(HttpContext context)<br/>   at ASP.admin_default_aspx.ProcessRequest(HttpContext context)<br/>   at System.Web.HttpApplication.CallHandlerExecutionStep.System.Web.HttpApplication.IExecutionStep.Execute()<br/>   at System.Web.HttpApplication.ExecuteStep(IExecutionStep step, Boolean& completedSynchronously)<br/><br/><b>Arithmetic operation resulted in an overflow.</b><br/>   at admin_Default.lbSimulateError_Click(Object sender, EventArgs e) in D:\Websites\ConsultSarath.com\admin\Default.aspx.vb:line 15<br/>   at System.Web.UI.WebControls.LinkButton.OnClick(EventArgs e)<br/>   at System.Web.UI.WebControls.LinkButton.RaisePostBackEvent(String eventArgument)<br/>   at System.Web.UI.Page.RaisePostBackEvent(IPostBackEventHandler sourceControl, String eventArgument)<br/>   at System.Web.UI.Page.ProcessRequestMain(Boolean includeStagesBeforeAsyncPoint, Boolean includeStagesAfterAsyncPoint)
            firstTable.Add(s)
        Next

        Dim body As XElement = html.Descendants.Skip(2).First
        heading.Value = "Server Error - " + request.Url.Host.ToUpper
        If accountName <> "" Then
            heading.Value += " - " + accountName
        End If
        'If HttpContext.Current IsNot Nothing AndAlso HttpContext.Current.GetCAMASession() IsNot Nothing Then
        '    heading.Value += " - " + HttpContext.Current.GetCAMASession.OrganizationName
        'End If
        body.Add(heading)
        body.Add(firstTable)

        body.Add(<p style="font-size:smaller;">CC-ERROR-MAILER</p>)


        Return html
    End Function

End Class
