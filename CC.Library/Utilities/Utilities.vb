﻿Imports CAMACloud.Data
Imports System.Configuration
Imports System.Net.Mail

Public Module Utilities

	Public Sub WriteToLog(message As String, source As String, Optional type As EventLogEntryType = EventLogEntryType.Information)
		'Dim e As New EventLog("CAMACloudWeb")
		'e.Source = source
		'e.WriteEntry(message, type)
	End Sub

	Public Function i_(query As String) As Integer
		Return Database.Tenant.GetIntegerValue(query)
	End Function

	Public Function d_(query As String) As DataTable
		Return Database.Tenant.GetDataTable(query)
	End Function

	Public Function t_(query As String) As DataRow
		Return Database.Tenant.GetTopRow(query)
	End Function

	Public Function s_(query As String) As String
		Return Database.Tenant.GetStringValue(query)
	End Function

	Public Sub e_(sql As String)
		Database.Tenant.Execute(sql)
	End Sub

	Public Sub e_(sql As String, allowNulls As Boolean, ParamArray params As Object())
		Database.Tenant.Execute(sql.SqlFormat(allowNulls, params))
	End Sub

	Public Sub LogExceptionToDisk(tag As String, ex As Exception, request As HttpRequest)
		
		ErrorMailer.ReportException(tag, ex, request)


		'Try
		'	f.WriteLine(ex.Message)
		'	f.WriteLine()
		'	f.WriteLine(ex.StackTrace)
		'	f.WriteLine()

		'	Dim divLength As Integer = 50
		'	Dim divider As String = Space(divLength).Replace(" ", "-")

		'	f.WriteLine(request.Url.ToString)
		'	f.WriteLine()
		'	f.WriteLine("Header Values")
		'	f.WriteLine(divider)
		'	For Each key In request.Headers
		'		If key <> "Cookie" Then
		'			f.WriteLine("{0,-20}:{1}", key, request.Headers(key))
		'		End If
		'	Next
		'	f.WriteLine()

		'	If request.QueryString.Count > 0 Then
		'		f.WriteLine("Query String Values")
		'		f.WriteLine(divider)
		'		For Each key In request.QueryString
		'			If key <> "" Then
		'				f.WriteLine("{0,-20}:{1}", key, request.QueryString(key))
		'			End If
		'		Next
		'		f.WriteLine()
		'	End If

		'	If request.Cookies.Count > 0 Then
		'		f.WriteLine()
		'		f.WriteLine("Cookies")
		'		f.WriteLine(divider)
		'		For Each key In request.Cookies
		'			Dim ck As HttpCookie = request.Cookies(key)
		'			If ck.HasKeys Then
		'				f.WriteLine("{0,-20}:{1}", key, "")
		'				For Each skey In ck.Values
		'					Dim value As String = ck.Values(skey)
		'					If Len(value) > 60 Then
		'						value = value.Substring(0, 60) + " ...."
		'					End If
		'					f.WriteLine("{0,-20} {1}: {2}", "", skey, value)
		'				Next
		'			Else
		'				Dim value As String = ck.Value
		'				If Len(value) > 60 Then
		'					value = value.Substring(0, 60) + " ...."
		'				End If
		'				f.WriteLine("{0,-20}:{1}", key, value)
		'			End If
		'			f.WriteLine("{0,-20} {1}: {2}", "", "DOMAIN", ck.Domain)
		'			f.WriteLine("{0,-20} {1}: {2}", "", "PATH", ck.Path)
		'			f.WriteLine("{0,-20} {1}: {2}", "", "EXPIRES", ck.Expires.ToString)
		'			f.WriteLine()
		'		Next
		'	End If


		'	If request.Form.AllKeys.Count > 0 Then
		'		f.WriteLine()
		'		f.WriteLine("Form Contents")
		'		f.WriteLine(divider)
		'		For Each key In request.Form
		'			If Not key.ToString.StartsWith("__") Then
		'				f.WriteLine("{0,-20}:{1}", key, request.Form(key))
		'			End If
		'		Next
		'		f.WriteLine()
		'	End If
		'Catch ex1 As Exception
		'	f.Close()
		'	Throw ex1
		'End Try


	End Sub


    Public Function GetHashedString(data As String, Optional unicode As Boolean = False) As String
        Dim encoding As System.Text.Encoding = System.Text.Encoding.ASCII
        If unicode Then
            encoding = System.Text.Encoding.Unicode
        End If
        Dim sha1 = System.Security.Cryptography.SHA1.Create()
        Dim op = sha1.ComputeHash(encoding.GetBytes(data))
        Dim hashedString As String = "0x"
        For Each b In op
            hashedString += Hex(b).PadLeft(2, "0")
        Next
        Return hashedString
    End Function


    Public Function BaseCharMap(num As Long, Optional base As Integer = 26) As String
        Dim charRef As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
        Dim q As Long = 0, r As Long = 0
        q = num
        Dim out As String = ""
        While q > 0
            r = q Mod base
            q = q \ base
            out = charRef(r) + out
        End While
        Return out
    End Function



End Module
