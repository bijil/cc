﻿Imports CAMACloud.Data

Public Class ErrorLogger

    Public Shared Sub RegisterError(
                                   tag As String,
                                   hostName As String,
                                   pathAndQuery As String,
                                   message As String,
                                   stackTrace As String,
                                   referrer As String,
                                   ipAddress As String,
                                   userAgent As String,
                                   composedHTML As String,
                                   ByRef errorID As Integer,
                                   ByRef sendEmailReport As Boolean,
                                   ByRef repeatCount As Integer
                                )

        Dim sqlInsertFormat As String = "INSERT INTO ErrorLog (Tag, HostName, PathAndQuery, ShortMessage, Message, StackTrace, Referrer, LastIPAddress, LastUserAgent, ComposedHTML) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}); SELECT CAST(@@IDENTITY AS INT) As NewId;"
        Dim shortMessage As String = message
        If shortMessage.Length > 80 Then shortMessage = shortMessage.Substring(0, 80)

        Dim sqlCheckFormat As String = "SELECT * FROM ErrorLog WHERE HostName = {0} AND PathAndQuery = {1} AND ShortMessage = {2} AND Tag = {3}"

        Dim drCheck As DataRow = Database.LogDB.GetTopRow(sqlCheckFormat.SqlFormat(False, hostName, pathAndQuery, shortMessage, tag))
        If drCheck IsNot Nothing Then
            errorID = drCheck.GetInteger("ID")
            repeatCount = drCheck.GetInteger("RepeatCount") + 1
            Dim priority As Integer = drCheck.GetInteger("Priority")
            Dim emailAfterHours = 12
            Select Case priority
                Case 1
                    emailAfterHours = 8
                Case 2
                    emailAfterHours = 4
                Case 3
                    emailAfterHours = 2
                Case 4
                    emailAfterHours = 1
                Case 5
                    emailAfterHours = 0
            End Select
            If Not drCheck.GetBoolean("DoNotReport") Then
                Dim lastOccurance As DateTime = drCheck.GetDate("LastOccuranceTime")
                If (Now - lastOccurance).TotalHours > emailAfterHours Then
                    sendEmailReport = True
                End If
            End If
            Database.LogDB.Execute("UPDATE ErrorLog SET LastOccuranceTime = GETUTCDATE(), RepeatCount = {1}, LastIPAddress = {2}, LastUserAgent = {3} WHERE ID = {0}".SqlFormat(False, errorID, repeatCount, ipAddress, userAgent))
        Else
            Dim sqlInsert As String = sqlInsertFormat.SqlFormat(False, tag, hostName, pathAndQuery, shortMessage, message, stackTrace, referrer, ipAddress, userAgent, composedHTML)
            repeatCount = 1
            errorID = Database.LogDB.GetIntegerValue(sqlInsert)
            sendEmailReport = True
        End If

    End Sub

End Class
