﻿Imports System.Net.Mail
Imports System.Collections.Specialized
Imports System.Configuration
Imports System.ServiceModel.Web
Imports System.ServiceModel
Imports System.ServiceModel.Channels
Imports System.Net
Imports CAMACloud.Data
Public Class ServiceMailer

    Public Shared Sub SendEmail(subject As String, body As String)
        Dim mail As New MailMessage
        mail.From = New MailAddress(ClientSettings.DefaultSender)
        mail.To.Add("consultsarath@gmail.com")
        mail.Subject = Now.ToString("yyyyMMdd\HHHmmss") + " : " + subject
        mail.Body = body
        mail.IsBodyHtml = True
        'AWSMailer.SendMail(mail)
    End Sub

End Class
