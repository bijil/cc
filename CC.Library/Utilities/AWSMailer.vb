﻿Imports Microsoft.VisualBasic
Imports Amazon.SimpleEmail
Imports Amazon.SimpleEmail.Model
Imports System.Net.Mail
Imports System.Configuration

Public Class AWSMailer

    Public Shared Function SendMail(ByVal mail As MailMessage) As String
        Try

            Dim msg As New Model.Message
            msg.Body = New Body()
            If mail.IsBodyHtml Then
                msg.Body.Html = New Content(mail.Body)
            Else
                msg.Body.Text = New Content(mail.Body)
            End If
            msg.Subject = New Content(mail.Subject)

            Dim destination As New Destination
            For Each ma As MailAddress In mail.To
                destination.ToAddresses.Add(ma.ToString)
            Next
            For Each ma As MailAddress In mail.CC
                destination.CcAddresses.Add(ma.ToString)
            Next
            For Each ma As MailAddress In mail.Bcc
                destination.BccAddresses.Add(ma.ToString)
            Next
            Dim req As New SendEmailRequest()
            req.Source = mail.From.ToString

            For Each ma As MailAddress In mail.ReplyToList
                req.ReplyToAddresses.Add(ma.ToString)
            Next

            req.Message = msg
            req.Destination = destination
            Dim ASES As New AmazonSimpleEmailServiceConfig()
            ASES.SignatureVersion = "4"
			ASES.RegionEndpoint = Amazon.RegionEndpoint.USEast1
            Dim accessKey As String = ConfigurationManager.AppSettings("AWSAccessKey")  'Using the AppSetting directly, instead of ClientSettings.AWSAccessKey
            Dim secretKey As String = ConfigurationManager.AppSettings("AWSSecretKey")

            Dim client As New AmazonSimpleEmailServiceClient(accessKey, secretKey, ASES)

            Dim resp As SendEmailResponse = client.SendEmail(req)
            Return resp.MessageId
        Catch ex As Exception
            Try
                Dim smtp As New SmtpClient("email-smtp.us-east-1.amazonaws.com")
                smtp.Port = 25
                smtp.EnableSsl = False
                smtp.Send(mail)
            Catch exM As Exception
                Throw New Exception("Mail sending failed - " + ex.Message + "; AND " + exM.Message)
            End Try
            Return "SMTP"
        End Try
    End Function

End Class
