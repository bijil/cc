﻿Imports OfficeOpenXml
Imports System.IO
Imports System.Web.UI.WebControls
Imports DocumentFormat.OpenXml.Drawing.ChartDrawing
Imports OfficeOpenXml.Style
Imports System.Text.RegularExpressions
Imports HtmlAgilityPack
Imports System.Net

Public Class ExcelGenerator


    Private Class ExcelColumn
        Public Property DataField As String
        Public Property TransformType As Type
        Public Property Width As Unit
        Public Property HeaderText As String
        Public Property FormatString As String
        Public Property HorizontalAlignment As ExcelHorizontalAlignment = ExcelHorizontalAlignment.Left
        Public Property FooterExpression As String
        Public Property AutoWidth As Boolean = False
        Public Property WordWrap As Boolean = False

        Sub SetHorizontalAlignment(v As HorizontalAlign)
            Select Case v
                Case HorizontalAlign.Left
                    Me.HorizontalAlignment = ExcelHorizontalAlignment.Left
                Case HorizontalAlign.Center
                    Me.HorizontalAlignment = ExcelHorizontalAlignment.Center
                Case HorizontalAlign.Right
                    Me.HorizontalAlignment = ExcelHorizontalAlignment.Right
                Case HorizontalAlign.Justify
                    Me.HorizontalAlignment = ExcelHorizontalAlignment.Justify
                Case Else
                    Me.HorizontalAlignment = ExcelHorizontalAlignment.General
            End Select
        End Sub
    End Class

    Public Shared Function ExportGrid(grid As GridView, data As DataTable, title As String, Optional sheetName As String = "data", Optional showTotal As Boolean = False, Optional columnName As String = "", Optional ColumnList As List(Of String) = Nothing) As Stream
        Dim colProps As New ArrayList
        Dim hasGroupCaption As Boolean = False

        Dim fixedRows As Integer = 0

        Dim output As New DataTable
        Dim gridColIndex As Integer = -1
        For Each c As DataControlField In grid.Columns
            gridColIndex += 1

            Dim col As New ExcelColumn

            Dim visible = c.Visible

            If (visible) Then
                Dim sourceFieldName As String = Nothing
                If TypeOf c Is BoundField Then
                    sourceFieldName = CType(c, BoundField).DataField
                End If


                If sourceFieldName IsNot Nothing And Not String.IsNullOrEmpty(sourceFieldName) Then
                    If sourceFieldName = "#" Then
                        Dim oc As New DataColumn("#", GetType(Integer))
                        output.Columns.Add(oc)
                    Else
                        If data.Rows.Count > 0 Then
                            Dim dc As DataColumn = data.Columns(sourceFieldName)
                            If dc IsNot Nothing Then
                                Dim oc As New DataColumn(dc.ColumnName, dc.DataType)
                                output.Columns.Add(oc)
                                If dc.DataType.ToString() = "System.DateTime" Then
                                    col.FormatString = "mm/dd/yyyy hh:mm:ss AM/PM"
                                End If
                            End If
                        Else

                        End If

                    End If
                    col.HeaderText = c.HeaderText
                    If c.ItemStyle.Width.IsEmpty Then
                        col.AutoWidth = True
                    Else
                        col.Width = c.ItemStyle.Width
                    End If


                    col.DataField = sourceFieldName
                    col.SetHorizontalAlignment(c.ItemStyle.HorizontalAlign)
                    colProps.Add(col)
                End If

            End If

        Next


        Dim rowIndex As Integer = 0
        For Each dr As DataRow In data.Rows
            rowIndex += 1
            Dim nr = output.NewRow
            For Each oc As DataColumn In output.Columns
                If oc.ColumnName = "#" Then
                    nr(oc.ColumnName) = rowIndex
                Else
                    nr(oc.ColumnName) = dr(oc.ColumnName)
                End If
            Next
            output.Rows.Add(nr)
        Next

        Dim packager As New ExcelPackage

        Dim sheet = packager.Workbook.Worksheets.Add(sheetName)

        Dim totalWidth As Double = 0
        For ci As Integer = 0 To colProps.Count - 1
            Dim col = sheet.Column(ci + 1)
            Dim prop As ExcelColumn = colProps(ci)
            col.Width = prop.Width.Value / 7                    'Factor 7 is by trial and error, better formula will be good.
            col.Style.Numberformat.Format = prop.FormatString
            col.Style.HorizontalAlignment = prop.HorizontalAlignment
            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top
            col.Style.WrapText = prop.WordWrap
            If prop.AutoWidth Then
                col.AutoFit()
                totalWidth += 40
            Else
                totalWidth += col.Width
            End If
        Next

        Dim wideSheet As Boolean = totalWidth > 400

        fixedRows += 1
        Dim colIndex As Integer = 1
        For Each cp As ExcelColumn In colProps
            Dim headerCell = sheet.Cells(1, colIndex)
            headerCell.Value = cp.HeaderText
            setHeaderStyle(headerCell, cp)
            colIndex += 1
        Next
        sheet.Row(1).Height = 30
        sheet.Cells(2, 1).LoadFromDataTable(output, False)
        If ColumnList IsNot Nothing AndAlso ColumnList.Count > 0 Then
            sheet.Cells(output.Rows.Count + 3, 1, output.Rows.Count + 3, 2).Merge = True
            sheet.Cells(output.Rows.Count + 3, 1).Value = "TOTAL"
            For Each Name As String In ColumnList
                If output.Columns(Name).DataType.ToString <> "System.String" Then
                    Dim col_index = output.Columns.IndexOf(Name)
                    Dim col_Name = GetExcelColumnName(col_index + 1)
                    sheet.Cells(output.Rows.Count + 3, col_index + 1).Formula = "SUM(" + col_Name + "2:" + col_Name + (output.Rows.Count + 2).ToString() + ")"
                End If

            Next
            sheet.Cells(output.Rows.Count + 3, 1).Style.Font.Bold = True
            sheet.Cells(output.Rows.Count + 3, 3).Style.Font.Bold = True
            sheet.Cells(output.Rows.Count + 3, 1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right
            packager.Workbook.CalcMode = ExcelCalcMode.Manual
            packager.Workbook.Calculate()
        ElseIf showTotal AndAlso Not String.IsNullOrEmpty(columnName) Then
            Dim col_index = output.Columns.IndexOf(columnName)
            Dim col_Name = GetExcelColumnName(col_index + 1)
            sheet.Cells(output.Rows.Count + 3, col_index + 1).Formula = "SUM(" + col_Name + "2:" + col_Name + (output.Rows.Count + 2).ToString() + ")"
            sheet.Cells(output.Rows.Count + 3, 1, output.Rows.Count + 3, output.Columns.Count - 1).Merge = True
            sheet.Cells(output.Rows.Count + 3, 1).Value = "TOTAL"
            sheet.Cells(output.Rows.Count + 3, 1).Style.Font.Bold = True
            sheet.Cells(output.Rows.Count + 3, 3).Style.Font.Bold = True
            sheet.Cells(output.Rows.Count + 3, 1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right
            packager.Workbook.CalcMode = ExcelCalcMode.Manual
            packager.Workbook.Calculate()

        End If
        sheet.InsertRow(1, 1)
        fixedRows += 1
        Dim titleCell = sheet.Cells(1, 1)
        With titleCell
            .Value = title
        End With
        setTitleStyle(titleCell)
        sheet.Row(1).Height = 21
        If wideSheet Then
            titleCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left
        End If
        sheet.Cells(1, 1, 1, colProps.Count).Merge = True
        If colProps.Count > 7 Then
            sheet.PrinterSettings.Orientation = eOrientation.Landscape
        End If

        sheet.PrinterSettings.TopMargin = 1 / 2.54D
        sheet.PrinterSettings.PaperSize = ePaperSize.A4
        sheet.PrinterSettings.FitToPage = True
        sheet.PrinterSettings.FitToWidth = 1
        sheet.PrinterSettings.FitToHeight = 0
        sheet.PrinterSettings.BlackAndWhite = True
        sheet.PrinterSettings.RepeatRows = New ExcelAddress("$1:$" & fixedRows)
        Dim ms As New MemoryStream(packager.GetAsByteArray.ToArray)
        Return ms
    End Function

    Private Shared Sub setHeaderStyle(headerCell As OfficeOpenXml.ExcelRange, cp As ExcelColumn)
        headerCell.Style.Font.Bold = True
        headerCell.Style.Fill.PatternType = ExcelFillStyle.Solid
        headerCell.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGray)
        headerCell.Style.Border.Bottom.Style = ExcelBorderStyle.Thin
        headerCell.Style.Border.Bottom.Color.SetColor(System.Drawing.Color.DarkGray)
        headerCell.Style.HorizontalAlignment = cp.HorizontalAlignment
        headerCell.Style.VerticalAlignment = ExcelVerticalAlignment.Bottom
        headerCell.Style.WrapText = True
    End Sub

    Private Shared Sub setTitleStyle(titleCell As ExcelRange)
        With titleCell
            With .Style
                With .Fill
                    .PatternType = ExcelFillStyle.Solid
                    .BackgroundColor.SetColor(System.Drawing.Color.DarkGray)
                End With
                .Font.Color.SetColor(System.Drawing.Color.White)
                .HorizontalAlignment = ExcelHorizontalAlignment.Center
                .VerticalAlignment = ExcelVerticalAlignment.Center
                .Font.Bold = True
                .Font.Size = 16
            End With
        End With
    End Sub


    Private Shared Function GetExcelColumnName(columnNumber As Integer) As String
        Dim dividend As Integer = columnNumber
        Dim columnName As String = String.Empty
        Dim modulo As Integer

        While dividend > 0
            modulo = (dividend - 1) Mod 26
            columnName = Convert.ToChar(65 + modulo).ToString() & columnName
            dividend = CInt((dividend - modulo) / 26)
        End While

        Return columnName
    End Function

End Class

Public Class TableToExcel
    Dim excel As New ExcelPackage()
    Dim sheet As ExcelWorksheet
    Private maxrow As Integer
    Private cellsOccupied As Dictionary(Of String, Object) = New Dictionary(Of String, Object)()

    Public Sub New()
        sheet = excel.Workbook.Worksheets.Add("Sheet1")
        sheet.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center
        sheet.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center
        'sheet.Cells.Style.ShrinkToFit = True
        sheet.Cells.Style.WrapText = True
    End Sub
    Public Function Process(html As String, Optional multipleTables As Boolean = False) As Stream
        Try
            processExcel(html, multipleTables)
            Dim stream = New MemoryStream(excel.GetAsByteArray.ToArray)
            Return stream
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Sub processExcel(ByVal html As String, Optional multipleTables As Boolean = False)
        Dim doc = New HtmlAgilityPack.HtmlDocument()
        doc.LoadHtml(html)
        Dim table As HtmlNode = doc.GetElementbyId("test_table")
        If multipleTables = False Then
            Try
                process_Table(table)
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try
        Else
            Dim ta As HtmlNodeCollection = table.ChildNodes
            For Each node As HtmlNode In ta
                If node.Name = "table" Then
                    Try
                        process_Table(node, False)
                    Catch ex As Exception
                        Throw New Exception(ex.Message)
                    End Try
                End If
                If node.Name = "#text" And node.InnerText.Trim() <> "&nbsp;" Then
                    Dim newNode As HtmlNode = HtmlNode.CreateNode("<table><tr><th>" + node.InnerText.Trim() + "</th></tr></table")
                    Try
                        process_Table(newNode, False, True)
                    Catch ex As Exception
                        Throw New Exception(ex.Message)
                    End Try

                End If
            Next
        End If

    End Sub
    Private Sub process_Table(table As HtmlNode, Optional formatting As Boolean = True, Optional header As Boolean = False)
        Dim rowIndex As Integer = 1
        Dim colIndex As Integer = 1
        Dim rowSpan As Integer
        Dim colSpan As Integer
        Dim htmlnodecount As Integer = 0
        Dim temp As Single
        Dim MergedRow As Integer = 0
        Dim MergedCol As Integer = 0
        If (maxrow > 0) Then
            rowIndex = maxrow
        End If
        Dim htmlnode As HtmlNodeCollection = table.SelectNodes("tr")
        htmlnodecount = htmlnode.Count
        For Each nodes As HtmlNode In htmlnode
        	Dim attr As Boolean = False
            Dim th As Boolean = False
            Dim classAttr = nodes.Attributes.AttributesWithName("class")
            attr = classAttr.Select(Function(x) x.Value = "footer-row").FirstOrDefault()
            Dim tds As HtmlNodeCollection = nodes.SelectNodes("th")
            If tds Is Nothing Then
            	tds = nodes.SelectNodes("td")
            Else
            	th = True
            End If
            If tds IsNot Nothing Then
                For Each td As HtmlNode In tds
                    While cellsOccupied.ContainsKey(rowIndex.ToString() + "_" + colIndex.ToString())
                        colIndex += 1
                    End While
                    Dim atCollection As HtmlAttributeCollection = td.Attributes
                    Dim a = atCollection.AttributesWithName("class")
                    Dim k = a.Select(Function(x) x.Value = "ind-insuff").FirstOrDefault()
                    Dim w = a.Select(Function(x) x.Value = "label").FirstOrDefault()
                    If w Then
                    	sheet.Column(colIndex).Width = 25
                    End If
                    If th Then
                    	sheet.Cells(rowIndex, colIndex).Style.Font.Bold = True
                    End If
                    rowSpan = getSpan(td.OuterHtml, 0)
                    colSpan = getSpan(td.OuterHtml, 1)
                    If td.InnerHtml.Trim() <> "&nbsp;" Then
                        sheet.Cells(rowIndex, colIndex).Value = td.InnerText.Trim()
                        If header = True Then
                            sheet.Cells(rowIndex, 1, rowIndex, 8).Merge = True
                            sheet.Cells(rowIndex, 1, rowIndex, 8).Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid
                            sheet.Cells(rowIndex, 1, rowIndex, 8).Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.DimGray)
                        End If
                        If k Or attr Then

                            If rowIndex = htmlnodecount Then
                                sheet.Cells(rowIndex, colIndex).Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid
                                sheet.Cells(rowIndex, colIndex).Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Red)
                            Else
                                sheet.Cells(rowIndex, colIndex).Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid
                                sheet.Cells(rowIndex, colIndex).Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow)
                            End If

                        End If
                    End If

                    If Single.TryParse(td.InnerText, temp) And formatting = True Then
                        sheet.Cells(rowIndex, colIndex).Value = temp
                        sheet.Cells(rowIndex, colIndex).Style.Numberformat.Format = "#,##0.00"
                    End If
                    If colSpan > 1 And rowSpan > 1 Then
                        spanRowAndCol(rowIndex, colIndex, rowSpan, colSpan)
                        colIndex += colSpan
                    ElseIf colSpan > 1 Then
                        spanCol(rowIndex, colIndex, colSpan)
                        colIndex += colSpan
                    ElseIf rowSpan > 1 Then
                        spanRow(rowIndex, colIndex, rowSpan)
                        MergedRow = rowSpan
                        MergedCol = colSpan
                        colIndex += 1
                    Else
                        colIndex += 1
                    End If
                Next
            End If
            MergedRow -= 1
            rowIndex += 1
            If MergedRow >= 1 Then
                colIndex = MergedCol + 1
            Else
                colIndex = 1
            End If

            If rowIndex > maxrow Then
                maxrow = rowIndex
            End If
        Next
        'sheet.Cells(2, 1).Value = "ABCD"

    End Sub

    Private Function getSpan(html As String, Optional spanType As Integer = 0) As Integer
        Dim spanTypeText As String
        Dim span As Integer
        If spanType = 0 Then
            spanTypeText = "row"
        Else
            spanTypeText = "col"
        End If
        Dim equation As String = Regex.Match(html.ToLower(), spanTypeText + "span=.*?\d{1,}").ToString()
        If Not Int32.TryParse(Regex.Match(equation, "\d{1,}").ToString(), span) Then
            span = 1
        End If
        Return span
    End Function
    Private Sub spanRowAndCol(rowIndex As Integer, colIndex As Integer, rowSpan As Integer, colSpan As Integer)
        sheet.Cells(rowIndex, colIndex, rowIndex + rowSpan - 1, colIndex + colSpan - 1).Merge = True
        For i As Integer = 0 To i < rowSpan
            For j As Integer = 0 To j < colSpan
                cellsOccupied.Add((rowIndex + i) + "_" + (colIndex + j), True)
            Next
            If rowIndex + rowSpan - 1 > maxrow Then
                maxrow = rowIndex + rowSpan - 1
            End If

        Next
    End Sub
    Private Sub spanCol(rowIndex As Integer, colIndex As Integer, colSpan As Integer)
        sheet.Cells(rowIndex, colIndex, rowIndex, colIndex + colSpan - 1).Merge = True
    End Sub
    Private Sub spanRow(rowIndex As Integer, colIndex As Integer, rowSpan As Integer)

        sheet.Cells(rowIndex, colIndex, rowIndex + rowSpan - 1, colIndex).Merge = True
        For i As Integer = 0 To i < rowSpan
            cellsOccupied.Add((rowIndex + i) + "_" + colIndex, True)
            If rowIndex + rowSpan - 1 > maxrow Then
                maxrow = rowIndex + rowSpan - 1
            End If
        Next
    End Sub

End Class

Public Class TableExportExcel
    '' Exports HTML Tables to Excel. IF need more than one table, enclose thetables in a div with Id as TableParent
    '' To pass color, add RGB value as attribute. eg : <td color=215,251,180> 

    Dim excel As New ExcelPackage()
    Dim sheet As ExcelWorksheet
    Private FilledRow As Integer
    Private cellsOccupied As Dictionary(Of String, Object) = New Dictionary(Of String, Object)()

    Public Sub New()
        sheet = excel.Workbook.Worksheets.Add("Sheet1")
        sheet.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left
        sheet.Cells.Style.VerticalAlignment = ExcelVerticalAlignment.Center
        sheet.Cells.Style.ShrinkToFit = True
        sheet.Cells.Style.WrapText = True
    End Sub
    Public Function Generate(html As String) As Stream
        Try
            ProcessingTables(html)
            Dim stream = New MemoryStream(excel.GetAsByteArray.ToArray)
            Return stream
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Sub ProcessingTables(ByVal html As String)
        Dim doc = New HtmlAgilityPack.HtmlDocument()
        doc.LoadHtml(html)
        Dim table As HtmlNode = doc.GetElementbyId("TableParent")
        Dim ta As HtmlNodeCollection = table.ChildNodes
        For Each node As HtmlNode In ta
            If node.Name = "table" Then
                Try
                    FillExcelSheet(node)
                Catch ex As Exception
                    Throw New Exception(ex.Message)
                End Try
            End If
        Next
    End Sub

    Private Sub FillExcelSheet(table As HtmlNode)
        Dim rowIndex As Integer = 1
        Dim colIndex As Integer = 1
        Dim htmlnodecount As Integer = 0
        Dim htmlnode As HtmlNodeCollection = table.SelectNodes("tr")
        htmlnodecount = htmlnode.Count
        If FilledRow > 0 Then
            rowIndex = FilledRow
        End If
        For Each nodes As HtmlNode In htmlnode
            Dim tds As HtmlNodeCollection = nodes.SelectNodes("th")
            If tds Is Nothing Then
                tds = nodes.SelectNodes("td")
            End If
            colIndex = 1
            If tds IsNot Nothing Then
                For Each td As HtmlNode In tds
                    While cellsOccupied.ContainsKey(rowIndex.ToString() + "_" + colIndex.ToString())
                        colIndex += 1
                    End While
                    Dim col As String = td.GetAttributeValue("color", "")
                    Dim brdr As String = td.GetAttributeValue("b", "")
                    If td.InnerHtml.Trim() <> "&nbsp;" Then
                        sheet.Cells(rowIndex, colIndex).Value = td.InnerText.Trim()
                    End If
                    If col <> "" Then
                        Dim rgb = col.Split(","c).[Select](Function(n) Integer.Parse(n)).ToList()
                        sheet.Cells(rowIndex, colIndex).Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid
                        sheet.Cells(rowIndex, colIndex).Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(rgb(0), rgb(1), rgb(2)))
                    End If
                    If brdr = "TB" Then
                        ''sheet.Cells(rowIndex, colIndex).Style.Border.Top.Style = ExcelBorderStyle.Thick
                        sheet.Cells(rowIndex, colIndex).Style.Border.Bottom.Style = ExcelBorderStyle.Thick
                    End If
                    If brdr = "LR" Then
                        sheet.Cells(rowIndex, colIndex).Style.Border.Right.Style = ExcelBorderStyle.Thick
                        ''sheet.Cells(rowIndex, colIndex).Style.Border.Left.Style = ExcelBorderStyle.Thick
                    End If
                    colIndex += 1
                Next
            End If
            rowIndex += 1
        Next
        FilledRow = rowIndex + 1
    End Sub

End Class

