﻿Imports System.Net.Mail
Imports System.Configuration
Imports CAMACloud
Imports CAMACloud.Data

'Munir 14/08/2013
Public Class NotificationMailer

    Public Shared Sub SendNotification(ByVal loginId As String, ByVal subject As String, ByVal message As String, Optional ignoreError As Boolean = True)
        Dim Email As String
        Dim MobileCarrierId As Integer
        Dim MobileNo As String
        Dim Gateway As String
        Dim Body As String
        Dim drUserSet As DataRow = Database.Tenant.GetTopRow("SELECT * FROM UserSettings WHERE LoginId = {0}".SqlFormatString(loginId))
        If IsNothing(drUserSet) = False Then
            If drUserSet.GetBoolean("NotifyByEmail") = True Then
                Email = drUserSet.GetString("Email")
                If (Email IsNot Nothing Or Email <> "") Then
                    Try
                        Dim mail As New MailMessage
                        mail.From = New MailAddress(ClientSettings.NotificationSender)
                        mail.To.Add(Email)
                        mail.Subject = Now.ToString("yyyyMMdd\HHHmmss") + " :- " + subject
                        Body = message + "<br/><br/><br/><span style='font-size:smaller'>Replies to this message are not received.</span>"
                        mail.Body = Body
                        mail.IsBodyHtml = True
                        AWSMailer.SendMail(mail)
                    Catch ex1 As Exception
                        If Not ignoreError Then
                            Throw New Exception("Mail not send ...", ex1)
                        End If
                    End Try
                End If
            End If
            If drUserSet.GetBoolean("NotifyBySMS") = True Then
                MobileCarrierId = drUserSet.GetInteger("MobileCarrierId")
                MobileNo = drUserSet.GetString("Mobile")
                If String.IsNullOrEmpty(MobileNo) Then
                    ignoreError = True
                End If
                Gateway = Database.System.GetStringValue("SELECT Gateway FROM MobileCarrier WHERE Id = {0}".SqlFormatString(MobileCarrierId))
                'Email = drUserSet.GetString("Email")
                Email = MobileNo + Gateway
                Try
                    Dim mail As New MailMessage
                    mail.From = New MailAddress(ClientSettings.NotificationSender)
                    mail.To.Add(Email)
                    mail.Subject = subject
                    mail.Body = Body
                    mail.IsBodyHtml = True
                    AWSMailer.SendMail(mail)
                Catch ex1 As Exception
                    If Not ignoreError Then
                        Throw New Exception("SMS not send ...", ex1)
                    End If
                End Try

            End If
        End If
    End Sub
    Public Shared Sub SendNotificationToVendorCDS(ByVal loginId As String, ByVal type As String, ByVal page As String)
        Dim via As String = ""
        Select Case type
            Case 1
                via = "XML Import"
            Case 2
                via = "UI"
            Case 3
                via = "CSV Import"
        End Select
        Dim subject As String = "CAMA Cloud Configuration modified in {0}.".FormatString(HttpContext.Current.GetCAMASession.OrganizationName)
        Dim msg As String = "<p>Greetings from CAMA Cloud,</p><p>This is to notify that the environment '<b>{0}</b>' configuration has been changed/modified on {1} by '<b>{2}</b>' through {3} from {4} page.</p><p>Sincererly,<br/>CAMA Cloud</p>".FormatString(HttpContext.Current.GetCAMASession.OrganizationName, Date.UtcNow.ToString("MMMM dd, yyyy"), loginId, via, page)
        Try
            SendNotification("vendor-cds", subject, msg)
        Catch ex As Exception

        End Try
    End Sub
End Class
