﻿Imports System.Xml

Public Class TestClass

    Public Shared testData As XElement = <data>1375910976851|1949|CCSketchAnnotation||Test sketch note|
1375911143649|2005|FieldAlertType||2|
1375911153202|2005|FieldAlert||Test test test|
1375911171747|2005|470|R    |R         |
1375911180508|1949|795|null|7295|
1375911194505|1949|797|null|Why no data|
1375911215249|1949|354|A1   |21        |2141
1375911278775|2005|Reviewed||true|
1376054783696|1949|VectorString||% CAMACLOUD SKETCH v1
!L[0]:SKETCHLAYER01</data>

    Public Shared Sub TestParcelDataString(data As String)

        data = testData.Value

        If data.IsNotEmpty Then
            data = data.Trim
            Dim lines As String() = data.Split(vbLf)

            Dim splitLines As New List(Of String)

            For Each line As String In lines
                Dim v = line.Trim.Split("|")
                Dim isLine As Boolean = True
                If v.Length = 5 Or v.Length = 6 Then
                    Try
                        Dim timestamp As Long = CLng(v(0))
                    Catch ex As Exception
                        isLine = False
                    End Try
                Else
                    isLine = False
                    Try
                        Dim timestamp As Long = CLng(v(0))
                        isLine = True
                    Catch ex As Exception
                        isLine = False
                    End Try
                End If
                If isLine Then
                    splitLines.Add(line)
                Else
                    Dim lastLine = splitLines(splitLines.Count - 1)
                    lastLine += vbNewLine + line
                    splitLines(splitLines.Count - 1) = lastLine
                End If
            Next

            For Each line In splitLines
                Debug.Print("****" + line)
            Next
        End If
    End Sub

End Class
