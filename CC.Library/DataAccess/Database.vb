﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System
Imports System.Configuration
Imports System.Linq
Imports System.Web.Security
Namespace Data
    Public Class Database

        Const SlowQueryCutOff As Integer = 3000

        Const SESSION_DB_OBJECT As String = "USERDB"
        Const SESSION_DB_OBJECT2 As String = "USERDB2"
        Const SYSTEM_DB_OBJECT As String = "SYSDB"
        Const LOG_DB_OBJECT As String = "LOGDB"
        Const SESSION_CONN_STR As String = "SESSION_CONN_STR"

        Public DataCache As New Hashtable
        Public Property TransactionMode As Boolean
        Public Property CurrentTransaction As SqlTransaction

        Private _conn As New SqlConnection
        Private _connectionString As String

        Private _organizationId As Integer = -1
        Private _dbName As String
        Private _fullDbName As String

        Private _isApplicationConnection As Boolean = False

        Private Shared _lastExecutedQuery As String
        Public Property LastExecutedQuery As String
            Get
                Return _lastExecutedQuery
            End Get
            Set(ByVal value As String)
                _lastExecutedQuery = value
                'Diagnostics.Debug.Print(value)
                WriteToLog(value, "Query")
            End Set
        End Property

        Public ReadOnly Property Name As String
            Get
                Return _dbName
            End Get
        End Property

        Public ReadOnly Property FullName As String
            Get
                Return _fullDbName
            End Get
        End Property

        Public ReadOnly Property OrganizationId As Integer
            Get
                Return _organizationId
            End Get
        End Property
        
        Public Function RefreshName() As String
            _dbName = GetStringValue("SELECT DB_NAME();")
        End Function
        

        Public Shared Function GetConnectionStringFromOrganization(oid As Integer, Optional useDemo As Boolean = False) As String
            Dim connStr As String = "Server={0};Database={1};"
            Dim org As DataRow = Database.System.GetTopRow("SELECT * FROM Organization WHERE Id = " & oid)
            If org Is Nothing Then
                Throw New Exception("Invalid database connection attempt. Tenant connection might have been invoked from a stateless application.")
            End If
            Dim maintenanceMode As Boolean = False
            Dim maintenanceDuration As Integer = 60
            Try
                If org("Maintenance") = True Then
                    maintenanceMode = True
                    If org("MaintenanceDuration") IsNot DBNull.Value Then
                        maintenanceDuration = org("MaintenanceDuration")
                    End If
                End If
            Catch ex As Exception

            End Try
            If maintenanceMode Then
                Throw New Exceptions.EnvironmentMaintenanceException(maintenanceDuration)
            End If

            Dim hostField As String = "DBHost"
            Dim nameField As String = "DBName"
            Dim userField As String = "DBUser"
            Dim passwordField As String = "DBPassword"

            Dim host As CAMACloudHost = CAMACloudHost.GetWorkingHost
            If host.Stage = HostStage.ReleaseCandidate Or useDemo Then
                hostField = "DemoDBHost"
                nameField = "DemoDBName"
                userField = "DemoDBUser"
                passwordField = "DemoDBPassword"
            End If



            connStr = String.Format(connStr, org.GetString(hostField), org.GetString(nameField))
            If org.GetString(userField) = "" Then
                connStr += "Integrated Security=True;"
            Else
                connStr += "User Id={0};Password={1};".FormatString(org.GetString(userField), org.GetString(passwordField))
            End If
            connStr += "Connection Timeout=600;"
            Return connStr
        End Function

        Private Shared Function GetTenantConnectionString() As String

            Dim cs As Object = HttpContext.Current.Session(SESSION_CONN_STR)
            If cs Is Nothing Then
                cs = GetConnectionStringFromOrganization(HttpContext.Current.GetCAMASession().OrganizationId)
            End If
            Return cs.ToString()
        End Function

        Private Shared ReadOnly Property SystemConnectionString As String
            Get
                Dim systemConnection = ConfigurationManager.ConnectionStrings("ControlDB")
                If systemConnection Is Nothing Then
                    Throw New Exception("ControlDB ConnectionString is not initialized. Check machine.config.")
                End If
                Dim controlDbConnectionString As String = systemConnection.ConnectionString
                If Not controlDbConnectionString.ToLower.Contains("multipleactiveresultsets=true") Then
                    controlDbConnectionString = controlDbConnectionString.Trim.TrimEnd(";") + ";" + "MultipleActiveResultSets=True;"
                End If
                If Not controlDbConnectionString.ToLower.Contains("connection timeout") Then
                    controlDbConnectionString = controlDbConnectionString.Trim.TrimEnd(";") + ";" + "Connection Timeout=600;"
                End If
                Return controlDbConnectionString
            End Get
        End Property

        Public Sub New(organizationId As Integer, ByVal connectionString As String)
            MyClass.New(connectionString)
            _organizationId = organizationId
        End Sub

        Public Sub New(ByVal connectionString As String)
            _connectionString = connectionString
            _conn = New SqlConnection(connectionString)
        End Sub

        Public Property ConnectionString() As String
            Get
                Return _connectionString
            End Get
            Set(ByVal newConnectionString As String)
                _connectionString = newConnectionString
                If _conn IsNot Nothing Then
                    If _conn.State = ConnectionState.Open Then
                        _conn.Close()
                    End If
                    _conn.ChangeDatabase(_connectionString)
                    _conn.Open()
                End If
            End Set
        End Property

        Private Shared Property SystemDBObject() As Database
            Get
                If HttpContext.Current.Session Is Nothing Then
                    Return HttpContext.Current.Application(SYSTEM_DB_OBJECT)
                Else
                    Return HttpContext.Current.Session(SYSTEM_DB_OBJECT)
                End If
            End Get
            Set(value As Database)
                If HttpContext.Current.Session Is Nothing Then
                    value._isApplicationConnection = True
                    HttpContext.Current.Application(SYSTEM_DB_OBJECT) = value
                Else
                    HttpContext.Current.Session(SYSTEM_DB_OBJECT) = value
                End If
            End Set
        End Property

        Private Shared Property LogDBObject() As Database
            Get
                If HttpContext.Current.Session Is Nothing Then
                    Return HttpContext.Current.Application(LOG_DB_OBJECT)
                Else
                    Return HttpContext.Current.Session(LOG_DB_OBJECT)
                End If
            End Get
            Set(value As Database)
                If HttpContext.Current.Session Is Nothing Then
                    HttpContext.Current.Application(LOG_DB_OBJECT) = value
                Else
                    HttpContext.Current.Session(LOG_DB_OBJECT) = value
                End If
            End Set
        End Property

        Public Shared ReadOnly Property System() As Database
            Get
                If SystemDBObject Is Nothing Then
                    Dim db As New Database(SystemConnectionString)
                    db.open()
                    db._dbName = db.GetStringValue("SELECT DB_NAME();")
                    SystemDBObject = db
                End If
                Dim sdb As Database = SystemDBObject
                If sdb.Connection.State = ConnectionState.Closed Then
                    Try
                        sdb.Connection.Open()
                    Catch ex As SqlException
                        Throw New Exception("Network connection to control database is broken, or you have specified incorrect connection parameters.", ex)
                    End Try
                End If
                Return sdb
            End Get
        End Property

        Public Shared ReadOnly Property Tenant() As Database
            Get
                If HttpContext.Current.Request.Url.ToString.StartsWith("http://admin") Then
                    Throw New Exception("Database.Tenant access denied for admin!")
                End If
                If HttpContext.Current.Session(SESSION_DB_OBJECT) Is Nothing Then
                    Dim db As New Database(GetTenantConnectionString)

                    db._organizationId = HttpContext.Current.GetCAMASession.OrganizationId
                    db.open()
                    db._dbName = db.GetStringValue("SELECT DB_NAME();")
                    HttpContext.Current.Session(SESSION_DB_OBJECT) = db
                End If
                Dim sdb As Database = HttpContext.Current.Session(SESSION_DB_OBJECT)
                If sdb.Connection.State = ConnectionState.Closed Then
                    Try
                        sdb.Connection.Open()
                    Catch ex As SqlException
                        Throw New Exception("Network connection to database is broken, or you have specified incorrect connection parameters. Please contact Support.")
                    End Try
                End If
                Return sdb
            End Get
        End Property

        Public Shared ReadOnly Property LogDB() As Database
            Get
                If LogDBObject Is Nothing Then
                    Dim db As New Database(ApplicationSettings.LogDatabaseConnectionString)
                    db.open()
                    db._dbName = db.GetStringValue("SELECT DB_NAME();")
                    LogDBObject = db
                End If
                Dim ldb As Database = LogDBObject
                If ldb.Connection.State = ConnectionState.Closed Then
                    Try
                        ldb.Connection.Open()
                    Catch ex As SqlException
                        Throw New Exception("Network connection to log database is broken. Please check LogDBConnectionString in CCMAIN.dbo.ApplicationSettings", ex)
                    End Try
                End If
                Return ldb
            End Get
        End Property

        Public Shared ReadOnly Property AltTenant() As Database
            Get
                If HttpContext.Current.Session(SESSION_DB_OBJECT2) Is Nothing Then
                    Dim db As New Database(GetTenantConnectionString)
                    db.open()
                    HttpContext.Current.Session(SESSION_DB_OBJECT2) = db
                End If
                Dim sdb As Database = HttpContext.Current.Session(SESSION_DB_OBJECT2)
                If sdb.Connection.State = ConnectionState.Closed Then
                    Try
                        sdb.Connection.Open()
                    Catch ex As SqlException
                        Throw New Exception("Network connection to database is broken, or you have specified incorrect connection parameters. Please contact Support.")
                    End Try
                End If
                Return sdb
            End Get
        End Property


        Private Sub open()
            If _connectionString = "" Then
                _connectionString = GetTenantConnectionString()
                If _connectionString = "" Then
                    Throw New Exception("ConnectionString is not initialized. Configure ConnectionString setting in ASP.NET Configuration.")
                End If
            End If
            If _conn Is Nothing Then
                _conn = New SqlConnection(_connectionString)
            End If
            If _conn.State = ConnectionState.Closed Then
                Try
                    _conn.Open()
                Catch ex As SqlException
                    Throw New Exception("Network connection to database is broken, or you have specified incorrect connection parameters. Please contact Support.")
                End Try
            End If
        End Sub

        Public Sub Close()
            If _isApplicationConnection Then
                Return
            End If
            If _conn.State <> ConnectionState.Closed Then
                _conn.Close()
            End If
        End Sub

        Private Sub _closeConnectionIfNotInTransaction()
            If _isApplicationConnection Then
                Return
            End If
            If TransactionMode Then
                Return
            End If
            If _conn.State <> ConnectionState.Closed Then
                _conn.Close()
            End If
        End Sub

        Public ReadOnly Property Connection() As SqlConnection
            Get
                Return _conn
            End Get
        End Property

#Region "Database Queries and Related"

        Public Function GetDataSet(ByVal query As String) As DataSet
            Try
                Dim st As DateTime = DateTime.Now
                If _conn.State = ConnectionState.Closed Then _conn.Open()
                LastExecutedQuery = query

                Dim sqlStatement As String = "SET ARITHABORT ON; " + query
                Dim dt As New DataSet
                Dim ada As New SqlDataAdapter(sqlStatement, _conn)
                ada.SelectCommand.CommandTimeout = 1200
                ada.Fill(dt)
                _closeConnectionIfNotInTransaction()

                LogIfQueryIsSlow(st, query)
                Return dt
            Catch ex As Exception
            	_closeConnectionIfNotInTransaction()
        		Dim msg As String = "Error on executing query - [" & query & "]. Error Message is - " & ex.Message
                Throw New Exception(msg, ex)
            End Try

        End Function


        'Public Function GetDataTable(ByVal query As String, Optional ByVal timeout As Integer = 1200)
        '    If _conn.State = ConnectionState.Closed Then _conn.Open()
        '    LastExecutedQuery = query
        '    Dim st As DateTime = DateTime.Now

        '    Dim dt As New DataTable
        '    Dim cmd As New SqlCommand(query, _conn)
        '    cmd.CommandTimeout = timeout

        '    If CurrentTransaction IsNot Nothing Then cmd.Transaction = CurrentTransaction 
        '    Dim ada As New SqlDataAdapter(cmd)

        '    Try
        '        ada.Fill(dt)
        '        _closeConnectionIfNotInTransaction()
        '    Catch ex As Exception
        '        _closeConnectionIfNotInTransaction()
        '        If ex.Message.ToLower.StartsWith("invalid object name") Then
        '            Throw New Exceptions.MissingTableException(HttpContext.Current, ex)
        '        Else
        '            Dim msg As String = "Error on executing query - [" & query & "]. Error Message is - " & ex.Message
        '            Throw New Exception(msg)
        '        End If
        '    End Try
        '    LogIfQueryIsSlow(st, query)

        '    Return dt
        'End Function

        Public Function GetDataTable(ByVal query As String, Optional ByVal timeout As Integer = 1200) As DataTable
            If _conn.State = ConnectionState.Closed Then _conn.Open()
            LastExecutedQuery = query
            Dim st As DateTime = DateTime.Now
            Dim sqlStatement As String = "SET ARITHABORT ON; " + query
            Dim dt As New DataTable
            Dim cmd As New SqlCommand(sqlStatement, _conn)
            cmd.CommandTimeout = timeout

            If CurrentTransaction IsNot Nothing Then cmd.Transaction = CurrentTransaction
            Dim ada As New SqlDataAdapter(cmd)

            Try
                ada.Fill(dt)
                _closeConnectionIfNotInTransaction()
            Catch ex As Exception
            	_closeConnectionIfNotInTransaction()
                If ex.Message.ToLower.StartsWith("invalid object name") Then
                    Throw New Exceptions.MissingTableException(HttpContext.Current, ex)
                Else
                    Dim msg As String = "Error on executing query - [" & query & "]. Error Message is - " & ex.Message
                    Throw New Exception(msg, ex)
                End If
            End Try
            LogIfQueryIsSlow(st, query)

            'If cacheName IsNot Nothing AndAlso cacheName <> "" Then
            '    DataCache.Add(cacheName, dt)
            'End If
            Return dt
        End Function

        Public Function GetDataTableFromSP(ByVal storedProc As String, ByVal parameters As SqlParameterCollection) As DataTable
            If _conn.State = ConnectionState.Closed Then _conn.Open()
            LastExecutedQuery = storedProc
            Dim st As DateTime = DateTime.Now

            Dim cmd As New SqlCommand(storedProc, _conn)
            cmd.CommandType = CommandType.StoredProcedure
            If CurrentTransaction IsNot Nothing Then cmd.Transaction = CurrentTransaction
            For Each param As SqlParameter In parameters
                Dim newP As SqlParameter = cmd.CreateParameter()
                newP.ParameterName = param.ParameterName
                newP.DbType = param.DbType
                newP.Value = param.Value
                cmd.Parameters.Add(newP)
            Next

            Dim dt As New DataTable
            Dim ada As New SqlDataAdapter(cmd)
            Try
                ada.Fill(dt)
                _closeConnectionIfNotInTransaction()
            Catch ex As Exception
            	_closeConnectionIfNotInTransaction()
                Dim msg As String = "Error on executing command - [" & cmd.CommandText & "].# Error Message is - " & ex.Message
                Throw New Exception(msg, ex)
            End Try

            LogIfQueryIsSlow(st, storedProc)

            Return dt
        End Function

        Public Function GetDataTable(ByVal commandText As String, ByVal commandType As CommandType, Optional ByVal parameters As System.Collections.Specialized.NameValueCollection = Nothing, Optional ByVal timeout As Integer = 1200) As DataTable
            If _conn.State = ConnectionState.Closed Then _conn.Open()
            LastExecutedQuery = commandText
            Dim st As DateTime = DateTime.Now

            Dim cmd As New SqlCommand(commandText, _conn)
            cmd.CommandType = commandType
            cmd.CommandTimeout = timeout
            If CurrentTransaction IsNot Nothing Then cmd.Transaction = CurrentTransaction
            If parameters IsNot Nothing Then
                For Each key As String In parameters.AllKeys
                    Dim newP As SqlParameter = cmd.CreateParameter()
                    newP.ParameterName = key
                    If parameters(key) Is Nothing OrElse parameters(key) = "" OrElse parameters(key) = "0001-01-01" Then
                        'newP.Value = DBNull.Value
                        newP.Value = ""
                    Else
                        newP.Value = parameters(key)
                    End If

                    cmd.Parameters.Add(newP)
                Next
            End If
            Dim dt As New DataTable
            Dim ada As New SqlDataAdapter(cmd)
            Try
                ada.Fill(dt)
                _closeConnectionIfNotInTransaction()
            Catch ex As Exception
            	_closeConnectionIfNotInTransaction()
                Dim msg As String = "Error on executing command - [" & cmd.CommandText & "].# Error Message is - " & ex.Message
                If parameters IsNot Nothing Then
                    msg += ". Parameters are "
                    For Each key As String In parameters.AllKeys
                        msg += key + "='" & parameters(key) & "';"
                    Next
                End If
                Throw New Exception(msg, ex)
            End Try

            LogIfQueryIsSlow(st, commandText)

            Return dt
        End Function

        Public Function GetTopRow(ByVal query As String) As DataRow
            LastExecutedQuery = query
            Dim dt As DataTable = GetDataTable(query)
            If dt.Rows.Count > 0 Then
                Return dt.Rows(0)
            Else
                Return Nothing
            End If
        End Function

        Public Function GetStringValue(ByVal query As String, Optional nullValue As String = "") As String
            If _conn.State = ConnectionState.Closed Then _conn.Open()
            LastExecutedQuery = query
            Dim st As DateTime = DateTime.Now

            Dim cmd As New SqlCommand(query, _conn)
            cmd.CommandTimeout = 600
            If CurrentTransaction IsNot Nothing Then cmd.Transaction = CurrentTransaction
            Try
            	Dim res As Object = cmd.ExecuteScalar()
'            	If _organizationId = 1029 Then
'                	Dim dst As DateTime = DateTime.Now
'                	cmd.CommandText = "INSERT INTO temp_sql_log_rowuid_issue (spId, qry, sttime, endtime) values ({0}, {1}, {2}, {3})".SqlFormat(True, _conn.ClientConnectionId, query, st, dst)
'                	If _conn.ConnectionString.Contains("TEST_SAMPLE3") Then
'                		Dim cdd As Integer = cmd.ExecuteNonQuery
'                	End If
'                End If
                _closeConnectionIfNotInTransaction()

                LogIfQueryIsSlow(st, query)


                If res Is DBNull.Value Then
                    Return nullValue
                ElseIf res Is Nothing Then
                    Return nullValue
                Else
                    Return res.ToString
                End If
            Catch ex As Exception
                _closeConnectionIfNotInTransaction()
        		Dim msg As String = "Error on executing query - [" & query & "]. Error Message is - " & ex.Message
                Throw New Exception(msg)
            End Try

        End Function

        Public Function GetIntegerValue(ByVal query As String, Optional nullValue As Integer = 0) As Integer
            If _conn.State = ConnectionState.Closed Then _conn.Open()
            LastExecutedQuery = query
            Dim st As DateTime = DateTime.Now

            Try
                Dim cmd As New SqlCommand(query, _conn)
                cmd.CommandTimeout = 600
                If CurrentTransaction IsNot Nothing Then cmd.Transaction = CurrentTransaction
                Dim res As Object = cmd.ExecuteScalar()
'                If _organizationId = 1029 Then
'                	Dim dst As DateTime = DateTime.Now
'                	cmd.CommandText = "INSERT INTO temp_sql_log_rowuid_issue (spId, qry, sttime, endtime) values ({0}, {1}, {2}, {3})".SqlFormat(True, _conn.ClientConnectionId, query, st, dst)
'                	If _conn.ConnectionString.Contains("TEST_SAMPLE3") Then
'                		Dim cdd As Integer = cmd.ExecuteNonQuery
'                	End If
'                End If
                _closeConnectionIfNotInTransaction()

                LogIfQueryIsSlow(st, query)

                If IsNumeric(res) Then
                    Return res
                Else
                    Return nullValue
                End If
            Catch ex As SqlException
                _closeConnectionIfNotInTransaction()
                Throw New Exception("Error on executing:" + vbNewLine + query + vbNewLine + vbNewLine +"#"+ ex.Message, ex)
            End Try
        End Function

        Public Function GetIdentityFromInsert(ByVal query As String) As Integer
            If Not query.ToUpper.Contains("@@IDENTITY") Then
                If Not query.Trim.EndsWith(";") Then
                    query += ";"
                End If
                query += " SELECT CAST(@@IDENTITY AS INT) As NewId;"
            End If
            Return GetIntegerValue(query)
        End Function

        Public Function GetNewId(ByVal insertStatement As String) As Integer
            Return GetIntegerValue(insertStatement + "; SELECT CAST(@@IDENTITY AS INT) As NewId")
        End Function

        Public Function GetIntegerValueOrInvalid(ByVal query As String) As Integer
            LastExecutedQuery = query
            Dim st As DateTime = DateTime.Now

            If _conn.State = ConnectionState.Closed Then _conn.Open()
            Dim cmd As New SqlCommand(query, _conn)
            cmd.CommandTimeout = 600
            If CurrentTransaction IsNot Nothing Then cmd.Transaction = CurrentTransaction
            Dim res As Object = cmd.ExecuteScalar()
            _closeConnectionIfNotInTransaction()

            LogIfQueryIsSlow(st, query)

            If TypeOf res Is Integer Or TypeOf res Is Single Then
                Return res
            Else
                Return -1
            End If
        End Function

        Public Function GetDataReader(ByVal query As String) As SqlDataReader
            If _conn.State = ConnectionState.Closed Then _conn.Open()
            Dim cmd As New SqlCommand(query, _conn)
            cmd.CommandTimeout = 600
            Return cmd.ExecuteReader
        End Function

        Public Function EvaluateScalarFunction(ByVal functionCall As String, Optional ByVal owner As String = "dbo") As Object
            Dim sql As String = "SELECT " + owner + "." + functionCall.Trim
            Dim o As Object = GetTopRow(sql)(0)
            If TypeOf o Is DBNull Then
                Return Nothing
            Else
                Return o
            End If
        End Function

        Public Function EvaluateTableFunction(ByVal functionCall As String, Optional ByVal owner As String = "dbo") As DataTable
            Dim sql As String = "SELECT * FROM " + owner + "." + functionCall.Trim
            Dim dt As DataTable = GetDataTable(sql)
            Return dt
        End Function

        Public Function DoRowsExist(tableName As String, Optional filter As String = "1=1") As Boolean
            Dim sql As String = "SELECT COUNT(*) FROM " + tableName + " WHERE " + filter
            If GetIntegerValue(sql) > 0 Then
                Return True
            Else
                Return False
            End If
        End Function

#End Region

#Region "Execute Query Commands, and related"
        Public Function Execute(ByVal sql As String, Optional ByVal timeout As Integer = 1200) As Integer
            LastExecutedQuery = sql



            Dim st As DateTime = DateTime.Now

            If _conn.State = ConnectionState.Closed Then
                _conn.Open()
            End If

            Dim sqlStatement As String = sql
            Dim cmd As New SqlCommand(sqlStatement, _conn)
            If CurrentTransaction IsNot Nothing Then cmd.Transaction = CurrentTransaction
            cmd.CommandTimeout = timeout
            Try
            	Dim c As Integer = cmd.ExecuteNonQuery
'            	If _organizationId = 1029 Then
'                	Dim dst As DateTime = DateTime.Now
'                	cmd.CommandText = "INSERT INTO temp_sql_log_rowuid_issue (spId, qry, sttime, endtime) values ({0}, {1}, {2}, {3})".SqlFormat(True, _conn.ClientConnectionId, sql, st, dst)
'                	If _conn.ConnectionString.Contains("TEST_SAMPLE3") Then
'                		Dim cdd As Integer = cmd.ExecuteNonQuery
'                	End If
'                End If
                _closeConnectionIfNotInTransaction()

                LogIfQueryIsSlow(st, sql)


                Return c
            Catch ex As Exception
                _closeConnectionIfNotInTransaction()
                Dim msg As String = "Error {{ " + ex.Message + " }} on executing command - [" & sql & "]."
                Throw New Exception(msg, ex)
            End Try
        End Function
        
        
         Public Function GetDTWithDT(ByVal sql As String, ByVal dt As DataTable, ByVal dt2 As DataTable,Optional ByVal transType As String = Nothing, Optional ByVal timeout As Integer = 1200) As DataTable
            LastExecutedQuery = sql

            Dim sqlStatement As String = "SET ARITHABORT ON; " + sql
            Dim st As DateTime = DateTime.Now
			 Dim dtResult As New DataTable
            If _conn.State = ConnectionState.Closed Then
                _conn.Open()
            End If
            Dim cmd As New SqlCommand(sql, _conn)
            cmd.CommandType = CommandType.StoredProcedure
           	 cmd.Parameters.AddWithValue("@dataTable", dt)
           	 If dt2 Is Nothing Then
            Else
            	cmd.Parameters.AddWithValue("@dataTable2", dt2)
            End If
            If transType Is Nothing Then
            Else
            	If transType<>"" then
            		cmd.Parameters.AddWithValue("@TransType", transType)
            	End if
            End If
            If CurrentTransaction IsNot Nothing Then cmd.Transaction = CurrentTransaction
            cmd.CommandTimeout = timeout
            Try
              
           		 Dim ada As New SqlDataAdapter(cmd)
                	ada.Fill(dtResult)
                _closeConnectionIfNotInTransaction()

                LogIfQueryIsSlow(st, sql)
                Return dtResult
            Catch ex As Exception
                _closeConnectionIfNotInTransaction()
                Dim msg As String = "Error {{ " + ex.Message + " }} on executing command with data table- [" & sql & "]."
                Throw New Exception(msg)
            End Try
         End Function

        Public Function ExecuteWithDataTable(ByVal sql As String, ByVal dt1 As DataTable, Optional ByVal dt2 As DataTable = Nothing, Optional ByVal timeout As Integer = 1200) As Integer
            LastExecutedQuery = sql

            Dim st As DateTime = DateTime.Now
            Dim dtResult As New DataTable
            If _conn.State = ConnectionState.Closed Then
                _conn.Open()
            End If

            Dim cmd As New SqlCommand(sql, _conn)
            cmd.CommandType = CommandType.StoredProcedure
            If dt1.Rows.Count > 0 Then
                cmd.Parameters.AddWithValue("@dataTable1", dt1)
            End If
            If dt2 Is Nothing Then
            Else
                cmd.Parameters.AddWithValue("@dataTable2", dt2)
            End If
            If CurrentTransaction IsNot Nothing Then cmd.Transaction = CurrentTransaction
            cmd.CommandTimeout = timeout
            Try

                Dim c As Integer = cmd.ExecuteNonQuery
                _closeConnectionIfNotInTransaction()

                LogIfQueryIsSlow(st, sql)


                Return c
            Catch ex As Exception
                _closeConnectionIfNotInTransaction()
                Dim msg As String = "Error {{ " + ex.Message + " }} on executing command with data table- [" & sql & "]."
                Throw New Exception(msg)
            End Try
        End Function

        Public Function ExecuteWithDatatableAndParams(ByVal sql As String, ByVal dt1 As DataTable, Optional ByVal dt2 As DataTable = Nothing, Optional parmeter As Integer = 0, Optional ByVal timeout As Integer = 1200) As DataSet
            LastExecutedQuery = sql

            Dim st As DateTime = DateTime.Now
            Dim dtResult As New DataSet
            If _conn.State = ConnectionState.Closed Then
                _conn.Open()
            End If

            Dim cmd As New SqlCommand(sql, _conn)
            cmd.CommandType = CommandType.StoredProcedure
            If dt1.Rows.Count > 0 Then
                cmd.Parameters.AddWithValue("@dataTable1", dt1)
            End If
            If dt2 Is Nothing Then
            Else
                cmd.Parameters.AddWithValue("@dataTable2", dt2)
            End If
            If parmeter <> 0 Then
                cmd.Parameters.AddWithValue("@Id", parmeter)
            End If
            If CurrentTransaction IsNot Nothing Then cmd.Transaction = CurrentTransaction
            cmd.CommandTimeout = timeout
            Try

                Dim ado As New SqlDataAdapter(cmd)
                ado.Fill(dtResult)
                _closeConnectionIfNotInTransaction()

                LogIfQueryIsSlow(st, sql)


                Return dtResult
            Catch ex As Exception
                _closeConnectionIfNotInTransaction()
                Dim msg As String = "Error {{ " + ex.Message + " }} on executing command with data table- [" & sql & "]."
                Throw New Exception(msg)
            End Try
        End Function
        'Use this function only for generated SQL script evaluations.
        'The difference is only in the error message - Not providing the actual SQL statement in exception message
        Public Function TrialExecute(ByVal sql As String) As Integer
            LastExecutedQuery = sql
            If _conn.State = ConnectionState.Closed Then
                _conn.Open()
            End If

            Dim sqlStatement As String = sql
            Dim cmd As New SqlCommand(sqlStatement, _conn)
            If CurrentTransaction IsNot Nothing Then cmd.Transaction = CurrentTransaction
            cmd.CommandTimeout = 1200
            Try
                Dim c As Integer = cmd.ExecuteNonQuery
                _closeConnectionIfNotInTransaction()
                Return c
            Catch ex As Exception
                _closeConnectionIfNotInTransaction()
                Throw ex
            End Try
        End Function

        Public Function Execute(ByVal sql As String, ByVal ParamArray values As String()) As Integer
            Dim newSql As String = String.Format(sql, values)
            LastExecutedQuery = newSql
            Return Execute(newSql)
        End Function

        Public Sub UpdateField(ByVal tableName As String, ByVal columnName As String, ByVal newValue As String, ByVal condition As String)
            Dim newSql As String = "UPDATE " + tableName + " SET " + columnName + " = '" + newValue + "' WHERE " + condition
            LastExecutedQuery = newSql
            Execute(newSql)
        End Sub

#End Region

#Region "Special Functions"

        Public Function DoesTableExists(ByVal tableName As String, Optional ByVal schema As String = "dbo") As Boolean
            If GetTopRow("SELECT * FROM INFORMATION_SCHEMA.Tables WHERE TABLE_NAME = '" + tableName + "' AND TABLE_SCHEMA = '" + schema + "'") IsNot Nothing Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Function GetAllTableNames() As String()
            Dim names = GetDataTable("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE'").Rows.Cast(Of DataRow)().Select(Function(x) x.GetString("TABLE_NAME"))
            Return names.ToArray
        End Function

        Public Sub DeleteTableRecordsWithIdentityReset(tableName As String)
            If DoesTableExists(tableName) Then
                'Execute("DELETE FROM {0}; DBCC CHECKIDENT ({0}, RESEED, 0);SELECT CAST(@@ROWCOUNT  AS INT) As recordAffected".FormatString(tableName))
                Execute("DELETE FROM {0}; DBCC CHECKIDENT ({0}, RESEED, 10000);".FormatString(tableName))
            End If
        End Sub

        Public Sub DropTable(tableName As String)
            If DoesTableExists(tableName) Then
                Execute("DROP TABLE {0};".FormatString(tableName))
            End If
        End Sub

        Public Function GetRowCount(tableName As String) As Long
            If DoesTableExists(tableName) Then
                Return GetIntegerValue("SELECT COUNT(*) FROM {0};".FormatString(tableName))
            Else
                Throw New Exceptions.MissingTableException(HttpContext.Current, tableName)
            End If
        End Function

        Function GetDataType(sketchVectorTargetTable As String, fieldName As String) As Type
            Dim testSql As String = "SELECT TOP 10 " + fieldName + " FROM " + sketchVectorTargetTable
            Dim dt As DataTable = GetDataTable(testSql)
            Return dt.Columns(0).DataType
        End Function

        Public Sub BulkLoadTableFromDataTable(tableName As String, dataTable As DataTable, Optional tableFields As List(Of KeyValuePair(Of String, String)) = Nothing, Optional backupData As Boolean = False)
            Try
                Dim tableExists As Boolean = DoesTableExists(tableName)
                If tableExists AndAlso Not backupData Then
                    DropTable(tableName)
                    tableExists = False
                End If
                If _conn.State = ConnectionState.Closed Then
                    _conn.Open()
                End If
                If Not tableExists Then
                    Dim creator As New SqlTableCreator(_conn)
                    creator.DestinationTableName = tableName
                    creator.CreateFromDataTable(dataTable, tableFields)
                End If
                Using bulkCopy As New SqlBulkCopy(_conn)
                    bulkCopy.DestinationTableName = tableName
                    For Each col As DataColumn In dataTable.Columns
                        Dim mapping As New SqlBulkCopyColumnMapping
                        mapping.DestinationColumn = col.ColumnName
                        mapping.SourceColumn = col.ColumnName
                        bulkCopy.ColumnMappings.Add(mapping)
                    Next
                    bulkCopy.WriteToServer(dataTable)
                    _closeConnectionIfNotInTransaction()
                End Using
            Catch ex As Exception
                Dim errorMessage As String = "Error on loading " + dataTable.TableName + "; "
                If ex.Message.Contains("colid") Then
                    Try
                        Dim colid As Integer = ex.Message.Substring(ex.Message.IndexOf("colid") + 5).Trim.TrimEnd(".").Split(New Char() {" ", ".", ","})(0)
                        errorMessage += "Field: " + dataTable.Columns(colid - 1).ColumnName + "; "
                    Catch ex1 As Exception

                    End Try
                End If
                errorMessage += ex.Message
                _closeConnectionIfNotInTransaction()
                Throw New Exception(errorMessage, ex)
            End Try
        End Sub

        Public Sub LoadTableFromDataTable(tableName As String, dataTable As DataTable,Optional adhoc As Integer=0)
         	Try
         		If adhoc = 1 Then
         			HttpContext.Current.Session("ptid") = Database.Tenant.GetIntegerValue("INSERT INTO PriorityList (UserName,Status) VALUES('" + Membership.GetUser().ToString() +"','Pending')SELECT @@IDENTITY ")
         			dataTable.Columns.Add("PriorityListId")
         			
            	For Each ro As DataRow In dataTable.Rows
            		ro("PriorityListId") = HttpContext.Current.Session("ptid")
            	Next	
         		End If
                If _conn.State = ConnectionState.Closed Then
                    _conn.Open()
                End If
                Using bulkCopy As New SqlBulkCopy(_conn)
                    bulkCopy.DestinationTableName = tableName
                    For Each col As DataColumn In dataTable.Columns
                        Dim mapping As New SqlBulkCopyColumnMapping
                        mapping.DestinationColumn = col.ColumnName
                        mapping.SourceColumn = col.ColumnName
                        bulkCopy.ColumnMappings.Add(mapping)
                    Next
                    bulkCopy.WriteToServer(dataTable)
                    _closeConnectionIfNotInTransaction()
                End Using
            Catch ex As Exception
                Dim errorMessage As String = "Error on loading " + dataTable.TableName + "; "
                If ex.Message.Contains("colid") Then
                    Try
                        Dim colid As Integer = ex.Message.Substring(ex.Message.IndexOf("colid") + 5).Trim.TrimEnd(".").Split(New Char() {" ", ".", ","})(0)
                        errorMessage += "Field: " + dataTable.Columns(colid - 1).ColumnName + "; "
                    Catch ex1 As Exception

                    End Try
                End If
                errorMessage += ex.Message
                _closeConnectionIfNotInTransaction()
                Throw New Exception(errorMessage, ex)
            End Try
        End Sub

#End Region


        Public Sub LogIfQueryIsSlow(startTime As DateTime, query As String, Optional cutoff As Integer = SlowQueryCutOff)
            If HttpContext.Current Is Nothing Then
                Return
            End If
            If HttpContext.Current.Session Is Nothing Then
                Exit Sub
            End If
            If _organizationId = -1 Then
                Return
            End If
            Dim durationSeconds As Integer = (DateTime.Now - startTime).TotalMilliseconds
            If durationSeconds > cutoff Then
                Dim dbName As String = Database.System.GetStringValue("SELECT DBName FROM Organization WHERE Id = " & _organizationId)
                If Not Database.LogDB.DoesTableExists("SlowQueries") Then
                    Database.LogDB.Execute("CREATE TABLE SlowQueries (ID INT IDENTITY PRIMARY KEY, QueryTime DATETIME NOT NULL DEFAULT GETDATE(), DBName VARCHAR(100), QueryText VARCHAR(MAX), Duration INT)")
                End If
                Dim sqlInsert As String = "INSERT INTO SlowQueries (DBName, QueryText, Duration) VALUES ({0}, {1}, {2});".SqlFormatString(dbName, query, durationSeconds)
                Database.LogDB.Execute(sqlInsert)
            End If
        End Sub


        Private _application As ClientApplication
        Public ReadOnly Property Application As ClientApplication
            Get
                If _application Is Nothing Then
                    _application = New ClientApplication(Me)
                End If
                Return _application
            End Get
        End Property


        Public Shared ReadOnly Property AtiTemp() As Database
            Get
                Dim host = HttpContext.Current.Request.Url.Host
                Dim db As Database
                Dim name As String
                If host.Contains("beta") Then
                    db = New Database("Server=10.0.0.112;Database=RMS_APP_beta;user = sa; password = Cama@2020;")
                    name = "RMS_APP_beta"
                ElseIf (host.Contains("alpha")) Then
                    db = New Database("Server=10.0.0.112;Database=RMS_APP_alpha;user = sa; password = Cama@2020;")
                    name = "RMS_APP_alpha"
                ElseIf (host.Contains("localhost") Or host = "") Then
                    db = New Database("Server=192.168.2.11\MSSQLSERVER17;Database=RMS_APP;user = sa; password = 123;")
                    name = "RMS_APP"
                Else
                    db = New Database("Server=10.0.0.112;Database=RMS_APP;user = sa; password = Cama@2020;")
                    name = "RMS_APP"
                End If
                db.open()
                db._dbName = name
                Dim sdb As Database = db
                If sdb.Connection.State = ConnectionState.Closed Then
                    Try
                        sdb.Connection.Open()
                    Catch ex As SqlException
                        Throw New Exception("Network connection to control database is broken, or you have specified incorrect connection parameters.", ex)
                    End Try
                End If
                Return sdb
            End Get
        End Property

        Public Shared ReadOnly Property AtiMain() As Database
            Get
                Dim host = HttpContext.Current.Request.Url.Host
                Dim db As Database
                Dim name As String
                If host.Contains("beta") Then
                    db = New Database("Server=10.0.0.112;Database=ATI_MAIN_BETA;user = sa; password = Cama@2020;")
                    name = "ATI_MAIN_BETA"
                ElseIf (host.Contains("alpha")) Then
                    db = New Database("Server=10.0.0.112;Database=ATI_MAIN_ALPHA;user = sa; password = Cama@2020;")
                    name = "ATI_MAIN_ALPHA"
                ElseIf (host.Contains("localhost") Or host = "") Then
                    db = New Database("Server=192.168.2.11\MSSQLSERVER17;Database=ATIMAIN;user = sa; password = 123;")
                    name = "ATIMAIN"
                Else
                    db = New Database("Server=10.0.0.112;Database=ATI_MAIN;user = sa; password = Cama@2020;")
                    name = "ATI_MAIN"
                End If
                db.open()
                db._dbName = name
                Dim sdb As Database = db
                If sdb.Connection.State = ConnectionState.Closed Then
                    Try
                        sdb.Connection.Open()
                    Catch ex As SqlException
                        Throw New Exception("Network connection to control database is broken, or you have specified incorrect connection parameters.", ex)
                    End Try
                End If
                Return sdb
            End Get
        End Property

        Public Shared ReadOnly Property AtiUtilityTemp() As Database
            Get
                Dim host = HttpContext.Current.Request.Url.Host
                Dim db As Database
                Dim name As String
                If host.Contains("beta") Then
                    db = New Database("Server=10.0.0.112;Database=RMS_UTILITY_beta;user = sa; password = Cama@2020;")
                    name = "RMS_APP_beta"
                ElseIf (host.Contains("alpha")) Then
                    db = New Database("Server=10.0.0.112;Database=RMS_UTILITY_alpha;user = sa; password = Cama@2020;")
                    name = "RMS_APP_alpha"
                ElseIf (host.Contains("localhost") Or host = "") Then
                    db = New Database("Server=192.168.2.11\MSSQLSERVER17;Database=RMS_UTILITY;user = sa; password = 123;")
                    name = "RMS_APP"
                Else
                    db = New Database("Server=10.0.0.112;Database=RMS_UTILITY;user = sa; password = Cama@2020;")
                    name = "RMS_APP"
                End If
                db.open()
                db._dbName = "DCS_APP"
                Dim sdb As Database = db
                If sdb.Connection.State = ConnectionState.Closed Then
                    Try
                        sdb.Connection.Open()
                    Catch ex As SqlException
                        Throw New Exception("Network connection to control database is broken, or you have specified incorrect connection parameters.", ex)
                    End Try
                End If
                Return sdb
            End Get
        End Property

    End Class
End Namespace



