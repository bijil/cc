﻿Imports System.Data.SqlClient

Namespace Data
    Public Class SqlTableCreator
#Region "Instance Variables"
        Private _connection As SqlConnection
        Public Property Connection() As SqlConnection
            Get
                Return _connection
            End Get
            Set(value As SqlConnection)
                _connection = value
            End Set
        End Property

        Private _transaction As SqlTransaction
        Public Property Transaction() As SqlTransaction
            Get
                Return _transaction
            End Get
            Set(value As SqlTransaction)
                _transaction = value
            End Set
        End Property

        Private _tableName As String
        Public Property DestinationTableName() As String
            Get
                Return _tableName
            End Get
            Set(value As String)
                _tableName = value
            End Set
        End Property
#End Region

#Region "Constructor"
        Public Sub New()
        End Sub
        Public Sub New(connection As SqlConnection)
            Me.New(connection, Nothing)
        End Sub
        Public Sub New(connection As SqlConnection, transaction As SqlTransaction)
            _connection = connection
            _transaction = transaction
        End Sub
#End Region

#Region "Instance Methods"
        Public Function Create(schema As DataTable) As Object
            Return Create(schema, 0)
        End Function
        Public Function Create(schema As DataTable, numKeys As Integer) As Object
            Dim primaryKeys As Integer() = New Integer(numKeys - 1) {}
            For i As Integer = 0 To numKeys - 1
                primaryKeys(i) = i
            Next
            Return Create(schema, primaryKeys)
        End Function
        Public Function Create(schema As DataTable, primaryKeys As Integer()) As Object
            Dim sql As String = GetCreateSQL(_tableName, schema, primaryKeys)

            Dim cmd As SqlCommand
            If _transaction IsNot Nothing AndAlso _transaction.Connection IsNot Nothing Then
                cmd = New SqlCommand(sql, _connection, _transaction)
            Else
                cmd = New SqlCommand(sql, _connection)
            End If

            Return cmd.ExecuteNonQuery()
        End Function

        Public Function CreateFromDataTable(table As DataTable, tableFields As List(Of KeyValuePair(Of String, String))) As Object
            Dim sql As String = GetCreateFromDataTableSQL(_tableName, table, tableFields)

            Dim cmd As SqlCommand
            If _transaction IsNot Nothing AndAlso _transaction.Connection IsNot Nothing Then
                cmd = New SqlCommand(sql, _connection, _transaction)
            Else
                cmd = New SqlCommand(sql, _connection)
            End If

            Return cmd.ExecuteNonQuery()
        End Function
#End Region

#Region "Static Methods"

        Public Shared Function GetCreateSQL(tableName As String, schema As DataTable, primaryKeys As Integer()) As String
            Dim sql As String = "CREATE TABLE " & tableName & " (" & vbLf

            ' columns
            For Each column As DataRow In schema.Rows
                If Not (schema.Columns.Contains("IsHidden") AndAlso CBool(column("IsHidden"))) Then
                    sql += column("ColumnName").ToString() & " " & SQLGetType(column) & "," & vbLf
                End If
            Next
            sql = sql.TrimEnd(New Char() {","c, ControlChars.Lf}) & vbLf

            ' primary keys
            Dim pk As String = "CONSTRAINT PK_" & tableName & " PRIMARY KEY CLUSTERED ("
            Dim hasKeys As Boolean = (primaryKeys IsNot Nothing AndAlso primaryKeys.Length > 0)
            If hasKeys Then
                ' user defined keys
                For Each key As Integer In primaryKeys
                    pk += schema.Rows(key)("ColumnName").ToString() & ", "
                Next
            Else
                ' check schema for keys
                Dim keys As String = String.Join(", ", GetPrimaryKeys(schema))
                pk += keys
                hasKeys = keys.Length > 0
            End If
            pk = pk.TrimEnd(New Char() {","c, " "c, ControlChars.Lf}) & ")" & vbLf
            If hasKeys Then
                sql += pk
            End If
            sql += ")"

            Return sql
        End Function

        Public Shared Function GetCreateFromDataTableSQL(tableName As String, table As DataTable, Optional tableFields As List(Of KeyValuePair(Of String, String)) = Nothing) As String
            Dim sql As String = "CREATE TABLE [" & tableName & "] (" & vbLf
            ' columns
            If tableFields Is Nothing Then
                For Each column As DataColumn In table.Columns
                    sql += "[" + column.ColumnName & "] " & SQLGetType(column) & "," & vbLf
                Next
            Else
                For Each column As DataColumn In table.Columns
                    Dim colSpec As String = SQLGetType(column)
                    If tableFields.Where(Function(x) x.Key = column.ColumnName).Count > 0 Then
                        colSpec = tableFields.Where(Function(x) x.Key = column.ColumnName).First.Value
                    End If
                    sql += "[" + column.ColumnName & "] " & colSpec & "," & vbLf
                Next
            End If
            
            sql = sql.TrimEnd(New Char() {","c, ControlChars.Lf}) & vbLf
            ' primary keys
            If table.PrimaryKey.Length > 0 Then
                sql += "CONSTRAINT [PK_" & tableName & "] PRIMARY KEY CLUSTERED ("
                For Each column As DataColumn In table.PrimaryKey
                    sql += "[" + column.ColumnName & "],"
                Next
                sql = sql.TrimEnd(New Char() {","c}) & ")" & vbLf
            End If

            sql += ")" + vbLf
            Return sql
        End Function

        Public Shared Function GetPrimaryKeys(schema As DataTable) As String()
            Dim keys As New List(Of String)()

            For Each column As DataRow In schema.Rows
                If schema.Columns.Contains("IsKey") AndAlso CBool(column("IsKey")) Then
                    keys.Add(column("ColumnName").ToString())
                End If
            Next

            Return keys.ToArray()
        End Function

        ' Return T-SQL data type definition, based on schema definition for a column
        Public Shared Function SQLGetType(columnName As String, type As Object, columnSize As Integer, numericPrecision As Integer, numericScale As Integer) As String
            Select Case type.ToString()
                Case "System.String"
                    Return "VARCHAR(" & (If((columnSize = -1), "MAX", columnSize.ToString)) & ")"
                Case "System.Decimal"
                    If numericScale > 0 Then
                        Return "FLOAT"
                    ElseIf numericPrecision > 10 Then
                        Return "BIGINT"
                    Else
                        Return "INT"
                    End If
                Case "System.Double", "System.Single"
                    Return "FLOAT"
                Case "System.Int64"
                    Return "BIGINT"
                Case "System.Int16", "System.Int32"
                    Return "INT"
                Case "System.DateTime"
                    Return "DATETIME"
                Case "System.Boolean"
                    Return "BIT"
                Case "System.Byte"
                    Return "TINYINT"
                Case Else

                    Throw New Exception("The uploaded data contains an unallowed datatype - " & type.ToString() & " on field [" + columnName + "]. Please verify your schema and data, and try again.")
            End Select
        End Function

        ' Overload based on row from schema table
        Public Shared Function SQLGetType(schemaRow As DataRow) As String
            Return SQLGetType(schemaRow("ColumnName"), schemaRow("DataType"), Integer.Parse(schemaRow("ColumnSize").ToString()), Integer.Parse(schemaRow("NumericPrecision").ToString()), Integer.Parse(schemaRow("NumericScale").ToString()))
        End Function
        ' Overload based on DataColumn from DataTable type
        Public Shared Function SQLGetType(column As DataColumn) As String
            Return SQLGetType(column.ColumnName, column.DataType, column.MaxLength, 10, 2)
        End Function
#End Region
    End Class
End Namespace