﻿Public Class ScreenMenu
    Inherits LiteralControl

    Dim content As XElement = <nav class="screen-menu" action="_nav(this);showDigitalPRC();">
                                  <div class="nav-icon nav-digital-prc"></div>
                                  <div class="nav-div">
                                      <a class="menulink">Property Record Card</a>
                                      <span>View all details in Property Record Card format (Digital PRC)</span>
                                  </div>
                              </nav>

    Public Property Action As String
    Public Property CssClass As String
    Public Property Title As String
    Public Property Info As String

    Protected Overrides Sub Render(output As System.Web.UI.HtmlTextWriter)
        content.@action = "_nav(this); doNavAction('" + Action.Replace("'", "\'") + "');"
        content...<div>.First.@class = ("nav-icon " + CssClass).Trim
        content...<div>.Last.<a>.Value = Title
        content...<div>.Last.<span>.Value = Info
        output.WriteLine(content.ToString)
    End Sub

End Class
