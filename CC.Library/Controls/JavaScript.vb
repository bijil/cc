﻿Imports System.IO

Public Class JavaScript
	Inherits LiteralControl

	Private _includeFolder As String = ""

	Public Property IncludeFolder As String
		Get
			Return _includeFolder
		End Get
		Set(value As String)
			_includeFolder = value
		End Set
	End Property

	Protected Overrides Sub Render(output As System.Web.UI.HtmlTextWriter)
		Dim root As String = Page.MapPath("~/")
		For Each fi In Directory.GetFiles(Page.MapPath(IncludeFolder))
			Dim info As New FileInfo(fi)
            Dim path As String = Page.ResolveUrl(fi.Replace(root, "/").Replace("\", "/")) + "?" & info.LastWriteTimeUtc.Ticks
			Dim script = <script type="text/javascript" src=""></script>
			script.@src = path
			output.WriteLine(script.ToString())
		Next
	End Sub

End Class
