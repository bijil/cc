﻿Public Class JavaScriptCompressor
    Inherits LiteralControl
#Region "Variables"
    Private _OutPutFileName, _JSPath As String
    Private _EnableCompression As Boolean = True
    Private _IncludeSubFolders As Boolean = False
#End Region
#Region "Property"

    Public Property IncludeSubFolders() As Boolean
        Get
            Return _IncludeSubFolders
        End Get
        Set(ByVal value As Boolean)
            _IncludeSubFolders = value
        End Set
    End Property
    Public Property EnableCompression() As Boolean
        Get
            Return _EnableCompression
        End Get
        Set(ByVal value As Boolean)
            _EnableCompression = value
        End Set
    End Property

    Public Property JSPath As String
        Get
            Return _JSPath
        End Get
        Set(value As String)
            _JSPath = value
        End Set
    End Property
    Public Property OutPutFileName As String
        Get
            Return _OutPutFileName
        End Get
        Set(value As String)
            _OutPutFileName = value
        End Set
    End Property

#End Region

#Region " Modules"
    Protected Overrides Sub Render(output As System.Web.UI.HtmlTextWriter)

        Dim root As String = Page.MapPath("~/")
        Dim IncludeAllFolders As Boolean = True
        If Not _EnableCompression Then
            For Each fi In Directory.GetFiles(Page.MapPath(JSPath), "*.js", IIf(IncludeAllFolders = IncludeSubFolders, IO.SearchOption.AllDirectories, IO.SearchOption.TopDirectoryOnly))
                Dim info As New FileInfo(fi)
                Dim path As String = Page.ResolveUrl(fi.Replace(root, "/").Replace("\", "/")) + "?" & info.LastWriteTimeUtc.Ticks
                Dim script = <script type="text/javascript" src=""></script>
                script.@src = path
                output.WriteLine(script.ToString())
            Next
        Else
            Dim JSroot As String = Page.MapPath(JSPath)
            Dim v = Now.Ticks
            Dim newPath As String = JSPath.Replace(root, "/") & "/" & OutPutFileName & "?V=" & DataTools.DataConversion.EncodeToBase64(v) & "&IAF=" & DataTools.DataConversion.EncodeToBase64(IncludeAllFolders) & "&jsP=" & DataTools.DataConversion.EncodeToBase64(JSPath) & "&OFN=" & DataTools.DataConversion.EncodeToBase64(OutPutFileName)
            Dim fullyQualifiedUrl As String = Page.ResolveUrl(newPath)
            Dim script = <script type="text/javascript" src=""></script>
            script.@src = fullyQualifiedUrl
            output.WriteLine(script.ToString())
        End If
    End Sub
#End Region
End Class
