﻿Namespace Formatting
    Public Class JSONStandardResponse
        Public Property status As String = "Unknown"
        Public Property message As String
        Public Property stackTrace As String
        Public Property failed As Boolean
        Public Property licenseFailed As Boolean
        Public Property data As DataTable
        Public Property header As DataRow
        Public Property records As Integer
        Public Property dataQuery As String
    End Class
End Namespace
