﻿Imports System.Reflection
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Json
Imports CAMACloud.Formatting
Imports System.Text.RegularExpressions
Imports Newtonsoft.Json

Public Class JSONResponse

    Private _context As HttpContext

    Private _objectCount, _dataSize, _processTime As Integer
    Private _response, _error As String
    Private _success As Boolean = True
    Private _intermediateTick As Long


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property ObjectsAffected As Integer
        Get
            Return _objectCount
        End Get
    End Property

    Public ReadOnly Property DataSize As Integer
        Get
            Return _dataSize
        End Get
    End Property

    Public ReadOnly Property ProcessTime As Integer
        Get
            Return _processTime
        End Get
    End Property

    Public ReadOnly Property ResponseText As String
        Get
            Return _response
        End Get
    End Property

    Public ReadOnly Property ErrorMessage As String
        Get
            Return _error
        End Get
    End Property

    Public ReadOnly Property Success As Boolean
        Get
            Return _success
        End Get
    End Property

    Private Sub resetVariables()
        _objectCount = 0
        _success = True
        _error = ""
        _response = ""
        _dataSize = 0
        _processTime = 0
    End Sub

    Private Shared Function jstr(s As String, Optional ByVal allowReplace As Boolean = True) As String
        s = Regex.Replace(s, "[^\u0020-\u007E\u0009\u000A\u000D]", String.Empty) 'Remove all characters excluding Tab, NL, CR, visible printable ASCII characters.
        s = s.Replace(Chr(27), String.Empty)
        If s.TrimEnd().Length > 0 Then
            s = s.TrimEnd()
        End If
        If (allowReplace) Then
            s = """" + s.Replace("\", "\\").Replace("/", "\/").Replace(">", "&gt;").Replace("<", "&lt;").Replace("""", "\""").Replace(vbCr, "").Replace(vbLf, "\n").Replace(vbTab, "\t") + """"
        Else
            s = """" + s.Replace("\", "\\").Replace("/", "\/").Replace("""", "\""").Replace(vbCr, "").Replace(vbLf, "\n").Replace(vbTab, "\t") + """"
        End If
        Return s
    End Function


    Private Shared Function prop(name As String, value As String)
        Return jstr(name) + ": " + value
    End Function

    Private Shared Function typestr(t As Type, value As Object, Optional allowNull As Boolean = True, Optional allowReplace As Boolean = True)
        If allowNull Then
            If value Is Nothing OrElse value Is DBNull.Value Then
                Return "null"
            End If
        End If
        If Not allowNull Then
            If value Is Nothing OrElse value Is DBNull.Value Then
                Select Case t
                    Case GetType(String)
                        value = ""
                    Case GetType(Integer), GetType(Decimal)
                        value = 0
                    Case GetType(Boolean)
                        value = False
                    Case GetType(Date), GetType(DateTime)
                        'Return "null"
                        'value = DateTime.MinValue
                        Return jstr(DateTime.MinValue.ToString("yyyy-MM-dd HH:mm"), allowReplace:=allowReplace)
                    Case GetType(DataTable)
                        Return "[]"
                    Case GetType(DataRow)
                        Return "{}"
                        'Do Nothing
                End Select
            End If
        End If

        Dim sval As String
        If allowNull Then
            sval = value.ToString
        Else
            If value Is Nothing OrElse value Is DBNull.Value Then
                sval = ""
            Else
                sval = value.ToString
            End If
        End If

        Select Case t
            Case GetType(String)
                sval = jstr(value, allowReplace)
            Case GetType(Integer)
                sval = value.ToString
            Case GetType(Date), GetType(DateTime)
                If CDate(value) = Date.MinValue Then
                    sval = "null"
                Else
                    sval = jstr(CDate(value).ToString("yyyy-MM-dd HH:mm"), allowReplace:=allowReplace)
                End If
            Case GetType(Decimal)
                sval = value.ToString
            Case GetType(Boolean)
                sval = value.ToString.ToLower
            Case GetType(Array)
                Dim items As New List(Of String)
                For Each i In value
                    items.Add(Serialize(i, allowNull))
                Next
                sval = "[" + String.Join(", ", items.ToArray) + "]"
            Case GetType(DataTable)
                sval = _dataTableToJSON(value, , allowNull)
            Case GetType(DataRow)
                sval = _dataRowToJSON(value, allowNull)
        End Select
        If t.BaseType Is GetType(Array) Then
            Dim items As New List(Of String)
            For Each i In value
                items.Add(Serialize(i, allowNull))
            Next
            sval = "[" + String.Join(", ", items.ToArray) + "]"
        End If
        Return sval
    End Function

    Public Shared Function Serialize(o As Object, Optional allowNull As Boolean = True) As String
        Dim json As String = ""
        Dim props As New List(Of String)

        For Each fi As PropertyInfo In o.GetType.GetProperties()
            Dim value As Object = fi.GetValue(o, Nothing)
            Dim sval As String = typestr(fi.PropertyType, value, allowNull)
            props.Add(prop(fi.Name, sval))
        Next
        Return "{" + String.Join(", ", props.ToArray) + "}"
    End Function

    Public Function Deserialize(Of T)(json As String) As T
        Dim ms As New MemoryStream(System.Text.Encoding.Unicode.GetBytes(json))
        Dim serializer As New DataContractJsonSerializer(GetType(T))
        Return DirectCast(serializer.ReadObject(ms), T)
    End Function

    Public Sub New(context As HttpContext)
        _context = context
    End Sub

    Public Sub WriteString(str As String)
        _context.Response.Clear()
        _context.Response.ContentType = "application/json"
        _context.Response.Write(str)
        _response = str
        _dataSize = str.Length
    End Sub

    Public Sub Write(o As Object, Optional allowNull As Boolean = True)
        WriteString(JSONResponse.Serialize(o, allowNull))
    End Sub

    Public Sub OK()
        Write(New With {.status = "OK"})
    End Sub

    Public Sub OK(message As String)
        Write(New With {.status = "OK", .message = message})
    End Sub

    Public Sub [Error](message As String)
        Write(New With {.status = "Error", .message = message, .failed = True})
    End Sub

    Public Sub [Error](ex As Exception)
        Write(New With {.status = "Error", .message = ex.Message, .stackTrace = ex.StackTrace, .failed = True})
    End Sub

    Public Function ObjectToJSON(t As Type, o As Object, Optional allowNull As Boolean = True)
        Return typestr(t, o, allowNull)
    End Function

    Private Shared Function _dataTableToJSON(dt As DataTable, Optional formatted As Boolean = False, Optional allowNull As Boolean = True, Optional allowReplace As Boolean = True)
        Dim json As String = ""
        Dim items As New List(Of String)
        For Each dr As DataRow In dt.Rows
            items.Add(_dataRowToJSON(dr, allowNull, allowReplace:=allowReplace))
        Next
        Dim separator As String = ", "
        If formatted Then
            separator = ", " + vbNewLine + vbTab
        End If
        json = "[" + IIf(formatted, vbNewLine + vbTab, "") + String.Join(separator, items.ToArray) + IIf(formatted, vbNewLine, "") + "]"
        Return json
    End Function

    Private Shared Function _dataRowToJSON(dr As DataRow, Optional allowNull As Boolean = True, Optional allowReplace As Boolean = True) As String
        If dr Is Nothing Then
            If allowNull Then
                Return "null"
            Else
                Return "{}"
            End If
        End If
        Dim json As String = ""
        Dim props As New List(Of String)
        For Each dc As DataColumn In dr.Table.Columns
            Dim columnName As String = dc.ColumnName
            Dim value As String = typestr(dc.DataType, dr(columnName), allowNull, allowReplace:=allowReplace)
            props.Add(prop(columnName, value))
        Next
        json = "{" + String.Join(", ", props.ToArray) + "}"
        Return json
    End Function

    Private Shared Function _JSONToObject(json As String) As Object
        json = json.Replace("'", """")
        Return Newtonsoft.Json.JsonConvert.DeserializeObject(json)
    End Function

    Public Sub SendDataRow(db As Data.Database, query As String)
        Dim t1 As Long = Now.Ticks
        resetVariables()
        Try
            Dim json As String = ""
            Dim dr As DataRow = db.GetTopRow(query)
            _objectCount = 1
            json = _dataRowToJSON(dr)
            WriteString(json)
        Catch ex As Exception
            _error = ex.Message + vbNewLine + vbNewLine + ex.StackTrace
            _success = False
        End Try
        Dim t2 As Long = Now.Ticks
        _processTime = t2 - t1
    End Sub

    Public Sub SendTable(db As Data.Database, query As String, Optional formatted As Boolean = False, Optional allowReplace As Boolean = True)
        Dim t1 As Long = Now.Ticks
        resetVariables()
        Try
            Dim dt As DataTable = db.GetDataTable(query)
            Dim json As String = _dataTableToJSON(dt, formatted, allowReplace:=allowReplace)
            WriteString(json)
            _objectCount = dt.Rows.Count
        Catch ex As Exception
            _error = ex.Message + vbNewLine + vbNewLine + ex.StackTrace
            _success = False
        End Try
        Dim t2 As Long = Now.Ticks
        _processTime = t2 - t1
    End Sub

    Public Sub SendTable(dt As DataTable, Optional formatted As Boolean = False, Optional allowReplace As Boolean = True)
        Dim t1 As Long = Now.Ticks
        resetVariables()
        Try
            Dim json As String = _dataTableToJSON(dt, False, allowReplace:=allowReplace)
            WriteString(json)
            _objectCount = dt.Rows.Count
        Catch ex As Exception
            _error = ex.Message + vbNewLine + vbNewLine + ex.StackTrace
            _success = False
        End Try
        Dim t2 As Long = Now.Ticks
        _processTime = t2 - t1
    End Sub

    Public Sub SendTableAsStandardResponse(dt As DataTable, Optional allowNull As Boolean = True)
        Dim resp As New JSONStandardResponse
        resp.status = "OK"
        resp.data = dt
        resp.records = dt.Rows.Count
        Write(resp, allowNull)
    End Sub

    Public Sub SendTableAsStandardResponse(db As Data.Database, query As String, Optional allowNull As Boolean = True)
        Dim resp As New JSONStandardResponse
        Try
            Dim dt As DataTable = db.GetDataTable(query)
            resp.status = "OK"
            resp.data = dt
            resp.records = dt.Rows.Count
            resp.dataQuery = query
        Catch ex As Exception
            resp.status = "Error"
            resp.message = ex.Message
            resp.failed = True
        End Try
        Write(resp, allowNull)
    End Sub

    Public Sub SendRowObjectAsStandardResponse(dr As DataRow, Optional allowNull As Boolean = True)
        Dim resp As New JSONStandardResponse
        resp.status = "OK"
        resp.header = dr
        resp.records = 0
        Write(resp, allowNull)
    End Sub

    Public Sub SendRowObjectAsStandardResponse(db As Data.Database, query As String, Optional allowNull As Boolean = True)
        Dim resp As New JSONStandardResponse
        Try
            Dim dr As DataRow = db.GetTopRow(query)
            resp.status = "OK"
            resp.header = dr
            resp.records = 0
        Catch ex As Exception
            resp.status = "Error"
            resp.message = ex.Message
            resp.failed = True
        End Try
        Write(resp, allowNull)
    End Sub

    Public Function GetTableAsJSON(db As Data.Database, query As String, Optional allowNull As Boolean = True) As String
        Dim dt As DataTable = db.GetDataTable(query)
        _objectCount = dt.Rows.Count
        Return _dataTableToJSON(dt, , allowNull)
    End Function

    Public Function GetTableAsJSON(dt As DataTable, Optional allowNull As Boolean = True) As String
        If dt Is Nothing Then
            Return "[]"
        End If
        _objectCount = dt.Rows.Count
        Return _dataTableToJSON(dt, , allowNull)
    End Function

    Public Function GetRowAsJSON(dr As DataRow, Optional allowNull As Boolean = True) As String
        Return _dataRowToJSON(dr, allowNull)
    End Function

    Public Sub StartTick()
        resetVariables()
        _intermediateTick = Now.Ticks
    End Sub

    Public Sub EndTick()
        _processTime = Now.Ticks - _intermediateTick
    End Sub

    Public Sub SetObjectCount(count As Integer)
        _objectCount = count
    End Sub

    Public Shared Function GetObjectFromJSON(json As String) As Object
        Return _JSONToObject(json)
    End Function
    Public Shared Function GetObjectValueFromJSON(json As String, Key As String) As String
        Dim value As String
        If json <> Nothing AndAlso json <> "{}" Then
            Try
                Dim obj As Object = _JSONToObject(json)
                If obj(Key) <> Nothing Then
                    value = obj(Key)
                End If
            Catch ex As Exception
                value = Nothing
            End Try
        End If
        Return value
    End Function
    Public Function GetListAsJson(list As List(Of String), Optional allowNull As Boolean = True) As String
        If list IsNot Nothing Then
            Dim strserialize As String = JsonConvert.SerializeObject(list)
            Return strserialize
        Else
            Return ""
        End If

    End Function
End Class



