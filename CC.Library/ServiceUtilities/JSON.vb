﻿Imports System.Runtime.Serialization.Json
Imports System.Text
Imports System.Reflection
Imports System.Text.RegularExpressions

Namespace DataTools

    Public Class JSON

        Public Shared Function Serialize(Of T)(type As T) As String
            Dim jsonString As String
            If GetType(T) IsNot GetType(Object) Then
                Dim ser As New DataContractJsonSerializer(GetType(T))
                Dim ms As New MemoryStream()
                ser.WriteObject(ms, type)
                jsonString = Encoding.UTF8.GetString(ms.ToArray())
                ms.Close()
                jsonString=Utfstr(jsonString)
                Return jsonString
            Else
            	jsonString = _serialize(type)
            	jsonString=Utfstr(jsonString)
            End If
            Return jsonString
        End Function

        Public Shared Function Deserialize(Of T)(jsonString As String) As T
            Dim ser As New DataContractJsonSerializer(GetType(T))
            Dim ms As New MemoryStream(Encoding.UTF8.GetBytes(jsonString))
            Dim obj As T = DirectCast(ser.ReadObject(ms), T)
            Return obj
        End Function

        Private Shared Function _serialize(o As Object) As String
            If o Is Nothing Then
                Return ""
            End If

            Dim props As New List(Of String)()
            For Each fi As PropertyInfo In o.[GetType]().GetProperties()
                Dim value As Object = fi.GetValue(o, Nothing)
                Dim sval As String = typestr(fi.PropertyType, value)

                props.Add(fi.Name + "=" & sval)
            Next

            Return String.Join("&", props.ToArray())
        End Function

        Private Shared Function typestr(t As Type, value As [Object]) As String
            If (value Is Nothing) OrElse (value Is DBNull.Value) Then
                Return "null"
            End If

            Dim sval As String = value.ToString()
            Select Case t.[GetType]().ToString()
                Case "String"
                    sval = ""
                    For Each c In value.ToString().ToCharArray
                        If Not Char.IsControl(c) Or c = vbCr Or c = vbLf Or c = vbTab Then
                            sval += c
                        End If
                    Next
                Case "Integer"
                    sval = value.ToString()
                Case "Date", "DateTime"
                    sval = Convert.ToDateTime((value)).ToString("yyyy-MM-dd HH:mm")
                Case "Decimal"
                    sval = value.ToString()
                Case "Boolean"
                    sval = value.ToString().ToLower()
            End Select
            Return System.Web.HttpUtility.HtmlEncode(sval)
        End Function

          Public Shared Function Utfstr(s As String) As String
	        s = Regex.Replace(s, "[^\u0020-\u007E\u0009\u000A\u000D]", String.Empty) 'Remove all characters excluding Tab, NL, CR, visible printable ASCII characters.
	        s = s.Replace(Chr(27), String.Empty)
	        Return s
          End Function
          
           Public Shared Function Windowsstr(s As String) As String
		      'Replace  all \n with \r\n.
		       s = s.Replace("\u000d\u000a", Chr(7))
		       s = s.Replace("\u000a", "\u000d\u000a")
		       s = s.Replace(Chr(7), "\u000d\u000a")
		        Return s
           End Function
           
            Public Shared Function Windowsstrnew(s As String) As String
		      'Replace  all \r with \r\n.
		       s = s.Replace("\u000d\u000a", Chr(7))
		       s = s.Replace("\u000d", "\u000d\u000a")
		       s = s.Replace(Chr(7), "\u000d\u000a")
		        Return s
           End Function
           
           Public Shared Function UnixStr(s As String) As String
	        'Replace  all \r\n with \n.
	        s = s.Replace("\u000d\u000a", "\u000a")
	        Return s
           End Function
           
            Public Shared Function UnixStrnew(s As String) As String
	        'Replace  all \r\n with \r.
	        s = s.Replace("\u000d\u000a", "\u000d")
	        s = s.Replace("\u000a", "\u000d")
	        Return s
           End Function
           
           Public Shared Function NoLinebreakstr(s As String) As String
		      'Replace  all \n and  \r\n with space
		       s = s.Replace("\u000d\u000a", " ")
		       s = s.Replace("\u000a", " ")
	        Return s
           End Function
           
           Public Shared Function NoLinebreakstrnew(s As String) As String
		      'Replace  all \r and  \r\n with space
		       s = s.Replace("\u000d\u000a", " ")
		       s = s.Replace("\u000d", " ")
		       s = s.Replace("\u000a", " ")
	        Return s
           End Function
        

    End Class
    
End Namespace

