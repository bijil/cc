﻿Imports System.IO.Compression
Imports System.Text.Encoding
Imports System.IO
Imports ICSharpCode.SharpZipLib.Zip
Imports ICSharpCode.SharpZipLib.Core

Namespace DataTools

    Public Class DataConversion

        Public Shared Function EncodeToBase64(input As String) As String
            Dim data As Byte() = Unicode.GetBytes(input)
            Return System.Convert.ToBase64String(data)
        End Function

        Public Shared Function DecodeFromBase64(input As String) As String
            Dim data As Byte() = System.Convert.FromBase64String(input)
            Return System.Text.Encoding.Unicode.GetString(data)
        End Function

        Public Shared Function Compress(data As String) As String
            Dim bytes = Unicode.GetBytes(data)
            Dim msI = New MemoryStream(bytes)
            Dim msO = New MemoryStream
            Dim gz = New GZipStream(msO, CompressionMode.Compress)
            msI.CopyTo(gz)

            Return System.Convert.ToBase64String(msO.ToArray)
        End Function

        Public Shared Function Decompress(data As String) As String
            Dim bytes = System.Convert.FromBase64String(data)

            Dim content As String = Unicode.GetString(bytes)

            Dim msI = New MemoryStream(bytes)
            Dim msO = New MemoryStream

            Dim buffer(4095) As Byte
            Dim zs As New ZipInputStream(msI)
            Dim ze As ZipEntry = zs.GetNextEntry()
            StreamUtils.Copy(zs, msO, buffer)

            'Dim gz = New GZipStream(msI, CompressionMode.Decompress)
            'gz.CopyTo(msO)
            Return Unicode.GetString(msO.ToArray)
        End Function

        Public Shared Function GZipCompress(data As String) As String
            Dim bytes = Unicode.GetBytes(data)
            Dim msI = New MemoryStream(bytes)
            Dim msO = New MemoryStream
            Dim gz = New GZipStream(msO, CompressionMode.Compress)
            msI.CopyTo(gz)
            Return System.Convert.ToBase64String(msO.ToArray)
        End Function

        Public Shared Function GZipDecompress(data As String) As String
            Dim bytes = System.Convert.FromBase64String(data)
            Dim msI = New MemoryStream(bytes)
            Dim msO = New MemoryStream
            Dim gz = New GZipStream(msI, CompressionMode.Decompress)
            gz.CopyTo(msO)
            Return Unicode.GetString(msO.ToArray)
        End Function

        Public Shared Function ExtractDataFromPack(data As String, format As String, encoding As String, isCompressed As Boolean, compressionFormat As String) As String
            encoding = encoding.ToLower
            Dim enc As System.Text.Encoding
            Select Case encoding
                Case "ascii", "asc"
                    enc = System.Text.Encoding.ASCII
                Case "utf8", "utf"
                    enc = System.Text.Encoding.UTF8
                Case "unicode", "utf16"
                    enc = System.Text.Encoding.Unicode
                Case Else
                    enc = System.Text.Encoding.Unicode
            End Select
            Dim bytes As Byte()
            Select Case format.ToLower
                Case "base64"
                    bytes = System.Convert.FromBase64String(data)
                Case "raw"
                    bytes = enc.GetBytes(data)
                Case Else
                    bytes = enc.GetBytes(data)
            End Select

            Dim msI = New MemoryStream(bytes)
            Dim msO = New MemoryStream

            If isCompressed Then
                Select Case compressionFormat.ToLower
                    Case "gzip"
                        Dim gz = New GZipStream(msI, CompressionMode.Decompress)
                        gz.CopyTo(msO)
                    Case Else
                        Dim buffer(4095) As Byte
                        Dim zs As New ZipInputStream(msI)
                        Dim ze As ZipEntry = zs.GetNextEntry()
                        StreamUtils.Copy(zs, msO, buffer)
                End Select
            Else
                msI.CopyTo(msO)
            End If

            Return enc.GetString(msO.ToArray)
        End Function

    End Class
End Namespace
