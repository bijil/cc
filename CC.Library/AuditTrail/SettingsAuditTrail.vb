﻿Imports CAMACloud.Data
Public Class SettingsAuditTrail

    Public Shared Sub updateSettingsAuditTrail(dtField As DataTable, fieldId As Integer, loginId As String)
    	Dim i As Integer = 0
    	Dim type As String = ""
        For Each column As DataColumn In dtField.Columns
            If dtField.Rows(0)(i).ToString() <> dtField.Rows(1)(i).ToString() Then
                _updateSettingsAuditTrail(column.ColumnName.ToString(), dtField.Rows(0)(i).ToString(), dtField.Rows(1)(i).ToString(), fieldId, loginId, type)
                
            End If
            i += 1
        Next
    End Sub
	Public Shared Sub _updateSettingsAuditTrail(SettingName As String, oldValue As String, newValue As String, fieldId As Integer, loginId As String, type As String)
        newValue = IIf(newValue <> Nothing, newValue.Replace("'", """"), newValue)
        oldValue = IIf(oldValue <> Nothing, oldValue.Replace("'", """"), oldValue)
        Dim name As String = Database.Tenant.GetStringValue("SELECT Name FROM DataSourceField WHERE Id = '" & fieldId & "'")
        Dim catname As String = Database.Tenant.GetStringValue("SELECT SourceTable FROM DataSourceField WHERE Id = '" & fieldId & "'")
        If type = "checkbox" Then
        	If newValue = "True" Then
        		Database.Tenant.Execute("INSERT INTO SettingsAuditTrail(EventDate,EventTime,LoginId,Description) VALUES(CONVERT(DATE, GETDATE()),GETUTCDATE(),'"+loginId.ToString()+"','"+SettingName+" enabled for field "+catname+"."+name+" ')")
        		Return
        	Else        		
        		Database.Tenant.Execute("INSERT INTO SettingsAuditTrail(EventDate,EventTime,LoginId,Description) VALUES(CONVERT(DATE, GETDATE()),GETUTCDATE(),'"+loginId.ToString()+"','"+SettingName+" disabled for field "+catname+"."+name+" ')")
        		Return
        	End If
        End If
        Database.Tenant.Execute("INSERT INTO SettingsAuditTrail(EventDate,EventTime,LoginId,Description) VALUES(CONVERT(DATE, GETDATE()),GETUTCDATE(),'" + loginId.ToString() + "','" + SettingName + " changed from " + If(String.IsNullOrEmpty(oldValue), "<blank>", oldValue.ToString()) + " to " + newValue.ToString() + " for field " + catname + "." + name + " ')")
    End Sub
    
	Public Shared Sub insertSettingsAuditTrail(loginId As String, Description As String)
        Dim o As DataRow = Database.System.GetTopRow("SELECT EnableXMLAuditTrail FROM OrganizationSettings WHERE OrganizationId = " & HttpContext.Current.GetCAMASession().OrganizationId)
        If (o.GetBoolean("EnableXMLAuditTrail")) Then
        	If Not (Description = Nothing)
        		Description = Description.Replace("'","''")
        	End If
            Database.Tenant.Execute("INSERT INTO SettingsAuditTrail(EventDate,EventTime,LoginId,Description) VALUES(GETUTCDATE(),GETUTCDATE(),'"+loginId.ToString()+"','"+Description+"')")
        End If
    End Sub
End Class
