﻿Imports CAMACloud.Data

Public Class APIRequestTrace

    Public Shared Function StartTrace(db As Database, path As String, ByRef output_pathId As Integer) As Integer
        Dim pr As DataRow = db.GetTopRow("EXEC API_StartTrace @Path =" + path.ToSqlValue)
        output_pathId = pr.GetInteger("PathId")
        Return pr.GetInteger("TraceId")
    End Function

    Public Shared Sub SetError(db As Database, pathId As Integer, traceId As Integer, ex As Exception, Optional data As Object = Nothing)
        db.Execute("UPDATE API_Path SET LastSuccess = 0, LastErrorMessage = {1}, LastErrorStackTrace = {2} WHERE Id = {0}".SqlFormat(True, pathId, ex.Message, ex.StackTrace))
        Dim shortMessage As String = ex.Message
        If shortMessage.Length > 2000 Then
            shortMessage = shortMessage.Substring(0, 2000)
        End If
        db.Execute("UPDATE API_TraceLog SET Success = 0, ErrorMessage = {1}, ErrorTime = GETUTCDATE() WHERE EID = {0}".SqlFormat(True, traceId, shortMessage))
    End Sub

    Public Shared Sub SetSuccess(db As Database, pathId As Integer, traceId As Integer)
        db.Execute("UPDATE API_Path SET LastSuccess = 1, LastErrorMessage = NULL, LastErrorStackTrace = NULL, LastErrorData = NULL WHERE Id = {0}".SqlFormat(True, pathId))
        db.Execute("UPDATE API_TraceLog SET Success = 1 WHERE EID = {0}".SqlFormat(True, traceId))
    End Sub

End Class
