﻿Imports CAMACloud.Data

Public Class ParcelEventLog

    'IMPLEMENTATION LOG
    'GISTransferHelper.SaveParcelGISRecords
    'PhotoTransferHelper.PhotoDownSync_Commit
    'PhotoTransferHelper.PhotoDownSync_Data - Photo d/l failed
    'PhotoTransferHelper.UploadPhoto
    'UpdateParcelHelp.UpdateParcelChanges - fieldedit, delete



    Public Shared ReadOnly Property Enabled As Boolean
        Get
            Return False
        End Get
    End Property

    Public Enum EventType As Integer
        Created = 0                 'data transfer reset
        Refreshed = 1               'data transfer refresh
        Deleted = 2                 'delete api
        PriorityChange = 3
        DownloadedToMobile = 4
        QCModified = 5
        Downsynced = 6              'SP
        MapDownloaded = 7
        Reviewed = 10               'MA
        UnReviewed = 11             'MA
        Approved = 12               'SP
        Rejected = 13               'SP
        QCReset = 14                'SP
        Committed = 15              'commit
        SyncFailed = 16             'reject api
        RowAdded = 20               '/transfer/data
        RowUpdated = 21             '/transfer/data
        RowDeleted = 22             '/transfer/update/upsync
        FieldUpdated = 23           '/transfer/update/upsync
        RowRecovered = 24
        PhotoAdded = 30             '/content/photos/upload, MA, QC
        PhotoDeleted = 31           'MA, QC
        PhotoModified = 32
        PhotoFlagMain = 33
        PhotoDownloaded = 34
        PhotoUploadFailed = 38
        PhotoDownloadFailed = 39
        GISLoaded = 40
        TrackedValueCommit = 41
        KeysUpdated = 42
    End Enum

    Public Enum ApplicationType As Integer
        API = 0
        Console = 1
        MA = 2
    End Enum

    Public Shared Sub CreateEvent(db As Database, e As EventType, app As ApplicationType, parcelId As Integer, Optional p1 As Integer = -999, Optional p2 As Integer = -999, Optional p3 As Integer = -999, Optional loginId As String = Nothing)
        If Enabled Then
            Dim sql As String = "INSERT INTO ParcelEventLog(ParcelId, ApplicationId, EventId, P1, P2, P3, LoginId) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6});"
            Dim sqli As String = sql.SqlFormat(True, parcelId, app.GetHashCode, e.GetHashCode, p1, p2, p3, loginId)
            db.Execute(sqli)
        End If
    End Sub

    Public Shared Sub CreateFlagEvent(db As Database, e As EventType, app As ApplicationType, parcelId As Integer, Optional loginId As String = Nothing)
        CreateEvent(db, e, app, parcelId, , , , loginId)
    End Sub

    Public Shared Sub CreateTableEvent(db As Database, e As EventType, app As ApplicationType, parcelId As Integer, tableId As Integer, rowUID As Integer, Optional loginId As String = Nothing)
        CreateEvent(db, e, app, parcelId, tableId, rowUID, , loginId)
    End Sub

    Public Shared Sub CreateFieldEvent(db As Database, e As EventType, app As ApplicationType, parcelId As Integer, tableId As Integer, rowUID As Integer, fieldId As Integer, Optional loginId As String = Nothing)
        CreateEvent(db, e, app, parcelId, tableId, rowUID, fieldId, loginId)
    End Sub

    Public Shared Sub CreatePhotoEvent(db As Database, e As EventType, app As ApplicationType, parcelId As Integer, imageId As Integer, Optional refImageId As Integer = -1, Optional loginId As String = Nothing)
        CreateEvent(db, e, app, parcelId, imageId, refImageId, , loginId)
    End Sub

    Public Shared Sub CreatePriorityEvent(db As Database, app As ApplicationType, parcelId As Integer, priority As Integer, Optional loginId As String = Nothing)
        CreateEvent(db, EventType.PriorityChange, app, parcelId, priority, , , loginId)
    End Sub

    Public Shared Sub CreatePhotoFlagEvent(db As Database, app As ApplicationType, parcelId As Integer, imageId As Integer, Optional loginId As String = Nothing)
        CreateEvent(db, EventType.PhotoFlagMain, app, parcelId, imageId, , , loginId)
    End Sub

    Public Shared Sub CreateGISLoadEvent(db As Database, app As ApplicationType, parcelId As Integer, imageId As Integer, Optional loginId As String = Nothing)
        CreateEvent(db, EventType.GISLoaded, app, parcelId, , , , loginId)
    End Sub

End Class
