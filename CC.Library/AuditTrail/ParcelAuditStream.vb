﻿Imports CAMACloud.Data

Public Class ParcelAuditStream

    Private Shared sqlFormat As String = "INSERT INTO ParcelAuditTrail (EventDate, EventTime, ParcelId, LoginID, EventType, Description, CorrespondingChangeId,ApplicationType) VALUES ({0}, {0}, {1}, {2}, {3}, {4}, {5},{6})"

    Public Enum ParcelEventType
        FlagChange = 0
        DataChange = 1
        QCChange = 2
        Insertion = 3
        Deletion = 4
    End Enum

    Public Shared Sub CreateEvent(db As Database, eventTime As Date, parcelId As Integer, loginId As String, eventType As ParcelEventType, description As String, ApplicationType As String, Optional correspondingChangeId As Integer = 0)
        Dim sql As String = sqlFormat.SqlFormat(True, eventTime, parcelId, loginId, eventType.GetHashCode, description, IIf(correspondingChangeId = 0, "", correspondingChangeId.ToString), ApplicationType)
        db.Execute(sql)
    End Sub

    Public Shared Sub CreateFlagEvent(db As Database, eventTime As Date, parcelId As Integer, loginId As String, description As String, ApplicationType As String)
        CreateEvent(db, eventTime, parcelId, loginId, ParcelEventType.FlagChange, description, ApplicationType)
    End Sub

    Public Shared Sub CreateDataChangeEvent(db As Database, eventTime As Date, parcelId As Integer, auxRowUID As Integer, loginId As String, fieldId As Integer, newValue As String, changeId As Integer, AppType As String)


        db.Execute("EXEC CC_CreateParcelAuditTrail {0},{1},{2},{3},{4},{5},{6},@AppType = {7}".SqlFormat(True, parcelId, loginId, changeId, eventTime, auxRowUID, fieldId, newValue, AppType))
        'If newValue Is Nothing Then newValue = ""
        ''If newValue.Length > 50 Then newValue = newValue.Substring(0, 48) + " ..."
        'Dim customFormat As Boolean = False
        'Dim fr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM DataSourceField WHERE Id = " + fieldId.ToString)
        'Dim doNotIncludeAuditTrail As Boolean = fr.GetBoolean("DoNotIncludeInAuditTrail")
        'Dim format As String = fr.GetString("UIProperties")
        'Dim doNotShowOnDE As Boolean = fr.GetBoolean("DoNotShowOnDE")
        'If doNotIncludeAuditTrail = True Or doNotShowOnDE = True Then
        '    Exit Sub
        'End If
        'If format.Contains("CustomFormat") Then
        '    customFormat = True
        'End If
        'Dim description As String = ""
        'If (fr.GetInteger("DataType") = 4 AndAlso customFormat = False) Then
        '    Try
        '        newValue = Database.Tenant.GetStringValue("SELECT dbo.GetLocalDate('" + newValue + "')")
        '        newValue = newValue + "(" + ClientSettings.TimeZone + ")"
        '    Catch ex As Exception

        '    End Try
        '    description = "Data modified - " + fr.GetString("SourceTable") + "." + fr.GetString("Name") + " changed to " + newValue
        'Else
        '    description = "Data modified - " + fr.GetString("SourceTable") + "." + fr.GetString("Name") + " changed to " + newValue
        'End If

        'If auxRowUID > 0 Then
        '    description += " for Aux Record #" + auxRowUID.ToString + "."
        'End If

        'CreateEvent(db, eventTime, parcelId, loginId, ParcelEventType.DataChange, description, changeId)
    End Sub

    Public Shared Sub UpdateLastSyncDate(db As Database, SyncField As String)

        Dim isRowExist As Boolean = db.GetIntegerValue("SELECT COUNT(*) FROM ApplicationStatus")
        If Not isRowExist Then
            db.Execute("INSERT INTO ApplicationStatus (" + SyncField + ") VALUES (GETUTCDATE())")
        Else
            db.Execute("UPDATE ApplicationStatus SET " + SyncField + " = GETUTCDATE() WHERE ROWID=1")
        End If
    End Sub
    Public Shared Sub CreatePhotoFlagEvent(db As Database, eventTime As Date, parcelId As Integer, loginId As String, description As String, ApplicationType As String)
        CreateEvent(db, eventTime, parcelId, loginId, ParcelEventType.FlagChange, description, ApplicationType)
    End Sub
End Class
