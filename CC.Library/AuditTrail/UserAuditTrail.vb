﻿Imports CAMACloud.Data

Public Class UserAuditTrail

    Private Shared _sqlFormat As String = "INSERT INTO UserAuditTrail (LoginId, EventDate, EventTime, LocalTime, EventType, Application, Description, ItemCount, IsError, Data, IPAddress) VALUES ({0}, GETUTCDATE(), GETUTCDATE(), dbo.GetLocalDate(GETUTCDATE()), {1}, {2}, {3}, {4}, {5}, {6}, {7});"

    Public Enum ApplicationType
        MobileAssessor
        Console
        Auth
        Other
    End Enum

    Public Enum EventType
        Login
        Logout
        MobileTask
        DownloadData
        UploadChanges
        UploadPhoto
        OpenQC
        SearchParcels
        BulkJob
        Other
    End Enum

    Public Shared Sub CreateEvent(loginId As String, app As ApplicationType, eType As EventType, description As String, Optional itemCount As Integer = 0, Optional isError As Boolean = False, Optional data As String = "")
        Dim application As String
        Select Case app
            Case ApplicationType.Console
                application = "CO"
            Case ApplicationType.MobileAssessor
                application = "MA"
            Case ApplicationType.Auth
                application = "AU"
            Case Else
                application = "XX"
        End Select

        Dim eventType As String
        Select Case eType
            Case UserAuditTrail.EventType.BulkJob
                eventType = "BULK"
            Case UserAuditTrail.EventType.DownloadData
                eventType = "UPDATE"
            Case UserAuditTrail.EventType.Login
                eventType = "LOGIN"
            Case UserAuditTrail.EventType.Logout
                eventType = "LOGOUT"
            Case UserAuditTrail.EventType.MobileTask
                eventType = "OPER"
            Case UserAuditTrail.EventType.OpenQC
                eventType = "OPER"
            Case UserAuditTrail.EventType.SearchParcels
                eventType = "SEARCH"
            Case UserAuditTrail.EventType.UploadChanges
                eventType = "MADATA"
            Case UserAuditTrail.EventType.UploadChanges
                eventType = "MAPHOTO"
            Case Else
                eventType = "OTHER"
        End Select

        Dim ipAddress As String = ""
        If HttpContext.Current IsNot Nothing Then
            ipAddress = HttpContext.Current.Request.UserHostAddress
        End If
        Dim sql As String = ""
        sql = _sqlFormat.SqlFormat(True, loginId, eventType, application, description, itemCount, isError.GetHashCode, data, ipAddress)
        Database.Tenant.Execute(sql)
    End Sub
End Class
