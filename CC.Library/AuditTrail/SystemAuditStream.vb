﻿Imports CAMACloud.Data

Public Class SystemAuditStream

    Private Shared ReadOnly tableScript As XElement = <sql>
CREATE TABLE [dbo].[SystemAuditTrail](
	[Id] [int] IDENTITY(1,1) PRIMARY KEY,
	[EventDate] [date] NOT NULL  DEFAULT (getutcdate()),
	[EventTime] [datetime] NOT NULL  DEFAULT (getutcdate()),
	[EventType] [varchar](50) NULL,
	[EventCode] [int] NULL,
	[Source] [varchar](100) NULL,
	[Description] [varchar](max) NULL,
	[IsError] [bit] NOT NULL  DEFAULT (0),
	[LoginId] [varchar](50) NULL,
    [IPAddress] [varchar](50) NULL,
	[ApplicationId] [int] NULL,
	[Data] [varchar](max) NULL
) 
</sql>

    Public Shared Sub CreateErrorEvent(db As Database, ex As Exception)
        Dim message As String = "An error occurred while processing your request."
        If TypeOf ex Is Exceptions.ServiceExceptionBase Then
            message = ex.Message
        End If
        _createEvent(db, "ERROR", "9999", message, True, ex.StackTrace)
    End Sub

    Public Shared Sub CreateStandardEvent(db As Database, status As AuditEventType, ParamArray params As String())
        _createEvent(db, status.EventType, status.Code, status.Message.FormatString(params), False, Nothing)
    End Sub

    Private Shared Sub _createEvent(db As Database, eventType As String, eventCode As String, description As String, isError As Boolean, data As String)
        Dim context As HttpContext = HttpContext.Current
        Dim ipAddress As String = "::?"
        If context IsNot Nothing Then
            ipAddress = context.Request.ClientIPAddress
        End If
        Dim source As String = ClientSettings.CAMACloudApplicationName
        If source = "" AndAlso context IsNot Nothing Then
            source = context.Request.Url.Host
        End If
        Dim loginId As String = ""
        If System.Web.Security.FormsAuthentication.IsEnabled Then
            If System.Web.Security.Membership.GetUser() IsNot Nothing Then
                loginId = System.Web.Security.Membership.GetUser().UserName
            End If
        End If
        Dim applicationId As Integer = 0

        _createEvent(db, eventType, eventCode, source, description, False, loginId, ipAddress, applicationId, data)
    End Sub

    Private Shared Sub _createEvent(db As Database, eventType As String, eventCode As String, source As String, description As String, isError As Boolean, loginId As String, ipAddress As String, applicationId As Integer, data As String)
        _checkForAuditTrailTable(db)
        Dim sql As String = "INSERT INTO SystemAuditTrail (EventType, EventCode, Source, Description, IsError, LoginId, IPAddress, ApplicationId, Data) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8});"
        Dim auditSql As String = sql.SqlFormat(True, eventType, eventCode, source, description, isError.GetHashCode, loginId, ipAddress, applicationId, data)
        db.Execute(auditSql)
    End Sub

    Private Shared Sub _checkForAuditTrailTable(db As Database)
        If Not db.DoesTableExists("SystemAuditTrail") Then
            db.Execute(tableScript.Value)
        End If
    End Sub

End Class
