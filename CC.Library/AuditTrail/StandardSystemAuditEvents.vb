﻿Public Module StandardSystemAuditEvents

    'ERROR Event Types
    Public ReadOnly ERR_9999 As New AuditEventType("ERROR", 9999, "An error occurred while processing your request.")

    'SYSTEM Event Types
    Public ReadOnly SYS_10000_INIT As New AuditEventType("SYSTEM", 10000, "CAMA Cloud Account Initialized.")
    Public ReadOnly SYS_10001_UNLK As New AuditEventType("SYSTEM", 10001, "Account access unlocked.")
    Public ReadOnly SYS_10002_LOCK As New AuditEventType("SYSTEM", 10002, "Account access locked.")
    Public ReadOnly SYS_10003_RESET As New AuditEventType("SYSTEM", 10003, "Account data reset.")
    Public ReadOnly SYS_10004_CHK As New AuditEventType("SYSTEM", 10004, "Account status verified successfully.")

    'SECURITY Event Types
    Public ReadOnly SEC_20000_INIT As New AuditEventType("SECURITY", 20000, "User access initiated; admin account has been created.")
    Public ReadOnly SEC_20001_ADDUSER As New AuditEventType("SECURITY", 20001, "New user account created - '{0}'")
    Public ReadOnly SEC_20002_EDITUSER As New AuditEventType("SECURITY", 20002, "User account '{0}' modified; Action = {1}")
    Public ReadOnly SEC_20003_UNLK As New AuditEventType("SECURITY", 20003, "User account '{0}' unlocked.")
    Public ReadOnly SEC_20004_LOCK As New AuditEventType("SECURITY", 20004, "User account '{0}' locked.")
    Public ReadOnly SEC_20005_LOGIN As New AuditEventType("SECURITY", 20005, "User '{0}' logged in successfully.")
    Public ReadOnly SEC_20006_LOGOUT As New AuditEventType("SECURITY", 20006, "User '{0}' logged out.")
    Public ReadOnly SEC_20007_PROFILE As New AuditEventType("SECURITY", 20007, "User '{0}' modified profile from console.")
    Public ReadOnly SEC_20008_FAILED As New AuditEventType("SECURITY", 20008, "Failed login attempt for user '{0}'.")

    'SCHEMA Event Types
    Public ReadOnly SCH_30000_RESET As New AuditEventType("SCHEMA", 30000, "Schema reset successfully.")
    Public ReadOnly SCH_30001_INIT As New AuditEventType("SCHEMA", 30001, "Schema loaded successfully after reset.")
    Public ReadOnly SCH_30002_RELATION As New AuditEventType("SCHEMA", 30002, "Schema relations setup successfully.")
    Public ReadOnly SCH_30003_COPY As New AuditEventType("SCHEMA", 30003, "Schema download request processed.")
    Public ReadOnly SCH_30004_UPDATE As New AuditEventType("SCHEMA", 30001, "Schema updated successfully.")

    'DATATRANSFER Event Types
    Public ReadOnly DTX_40000_RESET As New AuditEventType("DATATRANSFER", 40000, "All data wiped out successfully.")
    Public ReadOnly DTX_40009_CANCEL As New AuditEventType("DATATRANSFER", 40009, "All pending jobs cancelled")
    Public ReadOnly DTX_41000_INIT As New AuditEventType("DATATRANSFER", 41000, "Reset Transfer Initialized; Job ID = {0}")
    Public ReadOnly DTX_41001_DATA As New AuditEventType("DATATRANSFER", 41001, "Reset Transfer Data; Job ID = {0}; Page {1}")
    Public ReadOnly DTX_41002_DTC As New AuditEventType("DATATRANSFER", 41002, "Reset Transfer page completed; Job ID = {0}; Page {1}; Affected Tables = {2}; Affected Parcels = {3}")
    Public ReadOnly DTX_41009_CANCEL As New AuditEventType("DATATRANSFER", 41010, "Reset Transfer cancelled; Job ID = {0}")
    Public ReadOnly DTX_41010_COMMIT As New AuditEventType("DATATRANSFER", 41010, "Reset Transfer Committed successfully; Job ID = {0}")
    Public ReadOnly DTX_42000_INIT As New AuditEventType("DATATRANSFER", 42000, "Refresh Transfer Initialized; Job ID = {0}")
    Public ReadOnly DTX_42001_DATA As New AuditEventType("DATATRANSFER", 42001, "Refresh Transfer Data; Job ID = {0}; Page {1}")
    Public ReadOnly DTX_42002_DTC As New AuditEventType("DATATRANSFER", 42002, "Refresh Transfer page completed; Job ID = {0}; Page {1}; Affected Tables = {2}; Affected Parcels = {3}")
    Public ReadOnly DTX_42009_CANCEL As New AuditEventType("DATATRANSFER", 42009, "Refresh Transfer cancelled; Job ID = {0}")
    Public ReadOnly DTX_42010_COMMIT As New AuditEventType("DATATRANSFER", 42010, "Refresh Transfer Committed successfully; Job ID = {0}")
    Public ReadOnly DTX_43000_INIT As New AuditEventType("DATATRANSFER", 43000, "Change Transfer Initialized; Job ID = {0}")
    Public ReadOnly DTX_43001_DATA As New AuditEventType("DATATRANSFER", 43001, "Change Transfer Data; Job ID = {0}; Page {1}")
    Public ReadOnly DTX_43002_DTC As New AuditEventType("DATATRANSFER", 43002, "Change Transfer page completed; Job ID = {0}; Page {1}; Affected Tables = {2}; Affected Parcels = {3}")
    Public ReadOnly DTX_43009_CANCEL As New AuditEventType("DATATRANSFER", 43009, "Change Transfer cancelled; Job ID = {0}")
    Public ReadOnly DTX_43010_COMMIT As New AuditEventType("DATATRANSFER", 43010, "Change Transfer Committed successfully; Job ID = {0}")
    Public ReadOnly DTX_44000_DATA As New AuditEventType("DATATRANSFER", 44000, "Update Transfer upsync started")
    Public ReadOnly DTX_44001_DATA As New AuditEventType("DATATRANSFER", 44001, "Update Transfer upsync completed")
    Public ReadOnly DTX_44002_DATA As New AuditEventType("DATATRANSFER", 44002, "Update Transfer downsync started")
    Public ReadOnly DTX_44003_DATA As New AuditEventType("DATATRANSFER", 44003, "Update Transfer dowsync completed")
    Public ReadOnly DTX_45000_START As New AuditEventType("DATATRANSFER", 45000, "Lookup Transfer started.")
    Public ReadOnly DTX_45001_DTC As New AuditEventType("DATATRANSFER", 45001, "Lookup Transfer completed;Affected Lookups={0}; Affected Rows={1}")
    Public ReadOnly DTX_46000_REVERT As New AuditEventType("DATATRANSFER", 46000, "Parcel Changes revert started")
    Public ReadOnly DTX_46001_REVERT As New AuditEventType("DATATRANSFER", 46001, "Parcel Changes revert completed")
	Public ReadOnly DTX_47000_INIT As New AuditEventType("DATATRANSFER", 47000, "Clean Transfer Initialized; Job ID = {0}")
	Public ReadOnly DTX_47001_DATA As New AuditEventType("DATATRANSFER", 47001, "Clean Transfer Data; Job ID = {0}; Page {1}")
	Public ReadOnly DTX_47002_DTC As New AuditEventType("DATATRANSFER", 47002, "Clean Transfer page completed; Job ID = {0}; Page {1}; Affected Tables = {2}; Affected Records = {3}")
	Public ReadOnly DTX_47009_CANCEL As New AuditEventType("DATATRANSFER", 47009, "Clean Transfer cancelled; Job ID = {0}")
    Public ReadOnly DTX_47010_COMMIT As New AuditEventType("DATATRANSFER", 47010, "Clean Transfer Committed successfully; Job ID = {0}")
    Public ReadOnly DTX_47008_TABLE As New AuditEventType("DATATRANSFER", 47010, "Table {1} cleaned successfully; Job ID = {0}; Time elapsed = {2}s; Records cleaned = {3}")

    Public ReadOnly DTX_46010_DELETE As New AuditEventType("DATATRANSFER", 46010, "Parcel deleted from the database.")
    ' Public ReadOnly DTX_46011_DELETE As New AuditEventType("", 46011, "Parcel/Parcels are already cleared or not existed in the database")
    ' Public ReadOnly DTX_46012_DELETE As New AuditEventType ("",46012,"Parcel/Parcels are Already Cleared"

    Public ReadOnly CNT_80000_START As New AuditEventType("CONTENTTRANSFER", 80000, "Photo upsync process started.")
    Public ReadOnly CNT_80001_END As New AuditEventType("CONTENTTRANSFER", 80001, "Photo upsync process completed.")
    Public ReadOnly CNT_80002_START As New AuditEventType("CONTENTTRANSFER", 80002, "Photo downsync process started.")
    Public ReadOnly CNT_80003_END As New AuditEventType("CONTENTTRANSFER", 80003, "Photo downsync process completed.")

    Public ReadOnly CNT_80004_INIT As New AuditEventType("CONTENTTRANSFER", 80004, "Photo downsync Transfer Initialized; Job ID = {0}")
    Public ReadOnly CNT_80005_DATA As New AuditEventType("CONTENTTRANSFER", 80005, "Photo DownSync Transfer page completed;  Job ID = {0}; Page {1};")
    Public ReadOnly CNT_80006_COMMIT As New AuditEventType("CONTENTTRANSFER", 80006, "Photo DownSync Committed successfully; Job ID = {0}; Photos Transferred = {1}")
    Public ReadOnly CNT_80007_CANCEL As New AuditEventType("CONTENTTRANSFER", 80007, "Photo downsync Transfer Cancelled;  Job ID = {0};")

    Public ReadOnly FNL_90001_START As New AuditEventType("FINALIZE", 90001, "Finalize call started.")
    Public ReadOnly FNL_90002_END As New AuditEventType("FINALIZE", 90002, "Finalize procedure ended.")


End Module