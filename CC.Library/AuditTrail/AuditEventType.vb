﻿Public Class AuditEventType

    Private _code As Integer
    Private _message As String
    Private _type As String

    Public ReadOnly Property Code As String
        Get
            Return _code
        End Get
    End Property

    Public ReadOnly Property Message As String
        Get
            Return _message
        End Get
    End Property

    Public ReadOnly Property EventType As String
        Get
            Return _type
        End Get
    End Property

    Public Sub New(eventType As String, code As Integer, message As String)
        _type = eventType
        _code = code
        _message = message
    End Sub

End Class
