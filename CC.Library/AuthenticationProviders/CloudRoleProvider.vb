﻿Imports System.Web.Security

Public Class CloudRoleProvider
	Inherits SqlRoleProvider

	Public Overrides Property ApplicationName As String
		Get
			If HttpContext.Current.GetCAMASession Is Nothing Then
				Return "camacloud"
			Else
                Return HttpContext.Current.GetCAMASession.TenantKey
			End If
			Return MyBase.ApplicationName
		End Get
		Set(value As String)
			MyBase.ApplicationName = value
		End Set
	End Property

End Class
