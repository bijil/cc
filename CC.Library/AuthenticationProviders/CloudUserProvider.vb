﻿Imports System.Web.Security
Imports CAMACloud.Data

Public Class CloudUserProvider
	Inherits SqlMembershipProvider

	Public Overrides Property ApplicationName As String
		Get
			If HttpContext.Current.GetCAMASession Is Nothing Then
				Return "camacloud"
			Else
                Return HttpContext.Current.GetCAMASession.TenantKey
			End If
			Return MyBase.ApplicationName
		End Get
		Set(value As String)
			MyBase.ApplicationName = value
		End Set
    End Property

    Public Overrides ReadOnly Property MaxInvalidPasswordAttempts As Integer
        Get
            Return 10000
        End Get
    End Property

	Public Overrides Sub Initialize(name As String, config As System.Collections.Specialized.NameValueCollection)
		MyBase.Initialize(name, config)
		Try
			If HttpContext.Current.GetCAMASession Is Nothing Then
				MyBase.ApplicationName = "camacloud"
			Else
                MyBase.ApplicationName = HttpContext.Current.GetCAMASession.TenantKey
			End If
		Catch ex As Exception
			MyBase.ApplicationName = "camacloud" + ex.Message
		End Try
    End Sub

    Public Shared Sub VerifyOrganizationUsers()
        If Roles.GetAllRoles.Count = 0 Then
            Roles.CreateRole("DataSetup")
        End If

        If Not Roles.RoleExists("DataSetup") Then
            Roles.CreateRole("DataSetup")
        End If

        If Not Roles.RoleExists("Support") Then
            Roles.CreateRole("Support")
        End If
        Dim adminRoles = {"DataSetup", "MobileAssessor", "QC", "Tracking", "Reports", "TaskManager"}
        If Membership.GetUser("admin") Is Nothing Then
            Dim newUser = Membership.CreateUser("admin", "adminDCS2O22!", "admin@camacloud.com")
            For Each rn In adminRoles
                If Not Roles.RoleExists(rn) Then
                    Roles.CreateRole(rn)
                End If
                If Roles.IsUserInRole(rn) Then
                    Roles.RemoveUserFromRole("admin", rn)
                    Roles.AddUserToRole("admin", rn)
                Else
                    Roles.AddUserToRole("admin", rn)
                End If
            Next
            If Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM UserSettings WHERE LoginId = 'admin'") = 0 Then
                Database.Tenant.Execute("INSERT INTO UserSettings (LoginId, FirstName) VALUES ('admin', 'Administrator')")
            End If
        Else
            For Each rn In adminRoles
                If Not Roles.RoleExists(rn) Then
                    Roles.CreateRole(rn)
                End If
                If Not Roles.IsUserInRole("admin", rn) Then
                    Roles.AddUserToRole("admin", rn)
                End If
            Next
        End If
        
        If Roles.IsUserInRole("admin", "BasicSettings") Then
			Roles.RemoveUserFromRole("admin","BasicSettings")
		End If

        If Membership.GetUser("dcs-support") Is Nothing Then
            Dim newUser = Membership.CreateUser("dcs-support", "dCS$upport2022!", "support@camacloud.com")

            'Assiging dcs-support roles
            Dim dcsSupportRoles = {"DataSetup", "Support", "MobileAssessor", "QC", "Tracking", "Reports", "TaskManager"}
            For Each rn In dcsSupportRoles
			
			   If Not Roles.RoleExists(rn) Then
                    Roles.CreateRole(rn)
                End If
                
	            If Roles.RoleExists(rn) Then
	                If Roles.IsUserInRole("dcs-support", rn) Then
	                    Roles.RemoveUserFromRole("dcs-support", rn)
	                    Roles.AddUserToRole("dcs-support", rn)
	                Else
	                    Roles.AddUserToRole("dcs-support", rn)
	                End If
	            End If
	        Next

            If Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM UserSettings WHERE LoginId = 'dcs-support'") = 0 Then
                Database.Tenant.Execute("INSERT INTO UserSettings (LoginId, FirstName) VALUES ('dcs-support', 'DCS Support')")
            End If
        Else
            If Not Roles.IsUserInRole("dcs-support", "DataSetup") Then
                Roles.AddUserToRole("dcs-support", "DataSetup")
            End If

            If Not Roles.IsUserInRole("dcs-support", "Support") Then
                Roles.AddUserToRole("dcs-support", "Support")
            End If
        End If
        
    	If Roles.IsUserInRole("dcs-support", "BasicSettings") Then
			Roles.RemoveUserFromRole("dcs-support","BasicSettings")
		End If

        If Membership.GetUser("vendor-cds") Is Nothing Then
            Dim newUser = Membership.CreateUser("vendor-cds", "vendorcds@DCS2O23!", "vendor@camacloud.com")
            'Assigning vendor-cds role
            Dim vendorCDSRoles = {"DataSetup", "TaskManager", "MobileAssessor", "Reports", "Tracking", "QC"}
            For Each rn In vendorCDSRoles
				If Roles.RoleExists(rn) Then
				    If Roles.IsUserInRole("vendor-cds", rn) Then
				        Roles.RemoveUserFromRole("vendor-cds", rn)
				        Roles.AddUserToRole("vendor-cds", rn)
				    Else
				        Roles.AddUserToRole("vendor-cds", rn)
				    End If
				End If
            Next
            
            If Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM UserSettings WHERE LoginId = 'vendor-cds'") = 0 Then
                Database.Tenant.Execute("INSERT INTO UserSettings (LoginId, FirstName) VALUES ('vendor-cds', 'Vendor-cds')")
            End If
        End If
        
        'Create a New User - malite_admin 
        
        If Membership.GetUser("malite_admin") Is Nothing Then
            Dim newUser = Membership.CreateUser("malite_admin", "malite365", "malite@camacloud.com")
            'Assigning malite_admin role
            Dim maliteCDSRoles = {"DataSetup", "TaskManager", "MobileAssessor", "Reports", "Tracking", "QC"}
            For Each rn In maliteCDSRoles
				If Roles.RoleExists(rn) Then
				    If Roles.IsUserInRole("malite_admin", rn) Then
				        Roles.RemoveUserFromRole("malite_admin", rn)
				        Roles.AddUserToRole("malite_admin", rn)
				    Else
				        Roles.AddUserToRole("malite_admin", rn)
				    End If
				End If
            Next
            
            If Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM UserSettings WHERE LoginId = 'malite_admin'") = 0 Then
                Database.Tenant.Execute("INSERT INTO UserSettings (LoginId, FirstName) VALUES ('malite_admin', 'malite_admin')")
            End If
        End If
        
        'END (malite_admin) 
        
        If Membership.GetUser("dcs-qa") Is Nothing Then
            Dim newUser = Membership.CreateUser("dcs-qa", "dcsqa2022!", "csm@datacloudsolutions.net")

            Dim checkRoles = {"DataSetup", "MobileAssessor", "QC", "Tracking", "Reports", "TaskManager"}
            For Each rn In checkRoles
                If Roles.RoleExists(rn) Then
                    If Roles.IsUserInRole("dcs-qa", rn) Then
                        Roles.RemoveUserFromRole("dcs-qa", rn)
                        Roles.AddUserToRole("dcs-qa", rn)
                    Else
                        Roles.AddUserToRole("dcs-qa", rn)
                    End If
                End If
            Next

            If Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM UserSettings WHERE LoginId = 'dcs-qa'") = 0 Then
                Database.Tenant.Execute("INSERT INTO UserSettings (LoginId, FirstName, LastName, Email, DoNotTrack) VALUES ('dcs-qa', 'DCS', 'QA', 'csm@datacloudsolutions.net', 1)")
            End If
        End If

        If Membership.GetUser("dcs-rd") Is Nothing Then
            Dim newUser = Membership.CreateUser("dcs-rd", "dc$rd2022_!", "sync@datacloudsolutions.net")

            Dim dcsRdCheckRoles = {"DataSetup", "MobileAssessor", "QC", "Tracking", "Reports", "TaskManager"}
            For Each rn In dcsRdCheckRoles
                If Roles.RoleExists(rn) Then
                    If Roles.IsUserInRole("dcs-rd", rn) Then
                        Roles.RemoveUserFromRole("dcs-rd", rn)
                        Roles.AddUserToRole("dcs-rd", rn)
                    Else
                        Roles.AddUserToRole("dcs-rd", rn)
                    End If
                End If
            Next

            If Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM UserSettings WHERE LoginId = 'dcs-rd'") = 0 Then
                Database.Tenant.Execute("INSERT INTO UserSettings (LoginId, FirstName, LastName, Email, DoNotTrack) VALUES ('dcs-rd', 'DCS', 'RD', 'sync@datacloudsolutions.net', 1)")
            End If
        End If

        If Membership.GetUser("dcs-integration") Is Nothing Then
            Dim newUser = Membership.CreateUser("dcs-integration", "Dc$INT2O22!", "integration@datacloudsolutions.net")

            Dim checkRoles = {"DataSetup", "MobileAssessor", "QC", "Tracking", "Reports", "TaskManager"}
            For Each rn In checkRoles
                If Roles.RoleExists(rn) Then
                    If Roles.IsUserInRole("dcs-integration", rn) Then
                        Roles.RemoveUserFromRole("dcs-integration", rn)
                        Roles.AddUserToRole("dcs-integration", rn)
                    Else
                        Roles.AddUserToRole("dcs-integration", rn)
                    End If
                End If
            Next

            If Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM UserSettings WHERE LoginId = 'dcs-integration'") = 0 Then
                Database.Tenant.Execute("INSERT INTO UserSettings (LoginId, FirstName, LastName, Email, DoNotTrack) VALUES ('dcs-integration', 'DCS', 'Integration', 'csm@datacloudsolutions.net', 1)")
            End If
        End If

        If Membership.GetUser("dcs-ps") Is Nothing Then
            Dim newUser = Membership.CreateUser("dcs-ps", "Dc$p$2023!", "dcs-ps@woolpert.com")

            'Assiging dcs-support roles
            Dim dcsSupportRoles = {"DataSetup", "Support", "MobileAssessor", "QC", "Tracking", "Reports", "TaskManager"}
            For Each rn In dcsSupportRoles

                If Not Roles.RoleExists(rn) Then
                    Roles.CreateRole(rn)
                End If

                If Roles.RoleExists(rn) Then
                    If Roles.IsUserInRole("dcs-ps", rn) Then
                        Roles.RemoveUserFromRole("dcs-ps", rn)
                        Roles.AddUserToRole("dcs-ps", rn)
                    Else
                        Roles.AddUserToRole("dcs-ps", rn)
                    End If
                End If
            Next

            If Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM UserSettings WHERE LoginId = 'dcs-ps'") = 0 Then
                Database.Tenant.Execute("INSERT INTO UserSettings (LoginId, FirstName, LastName, Email, DoNotTrack) VALUES ('dcs-ps', 'DCS', 'PS', 'dcs-ps@woolpert.com', 1)")
            End If
        Else
            If Not Roles.IsUserInRole("dcs-ps", "DataSetup") Then
                Roles.AddUserToRole("dcs-ps", "DataSetup")
            End If

            If Not Roles.IsUserInRole("dcs-ps", "Support") Then
                Roles.AddUserToRole("dcs-ps", "Support")
            End If
        End If

        If Roles.IsUserInRole("dcs-ps", "BasicSettings") Then
            Roles.RemoveUserFromRole("dcs-ps", "BasicSettings")
        End If

    End Sub

    Public Overrides Function ValidateUser(username As String, password As String) As Boolean
        'If username = "tester" And password = "123456" Then
        '    Return True
        'End If
        Dim orgId As String = HttpContext.Current.GetCAMASession.OrganizationId
        If orgId = -1 Then
            Return False
        End If

        Dim validated As Boolean = (MyBase.ValidateUser(username, password))
        If validated Then
            Return True
        Else
            Dim u As MembershipUser = Membership.GetUser(username, False)
            Try
                Dim pw As String = u.GetPassword

                Dim ve As New VisionEncryptionModule.VisionEncrption
                If ve.Encrypt(password, "65|112|112|114|97|105|115|97|108|32|86|105|115|105|111|110") = pw Then
                    Return True
                End If

                Dim ee As New Encompass.Cryptography.Hashing.Hasher
                If pw.Contains("|") Then
                    Dim hash As String = pw.Split("|")(0)
                    Dim salt As String = pw.Split("|")(1)
                    If ee.ChallengeText(password, hash, salt) Then
                        Return True
                    End If
                End If

                Dim maybeHashed As Boolean = False
                If pw.Length = 42 AndAlso pw.ToLower.StartsWith("0x") AndAlso Not pw.Contains(" ") Then
                    maybeHashed = True
                End If

                If pw <> "" AndAlso ((Not maybeHashed AndAlso password = pw) OrElse (maybeHashed AndAlso (GetHashedString(password, True).ToUpper = pw.ToUpper OrElse GetHashedString(password).ToUpper = pw.ToUpper))) Then
                    u = Membership.GetUser(username, True)
                    'u.LastLoginDate = DateTime.UtcNow
                    'Membership.UpdateUser(u)
                    Return True
                Else
                    Return False
                End If
            Catch nex As NullReferenceException
                'Throw New Exception("You are trying to login as a non-existent user.")
                Return False
            Catch ex As Exception
                'Throw New Exception("Invalid password. Please try again.")
                Return False
            End Try
        End If
    End Function

    'Private Shared Function GetHashedString(data As String) As String
    '    Dim sha1 = System.Security.Cryptography.SHA1.Create()
    '    Dim op = sha1.ComputeHash(System.Text.Encoding.ASCII.GetBytes(data))
    '    Dim hashedString As String = "0x"
    '    For Each b In op
    '        hashedString += Hex(b).PadLeft(2, "0")
    '    Next
    '    Return hashedString
    'End Function
End Class
