﻿Imports System.Net.Sockets
Imports System.Net

Namespace RemoteServices
    Public Class ServiceConnector

        Private _ip As IPAddress
        Private _port As Integer

        Public Sub New(ipAddress As IPAddress, port As Integer)
            _ip = ipAddress
            _port = port
        End Sub

        Public Function SendRequest(request As ServiceRequest) As ServiceResponse
            Dim resp As New ServiceResponse
            Dim netCommand As String = request.ToCommandString
            Dim buffer = System.Text.Encoding.UTF8.GetBytes(netCommand)
            Dim client As New TcpClient
            Try
                client.Connect(_ip, _port)
                client.GetStream().Write(buffer, 0, buffer.Length)
            Catch ex As Exception
                resp = New ServiceResponse(False, ex.Message)
            End Try
            Return resp
        End Function

    End Class
End Namespace