﻿Namespace RemoteServices
    Public Class ServiceResponse

        Private _success As Boolean
        Private _errorMessage As String

        Friend Sub New()
            _success = True
        End Sub

        Friend Sub New(success As Boolean, errorMessage As String)
            _success = success
            _errorMessage = errorMessage
        End Sub

        Public ReadOnly Property Success() As Boolean
            Get
                Return _success
            End Get
        End Property

        Public ReadOnly Property ErrorMessage As String
            Get
                Return _errorMessage
            End Get
        End Property

    End Class
End Namespace