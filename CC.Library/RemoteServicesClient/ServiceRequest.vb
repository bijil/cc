﻿Imports System.Collections.Specialized

Namespace RemoteServices
    Public Class ServiceRequest

        Private _command As String
        Private _properties As NameValueCollection

        Public Sub New(command As String)
            _command = command
            _properties = New NameValueCollection
        End Sub

        Public Sub Add(name As String, value As String)
            _properties.Add(name, value)
        End Sub



        Friend Function ToCommandString()
            Dim res As String = _command + vbCr
            For Each p In _properties.AllKeys
                res += p + "=" + _properties(p) + vbCr
            Next
            Return res
        End Function

    End Class
End Namespace

