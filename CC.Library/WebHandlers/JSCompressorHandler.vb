﻿Imports System.IO
Imports System.Text
Imports Microsoft.Ajax.Utilities
Imports System.IO.Compression

Public Class JSCompressorHandler
    Inherits LiteralControl
    Implements IHttpHandler
    Private JSPath, OutPutFileName, Version As String
    Private IncludeAllFolders As Boolean = False
    Private CACHE_DURATION As TimeSpan = TimeSpan.FromDays(30)
    'Private context As HttpContext
    Public ReadOnly Property IsReusable As Boolean Implements System.Web.IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub ProcessRequest(context As System.Web.HttpContext) Implements System.Web.IHttpHandler.ProcessRequest
        Dim request = context.Request
        Version = DataTools.DataConversion.DecodeFromBase64(request("v"))
        JSPath = DataTools.DataConversion.DecodeFromBase64(request("jsP"))
        IncludeAllFolders = DataTools.DataConversion.DecodeFromBase64(request("IAF"))
        OutPutFileName = DataTools.DataConversion.DecodeFromBase64(request("OFN"))

        '''Decide if browser supports compressed response
        Dim isCompressed As Boolean = CanGZip(context.Request)
        Dim setname = OutPutFileName


        ''' If the set has already been cached, write the response directly from cache. Otherwise generate the response and cache it
        'If Not (WriteFromCache(setname, Version, isCompressed)) Then
        Using ms As MemoryStream = New MemoryStream(8092)
            Using strWriter As Stream = ms

                Dim mergeScripts As StringBuilder = New StringBuilder()
                Dim minfScript As String = String.Empty
                Dim strFullPath = System.Web.HttpContext.Current.Server.MapPath(JSPath)
                Dim IncludeSubFolders As Boolean = True
                For Each fi In Directory.GetFiles(strFullPath, "*.js", IIf(IncludeAllFolders = IncludeSubFolders, IO.SearchOption.AllDirectories, IO.SearchOption.TopDirectoryOnly))
                    mergeScripts.AppendLine(File.ReadAllText(fi))
                Next
                If mergeScripts.Length > 0 Then
                    Dim min1 As New Minifier
                    minfScript = min1.MinifyJavaScript(mergeScripts.ToString)
                End If

                Dim bts = Encoding.UTF8.GetBytes(minfScript)
                strWriter.Write(bts, 0, bts.Length)
            End Using

            Dim responseBytes = ms.ToArray()
            context.Cache.Insert(GetCacheKey(setname, Version, isCompressed), responseBytes, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, CACHE_DURATION)
            WriteBytes(responseBytes, isCompressed)
        End Using
        'End If
    End Sub
    Private Sub WriteBytes(ByVal bytes As Byte(), ByVal isCompressed As Boolean)

        Dim response = Context.Response
        response.AppendHeader("Content-Length", bytes.Length.ToString())
        response.ContentType = "application/x-javascript"
        response.AppendHeader("Content-Encoding", "utf-8")
        Context.Response.Cache.SetCacheability(HttpCacheability.Public)
        Context.Response.Cache.SetExpires(DateTime.Now.Add(CACHE_DURATION))
        Context.Response.Cache.SetMaxAge(CACHE_DURATION)

        response.ContentEncoding = Encoding.Unicode
        response.OutputStream.Write(bytes, 0, bytes.Length)
        response.Flush()
        response.End()
    End Sub

    Private Function WriteFromCache(ByVal setName As String, ByVal version As String, ByVal isCompressed As Boolean) As Boolean
        Dim responseBytes As Byte() = Context.Cache(GetCacheKey(setName, version, isCompressed))

        If (IsNothing(responseBytes)) Then
            Return False
        Else
            WriteBytes(responseBytes, isCompressed)
            Return True
        End If
    End Function
    Private Function CanGZip(ByVal request As HttpRequest) As Boolean
        Dim acceptEncoding As String = request.Headers("Accept-Encoding")
        If (Not String.IsNullOrEmpty(acceptEncoding) And (acceptEncoding.Contains("gzip") Or acceptEncoding.Contains("deflate"))) Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Function GetCacheKey(ByVal setName As String, ByVal version As String, ByVal isCompressed As Boolean) As String
        Return "HttpCombiner." + setName + "." + version + "." + isCompressed.ToString
    End Function

End Class
