﻿Public Class HealthCheckHandler
    Implements IHttpHandler

    Public ReadOnly Property IsReusable As Boolean Implements System.Web.IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub ProcessRequest(context As System.Web.HttpContext) Implements System.Web.IHttpHandler.ProcessRequest
        Dim n As Integer = 5000
        Dim startTime As DateTime = DateTime.Now
        Dim glist As New List(Of String)
        For i As Integer = 1 To n
            glist.Add(Guid.NewGuid.ToString)
        Next

        Dim endTime As DateTime = DateTime.Now
        context.Response.Clear()
        context.Response.ContentType = "text/plain"
        Dim timeTaken As Integer = (endTime - startTime).TotalMilliseconds

        context.Response.Status = "200 OK"
        context.Response.StatusCode = 200
        context.Response.StatusDescription = "OK"
        context.Response.Cache.SetNoStore()
        context.Response.WriteLine("CAMACloud Instance HealthCheck")
        context.Response.WriteLine("========================================================")
        context.Response.WriteLine(n & " unique ids generated in " & timeTaken & " milliseconds.")
        If timeTaken < 600 Then
            context.Response.WriteLine("Health Condition: Excellent")
        ElseIf timeTaken < 1200 Then
            context.Response.WriteLine("Health Condition: Good")
        ElseIf timeTaken < 2000 Then
            context.Response.WriteLine("Health Condition: Average")
        Else
            context.Response.WriteLine("Health Condition: Poor")
        End If
        context.ApplicationInstance.CompleteRequest()
    End Sub
End Class
