﻿Imports System.IO
Public Class AppCacheHandler
    Implements IHttpHandler, IRequiresSessionState

    Dim basePath As String
    Dim exludePaths As New List(Of String)

    Public ReadOnly Property IsReusable As Boolean Implements System.Web.IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub ProcessRequest(context As System.Web.HttpContext) Implements System.Web.IHttpHandler.ProcessRequest
        Dim r = context.Response
        Dim q = context.Request
        Dim s = context.Server

        r.Cache.SetNoStore()

        Dim cacheName = q.Url.AbsolutePath.ToLower.TrimStart("/").Replace(".appcache", "")

        basePath = s.MapPath("~/")
        exludePaths.Add(s.MapPath("~/logs").TrimEnd("/"))

        Dim maxDate As Date = #1/1/2010#
        'Dim lastUpdated As Date = #1/1/2013#
        'GetLatestDateModified(s.MapPath("~/"), lastUpdated)
        'If lastUpdated > ApplicationSettings.ApplicationUpdateDate.AddMinutes(5) Then
        '    ApplicationSettings.ApplicationUpdateDate = lastUpdated
        'End If
        If ClientSettings.CustomizationDate > maxDate Then
            maxDate = ClientSettings.CustomizationDate
        End If
        If Data.Database.Tenant.Application.SchemaVersionDate > maxDate Then
            maxDate = Data.Database.Tenant.Application.SchemaVersionDate
        End If

        'If ApplicationSettings.ApplicationUpdateDate > maxDate Then
        '    maxDate = ApplicationSettings.ApplicationUpdateDate
        'End If

        Dim buildDate As Date = Date.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings("AppBuildTime"))
        If buildDate > maxDate Then
            maxDate = buildDate
        End If

        'If q.UserHostAddress.StartsWith("192.168") Or q.IsLocal Then
        '    Dim lastUpdated As Date = #1/1/2013#
        '    GetLatestDateModified(s.MapPath("~/"), lastUpdated)
        '    If lastUpdated > maxDate Then
        '        maxDate = lastUpdated
        '    End If
        'End If

        Dim version As Long = (maxDate.Ticks - #3/3/2013#.Ticks) / (10000 * 1000)

        If q.QueryString.AllKeys.Contains("reset") Then
            version = q("reset")
        End If

        If HttpContext.Current.Session("REPAIRCACHE1") IsNot Nothing AndAlso HttpContext.Current.Session("REPAIRCACHE1").ToString.Length > 0 Then
            version = HttpContext.Current.Session("REPAIRCACHE1").ToString
            cacheName = "temponline"
            HttpContext.Current.Session.Remove("REPAIRCACHE1")
        ElseIf HttpContext.Current.Session("REPAIRCACHE2") IsNot Nothing AndAlso HttpContext.Current.Session("REPAIRCACHE2").ToString.Length > 0 Then
            version = HttpContext.Current.Session("REPAIRCACHE2").ToString
            cacheName = "temponline"
            HttpContext.Current.Session.Remove("REPAIRCACHE2")
        End If

        r.AddHeader("Last-Modified", Date.UtcNow.ToString("ddd, d MMM yyyy HH:mm:ss " + "GMT"))
        r.Clear()
        r.ContentType = "text/plain"
        r.WriteLine("CACHE MANIFEST")
        r.WriteLine("# Cache Manifest Version: " & version)
        Select Case cacheName
            Case "offline"
                r.WriteLine("# OFFLINE CACHE")
                ListAllPages(r, context.Request)
                ListAllImages(r, s)
                ListAllStylesheets(s.MapPath("~/static"), r)
                ListAllScripts(r, s)
                r.WriteLine("/rwi-worker.js")
                r.WriteLine()
                'r.WriteLine("# page")
                'Dim fn = s.MapPath("~/Default.aspx")
                'Dim info As New FileInfo(fn)
                'Dim relPath As String = fn.Replace(basePath, "\").Replace("\", "/") + "?" & info.LastWriteTimeUtc.Ticks
                'r.WriteLine(relPath)
                'r.WriteLine()
                'r.WriteLine("https://maps.gstatic.com/maps-api-v3/api/js/27/12/main.js")
                'r.WriteLine("https://maps.gstatic.com/maps-api-v3/api/js/27/12/geometry.js")
                'r.WriteLine("https://maps.gstatic.com/maps-api-v3/api/js/27/12/common.js")
                'r.WriteLine("https://maps.gstatic.com/maps-api-v3/api/js/27/12/map.js")
                'r.WriteLine("https://maps.gstatic.com/maps-api-v3/api/js/27/12/util.js")
                'r.WriteLine("https://maps.gstatic.com/maps-api-v3/api/js/27/12/poly.js")
                'r.WriteLine("https://maps.gstatic.com/maps-api-v3/api/js/27/12/onion.js")
                'r.WriteLine("https://maps.gstatic.com/maps-api-v3/api/js/27/12/stats.js")
                r.WriteLine("https://maps.googleapis.com/maps/api/js?libraries=geometry&sensor=false&region=GB")
            Case "images"
                r.WriteLine("# CACHE ONLY IMAGES")
                ListAllImages(r, s)
            Case "styles"
                r.WriteLine("# CACHE ONLY STYLES")
                ListAllStylesheets(r, s)
            Case "scripts"
                r.WriteLine("# CACHE ONLY SCRIPTS")
                ListAllScripts(r, s)
            Case "styles,images", "images,styles"
                r.WriteLine("# CACHE IMAGES & STYLES")
                ListAllImages(r, s)
                ListAllStylesheets(r, s)
            Case Else
                r.WriteLine("# DEFAULT ONLINE CACHE")
        End Select
        r.WriteLine()
        r.WriteLine("NETWORK:")
        r.WriteLine("/repair/")
        r.WriteLine("/repair/default.aspx")
        r.WriteLine("*")
        r.WriteLine()

        'r.WriteLine("# " + q.Url.AbsolutePath.ToLower.TrimStart("/").Replace(".appcache", ""))
        r.End()
    End Sub


    Private Sub GenerateOfflineCacheScript(r As HttpResponse, q As HttpRequest, s As HttpServerUtility)

    End Sub


    Private Sub GetLatestDateModified(path As String, ByRef maxDate As Date)
        For Each fn In Directory.GetFiles(path)
            Dim testDate = File.GetLastWriteTime(fn)
            If testDate > maxDate Then
                maxDate = testDate
            End If
        Next
        For Each dn In Directory.GetDirectories(path)
            If Not exludePaths.Contains(path.TrimEnd("/")) Then
                GetLatestDateModified(dn, maxDate)
            End If
        Next
    End Sub

    Private Sub ListAllPages(r As HttpResponse, context As System.Web.HttpRequest)
        Dim macID = HttpContext.Current.GetKeyValue("MACID")
        Dim ua As String = HttpContext.Current.Request.UserAgent
        If (ua.Contains("OS 11") Or ua.Contains("OS 12")) Then
            r.WriteLine("/default.aspx?macid=" + macID)
            r.WriteLine("/default.aspx?macid=" + macID + "&type=webapp")
        Else
            r.WriteLine("/")
        End If

    End Sub

    Private Sub ListAllImages(r As HttpResponse, s As HttpServerUtility)
        r.WriteLine()
        r.WriteLine("# Images")
        ListAllImages(s.MapPath("~/static"), r)
        r.WriteLine("/applogo.png?logo=logo-ma-app-top-left")
        r.WriteLine("/applogo.png?logo=logo-ma-app-top-right")
        r.WriteLine("/applogo.png?logo=logo-ma-app-splash")
    End Sub

    Private Sub ListAllImages(path As String, r As HttpResponse)
        For Each fn In Directory.GetFiles(path)
            Dim ext = IO.Path.GetExtension(fn).TrimStart(".").ToLower
            If ext = "png" Or ext = "jpg" Or ext = "gif" Or ext = "bmp" Then
                Dim relPath As String = fn.Replace(basePath, "\").Replace("\", "/")
                r.WriteLine(relPath)
            End If
        Next
        For Each dn In Directory.GetDirectories(path)
            If Not exludePaths.Contains(path.TrimEnd("/")) Then
                ListAllImages(dn, r)
            End If
        Next
    End Sub

    Private Sub ListAllStylesheets(r As HttpResponse, s As HttpServerUtility)
        r.WriteLine()
        r.WriteLine("# Stylesheets")
        ListAllStylesheets(s.MapPath("~/static"), r)
    End Sub

    Private Sub ListAllStylesheets(path As String, r As HttpResponse)
        For Each fn In Directory.GetFiles(path)
            Dim ext = IO.Path.GetExtension(fn).TrimStart(".").ToLower
            If ext = "css" Then
                Dim info As New FileInfo(fn)
                Dim relPath As String = fn.Replace(basePath, "\").Replace("\", "/") & "?" & info.LastWriteTimeUtc.Ticks
                r.WriteLine(relPath)
            End If
        Next
        For Each dn In Directory.GetDirectories(path)
            If Not exludePaths.Contains(path.TrimEnd("/")) Then
                ListAllStylesheets(dn, r)
            End If
        Next
    End Sub

    Private Sub ListAllScripts(r As HttpResponse, s As HttpServerUtility)
        r.WriteLine()
        r.WriteLine("# Scripts")
        ListScriptsPath(s.MapPath("~/static/js/lib"), r, False)
        ListScriptsPath(s.MapPath("~/static/js/lib2"), r, True)
        ListScriptsPath(s.MapPath("~/static/js"), r, True)
        ListScriptsPath(s.MapPath("~/static/js/sketchlib"), r, True)
        ListScriptsPath(s.MapPath("~/static/js/crypto"), r, True)
    End Sub

    Private Sub ListScriptsPath(path As String, r As HttpResponse, Optional autoRefreshLatest As Boolean = False)
        'If HttpContext.Current.Request.IsLocal Then
        '    autoRefreshLatest = True
        'End If
        For Each fn In Directory.GetFiles(path)
            Dim ext = IO.Path.GetExtension(fn).TrimStart(".").ToLower
            If ext = "js" Then
                Dim info As New FileInfo(fn)
                Dim relPath As String = fn.Replace(basePath, "\").Replace("\", "/") + IIf(autoRefreshLatest, "?" & info.LastWriteTimeUtc.Ticks, "")
                r.WriteLine(relPath)
            End If
        Next
    End Sub
End Class
