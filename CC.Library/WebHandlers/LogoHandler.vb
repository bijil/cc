﻿Imports System.IO
Public Class LogoHandler
    Implements IHttpHandler, IRequiresSessionState

    Dim basePath As String
    Dim exludePaths As New List(Of String)

    Public ReadOnly Property IsReusable As Boolean Implements System.Web.IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub ProcessRequest(context As System.Web.HttpContext) Implements System.Web.IHttpHandler.ProcessRequest
        Dim r = context.Response
        Dim q = context.Request
        Dim s = context.Server

        Dim isDev As Boolean = q.IsLocal OrElse q.Url.Host.StartsWith("192.168.")
        isDev = False
        Try
            Dim s3 As New S3FileManager
            Dim cacheDuration As TimeSpan = TimeSpan.FromSeconds(30)
            Dim file As String = q("logo")
            If file = "" AndAlso q.FilePath IsNot Nothing AndAlso q.FilePath.Contains("applogo-logo-ma-app") Then
                file = q.FilePath.Replace("/applogo-", "").Replace(".png", "")
            End If
            If file <> "" Then
                Select Case file
                    Case "logo-ma-app-top-left"
                        If Not isDev Then
                            r.Cache.SetCacheability(HttpCacheability.Public)
                            r.Cache.SetExpires(DateTime.Now.Add(cacheDuration))
                            r.Cache.SetMaxAge(cacheDuration)
                            DownloadFromS3(s3, ClientSettings.MobileAssessorTopLeftLogo, "~/static/app/logo-ma-app-top-left.png", r, s)
                            's3.DownloadFile(ClientSettings.MobileAssessorTopLeftLogo, Nothing, r)
                        Else
                            r.Clear()
                            r.ContentType = "image/png"
                            r.TransmitFile(s.MapPath("~/static/app/logo-ma-app-top-left.png"))
                        End If
                    Case "logo-ma-app-top-right"
                        If Not isDev Then
                            r.Cache.SetCacheability(HttpCacheability.Public)
                            r.Cache.SetExpires(DateTime.Now.Add(cacheDuration))
                            r.Cache.SetMaxAge(cacheDuration)
                            DownloadFromS3(s3, ClientSettings.MobileAssessorTopRightLogo, "~/static/app/logo-ma-app-top-right.png", r, s)
                            's3.DownloadFile(ClientSettings.MobileAssessorTopRightLogo, Nothing, r)
                        Else
                            r.Clear()
                            r.ContentType = "image/png"
                            r.TransmitFile(s.MapPath("~/static/app/logo-ma-app-top-right.png"))
                        End If
                    Case "logo-ma-app-splash"
                        If Not isDev Then
                            r.Cache.SetCacheability(HttpCacheability.Public)
                            r.Cache.SetExpires(DateTime.Now.Add(cacheDuration))
                            r.Cache.SetMaxAge(cacheDuration)
                            DownloadFromS3(s3, ClientSettings.MobileAssessorSplash, "~/static/app/ma-splash-image.png", r, s)
                            's3.DownloadFile(ClientSettings.MobileAssessorSplash, Nothing, r)
                        Else
                            r.Clear()
                            r.ContentType = "image/png"
                            r.TransmitFile(s.MapPath("~/static/app/ma-splash-image.png"))
                        End If
                    Case "console-main"
                        If Not isDev Then
                            DownloadFromS3(s3, ClientSettings.ConsoleMain, "~/static/app/console-main.png", r, s)
                            's3.DownloadFile(ClientSettings.MobileAssessorSplash, Nothing, r)
                        Else
                            r.Clear()
                            r.ContentType = "image/png"
                            r.TransmitFile(s.MapPath("~/static/app/console-main.png"))
                        End If
                    Case "ma-vendor-logo"
                        If Not isDev Then
                            DownloadFromS3(s3, VendorInfo.MAVendorLogo, "~/App_Static/images/ccma-small.png ", r, s)
                        Else
                            r.Clear()
                            r.ContentType = "image/png"
                            r.TransmitFile(s.MapPath("~/App_Static/images/ccma-small.png"))
                        End If

                End Select
            Else
                s3.DownloadFile("assets/colors/red.png", Nothing, r)

            End If

        Catch ex As Exception
            ErrorMailer.ReportException("SyncError", ex, q)
            r.Clear()
            r.ContentType = "image/png"
            r.TransmitFile(s.MapPath("~/static/css/colors/ffffff.png"))
        End Try


    End Sub


    'Public Sub DownloadFromS3(s3 As S3FileManager, path As String, defaultPath As String, response As HttpResponse, s As HttpServerUtility)
    '    Try
    '        s3.DownloadImageAsync(path, response)
    '    Catch ex1 As Exception
    '        Try
    '            s3.DownloadImageAsync(path, response)
    '        Catch ex2 As Exception
    '            Try
    '                s3.DownloadImageAsync(path, response)
    '            Catch ex3 As Exception
    '                Try
    '                    s3.DownloadImageasync(path, response)
    '                Catch ex4 As Exception
    '                    ErrorMailer.ReportException("SyncError", ex4, HttpContext.Current.Request)
    '                    response.Clear()
    '                    response.ContentType = "image/png"
    '                    If IO.File.Exists(s.MapPath(defaultPath)) Then
    '                        response.TransmitFile(s.MapPath(defaultPath))
    '                    Else
    '                        response.TransmitFile(s.MapPath("~/App_Static/css/images/white.png"))
    '                    End If


    '                End Try
    '            End Try
    '        End Try
    '    End Try
    'End Sub


    Public Sub DownloadFromS3(s3 As S3FileManager, path As String, defaultPath As String, response As HttpResponse, s As HttpServerUtility)
        Try
            Dim foundInS3 As Boolean = False
            Try
                s3.DownloadImage(path, response)
                foundInS3 = True
            Catch ex As Exception
                ErrorMailer.ReportException("AWS.S3", ex, HttpContext.Current.Request)
            End Try

            If Not foundInS3 Then
                response.Clear()
                response.ContentType = "image/png"
                If IO.File.Exists(s.MapPath(defaultPath)) Then
                    response.TransmitFile(s.MapPath(defaultPath))
                Else
                    response.TransmitFile(s.MapPath("~/App_Static/css/images/white.png"))
                End If
            End If
        Catch exF As Exception
            CAMACloudRequestLogger.Default.LogError("LogoHandler.DownloadFromS3", exF.Message)
        End Try
    End Sub


End Class
