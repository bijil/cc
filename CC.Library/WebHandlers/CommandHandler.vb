﻿Public Class CommandHandler
    Implements IHttpHandler

    Public ReadOnly Property IsReusable As Boolean Implements System.Web.IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub ProcessRequest(context As System.Web.HttpContext) Implements System.Web.IHttpHandler.ProcessRequest
        Dim r = context.Response
        Dim q = context.Request
        Dim s = context.Server

        r.Cache.SetNoStore()

        Dim command = q.Url.AbsolutePath.ToLower.TrimStart("/").Replace(".cmd", "") '

        Select Case command
            Case "__updateversion"
                ApplicationSettings.ApplicationUpdateDate = Now
        End Select
    End Sub
End Class
