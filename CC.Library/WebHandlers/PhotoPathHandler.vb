﻿Imports System.IO
Public Class PhotoPathHandler
    Implements IHttpHandler, IRequiresSessionState

    Dim basePath As String
    Dim exludePaths As New List(Of String)

    Public ReadOnly Property IsReusable As Boolean Implements System.Web.IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Sub ProcessRequest(context As System.Web.HttpContext) Implements System.Web.IHttpHandler.ProcessRequest
        Dim r = context.Response
        Dim q = context.Request
        Dim s = context.Server
        Dim isDev As Boolean = False And (q.IsLocal OrElse q.Url.Host.StartsWith("192.168."))

        Dim s3 As New S3FileManager

        Dim requestPath = q.Path.TrimStart("/")
        Dim s3Path As String = requestPath.Split("/".ToCharArray, 2)(1)
        If isDev Then
            Dim fileName As String = IO.Path.GetFileName(requestPath)
            Dim tempFileName As String = s.MapPath("~/~clients/test/" + fileName)
            r.Clear()
            r.ContentType = "image/jpeg"
            r.WriteFile(tempFileName)
        Else
            Try
                r.Cache.SetExpires(Now.AddDays(30))
                s3.DownloadImage(s3Path, r)
            Catch ex As Exception
                Throw New Exception("Error opening image " + requestPath, ex)
            End Try

        End If


        'Dim file As String = q("logo")
        'If file <> "" Then
        '    Select Case file
        '        Case "logo-ma-app-top-left"
        '            If Not isDev Then
        '                s3.DownloadFile(ClientSettings.MobileAssessorTopLeftLogo, Nothing, r)
        '            Else
        '                r.TransmitFile(s.MapPath("~/static/app/logo-ma-app-top-left.png"))
        '            End If
        '        Case "logo-ma-app-top-right"
        '            If Not isDev Then
        '                s3.DownloadFile(ClientSettings.MobileAssessorTopRightLogo, Nothing, r)
        '            Else
        '                r.TransmitFile(s.MapPath("~/static/app/logo-ma-app-top-right.png"))
        '            End If

        '    End Select
        'Else
        '    s3.DownloadFile("assets/colors/red.png", Nothing, r)
        'End If

    End Sub


End Class
