﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/CAMASV.master"
	AutoEventWireup="false" CodeFile="reports.aspx.vb" Inherits="reports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Styles" runat="Server">
	<link rel="Stylesheet" href="/App_Static/css/reports.css" type="text/css" />
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="head">
	<script type="text/javascript">
		var rlayout;
		var sht;
		function setPageLayout(w, h) {
			sht = h;
			setTimeout('setReportHeight()', 500);
			$('.report-container-cell').height(h - $('.page-title').height());
		}

		function setReportHeight() {
			rlayout = $('#ctl00_MainContent_viewer_fixedTable > tbody > tr')[4];
			$(rlayout).attr('class', 'report-view-row');

			var h = sht;
			var rh = h - $('.page-title').height() - 29 - 11;
			$('.report-view-row > td').height(rh);
		}

		function setPageFunctions() {
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<section class="screen" id="reports">
		<div class="title-line page-title">
			Sketch Validation Reports</div>
		<table class="report-container">
			<tr>
				<td class="report-container-cell report-selection-frame">
					<div class="report-selection">
						<div>
							<asp:DropDownList runat="server" ID="ddlReport">
							</asp:DropDownList>
						</div>
						<div>
							<asp:Button runat="server" ID="btnGenerate" Text="Generate Report" />
						</div>
					</div>
				</td>
				<td class="report-container-cell report-frame">
					<reports:ReportViewer ID="viewer" runat="server" Width="100%" ShowPrintButton="true"
						ShowFindControls="false" ShowZoomControl="true" KeepSessionAlive="true" CssClass="report-viewer" ExportContentDisposition="AlwaysAttachment" 
						ClientIDMode="Static">
					</reports:ReportViewer>
				</td>
			</tr>
		</table>
	</section>
</asp:Content>
