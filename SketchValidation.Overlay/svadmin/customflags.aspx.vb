﻿
Partial Class svadmin_customflags
    Inherits System.Web.UI.Page

    Sub LoadGrid()
        grid.DataSource = Database.Tenant.GetDataTable("SELECT * FROM SketchReviewFlags ORDER BY Name")
        grid.DataBind()
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If txtFlagName.Text.Trim <> "" Then
            Database.Tenant.Execute("INSERT INTO SketchReviewFlags (Name) VALUES (" + txtFlagName.Text.ToSqlValue + ");")
            LoadGrid()
            txtFlagName.Text = ""
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadGrid()
        End If
    End Sub

    Protected Sub grid_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grid.RowCommand
        If e.CommandName = "DeleteFlag" Then
            Database.Tenant.Execute("DELETE FROM SketchReviewFlags WHERE Id = " & e.CommandArgument)
            LoadGrid()
        End If
    End Sub
End Class
