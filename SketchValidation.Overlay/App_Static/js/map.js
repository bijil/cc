﻿var tileSize = 256;

var countyMapOptions = {
	getTileUrl: function (coord, zoom) {
		var x = coord.x;
		var y = coord.y;
		var lng = ((x * tileSize) - (tileSize * (Math.pow(2, zoom - 1)))) / ((tileSize * (Math.pow(2, zoom))) / 360);
		var ex = ((y * tileSize) - (tileSize * (Math.pow(2, zoom - 1)))) / ((-tileSize * Math.pow(2, zoom)) / (2 * Math.PI));
		var lat = ((2 * Math.atan(Math.exp(ex))) - (Math.PI / 2)) / (Math.PI / 180);
		if (lng > 180) lng -= 360;
		if (lng < -180) lng += 360;
		if (lat < -90) lat = -90;
		if (lat > 90) lat = 90;
		return "/mapsvc/tile/?lat=" + lat + "&lng=" + lng + '&zoom=' + zoom;
		//return "http://localhost:8808/?ll=" + lat + "," + lng + '&z=' + zoom;
	},
	tileSize: new google.maps.Size(tileSize, tileSize),
	maxZoom: 20,
	minZoom: 12,
	zoom:19,
	name: "County Imagery"
};

var countyMapType = new google.maps.ImageMapType(countyMapOptions);

function loadSearchMap() {
	var mapOptions = {
		zoom: 18,
		maxZoom: 20,
		minZoom: 6,
		center: new google.maps.LatLng(41.68953, -83.576147),
		tilt: 0,
		streetViewControl: false
//        ,
//		mapTypeControlOptions: {
//			mapTypeIds: ["CountyImagery", google.maps.MapTypeId.HYBRID]
//		}
	};
	var mc = document.getElementById('search-map');
	map = new google.maps.Map(mc, mapOptions);

	google.maps.event.addListener(map, 'bounds_changed', (function () {
	    window.setTimeout('showResultsOnMap();', 500);
	}));

    //map.mapTypes.set('CountyImagery', countyMapType);
	//map.setMapTypeId('CountyImagery');
	map.setMapTypeId('hybrid');

//	google.maps.event.addListener(map, 'zoom_changed', (function () {
//	    if ((map.getZoom() > 17) && (activeParcel != null)) {
//	        map.setMapTypeId('CountyImagery');
//	    } else {
//	        map.setMapTypeId('hybrid');
//	    }
//	}));



	
//	google.maps.event.addListener(parcelPolygon, 'click', (function (e) {
//	    if (measure.on) {
//	        if (measure.start == null) {
//	            measure.start = e.latLng;
//	        } else {
//	            if (measure.end == null) {
//	                measure.end = e.latLng;
//	            } else {
//	                measure.start = e.latLng;
//	                measure.end = null;
//	            }
//	        }

//	        measure.updateGraphics();
//	        e.stop();
//	    }
//	}));
	
}

