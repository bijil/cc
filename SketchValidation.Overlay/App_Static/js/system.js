﻿function setPageLayout(w, h) {

}

function setPageFunctions() {
}
$(window).bind('resize', setScreenLayout);
function setScreenLayout() {
	document.body.scrollTop = 0;
	document.body.scrollLeft = 0;

	var mb = 5 + 5 + 2; 			//body margin & border
	var wH = $(window).height(); //window height
	var wW = $(window).width(); 	//window width
	$(document.body).height(wH - mb);
	$(document.body).width(wW - mb);

	var h = wH - ($('header').height() + $('footer').height() + $('menu').height() + mb);
	var w = wW - mb;
	$('.wrapper').height(h);
	$('.wrapper').width(w);

	setPageLayout(w, h);
	
}

$(function () {
	setScreenLayout();
	$('input').blur(setScreenLayout);
	$('header').bind('click', setScreenLayout);
	$('footer').bind('click', setScreenLayout);
	setPageFunctions();

	if (!theForm) {
		theForm = document.dcs;
	}
});

$(document).bind('touchmove', function (e) {
	e.preventDefault();
});

function showHistory() {
	$('#btnRefreshHistory').click();
	$('.screen').hide();
	$('#history').show();
}

function showSV() {
	$('.screen').hide();
	$('#sketch-validation').show();
}

function showReports() {
	$('.screen').hide();
	$('#reports').show();
}

function openSV(pid) {
	showSV();
	openParcel(pid);
	return false;
}