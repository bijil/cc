﻿var hasNbhdBounds = false;

function doSearch() {
    closeParcel();
    var parcel = $('.src-parcel').val();
    var nbhd = $('.src-nbhd').val();
    var flag = $('.src-flag').val();
    var user = $('.src-user').val();

    if ((parcel != '') && (nbhd != '')) {
        alert('You cannot use Parcel ID and Neighborhood in same search.');
        return false;
    }

    //    if ((parcel == '') && (nbhd == '')) {
    //        alert('You need to enter either Parcel ID or Neighborhood to search.');
    //        return false;
    //    }

    $.ajax({
        url: '/sv/search.jrq',
        data: {
            parcel: parcel,
            nbhd: nbhd,
            flag: flag,
            user: user
        },
        dataType: 'json',
        success: function (resp) {
            displaySearchResults(resp);
        },
        error: function (resp) { }
    });

    return false;
}

function clearSearch() {
    hasNbhdBounds = false;

    if (nbhdpolygon != null) {
        nbhdpolygon.setMap(null);
        delete nbhdpolygon;
    }

    for (x in markers) {
        markers[x].setMap(null);
        delete markers[x];
    }

    for (x in parcels) {
        delete parcels[x];
    }

    closeParcel();

}

var nbhdpolygon;
var parcels;
var parcelindex;
function displaySearchResults(results) {
    if (results.status == 'Error') {
        alert(resp.message);
        return;
    }
    parcels = results.Parcels;
    parcelindex = 0;
    hasNbhdBounds = results.HasNeighborhood;

    if (results.NeighborhoodPoints) {
        var points = [];
        var bounds = new google.maps.LatLngBounds();

        for (x in results.NeighborhoodPoints) {
            var pt = results.NeighborhoodPoints[x];
            latlng = new google.maps.LatLng(pt.Latitude, pt.Longitude);
            points.push(latlng);
            bounds.extend(latlng);
        }

        if (map) {
            var polyOptions = {
                path: points,
                strokeColor: "#FF0000",
                strokeOpacity: 0.8,
                strokeWeight: 4,
                fillOpacity: 0,
                visible: true,
                clickable: false
            }
            if (nbhdpolygon == null) {
                nbhdpolygon = new google.maps.Polygon(polyOptions);
            }
            else {
                if (nbhdpolygon.setOptions)
                    nbhdpolygon.setOptions(polyOptions);
            }
            nbhdpolygon.setMap(map);
            map.setCenter(bounds.getCenter());
            map.fitBounds(bounds);
        }
    }
    if (parcels.length == 0) {
        alert('Your search did not produce any results.');
    } else if (parcels.length == 1) {
        openParcel(parcels[0].ID);
    } else {
        showResultsOnMap(true);
        if ((map.getZoom() < 19) && (parcels.length > 30)) {
            alert('Zoom down map to appropriate location to view the parcels.');
        }
    }
}


var svg = '<?xml version="1.0" standalone="no"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg width="4cm" height="4cm" viewBox="0 0 400 400" xmlns="http://www.w3.org/2000/svg" version="1.1"><title>Example triangle01- simple example of a "path"</title><desc>A path that draws a triangle</desc><rect x="1" y="1" width="398" height="398" fill="none" stroke="blue" /><path d="M 100 100 L 300 100 L 200 300 z" fill="red" stroke="blue" stroke-width="3" /></svg>'

var markers = [];
var infoWindow;


function showResultsOnMap(onsearch) {
    if (parcels == null) {
        return;
    }
    console.log("ShowResults ", (new Date()).toLocaleTimeString());
    clearMarkers();
    console.log("Markers Cleared at ", (new Date()).toLocaleTimeString());
    var mz = map.getZoom();
    if (hasNbhdBounds) {
        if ((mz < 19) && (parcels.length > 30)) {
            return;
        }
    }

    if (onsearch && !hasNbhdBounds) {
        var pbounds = new google.maps.LatLngBounds();
        for (x in parcels) {
            var p = parcels[x];
            var marker = getMarker(p);
            pbounds.extend(marker.position);
        }

        map.setCenter(pbounds.getCenter());
        map.fitBounds(pbounds);

    } else {
        console.log("Start ", (new Date()).toLocaleTimeString());
        
        var bounds = map.getBounds();
        var sw = bounds.getSouthWest();
        var ne = bounds.getNorthEast();



        var lat1 = sw.lat();
        var lon1 = sw.lng();
        var lat2 = ne.lat();
        var lon2 = ne.lng();

        console.log("Prepared ", (new Date()).toLocaleTimeString());

        for (x in parcels) {
            var p = parcels[x];
            if ((p.Latitude > lat1) && (p.Latitude < lat2) && (p.Longitude > lon1) && (p.Longitude < lon2)) {
                var marker = getMarker(p);
            }
        }
    }


    showMarkersInMap();
    console.log("Done ", (new Date()).toLocaleTimeString());
}

function clearMarkers() {
    for (x in markers) {
        markers[x].setMap(null);
        delete markers[x];
    }
}

function showMarkersInMap() {
    for (x in markers) {
        markers[x].setMap(map);
    }

}

function getMarker(p) {
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(p.Latitude, p.Longitude),
        icon: {
            path: google.maps.SymbolPath.CIRCLE,
            scale: 7,
            fillColor: p.ColorCode,
            strokeColor: p.ColorCode,
            fillOpacity: 0.3,
            strokeWeight: 2
        }
//        ,
//        map: map
    });


    marker.ParcelId = p.ID;

    google.maps.event.addListener(marker, "click", function () {
        openParcel(this.ParcelId);
    });

    markers.push(marker);

    return marker;
}