﻿$(function () {

    $('.cmd-search').click(function () {
        doSearch();
        return false;
    });

    $('.cmd-cancel').click(function () {
        $('.src-field').val('');
        clearSearch();
        return false;
    });

    $('.cmd-reset').click(function () {
        if (activeParcel != null) {
            openParcel(activeParcel.ID);
        }
        return false;
    });

    $('.cmd-savereview').click(function () {
        var data = {
            pid: activeParcel.ID,
            notes: $('.review-note').val(),
            flag: '',
            szoom: mapFrame.scale,
            mzoom:mapFrame.mapZoom,
            rotate: mapFrame.degree,
            salat: mapFrame.getParcelCenter().lat(),
            salng: mapFrame.getParcelCenter().lng(),
            status: ''
        };

        $('.flag-fields :checkbox').each(function () {
            if (this.checked) {
                if (data['flag'] != '') {
                    data['flag'] += ',';
                }
                data['flag'] += $(this).attr('flag');
            }
        });

        var fstat = $('#rblStatus input:checked').val();
        if ((fstat == undefined) || (fstat == null)) {
            fstat = '';
        }
        data['status'] = fstat;

        $.ajax({
            url: '/sv/savereview.jrq',
            data: data,
            dataType: 'json',
            success: function (resp) {
                if (activeParcel != null) {
                    openParcel(activeParcel.ID);
                }
            },
            error: function (resp) { }
        });

        return false;
    });

    $('.cmd-next').click(function () {
        if (parcels == undefined) {
            alert('Navigate option not available. Run any query to enable list navigation.');
            return false;
        }
        try {
            if (parcelindex < parcels.length) {
                parcelindex++;
                openParcel(parcels[parcelindex].ID);
            }
        }
        catch (e) {
            alert(e);
        }
        return false;
    });

    $('.cmd-prev').click(function () {
        if (parcels == undefined) {
            alert('Navigate option not available. Run any query to enable list navigation.');
            return false;
        }
        if (parcelindex > 0) {
            parcelindex--;
            openParcel(parcels[parcelindex].ID);
        }
        return false;
    });
});