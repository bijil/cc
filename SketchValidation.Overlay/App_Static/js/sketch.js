﻿var iResize, iRotate;

function setPageLayout(w, h) {
    $('.screen').height(h);
    $('.contentpage').height(h - $('.page-title').height());
    $('.contentpage').css({ 'overflow-y': 'auto' });
    $('#search-map').height(h - $('.search-panel').height() - 8);
    $('#mapFrame').height(h - $('.search-panel').height() - 8);
    $('.parcel-frame-table').height($('#search-map').height() - $('.toolbar').height());
    $('#sketchvas').height($('#search-map').height() - 198);
    $('.view-oby').height($('#search-map').height() - 140);
    $('.view-oby').css({ 'overflow': 'auto' });
    $('.otherflags').height($('#search-map').height() - 348);
    if (currentSketch != null) {
        paintSketch();
    }
}

function setPageFunctions() {
    $('#sketchvas').bind('mousewheel', function (e) {
        var zoom = parseFloat($('.sketch-resize').val());
        var step = 0.1;
        if (e.originalEvent.wheelDelta > 0) {
            zoom += step;
        } else {
            zoom -= step;
        }

        if (zoom > 2) {
            zoom = 2;
        }
        if (zoom < 0.5) {
            zoom = 0.5;
        }
        $('.sketch-resize').val(zoom);
        paintSketch();
    });

    $('.review-form .flag-fields :checkbox').iphoneStyle({ checkedLabel: 'YES', uncheckedLabel: 'NO' });
    iResize = new Slider(document.getElementById("sketch-resize"), document.getElementById("sketch-resize-input"));


    closeParcel();

    loadSearchMap();

    $('.screen').hide();
    $('#sketch-validation').show();

    if (window.location.hash != '') {
        var param = window.location.hash.replace('#', '');
        if (parseInt(param) != NaN) {
            openSV(param);
        }
    }


    $('.a-view-oby').click(function () {
        $('.view-sketch').hide();
        $('.view-oby').show();
        $('.a-view-sketch').show();
        $('.a-view-oby').hide();

        if (activeParcel != null) {

            if (activeParcel.OutBuildings.length > 0) {
                var table = "";
                var foby = activeParcel.OutBuildings[0];

                table += "<tr>";


                for (y in foby) {
                    table += "<th>" + y + "</th>";
                }

                table += "</tr>";
                for (x in activeParcel.OutBuildings) {
                    var oby = activeParcel.OutBuildings[x];
                    table += "<tr>";
                    for (y in oby) {
                        table += "<td>" + oby[y] + "</td>";
                    }
                    table += "</tr>";
                }

                table = "<table style='width:100%;border-spacing:0px;border-collapse:collapse;' border='1'>" + table + "</table>";

                $('.view-oby').html(table);

            }

            return false;
        }

    });


    $('.a-view-sketch').click(function () {
        $('.view-sketch').show();
        $('.view-oby').hide();
        $('.a-view-sketch').hide();
        $('.a-view-oby').show();

        return false;
    });


}