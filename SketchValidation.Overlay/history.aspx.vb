﻿
Partial Class history
	Inherits System.Web.UI.Page

	Sub LoadHistory()
		rpAudit.DataSource = d_("SELECT TOP 50 pa.*, dbo.GetLocalDate(EventTime) As ReviewTime, p.StreetAddress, p.KeyValue1 FROM ParcelAuditTrail pa LEFT OUTER JOIN Parcel p ON pa.ParcelId = p.Id WHERE EventType = 10 ORDER BY EventTime DESC")
		rpAudit.DataBind()
		lblAuditCount.Text = rpAudit.Items.Count
	End Sub

	Protected Sub btnRefreshHistory_Click(sender As Object, e As System.EventArgs) Handles btnRefreshHistory.Click
		LoadHistory()
	End Sub

	Dim lastDate As String = ""
	Public Function ShowDate() As String
		Dim dt As String = CDate(Eval("ReviewTime")).ToString("MMM d, yyyy")
		If dt <> lastDate Then
			lastDate = dt
			Return "<div class='date-header'>" + dt + "</div>"
		End If
		lastDate = dt
		Return ""
	End Function

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
			LoadHistory()
		End If
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class
