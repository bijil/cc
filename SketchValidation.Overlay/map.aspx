﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="map.aspx.vb" Inherits="sketchv_map" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="/App_Static/css/measure.css" />
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=geometry&sensor=false&region=US"></script>
    <script type="text/javascript" src="/App_Static/jslib/jquery.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/jquery-ui-1.8.20.custom.min.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/jquery.rotate.js"></script>
    <script type="text/javascript" src="/App_Static/js/sigmasketch.js?1"></script>
    <script type="text/javascript">

        var map;
        var mapZoom;
        var drag = false;
        var overlay;
        var polygonCenter;
        var degree;
        var scale;
        var RotationDegree;
        var activeParcel;
        var Parcelid;
        var ParcelDataExist = false;
        var parcelPolygon = new google.maps.Polygon();
        var ParcelCenter;
        var alreadyDone;
        var parcelLoaded = false;
        $(function () {
            setPageDimensions();
            initMap();
        });

        $(window).resize(setPageDimensions);

        function initMap() {
            var options = {
                zoom: 20,
                zoomControl: true,
                center: new google.maps.LatLng(41.655471, -83.534546),
                mapTypeId: google.maps.MapTypeId.HYBRID,
                minZoom: 17,
                tilt: 0
            };
            var mc = document.getElementById('map');
            map = new google.maps.Map(mc, options);
            map.setZoom(20);

            overlay = new google.maps.OverlayView();
            overlay.draw = function () { };
            overlay.setMap(map);
            google.maps.event.addListener(map, 'drag', function () {
                setPositionOverlayIfDataExist();
            });
            google.maps.event.addListener(map, 'zoom_changed', function () {
                var zoomLevel = map.getZoom();
                if (zoomLevel == 20) {
                    zoomSketch(parseFloat(.6));
                }
                else {
                    zoomSketch(parseFloat(zoomLevel / 80));
                }
                setPositionOverlayIfDataExist();
                mapZoom = zoomLevel;
            });

            var measure;
            var measureDiv = document.createElement('div');
            measure = new MeasurementScale(measureDiv, map);
            measureDiv.index = 1;
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(measureDiv);

            google.maps.event.addListener(map, 'click', (function (e) {
                if (measure.on) {
                    if (measure.start == null) {
                        measure.start = e.latLng;
                    } else {
                        if (measure.end == null) {
                            measure.end = e.latLng;
                        } else {
                            measure.start = e.latLng;
                            measure.end = null;
                        }
                    }

                    measure.updateGraphics();
                    e.stop();
                }
            }));

        }

        function openParcel(parcelId, callback) {
            parcelLoaded = false;
            alreadyDone = true;
            ParcelDataExist = false;
            degree = 0;
            Parcelid = 0;
            getParcelMapAndVector(parcelId, function (parcel) {
                activeParcel = parcel;
                var polyOptions = {
                    path: parcel.LatLngPoints,
                    strokeColor: "#FF0000",
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: "#FF0000",
                    fillOpacity: 0.1,
                    visible: true
                }
                if (parcelPolygon == null) {
                    parcelPolygon = new google.maps.Polygon(polyOptions);
                }
                else {
                    if (parcelPolygon.setOptions)
                        parcelPolygon.setOptions(polyOptions);
                }
                parcelPolygon.setMap(map);
                google.maps.event.trigger(map, 'resize');

                //parcel.dataExist = false;
                console.log(parcel);
                if (parcel.dataExist) {
                    drawParcel(0, 0, parcel.SketchVector, 'white');
                    ParcelCenter = new google.maps.LatLng(parcel.lat, parcel.lng);
                    ParcelDataExist = true;
                    setPositionOverlayIfDataExist();
                    scale = parseFloat(parcel.sketchZoom);
                    RotationDegree = parseFloat(parcel.degree);
                    

                    map.setCenter(parcel.Bounds.getCenter());
                    map.setZoom(parseInt(parcel.mapZoom));
                    zoomSketch(parcel.sketchZoom);
                    rotateSketch(parseFloat(parcel.degree));
                }
                else {
                    drawParcel(0, 0, parcel.SketchVector, 'yellow');
                    zoomSketch(0.5);
                    setPositionOverlayIfDataExist();
                    map.setCenter(parcel.Bounds.getCenter());
                    map.setZoom(20);

                }
                if (callback) callback(parcel);

            });

        }

        function getParcelMapAndVector(parcelid, callback) {
            Parcelid = parcelid;
            $.ajax({
                url: '/sv/getparcel.jrq',
                method: 'POST',
                data: {
                    parcel: parcelid
                },
                success: function (resp) {
                    var points = [];
                    var bounds = new google.maps.LatLngBounds();
                    for (var i = 0; i < resp.Points.length; i++) {
                        lat = resp.Points[i].Latitude;
                        lng = resp.Points[i].Longitude;
                        latlng = new google.maps.LatLng(lat, lng);
                        points.push(latlng);
                        bounds.extend(latlng);
                    }
                    polygonCenter = bounds.getCenter();
                    resp["LatLngPoints"] = points;
                    resp["Bounds"] = bounds;

                    if (callback) callback(resp);
                }
            });
        }

        function drawParcel(latitude, longitude, vector, color) {
            var sketchUrl = sigmaSketch(color, vector, 'scanvas', false, false);
            $('#sketch').attr('src', sketchUrl);
            $(".overlay").draggable({ containment: "#map", scroll: false });
            $("#map").droppable();
        }

        function LatLngToPoint(latlng) {
            return overlay.getProjection().fromLatLngToContainerPixel(latlng);
        }
        function PointToLatLng(point) {
            return overlay.getProjection().fromContainerPixelToLatLng(point)
        }

        function setPositionOverlayIfDataExist() {
            var position;
            ///console.log(polygonCenter, ParcelCenter);
            if (ParcelDataExist) {
                position = LatLngToPoint(ParcelCenter);
            }
            else {
                position = LatLngToPoint(polygonCenter);
            }
            var x = parseFloat(position.x);
            var y = parseFloat(position.y);
            var mapH = parseFloat($('#map').height());
            var divW = parseFloat($('#resizable-wrapper').css("width")) / 2;
            var divH = parseFloat($('#resizable-wrapper').css("height")) / 2;
            y = y - mapH - divH;
            x = (x - divW);

            $('.overlay').css({
                'top': y + 'px',
                'left': x + 'px',
                'z-index': '1000'
            });

            //ParcelCenter = PointToLatLng(new google.maps.Point(position.x, position.y));
        }

        function setPageDimensions() {
            $('#map').height($(window).height());
            $('#map').width($(window).width());
            try {
                setPositionOverlayIfDataExist();
            }
            catch (err) { 
            }
        }

        function zoomSketch(zoom) {
            scale = zoom;
            $('#sketch').width(parseInt($('#scanvas').attr('width')) * zoom);
            $('#sketch').height(parseInt($('#scanvas').attr('height')) * zoom);
            $('#resizable-wrapper').width(parseInt($('#scanvas').attr('width')) * zoom);
            $('#resizable-wrapper').height(parseInt($('#scanvas').attr('height')) * zoom);
            setPositionOverlayIfDataExist();
        }

        function rotateSketch(deg) {
            degree = deg;
            $('#sketch').rotate({ animateTo: deg });
        }

        function saveForm(comment, flagChecked, validChecked) {
            $.ajax({
                url: '/sv/saveForm.jrq',
                method: 'POST',
                data: {
                    parcel: Parcelid,
                    exist: ParcelDataExist,
                    comment: comment,
                    flag: flagChecked,
                    valid: validChecked
                },
                success: function (response) { }
            });
        }


        function getParcelCenter() {
            var map_width = parseFloat($("#map").css("width"));
            var map_height = parseFloat($("#map").css("height"));
            var left = parseFloat($("#resizable-wrapper").css("left"));
            var top = parseFloat($("#resizable-wrapper").css("top"));
            var x = left + parseFloat($('#sketch').css("width")) / 2;
            var y = map_height + top + parseFloat($('#sketch').css("height")) / 2;
            ParcelCenter = PointToLatLng(new google.maps.Point(x, y));
            return ParcelCenter;
        }

        function AnchorSketch() {

            var map_width = parseFloat($("#map").css("width"));
            var map_height = parseFloat($("#map").css("height"));
            var left = parseFloat($("#resizable-wrapper").css("left"));
            var top = parseFloat($("#resizable-wrapper").css("top"));
            var x = left + parseFloat($('#sketch').css("width")) / 2;
            var y = map_height + top + parseFloat($('#sketch').css("height")) / 2;
            var canvasSize = parseFloat($('#scanvas').attr('width'));
            var sketZoom = parseFloat($('#sketch').css("width")) / parseFloat($('#scanvas').attr('width'));
            var mapZoom = map.zoom;
            ParcelCenter = PointToLatLng(new google.maps.Point(x, y));

            $.ajax({
                url: '/sv/updatesketch.jrq',
                method: 'POST',
                data: {
                    parcel: Parcelid,
                    exist: ParcelDataExist,
                    degree: degree,
                    canvasSize: canvasSize,
                    sketchZoom: sketZoom,
                    mapZoom: mapZoom,
                    lat: ParcelCenter.lat(),
                    lng: ParcelCenter.lng()


                },
                success: function (resp) {
                    new Messi('Sketch anchored.', { autoclose: 700 });
                }
            });
        }

        function MeasurementScale(div, map) {
            var ms = this;
            this.control = div;
            this.on = false;
            this.start = null;
            this.end = null;

            var mA, mB, line;
            mA = new google.maps.Marker();
            mB = new google.maps.Marker();
            line = new google.maps.Polyline({ strokeColor: 'Yellow' });

            this.clear = function () {
                this.start = null;
                this.end = null;
                this.updateGraphics();
                label.innerHTML = '0.0ft';
            }

            this.updateGraphics = function () {
                mA.setMap(null);
                mB.setMap(null);
                line.setMap(null);

                if (ms.start != null) {
                    mA.setPosition(ms.start);
                    mA.setMap(map);
                }

                if (ms.end != null) {
                    mB.setPosition(ms.end);
                    mB.setMap(map);
                }

                if ((ms.start != null) && (ms.end != null)) {
                    line.setPath([ms.start, ms.end]);
                    line.setMap(map);
                    label.innerHTML = Math.round(ms.distance() * 10) / 10 + 'ft';
                }
            }

            div.className = 'measurement-tool measure-off';

            var button = document.createElement('span');
            button.className = 'button';
            div.appendChild(button);

            var label = document.createElement('span');
            label.className = 'distance';
            div.appendChild(label);
            label.innerHTML = '0.0ft'

            google.maps.event.addDomListener(button, 'click', function (e) {
                if (ms.on) {
                    map.setOptions({ draggableCursor: null });
                    div.className = 'measurement-tool measure-off';
                    ms.clear();
                } else {
                    map.setOptions({ draggableCursor: 'crosshair' });
                    div.className = 'measurement-tool measure-on';
                    ms.clear();
                }

                ms.on = !ms.on;
                e.preventDefault();
            });

            this.distance = function () {
                if ((ms.start != null) && (ms.end != null)) {
                    return google.maps.geometry.spherical.computeDistanceBetween(ms.start, ms.end) * 3.28084;
                } else {
                    return 0;
                }
            }

            //this.__defineGetter__("distance", );

        }
    </script>
    <style type="text/css">
        .overlay
        {
            width: 150px;
            height: 150px;
            position: relative;
            z-index: 1000;
            cursor: pointer;
        }
    </style>
</head>
<body style="overflow: hidden; background: #777; margin: 0px;">
    <form id="form1" runat="server">
    <div id="map">
    </div>
    <div class="overlay" id="resizable-wrapper">
        <img id="sketch" />
    </div>
    <div style="display: none;">
        <canvas height="300" width="300" id="scanvas">
        </canvas>
    </div>
    </form>
</body>
</html>
