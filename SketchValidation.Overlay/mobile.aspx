﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="mobile.aspx.vb" Inherits="_mobile" %>

<!DOCTYPE html>
<html>
<head runat="server">
	<title>Sketch Validation - CourthouseUSA</title>
	<meta name="viewport" content="user-scalable=no,initial-scale=1.0,maximum-scale=1.0,width=device-width,height=device-height" />
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="default" />
	<meta name="format-detection" content="telephone=no">
	<link rel="apple-touch-icon" href="/App_Static/images/touch-icon.png" />
	<link rel="Stylesheet" href="/App_Static/themes/css/apple.css" />
	<link rel="Stylesheet" href="/App_Static/css/cloudsv.css" type="text/css" />
	<script type="text/javascript">
		var google = false;
		var smap, map;
		var theForm;
	</script>
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=geometry&sensor=false&region=US"></script>
	<script type="text/javascript" src="/App_Static/js/lib/zepto.min.js"></script>
	<%If DesignMode Then%>
	<script type="text/javascript" src="/App_Static/js/lib2/jqtouch.js"></script>
	<script type="text/javascript" src="/App_Static/js/lib2/jsrepeater.js"></script>
	<script type="text/javascript" src="/App_Static/js/lib2/json2.js"></script>
	<script type="text/javascript" src="/App_Static/js/lib2/iScroll.js"></script>
	<%Else%>
	<cc:JavaScript ID="JavaScript1" runat="server" IncludeFolder="~/App_Static/js/lib2" />
	<cc:JavaScript ID="JavaScript2" runat="server" IncludeFolder="~/App_Static/js/" />
	<%End If%>
	<script type="text/javascript">
		var $T = new $.jQTouch({
			preloadImages: [],
			pageChange: function (target) {
				activeScreenId = $(target).attr('id');
				adjustGISTimerForMap(activeScreenId);
			}
		});

		$(window).bind('load resize orientationchange', setScreenLayout);
		function setScreenLayout() {
			document.body.scrollTop = 0;
			document.body.scrollLeft = 0;

			var mb = 3 + 3 + 2; 			//body margin & border
			var wH = $(window).height(); //window height
			var wW = $(window).width(); 	//window width
			$(document.body).height(wH - mb);
			$(document.body).width(wW - mb);

			var h = wH - ($('header').height() + $('footer').height() + $('menu').height() + mb);
			var w = wW - mb;
			$('.wrapper').height(h);
			$('.wrapper').width(w);

			$('#search-map').height(h - $('.search-panel').height());
		}

		$(function () {
			$('input').blur(setScreenLayout);
			$('header').bind('click', setScreenLayout);
			$('footer').bind('click', setScreenLayout);
			loadSearchMap();

			if (!theForm) {
				theForm = document.dcs;
			}
		});

		$(document).bind('touchmove', function (e) {
			e.preventDefault();
		});

		function loadSearchMap() {
			var mapOptions = {
				zoom: 10,
				maxZoom: 20,
				minZoom: 6,
				center: new google.maps.LatLng(0, 0),
				mapTypeId: google.maps.MapTypeId.HYBRID,
				tilt: 0,
				streetViewControl: false
			};
			var mc = document.getElementById('search-map');
			smap = new google.maps.Map(mc, mapOptions);
		}
	</script>
</head>
<body>
	<form id="dcs" runat="server">
	<header>
		<div class="sv-logo">
		</div>
		<div class="cusa-logo">
		</div>
	</header>
	<menu>
		<table>
			<tr>
				<td><a class="menu-search">Search</a></td>
				<td><a class="menu-reports">Reports</a></td>
				<td><a class="menu-history">History</a></td>
				<td>
					<asp:LoginStatus runat="server" CssClass="menu-logout" />
				</td>
			</tr>
		</table>
	</menu>
	<div class="wrapper">
		<div id="jqt">
			<section class="screen" id="parcel-search">
				<div class="search-panel">
					<table>
						<tr>
							<td><span>Parcel ID:</span></td>
							<td>
								<input type="text" autocorrect="off" autocapitalize="off" maxlength="20" /></td>
							<td><span>Neighborhood:</span></td>
							<td>
								<input type="text" autocorrect="off" autocapitalize="off" maxlength="20" /></td>
						</tr>
						<tr>
							<td><span>Flag Status:</span></td>
							<td>
								<select>
									<option>All</option>
								</select></td>
							<td><span>Reviewed By:</span></td>
							<td>
								<select>
									<option>All</option>
								</select></td>
						</tr>
						<tr>
							<td colspan="4" class="search-buttons">
								<button>
									Search</button>
								<button>
									Clear</button>
							</td>
						</tr>
					</table>
				</div>
				<div id="search-map">
				</div>
			</section>
			<section class="screen" id="sketch-validation">
			</section>
			<section class="screen" id="reports">
			</section>
			<section class="screen" id="history">
			</section>
		</div>
	</div>
	<footer>
		<div class="dcs-footer">
			<a class="logo" href="http://www.datacloudsolutions.net/" target="_datacloud"></a>
			<div class="copyright">
				<strong>CAMA Cloud - <a href="http://www.datacloudsolutions.net/" target="_datacloud">
					Data Cloud Solutions, LLC.</a></strong><br />
				<span>Copyrights - 2012 - All rights reserved.</span>
			</div>
		</div>
		<div class="cusa-footer">
		</div>
	</footer>
	<script type="text/javascript">
		
		function __doPostBack(eventTarget, eventArgument) {
			if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
				theForm.__EVENTTARGET.value = eventTarget;
				theForm.__EVENTARGUMENT.value = eventArgument;
				theForm.submit();
			}
		}

	</script>
	</form>
</body>
</html>
