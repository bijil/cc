﻿Imports System.IO
Imports System.Threading
Imports System.Configuration
Imports CAMACloud.Data
Imports System.Data.SqlClient
Imports System.Text
Public Class BackupSchedule

#Region "Global Variables"
    Private Schedular As System.Threading.Timer
    Private GetBackupSchedular As System.Threading.Timer
    Private Shared AsyncOpDone As New System.Threading.AutoResetEvent(False)
    Dim connectionString As String = ConfigurationManager.ConnectionStrings("ControlDB").ConnectionString
    ''"Server=10.0.0.100;Database=CCMAIN;User Id=sa;Password=Cama@2014;"
    'Dim connectionString As String = "Data Source=TN01DEVDW7007;Initial Catalog=CCMAIN;Integrated Security=True"
#End Region

#Region "SqlConnection"
    Sub Execute(ByVal sql As String, Optional ByVal timeout As Integer = 1200)
        Dim con As New SqlConnection(connectionString)
        Dim cmd As New SqlCommand
        Try
            If con.State = ConnectionState.Closed Then
                con.Open()
            End If
            cmd = New SqlCommand(sql, con)
            cmd.CommandTimeout = 4000
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Dim msg As String = "Error on executing command - [" & sql & "]. Error Message is - " & ex.Message
            WriteToFile(msg)
        Finally
            con.Close()
        End Try
    End Sub
    Public Function GetDataTable(ByVal query As String, Optional ByVal timeout As Integer = 1200) As DataTable
        Dim con As New SqlConnection(connectionString)
        Dim cmd As New SqlCommand
        If con.State = ConnectionState.Closed Then
            con.Open()
        End If
        Dim ds As New DataSet()
        cmd.CommandType = CommandType.Text
        cmd = New SqlCommand(query, con)
        Dim dt As New DataTable
        Dim ada As New SqlDataAdapter(cmd)
        Try
            ada.Fill(dt)
            con.Close()
        Catch ex As Exception
            Dim msg As String = "Error on executing command - [" & cmd.CommandText & "]. Error Message is - " & ex.Message
            WriteToFile(msg)
        Finally
            con.Close()
        End Try
        Return dt
    End Function
    Public Function GetTopRow(ByVal query As String) As DataRow
        Dim dt As DataTable = GetDataTable(query)
        If dt.Rows.Count > 0 Then
            Return dt.Rows(0)
        Else
            Return Nothing
        End If
    End Function
    Public Function GetIntegerValue(ByVal query As String, Optional nullValue As Integer = 0, Optional ByVal timeout As Integer = 1200) As Integer
        Dim con As New SqlConnection(connectionString)
        Dim cmd As New SqlCommand
        If con.State = ConnectionState.Closed Then
            con.Open()
        End If
        cmd.CommandType = CommandType.Text
        cmd = New SqlCommand(query, con)
        cmd.CommandTimeout = timeout
        Try
            Dim res As Integer = Integer.Parse(cmd.ExecuteScalar().ToString())
            If IsNumeric(res) Then
                Return res
            Else
                Return nullValue
            End If
        Catch ex As SqlException
            Dim msg As String = "Error on executing command - [" & cmd.CommandText & "]. Error Message is - " & ex.Message
            Throw New Exception(msg)
        Finally
            con.Close()
        End Try
    End Function
#End Region

#Region "Events"
    Protected Overrides Sub OnStart(ByVal args() As String)
        Try
            DeleteOldServiceLog()
            Me.WriteToFile("Backup Database Service started at " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))
            Me.ScheduleService()
            Me.ScheduleGetBackup()
        Catch ex As Exception
            WriteToFile(ex.Message)
        End Try
    End Sub
    Protected Overrides Sub OnStop()
        Try
            Me.WriteToFile("Backup Database Service stopped at " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))
            Me.Schedular.Dispose()
            Me.GetBackupSchedular.Dispose()
        Catch ex As Exception
            WriteToFile(ex.Message)
        End Try
    End Sub
#End Region

#Region "ScheduleService"
    Public Sub ScheduleService()
        Try
            DeleteOldServiceLog()
            Schedular = New Timer(New TimerCallback(AddressOf SchedularCallback))
            Dim mode As String = ConfigurationManager.AppSettings("Mode").ToUpper()
            Me.WriteToFile((Convert.ToString("Backup Database Service Mode: ") & mode) + " {0}")
            'Set the Default Time.
            Dim scheduledTime As DateTime = DateTime.MinValue
            If mode.ToUpper() = ("DAILY").ToUpper Then
                'Get the Interval in Minutes from AppSettings.
                Dim updateInterval As Integer = Convert.ToInt32(ConfigurationManager.AppSettings("IntervalMinutes"))

                'Set the Scheduled Time by adding the Interval to Current Time.
                scheduledTime = DateTime.Now.AddSeconds(updateInterval)
                If DateTime.Now > scheduledTime Then
                    'If Scheduled Time is passed set Schedule for the next Interval.
                    scheduledTime = scheduledTime.AddSeconds(updateInterval)
                End If
            End If

            If mode.ToUpper() = ("INTERVAL").ToUpper Then
                'Get the Interval in Minutes from AppSettings.
                Dim updateInterval As Integer = Convert.ToInt32(ConfigurationManager.AppSettings("IntervalMinutes"))
                'Set the Scheduled Time by adding the Interval to Current Time.
                scheduledTime = DateTime.Now.AddMinutes(updateInterval)
                If DateTime.Now > scheduledTime Then
                    'If Scheduled Time is passed set Schedule for the next Interval.
                    scheduledTime = scheduledTime.AddMinutes(updateInterval)
                End If
            End If

            Dim timeSpan As TimeSpan = scheduledTime.Subtract(DateTime.Now)
            Dim nextSchedule As String = String.Format("{0} day(s) {1} hour(s) {2} minute(s) {3} seconds(s)", timeSpan.Days, timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds)

            Me.WriteToFile((Convert.ToString("Backup Database Service scheduled to run after: ") & nextSchedule) + " {0}")

            'Get the difference in Minutes between the Scheduled and Current Time.
            Dim dueTime As Integer = Convert.ToInt32(timeSpan.TotalMilliseconds)

            'Change the Timer's Due Time.
            Schedular.Change(dueTime, Timeout.Infinite)
        Catch ex As Exception
            WriteToFile("Backup Database Service Error on: {0} " + ex.Message + ex.StackTrace)
            'Stop the Windows Service.
        End Try
    End Sub

#End Region

#Region "SchedularCallback"
    Private Sub SchedularCallback(e As Object)
        Try
            Dim ThreadSetBackup As New Thread(AddressOf SetBackup)
            ThreadSetBackup.Start()
            Me.WriteToFile("Backup Database Service Service Log: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))
            Me.ScheduleService()
        Catch ex As Exception
            WriteToFile("Backup Database Service Schedular Callback Error on: {0} " + ex.Message)
        End Try
    End Sub

#End Region

#Region "SetBackup"
    Public Sub SetBackup()
        Try
            'Setting a Backup at every one hour
            Dim dtBackupScheduled As New DataTable
            Dim sqlQuery As String = String.Format("EXEC sp_AutoBackupDatabaseSchedule")
            Execute(sqlQuery, 4000)
        Catch ex As Exception
            WriteToFile("Set Backup Error on: {0} " + ex.Message)
        End Try
    End Sub
#End Region

#Region "ScheduleGetBackup"
    Public Sub ScheduleGetBackup()
        Try
            GetBackupSchedular = New Timer(New TimerCallback(AddressOf ScheduleGetBackupCallBack))
            Dim mode As String = ConfigurationManager.AppSettings("GetBackupMode").ToUpper()
            'Set the Default Time.
            Dim scheduledTime As DateTime = DateTime.MinValue

            If mode.ToUpper() = ("IntervalInSeconds").ToUpper Then
                'Get the Interval in Minutes from AppSettings.
                Dim updateInterval As Integer = Convert.ToInt32(ConfigurationManager.AppSettings("GetBackupIntervalSeconds"))

                'Set the Scheduled Time by adding the Interval to Current Time.
                scheduledTime = DateTime.Now.AddSeconds(updateInterval)
                If DateTime.Now > scheduledTime Then
                    'If Scheduled Time is passed set Schedule for the next Interval.
                    scheduledTime = scheduledTime.AddSeconds(updateInterval)
                End If
            End If

            If mode.ToUpper() = ("IntervalInMinutes").ToUpper Then
                'Get the Interval in Minutes from AppSettings.
                Dim updateInterval As Integer = Convert.ToInt32(ConfigurationManager.AppSettings("GetBackupIntervalMinutes"))

                'Set the Scheduled Time by adding the Interval to Current Time.
                scheduledTime = DateTime.Now.AddMinutes(updateInterval)
                If DateTime.Now > scheduledTime Then
                    'If Scheduled Time is passed set Schedule for the next Interval.
                    scheduledTime = scheduledTime.AddMinutes(updateInterval)
                End If
            End If

            Dim timeSpan As TimeSpan = scheduledTime.Subtract(DateTime.Now)

            Dim dueTime As Integer = Convert.ToInt32(timeSpan.TotalMilliseconds)

            'Change the Timer's Due Time.
            GetBackupSchedular.Change(dueTime, Timeout.Infinite)
        Catch ex As Exception
            'Stop the Multiple Backup service.
            WriteToFile("Get Backup Service Error on: {0} " + ex.Message + ex.StackTrace)
        End Try
    End Sub
#End Region

#Region "ScheduleGetBackupCallBack"
    Private Sub ScheduleGetBackupCallBack(e As Object)
        Try
            Dim ThreadGetBackup As New Thread(AddressOf GetBackup)
            Me.WriteToFile(Thread.CurrentThread.Name)
            ThreadGetBackup.Start()
            Me.ScheduleGetBackup()
        Catch ex As Exception
            WriteToFile("Get Backup Service CallBack Error on: {0} " + ex.Message)
        End Try
    End Sub
#End Region

#Region "GetBackup"
    Public Sub GetBackup()
        Try
            'Taking Backup at every one minuts
            Dim dtServerList As New DataTable
            Dim host As String
            Dim id As String
            Dim count As Integer = GetIntegerValue("SELECT COUNT(*) FROM ScheduledDatabaseBackup WHERE Status in('W')")
            If (count > 0) Then
                dtServerList = GetDataTable("SELECT Name,host FROM DatabaseServer")
                If dtServerList IsNot Nothing AndAlso dtServerList.Rows.Count > 0 Then
                    For Each dr As DataRow In dtServerList.Rows
                        host = dr("Host").ToString()
                        If GetDataTable(String.Format("SELECT * FROM ScheduledDatabaseBackup  WHERE Status='W'")).AsEnumerable().Select(Function(x) x.Item("HostName")).Contains(host) Then
                            Dim drScheduledBackup As DataRow = GetTopRow("SELECT TOP (1) * FROM ScheduledDatabaseBackup  WHERE  Status='W' AND HostName='" + host + "'")
                            If drScheduledBackup IsNot Nothing Then
                                id = drScheduledBackup("Id").ToString()
                                ThreadPool.QueueUserWorkItem(New System.Threading.WaitCallback(AddressOf Task), id)  ' Queue a task.
                                host = Nothing
                                id = Nothing
                            End If
                        End If
                    Next

                End If
            End If
        Catch ex As Exception
            WriteToFile("Get Backup Error on: {0} " + ex.Message + ex.StackTrace)
        End Try
    End Sub
#End Region

#Region "BackupTask"
    Sub Task(ByVal Arg As Object)
        Try
            System.Threading.Thread.Sleep(1000) ' Wait 1 seconds.
            Dim Id As String = CStr(Arg)
            Dim sql As String = "EXEC sp_GetScheduledBackup @Id = '" + Id + "'"
            Execute(sql, 6000)
            AsyncOpDone.Set()   ' Signal that the thread is done.
        Catch ex As Exception
            WriteToFile("BackupTask Error on: {0} " + ex.Message)
        End Try
    End Sub
#End Region

#Region "WriteToFile"
    Private Sub WriteToFile(text As String)
        If (text IsNot Nothing) Then
            Dim folderPath As String = "C:\BackupDatabaseServiceLog"
            If Not Directory.Exists(folderPath) Then
                Directory.CreateDirectory(folderPath)
            End If
            Dim path As String = "C:\BackupDatabaseServiceLog\BackupDatabaseServiceLog" & DateTime.Now.ToString("dd'_'MM'_'yyyy") & ".txt"
            Using writer As New StreamWriter(path, True)
                writer.WriteLine(String.Format(text, DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")))
                writer.Close()
            End Using
        End If
    End Sub
#End Region

#Region "DeleteOldServiceLog"
    Private Sub DeleteOldServiceLog()
        Dim folderPath As String = "C:\BackupDatabaseServiceLog"
        Dim files As String() = Directory.GetFiles(folderPath)
        For Each file As String In files
            Dim fi As New FileInfo(file)
            If fi.CreationTime < DateTime.Now.AddDays(-30) Then
                fi.Delete()
            End If
        Next
    End Sub
#End Region

End Class