﻿Imports CAMACloud.BusinessLogic.RemoteIntegration


Public Class FieldCategory

    Private Shared ReadOnly Property db As Database
        Get
            Return Database.Tenant
        End Get
    End Property

    Public Shared Sub Create(categoryName As String)
        Dim dr As DataRow = db.GetTopRow("SELECT * FROM FieldCategory WHERE Name = " + categoryName.ToSqlValue)
        If dr IsNot Nothing Then
            Throw New Exceptions.DuplicateEntryExpections
        End If
        db.Execute("INSERT INTO FieldCategory (Name, Ordinal) VALUES (" + categoryName.ToSqlValue + ", 9999)")
        ReOrderCategories()
        'NotificationMailer.SendNotificationToVendorCDS(LoginId, 1)
    End Sub

    Public Shared Sub AddFieldToCategory(catId As Integer, fieldId As Integer, Optional tableId As Integer = -1)
        Dim f As DataRow = db.GetTopRow("SELECT * FROM DataSourceField WHERE Id = " & fieldId)
        Dim t As DataRow = db.GetTopRow("SELECT * FROM DataSourceTable WHERE Id = " & f.GetInteger("TableId"))
        Dim c As DataRow = db.GetTopRow("SELECT * FROM FieldCategory WHERE Id = " & catId)
        Dim tc As DataRow = db.GetTopRow("SELECT TOP 1 * FROM DataSourceField WHERE CategoryId = " & catId)

        If tableId = -1 Then
            tableId = t.GetInteger("Id")
        End If
        Dim importType As String = t.GetString("ImportType")
        If c.GetBoolean("TabularData") Then
            If t.GetInteger("ImportType") <> 1 Then
                Throw New Exception("You cannot add a field from Parcel Data table to a category containing Auxiliary Data.")
            End If
            If tc IsNot Nothing AndAlso tc.GetInteger("TableId") <> tableId Then
                Try
                    If c.GetIntegerWithDefault("SourceTableId") <> tableId Then
                        Throw New Exception("Auxiliary Data mismatch. You cannot drop fields from different tables on to any category.")
                    End If
                Catch ex As Exception
                    Throw New Exception("Auxiliary Data mismatch. You cannot drop fields from different tables on to any category.")
                End Try
            End If
        Else
            If tc IsNot Nothing AndAlso t.GetInteger("ImportType") = 1 Then
                Throw New Exception("You cannot add a field from Auxiliary Data table to a category containing Parcel Data.")
            End If
        End If
        db.Execute("UPDATE DataSourceField SET CategoryId = {0}, CategoryOrdinal = 9999 WHERE Id = {1}".FormatString(catId, fieldId))
        If t.GetInteger("ImportType") = 1 Then
            db.Execute("UPDATE FieldCategory SET TabularData = 1, SourceTableId = " & tableId & ", SourceTableName = " + t.GetString("Name").ToSqlValue + "  WHERE Id = " & catId)
        Else
            db.Execute("UPDATE FieldCategory SET TabularData = 0, SourceTableId = NULL, SourceTableName = NULL  WHERE Id = " & catId)
        End If




        ReorderCategoryFields(catId)
    End Sub

    Public Shared Sub AddFieldToQRTab(fieldId As String)
        db.Execute("UPDATE DataSourceField SET IsFavorite = 1, FavoriteOrdinal = 9999 WHERE Id = {0}".FormatString(fieldId))
        ReorderQuickReviewFields()
    End Sub

    Public Shared Sub RemoveFieldFromQRTab(fieldId As String)
        db.Execute("UPDATE DataSourceField SET IsFavorite = 0, FavoriteOrdinal = -1 WHERE Id = {0}".FormatString(fieldId))
        If Database.Tenant.GetIntegerValue("SELECT IsCustomField FROM DataSourceField WHERE Id = " & fieldId) = 1 Then
            Database.Tenant.Execute("DELETE FROM DataSourceField WHERE Id = " & fieldId)
        End If
        ReorderQuickReviewFields()
    End Sub

    Public Shared Sub ReorderQuickReviewFields()
        Dim sql As String = "CREATE TABLE #sort (Sort INT IDENTITY(10,10),CID INT);" + vbNewLine
        sql += "INSERT INTO #sort SELECT Id FROM DataSourceField WHERE IsFavorite = 1 ORDER BY FavoriteOrdinal;" + vbNewLine
        sql += "UPDATE DataSourceField SET FavoriteOrdinal = Sort FROM #sort WHERE Id = CID;" + vbNewLine
        sql += "DROP TABLE #sort;" + vbNewLine

        Database.Tenant.Execute(sql)
    End Sub

    Shared Sub MoveQRField(fieldId As String, newIndex As String)
        db.Execute("UPDATE DataSourceField SET FavoriteOrdinal = {1} WHERE Id = {0}".FormatString(fieldId, newIndex))
        ReorderQuickReviewFields()
    End Sub

    Public Shared Sub RemoveField(fieldId As String, catId As String)
        db.Execute("UPDATE DataSourceField SET CategoryId = NULL, CategoryOrdinal = 0 WHERE Id = {0}".FormatString(fieldId))
        Dim tc As DataRow = db.GetTopRow("SELECT TOP 1 * FROM DataSourceField WHERE CategoryId = " & catId)
        If tc Is Nothing Then
            db.Execute("UPDATE FieldCategory SET TabularData = 0, SourceTableId = NULL, SourceTableName = NULL WHERE Id = " & catId)
        End If
        If Database.Tenant.GetIntegerValue("SELECT IsCustomField FROM DataSourceField WHERE Id = " & fieldId) = 1 Then
            Database.Tenant.Execute("DELETE FROM DataSourceField WHERE Id = " & fieldId)
        End If
        ReorderCategoryFields(catId)
    End Sub

    Public Shared Sub RemoveMultipleField(fieldId As String, catId As String)
        db.Execute("UPDATE DataSourceField SET CategoryId = NULL, CategoryOrdinal = 0 WHERE Id IN ({0})".FormatString(fieldId))
        Dim tc As DataRow = db.GetTopRow("SELECT TOP 1 * FROM DataSourceField WHERE CategoryId = " & catId)
        If tc Is Nothing Then
            db.Execute("UPDATE FieldCategory SET TabularData = 0, SourceTableId = NULL, SourceTableName = NULL WHERE Id = " & catId)
        End If

        ReorderCategoryFields(catId)
    End Sub

    Public Shared Sub ReorderCategoryFields(catId As Integer)
        db.Execute("EXEC cc_ReorderCategoryFields " & catId)
    End Sub

    Public Shared Sub ReOrderCategories()
        db.Execute("EXEC cc_ReorderCategories")
    End Sub

    Public Shared Sub MoveCategory(catId As Integer, newIndex As Integer)
        db.Execute("UPDATE FieldCategory SET Ordinal = {1} WHERE Id = {0}".FormatString(catId, newIndex))
        ReOrderCategories()
    End Sub

    Shared Sub MoveField(fieldId As String, newIndex As String)
        db.Execute("UPDATE DataSourceField SET CategoryOrdinal = {1} WHERE Id = {0}".FormatString(fieldId, newIndex))
        ReorderCategoryFields(db.GetIntegerValue("SELECT CategoryId FROM DataSourceField WHERE Id = {0}".FormatString(fieldId)))
    End Sub

    Shared Sub Rename(catId As String, newName As String)
        db.Execute("UPDATE FieldCategory SET Name = {1} WHERE Id = {0}".FormatString(catId, newName.ToSqlValue))
    End Sub

    Shared Sub Delete(catId As String)
        db.Execute("UPDATE DataSourceField SET CategoryId = NULL, CategoryOrdinal = 0 WHERE CategoryId = {0}".FormatString(catId))
        db.Execute("DELETE FROM FieldCategory WHERE Id = " & catId)
        ReOrderCategories()
    End Sub

    'Public Shared Function ExportAsXML() As String
    '    Dim doc = New XDocument(New XDeclaration("1.0", "utf-8", True))
    '    Dim root = <settings type="FieldCategory"></settings>
    '    doc.Add(root)
    '    _exportToXML(root)


    '    Return doc.ToString(SaveOptions.None)
    'End Function

    'Public Shared Sub GeneratePhotoProperties()

    'End Sub

    'Public Shared Sub _exportToXML(ByRef node As XElement, Optional parentCategoryId As Integer = 0)
    '    Dim filter As String = IIf(parentCategoryId = 0, "ParentCategoryId IS NULL", "ParentCategoryId = " & parentCategoryId)
    '    Dim categories As DataTable = db.GetDataTable("SELECT * FROM FieldCategory WHERE " + filter)
    '    Dim categoryDet As DataTable
    '    If parentCategoryId = 0 Then
    '        Dim photoRow As DataRow = categories.NewRow()
    '        photoRow("Name") = "_photo_meta_data"
    '        photoRow("SourceTableName") = "_photo_meta_data"
    '        photoRow("Id") = -2
    '        categories.Rows.Add(photoRow)
    '    End If
    '    For Each cr As DataRow In categories.Rows
    '        Dim cat = <FieldCategory></FieldCategory>
    '        Dim catId As String = cr.GetString("Id")
    '        Dim sDescription As String = cr.GetString("Description")
    '        Dim sHeaderInfo As String = cr.GetString("HeaderInfo")
    '        cat.@Ordinal = cr.GetString("Ordinal")
    '        cat.@Name = cr.GetString("Name")
    '        cat.@TabularData = cr.GetString("TabularData")

    '        cat.@SourceTableName = cr.GetString("SourceTableName")
    '        'cat.@ParentCategoryId = cr.GetString("ParentCategoryId")
    '        cat.@FilterFields = cr.GetString("FilterFields")

    '        If cr.GetString("ShowCategory") <> "1" Then
    '            cat.@Hidden = "True"
    '        End If

    '        cat.@SortExpression = cr.GetString("SortExpression")
    '        cat.@AllowCopy = cr.GetString("AllowCopy")

    '        cat.@AllowAddDelete = cr.GetString("AllowAddDelete")
    '        cat.@IsReadOnly = cr.GetString("IsReadOnly")
    '        cat.@AllowMassUpdate = cr.GetString("AllowMassUpdate")
    '        cat.@DoNotAllowEditExistingRecords = cr.GetString("DoNotAllowEditExistingRecords")

    '        If sDescription <> "" Then
    '            Dim xDesc = <Description></Description>
    '            xDesc.Value = sDescription
    '            cat.Add(xDesc)
    '        End If

    '        If sHeaderInfo <> "" Then
    '            Dim xHead = <HeaderInfo></HeaderInfo>
    '            xHead.Add(New XCData(vbNewLine + sHeaderInfo.Trim + vbNewLine))
    '            cat.Add(xHead)
    '        End If
    '        If cr.GetString("Name") = "_photo_meta_data" Then
    '            categoryDet = db.GetDataTable("SELECT * FROM DataSourceField WHERE SourceTable = '_photo_meta_data' ORDER BY CategoryOrdinal")
    '        Else
    '            categoryDet = db.GetDataTable("SELECT * FROM DataSourceField WHERE CategoryId = " & cr.GetInteger("Id") & " ORDER BY CategoryOrdinal")
    '        End If
    '        For Each fr As DataRow In categoryDet.Rows
    '            Dim f = <Field/>
    '            f.@CategoryOrdinal = fr.GetInteger("CategoryOrdinal")
    '            f.@SourceTable = fr.GetString("SourceTable")
    '            f.@Name = fr.GetString("Name")
    '            f.@IsReadOnly = fr.GetString("IsReadOnly")
    '            f.@DoNotShowOnDE = fr.GetString("DoNotShowOnDE")
    '            f.@DisplayLabel = fr.GetString("DisplayLabel")
    '            f.@AssignedName = fr.GetString("AssignedName")
    '            f.@CalculationExpression = fr.GetString("CalculationExpression")
    '            f.@ReadOnlyExpression = fr.GetString("ReadOnlyExpression")
    '            f.@VisibilityExpression = fr.GetString("VisibilityExpression")
    '            f.@LookupTable = fr.GetString("LookupTable")
    '            f.@IsFavorite = fr.GetString("IsFavorite")
    '            f.@FavoriteOrdinal = fr.GetString("FavoriteOrdinal")
    '            f.@DataType = fr.GetString("DataType")
    '            f.@IsRequired = fr.GetBoolean("IsRequired")
    '            f.@ShowOnGrid = fr.GetBoolean("ShowOnGrid")
    '            f.@DefaultValue = fr.GetString("DefaultValue")
    '            f.@SortType = If(fr.GetString("SortType"), "0")
    '            f.@IsMassUpdateField = IIf(fr.GetString("IsMassUpdateField") = "", False, fr.GetString("IsMassUpdateField"))
    '            f.@IsUniqueInSiblings = fr.GetBoolean("IsUniqueInSiblings")
    '            f.@LookupShowNullValue = fr.GetBoolean("LookupShowNullValue")
    '            f.@LookupQuery = fr.GetString("LookupQuery")
    '            cat.Add(f)
    '        Next
    '        _exportToXML(cat, catId)
    '        node.Add(cat)
    '    Next
    'End Sub

    'Public Shared Sub ImportCategoriesFromStream(settings As XDocument, Optional isFullSettingsImport As Boolean = False)
    '    Try
    '        Dim root As XElement
    '        If isFullSettingsImport = False Then
    '            root = settings.Descendants("settings").First()
    '        Else
    '            root = settings.Descendants("FieldSettings").First()
    '        End If


    '        'Validate
    '        For Each cat In root.Descendants("FieldCategory")
    '            If cat.@TabularData.ToLower = "true" Then
    '                If cat.@SourceTableName.Trim = "" Then
    '                    Throw New Exception("One or more FieldCategory with TabularData=true does not have SourceTableName defined.")
    '                End If
    '            End If
    '            For Each field In cat.Descendants("Field")

    '            Next
    '        Next

    '        db.Execute("TRUNCATE TABLE FieldCategory; ")
    '        db.Execute("UPDATE DataSourceField SET CategoryId = NULL, CategoryOrdinal = 0;")
    '        _importCategory(root)


    '        db.Execute("UPDATE DataSourceField SET IsRequired = 0 WHERE CategoryId IS NULL AND IsRequired = 1;")
    '        db.Execute("UPDATE DataSourceField SET IsRequired = 0 WHERE DoNotShowOnDE = 1 AND IsRequired = 1;")
    '        db.Execute("UPDATE DataSourceField SET IsRequired = 0 WHERE IsReadonly = 1 AND IsRequired = 1;")
    '    Catch ex As Xml.XmlException
    '        Throw New Exception("The uploaded file contains invalid XML. Import process has been aborted.")
    '    Catch ex As Exception
    '        Throw New Exception("You have uploaded an invalid Field Category settings file - " + ex.Message, ex)
    '    End Try
    'End Sub

    'Private Shared Sub _importCategory(node As XElement, Optional parentCategoryId As Integer = 0)
    '    For Each cat In node.Elements("FieldCategory")
    '        Dim sqlFCTemplate As String = "INSERT INTO FieldCategory (Name, Description, HeaderInfo, Ordinal, ShowCategory, TabularData, SourceTableId, SourceTableName,SortExpression,AllowCopy, AllowMassUpdate, DoNotAllowEditExistingRecords) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7},{8},{9}, {10}, {11}); SELECT CAST(@@IDENTITY AS INT)"
    '        Dim showCategory As String = "1"
    '        Dim isReadOnly As String = "0"
    '        Dim allowAddDelete As String = "1"
    '        Dim SortExpression As String = ""
    '        Dim allowCopy As String = "0"
    '        Dim allowMassUpdate As String = "0"
    '        Dim DoNotAllowEditExistingRecords As String = "0"

    '        Dim headerInfo As String = "", description As String = ""
    '        If cat.@Hidden IsNot Nothing AndAlso cat.@Hidden.ToLower = "true" Then
    '            showCategory = "0"
    '        End If
    '        If cat.@IsReadOnly IsNot Nothing AndAlso cat.@IsReadOnly.ToLower = "true" Then
    '            isReadOnly = "1"
    '        End If
    '        If cat.@AllowAddDelete IsNot Nothing AndAlso cat.@AllowAddDelete.ToLower = "false" Then
    '            allowAddDelete = "0"
    '        End If

    '        If cat.@AllowMassUpdate IsNot Nothing AndAlso cat.@AllowMassUpdate.ToLower = "true" Then
    '            allowMassUpdate = "1"
    '        End If

    '        If cat.@DoNotAllowEditExistingRecords IsNot Nothing AndAlso cat.@DoNotAllowEditExistingRecords.ToLower = "true" Then
    '            DoNotAllowEditExistingRecords = "1"
    '        End If

    '        Dim sourceTableId As String = ""
    '        If cat.@SourceTableName <> "" Then
    '            sourceTableId = Database.Tenant.GetIntegerValue("SELECT Id FROM DataSourceTable WHERE Name = {0}".SqlFormatString(cat.@SourceTableName))
    '        End If
    '        If cat.Elements("HeaderInfo").Count() > 0 Then
    '            headerInfo = cat.Elements("HeaderInfo").First.Value
    '        End If
    '        If cat.Elements("Description").Count() > 0 Then
    '            description = cat.Elements("Description").First.Value
    '        End If

    '        If cat.@SortExpression IsNot Nothing AndAlso cat.@SortExpression <> "" Then
    '            SortExpression = cat.@SortExpression
    '        End If

    '        If cat.@AllowCopy IsNot Nothing AndAlso cat.@AllowCopy <> "" Then
    '            allowCopy = cat.@AllowCopy
    '        End If

    '        Dim catName As String = cat.@Name
    '        Dim catId As Integer
    '        If catName = "_photo_meta_data" Then
    '            catId = -2
    '        Else
    '            catId = db.GetIntegerValue(sqlFCTemplate.SqlFormat(True, cat.@Name, description, headerInfo, cat.@Ordinal, showCategory, cat.@TabularData, sourceTableId, cat.@SourceTableName, SortExpression, allowCopy, allowMassUpdate, DoNotAllowEditExistingRecords))
    '        End If



    '        Dim fieldOrdinal As Integer = 10
    '        For Each field In cat.Elements("Field")
    '            Dim isRequired As String = "0"
    '            If field.@IsRequired IsNot Nothing AndAlso field.@IsRequired.ToLower = "true" Then
    '                isRequired = "1"
    '            End If
    '            Dim showOnGrid As String = "0"
    '            If field.@ShowOnGrid IsNot Nothing AndAlso field.@ShowOnGrid.ToLower = "true" Then
    '                showOnGrid = "1"
    '            End If

    '            Dim sortType As String = 0
    '            If field.@SortType IsNot Nothing Then
    '                sortType = field.@SortType
    '            End If

    '            Dim IsMassUpdateField As Boolean = False
    '            If field.@IsMassUpdateField IsNot Nothing Then
    '                IsMassUpdateField = field.@IsMassUpdateField
    '            End If

    '            Dim isUniqueInSiblings As String = "0"
    '            If field.@IsUniqueInSiblings IsNot Nothing AndAlso field.@IsUniqueInSiblings.ToLower = "true" Then
    '                isUniqueInSiblings = "1"
    '            End If

    '            Dim lookupShowNullValue As String = "0"
    '            If field.@LookupShowNullValue IsNot Nothing AndAlso field.@LookupShowNullValue.ToLower = "true" Then
    '                lookupShowNullValue = "1"
    '            End If
    '            'LookupShowNullValue()

    '            Dim defaultValue As String = field.@DefaultValue

    '            Dim checkQuery As String = "SELECT COUNT(*) FROM DataSourceField  WHERE SourceTable = {0} AND Name = {1};".SqlFormatString(field.@SourceTable, field.@Name)
    '            If db.GetIntegerValue(checkQuery) = 0 Then
    '                AddCustomField(field.@Name, "", catId)
    '            End If

    '            Dim updateCatId As String = catId
    '            If catId = -2 Then
    '                updateCatId = 0
    '            End If
    '            Dim sqlFUTemplate As String = "UPDATE DataSourceField SET CategoryId = {0}, IsReadOnly = {3}, DoNotShowOnDE = {4}, DisplayLabel = {5}, AssignedName = {6}, CalculationExpression = {7}, LookupTable = {8}, IsFavorite = {9}, FavoriteOrdinal = {10}, DataType = {11}, CategoryOrdinal = {12}, IsRequired = {13}, ShowOnGrid = {14}, DefaultValue = {15},SortType={16},IsMassUpdateField={17}, ReadOnlyExpression = {18}, VisibilityExpression = {19},IsUniqueInSiblings={20},LookupShowNullValue={21}, LookupQuery={22} WHERE SourceTable = {1} AND Name = {2};"
    '            db.Execute(sqlFUTemplate.SqlFormat(True, updateCatId, field.@SourceTable, field.@Name, field.@IsReadOnly, field.@DoNotShowOnDE, field.@DisplayLabel, field.@AssignedName, field.@CalculationExpression, field.@LookupTable, field.@IsFavorite, field.@FavoriteOrdinal, field.@DataType, IIf(field.@CategoryOrdinal = 0, fieldOrdinal, field.@CategoryOrdinal), isRequired, showOnGrid, defaultValue, sortType, IsMassUpdateField, field.@ReadOnlyExpression, field.@VisibilityExpression, isUniqueInSiblings, lookupShowNullValue, field.@LookupQuery))
    '            fieldOrdinal += 10
    '        Next
    '        If catId > 0 Then
    '            Dim sqlFUTemplate2 As String = "UPDATE FieldCategory SET ParentCategoryId = {1}, FilterFields = {2}, IsReadOnly = {3}, AllowAddDelete = {4}  WHERE Id = {0};"
    '            If parentCategoryId = 0 Then
    '                db.Execute(sqlFUTemplate2.FormatString(catId, "NULL", "NULL", isReadOnly, allowAddDelete))
    '            Else
    '                db.Execute(sqlFUTemplate2.SqlFormatString(catId, parentCategoryId, cat.@FilterFields, isReadOnly, allowAddDelete))
    '            End If
    '            _importCategory(cat, catId)
    '        End If

    '        db.Execute("UPDATE DataSourceField SET CategoryId = NULL WHERE CategoryId = 0")
    '    Next
    'End Sub

    Public Shared Function GetFieldCategories(Optional doNotincludeMultiGrid As Boolean = False) As DataTable
        Dim fc As DataTable = Database.Tenant.GetDataTable("select fc.*,fc.SourceTableName As SourceTable,dt.AutoInsertProperties,dt.YearPartitionField,fc.DisplayType as Type from FieldCategory fc left outer join DataSourceTable dt on dt.name=fc.SourceTableName")
        For Each fr As DataRow In fc.Rows
            If fr.GetIntegerWithDefault("SourceTableId") <> 0 Then
                Dim dsf As DataTable = Database.Tenant.GetDataTable("SELECT DISTINCT SourceTable FROM DataSourceField WHERE CategoryId = {0}".SqlFormatString(fr.GetInteger("Id")))
                For Each dsr As DataRow In dsf.Rows
                    Dim tname As String = dsr.GetString("SourceTable")
                    If tname <> "" Then
                        Dim ma_target As String = tname
                        DataSource.FindTargetTable(Database.Tenant, tname, ma_target)
                        fr("SourceTable") = ma_target
                        Exit For
                    End If
                Next
            End If
        Next
        Return fc
    End Function

    Public Shared Function GetSortExpression(ByVal tableName As String, Optional tablePrefix As String = "")
        Dim sortExpression = Database.Tenant.GetStringValue("SELECT SortExpression FROM FieldCategory WHERE SourceTableName={0}".SqlFormatString(tableName))
        Dim sortFormula As String = ""
        Dim SField As String
        If sortExpression <> "" AndAlso sortExpression IsNot Nothing Then
        	
        	If sortExpression.Contains("CASE") And sortExpression.Contains("WHEN") Then
        		Dim arr As String() = sortExpression.Split({" "c}, StringSplitOptions.RemoveEmptyEntries)
				sortExpression = String.Join(" ", arr)
        		Dim fields() As String=sortExpression.Split(" ")
        		SField=fields(1).trim()
        		sortFormula=sortExpression.Replace(SField,tablePrefix+SField)
        	Else
        		If sortExpression.Contains(",") Then
                	Dim sortingfields As String() = sortExpression.Split(New Char() {","c})
                	For Each fields In sortingfields
                    	If sortFormula <> "" Then
                        	sortFormula += ","
                    	End If
                    	sortFormula += tablePrefix + fields
                	Next
            	Else
                	sortFormula = tablePrefix + sortExpression
            	End If
            	
        	End If
        	If (sortFormula.ToLower().EndsWith("asc") = False AndAlso sortFormula.ToLower().EndsWith("desc") = False) Then
                	sortFormula = " ORDER BY " + sortFormula + " ASC"
            Else
                	sortFormula = " ORDER BY " + sortFormula
            End If
   
        Else
            sortFormula = ""
        End If
        Return sortFormula
    End Function

    Public Shared Function GetTableListForCategories() As DataTable
        DataSource.CategorizeDataTables()

        Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT Id, Name, ImportType, CAST(1 AS BIT) As Selected FROM DataSourceTable WHERE ImportType IN (0, 1, 3) ORDER BY ImportType, Name")
        For Each dr As DataRow In dt.Rows
            Dim tableId As String = dr.GetString("Id")
            Dim tableName As String = dr.GetString("Name")
            Dim targetTableName As String = tableName
            DataSource.FindTargetTable(Database.Tenant, tableName, targetTableName)
            If targetTableName = tableName Then
                Dim connectedTableNames As String = tableName
                getAssociateTables(tableId, connectedTableNames)
                dr("Name") = connectedTableNames
            Else
                dr("Selected") = False
            End If
            Select Case dr.GetInteger("ImportType")
                Case 0
                Case 1
                    dr("Name") = "[AUX] " + dr.GetString("Name")
                Case 3
                    dr("Name") = "[NBHD] " + dr.GetString("Name")
            End Select
        Next
        Dim target As DataTable = dt.Clone()
        dt.Select("Selected = 1").CopyToDataTable(target, LoadOption.OverwriteChanges)
        Return target
    End Function

    Private Shared Sub getAssociateTables(tableId As Integer, ByRef connectedTableNames As String)
        Dim sql As String = "SELECT t.Id, t.Name FROM DataSourceRelationships r INNER JOIN DataSourceTable p ON r.ParentTableId = p.Id INNER JOIN DataSourceTable t ON r.TableId = t.Id WHERE Relationship = 0 AND p.Id = " & tableId
        For Each dr As DataRow In Database.Tenant.GetDataTable(sql).Rows
            If connectedTableNames <> "" Then
                connectedTableNames += ", "
            End If
            connectedTableNames += dr.GetString("Name")
            getAssociateTables(dr.GetInteger("Id"), connectedTableNames)
        Next
    End Sub

    Public Shared Function GetFields(Optional table As String = "") As DataTable
        Dim fields As DataTable = Database.Tenant.GetDataTable("SELECT f.*, t.ImportType As ImportTypeId FROM dbo.DataSourceField f LEFT OUTER JOIN DataSourceTable t ON f.TableId = t.Id WHERE (f.ExclusiveForSS IS NULL OR f.ExclusiveForSS=0) " + IIf(table = "", "", " AND t.name in('" + table + "') ") + " ORDER BY t.ImportType, f.Name ")
        Dim targetTables As New Dictionary(Of String, String)
        For Each dr As DataRow In fields.Rows
            Dim tableName As String = dr.GetString("SourceTable")
            If tableName <> "" Then
                If targetTables.ContainsKey(tableName) Then
                    Dim targetTableName As String = targetTables(tableName)
                    If targetTableName = Database.Tenant.Application.ParcelTable Then
                        dr("SourceTable") = DBNull.Value
                    Else
                        dr("SourceTable") = targetTableName
                    End If
                Else
                    Dim targetTableName As String = ""
                    DataSource.FindTargetTable(Database.Tenant, tableName, targetTableName)
                    If targetTableName = Database.Tenant.Application.ParcelTable Then
                        dr("SourceTable") = DBNull.Value
                    Else
                        dr("SourceTable") = targetTableName
                    End If
                    targetTables.Add(tableName, targetTableName)
                End If
            End If

            If tableName = "_photo_meta_data" Then
                dr("CategoryId") = -2
            End If
        Next

        fields.Columns("DisplayLabel").ColumnName = "Label"
        fields.Columns("CategoryOrdinal").ColumnName = "Serial"
        fields.Columns("IsReadOnly").ColumnName = "ReadOnly"
        fields.Columns("IsCalculated").ColumnName = "Calculated"
        fields.Columns("DoNotShowOnDE").ColumnName = "DoNotShow"
        fields.Columns("DataType").ColumnName = "InputType"
        fields.Columns("IsFavorite").ColumnName = "QuickReview"
        Return fields
    End Function

    Public Shared Sub AddCustomField(fieldName As String, calculation As String, categoryId As Integer, ByRef sourceTable As String)

        Dim isCalculated As Boolean = False
        Dim isReadOnly As Boolean = False
        Dim tableId As String = ""
        'Dim sourceTable = ""

        If calculation.Trim() <> "" Then
            isCalculated = True
            isReadOnly = True
            CreateCustomField(categoryId, fieldName, tableId, sourceTable, True)
        Else
            CreateCustomField(categoryId, fieldName, tableId, sourceTable, True)
        End If

        If categoryId = -1 Then

            Dim sql As String = ""
            If calculation = "" Then
                sql = "INSERT INTO DataSourceField (IsCustomField, Name, AssignedName, DisplayLabel, CalculationExpression, IsCalculated, DataType, IsFavorite, FavoriteOrdinal, SourceOrdinal,IsReadOnly) VALUES (1, {0}, {0}, {0}, NULL, {1}, 1, 1, 9999, 0,{2})".SqlFormatString(fieldName, isCalculated, isReadOnly)
            Else
                sql = "INSERT INTO DataSourceField (IsCustomField, Name, AssignedName, DisplayLabel, CalculationExpression, IsCalculated, DataType, IsFavorite, FavoriteOrdinal, SourceOrdinal,IsReadOnly) VALUES (1, {0}, {0}, {0}, {1}, {2}, 1, 1, 9999, 0,{3})".SqlFormatString(fieldName, calculation, isCalculated, isReadOnly)
            End If
            Database.Tenant.Execute(sql)
            ReorderQuickReviewFields()
        ElseIf categoryId = -2 Then
            Dim checkQuery As String = "SELECT id FROM DataSourcetable WHERE name = '_photo_meta_data'"
            Dim photoMetaTableID = db.GetIntegerValue(checkQuery)
            If photoMetaTableID = 0 Then
                photoMetaTableID = db.GetIntegerValue("INSERT INTO DataSourceTable (Name, ImportType) VALUES ('_photo_meta_data', 4) SELECT @@IDENTITY AS photoMetaTableID")
            End If
            Dim sql As String = "INSERT INTO DataSourceField (Name, AssignedName, DisplayLabel, DataType, SourceOrdinal,TableId,SourceTable,IsReadOnly) VALUES ({0}, {0}, {0}, 1, 0,{1},{2},{3})".SqlFormatString(fieldName, photoMetaTableID, "_photo_meta_data", isReadOnly)
            Database.Tenant.Execute(sql)
        Else
            Dim sql As String = "INSERT INTO DataSourceField (IsCustomField, Name, AssignedName, DisplayLabel, CalculationExpression, IsCalculated, DataType, CategoryId, CategoryOrdinal, SourceOrdinal,TableId,SourceTable,IsReadOnly) VALUES (1, {0}, {0}, {0}, {1}, {2}, 1, {3}, 9999, 0,{4},{5},{6})".SqlFormatString(fieldName, calculation, isCalculated, categoryId, tableId, sourceTable, isReadOnly)
            Database.Tenant.Execute(sql)
            ReorderCategoryFields(categoryId)
        End If

    End Sub

    Public Shared Function getSourceTable(ByVal categoryId As Integer, ByRef tableId As String, ByRef sourceTable As String)
        Dim row As DataRow = Database.Tenant.GetTopRow("SELECT TOP 1 t.Name,t.Id FROM DataSourceTable t INNER JOIN DataSourceField f ON f.TableId=t.Id WHERE f.CategoryId={0}".SqlFormatString(categoryId))

        If IsNothing(row) = False Then
            tableId = row.GetString("Id")
            sourceTable = row.GetString("Name")
        End If
        If sourceTable <> "" Then
            tableId = Database.Tenant.GetIntegerValue("SELECT Id FROM DataSourceTable WHERE Name={0}".SqlFormatString(sourceTable))
        ElseIf tableId = "" Then
            sourceTable = Database.Tenant.Application.ParcelTable
            tableId = Database.Tenant.GetIntegerValue("SELECT Id FROM DataSourceTable WHERE Name={0}".SqlFormatString(sourceTable))
        End If
        Dim targetTable = DataSource.FindTargetTable(Database.Tenant, sourceTable)
        Return targetTable

    End Function

    Public Shared Sub CreateCustomField(categoryId As Integer, fieldName As String, ByRef tableId As String, ByRef sourceTable As String, Optional showAlert As Boolean = False)

        If categoryId <> -1 AndAlso categoryId <> -2 Then
            Dim table = getSourceTable(categoryId, tableId, sourceTable)

            Dim sqlCheck As String = "SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ={0} AND COLUMN_NAME ={1}".SqlFormatString(table, fieldName)

            If (db.GetIntegerValue(sqlCheck) = 0) Then
                Dim alterSql As String = "ALTER TABLE [{0}] ADD {1} {2};".FormatString(table, fieldName, "VARCHAR(MAX)")
                db.Execute(alterSql)
            Else
                If showAlert Then
                    Throw New Exception("Field '" + sourceTable + "." + fieldName + "' already exists. CategoryID: " & categoryId & ". Please use another name for the custom field. ")
                End If
                Return
            End If
        End If

    End Sub
End Class
