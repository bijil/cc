﻿Public Class ApplicationConfigSettings

    'Public Shared Function ExportAsXML() As String
    '    Dim doc = New XDocument(New XDeclaration("1.0", "utf-8", True))
    '    Dim root = <settings type="ApplicationSettings"></settings>
    '    doc.Add(root)
    '    _exportXML(root)
    '    Return doc.ToString(SaveOptions.None)
    'End Function

    Public Shared Sub _exportXML(ByRef node As XElement)
        Dim parcelCat = <ParcelSettings/>

        parcelCat.@ParcelTable = Database.Tenant.Application.ParcelTable
        For i = 1 To 10
            Dim key = "KeyField" + i.ToString()
            parcelCat.SetAttributeValue(key, Database.Tenant.Application.PropertyValue(key))
        Next
        node.Add(parcelCat)

        Dim streetCategory = <StreetAddressSettings/>
        streetCategory.@StreetAddressTable = StreetAddress.GetStreetAddressTable(Database.Tenant)
        streetCategory.@StreetAddressField = StreetAddress.GetStreetAddressField(Database.Tenant)
        streetCategory.@StreetNumberField = StreetAddress.GetStreetNumberField(Database.Tenant)
        streetCategory.@StreetNameField = StreetAddress.GetStreetNameField(Database.Tenant)
        streetCategory.@StreetNameSuffixField = StreetAddress.GetStreetNameSuffixField(Database.Tenant)
        streetCategory.@StreetAddressFormula = StreetAddress.GetStreetAddressFormula(Database.Tenant)
        node.Add(streetCategory)

        Dim neighborhoodCategory = <NeighborhoodSettings/>
        neighborhoodCategory.@NeighborhoodTable = Database.Tenant.Application.NeighborhoodTable
        neighborhoodCategory.@NeighborhoodField = Database.Tenant.Application.NeighborhoodField
        neighborhoodCategory.@NeighborhoodNameField = Database.Tenant.Application.NeighborhoodNameField
        node.Add(neighborhoodCategory)

        Dim sketchCategory = <SketchSettings/>
        sketchCategory.@Table = ClientSettings.PropertyValue("SketchTable")
        sketchCategory.@Label = ClientSettings.PropertyValue("SketchLabel")
        sketchCategory.@CommandField = ClientSettings.PropertyValue("SketchCommandField")
        sketchCategory.@KeyField = ClientSettings.PropertyValue("SketchKeyField")
        sketchCategory.@VectorTable = ClientSettings.PropertyValue("SketchVectorTable")
        sketchCategory.@AreaField = ClientSettings.PropertyValue("SketchAreaField")
        sketchCategory.@Perimeter = ClientSettings.PropertyValue("SketchPerimeter")
        sketchCategory.@VectorLabelField = ClientSettings.PropertyValue("SketchVectorLabelField")
        sketchCategory.@VectorCommandField = ClientSettings.PropertyValue("SketchVectorCommandField")
        sketchCategory.@VectorConnectingField = ClientSettings.PropertyValue("SketchVectorConnectingField")
        sketchCategory.@SketchDecimals = ClientSettings.PropertyValue("SketchDecimals")
        node.Add(sketchCategory)

        Dim gisLayerCategory = <GISLayerSettings/>
        Dim dtLayer As DataTable = Database.Tenant.GetDataTable("SELECT Name,NameField,CustomField1,CustomField2,CustomField3 FROM GIS_ObjectLayer WHERE Id<>1 ")
        If dtLayer.Rows.Count > 0 Then
            For Each dr As DataRow In dtLayer.Rows
                Dim layer = <Layer/>
                layer.@Name = dr.GetString("Name")
                layer.@NameField = dr.GetString("NameField")
                layer.@CustomField1 = dr.GetString("CustomField1")
                layer.@CustomField2 = dr.GetString("CustomField2")
                layer.@CustomField3 = dr.GetString("CustomField3")
                gisLayerCategory.Add(layer)
            Next
        End If
        node.Add(gisLayerCategory)

        Dim bulkEditCategory = <BulkEditSettings/>
        bulkEditCategory.@BatchSize = If(ClientSettings.PropertyValue("BatchSize"), 100)
        bulkEditCategory.@TimeInterval = If(ClientSettings.PropertyValue("TimeInterval"), 15)
        node.Add(bulkEditCategory)

    End Sub

    'Public Shared Sub ImportSettingsFromStream(settings As XDocument, Optional isFullSettingsImport As Boolean = False)
    '    Try

    '        Dim root As XElement
    '        If isFullSettingsImport = False Then
    '            root = settings.Descendants("settings").First()
    '        Else
    '            root = settings.Descendants("ApplicationSettings").First()
    '        End If

    '        'Validate
    '        For Each cat In root.Descendants("ParcelSettings")
    '            If (cat.@ParcelTable <> "" AndAlso cat.@ParcelTable IsNot Nothing) Then
    '                If (cat.Attributes.Count < 2) Then
    '                    Throw New Exception("KeyFields are not defined in Parcel Settings. ")
    '                    Return
    '                Else
    '                    Parcel.importParcelSettingsFromXML(cat)
    '                End If
    '            Else
    '                Throw New Exception("ParcelTable is not defined in Parcel Settings. ")
    '                Return
    '            End If
    '        Next


    '        For Each cat In root.Descendants("StreetAddressSettings")
    '            Dim sfield = cat.@StreetAddressField
    '            If ((cat.@StreetAddressTable = "" Or cat.@StreetAddressTable Is Nothing) Or (cat.@StreetAddressField = "" Or cat.@StreetAddressField Is Nothing)) Then
    '                Throw New Exception("One or more required parameters are not defined in Street Address Settings.")
    '                Return
    '            Else
    '                StreetAddress.importStreetAddressSettings(cat)
    '            End If
    '        Next

    '        For Each cat In root.Descendants("NeighborhoodSettings")
    '            If ((cat.@NeighborhoodTable = "" Or cat.@NeighborhoodTable Is Nothing) Or (cat.@NeighborhoodField = "" Or cat.@NeighborhoodField Is Nothing) Or (cat.@NeighborhoodNameField = "" Or cat.@NeighborhoodNameField Is Nothing)) Then
    '                Throw New Exception("One or more required parameters are not defined in Neighborhood Settings.")
    '                Return
    '            Else
    '                Neighborhood.importNeighborhoodSettingsFromXML(cat)
    '            End If
    '        Next

    '        For Each cat In root.Descendants("SketchSettings")
    '            If ((cat.@Table = "" Or cat.@Table Is Nothing) Or (cat.@Label = "" Or cat.@Label Is Nothing) Or (cat.@CommandField = "" Or cat.@CommandField Is Nothing) Or (cat.@AreaField = "" Or cat.@AreaField Is Nothing) Or (cat.@Perimeter = "" Or cat.@Perimeter Is Nothing) Or (cat.@VectorTable = "" Or cat.@VectorTable Is Nothing) Or (cat.@VectorLabelField = "" Or cat.@VectorLabelField Is Nothing) Or (cat.@VectorCommandField = "" Or cat.@VectorCommandField Is Nothing) Or (cat.@VectorConnectingField = "" Or cat.@VectorConnectingField Is Nothing)) Then
    '                Throw New Exception("One or more required parameters are not defined in Sketch Settings.")
    '                Return
    '            Else
    '                importSketchSettings(cat)
    '            End If
    '        Next

    '        For Each cat In root.Descendants("GISLayerSettings")
    '            For Each layer In cat.Descendants("Layer")
    '                If ((layer.@Name = "" Or layer.@Name Is Nothing)) Then
    '                    Throw New Exception("One or more required parameters are not defined in GIS Layer Settings.")
    '                    Return
    '                Else
    '                    ImportGISLayerSettings(layer)
    '                End If
    '            Next
    '        Next

    '        For Each cat In root.Descendants("BulkEditSettings")
    '            If (cat.@BatchSize = "" Or cat.@BatchSize Is Nothing) Then
    '                cat.@BatchSize = 100
    '            End If
    '            If (cat.@TimeInterval = "" Or cat.@TimeInterval Is Nothing) Then
    '                cat.@TimeInterval = 15
    '            End If
    '            importBulkEditSettings(cat)
    '        Next

    '    Catch ex As System.Xml.XmlException
    '        Throw New Exception("The uploaded file contains invalid XML. Import process has been aborted.")
    '    Catch ex As Exception
    '        Throw New Exception("You have uploaded an invalid Application settings file - " + ex.Message)
    '    End Try
    'End Sub

    'Private Shared Sub importBulkEditSettings(cat As XElement)
    '    ClientSettings.PropertyValue("BatchSize") = cat.@BatchSize
    '    ClientSettings.PropertyValue("TimeInterval") = cat.@TimeInterval
    'End Sub

    'Private Shared Sub importSketchSettings(cat As XElement)
    '    ClientSettings.PropertyValue("SketchTable") = cat.@Table
    '    ClientSettings.PropertyValue("SketchLabel") = cat.@Label
    '    ClientSettings.PropertyValue("SketchCommandField") = cat.@CommandField
    '    ClientSettings.PropertyValue("SketchKeyField") = cat.@KeyField
    '    ClientSettings.PropertyValue("SketchVectorTable") = cat.@VectorTable
    '    ClientSettings.PropertyValue("SketchAreaField") = cat.@AreaField
    '    ClientSettings.PropertyValue("SketchPerimeter") = cat.@Perimeter
    '    ClientSettings.PropertyValue("SketchVectorLabelField") = cat.@VectorLabelField
    '    ClientSettings.PropertyValue("SketchVectorCommandField") = cat.@VectorCommandField
    '    ClientSettings.PropertyValue("SketchVectorConnectingField") = cat.@VectorConnectingField
    '    ClientSettings.PropertyValue("SketchDecimals") = cat.@SketchDecimals
    'End Sub

    'Private Shared Sub ImportGISLayerSettings(cat As XElement)
    '    Database.Tenant.Execute("UPDATE GIS_ObjectLayer SET NameField={0},CustomField1={1},CustomField2={2},CustomField3={3} WHERE Name={4}".SqlFormatString(cat.@NameField, cat.@CustomField1, cat.@CustomField2, cat.@CustomField3, cat.@Name))
    'End Sub
End Class
