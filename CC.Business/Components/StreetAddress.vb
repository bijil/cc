﻿Imports CAMACloud.BusinessLogic.RemoteIntegration
Imports System.Text.RegularExpressions
Public Class StreetAddress

    Public Shared Function UpdateStreetAddress(db As Database, Optional parcelId As String = "") As String

        Dim filterFormula = ""
        Dim streetAddressField = GetStreetAddressField(db)
        Dim streetNameField = GetStreetNameField(db)
        Dim streetNumberField = GetStreetNumberField(db)
        Dim streetNameSuffixField = GetStreetNameSuffixField(db)
        Dim streetUnitField = GetStreetUnitField(db)
        Dim streetAddressTable = GetStreetAddressTable(db)
        Dim streetAddressFilter = GetStreetAddressFormula(db)
        Dim streetAddressFilterValue = GetStreetAddressFilterValue(db)

        If streetAddressField Is Nothing Or streetAddressField = "" Or streetAddressTable Is Nothing Or streetAddressTable = "" Then
            Throw New Exception("StreetAddress configuration fields are not setup. Please use application configuration API or page to fill this information.")
        End If

        Dim updateColumns = ""
        If streetAddressField <> "" AndAlso IsNothing(streetAddressField) = False Then
            updateColumns = "StreetAddress=" + Regex.Replace(streetAddressField, "[A-Za-z][A-Za-z_0-9]{2,}", "ISNULL(LTRIM(RTRIM(s.$0 ))+ ' ', '')")
        End If
        'If streetAddressField <> "" AndAlso IsNothing(streetAddressField) = False Then
        '    Dim addrField() As String = streetAddressField.Split("+")
        '    Dim count As Integer
        '    Dim updateAddress = ""
        '    Dim IsFormula = False,
        '    For count = 0 To addrField.Length - 1
        '        If (updateAddress <> "") Then
        '            updateAddress += " + "
        '        End If
        '        If Not (addrField(count).Trim().StartsWith("'") AndAlso addrField(count).Trim().EndsWith("'")) Then
        '            updateAddress += "COALESCE(CONVERT(VARCHAR(100),s." + addrField(count) + ",128) ,'')"
        '        Else
        '            updateAddress += addrField(count)
        '       End if
        '    Next
        '    updateColumns = "StreetAddress= " + updateAddress

        'End If

        If streetNameField <> "" AndAlso IsNothing(streetNameField) = False Then
            If updateColumns <> "" Then
                updateColumns += ","
            End If
            updateColumns += "StreetName= CONVERT(VARCHAR(100),s." + streetNameField + ",128)"
        End If

        If streetNumberField <> "" AndAlso IsNothing(streetNumberField) = False Then
            If updateColumns <> "" Then
                updateColumns += ","
            End If
            updateColumns += "StreetNumber= CONVERT(VARCHAR(100),s." + streetNumberField  + ",128)"
        End If

        If streetNameSuffixField <> "" AndAlso IsNothing(streetNameSuffixField) = False Then
            If updateColumns <> "" Then
                updateColumns += ","
            End If
            updateColumns += "StreetNameSuffix= CONVERT(VARCHAR(100),s." + streetNameSuffixField + ",128)"
        End If

        If streetUnitField <> "" AndAlso IsNothing(streetUnitField) = False Then
            If updateColumns <> "" Then
                updateColumns += ","
            End If
            updateColumns += "StreetUnit= CONVERT(VARCHAR(100),s." + streetUnitField + ",128)"
        End If

        If streetAddressFilter <> "" AndAlso IsNothing(streetAddressFilter) = False Then
            filterFormula = " AND " + streetAddressFilter
        End If

        If parcelId <> "" Then
            filterFormula += " AND " + "Parcel.Id=" + parcelId.ToSqlValue
        End If
        Dim targetStreetAddressTable = DataSource.FindTargetTable(db, streetAddressTable)

        Dim updateSql = "UPDATE Parcel SET  " + updateColumns + "  FROM  " + targetStreetAddressTable + " s WHERE Parcel.Id = s.CC_ParcelId " + filterFormula
        Dim parcelsAffected As Integer = db.Execute(updateSql)
        'added for avoiding unwanted space in street address And solving QC search issue with street adddress-FD 8433
        Dim updateAddressSql = "UPDATE Parcel SET  StreetAddress=LTRIM(RTRIM(replace(replace(replace(StreetAddress,' ','<>'),'><',''),'<>',' ')))  FROM  " + targetStreetAddressTable + " s WHERE Parcel.Id = s.CC_ParcelId " + filterFormula
        Dim streetAddressAffected As Integer = db.Execute(updateAddressSql)

        Dim status As String = parcelsAffected & " parcels map with streetaddresstable."
        Return status

    End Function

    Public Shared Function GetStreetAddressField(db As Database) As String
        Return GetStreetTableSettings(db, "StreetAddressField", "StreetAddressField")
    End Function

    Public Shared Function GetStreetAddressTable(db As Database)
        Return GetStreetTableSettings(db, "StreetAddressTable", "StreetAddressTable")
    End Function

 
    Public Shared Function GetStreetAddressFormula(db As Database)
        Return GetStreetTableSettings(db, "StreetAddressFormula", "StreetAddressFilter")
    End Function

    Public Shared Function GetStreetNumberField(db As Database)
        Return GetStreetTableSettings(db, "StreetNumberField", "StreetNumberField")
    End Function

    Public Shared Function GetStreetNameField(db As Database)
        Return GetStreetTableSettings(db, "StreetNameField", "StreetNameField")
    End Function

    Public Shared Function GetStreetNameSuffixField(db As Database)
        Return GetStreetTableSettings(db, "StreetNameSuffixField", "StreetNameSuffixField")
    End Function
    Public Shared Function GetStreetUnitField(db As Database)
        Return GetStreetTableSettings(db, "StreetUnitField", "StreetUnitField")
    End Function

    Public Shared Function GetStreetAddressFilterValue(db As Database)
        Return GetStreetTableSettings(db, "streetAddressFilterValue", "streetAddressFilterValue")
    End Function

    Public Shared Function GetCityField(db As Database)
        Return GetStreetTableSettings(db, "CityField", "CityField")
    End Function

    Public Shared Function GetStreetTableSettings(db As Database, clientFieldName As String, applicationFieldName As String)

        Dim applicationSettings As New ClientApplication(db)
        Dim clientValue = ClientSettings.PropertyValue(clientFieldName)
        Dim appValue = applicationSettings.PropertyValue(applicationFieldName)
        If (clientValue <> "" AndAlso IsNothing(clientValue) = False) Then
            Return clientValue
        ElseIf (appValue <> "" AndAlso IsNothing(appValue) = False) Then
            Return appValue
        Else
            Return ""
        End If

    End Function

    Private Shared Function IsClientSettingsEmpty(ByVal name As String)

        If (name = "" Or IsNothing(name)) Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Shared Sub importStreetAddressSettings(cat As XElement)

        Dim appUpdateColumns = "UPDATE Application SET StreetAddressTable=" + cat.@StreetAddressTable.ToSqlValue + ",StreetAddressField=" + cat.@StreetAddressField.ToSqlValue + ",StreetAddressFilter=" + cat.@StreetAddressFormula.ToSqlValue
        Dim optionalColumns = ""
        'Street Address Table
        ClientSettings.PropertyValue("StreetAddressTable") = cat.@StreetAddressTable

        'Street Address Table
        ClientSettings.PropertyValue("StreetAddressField") = cat.@StreetAddressField

        'Street Address Filter

        ClientSettings.ClientSettUpdate("StreetAddressFormula", cat.@StreetAddressFormula)

        ''Street Number Field
        If cat.@StreetNumberField <> "" AndAlso cat.@StreetNumberField IsNot Nothing Then
            ClientSettings.PropertyValue("StreetNumberField") = cat.@StreetNumberField
            optionalColumns = "StreetNumberField=" + cat.@StreetNumberField.ToSqlValue
        Else
            ClientSettings.Delete("StreetNumberField")
        End If


        ''Street Name Field
        If cat.@StreetNameField <> "" AndAlso cat.@StreetNameField IsNot Nothing Then
            ClientSettings.PropertyValue("StreetNameField") = cat.@StreetNameField
            If optionalColumns <> "" Then optionalColumns += ","
            optionalColumns += "StreetNameField=" + cat.@StreetNameField.ToSqlValue
        Else
            ClientSettings.Delete("StreetNameField")
        End If

        ''Street Name Suffix Field
        If cat.@StreetNameSuffixField <> "" AndAlso cat.@StreetNameSuffixField IsNot Nothing Then
            ClientSettings.PropertyValue("StreetNameSuffixField") = cat.@StreetNameSuffixField
            If optionalColumns <> "" Then optionalColumns += ","
            optionalColumns += "StreetNameSuffixField=" + cat.@StreetNameSuffixField.ToSqlValue
        Else
            ClientSettings.Delete("StreetNameSuffixField")
        End If

        If optionalColumns <> "" Then appUpdateColumns += "," + optionalColumns
        Database.Tenant.Execute(appUpdateColumns)

    End Sub

End Class
