﻿Public Class Neighborhood

    Public Shared Function getNeighborhoodName(Optional type As String = "", Optional preppend As String = "", Optional append As String = "") As String
        'Dim retText As String = "nbhd_change_needed "
        'If type = "shortname" Then
        '    retText = "nbhd_short "
        'End If
        'If type = "lower" Then
        '    retText = "nbhd_lower "
        'End If
        'If preppend <> "" Then
        '    retText = preppend & " " & retText
        'End If
        'If append <> "" Then
        '    retText = retText & " " & append
        'End If
        Try

            Return Database.Tenant.GetStringValue("SELECT  dbo.getNeighborhoodName('{0}','{1}','{2}')".FormatString({type, preppend, append}))
        Catch ex As Exception

            Dim sql = <sql>
CREATE FUNCTION getNeighborhoodName (
    @type VARCHAR(MAX)
    ,@prep VARCHAR(MAX)
    ,@app VARCHAR(MAX)) 
    RETURNS VARCHAR(MAX) 
    AS 
    BEGIN 
    DECLARE @retVal VARCHAR(MAX)
    SET @retVal = 'Assignment Group'
    IF  @type='shortname'
         SET @retVal='Assgn Grp'
    ELSE IF @type='lower'
         SET @retVal='assignment group'
    IF @prep IS NOT NULL AND @prep!=''
        SET @retVal=@prep+' '+@retVal
    IF @app IS NOT NULL AND @app!=''
        SET @retVal=@retVal+' '+@app
   RETURN @retVal
END
            </sql>
            Database.Tenant.Execute(sql)
            Return Database.Tenant.GetStringValue("SELECT  dbo.getNeighborhoodName('{0}','{1}','{2}')".FormatString({type, preppend, append}))
        End Try
    End Function


    Public Shared Sub SetLastVisitedDate(db As Database, id As Integer, Optional eventDate As Date = #1/1/1900#)
        If eventDate = #1/1/1900# Then
            eventDate = Date.UtcNow
        End If

        db.Execute("UPDATE Neighborhood SET LastVisitedDate = {1} WHERE Id = {0}".SqlFormat(False, id, eventDate))
    End Sub

    Public Shared Sub CheckForCompletion(db As Database, neighborhoodId As Integer, loginId As String)
        Dim n As DataRow = Database.Tenant.GetTopRow("SELECT * FROM Neighborhood WHERE Id = " & neighborhoodId)
        If n IsNot Nothing Then
            If Not n.GetBoolean("Completed") Then
                Dim totalJobs As Integer = n.GetInteger("TotalAlerts") + n.GetInteger("TotalPriorities")
                Dim completedJobs As Integer = n.GetInteger("VisitedAlerts") + n.GetInteger("VisitedPriorities")
                If totalJobs > 0 AndAlso totalJobs = completedJobs Then
                    db.Execute("UPDATE Neighborhood SET Completed = 1 WHERE Id = {0}".SqlFormatString(neighborhoodId))
                    'TODO On Completion of neighborhood, what actions to be done.
                End If
            End If
        End If
    End Sub

    Public Shared Function RebuildNeighborhoods(db As Database, Optional relinkOnly As Boolean = False, Optional RelinkAll As Boolean = False, Optional listId As String = "", Optional LoginId As String = "", Optional ParcelAction As String = "") As String

        Dim relinkSql As String '= "IF OBJECT_ID('tempdb..#nmap') IS NOT NULL DROP TABLE #nmap; SELECT 	CC_ParcelId, n.Id AS NbhdId INTO #nmap FROM ParcelData pv INNER JOIN Parcel p ON pv.CC_ParcelId = p.Id LEFT OUTER JOIN Neighborhood n ON pv.{0} = n.Number WHERE 1 = 1"
        relinkSql = "IF OBJECT_ID('tempdb..#nmap') IS NOT NULL DROP TABLE #nmap; SELECT CC_ParcelId, n.Id AS NbhdId, n.Number NbhdNbr, p.NeighborhoodId pNbhdId INTO #nmap FROM ParcelData pv INNER JOIN Parcel p ON pv.CC_ParcelId = p.Id"

        If listId <> "" Then
            relinkSql += " INNER JOIN ProcessList li ON p.NeighborhoodId = li.ItemId AND li.ListID = {0}".SqlFormat(False, listId)
        End If
        relinkSql += " LEFT OUTER JOIN Neighborhood n ON pv.[{0}] = n.Number "

        Dim ignoreAdHocs As Boolean = False

        If db.Application.NeighborhoodField Is Nothing Then
            Throw New Exception("Neighborhood configuration fields are not setup. Please use application configuration API or page to fill this information.")
        End If

        Dim ignoreAdHocsFilter As String ' = "(p.NeighborhoodId IS NULL OR p.NeighborhoodId NOT IN (SELECT Id  FROM Neighborhood WHERE Number NOT IN (SELECT DISTINCT {0} FROM ParcelData WHERE {0} IS NOT NULL)))"
        ignoreAdHocsFilter = "LEFT OUTER JOIN (SELECT Id FROM Neighborhood nx LEFT OUTER JOIN (SELECT DISTINCT [{0}] As grp FROM ParcelData WHERE [{0}] IS NOT NULL) gx ON nx.Number = gx.grp WHERE gx.grp IS NULL) gnx ON p.NeighborhoodId = gnx.Id WHERE  (p.NeighborhoodId IS NULL OR gnx.Id IS NULL)"

        Dim numberField As String = db.Application.NeighborhoodField

        Dim adhocSetting = db.GetTopRow("SELECT * FROM ClientSettings WHERE Name = 'EnableAdhocAssignment'")
        If adhocSetting IsNot Nothing AndAlso (adhocSetting("Value") = "1" Or adhocSetting.GetString("Value").ToLower = "true") And (RelinkAll <> True) Then
            ignoreAdHocs = True
        End If



        Dim nameField As String = IIf(db.Application.NeighborhoodNameField Is Nothing, numberField, db.Application.NeighborhoodNameField)
        If Not db.DoesTableExists("NeighborhoodData") Then
            Dim createSql As String
            If nameField = numberField Then
                createSql = "CREATE Table NeighborhoodData (ROWUID INT IDENTITY PRIMARY KEY, ParentROWUID int, ClientROWUID BIGINT, ClientParentROWUID BIGINT, CC_LastUpdateTime DATETIME NOT NULL DEFAULT GETUTCDATE(), NbhdId INT, " + numberField + " VARCHAR(100))"

            Else
                createSql = "CREATE TABLE NeighborhoodData (ROWUID INT IDENTITY PRIMARY KEY, ParentROWUID int, ClientROWUID BIGINT, ClientParentROWUID BIGINT, CC_LastUpdateTime DATETIME NOT NULL DEFAULT GETUTCDATE(), NbhdId INT, " + numberField + " VARCHAR(100), " + nameField + " VARCHAR(100))"
            End If
            db.Execute(createSql)
            relinkOnly = False
        End If

        'CHangeover logic.. A new setting has been added per environment,  to allow Adhoc settings to be converted while preserving old method to work until.
        Dim newRelinkLogic = db.GetIntegerValueOrInvalid("SELECT CAST(Value AS INT) As IntValue FROM ClientSettings WHERE Name = 'UseOldRelinkMethod'")
        If newRelinkLogic <> 1 Then
            ignoreAdHocsFilter = " LEFT OUTER JOIN Neighborhood no ON p.NeighborhoodId = no.Id WHERE no.IsAdhoc IS NULL OR no.IsAdhoc = 0"
        End If

        If Not relinkOnly Then
            If Not ignoreAdHocs Then
                db.Execute("UPDATE Parcel SET NeighborhoodId = NULL")
                'db.DeleteTableRecordsWithIdentityReset("Assignment")
                'db.DeleteTableRecordsWithIdentityReset("Neighborhood")
                'Else
                '    db.Execute("UPDATE Parcel SET NeighborhoodId = NULL WHERE " + ignoreAdHocsFilter.FormatString(numberField))
            End If
        End If

        '

        Dim neighborhoodsAdded As Integer = 0
        'Dim insertSql As String = "INSERT INTO Neighborhood (Number, Name) SELECT DISTINCT [{0}], [{1}] FROM ParcelData WHERE [{0}] NOT IN (SELECT Number FROM Neighborhood) ORDER BY 1".FormatString(numberField, nameField)
        'neighborhoodsAdded = db.Execute(insertSql)
        'SELECT Id  FROM Neighborhood WHERE Number NOT IN (SELECT DISTINCT {0} FROM ParcelData WHERE {0} IS NOT NULL)

        Dim updateSql As String = relinkSql '"UPDATE Parcel SET NeighborhoodId = pn.NbhdId FROM (SELECT CC_ParcelId, n.Id AS NbhdId FROM ParcelData pv LEFT OUTER JOIN Neighborhood n ON pv.{0} = n.Number) pn WHERE Parcel.Id = pn.CC_ParcelId"
        If ignoreAdHocs Then
            updateSql += ignoreAdHocsFilter
        End If

        If listId = "" And LoginId <> "" Then
            updateSql += "; IF OBJECT_ID('tempdb..#nmap2') IS NOT NULL DROP TABLE #nmap2; SELECT ROW_NUMBER() OVER (Partition by CC_ParcelId ORDER BY CC_ParcelId) AS RN_NUM, * INTO #nmap2 FROM #nmap; INSERT INTO ParcelAuditTrail(ParcelId, LoginID, EventType, Description,ApplicationType) SELECT n.CC_ParcelId, '" + LoginId + "', 0, CONCAT('Parcel included in Relink Assignment Groups task, moved from ', np.number , ' to ', n.NbhdNbr),'Desktop' FROM #nmap2 n LEFT JOIN Neighborhood np ON n.pNbhdId= np.Id WHERE n.RN_NUM = 1 AND (n.NbhdNbr IS NOT NULL OR n.pNbhdId IS NOT NULL)"
        End If

        Dim updatePlSql As String = ""
        Dim PauditEntry As String = ""
        Dim lId As String = ""

        If ParcelAction <> "" Then
            Dim ParcelActions As String() = ParcelAction.Split(","c)

            For i As Integer = 0 To ParcelActions.Length - 1
                Dim Paction As String = ParcelActions(i)
                Select Case i
                    Case 0
                        If Paction = "1" Then
                            updatePlSql += " [Priority] = 0,"
                            PauditEntry += "Priority,"
                        End If
                    Case 1
                        If Paction = "1" Then
                            updatePlSql += " [FieldAlert] = NULL,"
                            PauditEntry += "Notes,"
                        End If
                    Case 2
                        If Paction = "1" Then
                            updatePlSql += " [FieldAlertType] = NULL,"
                            PauditEntry += "Alert from Field,"
                        End If
                    Case 3
                        If Paction = "1" Then
                            updatePlSql += " [ParcelAlert] = NULL,"
                            PauditEntry += "Alert from Office,"
                        End If
                    Case 4
                        lId = Paction
                End Select
            Next
            If PauditEntry <> "" Then
                If PauditEntry.EndsWith(",") Then
                    PauditEntry = PauditEntry.TrimEnd(",")
                End If
                updateSql += "; INSERT INTO ParcelAuditTrail (ParcelId, LoginID, EventType, Description, ApplicationType) SELECT n.CC_ParcelId, '" + lId + "', 0, 'Parcel " + PauditEntry + " has been reset.', 'Desktop' FROM #nmap n JOIN parcel p ON n.CC_ParcelId = p.Id"
            End If
        End If

        updateSql += "; select @@ROWCOUNT; UPDATE Parcel SET " + updatePlSql + "NeighborhoodId = pn.NbhdId FROM #nmap pn WHERE Parcel.Id = pn.CC_ParcelId; "

        Dim parcelsAffected As Integer
        Dim parcels As DataTable = db.GetDataTable(updateSql.FormatString(numberField))
        If (parcels.Rows.Count > 0) Then
            parcelsAffected = parcels.Rows(0)(0)
        End If

        db.Execute("EXEC cc_UpdateNeighborhoodStatistics NULL, 0")

        Dim status As String = parcelsAffected & " parcels relinked with Assignment Groups."
        If neighborhoodsAdded > 0 Then
            status = neighborhoodsAdded & " new neighborhoods added. " + status
        End If
        Return status
    End Function

    Public Shared Sub MarkAsNotCompleted(db As Database, parcelId As Integer)
        'TODO This part requires refining
        Dim NbhdId = db.GetIntegerValue("SELECT NeighborhoodId FROM Parcel WHERE Id=" + parcelId.ToString())
        If NbhdId <> 0 Then

            db.Execute("EXEC [cc_UpdateNeighborhoodStatistics] {0}".SqlFormatString(NbhdId))

        End If

    End Sub

    Public Shared Sub importNeighborhoodSettingsFromXML(cat As XElement)
        Dim updateColumns = "NeighborhoodTable=" + cat.@NeighborhoodTable.ToSqlValue + ", NeighborhoodField=" + cat.@NeighborhoodField.ToSqlValue + ", NeighborhoodNameField=" + cat.@NeighborhoodNameField.ToSqlValue
        Database.Tenant.Execute("UPDATE Application SET " + updateColumns)
    End Sub

    Public Shared Function DoesGroupExist(db As Database, name As String, ByRef parcelCount As Integer) As Boolean
        Dim nbhdId As Integer = db.GetIntegerValueOrInvalid("SELECT Id FROM Neighborhood WHERE LTRIM(RTRIM(Name)) = " + LTRIM(RTRIM(name.ToSqlValue)))
        If nbhdId = -1 Then
            Return False
        Else
            parcelCount = db.GetIntegerValue("SELECT COUNT(*) FROM Parcel WHERE NeighborhoodId = " & nbhdId)
            Return True
        End If
    End Function

    Public Shared Sub DeleteAllEmptyGroups(db As Database)
        db.Execute("DELETE FROM NeighborhoodData WHERE NbhdId IN (SELECT Id FROM Neighborhood WHERE TotalParcels=0)") '  AND LastComment IS NULL AND FirstVisitedDate IS NULL)")
        db.Execute("DELETE FROM Neighborhood WHERE TotalParcels=0") ' AND LastComment IS NULL AND FirstVisitedDate IS NULL")
    End Sub

End Class
