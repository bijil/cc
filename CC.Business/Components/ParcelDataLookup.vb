﻿Imports System.Web
Imports System.Text.RegularExpressions


Public Class ParcelDataLookup

    Public Shared Function GetLookupTableList() As DataTable
        Return Database.Tenant.GetDataTable("SELECT LookupName, COUNT(*) As ValueCount FROM ParcelDataLookup GROUP BY LookupName ORDER BY LookupName")
    End Function

    Public Shared Function ExportLookupData(db As Database, Optional trim As Boolean = False) As String
        Dim doc = New XDocument(New XDeclaration("1.0", "utf-8", True))
        Dim root = <settings type="LookupData"></settings>
        doc.Add(root)

        For Each dr As DataRow In db.GetDataTable("SELECT LookupName, COUNT(*) As ValueCount FROM ParcelDataLookup GROUP BY LookupName ORDER BY LookupName").Rows
            Dim lookupName As String = dr.GetString("LookupName")

            Dim lookup = <Lookup Name=""></Lookup>
            lookup.@Name = lookupName

            For Each cr As DataRow In db.GetDataTable("SELECT * FROM ParcelDataLookup WHERE LookupName = " + lookupName.ToSqlValue).Rows
                Dim item = <Item/>
                If trim Then
                    item.@Id = cr.GetString("IdValue").Trim
                    item.@Name = cr.GetString("NameValue").Trim
                    item.@Desc = cr.GetString("DescValue").Trim
                Else
                    item.@Id = cr.GetString("IdValue")
                    item.@Name = cr.GetString("NameValue")
                    item.@Desc = cr.GetString("DescValue")
                End If

                item.@Ordinal = cr.GetString("Ordinal")
                item.@colorcode = cr.GetString("colorcode")
                item.@AdditionalValue1 = cr.GetString("AdditionalValue1")
                item.@AdditionalValue2 = cr.GetString("AdditionalValue2")
                item.@NumericValue = cr.GetSingleWithDefault("Value", 0)
                lookup.Add(item)
            Next

            root.Add(lookup)
        Next



        Return doc.ToString(SaveOptions.None)
    End Function

    Public Shared Function ExportLookup(lookupName As String) As String
        Dim doc = New XDocument(New XDeclaration("1.0", "utf-8", True))
        Dim root = <settings type="LookupData"></settings>
        doc.Add(root)

        Dim lookup = <Lookup Name=""></Lookup>
        lookup.@Name = lookupName

        For Each cr As DataRow In Database.Tenant.GetDataTable("SELECT * FROM ParcelDataLookup WHERE LookupName = " + lookupName.ToSqlValue).Rows
            Dim item = <Item/>
            item.@Id = cr.GetString("IdValue")
            item.@Name = cr.GetString("NameValue")
            item.@Desc = cr.GetString("DescValue")
            item.@Ordinal = cr.GetString("Ordinal")
            item.@NumericValue = cr.GetSingleWithDefault("Value", 0)
            item.@colorcode = cr.GetString("colorcode")
            item.@AdditionalValue1 = cr.GetString("AdditionalValue1")
            item.@AdditionalValue2 = cr.GetString("AdditionalValue2")
            lookup.Add(item)
        Next

        root.Add(lookup)

        Return doc.ToString(SaveOptions.None)
    End Function

    Public Shared Function ImportLookupFromXml(stream As IO.Stream, Optional trim As Boolean = False, Optional ByRef lookupName As String = Nothing, Optional ByRef lookupsImported As Integer = 0, Optional ByRef apexLookupOption As String = "", Optional ByRef _fromSketchCodes As Integer = 0, Optional ByRef _sktSettings As XDocument = Nothing) As Integer
        Dim importCount As Integer = 0
        Dim lookupOrdinal As Integer = 0
        Dim lookupItemOrdinal As Integer = 0
        Try
            Database.Tenant.Execute("CREATE TABLE tempLookup (Id int,Reference varchar(50),LookupName varchar(50),IdValue varchar(MAX),NameValue varchar(MAX),DescValue varchar(MAX),Ordinal int,Description varchar(50),Value float,ColorCode varchar(25),AdditionalValue1 varchar(50),AdditionalValue2 varchar(50))")
            Dim settings As XDocument 
            If _fromSketchCodes = 1 Then
            	settings = _sktSettings
            Else
            	settings = XDocument.Load(stream)
            End If
            
            Dim root = settings.Descendants("settings").First
            'Validate  
            lookupOrdinal = 0
            For Each lookup In root.Elements("Lookup")
                If lookupOrdinal = 57 Then
                    lookupOrdinal = (lookupOrdinal + 1) - 1
                End If
                lookupOrdinal += 1
                lookupName = IIf(_fromSketchCodes = 1, lookupName, lookup.@Name)  
                If lookupName Is Nothing OrElse lookupName.Trim = String.Empty Then
                    Throw New Exception("Name of the lookup cannot be empty")
                End If
                Database.Tenant.Execute("INSERT INTO tempLookup (LookupName, IdValue, NameValue, DescValue,Ordinal, Value, colorcode, AdditionalValue1, AdditionalValue2) SELECT LookupName, IdValue, NameValue, DescValue,Ordinal, Value, colorcode, AdditionalValue1, AdditionalValue2 FROM ParcelDataLookup WHERE LookupName = " + lookupName.ToSqlValue)
                Database.Tenant.Execute("DELETE FROM ParcelDataLookup WHERE LookupName = " + lookupName.ToSqlValue)
                lookupItemOrdinal = 0
                For Each item In lookup.Elements("Item")
                    lookupItemOrdinal += 1
                    Dim id As String = item.@Id
                    Dim name As String = item.@Name
                    Dim desc As String = item.@Desc
                    Dim ordinal As String = item.@Ordinal
                    Dim addVal As String = item.@AdditionalValue1
                    Dim addValTwo As String = item.@AdditionalValue2
                    Dim numericValue As String = 0.0
                    If item.@NumericValue IsNot Nothing And item.@NumericValue <> "" Then
                        If Not Single.TryParse(item.@NumericValue, numericValue) Then
                            Throw New Exception("Unaccepted numeric value - " + item.@NumericValue)
                        End If
                    End If
                Next
            Next

            'Import
            For Each lookup In root.Elements("Lookup")
                lookupName = IIf(_fromSketchCodes = 1, lookupName, lookup.@Name)  
                If lookupName = "ext_wall_mat" Then
                    Dim a = lookupName
                End If
                For Each item In lookup.Elements("Item")
                    Dim id As String = item.@Id
                    Dim name As String = item.@Name
                    Dim desc As String = item.@Desc
                    Dim ordinal As String = item.@Ordinal
                    Dim addVal As String = IIf(apexLookupOption = "noAddVal", "", item.@AdditionalValue1)   
                    Dim addValTwo As String = item.@AdditionalValue2
                    Dim colorcode As String = item.@colorcode
                    If id = "Brick\ Brick Veneer\ Stucco Brd\ or Part Brick (&gt;=50%)" Then
                        Dim B = 2
                    End If
                    Dim numericValue As String = 0.0
                    If item.@NumericValue IsNot Nothing Then
                        Single.TryParse(item.@NumericValue, numericValue)
                    End If
                    If lookupName Is Nothing OrElse lookupName.Trim = String.Empty Then
                        Throw New Exception("Name of the lookup cannot be empty")
                    End If
                    If name Is Nothing OrElse name.Trim = "" Then
                        name = id
                    End If
                    If name Is Nothing OrElse name.Trim = "" Then
                        name = ""
                    End If
                    name = Regex.Replace(name, "[!@#$^&*\,]", "")
                    desc = Regex.Replace(desc, "[!@#$^&*\,]", "")
                    If addVal Is Nothing OrElse addVal.Trim = "" Then
                        addVal = ""
                    End If
                    If trim Then
                        id = id.Trim
                        name = name.Trim
                        desc = desc.Trim
                    End If
                    Database.Tenant.Execute("INSERT INTO ParcelDataLookup (LookupName, IdValue, NameValue, DescValue,Ordinal, Value, colorcode, AdditionalValue1, AdditionalValue2) VALUES ({0}, {1}, {2}, {3},{4}, {5}, {6}, {7}, {8});".SqlFormatString(lookupName, id, name, desc, ordinal, numericValue, colorcode, addVal, addValTwo))
                    importCount += 1
                Next
                lookupsImported += 1
            Next
            Database.Tenant.Execute("DROP TABLE tempLookup")
            Return importCount
        Catch ex As Xml.XmlException
            'Database.Tenant.Execute("INSERT INTO ParcelDataLookup (LookupName, IdValue, NameValue, DescValue,Ordinal, Value, colorcode, AdditionalValue1) SELECT LookupName, IdValue, NameValue, DescValue,Ordinal, Value, colorcode, AdditionalValue1 FROM tempLookup")
            If Database.Tenant.DoesTableExists("tempLookup") Then
        		Database.Tenant.Execute("DROP TABLE tempLookup")
        	End If	
            Throw New Exception("The uploaded file contains invalid XML. Import process has been aborted.")
        Catch ex As Exception
            Database.Tenant.Execute("INSERT INTO ParcelDataLookup (LookupName, IdValue, NameValue, DescValue,Ordinal, Value, colorcode, AdditionalValue1, AdditionalValue2) SELECT LookupName, IdValue, NameValue, DescValue,Ordinal, Value, colorcode, AdditionalValue1, AdditionalValue2 FROM tempLookup")
            If Database.Tenant.DoesTableExists("tempLookup") Then
        		Database.Tenant.Execute("DROP TABLE tempLookup")
        	End If	
            Throw New Exception("You have uploaded an invalid Lookup Data settings file.; Lookup# " & lookupOrdinal & "; Item# " & lookupItemOrdinal, ex)
        End Try
    End Function


    Public Shared Function ImportLookupFromCSV(stream As IO.Stream, lookupName As String, firstLineHeadings As Boolean, Optional trim As Boolean = False) As Integer
        Try
            Database.Tenant.Execute("CREATE TABLE tempLookup (Id int,Reference varchar(50),LookupName varchar(50),IdValue varchar(MAX),NameValue varchar(MAX),DescValue varchar(MAX),Ordinal int,Description varchar(50),Value float,ColorCode varchar(25),AdditionalValue1 varchar(50))")
            Dim importCount As Integer = 0
            Dim buffer(stream.Length - 1) As Byte
            stream.Read(buffer, 0, stream.Length)
            If stream.Length = 0 Then
                Throw New Exception("The uploaded file contains invalid CVS. Import process has been aborted.")
            End If
            Dim content As String = System.Text.Encoding.UTF8.GetString(buffer)
            If lookupName Is Nothing OrElse lookupName.Trim = String.Empty Then
                Throw New Exception("Name of the lookup cannot be empty")
            End If
            Database.Tenant.Execute("INSERT INTO tempLookup (LookupName, IdValue, NameValue, DescValue,Ordinal, Value, colorcode, AdditionalValue1) SELECT LookupName, IdValue, NameValue, DescValue,Ordinal, Value, colorcode, AdditionalValue1 FROM ParcelDataLookup WHERE LookupName = " + lookupName.ToSqlValue)
            Database.Tenant.Execute("DELETE FROM ParcelDataLookup WHERE LookupName = " + lookupName.ToSqlValue)
            Dim lineNo As Integer = 0
            Dim ordinalCount = 1
            For Each line As String In content.Trim().Split(vbLf)
                'If Not line = "Id,Name,Description fields\r" Then
                'Throw New Exception("Invalid column names, please revise your entry.")
                'End If
                If firstLineHeadings And lineNo = 0 Then
                    lineNo += 1
                    Continue For
                End If
                line = line.Trim
                Dim parts() As String = line.Split(",")
                Dim id As String = parts(0)
                Dim name As String = id
                Dim desc As String = name
                Dim ordinal As String = 1
                Dim numericVal As String = 0
                Dim addVal As String = ""
                Dim addValTwo As String = ""
            	Dim colorcode As String = ""
                If parts.Length > 1 Then
                    name = parts(1)
                    desc = name
                End If

                If parts.Length > 2 Then
                    desc = parts(2)
                End If
                If parts.Length > 3 Then
                    ordinal = parts(3)
                Else
                    ordinal = ordinalCount
                    ordinalCount += 1
                End If
                If parts.Length > 4 Then
                    numericVal = parts(4)
                  End If
                 If parts.Length > 5 Then
                    colorcode = parts(5)
                 End If
                If parts.Length > 6 Then
                    addVal = parts(6)
                End If
                If parts.Length > 7 Then
                    addValTwo = parts(7)
                End If

                name = Regex.Replace(name, "[!@#$^&*\,]", "")
                desc = Regex.Replace(desc, "[!@#$^&*\,]", "")
                If trim Then
                    id = id.Trim
                    name = name.Trim
                    desc = desc.Trim
                End If

                Database.Tenant.Execute("INSERT INTO ParcelDataLookup (LookupName, IdValue, NameValue, DescValue,Ordinal,Value, AdditionalValue1,AdditionalValue2,colorcode) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6},{7},{8});".SqlFormatString(lookupName, id, name, desc, ordinal, numericVal, addVal,addValTwo,colorcode))
                importCount += 1
                lineNo += 1
            Next

            Database.Tenant.Execute("DROP TABLE tempLookup")
            Return importCount
        Catch ex As Exception
            Database.Tenant.Execute("INSERT INTO ParcelDataLookup (LookupName, IdValue, NameValue, DescValue,Ordinal, Value, colorcode, AdditionalValue1) SELECT LookupName, IdValue, NameValue, DescValue,Ordinal, Value, colorcode, AdditionalValue1 FROM tempLookup")
            Database.Tenant.Execute("DROP TABLE tempLookup")
            ' Throw New Exception("You have uploaded an invalid Lookup Data settings file - " + ex.Message, ex)
             Throw New Exception("You have uploaded an invalid Lookup Data settings file")
        End Try
    End Function
    
    Public Shared Function ImportLookupFromSketchCodesXml(stream As IO.Stream, Optional trim As Boolean = False, Optional ByRef lookupName As String = Nothing, Optional ByRef lookupsImported As String = "noAddVal") As Integer
        Dim importCount As Integer = 0
        Dim lookupOrdinal As Integer = 0
        Dim lookupItemOrdinal As Integer = 0
        Dim lookupApexOrdinal As Integer = 0
        Try
            Dim settings As XDocument = XDocument.Load(stream)
            Dim rootName As String = settings.Root.Name.ToString()
            Dim rootSet = settings.Element("ApexSettings")
            If IsNothing(rootSet) AndAlso rootName.IndexOf("ApexSettings", StringComparison.OrdinalIgnoreCase) = -1 Then
                importCount = ImportLookupFromXml(stream, trim, lookupName, 0, lookupsImported, 1, settings)
            Else
                Database.Tenant.Execute("CREATE TABLE tempLookup (Id int,Reference varchar(50),LookupName varchar(50),IdValue varchar(MAX),NameValue varchar(MAX),DescValue varchar(MAX),Ordinal int,Description varchar(50),Value float,ColorCode varchar(25),AdditionalValue1 varchar(50),AdditionalValue2 varchar(50))")
                Dim root = settings.Root
                If Not IsNothing(rootSet) Then
                    settings.Descendants("ApexSettings").First
                End If

                'Validate  
                lookupOrdinal = 0
                lookupName = lookupName
                For Each lookup In root.Elements("areaCodeTable")
                    If lookupOrdinal = 57 Then
                        lookupOrdinal = (lookupOrdinal + 1) - 1
                    End If
                    lookupOrdinal += 1
                    If lookupName Is Nothing OrElse lookupName.Trim = String.Empty Then
                        Throw New Exception("Name of the lookup cannot be empty")
                    End If
                    Database.Tenant.Execute("INSERT INTO tempLookup (LookupName, IdValue, NameValue, DescValue,Ordinal, Value, colorcode, AdditionalValue1, AdditionalValue2) SELECT LookupName, IdValue, NameValue, DescValue,Ordinal, Value, colorcode, AdditionalValue1, AdditionalValue2 FROM ParcelDataLookup WHERE LookupName = " + lookupName.ToSqlValue)
                    Database.Tenant.Execute("DELETE FROM ParcelDataLookup WHERE LookupName = " + lookupName.ToSqlValue)
                    lookupItemOrdinal = 0
                    For Each areaCodeEntry In lookup.Elements("areaCodeEntry")
                        lookupItemOrdinal += 1
                        Dim apexCount = areaCodeEntry.Element("areaCodeEntry")
                        Dim addValOne = areaCodeEntry.@code
                        If IsNothing(apexCount) Then
                           ImportSketchCodesXmlHandler(areaCodeEntry, 0, 0, lookupName, lookupsImported, trim, addValOne)
                        Else
                            For Each item In areaCodeEntry.Elements("areaCodeEntry")
                                   ImportSketchCodesXmlHandler(item, 0, 0, lookupName, lookupsImported, trim, addValOne)
                              Next
                        End If
                    Next
                Next

                'Import
                For Each lookup In root.Elements("areaCodeTable")
	                For Each areaCodeEntry In lookup.Elements("areaCodeEntry")
	                	Dim apexCount = areaCodeEntry.Element("areaCodeEntry")
	                	Dim addValOne = areaCodeEntry.@code 
                        If IsNothing(apexCount) Then
	                	For each item In areaCodeEntry.Elements("areaCodeEntry")
                           If(item.@code <> "") Then
                            lookupApexOrdinal += 10
                            ImportSketchCodesXmlHandler(areaCodeEntry, 1, lookupApexOrdinal, lookupName, lookupsImported, trim, addValOne)
	                		importCount += 1
                            End If
                            Next
                         Else
	                		For Each item In areaCodeEntry.Elements("areaCodeEntry")
	                			If (item.@code <> "") Then
                                lookupApexOrdinal += 10
	                			ImportSketchCodesXmlHandler(item, 1, lookupApexOrdinal, lookupName, lookupsImported, trim, addValOne)
	                			importCount += 1
                                End If
                            Next
	                	End If
	                Next
	            Next
	            Database.Tenant.Execute("DROP TABLE tempLookup")
        	End If
        	Return importCount
        Catch ex As Xml.XmlException
        	If Database.Tenant.DoesTableExists("tempLookup") Then
        		Database.Tenant.Execute("DROP TABLE tempLookup")
        	End If	
            Throw New Exception("The uploaded file contains invalid XML. Import process has been aborted.")
        Catch ex As Exception
        	If Database.Tenant.DoesTableExists("tempLookup") Then
            	Database.Tenant.Execute("INSERT INTO ParcelDataLookup (LookupName, IdValue, NameValue, DescValue,Ordinal, Value, colorcode, AdditionalValue1, AdditionalValue2) SELECT LookupName, IdValue, NameValue, DescValue,Ordinal, Value, colorcode, AdditionalValue1, AdditionalValue2 FROM tempLookup")
        		Database.Tenant.Execute("DROP TABLE tempLookup")
        	End If	
            Throw New Exception("You have uploaded an invalid Lookup Data settings file.; Lookup# " & lookupOrdinal & "; Item# " & lookupItemOrdinal, ex)
        End Try
    End Function
    
    Public Shared Sub ImportSketchCodesXmlHandler(Item As XElement, Optional ByRef condition As Integer = 2, Optional ByRef lookupApexOrdinal As Integer = 0, Optional ByRef lookupName As String = Nothing, Optional ByRef lookupsImported As String = "noAddVal", Optional trim As Boolean = False, Optional ByRef addlValue As String = "")
    	If condition = 0 Then
    		Dim id As String = item.@code 
            Dim name As String = item.@areaName 
            Dim desc As String = item.@areaDescription
            Dim addVal As String = IIf(lookupsImported = "noAddVal", "", addlValue)
            Dim numericValue As String = 0.0
            If item.@multiplier IsNot Nothing And item.@multiplier <> "" Then
                If Not Single.TryParse(item.@multiplier, numericValue) Then
                    Throw New Exception("Unaccepted numeric value - " + item.@multiplier)
                End If
            End If
    	Else If condition = 1 Then
    		Dim id As String = item.@code 
            Dim name As String = item.@areaName 
            Dim desc As String = item.@areaDescription
            Dim ordinal As String = lookupApexOrdinal
            Dim addVal As String = IIf(lookupsImported = "noAddVal", "", addlValue) 
            Dim addValTwo As String = ""
            Dim colorcode As String = ""
            Dim numericValue As String = 0.0
            If item.@multiplier IsNot Nothing Then
                Single.TryParse(item.@multiplier, numericValue)
            End If
            If lookupName Is Nothing OrElse lookupName.Trim = String.Empty Then
                Throw New Exception("Name of the lookup cannot be empty")
            End If
            If name Is Nothing OrElse name.Trim = "" Then
                name = id
            End If
            If name Is Nothing OrElse name.Trim = "" Then
                name = ""
            End If
            If desc Is Nothing Then
            	desc = name
            End If
            name = Regex.Replace(name, "[!@#$^&*\,]", "")
            desc = Regex.Replace(desc, "[!@#$^&*\,]", "")
            If addVal Is Nothing OrElse addVal.Trim = "" Then
                addVal = ""
            End If
            If trim Then
                id = id.Trim
                name = name.Trim
                desc = desc.Trim
            End If
            Database.Tenant.Execute("INSERT INTO ParcelDataLookup (LookupName, IdValue, NameValue, DescValue,Ordinal, Value, colorcode, AdditionalValue1, AdditionalValue2) VALUES ({0}, {1}, {2}, {3},{4}, {5}, {6}, {7}, {8});".SqlFormatString(lookupName, id, name, desc, ordinal, numericValue, colorcode, addVal, addValTwo))
    	End If
    End Sub
    
    Public Shared Sub DeleteLookupTable(lookupName As String)
        If lookupName.IsNotEmpty Then
            Database.Tenant.Execute("UPDATE DataSourceField SET LookupTable = NULL, DataType = 1 WHERE LookupTable = " + lookupName.ToSqlValue)
            Database.Tenant.Execute("DELETE FROM ParcelDataLookup WHERE LookupName = " + lookupName.ToSqlValue)
        End If

    End Sub

    Public Shared Sub DownloadLookup(response As HttpResponse, lookupName As String)
        response.Clear()
        Dim o = Database.System.GetTopRow("SELECT * FROM Organization WHERE Id = " & HttpContext.Current.GetCAMASession().OrganizationId)
        Dim orgName As String = o.GetString("Name")
        Dim exportName As String = orgName.ToLower() + "-lookup-" + lookupName + "-" + Now.ToString("yyMM-dd-HHmmss") + ".xml"
        response.ContentType = "application/xml"
        response.WriteAsAttachment(CAMACloud.BusinessLogic.ParcelDataLookup.ExportLookup(lookupName), "" & exportName & "")
    End Sub

End Class
