﻿Public Class AutoDataEntryConfig

    Public Shared Sub ReOrderFields(ByVal configId As Integer, ByVal stageId As Integer)
        e_("EXEC [ADE_ReorderSteps] " & configId & ", " & stageId)
    End Sub

    Public Shared Sub MoveStep(ByVal configId As Integer, ByVal stageId As Integer, ByVal stepId As Integer, ByVal newIndex As Integer)
        e_("UPDATE ADE_Step SET Ordinal = {1} WHERE Id = {0}".FormatString(stepId, newIndex))
        ReOrderFields(configId, stageId)
    End Sub


End Class


