﻿Option Compare Text
Imports System.Text
Imports System.Text.RegularExpressions

Public Class SalesComparisonRule

	Private _id As Integer
	Private _isGroupRule As Boolean

    Private _compiledFormat As New CompiledFormat
    Private _compiledSql As String

    Const VARIABLE_NAME_PATTERN As String = "[A-Za-z][A-Za-z0-9_]+(?![\(\w@]))([\s\+\),\-\*\/]" ' "[A-Za-z][A-Za-z0-9_]+(?![\(\w])([\s\+\),\-\*\/])"

    Public Shared ExpressionKeywords As String() = New String() {"CASE", "WHEN", "THEN", "ELSE", "END", "COALESCE", "ROUND", "ABS", "SGN", "ISNULL"}

	Public Sub New(ruleId As Integer)
		Dim dr As DataRow = t_("SELECT * FROM cse.Rules WHERE Id = " & ruleId)
		If dr Is Nothing Then
			_id = -1
		Else
			_id = ruleId
            _Name = dr.GetString("Name")
            _SourceTable = dr.GetString("SourceTable")
            _FieldSpecification = dr.GetString("FieldSpec")
            _AggregateFunction = dr.GetString("AggregateFunction")
			_RuleFormula = dr.GetString("RuleFormula")
			_RankFormula = dr.GetString("RankFormula")
			_PercentFormulaIfPass = dr.GetString("PercentFormulaIfPass")
			_PercentFormulaIfFail = dr.GetString("PercentFormulaIfFail")
			_UseLookup = dr.GetBoolean("UseLookup")
			_UseRangeLookup = dr.GetBoolean("UseRangeLookup")
			_LookupName = dr.GetString("LookupName")
            _isGroupRule = dr.GetBoolean("IsGroupRule")
            _UseZeroIfNull = dr.GetBoolean("UseZeroIfNull")
		End If
	End Sub

	Public Sub New(name As String, groupRule As Boolean)
		_Name = name
		_isGroupRule = groupRule
	End Sub

	Public ReadOnly Property IsGroupRule As Boolean
		Get
			Return _isGroupRule
		End Get
	End Property

	Public ReadOnly Property ID As Integer
		Get
			Return _id
		End Get
	End Property

	Public ReadOnly Property CompiledSQL As String
		Get
            Return _compiledSql
		End Get
	End Property

    Private ReadOnly Property CompiledSpecs As CompiledFormat
        Get
            Return _compiledFormat
        End Get
    End Property

    Public Property Name As String
    Public Property SourceTable As String
    Public Property FieldSpecification As String
    Public Property AggregateFunction As String
	Public Property RuleFormula As String
	Public Property RankFormula As String
	Public Property PercentFormulaIfPass As String
	Public Property PercentFormulaIfFail As String
	Public Property UseLookup As Boolean
	Public Property UseRangeLookup As Boolean
	Public Property LookupName As String
    Public Property UseZeroIfNull As Boolean

	Public Function CreateRule(author As String) As Integer
		Dim sql As String = "INSERT INTO cse.Rules (IsGroupRule, Name, CreatedBy) VALUES ({0}, {1}, {2}); SELECT CAST(@@IDENTITY AS INT) AS NewId;".FormatString(IsGroupRule.GetHashCode.ToString, Name.ToSqlValue, author.ToSqlValue)
		_id = Database.Tenant.GetIntegerValue(sql)
		Return _id
	End Function

	Public Sub SaveRule(Optional author As String = Nothing)
		_saveRule(author)
	End Sub

    'Private Sub _compileRuleAsSql()
    '	Dim sql As String = ""
    '       Dim field, fieldSpec, rule, rank, percp, percf As String
    '	field = FieldSpecification
    '	rule = RuleFormula
    '	rank = RankFormula
    '	percp = PercentFormulaIfPass
    '	percf = PercentFormulaIfFail

    '	_parseFormula(field)
    '	_parseFormula(rank)
    '	_parseFormula(rule)
    '	_parseFormula(percp)
    '	_parseFormula(percf)



    '       Dim tableSpec As String = "ParcelData"
    '       If SourceTable <> "" Then
    '           tableSpec = DataSource.FindTargetTable(Database.Tenant, SourceTable)
    '       End If

    '       Dim dataQuery = " FROM " + tableSpec + " d INNER JOIN Parcel p ON p.Id = d.CC_ParcelId INNER JOIN NeighborhoodData n ON p.NeighborhoodId = n.NbhdId"



    '       Dim filterExp As String = " WHERE P.Id = "

    '	If UseLookup Then
    '           If AggregateFunction <> "" Then
    '               fieldSpec = AggregateFunction + "(Value)"
    '           Else
    '               fieldSpec = "Value"
    '           End If
    '           sql += "SELECT @SUBJ = " + fieldSpec + " FROM ParcelDataLookup WHERE IdValue = (SELECT " + field + dataQuery + filterExp + "@SubjectId) AND LookupName = '" + LookupName + "'" + vbNewLine
    '           sql += "SELECT @COMP = " + fieldSpec + " FROM ParcelDataLookup WHERE IdValue = (SELECT " + field + dataQuery + filterExp + "@CompId) AND LookupName = '" + LookupName + "'" + vbNewLine
    '       ElseIf UseRangeLookup Then
    '           If AggregateFunction <> "" Then
    '               fieldSpec = AggregateFunction + "(RangeValue)"
    '           Else
    '               fieldSpec = "RangeValue"
    '           End If
    '           sql += "SELECT @SUBJ = " + fieldSpec + " FROM RangeLookup WHERE (SELECT " + field + dataQuery + filterExp + "@SubjectId) BETWEEN ValueFrom AND ValueTo AND GroupName = '" + LookupName + "'" + vbNewLine
    '           sql += "SELECT @COMP = " + fieldSpec + " FROM RangeLookup WHERE (SELECT " + field + dataQuery + filterExp + "@CompId) BETWEEN ValueFrom AND ValueTo AND GroupName = '" + LookupName + "'" + vbNewLine
    '       Else
    '           If AggregateFunction <> "" Then
    '               field = AggregateFunction + "(CAST((" + field + ") AS FLOAT))"
    '           End If
    '           sql += "SELECT @SUBJ = " + field + dataQuery + filterExp + "@SubjectId" + vbNewLine
    '           sql += "SELECT @COMP = " + field + dataQuery + filterExp + "@CompId" + vbNewLine
    '	End If

    '	sql += "SET @RANK = CASE WHEN (@SUBJ IS NULL AND @COMP IS NULL) THEN 0 ELSE " + rank + " END" + vbNewLine
    '	sql += "SET @RULE = CASE WHEN " + rule + " THEN 1 WHEN (@SUBJ IS NULL AND @COMP IS NULL) THEN 1 ELSE 0 END" + vbNewLine
    '	sql += "SET @PERC = CASE WHEN (@SUBJ IS NULL AND @COMP IS NULL) THEN 1 WHEN @RULE = 1 THEN " + percp + " ELSE " + percf + " END" + vbNewLine
    '	_compiledSql = sql
    'End Sub


    'Private Sub _compileRuleAsSql()

    '    Dim testParcelId As Integer = Database.Tenant.GetIntegerValueOrInvalid("SELECT MIN(Id) FROM Parcel")

    '    Dim sql As String = ""
    '    Dim field, fieldSpec, rule, rank, percp, percf As String
    '    field = FieldSpecification
    '    rule = RuleFormula
    '    rank = RankFormula
    '    percp = PercentFormulaIfPass
    '    percf = PercentFormulaIfFail

    '    _parseFormula(field)
    '    _parseFormula(rank)
    '    _parseFormula(rule)
    '    _parseFormula(percp)
    '    _parseFormula(percf)



    '    Dim tableSpec As String = "ParcelData"
    '    If SourceTable <> "" Then
    '        tableSpec = DataSource.FindTargetTable(Database.Tenant, SourceTable)
    '    End If

    '    Dim dataQuery = " FROM " + tableSpec + " d INNER JOIN Parcel p ON p.Id = d.CC_ParcelId INNER JOIN NeighborhoodData n ON p.NeighborhoodId = n.NbhdId"

    '    Dim setFieldSpec = Function(f, ag)
    '                           If ag <> "" Then
    '                               Return ag + "(" + f + ")"
    '                           Else
    '                               Return f
    '                           End If
    '                       End Function

    '    Dim addFieldPrefix = Function(f, prefix)
    '                             Dim matches = Regex.Matches(f, "([']" + VARIABLE_NAME_PATTERN + "['])?", RegexOptions.IgnoreCase)
    '                             Dim stringEscapes As New Dictionary(Of String, String)
    '                             Dim escapeCount As Integer = 0
    '                             For Each m As Match In matches
    '                                 Dim stringValue As String = m.Groups(1).Value
    '                                 escapeCount += 1
    '                                 Dim escapeKey As String = "@@@" & escapeCount & "@@@"
    '                                 f = Replace(f, stringValue, "@@@" & escapeCount & "@@@")
    '                                 stringEscapes.Add(escapeKey, stringValue)
    '                             Next

    '                             For Each fname In evalFields()
    '                                 If UseZeroIfNull Then
    '                                     f = Replace(f, fname, "ISNULL(" + prefix + fname + ",0)")
    '                                 Else
    '                                     f = Replace(f, fname, prefix + fname)
    '                                 End If
    '                             Next

    '                             For Each key In stringEscapes.Keys
    '                                 f = Replace(f, key, stringEscapes(key))
    '                             Next

    '                             Return f
    '                         End Function

    '    If UseLookup Then
    '        fieldSpec = setFieldSpec("pdl.Value", AggregateFunction)
    '        dataQuery += " INNER JOIN ParcelDataLookup pdl ON " + addFieldPrefix(field, "d.") + " = pdl.IdValue AND pdl.LookupName = " + LookupName.ToSqlValue + " "
    '    ElseIf UseRangeLookup Then
    '        fieldSpec = setFieldSpec("rl.RangeValue", AggregateFunction)
    '        dataQuery += " INNER JOIN RangeLookup rl ON " + addFieldPrefix(field, "d.") + " BETWEEN rl.ValueFrom AND rl.ValueTo AND rl.GroupName = " + LookupName.ToSqlValue + " "
    '    Else
    '        fieldSpec = setFieldSpec("CAST(" + addFieldPrefix(field, "d.") + " AS FLOAT)", AggregateFunction)
    '    End If

    '    Dim filterExp As String = " WHERE P.Id = "

    '    sql += "SELECT @SUBJ = " + fieldSpec + dataQuery + filterExp + "@SubjectId" + vbNewLine
    '    sql += "SELECT @COMP = " + fieldSpec + dataQuery + filterExp + "@CompId" + vbNewLine

    '    Dim testSql As String = "SELECT " + fieldSpec + dataQuery + filterExp & testParcelId

    '    Database.Tenant.Execute(testSql)

    '    'If UseLookup Then
    '    '    If AggregateFunction <> "" Then
    '    '        fieldSpec = AggregateFunction + "(Value)"
    '    '    Else
    '    '        fieldSpec = "Value"
    '    '    End If
    '    '    sql += "SELECT @SUBJ = " + fieldSpec + " FROM ParcelDataLookup WHERE IdValue = (SELECT " + field + dataQuery + filterExp + "@SubjectId) AND LookupName = '" + LookupName + "'" + vbNewLine
    '    '    sql += "SELECT @COMP = " + fieldSpec + " FROM ParcelDataLookup WHERE IdValue = (SELECT " + field + dataQuery + filterExp + "@CompId) AND LookupName = '" + LookupName + "'" + vbNewLine
    '    'ElseIf UseRangeLookup Then
    '    '    If AggregateFunction <> "" Then
    '    '        fieldSpec = AggregateFunction + "(RangeValue)"
    '    '    Else
    '    '        fieldSpec = "RangeValue"
    '    '    End If
    '    '    sql += "SELECT @SUBJ = " + fieldSpec + " FROM RangeLookup WHERE (SELECT " + field + dataQuery + filterExp + "@SubjectId) BETWEEN ValueFrom AND ValueTo AND GroupName = '" + LookupName + "'" + vbNewLine
    '    '    sql += "SELECT @COMP = " + fieldSpec + " FROM RangeLookup WHERE (SELECT " + field + dataQuery + filterExp + "@CompId) BETWEEN ValueFrom AND ValueTo AND GroupName = '" + LookupName + "'" + vbNewLine
    '    'Else
    '    '    If AggregateFunction <> "" Then
    '    '        field = AggregateFunction + "(CAST((" + field + ") AS FLOAT))"
    '    '    End If
    '    '    sql += "SELECT @SUBJ = " + field + dataQuery + filterExp + "@SubjectId" + vbNewLine
    '    '    sql += "SELECT @COMP = " + field + dataQuery + filterExp + "@CompId" + vbNewLine
    '    'End If

    '    _compiledFormat.ValueQuery = ""

    '    sql += "SET @RANK = CASE WHEN (@SUBJ IS NULL AND @COMP IS NULL) THEN 0 ELSE " + rank + " END" + vbNewLine
    '    sql += "SET @RULE = CASE WHEN " + rule + " THEN 1 WHEN (@SUBJ IS NULL AND @COMP IS NULL) THEN 1 ELSE 0 END" + vbNewLine
    '    sql += "SET @PERC = CASE WHEN (@SUBJ IS NULL AND @COMP IS NULL) THEN 1 WHEN @RULE = 1 THEN " + percp + " ELSE " + percf + " END" + vbNewLine
    '    _compiledSql = sql
    '    _compiledFormat.SQL = sql
    'End Sub

    Private Sub _compileRuleAsSql()

        Dim testParcelId As Integer = Database.Tenant.GetIntegerValueOrInvalid("SELECT MIN(Id) FROM Parcel")

        Dim sql As String = ""
        Dim field, fieldSpec, rule, rank, percp, percf As String
        field = FieldSpecification
        rule = RuleFormula
        rank = RankFormula
        percp = PercentFormulaIfPass
        percf = PercentFormulaIfFail

        _parseFormula(field)
        _parseFormula(rank)
        _parseFormula(rule)
        _parseFormula(percp)
        _parseFormula(percf)



        Dim tableSpec As String = "ParcelData"
        If SourceTable <> "" Then
            tableSpec = DataSource.FindTargetTable(Database.Tenant, SourceTable)
        End If

        Dim dataQuery = " FROM " + tableSpec + " d"

        Dim setFieldSpec = Function(f, ag)
                               If ag <> "" AndAlso ag <> "INLINE" Then
                                   Return ag + "(" + f + ")"
                               Else
                                   Return f
                               End If
                           End Function

        Dim addFieldPrefix = Function(f, prefix)
                                 Dim matches = Regex.Matches(f, "([']" + VARIABLE_NAME_PATTERN + "['])?", RegexOptions.IgnoreCase)
                                 Dim stringEscapes As New Dictionary(Of String, String)
                                 Dim escapeCount As Integer = 0
                                 For Each m As Match In matches
                                     Dim stringValue As String = m.Groups(1).Value
                                     escapeCount += 1
                                     Dim escapeKey As String = "@@@" & escapeCount & "@@@"
                                     f = Replace(f, stringValue, "@@@" & escapeCount & "@@@")
                                     stringEscapes.Add(escapeKey, stringValue)
                                 Next

                                 For Each fname In evalFields()
                                     If UseZeroIfNull Then
                                         f = Replace(f, fname, "ISNULL(" + prefix + fname + ",0)")
                                     Else
                                         f = Replace(f, fname, prefix + fname)
                                     End If
                                 Next

                                 For Each key In stringEscapes.Keys
                                     f = Replace(f, key, stringEscapes(key))
                                 Next

                                 Return f
                             End Function

        If UseLookup Then
            fieldSpec = setFieldSpec("pdl.Value", AggregateFunction)
            dataQuery += " INNER JOIN ParcelDataLookup pdl ON " + addFieldPrefix(field, "d.") + " = pdl.IdValue AND pdl.LookupName = " + LookupName.ToSqlValue + " "
        ElseIf UseRangeLookup Then
            fieldSpec = setFieldSpec("rl.RangeValue", AggregateFunction)
            dataQuery += " INNER JOIN RangeLookup rl ON " + addFieldPrefix(field, "d.") + " BETWEEN rl.ValueFrom AND rl.ValueTo AND rl.GroupName = " + LookupName.ToSqlValue + " "
        Else
            fieldSpec = setFieldSpec("CAST(" + addFieldPrefix(field, "d.") + " AS FLOAT)", AggregateFunction)
        End If

        Dim filterExp As String = " WHERE d.CC_ParcelId = "

        sql += "SELECT @SUBJ = " + fieldSpec + dataQuery + filterExp + "@SubjectId" + vbNewLine
        sql += "SELECT @COMP = " + fieldSpec + dataQuery + filterExp + "@CompId" + vbNewLine

        Dim testSql As String = "SELECT " + fieldSpec + dataQuery + filterExp & testParcelId

        _compiledFormat.FieldSpec = fieldSpec
        _compiledFormat.TableSpec = dataQuery

        Database.Tenant.Execute(testSql)

        'If UseLookup Then
        '    If AggregateFunction <> "" Then
        '        fieldSpec = AggregateFunction + "(Value)"
        '    Else
        '        fieldSpec = "Value"
        '    End If
        '    sql += "SELECT @SUBJ = " + fieldSpec + " FROM ParcelDataLookup WHERE IdValue = (SELECT " + field + dataQuery + filterExp + "@SubjectId) AND LookupName = '" + LookupName + "'" + vbNewLine
        '    sql += "SELECT @COMP = " + fieldSpec + " FROM ParcelDataLookup WHERE IdValue = (SELECT " + field + dataQuery + filterExp + "@CompId) AND LookupName = '" + LookupName + "'" + vbNewLine
        'ElseIf UseRangeLookup Then
        '    If AggregateFunction <> "" Then
        '        fieldSpec = AggregateFunction + "(RangeValue)"
        '    Else
        '        fieldSpec = "RangeValue"
        '    End If
        '    sql += "SELECT @SUBJ = " + fieldSpec + " FROM RangeLookup WHERE (SELECT " + field + dataQuery + filterExp + "@SubjectId) BETWEEN ValueFrom AND ValueTo AND GroupName = '" + LookupName + "'" + vbNewLine
        '    sql += "SELECT @COMP = " + fieldSpec + " FROM RangeLookup WHERE (SELECT " + field + dataQuery + filterExp + "@CompId) BETWEEN ValueFrom AND ValueTo AND GroupName = '" + LookupName + "'" + vbNewLine
        'Else
        '    If AggregateFunction <> "" Then
        '        field = AggregateFunction + "(CAST((" + field + ") AS FLOAT))"
        '    End If
        '    sql += "SELECT @SUBJ = " + field + dataQuery + filterExp + "@SubjectId" + vbNewLine
        '    sql += "SELECT @COMP = " + field + dataQuery + filterExp + "@CompId" + vbNewLine
        'End If

        sql += "SET @RANK = CASE WHEN (@SUBJ IS NULL AND @COMP IS NULL) THEN 0 ELSE " + rank + " END" + vbNewLine
        sql += "SET @RULE = CASE WHEN " + rule + " THEN 1 WHEN (@SUBJ IS NULL AND @COMP IS NULL) THEN 1 ELSE 0 END" + vbNewLine
        sql += "SET @PERC = CASE WHEN (@SUBJ IS NULL AND @COMP IS NULL) THEN 1 WHEN @RULE = 1 THEN " + percp + " ELSE " + percf + " END" + vbNewLine
        _compiledSql = sql
        _compiledFormat.SQL = sql
    End Sub

    Private Function evalFields() As String()
        Dim fields As New List(Of String)
        'Dim formulas() As String = New String() {FieldSpecification, RankFormula, RuleFormula, PercentFormulaIfPass, PercentFormulaIfFail}
        Dim formulas() As String = New String() {FieldSpecification}
        For Each formula In formulas
            Dim matches = Regex.Matches(formula, "(" + VARIABLE_NAME_PATTERN + ")?", RegexOptions.IgnoreCase)
            For Each m As Match In matches
                For Each g As Group In m.Groups
                    For Each c As Capture In g.Captures

                    Next
                Next
                Dim field As String = m.Groups(1).Value
                If Not fields.Contains(field) AndAlso Not ExpressionKeywords.Contains(field.ToUpper) Then fields.Add(field)
            Next
        Next

        Return fields.ToArray
    End Function

    Private Sub _parseFormula(ByRef formula As String)
        If formula.ToLower.StartsWith("when") Then
            formula = "CASE " + formula + " END"
        End If

        Dim opt As RegexOptions = RegexOptions.IgnoreCase + RegexOptions.Multiline
        formula = Regex.Replace(formula, "lookup\(\s*([A-Z0-9_]*)\s*,\s*([A-Z0-9_]*)\s*\)", "cse.LOOKUPVALUEBYGROUP('$2', '$1', $1)", opt)
        formula = Regex.Replace(formula, "lookup\(\s*([A-Z0-9_]*)\s*\)", "cse.LOOKUPVALUE('$1', $1)", opt)
        formula = Regex.Replace(formula, "if\((.*?),(.*?),(.*?)\)", "CASE WHEN $1 THEN $2 ELSE $3 END", opt)
        formula = Regex.Replace(formula, "range\(\s*['""]([A-Z0-9_]*)['""]\s*,\s*([A-Z0-9_]*)\s*\)", "cse.RANGELOOKUP('$1', $2)", opt)
        formula = Regex.Replace(formula, "nullifzero\(\s*(.*?)\s*\)", "NULLIF($1, 0)", opt)
    End Sub

    Private Sub _saveRule(author As String)
        Try
            _compileRuleAsSql()
        Catch ex As Exception
            Throw New Exception("Error on evaluating rule '" + Name + "' - " + ex.Message)
        End Try


        Dim xs = <string>
UPDATE cse.Rules
SET
	Name = {0},
	FieldSpec = {1},
	RankFormula = {2},
	RuleFormula = {3},
	PercentFormulaIfPass = {4},
	PercentFormulaIfFail = {5},
	UseLookup = {6},
	UseRangeLookup = {7},
	LookupName = {8},
	CompiledSQL = {9},
    AggregateFunction = {10},
    SourceTable = {11},
    UseZeroIfNull = {12},
    FieldSpecCompiled = {13},
    TableSpecCompiled = {14}
WHERE Id = 
				 </string>
        Dim sql As String = xs.Value.Trim + " " & _id
        sql = sql.SqlFormat(True,
         Name,
         FieldSpecification,
         RankFormula,
         RuleFormula,
         PercentFormulaIfPass,
         PercentFormulaIfFail,
         UseLookup.GetHashCode,
         UseRangeLookup,
         LookupName,
         CompiledSQL,
         AggregateFunction,
         SourceTable,
         UseZeroIfNull.GetHashCode,
         _compiledFormat.FieldSpec,
         _compiledFormat.TableSpec
         )

        Database.Tenant.Execute(sql)
        If author.IsNotEmpty Then
            Database.Tenant.Execute("UPDATE cse.Rules SET LastModifiedBy = " + author.ToSqlValue + ", LastModifiedDate = GETUTCDATE() WHERE Id = " & ID)
        End If
    End Sub

    Private Class CompiledFormat
        Public Property SQL As String
        Public Property FieldSpec As String
        Public Property TableSpec As String
        Public Property RankFormula As String
        Public Property RuleFormula As String
        Public Property PercentFormulaIfPassed As String

    End Class

End Class
