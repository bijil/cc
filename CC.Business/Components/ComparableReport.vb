﻿Public Class ComparableReport

	Public Shared Sub ReOrderFields()
		e_("EXEC [cc_ReorderComparables]")
	End Sub

	Public Shared Sub MoveField(catId As Integer, newIndex As Integer)
		e_("UPDATE ComparableFields SET Ordinal = {1} WHERE Id = {0}".FormatString(catId, newIndex))
		ReOrderFields()
	End Sub

End Class
