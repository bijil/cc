﻿Imports CAMACloud.BusinessLogic.RemoteIntegration


Public Class CSEFieldCategory
    Private Shared ReadOnly Property db As Database
        Get
            Return Database.Tenant
        End Get
    End Property
    Public Shared Sub Create(categoryName As String)
        Dim dr As DataRow = db.GetTopRow("SELECT * FROM cse.FieldCategory WHERE category = " + categoryName.ToSqlValue)
        If dr IsNot Nothing Then
            Throw New Exceptions.DuplicateEntryExpections
        End If
        db.Execute("INSERT INTO cse.FieldCategory (category, Ordinal) VALUES (" + categoryName.ToSqlValue + ", 9999)")
        ReOrderCategories()
    End Sub
    Public Shared Sub saveeditfield(fieldid As Integer, label As String, datatye As Integer, lookuptable As String,Username As string)
    	
        db.Execute("update cse.ComparableFields set label={0},OutputType={1}, LookupTableOverride = {3} where id={2} ".FormatString(label.ToSqlValue, datatye, fieldid, lookuptable.ToSqlValue))
        
    End Sub
    Public Shared Function AddFieldToCategory(catId As Integer, fieldId As Integer, label As String,Username As string) As Integer
        Dim dr As DataRow = db.GetTopRow("SELECT DisplayLabel,DataType FROM DataSourceField WHERE id = " + fieldId.ToString)
        Dim name As String = dr("DisplayLabel").ToString
        Dim sql As String = "INSERT INTO cse.ComparableFields (CategoryId, FieldId, Label,ordinal,OutputType, FieldType) VALUES ({0}, {1}, {2},9999,{3}, 1) SELECT @@IDENTITY AS cseFID".FormatString(catId, fieldId, name.ToSqlValue, dr("Datatype"))
        Dim cseFID As Integer = db.GetIntegerValue(sql)
        
        Dim drC As DataRow = db.GetTopRow("SELECT Id, Category FROM cse.FieldCategory where Id = " + catId.ToString)
        Dim Cname As String = drC("Category").ToString()
        Dim Description As String = "Field ("+name+") has been added to Category ("+Cname.ToString()+")."
        SettingsAuditTrail.insertSettingsAuditTrail(Username,Description)
        
        'Dim f As DataRow = db.GetTopRow("SELECT * FROM DataSourceField WHERE Id = " & fieldId)
        'Dim t As DataRow = db.GetTopRow("SELECT * FROM DataSourceTable WHERE Id = " & f.GetInteger("TableId"))
        'Dim c As DataRow = db.GetTopRow("SELECT * FROM FieldCategory WHERE Id = " & catId)
        'Dim tc As DataRow = db.GetTopRow("SELECT TOP 1 * FROM DataSourceField WHERE CategoryId = " & catId)

        'If tableId = -1 Then
        '    tableId = t.GetInteger("Id")
        'End If
        'Dim importType As String = t.GetString("ImportType")
        'If c.GetBoolean("TabularData") Then
        '    If t.GetInteger("ImportType") <> 1 Then
        '        Throw New Exception("You cannot add a field from Parcel Data table to a category containing Auxiliary Data.")
        '    End If
        '    If tc IsNot Nothing AndAlso tc.GetInteger("TableId") <> tableId Then
        '        Try
        '            If c.GetIntegerWithDefault("SourceTableId") <> tableId Then
        '                Throw New Exception("Auxiliary Data mismatch. You cannot drop fields from different tables on to any category.")
        '            End If
        '        Catch ex As Exception
        '            Throw New Exception("Auxiliary Data mismatch. You cannot drop fields from different tables on to any category.")
        '        End Try
        '    End If
        'Else
        '    If tc IsNot Nothing AndAlso t.GetInteger("ImportType") = 1 Then
        '        Throw New Exception("You cannot add a field from Auxiliary Data table to a category containing Parcel Data.")
        '    End If
        'End If
        'db.Execute("UPDATE DataSourceField SET CategoryId = {0}, CategoryOrdinal = 9999 WHERE Id = {1}".FormatString(catId, fieldId))
        'If t.GetInteger("ImportType") = 1 Then
        '    db.Execute("UPDATE FieldCategory SET TabularData = 1, SourceTableId = " & tableId & ", SourceTableName = " + t.GetString("Name").ToSqlValue + "  WHERE Id = " & catId)
        'Else
        '    db.Execute("UPDATE FieldCategory SET TabularData = 0, SourceTableId = NULL, SourceTableName = NULL  WHERE Id = " & catId)
        'End If




        ReorderCategoryFields(catId)
        Return cseFID
    End Function





    Public Shared Sub RemoveField(fieldId As String, catId As String,Username As string)
        'db.Execute("UPDATE DataSourceField SET CategoryId = NULL, CategoryOrdinal = 0 WHERE Id = {0}".FormatString(fieldId))
        'Dim tc As DataRow = db.GetTopRow("SELECT TOP 1 * FROM DataSourceField WHERE CategoryId = " & catId)
        'If tc Is Nothing Then
        '    db.Execute("UPDATE FieldCategory SET TabularData = 0, SourceTableId = NULL, SourceTableName = NULL WHERE Id = " & catId)
        'End If
        'If Database.Tenant.GetIntegerValue("SELECT IsCustomField FROM DataSourceField WHERE Id = " & fieldId) = 1 Then
        '    Database.Tenant.Execute("DELETE FROM DataSourceField WHERE Id = " & fieldId)
        'End If
        
        Dim dr As DataRow = db.GetTopRow("SELECT Label FROM cse.ComparableFields WHERE Id = " + fieldId.ToString)
        Dim name As String = dr("Label").ToString
        
        Dim drC As DataRow = db.GetTopRow("SELECT Id, Category FROM cse.FieldCategory where Id = " + catId.ToString)
        Dim Cname As String = drC("Category").ToString()
        
        Database.Tenant.Execute("DELETE FROM cse.ComparableFields WHERE Id= " & fieldId)
        
        Dim Description As String = "Field ("+name+") has been removed from Category ("+Cname.ToString()+")."
        SettingsAuditTrail.insertSettingsAuditTrail(Username,Description)
          
        ReorderCategoryFields(catId)
    End Sub

    Public Shared Sub ReorderCategoryFields(catId As Integer)
        db.Execute("EXEC cc_ReorderCategoryFields " + catId.ToString + ", 1")
    End Sub

    Public Shared Sub ReOrderCategories()
        db.Execute("EXEC cc_ReorderCategories 1")
    End Sub

    Public Shared Sub MoveCategory(catId As Integer, newIndex As Integer)
        db.Execute("UPDATE cse.FieldCategory SET Ordinal = {1} WHERE Id = {0}".FormatString(catId, newIndex))
        ReOrderCategories()
    End Sub

    Shared Sub MoveField(fieldId As String, newIndex As String)
        db.Execute("UPDATE cse.ComparableFields SET Ordinal = {1} WHERE Id = {0}".FormatString(fieldId, newIndex))
        ReorderCategoryFields(db.GetIntegerValue("SELECT CategoryId FROM cse.ComparableFields WHERE Id = {0}".FormatString(fieldId)))
    End Sub

    Shared Sub Rename(catId As String, newName As String,Username As String)
    	
        Dim drC As DataRow = db.GetTopRow("SELECT Id, Category FROM cse.FieldCategory where Id = " + catId.ToString)
        
        db.Execute("UPDATE cse.FieldCategory SET category = {1} WHERE Id = {0}".FormatString(catId, newName.ToSqlValue))
    	
        Dim Cname As String = drC("Category").ToString()
        Dim Description As String = "Category ("+Cname+") has been renamed as ("+newName+")."
        SettingsAuditTrail.insertSettingsAuditTrail(Username,Description)
        
    End Sub

Shared Sub Delete(catId As String,Username As String)
	
		Dim drC As DataRow = db.GetTopRow("SELECT Id, Category FROM cse.FieldCategory where Id = " + catId.ToString)
	
        db.Execute("DELETE cse.ComparableFields  WHERE CategoryId = {0}".FormatString(catId))
        db.Execute("DELETE FROM cse.FieldCategory WHERE Id = " & catId)
        
        Dim Cname As String = drC("Category").ToString()
        Dim Description As String = "Category ("+Cname+") has been deleted."
        SettingsAuditTrail.insertSettingsAuditTrail(Username,Description)
        
        ReOrderCategories()
    End Sub
    

    

    

    

    



 

End Class
