﻿Public Class CCExportSettings
    Private Shared ReadOnly Property db As Database
        Get
            Return Database.Tenant
        End Get
    End Property

    Public Shared Sub _exportToXML(ByRef node As XElement)
        ExportClientAction(node)
        ExportClientTemplate(node)
        ExportClientValidation(node)
        ExportAppraisalType(node)
        ExportFieldAlertTypes(node)
    End Sub

    Public Shared Sub ExportClientAction(ByRef node As XElement)
        Dim clientItem = <ClientCustomAction/>
        Dim dtLayer As DataTable = db.GetDataTable("SELECT * FROM ClientCustomAction ORDER BY Ordinal, Id")
        If dtLayer.Rows.Count > 0 Then
            For Each dr As DataRow In dtLayer.Rows
                Dim layer = <Record/>
                layer.@Id = dr.GetString("Id")
                layer.@Action = dr.GetString("Action")
                layer.@TableName = dr.GetString("TableName")
                layer.@FieldName = dr.GetString("FieldName")
                layer.@ValueTag = dr.GetString("ValueTag")
                layer.@CustomText = dr.GetString("CustomText")
                layer.@Enabled = dr.GetString("Enabled")
                clientItem.Add(layer)
            Next
        End If
        node.Add(clientItem)
    End Sub

    Public Shared Sub ExportClientValidation(ByRef node As XElement)
        Dim clientItem = <ClientCustomValidation/>
        Dim dtLayer As DataTable = db.GetDataTable("SELECT * FROM ClientValidation")
        If dtLayer.Rows.Count > 0 Then
            For Each dr As DataRow In dtLayer.Rows
                Dim layer = <Record/>
                layer.@Name = dr.GetString("Name")
                If dr.GetString("Condition") <> "" Then
                    Dim xHead = <Condition></Condition>
                    xHead.Add(New XCData(vbNewLine + dr.GetString("Condition") + vbNewLine))
                    layer.Add(xHead)
                End If
                layer.@ErrorMessage = dr.GetString("ErrorMessage")
                layer.@Enabled = dr.GetString("Enabled")
                layer.@SourceTable = dr.GetString("SourceTable")
                clientItem.Add(layer)
            Next
        End If
        node.Add(clientItem)
    End Sub

    Public Shared Sub ExportClientTemplate(ByRef node As XElement)
        Dim clientItem = <ClientCustomTemplates/>
        Dim dtLayer As DataTable = db.GetDataTable("SELECT * FROM  ClientTemplates")
        If dtLayer.Rows.Count > 0 Then
            For Each dr As DataRow In dtLayer.Rows
                Dim layer = <Record/>
                layer.@Name = dr.GetString("Name")
                If dr.GetString("TemplateContent") <> "" Then
                    Dim xHead = <TemplateContent></TemplateContent>
                    xHead.Add(New XCData(vbNewLine + dr.GetString("TemplateContent") + vbNewLine))
                    layer.Add(xHead)
                End If
                clientItem.Add(layer)
            Next
        End If
        node.Add(clientItem)
    End Sub

    Public Shared Sub ExportAppraisalType(ByRef node As XElement)
        Dim clientItem = <AppraisalType/>
        Dim dtLayer As DataTable = db.GetDataTable("SELECT * FROM  AppraisalType")
        If dtLayer.Rows.Count > 0 Then
            For Each dr As DataRow In dtLayer.Rows
                Dim layer = <Record/>
                layer.@Name = dr.GetString("Name")
                layer.@Description = dr.GetString("Description")
                layer.@Code = dr.GetString("Code")
                clientItem.Add(layer)
            Next
        End If
        node.Add(clientItem)
    End Sub

    Public Shared Sub ExportFieldAlertTypes(ByRef node As XElement)
        Dim clientItem = <FieldAlertTypes/>
        Dim dtLayer As DataTable = db.GetDataTable("SELECT * FROM  FieldAlertTypes")
        If dtLayer.Rows.Count > 0 Then
            For Each dr As DataRow In dtLayer.Rows
                Dim layer = <Record/>
                layer.@Name = dr.GetString("Name")
                clientItem.Add(layer)
            Next
        End If
        node.Add(clientItem)
    End Sub

    Public Shared Sub _ImportToXML(settings As XDocument, Optional isFullSettingsImport As Boolean = False)
        Dim root As XElement
        root = settings.Descendants("ClientCustomAction").First()
        For Each rec In root.Descendants("Record")
            If ((rec.@Action = "" Or rec.@Action Is Nothing) Or (rec.@TableName = "" Or rec.@TableName Is Nothing) Or (rec.@FieldName = "" Or rec.@FieldName Is Nothing)) Then
                Throw New Exception("One or more required parameters are not defined in Client Custom Action.")
                Return
            End If
        Next
        ImportClientCustomAction(root)

        root = settings.Descendants("ClientCustomTemplates").First()
        For Each rec In root.Descendants("Record")
            If ((rec.@Name = "" Or rec.@Name Is Nothing)) Then
                Throw New Exception("One or more required parameters are not defined in Client Custom Templates.")
                Return
            End If
        Next
        ImportClientTemplates(root)

        root = settings.Descendants("ClientCustomValidation").First()
        For Each rec In root.Descendants("Record")
            If ((rec.@Name = "" Or rec.@Name Is Nothing) Or (rec.@ErrorMessage = "" Or rec.@ErrorMessage Is Nothing) Or (rec.@Enabled = "" Or rec.@Enabled Is Nothing) Or (rec.@SourceTable = "" Or rec.@SourceTable Is Nothing)) Then
                Throw New Exception("One or more required parameters are not defined in ClientValidation.")
                Return
            End If
        Next
        ImportClientValidation(root)

        root = settings.Descendants("AppraisalType").First()
        For Each rec In root.Descendants("Record")
            If ((rec.@Name = "" Or rec.@Name Is Nothing) Or (rec.@Description = "" Or rec.@Description Is Nothing) Or (rec.@Code = "" Or rec.@Code Is Nothing)) Then
                Throw New Exception("One or more required parameters are not defined in Appraisal Type.")
                Return
            End If
        Next
        ImportAppraisalType(root)

        root = settings.Descendants("FieldAlertTypes").First()
        For Each rec In root.Descendants("Record")
            If ((rec.@Name = "" Or rec.@Name Is Nothing)) Then
                Throw New Exception("One or more required parameters are not defined in Field Alert Types.")
                Return
            End If
        Next
        ImportFieldAlertTypes(root)
    End Sub

    Private Shared Sub ImportClientCustomAction(item As XElement)
        db.Execute("TRUNCATE TABLE ClientCustomAction; ")
        Dim i = 10
        For Each rowItem In item.Elements("Record")
            Dim sqlFCTemplate As String = "INSERT INTO ClientCustomAction (Action, TableName, FieldName, ValueTag,CustomText,Ordinal,Enabled) VALUES ({0}, {1}, {2}, {3},{4})"
            db.Execute(sqlFCTemplate.SqlFormat(True, rowItem.@Action, rowItem.@TableName, rowItem.@FieldName, rowItem.@ValueTag, rowItem.@CustomText, i, rowItem.@Enabled))
            i = i + 10
        Next
    End Sub

    Private Shared Sub ImportClientTemplates(item As XElement)
        db.Execute("TRUNCATE TABLE ClientTemplates; ")
        For Each rowItem In item.Elements("Record")
            Dim templateNode = rowItem.Descendants("TemplateContent")
            Dim sqlFCTemplate As String = "INSERT INTO ClientTemplates (Name, TemplateContent) VALUES ({0}, {1})"
            db.Execute(sqlFCTemplate.SqlFormat(True, rowItem.@Name, templateNode.First().Value()))
        Next
    End Sub

    Private Shared Sub ImportClientValidation(item As XElement)
        db.Execute("TRUNCATE TABLE ClientValidation; ")
        Dim i = 10
        For Each rowItem In item.Elements("Record")
            Dim ConditionNode = rowItem.Descendants("Condition")
            Dim sqlFCTemplate As String = "INSERT INTO ClientValidation (Name, Condition, ErrorMessage, Ordinal,Enabled,SourceTable) VALUES ({0}, {1}, {2}, {3},{4},{5})"
            db.Execute(sqlFCTemplate.SqlFormat(True, rowItem.@Name, ConditionNode.First().Value(), rowItem.@ErrorMessage, i, rowItem.@Enabled, rowItem.@SourceTable))
            i = i + 10
        Next
    End Sub

    Private Shared Sub ImportAppraisalType(item As XElement)
        db.Execute("TRUNCATE TABLE AppraisalType; ")
        For Each rowItem In item.Elements("Record")
            Dim sqlFCTemplate As String = "INSERT INTO AppraisalType (Name, Description,Code) VALUES ({0}, {1},{2})"
            db.Execute(sqlFCTemplate.SqlFormat(True, rowItem.@Name, rowItem.@Description, rowItem.@Code))
        Next
    End Sub

    Private Shared Sub ImportFieldAlertTypes(item As XElement)
        db.Execute("TRUNCATE TABLE FieldAlertTypes; ")
        For Each rowItem In item.Elements("Record")
            Dim sqlFCTemplate As String = "INSERT INTO FieldAlertTypes (Name) VALUES ({0})"
            db.Execute(sqlFCTemplate.SqlFormat(True, rowItem.@Name))
        Next
    End Sub
End Class
