﻿Imports System.IO
Imports System.Data.OleDb
Imports System.Web

Public Class DataSource

    Public Enum DataSourceFileType As Integer
        AccessTable = 0
        CSVFlatFile = 1
        XMLFile = 2
        XMLAPI = 3
    End Enum
    
    Public Enum DataSourceImportType As Integer
        ParcelData = 0
        ParcelSubTable = 1
        LookupTable = 2
    End Enum

    Public Shared Function RegisterDataFile(temppath As String, s3path As String, userName As String, ipAddress As String) As Integer
        Dim fileName As String = Path.GetFileName(temppath)
        Dim ext As String = Path.GetExtension(fileName).ToLower.TrimStart(".")
        Dim fi As New FileInfo(temppath)
        Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM DataSourceFile WHERE Name = " + fileName.ToSqlValue)
        Dim dsfid As Integer
        If dr Is Nothing Then
            Dim sql As String = "INSERT INTO DataSourceFile (Name, Type, S3Path, Bucket, Size, UploadedBy, UploadedIP) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}); SELECT CAST(@@IDENTITY AS INT) As NewId;".SqlFormatString(fileName, ext, s3path, ClientSettings.AWSS3Bucket, fi.Length, userName, ipAddress)
            dsfid = Database.Tenant.GetIntegerValue(sql)
        Else
            dsfid = dr.GetInteger("Id")
            Dim sql As String = "UPDATE DataSourceFile SET S3Path = {1}, Bucket = {2}, Size = {3}, UploadedBy = {4}, UploadedIp = {5} WHERE Id = {0}".SqlFormatString(dsfid, s3path, ClientSettings.AWSS3Bucket, fi.Length, userName, ipAddress)
            Database.Tenant.Execute(sql)
        End If
        Return dsfid
    End Function

    Public Shared Sub BuildDataStructureFromAccessDatabase(fileid As Integer, path As String)
        Dim connStr As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=""{0}"";Persist Security Info=False".FormatString(path)
        Using conn As New OleDbConnection(connStr)
            conn.Open()
            Dim schemaTable As DataTable = conn.GetOleDbSchemaTable(OleDb.OleDbSchemaGuid.Tables, New Object() {Nothing, Nothing, Nothing, "TABLE"})
            For Each row As DataRow In schemaTable.Rows
                Dim newTable As Boolean = False
                Dim tableName As String = row.GetString("TABLE_NAME")
                Dim tableId As Integer = DataSource.GetOrCreateTable(fileid, tableName, , newTable)
                If newTable Then
                    Dim columns As DataTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Columns, New Object() {Nothing, Nothing, tableName, Nothing})
                    For Each col As DataRow In columns.Rows
                        Dim columnName As String = col.GetString("COLUMN_NAME")
                        Dim pos As Integer = col.GetInteger("ORDINAL_POSITION")
                        CreateField(tableId, tableName, columnName, pos)
                    Next
                End If
            Next row
            conn.Close()
        End Using
    End Sub

    Public Shared Function GetDataSourceFiles() As DataTable
        Return Database.Tenant.GetDataTable("SELECT * FROM DataFilesUsage ORDER BY UploadedDate DESC")
    End Function

    Public Shared Function GetOrCreateTable(fileid As Integer, tableName As String, Optional fileType As DataSourceFileType = DataSourceFileType.AccessTable, Optional ByRef newTable As Boolean = False) As Integer
        Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM DataSourceTable WHERE Name = '" + tableName + "'")
        If dr Is Nothing Then
            Dim importType As DataSourceImportType = DataSourceImportType.ParcelData
            If tableName.ToLower.EndsWith("_lv") Or tableName.ToLower.EndsWith("_lookup") Then
                importType = DataSourceImportType.LookupTable
            End If
            Database.Tenant.Execute("INSERT INTO DataSourceTable (DataSourceFileId, Name, FileType, ImportType) VALUES (" & fileid & ", " + tableName.ToSqlValue + ", " & fileType.GetHashCode & ", " & importType.GetHashCode & ")")
            dr = Database.Tenant.GetTopRow("SELECT * FROM DataSourceTable WHERE Name = '" + tableName + "'")
            newTable = True
        End If
        Return dr.GetInteger("Id")
    End Function

    Private Shared Sub CreateField(tableId As Integer, tableName As String, columnName As String, pos As Integer)
        Database.Tenant.Execute("INSERT INTO DataSourceField(Name, AssignedName, DisplayLabel, TableId, SourceTable, SourceOrdinal) VALUES ( {0}, {0}, {0}, {1}, {2}, {3})".FormatString(columnName.ToSqlValue, tableId, tableName.ToSqlValue, pos))
    End Sub

    Public Shared Sub ChangeImportType(tableName As String, type As DataSourceImportType)
        Dim sql As String = "UPDATE DataSourceTable SET DoNotImport = 0, ImportType='" + type.GetHashCode.ToString + "' WHERE Name=" + tableName.ToSqlValue
        Database.Tenant.Execute(sql)

        If type = DataSourceImportType.LookupTable Then
            AssignLookupFields(tableName)
        Else
            NullifyLookupFields(tableName)
        End If
    End Sub

    Public Shared Sub MarkTableAsDNI(tableName As String)
        Database.Tenant.Execute("UPDATE DataSourceTable SET DoNotImport= 1 WHERE Name=" + tableName.ToSqlValue)
    End Sub

    Public Shared Sub AssignLookupFields(tableName As String)
        Dim lf As DataRow = Database.Tenant.GetTopRow("SELECT F1.Name As LF1, F2.Name As LF2, F3.Name As LF3 FROM DataSourceTable DST INNER JOIN DataSourceField F1 ON (F1.TableId = DST.Id AND F1.SourceOrdinal = 1) INNER JOIN DataSourceField F2 ON (F2.TableId = DST.Id AND F2.SourceOrdinal = 2) LEFT OUTER JOIN DataSourceField F3 ON (F3.TableId = DST.Id AND F3.SourceOrdinal = 3) WHERE DST.Name = " & tableName.ToSqlValue)
        If lf IsNot Nothing Then
            Database.Tenant.Execute("UPDATE DataSourceTable SET LookupIDField = {1}, LookupNameField = {2}, LookupDescField = {3} WHERE Name= {0}".SqlFormatStringWithNulls(tableName, lf.GetString("LF1"), lf.GetString("LF2"), lf.GetString("LF3")))
        End If
    End Sub

    Public Shared Sub NullifyLookupFields(tableName As String)
        Database.Tenant.Execute("UPDATE DataSourceTable SET LookupIDField = NULL, LookupNameField = NULL, LookupDescField = NULL WHERE Name= {0}".SqlFormatString(tableName))
    End Sub

    Public Shared Sub UpdateFieldProperty(fieldId As Integer, columnName As String, type As String, value As String, loginId As String)
    	If columnName = "CalculationExpression" Then
            If value.Trim = "" Then
                Database.Tenant.Execute("UPDATE DataSourceField SET IsCalculated = 0 WHERE Id = " & fieldId)
            Else
                Database.Tenant.Execute("UPDATE DataSourceField SET IsCalculated = 1 WHERE Id = " & fieldId)
            End If
        End If
        'If columnName = "DataType" And value <> "5" Then
        '	Database.Tenant.Execute("UPDATE DataSourceField SET Lookup = False WHERE Id = " & fieldId)
        'End If
        Dim updateColumnSet As String = columnName + " = " + value.ToSqlValue
        If columnName = "DataType" And value <> "5" Then
            Dim prevValue As String = Database.Tenant.GetStringValue("SELECT DataType FROM DataSourceField WHERE ID = " & fieldId)
            If prevValue = "5" Then
                updateColumnSet += ", LookupTable = NULL, LookupQuery = NULL, LookupQueryForCache = NULL, LookupQueryCustomFields = NULL, AllowTextInput = 0"
            End If
        End If

        If columnName = "DataType" And value <> "1" Then
            Dim prevValue As String = Database.Tenant.GetStringValue("SELECT DataType FROM DataSourceField WHERE ID = " & fieldId)
            If prevValue = "1" Then
                updateColumnSet += ", DoNotAllowSpecialCharacters = 0"
            End If
        End If

        If columnName = "DataType"  Then
            	updateColumnSet += ", CustomInputType = 1"
        End If
         
         Dim oldValue As String = Database.Tenant.GetStringValue("SELECT " + columnName + " FROM DataSourceField WHERE ID = " & fieldId)
         If columnName = "DisplayLabel" Then
         	If Database.Tenant.GetIntegerValue("Select dt.ImportType From DatasourceTable dt join DataSourceField df On df.TableId = dt.Id Where df.Id = " & fieldId) = 0 Then
         	Dim rowExist As Boolean = Database.Tenant.GetIntegerValue("SELECT Count(*) From UI_GridViewFieldSettings WHERE Heading ={0}".SqlFormatString(oldValue))
         		If rowExist Then 
         			Database.Tenant.Execute("UPDATE UI_GridViewFieldSettings SET Heading = " + value.ToSqlValue + " WHERE Heading = {0}".SqlFormatString(oldValue))
         		End If
         	End If
         End If
        If	columnName = "DataType" Then
        		Dim tempold As DataRow = Database.System.GetTopRow("SELECT Name FROM GlobalFieldInputTypes WHERE ID = " & oldValue)
       	    	oldValue= tempold.GetString("Name")
       	    	Dim tempvalue As DataRow = Database.System.GetTopRow("SELECT Name FROM GlobalFieldInputTypes WHERE ID = " & value)
       	    	value= tempvalue.GetString("Name")
       	End If
        Database.Tenant.Execute("UPDATE DataSourceField SET " + updateColumnSet + " WHERE Id = " & fieldId)
        Dim o As DataRow = Database.System.GetTopRow("SELECT EnableXMLAuditTrail FROM OrganizationSettings WHERE OrganizationId = " & HttpContext.Current.GetCAMASession().OrganizationId)
        If (o IsNot Nothing AndAlso o.GetBoolean("EnableXMLAuditTrail")) Then 
             If columnName = "DisplayLabel" Or columnName = "DataType" Or columnName = "LookupTable" Then
                 SettingsAuditTrail._updateSettingsAuditTrail(columnName, oldValue,value, fieldId, loginId, type)        
             Else
                 SettingsAuditTrail._updateSettingsAuditTrail(columnName, oldValue, Convert.ToBoolean(Convert.ToInt32(value)), fieldId, loginId, type)    
             End If
        End If
    End Sub

    Public Shared Sub UpdateTableProperty(tableId As Integer, columnName As String, value As String)
        Database.Tenant.Execute("UPDATE DataSourceTable SET " + columnName + " = " + value.ToSqlValue(True) + " WHERE Id = " & tableId)
    End Sub

    Public Shared Sub AssignKeyFields()
        If ClientSettings.CommonKeyField1.IsNotEmpty Then

        End If
    End Sub

    Public Shared Function DeleteSourceFileAndDependencies(sourceFileId As Integer) As Boolean
        Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM DataSourceFile WHERE Id = " + sourceFileId.ToString())
        Dim s3Path As String = dr.GetString("S3Path")
        Dim s3 As New S3FileManager
        If Not s3.DeleteFile(s3Path) Then
            'Return False
        End If
        Database.Tenant.Execute("DELETE FROM DataSourceField WHERE TableId IN (SELECT Id FROM DataSourceTable WHERE DataSourceFileId = " & sourceFileId & ")")
        Database.Tenant.Execute("DELETE FROM DataSourceTable WHERE DataSourceFileId = " & sourceFileId)
        Database.Tenant.Execute("DELETE FROM DataSourceFile WHERE Id = " & sourceFileId)
        Return True
    End Function

#Region "Queries"

    'Public Shared Function DataSource.FindTargetTable(ByVal db As Database, ByVal tableName As String, Optional ByRef targetTableOrginalName As String = "") As String
    '    If tableName = "_photo_meta_data" Then
    '        targetTableOrginalName = tableName
    '        Return String.Empty
    '    End If
    '    Dim sql As String = "SELECT t2.Name FROM DataSourceRelationships r INNER JOIN DataSourceTable t1 ON r.TableId = t1.Id AND r.Relationship = 0 INNER JOIN DataSourceTable t2 ON r.ParentTableId = t2.Id WHERE t1.Name = {0}"
    '    Dim parentTable As String = "", targetTable As String = tableName
    '    While True
    '        parentTable = db.GetStringValue(sql.SqlFormatString(targetTable))
    '        If parentTable = "" Then
    '            Exit While
    '        Else
    '            targetTable = parentTable
    '        End If
    '    End While

    '    targetTableOrginalName = targetTable

    '    If targetTable.Trim.ToLower = db.Application.ParcelTable.Trim.ToLower Then
    '        Return "ParcelData"
    '    ElseIf targetTable.Trim.ToLower = db.Application.NeighborhoodTable.Trim.ToLower Then
    '        Return "NeighborhoodData"
    '    ElseIf targetTable <> String.Empty Then
    '        Return "XT_" + targetTable
    '    Else
    '        Return String.Empty
    '    End If
    'End Function
    Public Shared Function FindTargetTable(ByVal db As Database, ByVal tableName As String, Optional ByRef targetTableOrginalName As String = "", Optional ByRef isLookupTable As Boolean = False) As String
        If tableName = "_photo_meta_data" Then
            targetTableOrginalName = tableName
            Return String.Empty
        End If

        Dim sql As String = "SELECT t2.Name FROM DataSourceRelationships r INNER JOIN DataSourceTable t1 ON r.TableId = t1.Id AND r.Relationship = 0 INNER JOIN DataSourceTable t2 ON r.ParentTableId = t2.Id WHERE t1.Name = {0}"
        Dim parentTable As String = "", targetTable As String = tableName
        While True
            parentTable = db.GetStringValue(sql.SqlFormatString(targetTable))
            If parentTable = "" Then
                Exit While
            Else
                targetTable = parentTable
            End If
        End While

        targetTableOrginalName = targetTable

        Dim noOfRelations As Integer = db.GetIntegerValue("SELECT COUNT(*) FROM DataSourceRelationships r INNER JOIN DataSourceTable t1 ON r.TableId = t1.Id AND r.Relationship IS NOT NULL WHERE t1.Name = {0}".SqlFormatString(tableName))

        If db.Application.ParcelTable IsNot Nothing AndAlso targetTable.Trim.ToLower = db.Application.ParcelTable.Trim.ToLower Then
            Return "ParcelData"
        ElseIf db.Application.NeighborhoodTable IsNot Nothing AndAlso targetTable.Trim.ToLower = db.Application.NeighborhoodTable.Trim.ToLower Then
            Return "NeighborhoodData"
        ElseIf targetTable <> String.Empty AndAlso noOfRelations > 0 Then
            Return "XT_" + targetTable.Trim()
        ElseIf noOfRelations = 0 Then
            isLookupTable = True
            Return "LT_" + targetTable.Trim()
        Else
            Return String.Empty
        End If
    End Function

    Public Shared Function GetTargetTables(db As Database) As Dictionary(Of String, String)
        Return db.GetDataTable("SELECT Name, CC_TargetTable FROM DataSourceTable").AsEnumerable.ToDictionary(Function(x) x.GetString("Name"), Function(y) y.GetString("CC_TargetTable"))
    End Function

    Public Shared Sub CategorizeDataTables()
        CategorizeDataTables(Database.Tenant)
    End Sub

    Public Shared Sub CategorizeDataTables(db As Database)
        db.Execute("EXEC cc_CategorizeDataTables")
        'For Each dr As DataRow In db.GetDataTable("SELECT * FROM DataSourceTable").Rows
        '    Dim tableId As String = dr.GetString("Id")
        '    Dim tableName As String = dr.GetString("Name")
        '    Dim targetTableName = DataSource.FindTargetTable(db, tableName)
        '    If targetTableName = "ParcelData" Then
        '        db.Execute("UPDATE DataSourceTable SET ImportType = 0 WHERE Id = " + tableId)
        '    ElseIf targetTableName = "NeighborhoodData" Then
        '        db.Execute("UPDATE DataSourceTable SET ImportType = 3 WHERE Id = " + tableId)
        '    ElseIf targetTableName.StartsWith("XT_") Then
        '        db.Execute("UPDATE DataSourceTable SET ImportType = 1 WHERE Id = " + tableId)
        '        'ElseIf tableName = "_photo_meta_data" Or targetTableName.StartsWith("LT_") Then
        '    ElseIf tableName = "_photo_meta_data" Then
        '        db.Execute("UPDATE DataSourceTable SET ImportType = 4 WHERE Id = " + tableId)
        '    End If
        'Next
    End Sub

    Public Shared Function GetNamesOfParcelDataTables() As DataTable
        CategorizeDataTables()
        Return Database.Tenant.GetDataTable("SELECT Id, CASE WHEN ImportType = 1 THEN '[AUX] ' + Name WHEN ImportType = 3 THEN '[NBHD] ' + Name ELSE Name END As Name, ImportType FROM DataSourceTable WHERE DoNotImport = 0 AND ImportType IN (0,1,3) ORDER BY ImportType, Name")
    End Function



#End Region

    Public Shared Sub MoveComparableField(cfid As Integer, newIndex As Integer)
        e_("UPDATE FieldCategory SET Ordinal = {1} WHERE Id = {0}", False, {cfid, newIndex})
        e_("EXEC cc_ReorderComparables")
    End Sub

    Public Shared Function GetParcelTables() As DataTable
        'If Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM DataSourceTable WHERE ImportType = 0") = 0 Then
        '    categorizeDataTables()
        'End If
        CategorizeDataTables()
        Return Database.Tenant.GetDataTable("SELECT Id, Name FROM DataSourceTable WHERE ImportType = 0")
    End Function

    'Public Shared Function GetAssociateTables() As List(Of String)
    '    Dim tables As New List(Of String)
    '    _getConnectedTables(tables, Database.Tenant.Application.ParcelTable)
    '    Return tables
    'End Function

    'Private Shared Sub _getConnectedTables(ByVal tables As List(Of String), ByVal tableName As String)
    '    If Not tables.Contains(tableName) Then tables.Add(tableName)
    '    For Each connectTableName In db.GetDataTable("SELECT t.Name, r.Relationship FROM DataSourceRelationships r INNER JOIN DataSourceTable t ON r.TableId = t.Id INNER JOIN DataSourceTable p ON r.ParentTableId = p.Id WHERE p.Name = '{0}' ORDER BY Relationship".FormatString(tableName)).Rows.OfType(Of DataRow).Select(Function(x) x.GetString("Name")).ToArray
    '        _getConnectedTables(db, tables, connectTableName)
    '    Next
    'End Sub

    Public Shared Function GetParentChildTable(Optional ByVal dataHierarchy As Boolean = False) As DataTable
        Dim targetTables As New Dictionary(Of String, String)
        Dim dt As New DataTable
        Dim s As String = ""
        If (dataHierarchy) Then
            s = "tt.id,"
        End If
        dt = Database.Tenant.GetDataTable("SELECT DISTINCT pt.Name As ParentTable," + s + "tt.Name As ChildTable FROM DataSourceRelationships r LEFT OUTER JOIN DataSourceTable pt ON r.ParentTableId = pt.Id LEFT OUTER JOIN DataSourceTable tt ON r.TableId = tt.Id WHERE r.Relationship = 2")
        For Each dr As DataRow In dt.Rows
            dr("ParentTable") = GetTargetTableNameForMA(targetTables, dr("ParentTable"))
            dr("ChildTable") = GetTargetTableNameForMA(targetTables, dr("ChildTable"))
        Next

        Return dt
    End Function

    Public Shared Function GetParentChildTableInSchema(db As Database) As DataTable
        Return db.GetDataTable("SELECT DISTINCT pt.Name As ParentTable, pt.Id As ParentTableId, tt.Name As ChildTable, tt.Id As ChildTableId, r.Relationship FROM DataSourceRelationships r LEFT OUTER JOIN DataSourceTable pt ON r.ParentTableId = pt.Id LEFT OUTER JOIN DataSourceTable tt ON r.TableId = tt.Id WHERE r.Relationship IN (0,2)")
    End Function

    Private Shared Function GetTargetTableNameForMA(ByRef targetTables As Dictionary(Of String, String), tableName As String)
        If tableName <> "" Then
            If targetTables.ContainsKey(tableName) Then
                Dim targetTableName As String = targetTables(tableName)
                If targetTableName = Database.Tenant.Application.ParcelTable Then
                    Return "Parcel"
                Else
                    Return targetTableName
                End If
            Else
                Dim targetTableName As String = ""
                DataSource.FindTargetTable(Database.Tenant, tableName, targetTableName)
                If targetTableName = Database.Tenant.Application.ParcelTable Then
                    Return "Parcel"
                Else
                    Return targetTableName
                End If
                targetTables.Add(tableName, targetTableName)
            End If
        End If
        Return ""
    End Function

    Public Shared Function TranslateInputTypeToSqlType(inputType As Integer, Optional maxLength As Integer = 0)
        Select Case inputType
            Case 1
                If maxLength = 0 Then maxLength = 255
                Return "varchar(" & maxLength & ")"
            Case 2
                Return "float"
            Case 3
                Return "bit"
            Case 4
                Return "datetime"
            Case 5
                Return "varchar(255)"
            Case 6
                Return "varchar(MAX)"
            Case 7
                Return "numeric(18, 2)"
            Case 8
                Return "int"
            Case 9
                Return "int"
            Case 10
                Return "int"
            Case 11
                Return "varchar(MAX)"
            Case Else
                Return "varchar(255)"
        End Select
    End Function


    Public Shared Function GetKeyValueListForTable(ByVal target As Data.Database, ByVal tableName As String, ByVal rowUID As Integer, ByVal parcelId As Integer, Optional ByRef rowExist As Boolean = True) As List(Of KeyValuePair(Of String, String))
        Dim filter As String = ""
        Dim tableId = 0
        Dim keyValueList As New List(Of KeyValuePair(Of String, String))

        Dim targetTable = FindTargetTableAndFilterCondition(target, tableName, rowUID, parcelId, filter)
        If (targetTable = String.Empty) Then
            Return keyValueList
        End If

        Dim keys = GetTableKeys(target, tableName)

        For Each key As String In keys
            Dim sqlSelect = "SELECT ["+ key +"] FROM " + targetTable + " WHERE " + filter
            Dim value = target.GetStringValue(sqlSelect, Nothing)
            keyValueList.Add(New KeyValuePair(Of String, String)(key, value))
        Next

        If (target.GetIntegerValue("SELECT COUNT(*) FROM " + targetTable + " WHERE " + filter) = 0) Then
            rowExist = False
        End If

        Return keyValueList
    End Function


    Public Shared Function GetTableKeys(ByVal db As Database, ByVal tableName As String) As String()
    	Dim sqlKeys As String = "SELECT DISTINCT  f.Name  AS Name FROM DataSourceField f INNER JOIN  DataSourceKeys k ON f.Id = k.FieldId INNER JOIN  DataSourceRelationships r ON k.RelationshipId = r.Id INNER JOIN DataSourceTable t ON r.TableId = t.Id WHERE k.IsPrimaryKey=1 AND t.Name = '" + tableName + "'"
		Dim keys = db.GetDataTable(sqlKeys).Rows.Cast(Of DataRow)().Select(Function(x) x.GetString("Name")).ToArray
        Return keys
    End Function

    Public Shared Function GetParentTableKeys(ByVal db As Database, ByVal tableName As String, ByVal parentTableName As String) As Dictionary(Of String, String)
        Dim sqlKeys As String = "SELECT f.Name  As FieldName, rf.Name As ReferenceFieldName FROM DataSourceField f INNER JOIN  DataSourceKeys k ON f.Id = k.FieldId INNER JOIN  DataSourceRelationships r ON k.RelationshipId = r.Id INNER JOIN DataSourceTable t ON r.TableId = t.Id INNER JOIN DataSourceTable rt ON r.ParentTableId = rt.Id INNER JOIN DataSourceField rf ON k.ReferenceFieldId = rf.Id WHERE k.IsPrimaryKey=1 AND t.Name = '" + tableName + "' AND rt.Name = " + parentTableName.ToSqlValue
        Dim keys = db.GetDataTable(sqlKeys).Rows.Cast(Of DataRow)().ToDictionary(Of String, String)(Function(x) x.GetString("FieldName"), Function(x) x.GetString("ReferenceFieldName"))
        Return keys
    End Function

    Public Shared Function FindTargetTableAndFilterCondition(ByVal db As Data.Database, ByVal targetTable As String, ByVal rowUID As String, ByVal parcelId As String, ByRef filter As String) As String
        If (parcelId <> 0) Then
            filter = "CC_ParcelId=" + parcelId
        End If

        If rowUID <> 0 Then
            If filter <> "" Then
                filter += " AND "
            End If
            filter += " ROWUID=" + rowUID
        End If
        Return DataSource.FindTargetTable(db, targetTable, "")
    End Function

    Public Shared Function GetTableId(db As Database, tableName As String) As Integer
        Dim tableId As Integer = db.GetIntegerValueOrInvalid("SELECT Id FROM DataSourceTable WHERE Name = " + tableName.ToSqlValue)
        Return tableId
    End Function

    Public Shared Sub DeleteTable(db As Database, ByVal tableId As String)
    	Dim targetTable As String = ""
    	Dim tableExists As Integer = 0
        Dim childTableIds = db.GetDataTable("SELECT TableId FROM DataSourceRelationships WHERE ParentTableId = " & tableId).AsEnumerable.Select(Function(x) x.GetInteger("TableId")).ToList
        For Each ct In childTableIds
            DeleteTable(db, ct)
        Next
        ''Delete data from ParcelChanges
        db.Execute("DELETE FROM  ParcelChanges WHERE FieldId IN(SELECT Id FROM DataSourceField WHERE TableId= " + tableId + " )")
        ''Delete data from targetTable
        Dim dt As DataTable = db.GetDataTable("SELECT * FROM  DataSourceField WHERE Tableid=" + tableId + " AND id NOT IN(SELECT FieldId FROM DataSourceKeys WHERE Tableid= " + tableId + ")")
        If (dt.Rows.Count > 0) Then
            '' if field is exist for corresponding table then delete the corresponding columns from targetTable
            targetTable = DataSource.FindTargetTable(db, dt.Rows(0)("SourceTable"))
            tableExists = db.GetIntegerValue("IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'"+targetTable+"') BEGIN SELECT 1 as tableExist END ELSE BEGIN SELECT 0 as tableExist End")
            Dim index As String = Nothing
            For Each dr As DataRow In dt.Rows
                ''Check ColumnsExists
                Dim dtColumnsExists As DataTable = db.GetDataTable("SELECT * FROM sys.columns WHERE Name = N'" + dr.Item("name") + "' AND Object_ID = Object_ID(N'" + targetTable + "')")
                If (dtColumnsExists.Rows.Count > 0) Then
                    ''Check IndexExists
                    Dim dtIndexExists As DataTable = db.GetDataTable("SELECT TableName = t.name,IndexName = ind.name,ColumnName = col.name FROM sys.indexes ind INNER JOIN sys.index_columns ic ON  ind.object_id = ic.object_id and ind.index_id = ic.index_id INNER JOIN sys.columns col ON ic.object_id = col.object_id and ic.column_id = col.column_id INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE t.name='" + targetTable + "' AND col.name='" + dr.Item("name") + "'")
                    If (dtIndexExists.Rows.Count > 0) Then
                        For Each drIndexExists As DataRow In dtIndexExists.Rows
                            ''Delete INDEX of column from targetTable
                            index = drIndexExists("IndexName")
                            If tableExists = 1 Then
				        		db.Execute("DROP INDEX " + targetTable + "." + index)
				        	End If
                            index = Nothing
                        Next
                    End If
                    ''Delete data from targetTable
                    If tableExists = 1 Then
		        		db.Execute("ALTER TABLE " + targetTable + " DROP COLUMN [" + dr.Item("name") + "]")
		        	End If
                End If
            Next
        Else
            ''if field is not exist for corresponding table then get the targetTable
            targetTable = db.GetStringValue("SELECT CC_TargetTable FROM DataSourceTable WHERE id= " + tableId + "")
            tableExists = db.GetIntegerValue("IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'"+targetTable+"') BEGIN SELECT 1 as tableExist END ELSE BEGIN SELECT 1 as tableExist End")
        End If

        ''Drop the table from targetTable
        If (targetTable = "ParcelData") Then
            Dim noOfColumns As Integer = db.GetIntegerValue("SELECT COUNT(COLUMN_NAME) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME ='" + targetTable + "'")
            If (noOfColumns = 0) Then
                db.Execute("DROP TABLE " + targetTable)
            End If
        Else
        	If tableExists = 1 Then
        		db.Execute("DROP TABLE " + targetTable)
        	End If
        End If

        ''Delete data from DataSourceKeys
        db.Execute("DELETE FROM DataSourceKeys WHERE Tableid= " + tableId)

        ''Delete data from DataSourceRelationships
        db.Execute("DELETE FROM DataSourceRelationships WHERE TableId= " + tableId)

        ''Delete data from DataSourceField
        db.Execute("DELETE FROM DataSourceField WHERE TableId= " + tableId)

        ''Delete data from DataSourceTable
        db.Execute("DELETE FROM DataSourceTable WHERE id=" + tableId)
    End Sub

    'Public Shared Function GetTableNames(db As Database) As Dictionary(Of String, String)
    '	Throw New NotImplementedException
    'End Function

End Class
