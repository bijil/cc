﻿Public Class CCImportSettings
    Private Shared ReadOnly Property db As Database
        Get
            Return Database.Tenant
        End Get
    End Property

    Public Shared Sub _ImportToXML(settings As XDocument, Optional isFullSettingsImport As Boolean = False)
        Dim root As XElement
        root = settings.Descendants("ClientCustomAction").First()
        For Each rec In root.Descendants("Record")
            If ((rec.@Action = "" Or rec.@Action Is Nothing) Or (rec.@TableName = "" Or rec.@TableName Is Nothing) Or (rec.@FieldName = "" Or rec.@FieldName Is Nothing)) Then
                Throw New Exception("One or more required parameters are not defined in Client Custom Action.")
                Return
            Else '''
                ImportClientCustomAction(rec) '''
            End If
        Next

        root = settings.Descendants("ClientCustomTemplates").First()
        For Each rec In root.Descendants("Record")
            If ((rec.@Name = "" Or rec.@Name Is Nothing)) Then
                Throw New Exception("One or more required parameters are not defined in Client Custom Templates.")
                Return
            Else '''
                ImportClientTemplates(rec) '''
            End If
        Next

        root = settings.Descendants("ClientCustomValidation").First()
        For Each rec In root.Descendants("Record")
            If ((rec.@Name = "" Or rec.@Name Is Nothing) Or (rec.@Condition = "" Or rec.@Condition Is Nothing) Or (rec.@ErrorMessage = "" Or rec.@ErrorMessage Is Nothing) Or (rec.@Enabled = "" Or rec.@Enabled Is Nothing) Or (rec.@SourceTable = "" Or rec.@SourceTable Is Nothing)) Then
                Throw New Exception("One or more required parameters are not defined in ClientValidation.")
                Return
            Else '''
                ImportClientValidation(rec) '''
            End If
        Next

        root = settings.Descendants("AppraisalType").First()
        For Each rec In root.Descendants("Record")
            If ((rec.@Name = "" Or rec.@Name Is Nothing) Or (rec.@Description = "" Or rec.@Description Is Nothing) Or (rec.@Code = "" Or rec.@Code Is Nothing)) Then
                Throw New Exception("One or more required parameters are not defined in Appraisal Type.")
                Return
            Else '''
                ImportAppraisalType(rec) '''
            End If
        Next

        root = settings.Descendants("FieldAlertTypes").First()
        For Each rec In root.Descendants("Record")
            If ((rec.@Name = "" Or rec.@Name Is Nothing)) Then
                Throw New Exception("One or more required parameters are not defined in Field Alert Types.")
                Return
            Else '''
                ImportFieldAlertTypes(rec) '''
            End If
        Next
    End Sub

    Private Shared Sub ImportClientCustomAction(item As XElement)
        db.Execute("TRUNCATE TABLE ClientCustomAction; ")
        Dim i = 10
        For Each rowItem In item.Elements("Record")
            Dim sqlFCTemplate As String = "INSERT INTO ClientCustomAction (Action, TableName, FieldName, ValueTag,CustomText,Ordinal,Enabled) VALUES ({0}, {1}, {2}, {3},{4})"
            db.Execute(sqlFCTemplate.SqlFormat(True, rowItem.@Action, rowItem.@TableName, rowItem.@FieldName, rowItem.@ValueTag, rowItem.@CustomText, i, rowItem.@Enabled))
            i = i + 10
        Next
    End Sub

    Private Shared Sub ImportClientTemplates(item As XElement)
        db.Execute("TRUNCATE TABLE ClientTemplates; ")
        For Each rowItem In item.Elements("Record")
            Dim sqlFCTemplate As String = "INSERT INTO ClientTemplates (Name, TemplateContent) VALUES ({0}, {1})"
            db.Execute(sqlFCTemplate.SqlFormat(True, rowItem.@Name, rowItem.@TemplateContent)) '''
        Next
    End Sub

    Private Shared Sub ImportClientValidation(item As XElement)
        db.Execute("TRUNCATE TABLE ClientValidation; ")
        Dim i = 10
        For Each rowItem In item.Elements("Record")
            Dim sqlFCTemplate As String = "INSERT INTO ClientValidation (Name, Condition, ErrorMessage, Ordinal,Enabled,SourceTable) VALUES ({0}, {1}, {2}, {3},{4},{5})"
            db.Execute(sqlFCTemplate.SqlFormat(True, rowItem.@Name, rowItem.@Condition, rowItem.@ErrorMessage, i, rowItem.@Enabled, rowItem.@SourceTable)) '''
            i = i + 10
        Next
    End Sub

    Private Shared Sub ImportAppraisalType(item As XElement)
        db.Execute("TRUNCATE TABLE AppraisalType; ")
        For Each rowItem In item.Elements("Record")
            Dim sqlFCTemplate As String = "INSERT INTO AppraisalType (Name, Description,Code) VALUES ({0}, {1},{2})"
            db.Execute(sqlFCTemplate.SqlFormat(True, rowItem.@Name, rowItem.@Description, rowItem.@Code))
        Next
    End Sub

    Private Shared Sub ImportFieldAlertTypes(item As XElement)
        db.Execute("TRUNCATE TABLE FieldAlertTypes; ")
        For Each rowItem In item.Elements("Record")
            Dim sqlFCTemplate As String = "INSERT INTO FieldAlertTypes (Name) VALUES ({0})"
            db.Execute(sqlFCTemplate.SqlFormat(True, rowItem.@Name.ToSqlValue)) '''
        Next
    End Sub

End Class
