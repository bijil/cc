﻿Imports CAMACloud.BusinessLogic.RemoteIntegration
Public Class ProcessParcelDataChange

    Public Shared Sub RevertChanges(db As Data.Database, parcelId As String)

        Dim dt As DataTable = db.GetDataTable("SELECT * FROM ParcelChanges WHERE ParcelId=" + parcelId)
        For Each dr As DataRow In dt.Rows
            Dim Action = dr.GetString("Action")
            Dim AuxRowUid = dr.GetString("AuxROWUID")
            Dim fieldId = dr.GetString("FieldId")
            Dim table = db.GetStringValue("SELECT SourceTable FROM DataSourceField WHERE Id=" + fieldId)
            Dim targetTable = DataSource.FindTargetTable(db, table)
            If Action = "new" Then
                db.Execute("DELETE FROM " + targetTable + " WHERE ROWUID=" + AuxRowUid.ToSqlValue)
            Else
                If (Action = "delete") Then
                    db.Execute("UPDATE " + targetTable + "SET CC_Deleted=0 WHERE ROWUID=" + AuxRowUid.ToSqlValue)
                End If
            End If
        Next
    End Sub

    Public Shared Sub DeleteChanges(db As Data.Database, parcelId As String)
        db.Execute("DELETE ParcelChanges WHERE ParcelId=" + parcelId)
    End Sub
    
    Public Shared Function  SketchSettingsPropertyValue(sketchPropertyName As String) As String
        Try
            Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT Value FROM SketchSettings WHERE Name = '" + sketchPropertyName + "'")
            If dr Is Nothing Then
                Return Nothing
            End If
            Return dr.GetString("Value")
        Catch ex As Exception
            Return Nothing
       End Try
    End Function

    Public Shared Sub RevertSketchChanges(db As Data.Database, parcelId As String)

        Dim affectedRows = 0
        Dim sketchCommandField = IIf( ClientSettings.PropertyValue("SketchVectorCommandField") isNot Nothing,  ClientSettings.PropertyValue("SketchVectorCommandField"), SketchSettingsPropertyValue("SketchVectorCommandField") )
        Dim sketchAreaField = IIf( ClientSettings.PropertyValue("SketchAreaField") isNot Nothing,  ClientSettings.PropertyValue("SketchAreaField"), SketchSettingsPropertyValue("SketchAreaField") )
        Dim sketchVectorTable = IIf( ClientSettings.PropertyValue("SketchVectorTable") isNot Nothing,  ClientSettings.PropertyValue("SketchVectorTable"), SketchSettingsPropertyValue("SketchVectorTable") )
        Dim targetTable = DataSource.FindTargetTable(db, sketchVectorTable)

        'Dim dt As DataTable = db.GetDataTable("SELECT MIN(OriginalValue)As OriginalValue ,MIN(UpdatedTime),AuxROWUID  FROM ParcelChanges pc INNER JOIN DataSourceField d ON d.Id=pc.FieldId WHERE d.Name={0} AND ParcelId={1}  group by AuxROWUID".SqlFormatString(sketchCommandField, parcelId))
        Dim dt As DataTable = db.GetDataTable("SELECT p.OriginalValue ,p.AuxROWUID FROM parcelchanges p INNER JOIN DataSourceField d ON d.Id=p.FieldId INNER JOIN (SELECT MIN(UpdatedTime)As UpdatedTime,AuxROWUID  FROM ParcelChanges pc INNER JOIN DataSourceField d ON d.Id=pc.FieldId WHERE d.Name={0} AND ParcelId={1}  group by AuxROWUID )As c ON c.AuxROWUID=p.AuxROWUID AND p.UpdatedTime=c.UpdatedTime WHERE d.Name={0} AND ParcelId={1}".SqlFormatString(sketchCommandField, parcelId))
        For Each dr As DataRow In dt.Rows
            Dim originalValue = dr.GetString("OriginalValue")
            Dim RowUID = dr.GetString("AuxROWUID")
            db.Execute("UPDATE " + targetTable + " SET " + sketchCommandField + "={0} WHERE ROWUID={1}".SqlFormatString(originalValue, RowUID))
            affectedRows += 1
        Next

        Dim dtArea As DataTable = db.GetDataTable("SELECT p.OriginalValue, p.AuxROWUID FROM parcelchanges p INNER JOIN DataSourceField d ON d.Id=p.FieldId INNER JOIN (SELECT MIN(UpdatedTime)As UpdatedTime,AuxROWUID  FROM ParcelChanges pc INNER JOIN DataSourceField d ON d.Id=pc.FieldId WHERE d.Name={0} AND ParcelId={1}  group by AuxROWUID )As c ON c.AuxROWUID=p.AuxROWUID AND p.UpdatedTime=c.UpdatedTime WHERE d.Name={0} AND ParcelId={1}".SqlFormatString(sketchAreaField, parcelId))
        For Each dr As DataRow In dtArea.Rows
            Dim originalValue = dr.GetString("OriginalValue")
            Dim RowUID = dr.GetString("AuxROWUID")
            db.Execute("UPDATE " + targetTable + " SET " + sketchAreaField + "={0} WHERE ROWUID={1}".SqlFormatString(originalValue, RowUID))
        Next

    End Sub

End Class
