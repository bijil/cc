﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects

Public Class Parcel

    Public Enum TaskPriority
        Proximity = 0
        Normal = 1
        Medium = 2
        High = 3
        Urgent = 4
        Critical = 5
    End Enum

    Public Shared ReadOnly Property EnableNewPriorities As Boolean
        Get
            Dim EnableNewPriority = False
            Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM ClientSettings WHERE Name = 'EnableNewPriorities' AND Value = 1")
            If dr IsNot Nothing Then
                EnableNewPriority = True
            End If
            Return EnableNewPriority
        End Get
    End Property

    Public Shared Sub UpdateParcelField(parcelId As Integer, fieldName As String, value As Object)
        UpdateParcelField(Database.Tenant, parcelId, fieldName, value)
    End Sub

    Public Shared Sub UpdateParcelField(db As Database, parcelId As Integer, fieldName As String, value As Object)
        Dim sqlValue As String = value.ToString.ToSqlValue
        Select Case value.GetType
            Case GetType(Date)
                sqlValue = "'" + CType(value, Date).ToString("yyyy-MM-dd HH:mm:ss") + "'"
            Case GetType(Integer)
                sqlValue = value.ToString
            Case GetType(Single)
                sqlValue = value.ToString
            Case GetType(Double)
                sqlValue = value.ToString
            Case Else
                If value = "" Then
                    sqlValue = "NULL"
                End If
        End Select

        db.Execute("UPDATE Parcel SET [" + fieldName + "] = " + sqlValue + " WHERE Id = " + parcelId.ToString)
    End Sub

    Public Shared Sub ResetQCStatus(db As Database, parcelId As Integer)
        db.Execute("UPDATE Parcel SET QC = NULL, QCDate = NULL, QCBy = NULL,StatusFlag = 0 WHERE Id = " + parcelId.ToString)
    End Sub

    Public Shared Sub ElevatePriorityIfNormal(db As Database, parcelId As Integer, loginId As String)
        Dim prty = 1
        If EnableNewPriorities Then
            prty = 3
        End If
        Dim changes As Integer = db.Execute("UPDATE Parcel SET Priority = " + prty.ToString + ",Committed=0 WHERE (Priority = 0 OR Priority IS NULL) AND Id = " + parcelId.ToString)
        If changes > 0 Then
            db.Execute("UPDATE Neighborhood SET TotalPriorities = COALESCE(TotalPriorities, 0) + 1, Completed = 0 WHERE Id IN (SELECT NeighborhoodId FROM Parcel WHERE Id = {0})".FormatString(parcelId))
            If ParcelEventLog.Enabled Then
                ParcelEventLog.CreatePriorityEvent(db, ParcelEventLog.ApplicationType.MA, parcelId, 1, loginId)
            End If
        End If
    End Sub

    Public Shared Sub ResetReviewStatus(db As Database, parcelId As Integer, loginId As String)
        Dim sql As String = ""
        sql += "UPDATE Parcel SET Reviewed = 0, ReviewedBy = NULL, ReviewDate = NULL, QC = NULL, QCDate = NULL, QCBy = NULL,Committed=0 , StatusFlag = NULL WHERE Id = " + parcelId.ToString + vbNewLine
        sql += "UPDATE Neighborhood SET Completed = 0 WHERE Id IN (SELECT NeighborhoodId FROM Parcel WHERE Id = {0} AND Priority <> 0)".FormatString(parcelId)
        db.Execute(sql)

        If ParcelEventLog.Enabled Then
            ParcelEventLog.CreateFlagEvent(db, ParcelEventLog.EventType.UnReviewed, ParcelEventLog.ApplicationType.MA, parcelId, loginId)
        End If
    End Sub

    Public Shared Function RollbackQCstatus(db As Database, parcelId As Integer)
        Return db.GetIntegerValue("UPDATE Parcel SET Reviewed = 0, Priority = 4, QC = 0, QCDate = NULL,Committed=0 , StatusFlag = NULL WHERE Id = " + parcelId.ToString() + ";SELECT CAST(@@ROWCOUNT  AS INT) As recordAffected")
    End Function

    Public Shared Sub SetPriority(parcelId As Integer, loginId As String, priority As TaskPriority, alertMessage As String)
        Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM Parcel WHERE Id = " & parcelId)
        Dim oldPriority As Integer = dr.GetInteger("Priority")
        Dim oldMessage As String = dr.GetString("ParcelAlert")

        If priority = TaskPriority.Normal Then alertMessage = ""

        If priority <> oldPriority Or oldMessage <> alertMessage Then
            Parcel.UpdateParcelField(parcelId, "Priority", priority.GetHashCode)
            Parcel.UpdateParcelField(parcelId, "ParcelAlert", alertMessage)

            Dim eventDescription As String = "Parcel Priority changed to "
            If EnableNewPriorities Then
                Select Case priority
                    Case TaskPriority.Proximity
                        eventDescription += "Proximity"
                    Case TaskPriority.Normal
                        eventDescription += "Normal"
                    Case TaskPriority.Medium
                        eventDescription += "Medium"
                    Case TaskPriority.High
                        eventDescription += "High"
                    Case TaskPriority.Urgent
                        eventDescription += "Urgent"
                    Case TaskPriority.Critical
                        eventDescription += "Critical"
                End Select
            Else
                Select Case priority
                    Case 0
                        eventDescription += "Normal"
                    Case 1
                        eventDescription += "High"
                    Case 2
                        eventDescription += "Urgent"
                End Select
            End If



            ParcelAuditStream.CreateFlagEvent(Database.Tenant, DateTime.UtcNow, parcelId, loginId, eventDescription, "")
        End If
    End Sub

    Public Shared Function GetParcelId(ByVal target As Data.Database, keys As List(Of KeyValuePair(Of String, String))) As String
        Dim i As Integer = 0

        Dim datafilter As String = String.Empty
        For Each keyvalue In keys
            If (datafilter <> "") Then
                datafilter += " AND "
                i += 1
            End If
            datafilter += keyvalue.Key + " = " + keyvalue.Value.ToSqlValue
        Next
        Dim sql = "SELECT CC_ParcelId FROM parceldata WHERE " + datafilter
        Dim parcelId = target.GetStringValue(sql)
        Return IIf(parcelId = "", "0", parcelId)
    End Function

    Public Shared Sub importParcelSettingsFromXML(cat As XElement)

        Dim updateColumns = "ParcelTable=" + cat.@ParcelTable.ToSqlValue
        Dim KeyColumns = ""
        For i = 1 To cat.Attributes.Count - 1
            If KeyColumns <> "" Then KeyColumns += ","
            KeyColumns += "KeyField" + i.ToString() + "=" + cat.Attribute("KeyField" + i.ToString()).Value.ToString().ToSqlValue
        Next
        updateColumns += "," + KeyColumns
        Database.Tenant.Execute("UPDATE Application SET " + updateColumns)

    End Sub

    Public Shared Function GetPriority(parcelId As Integer)
        Return Database.Tenant.GetIntegerValue("SELECT CASE Priority WHEN NULL THEN 0 ELSE Priority END As Priority FROM Parcel WHERE Id={0} ".SqlFormatString(parcelId))
    End Function

    Public Shared Function CommitChanges(ByVal db As Data.Database, parcelId As Integer) As Integer

        Dim tableScript As XElement = <sql>
                CREATE TABLE [dbo].[archive_ParcelChanges](
	            [ROWUID] [int] IDENTITY(1,1) NOT NULL,
                [ChangeId] [int],
	            [ParcelId] [int] NULL,
	            [FieldId] [int] NULL,
	            [ReviewedBy] [varchar](50) NULL,
	            [OriginalValue] [varchar](max) NULL,
	            [NewValue] [varchar](max) NULL,
	            [Latitude] [numeric](11, 8) NULL,
	            [Longitude] [numeric](11, 8) NULL,
	            [ChangedTime] [datetime] NULL,
	            [UpdatedTime] [datetime] NOT NULL,
	            [LocationUpdatedTime] [datetime] NULL,
	            [AuxROWUID] [int] NULL,
	            [QCChecked] [bit] NOT NULL,
	            [QCApproved] [bit] NOT NULL,
	            [QCApprovedTime] [datetime] NULL,
	            [ParentAuxROWUID] [int] NULL,
	            [Action] [varchar](20) NULL,
	            [DownSynced] [bit] NOT NULL,SyncID [varchar](50) NULL)
            </sql>

        If db.DoesTableExists("archive_ParcelChanges") = False Then
            db.Execute(tableScript)
            If db.DoesTableExists("Temp_ParcelChanges") Then
                db.Execute("INSERT INTO archive_ParcelChanges(ParcelId,FieldId,ReviewedBy,OriginalValue,NewValue,Latitude,Longitude,ChangedTime,UpdatedTime,LocationUpdatedTime,AuxROWUID,QCChecked,QCApproved,QCApprovedTime,ParentAuxROWUID,Action,DownSynced,SyncID) SELECT ParcelId,FieldId,ReviewedBy,OriginalValue,NewValue,Latitude,Longitude,ChangedTime,UpdatedTime,LocationUpdatedTime,AuxROWUID,QCChecked,QCApproved,QCApprovedTime,ParentAuxROWUID,Action,DownSynced,SyncID FROM Temp_ParcelChanges ORDER BY Id")
                db.DropTable("Temp_ParcelChanges")
            End If
        End If
        db.Execute("INSERT INTO archive_ParcelChanges(ChangeId, ParcelId,FieldId,ReviewedBy,OriginalValue,NewValue,Latitude,Longitude,ChangedTime,UpdatedTime,LocationUpdatedTime,AuxROWUID,QCChecked,QCApproved,QCApprovedTime,ParentAuxROWUID,Action,DownSynced,SyncID) SELECT Id, ParcelId,FieldId,ReviewedBy,OriginalValue,NewValue,Latitude,Longitude,ChangedTime,UpdatedTime,LocationUpdatedTime,AuxROWUID,QCChecked,QCApproved,QCApprovedTime,ParentAuxROWUID,Action,DownSynced,SyncID FROM ParcelChanges WHERE ParcelId={0} AND DownSynced=1".SqlFormatString(parcelId))
        Try
            db.Execute("EXEC cc_CommitParcelChanges " & parcelId & ", 0,'',0")
        Catch ex As Exception

        End Try

        Dim sql = "DELETE FROM ParcelChanges WHERE ParcelId={0} AND DownSynced=1 ;SELECT CAST(@@ROWCOUNT  AS INT) As recordAffected".SqlFormatString(parcelId)
        Dim recordAffected As Integer = db.GetIntegerValue(sql)

        db.Execute("UPDATE Parcel SET FailedOnDownSync = 0, FailedOnDownSyncStatus = 0, DownSyncRejectReason = NULL ,Committed = 1 , StatusFlag = 3 WHERE Id = " & parcelId)
        Return recordAffected
    End Function

    Public Shared Function UpdateKeys(ByVal db As Data.Database, parcelId As Integer, parcelKeys As KeyValuePair(Of String, String)) As Integer

        Try
            Dim UpdateparcelSql = ""
            Dim updateColumns = ""
            Dim recordAffected As Integer
            Dim sql = "SELECT * FROM Parcel  WHERE ID = " & parcelId & " And [KeyValue1] = '0'"
            Dim count As Integer = db.GetIntegerValue(sql)
            If count > 0 Then
                Dim upParcelUpdate As String = "UPDATE Parcel SET [KeyValue1] = '" + parcelKeys.Value + "'  WHERE ID= " & parcelId & " AND [KeyValue1] = '0' ;SELECT CAST(@@ROWCOUNT  AS INT)"
                recordAffected += db.GetIntegerValue(upParcelUpdate)
                updateColumns += "UPDATE ParcelData SET [" + parcelKeys.Key + "] = '" + parcelKeys.Value + "' WHERE CC_ParcelId=" & parcelId & " ;SELECT CAST(@@ROWCOUNT  AS INT)"
                recordAffected += db.GetIntegerValue(updateColumns)
                Try
                    db.Execute("EXEC ccad_FillMetadataMissing 0," & parcelId & " ")
                Catch ex As Exception

                End Try
            End If
        Catch ex As Exception
            Throw ex
        End Try


    End Function



    Public Shared Function CommitTrackedChanges(ByVal db As Data.Database, parcelId As Integer, TrackedValue As String, TrackedFieldValues As List(Of TrackingField)) As Integer

        Dim tableScript As XElement = <sql>
                CREATE TABLE [dbo].[archive_ParcelChanges](
	            [ROWUID] [int] IDENTITY(1,1) NOT NULL,
                [ChangeId] [int],
	            [ParcelId] [int] NULL,
	            [FieldId] [int] NULL,
	            [ReviewedBy] [varchar](50) NULL,
	            [OriginalValue] [varchar](max) NULL,
	            [NewValue] [varchar](max) NULL,
	            [Latitude] [numeric](11, 8) NULL,
	            [Longitude] [numeric](11, 8) NULL,
	            [ChangedTime] [datetime] NULL,
	            [UpdatedTime] [datetime] NOT NULL,
	            [LocationUpdatedTime] [datetime] NULL,
	            [AuxROWUID] [int] NULL,
	            [QCChecked] [bit] NOT NULL,
	            [QCApproved] [bit] NOT NULL,
	            [QCApprovedTime] [datetime] NULL,
	            [ParentAuxROWUID] [int] NULL,
	            [Action] [varchar](20) NULL,
	            [DownSynced] [bit] NOT NULL,SyncID [varchar](50) NULL)
            </sql>

        If db.DoesTableExists("archive_ParcelChanges") = False Then
            db.Execute(tableScript)
            If db.DoesTableExists("Temp_ParcelChanges") Then
                db.Execute("INSERT INTO archive_ParcelChanges(ParcelId,FieldId,ReviewedBy,OriginalValue,NewValue,Latitude,Longitude,ChangedTime,UpdatedTime,LocationUpdatedTime,AuxROWUID,QCChecked,QCApproved,QCApprovedTime,ParentAuxROWUID,Action,DownSynced,SyncID) SELECT ParcelId,FieldId,ReviewedBy,OriginalValue,NewValue,Latitude,Longitude,ChangedTime,UpdatedTime,LocationUpdatedTime,AuxROWUID,QCChecked,QCApproved,QCApprovedTime,ParentAuxROWUID,Action,DownSynced,SyncID FROM Temp_ParcelChanges ORDER BY Id")
                db.DropTable("Temp_ParcelChanges")
            End If
        End If
        db.Execute("INSERT INTO archive_ParcelChanges(ChangeId, ParcelId,FieldId,ReviewedBy,OriginalValue,NewValue,Latitude,Longitude,ChangedTime,UpdatedTime,LocationUpdatedTime,AuxROWUID,QCChecked,QCApproved,QCApprovedTime,ParentAuxROWUID,Action,DownSynced,SyncID) SELECT pc.Id, pc.ParcelId,pc.FieldId,ReviewedBy,OriginalValue,NewValue,Latitude,Longitude,ChangedTime,UpdatedTime,LocationUpdatedTime,AuxROWUID,QCChecked,QCApproved,QCApprovedTime,ParentAuxROWUID,Action,DownSynced,SyncID FROM ParcelChanges pc INNER JOIN ParcelChangesTracking pt on pt.ChangeID=pc.Id   
                    WHERE pc.ParcelId={0} AND DownSynced=1 AND pt.Value={1} ".SqlFormatString(parcelId, TrackedValue))
        Try
            db.Execute("EXEC cc_CommitParcelChanges " & parcelId & ", 0," & TrackedValue & ",1")
        Catch ex As Exception

        End Try

        Dim sql = "DELETE pc FROM ParcelChanges pc INNER JOIN ParcelChangesTracking pt on pt.ChangeID=pc.Id  WHERE pc.ParcelId={0} AND pt.Value={1} And DownSynced=1 ;Select CAST(@@ROWCOUNT  As INT) As recordAffected".SqlFormatString(parcelId, TrackedValue)
        Dim recordAffected As Integer = db.GetIntegerValue(sql)
        Return recordAffected
    End Function

    Public Shared Function GetKeyValue1(ByVal db As Data.Database, ByVal parcelId As Integer) As String
        Dim sql = "Select KeyValue1 FROM Parcel where Id={0}"
        Return db.GetStringValue(sql.SqlFormatString(parcelId))
    End Function

    Public Shared Sub DeleteAllNewRecords(ByVal db As Data.Database, ByVal parcelId As Integer)
        For Each tableName As String In db.GetDataTable("Select TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME Like 'XT__%'").Select().Select(Function(x) x.GetString("TABLE_NAME"))
            Dim deleteSql As String = "DELETE FROM " + tableName + " WHERE CC_RecordStatus = 'I' AND CC_ParcelId = {0}".SqlFormat(False, parcelId)
            db.Execute(deleteSql)
        Next
    End Sub
End Class
