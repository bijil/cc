﻿Public Class SketchDataProcessor

    Public Shared Function UpdateSketch(db As Database, parcelId As Integer, loginId As String, ByRef rowUID As Long, sketchFields As String, sketchData As String, eventDate As Date, latitude As Single, longitude As Single, ByRef errorMessage As String, Optional SyncID As String = "", Optional appraiserType As String = "", Optional isMA As String = "") As String		
    	Dim type As String = "vector"
        If sketchData.Contains("$note$") Then
            type = "note"
            sketchData = sketchData.Replace("$note$", "")
        End If
        Dim fieldParts = sketchFields.Split("|")
        Dim dataParts = sketchData.Split("|")
        Dim sql As String
        Dim changeAction As String = "edit"
        Dim sketchVectorTable As String

        Dim fields As DataTable = db.GetDataTable("SELECT * FROM DataSourceField WHERE Id IN " + String.Join(", ", fieldParts).Wrap("()"))

        If fields.Rows.Count = 0 Then
            Throw New Exception("Invalid fields used in sketch configuration.")
        End If

        sketchVectorTable = fields(0).GetString("SourceTable")
        Dim sketchVectorTargetTable = DataSource.FindTargetTable(db, sketchVectorTable)

        Dim dataFields = fields.AsEnumerable.Select(Function(x) New With {.Name = x.GetString("Name"), .ID = x.GetInteger("ID")}) _
                         .ToDictionary(Of Integer, String)(Function(x) x.ID, Function(y) y.Name)


        If fieldParts.Length <> dataParts.Length Then
            Throw New Exception("Invalid UPDATESKETCH call. Number of fields and data elements do not match")
        End If

        If rowUID < 0 Then
            rowUID = db.GetIntegerValue("SELECT ROWUID FROM  " + sketchVectorTargetTable + "  WHERE ClientROWUID = " + rowUID.ToString)
            If rowUID <= 0 Then
                errorMessage = "Attempt to update sketch data to a non-existent record."
                'Return errorMessage
                Return ""
            End If
            changeAction = "new"
        End If

        For Each dp In dataParts
            If dp = "*DEL*" Then
                changeAction = "delete"
            End If
        Next
        Dim rec As DataRow = db.GetTopRow("SELECT * FROM  " + sketchVectorTargetTable + "  WHERE ROWUID = " + rowUID.ToString)
        If rec Is Nothing Then
            errorMessage = "Attempt to update sketch data to a non-existent record."
            'Return errorMessage
            Return ""
        End If

        If changeAction = "edit" AndAlso rec.GetString("CC_RecordStatus") = "I" Then
            changeAction = "new"
        End If

        Dim parentRowUID = rec.GetString("ParentROWUID")
		Dim CC_year_st As String = ""
		If sketchVectorTargetTable <> "ParcelData" Then
			CC_year_st = rec.GetString("CC_YearStatus")
		End If
        Dim updateSetData As String = "", changeId As Integer
        Dim isQCRes As DataRow = db.GetTopRow("SELECT top 1 QC FROM Parcel WHERE Id = {0}".SqlFormat(True, parcelId))
        Dim isQC As Integer = If((Not IsDbNull(isQCRes("QC")) AndAlso isQCRes("QC") = True AndAlso appraiserType <> ""), 1, 0)
        Dim changeSql As String = "INSERT INTO ParcelChanges  (ParcelId, FieldId, Action, ReviewedBy, OriginalValue, NewValue, ChangedTime, Latitude, Longitude, AuxROWUID, ParentAuxROWUID, SyncID, CC_YearStatus, QCChecked, QCApproved) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}); SELECT CAST(@@IDENTITY AS INT) As NewId;"
        For i As Integer = 0 To fieldParts.Length - 1
            Dim fieldId As String = fieldParts(i)
            If IsNumeric(fieldId) AndAlso CInt(fieldId) > -1 Then
                Dim fieldName As String = dataFields(fieldId)
                Dim newValue As String = dataParts(i)
                Dim oldValue As String = rec.GetString(fieldName)
                Dim currentValue As String = String.Empty
                Dim changeRow As DataRow = db.GetTopRow("select top 1 newvalue from parcelchanges where parcelid={0} and fieldid={1} and auxrowuid={2} and action in('new','edit') order by ChangedTime desc".SqlFormat(True, parcelId, fieldId, rowUID))
                If (changeRow Is Nothing) Then
                    currentValue = oldValue
                Else
                    currentValue = changeRow.GetString("newvalue")
                End If
                If db.GetDataType(sketchVectorTargetTable, fieldName) = GetType(Integer) Then
                    Dim intValue As Integer = 0
                    If IsNumeric(newValue) Then
                        intValue = CInt(newValue)
                        newValue = intValue.ToString
                    End If
                End If
                If (changeAction = "delete" And newValue <> "*DEL*") Then
                    Continue For
                End If

                If changeAction <> "delete" Then
                    updateSetData.Append(fieldName.Wrap("[]") + " = " + newValue.ToSqlValue, ", ")
                End If
                If type = "note" And changeAction = "delete" Then
                    newValue = ""
                    oldValue = ""
                End If
                newValue = db.GetStringValue("EXEC CC_ProcessAndFillColumnValue {0},{1}".SqlFormat(True, fieldName, newValue))
                changeId = db.GetIntegerValue(changeSql.SqlFormat(True, parcelId, fieldId, changeAction, loginId, oldValue, newValue, eventDate, latitude, longitude, rowUID, parentRowUID, SyncID, CC_year_st, isQC, isQC))
                db.Execute("EXEC processFieldChangesTracking {0},{1},{2},{3}".SqlFormat(True, parcelId, fieldId, changeId, newValue))
                If (currentValue <> newValue And changeAction <> "delete") Then
                    ParcelAuditStream.CreateDataChangeEvent(db, eventDate, parcelId, rowUID, loginId, fieldId, newValue, changeId, appraiserType)
                End If
                If type = "note" And changeAction = "delete" Then
                    Exit For
                End If
            End If
        Next

        If changeAction = "delete" Then
            ''sql = "UPDATE  " + sketchVectorTargetTable + " SET CC_Deleted = 1 WHERE ROWUID = " + rowUID.ToString
            ''db.Execute(sql)
            ''If (appraiserType = "DTR" Or appraiserType = "QC" Or appraiserType = "SV") Then
             ''   appraiserType = "by " + appraiserType
            ''End If
            ''ParcelAuditStream.CreateFlagEvent(db, eventDate, parcelId, loginId, "Aux Record #" + rowUID.ToString() + " deleted " + appraiserType + " from " + sketchVectorTable)
            Dim eventDescription As String = ""
            If isMA = "1" Then
            	eventDescription = MobileSyncUploadProcessor._deleteRecordSketchMA(db, parcelId, loginId, rowUID, sketchVectorTable, eventDate, latitude, longitude, SyncID)
            	If eventDescription <> "" Then
                    ParcelAuditStream.CreateFlagEvent(db, eventDate, parcelId, loginId, eventDescription, "MA")
                End If
            Else
		        QCProcessor.DeleteRecord(db, parcelId, loginId, rowUID, sketchVectorTable, 0, 0, eventDate, appraiserType)
            End If
        End If



        If (appraiserType <> "DTR" And appraiserType <> "QC" And appraiserType <> "SV") Then
            Parcel.ElevatePriorityIfNormal(db, parcelId, loginId)
            Parcel.ResetReviewStatus(db, parcelId, loginId)
        End If


        Return type

        ''' OLD CODE
        'Dim commandFieldId As Integer = fieldParts(0)
        'Dim areaFieldId As Integer = -1
        'Dim perimeterFieldId As Integer = -1
        'Dim sketchCmds As String = dataParts(0)
        'Dim area As Single = 0
        'Dim perimeter As Single = 0

        'If fieldParts.Length > 1 AndAlso dataParts.Length > 1 Then
        '    areaFieldId = fieldParts(1)
        '    area = dataParts(1)
        'End If

        'If fieldParts.Length > 2 AndAlso dataParts.Length > 2 Then
        '    perimeterFieldId = fieldParts(2)
        '    perimeter = dataParts(2)
        'End If

        'Dim oldAreaValue As String = "", oldPerimeterValue As String = ""
        'Dim sketchFieldRow = db.GetTopRow("SELECT * FROM DataSourceField WHERE Id = " & commandFieldId)

        'Dim commandFieldName As String = sketchFieldRow.GetString("Name")
        'Dim areaFieldName As String = "", perimeterFieldName As String = ""
        'If areaFieldId <> -1 Then
        '    areaFieldName = db.GetTopRow("SELECT * FROM DataSourceField WHERE Id = " & areaFieldId).GetString("Name")
        'End If
        'If perimeterFieldId <> -1 Then
        '    perimeterFieldName = db.GetTopRow("SELECT * FROM DataSourceField WHERE Id = " & perimeterFieldId).GetString("Name")
        'End If


        'Dim parentRowUID As String

        'If rowUID < 0 Then
        '    rowUID = db.GetIntegerValue("SELECT ROWUID FROM  " + sketchVectorTargetTable + "  WHERE ClientROWUID = " + rowUID.ToString)
        '    'If rowUID = 0 Then
        '    '    Throw New Exception("Sketch update failed since the target row does not exist.")
        '    'End If
        '    changeAction = "new"
        'End If

        'If rowUID > 0 Then
        '    Dim rec As DataRow = db.GetTopRow("SELECT * FROM  " + sketchVectorTargetTable + "  WHERE ROWUID = " + rowUID.ToString)
        '    Dim oldValue = rec.GetString(commandFieldName)
        '    If areaFieldId <> -1 Then oldAreaValue = rec.GetString(areaFieldName)
        '    If perimeterFieldId <> -1 Then oldPerimeterValue = rec.GetString(perimeterFieldName)


        '    Dim updateSetData = commandFieldName + " = " + sketchCmds.ToSqlValue
        '    If areaFieldId <> -1 Then
        '        updateSetData += ", " + areaFieldName + " = " + area.ToString
        '    End If
        '    If perimeterFieldId <> -1 Then
        '        updateSetData += ", " + perimeterFieldName + " = " + perimeter.ToString
        '    End If
        '    sql = "UPDATE  " + sketchVectorTargetTable + "     SET " + updateSetData + " WHERE ROWUID = " + rowUID.ToString
        'Else
        '    errorMessage = "Attempt to update sketch data to a non-existent record."
        '    Return False
        'End If
        'db.Execute(sql)


        'Dim changeId As Integer = db.GetIntegerValue(changeSql.SqlFormat(True, parcelId, commandFieldId, changeAction, loginId, oldValue, sketchCmds, eventDate, latitude, longitude, rowUID, parentRowUID))

        'If areaFieldId <> -1 Then
        '    changeId = db.GetIntegerValue(changeSql.SqlFormat(True, parcelId, areaFieldId, changeAction, loginId, oldAreaValue, area, eventDate, latitude, longitude, rowUID, parentRowUID))
        'End If
        'If perimeterFieldId <> -1 Then
        '    changeId = db.GetIntegerValue(changeSql.SqlFormat(True, parcelId, perimeterFieldId, changeAction, loginId, oldPerimeterValue, perimeter, eventDate, latitude, longitude, rowUID, parentRowUID))
        'End If


    End Function

End Class
