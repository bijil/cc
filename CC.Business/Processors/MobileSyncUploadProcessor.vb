﻿Imports System.Transactions
Imports CAMACloud.BusinessLogic.RemoteIntegration
Imports System.Text
Imports System.IO
Imports System.Web
Imports System.Configuration
Imports System.Data.SqlClient

Public Class MobileSyncUploadProcessor

    Shared lockType As String = "ROWLOCK"

    Public Shared ReadOnly Property EnableNewPriorities As Boolean
        Get
            Dim EnableNewPriority = False
            Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM ClientSettings WHERE Name = 'EnableNewPriorities' AND Value = 1")
            If dr IsNot Nothing Then
                EnableNewPriority = True
            End If
            Return EnableNewPriority
        End Get
    End Property

    Public Shared Sub EnqueueParcelChangeData(userName As String, data As String, lat As Single, lon As Single, Optional archive As Boolean = True)
        Dim sqlUserName As String = userName.ToSqlValue
        If data.IsNotEmpty Then
            Dim batchId As Integer = 0
            Dim latitude, longitude As Single
            Dim batchData As String = data
            Single.TryParse(lat, latitude)
            Single.TryParse(lon, longitude)

            Dim batchSql As String = "INSERT INTO UserParcelChangeBatch (LoginId, Latitude, Longitude, BatchData) VALUES ({0}, {1}, {2}, {3}); SELECT CAST(@@IDENTITY AS INT) As NewId;"
            batchId = Database.Tenant.GetIntegerValue(batchSql.FormatString(sqlUserName, latitude, longitude, batchData.ToSqlValue))
            
            'Batch file creation-----------------------------------------------------------------------------
            Try
	            Dim orgId As Integer = HttpContext.Current.GetCAMASession.OrganizationId
	            Dim sLocation As String = ConfigurationManager.appSettings("BatchLogLocation") + "\" + orgId.ToString()
			    If Not Directory.Exists(sLocation) Then
			    	Directory.CreateDirectory(sLocation)
			    End If			    
	            sLocation = sLocation + "\" + userName + "-" + batchId.ToString() + ".txt"
	            Dim fs As New StreamWriter(sLocation)
	            fs.WriteLine(batchData)
    			fs.Close()
            Catch ex As Exception
            	
            End Try
			'End -----------------------------------------------------------------------------
	
            data = data.Trim
            Dim lines As String() = data.Split(vbLf)

            Dim splitLines As New List(Of String)

            For Each line As String In lines
                Dim v = line.Trim.Split("|")
                Dim isLine As Boolean = True
                If v.Length = 5 Or v.Length = 6 Or v.Length = 7 Or v.Length = 8 Or v.Length = 9 Then
                    Try
                        Dim timestamp As Long = CLng(v(0))
                    Catch ex As Exception
                        isLine = False
                    End Try
                Else
                    isLine = False
                    Try
                        Dim timestamp As Long = CLng(v(0))
                        isLine = True
                    Catch ex As Exception
                        isLine = False
                    End Try
                End If
                If isLine Then
                    splitLines.Add(line)
                Else
                    Dim lastLine = splitLines(splitLines.Count - 1)
                    lastLine += vbNewLine + line
                    splitLines(splitLines.Count - 1) = lastLine
                End If
            Next

            MobileSyncUploadProcessor.ProcessParcelChangeData(batchId, userName, splitLines.ToArray, lat, lon, archive)

            Dim lineCount As Integer = splitLines.Count
            UserAuditTrail.CreateEvent(userName, UserAuditTrail.ApplicationType.MobileAssessor, UserAuditTrail.EventType.UploadChanges, "Uploaded " & lineCount & " mobile changes.", lineCount)
        Else
            MobileSyncUploadProcessor.ProcessParcelChangeBatches(userName, False)
        End If
    End Sub

    Public Shared Sub ProcessParcelChangeData(batchId As Integer, loginId As String, lines() As String, latitude As Long, longitude As Long, Optional archive As Boolean = True)

        If Not Database.Tenant.DoesTableExists("archive_UserParcelChanges") Then
            Database.Tenant.Execute("CREATE TABLE archive_UserParcelChanges (SyncId VARCHAR(50) PRIMARY KEY, BatchId INT, Data VARCHAR(MAX), SyncDate DATETIME DEFAULT GETUTCDATE(), ExecStatus Bit DEFAULT 0);")
        End If

        Dim changeLogSql As String = "INSERT INTO UserParcelChanges (BatchId, Data, SyncId) VALUES ({0}, {1}, {2})"
        Dim archiveInsertSql As String = "INSERT INTO archive_UserParcelChanges (BatchId, Data, SyncId) VALUES ({0}, {1}, {2})"
        For Each line As String In lines
            Dim syncId As String = loginId + "-" + line.Split("|")(0)
            Dim archived As Boolean = False
            If archive Then
                Try
                    Database.Tenant.Execute(archiveInsertSql, batchId, line.ToSqlValue, syncId.ToSqlValue)
                    archived = True
                Catch exx As System.Data.SqlClient.SqlException
                    archived = False
                    'Possibly archived earlier, and got a Primary Exception now because of duplicate records.
                    'Trap done to eliminate ghost records
                Catch ex As Exception
                    archived = False
                End Try
            Else
                archived = True
            End If


            If archived Then
                Dim retries As Integer = 2 'FD_25863, added a retry if deadlock occured in UserParcelChanges insert and removes the archive_UserParcelChanges based on syncId entry.
                Dim retryCount As Integer = 0
                Do
                    Try
                        Database.Tenant.Execute(changeLogSql, batchId, line.ToSqlValue, syncId.ToSqlValue)
                        Exit Do
                    Catch ex As SqlException When ex.Number = 1205 AndAlso (retryCount + 1) < retries
                        retryCount += 1
                    Catch ex As Exception
                        Database.Tenant.Execute("DELETE FROM archive_UserParcelChanges WHERE SyncId = {0}", syncId.ToSqlValue)
                        Throw New Exception("Insert into UserParcelChanges table failed!")
                    End Try
                Loop While retryCount < retries
            End If
        Next
        ProcessParcelChangeBatches(loginId, False)
    End Sub

    Public Shared Sub ProcessParcelChangeBatches(loginId As String, Optional useTransaction As Boolean = True)

		Dim batchFilter As String = ""
		Dim batchFileLocation As String = ConfigurationManager.appSettings("BatchLogLocation") + "\" + HttpContext.Current.GetCAMASession.OrganizationId.ToString()
        If Not useTransaction Then
            batchFilter = "AND ExecStatus NOT IN (1, 2)"
        End If

        Dim cdb As Database = Database.AltTenant
        Dim db As Database = Database.Tenant
        If Not db.DataCache.ContainsKey("TablesById") Then
            Dim dtTables = db.GetDataTable("SELECT * FROM DataSourceTable")
            Dim tablesById = dtTables.AsEnumerable.ToDictionary(Function(x) x.GetInteger("Id"), Function(x) x)
            Dim tablesByName = dtTables.AsEnumerable.ToDictionary(Function(x) x.GetString("Name"), Function(x) x)

            db.DataCache.Add("TablesById", tablesById)
            db.DataCache.Add("TablesByName", tablesByName)
        End If
        If Not db.DataCache.ContainsKey("FieldTableId") Then
            Dim dtFieldTable = db.GetDataTable("SELECT Id, TableId FROM DataSourceField")
            Dim fieldIdToTableId = dtFieldTable.AsEnumerable.ToDictionary(Function(x) x.GetInteger("Id"), Function(x) x.GetInteger("TableId"))
            db.DataCache.Add("FieldTableId", fieldIdToTableId)
        End If

        If db.Connection.State <> ConnectionState.Open Then db.Connection.Open()
        If useTransaction Then
            db.TransactionMode = True
            'Dim trans = db.Connection.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted)
            'db.CurrentTransaction = trans
            db.Execute("SET TRANSACTION ISOLATION LEVEL READ COMMITTED")
            db.Execute("BEGIN TRANSACTION")
        End If
        Dim batches As String = ""
        Dim sendErrorMail = True
        Try
            For Each batch As DataRow In cdb.GetDataTable("SELECT * FROM UserParcelChangeBatch WHERE LoginId = " & loginId.ToSqlValue & batchFilter & " ORDER BY Id").Rows
                If batches <> "" Then
                    batches += ","
                End If
                Dim batchId As Integer = batch.GetInteger("Id")
                batches &= batchId
                cdb.Execute("UPDATE UserParcelChangeBatch SET ExecStatus = 2 WHERE Id = " & batchId)
                Dim latitude As Single = batch.GetSingleWithDefault("Latitude", 0)
                Dim longitude As Single = batch.GetSingleWithDefault("Longitude", 0)

                Dim lineNo As Integer = 0
                Dim rowuid As Integer
                Try
                	Dim UpcBatchData = cdb.GetDataTable("SELECT * FROM UserParcelChanges WHERE BatchId = " & batchId & batchFilter & " ORDER BY ROWUID")
                	Dim UpcCount = UpcBatchData.Rows.Count
                    For Each change As DataRow In UpcBatchData.Rows
                        lineNo += 1
                        rowuid = change.GetInteger("ROWUID")
                        'cdb.Execute("UPDATE UserParcelChanges SET ExecStatus = 2 WHERE ROWUID = " & rowuid)

                        Dim syncID = change.GetString("SyncId")
                        _processParcelChangeDataLine(db, loginId, change.GetString("Data"), latitude, longitude, syncID)
                        cdb.Execute("UPDATE UserParcelChanges SET ExecStatus = 1 WHERE ROWUID = " & rowuid)
                        cdb.Execute("UPDATE archive_UserParcelChanges SET ExecStatus = 1 WHERE SyncId = {0}".SqlFormat(False,syncID))
                    Next
                    If UpcCount = lineNo Then
                    	cdb.Execute("UPDATE UserParcelChanges SET ExecStatus = 1 WHERE BatchId = " & batchId)
                    	cdb.Execute("UPDATE UserParcelChangeBatch SET ExecStatus = 1 WHERE Id = " & batchId)
                    	batchFileLocation = batchFileLocation + "\" + loginId + "-" + batchId.ToString() + ".txt"					
						If File.Exists(batchFileLocation) = True Then
							File.Delete(batchFileLocation)
						End If
                    Else
                    	Throw New Exception("An unknown error has occured while processing the batch")
                    End If
                    
                Catch ex As Exception
                    If sendErrorMail = True Then
                        ErrorMailer.ReportException("MA-Sync-Fail", ex, System.Web.HttpContext.Current.Request)
                    End If
                    sendErrorMail = False
                    cdb.Execute("UPDATE UserParcelChanges SET ExecStatus = 9, ErrorMessage = {0} WHERE ROWUID = " & rowuid, ex.Message.ToSqlValue)
                    cdb.Execute("UPDATE UserParcelChangeBatch SET ExecStatus = 9, ErrorLine = " & rowuid & " WHERE Id = " & batchId)
                    'ErrorMailer.ReportException("IMMEDIATE!-MA-SYNC-FAILED", ex, System.Web.HttpContext.Current.Request)
                    Throw ex
                End Try
            Next
            'For Each line As String In lines
            '    _processParcelChangeDataLine(db, loginId, line, latitude, longitude)
            'Next
            If useTransaction Then
                db.Execute("COMMIT TRANSACTION")
            End If
            If batches <> "" Then
                cdb.Execute("DELETE FROM UserParcelChanges WHERE ExecStatus = 1 AND BatchId IN (" + batches + ");")
                cdb.Execute("DELETE FROM UserParcelChangeBatch WHERE ExecStatus = 1 AND Id IN (" + batches + ");")
            End If
        Catch ex As Exception
            If useTransaction Then
                db.Execute("ROLLBACK TRANSACTION")

                'If Transaction is used, and is rolled back, then reset ExecBatches of even completed batches
                If batches <> "" Then
                    cdb.Execute("UPDATE UserParcelChangeBatch SET ExecStatus = 0 WHERE ExecStatus IN (1, 2) AND Id IN (" + batches + ");")
                End If
            End If

        End Try
        db.TransactionMode = False
        db.CurrentTransaction = Nothing
    End Sub

    Private Shared Sub _processParcelChangeDataLine(db As Database, loginId As String, data As String, latitude As Single, longitude As Single, MASyncID As String)


        Dim v = data.Trim.Split("|")

        Dim syncId As String = loginId + "-" + v(0)

        Dim timestamp As Long = CLng(v(0))
        Dim refid As String = v(1)
        Dim field As String = v(2)
        Dim oldvalue As String = v(3)
        Dim newvalue As String = v(4)
        Dim auxRowId As String = "NULL"
        If v.Length > 5 Then
            auxRowId = v(5)
        End If
        If auxRowId.Trim = "" Then
            auxRowId = "NULL"
        End If
        If auxRowId = "NULL" Or auxRowId = "" Then
            auxRowId = 0
        End If
        If v.Length > 6 AndAlso v(6) = "!" Then
            newvalue = HttpUtility.UrlDecode(decodeValueFromChangeString(newvalue))
            oldvalue = HttpUtility.UrlDecode(decodeValueFromChangeString(oldvalue))
        End If
        If v.Length > 8 Then ''lat and lng locations appending along each PCIs
        	Single.TryParse(v(7), latitude)
            Single.TryParse(v(8), longitude)
        End If
        
        
        '  Dim j As String = Encoding.ASCII.GetString("gdfg){").tostring
        ProcessParcelChange(db, loginId, timestamp, refid, auxRowId, field, oldvalue, newvalue, latitude, longitude, MASyncID)
    End Sub

    Private Shared Function decodeValueFromChangeString(str As String) As String
        Dim bytes = Convert.FromBase64String(str)
        Return System.Text.Encoding.Default.GetString(bytes).Trim
    End Function


    Public Shared Sub ProcessParcelChange(db As Database, loginId As String, timestamp As Long, refId As String, rowUID As String, fieldOrCommand As String, oldValueOrOption As String, newValue As String, latitude As Single, longitude As Single, SyncID As String)
        Dim eventDate As Date = #1/1/1970#
        If timestamp.ToString.Length > 13 Then
            timestamp = Long.Parse(timestamp.ToString.Substring(0, 13))
        End If
        eventDate = eventDate.AddMilliseconds(timestamp)
        If oldValueOrOption.Trim = "null" Then oldValueOrOption = ""

        Dim eventDescription As String = ""
        If fieldOrCommand <> "NBHD" And IsNumeric(refId) Then
            If (refId < 0) Then
                refId = db.GetIntegerValue("SELECT TOP 1 CC_ParcelID FROM ParcelData WHERE ClientROWUID={0}".SqlFormatString(refId.ToString()))
            End If
        End If
        'If fieldOrCommand Is Numeric, then it's a FieldId, and the corresponding change is a ParcelChange
        If IsNumeric(fieldOrCommand) Then
            _processParcelDataChange(db, loginId, eventDate, refId, rowUID, fieldOrCommand, oldValueOrOption, newValue, latitude, longitude, SyncID)
        ElseIf fieldOrCommand = "NBHD" Then
            _processNeighborhoodAction(db, loginId, eventDate, refId, oldValueOrOption, newValue)
        Else
            Dim parcelId As Integer = 0
            Dim nbhdId As Integer = 0
            If IsNumeric(refId) Then
                parcelId = refId
                nbhdId = db.GetIntegerValueOrInvalid("SELECT NeighborhoodId FROM Parcel WHERE Id = " + parcelId.ToString)
            End If

            Select Case fieldOrCommand.ToLower
                Case "selectedappraisaltype", "selectedvaluetype"
                    eventDescription = _processParcelSelectedAppraisalType(db, parcelId, newValue, loginId)
                    doCustomChangeAction(db, parcelId, loginId, "AppraisalMethod", newValue, eventDate, latitude, longitude, SyncID)

                Case "fieldalerttype"
                    eventDescription = _setFieldAlertType(db, parcelId, newValue, loginId)
                    doCustomChangeAction(db, parcelId, loginId, "FieldAlertType", newValue, eventDate, latitude, longitude, SyncID)

                Case "fieldalert"
                    eventDescription = _setFieldAlert(db, parcelId, newValue, loginId)
                    doCustomChangeAction(db, parcelId, loginId, "FieldAlert", newValue, eventDate, latitude, longitude, SyncID)
                Case "cc_error"
                    eventDescription = _setCC_Error(db, parcelId, newValue, loginId)
                    doCustomChangeAction(db, parcelId, loginId, "CC_Error", newValue, eventDate, latitude, longitude, SyncID)

                Case "audittrailentry"
                    ParcelAuditStream.CreateFlagEvent(db, eventDate, parcelId, loginId, newValue, "MA")

                Case "reviewed"
                    eventDescription = _markParcelAsReviewed(db, parcelId, nbhdId, loginId, eventDate, latitude, longitude)
                    doCustomChangeAction(db, parcelId, loginId, "MarkAsComplete", newValue, eventDate, latitude, longitude, SyncID)
                    Dim editAction As DataTable = Database.Tenant.GetDataTable("SELECT * FROM DataSourceTableAction WHERE Action='E'")
                    If editAction.Rows.Count > 0 Then
                        QCProcessor.EditTableAction(db, parcelId, "1", loginId, eventDate, longitude, latitude, SyncID)
                    End If
                    QCProcessor.EditDescendentTableAction(db, parcelId, "1", loginId, eventDate, longitude, latitude, SyncID)
                    If ParcelEventLog.Enabled Then
                        ParcelEventLog.CreateFlagEvent(db, ParcelEventLog.EventType.Reviewed, ParcelEventLog.ApplicationType.MA, parcelId, loginId)
                    End If
                Case "ccsketchannotation"
                    eventDescription = _setSketchAnnotation(db, parcelId, loginId, eventDate, newValue)

                Case "ccmamarkupstring"
                    eventDescription = _saveSketchMarkup(db, parcelId, loginId, eventDate, newValue)

                Case "vectorstring"
                    eventDescription = _saveSketchVector(db, parcelId, loginId, eventDate, newValue)

                Case "deleteparcelphoto"
                    eventDescription = _deleteParcelPhoto(db, newValue)

                Case "markasprimaryphoto"
                    eventDescription = _markAsPrimaryPhoto(db, newValue, loginId, eventDate, latitude, longitude)
                Case "new"
                    eventDescription = _insertNewRecord(db, parcelId, loginId, rowUID, IIf(oldValueOrOption = "", 0, oldValueOrOption), newValue, eventDate, latitude, longitude, SyncID)
                Case "tcp"
                    eventDescription = _templateCopy(db, parcelId, rowUID, loginId, oldValueOrOption, newValue, eventDate, latitude, longitude, SyncID)
                Case "fcp", "fcpa"
                    eventDescription = _futureYearCopy(db, parcelId, rowUID, loginId, oldValueOrOption, newValue, eventDate, latitude, longitude, SyncID, IIf(fieldOrCommand.ToLower = "fcpa", "1", "0"))
                Case "icp"
                    Dim sourceRowUid As Long = 0, clientParentRowUid As Long = 0
                    Dim sourceParcelData = oldValueOrOption.Split("$")
                    Dim sourceParcelId = sourceParcelData(0)
                    If sourceParcelData.Length > 1 Then
                        sourceRowUid = IIf(sourceParcelData(1) = "", 0, sourceParcelData(1))
                    End If

                    Dim targetParcelData = rowUID.ToString().Split("$")
                    Dim clientRowUid = targetParcelData(0)

                    If targetParcelData.Length > 1 Then
                        clientParentRowUid = IIf(targetParcelData(1) = "", 0, targetParcelData(1))
                    End If

                    eventDescription = _improvementCopy(db, parcelId, sourceParcelId, sourceRowUid, loginId, clientRowUid, clientParentRowUid, newValue, eventDate, latitude, longitude, SyncID)
                Case "vcp"
                    Dim sourceRowUid As Long = 0, clientParentRowUid As Long = 0
                    Dim sourceParcelData = oldValueOrOption.Split("$")
                    Dim sourceParcelId = sourceParcelData(0)
                    If sourceParcelData.Length > 1 Then
                        sourceRowUid = IIf(sourceParcelData(1) = "", 0, sourceParcelData(1))
                    End If

                    Dim targetParcelData = rowUID.ToString().Split("$")
                    Dim clientRowUid = targetParcelData(0)

                    If targetParcelData.Length > 1 Then
                        clientParentRowUid = IIf(targetParcelData(1) = "", 0, targetParcelData(1))
                    End If

                    eventDescription = _improvementCopy(db, parcelId, sourceParcelId, sourceRowUid, loginId, clientRowUid, clientParentRowUid, newValue, eventDate, latitude, longitude, SyncID, "visionCopy")
                Case "audit"
                    ParcelAuditStream.CreateFlagEvent(db, eventDate, parcelId, loginId, newValue, "MA")
                Case "delete"
                    eventDescription = _deleteRecord(db, parcelId, loginId, rowUID, newValue, eventDate, latitude, longitude, SyncID)

                Case "deletechild"
                    eventDescription = _deleteChildRecordsOnly(db, parcelId, loginId, rowUID, newValue, oldValueOrOption, eventDate, latitude, longitude, SyncID)

                Case "tasketch"
                    eventDescription = _updateSketchForTA(db, parcelId, loginId, rowUID, newValue, eventDate, latitude, longitude, SyncID)
                Case "updatesketch"
                    eventDescription = _updateSketch(db, parcelId, loginId, rowUID, oldValueOrOption, newValue, eventDate, latitude, longitude, SyncID)
                Case "tasketcharea"
                    eventDescription = _updateSketchAreaForTA(db, parcelId, loginId, rowUID, newValue, eventDate, latitude, longitude, SyncID)
                Case "updatesketcharea"
                Case "newparcel"
                    eventDescription = _createNewBPPParcel(db, parcelId, newValue, loginId,eventDate,rowUID)
                Case "deleteparcel"
                	eventDescription = _deleteBPPParcel(db, parcelId, loginId, eventDate)
                Case "recoverdeletedparcel"
                    eventDescription = _recoverparceldeleteBPPParcel(db, parcelId, loginId, eventDate)
                Case "photometadata"
                	_savePhotoMetaData(db, loginId, eventDate, refId, oldValueOrOption, newValue)
                Case "masvsave"
                    _saveSvReview(db, loginId, eventDate, parcelId, newValue)
                Case "uploadprc"
                    _uploadPRCHtmlFile(db, loginId, parcelId, newValue)
            End Select

            If nbhdId > 0 Then
                Neighborhood.SetLastVisitedDate(db, nbhdId, eventDate)
                db.Execute("UPDATE UserTrackingCurrent SET LastParcelId = {1} WHERE LoginId = {0} AND NbhdId = {2}", loginId.ToSqlValue, parcelId, nbhdId)
            End If

            If eventDescription <> "" Then
            	   If Not eventDescription.Contains("New Aux Record ") Then
                    ParcelAuditStream.CreateFlagEvent(db, eventDate, parcelId, loginId, eventDescription, "MA")
                End If
                If fieldOrCommand.ToLower = "reviewed" Then
                    Dim autoQc As Boolean = db.GetIntegerValue("select autoQC from UserSettings where LoginId={0}".SqlFormatString(loginId))
                    If autoQc = True Then

                        db.Execute("EXEC qc_Parcel_Set {0}, {1}, {2},{3},{4}".SqlFormat(False, loginId, parcelId, "1", eventDate.AddMilliseconds(1000), 1))
                    End If
                Else
                    Parcel.ResetQCStatus(db, parcelId)
                End If
            End If
        End If
    End Sub

    Private Shared Sub _processParcelDataChange(db As Database, loginId As String, eventDate As DateTime, parcelId As Integer, auxRowUID As Long, fieldId As Integer, oldValue As String, newValue As String, latitude As Single, longitude As Single, SyncID As String, Optional ByVal action As String = "edit", Optional ByVal IsParentDeleted As Boolean = False)
        Dim field As DataRow = db.GetTopRow("SELECT * FROM DataSourceField WHERE Id = " & fieldId)
        If (field Is Nothing) Then
            Throw New Exception("No Field Found Exception")
            Return
        End If
        Dim tableName As String = field.GetString("SourceTable")
        Dim isCustomField As Boolean = field.GetBoolean("IsCustomField")
        Dim mapToSourceTable As String = field.GetString("MapToSourceTable")

        tableName = DataSource.FindTargetTable(db, tableName)
        Dim parentAuxRowUID As Integer = 0
        Dim CC_YearStatus As String = Nothing
        Dim ar As DataRow = Nothing
        If auxRowUID < 0 Then
            ar = db.GetTopRow("SELECT * FROM " + tableName + " WHERE ClientROWUID = " & auxRowUID)
            If ar IsNot Nothing Then
                auxRowUID = ar.GetInteger("ROWUID")
            End If
            action = "new"
        ElseIf auxRowUID > 0 Then
        	ar = db.GetTopRow("SELECT * FROM " + tableName + " WHERE ROWUID = "+auxRowUID.ToString+" OR ClientROWUID = "+auxRowUID.ToString)
        	If ar.GetInteger("ROWUID") <> auxRowUID Then
        		auxRowUID = ar.GetInteger("ROWUID")
        	End If
        End If
        If ar IsNot Nothing Then
            parentAuxRowUID = ar.GetInteger("ParentROWUID")
            CC_YearStatus = ar.GetString("CC_YearStatus")
            If action = "edit" AndAlso ar.GetString("CC_RecordStatus") = "I" Then
                action = "new"
            End If
        End If

        If auxRowUID < 0 Then
            'Ignore.. the record is missing
            Return
        End If
        newValue = db.GetStringValue("EXEC CC_ProcessAndFillColumnValue {0},{1}".SqlFormat(True, field("name"), newValue))

        If mapToSourceTable IsNot Nothing AndAlso mapToSourceTable <> String.Empty Then
            Dim newValueRowuid As String
            Dim TableandRowuid() As String
            TableandRowuid = newValue.Split("{")
            If TableandRowuid.Length > 1 Then
                newValueRowuid = TableandRowuid(1).Split("}")(0)
            Else
                newValueRowuid = newValue
            End If
            If IsNumeric(newValueRowuid) AndAlso CLng(newValueRowuid) < 0 Then
                Dim targetTables() As String
                targetTables = mapToSourceTable.Split(";")
                If targetTables.Length > 1 Then
                    For Each targetsTable As String In targetTables
                        Dim targetTable As String = DataSource.FindTargetTable(db, targetsTable)
                        If targetTable IsNot Nothing AndAlso targetTable <> String.Empty Then
                            Dim sqlMapping As String = "SELECT ROWUID FROM " + targetTable + " WHERE ClientROWUID = " + newValueRowuid
                            Dim replacedValue As String = db.GetIntegerValue(sqlMapping)
                            If replacedValue <> Nothing AndAlso replacedValue <> "0" AndAlso replacedValue.Length > 0 Then
                                newValue = "&%" + targetsTable + "{" + replacedValue + "}&%"
                                Exit For
                            End If
                        End If
                    Next
                Else
                    Dim targetsTable = targetTables(0)
                    Dim targetTable As String = DataSource.FindTargetTable(db, targetsTable)
                    If targetTable IsNot Nothing AndAlso targetTable <> String.Empty Then
                        Dim sqlMapping As String = "SELECT ROWUID FROM " + targetTable + " WHERE ClientROWUID = " + newValueRowuid
                        Dim replacedValue As String = db.GetIntegerValue(sqlMapping)
                        newValue = replacedValue
                    End If
                End If
            End If
        End If
        Dim changeId As Integer
        If IsParentDeleted Then
            Dim sql1 As String = "SELECT * FROM ParcelChanges WHERE ParcelId = {0} AND FieldId = {1} AND AuxROWUID = {2} AND Action={3}"
            Dim dr As DataRow = db.GetTopRow(sql1.SqlFormat(True, parcelId, fieldId, auxRowUID, "delete"))
            If dr IsNot Nothing Then
                Dim chngId As Integer = dr.GetInteger("Id")
                db.Execute("UPDATE ParcelChanges SET IsParentDeleted  = 1 WHERE Id = {0}".SqlFormat(True, chngId))
            Else
                sql1 = "INSERT INTO ParcelChanges (ParcelId, FieldId, Action, ReviewedBy, OriginalValue, NewValue, ChangedTime, Latitude, Longitude, AuxROWUID, ParentAuxROWUID,SyncID,CC_YearStatus,IsParentDeleted ) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10},{11},{12},{13}); SELECT CAST(@@IDENTITY AS INT) As NewId;"
                changeId = db.GetIntegerValue(sql1.SqlFormat(True, parcelId, fieldId, action, loginId, oldValue, newValue, eventDate, latitude, longitude, IIf(auxRowUID = 0, "", auxRowUID.ToString), IIf(parentAuxRowUID = 0, "", parentAuxRowUID.ToString), SyncID, CC_YearStatus, 1))
            End If
        Else
        	Dim DonotInsertDeletePCIifExists As Integer = 0
        	If action = "delete" Then
        		Dim delSql As String = "SELECT * FROM ParcelChanges WHERE ParcelId = {0} AND FieldId = {1} AND AuxROWUID = {2} AND Action={3}"
            	Dim dr As DataRow = db.GetTopRow(delSql.SqlFormat(True, parcelId, fieldId, auxRowUID, "delete"))
            	If dr IsNot Nothing Then
            		DonotInsertDeletePCIifExists = 1
            	End If
        	End If
        	If DonotInsertDeletePCIifExists = 0 Then
        		 Dim sql As String = "INSERT INTO ParcelChanges (ParcelId, FieldId, Action, ReviewedBy, OriginalValue, NewValue, ChangedTime, Latitude, Longitude, AuxROWUID, ParentAuxROWUID,SyncID,CC_YearStatus) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10},{11},{12}); SELECT CAST(@@IDENTITY AS INT) As NewId;"
            	 changeId = db.GetIntegerValue(sql.SqlFormat(True, parcelId, fieldId, action, loginId, oldValue, newValue, eventDate, latitude, longitude, IIf(auxRowUID = 0, "", auxRowUID.ToString), IIf(parentAuxRowUID = 0, "", parentAuxRowUID.ToString), SyncID, CC_YearStatus))
        	End If
        End If

        If newValue <> Nothing AndAlso newValue.Length > 0 AndAlso newValue.Trim = "" Then
            db.Execute("UPDATE ParcelChanges SET NewValue ='" & newValue & "' WHERE Id =" & changeId)
        End If
        db.Execute("EXEC processFieldChangesTracking {0},{1},{2},{3}".SqlFormat(True, parcelId, fieldId, changeId, newValue))
        ParcelAuditStream.CreateDataChangeEvent(db, eventDate, parcelId, auxRowUID, loginId, fieldId, newValue, changeId, "MA")

        If Not isCustomField Then
            Parcel.ElevatePriorityIfNormal(db, parcelId, loginId)
            Parcel.ResetReviewStatus(db, parcelId, loginId)
        End If
    End Sub

    Private Shared Sub _processNeighborhoodAction(db As Database, loginId As String, eventDate As DateTime, neighborhood As String, action As String, value As String)
        Select Case action.ToLower
            Case "agree"
                db.UpdateField("Neighborhood", "ProfileReasonable", IIf(value.ToLower = "true", "1", "0"), "Id = " + neighborhood.ToSqlValue)
            Case "comment"
                db.UpdateField("Neighborhood", "LastComment", value.ToSqlValue.Trim("'"), "Id = " + neighborhood.ToSqlValue)
        End Select
    End Sub

    Private Shared Function _processParcelSelectedAppraisalType(db As Database, parcelId As Integer, appraisalType As String, loginId As String)
        If String.IsNullOrEmpty(appraisalType) Then
            Parcel.UpdateParcelField(db, parcelId, "SelectedAppraisalType", "")
            Parcel.ElevatePriorityIfNormal(db, parcelId, loginId)
            Parcel.ResetReviewStatus(db, parcelId, loginId)
            Return "Selected Appraisal Method:  "
        End If
        Dim appr As DataRow = db.GetTopRow("SELECT * FROM AppraisalType WHERE Name = " + appraisalType.ToSqlValue)
        If appr Is Nothing Then
            Return "INVALID APPRAISAL METHOD!"
        Else
            Parcel.UpdateParcelField(db, parcelId, "SelectedAppraisalType", appr.GetInteger("Id"))
            Parcel.ElevatePriorityIfNormal(db, parcelId, loginId)
            Parcel.ResetReviewStatus(db, parcelId, loginId)
            Return "Selected Appraisal Method: " + appr.GetString("Name")
        End If
    End Function

    Private Shared Function _setFieldAlertType(db As Database, parcelId As Integer, alertType As String, loginId As String)
        Dim far As DataRow = db.GetTopRow("SELECT * FROM FieldAlertTypes WHERE Id = " + alertType.ToSqlValue)

        Parcel.ElevatePriorityIfNormal(db, parcelId, loginId)
        Parcel.ResetReviewStatus(db, parcelId, loginId)
        If far Is Nothing Then
            If alertType = "0" Then
                Parcel.UpdateParcelField(parcelId, "FieldAlertType", "")
                Return "Cleared field alert type."
            Else
                Return "INVALID ALERT TYPE!"
            End If
        Else
            Parcel.UpdateParcelField(db, parcelId, "FieldAlertType", far.GetInteger("Id"))
            Return "Field Alert Type: " + far.GetString("Name")
        End If


    End Function

    Private Shared Function _setFieldAlert(db As Database, parcelId As Integer, fieldAlertMessage As String, loginId As String)
        Parcel.UpdateParcelField(db, parcelId, "FieldAlert", fieldAlertMessage)


        Parcel.ElevatePriorityIfNormal(db, parcelId, loginId)
        Parcel.ResetReviewStatus(db, parcelId, loginId)
        If fieldAlertMessage.Length > 50 Then
            fieldAlertMessage = fieldAlertMessage.Substring(1, 50) + "..."
        End If
        Return "Field alert text Changed to " + fieldAlertMessage + "."
    End Function
    Public Shared Function _recoverparceldeleteBPPParcel(db As Database, parcelId As Integer, loginId As String, eventDate As Date)
    	db.Execute("UPDATE ParcelData SET CC_Deleted = 0  WHERE CC_ParcelId = " + parcelId.ToString())
    	Dim ClientRowuid As String = db.GetStringValue("SELECT KeyValue1 from parcel where Id = {0}".SqlFormatString(parcelId))
    	Dim pdDetails As DataRow = db.GetTopRow("SELECT ClientROWUID,ROWUID from parceldata where CC_ParcelId = {0}".SqlFormatString(parcelId))
    	If ClientRowuid = "0" Then
    		ClientRowuid = pdDetails.GetString("ClientROWUID")
    	End If
    	db.Execute("DELETE FROM ParcelChanges where Action = 'delete' and AuxROWUID = {1} and ParcelId = {0}".SqlFormatString(parcelId,COALESCE(pdDetails.GetString("ROWUID"),"")))
    	db.Execute("INSERT INTO ParcelActionLog(ParcelID,KeyValue1,Action,ActionBy,ApplicationType,[UpdatedTime]) VALUES({0},{1},{2},{3},{4},{5})".SqlFormatString(parcelId,ClientRowuid,2,loginId,"MA",eventDate))
        Return "Property Recovered."
    End Function
    Public Shared Function _deleteBPPParcel(db As Database, parcelId As Integer, loginId As String, eventDate As Date)
    	db.Execute("UPDATE ParcelData SET CC_Deleted = 1  WHERE CC_ParcelId = " + parcelId.ToString())
    	Dim ClientRowuid As String = db.GetStringValue("SELECT KeyValue1 from parcel where Id = {0}".SqlFormatString(parcelId))
    	Dim pdDetails As DataRow = db.GetTopRow("SELECT ClientROWUID,ROWUID from parceldata where CC_ParcelId = {0}".SqlFormatString(parcelId))
    	If ClientRowuid = "0" Then
    		ClientRowuid = pdDetails.GetString("ClientROWUID")
    	End If
    	db.Execute("INSERT INTO ParcelActionLog(ParcelID,KeyValue1,Action,ActionBy,ApplicationType,[UpdatedTime]) VALUES({0},{1},{2},{3},{4},{5})".SqlFormatString(parcelId,ClientRowuid,1,loginId,"MA",eventDate))
        Dim parceltable = db.GetStringValue("select top 1 parceltable from Application ")
        Dim DFID = db.GetIntegerValue("SELECT MIN(Id) FROM DataSourceField WHERE SourceTable ={0}".SqlFormatString(parceltable))
        _processParcelDataChange(db, loginId, eventDate, parcelId, pdDetails.GetString("ROWUID"), DFID, Nothing, Nothing, 0, 0, "", "delete")
        Return "Property deleted."
    End Function
    Private Shared Function _createNewBPPParcel(db As Database, parentParcelId As Integer, clientParcelID As String, loginId As String,eventDate As Date,refparcelId As String )
        Dim AuditTrailMsg As String
        Dim BPPCOunt = db.GetIntegerValue("SELECT count(*) FROM PARCEL WHERE ParentParcelId =" + parentParcelId.ToString())
        Dim alternateKey = db.GetStringValue("SELECT Alternatekeyfieldvalue FROM Application")
        Dim parentDetails = db.GetTopRow("SELECT * FROM PARCEL WHERE Id =" + parentParcelId.ToString())
        Dim newPId
        Dim prty = 1
        If EnableNewPriorities Then
            prty = 3
        End If
        If (parentDetails Is Nothing) Then
            newPId = db.GetIntegerValue("INSERT INTO Parcel (KeyValue1, NeighborhoodId, Priority, IsPersonalProperty) VALUES ({0}, {1},{2},{3});SELECT CAST(@@IDENTITY AS INT) As NewId".SqlFormat(True, 0, refparcelId, prty, 1))
            db.Execute("INSERT INTO ParcelData(CC_ParcelID,clientROWUID) VALUES ({0},{1})".SqlFormat(True, newPId, clientParcelID))
            db.Execute("UPDATE ParcelImages SET ParcelId = {0} WHERE BppClientROWUID = {1}".SqlFormatString(newPId,clientParcelID))
            ParcelAuditStream.CreateFlagEvent(db, eventDate, newPId, loginId, "New StandAlone personal property created.", "MA")
            AuditTrailMsg = ""
        Else
            newPId = db.GetIntegerValue("INSERT INTO Parcel (KeyValue1, NeighborhoodId, StreetAddress, Priority, IsPersonalProperty, parentParcelId, StreetNumber, StreetName, StreetNameSuffix) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8});SELECT CAST(@@IDENTITY AS INT) As NewId".SqlFormat(True, 0, parentDetails.GetString("NeighborhoodId"), parentDetails.GetString("StreetAddress"), prty, 1, parentParcelId, parentDetails.GetString("StreetNumber"), parentDetails.GetString("StreetName"), parentDetails.GetString("StreetNameSuffix")))
            db.Execute("INSERT INTO ParcelData(CC_ParcelID,clientROWUID) VALUES ({0},{1})".SqlFormat(True, newPId, clientParcelID))
            db.Execute("UPDATE ParcelImages SET ParcelId = {0} WHERE BppClientROWUID = {1}".SqlFormatString(newPId,clientParcelID))
            db.Execute("INSERT INTO ParcelActionLog(ParcelID,KeyValue1,Action,ActionBy,ApplicationType,[UpdatedTime]) VALUES({0},{1},{2},{3},{4},{5})".SqlFormatString(newPId, clientParcelID, 0, loginId, "MA", eventDate))
            ParcelAuditStream.CreateFlagEvent(db, eventDate, newPId, loginId, "New property created.", "MA")
            AuditTrailMsg = "New business Personal Property (" + (parentDetails.GetString("KeyValue1") + "-" + (BPPCOunt + 1).ToString()) + ") created"
            If alternateKey <> Nothing Then
                Dim parentAltKeyDetails = db.GetStringValue("SELECT " + alternateKey + " FROM PARCELDATA WHERE CC_ParcelId =" + parentParcelId.ToString())
                AuditTrailMsg = "New business Personal Property (" + (parentAltKeyDetails + "-" + (BPPCOunt + 1).ToString()) + ") created"
            End If

        End If

        Dim parceltable = db.GetStringValue("select top 1 parceltable from Application ")
        Dim IFID = db.GetIntegerValue("SELECT MIN(Id) FROM DataSourceField WHERE SourceTable ={0}".SqlFormatString(parceltable))
        Dim sql As String = "INSERT INTO ParcelChanges  (ParcelId, FieldId, Action, ReviewedBy, OriginalValue, NewValue, ChangedTime, Latitude, Longitude) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}); SELECT CAST(@@IDENTITY AS INT) As NewId;"
        Dim changeId As Integer = db.GetIntegerValue(sql.SqlFormat(True, newPId, IFID, "new", loginId, "", "", eventDate, 0, 0))
        Return AuditTrailMsg
    End Function
    Private Shared Function _setCC_Error(db As Database, parcelId As Integer, cc_error As String, loginId As String)
        If cc_error = "" Then
            Dim prev_cc_error As String = db.GetStringValue("SELECT CC_Error FROM Parcel WHERE Id = " + parcelId.ToString)
            If prev_cc_error.IndexOf("Sketch labels are invalid") < 0 And prev_cc_error.IndexOf("There are multiple Sections for this Building") < 0 Then
                cc_error = prev_cc_error
            End If
        End If
        Parcel.UpdateParcelField(db, parcelId, "CC_Error", cc_error)

        Return ""
    End Function

    Private Shared Function _markParcelAsReviewed(db As Database, parcelId As Integer, nbhdId As Integer, loginId As String, eventDate As Date, latitude As Single, longitude As Single)
        Dim dr As DataRow = db.GetTopRow("UPDATE Parcel SET Reviewed = 1, ReviewedBy = {1}, ReviewDate = {2}, QC = NULL, QCDate = NULL, QCBy = NULL,Committed=0 , StatusFlag = NULL WHERE Id = {0}; SELECT * FROM Parcel WHERE Id = {0};".SqlFormatString(parcelId, loginId, eventDate.ToSqlValue.Trim("'")))

        'Parcel.UpdateParcelField(db, parcelId, "Reviewed", 1)
        'Parcel.UpdateParcelField(db, parcelId, "ReviewedBy", loginId)
        'Parcel.UpdateParcelField(db, parcelId, "ReviewDate", eventDate)
        'Parcel.ResetQCStatus(db, parcelId)

        If dr Is Nothing Then
            'Ignore - Parcel might have been deleted.
            Return ""
        End If


        db.Execute("EXEC cc_UpdateNeighborhoodStatistics " & dr.GetInteger("NeighborhoodId") & ", 0")
        Dim priority = dr.GetInteger("Priority")
        db.Execute("EXEC SP_ParcelReview @ParcelId ={0},@LoginId = {1},@EventDate = {2},@Latitude = {3},@Longitude = {4},@priority = {5},@Application ={6}".FormatString(parcelId, loginId.ToSqlValue, eventDate.ToSqlValue, latitude, longitude, priority, "'MA'"))
        'db.Execute("INSERT INTO ParcelReviewLog  (ParcelId, ReviewedBy, ReviewTime, Latitude, Longitude,Priority,ApplicationType) VALUES ({0}, {1}, {2}, {3}, {4},{5},{6})", parcelId, loginId.ToSqlValue, eventDate.ToSqlValue, latitude, longitude, priority, "'MA'")
        Try
            db.Execute("cc_UpdateParcelPriorityLog {0},{1},{2}".FormatString(parcelId, loginId.ToSqlValue, eventDate.ToSqlValue))
            Dim autoQc As Boolean = db.GetIntegerValue("select autoQC from UserSettings where LoginId={0}".SqlFormatString(loginId))

        Catch ex As Exception

        End Try

        If ParcelEventLog.Enabled Then
            ParcelEventLog.CreateFlagEvent(db, ParcelEventLog.EventType.Reviewed, ParcelEventLog.ApplicationType.MA, parcelId, loginId)
        End If

        Return "Parcel reviewed and marked as complete."
    End Function

    Private Shared Function _setSketchAnnotation(db As Database, parcelId As Integer, loginId As String, eventDate As Date, annotation As String)
        db.Execute("UPDATE ParcelSketch   SET Annotation ={0}, IsReviewed = 1, ReviewBy ={1}, ReviewDate ={2} WHERE ParcelId ={3}".SqlFormatString(annotation, loginId, eventDate, parcelId))
        Return "Annotation added to sketch."
    End Function

    Private Shared Function _saveSketchMarkup(db As Database, parcelId As Integer, loginId As String, eventDate As Date, markup As String)
        db.Execute("UPDATE ParcelSketch   SET MarkupString ={0}, IsMarkedUp = 1, MarkupBy ={1}, MarkupDate ={2}, IsReviewed = 1, ReviewBy ={3}, ReviewDate ={4}  WHERE ParcelId ={5}".SqlFormatString(markup, loginId, eventDate, loginId, eventDate, parcelId))
        Return "Markup added over sketch."
    End Function

    Private Shared Function _saveSketchVector(db As Database, parcelId As Integer, loginId As String, eventDate As Date, vector As String)
        db.Execute("UPDATE ParcelSketch   SET CCMAVectorString ={0}, IsConverted = 1, MarkupBy ={1}, MarkupDate ={2}, IsReviewed = 1, ReviewBy ={3}, ReviewDate ={4} WHERE ParcelId ={5}".SqlFormatString(vector, loginId, eventDate, loginId, eventDate, parcelId))
        Return "Sketch vector string updated (CCMA)"
    End Function

    Private Shared Function _deleteParcelPhoto(db As Database, strPhotoId As String)
        Dim deleteFilter As String
        If strPhotoId.StartsWith("pu") Then
            deleteFilter = "LocalId = " + strPhotoId.ToSqlValue
        Else
            deleteFilter = "Id = " + strPhotoId.ToSqlValue
        End If

        Dim dr As DataRow = db.GetTopRow("SELECT * FROM ParcelImages WHERE " + deleteFilter)
        If dr IsNot Nothing Then
            Dim photoId As Integer = dr.GetInteger("Id")
            If ClientSettings.PropertyValue("PhotoDeleteAndRecover") = "1" Then
                Dim DownSynced = dr.GetString("downsynced")
                If db.GetIntegerValue("SELECT COUNT(*) FROM ClientSettings WHERE Name = 'PhotoNoDownsyncOnFlagging' AND (Value = '1' OR LOWER(Value) = 'true')") > 0 And (DownSynced = "1" Or DownSynced.ToLower = "true") Then
                    db.Execute("UPDATE ParcelImages SET CC_Deleted = 1, IsPrimary = 0 WHERE Id = " & photoId)
                Else
                    db.Execute("UPDATE ParcelImages SET CC_Deleted = 1, IsPrimary = 0, DownSynced = 0  WHERE Id = " & photoId)
                End If
            Else
                Dim s3 As New S3FileManager()
                s3.DeleteFile(dr("Path"))
                db.Execute("DELETE FROM ParcelImages WHERE Id = " & photoId)
            End If
            db.Execute("DELETE FROM ParcelChanges WHERE NewValue={0} AND Action='PhotoFlagMain'".SqlFormatString(photoId.ToString()))

            'Parcel.ElevatePriorityIfNormal(db, parcelId)
            'Parcel.ResetReviewStatus(db, parcelId)
            Return String.Concat("Photo #", photoId, " deleted from parcel")
            Else
                Return ""
        End If

    End Function

    Private Shared Function _markAsPrimaryPhoto(db As Database, photoId As String, ByVal reviewedBy As String, eventDate As Date, latitude As Single, longitude As Single)
        Dim filter = ""
        If photoId.StartsWith("pu") Then
            filter = "LocalId={0}".SqlFormatString(photoId)
        Else
            If photoId <> "" Then
                filter = "Id ={0}".SqlFormatString(photoId)
            End If
        End If
        Dim dr As DataRow = db.GetTopRow("SELECT * FROM ParcelImages WHERE " + filter)
        If dr IsNot Nothing Then


            Dim parcelId = dr.GetString("ParcelId")
            Dim ImageId = dr.GetString("Id")
            Dim imgReferenceId = dr.GetString("ReferenceId")
            Dim DownSynced = dr.GetString("downsynced")
            Dim newValue = dr.GetString("Id")
            If IsNothing(imgReferenceId) = True Then
                imgReferenceId = ""
            End If

            Dim KeyValue1 = db.GetStringValue("SELECT KeyValue1 FROM Parcel WHERE Id={0}".SqlFormatString(parcelId))
            db.Execute("UPDATE ParcelImages SET IsPrimary=0 WHERE ParcelId={0}".SqlFormatString(parcelId))

            If db.GetIntegerValue("SELECT COUNT(*) FROM ClientSettings WHERE Name = 'PhotoNoDownsyncOnFlagging' AND (Value = '1' OR LOWER(Value) = 'true')") > 0 And (DownSynced = "1" Or DownSynced.ToLower = "true") Then
                db.Execute("UPDATE ParcelImages SET UploadedBy = " + reviewedBy.ToSqlValue + ", IsPrimary=1 WHERE " + filter)
            Else
                db.Execute("UPDATE ParcelImages SET UploadedBy = " + reviewedBy.ToSqlValue + ", Downsynced = 0, IsPrimary=1 WHERE " + filter)
            End If

            '   db.Execute("INSERT INTO ParcelChanges(ParcelId,ReviewedBy,ChangedTime,OriginalValue,NewValue,Latitude, Longitude,Action) VALUES({0},{1},{2},{3},{4},{5},{6},{7})".SqlFormat(True, parcelId, reviewedBy, eventDate, imgReferenceId, newValue, latitude, longitude, "PhotoFlagMain"))

            'Parcel.ElevatePriorityIfNormal(db, parcelId)
            'Parcel.ResetReviewStatus(db, parcelId)
            Return "Primary photo of parcel " + KeyValue1 + " changed to #" + ImageId + "."

        Else
            Return ""
            'Throw New Exception("Failed to set main flag for parcel ")
        End If
    End Function
    '405510
    Private Shared Function _insertNewRecord(db As Database, parcelId As Integer, loginId As String, clientROWUID As Long, clientParentROWUID As Long, tableName As String, eventDate As Date, latitude As Single, longitude As Single, SyncID As String)
        Dim insertFields = ""
        Dim selectFields = ""
        Dim newId As Integer
        Dim arr = tableName.Split("$")
        Dim yearType = "NULL"
        If (arr.Length > 1) Then
            yearType = arr(1).ToString
        End If
        tableName = arr(0)
        Dim sourceTable = tableName
        Dim parentTableName As String = ""
        Dim IFID = db.GetIntegerValue("SELECT MIN(df.Id) FROM DataSourceField df LEFT OUTER JOIN DataSourceKeys dk ON df.Id=dk.FieldId WHERE SourceTable={0} AND IsPrimaryKey IS NULL AND IsReferenceKey IS NULL AND( ExclusiveForSS = 0 OR ExclusiveForSS IS NULL) ".SqlFormatString(tableName))
        'If no Then non PK field Is there While creating a record.generate PCI for the first PK field in order to avoid downsync failure
        If IFID = 0 Then
            IFID = db.GetIntegerValue("SELECT MIN(Id) FROM DataSourceField  WHERE SourceTable={0}  AND ( ExclusiveForSS = 0 OR ExclusiveForSS IS NULL) ".SqlFormatString(tableName))
        End If
        Dim fieldName = db.GetStringValue("SELECT Name FROM DataSourceField WHERE id ={0}".SqlFormatString(IFID))
        Dim tableId As Integer = db.GetIntegerValueOrInvalid("SELECT Id FROM DataSourceTable WHERE Name = " + tableName.ToSqlValue)

        Dim getSql = "SELECT pt.Name, tf.Name As TableField, pf.Name As ReferenceField FROM DataSourceRelationships r LEFT OUTER JOIN DataSourceKeys k ON k.RelationshipId = r.Id LEFT OUTER JOIN DataSourceField tf ON k.FieldId = tf.Id LEFT OUTER JOIN DataSourceField pf ON k.ReferenceFieldId = pf.Id 	LEFT OUTER JOIN DataSourceTable tt ON r.TableId = tt.Id LEFT OUTER JOIN DataSourceTable pt ON r.ParentTableId = pt.Id WHERE tt.Name ={0} AND Relationship IS NOT NULL AND Relationship <> 0 AND k.IsReferenceKey = 1".SqlFormatString(tableName)
        Dim dt As DataTable = db.GetDataTable(getSql)
        Dim yearField As String = db.GetStringValue("SELECT YearPartitionField FROM DatasourceTable WHERE name = {0}".SqlFormatString(tableName))
        Dim ActualYearValue As String = db.GetStringValue("SELECT value from clientsettings  WHERE name = 'CurrentYearValue'")
        Dim FutureYearEnabled As String = db.GetStringValue("SELECT value from clientsettings  WHERE name = 'FutureYearEnabled'")
        Dim FYValue As String = db.GetStringValue("SELECT value FROM clientsettings WHERE name = 'FutureYearValue'")
        If String.IsNullOrEmpty(FYValue) Then
            FYValue = "0"
        End If
        Dim yearFieldUpdated = False
        For Each dr As DataRow In dt.Rows
            sourceTable = dr.GetString("Name")
            insertFields = insertFields + "," + dr.GetString("TableField")
            If (dr.GetString("TableField") = yearField And (FutureYearEnabled = "1" Or FutureYearEnabled.ToLower = "true")) Then
                selectFields = selectFields + IIf(yearType = "A", "," + ActualYearValue, "," + FYValue)
                yearFieldUpdated = True
            Else
                selectFields = selectFields + "," + dr.GetString("ReferenceField")
            End If
        Next
        If (Not yearFieldUpdated) And yearField <> "" And (FutureYearEnabled = "1" Or FutureYearEnabled.ToLower = "true") Then
            insertFields = insertFields + "," + yearField
            selectFields = selectFields + IIf(yearType = "A", "," + ActualYearValue, "," + FYValue)
        End If


        Dim tempParentTable = sourceTable
        Dim coParentTable = DataSource.FindTargetTable(db, tempParentTable)

        'Protection against ghost records:
        'While syncing changes from MA, if internet connection is lost so that the response is lost, MA will resend the same data - If there were
        'inserts, they would re-execute.
        Dim checkSql As String = "SELECT COUNT(*) FROM XT_" + tableName + " WHERE ClientROWUID = '" + clientROWUID.ToString() + "'"
        If db.GetIntegerValue(checkSql) > 0 Then
            Return ""
        End If

        Dim parentROWUID As Integer = 0
        If clientParentROWUID = 0 Then
            Dim insertSql = "INSERT INTO XT_" + tableName + "   (CC_ParcelId, ClientROWUID,CC_YearStatus " + insertFields + " ) SELECT '" + parcelId.ToString + "','" + clientROWUID.ToString() + "' ," + "'" + yearType + "'" + selectFields + "  FROM   " + coParentTable + " WHERE CC_ParcelId ={0}; SELECT CAST(@@IDENTITY  AS INT);".SqlFormatString(parcelId)
            newId = db.GetIntegerValue(insertSql)
        Else
            Dim dtValues = db.GetDataTable("SELECT p.Name,r.Id 	FROM DataSourceRelationships r INNER JOIN DataSourceTable t ON r.TableId = t.Id INNER JOIN DataSourceTable p ON r.ParentTableId = p.Id 	WHERE t.Name = {0}".SqlFormatString(tableName))
            parentTableName = dtValues.Rows(0).GetString("Name")
            If clientParentROWUID > 0 Then
                parentROWUID = clientParentROWUID
            Else
                If (dtValues.Rows.Count > 0) Then
                    Dim relationshipId = dtValues.Rows(0).GetInteger("Id")
                    parentROWUID = db.GetIntegerValue("SELECT ROWUID FROM XT_" + parentTableName + " WHERE ClientROWUID={0}".SqlFormatString(clientParentROWUID.ToString()))
                End If
            End If
            
            If parentROWUID > 0 Then
            	Dim insertString As String
            	Dim clientROWUIDCheck As Integer = db.GetIntegerValue("select top 1 1 FROM " + coParentTable + "  WHERE ROWUID={0}".SqlFormatString(parentROWUID.ToString()))
            	If clientROWUIDCheck = 1 Then
            		insertString = "INSERT INTO XT_" + tableName + "    (CC_ParcelId, ParentROWUID, ClientROWUID, ClientParentROWUID,CC_YearStatus " + insertFields + ")  SELECT '" + parcelId.ToString() + "','" + parentROWUID.ToString() + "','" + clientROWUID.ToString() + "','" + clientParentROWUID.ToString() + "' ," + "'" + yearType + "'" + selectFields + " FROM " + coParentTable + "  WHERE ROWUID={0};SELECT CAST(@@IDENTITY  AS INT)".SqlFormatString(parentROWUID.ToString())
            	Else
            		insertString = "INSERT INTO XT_" + tableName + "    (CC_ParcelId, ParentROWUID, ClientROWUID, ClientParentROWUID,CC_YearStatus " + insertFields + ")  SELECT '" + parcelId.ToString() + "','" + parentROWUID.ToString() + "','" + clientROWUID.ToString() + "','" + clientParentROWUID.ToString() + "' ," + "'" + yearType + "'" + selectFields + " FROM " + coParentTable + "  WHERE ClientROWUID={0};SELECT CAST(@@IDENTITY  AS INT)".SqlFormatString(parentROWUID.ToString())
            	End If
                newId = db.GetIntegerValue(insertString)
            End If
        End If
        If newId > 0 Then
            insertNewRecordDetails(db, parcelId, newId, tableName, clientROWUID, eventDate, loginId)
            If ParcelEventLog.Enabled Then
                ParcelEventLog.CreateTableEvent(db, ParcelEventLog.EventType.RowAdded, ParcelEventLog.ApplicationType.MA, parcelId, tableId, newId)
            End If

            '   Dim newValue = ""
            '  Dim selectSql = "SELECT  " + fieldName + "  FROM  XT_" + tableName + " WHERE ROWUID={0}".SqlFormatString(newId)
            '   newValue = db.GetStringValue(selectSql)
            Dim sql As String = "INSERT INTO ParcelChanges  (ParcelId, FieldId, Action, ReviewedBy, OriginalValue, NewValue, ChangedTime, Latitude, Longitude, AuxROWUID, ParentAuxROWUID,SyncID,CC_YearStatus) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10},{11},{12}); SELECT CAST(@@IDENTITY AS INT) As NewId;"
            Dim changeId As Integer = db.GetIntegerValue(sql.SqlFormat(True, parcelId, IFID, "new", loginId, "", "", eventDate, latitude, longitude, newId, parentROWUID.ToString.TrimStart("0"), SyncID, yearType))
            ParcelAuditStream.CreateFlagEvent(db, eventDate, parcelId, loginId, "New Aux Record " + newId.ToString() + " inserted to " + tableName, "MA")
            db.Execute("EXEC cc_InsertDefaulltPCIs {0},{1},{2},{3},{4},{5},{6},{7},{8},{9}".SqlFormat(True, parcelId, loginId, "new", eventDate, latitude, longitude, newId, parentROWUID.ToString.TrimStart("0"), tableName, SyncID))
            db.Execute("EXEC processFieldChangesTracking {0},{1},{2},{3}".SqlFormat(True, parcelId, IFID, changeId, ""))
            '_processParcelDataChange(loginId, eventDate, parcelId, newId, IFID, Nothing, Nothing, latitude, longitude, "new")
            Dim autonumberfields As DataTable = db.GetDataTable("select * from datasourcefield where sourcetable={0} and autonumber=1".SqlFormatString(tableName))
            Dim sibling As DataTable = New DataTable
            If autonumberfields.Rows.Count > 0 Then
                If clientParentROWUID = 0 Then
                    sibling = db.GetDataTable("select * from XT_" + tableName + " where CC_ParcelId=" + parcelId.ToString())
                Else
                    If (parentTableName <> "") Then
                        sibling = db.GetDataTable("EXEC cc_GetChildROWUIDs {0}, {1}, {2},{3}".SqlFormatString(parentTableName, tableName, parentROWUID, 1))
                    End If
                End If
            End If
            For Each dr As DataRow In autonumberfields.Rows
                Dim value As Integer
                Dim autoIncrementSeed As String = JSONResponse.GetObjectValueFromJSON(dr.GetString("UIProperties"), "autoIncrementSeed")
                If sibling.Rows.Count = 1 Then
                    If autoIncrementSeed <> Nothing Then
                        value = Convert.ToInt32(autoIncrementSeed) - 1
                    Else
                        value = 0
                    End If
                Else
                    value = IIf(IsDBNull(sibling.Compute("MAX(" + dr.GetString("name") + ")", "")), 0, sibling.Compute("MAX(" + dr.GetString("name") + ")", ""))
                End If
                value += 1
                db.Execute("UPDATE XT_" + tableName + "   SET " + dr.GetString("name").ToString() + " = " + value.ToString() + " WHERE ROWUID = {0}".SqlFormatString(newId.ToString()))
                Dim sql2 As String = "INSERT INTO ParcelChanges  (ParcelId, FieldId, Action, ReviewedBy, OriginalValue, NewValue, ChangedTime, Latitude, Longitude, AuxROWUID, ParentAuxROWUID,SyncID,CC_YearStatus) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10},{11},{12}); SELECT CAST(@@IDENTITY AS INT) As NewId;"
                Dim changeId2 As Integer = db.GetIntegerValue(sql2.SqlFormat(True, parcelId, dr.GetString("Id"), "new", loginId, "", value, eventDate, latitude, longitude, newId, parentROWUID.ToString.TrimStart("0"), SyncID, yearType))
                db.Execute("EXEC processFieldChangesTracking {0},{1},{2},{3}".SqlFormat(True, parcelId, dr.GetString("Id"), changeId, value))
            Next
            Parcel.ElevatePriorityIfNormal(db, parcelId, loginId)
            Parcel.ResetReviewStatus(db, parcelId, loginId)
            Return "New Aux Record " + newId.ToString() + " inserted to " + tableName
        Else
            'Ignore this error. A parent record might have deleted in a previous update.
            'Throw New Exception("Failed to insert new record in " + tableName)
            Return ""
        End If
    End Function

    Private Shared Function _deleteRecord(db As Database, parcelId As Integer, loginId As String, clientROWUID As Long, tableName As String, eventDate As Date, latitude As Single, longitude As Single, SyncID As String)

        Dim DFID = db.GetIntegerValue("SELECT MIN(Id) FROM DataSourceField WHERE SourceTable ={0}".SqlFormatString(tableName))
        Dim dr As DataRow = db.GetTopRow("SELECT Id,DisableDeletePCI FROM DataSourceTable WHERE Name = " + tableName.ToSqlValue)
        Dim tableId As Integer = dr.GetInteger("Id")
        Dim DisableDeletePCI As Boolean = dr.GetBoolean("DisableDeletePCI")
        If clientROWUID < 0 Then
            clientROWUID = db.GetIntegerValue("SELECT ROWUID FROM XT_" + tableName + " WHERE ClientROWUID ={0} ".SqlFormatString(clientROWUID.ToString()))
        End If

        'If Not db.DataCache.ContainsKey("TableDeleteAction") Then
        '    db.GetDataTable("SELECT dta.*, f.Id As FieldId FROM DataSourceTableAction dta INNER JOIN DataSourceField f ON dta.TableId = f.TableId AND dta.FieldName = f.Name WHERE TableId = {0} AND Action = 'D'".SqlFormat(False, tableId), , "TableDeleteAction")
        'End If
        If clientROWUID > 0 Then
            __deleteChildRecords(db, parcelId, clientROWUID, tableName, loginId, eventDate, latitude, longitude, SyncID)
            db.Execute("UPDATE XT_" + tableName + "   SET CC_Deleted = 1 WHERE ROWUID = {0}".SqlFormatString(clientROWUID.ToString()))
            If ParcelEventLog.Enabled Then
                ParcelEventLog.CreateTableEvent(db, ParcelEventLog.EventType.RowDeleted, ParcelEventLog.ApplicationType.MA, parcelId, tableId, clientROWUID)
            End If
            DeleteTableAction(db, parcelId, clientROWUID, tableId, loginId, eventDate, longitude, latitude, SyncID)
            Dim ChangeCount As Integer = db.GetIntegerValue("SELECT COUNT(*) FROM ParcelChanges pc INNER JOIN DataSourceField df ON df.id=pc.FieldId WHERE AuxROWUID={0} AND pc.parcelId={1} AND df.TableId={2}  AND df.IsCustomField=0 AND action='new'".SqlFormat(False, clientROWUID, parcelId, tableId))
            If ChangeCount = 0 Then
                _processParcelDataChange(db, loginId, eventDate, parcelId, clientROWUID, DFID, Nothing, Nothing, latitude, longitude, SyncID, "delete")
            End If
            Dim id = db.GetIntegerValue("SELECT Id FROM parcel WHERE id=" & parcelId & " and CC_Error like '%" + tableName + "(" & clientROWUID & ")%'")
            If id > 0 Then
                db.Execute("update parcel set CC_Error=NULL where id=" + id.ToString())
            End If
            Return "Aux Record #" + clientROWUID.ToString() + " deleted from " + tableName
        Else
            Return ""
        End If

    End Function
    Public Shared Sub DeleteTableAction(db As Database, parcelid As Integer, clientROWUID As Integer, tableId As Integer, userName As String, eventDate As DateTime, Optional longitude As Long = 0, Optional latitude As Long = 0, Optional SyncID As String = "")
        Dim ChangeCount As Integer = db.GetIntegerValue("SELECT COUNT(*) FROM ParcelChanges pc INNER JOIN DataSourceField df ON df.id=pc.FieldId WHERE AuxROWUID={0} AND pc.parcelId={1} AND df.TableId={2} AND df.IsCustomField=0 AND action='new'".SqlFormat(False, clientROWUID, parcelid, tableId))
        If ChangeCount = 0 Then
            Dim deleteActions = db.GetDataTable("SELECT dta.*, f.Id As FieldId FROM DataSourceTableAction dta INNER JOIN DataSourceField f ON dta.TableId = f.TableId AND dta.FieldName = f.Name WHERE dta.TableId = {0} AND Action = 'D'".SqlFormat(False, tableId)).AsEnumerable.Select(Function(x) New With {.FieldName = x("FieldName"), .ValueTag = x("ValueTag"), .FieldId = x("FieldId")}).ToList
            For Each da In deleteActions
                Dim fieldValue As Object
                Dim tag As String
            	If (string.IsNullOrEmpty(da.ValueTag.ToString())) Then
            		tag = ""
            	Else
            		tag = da.ValueTag
            	End If
                Select Case tag
                    Case "LoginId"
                        fieldValue = userName
                    Case "EventDate"
                        fieldValue = eventDate
                    Case "CurrentYear"
                        fieldValue = eventDate.Year
                    Case "CurrentMonth"
                        fieldValue = eventDate.Month
                    Case "CurrentDay"
                        fieldValue = eventDate.Day
                    Case Else
                        fieldValue = tag
                End Select
                _processParcelDataChange(db, userName, eventDate, parcelid, clientROWUID, da.FieldId, Nothing, fieldValue, latitude, longitude, SyncID, "edit")
            Next
        End If

    End Sub
    Private Shared Function _deleteChildRecordsOnly(db As Database, parcelId As Integer, loginId As String, clientROWUID As Long, tableName As String, childTableNameFilter As String, eventDate As Date, latitude As Single, longitude As Single, SyncID As String, Optional RegisterAuditTrail As Boolean = False)
        Dim countString As String = ""

        If clientROWUID < 0 Then
            clientROWUID = db.GetIntegerValue("SELECT ROWUID FROM XT_" + tableName + " WHERE ClientROWUID ={0} ".SqlFormatString(clientROWUID.ToString()))
        End If

        If clientROWUID > 0 Then
            Dim childTablesSql As String = "SELECT DISTINCT tt.Name FROM DataSourceRelationships r LEFT OUTER JOIN DataSourceTable pt ON r.ParentTableId = pt.Id LEFT OUTER JOIN DataSourceTable tt ON r.TableId = tt.Id WHERE r.Relationship = 2 AND pt.Name = {0}"
            For Each c As DataRow In db.GetDataTable(childTablesSql.SqlFormatString(tableName)).Rows
                Dim n As Integer = 0
                Dim childTableName As String = c.GetString("Name")
                'Dim tableId As Integer = db.GetIntegerValueOrInvalid("SELECT Id FROM DataSourceTable WHERE Name = " + childTableName.ToSqlValue)

                Dim dr As DataRow = db.GetTopRow("SELECT Id,DisableDeletePCI FROM DataSourceTable WHERE Name = " + childTableName.ToSqlValue)
                Dim tableId As Integer = dr.GetInteger("Id")
                Dim DisableDeletePCI As Boolean = dr.GetBoolean("DisableDeletePCI")
                'vm Dim deleteActions = db.GetDataTable("SELECT dta.*, f.Id As FieldId FROM DataSourceTableAction dta INNER JOIN DataSourceField f ON dta.TableId = f.TableId AND dta.FieldName = f.Name WHERE dta.TableId = {0} AND Action = 'D'".SqlFormat(False, tableId)).AsEnumerable.Select(Function(x) New With {.FieldName = x("FieldName"), .ValueTag = x("ValueTag"), .FieldId = x("FieldId")}).ToList


                If childTableNameFilter = "*" Or childTableNameFilter = "" Or childTableNameFilter Is Nothing Then
                ElseIf childTableNameFilter <> childTableName Then
                    Continue For
                End If

                Dim DFID = db.GetIntegerValue("SELECT MIN(Id) FROM DataSourceField WHERE SourceTable ={0}".SqlFormatString(childTableName))
                For Each cr As DataRow In db.GetDataTable("EXEC cc_GetChildROWUIDs {0}, {1}, {2}".SqlFormatString(tableName, childTableName, clientROWUID)).Rows
                    Dim childRowUID As Integer = cr.GetInteger("ROWUID")
                    __deleteChildRecords(db, parcelId, childRowUID, childTableName, loginId, eventDate, latitude, longitude, SyncID)
                    db.Execute("UPDATE XT_" + childTableName + "   SET CC_Deleted = 1 WHERE ROWUID = {0}".SqlFormatString(childRowUID.ToString()))
                    If ParcelEventLog.Enabled Then
                        ParcelEventLog.CreateTableEvent(db, ParcelEventLog.EventType.RowDeleted, ParcelEventLog.ApplicationType.MA, parcelId, tableId, childRowUID)
                    End If
                    DeleteTableAction(db, parcelId, clientROWUID, tableId, loginId, eventDate, longitude, latitude, SyncID)
                    Dim ChangeCount As Integer = db.GetIntegerValue("SELECT COUNT(*) FROM ParcelChanges pc INNER JOIN DataSourceField df ON df.id=pc.FieldId WHERE AuxROWUID={0} AND pc.parcelId={1} AND df.TableId={2} AND df.IsCustomField=0 AND action='new'".SqlFormat(False, clientROWUID, parcelId, tableId))
                    If  ChangeCount = 0 Then
                        _processParcelDataChange(db, loginId, eventDate, parcelId, childRowUID, DFID, Nothing, Nothing, latitude, longitude, SyncID, "delete")
                    End If

                    n += 1
                Next
                If n > 0 Then
                    countString += n & " record(s) deleted from " + childTableName + "; "
                End If
            Next
            If (RegisterAuditTrail = True And countString <> "") Then
                ParcelAuditStream.CreateFlagEvent(db, eventDate, parcelId, loginId, countString, "MA")
            End If
            Return countString
        Else
            Return ""
        End If
    End Function

    Private Shared Sub __deleteChildRecords(db As Database, parcelId As Integer, parentRowUID As Long, tableName As String, loginId As String, eventDate As Date, latitude As Single, longitude As Single, SyncID As String)
        Dim childTablesSql As String = "SELECT DISTINCT tt.Name FROM DataSourceRelationships r LEFT OUTER JOIN DataSourceTable pt ON r.ParentTableId = pt.Id LEFT OUTER JOIN DataSourceTable tt ON r.TableId = tt.Id WHERE r.Relationship = 2 AND pt.Name = {0}"
        For Each c As DataRow In db.GetDataTable(childTablesSql.SqlFormatString(tableName)).Rows
            Dim childTableName As String = c.GetString("Name")
            Dim tableId As Integer = db.GetIntegerValueOrInvalid("SELECT Id FROM DataSourceTable WHERE Name = " + childTableName.ToSqlValue)
            Dim DFID = db.GetIntegerValue("SELECT MIN(Id) FROM DataSourceField WHERE SourceTable ={0}".SqlFormatString(childTableName))
            'vm Dim deleteActions = db.GetDataTable("SELECT dta.*, f.Id As FieldId FROM DataSourceTableAction dta INNER JOIN DataSourceField f ON dta.TableId = f.TableId AND dta.FieldName = f.Name WHERE dta.TableId = {0} AND Action = 'D'".SqlFormat(False, tableId)).AsEnumerable.Select(Function(x) New With {.FieldName = x("FieldName"), .ValueTag = x("ValueTag"), .FieldId = x("FieldId")}).ToList


            For Each cr As DataRow In db.GetDataTable("EXEC cc_GetChildROWUIDs {0}, {1}, {2}".SqlFormatString(tableName, childTableName, parentRowUID)).Rows
                Dim childRowUID As Integer = cr.GetInteger("ROWUID")
                __deleteChildRecords(db, parcelId, childRowUID, childTableName, loginId, eventDate, latitude, longitude, SyncID)
                db.Execute("UPDATE XT_" + childTableName + "   SET CC_Deleted = 1 WHERE ROWUID = {0}".SqlFormatString(childRowUID.ToString()))
                If ParcelEventLog.Enabled Then
                    ParcelEventLog.CreateTableEvent(db, ParcelEventLog.EventType.RowDeleted, ParcelEventLog.ApplicationType.MA, parcelId, tableId, childRowUID)
                End If
                Dim ChangeCount As Integer = db.GetIntegerValue("SELECT COUNT(*) FROM ParcelChanges pc INNER JOIN DataSourceField df ON df.id=pc.FieldId WHERE AuxROWUID={0} AND pc.parcelId={1} AND df.TableId={2} AND df.IsCustomField=0 AND action='new'".SqlFormat(False, childRowUID, parcelId, tableId))
                If ChangeCount = 0 Then
                    _processParcelDataChange(db, loginId, eventDate, parcelId, childRowUID, DFID, Nothing, Nothing, latitude, longitude, SyncID, "delete", True)
                End If
                DeleteTableAction(db, parcelId, childRowUID, tableId, loginId, eventDate, longitude, latitude, SyncID)
            Next
        Next
    End Sub

    'Private Shared Function DataSource.FindTargetTable(ByVal db As Database, ByVal tableName As String, Optional ByRef targetTableOrginalName As String = "") As String
    '    Dim sql As String = "SELECT t2.Name FROM DataSourceRelationships r INNER JOIN DataSourceTable t1 ON r.TableId = t1.Id AND r.Relationship = 0 INNER JOIN DataSourceTable t2 ON r.ParentTableId = t2.Id WHERE t1.Name = {0}"
    '    Dim parentTable As String = "", targetTable As String = tableName
    '    While True
    '        parentTable = db.GetStringValue(sql.SqlFormatString(targetTable))
    '        If parentTable = "" Then
    '            Exit While
    '        Else
    '            targetTable = parentTable
    '        End If
    '    End While

    '    targetTableOrginalName = targetTable

    '    If targetTable.Trim.ToLower = db.Application.ParcelTable.Trim.ToLower Then
    '        Return "ParcelData"
    '    ElseIf targetTable.Trim.ToLower = db.Application.ParcelTable.Trim.ToLower Then
    '        Return "NeighborhoodData"
    '    ElseIf targetTable <> String.Empty Then
    '        Return "XT_" + targetTable
    '    Else
    '        Return String.Empty
    '    End If
    'End Function

    Public Shared Sub TestCustomChangeAction(db As Database, parcelId As Integer, loginId As String, action As String, value As Object, eventDate As DateTime, latitude As Long, longitude As Long)
        doCustomChangeAction(db, parcelId, loginId, action, value, eventDate, latitude, longitude)
    End Sub


    Private Shared Sub doCustomChangeAction(db As Database, parcelId As Integer, loginId As String, action As String, value As Object, eventDate As DateTime, latitude As Single, longitude As Single, Optional SyncID As String = "")

        db.Execute("EXEC CC_doCustomChangeAction {0},{1},{2},{3},{4},{5},{6},{7}".SqlFormat(True, parcelId, loginId, action, value, eventDate, latitude, longitude, SyncID))

        '        For Each dr As DataRow In db.GetDataTable("SELECT * FROM ClientCustomAction WHERE Action = " + action.ToSqlValue + " AND Enabled = 1 ORDER BY Ordinal, Id").Rows
        '            Dim fieldValue As Object = value
        '            Dim tableName As String = dr.GetString("TableName")
        '            Dim fieldName As String = dr.GetString("FieldName")
        '            Dim valueTag As String = dr.GetString("ValueTag")
        '            Dim customText As String = dr.GetString("CustomText")
        '            Dim excludeTimeValue As String = ClientSettings.PropertyValue("ExcludeTimeFromCustomAction")
        '            Dim tableId As Integer = db.GetIntegerValueOrInvalid("SELECT Id FROM DataSourceTable WHERE Name = " + tableName.ToSqlValue)
        '            If tableId <> -1 Then
        '                Dim fieldId As Integer = db.GetIntegerValueOrInvalid("SELECT Id FROM DataSourceField WHERE Name = " + fieldName.ToSqlValue + " AND TableId = " + tableId.ToString)
        '                If fieldId <> -1 Then
        '                    Select Case valueTag
        '                        Case ""
        '                            fieldValue = value
        '                        Case "EventDate"
        '                            fieldValue = eventDate
        '                            If excludeTimeValue = 1 Then
        '                                fieldValue = fieldValue.ToString.Split(" ")(0) + " 12:00 AM"
        '                            End If
        '                        Case "EventDateLocal"
        '                            fieldValue = Database.Tenant.GetStringValue("SELECT dbo.GetLocalDate('" + eventDate + "')")
        '                            If excludeTimeValue = 1 Then
        '                                fieldValue = fieldValue.ToString.Split(" ")(0) + " 12:00 AM"
        '                            End If
        '                        Case "LoginId"
        '                            fieldValue = loginId
        '                        Case "CurrentYear"
        '                            fieldValue = eventDate.Year
        '                        Case "CurrentMonth"
        '                            fieldValue = eventDate.Month
        '                        Case "CurrentDay"
        '                            fieldValue = eventDate.Day
        '                        Case "CustomValue"
        '                            fieldValue = customText
        '                        Case Else
        '                            If IsNumeric(valueTag) Then
        '                                Dim sourceFieldName As String = db.GetStringValue("SELECT Name FROM DataSourceField WHERE Id = " + valueTag)
        '                                fieldValue = db.GetStringValue("SELECT [" + sourceFieldName + "] FROM ParcelData WHERE CC_ParcelId = " & parcelId)
        '                            End If
        '                    End Select
        '
        '                    Dim sql As String = "INSERT INTO ParcelChanges  (ParcelId, FieldId, Action, ReviewedBy, OriginalValue, NewValue, ChangedTime, Latitude, Longitude, ParentAuxROWUID,SyncID) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}); SELECT CAST(@@IDENTITY AS INT) As NewId;"
        '                    Dim changeId As Integer = db.GetIntegerValue(sql.SqlFormat(True, parcelId, fieldId, action, loginId, "", fieldValue, eventDOKate, latitude, longitude, 0, SyncID))
        '                    db.Execute("EXEC processFieldChangesTracking {0},{1},{2},{3}".SqlFormat(True, parcelId, dr.GetString("Id"), changeId, fieldValue))
        '                    ParcelAuditStream.CreateDataChangeEvent(db, eventDate, parcelId, 0, loginId, fieldId, fieldValue, changeId)
        '                End If
        '            End If
        '        Next
    End Sub

    Private Shared Sub _actionOnMarkAsCompleted(db As Database, parcelId As Integer, loginId As Integer, eventDate As Date)
        Dim markAsCompleteTable As String = ClientSettings.PropertyValue("customaction_markascomplete_table")
        Dim markAsCompleteField As String = ClientSettings.PropertyValue("customaction_markascomplete_table")
        Dim customValue As String = ClientSettings.PropertyValue("customaction_markascomplete_table")
    End Sub

    Private Shared Function _updateSketchForTA(db As Database, parcelId As Integer, loginId As String, rowUID As Long, sketchVector As String, eventDate As Date, latitude As Single, longitude As Single, SyncID As String) As String

        Dim sql As String
        Dim changeAction As String = "edit"
        Dim fieldName As String = IIf(ClientSettings.PropertyValue("SketchVectorCommandField") IsNot Nothing, ClientSettings.PropertyValue("SketchVectorCommandField"), ProcessParcelDataChange.SketchSettingsPropertyValue("SketchVectorCommandField"))
        Dim sketchVectorTable = IIf(ClientSettings.PropertyValue("SketchVectorTable") IsNot Nothing, ClientSettings.PropertyValue("SketchVectorTable"), ProcessParcelDataChange.SketchSettingsPropertyValue("SketchVectorTable"))
        Dim sketchVectorTargetTable = DataSource.FindTargetTable(db, sketchVectorTable)
        Dim newValue As String = ""
        Dim fieldId As Integer = db.GetIntegerValue("SELECT Id FROM DataSourceField WHERE Name = " + fieldName.ToSqlValue + " AND SourceTable = '" + sketchVectorTable + "'")
        ' Dim parentRowUID As String

        If rowUID < 0 Then
            rowUID = db.GetIntegerValue("SELECT ROWUID FROM  " + sketchVectorTargetTable + "  WHERE ClientROWUID = " + rowUID.ToString)
            If rowUID = 0 Then
                Return "Sketch update failed since the target row does not exist."
            End If
            changeAction = "new"
        End If
        Dim ar As DataRow = Nothing
        If rowUID > 0 Then
            ar = db.GetTopRow("SELECT " + fieldName + ",ParentROWUID FROM  " + sketchVectorTargetTable + "  WHERE ROWUID = " + rowUID.ToString)
            'oldValue = db.GetStringValue("SELECT " + fieldName + " FROM  " + sketchVectorTargetTable + "  WHERE ROWUID = " + rowUID.ToString)
            'parentRowUID = db.GetStringValue("SELECT ParentROWUID FROM  " + sketchVectorTargetTable + "  WHERE ROWUID = " + rowUID.ToString)
            sql = "UPDATE  " + sketchVectorTargetTable + "     SET " + fieldName + " = " + sketchVector.ToSqlValue + " WHERE ROWUID = " + rowUID.ToString
        Else
            'Ignore this error, as the update should have come from a sketch edit on a deleted record. There might be a non-fatal bug on MA allowing editing of deleted records.
            'Throw New Exception("Attempt to update sketch data to a non-existent record.")
            Return ""
        End If
        Try
            db.Execute(sql)

            Dim changeSql As String = "INSERT INTO ParcelChanges  (ParcelId, FieldId, Action, ReviewedBy, OriginalValue, NewValue, ChangedTime, Latitude, Longitude, AuxROWUID, ParentAuxROWUID,SyncID,CC_YearStatus) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10},{11},{12}); SELECT CAST(@@IDENTITY AS INT) As NewId;"
            Dim changeId As Integer = db.GetIntegerValue(changeSql.SqlFormat(True, parcelId, fieldId, changeAction, loginId, ar.GetString(fieldName), sketchVector, eventDate, latitude, longitude, rowUID, ar.GetString(" parentRowUID"), SyncID, ar.GetString(" CC_YearStatus")))
            db.Execute("EXEC processFieldChangesTracking {0},{1},{2},{3}".SqlFormat(True, parcelId, fieldId, changeId, sketchVector))
            Parcel.ElevatePriorityIfNormal(db, parcelId, loginId)
            Parcel.ResetReviewStatus(db, parcelId, loginId)

            Return "Sketch vector updated."
        Catch ex As Exception
            ErrorMailer.ReportException("MA-Sync", ex, System.Web.HttpContext.Current.Request)
            Return "Sketch vector update failed."
        End Try
    End Function

    Private Shared Function _updateSketch(db As Database, parcelId As Integer, loginId As String, ByRef rowUID As Long, sketchFields As String, sketchData As String, eventDate As Date, latitude As Single, longitude As Single, SyncID As String) As String
        Dim errorMessage As String = SketchDataProcessor.UpdateSketch(db, parcelId, loginId, rowUID, sketchFields, sketchData, eventDate, latitude, longitude, errorMessage, SyncID, "", "1")
        If errorMessage = "note" Or errorMessage = "vector" Then
            Return "Sketch " + errorMessage + " updated for Aux Record #" + rowUID.ToString() + "."
        ElseIf errorMessage = "" Then
            Return ""
        Else
            Return "Sketch vector update failed. " + errorMessage
        End If

        'Dim fieldParts = sketchFields.Split("|")
        'Dim dataParts = sketchData.Split("|")

        'Dim commandFieldId As Integer = fieldParts(0)
        'Dim areaFieldId As Integer = -1
        'Dim perimeterFieldId As Integer = -1
        'Dim sketchCmds As String = dataParts(0)
        'Dim area As Single = 0
        'Dim perimeter As Single = 0

        'If fieldParts.Length > 1 AndAlso dataParts.Length > 1 Then
        '    areaFieldId = fieldParts(1)
        '    area = dataParts(1)
        'End If

        'If fieldParts.Length > 2 AndAlso dataParts.Length > 2 Then
        '    perimeterFieldId = fieldParts(2)
        '    perimeter = dataParts(2)
        'End If


        'Dim sql As String
        'Dim changeAction As String = "edit"

        'Dim oldValue As String = "", newValue As String = "", oldAreaValue As String = "", oldPerimeterValue As String = ""
        'Dim sketchFieldRow = db.GetTopRow("SELECT * FROM DataSourceField WHERE Id = " & commandFieldId)
        'Dim sketchVectorTable = sketchFieldRow.GetString("SourceTable")
        'Dim commandFieldName As String = sketchFieldRow.GetString("Name")
        'Dim areaFieldName As String = "", perimeterFieldName As String = ""
        'If areaFieldId <> -1 Then
        '    areaFieldName = db.GetTopRow("SELECT * FROM DataSourceField WHERE Id = " & areaFieldId).GetString("Name")
        'End If
        'If perimeterFieldId <> -1 Then
        '    perimeterFieldName = db.GetTopRow("SELECT * FROM DataSourceField WHERE Id = " & perimeterFieldId).GetString("Name")
        'End If

        'Dim sketchVectorTargetTable = DataSource.FindTargetTable(db, sketchVectorTable)
        'Dim parentRowUID As String

        'If rowUID < 0 Then
        '    rowUID = db.GetIntegerValue("SELECT ROWUID FROM  " + sketchVectorTargetTable + "  WHERE ClientROWUID = " + rowUID.ToString)
        '    If rowUID = 0 Then
        '        Return "Sketch update failed since the target row does not exist."
        '    End If
        '    changeAction = "new"
        'End If

        'If rowUID > 0 Then
        '    Dim rec As DataRow = db.GetTopRow("SELECT * FROM  " + sketchVectorTargetTable + "  WHERE ROWUID = " + rowUID.ToString)
        '    oldValue = rec.GetString(commandFieldName)
        '    If areaFieldId <> -1 Then oldAreaValue = rec.GetString(areaFieldName)
        '    If perimeterFieldId <> -1 Then oldPerimeterValue = rec.GetString(perimeterFieldName)
        '    parentRowUID = rec.GetString("ParentROWUID")

        '    Dim updateSetData = commandFieldName + " = " + sketchCmds.ToSqlValue
        '    If areaFieldId <> -1 Then
        '        updateSetData += ", " + areaFieldName + " = " + area.ToString
        '    End If
        '    If perimeterFieldId <> -1 Then
        '        updateSetData += ", " + perimeterFieldName + " = " + perimeter.ToString
        '    End If
        '    sql = "UPDATE  " + sketchVectorTargetTable + "     SET " + updateSetData + " WHERE ROWUID = " + rowUID.ToString
        'Else
        '    Throw New Exception("Attempt to update sketch data to a non-existent record.")
        'End If
        'Try
        '    db.Execute(sql)

        '    Dim changeSql As String = "INSERT INTO ParcelChanges  (ParcelId, FieldId, Action, ReviewedBy, OriginalValue, NewValue, ChangedTime, Latitude, Longitude, AuxROWUID, ParentAuxROWUID) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}); SELECT CAST(@@IDENTITY AS INT) As NewId;"

        '    Dim changeId As Integer = db.GetIntegerValue(changeSql.SqlFormat(True, parcelId, commandFieldId, changeAction, loginId, oldValue, sketchCmds, eventDate, latitude, longitude, rowUID, parentRowUID))
        '    If areaFieldId <> -1 Then
        '        changeId = db.GetIntegerValue(changeSql.SqlFormat(True, parcelId, areaFieldId, changeAction, loginId, oldAreaValue, area, eventDate, latitude, longitude, rowUID, parentRowUID))
        '    End If
        '    If perimeterFieldId <> -1 Then
        '        changeId = db.GetIntegerValue(changeSql.SqlFormat(True, parcelId, perimeterFieldId, changeAction, loginId, oldPerimeterValue, perimeter, eventDate, latitude, longitude, rowUID, parentRowUID))
        '    End If

        '    Parcel.ElevatePriorityIfNormal(db, parcelId)
        '    Parcel.ResetReviewStatus(db, parcelId)

        '    Return "Sketch vector updated."
        'Catch ex As Exception
        '    ErrorMailer.ReportException("MA-Sync", ex, System.Web.HttpContext.Current.Request)
        '    Return "Sketch vector update failed."
        'End Try
    End Function

    Private Shared Function _updateSketchAreaForTA(db As Database, parcelId As Integer, loginId As String, rowUID As Long, sketchArea As String, eventDate As Date, latitude As Single, longitude As Single, SyncID As String) As String


        Dim sql As String

        Dim changeAction As String = "edit"
        Dim fieldName As String = IIf(ClientSettings.PropertyValue("SketchAreaField") IsNot Nothing, ClientSettings.PropertyValue("SketchAreaField"), ProcessParcelDataChange.SketchSettingsPropertyValue("SketchAreaField"))
        If fieldName = "" Then
            Return ""
        End If

        Dim sketchVectorTable = IIf(ClientSettings.PropertyValue("SketchVectorTable") IsNot Nothing, ClientSettings.PropertyValue("SketchVectorTable"), ProcessParcelDataChange.SketchSettingsPropertyValue("SketchVectorTable"))
        Dim sketchTargetTable = DataSource.FindTargetTable(db, sketchVectorTable)
        Dim oldValue As String = "", newValue As String = ""
        Dim fieldId As Integer = db.GetIntegerValue("SELECT Id FROM DataSourceField WHERE Name = " + fieldName.ToSqlValue + " AND SourceTable = '" + sketchVectorTable + "'")
        Dim parentRowUID As String

        If rowUID < 0 Then
            rowUID = db.GetIntegerValue("SELECT ROWUID FROM  " + sketchTargetTable + "  WHERE ClientROWUID = " + rowUID.ToString)
            If rowUID = 0 Then
                Return "Sketch update failed since the target row does not exist."
            End If
            changeAction = "new"
        End If

        If rowUID > 0 Then
            oldValue = db.GetStringValue("SELECT " + fieldName + " FROM  " + sketchTargetTable + "  WHERE ROWUID = " + rowUID.ToString)
            parentRowUID = db.GetStringValue("SELECT ParentROWUID FROM  " + sketchTargetTable + "  WHERE ROWUID = " + rowUID.ToString)
            sql = "UPDATE  " + sketchTargetTable + "     SET " + fieldName + " = " + sketchArea.ToSqlValue + " WHERE ROWUID = " + rowUID.ToString
        Else
            Throw New Exception("Attempt to update sketch data to a non-existent record.")
        End If
        Try
            db.Execute(sql)
            Parcel.ElevatePriorityIfNormal(db, parcelId, loginId)
            Parcel.ResetReviewStatus(db, parcelId, loginId)

            Dim changeSql As String = "INSERT INTO ParcelChanges  (ParcelId, FieldId, Action, ReviewedBy, OriginalValue, NewValue, ChangedTime, Latitude, Longitude, AuxROWUID, ParentAuxROWUID,SyncID,CC_YearStatus) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10},{11},{12}); SELECT CAST(@@IDENTITY AS INT) As NewId;"
            Dim changeId As Integer = db.GetIntegerValue(changeSql.SqlFormat(True, parcelId, fieldId, changeAction, loginId, oldValue, sketchArea, eventDate, latitude, longitude, rowUID, parentRowUID, SyncID))

            db.Execute("EXEC processFieldChangesTracking {0},{1},{2},{3}".SqlFormat(True, parcelId, fieldId, changeId, sketchArea))
            Return ""
            'Return "Sketch vector updated."
        Catch ex As Exception
            ErrorMailer.ReportException("MA-Sync", ex, System.Web.HttpContext.Current.Request)
            Return ""
            'Return "Sketch vector update failed."
        End Try

    End Function
    Private Shared Function _futureYearCopy(db As Database, parcelId As Integer, rowUID As String, loginId As String, oldValueOrOption As String, tableName As String, eventDate As Date, latitude As Single, longitude As Single, SyncID As String, mode As String)
        Dim sourceRowUid As Long = 0, clientParentRowUid As Long = 0
        Dim sourceParcelData = oldValueOrOption.Split("$")
        Dim sourceParcelId = sourceParcelData(0)
        If sourceParcelData.Length > 1 Then
            sourceRowUid = IIf(sourceParcelData(1) = "", 0, sourceParcelData(1))
        End If

        Dim targetParcelData = rowUID.ToString().Split("$")
        Dim clientRowUid = targetParcelData(0)

        If targetParcelData.Length > 1 Then
            clientParentRowUid = IIf(targetParcelData(1) = "", 0, targetParcelData(1))
        End If
        Return _improvementCopy(db, parcelId, sourceParcelId, sourceRowUid, loginId, clientRowUid, clientParentRowUid, tableName, eventDate, latitude, longitude, SyncID, "futurecopy", mode)
    End Function
    Private Shared Function _templateCopy(db As Database, parcelId As Integer, rowUID As String, loginId As String, Filter As String, tableName As String, eventDate As Date, latitude As Single, longitude As Single, SyncID As String)
        Dim t As String = String.Empty
        Dim para = Filter.Split("$")
        Dim obj = tableName.Split("$")
        Dim templateTableName = obj(0)
        Dim parentTableName As String = para(0)
        Dim filterValues = para(1)
        Dim templateFutureCopy As String = "NULL"
        If para.Length > 2 Then
            templateFutureCopy = para(2)
        End If
        If parentTableName = "" Or templateTableName = "" Then
            Return "Invalid source row specification for " + templateTableName + " from parcel Template Data"
        End If
        Dim fieldCategoryName = db.GetStringValue("SELECT name FROM fieldcategory WHERE SourceTableName={0}".SqlFormatString(templateTableName))
        Dim ParentROWUID = db.GetIntegerValue("SELECT ROWUID from XT_" + parentTableName + " WHERE " + filterValues)
        Dim templateTable = db.GetDataTable("SELECT * from XT_" + templateTableName + " WHERE CC_ParcelID=-99 AND ParentROWUID =" + ParentROWUID.ToString())
        Dim clientROWUIDs = rowUID.Split("$")
        Dim index As Integer = 0
        Dim mcisCopy As Boolean = True
        If clientROWUIDs.Length > 1 AndAlso clientROWUIDs(1) = "first" Then
            If (clientROWUIDs(0).Contains("^")) Then
                Dim templateROWUID = clientROWUIDs(0).Split("^")(0)
                templateTable = db.GetDataTable("SELECT TOP 1 * from XT_" + templateTableName + " WHERE CC_ParcelID=-99 AND ParentROWUID =" + ParentROWUID.ToString() + " AND ROWUID =" + templateROWUID.ToString())
            End If
            If (templateTable.Rows.Count = 0) Then
                templateTable = db.GetDataTable("SELECT TOP 1 * from XT_" + templateTableName + " WHERE CC_ParcelID=-99 AND ParentROWUID =" + ParentROWUID.ToString())
            End If
            mcisCopy = False
        End If
        If (clientROWUIDs.Length <> templateTable.Rows.Count And mcisCopy) Or templateTable.Rows.Count = 0 Then
            db.Execute("update Parcel set CC_Error='Copy feature template failed due to mismatch in data. The original feature records have not been deleted from the account. The property should either be Soft Rejected and another Copy Feature Template done from mobile, or QC Approved and Copy Feature Template done within CAMA.||" + parentTableName + "(" + ParentROWUID.ToString() + ")" + "' where Id= " & parcelId.ToString())
            Return "Copy data from " + templateTableName + " ignored due to mismatch in data"
        End If
        If templateTable.Rows.Count > 0 And mcisCopy Then
            _deleteChildRecordsOnly(db, parcelId, loginId, obj(1), parentTableName, templateTableName, eventDate, latitude, longitude, SyncID, True)
        End If
        Dim availtemplate As Boolean = True
        For Each dr As DataRow In templateTable.Rows
            Dim clientRowID As String
            Dim i As Integer = 0
            If availtemplate Then
                While i < clientROWUIDs.Length
                    If clientROWUIDs(i).Contains(dr("ROWUID")) Then
                        clientRowID = clientROWUIDs(i).Split("^")(1)
                        Exit While
                    End If
                    i += 1
                End While
            End If
            If clientRowID Is Nothing Then
                clientRowID = clientROWUIDs(index).Split("^")(1)
                availtemplate = False
            End If
            If templateFutureCopy = "F" Then
                t = _improvementCopy(db, parcelId, dr("CC_Parcelid"), dr("ROWUID"), loginId, clientRowID, obj(1), templateTableName, eventDate, latitude, longitude, SyncID, "templatefuturecopy")
            Else
                t = _improvementCopy(db, parcelId, dr("CC_Parcelid"), dr("ROWUID"), loginId, clientRowID, obj(1), templateTableName, eventDate, latitude, longitude, SyncID)
            End If
            index += 1
        Next
        Return templateTable.Rows.Count.ToString + " " + t
    End Function
    Public Shared Function _improvementCopy(db As Database, parcelId As Integer, sourceParcelId As Integer, sourceRowUid As Long, loginId As String, clientROWUID As Long, clientParentROWUID As Long, tableName As String, eventDate As Date, latitude As Single, longitude As Single, SyncID As String, Optional copyType As String = "", Optional mode As String = "")
        Dim parentTableName As String = ""
        Dim parentROWUID As Integer = 0
        Dim obj = tableName.Split("$")
        tableName = obj(0)
        Dim TargetType As String = ""
        Dim isCurrentSingleCopy As String = ""
        If (obj.Count > 1) Then
            TargetType = obj(1)
        End If

        If clientParentROWUID = 0 Then
        ElseIf clientParentROWUID > 0 Then
            parentROWUID = clientParentROWUID
            parentTableName = tableName
        Else
            Dim dtValues = db.GetDataTable("SELECT p.Name,r.Id 	FROM DataSourceRelationships r INNER JOIN DataSourceTable t ON r.TableId = t.Id INNER JOIN DataSourceTable p ON r.ParentTableId = p.Id 	WHERE t.Name = {0}".SqlFormatString(tableName))
            If (dtValues.Rows.Count > 0) Then
                parentTableName = dtValues.Rows(0).GetString("Name")
                Dim relationshipId = dtValues.Rows(0).GetInteger("Id")

                Dim parentCopyTable As String = DataSource.FindTargetTable(db, parentTableName)
                If Not parentCopyTable.StartsWith("XT_") Then
                    Return "Copy data from " + tableName + " ignored."
                End If

                parentROWUID = db.GetIntegerValue("SELECT ROWUID FROM " + parentCopyTable + " WHERE ClientROWUID={0}".SqlFormatString(clientParentROWUID.ToString()))

                If parentROWUID = 0 Then
                    Return "Copy data from " + tableName + " ignored since the source data does not exist."
                End If
            End If
        End If
        Dim insertFields = ""
        Dim selectFields = ""
        Dim newId As Integer
        Dim parentSourceName = tableName

        Dim filter = "CC_ParcelId=" + sourceParcelId.ToString()
        If sourceRowUid > 0 Then
            filter += " AND (ROWUID=" + sourceRowUid.ToString() + " OR ClientROWUID=" + sourceRowUid.ToString() + ")"
        Else
            filter += " AND ClientROWUID=" + sourceRowUid.ToString()
        End If
        Dim targetTable = DataSource.FindTargetTable(db, tableName)
        If copyType = "futurecopy" And tableName = "parcel" Then
            targetTable = "parcelData"
        End If
        Dim fieldCategoryName = db.GetStringValue("SELECT name FROM fieldcategory WHERE SourceTableName={0}".SqlFormatString(tableName))
        Dim sourceAUXRowUID As String = db.GetStringValue("SELECT RowUID FROM " + targetTable + " WHERE " + filter)
        If sourceParcelId = -99 And sourceAUXRowUID = "" Then
            '  db.Execute("update Parcel set FailedOnDownSync=1 ,DownSyncRejectReason='" + fieldCategoryName + " Template Copy by user " + loginId.ToString() + " failed due to mismatch in data. The suggested action is to review all data for accuracy and update as needed.' where Id= " & parcelId)
            db.Execute("update Parcel set CC_Error='" + fieldCategoryName + " Template Copy by user " + loginId.ToString() + " failed due to mismatch in data. The suggested action is to review all data for accuracy and update as needed.||" + parentTableName + "(" + parentROWUID.ToString() + ")" + "' where Id= " & parcelId.ToString())
            Return "Invalid source row specification for " + tableName + " from parcel Template Data"
        End If


        Dim keyValue1 = db.GetStringValue("SELECT KeyValue1 FROM Parcel WHERE Id = {0}".SqlFormatString(sourceParcelId))
        Dim keyField1 = db.Application.KeyField(1)
        If sourceAUXRowUID = "" Then
            Dim existingID = ""
            If sourceRowUid < 0 Then
                sourceAUXRowUID = db.GetStringValue("SELECT AUROWUID FROM DB_ROWUIDMap WHERE  TableName='" + tableName + "' AND  ClientROWUID='" + sourceRowUid.ToString() + "'")
            End If
            'check rowUID is existing or not in table
            If (sourceAUXRowUID <> "") Then
                existingID = db.GetStringValue("SELECT RowUID FROM " + targetTable + " WHERE ROWUID =" + sourceAUXRowUID)
            End If
            If (existingID = "") Then
                Return "Invalid source row specification for " + tableName + " from parcel " + IIf(sourceParcelId = -99, "Template Data", sourceParcelId.ToString())
            End If

        End If
        '
        ''''
        Dim getSql = "SELECT pt.Name, tf.Name As TableField, pf.Name As ReferenceField FROM DataSourceRelationships r LEFT OUTER JOIN DataSourceKeys k ON k.RelationshipId = r.Id LEFT OUTER JOIN DataSourceField tf ON k.FieldId = tf.Id LEFT OUTER JOIN DataSourceField pf ON k.ReferenceFieldId = pf.Id 	LEFT OUTER JOIN DataSourceTable tt ON r.TableId = tt.Id LEFT OUTER JOIN DataSourceTable pt ON r.ParentTableId = pt.Id WHERE tt.CC_TargetTable ={0} AND Relationship IS NOT NULL AND Relationship <> 0 AND k.IsReferenceKey = 1".SqlFormatString(targetTable)

        Dim dt As DataTable = db.GetDataTable(getSql)
        Dim yearField As String = db.GetStringValue("SELECT YearPartitionField FROM DatasourceTable WHERE name = {0}".SqlFormatString(tableName))
        If copyType = "futurecopy" Then
            Dim yearStatus As String = db.GetStringValue("SELECT CC_YearStatus FROM " + targetTable + " WHERE " + filter)
            Dim AUXROWUID As Integer = 0
            If (yearStatus = "F") Then
                copyType = "currentcopy"
            End If
        End If
        Dim yearFieldUpdated = False
        Dim ActualYearValue As String = db.GetStringValue("SELECT value from clientsettings  WHERE name = 'CurrentYearValue'")
        Dim FutureYearEnabled As String = db.GetStringValue("SELECT value from clientsettings  WHERE name = 'FutureYearEnabled'")
        Dim FYValue As String = db.GetStringValue("SELECT value FROM clientsettings WHERE name = 'FutureYearValue'")
        If String.IsNullOrEmpty(FYValue) Then
            FYValue = "0"
        End If
        For Each dr As DataRow In dt.Rows
            parentSourceName = dr.GetString("Name")
            insertFields = insertFields + "," + dr.GetString("TableField")
            If (dr.GetString("TableField") = yearField And (FutureYearEnabled = "1" Or FutureYearEnabled = "true")) Then
                If copyType = "templatefuturecopy" Then
                    selectFields = selectFields + ",0"
                Else
                    selectFields = selectFields + IIf(copyType = "futurecopy" Or TargetType = "F", "," + FYValue, "," + ActualYearValue)
                End If
                yearFieldUpdated = True
            Else
                selectFields = selectFields + "," + dr.GetString("ReferenceField")
            End If

        Next
        If (Not yearFieldUpdated) And yearField <> "" And (FutureYearEnabled = "1" Or FutureYearEnabled = "true") Then
            insertFields = insertFields + "," + yearField
            If copyType = "templatefuturecopy" Then
                selectFields = selectFields + ",0"
            Else
                selectFields = selectFields + IIf(copyType = "futurecopy" Or TargetType = "F", "," + FYValue, "," + ActualYearValue)
            End If
        End If
        Dim parentSourceTable = DataSource.FindTargetTable(db, parentSourceName)


        If Not targetTable.StartsWith("XT_") And copyType <> "futurecopy" Then
            Return "Copy data from " + tableName + " ignored."
        End If


        If (tableName = "parcel") Then
            Dim insertSql = ""
            If copyType = "visionCopy" Then
                insertSql = "INSERT INTO " + targetTable + "   (CC_ParcelId, ClientROWUID, CC_YearStatus) Select '" + parcelId.ToString + "','" + clientROWUID.ToString() + "','" + TargetType.ToString() + " SELECT CAST(@@IDENTITY  AS INT);".SqlFormatString(parcelId)
            Else
                insertSql = "INSERT INTO " + targetTable + "   (CC_ParcelId, ClientROWUID  " + IIf(copyType = "futurecopy" Or TargetType = "F", ",CC_LinkedROWUID,CC_YearStatus", "") + " ) Select '" + parcelId.ToString + "','" + clientROWUID.ToString() + "' " + IIf(copyType = "futurecopy" Or TargetType = "F", ("," + sourceAUXRowUID + ",'F'"), "") + " SELECT CAST(@@IDENTITY  AS INT);".SqlFormatString(parcelId)
            End If
            newId = db.GetIntegerValue(insertSql)
        ElseIf clientParentROWUID = 0 Then
            Dim insertSql = ""
            If copyType = "visionCopy" Then
                insertSql = "INSERT INTO " + targetTable + "   (CC_ParcelId, ClientROWUID, CC_YearStatus " + insertFields + " ) SELECT TOP 1 '" + parcelId.ToString + "','" + clientROWUID.ToString() + "','" + clientROWUID.ToString() + "','" + TargetType.ToString() + "' " + selectFields + " FROM   " + parentSourceTable + " WHERE CC_ParcelId ={0}; SELECT CAST(@@IDENTITY  AS INT);".SqlFormatString(parcelId)
            Else
                insertSql = "INSERT INTO " + targetTable + "   (CC_ParcelId, ClientROWUID " + insertFields + IIf(copyType = "futurecopy" Or TargetType = "F", ",CC_LinkedROWUID,CC_YearStatus", "") + " ) SELECT TOP 1 '" + parcelId.ToString + "','" + clientROWUID.ToString() + "' " + selectFields + IIf(copyType = "futurecopy" Or TargetType = "F", ("," + sourceAUXRowUID + ",'F'"), "") + "  FROM   " + parentSourceTable + " WHERE CC_ParcelId ={0}; SELECT CAST(@@IDENTITY  AS INT);".SqlFormatString(parcelId)
            End If
            newId = db.GetIntegerValue(insertSql)
        Else
            If parentROWUID > 0 Then
                Dim insertString = ""
                If copyType = "visionCopy" Then
                    insertString = "INSERT INTO " + targetTable + " (CC_ParcelId, ParentROWUID, ClientROWUID, ClientParentROWUID, CC_YearStatus " + insertFields + ")  SELECT '" + parcelId.ToString() + "','" + parentROWUID.ToString() + "','" + clientROWUID.ToString() + "','" + clientParentROWUID.ToString() + "','" + TargetType.ToString() + "' " + selectFields + " FROM " + parentSourceTable + "  WHERE ROWUID={0} OR ClientROWUID ={0};SELECT CAST(@@IDENTITY  AS INT)".SqlFormatString(parentROWUID.ToString())
                Else
                    insertString = "INSERT INTO " + targetTable + "  (CC_ParcelId, ParentROWUID, ClientROWUID, ClientParentROWUID " + insertFields + IIf(copyType = "futurecopy" Or TargetType = "F", ", CC_LinkedROWUID, CC_YearStatus", IIf(copyType = "templatefuturecopy", ", CC_YearStatus", "")) + ")  SELECT '" + parcelId.ToString() + "','" + parentROWUID.ToString() + "','" + clientROWUID.ToString() + "','" + clientParentROWUID.ToString() + "' " + selectFields + IIf(copyType = "futurecopy" Or TargetType = "F", ("," + sourceAUXRowUID + ",'F'"), IIf(copyType = "templatefuturecopy", ",'F'", "")) + " FROM " + parentSourceTable + "  WHERE ROWUID={0} OR ClientROWUID ={0};SELECT CAST(@@IDENTITY  AS INT)".SqlFormatString(parentROWUID.ToString())
                End If
                'Dim insertString = "INSERT INTO " + targetTable + "    (CC_ParcelId, ParentROWUID, ClientROWUID, ClientParentROWUID)  SELECT '" + parcelId.ToString() + "','" + parentROWUID.ToString() + "','" + clientROWUID.ToString() + "','" + clientParentROWUID.ToString() + "' FROM " + relatedParentTable + "  WHERE ROWUID={0};SELECT CAST(@@IDENTITY  AS INT)".SqlFormatString(parentROWUID.ToString())
                newId = db.GetIntegerValue(insertString)
            End If
        End If
copyChanges:
        If newId > 0 Then
            If (mode = "0") Then 'single record copy
                If (copyType = "currentcopy") Then
                    isCurrentSingleCopy = "currentcopy"
                End If
                copyType = "futureInsert"
            End If
            insertNewRecordDetails(db, parcelId, newId, tableName, clientROWUID, eventDate, loginId)
            Dim selectCopyFields = getFieldsWithoutPrimaryKeys(db, targetTable, False, copyType)
            Dim selectCopyFieldsConverted = getFieldsWithoutPrimaryKeys(db, targetTable, True, copyType)

            Dim updateSql As String = "UPDATE " + targetTable + " SET "
            Dim updatedColumns As Integer = 0
            For Each dr As DataRow In dt.Rows
                If db.GetIntegerValue("SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNs WHERE TABLE_NAME = 'ParcelData' AND COLUMN_NAME = " + dr.GetString("ReferenceField").ToSqlValue) > 0 Then
                    updatedColumns += 1
                    updateSql += "[" + dr.GetString("TableField") + "] = pd." + dr.GetString("ReferenceField") + ", "
                End If
                'relatedParentName = dr.GetString("Name")
                'insertFields = insertFields + "," + dr.GetString("TableField")
                'selectFields = selectFields + "," + dr.GetString("ReferenceField")
            Next
            updateSql = updateSql.Trim.TrimEnd(",")
            updateSql += " FROM ParcelData pd WHERE pd.CC_ParcelId = " + targetTable + ".CC_ParcelId AND  " + targetTable + ".ROWUID = " + newId.ToString
            If updatedColumns > 0 Then
                db.Execute(updateSql)
            End If


            Dim changeSql = "EXEC ProcessChangesForImprovementCopy {0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14}"
            db.Execute(changeSql.SqlFormatString(tableName, sourceParcelId.ToString(), parcelId.ToString(), sourceAUXRowUID, newId.ToString(), parentROWUID.ToString(), eventDate, loginId, latitude, longitude, String.Join(",", selectCopyFields), String.Join(",", selectCopyFieldsConverted), SyncID, copyType, isCurrentSingleCopy))
            'modified 2015.12.11
            If copyType <> "futurecopy" And copyType <> "visionCopy" And (targetTable = "XT_imprv" Or targetTable = "XT_COMMIMP" Or targetTable = "XT_REPROP") Then
                ParcelAuditStream.CreateFlagEvent(db, eventDate.AddSeconds(2), parcelId, loginId, fieldCategoryName + " copied from account " + keyValue1.ToString() + " (" + keyField1 + ")", "MA")
            End If

            Parcel.ElevatePriorityIfNormal(db, parcelId, loginId)
            Parcel.ResetReviewStatus(db, parcelId, loginId)

            If (isCurrentSingleCopy = "currentcopy" And copyType = "futureInsert") Then
                copyType = "currentcopy"
            End If

            If copyType = "futurecopy" Then
                db.Execute("UPDATE Parcel SET HasFutureYear = 1 where Id= " & parcelId.ToString())
                Return tableName + " record #" + sourceAUXRowUID + "copied to future year."
            ElseIf copyType = "futureInsert" Then
                Return tableName + " record #" + sourceAUXRowUID + "copied to future year."
            ElseIf copyType = "currentcopy" Then
                Return tableName + " record #" + sourceAUXRowUID + "copied to Current year."
            ElseIf copyType = "templatefuturecopy" Then
                Return "New record(s) copied To " + tableName + " table from parcel Template Data"
            ElseIf copyType = "visionCopy" Then
                Return "New Aux Record " + newId.ToString() + " inserted to " + tableName
            Else
                Return "New record(s) copied To " + tableName + " table from parcel " + IIf(sourceParcelId = -99, "Template Data", sourceParcelId.ToString())
            End If

        Else
            Throw New Exception() '"Failed To copy " + tableName + " from parcel " + IIf(sourceParcelId = -99, "Template Data", sourceParcelId.ToString()))
        End If


    End Function




    Private Shared Function getFieldsWithoutPrimaryKeys(ByVal db As Database, ByVal targetTable As String, Optional conversionFlag As Boolean = False, Optional copyType As String = "") As String()

        Dim sqlKeys = ""
        If conversionFlag Then
            sqlKeys = "Select Case When f.DataType In (7, 8, 9, 10) Then 'CONVERT(VARCHAR(MAX), LTRIM(STR([' + f.Name + ']))) ' ELSE 'CONVERT(VARCHAR(MAX),['+f.Name+']) ' END + ' As ['+f.name+']' As Name,f.Id FROM DataSourceField f INNER JOIN DataSourceTable t ON f.TableId = t.Id WHERE  t.CC_TargetTable = '" + targetTable + "'  AND f.Id NOT IN(  SELECT f.Id FROM DataSourceField f INNER JOIN  DataSourceKeys k ON f.Id = k.FieldId INNER JOIN  DataSourceRelationships r ON k.RelationshipId = r.Id INNER JOIN DataSourceTable t ON r.TableId = t.Id WHERE (MustIncludeInDataCopy =0 OR MustIncludeInDataCopy is null) AND isPrimaryKey=1 AND t.CC_TargetTable ='" + IIf(copyType = "futurecopy", "", targetTable) + "')  "
        Else
            sqlKeys = "SELECT '['+f.Name+']' as Name,f.id FROM DataSourceField f INNER JOIN DataSourceTable t ON f.TableId = t.Id WHERE  t.CC_TargetTable = '" + targetTable + "' AND f.Id NOT IN(  SELECT f.Id FROM DataSourceField f INNER JOIN  DataSourceKeys k ON f.Id = k.FieldId INNER JOIN  DataSourceRelationships r ON k.RelationshipId = r.Id INNER JOIN DataSourceTable t ON r.TableId = t.Id WHERE (MustIncludeInDataCopy =0 OR MustIncludeInDataCopy is null) AND isPrimaryKey=1 AND t.CC_TargetTable = '" + IIf(copyType = "futurecopy", "", targetTable) + "') "
        End If

        Dim fields = db.GetDataTable(sqlKeys).Rows.Cast(Of DataRow)().Select(Function(x) x.GetString("Name")).ToArray

        Return fields

    End Function

    Private Shared Sub _savePhotoMetaData(db As Database, loginId As String, eventDate As Date, photoId As String, metaFieldName As String, value As String)
        Dim filter = ""
        If photoId.StartsWith("pu") Then
            filter = "LocalId = {0}".SqlFormatString(photoId)
        Else
            If photoId <> "" Then
                filter = "Id = {0}".SqlFormatString(photoId)
            End If
        End If
        Dim dr As DataRow = db.GetTopRow("SELECT * FROM ParcelImages WHERE " + filter)
        If dr Is Nothing Then
            Exit Sub
        End If
        Dim DownSynced = dr.GetString("downsynced")
        Dim ParcelId = dr.GetString("ParcelId")
        Dim RecId = dr.GetString("Id")
        If dr IsNot Nothing Then
            Dim sql As String = ""
            If db.GetIntegerValue("SELECT COUNT(*) FROM ClientSettings WHERE Name = 'PhotoNoDownsyncOnFlagging' AND (Value = '1' OR LOWER(Value) = 'true')") > 0 And (DownSynced = "1" Or DownSynced.ToLower = "true") Then
                sql = "UPDATE ParcelImages SET UploadedBy = " + loginId.ToSqlValue + ", " + metaFieldName + " = " + value.ToSqlValue + " WHERE " + filter
            Else
                sql = "UPDATE ParcelImages SET UploadedBy = " + loginId.ToSqlValue + ", " + metaFieldName + " = " + value.ToSqlValue + ", DownSynced = 0 WHERE " + filter
            End If
            db.Execute(sql)
            Dim ndr As DataRow = db.GetTopRow("SELECT Name, DoNotIncludeInAuditTrail FROM DataSourceField WHERE AssignedName ={0} ".SqlFormatString(metaFieldName.Replace("MetaData", "PhotoMetaField")))
            Dim metaFieldLabel = ndr.GetString("Name")     'db.GetStringValue("SELECT Name, DoNotIncludeInAuditTrail FROM DataSourceField WHERE AssignedName ={0} ".SqlFormatString(metaFieldName.Replace("MetaData", "PhotoMetaField")))
            Dim DoNotAuditTrail As Integer = IIf(ndr.GetString("DoNotIncludeInAuditTrail") = "True", 1, 0)
            Dim nsql As String = "INSERT INTO ParcelAuditTrail (EventDate, EventTime, ParcelId, LoginID, EventType, Description, ApplicationType, DoNotInsertInAuditTrail) VALUES ({0}, {0}, {1}, {2}, {3}, {4}, {5}, {6})".SqlFormat(True, eventDate, ParcelId, loginId, 0, " Data Modified - Photo." + metaFieldLabel + " Changed to " + value + " for image #" + RecId + ".", "MA", DoNotAuditTrail)
            db.Execute(nsql)
            'ParcelAuditStream.CreateFlagEvent(db, eventDate, ParcelId, loginId, " Data Modified - Photo." + metaFieldLabel + " Changed to " + value + " for image #" + RecId + ".", "MA", DoNotAuditTrail)
            Parcel.ResetReviewStatus(db, ParcelId, loginId)
        End If
    End Sub
    Public Shared Sub insertNewRecordDetails(db As Database, ParcelID As Integer, rowuid As Integer, TableName As String, clientROWUID As String, eventDate As DateTime, loginID As String)
        Dim sql As String = "INSERT INTO DB_ROWUIDMap  (ParcelId, TableName, AUROWUID, ClientROWUID, CreatedTime,CreatedBy) VALUES ({0}, {1}, {2}, {3}, {4},{5});"
        db.Execute(sql.SqlFormat(True, ParcelID, TableName, rowuid, clientROWUID, eventDate, loginID))
    End Sub
    
    Private Shared Sub _saveSvReview(db As Database, loginId As String, eventDate As Date,  parcelId As Integer, value As String)
    	Dim svdesc As String = "Sketch reviewed from MA."
    	Try
    		Dim NewValue = Newtonsoft.Json.JsonConvert.DeserializeObject(Of ParcelSVReview)(value)
    		Dim sktData As String = db.GetStringValue("EXEC CC_ProcessAndFillColumnValue {0}, {1}, {2}, {3}, {4}, {5}".SqlFormat(True, "MASVSAVE", NewValue.NewSketchData, "$", "ROWUID", 0, 1))
	    	Dim sql As String = "UPDATE Parcel SET SketchZoom = {1}, SketchRotation = {2}, MapZoom = {3}, SketchReviewedBy = {4}, SketchReviewedDate = {5}, SketchData = {6}, sketchLatLng = {7}, NewSketchData = {8} WHERE Id = {0}"
	        Database.Tenant.Execute(sql.SqlFormat(True, parcelId, NewValue.SketchZoom, NewValue.SketchRotation, NewValue.MapZoom, loginId, eventDate, sktData, NewValue.sketchLatLng, sktData))
            Database.Tenant.Execute("INSERT INTO ParcelAuditTrail (ParcelId, LoginID, EventType, Description,ApplicationType) VALUES ({0}, {1}, {2}, {3},'MA')".SqlFormat(True, parcelId, loginId, 10, svdesc))
        Catch ex As Exception
        	Throw New Exception(ex.Message)
            Return
        End Try
    End Sub

    Public Shared Function _deleteRecordSketchMA(db As Database, parcelId As Integer, loginId As String, clientROWUID As Long, tableName As String, eventDate As Date, latitude As Single, longitude As Single, SyncID As String)
        Dim rtString As String = _deleteRecord(db, parcelId, loginId, clientROWUID, tableName, eventDate, latitude, longitude, SyncID)
        Return rtString
    End Function

    Public Shared Sub _uploadPRCHtmlFile(db As Database, loginId As String, parcelId As Integer, value As String)
        Try
            'Dim htmlContent = value
            Dim pid As String = parcelId
            Dim Path As String = "PrcBackup/" + HttpContext.Current.GetCAMASession.TenantKey + "/" + pid.ToString

            Dim base64Content = value
            Dim base64EncodedBytes As Byte() = Convert.FromBase64String(base64Content)
            Dim htmlContent = Encoding.UTF8.GetString(base64EncodedBytes)
            Console.WriteLine(htmlContent)
            Dim insertSql = "INSERT INTO PrcBackup (LoginId, ParcelId, UploadTime) VALUES({0}, {1}, GETDATE()); SELECT CAST(@@IDENTITY AS INT);".SqlFormat(True, loginId, pid)
            Dim newId = db.GetIntegerValue(insertSql)
            Path = Path + "/" + "beforeMAC" + newId.ToString() + ".html"
            Dim s3 As New S3FileManager()
            s3.UploadHtmlFile(htmlContent, Path)
            db.Execute("UPDATE PrcBackup SET Path='" + Path + "', FromSource='MA' WHERE Id=" + newId.ToString())
            Dim dt As DataTable = db.GetDataTable("SELECT Id,Path FROM PrcBackup WHERE ParcelId=" + pid + " ORDER BY Id DESC")
            If dt.Rows.Count > 20 Then
                Dim dsql As String = "DELETE FROM PrcBackup WHERE Id IN ("
                For i As Integer = 20 To dt.Rows.Count - 1
                    Dim row As DataRow = dt.Rows(i)
                    s3.DeleteFile(row.GetString("Path"))
                    dsql += row.GetString("Id") + ","
                Next
                dsql = dsql.Substring(0, dsql.Length - 1)
                dsql += ")"
                db.Execute(dsql)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return
        End Try
    End Sub
End Class

Public Class ParcelSVReview
	Public Property Description As Integer 	
	public Property pid As String 
    Public Property SketchZoom As Double
    Public Property MapZoom As String
    Public Property SketchRotation As Integer
	Public Property SketchData As String
	Public Property sketchLatLng As String
	Public Property NewSketchData As String	
	Public Property polygonArray As String 		
End Class  
