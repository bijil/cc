﻿Public Class QCProcessor

    Public Shared Sub ApproveParcel()

    End Sub

    Public Shared Sub RejectParcel()

    End Sub

    Public Shared Sub BulkApproveParcels()

    End Sub

    Public Shared Sub BulkRejectAll()

    End Sub

    Public Shared Function DeleteRecord(db As Database, parcelId As Integer, loginId As String, rowuid As Long, tableName As String, latitude As Single, longitude As Single, eventDate As DateTime, Optional ByVal isDTR As String = "")

        Dim DFID = db.GetIntegerValue("SELECT MIN(Id) FROM DataSourceField WHERE SourceTable ={0}".SqlFormatString(tableName))
        Dim dr As DataRow = db.GetTopRow("SELECT Id,DisableDeletePCI FROM DataSourceTable WHERE Name = " + tableName.ToSqlValue)
        Dim tableId As Integer = dr.GetInteger("Id")
        Dim DisableDeletePCI As Boolean = dr.GetBoolean("DisableDeletePCI")
        'If rowuid < 0 Then
        '    rowuid = db.GetIntegerValue("SELECT ROWUID FROM XT_" + tableName + " WHERE ClientROWUID ={0} ".SqlFormatString(rowuid.ToString()))
        'End If
        'vm Dim deleteActions = db.GetDataTable("SELECT dta.*, f.Id As FieldId FROM DataSourceTableAction dta INNER JOIN DataSourceField f ON dta.TableId = f.TableId AND dta.FieldName = f.Name WHERE dta.TableId = {0} AND Action = 'D'".SqlFormat(False, tableId)).AsEnumerable.Select(Function(x) New With {.FieldName = x("FieldName"), .ValueTag = x("ValueTag"), .FieldId = x("FieldId")}).ToList
        If rowuid > 0 Then
            __deleteChildRecords(db, parcelId, rowuid, tableName, loginId, latitude, longitude, eventDate)
            db.Execute("UPDATE XT_" + tableName + "   SET CC_Deleted = 1 WHERE ROWUID = {0}".SqlFormatString(rowuid.ToString()))
            Dim yearValue = db.GetStringValue("SELECT CC_YearStatus FROM  XT_" + tableName + "  WHERE ROWUID = " + rowuid.ToString)
            DeleteTableAction(db, parcelId, tableId, rowuid, loginId, eventDate, longitude, latitude)
            Dim ChangeCount As Integer = db.GetIntegerValue("SELECT COUNT(*) FROM ParcelChanges pc INNER JOIN DataSourceField df ON df.id=pc.FieldId WHERE AuxROWUID={0} AND pc.parcelId={1} AND df.TableId={2} AND df.IsCustomField=0 AND action='new'".SqlFormat(False, rowuid, parcelId, tableId))
            If ChangeCount = 0 Then
                Dim delSql As String = "SELECT * FROM ParcelChanges WHERE ParcelId = {0} AND FieldId = {1} AND AuxROWUID = {2} AND Action={3}"
                Dim _dr As DataRow = db.GetTopRow(delSql.SqlFormat(True, parcelId, DFID, rowuid, "delete"))
                If _dr Is Nothing Then
                    Dim sql As String = "INSERT INTO ParcelChanges (ParcelId, FieldId, Action, ReviewedBy, OriginalValue, NewValue, ChangedTime, Latitude, Longitude, AuxROWUID,CC_YearStatus) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, GETUTCDATE(), {6}, {7}, {8}, {9}); SELECT CAST(@@IDENTITY AS INT) As NewId;"
                    Dim changeId As Integer = db.GetIntegerValue(sql.SqlFormat(True, parcelId, DFID, "delete", loginId, Nothing, Nothing, latitude, longitude, rowuid, yearValue))
                    db.Execute("EXEC processFieldChangesTracking {0},{1},{2},{3}".SqlFormat(True, parcelId, DFID, changeId, Nothing))
                End If
            End If

            'ParcelAuditStream.CreateFlagEvent(db, Date.UtcNow, parcelId, loginId, "1 record deleted by QC from " + tableName + " with Aux Record #" + rowuid.ToString())
            ParcelAuditStream.CreateFlagEvent(db, Date.UtcNow, parcelId, loginId, "Aux Record #" + rowuid.ToString() + " deleted by " + isDTR + " from " + tableName, isDTR)
            If ParcelEventLog.Enabled Then
                ParcelEventLog.CreateTableEvent(db, ParcelEventLog.EventType.RowDeleted, ParcelEventLog.ApplicationType.Console, parcelId, tableId, rowuid)
            End If

            Return isDTR + ": 1 aux record #" + rowuid.ToString() + " deleted from " + tableName
        Else
            Return ""
        End If

    End Function

    Private Shared Sub __deleteChildRecords(db As Database, parcelId As Integer, parentRowUID As Long, tableName As String, loginId As String, latitude As Single, longitude As Single, eventDate As DateTime)
        Dim childTablesSql As String = "SELECT DISTINCT tt.Name FROM DataSourceRelationships r LEFT OUTER JOIN DataSourceTable pt ON r.ParentTableId = pt.Id LEFT OUTER JOIN DataSourceTable tt ON r.TableId = tt.Id WHERE r.Relationship = 2 AND pt.Name = {0}"
        For Each c As DataRow In db.GetDataTable(childTablesSql.SqlFormatString(tableName)).Rows
            Dim childTableName As String = c.GetString("Name")
            Dim tableId As Integer = db.GetIntegerValueOrInvalid("SELECT Id FROM DataSourceTable WHERE Name = " + childTableName.ToSqlValue)
            Dim DFID = db.GetIntegerValue("SELECT MIN(Id) FROM DataSourceField WHERE SourceTable ={0}".SqlFormatString(childTableName))
            ' Dim deleteActions = db.GetDataTable("SELECT dta.*, f.Id As FieldId FROM DataSourceTableAction dta INNER JOIN DataSourceField f ON dta.TableId = f.TableId AND dta.FieldName = f.Name WHERE dta.TableId = {0} AND Action = 'D'".SqlFormat(False, tableId)).AsEnumerable.Select(Function(x) New With {.FieldName = x("FieldName"), .ValueTag = x("ValueTag"), .FieldId = x("FieldId")}).ToList

            For Each cr As DataRow In db.GetDataTable("EXEC cc_GetChildROWUIDs {0}, {1}, {2}".SqlFormatString(tableName, childTableName, parentRowUID)).Rows
                Dim childRowUID As Integer = cr.GetInteger("ROWUID")
                __deleteChildRecords(db, parcelId, childRowUID, childTableName, loginId, latitude, longitude, eventDate)
                db.Execute("UPDATE XT_" + childTableName + "   SET CC_Deleted = 1 WHERE ROWUID = {0}".SqlFormatString(childRowUID.ToString()))
                Dim yearValue = db.GetStringValue("SELECT CC_YearStatus FROM  XT_" + tableName + "  WHERE ROWUID = " + childRowUID.ToString)
                DeleteTableAction(db, parcelId, tableId, childRowUID, loginId, eventDate, longitude, latitude)
                Dim ChangeCount As Integer = db.GetIntegerValue("SELECT COUNT(*) FROM ParcelChanges pc INNER JOIN DataSourceField df ON df.id=pc.FieldId WHERE AuxROWUID={0} AND pc.parcelId={1} AND df.TableId={2} AND df.IsCustomField=0 AND action='new'".SqlFormat(False, childRowUID, parcelId, tableId))
                If ChangeCount = 0 Then
                    Dim sql1 As String = "SELECT * FROM ParcelChanges WHERE ParcelId = {0} AND FieldId = {1} AND AuxROWUID = {2} AND Action={3}"
                    Dim dr As DataRow = db.GetTopRow(sql1.SqlFormat(True, parcelId, DFID, childRowUID, "delete"))
                    If dr IsNot Nothing Then
                        Dim chngId As Integer = dr.GetInteger("Id")
                        db.Execute("UPDATE ParcelChanges SET IsParentDeleted  = 1 WHERE Id = {0}".SqlFormat(True, chngId))
                    Else
                        Dim sql As String = "INSERT INTO ParcelChanges (ParcelId, FieldId, Action, ReviewedBy, OriginalValue, NewValue, ChangedTime, Latitude, Longitude, AuxROWUID,CC_YearStatus,IsParentDeleted) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, GETUTCDATE(), {6}, {7}, {8}, {9},{10}); SELECT CAST(@@IDENTITY AS INT) As NewId;"
                        Dim changeId As Integer = db.GetIntegerValue(sql.SqlFormat(True, parcelId, DFID, "delete", loginId, Nothing, Nothing, latitude, longitude, childRowUID, yearValue, 1))
                        db.Execute("EXEC processFieldChangesTracking {0},{1},{2},{3}".SqlFormat(True, parcelId, DFID, changeId, Nothing))
                    End If

                End If
                If ParcelEventLog.Enabled Then
                    ParcelEventLog.CreateTableEvent(db, ParcelEventLog.EventType.RowDeleted, ParcelEventLog.ApplicationType.Console, parcelId, tableId, childRowUID)
                End If
            Next
        Next
    End Sub
    Private Shared Sub DeleteTableAction(db As Database, parcelid As Integer, tableID As Integer, rowUID As Long, loginId As String, eventDate As DateTime, Optional longitude As Long = 0, Optional latitude As Long = 0, Optional SyncID As String = "")

        Dim ChangeCount As Integer = db.GetIntegerValue("SELECT COUNT(*) FROM ParcelChanges pc INNER JOIN DataSourceField df ON df.id=pc.FieldId WHERE AuxROWUID={0} AND pc.parcelId={1} AND df.TableId={2} AND df.IsCustomField=0 AND action='new'".SqlFormat(False, rowUID, parcelid, tableID))
        If ChangeCount = 0 Then
            Dim deleteActions = db.GetDataTable("SELECT dta.*, f.Id As FieldId,isnull(f.UIProperties,'') as UIProperties FROM DataSourceTableAction dta INNER JOIN DataSourceField f ON dta.TableId = f.TableId AND dta.FieldName = f.Name WHERE dta.TableId = {0} AND Action = 'D'".SqlFormat(False, tableID)).AsEnumerable.Select(Function(x) New With {.FieldName = x("FieldName"), .ValueTag = x("ValueTag"), .FieldId = x("FieldId"), .UIProperties = x("UIProperties")}).ToList
            For Each da In deleteActions
            	Dim fieldValue As Object
            	Dim Propet As string = da.UIProperties
            	If Not IsDBNull(da.ValueTag)
	                Dim tag As String = da.ValueTag
	                Select Case tag
	                    Case "LoginId"
	                        fieldValue = loginId
	                    Case "EventDate"
	                        fieldValue = eventDate
	                    Case "CurrentYear"
	                        fieldValue = eventDate.Year
	                    Case "CurrentMonth"
	                        fieldValue = eventDate.Month
	                    Case "CurrentDay"
	                        fieldValue = eventDate.Day
	                    Case Else
	                        fieldValue = tag
	                End Select
	                Dim sql As String = "INSERT INTO ParcelChanges (ParcelId, FieldId, Action, ReviewedBy, OriginalValue, NewValue, ChangedTime, Latitude, Longitude, AuxROWUID) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, GETUTCDATE(), {6}, {7}, {8}); SELECT CAST(@@IDENTITY AS INT) As NewId;"
	                Dim changeid As Integer
	                If Propet.Contains("YYYYMMDD") Then
	               		changeid = db.GetIntegerValue(sql.SqlFormatTableAction(True, parcelid, da.FieldId, "edit", loginId, Nothing, fieldValue, latitude, longitude, rowUID))
	                Else
	                	changeid = db.GetIntegerValue(sql.SqlFormat(True, parcelid, da.FieldId, "edit", loginId, Nothing, fieldValue, latitude, longitude, rowUID))
	                End If
	                db.Execute("EXEC processFieldChangesTracking {0},{1},{2},{3}".SqlFormat(True, parcelid, da.FieldId, changeID, fieldValue))
            	End If        
        	Next
        End If
    End Sub
    Public Shared Sub EditTableAction(db As Database, parcelId As Integer, reviewed As String, uName As String, eventDate As DateTime, Optional longitude As Long = 0, Optional latitude As Long = 0, Optional SyncID As String = "")
        If reviewed = "1" Then
            Dim pId As String = parcelId
            Dim EditActionTab = Database.Tenant.GetDataTable("SELECT Distinct pc.AuxROWUID,dta.ValueTag,df.Id ,dta.FieldName,isnull(df.UIProperties,'') as UIProperties FROM ParcelChanges pc LEFT OUTER JOIN DataSourceField ds on ds.Id=pc.FieldId INNER JOIN DataSourceTableAction dta ON dta.TableId = ds.TableId INNER JOIN  DataSourceField df on df.Name = dta.FieldName and df.TableId =dta.TableId WHERE pc.ParcelId={0} and df.IsCustomField=0 and dta.Action='E' ".SqlFormat(False, pId)).AsEnumerable.Select(Function(x) New With {.FieldName = x("FieldName"), .ValueTag = x("ValueTag"), .FieldId = x("Id"), .auxId = x("AuxROWUID"), .UIProperties = x("UIProperties")}).ToList()
            For Each da In EditActionTab
                Dim fieldValue As Object
                Dim tag As String = da.ValueTag
                Dim Propet As String = da.UIProperties
                Select Case tag
                    Case "LoginId"
                        fieldValue = uName
                    Case "EventDate"
                        fieldValue = eventDate
                    Case "CurrentYear"
                        fieldValue = eventDate.Year
                    Case "CurrentMonth"
                        fieldValue = eventDate.Month
                    Case "CurrentDay"
                        fieldValue = eventDate.Day
                    Case Else
                        Dim value As String = formatCustomValue(tag, eventDate)
                        fieldValue = value
                End Select

                Dim changeID As Integer
                Dim sql As String = "INSERT INTO ParcelChanges (ParcelId, FieldId, Action, ReviewedBy, OriginalValue, NewValue, ChangedTime, Latitude, Longitude, SyncId,AuxROWUID) VALUES ({0}, {1}, {2}, {3}, {4}, {5},{6}, {7}, {8},{9},{10}); SELECT CAST(@@IDENTITY AS INT) As NewId;"
                If Propet.Contains("YYYYMMDD") Then
                    changeID = db.GetIntegerValue(sql.SqlFormatTableAction(True, pId, da.FieldId, "edit", uName, Nothing, fieldValue, eventDate, latitude, longitude, SyncID, da.auxId))
                Else
                    changeID = db.GetIntegerValue(sql.SqlFormat(True, pId, da.FieldId, "edit", uName, Nothing, fieldValue, eventDate, latitude, longitude, SyncID, da.auxId))
                End If
                db.Execute("EXEC processFieldChangesTracking {0},{1},{2},{3}".SqlFormat(True, parcelId, da.FieldId, changeID, fieldValue))
            Next
        End If
    End Sub

    Public Shared Sub EditDescendentTableAction(db As Database, parcelId As Integer, reviewed As String, uName As String, eventDate As DateTime, Optional longitude As Long = 0, Optional latitude As Long = 0, Optional SyncID As String = "")
        Dim edAction As DataTable = Database.Tenant.GetDataTable("SELECT dta.*, df.Id As FieldId, df.SourceTable, isnull(df.UIProperties, '') as UIProperties FROM DataSourceTableAction dta JOIN DataSourceField df on df.Name = dta.FieldName and df.TableId = dta.TableId WHERE df.IsCustomField = 0 AND dta.Action = 'R'")
        If reviewed = "1" And edAction.Rows.Count > 0 Then
            For Each row As DataRow In edAction.Rows
                Dim tblName As String = Database.Tenant.GetStringValue("SELECT CC_TargetTable FROM DataSourceTable WHERE Id = " + row("TableId").ToString())
                If tblName <> "" And (tblName = "ParcelData" Or tblName.Contains("XT_")) Then
                    Dim pciExist As Boolean = False
                    Dim fieldValue As Object
                    Dim tag As String = row("ValueTag")
                    Dim Propet As String = row("UIProperties")
                    Select Case tag
                        Case "LoginId"
                            fieldValue = uName
                        Case "EventDate"
                            fieldValue = eventDate
                        Case "CurrentYear"
                            fieldValue = eventDate.Year
                        Case "CurrentMonth"
                            fieldValue = eventDate.Month
                        Case "CurrentDay"
                            fieldValue = eventDate.Day
                        Case Else
                            Dim value As String = formatCustomValue(tag, eventDate)
                            fieldValue = value
                    End Select

                    Dim changeID As Integer
                    Dim sql As String = "INSERT INTO ParcelChanges (ParcelId, FieldId, Action, ReviewedBy, OriginalValue, NewValue, ChangedTime, Latitude, Longitude, SyncId,AuxROWUID) VALUES ({0}, {1}, {2}, {3}, {4}, {5},{6}, {7}, {8},{9},{10}); SELECT CAST(@@IDENTITY AS INT) As NewId;"

                    If tblName = "ParcelData" Then
                        Dim pciTble As DataTable = Database.Tenant.GetDataTable("SELECT pc.* FROM ParcelChanges pc JOIN DataSourceField ds on ds.Id = pc.FieldId WHERE pc.ParcelId = {0} and ds.IsCustomField = 0 AND pc.AuxROWUID IS NULL AND ds.TableId = {1}".SqlFormat(False, parcelId, row("TableId")))
                        If pciTble.Rows.Count > 0 Then
                            If Propet.Contains("YYYYMMDD") Then
                                changeID = db.GetIntegerValue(sql.SqlFormatTableAction(True, parcelId, row("FieldId"), "edit", uName, Nothing, fieldValue, eventDate, latitude, longitude, SyncID, Nothing))
                            Else
                                changeID = db.GetIntegerValue(sql.SqlFormat(True, parcelId, row("FieldId"), "edit", uName, Nothing, fieldValue, eventDate, latitude, longitude, SyncID, Nothing))
                            End If

                            If changeID > 0 Then
                                db.Execute("EXEC processFieldChangesTracking {0}, {1}, {2}, {3}".SqlFormat(True, parcelId, row("FieldId"), changeID, fieldValue))
                            End If
                        End If
                    Else
                        Dim recTble As DataTable = Database.Tenant.GetDataTable("SELECT ROWUID FROM " + tblName + " WHERE CC_ParcelId = " + parcelId.ToString())
                        If recTble.Rows.Count > 0 Then
                            For Each rrow As DataRow In recTble.Rows
                                Dim pciTble As DataTable = Database.Tenant.GetDataTable("SELECT pc.* FROM ParcelChanges pc JOIN DataSourceField ds on ds.Id = pc.FieldId WHERE pc.ParcelId = {0} and ds.IsCustomField = 0 AND pc.AuxROWUID = {1} AND ds.TableId = {2}".SqlFormat(False, parcelId, rrow("ROWUID"), row("TableId")))
                                If pciTble.Rows.Count > 0 Then
                                    pciExist = True
                                Else
                                    Dim st As Boolean = CheckPCIExistsInChildTables(db, parcelId, rrow("ROWUID"), row("SourceTable"))
                                    If st Then
                                        pciExist = True
                                    End If
                                End If
                                If pciExist Then
                                    If Propet.Contains("YYYYMMDD") Then
                                        changeID = db.GetIntegerValue(sql.SqlFormatTableAction(True, parcelId, row("FieldId"), "edit", uName, Nothing, fieldValue, eventDate, latitude, longitude, SyncID, rrow("ROWUID")))
                                    Else
                                        changeID = db.GetIntegerValue(sql.SqlFormat(True, parcelId, row("FieldId"), "edit", uName, Nothing, fieldValue, eventDate, latitude, longitude, SyncID, rrow("ROWUID")))
                                    End If

                                    If changeID > 0 Then
                                        db.Execute("EXEC processFieldChangesTracking {0}, {1}, {2}, {3}".SqlFormat(True, parcelId, row("FieldId"), changeID, fieldValue))
                                    End If
                                End If
                            Next
                        End If
                    End If
                End If
            Next
        End If
    End Sub

    Public Shared Function CheckPCIExistsInChildTables(db As Database, parcelId As Integer, rowuid As Integer, SourceTable As String) As Boolean
        Dim childTablesSql As String = "SELECT DISTINCT tt.Name FROM DataSourceRelationships r LEFT OUTER JOIN DataSourceTable pt ON r.ParentTableId = pt.Id LEFT OUTER JOIN DataSourceTable tt ON r.TableId = tt.Id WHERE r.Relationship = 2 AND pt.Name = {0}"
        For Each c As DataRow In db.GetDataTable(childTablesSql.SqlFormatString(SourceTable)).Rows
            Dim childTableName As String = c.GetString("Name")
            For Each cr As DataRow In db.GetDataTable("EXEC cc_GetChildROWUIDs {0}, {1}, {2}".SqlFormatString(SourceTable, childTableName, rowuid)).Rows
                Dim childRowUID As Integer = cr.GetInteger("ROWUID")
                Dim pciTble As DataTable = Database.Tenant.GetDataTable("SELECT pc.* FROM ParcelChanges pc JOIN DataSourceField ds on ds.Id = pc.FieldId WHERE pc.ParcelId = {0} and ds.IsCustomField = 0 AND pc.AuxROWUID = {1} AND ds.SourceTable = {2}".SqlFormat(False, parcelId, childRowUID, childTableName))
                If pciTble.Rows.Count > 0 Then
                    Return True
                End If
                Dim st As Boolean = CheckPCIExistsInChildTables(db, parcelId, childRowUID, childTableName)
                If st Then
                    Return True
                End If
            Next
        Next
        Return False
    End Function

    Public Shared Function formatCustomValue(tag As String, eventDate As DateTime) As String
        If (tag = "{MM/DD/YYYY}") Then
            tag = eventDate.ToString("MM/dd/yyyy")
        ElseIf (tag = "{MM-DD-YYYY}") Then
            tag = eventDate.ToString("MM-dd-yyyy")
        End If
        Return tag
    End Function

    Public Shared Function AggregateFieldSettings() As DataTable
        Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT * FROM AggregateFieldSettings")
        AddBlankRowIfNoRows(dt)
        Return dt
    End Function
    Public Shared Sub AddBlankRowIfNoRows(ByRef dt As DataTable, Optional defaultFieldName As String = "", Optional defaultValue As String = "")
        If dt.Rows.Count = 0 Then
            Dim blank As DataRow = dt.NewRow
            For Each dc As DataColumn In dt.Columns
                Select Case dc.DataType
                    Case GetType(Integer), GetType(Single), GetType(Double), GetType(Long), GetType(Decimal)
                        blank(dc.ColumnName) = -9999
                    Case GetType(Boolean)
                        blank(dc.ColumnName) = False
                    Case GetType(Date), GetType(DateTime)
                        blank(dc.ColumnName) = #1/1/1970#
                    Case Else
                        blank(dc.ColumnName) = ""
                End Select
                If dc.ColumnName = defaultFieldName Then
                    blank(dc.ColumnName) = defaultValue
                End If
            Next
            dt.Rows.Add(blank)
        End If
    End Sub
End Class
