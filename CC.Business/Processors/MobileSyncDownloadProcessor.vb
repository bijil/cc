﻿Imports System.IO
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Drawing.Imaging

Public Class MobileSyncDownloadProcessor

    Public Shared ReadOnly Property PageSize As Integer
        Get
            Return Database.Tenant.GetIntegerValue("SELECT dbo.PageSize() As PageSize")
        End Get
    End Property
    Private Shared Function getParentSyncCacheInlineQuery(loginId As String, pageNo As Integer) As String
        Dim psize As Integer = PageSize
        Return "(SELECT ParcelId FROM SyncUserCache WHERE IsParent =1 AND LoginId = {0} AND Ordinal BETWEEN ({1} - 1) * {2} + 1 AND {1} * {2})".SqlFormat(False, loginId, pageNo, psize)
    End Function
    Private Shared Function getSyncCacheInlineQuery(loginId As String, pageNo As Integer, Optional ignoreParentChanges As Boolean = True) As String
        Dim isBPPAppraiser As Boolean = Database.Tenant.GetIntegerValue("SELECT IsBPPAppraiser  from UserSettings where loginId ={0} ".SqlFormatString(loginId))
        Dim psize As Integer = PageSize
        Return "(SELECT ParcelId FROM SyncUserCache WHERE " + IIf(isBPPAppraiser And ignoreParentChanges = True, "IsParent = 0 AND  ", "") + " LoginId = {0} AND Ordinal BETWEEN ({1} - 1) * {2} + 1 AND {1} * {2})".SqlFormat(False, loginId, pageNo, psize)
    End Function

    Public Shared Sub AddBlankRowIfNoRows(ByRef dt As DataTable, Optional defaultFieldName As String = "", Optional defaultValue As String = "")
        If dt.Rows.Count = 0 Then
            Dim blank As DataRow = dt.NewRow
            For Each dc As DataColumn In dt.Columns
                Select Case dc.DataType
                    Case GetType(Integer), GetType(Single), GetType(Double), GetType(Long), GetType(Decimal)
                        blank(dc.ColumnName) = -9999
                    Case GetType(Boolean)
                        blank(dc.ColumnName) = False
                    Case GetType(Date), GetType(DateTime)
                        blank(dc.ColumnName) = #1/1/1970#
                    Case Else
                        blank(dc.ColumnName) = ""
                End Select
                If dc.ColumnName = defaultFieldName Then
                    blank(dc.ColumnName) = defaultValue
                End If
            Next
            dt.Rows.Add(blank)
        End If
    End Sub

    Public Shared Function SyncParcelImages(loginId As String, nbhd As String, page As Integer, Optional ByVal imgLimit As String = "400", Optional fetchParentParcelImages As Boolean = False, Optional ByRef dt1 As DataTable = Nothing) As DataTable
        Dim imageLimit As Integer = Convert.ToInt32(imgLimit)
        Dim sql As String = "pg.Id As Id, ParcelId, '' As Image, pg.Path, CASE pg.IsSketch WHEN 1 THEN 2 ELSE 1 END As Type, 0 As LocalId, 1 As Accepted, 1 As Synced,DownSynced,CreatedTime as UploadTime,IsPrimary, MetaData1, MetaData2, MetaData3, MetaData4, MetaData5, MetaData6, MetaData7, MetaData8, MetaData9, MetaData10,CC_YearStatus,CC_LinkedImageID FROM ParcelImages pg INNER JOIN Parcel p ON pg.ParcelId = p.Id INNER JOIN Neighborhood n ON p.NeighborhoodId = n.Id WHERE " + IIf(fetchParentParcelImages = True, "1 = 1", "Number IN ('" + nbhd.ToString() + "')") + " AND p.Id IN " + IIf(fetchParentParcelImages = True, getParentSyncCacheInlineQuery(loginId, page), getSyncCacheInlineQuery(loginId, page, True)) + " AND (pg.CC_Deleted = 0 OR pg.CC_Deleted IS NULL) ORDER BY ParcelId, IsPrimary DESC, CreatedTime DESC"
        If imageLimit = 3 Or imageLimit = 4 Then
            sql = "SELECT TOP 0 " + sql
        Else
            sql = "SELECT " + sql
        End If
        Dim dt As DataTable = Database.Tenant.GetDataTable(sql)

        Dim ParcelId As Integer, parcelImageCount As Integer, nonPrimCount As Integer = 1, path As String = ""
        For Each dr As DataRow In dt.Rows
            If ParcelId = 0 Then
                ParcelId = dr("ParcelId")
            End If
            If ParcelId = dr("ParcelId") Then
                parcelImageCount += 1
            Else
                parcelImageCount = 1
                nonPrimCount = 1
                ParcelId = dr("ParcelId")
            End If
            path = dr.GetString("Path")
            If path = "" Then
                dr("Image") = ""
            Else
                If imageLimit = 2 And dr("IsPrimary") <> "1" And parcelImageCount > 1 Then
                    dr("Image") = ""
                    Continue For
                End If
                If imageLimit = 1 And dr("IsPrimary") <> "1" Then
                    nonPrimCount += 1
                    If nonPrimCount > 3 Then
                        dr("Image") = ""
                        Continue For
                    End If
                End If
                dr("Image") = ConvertPhotoToBase64(path, dr("Type"))
            End If
            If (fetchParentParcelImages) Then
                dt1.Rows.Add(dr.ItemArray)
            End If
        Next

        AddBlankRowIfNoRows(dt)
        Return dt
    End Function
    Public Shared Function rplinkedbpps(nbhd As String) As DataTable
        Dim sql As String = "EXEC [ccma_SyncRpLinkedBpps] " + "'" + nbhd.ToString() + "'"
        Dim dt As DataTable = Database.Tenant.GetDataTable(sql)
        AddBlankRowIfNoRows(dt)
        Return dt
    End Function
    Public Shared Function parcelGISPoints(loginId As String, nbhd As String, page As Integer) As DataTable
        Dim sql As String = "EXEC [ccma_SyncGISPoints]  @LoginId= {0}, @Nbhd = {1}, @Page = {2}".SqlFormatString(loginId, nbhd, page)
        Dim dt As DataTable = Database.Tenant.GetDataTable(sql)
        AddBlankRowIfNoRows(dt)
        Return dt
    End Function
    Public Shared Function ConvertPhotoToBase64(path As String, Optional type As Integer = 1) As String
        Dim s3 As New S3FileManager()
        Try
            Dim ps As IO.MemoryStream
            Try
                ps = s3.GetFile(path)
            Catch ex As Exception
                Dim ext As String = IO.Path.GetExtension(path).Trim.TrimStart(".")
                If Char.IsLower(ext.Chars(0)) Then
                    ext = ext.ToUpper
                Else
                    ext = ext.ToLower
                End If
                path = IO.Path.ChangeExtension(path, ext)
                ps = s3.GetFile(path)
            End Try

            Dim bmp As New Bitmap(ps)

            Dim scaledStream As New IO.MemoryStream

            If (bmp.Width < 400) AndAlso (bmp.Height < 300) Then
                bmp.Save(scaledStream, Imaging.ImageFormat.Jpeg)
            Else
                Dim scaled As Bitmap = getScaledPicture(bmp, IIf(type = 2, 1200, 400), IIf(type = 2, 1200, 900))
                'Dim scaled As Bitmap = getScaledPicture(bmp, If(type = 2, 1200, 400), If(type = 2, 1200, 900))
                scaled.Save(scaledStream, Imaging.ImageFormat.Jpeg)
            End If
            '  Dim scaled As Bitmap = getScaledPicture(bmp, 500, 400)
            ' Dim scaledStream As New IO.MemoryStream

            Dim buffer() As Byte = scaledStream.ToArray
            scaledStream.Close()
            ps.Close()
            Dim base64Raw As String = System.Convert.ToBase64String(buffer)
            Dim base64 As String = "data:image/jpeg;base64," + base64Raw.Replace(vbLf, "").Replace(vbCr, "").Trim
            Return base64
        Catch ex As Exception
            Return ""
        End Try
    End Function
    Public Shared Function SyncParcelMapPoints(loginId As String, nbhd As String, page As Integer) As DataTable
        Dim ps As Integer = PageSize
        ' Dim sql As String = "SELECT m.*, str(m.Latitude, 16, 13) + ',' + str(m.Longitude, 16, 13) As Point FROM Parcel p INNER JOIN Neighborhood n ON p.NeighborhoodId = n.Id LEFT OUTER JOIN ParcelMapPoints m ON (p.Id = m.ParcelId) WHERE n.Number IN ('" + nbhd + "') AND p.Id IN " + getSyncCacheInlineQuery(loginId, page)
        Dim sql As String = "EXEC [ccma_getSyncCacheParcels] @LoginId= {0}, @nbhd = {1}, @page = {2}, @pageSize = {3}".SqlFormatString(loginId, nbhd, page, ps)
        Dim dt As DataTable = Database.Tenant.GetDataTable(sql)
        AddBlankRowIfNoRows(dt)
        Return dt
    End Function
    Public Shared Function SyncParcelChanges(loginId As String, nbhd As String, page As Integer) As DataTable
        Dim comparableRptCount As Integer = Database.Tenant.GetIntegerValue("SELECT count(*) FROM ScreenMenu WHERE [KEY]  = 'comparables'")
        Dim sql As String = ""
        Dim psize As Integer = PageSize
        If comparableRptCount > 0 Then
            sql = "EXEC [ccma_getSyncComparableParcels] @LoginId= {0}, @page = {1}, @pageSize = {2}, @tableName = {3}, @sortFormula = {4}, @nbhdID = {5}, @isBPPAppraiser = {6}, @getPCIChanges = {7} ".SqlFormatString(loginId, page, psize, "parcelchanges", "", nbhd.ToString(), 0, 1)
        Else
            sql = "SELECT pc.ParcelId, pc.AuxROWUID AS AuxRowId, pc.ParentAuxROWUID ParentAuxRowId, pc.Action, f.Name Field, pc.FieldId, pc.OriginalValue OldValue, pc.NewValue, 0 ChangedTime, 0 Latitude,0 Longitude,1 Synced FROM ParcelChanges pc LEFT OUTER JOIN DataSourceField f ON pc.FieldId = f.Id WHERE pc.ParcelId IN " + getSyncCacheInlineQuery(loginId, page, False) + " and pc.Id IN (SELECT MAX(Id) FROM ParcelChanges WHERE ParcelId IN " + getSyncCacheInlineQuery(loginId, page, False) + " group by ParcelId,FieldId,AuxROWUID) and (f.ExclusiveForSS IS NULL OR f.ExclusiveForSS=0) and (pc.IsDefault IS NULL OR pc.IsDefault = 0) order by  pc.ParcelId,pc.FieldId,pc.ChangedTime,pc.Id "
        End If
        Dim dt As DataTable = Database.Tenant.GetDataTable(sql)
        AddBlankRowIfNoRows(dt)
        Return dt
    End Function
    Public Shared Function syncStreetMapPoints(nbhd As String) As DataTable
        Dim sql As String = "EXEC [ccma_GetStreetPoints] " + nbhd.ToString()
        Dim dt As DataTable = Database.Tenant.GetDataTable(sql)
        AddBlankRowIfNoRows(dt)
        Return dt
    End Function

    Public Shared Function heatmapFields() As DataTable
        Dim sql As String = "select * from UI_HeatMap "
        Dim dt As DataTable = Database.Tenant.GetDataTable(sql)
        AddBlankRowIfNoRows(dt)
        Return dt
    End Function
    Public Shared Function heatmapLookup() As DataTable
        Dim sql As String = "select * from UI_HeatMapLookup"
        Dim dt As DataTable = Database.Tenant.GetDataTable(sql)
        AddBlankRowIfNoRows(dt)
        Return dt
    End Function
    Public Shared Function AggregateFieldSettings() As DataTable
        Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT * FROM AggregateFieldSettings")
        AddBlankRowIfNoRows(dt)
        Return dt
    End Function
    Public Shared Function SyncAuxiliary(loginId As String, nbhd As String, tableName As String, page As Integer) As DataTable
        Dim sortFormula As String = FieldCategory.GetSortExpression(tableName, "xt.")
        If (sortFormula = "") Then
            sortFormula = " ORDER BY ROWUID ASC "
        End If
        'Dim fieldsForDownload = GetFieldNamesForDownload(Database.Tenant, "XT_" + tableName, "xt.")
        'Dim sql As String = "SELECT '" + tableName + "' As DataSourceName, xt.CC_ParcelId, xt.CC_Deleted, xt.ROWUID, xt.ParentROWUID, xt.ClientROWUID, xt.ClientParentROWUID, " + fieldsForDownload + " FROM XT_" + tableName + " xt INNER JOIN Parcel p ON xt.CC_ParcelId = p.Id INNER JOIN Neighborhood n ON p.NeighborhoodId = n.Id  WHERE (CC_Deleted = 0 OR CC_Deleted IS NULL) AND n.Number = " + nbhd.ToSqlValue + " AND p.Id IN " + getSyncCacheInlineQuery(loginId, page) + " " + sortFormula
        Dim isBPPAppraiser As Boolean = Database.Tenant.GetIntegerValue("SELECT IsBPPAppraiser  from UserSettings where loginId ={0} ".SqlFormatString(loginId)) ''14151 download auxilary data of the rp parcel if not in group but BPP aprraiser and it's parent RP exists. PID existing in getSyncCacheInlineQuery then only download auxilary datas.

        Dim comparableRptCount As Integer = Database.Tenant.GetIntegerValue("SELECT count(*) FROM ScreenMenu WHERE [KEY]  = 'comparables'")
        Dim sql As String
        Dim psize As Integer = PageSize
        If comparableRptCount > 0 Then
            'sql = "EXEC [ccma_getSyncComparableParcels] @LoginId= {0}, @page = {1}, @pageSize = {2}, @tableName = {3}, @sortFormula = {4}, @isBPPAppraiser = {5} ".SqlFormatString(loginId, page, psize, tableName, sortFormula, IIf(isBPPAppraiser, 1, 0))
            sql = "EXEC [ccma_getSyncComparableParcels] @LoginId= {0}, @page = {1}, @pageSize = {2}, @tableName = {3}, @sortFormula = {4}, @nbhdID = {5}, @isBPPAppraiser = {6} ".SqlFormatString(loginId, page, psize, tableName, sortFormula, nbhd.ToString(), IIf(isBPPAppraiser, 1, 0))
        Else
            sql = "SELECT '" + tableName + "' As DataSourceName, xt.* FROM XT_" + tableName + " xt INNER JOIN Parcel p ON xt.CC_ParcelId = p.Id LEFT OUTER JOIN Neighborhood n ON p.NeighborhoodId = n.Id  WHERE (CC_Deleted = 0 OR CC_Deleted IS NULL) AND (n.Number IN ('" + nbhd.ToString() + "') OR p.Id = -99 " + IIf(isBPPAppraiser, "OR p.IsRPProperty = 1", "") + " ) AND p.Id IN " + getSyncCacheInlineQuery(loginId, page, False) + " " + sortFormula
        End If

        Dim dt As DataTable = Database.Tenant.GetDataTable(sql)
        AddBlankRowIfNoRows(dt, "DataSourceName", tableName)
        Return dt
    End Function

    Public Shared Function SyncLookupDataTable(loginId As String, nbhd As String, tableName As String) As DataTable
        Dim sortFormula As String = FieldCategory.GetSortExpression(tableName, "xt.")
        Dim sql As String = "SELECT '" + tableName + "' As DataSourceName, xt.* FROM LT_" + tableName + " xt "
        Dim dt As DataTable = Database.Tenant.GetDataTable(sql)
        AddBlankRowIfNoRows(dt, "DataSourceName", tableName)
        Return dt
    End Function

#Region "Image Conversion"

    Private Shared Function getScaledPicture(source As Bitmap, maxWidth As Integer, maxHeight As Integer) As Bitmap
        Dim width As Integer, height As Integer
        Dim aspectRatio As Single = CSng(source.Width) / CSng(source.Height)
        Dim newW = source.Width
        Dim newH = source.Height
        'this code is to prevent Indent Oritentation.
        'Dim orientationProperty As Imaging.PropertyItem = source.GetPropertyItem(&H112)

        Dim orientationProperty As PropertyItem = Nothing
        For Each propItem As PropertyItem In source.PropertyItems
            If propItem.Id = &H112 Then ' Exif orientation tag
                orientationProperty = propItem
                Exit For
            End If
        Next
        If orientationProperty IsNot Nothing Then
            Dim orientationValue As Short = BitConverter.ToInt16(orientationProperty.Value, 0)
            Select Case orientationValue
                Case 1
                Case 3
                    source.RotateFlip(RotateFlipType.Rotate180FlipNone)
                Case 6
                    source.RotateFlip(RotateFlipType.Rotate90FlipNone)
                    aspectRatio = CSng(newH) / CSng(newW)
                    newW = source.Height
                    newH = source.Width
                Case 8
                    source.RotateFlip(RotateFlipType.Rotate270FlipNone)
                    aspectRatio = CSng(newH) / CSng(newW)
                    newW = source.Height
                    newH = source.Width
            End Select
        End If
        'Scaling transformation.
        If (maxHeight > 0) AndAlso (maxWidth > 0) Then
            If (source.Width < maxWidth) AndAlso (source.Height < maxHeight) Then
                'If (newW < maxWidth) AndAlso (newH < maxHeight) Then
                'Return unchanged image
                Return source
            ElseIf aspectRatio > 1 Then
                ' Calculated width and height,
                ' and recalcuate if the height exceeds maxHeight
                width = maxWidth
                height = CInt(Math.Truncate(width / aspectRatio))
                If height > maxHeight Then
                    height = maxHeight
                    width = CInt(Math.Truncate(height * aspectRatio))
                End If
            Else
                ' Calculated width and height,
                ' and recalcuate if the width exceeds maxWidth
                height = maxHeight
                width = CInt(Math.Truncate(height * aspectRatio))
                If width > maxWidth Then
                    width = maxWidth
                    height = CInt(Math.Truncate(width / aspectRatio))
                End If
            End If
        ElseIf (maxHeight = 0) AndAlso (source.Width > maxWidth) Then
            'ElseIf (maxHeight = 0) AndAlso (newW > maxWidth) Then
            ' If MaxHeight is not provided (unlimited), and
            ' the source width exceeds maxWidth,
            ' then recalculate height
            width = maxWidth
            height = CInt(Math.Truncate(width / aspectRatio))
        ElseIf (maxWidth = 0) AndAlso (source.Height > maxHeight) Then
            'ElseIf (maxWidth = 0) AndAlso (newH > maxHeight) Then
            ' If MaxWidth is not provided (unlimited), and the
            ' source height exceeds maxHeight, then
            ' recalculate width
            height = maxHeight
                width = CInt(Math.Truncate(height * aspectRatio))
            Else
                'Return unchanged image
                Return source
        End If

        Dim newImage As Bitmap = getResizedImage(source, width, height)
        Return newImage
    End Function


    Private Shared Function getResizedImage(source As Bitmap, width As Integer, height As Integer) As Bitmap
        'This function creates the thumbnail image.
        'The logic is to create a blank image and to
        ' draw the source image onto it

        Dim thumb As New Bitmap(width, height)
        Dim gr As Graphics = Graphics.FromImage(thumb)

        gr.InterpolationMode = InterpolationMode.HighQualityBicubic
        gr.SmoothingMode = SmoothingMode.Default        'Adjust quality
        gr.PixelOffsetMode = PixelOffsetMode.HighQuality
        gr.CompositingQuality = CompositingQuality.Default 'Adjust quality

        gr.DrawImage(source, 0, 0, width, height)
        Return thumb
    End Function

    Public Shared Function GetFieldNamesForDownload(db As Database, tableName As String, Optional tableAlias As String = "") As String
        If tableAlias <> "" Then
            tableAlias = tableAlias.Trim.TrimEnd(".") + "."
        End If
        Dim str As String = ""
        For Each dr As DataRow In db.GetDataTable("SELECT DISTINCT f.Name FROM INFORMATION_SCHEMA.COLUMNS c INNER JOIN  DataSourceField f ON c.COLUMN_NAME = f.Name WHERE c.TABLE_NAME = " + tableName.ToSqlValue + " AND f.CategoryId IS NOT NULL").Rows
            str += IIf(str = "", "", ", ") + tableAlias + "[" + dr.GetString("Name") + "]"
        Next


        If str = "" Then
            str = "0 As NoFieldsAvailable"
        End If

        Return str
    End Function

#End Region

End Class
