﻿Namespace Sketching
    Public Interface ISketchProcessor

        Function ReadSketch(stream As System.IO.Stream) As List(Of SketchVector)
        Function UpdateSketch(stream As System.IO.Stream, vectors As List(Of SketchVector)) As System.IO.Stream

    End Interface
End Namespace

