﻿Imports System.Collections.Generic
Imports System.Runtime.Serialization

<DataContract(Namespace:="", Name:="KeyValue")> Public Class NameValue
    <DataMember()> Public Property key As String
    <DataMember()> Public Property value As String

    Public Sub New(key As String, value As String)
        Me.key = key
        Me.value = value
    End Sub
End Class

Public Class NameValueCollection
    Inherits List(Of NameValue)

    Public Sub New()
        MyBase.New()
    End Sub

    Public Overloads Sub Add(key As String, value As String)
        Me.Add(New NameValue(key, value))
    End Sub

End Class