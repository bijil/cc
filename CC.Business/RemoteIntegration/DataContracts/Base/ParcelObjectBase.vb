﻿Imports System.Runtime.Serialization

Namespace RemoteIntegration.Objects
    <DataContract()>
    Public MustInherit Class ParcelObjectBase

        <IgnoreDataMember()> Public Property parcel As String
            Set(value As String)
                parcelId = value
            End Set
            Get
                Return parcelId
            End Get
        End Property

        <DataMember()> Public Property parcelId As String
        <DataMember()> Public Property keys As List(Of KeyValuePair(Of String, String))


        Public Function GetParcelId(db As Database) As Integer
            If parcelId IsNot Nothing AndAlso IsNumeric(parcelId) Then
                Return parcelId
            ElseIf keys IsNot Nothing Then
                Return CAMACloud.BusinessLogic.Parcel.GetParcelId(db, keys)
            End If
            Return -1
        End Function
    End Class
End Namespace
