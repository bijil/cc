﻿Imports CAMACloud.RemoteIntegration
Imports System.Runtime.Serialization

Namespace RemoteIntegration.Objects

    Public Class ChangeField
        Public Property FieldId As Integer
        Public Property NewValue As String
        Public Property OldValue As String

    End Class
    Public Class TrackingField
        <DataMember()> Public Property FieldName As String
        <DataMember()> Public Property Value As String
        <DataMember()> Public Property TableName As String

    End Class
End Namespace
