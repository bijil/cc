﻿Imports CAMACloud.RemoteIntegration
Imports System.Runtime.Serialization

Namespace RemoteIntegration.Objects
    Public Class TableKey
        Implements IDatabaseObject

        <IgnoreDataMember()> Public Property keyId As Integer
        <IgnoreDataMember()> Public Property relationshipId As Integer
        <IgnoreDataMember()> Public Property tableId As Integer
        <DataMember(Order:=2)> Public Property fieldName As String
        <DataMember(Order:=1)> Public Property fieldId As Integer
        <IgnoreDataMember()> Public Property relatedTableId As Integer
        <DataMember(Order:=3)> Public Property relatedFieldId As Integer
        <DataMember(Order:=4)> Public Property relatedFieldName As String
        <IgnoreDataMember()> Public Property isPrimaryKey As Boolean
        <IgnoreDataMember()> Public Property isReferenceKey As Boolean

        Public Sub CreateIn(ByVal target As Data.Database, ByRef recordTracker As DataContracts.RecordCountInfo) Implements IDatabaseObject.CreateIn
            If fieldId = 0 And fieldName <> "" Then
                fieldId = Field.GetId(target, fieldName, tableId)
            End If
            If relatedFieldId = 0 And relatedFieldName <> "" Then
                relatedFieldId = Field.GetRelatedId(target, relatedFieldName, relatedTableId,tableId)
            End If
            Dim sql As String = "INSERT INTO DataSourceKeys (RelationshipId, TableId, FieldId, IsPrimaryKey, IsReferenceKey, ReferenceTableId, ReferenceFieldId) VALUES ( {0}, {1}, {2}, {3}, {4}, {5}, {6})"
            keyId = target.GetIdentityFromInsert(sql.FormatString(relationshipId, tableId, fieldId, isPrimaryKey.GetHashCode, isReferenceKey.GetHashCode, relatedTableId.NullIfZero, relatedFieldId.NullIfZero))

        End Sub

        Public Function DoesKeyRelationExist(ByVal target As Data.Database)

            If fieldId = 0 And fieldName <> "" Then
                fieldId = Field.GetId(target, fieldName, tableId)
            End If
            If relatedFieldId = 0 And relatedFieldName <> "" Then
                relatedFieldId = Field.GetRelatedId(target, relatedFieldName, relatedTableId,tableId)
            End If

            Dim sql = "SELECT COUNT(*) FROM DataSourceKeys WHERE RelationshipId={0} AND FieldId={1} AND ReferenceFieldId={2}".SqlFormatString(relationshipId, fieldId, relatedFieldId)
            If (target.GetIntegerValue(sql) > 0) Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Sub LoadFromDataRow(target As Database, dr As System.Data.DataRow) Implements IDatabaseObject.LoadFromDataRow
            Me.keyId = dr.Get("Id")
            Me.relatedFieldId = dr.Get("ReferenceFieldId")
            Me.relatedFieldName = Field.GetName(target, Me.relatedFieldId)
            Me.relatedTableId = dr.Get("ReferenceTableId")
            Me.relationshipId = dr.Get("RelationshipId")
            Me.tableId = dr.Get("TableId")
            Me.fieldId = dr.Get("FieldId")
            Me.fieldName = Field.GetName(target, Me.fieldId)
            Me.isPrimaryKey = dr.Get("IsPrimaryKey")
            Me.isReferenceKey = dr.Get("IsReferenceKey")
        End Sub
    End Class

End Namespace
