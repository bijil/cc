﻿Imports CAMACloud.RemoteIntegration
Imports System.Runtime.Serialization
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects
Imports System.IO

Namespace RemoteIntegration.Objects
    Public Class Table
        Implements IDatabaseObject

        <DataMember(Order:=1)> Public Property tableId As Integer
        <DataMember(Order:=2)> Public Property tableName As String
        <DataMember(Order:=3)> Public Property fields As List(Of Field)
        <IgnoreDataMember()> Public Property keyField1 As String
        <IgnoreDataMember()> Public Property keyField2 As String
        <IgnoreDataMember()> Public Property keyField3 As String
        <IgnoreDataMember()> Public Property auxKeyField1 As String
        <IgnoreDataMember()> Public Property auxKeyField2 As String
        <DataMember(Order:=4)> Public Property relationships As List(Of Relationship)
        <IgnoreDataMember()> Public Property keys As List(Of Relationship)
        <DataMember(Order:=5)> Public Property destinationAlias As String
        <DataMember(Order:=6)> Public Property destinationFilter As String
        <DataMember(Order:=7)> Public Property abbreviatedName As String
        <DataMember(Order:=15)> Public Property syncProperties As String
        <DataMember(Order:=16)> Public Property doNotKeepCondition As String
        <DataMember(Order:=17)> Public Property disableDeletePCI As Boolean
        <DataMember(Order:=18)> Public Property action As String


        Public Sub CreateIn(ByVal target As Data.Database, ByRef recordTracker As DataContracts.RecordCountInfo) Implements IDatabaseObject.CreateIn
            Dim sql As String = "INSERT INTO DataSourceTable (Name, KeyField1, KeyField2, KeyField3, AuxiliaryKeyField1, AuxiliaryKeyField2, DestinationAlias, DestinationFilter, AbbreviatedName, SyncProperties, DoNotKeepCondition, DisableDeletePCI) VALUES ( {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11})"
            tableId = target.GetIdentityFromInsert(sql.SqlFormat(True, tableName, keyField1, keyField2, keyField3, auxKeyField1, auxKeyField2, destinationAlias, destinationFilter, abbreviatedName, syncProperties, doNotKeepCondition, disableDeletePCI))

            If keys Is Nothing Then
                keys = New List(Of Relationship)
            End If

            For Each f In fields
                f.table = Me
                f.tableId = tableId
                f.CreateIn(target, recordTracker)
                recordTracker.fieldsAffected += 1
            Next

            For Each relation In keys
                relation.UpdatePrimaryKey(target, Me, recordTracker)
            Next

            recordTracker.tablesAffected += 1
        End Sub

        Public Sub ChangeSchema(ByVal target As Data.Database, ByRef recordTracker As DataContracts.RecordCountInfo)

            Dim removedFields As New List(Of Field)
			
            If keys Is Nothing Then
                keys = New List(Of Relationship)
            End If


            '' If Table does not exist && action <> 'remove'
            If action = "remove" Then
                If DoesTableExist(target) Then
                    tableId = target.GetStringValue("SELECT Id FROM DataSourceTable WHERE Name={0}".SqlFormatString(tableName))
                    DataSource.DeleteTable(target, tableId)
                    Exit Sub
                Else
                    Throw New Exceptions.BadRequestException("The requested table '" + tableName + "' does not exist in the schema, but is requested to be deleted.", Nothing)
                End If
            ElseIf (DoesTableExist(target) = False) Then
                CreateIn(target, recordTracker)
            Else
                Dim listID As String = Guid.NewGuid.ToString
                '' If table exists, just update fields. Remove or Add if not existing
                tableId = target.GetStringValue("SELECT Id FROM DataSourceTable WHERE Name={0}".SqlFormatString(tableName))
                For Each f In fields
                	  f.table = Me
                    Select Case f.action
                        Case "remove"
                            If (f.IsReferenceField(target) = True) Then
                                Dim errorMessage As String = "Error while removing field '{1}' on table '{0}',  is referenced by some other table fields".FormatString(f.table.tableName, f.name)
                                Throw New Exceptions.BadRequestException(errorMessage, Nothing)
                            End If
                        End Select
                Next
                
                For Each f In fields
                	Dim newMaxLength As Integer = -1
                    f.table = Me
                    f.tableId = tableId
                    Select Case f.action
                        Case "remove"
                            If (f.IsReferenceField(target) = False) Then
                                f.fieldId = f.GetFieldId(target, f.name, tableId)
                                f.RemoveFieldRelations(target)
                                f.RemoveField(target, tableName, f.name, tableId)
                                removedFields.Add(f)
                            End If
                        Case Else
                            If (Not f.DoesFieldExist(target)) Then
                                f.CreateIn(target, recordTracker)
                                For Each relation In keys
                                    relation.UpdatePrimaryKey(target, Me, recordTracker)
                                Next
                            Else
                                If f.action = "edit" Then
                                    Dim ccDataType As Integer = f.TranslateType(f.dataType, f.maxLength)
                                    Dim field As SchemaField = New SchemaField()
                                    field.Name = f.name
                                    field.DataType = ccDataType
                                    field.MaxLength = f.maxLength
                                    field.Precision = f.precision
                                    field.Scale = f.scale
                                    Dim schemaDataType As String = f.dataType
                                    If f.dataType.ToLower = "varchar" Then
                                        If f.maxLength > 255 Or f.maxLength < 1 Then
                                            schemaDataType = "varchar(MAX)"
                                        ElseIf f.maxLength > 0 Then
                                            schemaDataType = "varchar({0})".FormatString(f.maxLength)
                                        Else
                                            schemaDataType = "varchar(50)"
                                        End If
                                    ElseIf f.dataType.ToLower = "numeric" Then
                                        If f.precision > 0 Then
                                            If f.precision >= f.scale Then
                                            	If f.precision = f.scale Then
                                            		If f.maxLength <> 0 Then
                                            			newMaxLength = f.maxLength	
                                            		End If
                                            		f.maxLength = f.precision + 2
                                            		schemaDataType = "numeric" + "(" & f.precision+1 & "," & f.scale & ")"
                                            		
                                            	Else
                                            		If f.maxLength > f.precision + 1 Then
                                            		Else
                                            			f.maxLength = f.precision + 1
                                            		End If
						                    		schemaDataType = "numeric" + "(" & f.precision & "," & f.scale & ")"
						                    	End If  
                                            Else
                                                Dim errorMessage As String = "Error while parsing schema on table '{0}', field '{1}' -  Numeric property scale ({2}) must be less than or equal to precision ({3}).".FormatString(f.table.tableName, f.name, f.scale, f.precision)
                                                Throw New Exceptions.BadRequestException(errorMessage, Nothing)
                                            End If
                                        Else
                                            schemaDataType = "numeric"
                                        End If
                                    End If
                                    Try

                                        '										ItemId=tableid, ItemId1=newdatatypeid, ItemName=fieldname, ItemName2=newdatatype, ItemName3=schemadatatype
                                        Dim sql As String = "INSERT INTO ProcessList (ListID, ItemId,ItemId2, ItemName, ItemName2, ItemName3) VALUES ( {0}, {1}, {2}, {3}, {4}, {5})"
                                        target.Execute(sql.SqlFormat(True, listID, tableId, ccDataType, f.name, field.SqlTypeDeclaration, schemaDataType))
                                        '									
                                    Catch exs As System.Data.SqlClient.SqlException
                                        Throw New Exceptions.BadRequestException(exs.Message, Nothing)
                                    Catch ex As Exception
                                        Throw New Exceptions.BadRequestException(ex.Message, Nothing)
                                    End Try
                                End If
                                f.UpdateFieldProperties(target, tableId)
								 If newMaxLength <>-1 Then
									f.maxLength = newMaxLength
								 End If
                            End If
                    End Select
                Next

                Dim updatePropsSql As String = "UPDATE DataSourceTable SET DestinationAlias = {1}, DestinationFilter = {2}, AbbreviatedName = {3}, SyncProperties = {4} WHERE ID = {0}".SqlFormat(True, tableId, destinationAlias, destinationFilter, abbreviatedName, syncProperties)
                target.Execute(updatePropsSql)
                Try
                    Dim schemasql As String = "EXEC schema_ChangeDataTypeBulk  @listID = {0}"
                    target.Execute(schemasql.SqlFormatString(listID))
                Catch exs As System.Data.SqlClient.SqlException
                    Throw New Exceptions.BadRequestException(exs.Message, Nothing)
                Catch ex As Exception
                    Throw New Exceptions.BadRequestException(ex.Message, Nothing)
                End Try

                Dim deleteProcessListSql As String = "DELETE FROM processlist WHERE ListID= {0}"
                target.Execute(deleteProcessListSql.SqlFormatString(listID))
            End If


            If (removedFields.Count > 0) Then
                For Each flds In removedFields
                    fields.Remove(flds)
                Next
            End If

            If relationships Is Nothing Then
                relationships = New List(Of Relationship)
            End If

            For Each r In relationships
                r.Delete(target)
                r.CreateIn(target, recordTracker)
            Next
			

        End Sub

        Public Shared Function GetId(target As Database, name As String) As Integer
            Dim tId As Integer = target.GetIntegerValueOrInvalid("SELECT Id FROM DataSourceTable WHERE Name = " + name.ToSqlValue)
            If tId = -1 Then
                Throw New Exceptions.BadRequestException("The requested table '" + name + "' does not exist in the schema.", Nothing)
            End If
            Return tId
        End Function


        Public Shared Function GetName(target As Database, id As Integer) As String
            Return target.GetStringValue("SELECT Name FROM DataSourceTable WHERE Id = " & id)
        End Function

        Public Function DoesTableExist(target As Database) As Boolean
            Dim sql = "SELECT COUNT(*) FROM DataSourcetable WHERE Name={0}".SqlFormatString(tableName)
            If (target.GetIntegerValue(sql) > 0) Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Sub LoadFromDataRow(target As Database, dr As System.Data.DataRow) Implements IDatabaseObject.LoadFromDataRow
            auxKeyField1 = dr.Get("AuxiliaryKeyField1")
            auxKeyField2 = dr.Get("AuxiliaryKeyField2")
            keyField1 = dr.Get("KeyField1")
            keyField2 = dr.Get("KeyField2")
            keyField3 = dr.Get("KeyField3")
            tableId = dr.Get("Id")
            tableName = dr.Get("Name")
            destinationAlias = dr.Get("DestinationAlias")
            destinationFilter = dr.Get("DestinationFilter")
            abbreviatedName = dr.Get("AbbreviatedName")
            syncProperties = dr.Get("SyncProperties")
            doNotKeepCondition = dr.Get("DoNotKeepCondition")
            disableDeletePCI = dr.GetBoolean("DisableDeletePCI")
            fields = New List(Of Field)
            For Each fr As DataRow In target.GetDataTable("SELECT * FROM DataSourceField WHERE IsCustomField = 0 AND TableId = " & tableId & " ORDER BY SourceOrdinal").Rows
                Dim f As New Field
                f.table = Me
                f.LoadFromDataRow(target, fr)
                fields.Add(f)
            Next

            'keys = New List(Of Relationship)
            relationships = New List(Of Relationship)
            For Each rr As DataRow In target.GetDataTable("SELECT * FROM DataSourceRelationships WHERE TableId = " & tableId & " AND Relationship IS NULL").Rows
                Dim r As New Relationship
                r.tableId = tableId
                r.LoadFromDataRow(target, rr)
                relationships.Add(r)

                For Each rx In r.relations
                    Dim rf = rx
                    Dim pkFields = fields.Where(Function(x) x.fieldId = rf.fieldId)
                    If pkFields.Count = 0 Then
                        'Throw New Exception("Schema table " + tableName.ToSqlValue + " has no primary fields.")
                    Else
                        pkFields.First.isPrimaryKey = True
                    End If

                Next
            Next


            For Each rr As DataRow In target.GetDataTable("SELECT * FROM DataSourceRelationships WHERE TableId = " & tableId & " AND Relationship IS NOT NULL").Rows
                Dim r As New Relationship
                r.tableId = tableId
                r.LoadFromDataRow(target, rr)
                relationships.Add(r)
            Next


        End Sub

        Public Shared Function updateProperty(target As Database, tableName As String, PropertyName As String, value As String) As Integer
            Dim tId As Integer = target.GetIntegerValueOrInvalid("SELECT Id FROM DataSourceTable WHERE Name = " + tableName.ToSqlValue)
            If (PropertyName = "syncProperties" Or PropertyName = "abbreviatedName" Or PropertyName = "DestinationAlias" Or PropertyName = "destinationFilter" Or PropertyName = "destinationFilter" Or PropertyName = "CC_Note" Or PropertyName = "DisableDeletePCI") Then
                If tId > 0 Then
                    Dim updatePropsSql As String = "UPDATE DataSourceTable SET  [{1}] = {2} WHERE name  = {0}".FormatString(tableName.ToSqlValue, PropertyName, value.ToSqlValue)
                    target.Execute(updatePropsSql)
                Else
                    Throw New Exceptions.BadRequestException("The requested table '" + tableName + "' does not exist in the schema.", Nothing)
                End If
                Return tId
            Else
                Throw New Exceptions.BadRequestException("The requested Property '" + PropertyName + "' cannot be edited.", Nothing)
            End If

        End Function
    End Class


End Namespace

