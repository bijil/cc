﻿Imports CAMACloud.RemoteIntegration
Imports System.Runtime.Serialization

Namespace RemoteIntegration.Objects
    Public Enum RelationshipType As Integer
        OneToOne = 0
        OneToMany = 1
        ManyToOne = 2
        ManyToMany = 3
    End Enum

    Public Class Relationship
        Implements IDatabaseObject

        <IgnoreDataMember()> Public Property relationshipId As Integer
        <DataMember(Order:=1)> Public Property name As String
        <IgnoreDataMember()> Public Property tableId As Integer
        <DataMember(Order:=2)> Public Property tableName As String
        <IgnoreDataMember()> Public Property relatedTableId As Integer
        <DataMember(Order:=3)> Public Property relatedTableName As String
        <DataMember(Order:=4)> Public Property relationship As RelationshipType
        <DataMember(Order:=5)> Public Property relations As List(Of TableKey)

        Public Sub New()
            relations = New List(Of TableKey)
        End Sub

        Public Sub Delete(ByVal target As Data.Database)
            If tableId = 0 And tableName <> String.Empty Then
                tableId = Table.GetId(target, tableName)
            End If
            If relatedTableId = 0 And relatedTableName <> String.Empty Then
                relatedTableId = Table.GetId(target, relatedTableName)
            End If

            Dim isPrimaryKey As Boolean = False
            If relatedTableId = 0 Then
                isPrimaryKey = True
            End If

            Dim relationId As Integer = -1
            If isPrimaryKey Then
                relationId = target.GetIntegerValue("SELECT ID FROM DataSourceRelationships WHERE TableId = {0} AND Relationship IS NULL".SqlFormat(False, tableId))
            Else
                relationId = target.GetIntegerValue("SELECT ID FROM DataSourceRelationships WHERE TableId = {0} AND Relationship IS NOT NULL AND ParentTableId = {1}".SqlFormat(False, tableId, relatedTableId))
            End If

            target.Execute("DELETE FROM DataSourceKeys WHERE RelationshipId = " & relationId)
            target.Execute("DELETE FROM DataSourceRelationships WHERE ID = " & relationId)
        End Sub

        Public Sub CreateIn(ByVal target As Data.Database, ByRef recordTracker As DataContracts.RecordCountInfo) Implements IDatabaseObject.CreateIn

            If tableId = 0 And tableName <> String.Empty Then
                tableId = Table.GetId(target, tableName)
            End If
            If relatedTableId = 0 And relatedTableName <> String.Empty Then
                relatedTableId = Table.GetId(target, relatedTableName)
            End If

            Dim sql As String = "INSERT INTO DataSourceRelationships (Name, DataGroup, TableId, ParentTableId, Relationship) VALUES ( {0}, {1}, {2}, {3}, {4})"
            relationshipId = target.GetIdentityFromInsert(sql.FormatString(name.ToSqlValue, "P".ToSqlValue, tableId, relatedTableId.NullIfZero, IIf(relatedTableId = 0, "null", relationship.GetHashCode)))
            For Each key In relations
                key.isReferenceKey = (relatedTableId <> 0)
                key.isPrimaryKey = (relatedTableId = 0)
                key.relationshipId = relationshipId
                key.tableId = tableId
                key.relatedTableId = relatedTableId
                key.CreateIn(target, recordTracker)
            Next
        End Sub

        Public Sub ChangeCreateIn(ByVal target As Data.Database, ByRef recordTracker As DataContracts.RecordCountInfo)

            If tableId = 0 And tableName <> String.Empty Then
                tableId = Table.GetId(target, tableName)
            End If
            If relatedTableId = 0 And relatedTableName <> String.Empty Then
                relatedTableId = Table.GetId(target, relatedTableName)
            End If

            If (DoesRelationExist(target) = False) Then
                Dim sql As String = "INSERT INTO DataSourceRelationships (Name, DataGroup, TableId, ParentTableId, Relationship) VALUES ( {0}, {1}, {2}, {3}, {4})"
                relationshipId = target.GetIdentityFromInsert(sql.FormatString(name.ToSqlValue, "P".ToSqlValue, tableId, relatedTableId.NullIfZero, IIf(relatedTableId = 0, "null", relationship.GetHashCode)))
            Else
                relationshipId = GetRelationId(target)
            End If

            For Each key In relations
                key.isReferenceKey = (relatedTableId <> 0)
                key.isPrimaryKey = (relatedTableId = 0)
                key.relationshipId = relationshipId
                key.tableId = tableId
                key.relatedTableId = relatedTableId
                If (key.DoesKeyRelationExist(target) = False) Then
                    key.CreateIn(target, recordTracker)
                End If
            Next
        End Sub

        Public Sub UpdatePrimaryKey(ByVal target As Database, ByRef table As Table, ByRef recordTracker As DataContracts.RecordCountInfo)
            If target.GetIntegerValue("SELECT COUNT(*) FROM DataSourceRelationships WHERE Name = " + name.ToSqlValue + " AND TableId = " + tableId.ToString) = 0 Then
                CreateIn(target, recordTracker)
            End If
        End Sub

        Public Function DoesRelationExist(target As Database)
            Dim sql = "SELECT COUNT(*) FROM DataSourceRelationships WHERE TableId={0} AND ParentTableId={1}".SqlFormatString(tableId, relatedTableId)
            If (target.GetIntegerValue(sql) > 0) Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Function GetRelationId(target As Database)
            Return target.GetStringValue("SELECT Id FROM DataSourceRelationships WHERE TableId={0} AND ParentTableId={1}".SqlFormatString(tableId, relatedTableId))
        End Function

        Public Sub LoadFromDataRow(target As Database, dr As System.Data.DataRow) Implements IDatabaseObject.LoadFromDataRow
            Me.relationshipId = dr.Get("Id")
            Me.name = dr.Get("Name")
            Me.tableId = dr.Get("TableId")
            Me.tableName = Table.GetName(target, Me.tableId)
            Me.relatedTableId = dr.Get("ParentTableId")
            Me.relatedTableName = Table.GetName(target, Me.relatedTableId)
            Me.relationship = dr.Get("Relationship")

            relations = New List(Of TableKey)
            For Each fr As DataRow In target.GetDataTable("SELECT * FROM DataSourceKeys WHERE RelationshipId = " & relationshipId).Rows
                Dim f As New TableKey
                f.LoadFromDataRow(target, fr)
                relations.Add(f)
            Next
        End Sub
    End Class
End Namespace
