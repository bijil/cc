﻿Imports System.Runtime.Serialization

Namespace RemoteIntegration.DataContracts
    <DataContract(Namespace:="")>
    Public Class PropertyValue
        <DataMember(Order:=1)> Public Property TableName As String
        <DataMember(Order:=2)> Public Property FieldName As String
        <DataMember(Order:=3)> Public Property PropertyName As String
        <DataMember(Order:=4)> Public Property Value As String
    End Class

End Namespace