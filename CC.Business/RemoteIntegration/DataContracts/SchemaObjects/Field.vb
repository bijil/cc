﻿Imports CAMACloud
Imports CAMACloud.RemoteIntegration
Imports System.Runtime.Serialization

Namespace RemoteIntegration.Objects
    Public Class Field
        Implements IDatabaseObject


        <IgnoreDataMember()> Public Property table As Table
        <DataMember(Order:=1)> Public Property tableId As Integer
        <DataMember(Order:=2)> Public Property fieldId As Integer
        <DataMember()> Public Property action As String
        <DataMember(Order:=3)> Public Property name As String
        <DataMember(Order:=4)> Public Property assignedName As String
        <DataMember(Order:=5)> Public Property defaultValue As String
        <DataMember(Order:=6)> Public Property label As String
        <DataMember(Order:=7)> Public Property dataType As String
        <DataMember(Order:=8)> Public Property maxLength As Integer
        <DataMember(Order:=9)> Public Property precision As Integer
        <DataMember(Order:=10)> Public Property scale As Integer
        <DataMember(Order:=11)> Public Property isCalculated As Boolean
        <DataMember(Order:=12)> Public Property [readOnly] As Boolean
        <DataMember(Order:=13)> Public Property ordinal As Integer
        <DataMember(Order:=14)> Public Property isPrimaryKey As Boolean
        <DataMember(Order:=15)> Public Property aliasSource As String
        <DataMember(Order:=16)> Public Property sourceCalculationExpression As String
        <DataMember(Order:=17)> Public Property syncProperties As String
        <DataMember(Order:=18)> Public Property exclusiveForSS As Boolean
        <DataMember(Order:=19)> Public Property ssDefaultValue As String




        Public Sub CreateIn(ByVal target As Data.Database, ByRef recordTracker As DataContracts.RecordCountInfo) Implements IDatabaseObject.CreateIn
        	Dim ccDataType As Integer = TranslateType(dataType, maxLength)
        	Dim nMaxLength As Integer = -1
            Dim schemaDataType As String = dataType
            If dataType.ToLower = "varchar" Or dataType.ToLower = "text" Or dataType.ToLower = "date" Or dataType.ToLower = "bit" Then
            	precision = 0
            	scale = 0
            End If
            
            If dataType.ToLower = "varchar" Then
                If maxLength > 255 Or maxLength < 1 Then
                    schemaDataType = "varchar(MAX)"
                ElseIf maxLength > 0 Then
                    schemaDataType = "varchar({0})".FormatString(maxLength)
                Else
                    schemaDataType = "varchar(50)"
                End If
            ElseIf dataType.ToLower = "numeric" Then
                If precision > 0 Then
                    If precision >= scale Then
                        If precision = scale Then
                            If maxLength <> 0 Then
                                nMaxLength = maxLength
                            End If
                            maxLength = precision + 2
                            schemaDataType = "numeric" + "(" & precision + 1 & "," & scale & ")"
                        Else
                            If maxLength > precision + 1 Then
                            Else
                            	maxLength = precision + 1
                            End If
                    		schemaDataType = "numeric" + "(" & precision & "," & scale & ")"
                    	End If       
                  	Else
                        Dim errorMessage As String = "Error while parsing schema on table '{0}', field '{1}' - Numeric property scale ({2}) must be less than or equal to precision ({3}).".FormatString(table.tableName, name, scale, precision)
                        Throw New Exceptions.BadRequestException(errorMessage, Nothing)
                    End If
                Else
                    schemaDataType = "numeric"
                End If
            End If
            If precision = Nothing Then
                precision = 0
            ElseIf precision > 0 Then
                maxLength = precision + 1
            End If

            Dim sql As String = "INSERT INTO DataSourceField (TableId, Name, DataType, AssignedName, DisplayLabel, IsReadOnly, MaxLength, SourceOrdinal, SourceTable,DefaultValue, AliasSource, SourceCalculationExpression, SyncProperties, SchemaDataType, Precision,NumericPrecision,NumericScale,ExclusiveForSS,SSDefaultValue) VALUES ( {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8},{9}, {10}, {11}, {12}, {13}, {14},{15},{16},{17},{18})"
            fieldId = target.GetIdentityFromInsert(sql.SqlFormat(True, tableId, name, ccDataType, assignedName, label, [readOnly], maxLength, ordinal, table.tableName, defaultValue, aliasSource, sourceCalculationExpression, syncProperties, schemaDataType, scale, precision, scale, exclusiveForSS, ssDefaultValue))
            If isPrimaryKey Then
                Dim r As New Relationship
                r.name = "PK_" + table.tableName
                r.tableId = table.tableId
                If (table.keys.Count = 0) Then
                    table.keys.Add(r)
                Else
                    r = table.keys(0)
                End If


                Dim k As New TableKey
                k.fieldId = fieldId
                k.fieldName = name
                k.isPrimaryKey = True
                k.isReferenceKey = False
                k.tableId = table.tableId
                r.relations.Add(k)

            End If
            If nMaxLength <> -1 Then
            	maxLength = nMaxLength
            End If
           
        End Sub

        Public Function IsReferenceField(ByVal target As Database) As Boolean
            If target.GetIntegerValue("SELECT COUNT(*) FROM DataSourceKeys WHERE  ReferenceFieldId=" + fieldId.ToString) = 0 Then
                Return False
            Else
                Return True
            End If
        End Function

        Public Sub RemoveField(ByVal target As Database, ByVal tname As String, ByVal fieldname As String, ByVal tableId As Integer)

            target.Execute("DELETE FROM DataSourceField WHERE Name={0} AND TableId={1}".SqlFormatString(fieldname, tableId))
            Dim targetTable As String = DataSource.FindTargetTable(target, tname)
            If (target.GetIntegerValue("SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ={0} AND COLUMN_NAME ={1}".SqlFormatString(targetTable, fieldname)) > 0) Then
                'target.Execute("ALTER TABLE [{0}] DROP COLUMN  [{1}];".FormatString(targetTable, name))
                target.Execute("EXEC Api_SchemaFieldRemovalWithIndex {0}, {1}".FormatString(targetTable, fieldname))
            End If

        End Sub

		Public Sub UpdateFieldProperties(ByVal target As Data.Database, tableId As Integer)
            'Update - MaxLength, Precision, Scale, AssignedName, Label, Required
            target.Execute("UPDATE DataSourceField SET SyncProperties = {2} , MaxLength = {3}, Precision = {5}, NumericPrecision = {4},NumericScale = {5}, AssignedName = {6}, DisplayLabel = {7},ExclusiveForSS={8},SSDefaultValue={9},sourceCalculationExpression={10} WHERE Name = {0} AND TableID = {1}".SqlFormat(True, name, tableId, syncProperties, maxLength, precision, scale, assignedName, label, exclusiveForSS, ssDefaultValue, sourceCalculationExpression))

        End Sub

        '        Public Sub UpdateField(ByVal target As Database, ByVal tname As String, ByVal fieldname As String, fieldid As Integer, syncProperties As String, maxLength As String, newDataType As String)
        '
        '            Dim ccDataType As Integer = TranslateType(newDataType, maxLength)
        '            Dim schemaDataType As String = newDataType
        '            If newDataType.ToLower = "varchar" Then
        '                If maxLength > 255 Or maxLength < 1 Then
        '                    schemaDataType = "varchar(MAX)"
        '                ElseIf maxLength > 0 Then
        '                    schemaDataType = "varchar({0})".FormatString(maxLength)
        '                Else
        '                    schemaDataType = "varchar(50)"
        '                End If
        '            End If
        '            Dim targetTable As String = DataSource.FindTargetTable(target, tname)
        '            If (target.GetIntegerValue("SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ={0} AND COLUMN_NAME ={1}".SqlFormatString(targetTable, name)) > 0) Then
        '                Try
        '                    target.Execute("ALTER TABLE [{0}] ALTER COLUMN  {1} {2};".FormatString(targetTable, fieldname, schemaDataType))
        '                Catch ex As Exception
        '                    Throw New Exception("data type conversion failed due to some unknown reasons")
        '                End Try
        '            End If
        '            target.Execute("UPDATE Datasourcetable set datatype={0},maxlength{1},syncProperties={2},schemadatatype={3} where id={4}".SqlFormatString(ccDataType, maxLength, syncProperties, schemaDataType, fieldid))
        '        End Sub
        Public Sub RemoveFieldRelations(target As Database)
            target.Execute("DELETE FROM DataSourceKeys WHERE fieldId={0}".SqlFormatString(fieldId))
        End Sub

        Public Function DoesFieldExist(ByVal target As Database)
            If (target.GetIntegerValue("SELECT COUNT(*) FROM DataSourceField WHERE Name={0} AND TableId={1}".SqlFormatString(name, tableId.ToString)) > 0) Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Function GetFieldId(ByVal target As Database, fieldName As String, tableName As String)
            Return target.GetIntegerValue("SELECT Id FROM DatasourceField WHERE Name={0} AND TableId={1}".SqlFormatString(fieldName, tableName))
        End Function


        Public Shared Function TranslateType(ByVal sqlDataType As String, ByVal maxLength As Integer) As Integer
            sqlDataType = sqlDataType.ToLower
            If sqlDataType.Contains("varchar") Then
                maxLength = GetCharLength("varchar", sqlDataType, maxLength)
            ElseIf sqlDataType.Contains("char") Then
                maxLength = GetCharLength("char", sqlDataType, maxLength)
            End If

            If sqlDataType = "varchar" And maxLength <= 255 Then
                Return 1
            ElseIf sqlDataType = "float" Or sqlDataType = "numeric" Or sqlDataType = "decimal" Then
                Return 2
            ElseIf sqlDataType = "bit" Then
                Return 3
            ElseIf sqlDataType = "date" Or sqlDataType = "datetime" Then
                Return 4
            ElseIf sqlDataType = "varchar" And maxLength > 255 Then
                Return 6

            ElseIf sqlDataType = "text" Then
                Return 6
            ElseIf sqlDataType = "money" Then

                Return 7
            ElseIf sqlDataType = "int" Then
                Return 8
            Else
                Return 1
            End If
        End Function

        Public Function UntranslateType(ccDataType As Integer) As String
            Select Case ccDataType
                Case 7, 9
                    Return "money"
                Case 2
                    Return "numeric"
                Case 8
                    Return "int"
                Case 3
                    Return "bit"
                Case 4
                    Return "datetime"
                Case 10
                    Return "int"
                Case Else
                    Return "varchar"
            End Select
        End Function


        Private Shared Function GetCharLength(ByVal type As String, ByRef sqlDataType As String, ByRef maxLength As Integer) As Integer
            Dim testLength As Integer = 50
            Dim lengthPart As String = sqlDataType.Replace(type, "").Trim.TrimStart("(").TrimEnd(")").Trim
            sqlDataType = type
            If lengthPart = "max" Then
                testLength = 8000
            Else
                Integer.TryParse(lengthPart, testLength)
            End If
            If maxLength = Nothing OrElse maxLength <= 0 Then
                maxLength = testLength
            End If
            Return maxLength
        End Function

        Public Shared Function GetId(ByVal target As Database, ByVal name As String, ByVal tableId As Integer) As Integer
            Dim fId As Integer = target.GetIntegerValueOrInvalid("SELECT Id FROM DataSourceField WHERE Name = " + name.ToSqlValue + " AND TableId = " + tableId.ToString)
            If fId = -1 Then
                Dim tableName As String = target.GetStringValue("SELECT Name FROM DataSourceTable WHERE Id = " & tableId)
                If tableName Is Nothing Then
                    tableName = "<NoTable_" & tableId & ">"
                End If
                Throw New Exceptions.BadRequestException("The requested field '" + name + "' does not exist in the table '" & tableName & "' schema.", Nothing)
            End If
            Return fId
        End Function
        
         Public Shared Function GetRelatedId(ByVal target As Database, ByVal relatedFieldname As String, ByVal relatedtableId As Integer, ByVal tableId As Integer) As Integer
            Dim fId As Integer = target.GetIntegerValueOrInvalid("SELECT Id FROM DataSourceField WHERE Name = " + relatedFieldname.ToSqlValue + " AND TableId = " + relatedtableId.ToString)
            If fId = -1 Then
            	 Dim relatedtableName As String = target.GetStringValue("SELECT Name FROM DataSourceTable WHERE Id = " & relatedtableId)
                If relatedtableName Is Nothing Then
                    relatedtableName = "<NoTable_" & relatedtableId & ">"
                End If
                Dim tableName As String = target.GetStringValue("SELECT Name FROM DataSourceTable WHERE Id = " & tableId)
                If tableName Is Nothing Then
                    tableName = "<NoTable_" & tableId & ">"
                End If
                Throw New Exceptions.BadRequestException("The Foreign Key '" & relatedtableName &"." & relatedFieldname & "' does not exist in the table '" & tableName & "' schema.", Nothing)
            End If
            Return fId
        End Function

        Public Shared Function GetName(ByVal target As Database, ByVal id As Integer) As String
            Return target.GetStringValue("SELECT Name FROM DataSourceField WHERE Id = " & id)
        End Function

        Public Sub LoadFromDataRow(ByVal target As Database, ByVal dr As System.Data.DataRow) Implements IDatabaseObject.LoadFromDataRow
            Me.assignedName = dr.Get("AssignedName")
            Me.dataType = IIf(dr.Get("SchemaDataType") Is Nothing, UntranslateType(dr.Get("DataType")), dr.GetString("SchemaDataType"))
            If Me.dataType.Contains("(") AndAlso Not Me.dataType.ToLower.Contains("(max)") Then
                Me.dataType = Me.dataType.Substring(0, Me.dataType.IndexOf("(")).ToUpper
            End If
            Me.fieldId = dr.Get("Id")
            Me.isCalculated = dr.Get("IsCalculated")
            Me.label = dr.Get("DisplayLabel")
            Me.maxLength = dr.Get("MaxLength")
            Me.precision = dr.GetInteger("NumericPrecision")
            Me.scale = dr.GetInteger("NumericScale")
            Me.name = dr.Get("Name")
            Me.ordinal = dr.Get("SourceOrdinal")
            Me.readOnly = dr.Get("IsReadOnly")
            'Me.isPrimaryKey = dr.Get("IsPrimaryKey")
            Me.tableId = dr.Get("TableId")
            Me.defaultValue = dr.Get("DefaultValue")
            Me.aliasSource = dr.Get("AliasSource")
            Me.sourceCalculationExpression = dr.Get("SourceCalculationExpression")
            Me.syncProperties = dr.Get("SyncProperties")
            Me.exclusiveForSS = dr.Get("ExclusiveForSS")
            Me.ssDefaultValue = dr.Get("SSDefaultValue")
        End Sub

        Public Shared Function updateProperty(target As Database, tableName As String, fieldName As String, PropertyName As String, value As String) As Integer
            If (PropertyName = "syncProperties" Or PropertyName = "CC_Note" Or PropertyName = "SourceCalculationExpression" Or PropertyName = "AliasSource" Or PropertyName = "CalculationOverrideExpression" Or PropertyName = "LookupQuery" Or PropertyName = "VisibilityExpression" Or PropertyName = "MaxLength") Then
                Dim tId As Integer = target.GetIntegerValueOrInvalid("SELECT Id FROM DataSourceField WHERE Name ={0} and SourceTable={1}".SqlFormat(True, fieldName, tableName))


                Return tId
            Else
                Throw New Exceptions.BadRequestException("The requested Property '" + PropertyName + "' cannot be edited.", Nothing)
            End If


        End Function

    End Class

    Public Class SchemaField
        Public Name As String
        Public DataType As Integer
        Public SchemaDataType As String
        Public MaxLength As Integer
        Public Precision As Integer
        Public Scale As Integer


        Public ReadOnly Property SchemaTypeDeclaration As String
            Get
                Dim stdec As String = SqlTypeDeclaration
                If SchemaDataType IsNot Nothing Then
                    If SchemaDataType = "varchar" Then
                        If MaxLength = 0 Then
                            stdec = "varchar(255)"
                        ElseIf MaxLength > 255 Then
                            stdec = "varchar(MAX)"
                        Else
                            stdec = "varchar(" & MaxLength & ")"
                        End If
                    ElseIf SchemaDataType.ToLower = "char" Then
                        If MaxLength = 0 Then
                            stdec = "varchar(255)"
                        ElseIf MaxLength > 255 Then
                            stdec = "varchar(MAX)"
                        Else
                            stdec = "varchar(" & MaxLength & ")"
                        End If
                    Else
                        stdec = SchemaDataType
                    End If
                End If
                Return stdec
            End Get
        End Property

        Public ReadOnly Property SqlTypeDeclaration As String
            Get
                Select Case DataType
                    Case 1
                        If MaxLength = 0 Then MaxLength = 255
                        Return "varchar(" & MaxLength & ")"
                    Case 2
                    	If Precision > 0 Then
                    		If Precision=Scale Then
                    			Dim Prec As Integer=Precision+1
                    			Return "numeric(" + Prec.ToString() + "," + Scale.ToString() + ")"
                    		Else
                    			Return "numeric(" + Precision.ToString() + "," + Scale.ToString() + ")"
                    		End If
                            
                        Else
                            Return "float"
                        End If
                    Case 3
                        Return "bit"
                    Case 4
                        Return "datetime"
                    Case 5
                        Return "varchar(255)"
                    Case 6
                        Return "varchar(MAX)"
                    Case 7
                        Return "numeric(18, 2)"
                    Case 8
                        Return "int"
                    Case 9
                        Return "int"
                    Case 10
                        Return "int"
                    Case 11
                        Return "varchar(MAX)"
                    Case Else
                        Return "varchar(255)"
                End Select
            End Get
        End Property
    End Class

End Namespace
