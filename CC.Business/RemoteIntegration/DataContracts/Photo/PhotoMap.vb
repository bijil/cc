﻿
Imports System.Runtime.Serialization

Namespace RemoteIntegration.DataContracts
    <DataContract(Namespace:="")> _
    Public Class PhotoMap
        <DataMember(Order:=1)> Public Property keys As List(Of KeyValuePair(Of String, String))
        <DataMember(Order:=2)> Public Property fileName As String
    End Class

End Namespace
