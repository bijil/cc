﻿
Namespace RemoteIntegration.Objects
    Public Class JobInfo
        Public Property ID As Integer
        Public Property Type As String
        Public Property CreatedTime As DateTime
        Public Property SourceIPAddress As String
    End Class
End Namespace

