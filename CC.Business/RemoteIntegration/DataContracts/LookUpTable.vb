﻿Namespace RemoteIntegration.Objects
    Public Class LookUpTable
        Implements IDatabaseObject

        Public Property lookupName As String
        Public Property lookUpValues As LookUpValue()


        Public Sub CreateIn(ByVal target As Data.Database, ByRef recordTracker As DataContracts.RecordCountInfo) Implements IDatabaseObject.CreateIn

        	Dim tempParcelDataLookup="temp_ParcelDataLookup"
        	Dim updateSql = ""
        	If target.DoesTableExists(tempParcelDataLookup) Then
				target.DropTable(tempParcelDataLookup)
			End If
			Dim sqlBackup="SELECT * INTO "+ tempParcelDataLookup+ " FROM ParcelDataLookup WHERE LookupName={0}"
			target.Execute(sqlBackup.SqlFormatString(lookupName))
            Dim sql = "DELETE FROM ParcelDataLookup WHERE LookupName={0}"
            target.Execute(sql.SqlFormatString(lookupName))
            For Each lk As LookUpValue In lookUpValues
            	Dim insertSql = ""
            
                If lk.idValue Is Nothing Then
                    lk.idValue = ""
                End If
                insertSql = "INSERT INTO ParcelDataLookup(LookupName,IdValue,NameValue,DescValue,Ordinal) VALUES({0},{1},{2},{3},{4}) ".SqlFormatString(lookupName, lk.idValue, lk.nameValue, lk.descValue, lk.ordinal)
                target.Execute(insertSql)
                recordTracker.recordsInserted += 1
            Next
            updateSql="UPDATE p SET p.Value=t.Value FROM ParcelDataLookup p JOIN "+ tempParcelDataLookup +" t ON p.IdValue=t.IdValue AND p.NameValue=t.NameValue AND p.DescValue=t.DescValue AND p.Ordinal=t.Ordinal WHERE p.LookupName={0}"
            target.Execute(updateSql.SqlFormatString(lookupName))
            If target.DoesTableExists(tempParcelDataLookup) Then
				target.DropTable(tempParcelDataLookup)
			End If
        End Sub

        Public Sub LoadFromDataRow(ByVal target As Data.Database, ByVal dr As System.Data.DataRow) Implements IDatabaseObject.LoadFromDataRow

        End Sub

    End Class
End Namespace

