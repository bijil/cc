﻿Imports System.Runtime.Serialization

Namespace RemoteIntegration.Objects
    <DataContract()> Public Class SketchObject
        Inherits ParcelObjectBase

        <DataMember()> Public Property sketchData As String
        <DataMember()> Public Property fileName As String

    End Class
End Namespace
