﻿Imports System.Runtime.Serialization
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects
Imports CAMACloud.BusinessLogic.RemoteIntegration

<DataContract()>
Public Class GIS_Layers
    <DataMember(Order:=1)> Public Property name As String
    <DataMember(Order:=2)> Public Property isParcelReferenced As String
    <DataMember(Order:=3)> Public Property parcelRelated As String
    <DataMember(Order:=4)> Public Property nameField As String
    <DataMember(Order:=5)> Public Property customField1 As String
    <DataMember(Order:=6)> Public Property customField2 As String
    <DataMember(Order:=7)> Public Property customField3 As String

    Public Sub SetLayerConfig(target As Data.Database)
        'target.Execute("UPDATE GIS_ObjectLayer SET ParcelRelated={0},NameField={1},CustomField1={2},CustomField2={3},CustomField3={4} WHERE Name={5}".SqlFormatString(parcelRelated, nameField, IIf(customField1 Is Nothing, "", customField1), IIf(customField2 Is Nothing, "", customField2), IIf(customField3 Is Nothing, "", customField3), name))
        Dim layerId = target.GetIntegerValue("SELECT Id FROM GIS_ObjectLayer WHERE Name={0}".SqlFormatString(name))
        ClearAllLayerData(target, layerId)
    End Sub

    Public Function IsParcelRelated(target As Data.Database, layerId As Integer) As Boolean
        parcelRelated = target.GetStringValue("SELECT ParcelRelated FROM GIS_ObjectLayer WHERE Id={0}".SqlFormatString(layerId))
        Select Case parcelRelated.Trim()
            Case "M", "N"
                Return False
            Case "Y"
                Return True
            Case Else
                Return False
        End Select
    End Function

    Public Sub ClearAllLayerData(target As Data.Database, layerId As Integer)
        GISTransferHelper.ClearAllObjects(target, layerId)
    End Sub
End Class
