﻿Imports System.Runtime.Serialization
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects
Imports CAMACloud.BusinessLogic.RemoteIntegration

<DataContract()>
Public Class GISParcel

    <DataMember(Order:=1)> Public Property parcelKeys As Dictionary(Of String, String)
    <DataMember(Order:=2)> Public Property gisPoints As GISPoint()
    <DataMember(Order:=3)> Public Property recordNumber As Integer
    <DataMember(Order:=4)> Public Property pointLabel As String
End Class