﻿Imports System.Runtime.Serialization
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects
Imports CAMACloud.BusinessLogic.RemoteIntegration

<DataContract()>
Public Class GISStreet

    <DataMember(Order:=1)> Public Property key As String
    <DataMember(Order:=2)> Public Property name As String
    <DataMember(Order:=3)> Public Property type As String
    <DataMember(Order:=4)> Public Property points As GISPoint()

End Class