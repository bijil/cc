﻿Namespace RemoteIntegration.Objects

    Public Class GISPoint
        Public Property latitude As Double
        Public Property longitude As Double
        Public Property ordinal As Integer
    End Class
End Namespace

