﻿Namespace RemoteIntegration.Objects
    Public Class AuditTrail
        Public Property eventDate As Date
        Public Property eventTime As DateTime
        Public Property eventCode As Integer
        Public Property source As String
        Public Property description As String
        Public Property isError As Boolean
        Public Property loginId As String
        Public Property iPAddress As String
        Public Property data As String
    End Class
End Namespace
