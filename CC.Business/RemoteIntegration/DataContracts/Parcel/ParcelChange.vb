﻿Imports CAMACloud.RemoteIntegration
Imports System.Runtime.Serialization
Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts

Namespace RemoteIntegration.Objects
    <DataContract()> Public Class ParcelChange

        <DataMember(Order:=1)> Public Property ParcelId As Integer
        <DataMember(Order:=2)> Public Property FieldId As Integer
        <DataMember(Order:=3)> Public Property TableName As String
        <DataMember(Order:=4)> Public Property AuxROWUID As Integer
        <DataMember(Order:=5)> Public Property ParentROWUID As Integer
        <DataMember(Order:=6)> Public Property ParentTableName As String
        <DataMember(Order:=7)> Public Property Keys As List(Of KeyValuePair(Of String, String))
        <DataMember(Order:=8)> Public Property TrackedFieldValues As List(Of TrackingField)	
        <DataMember(Order:=9)> Public Property NewValue As String
        <DataMember(Order:=10)> Public Property OldValue As String
        <DataMember(Order:=11)> Public Property ReviewedBy As String
        <DataMember(Order:=12)> Public Property ReviewTime As String
        <DataMember(Order:=13)> Public Property QCBy As String
        <DataMember(Order:=14)> Public Property QCTime As String
        <DataMember(Order:=15)> Public Property Action As String
        <DataMember(Order:=16)> Public Property Group As String
        <DataMember(Order:=17)> Public Property Year As String		
        <DataMember(Order:=18)> Public Property LinkedROWUID As Integer
        <DataMember(Order:=19)> Public Property LinkingrowuidPKs As List(Of KeyValuePair(Of String, String))
        <DataMember(Order:=20)> Public Property ChangeID As Integer


    End Class

End Namespace
