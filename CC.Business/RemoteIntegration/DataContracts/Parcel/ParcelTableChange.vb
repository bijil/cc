﻿Imports CAMACloud.RemoteIntegration
Imports System.Runtime.Serialization


Namespace RemoteIntegration.Objects
    <DataContract()> Public Class ParcelTableChange

        <DataMember(Order:=1)> Public Property TableId As Integer
        <DataMember(Order:=2)> Public Property TableName As String
        <DataMember(Order:=3)> Public Property ParcelId As Integer
        <DataMember(Order:=4)> Public Property ParentROWUID As Integer
        <DataMember(Order:=5)> Public Property AuxROWUID As Integer
        <DataMember(Order:=6)> Public Property Keys As List(Of KeyValuePair(Of String, String))
        <DataMember(Order:=7)> Public Property rowdata As ChangeField()
        <DataMember(Order:=8)> Public Property ReviewedBy As String
        <DataMember(Order:=9)> Public Property ReviewTime As String
        <DataMember(Order:=10)> Public Property QCBy As String
        <DataMember(Order:=11)> Public Property QCTime As String
        <DataMember(Order:=12)> Public Property Action As String
    End Class

End Namespace
