﻿Imports System.Runtime.Serialization
Imports CAMACloud.BusinessLogic.RemoteIntegration
Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts

<DataContract()> Public Class ParcelUpdator
    <DataMember(Order:=1)> Public Property keys As List(Of Dictionary(Of String, String))

    Public Function RevertParcelChanges(target As Data.Database, ByRef recordTracker As RemoteIntegration.DataContracts.RecordCountInfo) As List(Of Integer)

        Dim recordIndex As Integer = 0
        Dim conFlictList As New List(Of Integer)
        For Each parcelKeys As Dictionary(Of String, String) In keys
            Dim parcelId = _generateParcelFilterString(target, parcelKeys)
            If parcelId <> "" Then
                Dim updatedCount = Parcel.RollbackQCstatus(target, parcelId)
                Neighborhood.MarkAsNotCompleted(target, parcelId)
                'Entered to rejection log
                target.Execute("INSERT INTO ParcelRejectLog SELECT Id,ReviewedBy,ReviewDate,GETUTCDATE(),'','','API' FROM Parcel WHERE Id={0}".SqlFormatString(parcelId))
                If updatedCount > 0 Then
                    ProcessParcelDataChange.RevertChanges(target, parcelId)
                    ProcessParcelDataChange.RevertSketchChanges(target, parcelId)
                    ProcessParcelDataChange.DeleteChanges(target, parcelId)
                    ParcelAuditStream.CreateFlagEvent(target, DateTime.Now, parcelId, "API", "Rolled-back all changed using API Rollback.","API")
                    recordTracker.recordsUpdated += updatedCount
                End If
                recordIndex += 1
            Else
                recordTracker.recordsIgnored += 1
                conFlictList.Add(recordIndex)
            End If
        Next
        Return conFlictList
    End Function

    Private Function _generateParcelFilterString(target As Data.Database, keypairs As Dictionary(Of String, String)) As String

        Dim dataFilter = ""
        Dim i As Integer = 1
        For Each key In keypairs
            If dataFilter <> "" Then
                dataFilter += " AND "
            End If
            dataFilter += "KeyValue" + i.ToString() + "=" + key.Value.ToSqlValue
        Next

        Dim ParcelId = target.GetStringValue("SELECT Id FROM Parcel WHERE " + dataFilter + " AND Reviewed=1")
        Return ParcelId
    End Function

End Class
