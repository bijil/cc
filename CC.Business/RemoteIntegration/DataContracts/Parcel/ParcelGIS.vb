﻿Imports CAMACloud.RemoteIntegration
Imports System.Runtime.Serialization


Namespace RemoteIntegration.Objects
    Public Class ParcelGIS
        Implements IDatabaseObject

        Public Property parcelId As Integer
        Public Property mapPoints As GISPoint()

        Public Sub CreateIn(ByVal target As Data.Database, ByRef recordTracker As DataContracts.RecordCountInfo) Implements IDatabaseObject.CreateIn
            Dim sql = "DELETE FROM ParcelMapPoints WHERE ParcelId={0}"
            target.Execute(sql.SqlFormatString(parcelId))
            For Each mp As GISPoint In mapPoints
                Dim insertSql = "INSERT INTO ParcelMapPoints(ParcelId,Latitude,Longitude,Ordinal) VALUES ({0},{1},{2},{3}) "
                target.Execute(insertSql.SqlFormatString(parcelId, mp.latitude, mp.longitude, mp.ordinal))
                recordTracker.recordsInserted += 1
            Next
        End Sub

        Public Function doesParcelExist(ByVal target As Data.Database) As Boolean
            Dim sql = "SELECT COUNT(*) FROM Parcel WHERE Id={0}"
            If (target.GetIntegerValue(sql.SqlFormatString(parcelId.ToString())) > 0) Then
                Return True
            Else
                Return False
            End If

        End Function
        Public Sub LoadFromDataRow(ByVal target As Data.Database, ByVal dr As System.Data.DataRow) Implements IDatabaseObject.LoadFromDataRow

        End Sub
    End Class

End Namespace
