﻿Imports System.Net
Imports System.IO
Imports System.Web
Imports System.Text.RegularExpressions
Imports CAMACloud.BusinessLogic
Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts
Imports System.Runtime.Serialization

Namespace RemoteIntegration.Objects

    Public Class ParcelPhoto
        Public Property parcel As String
        Public Property ccImageId As String
        Public Property fileName As String
        Public Property photo As String
        Public Property uploadedBy As String
        Public Property captureDate As DateTime
        Public Property isDefault As Boolean
        Public Property referenceId As String
        <IgnoreDataMember()> Public Property failed As Boolean
        Public Property keys As List(Of KeyValuePair(Of String, String))
        Public Property metadata As List(Of KeyValuePair(Of String, String))
        Public Property downsyncedDate As DateTime
        Public Property newBPP As Boolean
        Public Property keyfield1 As String
    End Class
End Namespace

