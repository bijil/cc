﻿Imports System.Runtime.Serialization

<DataContract()> Public Class ParcelReject

    <DataMember(Order:=1)> Public Property keys As Dictionary(Of String, String)
    <DataMember(Order:=2)> Public Property reason As String
    <DataMember(Order:=3)> Public Property parcelId As String

End Class
