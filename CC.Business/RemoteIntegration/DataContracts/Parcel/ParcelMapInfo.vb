﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts
Imports System.Runtime.Serialization
Imports System.Collections.Specialized
Namespace RemoteIntegration.Objects

    <DataContract(NameSpace:="", Name:="CC_ParcelID")>
    Public Class ParcelMapInfo

        <DataMember()> Public Property parcelId As String
        <DataMember()> Public trackingfield As String
        <DataMember()> Public TrackedFieldValues As List(Of TrackingField)
        <DataMember()> Public parcelKeys As New KeyValuePair(Of String, String)

        <DataMember()> Public Property keys As List(Of KeyValuePair(Of String, String))
        Dim chk As StandardResponse
        Dim Failedcount As Integer
        Dim parcelcount As Integer
        Public Function GetParcel(ByVal target As Data.Database) As String
            Dim i As Integer = 0


            Dim datafilter As String = String.Empty
            Dim filterValues As String = String.Empty
            Dim value(keys.Count) As String
            For Each keyvalue In keys
                If (datafilter <> "") Then
                    datafilter += " AND "
                    i += 1
                End If
                If (filterValues <> "") Then
                    filterValues += ","
                End If

                datafilter += keyvalue.Key + " = " + "{" + i.ToString() + "}"
                filterValues += keyvalue.Value
                value(i) = keyvalue.Value.ToString()
            Next

            Dim sql = "SELECT CC_ParcelId FROM parceldata WHERE " + datafilter.FormatString(value)
            parcelId = target.GetStringValue(sql)
            Return parcelId
        End Function

    End Class

End Namespace
