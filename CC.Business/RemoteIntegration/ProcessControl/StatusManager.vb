﻿Namespace RemoteIntegration
    Public Class StatusManager

        Public Shared ReadOnly Property HostName As String
            Get
                Return Web.HttpContext.Current.Request.Url.Host
            End Get
        End Property

        Public Shared ReadOnly Property ClientIPAddress As String
            Get
                Return Web.HttpContext.Current.Request.ClientIPAddress
            End Get
        End Property

        Public Shared Sub RegisterAPIAction(dbName As String, action As String)
            Dim sql As String = "EXEC API_RegisterAction @HostName = {0}, @DBName = {1}, @ActionName = {2}, @ClientIPAddress = {3};".SqlFormat(False, HostName, dbName, action, ClientIPAddress)
            Database.System.Execute(sql)
        End Sub

        Public Shared Sub UnregisterAPIAction(dbName As String, action As String)
            Dim sql As String = "EXEC API_UnregisterAction @HostName = {0}, @DBName = {1}, @ActionName = {2}, @ClientIPAddress = {3};".SqlFormat(False, HostName, dbName, action, ClientIPAddress)
            Database.System.Execute(sql)
        End Sub

    End Class
End Namespace
