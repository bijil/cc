﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts

Public Class FinalizeNeighborhood
    Public Shared Function FinalizeTAData(ByVal db As Database, ByVal request As StandardRequest) As StandardResponse
        Try
            SystemAuditStream.CreateStandardEvent(db, FNL_90001_START)
            db.Execute("EXEC [ta_FinalizeNeighborhoods]")
            SystemAuditStream.CreateStandardEvent(db, FNL_90002_END)
            Return New StandardResponse
        Catch ex As Exception
            SystemAuditStream.CreateStandardEvent(db, ERR_9999)
            Return New StandardResponse("500", ex.Message)
        End Try
    End Function

    Public Shared Function FinalizeData(ByVal db As Database, ByVal request As StandardRequest) As StandardResponse
        Try
            db.Execute("EXEC [FinalizeNeighborhoods]")
            Return New StandardResponse
        Catch ex As Exception
            Return New StandardResponse("500", ex.Message)
        End Try
    End Function
End Class
