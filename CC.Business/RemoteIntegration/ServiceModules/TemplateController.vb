﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects
Namespace RemoteIntegration
    Public Class TemplateController

        Public Shared Function SetTemplate(db As Database, request As TemplateRequest) As StandardResponse
            Try
                TemplateControllerHelper.CreateTemplateParcelIfNotExists(db, request)
                Dim targetParentTable As String = "", targetChildTable As String = ""

                Dim tempParentTable As String = TemplateControllerHelper.DumpTableDataIntoTemp(db, request, request.dataTemplateParentTable, request.parentData, targetParentTable)
                Dim tempChildTable As String = TemplateControllerHelper.DumpTableDataIntoTemp(db, request, request.dataTemplateTable, request.templateData, targetChildTable)

                If Not request.appendTemplate Then
                    db.Execute("DELETE FROM " + targetChildTable + " WHERE CC_ParcelId = -99")
                    db.Execute("DELETE FROM " + targetParentTable + " WHERE CC_ParcelId = -99")
                End If

                Dim prs As Integer = 0
                Dim crs As Integer = 0

                TemplateControllerHelper.CopyTemplateData(db, tempParentTable, tempChildTable, targetParentTable, targetChildTable, request.parentKeys, request.templateParentKeys, request.parentFilterFields, prs, crs)

                Dim sr As New StandardResponse
                sr.AddMessage(prs & " templates created.")
                sr.AddMessage(crs & " template data records created.")
                Return sr
            Catch ex As Exception
                Return New StandardResponse(ex, request)
            End Try
        End Function

        Public Shared Function ClearTemplate(db As Database, request As TemplateRequest) As StandardResponse
            Dim targetParentTable As String = "", targetChildTable As String = ""
            targetParentTable = DataSource.FindTargetTable(db, request.dataTemplateParentTable)
            targetChildTable = DataSource.FindTargetTable(db, request.dataTemplateTable)

            db.Execute("DELETE FROM " + targetChildTable + " WHERE CC_ParcelId = -99")
            db.Execute("DELETE FROM " + targetParentTable + " WHERE CC_ParcelId = -99")

            Return New StandardResponse
        End Function

        Public Shared Function ClearAllTemplates(db As Database, request As TemplateRequest) As StandardResponse
            Return New StandardResponse
        End Function

    End Class
End Namespace
