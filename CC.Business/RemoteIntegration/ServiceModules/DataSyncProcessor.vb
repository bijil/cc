﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects
Imports System.IO

Namespace RemoteIntegration
    Public Class DataSyncProcessor

        Public Shared Function GetFieldList(ByVal db As Database, ByVal request As DataTransferRequest) As StandardResponse
            Dim dt As DataTable = db.GetDataTable("SELECT f.Id, TableId, f.AssignedName As Name, DataType, COALESCE(t.DestinationAlias, SourceTable) As SourceTable, t.Name As CloudAliasTable, IsCustomField, IsCalculated, DestinationNullReplacement, DoNotInsertOrUpdateAtDestination, f.DefaultValue, f.MaxLength, f.Precision FROM DataSourceField f INNER JOIN DataSourceTable t ON f.TableId = t.Id")
            dt.TableName = "FieldList"

            Dim resp As New StandardResponse
            resp.data = DataTools.JSON.Serialize(Of DataTable)(dt)
            Return resp
        End Function

        Shared Function GetStatusQuery(db As Database, data As DataTransferRequest) As StandardResponse
            Dim qcr As DataRow

            Dim qcMinDate, qcMaxDate As DateTime
            Dim parcelCount As Integer
            Dim changeCount As Integer
            If data.getAllChanges Then
                qcr = db.GetTopRow("SELECT MIN(QCDate) As QCMinDate, MAX(QCDate) As QCMaxDate, COUNT(*) As ParcelCount FROM Parcel WHERE QC = 1")
                qcMinDate = qcr.GetDate("QCMinDate")
                qcMaxDate = qcr.GetDate("QCMaxDate")
                parcelCount = db.GetIntegerValue("SELECT COUNT(DISTINCT ParcelId) FROM ParcelChanges pc INNER JOIN DataSourceField f ON pc.FieldId = f.Id INNER JOIN parcel p on p.Id=pc.ParcelId LEFT OUTER JOIN FieldCategory fc ON f.TableId = fc.SourceTableId AND pc.Action <> 'delete' LEFT OUTER JOIN FieldCategory pfc ON fc.ParentCategoryId = pfc.Id WHERE f.IsCustomField = 0 AND p.QC=1 AND pc.QCApproved = 1 AND pc.FieldId IS NOT NULL AND (pc.Action <> 'PhotoFlagMain' OR pc.Action IS NULL) AND p.FailedOnDownSync = 0 ")
                changeCount = db.GetIntegerValue("SELECT COUNT(*) FROM ParcelChanges pc INNER JOIN DataSourceField f ON pc.FieldId = f.Id INNER JOIN parcel p on p.Id=pc.ParcelId LEFT OUTER JOIN FieldCategory fc ON f.TableId = fc.SourceTableId AND pc.Action <> 'delete' LEFT OUTER JOIN FieldCategory pfc ON fc.ParentCategoryId = pfc.Id WHERE f.IsCustomField = 0 AND p.QC=1 AND pc.QCApproved = 1 AND pc.FieldId IS NOT NULL AND (pc.Action <> 'PhotoFlagMain' OR pc.Action IS NULL) AND p.FailedOnDownSync = 0 ")
            Else
                qcMinDate = data.periodStart
                qcMaxDate = data.periodEnd
                parcelCount = db.GetIntegerValue("SELECT COUNT(DISTINCT ParcelId) FROM ParcelChanges pc INNER JOIN DataSourceField f ON pc.FieldId = f.Id INNER JOIN parcel p on p.Id=pc.ParcelId LEFT OUTER JOIN FieldCategory fc ON f.TableId = fc.SourceTableId AND pc.Action <> 'delete' LEFT OUTER JOIN FieldCategory pfc ON fc.ParentCategoryId = pfc.Id WHERE f.IsCustomField = 0 AND p.QC=1 AND pc.QCApproved = 1 AND QCDate BETWEEN {0} AND {1} AND pc.FieldId IS NOT NULL AND (pc.Action <> 'PhotoFlagMain' OR pc.Action IS NULL) AND p.FailedOnDownSync = 0 ".SqlFormatString(qcMinDate.ToString("yyyy-MM-dd HH:mm:ss"), qcMaxDate.ToString("yyyy-MM-dd HH:mm:ss")))
                changeCount = db.GetIntegerValue("SELECT COUNT(*) FROM ParcelChanges pc INNER JOIN DataSourceField f ON pc.FieldId = f.Id INNER JOIN parcel p on p.Id=pc.ParcelId LEFT OUTER JOIN FieldCategory fc ON f.TableId = fc.SourceTableId AND pc.Action <> 'delete' LEFT OUTER JOIN FieldCategory pfc ON fc.ParentCategoryId = pfc.Id WHERE f.IsCustomField = 0 AND p.QC=1 AND pc.QCApproved = 1 AND QCDate BETWEEN {0} AND {1} AND pc.FieldId IS NOT NULL AND (pc.Action <> 'PhotoFlagMain' OR pc.Action IS NULL) AND p.FailedOnDownSync = 0 ".SqlFormatString(qcMinDate.ToString("yyyy-MM-dd HH:mm:ss"), qcMaxDate.ToString("yyyy-MM-dd HH:mm:ss")))
            End If

            If qcMinDate = Nothing Then
                qcMinDate = Date.Now.Date
            End If
            If qcMaxDate = Nothing Then
                qcMaxDate = Date.Now.Date
            End If
            If parcelCount = 0 And changeCount = 0 Then
                ParcelAuditStream.UpdateLastSyncDate(db, "LastDownSyncDate")
            End If
            Dim resp As New StandardResponse With {.data = New SyncStatusInfo With {.qcMinDate = qcMinDate, .qcMaxDate = qcMaxDate, .parcelCount = parcelCount, .changeCount = changeCount}}
            'resp = New StandardResponse With {.data = New SyncStatusInfo}
            Return resp
        End Function

        Public Shared Function GetAuxTableEditSequence(db As Database, data As StandardRequest) As StandardResponse
            Dim dt As DataTable = db.GetDataTable("SELECT MAX(Id)  As Ordinal, SourceTableName As TableName FROM FieldCategory WHERE SourceTableName IS NOT NULL GROUP BY SourceTableName ORDER BY 1")
            Dim ol As New Dictionary(Of Integer, String)
            For Each dr As DataRow In dt.Rows
                ol.Add(dr.GetInteger("Ordinal"), dr.GetString("TableName"))
            Next
            Return New StandardResponse With {.data = ol, .recordCount = ol.Count}
        End Function

    End Class
End Namespace