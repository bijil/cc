﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects
Imports System.IO
Imports CAMACloud.BusinessLogic.Installation

Namespace RemoteIntegration
    Public Enum BulkDataTransferType
        Reset = 1
        Refresh = 2
        Change = 3
        PhotoDownSync = 4
        Clean = 5
    End Enum

    Public Class DataTransfer
        Public Shared Property enableAutoPriority As Boolean
        Public Shared Property AutoPriorityEnabled() As Boolean
            Get
                Return enableAutoPriority
            End Get
            Set(ByVal value As Boolean)
                If value Then
                    enableAutoPriority = True
                Else
                    enableAutoPriority = False
                End If
            End Set
        End Property
        Public Shared Function SystemReset(ByVal db As Database, ByVal request As DataTransferRequest) As StandardResponse
            Dim orr As DataRow = Database.System.GetTopRow("SELECT * FROM Organization WHERE ID = " & request.OrganizationId)
            If orr.GetBoolean("PurgeLocked") Then
                Throw New Exceptions.BadRequestException("The account " + orr.GetString("Name").ToUpper + " has been purge-locked. Please contact the Account Administrators to unlock this to continue purging the database.", Nothing)
            End If


            If request.reason Is Nothing OrElse request.reason.Trim = "" Then
                Throw New Exceptions.BadRequestException("A valid reason is required.", Nothing)
            End If
            Try
                Dim recordTracker As New RecordCountInfo
                If request.backupPhotos Then
                    PhotoStorageHelper.BackupPhotoIndex(db)
                End If
                TransferJob.CancelAll(db, request)
                DataTransferHelper.ClearAllParcelData(db, recordTracker, False)
                SchemaTransferHelper.ClearCompleteSchema(db)
                UIDataSettingHelper.ClearGridViewFieldSettings(db)
                AccessManagementHelper.ClearAllUsers(request.accessKey, request.passKey)
                db.Application.ApplicationStatus = ApplicationStatus.Empty
                SystemAuditStream.CreateStandardEvent(db, DTX_40000_RESET)

                TransferNotification.SendPurgeNotification(request)

                Return New StandardResponse(200, "All data reset successfully.")
            Catch ex As Exception
                Return New StandardResponse(ex, request)
            End Try

        End Function

        Public Shared Function Initialize(ByVal type As BulkDataTransferType, ByVal db As Database, ByVal request As DataTransferRequest) As StandardResponse
            '       If Reset, the drop and re-create all data tables.
            '       Lock(System)
            '       Update Account Status
            '       Create Job
            Dim jobId As Integer
            Try
                Dim tableList As New List(Of Table)
                Dim recordTracker As New RecordCountInfo
                If db.Application.ApplicationStatus = ApplicationStatus.Empty Then
                    Return New StandardResponse(412, "Data transfer cannot be started when the account database is empty. Please create schema and relations before proceeding to this step.")
                End If

                SystemHelper.LockSystem(db, request)
                jobId = TransferJob.Create(db, request, TransferTypes.DataTransfer)
                'ClientSettings.AutoPriorityEnabled = True
                Dim value As Integer = db.GetIntegerValue("SELECT Value FROM ClientSettings WHERE Name = 'EnableAutoPriority'")
                If value = 1 Then
                    DataTransfer.AutoPriorityEnabled = True
                Else
                    DataTransfer.AutoPriorityEnabled = False
                End If
                If type = BulkDataTransferType.Reset Then
                    DataTransferHelper.ClearAllParcelData(db, recordTracker, True)

                ElseIf type = BulkDataTransferType.Change Then
                    Dim tables As Table()
                    request.ExtractData(request.data)
                    tables = DataTools.JSON.Deserialize(Of Table())(request.extractedData)
                    For Each t In tables
                        t.ChangeSchema(db, recordTracker)
                        tableList.Add(t)
                    Next
                    DataTransferHelper.PrepareTables(db)
                End If


                Dim aet As AuditEventType = Nothing
                Select Case type
                    Case BulkDataTransferType.Reset
                        aet = DTX_41000_INIT
                    Case BulkDataTransferType.Refresh
                        aet = DTX_42000_INIT
                    Case BulkDataTransferType.Change
                        aet = DTX_43000_INIT
                    Case BulkDataTransferType.Clean
                        aet = DTX_47000_INIT
                End Select

                SystemAuditStream.CreateStandardEvent(db, aet, jobId)
                StatusManager.RegisterAPIAction(db.Name, "transfer." + type.ToString.ToLower)

                If type = BulkDataTransferType.Change Then
                    Dim changeString = DataTools.JSON.Serialize(Of Table())(tableList.ToArray)
                    Return New StandardResponse() With {.data = "jobId=" & jobId, .header = DataTools.DataConversion.EncodeToBase64(changeString)}
                Else
                    Return New StandardResponse() With {.data = "jobId=" & jobId}
                End If
            Catch exj As Exceptions.PendingTransferException
                Dim sp = New StandardResponse(500)
                sp.AddMessage(exj.Message)
                sp.AddMessage("Pending job types in response.header")
                sp.AddMessage("Pending job ids in response.data")
                sp.data = exj.PendingJobIds
                sp.header = exj.PendingJobTypeString
                Return sp
            Catch ex As Exception

                SystemAuditStream.CreateErrorEvent(db, ex)
                Throw ex
            End Try
        End Function

        Public Shared Function ProcessDataPage(ByVal type As BulkDataTransferType, ByVal db As Database, ByVal request As DataTransferRequest) As StandardResponse
            '       Dump the database into temporary tables
            '       Perform transfer operations
            '       Drop temporary tables

            TransferJob.Verify(db, request, TransferTypes.DataTransfer)   'To be implemented

            If request.threadId = Nothing Then
                request.threadId = 0
            End If

            Dim aet As AuditEventType = Nothing
            Dim recordTracker As New RecordCountInfo
            Select Case type
                Case BulkDataTransferType.Reset
                    aet = DTX_41001_DATA
                Case BulkDataTransferType.Refresh
                    aet = DTX_42001_DATA
                Case BulkDataTransferType.Change
                    aet = DTX_43001_DATA
                Case BulkDataTransferType.Clean
                    aet = DTX_47001_DATA
            End Select

            '' Create audit trail for data transfer page start
            SystemAuditStream.CreateStandardEvent(db, aet, request.jobId, request.pageIndex)
            Dim lockName As String = DirectCast(request, StandardRequest).accessKey + "~" + DirectCast(request, StandardRequest).passKey + "~" + request.threadId.ToString
            Try
                SyncLock lockName
                    Dim ds As New DataSet
                    Dim buffer As Byte() = System.Text.Encoding.Default.GetBytes(request.ExtractData(request.data))
                    Dim ms As New IO.MemoryStream(buffer)
                    ds.ReadXml(ms)
                    ms.Close()

                    If request.tableNames IsNot Nothing Then
                        If ds.Tables.Count <> request.tableNames.Length Then
                            Throw New Exceptions.BadRequestException("The number of tableNames do not match with the dataSet contents.", Nothing)
                        End If
                        For i As Integer = 0 To request.tableNames.Length - 1
                            ds.Tables(i).TableName = request.tableNames(i)
                        Next
                    Else
                        Dim tNames As New List(Of String)
                        For Each dt As DataTable In ds.Tables
                            tNames.Add(dt.TableName)
                        Next
                        request.tableNames = tNames.ToArray
                    End If

                    ''' Log tablenames in Transfer Data for commit verification
                    ''' BEGIN LOG
                    Dim jobData As String = TransferJob.GetData(db, request.jobId)
                    Dim tableNamesInJob As List(Of String) = jobData.Split(",").ToList
                    Dim tableAdded As Boolean = False
                    For Each t In request.tableNames
                        Dim rt = t.ToLower.Trim
                        If Not tableNamesInJob.Contains(rt) Then
                            tableNamesInJob.Add(rt)
                            tableAdded = True
                        End If
                    Next
                    If tableAdded Then
                        tableNamesInJob.Sort()
                        Dim newData As String = tableNamesInJob.Aggregate(Function(x, y) x + "," + y).TrimStart(",")
                        TransferJob.SetData(db, request.jobId, newData)
                    End If
                    ''' END LOG
                    ''' 

                    DataTransferHelper.DumpAllTables(type, db, ds, request.threadId, recordTracker)

                    Dim FutureYearEnabled As Integer = db.GetIntegerValue("SELECT  value from clientsettings where name='FutureYearEnabled'")
                    If FutureYearEnabled = 1 Then
                        db.Execute("EXEC cc_ProcessFYLdataonRefresh")
                    End If
                    ''additional logics to  execute after datarefresh
                    db.Execute("EXEC API_ProcessAfterDataRefresh")


                    Select Case type
                        Case BulkDataTransferType.Reset
                            aet = DTX_41002_DTC
                        Case BulkDataTransferType.Refresh
                            aet = DTX_42002_DTC
                        Case BulkDataTransferType.Change
                            aet = DTX_43002_DTC
                        Case BulkDataTransferType.Clean
                            aet = DTX_47002_DTC
                    End Select

                    TransferJob.IncrementJobPage(db, request.jobId)
                    TransferJob.RegisterPageDetails(db, request, ds, recordTracker)

                    '' Create audit trail for data transfer page completion
                    SystemAuditStream.CreateStandardEvent(db, aet, request.jobId, request.pageIndex, recordTracker.tablesAffected, recordTracker.recordsUpdated + recordTracker.recordsInserted)
                End SyncLock

                'Return New StandardResponse With {.header = New DataTransferProcessInfo With {.tablesAffected = recordTracker.tablesAffected, .rowsAffected = recordTracker.rowsAffected, .parcelCreated = recordTracker.parcelCreated, .parcelModified = recordTracker.parcelModified}}
                Return New StandardResponse With {.header = New DataTransferProcessInfo With {
                        .tablesAffected = recordTracker.tablesAffected,
                        .rowsAffected = recordTracker.rowsAffected,
                        .parcelCreated = recordTracker.parcelCreated,
                        .parcelModified = recordTracker.parcelModified,
                        .recordsInserted = recordTracker.recordsInserted,
                        .recordsUpdated = recordTracker.recordsUpdated
                    }}

            Catch ex As Exception
                SystemAuditStream.CreateErrorEvent(db, ex)
                Throw ex
            End Try
        End Function

        Public Shared Function Commit(ByVal type As BulkDataTransferType, ByVal db As Database, ByVal request As DataTransferRequest) As StandardResponse

            Try
                Dim targetTables = DataSource.GetTargetTables(db)

                '       Update System Status
                '       Clean up system if required
                '       Unlock account is not set as DoNotUnlock
                Dim resp As New StandardResponse

                Dim jobData As String = TransferJob.GetData(db, request.jobId)
                Dim tableNamesInJob As List(Of String) = jobData.Split(",").ToList
                Dim parcelTableName As String = db.Application.ParcelTable


                Dim dnkLog As New Dictionary(Of String, Integer)

                If tableNamesInJob.Count > 0 Then
                    Dim dnkTables = db.GetDataTable("SELECT Name FROM DataSourceTable WHERE (DoNotKeepCondition IS NOT NULL AND DoNotKeepCondition <> '') AND Name IN ( " + String.Join(",", tableNamesInJob.Select(Function(x) x.ToSqlValue).ToArray) + ")").AsEnumerable.Select(Function(x) x.GetString("Name")).ToArray
                    For Each tname In dnkTables
                        Dim targetTable = targetTables(tname)
                        Dim sql As String
                        If tname = parcelTableName Then
                            sql = "EXEC cc_CleanParcelsDNK"
                        Else
                            sql = "EXEC cc_CleanTableDNK @SchemaTableName = {0}".SqlFormat(False, tname)
                        End If
                        Dim recordsDeleted As Integer = db.GetIntegerValue(sql)
                        dnkLog.Add(tname, recordsDeleted)
                    Next
                End If

                If type = BulkDataTransferType.Refresh Then
                    If tableNamesInJob.Count > 0 Then
                        Dim deferedTables = db.GetDataTable("SELECT Name FROM DataSourceTable WHERE  AllowOrphanInsert = 1 AND Name IN ( " + String.Join(",", tableNamesInJob.Select(Function(x) x.ToSqlValue).ToArray) + ")").AsEnumerable.Select(Function(x) x.GetString("Name")).ToArray
                        For Each tname In deferedTables
                            DataTransferHelper.bindOrphanRecords(type, db, tname)

                        Next
                    End If

                End If

                If Not request.commitOnly Then
                    DataTransferHelper.CleanTable(type, db, request)
                Else

                End If

				If (type = BulkDataTransferType.Refresh)  Or (type = BulkDataTransferType.Refresh)Then
                    TransferJob.AutoPriorityUpdate(db, request, TransferTypes.DataTransfer)
                    Try
                        TransferJob.ProcessMAParcelComparable(db, request, TransferTypes.DataTransfer)
                    Catch ex As Exception

                    End Try

                End If


                TransferJob.Commit(db, request, TransferTypes.DataTransfer)
                db.Application.ApplicationStatus = ApplicationStatus.DataLoaded
                Dim aet As AuditEventType = Nothing
                Select Case type
                    Case BulkDataTransferType.Reset
                        aet = DTX_41010_COMMIT
                    Case BulkDataTransferType.Refresh
                        aet = DTX_42010_COMMIT
                    Case BulkDataTransferType.Change
                        aet = DTX_43010_COMMIT
                    Case BulkDataTransferType.Clean
                        aet = DTX_47010_COMMIT
                End Select
                SystemAuditStream.CreateStandardEvent(db, aet, request.jobId)

                If Not request.doNotUnlock Then
                    SystemHelper.UnlockSystem(db, request)
                End If



                If type = BulkDataTransferType.Reset Then
                    Database.System.Execute("UPDATE Organization SET PurgeLocked = 1 WHERE Id = " & request.OrganizationId)
                End If
                ParcelAuditStream.UpdateLastSyncDate(db, "LastUpsyncDate")

                StatusManager.UnregisterAPIAction(db.Name, "transfer." + type.ToString.ToLower)

                TransferNotification.SendCommitNotification(type, request)
                Return resp
            Catch ex As Exception
                Return New StandardResponse(ex, request)
            End Try
        End Function

        Public Shared Function Cancel(ByVal type As BulkDataTransferType, ByVal db As Database, ByVal request As DataTransferRequest) As StandardResponse
            '       Update System Status
            '       Clean up system if required
            '       Unlock account is not set as DoNotUnlock
            TransferJob.Cancel(db, request, TransferTypes.DataTransfer)
            db.Application.ApplicationStatus = ApplicationStatus.SchemaCreated
            If Not request.doNotUnlock Then
                SystemHelper.UnlockSystem(db, request)
            End If
            Dim aet As AuditEventType = Nothing
            Select Case type
                Case BulkDataTransferType.Reset
                    aet = DTX_41009_CANCEL
                Case BulkDataTransferType.Refresh
                    aet = DTX_42009_CANCEL
                Case BulkDataTransferType.Change
                    aet = DTX_43009_CANCEL
                Case BulkDataTransferType.Clean
                    aet = DTX_47009_CANCEL
            End Select
            SystemAuditStream.CreateStandardEvent(db, aet, request.jobId)
            StatusManager.UnregisterAPIAction(db.Name, "transfer." + type.ToString.ToLower)
            Return New StandardResponse()
        End Function

        Public Shared Function CancelAllJobs(ByVal db As Database, ByVal request As DataTransferRequest) As StandardResponse
            '       Update System Status
            '       Clean up system if required
            '       Unlock account is not set as DoNotUnlock
            TransferJob.CancelAll(db, request)
            db.Application.ApplicationStatus = ApplicationStatus.SchemaCreated
            If Not request.doNotUnlock Then
                SystemHelper.UnlockSystem(db, request)
            End If
            SystemAuditStream.CreateStandardEvent(db, DTX_40009_CANCEL, request.jobId)
            Return New StandardResponse()
        End Function

        Public Shared Function CleanTable(ByVal type As BulkDataTransferType, ByVal db As Database, ByVal request As DataTransferRequest) As StandardResponse
            If type <> BulkDataTransferType.Clean Then
                Throw New Exceptions.BadRequestException("Invalid operation request.", Nothing, False)
            End If
            Dim resp = DataTransferHelper.CleanTable(type, db, request)

            Return resp
        End Function

        Public Shared Function GetSequenceOfExport(ByVal db As Database, ByVal request As DataTransferRequest) As StandardResponse
            Dim relatedTables = DataTransferHelper.GetRelatedTables(db)
            Dim jsonString = DataTools.JSON.Serialize(Of List(Of String))(relatedTables)
            Dim sr As New StandardResponse() With {.data = DataTools.DataConversion.EncodeToBase64(jsonString), .recordCount = relatedTables.Count}
            Return sr
        End Function

        Public Shared Function RunScriptOnData(db As Database, data As DataTransferRequest) As StandardResponse
            Try
                db.Execute("EXEC cc_RunScript @Action = " + data.scriptAction.ToSqlValue)
                Return New StandardResponse
            Catch exs As System.Data.SqlClient.SqlException
                Return New StandardResponse(exs, data)
            Catch ex As Exception
                Return New StandardResponse(ex, data)
            End Try
        End Function

        'Method for API update/transfer/downsync
        Public Shared Function DownsyncChanges(ByVal db As Database, ByVal request As DataTransferRequest) As StandardResponse
            Dim changeList As New List(Of ParcelChange)
            Dim updateRequest As New UpdateTransferRequest
            Dim sp As New StandardResponse
            Dim clock As New ProcessClock()

            clock.StartClock()
            Try
                SystemAuditStream.CreateStandardEvent(db, DTX_44002_DATA)
                changeList = UpdateTransferHelper.DownloadParcelChanges(db, request, request.periodStart, request.periodEnd)
                SystemAuditStream.CreateStandardEvent(db, DTX_44003_DATA)
            Catch nex As NullReferenceException
                Return New StandardResponse(nex, request, 1)
            Catch ex As Exception
                Return New StandardResponse(400, ex.Message)
            End Try

            clock.EndClock()
            ParcelAuditStream.UpdateLastSyncDate(db, "LastDownSyncDate")
            Dim changeString = DataTools.JSON.Serialize(Of List(Of ParcelChange))(changeList)
            
            Dim lineBreakMode As Integer = db.GetIntegerValue("SELECT  value from clientsettings where name='lineBreakMode'")
            If lineBreakMode <> 0 Then
            	changeString=LineBreakConversion(changeString,lineBreakMode)
            End If
           
            Return New StandardResponse With {
                                             .header = New UpdateTransferDownSyncResponse With
                                               {
                                                .processTime = clock.processTime,
                                                .periodStart = request.periodStart,
                                                .periodEnd = request.periodEnd,
                                                .nextQCStartDate = DataSyncProcessorHelper.GetNextQCStartDate(db, request)
                                                },
                                             .data = DataTools.DataConversion.EncodeToBase64(changeString), .recordCount = changeList.Count
                                               }
        End Function
        'Method for API update/transfer/downsync2

        Public Shared Function DownloadParcelChanges(ByVal db As Database, ByVal request As DataTransferRequest) As StandardResponse
            Dim changeList As New List(Of ParcelTableChange)
            Dim updateRequest As New UpdateTransferRequest
            Dim sp As New StandardResponse
            Dim clock As New ProcessClock()

            clock.StartClock()
            Try
                changeList = ParcelTransferHelper.DownloadParcel(db, request.periodStart, request.periodEnd)

            Catch nex As NullReferenceException
                Return New StandardResponse(nex, request, 1)
            Catch ex As Exception
                Return New StandardResponse(400, ex.Message)
            End Try

            clock.EndClock()
            Dim changeString = DataTools.JSON.Serialize(Of List(Of ParcelTableChange))(changeList)
            Return New StandardResponse With {
                                             .header = New UpdateTransferDownSyncResponse With
                                                       {
                                                         .periodStart = request.periodStart,
                                                         .periodEnd = request.periodEnd,
                                                         .processTime = clock.processTime
                                                       },
                                             .data = DataTools.DataConversion.EncodeToBase64(changeString), .recordCount = changeList.Count
                                                       }

        End Function

        Public Shared Function UpsyncChanges(ByVal db As Database, ByVal request As DataTransferRequest) As StandardResponse
            Try
                Dim logFile As String = "", logCreated As Boolean

                Dim lockName As String = DirectCast(request, StandardRequest).accessKey + "~" + DirectCast(request, StandardRequest).passKey

                Dim changeList As New List(Of ParcelChange)
                Dim conflicts As New List(Of Integer)
                Dim updateRequest As New UpdateTransferRequest
                Dim recordTracker As New RecordCountInfo
                Dim clock As New ProcessClock

                clock.StartClock()
                request.ExtractData(request.data)

                If Configuration.ConfigurationManager.AppSettings("DetailedLog") = "true" Then
                    logFile = IO.Path.Combine(ClientSettings.ErrorLogRoot, "data\upsync\" + Now.ToString("yyyy-MM") + "\" + Now.ToString("dd\HHH") + "\" + request.accessKey + Now.ToString("-mm-ss-fff") + ".txt")
                    If Not Directory.Exists(Path.GetDirectoryName(logFile)) Then
                        Directory.CreateDirectory(Path.GetDirectoryName(logFile))
                    End If
                    IO.File.WriteAllText(logFile, request.data)
                    logCreated = True
                End If
                ParcelAuditStream.UpdateLastSyncDate(db, "LastUpsyncDate")
                If request.dataFormat <> Nothing Then
                    Try
                        SyncLock lockName
                            Select Case request.dataFormat.ToLower
                                Case "json"
                                    SystemAuditStream.CreateStandardEvent(db, DTX_44000_DATA)
                                    Dim changes = DataTools.JSON.Deserialize(Of ParcelChange())(request.extractedData)
                                    conflicts = UpdateTransferHelper.UpdateParcelChanges(db, changes, recordTracker, request.overwriteConflicts)
                                    SystemAuditStream.CreateStandardEvent(db, DTX_44001_DATA)
                            End Select
                        End SyncLock
                        Dim isRowExist As Integer = db.GetIntegerValue("SELECT count(*) FROM  ClientSettings where name ='patchPhotoMetadata'")
                        If isRowExist > 0 Then
                            If db.GetIntegerValue("SELECT COUNT(*) FROM DataSourceField WHERE SourceTable ='_photo_meta_data' AND DoNotShowOnDE = 0 ") > 0 Then
                                db.Execute("EXEC ccad_FillMetadataMissing")
                            End If
                        End If
                        'If logCreated Then
                        '    IO.File.Delete(logFile)
                        'End If
                    Catch nex As NullReferenceException
                        Return New StandardResponse(nex, request, 1)
                    Catch ex As Exception
                        Return New StandardResponse(ex, request)
                    End Try
                Else
                    Return New StandardResponse(406)
                End If
                clock.EndClock()
                Dim conflictString As String = DataTools.JSON.Serialize(Of List(Of Integer))(conflicts)
                Dim sp As New StandardResponse With {.data = conflictString, .header = New UpdateTransferUpSyncResponse With
                                                                               {
                                                                                   .tablesAffected = recordTracker.tablesAffected,
                                                                                   .failedCount = recordTracker.recordsIgnored,
                                                                                   .recordsAffected = recordTracker.recordsUpdated,
                                                                                   .processTime = clock.processTime
                                                                               }
                                                                               }
                Return sp
            Catch ex As Exception
                Return New StandardResponse(400, ex.Message)
            End Try
        End Function

        'For Reject ParcelChanges
        'Set Review=false
        'delete all changes

        Public Shared Function RollBackParcelChanges(ByVal db As Database, ByVal request As StandardRequest) As StandardResponse

            request.ExtractData(request.data)
            Dim recordTracker As New RecordCountInfo
            Dim parcelUpdator As New ParcelUpdator
            Dim conflicts As List(Of Integer)
            Try
                SystemAuditStream.CreateStandardEvent(db, DTX_46000_REVERT)
                parcelUpdator.keys = DataTools.JSON.Deserialize(Of List(Of Dictionary(Of String, String)))(request.extractedData)
                conflicts = parcelUpdator.RevertParcelChanges(db, recordTracker)
                SystemAuditStream.CreateStandardEvent(db, DTX_46001_REVERT)
            Catch nex As NullReferenceException
                Return New StandardResponse(nex, request, 1)
            Catch ex As Exception
                Return New StandardResponse(ex, request)
            End Try

            Dim conflictString = DataTools.JSON.Serialize(Of List(Of Integer))(conflicts)
            Return New StandardResponse With {.data = conflictString, .header = New ParcelRollBackResponse With {.parcelsAffected = recordTracker.recordsUpdated, .failedCount = recordTracker.recordsIgnored}}
        End Function

        ' transfer/update/reject
        Public Shared Function RejectParcelChanges(ByVal db As Database, ByVal request As StandardRequest) As StandardResponse

            request.ExtractData(request.data)
            Dim recordTracker As New RecordCountInfo
            Dim parcelRejected As New ParcelReject
            Dim conflicts As List(Of Integer)
            Try

                Dim parcelRejectList = DataTools.JSON.Deserialize(Of List(Of ParcelReject))(request.extractedData)
                conflicts = ParcelDeleteHelper.MarkParcelAsRejected(db, parcelRejectList, recordTracker)


            Catch nex As NullReferenceException
                Return New StandardResponse(nex, request, 1)
            Catch ex As Exception
                Return New StandardResponse(ex, request)
            End Try

            Dim conflictString = DataTools.JSON.Serialize(Of List(Of Integer))(conflicts)
            Return New StandardResponse With {.data = conflictString, .header = New ParcelRejectResponse With {.parcelsAffected = recordTracker.recordsUpdated, .failedCount = recordTracker.recordsIgnored}}

        End Function

        ' transfer/update/defer
        Public Shared Function DeferParcelChanges(ByVal db As Database, ByVal request As StandardRequest) As StandardResponse

            request.ExtractData(request.data)
            Dim recordTracker As New RecordCountInfo
            Dim parcelDeferList As New ParcelReject
            Dim conflicts As List(Of Integer)
            Try

                Dim parcelRejectList = DataTools.JSON.Deserialize(Of List(Of ParcelReject))(request.extractedData)
                conflicts = ParcelDeleteHelper.MarkParcelAsDeferred(db, parcelRejectList, recordTracker)


            Catch nex As NullReferenceException
                Return New StandardResponse(nex, request, 1)
            Catch ex As Exception
                Return New StandardResponse(ex, request)
            End Try

            Dim conflictString = DataTools.JSON.Serialize(Of List(Of Integer))(conflicts)
            Return New StandardResponse With {.data = conflictString, .header = New ParcelRejectResponse With {.parcelsAffected = recordTracker.recordsUpdated, .failedCount = recordTracker.recordsIgnored}}

        End Function


        'Public Shared Function UploadGIS(ByVal db As Database, ByVal request As StandardRequest) As StandardResponse
        '    request.ExtractData(request.data)
        '    If request.dataFormat <> Nothing Then
        '        If request.dataFormat.ToLower = "json" Then
        '            Dim parcels As ParcelGIS()
        '            Dim recordTracker As New RecordCountInfo
        '            Try
        '                parcels = DataTools.JSON.Deserialize(Of ParcelGIS())(request.extractedData)
        '                For Each p In parcels
        '                    If p.doesParcelExist(db) Then
        '                        p.CreateIn(db, recordTracker)
        '                        recordTracker.parcelCreated += 1
        '                    Else
        '                        recordTracker.recordsIgnored += 1
        '                    End If

        '                Next
        '            Catch nex As NullReferenceException
        '                Return New StandardResponse(nex, request, 1)
        '            Catch ex As Exception
        '                Return New StandardResponse(ex, request)
        '            End Try

        '            Return New StandardResponse With {.header = New GISResponse With
        '                                                     {
        '                                                         .failedCount = recordTracker.recordsIgnored,
        '                                                         .parcelAffected = recordTracker.parcelCreated,
        '                                                         .rowsAffected = recordTracker.recordsInserted
        '                                                    }
        '                                              }

        '        Else
        '            Return New StandardResponse(406)
        '        End If
        '    Else
        '        Return New StandardResponse(406)
        '    End If
        'End Function

        'Public Shared Function UploadShapeData(ByVal db As Database, ByVal request As StandardRequest) As StandardResponse

        '    request.ExtractData(request.data)
        '    If request.dataFormat <> Nothing Then
        '        If request.dataFormat.ToLower = "json" Then
        '            Dim parcels As GISRequest()
        '            Dim recordIndex = 0
        '            Dim conFlicted As New List(Of Integer)
        '            Dim recordTracker As New RecordCountInfo
        '            Try
        '                parcels = DataTools.JSON.Deserialize(Of GISRequest())(request.extractedData)
        '                For Each p In parcels
        '                    If (p.CreateIn(db, recordTracker)) Then
        '                        recordTracker.parcelCreated += 1
        '                    Else
        '                        conFlicted.Add(recordIndex)
        '                    End If
        '                    recordIndex += 1
        '                Next
        '            Catch nex As NullReferenceException
        '                Return New StandardResponse(nex, request, 1)
        '            Catch ex As Exception
        '                Return New StandardResponse(ex, request)
        '            End Try

        '            Return New StandardResponse With {.header = New GISResponse With
        '                                                     {
        '                                                         .failedCount = recordTracker.recordsIgnored,
        '                                                         .parcelAffected = recordTracker.parcelCreated,
        '                                                         .rowsAffected = recordTracker.recordsInserted
        '                                                    },
        '                                                   .data = DataTools.JSON.Serialize(Of List(Of Integer))(conFlicted)}



        '        Else
        '            Return New StandardResponse(406)
        '        End If
        '    Else
        '        Return New StandardResponse(406)
        '    End If
        'End Function

        Public Shared Function GetLayerConfiguration(ByVal db As Database, ByVal request As StandardRequest) As StandardResponse

            Dim dt As New DataTable
            If request.dataFormat <> Nothing Then
                If request.dataFormat.ToLower = "json" Then
                    Try
                        dt = GISTransferHelper.GetLayerConfig(db)
                        dt.TableName = "GIS_ObjectTypes"
                    Catch nex As NullReferenceException
                        Return New StandardResponse(nex, request, 1)
                    Catch ex As Exception
                        Return New StandardResponse(ex, request)
                    End Try
                    Return New StandardResponse With {.data = DataTools.JSON.Serialize(Of DataTable)(dt)}
                Else
                    Return New StandardResponse(406)
                End If
            Else
                Return New StandardResponse(406)
            End If

        End Function

        Public Shared Function SetLayerConfiguration(ByVal db As Database, ByVal request As StandardRequest) As StandardResponse

            request.ExtractData(request.data)
            Dim dt As New DataTable
            If request.dataFormat <> Nothing Then
                If request.dataFormat.ToLower = "json" Then
                    Try
                        Dim gisLayers As GIS_Layers = DataTools.JSON.Deserialize(Of GIS_Layers)(request.extractedData)
                        gisLayers.SetLayerConfig(db)
                    Catch nex As NullReferenceException
                        Return New StandardResponse(nex, request, 1)
                    Catch ex As Exception
                        Return New StandardResponse(ex, request)
                    End Try
                    Return New StandardResponse
                Else
                    Return New StandardResponse(406)
                End If
            Else
                Return New StandardResponse(406)
            End If

        End Function

        'Public Shared Function ExtractShapeLayers(ByVal db As Database, ByVal request As StandardRequest) As StandardResponse

        '    Dim recordTracker As New RecordCountInfo
        '    request.ExtractData(request.data)
        '    Dim gisObjectPoints As New List(Of GIS_ObjectPoints)
        '    If request.dataFormat <> Nothing Then
        '        If request.dataFormat.ToLower = "json" Then
        '            Try
        '                gisObjectPoints = DataTools.JSON.Deserialize(Of List(Of GIS_ObjectPoints))(request.extractedData)
        '                GIS_ObjectPoints.CreatedIn(db, gisObjectPoints, recordTracker)
        '            Catch nex As NullReferenceException
        '                Return New StandardResponse(nex, request, 1)
        '            Catch ex As Exception
        '                Return New StandardResponse(ex, request)
        '            End Try
        '            Return New StandardResponse With {.header = New GISResponse With
        '                                                      {
        '                                                          .failedCount = recordTracker.recordsIgnored,
        '                                                          .parcelAffected = recordTracker.parcelCreated,
        '                                                          .rowsAffected = recordTracker.recordsInserted
        '                                                     }
        '                                             }
        '        Else
        '            Return New StandardResponse(406)
        '        End If
        '    Else
        '        Return New StandardResponse(406)
        '    End If

        'End Function

        Public Shared Function UploadLookUp(ByVal db As Database, ByVal request As StandardRequest) As StandardResponse

            request.ExtractData(request.data)
            If request.dataFormat <> Nothing Then
                If request.dataFormat.ToLower = "json" Then
                    Dim lookups As LookUpTable()
                    Dim recordTracker As New RecordCountInfo
                    Try
                        lookups = DataTools.JSON.Deserialize(Of LookUpTable())(request.extractedData)
                        Dim aet As AuditEventType = Nothing
                        aet = DTX_45000_START
                        SystemAuditStream.CreateStandardEvent(db, aet)
                        For Each p In lookups
                            p.CreateIn(db, recordTracker)
                            recordTracker.lookUpAffected += 1
                        Next
                        aet = DTX_45001_DTC
                        SystemAuditStream.CreateStandardEvent(db, aet, recordTracker.lookUpAffected, recordTracker.recordsInserted)
                    Catch nex As NullReferenceException
                        Return New StandardResponse(nex, request, 1)
                    Catch ex As Exception
                        Return New StandardResponse(ex, request)
                    End Try

                    Return New StandardResponse With {.header = New LookupProcessInfo With
                                                                {
                                                                    .failedCount = recordTracker.recordsIgnored,
                                                                    .lookupsAffected = recordTracker.lookUpAffected,
                                                                    .valuesInserted = recordTracker.recordsInserted
                                                                }
                                                                }
                Else
                    Return New StandardResponse(406)
                End If
            Else
                Return New StandardResponse(406)
            End If

        End Function

        Public Shared Function GetActiveJobs(db As Database, data As StandardRequest) As StandardResponse
            Dim sp As New StandardResponse
            Try
                Dim jobs = TransferJob.GetActiveJobs(db)
                sp.data = jobs
                sp.recordCount = jobs.Count
            Catch ex As Exception
                sp = New StandardResponse(ex, data)
            End Try
            Return sp
        End Function

        Public Shared Function TestUpdateSql(db As Database, data As StandardRequest) As StandardResponse
            Dim sp As New StandardResponse
            Try
                Dim tables = data.data.Split(",")
                Dim massSql = ""
                For Each t In tables
                    If t.Trim <> "" Then
                        Dim sql = DataTransferHelper._getParcelUpdateSql(db, t, "") + " WHERE t0.CC_ParcelId IS NULL;" + vbNewLine
                        massSql += sql
                    End If
                Next

                Dim bytes = System.Text.Encoding.Default.GetBytes(massSql)
                sp.data = Convert.ToBase64String(bytes)
            Catch ex As Exception
                Throw New Exception(ex.StackTrace)
            End Try

            Return sp
        End Function
        Public Shared Function DownloadLookUp(ByVal target As Data.Database, ByVal data As StandardRequest) As StandardResponse
            Dim sp As New StandardResponse()
            Try
                Dim doc = CAMACloud.BusinessLogic.ParcelDataLookup.ExportLookupData(target)
                sp.data = DataTools.DataConversion.EncodeToBase64(doc.ToString)
            Catch ex As Exception
                sp = New StandardResponse(ex, data)
            End Try

            Return sp
        End Function
        Public Shared Function GetAllsettings(ByVal target As Data.Database, ByVal data As StandardRequest) As StandardResponse
            Dim sp As New StandardResponse()
            Try
                Dim gen = New EnvironmentConfiguration
                Dim doc = gen.ExportSettings(ConfigFileType.All, target)
                sp.data = DataTools.DataConversion.EncodeToBase64(doc.ToString)
            Catch ex As Exception
                sp = New StandardResponse(ex, data)
            End Try
            Return sp
        End Function
        Public Shared Function SetAllsettings(ByVal target As Data.Database, ByVal data As StandardRequest) As StandardResponse
            '  target.Execute("insert into test_abc values(" + data.data.ToString().ToSqlValue + ")")
            data.ExtractData(data.data)
            Dim settings As String = data.extractedData
            Dim gen = New EnvironmentConfiguration
            '  target.Execute("insert into test_abc values(" + settings.ToSqlValue + ")")
            gen.ImportSettings(settings, BusinessLogic.Installation.ConfigFileType.All, target, "API", "IMPORT","", data.OrganizationId)
            Return New StandardResponse
        End Function
        
         Public Shared Function LineBreakConversion(ByVal str As String, ByVal lineBreakMode As integer) As String
	         Select Case lineBreakMode
	                                Case 1
	                                        Return DataTools.JSON.UnixStr(str)
	                      		 	 Case 2
	                      		 	 	Return DataTools.JSON.Windowsstr(str)
	                      		 	 Case 3
	                      		 	 	Return DataTools.JSON.NoLinebreakstr(str)
	                      		 	 Case 4
	                      		 	 	Return DataTools.JSON.Windowsstrnew(str)
	                      		 	 Case 5
	                      		 	 	Return DataTools.JSON.UnixStrnew(str)
	                      		 	 Case 6
	                      		 	 	Return DataTools.JSON.NoLinebreakstrnew(str)	
	                      		 	 	
	         End Select
            Return str
        End Function
            

    End Class
End Namespace
