﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects
Namespace RemoteIntegration
    Public Class AccountManager

        Public Shared Function GetAuditTrail(ByVal db As Database, ByVal request As SystemRequest) As StandardResponse

            Dim auditList As New List(Of AuditTrail)
            Try
                auditList = AuditTrailHelper.GetAuditTrail(db, request.startTime, request.endTime)
            Catch nex As NullReferenceException
                Return New StandardResponse(nex, request, 1)
            Catch ex As Exception
                Return New StandardResponse(ex, request)
            End Try

                    Dim sp As New StandardResponse() With {.data = New AuditTrailResponse With {.auditInfo = auditList.ToArray}, .recordCount = auditList.Count}
                    Return sp
        End Function

        Public Shared Function GetSystemQuery(db As Database, request As StandardRequest) As StandardResponse
            Dim tables = DataTransferHelper.GetRelatedTables(db)
            Dim dict As New Dictionary(Of String, Integer)
            For Each t In tables
                Dim tt = DataSource.FindTargetTable(db, t)
                If db.DoesTableExists(tt) Then
                    Dim tc = db.GetIntegerValue("SELECT COUNT(*) FROM " + tt)
                    dict.Add(t, tc)
                End If
            Next
            Return New StandardResponse With {.data = dict, .description = db.Application.ApplicationStatus.ToString}
        End Function


    End Class
End Namespace
