﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts

Namespace RemoteIntegration
    Public Class GroupProcessor

        Public Shared Function DeleteEmptyGroups(ByVal db As Database, ByVal request As StandardRequest) As StandardResponse
            Neighborhood.DeleteAllEmptyGroups(db)
            Return New StandardResponse
        End Function

    End Class

End Namespace

