﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects
Namespace RemoteIntegration

    Public Class ParcelTransfer
        Public Shared Function GetParcel(ByVal db As Database, ByVal request As ParcelRequest) As StandardResponse
            Dim parcel As New ParcelMapInfo()
            Dim parcelList As New List(Of ParcelMapInfo)
            Try
                For Each parcelItems In request.parcels
                    parcel.keys = parcelItems.keys
                    parcelItems.parcelId = parcel.GetParcel(db)
                    If (parcelItems.parcelId <> Nothing) Then
                        parcelList.Add(parcelItems)
                    End If
                Next
            Catch nex As NullReferenceException
                Return New StandardResponse(400, 1, Nothing)
            Catch ex As Exception
                Return New StandardResponse(ex, request)
            End Try

            Dim parcelString = DataTools.JSON.Serialize(Of List(Of ParcelMapInfo))(parcelList)
            Dim sp As New StandardResponse() With {.data = DataTools.DataConversion.EncodeToBase64(parcelString), .recordCount = parcelList.Count}
            Return sp

        End Function

        Public Shared Function ExecuteParcelCommit(ByVal db As Database, ByVal request As ParcelRequest) As StandardResponse

            Dim affectedParcelList As New List(Of CommittedParcel)
            Try
                For Each p As ParcelMapInfo In request.parcels
                    Dim pciDeleted As Integer = Parcel.CommitChanges(db, p.parcelId)
                    If ParcelEventLog.Enabled Then
                        ParcelEventLog.CreateFlagEvent(db, ParcelEventLog.EventType.Committed, ParcelEventLog.ApplicationType.API, p.parcelId)
                    End If

                    If request.deleteNewRecords Then
                        Parcel.DeleteAllNewRecords(db, p.parcelId)
                    End If
                    If request.partial Then
                        ParcelAuditStream.CreateFlagEvent(db, DateTime.UtcNow, p.parcelId, "API", "Selected changes committed via API (" & pciDeleted & " records)", "API")
                    Else
                        ParcelAuditStream.CreateFlagEvent(db, DateTime.UtcNow, p.parcelId, "API", "Parcel changes committed via API (" & pciDeleted & " records)", "API")
                    End If
                    db.Execute("INSERT INTO ParcelCommitLog (ParcelId, IsPartial, CommitTime) VALUES ({0},{1},{2})".SqlFormatString(p.parcelId, request.partial, DateTime.UtcNow))
                    affectedParcelList.Add(New CommittedParcel With {.parcelId = p.parcelId, .recordsAffected = pciDeleted})
                Next
                ParcelAuditStream.UpdateLastSyncDate(db, "LastDownSyncDate")
            Catch nex As NullReferenceException
                Return New StandardResponse(400, 1, Nothing)
            Catch ex As Exception
                Return New StandardResponse(ex, request)
            End Try

            Dim parcelString = DataTools.JSON.Serialize(Of List(Of CommittedParcel))(affectedParcelList)
            Dim sp As New StandardResponse() With {.data = DataTools.DataConversion.EncodeToBase64(parcelString), .recordCount = affectedParcelList.Count}
            Return sp

        End Function


        Public Shared Function ExecuteTrackedValueCommit(ByVal db As Database, ByVal request As ParcelRequest) As StandardResponse

            Dim affectedParcelList As New List(Of CommittedParcel)
            Try
                For Each p As ParcelMapInfo In request.parcels
                    Dim pciDeleted As Integer = Parcel.CommitTrackedChanges(db, p.parcelId, p.trackingfield, p.TrackedFieldValues)
                    If ParcelEventLog.Enabled Then
                        ParcelEventLog.CreateFlagEvent(db, ParcelEventLog.EventType.TrackedValueCommit, ParcelEventLog.ApplicationType.API, p.parcelId)
                    End If


                    If request.partial Then
                        ParcelAuditStream.CreateFlagEvent(db, DateTime.UtcNow, p.parcelId, "API", "Selected changes committed via API (" & pciDeleted & " records)", "API")
                    Else
                        ParcelAuditStream.CreateFlagEvent(db, DateTime.UtcNow, p.parcelId, "API", "Parcel changes committed via API (" & pciDeleted & " records)  for Tracked Value Group " & p.trackingfield & " ", "API")
                    End If

                    affectedParcelList.Add(New CommittedParcel With {.parcelId = p.parcelId, .recordsAffected = pciDeleted})
                Next
                ParcelAuditStream.UpdateLastSyncDate(db, "LastDownSyncDate")
            Catch nex As NullReferenceException
                Return New StandardResponse(400, 1, Nothing)
            Catch ex As Exception
                Return New StandardResponse(ex, request)
            End Try

            Dim parcelString = DataTools.JSON.Serialize(Of List(Of CommittedParcel))(affectedParcelList)
            Dim sp As New StandardResponse() With {.data = DataTools.DataConversion.EncodeToBase64(parcelString), .recordCount = affectedParcelList.Count}
            Return sp

        End Function


        Public Shared Function ExecuteParcelUpdateKey(ByVal db As Database, ByVal request As ParcelRequest) As StandardResponse

            Dim affectedParcelList As New List(Of CommittedParcel)
            Try
                For Each p As ParcelMapInfo In request.parcels
                    Dim pciUpdated As Integer = Parcel.UpdateKeys(db, p.parcelId, p.parcelKeys)
                    If ParcelEventLog.Enabled Then
                        ParcelEventLog.CreateFlagEvent(db, ParcelEventLog.EventType.KeysUpdated, ParcelEventLog.ApplicationType.API, p.parcelId)
                    End If

                    If request.partial Then
                        ParcelAuditStream.CreateFlagEvent(db, DateTime.UtcNow, p.parcelId, "API", "Parcel keys updated via API (" & pciUpdated & " records)", "API")
                    Else
                        ParcelAuditStream.CreateFlagEvent(db, DateTime.UtcNow, p.parcelId, "API", "Parcel keys updated via API (" & pciUpdated & " records)", "API")
                    End If
                    affectedParcelList.Add(New CommittedParcel With {.parcelId = p.parcelId, .recordsAffected = pciUpdated})
                Next
            Catch nex As NullReferenceException
                Return New StandardResponse(400, 1, Nothing)
            Catch ex As Exception
                Return New StandardResponse(ex, request)
            End Try

            Dim parcelString = DataTools.JSON.Serialize(Of List(Of CommittedParcel))(affectedParcelList)
            Dim sp As New StandardResponse() With {.data = DataTools.DataConversion.EncodeToBase64(parcelString), .recordCount = affectedParcelList.Count}
            Return sp

        End Function



        Public Shared Function ClearAllParcelData(ByVal db As Database, ByVal request As ParcelRequest) As StandardResponse
            Return ParcelDeleteHelper.DeleteParcels(db, request.parcels)
        End Function

    End Class

End Namespace
