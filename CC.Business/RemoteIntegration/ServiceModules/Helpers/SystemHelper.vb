﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts

Namespace RemoteIntegration
    Friend Class SystemHelper

        Public Shared Sub LockSystem(db As Database, request As StandardRequest)
            Dim organizationId As Integer = Database.System.GetIntegerValueOrInvalid("SELECT OrganizationId FROM ApplicationAccess WHERE AccessKey = {0} AND PassKey = {1}".SqlFormatString(request.accessKey, request.passKey))
            Database.System.Execute("UPDATE Organization SET DataLocked = 1 WHERE Id = " & organizationId)
            SystemAuditStream.CreateStandardEvent(db, StandardSystemAuditEvents.SYS_10002_LOCK)
        End Sub

        Public Shared Sub UnlockSystem(db As Database, request As StandardRequest)
            Dim organizationId As Integer = Database.System.GetIntegerValueOrInvalid("SELECT OrganizationId FROM ApplicationAccess WHERE AccessKey = {0} AND PassKey = {1}".SqlFormatString(request.accessKey, request.passKey))
            Database.System.Execute("UPDATE Organization SET DataLocked = 0 WHERE Id = " & organizationId)
            SystemAuditStream.CreateStandardEvent(db, StandardSystemAuditEvents.SYS_10001_UNLK)
        End Sub


        ' 2 DataLoaded
        ' 1 Loading
        ' 0 Empty
        ' 
        'Public Shared Sub InitializeDataLoading(db As Database, request As StandardRequest)
        '    Dim organizationId As Integer = Database.System.GetIntegerValueOrInvalid("SELECT OrganizationId FROM ApplicationAccess WHERE AccessKey = {0} AND PassKey = {1}".SqlFormatString(request.accessKey, request.passKey))
        '    Database.System.Execute("UPDATE Organization SET DataLoadingStatus= 0 WHERE Id = " & organizationId)
        'End Sub

    End Class
End Namespace
