﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts
Imports CAMACloud.Exceptions
Imports System.Text.RegularExpressions
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects
Namespace RemoteIntegration
    Friend Class DataTransferHelper

        Const Prefix_XT As String = "XT_"
        Const Prefix_LT As String = "LT_"
        Const Prefix_Temp As String = "transfer_temp_"

        Shared transferJobSql = <sql>
CREATE TABLE TransferJob
(
	Id INT IDENTITY PRIMARY KEY,
	CreatedBy INT,
	CreatedTime DATETIME DEFAULT GETUTCDATE(),
	SourceIPAddress VARCHAR(100),
	TransferType INT,
	TotalPages INT,
	CurrentPage INT,
	Completed BIT DEFAULT 0,
    Cancelled BIT DEFAULT 0,
	Error BIT DEFAULT 0,
	Reason VARCHAR(1000),
	StatusMessage VARCHAR(1000),
	ErrorMessage VARCHAR(MAX)
)
                                </sql>

        Public Shared Sub ClearAllParcelData(ByVal db As Database, ByRef recordTracker As RecordCountInfo, recreateTables As Boolean)
            Try
                Dim tableNames As String() = db.GetAllTableNames
                For Each tname In tableNames
                    If tname.StartsWith(Prefix_XT) Or tname.StartsWith(Prefix_Temp) Or tname.StartsWith(Prefix_LT) Then
                        db.Execute("DROP TABLE " + tname)
                    End If
                Next
                db.DropTable("ParcelData")
                recordTracker.tablesAffected += 1
                db.DropTable("NeighborhoodData")
                recordTracker.tablesAffected += 1
                db.DeleteTableRecordsWithIdentityReset("ParcelChanges")
                If db.DoesTableExists("archive_ParcelChanges") Then
                    recordTracker.tablesAffected += 1
                    db.DeleteTableRecordsWithIdentityReset("archive_ParcelChanges")
                End If
                recordTracker.tablesAffected += 1
                db.DeleteTableRecordsWithIdentityReset("ParcelAuditTrail")
                recordTracker.tablesAffected += 1
                db.DeleteTableRecordsWithIdentityReset("ParcelEventLog")
                recordTracker.tablesAffected += 1
                db.DeleteTableRecordsWithIdentityReset("ParcelSketchVectors")
                recordTracker.tablesAffected += 1
                db.DeleteTableRecordsWithIdentityReset("ParcelSketch")
                recordTracker.tablesAffected += 1
                db.DeleteTableRecordsWithIdentityReset("ParcelSketchFlag")
                recordTracker.tablesAffected += 1
                db.DeleteTableRecordsWithIdentityReset("ParcelSketchSegments")
                recordTracker.tablesAffected += 1
                db.DeleteTableRecordsWithIdentityReset("ParcelImages")
                recordTracker.tablesAffected += 1
                db.DeleteTableRecordsWithIdentityReset("ParcelComparables")
                recordTracker.tablesAffected += 1
                db.DeleteTableRecordsWithIdentityReset("ParcelMapPoints")
                recordTracker.tablesAffected += 1
                db.DeleteTableRecordsWithIdentityReset("ParcelReviewLog")
                recordTracker.tablesAffected += 1
                db.DeleteTableRecordsWithIdentityReset("Parcel")
                recordTracker.tablesAffected += 1
                db.DeleteTableRecordsWithIdentityReset("Neighborhood")
                recordTracker.tablesAffected += 1
                db.DeleteTableRecordsWithIdentityReset("ParcelDataLookup")
                recordTracker.tablesAffected += 1
                db.DeleteTableRecordsWithIdentityReset("SyncUserCache")
                recordTracker.tablesAffected += 1
                db.DeleteTableRecordsWithIdentityReset("SyncLog")
                recordTracker.tablesAffected += 1
                db.DeleteTableRecordsWithIdentityReset("SyncUserCache")
                recordTracker.tablesAffected += 1
                db.DeleteTableRecordsWithIdentityReset("UserSync")
                recordTracker.tablesAffected += 1
                db.DeleteTableRecordsWithIdentityReset("UserSyncParcels")
                recordTracker.tablesAffected += 1
                db.DeleteTableRecordsWithIdentityReset("UserTrackingCurrent")
                recordTracker.tablesAffected += 1
                db.DeleteTableRecordsWithIdentityReset("UserTrackingHistory")
                recordTracker.tablesAffected += 1
                db.DeleteTableRecordsWithIdentityReset("UserSettings")
                recordTracker.tablesAffected += 1
                db.DeleteTableRecordsWithIdentityReset("UserAuditTrail")
                recordTracker.tablesAffected += 1
                db.DeleteTableRecordsWithIdentityReset("Assignment")
                recordTracker.tablesAffected += 1
                db.DeleteTableRecordsWithIdentityReset("UserParcelChanges")
                recordTracker.tablesAffected += 1
                db.DeleteTableRecordsWithIdentityReset("UserParcelChangesBatch")
                recordTracker.tablesAffected += 1
                'for deleting tables on purge based on a resultset from procedure'
                Dim dt As DataTable = db.GetDataTable("EXEC schema_ParcelTablesDeleteList")
                TableDeleteFromList(dt, db)

                If recreateTables Then
                    PrepareTables(db)
                End If


                SystemAuditStream.CreateStandardEvent(db, StandardSystemAuditEvents.DTX_40000_RESET)
            Catch ex As Exception
                Throw New DataSchemaException(ex)
            End Try

        End Sub

        Public Shared Sub TableDeleteFromList(dtField As DataTable, ByVal db As Database)
            Dim i As Integer = 0
            Dim strDetail As String = Nothing
            Dim names As Dictionary(Of String, String) = New Dictionary(Of String, String)

            For Each row As DataRow In dtField.Rows
                strDetail = row.Item("TableName")
                If names.ContainsKey(strDetail) Then
                    If names(strDetail) < row.Item("DropType") Then
                        names(strDetail) = row.Item("DropType")
                    End If
                Else
                    names.Add(row.Item("TableName"), row.Item("DropType"))
                End If
            Next row

            For Each kvp As KeyValuePair(Of String, String) In names
                Dim v1 As String = kvp.Key
                Dim v2 As String = kvp.Value

                If v2 = "1" Then
                    db.Execute("DELETE FROM " + v1.ToString())
                Else
                    db.DeleteTableRecordsWithIdentityReset(v1.ToString())
                End If

            Next
        End Sub

        Public Shared Sub DumpAllTables(ByVal type As BulkDataTransferType, ByVal db As Database, ByVal ds As DataSet, threadNumber As Integer, ByRef recordTracker As RecordCountInfo)
            For Each dt As DataTable In ds.Tables
                Dim sourceTable As String = dt.TableName.Trim
                Dim targetTable As String = DataSource.FindTargetTable(db, sourceTable)
                Dim tempTableName As String = Prefix_Temp + dt.TableName + threadNumber.ToString
                Dim tempTableBackup As String = Prefix_Temp + dt.TableName + "_backup" + threadNumber.ToString

                Dim tableFields = db.GetDataTable("SELECT f.* FROM DataSourceField f INNER JOIN DataSourceTable t ON f.TableId = t.Id WHERE t.Name = {0}".SqlFormatString(sourceTable)).Rows.Cast(Of DataRow)().Select(Function(x) New SchemaField With {.Name = x.GetString("Name"), .DataType = x.GetInteger("DataType"), .MaxLength = x.GetInteger("MaxLength"), .SchemaDataType = x.Get("SchemaDataType"), .Precision = x.Get("Numericprecision"), .Scale = x.Get("NumericScale")}).Select(Function(f) New KeyValuePair(Of String, String)(f.Name, f.SchemaTypeDeclaration)).ToList
                'Dim tfc As New NameValueCollection
                'For Each tf In tableFields
                '    tfc.Add(tf.Key, tf.Value)
                'Next
                Dim backupRefreshData As Integer = db.GetIntegerValue("SELECT Value FROM clientsettings WHERE name='KeepRefreshData'")
                Dim lockName As String = db.ConnectionString
                SyncLock lockName
                    db.BulkLoadTableFromDataTable(tempTableName, dt, tableFields)
                    If backupRefreshData = 1 Then
                        db.BulkLoadTableFromDataTable(tempTableBackup, dt, tableFields, True)
                    End If
                End SyncLock

                Dim isLookupTable As Boolean = False
                Dim targetTableOnDatabase As String = DataSource.FindTargetTable(db, sourceTable, , isLookupTable)

                If targetTableOnDatabase = String.Empty Then
                    If db.DoesTableExists(tempTableName) Then
                        db.DropTable(tempTableName)
                    End If
                    Throw New Exceptions.DataException("The uploaded table '" + dt.TableName + "' does not have relations established in the schema.")
                End If

                Dim tableId As Integer = DataSource.GetTableId(db, dt.TableName)

                'Process Primary Table
                If db.Application.ParcelTable.ToLower = sourceTable.ToLower Then
                    If type = BulkDataTransferType.Reset Or type = BulkDataTransferType.Refresh Or type = BulkDataTransferType.Clean Then
                        loadParcelTableForResetOrRefresh(type, db, tempTableName, recordTracker)
                    End If
                    loadParcelDataFromPrimaryForAll(type, db, dt, tableId, tempTableName, recordTracker)
                ElseIf targetTableOnDatabase = "ParcelData" Then
                    loadParcelDataFromPrimaryExtraTableForAll(type, db, dt, tableId, tempTableName, recordTracker)
                ElseIf db.Application.NeighborhoodTable.ToLower = sourceTable.ToLower Then
                    If type = BulkDataTransferType.Reset Or type = BulkDataTransferType.Refresh Then
                        loadNeighborhoodTableForResetOrRefresh(db, tempTableName, recordTracker)
                    End If
                    loadNeighborhoodDataForAll(db, dt, tempTableName, recordTracker)
                ElseIf isLookupTable Then
                    loadLookupDataForAll(db, dt, tempTableName, recordTracker)
                Else
                    loadAuxiliaryDataForAll(type, db, dt, tableId, tempTableName, recordTracker)
                End If

                If sourceTable = StreetAddress.GetStreetAddressTable(db) Then
                    MapStreetAddressTable(db, dt, tempTableName, targetTable)
                End If


                db.DropTable(tempTableName)
                recordTracker.tablesAffected += 1
            Next

        End Sub

        Private Shared Sub loadParcelTableForResetOrRefresh(ByVal type As BulkDataTransferType, ByVal db As Database, ByVal tempTableName As String, ByRef recordTracker As RecordCountInfo)
            Dim keyfields As String = ""
            Dim filter As String = ""
            Dim keyValueString As String = String.Empty
            Dim dr As DataRow = db.GetTopRow("SELECT * FROM Application")

            Dim parcelTable As String = db.Application.ParcelTable
            Dim pf As DataTable = db.GetDataTable("SELECT Name, DataType, SchemaDataType FROM DataSourceField WHERE SourceTable = " + parcelTable.ToSqlValue)
            Dim pfs = pf.AsEnumerable.Select(Function(x) New With {.Name = x.GetString("Name"), .DataType = x.GetInteger("DataType"), .SchemaDataType = x.GetString("SchemaDataType")}).ToDictionary(Function(x) x.Name)
            For i As Integer = 1 To 10
                Dim kf As String = dr.GetString("KeyField" & i)
                If kf <> "" Then
                    If Not pfs.ContainsKey(kf) Then
                        Dim errorMessage As String = kf + " is not a field in " + parcelTable + ". All KeyFields in Application settings are expected to be available in main parcel table."
                        Throw New CAMACloud.Exceptions.BadRequestException(errorMessage, Nothing, False)
                    End If
                    Dim field = pfs(kf)
                    Dim isNumeric = False
                    If {"int", "float", "decimal", "numeric"}.Contains(field.SchemaDataType.ToLower) Then
                        isNumeric = True
                    End If
                    If keyfields <> "" Then keyfields += ", "
                    If keyValueString <> "" Then keyValueString += ", "
                    If isNumeric Then
                        keyfields += "LTRIM(STR([" + kf + "]))"
                    Else
                        keyfields += "[" + kf + "]"
                    End If

                    keyValueString += "[KeyValue" & i & "]"

                    Dim parcelField As String = "[KeyValue" & i & "]"
                    Dim dataField As String = "[" + kf + "]"
                    If filter <> "" Then filter += " AND "
                    filter += "d." + dataField + " = " + "p." + parcelField
                End If
            Next
            If filter <> "" Then


                Dim lastParcelId = db.GetStringValue("SELECT COALESCE(MAX(Id),0) FROM Parcel")
                Dim sqlInsertFrom As String = "FROM [" + tempTableName + "] d LEFT OUTER JOIN Parcel p ON " + filter + " WHERE p.Id IS NULL;"

                recordTracker.parcelCreated += db.Execute("INSERT INTO Parcel (" + keyValueString + ") SELECT " + keyfields + sqlInsertFrom)

                If ParcelEventLog.Enabled Then
                    Dim sqlParcelEventLog As String = "INSERT INTO ParcelEventLog (ParcelId, EventId, ApplicationId) SELECT Id, 0, 0 FROM Parcel WHERE Id > " & lastParcelId
                    db.Execute(sqlParcelEventLog)
                End If

                Try
                    If DataTransfer.AutoPriorityEnabled Then
                        Dim sql As String = "EXEC API_ParcelRefreshLogCurrent @type = '" + type.ToString() + "',@maxParcelId = " + lastParcelId + ",@loadParcelTable = 1"
                        'Dim insertSql = "INSERT INTO ParcelRefreshLogCurrent (ParcelId, Refreshtype, RefreshDateTime) SELECT Id,'" + type.ToString() + "', GETUTCDATE() FROM Parcel WHERE Id > " & lastParcelId
                        'db.Execute(insertSql)
                        db.Execute(sql)
                    End If
                Catch e As Exception
                    Throw New Exception("An error occurred while processing your request. ", e)
                End Try


            Else
                Throw New Exception("The primary parcel table is not set in your application configuration settings. Loading Parcel entry failed. Please review settings and try again.")
            End If

        End Sub

        Private Shared Sub loadParcelDataFromPrimaryForAll(ByVal type As BulkDataTransferType, ByVal db As Database, ByVal dt As DataTable, tableId As Integer, ByVal tempTableName As String, ByRef recordTracker As RecordCountInfo)
            Dim filter As String = ""
            Dim filter2 As String = ""
            Dim updateFilter As String = ""
            Dim dr As DataRow = db.GetTopRow("SELECT * FROM Application")
            For i As Integer = 1 To 10
                Dim kf As String = dr.GetString("KeyField" & i)
                If kf <> "" Then
                    Dim parcelField As String = "[KeyValue" & i & "]"
                    Dim dataField As String = "[" + kf + "]"
                    If filter <> "" Then filter += " AND "
                    If filter2 <> "" Then filter2 += " AND "
                    If updateFilter <> "" Then updateFilter += " AND "
                    filter += "d." + dataField + " = " + "p." + parcelField
                    filter2 += "d." + dataField + " = " + "pd." + dataField
                    updateFilter += "ParcelData." + dataField + " = " + "d." + dataField
                End If
            Next

            Dim tableFields = db.GetDataTable("SELECT f.Name FROM DataSourceField f INNER JOIN DataSourceTable t ON f.TableId = t.Id WHERE t.Name = {0}".SqlFormatString(dt.TableName)).Rows.Cast(Of DataRow)().Select(Function(x) x.GetString("Name")).ToArray

            Dim insertStatement As String = "INSERT INTO ParcelData (CC_ParcelId,CC_RecordStatus"
            Dim updateStatement As String = "UPDATE ParcelData SET CC_LastUpdateTime = GETUTCDATE()"
            updateStatement += ", ParcelData.[CC_RecordStatus] =CASE WHEN [CC_RecordStatus]= 'I' THEN 'D' WHEN [CC_RecordStatus] = 'D' THEN 'D' ELSE 'X' END "
            Dim selectFields As String = "p.Id,'X'"
            For Each dc As DataColumn In dt.Columns
                If tableFields.Contains(dc.ColumnName) Then
                    insertStatement += ", [" + dc.ColumnName + "]"
                    selectFields += ", d.[" + dc.ColumnName + "]"
                    updateStatement += ", ParcelData.[" + dc.ColumnName + "] = d.[" + dc.ColumnName + "]"
                End If
            Next

            insertStatement += ") "
            insertStatement += "SELECT " + selectFields + " FROM " + tempTableName + " d INNER JOIN Parcel p ON " + filter + " LEFT OUTER JOIN ParcelData pd ON " + filter2 + " WHERE pd.ROWUID IS NULL"
            updateStatement += " FROM [" + tempTableName + "] d WHERE " + updateFilter

            'Dim insertEventSql As String = "INSERT INTO ParcelEventLog (ParcelId, EventId, ApplicationId, P1, P2) SELECT p.Id, " & tableId & ", 0 FROM " + tempTableName + " d INNER JOIN Parcel p ON " + filter + " LEFT OUTER JOIN ParcelData pd ON " + filter2 + " WHERE pd.ROWUID IS NULL"



            If filter <> "" AndAlso filter2 <> "" Then

                Dim sqlDateTimeBeforeInsert As String = db.GetStringValue("SELECT CONVERT(VARCHAR(25), GETUTCDATE(), 121)")
                recordTracker.parcelModified += db.Execute(updateStatement)
                Dim updateEventSql As String = "INSERT INTO ParcelEventLog (ParcelId, EventId, ApplicationId, P1, P2) SELECT ParcelData.CC_ParcelId, 1, 0, " & tableId & ", ParcelData.ROWUID FROM ParcelData INNER JOIN [" + tempTableName + "] d ON " + updateFilter
                If ParcelEventLog.Enabled Then
                    db.Execute(updateEventSql)
                End If


                recordTracker.recordsInserted += db.Execute(insertStatement)

                If ParcelEventLog.Enabled Then
                    Dim insertEventSql As String = "INSERT INTO ParcelEventLog (ParcelId, EventId, ApplicationId, P1, P2) SELECT ParcelData.CC_ParcelId, 20, 0, " & tableId & ", ParcelData.ROWUID FROM ParcelData INNER JOIN [" + tempTableName + "] d ON " + updateFilter + " WHERE CC_LastUpdateTime > " + sqlDateTimeBeforeInsert.ToSqlValue
                    db.Execute(insertEventSql)
                End If
                Try
                    'Dim insertSql = "INSERT INTO ParcelRefreshLogCurrent (ParcelId, Refreshtype, RefreshDateTime) SELECT ParcelData.CC_ParcelId,'" + type.ToString() + "', GETUTCDATE() FROM ParcelData INNER JOIN [" + tempTableName + "] d ON " + updateFilter + " WHERE CC_LastUpdateTime > " + sqlDateTimeBeforeInsert.ToSqlValue
                    'db.Execute(insertSql)
                    If DataTransfer.AutoPriorityEnabled Then
                        Dim sql As String = "EXEC API_ParcelRefreshLogCurrent @type = '" + type.ToString() + "',@tempTableName = " + tempTableName + " ,@updateFilter = '" + updateFilter + "',@sqlDateTimeBeforeInsert = " + sqlDateTimeBeforeInsert.ToSqlValue
                        db.Execute(sql)
                    End If
                Catch e As Exception
                    Throw New Exception("An error occurred while processing your request. ", e)
                End Try
                Try
                    Dim dtRow As DataRow = db.GetTopRow("SELECT top 1 * FROM MAParcelComparableFields")
                    If dtRow IsNot Nothing AndAlso dtRow.GetString("TableName") <> "" Then
                        Dim msql As String = "EXEC API_MAParcelComparableRefreshLogCurrent @type = '" + type.ToString() + "',@tempTableName = " + tempTableName + " ,@updateFilter = '" + updateFilter + "',@sqlDateTimeBeforeInsert = " + sqlDateTimeBeforeInsert.ToSqlValue
                        db.Execute(msql)
                    End If
                Catch e As Exception
                    Throw New Exception("An error occurred while processing your request. ", e)
                End Try

            Else
                Dim errorDetail As String = ""
                If filter = "" Then
                    errorDetail += "Parcel filter is unavailable. "
                End If
                If filter2 = "" Then
                    errorDetail += "Parcel Data filter is unavailable. "
                End If
                Throw New Exception("Updating Parcel Data failed. " + errorDetail + "Please review settings and try again.")
            End If

        End Sub

        Private Shared Sub loadParcelDataFromPrimaryExtraTableForAll(ByVal type As BulkDataTransferType, ByVal db As Database, ByVal dt As DataTable, ByVal tableId As Integer, ByVal tempTableName As String, ByRef recordTracker As RecordCountInfo)
            Dim primaryFilter As String = ""
            Dim parentDataFilter As String = ""
            Dim updateFilter As String = ""

            Dim parentTargetTable As String = ""
            Dim parentTableName As String = FindParentTable(db, dt.TableName, parentTargetTable)
            Dim connKeys = _getConnectingKeys(db, dt.TableName, parentTableName)

            For Each keyPair In connKeys
                Dim tableKey As String = "[" + keyPair(0) + "]"
                Dim parentKey As String = "[" + keyPair(1) + "]"
                If parentDataFilter <> "" Then parentDataFilter += " AND "
                parentDataFilter += "d." + tableKey + " = " + "pd." + parentKey

                If updateFilter <> "" Then updateFilter += " AND "
                updateFilter += "ParcelData." + parentKey + " = " + "d." + tableKey
            Next


            Dim dr As DataRow = db.GetTopRow("SELECT * FROM Application")
            For i As Integer = 1 To 10
                Dim kf As String = dr.GetString("KeyField" & i)
                If kf <> "" Then
                    Dim parcelField As String = "[KeyValue" & i & "]"
                    Dim dataField As String = "[" + kf + "]"
                    If primaryFilter <> "" Then primaryFilter += " AND "
                    primaryFilter += "pd." + dataField + " = " + "p." + parcelField
                End If
            Next

            Dim tableFields = db.GetDataTable("SELECT f.Name FROM DataSourceField f INNER JOIN DataSourceTable t ON f.TableId = t.Id WHERE t.Name = {0}".SqlFormatString(dt.TableName)).Rows.Cast(Of DataRow)().Select(Function(x) x.GetString("Name")).ToArray

            Dim insertStatement As String = "INSERT INTO ParcelData (CC_ParcelId,CC_RecordStatus"
            Dim updateStatement As String = "UPDATE ParcelData SET CC_LastUpdateTime = GETUTCDATE()"
            updateStatement += ", ParcelData.[CC_RecordStatus] =CASE WHEN [CC_RecordStatus]= 'I' THEN 'D' WHEN [CC_RecordStatus] = 'D' THEN 'D' ELSE 'X' END "
            Dim selectFields As String = "p.Id,'X'"
            For Each dc As DataColumn In dt.Columns
                If tableFields.Contains(dc.ColumnName) Then
                    insertStatement += ", [" + dc.ColumnName + "]"
                    selectFields += ", d.[" + dc.ColumnName + "]"
                    updateStatement += ", ParcelData.[" + dc.ColumnName + "] = d.[" + dc.ColumnName + "]"
                End If
            Next

            insertStatement += ") "
            insertStatement += "SELECT " + selectFields + " FROM " + tempTableName + " d INNER JOIN ParcelData pd ON " + parentDataFilter + " LEFT OUTER JOIN Parcel p ON " + primaryFilter + " WHERE pd.ROWUID IS NULL"
            updateStatement += " FROM [" + tempTableName + "] d WHERE " + updateFilter

            If primaryFilter <> "" AndAlso parentDataFilter <> "" Then
                Dim sqlDateTimeBeforeInsert As String = db.GetStringValue("SELECT CONVERT(VARCHAR(25), GETUTCDATE(), 121)")
                recordTracker.parcelModified += db.Execute(updateStatement)
                Dim updateEventSql As String = "INSERT INTO ParcelEventLog (ParcelId, EventId, ApplicationId, P1, P2) SELECT ParcelData.CC_ParcelId, 21, 0, " & tableId & ", ParcelData.ROWUID FROM ParcelData INNER JOIN [" + tempTableName + "] d ON " + updateFilter
                If ParcelEventLog.Enabled Then
                    db.Execute(updateEventSql)
                End If


                recordTracker.recordsInserted += db.Execute(insertStatement)


                If ParcelEventLog.Enabled Then
                    Dim insertEventSql As String = "INSERT INTO ParcelEventLog (ParcelId, EventId, ApplicationId, P1, P2) SELECT ParcelData.CC_ParcelId, 20, 0, " & tableId & ", ParcelData.ROWUID FROM ParcelData INNER JOIN [" + tempTableName + "] d ON " + updateFilter + " WHERE CC_LastUpdateTime > " + sqlDateTimeBeforeInsert.ToSqlValue
                    db.Execute(insertEventSql)
                End If

                Try
                    'Dim insertSql = "INSERT INTO ParcelRefreshLogCurrent (ParcelId, Refreshtype, RefreshDateTime) SELECT ParcelData.CC_ParcelId,'"+ type.ToString() +"', GETUTCDATE() FROM ParcelData INNER JOIN [" + tempTableName + "] d ON " + updateFilter + " WHERE CC_LastUpdateTime > " + sqlDateTimeBeforeInsert.ToSqlValue
                    'db.Execute(insertSql)
                    If DataTransfer.AutoPriorityEnabled Then
                        Dim sql As String = "EXEC API_ParcelRefreshLogCurrent @type = '" + type.ToString() + "',@tempTableName = " + tempTableName + " ,@updateFilter = '" + updateFilter + "',@sqlDateTimeBeforeInsert = " + sqlDateTimeBeforeInsert.ToSqlValue
                        db.Execute(sql)
                    End If
                Catch e As Exception
                    Throw New Exception("An error occurred while processing your request. ", e)
                End Try
                Try
                    Dim dtRow As DataRow = db.GetTopRow("SELECT top 1 * FROM MAParcelComparableFields")
                    If dtRow IsNot Nothing AndAlso dtRow.GetString("TableName") <> "" Then
                        Dim msql As String = "EXEC API_MAParcelComparableRefreshLogCurrent @type = '" + type.ToString() + "',@tempTableName = " + tempTableName + " ,@updateFilter = '" + updateFilter + "',@sqlDateTimeBeforeInsert = " + sqlDateTimeBeforeInsert.ToSqlValue
                        db.Execute(msql)
                    End If
                Catch e As Exception
                    Throw New Exception("An error occurred while processing your request. ", e)
                End Try
            Else
                Dim errorDetail As String = ""
                If primaryFilter = "" Then
                    errorDetail = "Primary filter is blank. "
                End If
                If parentDataFilter = "" Then
                    errorDetail += "Parent Data filter is blank. "
                End If
                errorDetail += "Table: " + dt.TableName + "; Parcel Table: " + db.Application.ParcelTable + ". "
                Throw New Exception("Updating Parcel Co-Table failed. " + errorDetail + "Please review settings and try again.")
            End If

        End Sub

        'Private Shared Sub loadAuxiliaryDataForReset(ByVal db As Database, ByVal dt As DataTable, ByVal tempTableName As String, ByRef recordTracker As RecordCountInfo)
        '    Dim auxTableName As String = DataSource.FindTargetTable(db, dt.TableName)
        '    Dim parentTargetTable As String = ""
        '    Dim parentTableName As String = FindParentTable(db, dt.TableName, parentTargetTable)

        '    Dim parentTableFilter As String = ""
        '    Dim rowExistsJoinCondition As String = ""
        '    Dim updateFilter As String = ""

        '    Dim dr As DataRow = db.GetTopRow("SELECT * FROM Application")
        '    Dim connKeys = _getConnectingKeys(db, dt.TableName, parentTableName)
        '    For Each keyPair In connKeys
        '        Dim tableKey As String = "[" + keyPair(0) + "]"
        '        Dim parentKey As String = "[" + keyPair(1) + "]"
        '        If parentTableFilter <> "" Then parentTableFilter += " AND "
        '        parentTableFilter += "d." + tableKey + " = " + "p." + parentKey

        '        If rowExistsJoinCondition <> "" Then rowExistsJoinCondition += " AND "
        '        If updateFilter <> "" Then updateFilter += " AND "
        '        rowExistsJoinCondition += "d." + tableKey + " = " + "pd." + parentKey
        '        updateFilter += auxTableName + "." + parentKey + " = " + "d." + tableKey
        '    Next

        '    Dim rowExistsJoinConditionMain As String = ""
        '    Dim updateFilterMain As String = ""

        '    Dim keys = _getKeys(db, dt.TableName)
        '    For Each key In keys
        '        If key <> "" Then
        '            Dim dataField As String = "[" + key + "]"
        '            If rowExistsJoinConditionMain <> "" Then rowExistsJoinConditionMain += " AND "
        '            If updateFilterMain <> "" Then updateFilterMain += " AND "
        '            rowExistsJoinConditionMain += "d." + dataField + " = " + "pd." + dataField
        '            updateFilterMain += auxTableName + "." + dataField + " = " + "d." + dataField
        '        End If
        '    Next

        '    Dim tableFields = db.GetDataTable("SELECT f.Name FROM DataSourceField f INNER JOIN DataSourceTable t ON f.TableId = t.Id WHERE t.Name = {0}".SqlFormatString(dt.TableName)).Rows.Cast(Of DataRow)().Select(Function(x) x.GetString("Name")).ToArray

        '    Dim insertStatement As String = "INSERT INTO " + auxTableName + " (CC_ParcelId"
        '    Dim updateStatement As String = "UPDATE " + auxTableName + " SET CC_LastUpdateTime = GETUTCDATE()"



        '    If parentTargetTable = auxTableName Then
        '        Dim selectFields As String = "pd.CC_ParcelId"
        '        For Each dc As DataColumn In dt.Columns
        '            If tableFields.Contains(dc.ColumnName) Then
        '                insertStatement += ", [" + dc.ColumnName + "]"
        '                selectFields += ", d.[" + dc.ColumnName + "]"
        '                updateStatement += ", " + auxTableName + ".[" + dc.ColumnName + "] = d.[" + dc.ColumnName + "]"
        '            End If
        '        Next

        '        If rowExistsJoinCondition <> "" Then
        '            insertStatement += ") "
        '            insertStatement += "SELECT " + selectFields + " FROM " + tempTableName + " d LEFT OUTER JOIN " + auxTableName + " pd ON " + rowExistsJoinCondition + " WHERE pd.ROWUID IS NULL"
        '            updateStatement += " FROM [" + tempTableName + "] d WHERE " + updateFilter
        '        Else
        '            Throw New Exception("The schema relations for table  " + dt.TableName + "  are incorrect.")
        '        End If

        '    Else
        '        Dim selectFields As String = "p.CC_ParcelId"
        '        For Each dc As DataColumn In dt.Columns
        '            If tableFields.Contains(dc.ColumnName) Then
        '                insertStatement += ", [" + dc.ColumnName + "]"
        '                selectFields += ", d.[" + dc.ColumnName + "]"
        '                updateStatement += ", " + auxTableName + ".[" + dc.ColumnName + "] = d.[" + dc.ColumnName + "]"
        '            End If
        '        Next
        '        If rowExistsJoinConditionMain <> "" AndAlso parentTargetTable <> "" AndAlso parentTableFilter <> "" Then
        '            insertStatement += ") "
        '            insertStatement += "SELECT " + selectFields + " FROM " + tempTableName + " d INNER JOIN " + parentTargetTable + " p ON " + parentTableFilter + " LEFT OUTER JOIN " + auxTableName + " pd ON " + rowExistsJoinConditionMain + " WHERE pd.ROWUID IS NULL"
        '            updateStatement += " FROM [" + tempTableName + "] d WHERE " + updateFilterMain
        '        Else
        '            Throw New Exception("The schema relations for table  " + dt.TableName + "  are incorrect.")
        '        End If

        '    End If

        '    If dt.TableName = "account" Then
        '        Dim i As Integer = 1
        '    End If

        '    recordTracker.recordsInserted += db.Execute(insertStatement)
        '    recordTracker.recordsUpdated += db.Execute(updateStatement)
        'End Sub
        Public Shared Sub bindOrphanRecords(ByVal type As BulkDataTransferType, ByVal db As Database, ByVal tname As String)
            Dim targetTable = DataSource.FindTargetTable(db, tname)
            Dim updateStatement As String
            Dim deleteStatement As String
            Dim parentTargetTable As String = ""
            Dim parentTableName As String = DataTransferHelper.FindParentTable(db, tname, parentTargetTable)
            Dim rowExistsJoinCondition As String = ""
            Dim updateFilter As String = ""
            Dim connKeys = _getConnectingKeys(db, tname, parentTableName)
            For Each keyPair In connKeys
                Dim tableKey As String = "[" + keyPair(0) + "]"
                Dim parentKey As String = "[" + keyPair(1) + "]"
                If rowExistsJoinCondition <> "" Then rowExistsJoinCondition += " AND "
                If updateFilter <> "" Then updateFilter += " AND "
                rowExistsJoinCondition += "d." + tableKey + " = " + "p." + parentKey

            Next
            If rowExistsJoinCondition <> "" Then
                deleteStatement = "DELETE d FROM [" + targetTable + "] d LEFT JOIN [" + parentTargetTable + "] p ON " + rowExistsJoinCondition + "  WHERE  d.CC_RecordStatus = 'S' AND p.ROWUID IS NULL"
                updateStatement = "UPDATE " + targetTable + " SET CC_ParcelId = p.CC_ParcelId, CC_RecordStatus = 'X'"
                updateStatement += " FROM [" + targetTable + "] d JOIN [" + parentTargetTable + "] p ON " + rowExistsJoinCondition + "  WHERE  d.CC_RecordStatus = 'S'"
            Else
                Throw New Exception("The schema relations for table  " + tname + "  are incorrect.")
            End If
            If updateStatement <> "" Then
                db.Execute(updateStatement)
            End If
            If deleteStatement <> "" Then
                db.Execute(deleteStatement)
            End If
        End Sub
        Private Shared Sub loadAuxiliaryDataForAll(ByVal type As BulkDataTransferType, ByVal db As Database, ByVal dt As DataTable, tableId As Integer, ByVal tempTableName As String, ByRef recordTracker As RecordCountInfo)
            Dim auxTableName As String = DataSource.FindTargetTable(db, dt.TableName)
            Dim parentTargetTable As String = ""
            Dim parentTableName As String = FindParentTable(db, dt.TableName, parentTargetTable)

            Dim AllowOrphanIns As Boolean = AllowOrphanInsert(db, dt.TableName)
            Dim parentTableFilter As String = ""
            Dim rowExistsJoinCondition As String = ""
            Dim updateFilter As String = ""

            Dim dr As DataRow = db.GetTopRow("SELECT * FROM Application")
            Dim connKeys = _getConnectingKeys(db, dt.TableName, parentTableName)
            For Each keyPair In connKeys
                Dim tableKey As String = "[" + keyPair(0) + "]"
                Dim parentKey As String = "[" + keyPair(1) + "]"
                If parentTableFilter <> "" Then parentTableFilter += " AND "
                parentTableFilter += "d." + tableKey + " = " + "p." + parentKey

                If rowExistsJoinCondition <> "" Then rowExistsJoinCondition += " AND "
                If updateFilter <> "" Then updateFilter += " AND "
                rowExistsJoinCondition += "d." + tableKey + " = " + "pd." + parentKey
                updateFilter += auxTableName + "." + tableKey + " = " + "d." + tableKey
            Next

            Dim rowExistsJoinConditionMain As String = ""
            Dim updateFilterMain As String = ""

            Dim keys = _getKeys(db, dt.TableName)
            For Each key In keys
                If key <> "" Then
                    Dim dataField As String = "[" + key + "]"
                    If rowExistsJoinConditionMain <> "" Then rowExistsJoinConditionMain += " AND "
                    If updateFilterMain <> "" Then updateFilterMain += " AND "
                    rowExistsJoinConditionMain += "d." + dataField + " = " + "pd." + dataField
                    updateFilterMain += auxTableName + "." + dataField + " = " + "d." + dataField
                End If
            Next

            Dim tableFields = db.GetDataTable("SELECT UPPER(f.Name) AS Name FROM DataSourceField f INNER JOIN DataSourceTable t ON f.TableId = t.Id WHERE t.Name = {0}".SqlFormatString(dt.TableName)).Rows.Cast(Of DataRow)().Select(Function(x) x.GetString("Name")).ToArray

            Dim insertStatement As String = "INSERT INTO " + auxTableName + " (CC_ParcelId, ParentROWUID,CC_RecordStatus"
            Dim updateStatement As String = "UPDATE " + auxTableName + " SET CC_LastUpdateTime = GETUTCDATE()" ', CC_ParcelId = pd.CC_ParcelId"
            Dim updateEventSql As String = "", insertEventSql As String = ""


            If parentTargetTable = auxTableName Then
                Dim selectFields As String = "pd.CC_ParcelId, NULL,'X'"
                For Each dc As DataColumn In dt.Columns
                    If tableFields.Contains(dc.ColumnName.ToUpper()) Then
                        insertStatement += ", [" + dc.ColumnName + "]"
                        selectFields += ", d.[" + dc.ColumnName + "]"
                        updateStatement += ", " + auxTableName + ".[" + dc.ColumnName + "] = d.[" + dc.ColumnName + "]"
                    End If
                Next
                If updateStatement <> "" Then
                    updateStatement += ", " + auxTableName + ".[CC_RecordStatus] =CASE WHEN [CC_RecordStatus]= 'I' THEN 'D' WHEN [CC_RecordStatus] = 'D' THEN 'D' ELSE 'X' END"
                End If


                If rowExistsJoinCondition <> "" Then
                    insertStatement += ") "
                    insertStatement += "SELECT " + selectFields + " FROM " + tempTableName + " d LEFT OUTER JOIN " + auxTableName + " pd ON " + rowExistsJoinCondition + " WHERE pd.ROWUID IS NULL"
                    updateStatement += " FROM [" + tempTableName + "] d WHERE " + updateFilter
                Else
                    Throw New Exception("The schema relations for table  " + dt.TableName + "  are incorrect.")
                End If

                updateEventSql = "INSERT INTO ParcelEventLog (ParcelId, EventId, ApplicationId, P1, P2) SELECT " + auxTableName + ".CC_ParcelId, 21, 0, " & tableId & ", " + auxTableName + ".ROWUID FROM " + auxTableName + " INNER JOIN [" + tempTableName + "] d ON " + updateFilter
                insertEventSql = "INSERT INTO ParcelEventLog (ParcelId, EventId, ApplicationId, P1, P2) SELECT " + auxTableName + ".CC_ParcelId, 20, 0, " & tableId & ", " + auxTableName + ".ROWUID FROM " + auxTableName + " INNER JOIN [" + tempTableName + "] d ON " + updateFilter


            Else
                Dim selectFields As String = "p.CC_ParcelId, NULL,'X'"
                For Each dc As DataColumn In dt.Columns
                    If tableFields.Contains(dc.ColumnName.ToUpper()) Then
                        insertStatement += ", [" + dc.ColumnName + "]"
                        selectFields += ", d.[" + dc.ColumnName + "]"
                        updateStatement += ", " + auxTableName + ".[" + dc.ColumnName + "] = d.[" + dc.ColumnName + "]"
                    End If
                Next

                If updateStatement <> "" Then
                    updateStatement += ", " + auxTableName + ".[CC_RecordStatus] =CASE WHEN [CC_RecordStatus]= 'I' THEN 'D' WHEN [CC_RecordStatus] = 'D' THEN 'D' WHEN [CC_RecordStatus] = 'S' THEN 'S' ELSE 'X' END"
                End If
                If rowExistsJoinConditionMain <> "" AndAlso parentTargetTable <> "" AndAlso parentTableFilter <> "" Then
                    insertStatement += ") "
                    insertStatement += "SELECT " + selectFields + " FROM " + tempTableName + " d INNER JOIN " + parentTargetTable + " p ON " + parentTableFilter + " LEFT OUTER JOIN " + auxTableName + " pd ON " + rowExistsJoinConditionMain + " WHERE pd.ROWUID IS NULL"
                    updateStatement += " FROM [" + tempTableName + "] d WHERE " + updateFilterMain
                Else
                    Throw New Exception("The schema relations for table  " + dt.TableName + "  are incorrect.")
                End If

                updateEventSql = "INSERT INTO ParcelEventLog (ParcelId, EventId, ApplicationId, P1, P2) SELECT " + auxTableName + ".CC_ParcelId, 21, 0, " & tableId & ", " + auxTableName + ".ROWUID FROM " + auxTableName + " INNER JOIN [" + tempTableName + "] d ON " + updateFilterMain
                insertEventSql = "INSERT INTO ParcelEventLog (ParcelId, EventId, ApplicationId, P1, P2) SELECT " + auxTableName + ".CC_ParcelId, 20, 0, " & tableId & ", " + auxTableName + ".ROWUID FROM " + auxTableName + " INNER JOIN [" + tempTableName + "] d ON " + updateFilterMain

            End If


            'Database.LogDB.Execute("INSERT INTO QueryLog (Statement) VALUES ({0})".SqlFormat(False, updateStatement))
            'Database.LogDB.Execute("INSERT INTO QueryLog (Statement) VALUES ({0})".SqlFormat(False, insertStatement))

            recordTracker.parcelModified += db.Execute(updateStatement)

            If ParcelEventLog.Enabled AndAlso updateEventSql <> "" Then
                db.Execute(updateEventSql)
            End If

            Try
                Dim parcelIdUpdateSql = _getParcelUpdateSql(db, dt.TableName, tempTableName)
                db.Execute(parcelIdUpdateSql)
            Catch ex As Exception

            End Try



            Dim sqlDateTimeBeforeInsert As String = db.GetStringValue("SELECT CONVERT(VARCHAR(25), GETUTCDATE(), 121)")
            recordTracker.recordsInserted += db.Execute(insertStatement)


            If ParcelEventLog.Enabled AndAlso updateEventSql <> "" Then
                insertEventSql += " WHERE CC_LastUpdateTime > " + sqlDateTimeBeforeInsert.ToSqlValue
                db.Execute(insertEventSql)
            End If


            If AllowOrphanIns Then
                insertStatement = "INSERT INTO " + auxTableName + " (CC_ParcelId, ParentROWUID,CC_RecordStatus"
                Dim selectFields As String = "pd.CC_ParcelId, NULL,'S'"
                For Each dc As DataColumn In dt.Columns
                    If tableFields.Contains(dc.ColumnName) Then
                        insertStatement += ", [" + dc.ColumnName + "]"
                        selectFields += ", d.[" + dc.ColumnName + "]"

                    End If
                Next
                If rowExistsJoinConditionMain <> "" AndAlso parentTargetTable <> "" AndAlso parentTableFilter <> "" Then
                    insertStatement += ") "
                    insertStatement += "SELECT " + selectFields + " FROM " + tempTableName + " d LEFT OUTER  JOIN " + parentTargetTable + " p ON " + parentTableFilter + " LEFT OUTER JOIN " + auxTableName + " pd ON " + rowExistsJoinConditionMain + " WHERE pd.ROWUID IS NULL AND P.ROWUID IS NULL"

                Else
                    Throw New Exception("The schema relations for table  " + dt.TableName + "  are incorrect.")
                End If
                insertEventSql = "INSERT INTO ParcelEventLog (ParcelId, EventId, ApplicationId, P1, P2) SELECT p.CC_ParcelId, 20, 0, " & tableId & ",p.ROWUID FROM " + tempTableName + " d LEFT OUTER  JOIN " + parentTargetTable + " p ON " + parentTableFilter + " LEFT OUTER JOIN " + auxTableName + " pd ON " + rowExistsJoinConditionMain + " WHERE pd.ROWUID IS NULL AND P.ROWUID IS NULL"
                recordTracker.recordsInserted += db.Execute(insertStatement)
                If ParcelEventLog.Enabled AndAlso insertStatement <> "" Then
                    'insertEventSql += " WHERE CC_LastUpdateTime > " + sqlDateTimeBeforeInsert.ToSqlValue
                    db.Execute(insertEventSql)
                End If

            End If

            Try
                If DataTransfer.AutoPriorityEnabled Then
                    Dim sql As String = "EXEC API_ParcelRefreshLogCurrent @type = '" + type.ToString() + "',@tempTableName = " + tempTableName + " ,@updateFilter = '" + updateFilter + "',@tableName = " + auxTableName
                    db.Execute(sql)
                End If
            Catch e As Exception
                Throw New Exception("An error occurred while processing your request. ", e)
            End Try
        End Sub

        Private Shared Sub loadNeighborhoodTableForResetOrRefresh(ByVal db As Database, ByVal tempTableName As String, ByRef recordTracker As RecordCountInfo)
            Dim nbhdField As String = db.Application.NeighborhoodNumberField
            Dim nbhdNameField As String = db.Application.NeighborhoodNameField
            If nbhdNameField Is Nothing OrElse nbhdNameField.Trim = "" Then
                nbhdNameField = nbhdField
            End If
            recordTracker.recordsInserted += db.GetIntegerValue("INSERT INTO Neighborhood (Number, Name) SELECT " + nbhdField + ", " + nbhdNameField + " FROM [" + tempTableName + "] d LEFT OUTER JOIN Neighborhood p ON p.Number = CAST(d." + nbhdField + " AS VARCHAR(50)) WHERE p.Id IS NULL")
        End Sub

        Private Shared Sub loadNeighborhoodDataForAll(ByVal db As Database, ByVal dt As DataTable, ByVal tempTableName As String, ByRef recordTracker As RecordCountInfo)
            Dim nbhdField As String = db.Application.NeighborhoodNumberField

            Dim filter As String = "rtrim(d." + nbhdField + ") = p.Number"
            Dim filter2 As String = "rtrim(d." + nbhdField + ") = pd." + nbhdField
            Dim updateFilter As String = "NeighborhoodData." + nbhdField + " = rtrim(d." + nbhdField + ")"

            Dim tableFields = db.GetDataTable("SELECT f.Name FROM DataSourceField f INNER JOIN DataSourceTable t ON f.TableId = t.Id WHERE t.Name = {0}".SqlFormatString(dt.TableName)).Rows.Cast(Of DataRow)().Select(Function(x) x.GetString("Name")).ToArray

            Dim insertStatement As String = "INSERT INTO NeighborhoodData (NbhdId"
            Dim updateStatement As String = "UPDATE NeighborhoodData SET CC_LastUpdateTime = GETUTCDATE()"
            Dim selectFields As String = "p.Id"
            For Each dc As DataColumn In dt.Columns
                If tableFields.Contains(dc.ColumnName) Then
                    insertStatement += ", [" + dc.ColumnName + "]"
                    selectFields += ", d.[" + dc.ColumnName + "]"
                    updateStatement += ", NeighborhoodData.[" + dc.ColumnName + "] = d.[" + dc.ColumnName + "]"
                End If
            Next

            insertStatement += ") "
            insertStatement += "SELECT " + selectFields + " FROM " + tempTableName + " d INNER JOIN Neighborhood p ON " + filter + " LEFT OUTER JOIN NeighborhoodData pd ON " + filter2 + " WHERE pd.ROWUID IS NULL"
            updateStatement += " FROM [" + tempTableName + "] d WHERE " + updateFilter
            recordTracker.recordsInserted += db.Execute(insertStatement)
            recordTracker.recordsUpdated += db.Execute(updateStatement)
        End Sub

        Private Shared Sub loadLookupDataForAll(ByVal db As Database, ByVal dt As DataTable, ByVal tempTableName As String, ByRef recordTracker As RecordCountInfo)
            Dim lookupTableName As String = DataSource.FindTargetTable(db, dt.TableName)

            Dim rowExistsJoinCondition As String = ""
            Dim updateFilter As String = ""

            Dim rowExistsJoinConditionMain As String = ""
            Dim updateFilterMain As String = ""

            Dim keys = _getKeys(db, dt.TableName)
            For Each key In keys
                If key <> "" Then
                    Dim dataField As String = "[" + key + "]"
                    If rowExistsJoinConditionMain <> "" Then rowExistsJoinConditionMain += " AND "
                    If updateFilterMain <> "" Then updateFilterMain += " AND "
                    rowExistsJoinConditionMain += "d." + dataField + " = " + "pd." + dataField
                    updateFilterMain += lookupTableName + "." + dataField + " = " + "d." + dataField
                End If
            Next

            Dim tableFields = db.GetDataTable("SELECT f.Name FROM DataSourceField f INNER JOIN DataSourceTable t ON f.TableId = t.Id WHERE t.Name = {0}".SqlFormatString(dt.TableName)).Rows.Cast(Of DataRow)().Select(Function(x) x.GetString("Name")).ToArray

            Dim insertStatement As String = "INSERT INTO " + lookupTableName + " (CC_LastUpdateTime"
            Dim updateStatement As String = "UPDATE " + lookupTableName + " SET CC_LastUpdateTime = GETUTCDATE()"


            Dim selectFields As String = "GETUTCDATE()"
            For Each dc As DataColumn In dt.Columns
                If tableFields.Contains(dc.ColumnName) Then
                    insertStatement += ", [" + dc.ColumnName + "]"
                    selectFields += ", d.[" + dc.ColumnName + "]"
                    updateStatement += ", " + lookupTableName + ".[" + dc.ColumnName + "] = d.[" + dc.ColumnName + "]"
                End If
            Next
            If rowExistsJoinConditionMain <> "" Then
                insertStatement += ") "
                insertStatement += "SELECT " + selectFields + " FROM " + tempTableName + " d LEFT OUTER JOIN " + lookupTableName + " pd ON " + rowExistsJoinConditionMain + " WHERE pd.ROWUID IS NULL"
                updateStatement += " FROM [" + tempTableName + "] d WHERE " + updateFilterMain
            Else
                Throw New Exception("The schema relations for table  " + dt.TableName + "  are incorrect.")
            End If


            recordTracker.recordsInserted += db.Execute(insertStatement)
            recordTracker.recordsUpdated += db.Execute(updateStatement)
        End Sub

        Public Shared Sub MapStreetAddressTable(ByVal db As Database, ByVal dt As DataTable, ByVal tempTableName As String, targetTable As String)

            Dim filterFormula = ""
            Dim streetAddressField = StreetAddress.GetStreetAddressField(db)
            Dim streetNameField = StreetAddress.GetStreetNameField(db)
            Dim streetNumberField = StreetAddress.GetStreetNumberField(db)
            Dim streetNameSuffixField = StreetAddress.GetStreetNameSuffixField(db)
            Dim streetAddressTable = StreetAddress.GetStreetAddressTable(db)
            Dim streetAddressFilter = StreetAddress.GetStreetAddressFormula(db)
            Dim cityField = StreetAddress.GetCityField(db)

            If streetAddressTable = "" Or IsNothing(streetAddressTable) Or streetAddressField = "" Or IsNothing(streetAddressField) Then
                Throw New Exception("StreetAddress configuration fields are not setup. Please use application configuration API or page to fill this information.")
            End If

            Dim updateColumns = ""

            If streetAddressField <> "" AndAlso IsNothing(streetAddressField) = False Then
                updateColumns = "StreetAddress=" + Regex.Replace(streetAddressField, "[A-Za-z][A-Za-z_0-9]{2,}", "ISNULL(LTRIM(RTRIM(s.$0 ))+ ' ', '')")
            End If

            If streetNameField <> "" AndAlso IsNothing(streetNameField) = False Then
                If updateColumns <> "" Then
                    updateColumns += ","
                End If
                updateColumns += "StreetName=s." + streetNameField
            End If

            If streetNumberField <> "" AndAlso IsNothing(streetNumberField) = False Then
                If updateColumns <> "" Then
                    updateColumns += ","
                End If
                updateColumns += "StreetNumber=s." + streetNumberField
            End If

            If streetNameSuffixField <> "" AndAlso IsNothing(streetNameSuffixField) = False Then
                If updateColumns <> "" Then
                    updateColumns += ","
                End If
                updateColumns += "StreetNameSuffix=s." + streetNameSuffixField
            End If

            If streetAddressFilter <> "" AndAlso IsNothing(streetAddressFilter) = False Then
                filterFormula = " AND " + "s." + streetAddressFilter
            End If

            If cityField <> "" AndAlso IsNothing(cityField) = False Then
                If updateColumns <> "" Then
                    updateColumns += ", "
                End If
                updateColumns += "CC_City=s.[" + cityField + "]"
            End If

            Dim rowExistsJoinConditionMain = " INNER JOIN " + tempTableName + " d ON "
            Dim connectingCondition = ""
            Dim keys = _getKeys(db, dt.TableName)

            For Each key In keys
                If key <> "" Then
                    If connectingCondition <> "" Then
                        connectingCondition += " AND "
                    End If
                    Dim dataField As String = "[" + key + "]"
                    connectingCondition += "s." + dataField + " = " + "d." + dataField
                End If
            Next

            If connectingCondition <> "" Then
                rowExistsJoinConditionMain += connectingCondition
            Else
                rowExistsJoinConditionMain = ""
            End If

            Dim updateSql = "UPDATE Parcel SET  " + updateColumns + "  FROM  " + targetTable + " s  " + rowExistsJoinConditionMain + "  WHERE Parcel.Id = s.CC_ParcelId " + filterFormula
            db.Execute(updateSql)

            Dim updateSAddressSql = "UPDATE Parcel SET  StreetAddress=LTRIM(RTRIM(replace(replace(replace(StreetAddress,' ','<>'),'><',''),'<>',' ')))   FROM  " + targetTable + " s  " + rowExistsJoinConditionMain + "  WHERE Parcel.Id = s.CC_ParcelId " + filterFormula
            db.Execute(updateSAddressSql)

        End Sub

        'Public Shared Function DataSource.FindTargetTable(ByVal db As Database, ByVal tableName As String, Optional ByRef targetTableOrginalName As String = "", Optional ByRef isLookupTable As Boolean = False) As String
        '    Dim sql As String = "SELECT t2.Name FROM DataSourceRelationships r INNER JOIN DataSourceTable t1 ON r.TableId = t1.Id AND r.Relationship = 0 INNER JOIN DataSourceTable t2 ON r.ParentTableId = t2.Id WHERE t1.Name = {0}"
        '    Dim parentTable As String = "", targetTable As String = tableName
        '    While True
        '        parentTable = db.GetStringValue(sql.SqlFormatString(targetTable))
        '        If parentTable = "" Then
        '            Exit While
        '        Else
        '            targetTable = parentTable
        '        End If
        '    End While

        '    targetTableOrginalName = targetTable

        '    Dim noOfRelations As Integer = db.GetIntegerValue("SELECT COUNT(*) FROM DataSourceRelationships r INNER JOIN DataSourceTable t1 ON r.TableId = t1.Id AND r.Relationship IS NOT NULL WHERE t1.Name = {0}".SqlFormatString(tableName))

        '    If targetTable.Trim.ToLower = db.Application.ParcelTable.Trim.ToLower Then
        '        Return "ParcelData"
        '    ElseIf targetTable.Trim.ToLower = db.Application.NeighborhoodTable.Trim.ToLower Then
        '        Return "NeighborhoodData"
        '    ElseIf targetTable <> String.Empty AndAlso noOfRelations > 0 Then
        '        Return "XT_" + targetTable
        '    ElseIf noOfRelations = 0 Then
        '        isLookupTable = True
        '        Return "LT_" + targetTable
        '    Else
        '        Return String.Empty
        '    End If
        'End Function

        Public Shared Function FindParentTable(ByVal db As Database, ByVal tableName As String, ByRef parentTargetTable As String) As String
            Dim sql As String = "SELECT t2.Name FROM DataSourceRelationships r INNER JOIN DataSourceTable t1 ON r.TableId = t1.Id AND r.Relationship IN (0, 1, 2) INNER JOIN DataSourceTable t2 ON r.ParentTableId = t2.Id WHERE t1.Name = {0}"
            Dim parentTable As String = "", targetTable As String = tableName
            parentTable = db.GetStringValue(sql.SqlFormatString(targetTable))
            parentTargetTable = DataSource.FindTargetTable(db, parentTable)
            Return parentTable
        End Function
        Public Shared Function AllowOrphanInsert(ByVal db As Database, ByVal tableName As String) As Boolean
            Dim sql As String = "SELECT count(*) FROM  DataSourceTable  WHERE Name = {0} AND AllowOrphanInsert=1".SqlFormat(False, tableName)
            Dim OrphanInsert As Integer = db.GetIntegerValue(sql)
            If OrphanInsert = 1 Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Sub PrepareTables(ByVal db As Database)
            Dim tables As New Dictionary(Of String, SortedList(Of String, SchemaField))
            Dim lookupTables As New List(Of String)
            DataSource.CategorizeDataTables(db)
            Dim allTables = db.GetDataTable("SELECT Name FROM DataSourceTable WHERE ImportType IN (0, 1, 3, 5)").Rows.Cast(Of DataRow)().Select(Function(x) x.GetString("Name")).ToArray
            For Each tableName As String In allTables
                Dim isLookupTable As Boolean = False
                Dim targetTable As String = DataSource.FindTargetTable(db, tableName, , isLookupTable)
                If Not tables.ContainsKey(targetTable) Then
                    tables.Add(targetTable, New SortedList(Of String, SchemaField))
                End If
                Dim table = tables(targetTable)
                Dim tableFields = db.GetDataTable("SELECT f.* FROM DataSourceField f INNER JOIN DataSourceTable t ON f.TableId = t.Id WHERE t.Name = {0}".SqlFormatString(tableName)).Rows.Cast(Of DataRow)().Select(Function(x) New SchemaField With {.Name = x.GetString("Name"), .DataType = x.GetInteger("DataType"), .MaxLength = x.GetInteger("MaxLength"), .Precision = x.GetInteger("NumericPrecision"), .Scale = x.GetInteger("NumericScale")}).ToArray
                For Each field In tableFields
                    If Not table.ContainsKey(field.Name) Then
                        table.Add(field.Name, field)
                    End If
                Next

                If isLookupTable Then
                    If Not lookupTables.Contains(targetTable) Then
                        lookupTables.Add(targetTable)
                    End If
                End If
            Next

            For Each tname As String In tables.Keys
                If Not db.DoesTableExists(tname) Then
                    Dim isLookupTable As Boolean = lookupTables.Contains(tname)
                    Dim referenceField As String = "CC_ParcelId"
                    Dim referenceDeclaration As String = referenceField + " INT REFERENCES Parcel(Id)"
                    If tname = "NeighborhoodData" Then
                        referenceField = "NbhdId"
                        referenceDeclaration = referenceField + " INT REFERENCES Neighborhood(Id)"
                    End If
                    Dim createSql As String = "CREATE TABLE [{0}] (ROWUID INT IDENTITY PRIMARY KEY, ParentROWUID int, ClientROWUID BIGINT, ClientParentROWUID BIGINT, " + referenceDeclaration + ", CC_LastUpdateTime DATETIME NOT NULL DEFAULT GETUTCDATE(), CC_Deleted BIT NOT NULL DEFAULT 0)"
                    If tname = "ParcelData" Then
                        createSql = "CREATE TABLE [{0}] (ROWUID INT IDENTITY PRIMARY KEY, ParentROWUID int, ClientROWUID BIGINT, ClientParentROWUID BIGINT, " + referenceDeclaration + ", CC_LastUpdateTime DATETIME NOT NULL DEFAULT GETUTCDATE(), CC_Deleted BIT NOT NULL DEFAULT 0,CC_LinkedROWUID INT  NULL,CC_YearStatus CHAR(1)  NULL,CC_RecordStatus CHAR(1)  NULL)"
                    End If
                    If tname.ToLower.StartsWith("xt") Then
                        createSql = "CREATE TABLE [{0}] (ROWUID INT IDENTITY PRIMARY KEY, ParentROWUID int, ClientROWUID BIGINT, ClientParentROWUID BIGINT, " + referenceDeclaration + ", CC_LastUpdateTime DATETIME NOT NULL DEFAULT GETUTCDATE(), CC_Deleted BIT NOT NULL DEFAULT 0,CC_RecordStatus CHAR(1)  NULL DEFAULT 'I',CC_LinkedROWUID INT  NULL,CC_YearStatus CHAR(1)  NULL)"
                    End If
                    If isLookupTable Then
                        createSql = "CREATE TABLE [{0}] (ROWUID INT IDENTITY PRIMARY KEY, CC_LastUpdateTime DATETIME NOT NULL DEFAULT GETUTCDATE(), CC_Deleted BIT NOT NULL DEFAULT 0)"
                    End If
                    db.Execute(createSql.FormatString(tname))
                    If Not isLookupTable Then
                        Try
                            db.Execute("CREATE INDEX IX_{0}_{1} ON {0}([{1}]);".FormatString(tname, referenceField))
                        Catch ex As Exception
                            Throw New Exception("Error on creating indexes for prime reference fields for [" & tname & "] on [" & referenceField & "]", ex)
                        End Try

                        db.Execute("CREATE INDEX IX_{0}_{1} ON {0}([{1}]);".FormatString(tname, "ClientROWUID"))
                    End If
                    db.Execute("CREATE INDEX IX_{0}_{1} ON {0}([{1}]);".FormatString(tname, "CC_Deleted"))
                End If

                For Each field In tables(tname).Values
                    If (db.GetIntegerValue("SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ={0} AND COLUMN_NAME ={1}".SqlFormatString(tname, field.Name)) = 0) Then
                        Dim alterSql As String = "ALTER TABLE [{0}] ADD [{1}] {2};".FormatString(tname, field.Name, field.SqlTypeDeclaration)
                        db.Execute(alterSql)
                    End If

                Next
            Next


            db.Execute("EXEC ccad_RebuildAllIndexes")
        End Sub

        Public Shared Function CleanTable(ByVal type As BulkDataTransferType, ByVal db As Database, ByVal request As DataTransferRequest) As StandardResponse

            Dim targetTables = DataSource.GetTargetTables(db)

            '       Update System Status
            '       Clean up system if required
            '       Unlock account is not set as DoNotUnlock
            Dim resp As New StandardResponse

            Dim jobData As String = TransferJob.GetData(db, request.jobId)
            Dim tableNamesInJob As List(Of String) = jobData.Split(",").ToList
            Dim parcelTableName As String = db.Application.ParcelTable

            Dim dnkLog As New Dictionary(Of String, Integer)

            If tableNamesInJob.Count > 0 Then
                Dim dnkTables = db.GetDataTable("SELECT Name FROM DataSourceTable WHERE (DoNotKeepCondition IS NOT NULL AND DoNotKeepCondition <> '') AND Name IN ( " + String.Join(",", tableNamesInJob.Select(Function(x) x.ToSqlValue).ToArray) + ")").AsEnumerable.Select(Function(x) x.GetString("Name")).ToArray
                For Each tname In dnkTables
                    If request.tableNames.Contains(tname) Then
                        Dim targetTable = targetTables(tname)
                        Dim sql As String
                        If tname = parcelTableName Then
                            sql = "EXEC cc_CleanParcelsDNK"
                        Else
                            sql = "EXEC cc_CleanTableDNK @SchemaTableName = {0}".SqlFormat(False, tname)
                        End If
                        Dim recordsDeleted As Integer = db.GetIntegerValue(sql)
                        dnkLog.Add(tname, recordsDeleted)
                    End If

                Next
            End If


            If type = BulkDataTransferType.Clean Then
                TransferJob.VerifyFull(db, request, TransferTypes.DataTransfer)

                If request.tableNames Is Nothing OrElse request.tableNames.Length = 0 Then
                    Throw New Exceptions.BadRequestException("Clean Table request requires parameter 'tableNames'. Expected value is a string array containing names of tables to be cleaned.", Nothing, False)
                End If

                ''' Verify tableNames against TransferData
                ''' BEGIN VERIFY

                Dim missingTables As New List(Of String)
                For Each t In request.tableNames
                    Dim rt = t.ToLower.Trim
                    If Not tableNamesInJob.Contains(rt) Then
                        missingTables.Add(t)
                    End If
                Next
                If missingTables.Count > 0 Then
                    Dim errorMessage As String = "Following tables are not refreshed in the current transfer job - " + missingTables.Aggregate(Function(x, y) x + ", " + y)
                    Throw New Exceptions.BadRequestException("Clean Table request aborted. " + errorMessage, Nothing, False)
                End If
                ''' END VERIFY
                '''  

                Dim cleanLog As New Dictionary(Of String, Integer)

                Dim cleanReferenceTime As DateTime = TransferJob.GetStartTime(db, request)
                Dim cleanParcels As Boolean = False
                Dim parcelsDeleted As Integer

                For Each tableName As String In request.tableNames
                    Dim targetTable As String = targetTables(tableName)
                    If tableName = parcelTableName Then
                        cleanParcels = True
                    Else
                        If Not cleanLog.ContainsKey(tableName) Then
                            Dim startTime = Date.Now
                            Dim sql As String = "EXEC cc_CleanTable @SchemaTableName = {0}, @ReferenceTime = {1}".SqlFormat(False, tableName, cleanReferenceTime)
                            Dim recordsDeleted As Integer = db.GetIntegerValue(sql)
                            'Dim recordsDeleted As Integer = db.Execute("DELETE FROM [" + targetTable + "] WHERE CC_LastUpdateTime < " & cleanReferenceTime.ToSqlValue)
                            cleanLog.Add(tableName, recordsDeleted)

                            Dim timeElapsed = (Date.Now - startTime).TotalSeconds
                            Dim aet As AuditEventType = DTX_47008_TABLE
                            SystemAuditStream.CreateStandardEvent(db, aet, request.jobId, tableName, timeElapsed, recordsDeleted)

                        End If
                    End If
                Next
                If cleanParcels Then
                    Dim startTime = Date.Now
                    parcelsDeleted = db.GetIntegerValue("EXEC cc_CleanOlderParcels @CleanTime = " + cleanReferenceTime.ToSqlValue)
                    cleanLog.Add(parcelTableName, parcelsDeleted)

                    Dim timeElapsed = (Date.Now - startTime).TotalSeconds
                    Dim aet As AuditEventType = DTX_47008_TABLE
                    SystemAuditStream.CreateStandardEvent(db, aet, request.jobId, parcelTableName, timeElapsed, parcelsDeleted)
                End If

                'Dim cleanResponseData As New Dictionary(Of String, Integer)
                '           For Each tableName In request.tableNames
                ''Dim targetTable As String = DataSource.FindTargetTable(db, tableName)
                'Dim recordsDeleted = cleanLog(tableName)
                '               cleanResponseData.Add(tableName, recordsDeleted)
                '           Next
                If cleanLog.Count > 0 Then
                    resp.data = cleanLog
                    resp.AddMessage("Clean operation successful. Count of records deleted is available in [response.data].")
                    If cleanParcels Then
                        resp.AddMessage(parcelsDeleted & " parcels deleted by clean operation.")
                    End If
                End If
            End If



            Return resp
        End Function


        Public Shared Function GetRelatedTables(ByVal db As Database) As List(Of String)
            Dim tables As New List(Of String)
            _getConnectedTables(db, tables, db.Application.ParcelTable)
            If db.Application.NeighborhoodTable.IsNotEmpty Then
                _getConnectedTables(db, tables, db.Application.NeighborhoodTable)
            End If
            _getNonConnectedTables(db, tables)
            Return tables
        End Function

        Private Shared Sub _getConnectedTables(ByVal db As Database, ByVal tables As List(Of String), ByVal tableName As String)
            If Not tables.Contains(tableName) Then tables.Add(tableName)
            For Each connectTableName In db.GetDataTable("SELECT t.Name, r.Relationship FROM DataSourceRelationships r INNER JOIN DataSourceTable t ON r.TableId = t.Id INNER JOIN DataSourceTable p ON r.ParentTableId = p.Id WHERE p.Name = '{0}' ORDER BY Relationship".FormatString(tableName)).Rows.OfType(Of DataRow).Select(Function(x) x.GetString("Name")).ToArray
                _getConnectedTables(db, tables, connectTableName)
            Next
        End Sub

        Private Shared Sub _getNonConnectedTables(ByVal db As Database, ByVal tables As List(Of String))
            For Each tableName In db.GetDataTable("SELECT t.Name FROM DataSourceTable t LEFT OUTER JOIN DataSourceRelationships r ON t.Id = r.TableId AND r.Relationship IS NOT NULL WHERE t.Name NOT LIKE '#_%' ESCAPE '#' AND t.Name NOT IN (SELECT ParcelTable FROM Application UNION SELECT NeighborhoodTable FROM Application UNION SELECT '_photo_meta_data') GROUP BY t.Name HAVING COUNT(r.TableId) = 0").Rows.OfType(Of DataRow).Select(Function(x) x.GetString("Name")).ToArray
                If Not tables.Contains(tableName) Then tables.Add(tableName)
            Next
        End Sub

        Private Shared Function _getKeys(ByVal db As Database, ByVal tableName As String) As String()
            ' Dim sqlKeys As String = "SELECT f.Name FROM DataSourceField f INNER JOIN  DataSourceKeys k ON f.Id = k.FieldId INNER JOIN  DataSourceRelationships r ON k.RelationshipId = r.Id INNER JOIN DataSourceTable t ON r.TableId = t.Id WHERE r.Relationship IS NULL AND t.Name = '" + tableName + "'"
            'Dim sqlKeys As String = "SELECT f.Name FROM DataSourceField f INNER JOIN  DataSourceKeys k ON f.Id = k.FieldId INNER JOIN  DataSourceRelationships r ON k.RelationshipId = r.Id INNER JOIN DataSourceTable t ON r.TableId = t.Id WHERE isPrimaryKey=1 AND t.Name = '" + tableName + "'"
            Dim sqlKeys As String = "SELECT DISTINCT f.Name FROM DataSourceField f INNER JOIN  DataSourceKeys k ON f.Id = k.FieldId INNER JOIN  DataSourceRelationships r ON k.RelationshipId = r.Id INNER JOIN DataSourceTable t ON r.TableId = t.Id WHERE  t.Name = '" + tableName + "'"
            Dim keys = db.GetDataTable(sqlKeys).Rows.Cast(Of DataRow)().Select(Function(x) x.GetString("Name")).ToArray
            Return keys
        End Function

        Private Shared Function _getConnectingKeys(ByVal db As Database, ByVal tableName As String, ByVal parentTableName As String) As String()()
            Dim sqlKeys As String = "SELECT t1.Name TableName, t2.Name ParentTableName, f1.Name As FieldName, f2.Name As RelatedFieldName FROM DataSourceRelationships r INNER JOIN DataSourceTable t1 ON r.TableId = t1.Id INNER JOIN DataSourceTable t2 ON r.ParentTableId = t2.Id INNER JOIN  DataSourceKeys k ON k.RelationshipId = r.Id INNER JOIN DataSourceField f1 ON  f1.Id = k.FieldId INNER JOIN DataSourceField f2 ON  f2.Id = k.ReferenceFieldId WHERE t1.Name = {0} AND t2.Name = {1}".SqlFormatString(tableName, parentTableName)
            Dim keys = db.GetDataTable(sqlKeys).Rows.Cast(Of DataRow)().Select(Function(x) New String() {x.GetString("FieldName"), x.GetString("RelatedFieldName")}).ToArray
            Return keys
        End Function

        Public Shared Function _getParcelUpdateSql(ByVal db As Database, ByVal tableName As String, tempTableName As String) As String
            Dim level As Integer = 0
            Dim ta As String = "t"
            Dim parentTableName As String = "", parentTargetTableName As String = ""
            Dim targetTable As String = ""
            targetTable = DataSource.FindTargetTable(db, tableName)
            parentTableName = FindParentTable(db, tableName, parentTargetTableName)

            Dim pkeys = _getKeys(db, tableName)


            Dim sql As String = "UPDATE t0 SET CC_ParcelId = pd.CC_ParcelId FROM " + targetTable + " t0"
            If tempTableName <> "" Then
                sql += " INNER JOIN " + tempTableName + " td ON "
                sql += pkeys.Select(Function(x) "t0.[" + x + "] = td.[" + x + "]").Aggregate(Function(x, y) x + " AND " + y)
            End If

            Dim lta As String = "t0"
            While True
                level += 1
                If parentTargetTableName = "ParcelData" Then
                    ta = "pd"
                Else
                    ta = "t" & level
                End If
                Dim keys = _getConnectingKeys(db, tableName, parentTableName)
                sql += " LEFT OUTER JOIN " + parentTargetTableName + " " + ta + " ON " + keys.Select(Function(x) lta + "." + x(0) + " = " + ta + "." + x(1)).Aggregate(Function(x, y) x + " AND " + y)

                If parentTargetTableName = "ParcelData" Then
                    Exit While
                End If

                tableName = parentTableName
                targetTable = DataSource.FindTargetTable(db, tableName)
                parentTableName = FindParentTable(db, tableName, parentTargetTableName)
                lta = ta
            End While

            Return sql


        End Function




    End Class
End Namespace

