﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects
Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts
Imports System.IO
Imports System.Text.RegularExpressions

Namespace RemoteIntegration
    Public Class PhotoTransferHelper

#Region "Upload and Download"

#End Region

        Private Shared ReadOnly tempTableName As String = "temp_Page_ParcelImages"
        '        Private Shared ReadOnly sqlTempImageTable As String = <sql>
        'CREATE TABLE temp_Page_ParcelImages
        '(
        '	[Id] [int] ,
        '	[ParcelId] [int] NULL,
        '	[IsSketch] [bit] NOT NULL,
        '	[Storage] [int] NULL,
        '	[Path] [varchar](500) NULL,
        '	[CreatedTime] [datetime] NULL,
        '	[UploadTime] [datetime] NULL,
        '	[UploadedBy] [varchar](50) NULL,
        '	[IsPrimary] [bit] NULL,
        '	[ReferenceId] [varchar](50) NULL,
        '	[LocalId] [varchar](50) NULL,
        '	[DownSynced] [bit] NULL,
        '    [MetaData1] varchar (255) NULL,
        '    [MetaData2] varchar(255) NULL,
        '    [MetaData3] varchar(255) NULL,
        '    [MetaData4] varchar(255) NULL,
        '    [MetaData5] varchar(255) NULL,
        '    [MetaData6] varchar(255) NULL,
        '    [MetaData7] varchar(255) NULL,
        '    [MetaData8] varchar(255) NULL,
        '    [MetaData9] varchar(255) NULL,
        '    [MetaData10] varchar(255) NULL
        ')
        '                                </sql>

        Public Shared Function GetMatchingPhotos(ByVal db As Database, ByVal photo As ParcelPhoto)

            If (photo.parcel <> 0 AndAlso IsNothing(photo.parcel) = False) Then
            Else
                photo.parcel = Parcel.GetParcelId(db, photo.keys)
            End If
            photo.fileName = db.GetStringValue("SELECT Path FROM ParcelImages WHERE ParcelId={0}  AND IsSketch=0".SqlFormatString(photo.parcel))
            Return photo
        End Function

        Public Shared Function SyncPhoto(ByVal db As Database, ByVal photo As ParcelPhoto)

            If (photo.parcel <> 0 AndAlso IsNothing(photo.parcel) = False) Then
            Else
                photo.parcel = Parcel.GetParcelId(db, photo.keys)
            End If
            If (db.GetIntegerValue("SELECT COUNT(*) FROM Parcel WHERE Id={0}".SqlFormatString(photo.parcel)) > 0) Then
                Dim sql = "INSERT INTO ParcelImages (ParcelId, Path, UploadedBy) VALUES ({0}, {1}, NULL);"
                db.Execute(sql.SqlFormatString(photo.parcel, photo.fileName))
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function PhotoDownSync_Init(ByVal target As Database, jobId As Integer, ByVal request As PhotoSyncRequest) As PhotoDownloadResponse
            Dim startTime As DateTime = DateTime.UtcNow, endTime As DateTime = DateTime.UtcNow
            'DateTime.TryParse(request.startTime, startTime)
            'DateTime.TryParse(request.endTime, endTime)

            Dim parcelCount = 0, totalPhotos = 0
            If target.DoesTableExists(tempTableName) Then
                target.DropTable(tempTableName)
            End If
            If Not target.DoesTableExists(tempTableName) Then
                target.Execute("CREATE TABLE " + tempTableName + " (ParcelImageId INT)")
            End If
            target.Execute("DELETE FROM " + tempTableName)
            Dim insertSql
            If request.capturedDate Then
                DateTime.TryParse(request.startTime, startTime)
                DateTime.TryParse(request.endTime, endTime)
                insertSql = " INSERT INTO " + tempTableName + " (ParcelImageId) SELECT pi.Id FROM ParcelImages pi INNER JOIN parcel p on p.Id=pi.ParcelId LEFT OUTER JOIN ParcelChanges pc ON pc.ParcelId = p.Id AND pc.Action <> 'PhotoFlagMain' AND pc.QCApproved = 1 LEFT OUTER JOIN DataSourceField f ON pc.FieldId = f.Id AND f.IsCustomField <> 1  WHERE (p.FailedOnDownSync = 0 OR p.FailedOnDownSync IS NULL) AND p.QC=1 AND pi.IsSketch=0 AND pi.UploadedBy IS NOT NULL AND (pi.DownSynced=0 OR pi.DownSynced IS NULL)  And pi.CreatedTime BETWEEN {0} And {1} GROUP BY pi.Id ".SqlFormat(True, startTime, endTime)
            Else
                insertSql = "INSERT INTO " + tempTableName + " (ParcelImageId) Select pi.Id FROM ParcelImages pi INNER JOIN parcel p On p.Id=pi.ParcelId LEFT OUTER JOIN ParcelChanges pc On pc.ParcelId = p.Id And pc.Action <> 'PhotoFlagMain' AND pc.QCApproved = 1 LEFT OUTER JOIN DataSourceField f ON pc.FieldId = f.Id AND f.IsCustomField <> 1  WHERE (p.FailedOnDownSync = 0 OR p.FailedOnDownSync IS NULL) AND p.QC=1 AND pi.IsSketch=0 AND pi.UploadedBy IS NOT NULL AND (pi.DownSynced=0 OR pi.DownSynced IS NULL) GROUP BY pi.Id"
            End If
            If request.committedParcelsOnly Then
                insertSql += " HAVING COUNT(f.Id) = 0"
            End If
            target.Execute(insertSql) '.SqlFormat(True, startTime.ToString("yyyy-MM-dd HH:mm:ss"), endTime.ToString("yyyy-MM-dd HH:mm:ss")))
            GetParcelPhotoTotal(target, parcelCount, totalPhotos)

            Dim pageCount As Integer = Math.Ceiling(CSng(totalPhotos) / CSng(request.pageSize))
            TransferJob.UpdatePagingDetails(target, request.pageSize, IIf(pageCount = 0 AndAlso totalPhotos > 0, 1, pageCount), jobId)
            Return New PhotoDownloadResponse With {.jobId = jobId, .pageCount = IIf(pageCount = 0 AndAlso totalPhotos > 0, 1, pageCount), .totalParcels = parcelCount, .totalPhotos = totalPhotos}
        End Function

        Public Shared Function PhotoDownSync_Data(ByVal target As Data.Database, request As PhotoSyncRequest, ByVal pageIndex As Integer, ByVal pageSize As Integer, jobId As Integer) As List(Of ParcelPhoto)

            Dim parcelPhotoArray As New List(Of ParcelPhoto)

            Dim totalPages = TransferJob.GetTotalPages(target, jobId)
            If pageIndex > totalPages Then
                Throw New Exception("There is no page found in the referenced index.")
            End If
            If pageIndex = 0 Then
                pageIndex = 1
            End If
            pageIndex = (pageIndex - 1) * pageSize

            Dim dtPMF As DataTable = target.GetDataTable("SELECT * FROM DataSourceField WHERE TableId = (SELECT Id FROM DataSourceTable WHERE Name = '_photo_meta_data') AND AssignedName LIKE 'PhotoMetaField%' ORDER BY AssignedName")
            Dim metaKeys As New Collections.Specialized.NameValueCollection
            Dim metaSources As New Collections.Specialized.NameValueCollection
            For Each drPMF As DataRow In dtPMF.Rows
                Dim findex As Integer = CInt(drPMF.GetString("AssignedName").Replace("PhotoMetaField", ""))
                Dim fieldName As String = drPMF.GetString("Name")
                Dim metaSourceTable As String = drPMF.GetString("MetaDataSourceTable")

                metaKeys.Add("MetaData" & findex, fieldName)
                If metaSourceTable <> "" Then
                    Dim targetMetaSourceTables As String = ""
                    For Each mst In metaSourceTable.Split(",")
                        targetMetaSourceTables += DataSource.FindTargetTable(target, mst.Trim()) + ","
                    Next
                    metaSourceTable = targetMetaSourceTables.TrimEnd(",")
                    metaSources.Add("MetaData" & findex, metaSourceTable)
                End If
            Next

            'For i As Integer = 1 To 10
            '    Dim fieldName As String = target.GetStringValue("SELECT Name FROM DataSourceField WHERE TableId = (SELECT Id FROM DataSourceTable WHERE Name = '_photo_meta_data') AND AssignedName = 'PhotoMetaField" & i & "'")
            '    If fieldName IsNot Nothing AndAlso fieldName <> String.Empty Then
            '        metaKeys.Add("MetaData" & i, fieldName)
            '    End If
            'Next

            Dim sql = "SELECT pi.*,p.KeyValue1 FROM ParcelImages pi INNER JOIN " + tempTableName + " tpi ON pi.Id = tpi.ParcelImageId INNER JOIN Parcel p ON p.Id = pi.ParcelId ORDER BY tpi.ParcelImageId OFFSET {0} ROWS FETCH NEXT {1} ROWS ONLY".FormatString(pageIndex, pageSize)
            Dim dt As DataTable = target.GetDataTable(sql)

            Dim s3 As New S3FileManager()

            For Each dr As DataRow In dt.Rows
                Dim s3Path As String = dr.GetString("Path")

                Dim parcelphoto As New ParcelPhoto()
                parcelphoto.parcel = dr.GetInteger("ParcelId")
                parcelphoto.keys = DataSource.GetKeyValueListForTable(target, target.Application.ParcelTable, 0, parcelphoto.parcel)
                parcelphoto.uploadedBy = dr.GetString("UploadedBy")
                parcelphoto.isDefault = dr.GetBoolean("IsPrimary")
                parcelphoto.fileName = System.IO.Path.GetFileName(s3Path)
                parcelphoto.ccImageId = dr.GetString("Id")
                parcelphoto.referenceId = NullIfBlank(dr.GetString("referenceId"))
                parcelphoto.captureDate = dr.GetDate("CreatedTime")
                parcelphoto.downsyncedDate = dr.GetDate("DownsyncedDate")
                parcelphoto.newBPP = dr.GetBoolean("NewBPP")
                parcelphoto.keyfield1 = dr.GetString("KeyValue1")

                If Not request.downloadUrlsOnly Then
                    Dim ms As New MemoryStream()
                    For ac = 1 To 3
                        Try
                            ms = s3.GetFile(s3Path)
                            Dim data As Byte() = ms.ToArray()
                            parcelphoto.photo = Convert.ToBase64String(data)
                            Exit For
                        Catch ex As Exception
                            parcelphoto.photo = ""
                            target.Execute("UPDATE Parcel SET CC_Error='" + ex.Message.ToString() + "' WHERE ID = " & dr.GetInteger("ParcelId"))
                        End Try
                    Next
                    If parcelphoto.photo = "" Then
                        If ParcelEventLog.Enabled Then
                            ParcelEventLog.CreatePhotoEvent(target, ParcelEventLog.EventType.PhotoDownloadFailed, ParcelEventLog.ApplicationType.API, parcelphoto.parcel, parcelphoto.ccImageId)
                        End If
                    End If
                Else
                    parcelphoto.photo = s3.GetDownloadURL(s3Path)
                End If


                For Each key In metaKeys.Keys
                    Dim value = dr.GetString(key)
                    If metaSources.AllKeys.Contains(key) Then
                        Dim valueParts As String() = value.Split("$")
                        If valueParts.Length > 1 AndAlso IsNumeric(valueParts(1)) AndAlso CLng(valueParts(1)) < 0 Then
                            Dim firstPart As String = valueParts(0)
                            Dim metaSource As String = metaSources(key)
                            For Each mst In metaSource.Split(",")
                                Dim metaGetSql As String = "SELECT ROWUID FROM [" + mst + "] WHERE ClientROWUID = " & valueParts(1)
                                Dim metaNewValue As String = target.GetStringValue(metaGetSql)
                                If metaNewValue <> "" Then
                                    value = firstPart + "$" + metaNewValue
                                    Exit For
                                End If
                            Next
                        End If
                    End If
                    If parcelphoto.metadata Is Nothing Then
                        parcelphoto.metadata = New List(Of KeyValuePair(Of String, String))
                    End If
                    parcelphoto.metadata.Add(New KeyValuePair(Of String, String)(metaKeys(key), value))
                Next


                parcelPhotoArray.Add(parcelphoto)
            Next

            'target.Execute("UPDATE " + temp_TableName + " SET DownSynced=1 WHERE ID IN(SELECT Id FROM Temp_ParcelImages pi ORDER BY Id  OFFSET {0} ROWS FETCH NEXT {1} ROWS ONLY)".FormatString(pageIndex, pageSize))
            TransferJob.UpdateCurrentPage(target, (pageIndex + 1), jobId)
            Return parcelPhotoArray
        End Function


        'Public Shared Sub PhotoDownSync_CommitPage(ByVal target As Data.Database, ByRef pageIndex As Integer, ByRef pageSize As Integer)
        '    'target.Execute("UPDATE " + temp_TableName + " SET DownSynced=1 WHERE ID IN(SELECT Id FROM Temp_ParcelImages pi ORDER BY Id  OFFSET {0} ROWS FETCH NEXT {1} ROWS ONLY)".FormatString(pageIndex, pageSize))
        'End Sub

        Public Shared Sub PhotoDownSync_Commit(ByVal target As Data.Database, ByRef parcelCount As Integer, ByRef totalPhotos As Integer, ByVal request As PhotoSyncRequest)
            If request.stopped Then
                target.Execute("INSERT INTO ParcelAuditTrail (EventDate, EventTime, ParcelId, LoginID, EventType, Description,ApplicationType) SELECT GETUTCDATE(), GETUTCDATE(), ParcelId, NULL, 3, CAST(COUNT(*) AS VARCHAR(6)) + ' photo(s) transferred via API' As PhotoCount,'API' FROM temp_Page_ParcelImages tppi INNER JOIN ParcelImages pig ON tppi.ParcelImageId = pig.Id WHERE pig.downsynced = 1 GROUP BY ParcelId")
            Else
                target.Execute("UPDATE ParcelImages SET DownSynced=1 WHERE ID IN(SELECT ParcelImageId FROM " + tempTableName + ")")
                If ClientSettings.PropertyValue("PhotoDeleteAndRecover") = "1" Then
                    Dim dt As DataTable = target.GetDataTable("SELECT Id, Path FROM ParcelImages WHERE Id IN (SELECT ParcelImageId FROM " + tempTableName + ") AND CC_Deleted = 1")
                    If dt.Rows.Count > 0 Then
                        Dim s3 As New S3FileManager()
                        Dim errorList As String = s3.DeleteMultipleFile(dt)
                        target.Execute("DELETE FROM ParcelImages WHERE Id IN (SELECT ParcelImageId FROM " + tempTableName + ") AND CC_Deleted = 1")
                        If errorList <> "" Then
                            target.Execute("INSERT INTO AdditionalLogs(ParcelId, LoginId, Description) VALUES({0}, {1}, {2})".SqlFormatString(1, "PhotoCommit", "Error occured when deleting photos" + errorList + " from S3"))
                        End If
                    End If
                End If
                target.Execute("INSERT INTO ParcelAuditTrail (EventDate, EventTime, ParcelId, LoginID, EventType, Description,ApplicationType) SELECT GETUTCDATE(), GETUTCDATE(), ParcelId, NULL, 3, CAST(COUNT(*) AS VARCHAR(6)) + ' photo(s) transferred via API' As PhotoCount,'API' FROM temp_Page_ParcelImages tppi INNER JOIN ParcelImages pig ON tppi.ParcelImageId = pig.Id GROUP BY ParcelId")
            End If
            If ParcelEventLog.Enabled Then
                Dim sqlParcelEventLog As String = "INSERT INTO ParcelEventLog (ParcelId, EventId, ApplicationId, P1) SELECT ParcelId, 34, 0, Id FROM ParcelImages pi INNER JOIN temp_Page_ParcelImages ON Id = ParcelImageId"
                target.Execute(sqlParcelEventLog)
            End If
            GetParcelPhotoTotal(target, parcelCount, totalPhotos, True)
            target.Execute("DELETE FROM " + tempTableName + "")
            target.DropTable(tempTableName)
        End Sub

        Private Shared Sub GetParcelPhotoTotal(ByVal target As Data.Database, ByRef parcelCount As Integer, ByRef totalPhotos As Integer, Optional downSyncFilter As Boolean = False)
            Dim filter = ""
            If downSyncFilter Then
                filter = "WHERE DownSynced=1"
            End If
            totalPhotos = target.GetIntegerValue("SELECT COUNT(*) FROM " + tempTableName)
            parcelCount = target.GetDataTable("SELECT DISTINCT ParcelID FROM ParcelImages WHERE Id IN (SELECT ParcelImageId FROM " + tempTableName + ")").Rows.Count

        End Sub

        Public Shared Function UploadPhoto(ByVal target As Data.Database, ByVal request As PhotoSyncRequest, photo As ParcelPhoto, ByVal s3mgr As S3FileManager, metaKeys As Dictionary(Of String, String), ByRef photoList As List(Of PhotoList), PhotoLimit As Integer) As Boolean

            Dim uploadFlag = False
            Dim imageId As Integer
            Dim isPhotoLimitExceeded As Boolean = False
            If (photo.photo Is Nothing OrElse photo.photo.IsEmpty) Then
                If photo.ccImageId Is Nothing OrElse photo.ccImageId.IsEmpty Then
                    Throw New Exception("CCImageId cannot be null when the photo is not sent in the request.")
                Else
                    imageId = photo.ccImageId
                    If photo.parcel Is Nothing OrElse photo.parcel.Trim.IsEmpty Then
                        Dim ir As DataRow = target.GetTopRow("SELECT ParcelId FROM ParcelImages WHERE Id = " & imageId)
                        If ir Is Nothing Then
                            Throw New Exception("Photo not found with ccImageId=" & imageId)
                        End If
                        photo.parcel = ir.GetInteger("ParcelId")
                        photo.referenceId = ir.GetString("ReferenceId")
                        photo.fileName = ir.GetString("FileName")
                    End If
                End If
            Else
                Dim bytes() As Byte = System.Convert.FromBase64String(photo.photo)

                If photo.parcel Is Nothing AndAlso photo.keys IsNot Nothing AndAlso photo.keys.Count > 0 Then
                    photo.parcel = Parcel.GetParcelId(target, photo.keys)
                End If
                Dim photocount As Integer = target.GetIntegerValue("SELECT COUNT(*) FROM ParcelImages WHERE ParcelId = " & photo.parcel)
                If (photocount >= PhotoLimit) Then
                    isPhotoLimitExceeded = True
                End If
                Dim pkey = Parcel.GetKeyValue1(target, photo.parcel)
                pkey = Regex.Replace(pkey, "[^A-Za-z0-9\-_]", "")
                If pkey = "" Or photo.photo = "" Then
                    Return False
                End If
                Dim ms As New MemoryStream(bytes)
                Dim tenantKey = "org" + Database.System.GetStringValue("SELECT OrganizationId FROM ApplicationAccess WHERE AccessKey = {0} AND PassKey={1}".SqlFormatString(request.accessKey, request.passKey)).PadLeft(3, "0")
                If photo.fileName Is Nothing OrElse photo.fileName.Trim.IsEmpty Then
                    Throw New Exception("fileName cannot be null.")
                End If
                Dim ext = Path.GetExtension(photo.fileName.Trim())
                If ext Is Nothing OrElse ext.Trim.IsEmpty Then
                    Throw New Exception("Invalid extension type.")
                End If
                Dim fname = Path.GetFileNameWithoutExtension(photo.fileName.Trim())
                Dim filePath As String = photo.parcel.ToString() + "-" + fname
                filePath = Regex.Replace(filePath, "[^A-Za-z0-9\-_]", "")
                Dim s3Path As String = request.GetTenantKey + "/pi/" + S3FileManager.GetBrokenPath(pkey) + filePath + ext
                'Dim breakUp() As Integer
                'If pkey.Length <= 8 Then
                '    pkey = pkey.PadLeft(8, "0").ReverseString
                '    breakUp = New Integer() {2, 3, 3}
                'ElseIf pkey.Length <= 15 Then
                '    pkey = pkey.PadLeft(15, "0").ReverseString
                '    breakUp = New Integer() {4, 4, 4, 3}
                'Else
                '    pkey = pkey.PadLeft(20, "0").ReverseString
                '    breakUp = New Integer() {4, 4, 5, 4, 3}
                'End If
                'For Each count In breakUp
                '    If pkey.Length > 0 Then
                '        Dim part As String = pkey.Substring(0, Math.Min(count, pkey.Length))
                '        pkey = pkey.Substring(part.Length)
                '        s3Path += part + "/"
                '    End If
                'Next


                's3Path += filePath
                If s3mgr Is Nothing Then
                    s3mgr = New S3FileManager
                End If
                Dim s3Exception As Exception = Nothing
                Try
                    Try
                        s3mgr.UploadFile(ms, s3Path)
                    Catch ex As Exception
                        s3Exception = ex
                        Throw New Exception("Error on uploading file '" + photo.fileName + "' with " & ms.ToArray.Length.ToString & " bytes")
                    End Try
                Catch ex As Exception
                    Dim originalErrorMessage As String = ""
                    If s3Exception IsNot Nothing Then
                        originalErrorMessage = "; Original Message: " + s3Exception.Message
                    End If

                    Throw New Exception("S3 Upload Error; Path : " + s3Path + originalErrorMessage, ex)
                End Try

                If photo.referenceId IsNot Nothing Then
                    imageId = target.GetIntegerValue("SELECT Id FROM ParcelImages WHERE ReferenceId = " & photo.referenceId.ToSqlValue)
                    If imageId <> 0 Then
                        Dim sql As String = "UPDATE ParcelImages SET Path = {1}, Downsynced = 1 WHERE ReferenceId = {0}; "
                        target.Execute(sql.SqlFormatString(photo.referenceId, s3Path))
                    End If
                End If

                If photo.referenceId Is Nothing Or imageId = 0 Then
                    Dim sql = "INSERT INTO ParcelImages (ParcelId, Path, UploadedBy,IsPrimary, Downsynced) VALUES ({0}, {1}, NULL,{2}, 1);".SqlFormatString(photo.parcel, s3Path, photo.isDefault)
                    If isPhotoLimitExceeded Then
                        sql += " DELETE FROM ParcelImages WHERE Id = (SELECT TOP 1 Id FROM ParcelImages WHERE ParcelId = {0} AND IsPrimary = 0 ORDER BY UploadTime)".SqlFormatString(photo.parcel)
                    End If

                    imageId = target.GetIdentityFromInsert(sql)
                End If

            End If

            If IsNothing(photo.isDefault) = True Then
                photo.isDefault = False
            End If

            Dim updateSql As String = ""

            If photo.referenceId IsNot Nothing Then
                updateSql += "UPDATE ParcelImages SET ReferenceId = {1} WHERE Id = {0};".SqlFormatString(imageId, photo.referenceId)
            End If

            If photo.isDefault Then
                updateSql += vbNewLine + "UPDATE ParcelImages SET IsPrimary=0 WHERE ParcelId={0} AND Id <> {1};".SqlFormatString(photo.parcel, imageId)
                updateSql += vbNewLine + "UPDATE ParcelImages SET IsPrimary=1 WHERE ParcelId={0} AND Id = {1};".SqlFormatString(photo.parcel, imageId)
            End If

            If photo.metadata IsNot Nothing Then
                If photo.metadata.Count > 0 Then
                    Dim sqlMeta As String = ""
                    For Each kvp In photo.metadata
                        Dim metaValue As String = ""
                        If metaKeys.TryGetValue(kvp.Key, metaValue) Then
                            If sqlMeta <> "" Then sqlMeta += ", "
                            Dim value As String = "NULL"
                            If kvp.Value IsNot Nothing Then
                                value = kvp.Value.ToSqlValue
                            End If
                            sqlMeta += "[" + metaValue + "] = " + value
                        End If
                    Next
                    If sqlMeta <> "" Then
                        sqlMeta = "UPDATE ParcelImages SET " + sqlMeta + " WHERE Id = " & imageId & ";"
                        updateSql += vbNewLine + sqlMeta
                        target.Execute(sqlMeta)
                    End If
                End If
            End If

            If updateSql <> "" Then
                target.Execute(updateSql)
            End If

            photoList.Add(New PhotoList With {.fileName = photo.fileName, .ccImageId = imageId, .referenceId = If(photo.referenceId, Nothing), .parcelId = photo.parcel})
            uploadFlag = True

            If ParcelEventLog.Enabled Then
                ParcelEventLog.CreatePhotoEvent(target, ParcelEventLog.EventType.PhotoAdded, ParcelEventLog.ApplicationType.API, photo.parcel, imageId)
            End If

            Return uploadFlag

        End Function

        Public Shared Function DownloadPhoto(ByVal target As Data.Database, ByVal startTime As DateTime, ByVal endTime As DateTime) As List(Of ParcelPhoto)

            Dim parcelPhotoArray As New List(Of ParcelPhoto)

            Dim sql = "SELECT pi.* FROM ParcelImages pi INNER JOIN parcel p on p.Id=pi.ParcelId WHERE (p.FailedOnDownSync = 0 OR p.FailedOnDownSync IS NULL) AND p.QC=1 AND p.QCDate BETWEEN {0} AND {1}  AND pi.IsSketch=0 AND pi.UploadedBy IS NOT NULL AND (pi.DownSynced=0 OR pi.DownSynced IS NULL)"
            Dim dt As DataTable = target.GetDataTable(sql.SqlFormat(True, startTime.ToString("yyyy-MM-dd HH:mm:ss"), endTime.ToString("yyyy-MM-dd HH:mm:ss")))


            Dim metaKeys As New Dictionary(Of String, String)
            For i As Integer = 1 To 10
                Dim fieldName As String = target.GetStringValue("SELECT Name FROM DataSourceField WHERE TableId IN (SELECT Id FROM DataSourceTable WHERE Name = '_photo_meta_data') AND AssignedName = 'PhotoMetaField" & i & "'")
                If fieldName IsNot Nothing AndAlso fieldName <> String.Empty Then
                    metaKeys.Add(fieldName, "MetaData" & i)
                End If
            Next

            Dim s3 As New S3FileManager()
            Dim changeIds As New List(Of String)
            For Each dr As DataRow In dt.Rows
                Dim parcelphoto As New ParcelPhoto()
                parcelphoto.parcel = dr.GetInteger("ParcelId")
                Dim s3Path As String = dr.GetString("Path")
                parcelphoto.uploadedBy = dr.GetString("UploadedBy")

                parcelphoto.isDefault = dr.GetBoolean("IsPrimary")
                parcelphoto.fileName = System.IO.Path.GetFileName(s3Path)
                parcelphoto.ccImageId = dr.GetString("Id")
                parcelphoto.referenceId = NullIfBlank(dr.GetString("referenceId"))
                'parcelphoto.fileName = s3Path

                Dim ms As New MemoryStream()

                For ac = 1 To 3
                    Try
                        ms = s3.GetFile(s3Path)
                        Dim data As Byte() = ms.ToArray()
                        parcelphoto.photo = Convert.ToBase64String(data)
                        Exit For
                    Catch ex As Exception
                        parcelphoto.photo = ""
                    End Try
                Next


                For Each key In metaKeys.Keys
                    Dim value = dr.GetString(metaKeys(key))
                    If parcelphoto.metadata Is Nothing Then
                        parcelphoto.metadata = New List(Of KeyValuePair(Of String, String))
                    End If
                    parcelphoto.metadata.Add(New KeyValuePair(Of String, String)(key, value))
                Next

                changeIds.Add(parcelphoto.ccImageId)
                parcelPhotoArray.Add(parcelphoto)
            Next

            Dim ids As String = String.Join(",", changeIds.ToArray)
            If ids.Trim <> "" Then
                Dim updateSql As String = "UPDATE ParcelImages SET DownSynced = 1,LocalId=NULL WHERE Id IN (" + ids + ")"
                target.Execute(updateSql)
            End If

            Return parcelPhotoArray
        End Function



    End Class
End Namespace

