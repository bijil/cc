﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects
Imports System.IO.FileStream

Namespace RemoteIntegration

    Public Class PhotoStorageHelper

        Private Const PhotoIndexBackup As String = "_backup_ParcelImages"
        Shared photoId As Integer

        Public Shared Function BackupPhotoIndex(db As Database) As Integer
            If db.DoesTableExists(PhotoIndexBackup) Then
                Dim rowsInBackup As Integer = db.GetRowCount(PhotoIndexBackup)
                Dim rowsInTable As Integer = db.GetRowCount("ParcelImages")
                If rowsInBackup > 0 And rowsInTable = 0 Then
                    Return rowsInBackup
                End If
                db.DropTable(PhotoIndexBackup)
            End If

            Dim xmlBackupSql = <sql>
                                    SELECT
                                        pi.Id,
	                                    p.KeyValue1, 
	                                    p.KeyValue2, 
	                                    p.KeyValue3, 
	                                    p.KeyValue4, 
	                                    p.KeyValue5, 
	                                    p.KeyValue6, 
	                                    p.KeyValue7, 
	                                    p.KeyValue8, 
	                                    p.KeyValue9, 
	                                    p.KeyValue10, 
	                                    pi.IsSketch,
	                                    pi.Storage,
	                                    pi.Path,
	                                    pi.IsPrimary,
	                                    pi.ReferenceId,
	                                    pi.MetaData1, 
	                                    pi.MetaData2, 
	                                    pi.MetaData3, 
	                                    pi.MetaData4, 
	                                    pi.MetaData5, 
	                                    pi.MetaData6, 
	                                    pi.MetaData7, 
	                                    pi.MetaData8, 
	                                    pi.MetaData9, 
	                                    pi.MetaData10
                                    INTO __BACKUP_TARGET__
                                    FROM 
	                                    ParcelImages pi
                                    INNER JOIN Parcel p
                                    ON pi.ParcelId = p.Id WHERE DownSynced = 1 OR DownSynced IS NULL
                               </sql>

            Dim backupSql As String = xmlBackupSql.Value.Replace("__BACKUP_TARGET__", PhotoIndexBackup)
            Try
                Return db.Execute(backupSql)
            Catch ex As Exception
                Throw New Exception("Photo Index Backup failed.", ex)
            End Try
        End Function

        Public Shared Function RestorePhotoIndex(db As Database) As Integer
            If Not db.DoesTableExists(PhotoIndexBackup) Then
                Return 0
            End If
            Dim keyValueString As String = String.Empty
            Dim dr As DataRow = db.GetTopRow("SELECT * FROM Application")
            For i As Integer = 1 To 10
                Dim kf As String = dr.GetString("KeyField" & i)
                If kf <> "" Then
                    If keyValueString <> "" Then keyValueString += " AND "
                    keyValueString += "bpi.[KeyValue" & i & "] = p.[KeyValue" & i & "]"
                End If
            Next

            db.DeleteTableRecordsWithIdentityReset("ParcelImages")
            Dim restoreSql As String = "SET IDENTITY_INSERT ParcelImages ON; INSERT INTO ParcelImages (Id, ParcelId, IsSketch, Storage, Path, IsPrimary, ReferenceId, MetaData1, MetaData2, MetaData3, MetaData4, MetaData5, MetaData6, MetaData7, MetaData8, MetaData9, MetaData10) SELECT bpi.Id, p.Id, bpi.IsSketch, bpi.Storage, bpi.Path, bpi.IsPrimary, bpi.ReferenceId, bpi.MetaData1, bpi.MetaData2, bpi.MetaData3, bpi.MetaData4, bpi.MetaData5, bpi.MetaData6, bpi.MetaData7, bpi.MetaData8, bpi.MetaData9, bpi.MetaData10 FROM " + PhotoIndexBackup + " bpi INNER JOIN Parcel p ON (" + keyValueString + "); SET IDENTITY_INSERT ParcelImages OFF;"
            Try
                Dim rows As Integer = db.Execute(restoreSql)

                Return rows
            Catch ex As Exception
                Throw New Exception("Photo Index Restore failed.", ex)
            End Try
        End Function

        Public Shared Function GetParcelsWithoutPhotos(db As Database) As List(Of Integer)
            Dim sql As String = "SELECT p.Id FROM Parcel p LEFT OUTER JOIN ParcelImages pi ON (p.Id = pi.ParcelId) WHERE pi.Id IS NULL"
            Dim pids = db.GetDataTable(sql).AsEnumerable().Select(Function(x) x.GetInteger("Id")).ToList
            Return pids
        End Function

        Public Shared Function DownSyncPhotoMap(ByVal db As Database, ByVal photoMapList As List(Of ParcelPhoto), ByRef recordTracker As RecordCountInfo) As List(Of PhotoMap)

            Dim depictedPhotoList As New List(Of PhotoMap)
            For Each photoKeys As ParcelPhoto In photoMapList
                Dim photo As New PhotoMap
                Dim photoMapped As ParcelPhoto = PhotoTransferHelper.GetMatchingPhotos(db, photoKeys)

                If photoMapped.fileName <> "" Then
                    photo.fileName = photoMapped.fileName
                    photo.keys = photoMapped.keys
                    depictedPhotoList.Add(photo)
                Else
                    recordTracker.recordsIgnored += 1
                End If
            Next
            Return depictedPhotoList
        End Function

        Public Shared Function UpSyncPhotoMap(ByVal db As Database, ByVal photoMapList As List(Of ParcelPhoto), ByRef recordTracker As RecordCountInfo) As List(Of Integer)

            Dim conflictedList As New List(Of Integer)
            Dim recordIndex As Integer = 0

            Dim depictedPhotoList As New List(Of ParcelPhoto)
            For Each photo As ParcelPhoto In photoMapList
                Try
                    If (PhotoTransferHelper.SyncPhoto(db, photo)) Then
                        recordTracker.recordsInserted += 1
                    End If
                    recordIndex += 1
                Catch ex As Exception
                    conflictedList.Add(recordIndex)
                    recordTracker.recordsIgnored += 1
                End Try
            Next
            Return conflictedList
        End Function


        Public Shared Function SetMainFlag(ByVal db As Database, ByVal photoIds As List(Of PhotoFlag))

            Dim conflictList As New List(Of String)
            For Each flagId In photoIds
                Try
                    If (db.GetIntegerValue("SELECT COUNT(*) FROM ParcelImages where Id={0}".SqlFormatString(flagId.ccImageId)) > 0) Then
                        db.Execute("UPDATE ParcelImages SET IsPrimary=0 WHERE ParcelId={0}".SqlFormatString(flagId.parcel))
                        '  db.Execute("UPDATE ParcelImages SET IsPrimary=1 WHERE Id={0}".SqlFormatString(flagId.ccImageId))
                        db.Execute("UPDATE ParcelImages SET IsPrimary=1 WHERE Id={0} AND ParcelId={1} ".SqlFormatString(flagId.ccImageId, flagId.parcel))
                        If ParcelEventLog.Enabled Then
                            ParcelEventLog.CreatePhotoFlagEvent(db, ParcelEventLog.ApplicationType.API, flagId.parcel, flagId.ccImageId)
                        End If
                        conflictList.Add(flagId.ccImageId)
                    Else
                        conflictList.Add(flagId.ccImageId)
                    End If
                Catch ex As Exception
                    conflictList.Add(flagId.ccImageId)
                End Try
            Next


            Return conflictList
        End Function

        Public Shared Sub UpdateClientImageId(ByVal db As Database, ByVal photoIds As List(Of PhotoFlag), ByRef recordTracker As RecordCountInfo)
            Dim conflictList As New List(Of String)
            For Each imageId In photoIds
                If (db.GetIntegerValue("SELECT COUNT(*) FROM ParcelImages where Id={0}".SqlFormatString(imageId.ccImageId)) > 0) Then
                    db.Execute("UPDATE ParcelImages SET ReferenceId={1} WHERE Id={0}".SqlFormatString(imageId.ccImageId, imageId.referenceId))

                    recordTracker.recordsUpdated += 1
                    conflictList.Add(imageId.ccImageId)
                Else
                    conflictList.Add(imageId.ccImageId)
                End If
            Next
       
        End Sub

        Private Shared Function SqlFormatString(p1 As String()) As Integer
            Throw New NotImplementedException
        End Function

        Private Shared Function NameValue(p1 As String()) As Integer
            Throw New NotImplementedException
        End Function

        Private Shared Function ConflictMode(p1 As String()) As Object
            Throw New NotImplementedException
        End Function


    End Class

End Namespace
