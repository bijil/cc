﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects
Imports System.IO

Namespace RemoteIntegration
    Friend Class DataSyncProcessorHelper

        Public Shared Function GetNextQCStartDate(db As Database, request As DataTransferRequest) As DateTime
            Dim periodEnd As DateTime = request.periodEnd
            Dim nextQCDate As DateTime
            nextQCDate = db.GetTopRow("SELECT MIN(p.QCDate) As NextDate  FROM Parcel p INNER JOIN ParcelChanges pc ON p.Id = pc.ParcelId LEFT OUTER JOIN DataSourceField f ON pc.FieldId = f.Id WHERE p.QC = 1 AND pc.QCApproved = 1 AND (pc.FieldId IS NULL OR f.IsCustomField = 0) AND pc.Action <> 'PhotoFlagMain' AND p.QCDate > " + periodEnd.ToSqlValue).GetDate("NextDate")
            If nextQCDate = Nothing Then
                nextQCDate = #1/1/1999#
            End If
            Return nextQCDate
        End Function

    End Class
End Namespace