﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects
Public Class AuditTrailHelper
    Public Shared Function GetAuditTrail(ByVal target As Data.Database, ByVal startTime As DateTime, ByVal endTime As DateTime) As List(Of AuditTrail)
        Dim sql = "SELECT * FROM SystemAuditTrail WHERE EventTime BETWEEN {0} AND {1}"
        Dim dt As DataTable = target.GetDataTable(sql.SqlFormat(True, startTime.ToString("yyyy-MM-dd HH:mm:ss"), endTime.ToString("yyyy-MM-dd HH:mm:ss")))
        Dim auditList As New List(Of AuditTrail)
        For Each dr As DataRow In dt.Rows
            Dim audit As New AuditTrail
            audit.eventDate = dr.GetDate("EventDate")
            audit.eventTime = dr.GetDate("EventTime")
            audit.eventCode = dr.GetInteger("EventCode")
            audit.source = dr.GetString("Source")
            audit.description = dr.GetString("Description")
            audit.isError = dr.GetBoolean("IsError")
            audit.loginId = dr.GetString("LoginId")
            audit.iPAddress = dr.GetString("IPAddress")
            audit.data = dr.GetString("Data")
            auditList.Add(audit)
        Next
        Return auditList
    End Function
End Class
