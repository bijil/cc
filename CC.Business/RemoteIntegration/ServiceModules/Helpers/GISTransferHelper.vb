﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts

Public Class GISTransferHelper

    Public Shared Sub SaveParcelGISRecords(db As Database, requestType As String, dataRecords As GISParcel(), ByRef recordTracker As RecordCountInfo)
        If recordTracker Is Nothing Then
            recordTracker = New RecordCountInfo
        End If
        If db.DoesTableExists("temp_ParcelMap") Then
            db.DropTable("temp_ParcelMap")
        End If
        If db.DoesTableExists("temp_ParcelMapPoints") Then
            db.DropTable("temp_ParcelMapPoints")
        End If

        If Not db.DoesTableExists("temp_ParcelMapPoints") Then
            Dim createSql As String = "CREATE TABLE temp_ParcelMapPoints (ROWID INT IDENTITY PRIMARY KEY, TempId INT, Latitude FLOAT, Longitude FLOAT, Ordinal INT); CREATE INDEX IX_temp_PMP ON temp_ParcelMapPoints(TempId ASC);"
            db.Execute(createSql)
        End If

        Dim joinCondition As String = ""
        Dim keyJoinCondition As String = ""

        'Trial loop, execute only for first record - to create temp_ParcelMap and to set joinCondition
        For Each pg As GISParcel In dataRecords
            If Not db.DoesTableExists("temp_ParcelMap") Then
                Dim createSql As String = "CREATE TABLE temp_ParcelMap(Id INT IDENTITY PRIMARY KEY"
                For Each k In pg.parcelKeys.Keys
                    createSql += ", [" + k + "] varchar(50)"
                    joinCondition = joinCondition.Append("pd.[" + k.Trim + "] = tpm.[" + k.Trim + "]", " AND ")
                    keyJoinCondition = "tpm.[" + k.Trim + "]"
                Next
                createSql += ", RecordNumber INT, PointLabel VARCHAR(MAX))"
                db.Execute(createSql)
            End If
            Exit For
        Next

        Dim sqlBatch As String = ""
        Dim sqlBatchCounter As Integer = 0
        Dim totalPointsCounter As Integer = 0

        For Each pg As GISParcel In dataRecords

            Dim parcelSql As String = ""

            Dim insertParcelSql As String = "INSERT INTO temp_ParcelMap ("
            Dim insertParcelValuesSql As String = ") VALUES ("
            Dim i As Integer = 0
            For Each k In pg.parcelKeys.Keys
                If i > 0 Then
                    insertParcelSql += ", "
                    insertParcelValuesSql += ", "
                End If
                insertParcelSql += "[" + k + "]"
                insertParcelValuesSql += pg.parcelKeys(k).ToSqlValue
                i += 1
            Next

            insertParcelSql += ", RecordNumber, PointLabel"
            insertParcelValuesSql += ", " & pg.recordNumber & ", " + If(pg.pointLabel Is Nothing, "null", pg.pointLabel.ToSqlValue)
            insertParcelSql = insertParcelSql + insertParcelValuesSql + "); "

            parcelSql += insertParcelSql + vbNewLine
            'Dim tempParcelId As Integer = db.GetIntegerValue(insertParcelSql)

            Dim multiSelect As String = ""

            Dim pointCounter As Integer = 0
            For Each gp In pg.gisPoints
                If multiSelect <> "" Then
                    multiSelect += vbNewLine + "UNION" + vbNewLine
                End If
                multiSelect += "SELECT IDENT_CURRENT('temp_ParcelMap'), {0}, {1}, {2}".SqlFormatString(gp.latitude, gp.longitude, gp.ordinal)

                If pointCounter = 200 Or totalPointsCounter = 800 Then
                    Dim insertMapPointSql As String = "INSERT INTO temp_ParcelMapPoints (TempId, Latitude, Longitude, Ordinal) " + vbNewLine + multiSelect + ";" + vbNewLine + vbNewLine
                    parcelSql += insertMapPointSql
                    sqlBatch += parcelSql
                    db.Execute(sqlBatch)

                    sqlBatch = ""
                    sqlBatchCounter = 0
                    parcelSql = ""
                    multiSelect = ""
                    pointCounter = 0
                    totalPointsCounter = 0
                End If
                pointCounter += 1
                totalPointsCounter += 1
            Next

            'Dim insertSql As String = "INSERT INTO temp_ParcelMapPoints (TempId, Latitude, Longitude, Ordinal) VALUES ( {0}, {1}, {2}, {3}); ".SqlFormatString(tempParcelId, gp.latitude, gp.longitude, gp.ordinal)
            'db.Execute(insertSql)

            If multiSelect <> "" Then
                Dim insertMapPointSql As String = "INSERT INTO temp_ParcelMapPoints (TempId, Latitude, Longitude, Ordinal) " + vbNewLine + multiSelect + ";" + vbNewLine + vbNewLine
                parcelSql += insertMapPointSql
            End If

            sqlBatch += parcelSql
            sqlBatchCounter += 1

            If sqlBatchCounter = 200 Then
                db.Execute(sqlBatch)
                sqlBatch = ""
                sqlBatchCounter = 0
            End If
            'db.Execute(parcelSql)
        Next

        If sqlBatch <> "" Then
            db.Execute(sqlBatch)
            sqlBatch = ""
        End If

        Dim totalParcels As Integer = db.GetIntegerValue("SELECT COUNT(*) FROM temp_ParcelMap")
        Dim totalMatchedParcels As Integer = db.GetIntegerValue("SELECT COUNT(*) FROM temp_ParcelMap tpm INNER JOIN ParcelData pd ON " + joinCondition)

        Dim UnmatchedParcels As String = db.GetStringValue("SELECT SUBSTRING((SELECT ', ' + CAST(" + keyJoinCondition + " AS varchar(50)) FROM temp_ParcelMap tpm LEFT JOIN ParcelData pd ON " + joinCondition + " WHERE pd.ROWUID IS NULL FOR XML path('')),2,100000) AS invalidParcels")
        Dim totalUnmatchedParcels As Integer = totalParcels - totalMatchedParcels

        recordTracker.parcelCreated = totalMatchedParcels
        recordTracker.recordsIgnored = totalUnmatchedParcels
        recordTracker.parcelIgnored = UnmatchedParcels

        Dim deleteSql As String = "", bulkCopySql As String = ""

        If requestType = "parcel" Then
            deleteSql = "DELETE FROM ParcelMapPoints WHERE ParcelId IN (SELECT CC_ParcelId FROM ParcelData pd INNER JOIN temp_ParcelMap tpm ON " + joinCondition + " WHERE tpm.RecordNumber IN (0,1))"
            bulkCopySql = "INSERT INTO ParcelMapPoints (ParcelId, Latitude, Longitude, Ordinal, RecordNumber) SELECT pd.CC_ParcelId, tpmp.Latitude, tpmp.Longitude, tpmp.Ordinal, tpm.RecordNumber FROM temp_ParcelMap tpm INNER JOIN ParcelData pd ON " + joinCondition + " INNER JOIN temp_ParcelMapPoints tpmp ON tpmp.TempId = tpm.Id;SELECT @@ROWCOUNT; "

        ElseIf requestType = "point" Then
            deleteSql = "DELETE FROM GIS_Points WHERE ParcelId IN (SELECT CC_ParcelId FROM ParcelData pd INNER JOIN temp_ParcelMap tpm ON " + joinCondition + " WHERE tpm.RecordNumber IN (0,1))"
            bulkCopySql = "INSERT INTO GIS_Points (ParcelId, Latitude, Longitude, Label) SELECT pd.CC_ParcelId, tpmp.Latitude, tpmp.Longitude, tpm.PointLabel FROM temp_ParcelMap tpm INNER JOIN ParcelData pd ON " + joinCondition + " INNER JOIN temp_ParcelMapPoints tpmp ON tpmp.TempId = tpm.Id;SELECT @@ROWCOUNT; "

        End If

        db.Execute(deleteSql)
        recordTracker.recordsInserted = db.GetIntegerValue(bulkCopySql)

        If ParcelEventLog.Enabled Then
            Dim sqlParcelEventLog As String = "INSERT INTO ParcelEventLog (ParcelId, EventId, ApplicationId) SELECT pd.CC_ParcelId, 40, 0 FROM temp_ParcelMap tpm INNER JOIN ParcelData pd ON " + joinCondition
            db.Execute(sqlParcelEventLog)
        End If

        db.DropTable("temp_ParcelMap")
        db.DropTable("temp_ParcelMapPoints")
    End Sub


    Public Shared Sub SaveStreetGISRecords(db As Database, streets As GISStreet(), recordTracker As RecordCountInfo)
        If recordTracker Is Nothing Then
            recordTracker = New RecordCountInfo
        End If
        If db.DoesTableExists("temp_StreetMap") Then
            db.DropTable("temp_StreetMap")
        End If
        If db.DoesTableExists("temp_StreetMapPoints") Then
            db.DropTable("temp_StreetMapPoints")
        End If

        If Not db.DoesTableExists("temp_StreetMapPoints") Then
            Dim createSql As String = "CREATE TABLE temp_StreetMapPoints (ROWID INT IDENTITY PRIMARY KEY, TempId INT, Latitude FLOAT, Longitude FLOAT, Ordinal INT); CREATE INDEX IX_temp_SMP ON temp_StreetMapPoints(TempId ASC);"
            db.Execute(createSql)
        End If

        If Not db.DoesTableExists("temp_StreetMap") Then
            Dim createSql As String = "CREATE TABLE temp_StreetMap(Id INT IDENTITY PRIMARY KEY, StreetUID varchar(50), StreetName varchar(100), StreetType varchar(50));"
            db.Execute(createSql)
        End If

        Dim sqlBatch As String = ""
        Dim sqlBatchCounter As Integer = 0
        Dim totalPointsCounter As Integer = 0

        For Each st As GISStreet In streets

            Dim streetSql As String = ""

            If st.key Is Nothing Then st.key = ""
            If st.type Is Nothing Then st.type = ""

            Dim insertStreetSql As String = "INSERT INTO temp_StreetMap (StreetUID, StreetName, StreetType) VALUES ({0}, {1}, {2});".SqlFormatString(st.key, st.name, st.type)
            streetSql += insertStreetSql + vbNewLine

            Dim multiSelect As String = ""

            Dim pointCounter As Integer = 0
            For Each gp In st.points
                If multiSelect <> "" Then
                    multiSelect += vbNewLine + "UNION" + vbNewLine
                End If
                multiSelect += "SELECT IDENT_CURRENT('temp_StreetMap'), {0}, {1}, {2}".SqlFormatString(gp.latitude, gp.longitude, gp.ordinal)

                If pointCounter = 200 Or totalPointsCounter = 800 Then
                    Dim insertMapPointSql As String = "INSERT INTO temp_StreetMapPoints (TempId, Latitude, Longitude, Ordinal) " + vbNewLine + multiSelect + ";" + vbNewLine + vbNewLine
                    streetSql += insertMapPointSql
                    sqlBatch += streetSql
                    db.Execute(sqlBatch)

                    sqlBatch = ""
                    sqlBatchCounter = 0
                    streetSql = ""
                    multiSelect = ""
                    pointCounter = 0
                    totalPointsCounter = 0
                End If
                pointCounter += 1
                totalPointsCounter += 1
            Next

            'Dim insertSql As String = "INSERT INTO temp_ParcelMapPoints (TempId, Latitude, Longitude, Ordinal) VALUES ( {0}, {1}, {2}, {3}); ".SqlFormatString(tempParcelId, gp.latitude, gp.longitude, gp.ordinal)
            'db.Execute(insertSql)

            If multiSelect <> "" Then
                Dim insertMapPointSql As String = "INSERT INTO temp_StreetMapPoints (TempId, Latitude, Longitude, Ordinal) " + vbNewLine + multiSelect + ";" + vbNewLine + vbNewLine
                streetSql += insertMapPointSql
            End If




            sqlBatch += streetSql
            sqlBatchCounter += 1

            If sqlBatchCounter = 200 Then
                db.Execute(sqlBatch)
                sqlBatch = ""
                sqlBatchCounter = 0
            End If
            'db.Execute(parcelSql)
        Next

        If sqlBatch <> "" Then
            db.Execute(sqlBatch)
            sqlBatch = ""
        End If

        'Dim totalParcels As Integer = db.GetIntegerValue("SELECT COUNT(*) FROM temp_StreetMap")
        Dim deleteSqlStreet As String = "DELETE FROM GIS_Street WHERE StreetUID IN (SELECT StreetUID FROM temp_StreetMap); "
        Dim bulkCreateStreet As String = "INSERT INTO GIS_Street (StreetUID, Name, Type) SELECT StreetUID, StreetName, StreetType FROM temp_StreetMap; ;SELECT @@ROWCOUNT;"

        db.Execute(deleteSqlStreet)
        recordTracker.recordsInserted = db.GetIntegerValue(bulkCreateStreet)

        Dim deleteSql As String = "DELETE FROM GIS_StreetPoints WHERE StreetId IN (SELECT st.Id FROM GIS_Street st INNER JOIN temp_StreetMap tsm ON tsm.StreetUID = st.StreetUID); "
        Dim bulkCopySql As String = "INSERT INTO GIS_StreetPoints (StreetId, Latitude, Longitude, Ordinal, RecordNumber) SELECT st.Id, tsmp.Latitude, tsmp.Longitude, tsmp.Ordinal, 1 FROM temp_StreetMap tsm INNER JOIN GIS_Street st  ON tsm.StreetUID = st.StreetUID INNER JOIN temp_StreetMapPoints tsmp ON tsmp.TempId = tsm.Id;SELECT @@ROWCOUNT; "

        db.Execute(deleteSql)
        db.Execute(bulkCopySql)

        db.DropTable("temp_StreetMap")
        db.DropTable("temp_StreetMapPoints")
    End Sub

    ''GIS_Layer

    Public Shared Function GetLayerConfig(target As Data.Database) As DataTable
        Return target.GetDataTable("SELECT * FROM GIS_ObjectLayer")
    End Function

    ''GIS_Objects

    Public Shared Sub ClearAllObjects(target As Data.Database, layer As Integer)
        Dim dtObjects = target.GetDataTable("SELECT Id FROM GIS_Objects WHERE LayerId={0}".SqlFormatString(layer))
        For Each rowObject As DataRow In dtObjects.Rows
            Dim objectId = rowObject.GetInteger("Id")
            GISTransferHelper.ClearGISMapPoints(target, objectId)
        Next
        target.Execute("DELETE FROM GIS_Objects WHERE LayerId={0}".SqlFormatString(layer))
    End Sub

    ''GIS_Segments

    Public Shared Sub ClearGISMapPoints(target As Database, objectId As String)
        target.Execute("DELETE FROM GIS_ObjectSegments WHERE ObjectId={0}".SqlFormatString(objectId))
        target.Execute("DELETE FROM GIS_ObjectPoints WHERE ObjectId={0}".SqlFormatString(objectId))
    End Sub


End Class
