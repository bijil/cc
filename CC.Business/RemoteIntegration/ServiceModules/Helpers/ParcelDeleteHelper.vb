﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects
Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts
Imports System.IO
Imports System.Data
Imports CAMACloud.Data.Database
Namespace RemoteIntegration

    Public Class ParcelDeleteHelper
        Private Shared ReadOnly temp_ParcelDelLog As String = " ParcelsDeletionLog"
        '        Private Shared ReadOnly ParcelDel = <sql>
        'CREATE TABLE [dbo].[ParcelsDeletionLog](
        '	[DeletedParcelID] [int] NOT NULL,
        '	[KeyValue1] [varchar](255) NULL,
        '	[KeyValue2] [varchar](255) NULL,
        '	[KeyValue3] [varchar](255) NULL,
        '	[KeyValue4] [varchar](255) NULL,
        '	[KeyValue5] [varchar](255) NULL,
        '	[KeyValue6] [varchar](255) NULL,
        '	[KeyValue7] [varchar](255) NULL,
        '	[KeyValue8] [varchar](255) NULL,
        '	[KeyValue9] [varchar](255) NULL,
        '	[KeyValue10] [varchar](255) NULL,
        '	[DeletedBy] [varchar](50) NULL,
        '	[DeletedDate] [datetime] NULL,
        ' CONSTRAINT [PK_ParcelsDeletionLog] PRIMARY KEY CLUSTERED 
        '(
        '	[DeletedParcelID] ASC
        ')WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ') ON [PRIMARY]

        'GO
        ' </sql>

        Public Shared Sub CreateParcelDeletionLogEntry(ByVal target As Data.Database, parcelId As String)
            'If Not target.DoesTableExists("ParcelsDeletionLog") Then
            '    target.Execute(ParcelDel)
            'End If
            If target.DoesTableExists("ParcelsDeletionLog") Then
                target.Execute("INSERT INTO ParcelsDeletionLog(DeletedParcelID,KeyValue1,KeyValue2,KeyValue3,KeyValue4,KeyValue5,KeyValue6,KeyValue7,KeyValue8,KeyValue9,KeyValue10,DeletedBy,DeletedDate)SELECT TOP 1 P.Id,P.KeyValue1 ,P.KeyValue2,P.KeyValue3 ,P.KeyValue4 ,P.KeyValue5 ,P.KeyValue6 ,P.KeyValue7 ,P.KeyValue8 ,P.KeyValue9 ,P.KeyValue10 ,'NULL',GETDATE() FROM Parcel  P INNER JOIN ParcelImages I on  P.Id = I.ParcelId where p.id ='{0}'", parcelId)
            End If

        End Sub

        Public Shared Sub CascadeDeleteParcelDescendants(ByVal target As Data.Database, tableId As String, tableName As String, parcelId As String)

            Dim dtchilds As DataTable = target.GetDataTable("SELECT dt.Id As TableId,dt.Name As  TableName,dsr.Id As RelationId FROM DataSourceTable dt INNER JOIN DataSourceRelationships dsr ON dt.Id=dsr.TableId WHERE ParentTableId={0}".SqlFormatString(tableId))

            For Each row As DataRow In dtchilds.Rows
                Dim childId = row.GetString("TableId")
                Dim childTableName = DataSource.FindTargetTable(target, row.GetString("TableName"))
                target.Execute("DELETE FROM " + childTableName + " WHERE CC_ParcelId={0} ".SqlFormatString(parcelId))
                CascadeDeleteParcelDescendants(target, childId, row.GetString("TableName"), parcelId)
            Next

            Dim targetTable = DataSource.FindTargetTable(target, tableName)
            target.Execute("DELETE FROM " + targetTable + " WHERE CC_ParcelId={0} ".SqlFormatString(parcelId))

        End Sub

        Public Shared Function DeleteParcels(ByVal target As Data.Database, ByVal parcelList As ParcelMapInfo()) As StandardResponse
            Dim parcelTable = target.GetStringValue("SELECT ParcelTable FROM Application")
            Dim propertyTableId = target.GetStringValue("SELECT Id FROM DataSourceTable WHERE Name={0}".SqlFormatString(parcelTable))

            Dim sp As New StandardResponse
            Dim deletedParcels As Integer = 0
            Dim missingParcels As Integer = 0

            For Each parcel In parcelList
                If parcel.parcelId Is Nothing AndAlso parcel.keys IsNot Nothing Then
                    parcel.parcelId = ParcelTransferHelper.GetParcelIdFromFilter(target, parcel.keys)
                End If
                If parcel.parcelId <> "" Then
                    ParcelDeleteHelper.CascadeDeleteParcelDescendants(target, propertyTableId, parcelTable, parcel.parcelId)
                    Dim recordsAffected As Integer = ParcelTransferHelper.ClearAllParcelRelatedTables(target, parcel.parcelId)
                    If recordsAffected = 0 Then
                        missingParcels += 1
                    Else
                        deletedParcels += 1

                        ParcelDeleteHelper.CreateParcelDeletionLogEntry(target, parcel.parcelId)                            'For Deleted Parcel log
                        SystemAuditStream.CreateStandardEvent(target, StandardSystemAuditEvents.DTX_46010_DELETE)      'For SystemAudittraillog
                    End If

                    If ParcelEventLog.Enabled Then
                        ParcelEventLog.CreateFlagEvent(target, ParcelEventLog.EventType.Deleted, ParcelEventLog.ApplicationType.API, parcel.parcelId)
                    End If
                Else
                    missingParcels += 1
                End If
            Next

            If deletedParcels = 0 Then
                sp.AddMessage("No parcels deleted.")
            Else
                sp.AddMessage(deletedParcels & " parcel(s) deleted.")
            End If
            If missingParcels > 0 Then
                sp.messages.Add(missingParcels & " parcel(s) not found.")
            End If
            sp.recordCount = deletedParcels
            Return sp
        End Function

        Public Shared Function MarkParcelAsRejected(target As Data.Database, rejectedParcel As List(Of ParcelReject), ByRef recordTracker As RemoteIntegration.DataContracts.RecordCountInfo) As List(Of Integer)
            'Try
            Dim recordIndex As Integer = 0
            Dim conflictList As New List(Of Integer)
            For Each parcel As ParcelReject In rejectedParcel
                If parcel.parcelId = "" Or parcel.parcelId Is Nothing Then
                    parcel.parcelId = _getParcelId(target, parcel.keys)
                End If

                If parcel.parcelId <> "" Then
                    recordTracker.recordsUpdated += target.Execute("UPDATE parcel SET FailedOnDownSync = 1, FailedOnDownSyncStatus = 1, DownSyncRejectReason = {0} WHERE Id = {1};SELECT CAST(@@ROWCOUNT  AS INT) As recordAffected".SqlFormatString(parcel.reason, parcel.parcelId))
                    target.Execute("INSERT INTO ParcelDownsyncErrorLog(ParcelId,ErrorData,ErrorType) VALUES ({0}, {1}, {2})".SqlFormatString(parcel.parcelId, parcel.reason, 0))
                    'Neighborhood.MarkAsNotCompleted(target, parcelId)
                    recordIndex += 1
                    ParcelAuditStream.CreateFlagEvent(target, DateTime.Now, parcel.parcelId, "API", "Parcel DownSync has failed due to errors.", "API")
                    If ParcelEventLog.Enabled Then
                        ParcelEventLog.CreateFlagEvent(target, ParcelEventLog.EventType.SyncFailed, ParcelEventLog.ApplicationType.API, parcel.parcelId)
                    End If
                Else
                    conflictList.Add(recordIndex)
                    recordTracker.recordsIgnored += 1
                End If
            Next
            Return conflictList
            'Catch ex As Exception
            '    ErrorMailer.ReportWCFException("API-Reject", ex, ServiceModel.OperationContext.Current, Web.HttpContext.Current.Request, request.accessKey)
            '    Throw ex
            'End Try
        End Function

        Public Shared Function MarkParcelAsDeferred(target As Data.Database, deferredParcels As List(Of ParcelReject), ByRef recordTracker As RemoteIntegration.DataContracts.RecordCountInfo) As List(Of Integer)
            'Try
            Dim recordIndex As Integer = 0
            Dim conflictList As New List(Of Integer)
            For Each parcel As ParcelReject In deferredParcels
                If parcel.parcelId = "" Or parcel.parcelId Is Nothing Then
                    parcel.parcelId = _getParcelId(target, parcel.keys)
                End If

                If parcel.parcelId <> "" Then
                    'recordTracker.recordsUpdated += target.Execute("UPDATE Parcel SET DownSyncRejectReason={0} WHERE Id={1};SELECT CAST(@@ROWCOUNT  AS INT) As recordAffected".SqlFormatString(parcel.reason, parcel.parcelId))
                    recordTracker.recordsUpdated += 1
                    'Neighborhood.MarkAsNotCompleted(target, parcelId)
                    recordIndex += 1

                    Dim deferReason = parcel.reason
                    If parcel.reason.Length > 200 Then
                        deferReason = deferReason.Substring(0, 200) + "...."
                    End If
                    ParcelAuditStream.CreateFlagEvent(target, Date.UtcNow, parcel.parcelId, "API", "Parcel DownSync has been deferred due to " + deferReason, "API")
                    If ParcelEventLog.Enabled Then
                        ParcelEventLog.CreateFlagEvent(target, ParcelEventLog.EventType.SyncFailed, ParcelEventLog.ApplicationType.API, parcel.parcelId)
                    End If
                Else
                    conflictList.Add(recordIndex)
                    recordTracker.recordsIgnored += 1
                End If
            Next
            Return conflictList
            'Catch ex As Exception
            '    ErrorMailer.ReportWCFException("API-Reject", ex, ServiceModel.OperationContext.Current, Web.HttpContext.Current.Request, request.accessKey)
            '    Throw ex
            'End Try
        End Function

        Private Shared Function _getParcelId(target As Data.Database, keypairs As Dictionary(Of String, String)) As String

            Dim dataFilter = ""
            Dim i As Integer = 1
            For Each key In keypairs
                If dataFilter <> "" Then
                    dataFilter += " AND "
                End If
                dataFilter += "KeyValue" + i.ToString() + "=" + key.Value.ToSqlValue
            Next

            Dim ParcelId = target.GetStringValue("SELECT Id FROM Parcel WHERE " + dataFilter)
            Return ParcelId
        End Function
    End Class
End Namespace