﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts
Imports System.Linq
Imports System.Text.RegularExpressions
Imports System.Security.Cryptography
Imports System.Text

Namespace RemoteIntegration
    Public Class AccessManagementHelper

        Public Shared Function getMembershipApplicationName(ByVal accessKey As String, ByVal passKey As String) As String
            Return "org" + Database.System.GetStringValue("SELECT OrganizationId FROM ApplicationAccess WHERE AccessKey = {0} AND PassKey={1}".SqlFormatString(accessKey, passKey)).PadLeft(3, "0")
        End Function

        Public Shared Function EncodePassword(target As Database, password As String)
            ''   Dim sha1Provider = HashAlgorithm.Create("SHA1")
            ' Dim binHash = (Encoding.Unicode.GetBytes(password))
            Return Database.System.GetStringValue("EXEC EncodePasswordHash " + GetHashedString(password) + "")
            'Return Database.System.GetStringValue("EXEC EncodePasswordHash " + password + "")
        End Function

        Public Shared Function getApplicationId(applicationName As String)
            Return Database.System.GetStringValue("SELECT ApplicationId FROM aspnet_Applications WHERE LoweredApplicationName={0}".SqlFormatString(applicationName))
        End Function


        Public Shared Sub VerifyRoles(ByVal request As AccessRequest, ByRef errorList As List(Of EventType))

            Dim applicationName As String = getMembershipApplicationName(request.accessKey, request.passKey)
            Dim applicationId = getApplicationId(applicationName)

            Dim roles As String() = {"DataSetup", "MobileAssessor", "TaskManager", "Reports", "Tracking", "QC"}

            If (request.roles.ToArray.Any(Function(item) item.Length = 0) = False) Then

                For Each role In roles
                    Dim checkSql As String = "DECLARE @Exists INT; EXEC @Exists = [aspnet_Roles_RoleExists] {0}, {1}; SELECT @Exists AS IsExists;".SqlFormatString(applicationName, role)
                    If Database.System.GetIntegerValue(checkSql) = 0 Then
                        Dim createRoleSql As String = "DECLARE @Exists INT; EXEC @Exists = [aspnet_Roles_CreateRole] {0}, {1}; SELECT @Exists AS IsExists;".SqlFormatString(applicationName, role)
                        Database.System.Execute(createRoleSql)
                    End If

                    Dim checkUserInRole = "SELECT COUNT(*)  FROM aspnet_users ur INNER JOIN   aspnet_UsersInRoles ar ON  ar.UserId =ur.UserId  INNER JOIN aspnet_Roles r ON r.RoleId=ar.RoleId WHERE ur.ApplicationId={0} AND ur.UserName={1} AND r.RoleName={2}".SqlFormatString(applicationId, request.username, role)
                    Dim IsUserExistInRole = Database.System.GetIntegerValue(checkUserInRole)

                    If (request.roles.Contains(role)) Then
                        If (IsUserExistInRole = 0) Then
                            Dim sql = "DECLARE @Now DATETIME; SET @Now = GETUTCDATE();EXEC [aspnet_UsersInRoles_AddUsersToRoles]@ApplicationName={0},@UserNames={1}, @RoleNames= {2},@CurrentTimeUtc=@Now".SqlFormatString(applicationName, request.username, role)
                            Database.System.Execute(sql)
                        Else
                        End If
                    Else
                        If (IsUserExistInRole > 0) Then
                            Dim sql1 = "EXEC [aspnet_UsersInRoles_RemoveUsersFromRoles]@ApplicationName={0},@UserNames={1}, @RoleNames= {2}".SqlFormatString(applicationName, request.username, role)
                            Database.System.Execute(sql1)
                        End If
                    End If

                Next
            Else
                errorList.Add(ERR_InvalidRoleNames)
            End If

        End Sub

        Public Shared Sub ClearAllUsers(ByVal accessKey As String, ByVal passKey As String)

            Dim applicationName As String = AccessManagementHelper.getMembershipApplicationName(accessKey, passKey)
            Dim sql As String = "EXEC [aspnet_Membership_GetAllUsers] @ApplicationName = {0}, @PageIndex = 0, @PageSize = 1000".SqlFormatString(applicationName)
            Dim dtUsers As DataTable = Database.System.GetDataTable(sql)
            For Each drUser As DataRow In dtUsers.Rows
                Dim username = drUser.GetString("UserName")
                If username.Trim.ToLower <> "admin" And username.Trim.ToLower <> "dcs-support" And username.Trim.ToLower <> "dcs-ps" Then
                    Dim sqlDelete As String = "EXEC [aspnet_Users_DeleteUser]@ApplicationName={0},@UserName={1}, @TablesToDeleteFrom =15,@NumTablesDeletedFrom =0".SqlFormatString(applicationName, username)
                    Database.System.Execute(sqlDelete)
                End If
            Next

        End Sub

        Public Shared Function _getUserId(ByVal db As Database, ByVal username As String, ByVal applicationName As String)
            Dim sqlGetString = "DECLARE @UserName nvarchar(256);DECLARE @ApplicationName nvarchar(256);SET @UserName={0};SET @ApplicationName={1};SELECT u.UserId  FROM  dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m  WHERE   LoweredUserName = LOWER(@UserName) AND u.ApplicationId = a.ApplicationId  AND LOWER(@ApplicationName) = a.LoweredApplicationName AND u.UserId = m.UserId "
            Return Database.System.GetStringValue(sqlGetString.SqlFormatString(username, applicationName))
        End Function

        Public Shared Function IsValidAccount(ByVal db As Database, ByVal Isvalidatepwd As Boolean, ByVal request As AccessRequest, ByRef errorList As List(Of EventType), ByVal action As String) As Boolean
            Dim f As Boolean = True
            Dim applicationName As String = AccessManagementHelper.getMembershipApplicationName(request.accessKey, request.passKey)
            Dim applicationId = AccessManagementHelper.getApplicationId(applicationName)

            If (request.email Is Nothing Or request.email = "") Then
                request.email = "user@camacloud.com"
            End If

            If (request.username <> Nothing Or request.username <> "") Then
                If (IsUserExist(db, request.username, applicationId) = False) Then
                    If (action = "edit") Then
                        errorList.Add(ERR_UsernameDoesNotExists)
                        Return False
                    End If

                Else
                    If (action = "edit") Then
                    Else
                        errorList.Add(ERR_UsernameExists)
                        Return False
                    End If

                End If
            Else
                errorList.Add(ERR_InvalidUsername)
                f = False
            End If
            If (request.email <> Nothing Or request.email <> "") Then
                If (IsValidEmailAddress(request.email)) Then
                Else
                    errorList.Add(ERR_InvalidEmailAddress)
                    f = False
                End If
            Else
                errorList.Add(ERR_EmptyEmailAddress)
                f = False
            End If

            Return f
        End Function

        Public Shared Function IsValidEmailAddress(ByVal emailAddress As String) As Boolean
            Dim regExPattern As String = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$"
            Dim emailAddressMatch As Match = Regex.Match(emailAddress, regExPattern)
            If emailAddressMatch.Success Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function IsUserExist(ByVal db As Database, username As String, applicationId As String) As Boolean

            Dim checkSql = "SELECT COUNT(*) FROM aspnet_Users au INNER JOIN aspnet_Membership  am ON au.UserId=am.UserId WHERE au.UserName={0} AND au.ApplicationId={1}"
            If (Database.System.GetIntegerValue(checkSql.SqlFormatString(username, applicationId)) > 0) Then
                Return True
            Else
                Return False
            End If
        End Function

        ''UserInfo from ResponseContracts
        Public Shared Function ListUser(ByVal db As Database, ByVal request As AccessRequest) As List(Of UserInfo)

            Dim applicationName As String = AccessManagementHelper.getMembershipApplicationName(request.accessKey, request.passKey)
            Dim sql As String = "EXEC [aspnet_Membership_GetAllUsers] @ApplicationName = {0}, @PageIndex = 0, @PageSize = 1000".SqlFormatString(applicationName)
            Dim dtUsers As DataTable = Database.System.GetDataTable(sql)
            Dim userList As New List(Of UserInfo)
            For Each row As DataRow In dtUsers.Rows
                Dim user As New UserInfo()
                user.username = row.GetString("UserName")
                'Dim sqlQuery As String = "SELECT FirstName,LastName FROM dbo.UserSettings WHERE LoginId={0}".SqlFormatString(request.username)
                'Dim names As DataRow = Database.Tenant.GetTopRow(sqlQuery)
                'user.firstName = names.GetString("FirstName")
                'user.lastName = names.GetString("LastName")
                user.email = row.GetString("Email")
                user.createdDate = row.GetDate("CreateDate")
                user.lastLoginDate = row.GetDate("LastLoginDate")
                user.isLockedOut = row.GetBoolean("IsLockedOut")
                Dim usrId As String = row.GetString("UserId")
                Dim strQuery As String = "SELECT aspnet_Roles.RoleName FROM aspnet_Roles INNER JOIN aspnet_UsersInRoles ON aspnet_Roles.RoleId = aspnet_UsersInRoles.RoleId where aspnet_UsersInRoles.UserId={0}"
                Dim dtRoles As DataTable = Database.System.GetDataTable(strQuery.SqlFormatString(usrId))
                For Each rowRoles As DataRow In dtRoles.Rows
                    user.roles.Add(rowRoles.GetString("RoleName"))
                Next    
                userList.Add(user)
            Next row
            Return userList
        End Function
        Public Shared Function UserCreation(ByVal db As Database, ByVal request As AccessRequest, ByRef sp As StandardResponse) As String

            Dim errorList As New List(Of EventType)
            Dim errorCode As New List(Of Integer)

            Dim applicationName As String = AccessManagementHelper.getMembershipApplicationName(request.accessKey, request.passKey)

            If request.roles Is Nothing Then
                request.roles = New List(Of String)
            End If

            If AccessManagementHelper.IsValidAccount(db, True, request, errorList, "create") Then

                Dim passwordFormat As Integer = 0
                Select Case request.passwordFormat
                    Case "clear", "plain"
                        passwordFormat = 0
                    Case "hashed"
                        passwordFormat = 1
                        request.password = AccessManagementHelper.EncodePassword(db, request.password)
                    Case Nothing, ""
                        passwordFormat = 0
                End Select



                Dim xsql = <sql>
                                DECLARE @UserId UNIQUEIDENTIFIER, @Now DATETIME;
                                SET @Now = GETUTCDATE();
                                EXEC [aspnet_Membership_CreateUser] 
                                    @ApplicationName = {0}, 
                                    @UserName = {1}, 
                                    @Password = {2}, 
                                    @PasswordSalt = '', 
                                    @Email = {3}, 
                                    @PasswordQuestion = 'PQ', 
                                    @PasswordAnswer = 'PA', 
                                    @IsApproved = 1, 
                                    @CurrentTimeUtc = @Now,
                                    @PasswordFormat={4}, 
                                    @UserId = @UserId OUT; 

                               SELECT @UserId As NewUserId
                           </sql>

                Dim password As String
                If (request.password <> Nothing Or request.password <> "") Then
                    password = request.password
                Else
                    password = ""
                End If
                Dim sql As String = xsql.Value.SqlFormatString(applicationName, request.username, password, request.email, passwordFormat)
                Dim userId As String = Database.System.GetStringValue(sql)

                If request.firstName Is Nothing Then request.firstName = request.username
                If request.lastName Is Nothing Then request.lastName = ""

                db.Execute("INSERT INTO UserSettings (LoginId, FirstName, LastName) VALUES ({0},{1},{2})".SqlFormatString(request.username, request.firstName, request.lastName))
                If request.roles Is Nothing Then
                Else
                    AccessManagementHelper.VerifyRoles(request, errorList)
                End If

                For Each err As EventType In errorList
                    sp.messages.Add(err.Message)
                    errorCode.Add(err.Code)
                Next
                If errorCode.Count > 0 Then
                    sp.data = DataTools.JSON.Serialize(Of List(Of Integer))(errorCode)
                End If
                Return userId
            Else
                For Each err As EventType In errorList
                    sp.messages.Add(err.Message)
                    errorCode.Add(err.Code)
                Next
                If errorCode.Count > 0 Then
                    sp.data = DataTools.JSON.Serialize(Of List(Of Integer))(errorCode)
                End If

                Return "0"
            End If

        End Function

        Public Shared Function UserEdit(ByVal db As Database, ByVal request As AccessRequest, ByRef sp As StandardResponse) As Boolean
            Dim validatepwd = False
            Dim errorList As New List(Of EventType)
            Dim errorCode As New List(Of Integer)


            Dim applicationName As String = AccessManagementHelper.getMembershipApplicationName(request.accessKey, request.passKey)
            Dim applicationId = AccessManagementHelper.getApplicationId(applicationName)
            If (request.status = Nothing Or request.status = "") Then
                request.status = "modify"
            End If

            If (AccessManagementHelper.IsUserExist(db, request.username, applicationId) = False) Then
                errorList.Add(ERR_UsernameDoesNotExists)
                For Each err As EventType In errorList
                    sp.messages.Add(err.Message)
                    errorCode.Add(err.Code)
                Next
                If errorCode.Count > 0 Then
                    sp.data = DataTools.JSON.Serialize(Of List(Of Integer))(errorCode)
                End If
                Return False
            End If

            Select Case request.status
                Case "modify"

                    Dim passwordFormat As Integer = 0
                    Select Case request.passwordFormat
                        Case "clear", "plain"
                            passwordFormat = 0
                        Case "hashed"
                            passwordFormat = 1
                            request.password = AccessManagementHelper.EncodePassword(db, request.password)
                        Case Nothing, ""
                            passwordFormat = 0
                    End Select

                    If ((request.email <> Nothing Or request.email <> "") AndAlso request.email.Trim() <> "user@camacloud.com") Then
                    Else
                        request.email = Database.System.GetStringValue("DECLARE @UserName nvarchar(256);DECLARE @ApplicationName nvarchar(256);SET @UserName={0};SET @ApplicationName={1}; Select m.Email FROM dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m  WHERE   LoweredUserName = LOWER(@UserName) AND  u.ApplicationId = a.ApplicationId  AND LOWER(@ApplicationName) = a.LoweredApplicationName AND u.UserId = m.UserId ".SqlFormatString(request.username, applicationName))
                    End If

                    If (request.password <> Nothing) Then
                        validatepwd = True
                    End If

                    If AccessManagementHelper.IsValidAccount(db, validatepwd, request, errorList, "edit") Then

                        If (request.roles Is Nothing) Then
                        Else
                            AccessManagementHelper.VerifyRoles(request, errorList)
                        End If

                        Dim sql As String = "DECLARE @Now DATETIME; SET @Now = GETUTCDATE();EXEC [aspnet_Membership_UpdateUser]@ApplicationName={0},@UserName={1}, @Email = {2}, @Comment = '', @IsApproved =1, @LastLoginDate ='',@LastActivityDate='',@UniqueEmail='',@CurrentTimeUtc= @Now".SqlFormatString(applicationName, request.username, request.email)
                        Database.System.Execute(sql)

                        If request.firstName Is Nothing Then request.firstName = request.username
                        If request.lastName Is Nothing Then request.lastName = ""

                        Dim changenames As String = "UPDATE dbo.UserSettings SET FirstName = {1}, LastName = {2} WHERE LoginId={0} ".SqlFormatString(request.username, request.firstName, request.lastName)
                        db.Execute(changenames)
                        If (validatepwd) Then
                            Dim changeSql As String = "DECLARE @Now DATETIME; SET @Now = GETUTCDATE();EXEC [aspnet_Membership_ResetPassword]@ApplicationName={0},@UserName={1}, @NewPassword = {2}, @MaxInvalidPasswordAttempts =0, @PasswordAttemptWindow = 0, @PasswordSalt ='',@PasswordFormat={3},@CurrentTimeUtc=@Now".SqlFormatString(applicationName, request.username, request.password, passwordFormat)
                            Database.System.Execute(changeSql)
                        End If
                    End If

                Case "delete"

                    Dim sqlDelete As String = "EXEC [aspnet_Users_DeleteUser]@ApplicationName={0},@UserName={1}, @TablesToDeleteFrom =15,@NumTablesDeletedFrom =0".SqlFormatString(applicationName, request.username)
                    Database.System.Execute(sqlDelete)
                    db.Execute("DELETE FROM UserSettings WHERE LoginId={0}".SqlFormatString(request.username))

                Case "unlock"

                    Dim sqlUnlock As String = "EXEC [aspnet_Membership_UnlockUser]@ApplicationName={0},@UserName={1}".SqlFormatString(applicationName, request.username)
                    Database.System.Execute(sqlUnlock)

                Case "lock"

                    Dim userId = AccessManagementHelper._getUserId(db, request.username, applicationName)
                    If (userId <> "") Then
                        Dim sqlLock = "DECLARE @Now DATETIME; SET @Now = GETUTCDATE();UPDATE dbo.aspnet_Membership  SET IsLockedOut = 1 ,LastLockoutDate =@Now  WHERE UserId={0}"
                        Database.System.Execute(sqlLock.SqlFormatString(userId))
                    End If

                Case Else
                    errorList.Add(ERR_InvalidStatus)
            End Select
            For Each err As EventType In errorList
                sp.messages.Add(err.Message)
                errorCode.Add(err.Code)
            Next
            If errorCode.Count > 0 Then
                sp.data = DataTools.JSON.Serialize(Of List(Of Integer))(errorCode)
            End If
            Return True
        End Function


    End Class
End Namespace