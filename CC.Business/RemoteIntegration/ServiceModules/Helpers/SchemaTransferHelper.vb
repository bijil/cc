﻿Namespace RemoteIntegration
    Friend Class SchemaTransferHelper

        Public Shared Sub ClearCompleteSchema(db As Database)
            db.DeleteTableRecordsWithIdentityReset("DataSourceTableAction")
            db.DeleteTableRecordsWithIdentityReset("DataSourceKeys")
            db.DeleteTableRecordsWithIdentityReset("DataSourceRelationships")
            db.DeleteTableRecordsWithIdentityReset("DataSourceField")
            db.DeleteTableRecordsWithIdentityReset("DataSourceTable")
            'db.Execute("DELETE FROM DataSourceKeys; DBCC CHECKIDENT (DataSourceKeys, RESEED, 0);")
            'db.Execute("DELETE FROM DataSourceRelationships; DBCC CHECKIDENT (DataSourceRelationships, RESEED, 0);")
            'db.Execute("DELETE FROM DataSourceField; DBCC CHECKIDENT (DataSourceField, RESEED, 0);")
            'db.Execute("DELETE FROM DataSourceTable; DBCC CHECKIDENT (DataSourceTable, RESEED, 0);")
        End Sub

        Public Shared Sub ClearRelations(db As Database)
            db.Execute("DELETE FROM DataSourceKeys WHERE RelationshipId IN (SELECT Id FROM DataSourceRelationships WHERE Relationship IS NOT NULL)")
            db.Execute("DELETE FROM DataSourceRelationships WHERE Relationship IS NOT NULL")
        End Sub

    End Class
End Namespace