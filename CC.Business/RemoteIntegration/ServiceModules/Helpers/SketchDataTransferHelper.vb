﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects
Imports System.IO
Imports CAMACloud.BusinessLogic.Sketching

Namespace RemoteIntegration
    Public Class SketchDataTransferHelper

        Private Const tempTableName As String = "temp_queue_DownSync_ParcelSketch"

        Public Shared Function InitializeSketchTransferProcess(db As Database, request As SketchTransferRequest) As Integer
            If request.direction.Trim.ToLower = "downsync" Then
                Dim periodStart As DateTime = request.periodStart
                Dim periodEnd As DateTime = request.periodEnd
                Dim periodCheckSql As String = ""
                If periodStart = periodEnd Or periodStart < #1/1/2010# Then
                    periodCheckSql = ""
                Else
                    periodCheckSql = " AND p.QCDate BETWEEN " + periodStart.ToSqlValue + " AND " + periodEnd.ToSqlValue + " "
                End If

                If Not db.DoesTableExists(tempTableName) Then
                    db.Execute("CREATE TABLE " + tempTableName + " (ParcelSketchId INT)")
                End If
                db.Execute("DELETE FROM " + tempTableName)
                Return db.GetIntegerValue("INSERT INTO " + tempTableName + " (ParcelSketchId) SELECT ps.Id FROM ParcelSketch ps INNER JOIN Parcel p ON ps.ParcelId = p.Id WHERE (p.FailedOnDownSync = 0 OR p.FailedOnDownSync IS NULL) AND p.QC = 1 AND ps.CreatedBy IS NOT NULL AND (ps.DownSynced=0 OR ps.DownSynced IS NULL)" + periodCheckSql + "; SELECT @@ROWCOUNT")
            Else
                Return -1
            End If
        End Function

        Public Shared Sub FinalizeSketchTransferProcess(db As Database, request As SketchTransferRequest)
            If request.direction.Trim.ToLower = "downsync" Then
                db.Execute("UPDATE ParcelSketch SET DownSynced = 1, DownSyncedDate = GETUTCDATE() WHERE Id IN (SELECT ParcelSketchId FROM " + tempTableName + ")")
            End If
            If db.DoesTableExists(tempTableName) Then
                db.Execute("DROP TABLE " + tempTableName)
            End If
        End Sub

        Public Shared Function ProcessSketchUpload(ByVal db As Database, ByVal request As SketchTransferRequest) As StandardResponse
            Dim s3mgr As New S3FileManager

            Dim spr As New SketchProcessResponse With {.sketchFormat = request.sketchFormat, .parcelCount = 0, .failedSketches = 0, .sketchCount = 0}
            Dim parcels As New List(Of Integer)
            For Each s As SketchObject In request.sketches
                Dim parcelId As Integer = s.GetParcelId(db)
                If parcelId <> -1 Then
                    Dim bytes() As Byte = System.Convert.FromBase64String(s.sketchData)
                    Dim pkey = Parcel.GetKeyValue1(db, parcelId)
                    If pkey = "" Or s.sketchData = "" Then
                        spr.failedSketches += 1
                    End If
                    Dim ms As New MemoryStream(bytes)

                    Dim s3Path As String = request.GetTenantKey + "/pi/" + S3FileManager.GetBrokenPath(pkey) + IIf(s.fileName Is Nothing OrElse s.fileName.Trim = "", s.parcelId + ".sbin", s.fileName)

                    Dim vectors As List(Of SketchVector) = request.sketchProcessor.ReadSketch(ms)

                    Dim s3Exception As Exception = Nothing
                    Try
                        Try
                            s3mgr.UploadFile(ms, s3Path)
                        Catch ex As Exception
                            s3Exception = ex
                            Throw New Exception("Error on uploading file '" + s.fileName + "' with " & ms.ToArray.Length.ToString & " bytes")
                        End Try
                    Catch ex As Exception
                        Dim originalErrorMessage As String = ""
                        If s3Exception IsNot Nothing Then
                            originalErrorMessage = "; Original Message: " + s3Exception.Message
                        End If

                        Throw New Exception("S3 Upload Error; Path : " + s3Path + originalErrorMessage, ex)
                    End Try

                    If Not parcels.Contains(parcelId) Then
                        parcels.Add(parcelId)
                    End If

                    Dim sql As String
                    sql = "INSERT INTO ParcelSketch (ParcelId, Path, VectorString) VALUES ({0}, {1}, {2});".SqlFormatString(parcelId, s3Path, "") + vbNewLine
                    ''Dim sketchId As Integer = db.GetIdentityFromInsert(sql)
                    For Each v As SketchVector In vectors
                        sql += "INSERT INTO ParcelSketchVectors (ParcelSketchId, Ordinal, Label, VectorString, ReferenceId) VALUES (IDENT_CURRENT('ParcelSketch'), {0}, {1}, {2}, {3})".SqlFormatString(v.Ordinal, v.Label, v.VectorString, v.ReferenceId) + vbNewLine
                    Next

                    db.Execute(sql)

                    spr.sketchCount += 1
                Else

                End If
            Next
            spr.parcelCount = parcels.Count
            Return New StandardResponse With {.header = spr}
        End Function

        Public Shared Function ProcessSketchDownload(ByVal db As Database, ByVal request As SketchTransferRequest) As StandardResponse
            Dim sketches As New List(Of SketchObject)
            Dim jobId As Integer = request.jobId
            Dim pageIndex As Integer = request.pageIndex
            Dim pageSize As Integer = request.pageSize
            Dim totalPages = TransferJob.GetTotalPages(db, jobId)
            Dim parcels As New List(Of Integer)

            Dim spr As New SketchProcessResponse With {.sketchFormat = request.sketchFormat, .parcelCount = 0, .failedSketches = 0, .sketchCount = 0}

            If pageIndex > totalPages Then
                Throw New Exception("There is no page found in the referenced index.")
            End If
            If pageIndex = 0 Then
                pageIndex = 1
            End If
            pageIndex = (pageIndex - 1) * pageSize

            Dim sql = "SELECT ps.* FROM ParcelSketch ps INNER JOIN " + tempTableName + " tps ON ps.Id = tps.ParcelSketchId ORDER BY tps.ParcelSketchId OFFSET {0} ROWS FETCH NEXT {1} ROWS ONLY".FormatString(pageIndex, pageSize)
            Dim dt As DataTable = db.GetDataTable(sql)

            Dim s3 As New S3FileManager()

            For Each dr As DataRow In dt.Rows
                Dim s3Path As String = dr.GetString("Path")

                Dim sketch As New SketchObject()
                sketch.parcel = dr.GetInteger("ParcelId")
                sketch.keys = DataSource.GetKeyValueListForTable(db, db.Application.ParcelTable, 0, sketch.parcelId)
                sketch.fileName = IO.Path.GetFileName(s3Path)

                Dim ms As New MemoryStream()
                For ac = 1 To 3
                    Try
                        ms = s3.GetFile(s3Path)
                        ms = request.sketchProcessor.UpdateSketch(ms, Nothing)
                        Dim data As Byte() = ms.ToArray()
                        sketch.sketchData = Convert.ToBase64String(data)
                        Exit For
                    Catch ex As Exception
                        sketch.sketchData = ""
                    End Try
                Next
                If sketch.sketchData = "" Then
                    spr.failedSketches += 1
                Else
                    sketches.Add(sketch)

                    If Not parcels.Contains(sketch.parcelId) Then
                        parcels.Add(sketch.parcelId)
                    End If
                    spr.sketchCount += 1
                End If
            Next

            spr.parcelCount = parcels.Count
            'target.Execute("UPDATE " + temp_TableName + " SET DownSynced=1 WHERE ID IN(SELECT Id FROM Temp_ParcelImages pi ORDER BY Id  OFFSET {0} ROWS FETCH NEXT {1} ROWS ONLY)".FormatString(pageIndex, pageSize))
            TransferJob.UpdateCurrentPage(db, (pageIndex + 1), jobId)
            Return New StandardResponse With {.data = sketches, .header = spr, .recordCount = sketches.Count}
        End Function

    End Class
End Namespace

