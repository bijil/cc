﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts

Namespace RemoteIntegration

    Public Class ApplicationConfigHelper

        Private Shared Sub InsertOrUpdateApplicationConfiguration(ByVal target As Data.Database, ByVal request As ApplicationConfigRequest)

            If (target.GetIntegerValue("SELECT COUNT(*) FROM Application") > 0) Then
                Dim updateSql = "UPDATE Application SET ParcelTable={0}, NeighborhoodTable={1}, NeighborhoodField={2}, KeyField1={3}, KeyField2={4}, KeyField3={5}, KeyField4={6}, KeyField5={7}, KeyField6={8}, KeyField7={9}, KeyField8={10} ,KeyField9={11}, KeyField10={12},NeighborhoodNameField={13},NeighborhoodNumberField={14},StreetAddressTable={15},StreetAddressField={16},StreetAddressFilter={17},StreetNumberField={18},StreetNameField={19},StreetNameSuffixField={20},StreetAddressFilterValue ={21}, CityField={22}"
                target.Execute(updateSql.SqlFormat(True, request.parcelTable, request.neighborhoodTable, request.neighborhoodField, request.keyField1, request.keyField2, request.keyField3, request.keyField4, request.keyField5, request.keyField6, request.keyField7, request.keyField8, request.keyField9, request.keyField10, request.neighborhoodNameField, request.neighborhoodNumberField, request.streetAddressTable, request.streetAddressField, request.streetAddressFilter, If(request.StreetNumberField, ""), If(request.StreetNameField, ""), If(request.StreetNameSuffixField, ""), If(request.streetAddressFilterValue, ""), If(request.cityField, "")))
            Else
                Dim sql = "INSERT INTO Application(DataModel, ParcelTable, NeighborhoodTable, NeighborhoodField, KeyField1, KeyField2, KeyField3, KeyField4, KeyField5, KeyField6, KeyField7, KeyField8 ,KeyField9, KeyField10,NeighborhoodNameField,NeighborhoodNumberField,StreetAddressTable,StreetAddressField,StreetAddressFilter,StreetNumberField,StreetNameField,StreetNameSuffixField,StreetAddressFilterValue, CityField) VALUES ('APISYNC', {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12},{13},{14},{15},{16},{17},{18},{19},{20},{21}, {22})"
                target.Execute(sql.SqlFormat(True, request.parcelTable, request.neighborhoodTable, request.neighborhoodField, request.keyField1, request.keyField2, request.keyField3, request.keyField4, request.keyField5, request.keyField6, request.keyField7, request.keyField8, request.keyField9, request.keyField10, request.neighborhoodNameField, request.neighborhoodNumberField, request.streetAddressTable, request.streetAddressField, request.streetAddressFilter, If(request.StreetNumberField, ""), If(request.StreetNameField, ""), If(request.StreetNameSuffixField, ""), If(request.streetAddressFilterValue, ""), If(request.cityField, "")))
            End If

        End Sub

        Public Shared Sub SetApplicationSettings(ByVal target As Data.Database, ByRef sp As StandardResponse, ByVal request As ApplicationConfigRequest)
            Dim keyFieldList As New List(Of String)
            If ApplicationConfigHelper.IsValidAppSetting(sp, request) Then
                InsertOrUpdateApplicationConfiguration(target, request)
            End If
        End Sub

        Public Shared Function GetApplicationSettings(ByVal target As Data.Database, ByVal request As ApplicationConfigRequest) As ApplicationResponse
            Dim response As New ApplicationResponse
            Dim sql = "SELECT * FROM Application"
            Dim dtApp = target.GetDataTable(sql)
            For Each dr As DataRow In dtApp.Rows
                response.parcelTable = dr("ParcelTable").ToString()
                response.neighborhoodTable = dr.GetString("NeighborhoodTable").ToString()
                response.neighborhoodField = dr.GetString("NeighborhoodField").ToString()
                response.neighborhoodNameField = dr.GetString("NeighborhoodNameField").ToString()
                response.neighborhoodNumberField = dr.GetString("NeighborhoodNumberField").ToString()
                response.streetAddressTable = StreetAddress.GetStreetAddressTable(target)
                'dr("StreetAddressTable").ToString()
                response.streetAddressField = StreetAddress.GetStreetAddressField(target)
                ' dr("StreetAddressField").ToString()
                response.streetAddressFilter = StreetAddress.GetStreetAddressFormula(target)
                'dr("StreetAddressFilter").ToString()
                response.StreetNameField = StreetAddress.GetStreetNameField(target)
                'dr("StreetNameField").ToString()
                response.StreetNumberField = StreetAddress.GetStreetNumberField(target)
                'dr("StreetNumberField").ToString()
                response.StreetNameSuffixField = StreetAddress.GetStreetNameSuffixField(target)
                'dr("StreetNameSuffixField").ToString()
                response.streetAddressFilterValue = StreetAddress.GetStreetAddressFilterValue(target)
                response.keyField1 = dr.GetString("KeyField1").ToString()
                response.keyField2 = dr.GetString("KeyField2").ToString()
                response.keyField3 = dr.GetString("KeyField3").ToString()
                response.keyField4 = dr.GetString("KeyField4").ToString()
                response.keyField5 = dr.GetString("KeyField5").ToString()
                response.keyField6 = dr.GetString("KeyField6").ToString()
                response.keyField7 = dr.GetString("KeyField7").ToString()
                response.keyField8 = dr.GetString("KeyField8").ToString()
                response.keyField9 = dr.GetString("KeyField9").ToString()
                response.keyField10 = dr.GetString("KeyField10").ToString()
                response.cityField = dr.GetString("CityField")
            Next
            Return response
        End Function

        Private Shared Function IsValidAppSetting(ByRef sp As StandardResponse, ByRef request As ApplicationConfigRequest) As Boolean
            Dim keyFieldList As New List(Of String)
            Dim flag As Boolean = True
            Dim counter As Integer = 0
            If request.parcelTable = Nothing Then
                request.parcelTable = ""
            End If
            If (request.neighborhoodTable = Nothing) Then
                request.neighborhoodTable = ""
            End If
            If (request.keyField2 = Nothing) Then
                request.keyField2 = ""
            End If
            If (request.keyField3 = Nothing) Then
                request.keyField3 = ""
            End If
            If (request.keyField4 = Nothing) Then
                request.keyField4 = ""
            End If
            If (request.keyField5 = Nothing) Then
                request.keyField5 = ""
            End If
            If (request.keyField6 = Nothing) Then
                request.keyField6 = ""
            End If
            If (request.keyField7 = Nothing) Then
                request.keyField7 = ""
            End If
            If (request.keyField8 = Nothing) Then
                request.keyField8 = ""
            End If
            If (request.keyField9 = Nothing) Then
                request.keyField9 = ""
            End If
            If (request.keyField10 = Nothing) Then
                request.keyField10 = ""
            End If
            Dim count As Integer = 0
            keyFieldList.Add(request.keyField1)
            keyFieldList.Add(request.keyField2)
            keyFieldList.Add(request.keyField3)
            keyFieldList.Add(request.keyField4)
            keyFieldList.Add(request.keyField5)
            keyFieldList.Add(request.keyField6)
            keyFieldList.Add(request.keyField7)
            keyFieldList.Add(request.keyField8)
            keyFieldList.Add(request.keyField9)
            keyFieldList.Add(request.keyField10)

            If (request.parcelTable.Length <= 0) Then
                sp.messages.Add("'parcelTable' field is mandatory.")
                flag = False
            End If
            If request.neighborhoodTable.Length <= 0 Then
                sp.messages.Add("'neighborhoodTable' field is mandatory.")
                flag = False
            End If
            If (request.streetAddressTable Is Nothing) Then
                sp.messages.Add("'streetAddressTable' field is mandatory.")
                flag = False
            Else
                If request.streetAddressTable.Length <= 0 Then
                    sp.messages.Add("'streetAddressTable' field is mandatory.")
                    flag = False
                End If
            End If
            If (request.keyField1.Length <= 0) Then
                sp.messages.Add("'keyField1' is mandatory.")
                flag = False
            End If
            For Each keylist In keyFieldList
                If (keylist.Length <= 0) Then
                    counter += 1
                ElseIf (counter >= 1) Then
                    sp.messages.Add("The key fields are not entered in proper order.")
                    flag = False
                    Return flag
                End If
            Next
            Return flag
        End Function
    End Class



End Namespace

