﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts

Public Class UIDataSettingHelper

    Public Shared Sub ClearGridViewFieldSettings(db As Database)
        db.Execute("DELETE FROM UI_GridViewFieldSettings ; DBCC CHECKIDENT (UI_GridViewFieldSettings , RESEED, 0);")
    End Sub
End Class
