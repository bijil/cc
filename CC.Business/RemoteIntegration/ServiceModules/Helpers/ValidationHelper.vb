﻿Imports System.Text.RegularExpressions
Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts

Namespace RemoteIntegration

    Public Class Validator
        'Validation methods
        Public Function isValidDates(ByVal startTime As String, ByVal endTime As String, ByRef sp As StandardResponse) As Boolean
            Dim flag As Boolean = True

            Dim expression As New Regex("^(\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2]\d|3[0-1])\s([0-1]\d|2[0-3])(:[0-5]\d){2})?$")
            If expression.IsMatch(startTime) Then
            Else
                flag = False
                sp.messages.Add("Please ensure parameter periodStart in proper datetime format (dd-mm-yyyy hh:mm:ss).")
            End If

            If expression.IsMatch(endTime) Then
            Else
                flag = False
                sp.messages.Add("Please ensure parameter periodEnd in proper datetime format  (dd-mm-yyyy hh:mm:ss) .")
            End If

            Return flag

        End Function
    End Class


End Namespace
