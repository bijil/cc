﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts

Namespace RemoteIntegration

    Public Class TemplateControllerHelper

        Shared Sub CreateTemplateParcelIfNotExists(db As Database, request As TemplateRequest)
            If Not db.DoRowsExist("Parcel", "Id = -99") Then
                db.Execute("SET IDENTITY_INSERT Parcel ON; INSERT INTO Parcel (Id, KeyValue1) VALUES (-99, '-99'); SET IDENTITY_INSERT Parcel OFF; INSERT INTO ParcelData (CC_ParcelId) VALUES (-99);")
            End If
        End Sub

        Shared Function DumpTableDataIntoTemp(db As Database, request As TemplateRequest, tableName As String, tableDataAsString As String, ByRef targetTable As String) As String

            Dim isLookupTable As Boolean = False
            Dim targetTableOnDatabase As String = DataSource.FindTargetTable(db, tableName, , isLookupTable)

            If targetTableOnDatabase = "ParcelData" Or targetTableOnDatabase = "NeighborhoodData" Or targetTableOnDatabase = String.Empty Or isLookupTable Then
                Throw New Exception("Template Data cannot be set using non-auxiliary tables.")
            End If

            targetTable = targetTableOnDatabase

            Dim tempTableName As String = "temp_template_" + tableName
            Dim ds As New DataSet
            Dim buffer As Byte() = System.Text.Encoding.Default.GetBytes(request.ExtractData(tableDataAsString))
            Dim ms As New IO.MemoryStream(buffer)
            ds.ReadXml(ms)
            ms.Close()

            db.BulkLoadTableFromDataTable(tempTableName, ds.Tables(0))

            Return tempTableName
        End Function

        Shared Sub CopyTemplateData(db As Database, tempParentTable As String, tempChildTable As String, targetParentTable As String, targetChildTable As String, parentKeys As List(Of String), childKeys As List(Of String), parentFilterFields As List(Of String), ByRef parentRecordsCreated As Integer, ByRef childRecordsCreated As Integer)
            parentRecordsCreated = 0
            childRecordsCreated = 0
            For Each pr As DataRow In db.GetDataTable("SELECT * FROM " + tempParentTable).Rows
                Dim parentFilter As String = ""
                For i As Integer = 0 To parentKeys.Count - 1
                    parentFilter = parentFilter.Append(parentKeys(i) + " = " + pr.GetString(parentKeys(i)), " AND ")
                Next

                Dim pcFilter As String = ""
                For i As Integer = 0 To parentKeys.Count - 1
                    pcFilter = pcFilter.Append(childKeys(i) + " = " + pr.GetString(parentKeys(i)), " AND ")
                Next

                Dim pInsertFields As String = "CC_ParcelId", pSelectFields As String = "-99", pJoinFields As String = "d.CC_ParcelId = -99"

                For Each field As String In parentFilterFields
                    pInsertFields += ", " + field.Wrap("[]")
                    pSelectFields += ", td." + field.Wrap("[]")
                    pJoinFields = pJoinFields.Append("td." + field.Wrap("[]") + " = d." + field.Wrap("[]"), " AND ")
                Next

                Dim parentROWUID As Integer = db.GetIntegerValue("INSERT INTO " + targetParentTable + " (" + pInsertFields + ") SELECT " + pSelectFields + " FROM " + tempParentTable + " td LEFT OUTER JOIN " + targetParentTable + " d ON " + pJoinFields + " WHERE d.ROWUID IS NULL AND td." + parentFilter + "; SELECT @@IDENTITY As NewId;")
                'For Each cr As DataRow In db.GetDataTable("SELECT * FROM " + tempChildTable + " WHERE " + pcFilter).Rows


                'Next

                Dim cInsertFields As String = "CC_ParcelId, ParentROWUID", cSelectFields As String = "-99, " & parentROWUID, cJoinFields As String = "d.CC_ParcelId = -99"

                For Each field As String In db.GetDataTable("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = {0} AND COLUMN_NAME IN (SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = {1})".SqlFormatString(tempChildTable, targetChildTable)).AsEnumerable.Select(Function(x) x.GetString("COLUMN_NAME"))
                    cInsertFields += ", " + field.Wrap("[]")
                    cSelectFields += ", td." + field.Wrap("[]")
                    cJoinFields += " AND td." + field.Wrap("[]") + " = d." + field.Wrap("[]")
                Next
                Debug.Print("SELECT " + cSelectFields + " FROM " + tempChildTable + " td LEFT OUTER JOIN " + targetChildTable + " d ON " + cJoinFields + " WHERE d.ROWUID IS NULL AND td." + pcFilter)
                'childRecordsCreated += db.GetIntegerValue("INSERT INTO " + targetChildTable + " (" + cInsertFields + ") SELECT " + cSelectFields + " FROM " + tempChildTable + " td LEFT OUTER JOIN " + targetChildTable + " d ON " + cJoinFields + " WHERE d.ROWUID IS NULL AND td." + pcFilter + "; SELECT @@ROWCOUNT As NewId;")
                childRecordsCreated += db.GetIntegerValue("INSERT INTO " + targetChildTable + " (" + cInsertFields + ") SELECT " + cSelectFields + " FROM " + tempChildTable + " td WHERE td." + pcFilter + "; SELECT @@ROWCOUNT As NewId;")

                parentRecordsCreated += 1
            Next
        End Sub

    End Class

End Namespace
