﻿Imports System.Runtime.Serialization
Imports System.Text.RegularExpressions
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects
Imports CAMACloud.BusinessLogic.RemoteIntegration
Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts


Public Class UpdateTransferHelper

    Public Shared Sub GetPhotoFlagChanges(ByVal target As Data.Database, ByRef _changeList As List(Of ParcelChange), ByRef changeIds As List(Of String), periodStart As DateTime, periodEnd As DateTime)

        Dim sql = "SELECT pc.*, p.QCBy, p.QCDate FROM ParcelChanges pc INNER JOIN parcel p on p.Id=pc.ParcelId WHERE p.QC=1 AND pc.QCApproved = 1 AND p.QCDate BETWEEN {0} AND {1} AND Action='PhotoFlagMain'  ORDER BY pc.Id asc"
        Dim dtChanges As DataTable = target.GetDataTable(sql.SqlFormat(True, periodStart.ToString("yyyy-MM-dd HH:mm:ss"), periodEnd.ToString("yyyy-MM-dd HH:mm:ss")))

        For Each dr As DataRow In dtChanges.Rows
            Dim change As New ParcelChange
            change.ParcelId = dr.GetInteger("ParcelId")
            change.FieldId = dr.GetInteger("FieldId", Nothing)
            change.TableName = Nothing
            change.AuxROWUID = dr.GetInteger("AuxROWUID", Nothing)
            change.ParentROWUID = dr.GetInteger("ParentAuxROWUID", Nothing)
            change.Keys = Nothing
            change.ReviewedBy = dr.GetString("ReviewedBy")
            change.ReviewTime = dr.GetDate("UpdatedTime")
            change.NewValue = dr.GetString("NewValue")
            change.OldValue = dr.GetString("OriginalValue")
            change.QCTime = dr.GetString("QCDate")
            change.Action = dr.GetString("Action")
            change.QCBy = dr.GetString("QCBy")

            changeIds.Add(dr.GetInteger("Id"))
            _changeList.Add(change)
        Next
    End Sub

    Public Shared Function GetFieldChange(ByVal target As Data.Database, ByVal periodStart As DateTime, ByVal periodEnd As DateTime, ByVal parcelId As Integer, ByVal AuxRowUID As Integer) As ChangeField()

        Dim fieldChangeList As New List(Of ChangeField)
        Dim sqlFields = "select distinct MAX(pc.UpdatedTime)As UpdateTime,pc.FieldId,pc.NewValue,pc.OriginalValue from parcelchanges pc INNER JOIN parcel p on p.Id=pc.ParcelId INNER JOIN DatasourceField df on df.Id=fieldId WHERE p.QC=1 AND  p.QCDate BETWEEN {0} AND {1} AND  p.Id={2} AND AuxROWUID={3}   group by FieldId,pc.NewValue,pc.OriginalValue order by UpdateTime desc"
        Dim dtFields As DataTable = target.GetDataTable(sqlFields.SqlFormat(True, periodStart.ToString("yyyy-MM-dd HH:mm:ss"), periodEnd.ToString("yyyy-MM-dd HH:mm:ss"), parcelId, AuxRowUID))

        Dim htFields As New Hashtable
        For Each drfield As DataRow In dtFields.Rows

            Dim fieldChange As New ChangeField

            If (Not htFields.ContainsKey(drfield.GetInteger("FieldId"))) Then

                fieldChange.NewValue = drfield.GetString("NewValue")
                fieldChange.FieldId = drfield.GetInteger("FieldId")
                fieldChange.OldValue = drfield.GetString("OriginalValue")
                htFields.Add(fieldChange.FieldId, fieldChange.NewValue)

                fieldChangeList.Add(fieldChange)

            End If
        Next
        Return fieldChangeList.ToArray
    End Function

    Public Shared Sub UpdateFieldChange(ByVal target As Data.Database, ByVal tableName As String, ByVal fieldName As String, changes As ParcelChange, ByRef recordIndex As Integer, ByRef conflictList As List(Of Integer), ByRef htTable As Hashtable, ByRef recordTracker As RecordCountInfo, ByVal overriteConflicts As Boolean)

        Dim filter = ""
        Dim updateColumns = ""
        Dim targetTable = DataSource.FindTargetTableAndFilterCondition(target, tableName, changes.AuxROWUID, changes.ParcelId, filter)

        Dim datafilter As String = ""
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim value As New List(Of String)
        Dim updatefilterValue As New List(Of String)
        Dim updateValues As New List(Of String)
        Dim fieldFilter = "[" + fieldName + "] ={0}".SqlFormatString(If(changes.NewValue, ""))


        updateColumns = "[" + fieldName + "] ={0}"
        updateValues.Add(If(changes.NewValue, ""))


        If changes.AuxROWUID <> 0 Then
            datafilter += "ROWUID={" + i.ToString() + "}"
            value.Add(changes.AuxROWUID)
            updatefilterValue.Add(changes.AuxROWUID)
        Else
            For Each keyvalue In changes.Keys
                If (datafilter <> "") Then
                    datafilter += " AND "
                    i += 1
                End If

                datafilter += "[" + keyvalue.Key + "] = " + "{" + i.ToString() + "}"
                value.Add(If(keyvalue.Value, ""))
                updatefilterValue.Add(If(keyvalue.Value, ""))
            Next

            If (filter <> "") Then
                datafilter += " AND " + filter
            End If
        End If

        For Each keypair In changes.Keys
            If (keypair.Key <> fieldName) Then
                If updateColumns <> "" Then
                    updateColumns += ","
                    j += 1
                End If
                updateColumns += "[" + keypair.Key + "] =" + "{" + j.ToString() + "}"
                updateValues.Add(If(keypair.Value, ""))
            End If
        Next

        i += 1
        value.Add(changes.ReviewTime)


        Try
            Dim checkSql = "SELECT CASE WHEN CC_LastUpdateTime >= {" + i.ToString() + "} THEN 1 ELSE 0 END As Status FROM " + targetTable + " WHERE " + datafilter
            Dim status = target.GetIntegerValue(checkSql.SqlFormatString(value.ToArray) + " AND " + fieldFilter)

            If status = 0 Or overriteConflicts = True Then
                updateColumns = updateColumns.SqlFormatString(updateValues.ToArray)
                Dim updateSql = "UPDATE " + targetTable + " SET " + updateColumns + ",CC_LastUpdateTime=GETUTCDATE()  WHERE " + datafilter.SqlFormatString(updatefilterValue.ToArray) + ";SELECT CAST(@@ROWCOUNT  AS INT) As recordAffected"
                recordTracker.recordsUpdated += target.GetIntegerValue(updateSql)

                If (htTable.ContainsKey(tableName)) Then
                Else
                    htTable.Add(tableName, fieldName)
                    recordTracker.tablesAffected += 1
                End If
            Else
                conflictList.Add(recordIndex)
                recordTracker.recordsIgnored += 1
            End If

            Dim BPPEnabled As Integer = target.GetIntegerValue("SELECT BPPEnabled FROM APPLICATION WHERE  ROWID = 1")
            If BPPEnabled = 1 Then
                Dim KeyField1 As String = target.GetStringValue("SELECT KeyField1 FROM APPLICATION WHERE  ROWID = 1")
                Dim ParcelTable As String = target.GetStringValue("SELECT ParcelTable FROM APPLICATION WHERE  ROWID = 1")
                Dim ParcelId = target.GetStringValue("SELECT CC_ParcelId FROM " + targetTable + " WHERE " + datafilter.SqlFormatString(updatefilterValue.ToArray))
                If fieldName = KeyField1 AndAlso tableName = ParcelTable Then
                    Parcel.UpdateParcelField(target, ParcelId, fieldName, If(changes.NewValue, ""))
                End If

            End If
            Dim streetTable = StreetAddress.GetStreetAddressTable(target)

            If (streetTable = tableName) Then
                If IsNothing(changes.ParcelId) Or changes.ParcelId = 0 Then
                    Dim strParcelId = target.GetStringValue("SELECT CC_ParcelId FROM " + targetTable + " WHERE " + datafilter.SqlFormatString(updatefilterValue.ToArray))
                    If strParcelId IsNot Nothing AndAlso strParcelId <> "" Then
                        changes.ParcelId = strParcelId
                    End If
                End If
                If IsNothing(changes.ParcelId) Or changes.ParcelId = 0 Then
                    StreetAddress.UpdateStreetAddress(target, changes.ParcelId)
                End If

            End If

        Catch ex As Exception
            Throw New Exception("An error occurred while processing your request. ", ex)
        End Try

    End Sub

    Public Shared Sub cascadeInsert(ByVal target As Data.Database, ByVal dictKeys As Dictionary(Of String, String), ByVal table As String, ByVal tableId As Integer, ByVal change As ParcelChange, ByRef htTable As Hashtable, ByRef recordIndex As Integer, ByRef conflictList As List(Of Integer), ByRef tableTracker As Hashtable, ByRef recordTracker As RecordCountInfo, ByVal overriteConflict As Boolean)

        Dim filter = ""
        Dim tempTable = ""
        Dim dtParent = target.GetDataTable("SELECT dt.Id As TableId,dt.Name as TableName FROM DataSourceTable dt INNER JOIN DataSourceRelationships dsr ON dt.Id=dsr.ParentTableId  WHERE dsr.TableId={0}".SqlFormatString(tableId.ToString()))
        If (dtParent.Rows.Count > 0) Then

            Dim parentTable = dtParent.Rows(0).GetString("TableName")
            Dim parentTableId = dtParent.Rows(0).GetInteger("TableId")
            Dim parentKeys = _getConnectingKeys(target, table, parentTable)
            For Each pkey In parentKeys
                If pkey.Key <> pkey.Value Then
                    If dictKeys.ContainsKey(pkey.Key) And dictKeys.ContainsKey(pkey.Value) = False Then
                        dictKeys.Add(pkey.Value, dictKeys(pkey.Key))
                    End If
                End If
            Next
            cascadeInsert(target, dictKeys, parentTable, parentTableId, change, htTable, recordIndex, conflictList, tableTracker, recordTracker, overriteConflict)
            _insertRecord(target, table, dictKeys, change, htTable, tableTracker, recordIndex, conflictList, recordTracker, overriteConflict)
        Else
            _insertRecord(target, table, dictKeys, change, htTable, tableTracker, recordIndex, conflictList, recordTracker, overriteConflict)
        End If
    End Sub

    Private Shared Sub _insertRecord(ByVal target As Data.Database, ByVal tableName As String, ByVal keys As Dictionary(Of String, String), ByVal change As ParcelChange, ByRef htTable As Hashtable, ByRef tableTracker As Hashtable, ByRef recordIndex As Integer, ByRef conflictList As List(Of Integer), ByRef recordTracker As RecordCountInfo, ByVal overriteConflict As Boolean)

        Dim insertColumns = "CC_ParcelId"
        Dim updateColumns = ""
        Dim insertValues = ""
        Dim i = 0
        Dim j = 0
        Dim datafilter = "CC_ParcelId={0}"
        Dim parentKeys As New Dictionary(Of String, String)
        Dim value As New List(Of String)
        Dim updatevalue As New List(Of String)
        Dim tableKeys As New List(Of String)
        Dim targetTable = DataSource.FindTargetTable(target, tableName)
        Dim UpdateparcelSql = ""
        Dim parcelFields As New List(Of String)
        Dim k = 0

        Dim changeKeys As New Dictionary(Of String, String)

        Dim primaryKeys = DataSource.GetTableKeys(target, tableName)

        For Each pk In primaryKeys
            If parentKeys.ContainsKey(pk) Then
            Else
                parentKeys.Add(pk, pk)
            End If
        Next
        If change.ParcelId > 0 Then
            updateColumns = "CC_ParcelId={0}"
        End If
        insertValues = "{0}"
        value.Add(change.ParcelId)
        updatevalue.Add(change.ParcelId)
        tableKeys.Add(change.ParcelId)

        For Each key In parentKeys
            If keys.ContainsKey(key.Key) Then
                If (insertColumns <> "") Then
                    insertColumns += ","
                    insertValues += ","
                    If (updateColumns <> "") Then
                        updateColumns += ","
                    End If
                    i += 1
                End If
                insertColumns += "[" + key.Key + "]"
                insertValues += "{" + i.ToString() + "}"
                updateColumns += "[" + key.Key + "] = " + "{" + i.ToString() + "}"
                value.Add(If(keys(key.Key), ""))
                updatevalue.Add(If(keys(key.Key), ""))
            End If
        Next

        For Each key In keys
            If (primaryKeys.Contains(key.Key)) Then
                If (datafilter <> "") Then
                    datafilter += " AND "
                    j += 1
                End If
                datafilter += "[" + key.Key + "] =" + "{" + j.ToString() + "}"
                tableKeys.Add(If(key.Value, ""))
            End If
        Next

        If ((change.FieldId.ToString() <> Nothing Or change.FieldId <> 0) And (change.NewValue <> Nothing)) Then
            Dim sql = "SELECT dt.Name As TableName,df.Name As FieldName FROM DataSourceTable dt INNER JOIN DataSourceField df ON df.TableId=dt.Id WHERE df.Id={0}"
            Dim dt = target.GetDataTable(sql.SqlFormatString(change.FieldId.ToString()))
            If dt.Rows.Count > 0 Then
                If (tableName = dt.Rows(0).GetString("TableName")) Then

                    If parentKeys.ContainsKey(dt.Rows(0).GetString("FieldName")) = False Then
                        i += 1
                        If insertColumns <> "" Then
                            insertColumns += ","
                            insertValues += ","
                        End If
                        insertColumns += "[" + dt.Rows(0).GetString("FieldName") + "]"
                        insertValues += "{" + i.ToString() + "}"
                        value.Add(change.NewValue)
                        If updateColumns <> "" Then
                            updateColumns += ","
                        End If
                        updateColumns += "[" + dt.Rows(0).GetString("FieldName") + "] = " + "{" + i.ToString() + "}"
                        updatevalue.Add(change.NewValue)
                        For k = 1 To 10
                            If tableName = target.Application.ParcelTable AndAlso Database.Tenant.Application.KeyField(k).IsNotEmpty Then
                                If Database.Tenant.Application.KeyField(k) = dt.Rows(0).GetString("FieldName") Then
                                    If UpdateparcelSql <> "" Then
                                        UpdateparcelSql = ","
                                    End If
                                    UpdateparcelSql = "[KeyValue" + k.ToString + "] = " + "{" + k.ToString() + "}"
                                    parcelFields.Add(change.NewValue)
                                End If
                            End If
                        Next
                    End If

                End If
            End If

        End If

        If UpdateparcelSql <> "" Then
            Dim upParcelUpdate As String = "UPDATE Parcel SET " + UpdateparcelSql + " WHERE ID= " + change.ParcelId + ";SELECT CAST(@@ROWCOUNT  AS INT)"
            recordTracker.recordsUpdated += target.GetIntegerValue(upParcelUpdate.SqlFormatString(parcelFields.ToArray()))

        End If

        Try

            Dim checkSql = "SELECT COUNT(*) FROM " + targetTable + " WHERE " + datafilter
            If (target.GetIntegerValue(checkSql.SqlFormatString(tableKeys.ToArray())) = 0) Then

                If (tableTracker.ContainsKey(targetTable) = False) Then
                    Dim insertSql = "INSERT INTO " + targetTable + "(" + insertColumns + ") Values(" + insertValues + ");SELECT CAST(@@IDENTITY  AS INT) As recordAffected"
                    Dim AuxId = target.GetIntegerValue(insertSql.SqlFormatString(value.ToArray()))
                    If (AuxId > 0) Then
                        recordTracker.recordsUpdated += 1
                    End If

                    If (tableTracker.ContainsKey(targetTable) = False And AuxId > 0) Then
                        tableTracker.Add(targetTable, AuxId.ToString())
                    End If
                Else
                    Dim upSql = "UPDATE " + targetTable + " SET " + updateColumns + " WHERE ROWUID={0};SELECT CAST(@@ROWCOUNT  AS INT)".SqlFormatString(tableTracker(targetTable))
                    recordTracker.recordsUpdated += target.GetIntegerValue(upSql.SqlFormatString(updatevalue.ToArray()))
                End If


                If (htTable.ContainsKey(tableName)) Then
                Else
                    htTable.Add(tableName, tableName)
                    recordTracker.tablesAffected += 1
                End If
            Else
                If overriteConflict = True Then
                    updateColumns = updateColumns.SqlFormatString(updatevalue.ToArray())
                    Dim updateSql = "UPDATE " + targetTable + " SET " + updateColumns + " WHERE " + datafilter + ";SELECT CAST(@@ROWCOUNT  AS INT) As recordAffected"
                    recordTracker.recordsUpdated += target.GetIntegerValue(updateSql.SqlFormatString(tableKeys.ToArray()))

                    If (htTable.ContainsKey(tableName)) Then
                    Else
                        htTable.Add(tableName, tableName)
                        recordTracker.tablesAffected += 1
                    End If
                Else
                    If (conflictList.Contains(recordIndex) = False) Then
                        conflictList.Add(recordIndex)
                    End If
                    recordTracker.recordsIgnored += 1
                End If
            End If



        Catch e As Exception
            Throw New Exception("An error occurred while processing your request. ", e)
        End Try
    End Sub


    Public Shared Sub CascadeDelete(ByVal target As Data.Database, ByVal tableId As Integer, ByVal table As String, ByVal rowuid As Integer, ByVal Parentuid As Integer, ByVal keyList As Dictionary(Of String, String), ByRef htTable As Hashtable, ByRef recordTracker As RecordCountInfo)
        Dim filter = ""
        Dim targetTable = ""
        If rowuid <> 0 Then
            keyList = _getKeysByAuxROWUID(target, table, rowuid)
        End If

        If keyList.Count = 0 Then
            Return
        End If

        Dim dataFilter = _generateTableFilter(keyList)

        If Parentuid <> 0 And dataFilter <> "" Then
            If Not dataFilter = "" Then
                dataFilter = "( " + dataFilter + " ) "
            End If
            dataFilter += " OR ParentROWUID = " & Parentuid
        End If

        targetTable = DataSource.FindTargetTable(target, table)

        Dim sqlSelect = "SELECT * FROM " + targetTable + " WHERE " + dataFilter
        Dim currentRow As DataRow = target.GetTopRow(sqlSelect)
        If currentRow Is Nothing Then
            Return
        End If

        If table = target.Application.ParcelTable Then
            Dim parcelId As Integer = currentRow("CC_ParcelId")
            ParcelDeleteHelper.DeleteParcels(target, New ParcelMapInfo() {New ParcelMapInfo With {.parcelId = parcelId}})
            recordTracker.parcelDeleted += 1
            recordTracker.tablesAffected += 1
            Return
        End If

        Dim parentROWUID = currentRow.GetInteger("ROWUID")
        Dim dtChildTables As DataTable = target.GetDataTable("SELECT dt.Id As TableId,dt.Name As  TableName,dsr.Id As RelationId FROM DataSourceTable dt INNER JOIN DataSourceRelationships dsr ON dt.Id=dsr.TableId WHERE ParentTableId={0} ".SqlFormatString(tableId.ToString()))
        For Each dtChild As DataRow In dtChildTables.Rows

            Dim childTable = dtChild.GetString("TableName")
            Dim childTableId = dtChild.GetInteger("TableId")
            Dim childTargetTable As String = DataSource.FindTargetTable(target, childTable)

            Dim childTableDef As String = childTargetTable + " c INNER JOIN " + targetTable + " p ON "
            Dim connectingKeys = _getConnectingKeys(target, childTable, table)
            Dim joinConditionFields As String = ""

            For Each key As String In connectingKeys.Keys
                joinConditionFields.Append("(c.[" + key + "] = p.[" + connectingKeys(key) + "])", " AND ")
                'childTableDef += "(c.[" + key + "] = p.[" + connectingKeys(key) + "]) AND "
            Next
            'If childTableDef.Trim.EndsWith("AND") Then childTableDef = childTableDef.Substring(0, childTableDef.Length - 4)

            childTableDef += "(" + joinConditionFields + ") OR (c.ParentROWUID = p.ROWUID AND c.ParentROWUID IS NOT NULL)"

            Dim childDataFilter As String = "p.ROWUID = " & parentROWUID

            Dim sqlSelectChild = "SELECT c.* FROM " + childTableDef + " WHERE " + childDataFilter
            Dim dt As DataTable = target.GetDataTable(sqlSelectChild)
            For Each dr As DataRow In dt.Rows
                Dim childRowUID As Integer = dr.GetInteger("ROWUID")
                CascadeDelete(target, childTableId, childTable, childRowUID, parentROWUID, Nothing, htTable, recordTracker)

                If ParcelEventLog.Enabled Then
                    ParcelEventLog.CreateTableEvent(target, ParcelEventLog.EventType.RowDeleted, ParcelEventLog.ApplicationType.API, dr.GetInteger("CC_ParcelId"), childTableId, childRowUID)
                End If
            Next

            If (htTable.ContainsKey(childTable)) Then
            Else
                htTable.Add(childTable, childTable)
                recordTracker.tablesAffected += 1
            End If
        Next
        Dim rowUIDString As String = ""
        Dim Count As Integer = 0
        Dim MaxCount As Integer
        Dim dtAuxRows As DataTable = target.GetDataTable("SELECT ROWUID FROM  " + targetTable + "  WHERE " + dataFilter)
        MaxCount = dtAuxRows.Rows.Count
        For Each dtRow As DataRow In dtAuxRows.Rows
            Dim uID As Integer = dtRow.GetInteger("ROWUID")
            If (Count = MaxCount - 1) Then
                rowUIDString += "" & uID
            Else
                rowUIDString += "" & uID & ","
            End If
            Count += 1
        Next

        If rowUIDString.Trim <> "" Then

            Dim trashInsert = "INSERT INTO trashed_ParcelChanges(ChangeId, ParcelId,FieldId,ReviewedBy,OriginalValue,NewValue,Latitude,Longitude,ChangedTime,UpdatedTime,LocationUpdatedTime,AuxROWUID,QCChecked,QCApproved,QCApprovedTime,ParentAuxROWUID,Action,DownSynced,SyncID) SELECT Id, ParcelId,FieldId,ReviewedBy,OriginalValue,NewValue,Latitude,Longitude,ChangedTime,UpdatedTime,LocationUpdatedTime,AuxROWUID,QCChecked,QCApproved,QCApprovedTime,ParentAuxROWUID,Action,DownSynced,SyncID FROM ParcelChanges WHERE Id in(SELECT P.Id FROM parcelchanges P INNER JOIN datasourcefield F ON P.FieldId=F.id WHERE SourceTable='" & table & "' AND AuxROWUID IN(" & rowUIDString & "))"
            target.Execute(trashInsert)
            Dim deleteParcelChanges = "DELETE FROM  parcelchanges WHERE Id in(SELECT P.Id FROM parcelchanges P INNER JOIN datasourcefield F ON P.FieldId=F.id WHERE SourceTable='" & table & "'  AND AuxROWUID IN(" + rowUIDString + "));SELECT CAST(@@ROWCOUNT  AS INT) As recordAffected"
            recordTracker.recordsUpdated += target.GetIntegerValue(deleteParcelChanges)
            If (htTable.ContainsKey("parcelchanges")) Then
            Else
                htTable.Add("parcelchanges", "parcelchanges")
                recordTracker.tablesAffected += 1
            End If

            Dim deleteSql = "DELETE FROM  " + targetTable + "  WHERE " + dataFilter + ";SELECT CAST(@@ROWCOUNT  AS INT) As recordAffected"
            recordTracker.recordsUpdated += target.GetIntegerValue(deleteSql)
            If (htTable.ContainsKey(table)) Then
            Else
                htTable.Add(table, table)
                recordTracker.tablesAffected += 1
            End If
        End If


        'If ParcelEventLog.Enabled Then
        '    ParcelEventLog.CreateTableEvent(ParcelEventLog.EventType.RowDeleted, ParcelEventLog.ApplicationType.API, parc
        'End If
    End Sub

    Public Shared Function GetKeyList(ByVal target As Data.Database, ByVal tableName As String, ByVal rowUID As Integer, ByVal parcelId As Integer, Optional ByRef rowExist As Boolean = True) As List(Of KeyValuePair(Of String, String))
        Dim filter As String = ""
        Dim tableId = 0
        Dim keyValueList As New List(Of KeyValuePair(Of String, String))

        Dim targetTable = DataSource.FindTargetTableAndFilterCondition(target, tableName, rowUID, parcelId, filter)
        If (targetTable = String.Empty) Then
            Return keyValueList
        End If

        Dim keys = DataSource.GetTableKeys(target, tableName)

        For Each key As String In keys
            Dim sqlSelect = "SELECT [" + key + "] FROM " + targetTable + " WHERE " + filter
            Dim value = target.GetStringValue(sqlSelect, Nothing)
            keyValueList.Add(New KeyValuePair(Of String, String)(key, value))
        Next

        If (target.GetIntegerValue("SELECT COUNT(*) FROM " + targetTable + " WHERE " + filter) = 0) Then
            rowExist = False
        End If

        Return keyValueList
    End Function

    Public Shared Function GetRecordStatus(ByVal target As Data.Database, ByVal tableName As String, ByVal rowUID As Integer, ByVal parcelId As Integer) As String
        Dim filter As String = ""
        Dim CC_RecordStatus As String = ""
        Dim targetTable = DataSource.FindTargetTableAndFilterCondition(target, tableName, rowUID, parcelId, filter)
        Dim dr As DataRow = target.GetTopRow("SELECT CC_RecordStatus FROM " + targetTable + " WHERE " + filter)

        If IsNothing(dr) = False Then
            CC_RecordStatus = dr.GetString("CC_RecordStatus")
        End If
        Return CC_RecordStatus

    End Function


    Public Shared Function GetFutureData(ByVal target As Data.Database, ByVal tableName As String, ByVal rowUID As Integer, ByVal parcelId As Integer, Optional ByRef rowExist As Boolean = True) As List(Of KeyValuePair(Of String, String))
        Dim filter As String = ""

        Dim tableId = 0
        Dim yearValue As String
        Dim LinkedRowUID As Integer
        Dim yearField As String

        Dim keyValueList As New List(Of KeyValuePair(Of String, String))

        Dim targetTable = DataSource.FindTargetTableAndFilterCondition(target, tableName, rowUID, parcelId, filter)
        yearField = target.GetStringValue("SELECT YearPartitionField FROM DataSourceTable WHERE Name='" + tableName + "'", Nothing)
        Dim Exist = target.GetIntegerValue("SELECT TOP 1 1  FROM DataSourceField WHERE SourceTable='" + tableName + "' AND Name='" + yearField + "'", Nothing)
        If Exist <> 0 Then
            If yearField <> "" Then
                Dim sqlSelect = "SELECT [" + yearField + "] FROM " + targetTable + " WHERE " + filter
                yearValue = target.GetStringValue(sqlSelect, Nothing)
            End If
        End If
        keyValueList.Add(New KeyValuePair(Of String, String)("Year", yearValue))

        Dim sql = "SELECT CC_LinkedROWUID FROM " + targetTable + " WHERE " + filter
        LinkedRowUID = target.GetIntegerValue(sql, Nothing)
        keyValueList.Add(New KeyValuePair(Of String, String)("LinkedROWUID", LinkedRowUID))


        If (target.GetIntegerValue("SELECT COUNT(*) FROM " + targetTable + " WHERE " + filter) = 0) Then
            rowExist = False
        End If

        Return keyValueList
    End Function

    Public Shared Function IsCC_Deleted(ByVal target As Data.Database, ByVal tableName As String, ByVal rowUID As Integer, ByVal parcelId As Integer)

        Dim filter As String = ""
        Dim CC_deleted As Boolean = False
        Dim targetTable = DataSource.FindTargetTableAndFilterCondition(target, tableName, rowUID, parcelId, filter)
        Dim dr As DataRow = target.GetTopRow("SELECT CC_Deleted FROM " + targetTable + " WHERE " + filter)

        If IsNothing(dr) = False Then
            CC_deleted = dr.GetBoolean("CC_Deleted")
        End If

        If CC_deleted Then
            Return True
        Else
            Return False
        End If

    End Function

    'Private Function getKeyValueListByTable(ByVal target As Data.Database, ByVal tableId As Integer, ByVal rowUID As Integer, ByVal parcelId As Integer) As List(Of KeyValuePair(Of String, String))

    '    Dim filter As String = ""
    '    Dim tableName = ""
    '    Dim keyValueList As New List(Of KeyValuePair(Of String, String))

    '    Dim sql = "SELECT DT.Name FROM  DataSourceTable DT  WHERE DT.Id={0}"
    '    tableName = target.GetStringValue(sql.SqlFormatString(tableId))


    '    Dim targetTable = DataSource.FindTargetTableAndFilterCondition(target, tableName, rowUID, parcelId, filter)
    '    If (targetTable = String.Empty) Then
    '        Return keyValueList
    '    End If

    '    Dim sqlKeys = "SELECT f.Name FROM DataSourceRelationships r INNER JOIN DataSourceKeys k ON k.RelationshipId = r.Id INNER JOIN DataSourceField f ON k.FieldId = f.Id WHERE r.Relationship IS NULL AND r.TableId = " + tableId.ToString()
    '    Dim keys = target.GetDataTable(sqlKeys).Rows.Cast(Of DataRow)().Select(Function(x) x.GetString("Name")).ToArray()

    '    For Each key As String In keys
    '        Dim sqlSelect = "SELECT " + key + " FROM " + targetTable + " WHERE " + filter
    '        Dim value = target.GetStringValue(sqlSelect)
    '        keyValueList.Add(New KeyValuePair(Of String, String)(key, value))
    '    Next

    '    Return keyValueList

    'End Function

    Private Shared Function _getConnectingKeys(ByVal db As Database, ByVal tableName As String, ByVal parentTableName As String) As Dictionary(Of String, String)
        Dim sqlKeys As String = "SELECT t1.Name TableName, t2.Name ParentTableName, f1.Name As FieldName, f2.Name As RelatedFieldName FROM DataSourceRelationships r INNER JOIN DataSourceTable t1 ON r.TableId = t1.Id INNER JOIN DataSourceTable t2 ON r.ParentTableId = t2.Id INNER JOIN  DataSourceKeys k ON k.RelationshipId = r.Id INNER JOIN DataSourceField f1 ON  f1.Id = k.FieldId INNER JOIN DataSourceField f2 ON  f2.Id = k.ReferenceFieldId WHERE t1.Name = {0} AND t2.Name = {1}".SqlFormatString(tableName, parentTableName)
        Dim keys = db.GetDataTable(sqlKeys).Rows.Cast(Of DataRow)().Select(Function(x) New String() {x.GetString("FieldName"), x.GetString("RelatedFieldName")})
        Dim dict As New Dictionary(Of String, String)
        For Each keypair In keys
            dict.Add(keypair(0), keypair(1))
        Next
        Return dict
    End Function

    Private Shared Function _getKeysByAuxROWUID(ByVal db As Database, ByVal tableName As String, ByVal auxRowUid As Integer)
        Dim primaryKeys = DataSource.GetTableKeys(db, tableName)
        Dim dict As New Dictionary(Of String, String)
        Dim targetTable = DataSource.FindTargetTable(db, tableName)
        Dim sql = "SELECT TOP 1 1 FROM " + targetTable + " WHERE  ROWUID={0}".SqlFormatString(auxRowUid)
        Dim RowExist = db.GetIntegerValue(sql)
        If RowExist = 1 Then
            For Each key As String In primaryKeys

                Dim sqlSelect = "SELECT [" + key + "] FROM " + targetTable + " WHERE  ROWUID={0}".SqlFormatString(auxRowUid)
                Dim value = db.GetStringValue(sqlSelect)
                dict.Add(key, value)

            Next
        End If
        Return dict
    End Function

    Private Shared Function _generateTableFilter(ByVal keys As Dictionary(Of String, String))
        Dim dataFilter = ""
        Dim i = 0
        Dim values As New List(Of String)
        For Each keyvalue In keys
            If keyvalue.Value IsNot Nothing Then
                If (dataFilter <> "") Then
                    dataFilter += " AND "
                    i += 1
                End If
                dataFilter += "[" + keyvalue.Key + "]=" + "{" + i.ToString() + "}"
                values.Add(keyvalue.Value)
            End If
        Next

        Return dataFilter.SqlFormatString(values.ToArray())
    End Function

    Private Function getKeyValueList(ByVal target As Data.Database, ByVal fieldId As Integer, ByVal rowUID As Integer, ByVal parcelId As Integer) As List(Of KeyValuePair(Of String, String))
        Dim filter As String = ""
        Dim tableName = ""
        Dim tableId = 0
        Dim keyValueList As New List(Of KeyValuePair(Of String, String))

        Dim sql = "SELECT DT.Name,DT.Id FROM DataSourceField DF INNER JOIN DataSourceTable DT ON DF.TableId=DT.Id WHERE DF.Id={0}"
        Dim dt As DataTable = target.GetDataTable(sql.SqlFormatString(fieldId))
        If (dt.Rows.Count > 0) Then
            tableId = dt.Rows(0).GetInteger("Id")
            tableName = dt.Rows(0).GetString("Name")
        End If

        Dim targetTable = DataSource.FindTargetTableAndFilterCondition(target, tableName, rowUID, parcelId, filter)
        If (targetTable = String.Empty) Then
            Return keyValueList
        End If

        Dim keys = DataSource.GetTableKeys(target, tableName)

        For Each key As String In keys
            Dim sqlSelect = "SELECT [" + key + "] FROM " + targetTable + " WHERE " + filter
            Dim value = target.GetStringValue(sqlSelect, Nothing)
            keyValueList.Add(New KeyValuePair(Of String, String)(key, value))
        Next

        Return keyValueList
    End Function

    Private Sub executeUpdateProcess(ByVal target As Data.Database, ByVal tableName As String, ByVal fieldName As String, changes As ParcelChange, ByRef recordIndex As Integer, ByRef conflictList As List(Of Integer), ByRef htTable As Hashtable, ByRef recordTracker As RecordCountInfo, ByVal overriteConflicts As Boolean)

        Dim filter = ""
        Dim updateColumns = ""
        'Dim conflictList As New List(Of Integer)
        ' Dim htTable As New Hashtable

        Dim targetTable = DataSource.FindTargetTableAndFilterCondition(target, tableName, changes.AuxROWUID, changes.ParcelId, filter)

        Dim datafilter As String = ""
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim value As New List(Of String)
        Dim updatefilterValue As New List(Of String)
        Dim updateValues As New List(Of String)
        Dim fieldFilter = fieldName + "={0}".SqlFormatString(If(changes.NewValue, ""))


        If changes.NewValue IsNot Nothing AndAlso changes.NewValue <> "" Then
            updateColumns = fieldName + "={0}"
            updateValues.Add(If(changes.NewValue, ""))
        End If

        If changes.AuxROWUID <> 0 Then
            datafilter += "ROWUID={" + i.ToString() + "}"
            value.Add(changes.AuxROWUID)
            updatefilterValue.Add(changes.AuxROWUID)


            For Each keypair In changes.Keys
                If (keypair.Key <> fieldName) Then
                    If updateColumns <> "" Then
                        updateColumns += ","
                        j += 1
                    End If
                    updateColumns += keypair.Key + "=" + "{" + j.ToString() + "}"
                    updateValues.Add(If(keypair.Value, ""))
                End If
            Next

        Else
            For Each keyvalue In changes.Keys
                If (datafilter <> "") Then
                    datafilter += " AND "
                    i += 1
                End If

                datafilter += keyvalue.Key + " = " + "{" + i.ToString() + "}"
                value.Add(If(keyvalue.Value, ""))
                updatefilterValue.Add(If(keyvalue.Value, ""))
            Next

            If (filter <> "") Then
                datafilter += " AND " + filter
            End If
        End If



        i += 1
        value.Add(changes.ReviewTime)

        If updateColumns.Trim <> "" Then
            updateColumns += ", "
        End If


        Try
            Dim checkSql = "SELECT CASE WHEN CC_LastUpdateTime>{" + i.ToString() + "} THEN 1 ELSE 0 END As Status FROM " + targetTable + " WHERE " + fieldFilter + " AND " + datafilter + ""
            Dim status = target.GetIntegerValue(checkSql.SqlFormatString(value.ToArray))

            If status = 0 Or overriteConflicts = True AndAlso updateColumns.Trim <> "" Then
                updateColumns = updateColumns.SqlFormatString(updateValues.ToArray)
                Dim updateSql = "UPDATE " + targetTable + " SET " + updateColumns + "CC_LastUpdateTime=GETUTCDATE()  WHERE " + datafilter + ";SELECT CAST(@@ROWCOUNT  AS INT) As recordAffected"
                recordTracker.recordsUpdated += target.GetIntegerValue(updateSql.SqlFormatString(updatefilterValue.ToArray))

                If (htTable.ContainsKey(tableName)) Then
                Else
                    htTable.Add(tableName, fieldName)
                    recordTracker.tablesAffected += 1
                End If
            Else
                'conflictList.Add(changes.FieldId, "Overwrite conflict Occurred.")
                conflictList.Add(recordIndex)
                recordTracker.recordsIgnored += 1
            End If

        Catch ex As Exception
            Throw New Exception("An error Occurred while processing your request. ", ex)
        End Try

    End Sub

    'Public Function UpdateParcelChanges(ByVal target As Data.Database, ByVal chageList As ParcelChange(), ByRef recordTracker As RecordCountInfo, ByVal overriteConflicts As Boolean) As List(Of Integer)
    '    Dim conflictList As New List(Of Integer)
    '    Dim htTable As New Hashtable
    '    Dim recordIndex As Integer = 0

    '    For Each changes As ParcelChange In chageList

    '        Dim tableName = ""
    '        Dim fieldName = ""
    '        Dim tableId = 0
    '        Dim filter = ""

    '        If (changes.Action <> Nothing Or changes.Action <> "") Then
    '        Else
    '            changes.Action = "edit"
    '        End If

    '        Dim sql = "SELECT DT.Id As TableId,DT.Name As TableName,DF.Name as FieldName FROM DataSourceField DF INNER JOIN DataSourceTable DT ON DF.TableId=DT.Id WHERE DF.Id={0}"
    '        Dim dt As DataTable = target.GetDataTable(sql.SqlFormatString(changes.FieldId))
    '        If (dt.Rows.Count > 0) Then
    '            fieldName = dt.Rows(0).GetString("FieldName")
    '            tableName = dt.Rows(0).GetString("TableName")
    '            tableId = dt.Rows(0).GetInteger("TableId")
    '        End If


    '        Select Case changes.Action
    '            Case "edit"
    '                executeUpdateProcess(target, tableName, fieldName, changes, recordIndex, conflictList, htTable, recordTracker, overriteConflicts)
    '            Case "new"
    '                Dim tableTracker As New Hashtable
    '                Dim dictionary As New Dictionary(Of String, String)

    '                For Each keyvalue In changes.Keys
    '                    dictionary.Add(keyvalue.Key, keyvalue.Value)
    '                Next
    '                cascadeInsert(target, dictionary, tableName, tableId, changes, htTable, recordIndex, conflictList, tableTracker, recordTracker, overriteConflicts)
    '                ' processCascadeInsert(target, tableName, tableName, tableId, changes, htTable, recordIndex, conflictList, tableTracker, recordTracker, overriteConflicts)
    '            Case "delete"
    '                Dim dictionary As New Dictionary(Of String, String)

    '                If IsNothing(changes.Keys) = False Then
    '                    For Each keyvalue In changes.Keys
    '                        dictionary.Add(keyvalue.Key, keyvalue.Value)
    '                    Next
    '                End If
    '                CascadeDelete(target, tableId, tableName, changes, dictionary, htTable, recordTracker)

    '        End Select

    '        recordIndex += 1
    '    Next
    '    Return conflictList

    'End Function

    Public Shared Function UpdateParcelChanges(ByVal target As Data.Database, ByVal chageList As ParcelChange(), ByRef recordTracker As RecordCountInfo, ByVal overriteConflicts As Boolean) As List(Of Integer)

        Dim conflictList As New List(Of Integer)
        Dim htTable As New Hashtable
        Dim recordIndex As Integer = 0

        For Each ch As ParcelChange In chageList

            Dim tableName = "", fieldName = "", tableId = 0, filter = ""

            If (ch.Action <> Nothing Or ch.Action <> "") Then
            Else
                ch.Action = "edit"
            End If

            Dim sql = "SELECT DT.Id As TableId,DT.Name As TableName,DF.Name as FieldName FROM DataSourceField DF INNER JOIN DataSourceTable DT ON DF.TableId=DT.Id WHERE DF.Id={0}"
            Dim dt As DataTable = target.GetDataTable(sql.SqlFormatString(ch.FieldId))
            If (dt.Rows.Count > 0) Then
                fieldName = dt.Rows(0).GetString("FieldName")
                tableName = dt.Rows(0).GetString("TableName")
                tableId = dt.Rows(0).GetInteger("TableId")
            End If

            Dim dictionary As New Dictionary(Of String, String)

            If IsNothing(ch.Keys) = False Then
                For Each keyvalue In ch.Keys
                    dictionary.Add(keyvalue.Key, keyvalue.Value)
                Next
            End If

            Select Case ch.Action
                Case "edit"
                    UpdateTransferHelper.UpdateFieldChange(target, tableName, fieldName, ch, recordIndex, conflictList, htTable, recordTracker, overriteConflicts)
                    If ParcelEventLog.Enabled Then
                        ParcelEventLog.CreateFieldEvent(target, ParcelEventLog.EventType.FieldUpdated, ParcelEventLog.ApplicationType.API, ch.ParcelId, tableId, ch.AuxROWUID, ch.FieldId)
                    End If

                Case "new"
                    Dim tableTracker As New Hashtable
                    UpdateTransferHelper.cascadeInsert(target, dictionary, tableName, tableId, ch, htTable, recordIndex, conflictList, tableTracker, recordTracker, overriteConflicts)
                Case "delete"
                    UpdateTransferHelper.CascadeDelete(target, tableId, tableName, ch.AuxROWUID, 0, dictionary, htTable, recordTracker)

                    If ParcelEventLog.Enabled Then
                        ParcelEventLog.CreateTableEvent(target, ParcelEventLog.EventType.RowDeleted, ParcelEventLog.ApplicationType.API, ch.ParcelId, tableId, ch.AuxROWUID)
                    End If
            End Select
            recordIndex += 1
        Next
        Return conflictList
    End Function

    Public Shared Sub AddDebugLine(db As Database, message As String)
        db.Execute("INSERT INTO API_Debug (DebugData) VALUES ({0})".SqlFormat(False, message))
    End Sub

    Public Shared Function DownloadParcelChanges(ByVal target As Data.Database, ByVal request As DataTransferRequest, periodStart As DateTime, periodEnd As DateTime) As List(Of ParcelChange)

        Dim pciTable As String = "ParcelChanges"

        If request.downsyncAlternate Then
            pciTable = "ParcelChangesAlt"
        End If

        Dim htInsertCheck As New Hashtable
        Dim _changeList As New List(Of ParcelChange)
        Dim _removedchangeList As New List(Of ParcelChange)

        Dim fieldSelectClause As String = ""
        If request.acceptFields <> "" Then
            fieldSelectClause += " pc.FieldId IN (" & request.acceptFields & ")"
        End If
        If request.ignoreFields <> "" Then
            If fieldSelectClause <> "" Then
                fieldSelectClause += " AND "
                fieldSelectClause += " pc.FieldId NOT IN (" & request.ignoreFields & ")"
            End If
        End If
        If fieldSelectClause <> "" Then
            fieldSelectClause = " AND (" + fieldSelectClause + ")"
        End If

        Dim fieldTableMap = target.GetDataTable("SELECT df.Id As FieldId, dt.Name As TableName, COALESCE(dt.DestinationAlias, dt.Name) As DestinationName FROM DataSourceTable dt INNER JOIN DataSourceField df ON df.TableId=dt.Id").AsEnumerable.Select(Function(x) New With {.FieldId = x.GetInteger("FieldId"), .TableName = x.GetString("TableName"), .DestinationName = x.GetString("DestinationName")}).ToDictionary(Function(x) x.FieldId)

        'Dim sql = "SELECT pc.*, p.QCBy, p.QCDate, f.TableId, f.DataType, pfc.SourceTableName As ParentTableName FROM " + pciTable + " pc INNER JOIN DataSourceField f ON pc.FieldId = f.Id INNER JOIN parcel p on p.Id=pc.ParcelId LEFT OUTER JOIN FieldCategory fc ON f.TableId = fc.SourceTableId AND pc.Action <> 'delete' LEFT OUTER JOIN FieldCategory pfc ON fc.ParentCategoryId = pfc.Id WHERE f.IsCustomField = 0 AND p.QC=1 AND pc.QCApproved = 1 AND p.QCDate BETWEEN {0} AND {1} AND pc.FieldId IS NOT NULL AND (pc.Action <> 'PhotoFlagMain' OR pc.Action IS NULL) AND p.FailedOnDownSync = 0" & fieldSelectClause & " ORDER BY pc.Id asc"
        'Dim dtChanges As DataTable = target.GetDataTable(sql.SqlFormat(True, periodStart.ToString("yyyy-MM-dd HH:mm:ss"), periodEnd.ToString("yyyy-MM-dd HH:mm:ss")))

        Dim sqlNew As String = "EXEC api_transfer_update_downsync_GetPCIList {0}, {1}, {2}, {3}, {4}".SqlFormat(False, periodStart, periodEnd, 200, pciTable, fieldSelectClause)
        Dim ds As DataSet = target.GetDataSet(sqlNew)
        Dim dtChanges As DataTable = ds.Tables(0)
        Dim trackedChanges As DataTable = ds.Tables(1)

        Dim newRecords As New List(Of String)
        Dim changeIds As New List(Of String)
        Dim removedchangeIds As New List(Of String)
        Dim keyList As New Dictionary(Of String, List(Of KeyValuePair(Of String, String)))

        Dim LinkedRowkeyList As New Dictionary(Of String, List(Of KeyValuePair(Of String, String)))
        Dim rowNotExistList As New List(Of String)

        AddDebugLine(target, "Prepared {0} changes for downsync.".FormatString(dtChanges.Rows.Count))

        For Each dr As DataRow In dtChanges.Rows
            Dim TrackedFields As New List(Of TrackingField)
            Dim rowExist As Boolean = True
            Dim recordIdentifier As String = dr.GetInteger("ParcelId") & "-" & dr.GetInteger("TableId") & "-" & dr.GetInteger("AuxROWUID")
            Dim change As New ParcelChange
            Dim trackedrows() As DataRow = trackedChanges.Select("changeID = '" + dr.GetString("ID") + "'")
            For Each dd As DataRow In trackedrows
                Dim TrackingFieldValues As New TrackingField
                TrackingFieldValues.FieldName = dd.GetString("name")
                TrackingFieldValues.TableName = dd.GetString("TableName")
                TrackingFieldValues.Value = dd.GetString("value")
                TrackedFields.Add(TrackingFieldValues)
                ' TrackedFields.Add(New TrackingField With {.FieldName = dd.GetString("name"), .Value = dd.GetString("value"), .TableName = dd.GetString("TableName")}).
            Next
            change.ParcelId = dr.GetInteger("ParcelId")
            change.FieldId = dr.GetInteger("FieldId")
            change.ChangeID = dr.GetInteger("Id")

            Dim sourceTableName As String = fieldTableMap(change.FieldId).TableName  'target.GetStringValue("SELECT dt.Name FROM DataSourceTable dt INNER JOIN DataSourceField df ON df.TableId=dt.Id WHERE df.Id={0}".SqlFormatString(change.FieldId))

            change.TableName = fieldTableMap(change.FieldId).TableName 'target.GetStringValue("SELECT COALESCE(dt.DestinationAlias, dt.Name) FROM DataSourceTable dt INNER JOIN DataSourceField df ON df.TableId=dt.Id WHERE df.Id={0}".SqlFormatString(change.FieldId))
            change.AuxROWUID = dr.GetInteger("AuxROWUID")
            change.ParentROWUID = dr.GetInteger("ParentAuxROWUID", Nothing)
            change.ParentTableName = dr.Get("ParentTableName")
            change.TrackedFieldValues = TrackedFields
            'Dim rowKey As String = sourceTableName + "+" + change.AuxROWUID.ToString
            If Not keyList.ContainsKey(recordIdentifier) Then
                keyList.Add(recordIdentifier, UpdateTransferHelper.GetKeyList(target, sourceTableName, change.AuxROWUID.ToString(), change.ParcelId.ToString(), rowExist))
                If Not rowExist Then
                    rowNotExistList.Add(recordIdentifier)
                End If
            End If

            Dim keyValueList As New List(Of KeyValuePair(Of String, String))
            keyValueList = UpdateTransferHelper.GetFutureData(target, sourceTableName, change.AuxROWUID.ToString(), change.ParcelId.ToString(), rowExist)


            For Each keypair In keyValueList
                If (keypair.Key = "Year") Then
                    change.Year = keypair.Value
                ElseIf (keypair.Key = "LinkedROWUID") Then
                    change.LinkedROWUID = keypair.Value
                End If
            Next
            If change.LinkedROWUID <> 0 Then

                If Not LinkedRowkeyList.ContainsKey(recordIdentifier) Then
                    LinkedRowkeyList.Add(recordIdentifier, UpdateTransferHelper.GetKeyList(target, sourceTableName, change.LinkedROWUID.ToString(), change.ParcelId.ToString(), rowExist))
                End If
                change.LinkingrowuidPKs = LinkedRowkeyList(recordIdentifier)
            End If

            change.Keys = keyList(recordIdentifier) 'UpdateTransferHelper.GetKeyList(target, sourceTableName, change.AuxROWUID.ToString(), change.ParcelId.ToString(), rowExist)
            rowExist = Not rowNotExistList.Contains(recordIdentifier)

            change.ReviewedBy = dr.GetString("ReviewedBy")
            change.ReviewTime = dr.GetDate("ChangedTime")
            change.NewValue = dr.GetString("NewValue")
            change.OldValue = dr.GetString("OriginalValue")
            change.QCTime = dr.GetString("QCDate")
            change.Action = dr.GetString("Action")
            change.QCBy = dr.GetString("QCBy")
            change.Group = dr.GetString("Neighborhood")
            'Fixing BIT/YesNo issues in PCI
            If dr.GetInteger("DataType") = 3 Then
                If change.NewValue.ToLower = "false" Then
                    change.NewValue = 0
                ElseIf change.NewValue.ToLower = "true" Then
                    change.NewValue = 1
                End If
            End If

            If change.Action = "" Then
                If (UpdateTransferHelper.IsCC_Deleted(target, sourceTableName, change.AuxROWUID.ToString(), change.ParcelId.ToString())) Then
                    change.Action = "delete"
                Else
                    If change.Keys.FindAll(Function(x) x.Value Is Nothing Or x.Value = "").Count = 0 AndAlso UpdateTransferHelper.GetRecordStatus(target, sourceTableName, change.AuxROWUID.ToString(), change.ParcelId.ToString()) <> "I" Then
                        change.Action = "edit"
                    Else
                        change.Action = "new"
                    End If
                End If
            End If



            If change.Action = "new" Then
                newRecords.Add(recordIdentifier)
            ElseIf newRecords.Contains(recordIdentifier) And change.Action <> "delete" Then
                If (target.GetIntegerValue("SELECT COUNT(*) FROM " + pciTable + " pc INNER JOIN DataSourceField f ON f.Id=pc.FieldId INNER JOIN DataSourceTable dt ON dt.Id=f.TableId WHERE AuxROWUID={0} AND dt.Name={1} AND DownSynced=1 AND Action='new'".SqlFormatString(change.AuxROWUID, sourceTableName)) > 0) Then
                    change.Action = "edit"
                Else
                    change.Action = "new"
                End If
            End If

            Select Case change.Action
                Case "new"
                    If change.Keys.FindAll(Function(x) x.Value Is Nothing Or x.Value = "").Count = 0 AndAlso UpdateTransferHelper.GetRecordStatus(target, sourceTableName, change.AuxROWUID.ToString(), change.ParcelId.ToString()) <> "I" Then
                        change.Action = "edit"
                    End If
                Case "edit"
                    If change.Keys.FindAll(Function(x) x.Value Is Nothing Or x.Value = "").Count > 0 AndAlso UpdateTransferHelper.GetRecordStatus(target, sourceTableName, change.AuxROWUID.ToString(), change.ParcelId.ToString()) = "I" AndAlso change.LinkedROWUID = 0 Then
                        change.Action = "new"
                    End If
            End Select
            changeIds.Add(dr.GetInteger("Id"))
            If _changeList.FindAll(Function(x) x.AuxROWUID = change.AuxROWUID And x.FieldId = change.FieldId And x.ParcelId = change.ParcelId And x.TrackedFieldValues.Select(Function(a) a.Value).Aggregate("tracking~", Function(a, b) a + "~" + b) = change.TrackedFieldValues.Select(Function(a) a.Value).Aggregate("tracking~", Function(a, b) a + "~" + b)).Count > 0 Then
                Dim pc As ParcelChange = _changeList.Find(Function(x) x.AuxROWUID = change.AuxROWUID And x.FieldId = change.FieldId And x.ParcelId = change.ParcelId And x.TrackedFieldValues.Select(Function(a) a.Value).Aggregate("tracking~", Function(a, b) a + "~" + b) = change.TrackedFieldValues.Select(Function(a) a.Value).Aggregate("tracking~", Function(a, b) a + "~" + b))
                If (pc.ChangeID < change.ChangeID And Not (change.Action = "MarkAsComplete" And pc.Action = "edit")) Then
                    pc.NewValue = change.NewValue
                End If
                If Not (change.Action = "MarkAsComplete" And pc.Action = "edit") Then
                    removedchangeIds.Add(dr.GetInteger("Id"))
                    Continue For
                End If
            End If

                If rowExist = True Then
                _changeList.Add(change)
                AddDebugLine(target, "Adding change #{0} with action {1}.".FormatString(changeIds.Count, change.Action))
            End If



            If newRecords.Contains(recordIdentifier) And change.Action = "delete" Then
                If _changeList.FindAll(Function(x) x.AuxROWUID = change.AuxROWUID And x.TableName = change.TableName).Count > 1 Then
                    _removedchangeList.Add(change)
                End If
                _changeList.RemoveAll(Function(x) x.AuxROWUID = change.AuxROWUID And x.TableName = change.TableName)
                '_removedchangeList.Add(_changeList.Find(Function(x) x.AuxROWUID = change.AuxROWUID And x.TableName = change.TableName))
                AddDebugLine(target, "Removing changes (1) for {0}, AuxRowUID {1}.".FormatString(change.TableName, change.AuxROWUID))
                'ElseIf change.Action = "delete" Then
                '    If _changeList.FindAll(Function(x) x.AuxROWUID = change.AuxROWUID And x.TableName = change.TableName And x.Action <> "delete").Count > 0 Then
                '        _removedchangeList.Add(change)
                '    End If

                '    _changeList.RemoveAll(Function(x) x.AuxROWUID = change.AuxROWUID And x.TableName = change.TableName And x.Action <> "delete")
                '    '_removedchangeList.Add(_changeList.Find(Function(x) x.AuxROWUID = change.AuxROWUID And x.TableName = change.TableName And x.Action <> "delete"))
                '    AddDebugLine(target, "Removing changes (2) for {0}, AuxRowUID {1}.".FormatString(change.TableName, change.AuxROWUID))
            End If

            If change.Action = "delete" Then
                If change.Keys.FindAll(Function(x) x.Value Is Nothing Or x.Value = "").Count > 0 AndAlso change.LinkedROWUID = 0 Then
                    If _changeList.FindAll(Function(x) x.AuxROWUID = change.AuxROWUID And x.TableName = change.TableName And x.Action = "delete").Count > 0 Then
                        _removedchangeList.Add(change)
                    End If
                    _changeList.RemoveAll(Function(x) x.AuxROWUID = change.AuxROWUID And x.TableName = change.TableName And x.Action = "delete")
                    '_removedchangeList.Add(_changeList.Find(Function(x) x.AuxROWUID = change.AuxROWUID And x.TableName = change.TableName And x.Action = "delete"))
                    AddDebugLine(target, "Removing changes (3) for {0}, AuxRowUID {1}.".FormatString(change.TableName, change.AuxROWUID))
                End If
            End If
        Next

        'UpdateTransferHelper.GetPhotoFlagChanges(target, _changeList, changeIds, periodStart, periodEnd)

        AddDebugLine(target, "Updating Downsync flag for {0} changes.".FormatString(changeIds.Count))
        For i As Integer = 0 To changeIds.Count - 1 Step 100
            'Dim si As Integer = i
            'Dim ei As Integer = Math.Min(i + 99, changeIds.Count - 1)
            Dim ids As String = String.Join(",", changeIds.Skip(i).Take(100).ToArray)
            If ids.Trim <> "" Then
                Dim updateSql As String = "UPDATE " + pciTable + " SET DownSynced = 1 WHERE Id IN (" + ids + ")"
                AddDebugLine(target, updateSql)
                target.Execute(updateSql)
            End If
        Next

        For Each p As ParcelChange In _removedchangeList
            removedchangeIds.Add(p.ChangeID)
        Next

        For i As Integer = 0 To removedchangeIds.Count - 1 Step 100
            'Dim si As Integer = i
            'Dim ei As Integer = Math.Min(i + 99, changeIds.Count - 1)
            Dim ids As String = String.Join(",", removedchangeIds.Skip(i).Take(100).ToArray)
            If ids.Trim <> "" Then
                Dim updateSql As String = "UPDATE " + pciTable + " SET DownSynced = 1 WHERE Id IN (" + ids + ")"
                AddDebugLine(target, updateSql)
                target.Execute(updateSql)
            End If
        Next

        AddDebugLine(target, "Finalized {0} changes.".FormatString(_changeList.Count))
        Return _changeList
    End Function

End Class
