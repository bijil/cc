﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts
Imports CAMACloud.BusinessLogic.RemoteIntegration
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects

Public Class ParcelTransferHelper

    Public Shared Function GetParcelIdFromFilter(ByVal target As Data.Database, ByVal keys As List(Of KeyValuePair(Of String, String)))

        Dim filter = ""
        Dim i As Integer = 1
        For Each keypair In keys
            Dim column = "KeyValue" + i.ToString()
            If filter <> "" Then
                filter += " AND "
            End If

            filter += column + "=" + keypair.Value.ToString().ToSqlValue
            i += 1
        Next

        Dim parcelId = target.GetStringValue("SELECT Id FROM Parcel WHERE " + filter)
        Return parcelId

    End Function

    Private Shared Function clearAllParcelData(ByVal target As Data.Database, ByVal tableName As String, filter As String) As Integer
        Return target.Execute("DELETE FROM " + tableName + " WHERE " + filter)
    End Function

    Public Shared Function ClearAllParcelRelatedTables(ByVal target As Data.Database, parcelId As String) As Integer
        Dim recordsAffected As Integer = 0
        recordsAffected += clearAllParcelData(target, "ParcelChanges", "ParcelId={0}".SqlFormatString(parcelId))
        recordsAffected += clearAllParcelData(target, "ParcelSketch", "ParcelId={0}".SqlFormatString(parcelId))
        recordsAffected += clearAllParcelData(target, "ParcelSketchFlag", "ParcelId={0}".SqlFormatString(parcelId))
        recordsAffected += clearAllParcelData(target, "ParcelImages", "ParcelId={0}".SqlFormatString(parcelId))
        recordsAffected += clearAllParcelData(target, "ParcelComparables", "ParcelId={0}".SqlFormatString(parcelId))
        recordsAffected += clearAllParcelData(target, "ParcelMapPoints", "ParcelId={0}".SqlFormatString(parcelId))
        recordsAffected += clearAllParcelData(target, "GIS_Points", "ParcelId={0}".SqlFormatString(parcelId))
        recordsAffected += clearAllParcelData(target, "ParcelReviewLog", "ParcelId={0}".SqlFormatString(parcelId))
        recordsAffected += clearAllParcelData(target, "Parcel", "Id={0}".SqlFormatString(parcelId))
        Return recordsAffected

    End Function

    Public Shared Sub FindParcel(ByVal target As Data.Database, parcelId As String)
        If (target.GetIntegerValue("SELECT COUNT(*) FROM Parcel P INNER JOIN ParcelImages I on  P.Id = I.ParcelId where P.Id={0}".SqlFormatString(parcelId)) > 0) Then
        Else
            Throw New Exception("There is no such specified parcel/Parcels existing")
        End If

    End Sub

    'transfer/update/downsync2

    Public Shared Function DownloadParcel(ByVal target As Data.Database, periodStart As DateTime, periodEnd As DateTime) As List(Of ParcelTableChange)

        Dim _changeList As New List(Of ParcelTableChange)
        Dim sql = "SELECT DISTINCT df.TableId FROM ParcelChanges pc INNER JOIN parcel p on p.Id=pc.ParcelId INNER JOIN DataSourceField df ON df.Id=pc.FieldId WHERE  p.QC=1 AND pc.QCApproved = 1 AND p.QCDate BETWEEN {0} AND {1} AND pc.FieldId IS NOT NULL"
        Dim dt As DataTable = target.GetDataTable(sql.SqlFormat(True, periodStart.ToString("yyyy-MM-dd HH:mm:ss"), periodEnd.ToString("yyyy-MM-dd HH:mm:ss")))

        For Each dr As DataRow In dt.Rows

            Dim tableId = dr.GetInteger("TableId")
            Dim tableName = target.GetStringValue("SELECT NAME FROM DataSourceTable WHERE Id={0}".SqlFormatString(tableId))
            Dim sqlString = "select distinct p.Id ,pc.ParentAuxROWUID,pc.AuxROWUID,MAX(pc.UpdatedTime)As UpdatedTime,pc.Action ,pc.QCApprovedTime from parcelchanges pc INNER JOIN parcel p on p.Id=pc.ParcelId INNER JOIN DatasourceField df on df.Id=pc.fieldId WHERE  p.QC=1 AND pc.QCApproved = 1 AND p.QCDate BETWEEN {0} AND {1} AND df.TableId={2} group by pc.QCApprovedTime,p.Id ,pc.AuxROWUID,pc.ParentAuxROWUID,pc.Action"
            Dim dtParcels As DataTable = target.GetDataTable(sqlString.SqlFormat(True, periodStart.ToString("yyyy-MM-dd HH:mm:ss"), periodEnd.ToString("yyyy-MM-dd HH:mm:ss"), tableId))

            For Each drParcel As DataRow In dtParcels.Rows

                Dim change As New ParcelTableChange
                Dim fieldRowList As New List(Of ChangeField)
                change.TableId = tableId
                change.TableName = tableName
                change.ParcelId = drParcel.GetInteger("Id")
                change.AuxROWUID = drParcel.GetInteger("AuxROWUID")
                change.ParentROWUID = drParcel.GetInteger("ParentAuxROWUID", Nothing)
                change.ReviewTime = drParcel.GetDate("UpdatedTime")
                change.Keys = UpdateTransferHelper.GetKeyList(target, change.TableName, change.AuxROWUID.ToString(), change.ParcelId.ToString())
                change.rowdata = UpdateTransferHelper.GetFieldChange(target, periodStart, periodEnd, change.ParcelId, change.AuxROWUID)
                change.ReviewedBy = target.GetStringValue("select pc.ReviewedBy from parcelchanges pc INNER JOIN parcel p on p.Id=pc.ParcelId INNER JOIN DatasourceField df on df.Id=fieldId WHERE p.QC=1 AND pc.QCApproved = 1 AND pc.AuxROWUID={0} AND df.TableId={1} AND p.Id={2} ".SqlFormatString(change.AuxROWUID, tableId, change.ParcelId))
                change.QCTime = drParcel.GetString("QCApprovedTime")
                change.QCBy = target.GetStringValue("select p.QCBy from parcelchanges pc INNER JOIN parcel p on p.Id=pc.ParcelId INNER JOIN DatasourceField df on df.Id=fieldId WHERE p.QC=1 AND pc.QCApproved = 1 AND pc.AuxROWUID={0} AND df.TableId={1} AND p.Id={2} ".SqlFormatString(change.AuxROWUID, tableId, change.ParcelId))
                change.Action = drParcel.GetString("Action")
                _changeList.Add(change)

            Next
        Next

        Return _changeList
    End Function

    ''ParcelUpdator
    'Public Shared Function RevertParcelChanges(target As Data.Database, ByRef recordTracker As RemoteIntegration.DataContracts.RecordCountInfo) As List(Of Integer)

    '    Dim recordIndex As Integer = 0
    '    Dim conFlictList As New List(Of Integer)
    '    For Each parcelKeys As Dictionary(Of String, String) In ParcelUpdator.keys
    '        Dim parcelId = _generateParcelFilterString(target, parcelKeys)
    '        If parcelId <> "" Then
    '            Dim updatedCount = Parcel.RollbackQCstatus(target, parcelId)
    '            Neighborhood.MarkAsNotCompleted(target, parcelId)
    '            'Entered to rejection log
    '            target.Execute("INSERT INTO ParcelRejectLog SELECT Id,ReviewedBy,ReviewDate,GETUTCDATE(),'','','API' FROM Parcel WHERE Id={0}".SqlFormatString(parcelId))
    '            If updatedCount > 0 Then
    '                ProcessParcelDataChange.RevertChanges(target, parcelId)
    '                ProcessParcelDataChange.RevertSketchChanges(target, parcelId)
    '                ProcessParcelDataChange.DeleteChanges(target, parcelId)
    '                ParcelAuditStream.CreateFlagEvent(target, DateTime.Now, parcelId, "API", "Rolled-back all changed using API Rollback.")
    '                recordTracker.recordsUpdated += updatedCount
    '            End If
    '            recordIndex += 1
    '        Else
    '            recordTracker.recordsIgnored += 1
    '            conFlictList.Add(recordIndex)
    '        End If
    '    Next
    '    Return conFlictList
    'End Function

    'Private Shared Function _generateParcelFilterString(target As Data.Database, keypairs As Dictionary(Of String, String)) As String

    '    Dim dataFilter = ""
    '    Dim i As Integer = 1
    '    For Each key In keypairs
    '        If dataFilter <> "" Then
    '            dataFilter += " AND "
    '        End If
    '        dataFilter += "KeyValue" + i.ToString() + "=" + key.Value.ToSqlValue
    '    Next

    '    Dim ParcelId = target.GetStringValue("SELECT Id FROM Parcel WHERE " + dataFilter + " AND Reviewed=1")
    '    Return ParcelId
    'End Function

    ''ParcelMap
    'Public Function GetParcel(ByVal target As Data.Database) As String
    '    Dim i As Integer = 0


    '    Dim datafilter As String = String.Empty
    '    Dim filterValues As String = String.Empty
    '    Dim value(ParcelMapInfo.keys.Count) As String
    '    For Each keyvalue In ParcelMapInfo.keys
    '        If (datafilter <> "") Then
    '            datafilter += " AND "
    '            i += 1
    '        End If
    '        If (filterValues <> "") Then
    '            filterValues += ","
    '        End If

    '        datafilter += keyvalue.Key + " = " + "{" + i.ToString() + "}"
    '        filterValues += keyvalue.Value
    '        value(i) = keyvalue.Value.ToString()
    '    Next

    '    Dim sql = "SELECT CC_ParcelId FROM parceldata WHERE " + datafilter.FormatString(value)
    '    ParcelMapInfo.parcelId = target.GetStringValue(sql)
    '    Return ParcelMapInfo.parcelId
    'End Function

End Class
'