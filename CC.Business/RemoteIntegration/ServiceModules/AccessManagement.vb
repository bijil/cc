﻿Imports CAMACloud.RemoteIntegration
Imports CAMACloud.BusinessLogic.RemoteIntegration
Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts
Imports System.Web.Security
Namespace RemoteIntegration

    Public Class AccessManagement

        Public Shared Function CreateUser(ByVal db As Database, ByVal request As AccessRequest) As StandardResponse

            Dim userList As New List(Of UserInfo)
            Dim sp As New StandardResponse
            Try
                If request.username = "admin" Then
                    Throw New Exception("Reserved username - admin.")
                End If
                Dim aet As AuditEventType = Nothing
                aet = SEC_20001_ADDUSER
                Dim userId = AccessManagementHelper.UserCreation(db, request, sp)
                If (userId <> "0") Then
                    SystemAuditStream.CreateStandardEvent(db, aet, request.username)
                End If
            Catch nex As NullReferenceException
                Return New StandardResponse(nex, request, 1)
            Catch ex As Exception
                Return New StandardResponse(ex, request)
            End Try

            If sp.messages.Count > 0 Then
                Return New StandardResponse(400) With {.messages = sp.messages, .data = sp.data}
            Else
                Return sp
            End If

        End Function

        Public Shared Function EditUser(ByVal db As Database, ByVal request As AccessRequest) As StandardResponse
            Dim sp As New StandardResponse
            Dim userList As New List(Of UserInfo)
            Try
                Dim aet As AuditEventType = Nothing
                AccessManagementHelper.UserEdit(db, request, sp)
                Select Case request.status
                    Case "modify"
                        aet = SEC_20002_EDITUSER
                        SystemAuditStream.CreateStandardEvent(db, aet, request.username, request.status)
                    Case "delete"
                        aet = SEC_20002_EDITUSER
                        SystemAuditStream.CreateStandardEvent(db, aet, request.username, request.status)
                    Case "unlock"
                        aet = SEC_20003_UNLK
                        SystemAuditStream.CreateStandardEvent(db, aet, request.username)
                    Case "lock"
                        aet = SEC_20004_LOCK
                        SystemAuditStream.CreateStandardEvent(db, aet, request.username)
                End Select

            Catch nex As NullReferenceException
                Return New StandardResponse(nex, request, 1)
            Catch ex As Exception
                Return New StandardResponse(ex, request)
            End Try

            If sp.messages.Count > 0 Then
                Return New StandardResponse(400) With {.messages = sp.messages, .data = sp.data}
            Else
                Return sp
            End If
            
        End Function


        Public Shared Function ListUsers(ByVal db As Database, ByVal request As AccessRequest) As StandardResponse
          
            Dim userList As New List(Of UserInfo)
            Try
                userList = AccessManagementHelper.ListUser(db, request)

            Catch nex As NullReferenceException
                Return New StandardResponse(nex, request, 1)
            Catch ex As Exception
                Return New StandardResponse(ex, request)
            End Try

            Dim userListString = DataTools.JSON.Serialize(Of List(Of UserInfo))(userList)
            Dim sp As New StandardResponse() With {.data = DataTools.DataConversion.EncodeToBase64(userListString), .recordCount = userList.Count}
            Return sp
            
        End Function

    End Class
End Namespace

