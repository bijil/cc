﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects
Namespace RemoteIntegration

    Public Class ApplicationConfigManagement

        Public Shared Function SetApplicationConfig(ByVal target As Data.Database, ByVal request As ApplicationConfigRequest) As StandardResponse

            Dim sp As New StandardResponse
            Try
                'request.SetApplicationSettings(target, sp)
                ApplicationConfigHelper.SetApplicationSettings(target, sp, request)

            Catch nex As NullReferenceException
                Return New StandardResponse(nex, request, 1)
            Catch ex As Exception
                Return New StandardResponse(ex, request)
            End Try
            If sp.messages.Count > 0 Then
                Return New StandardResponse(400) With {.messages = sp.messages}
            Else
                Return sp
            End If
        End Function
        Public Shared Function GetApplicationConfig(ByVal target As Data.Database, ByVal request As ApplicationConfigRequest) As StandardResponse
            Dim sp As New StandardResponse
            Dim response As New ApplicationResponse
          
            Try
                '  request.GetApplicationSettings(target)
                response = ApplicationConfigHelper.GetApplicationSettings(target, request)
            Catch nex As NullReferenceException
                Return New StandardResponse(nex, request, 1)
            Catch ex As Exception
                Return New StandardResponse(ex, request)
            End Try

            'sp.header = New ApplicationResponse With
            '        {.parcelTable = request.parcelTable,
            '         .neighborhoodTable = request.neighborhoodTable,
            '         .neighborhoodField = request.neighborhoodField,
            '         .neighborhoodNameField = request.neighborhoodNameField,
            '         .neighborhoodNumberField = request.neighborhoodNumberField,
            '         .streetAddressTable = request.streetAddressTable,
            '         .streetAddressField = request.streetAddressField,
            '         .streetAddressFilter = request.streetAddressFilter,
            '         .keyField1 = request.keyField1,
            '         .keyField2 = request.keyField2,
            '         .keyField3 = request.keyField3,
            '         .keyField4 = request.keyField4,
            '         .keyField5 = request.keyField5,
            '         .keyField6 = request.keyField6,
            '         .keyField7 = request.keyField7,
            '         .keyField8 = request.keyField8,
            '         .keyField9 = request.keyField9,
            '         .keyField10 = request.keyField10
            '                                       }

            sp.header = response
                    Return sp

        End Function

    End Class

End Namespace
