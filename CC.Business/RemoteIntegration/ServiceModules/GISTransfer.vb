﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects
Namespace RemoteIntegration
    Public Class GISTransfer

        Public Shared Function ProcessShapeData(ByVal db As Database, ByVal request As DataTransferRequest) As StandardResponse
            If request.type = "" Or request.type Is Nothing Then
                request.type = "parcel"
            End If

            If Not {"parcel", "street", "point"}.Contains(request.type) Then
                Return New StandardResponse("500", "request.type is not provided or not having an acceptable value.")
            End If

            request.ExtractData(request.data)
            If request.dataFormat <> Nothing Then
                If request.dataFormat.ToLower = "json" Then

                    Dim recordIndex = 0
                    Dim conflicted As New List(Of Integer)
                    Dim recordTracker As New RecordCountInfo
                    Try
                        Select Case request.type
                            Case "parcel", "point"
                                Dim parcels As GISParcel()
                                parcels = DataTools.JSON.Deserialize(Of GISParcel())(request.extractedData)
                                GISTransferHelper.SaveParcelGISRecords(db, request.type, parcels, recordTracker)
                            Case "street"
                                Dim streets As GISStreet()
                                streets = DataTools.JSON.Deserialize(Of GISStreet())(request.extractedData)
                                GISTransferHelper.SaveStreetGISRecords(db, streets, recordTracker)

                        End Select

                    Catch nex As NullReferenceException
                        Return New StandardResponse(nex, request, 1)
                    Catch ex As Exception
                        'Return New StandardResponse(ex, request)
                        Return New StandardResponse With {.header = New GISResponse With {.ErrorMsg = ex.Message}}
                    End Try

                    Return New StandardResponse With {.header = New GISResponse With
                                                             {
                                                                 .failedCount = recordTracker.recordsIgnored,
                                                                 .invalidParcels = recordTracker.parcelIgnored,
                                                                 .parcelAffected = recordTracker.parcelCreated,
                                                                 .rowsAffected = recordTracker.recordsInserted
                                                            },
                                                           .data = DataTools.JSON.Serialize(Of List(Of Integer))(conflicted)}
                Else
                    Return New StandardResponse(406)
                End If
            Else
                Return New StandardResponse(406)
            End If
        End Function

        Public Shared Function GetParcelFields(db As Database, data As StandardRequest) As StandardResponse
            Dim resp As New StandardResponse
            Dim fieldSql = "SELECT DISTINCT f.Name FROM DataSourceField f INNER JOIN DataSourceTable t ON f.TableId = t.Id WHERE dbo.cc_FindTargetTable(t.Name) = 'ParcelData' AND f.DataType IN (1, 2, 8) ORDER BY 1"
            Dim parcelFields = db.GetDataTable(fieldSql).AsEnumerable.Select(Function(dr) dr.GetString("Name")).ToList
            resp.data = parcelFields
            resp.recordCount = parcelFields.Count
            Return resp
        End Function

        Public Shared Function UpdateStatus(db As Database, ByVal request As DataTransferRequest) As StandardResponse
            Dim resp As New StandardResponse
            Try
                request.ExtractData(request.data)
                If request.dataFormat <> Nothing Then
                    If request.dataFormat.ToLower = "json" Then
                        Dim status As GISTrack
                        status = DataTools.JSON.Deserialize(Of GISTrack)(request.extractedData)
                        If request.type = "init" Then
                            Dim audit As String = "Gisload process started by " + IIf(status.fromUI = "True", "Manual load", "auto schdeular")
                            audit += "; System Name :- " + status.sysName + "; Shp file Location :- " + status.path + "; Ip Address :- " + status.ipAddress + "; Keymaps :- " + status.shapefile + "; Reset ShapeData :- " + status.reset
                            Dim source As String = IIf(ClientSettings.CAMACloudApplicationName = "", "api.camacloud.com", ClientSettings.CAMACloudApplicationName)
                            Dim sql As String = "INSERT INTO SystemAuditTrail (EventType, EventCode, Source, Description, LoginId, IPAddress) VALUES ({0}, {1}, {2}, {3}, {4}, {5});"
                            Dim auditSql As String = sql.SqlFormat(True, "GISTRANSFER", "70000", source, audit, status.sysName, status.ipAddress)
                            db.Execute(auditSql)
                        ElseIf request.type = "commit" Then
                            Dim audit As String = "Gisload process Completed by " + IIf(status.fromUI = "True", "Manual load", "auto schdeular")
                            audit += "; System Name :- " + status.sysName + "; Ip Address :- " + status.ipAddress + "; Reset ShapeData :- " + status.reset
                            Dim source As String = IIf(ClientSettings.CAMACloudApplicationName = "", "api.camacloud.com", ClientSettings.CAMACloudApplicationName)
                            Dim sql As String = "INSERT INTO SystemAuditTrail (EventType, EventCode, Source, Description, LoginId, IPAddress) VALUES ({0}, {1}, {2}, {3}, {4}, {5});"
                            Dim auditSql As String = sql.SqlFormat(True, "GISTRANSFER", "70001", source, audit, status.sysName, status.ipAddress)
                            db.Execute(auditSql)
                        End If
                    Else
                        Return New StandardResponse(406)
                    End If
                Else
                    Return New StandardResponse(406)
                End If
            Catch ex As Exception
                Return New StandardResponse With {.header = New GISResponse With {.ErrorMsg = ex.Message}}
            End Try
            Return resp
        End Function

        Public Shared Function ResetMapData(db As Database, request As DataTransferRequest) As StandardResponse
            Dim resp As New StandardResponse
            Dim recCount As Integer = 0, recCount2 As Integer = 0
            Select Case request.type
                Case "parcel"
                    recCount = db.Execute("TRUNCATE TABLE ParcelMapPoints")
                    resp.AddMessage(recCount & " parcel map points deleted.")
                Case "street"
                    recCount2 = db.Execute("DELETE FROM GIS_StreetPoints")
                    resp.AddMessage(recCount2 & " street map points deleted.")
                    recCount = db.Execute("DELETE FROM GIS_Street")
                    resp.AddMessage(recCount & " streets deleted.")
                Case Else
                    resp = New StandardResponse("400")
                    resp.AddMessage("request.type is required.")
            End Select
            resp.recordCount = recCount
            Return resp
        End Function

    End Class
    Public Class GISTrack
        Public Property path As String
        Public Property ipAddress As String
        Public Property sysName As String
        Public Property shapefile As String
        Public Property reset As String
        Public Property fromUI As String
    End Class
End Namespace

