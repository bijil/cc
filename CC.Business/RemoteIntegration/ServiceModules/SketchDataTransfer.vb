﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects
Imports System.IO

Namespace RemoteIntegration
    Public Class SketchDataTransfer

        Public Shared Function Initialize(ByVal db As Database, ByVal request As SketchTransferRequest) As StandardResponse
            Dim jobId As Integer

            If db.Application.ApplicationStatus = ApplicationStatus.Empty Or db.Application.ApplicationStatus = ApplicationStatus.SchemaCreated Then
                Return New StandardResponse(412, "Database is not ready for sketch transfer yet.")
            End If

            Try
                jobId = TransferJob.Create(db, request, TransferTypes.SketchTransfer)
                request.jobId = jobId

                Dim totalSketches As Integer = SketchDataTransferHelper.InitializeSketchTransferProcess(db, request)

                Return New StandardResponse With {.header = New SketchInitResponse With {.jobId = jobId, .totalSketches = totalSketches}}
            Catch exj As Exceptions.PendingTransferException
                Dim sp = New StandardResponse(500)
                sp.AddMessage(exj.Message)
                sp.AddMessage("Pending job types in response.header")
                sp.AddMessage("Pending job ids in response.data")
                sp.data = exj.PendingJobIds
                sp.header = exj.PendingJobTypeString
                Return sp
            Catch ex As Exception
                Try
                    request.jobId = jobId
                    TransferJob.Cancel(db, request, TransferTypes.SketchTransfer)
                Catch ex2 As Exception

                End Try
                SystemAuditStream.CreateErrorEvent(db, ex)
                Throw ex
            End Try

        End Function

        Public Shared Function ProcessPage(ByVal db As Database, ByVal request As SketchTransferRequest) As StandardResponse
            TransferJob.Verify(db, request, TransferTypes.SketchTransfer)
            Dim lockName As String = DirectCast(request, StandardRequest).accessKey + "~" + DirectCast(request, StandardRequest).passKey
            Dim sp As New StandardResponse
            SyncLock lockName
                Select Case request.direction.Trim.ToLower
                    Case "upsync"
                        Return SketchDataTransferHelper.ProcessSketchUpload(db, request)
                    Case "downsync"
                        Return SketchDataTransferHelper.ProcessSketchDownload(db, request)
                End Select
            End SyncLock
            Return sp
        End Function

        Public Shared Function Commit(ByVal db As Database, ByVal request As SketchTransferRequest) As StandardResponse
            TransferJob.Commit(db, request, TransferTypes.SketchTransfer)
            SketchDataTransferHelper.FinalizeSketchTransferProcess(db, request)
            Return New StandardResponse()
        End Function

        Public Shared Function Cancel(ByVal db As Database, ByVal request As SketchTransferRequest) As StandardResponse
            TransferJob.Cancel(db, request, TransferTypes.SketchTransfer)
            Return New StandardResponse()
        End Function

    End Class
End Namespace
