﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects
Namespace RemoteIntegration

    Public Class SchemaTransfer

        Public Shared Function ResetSchema(ByVal db As Database, ByVal request As SchemaRequest) As StandardResponse

            If db.Application.ApplicationStatus = ApplicationStatus.Empty Or db.Application.ApplicationStatus = ApplicationStatus.SchemaCreated Then
                SchemaTransferHelper.ClearCompleteSchema(db)
                db.Application.ApplicationStatus = ApplicationStatus.Empty
                Dim aet As AuditEventType = Nothing
                aet = SCH_30000_RESET
                SystemAuditStream.CreateStandardEvent(db, aet)
                Return New StandardResponse With {.header = New SchemaVersion With {.version = db.Application.NewSchemaVersion}}
            Else
                Return New StandardResponse(412, "Schema cannot be reset when the application has data loaded into it.")
            End If
        End Function

        Public Shared Function Copy(ByVal db As Database, ByVal request As SchemaRequest) As StandardResponse

            Dim processClock As New ProcessClock()
            processClock.StartClock()
            If Not (db.Application.ApplicationStatus = ApplicationStatus.Empty Or db.Application.ApplicationStatus = ApplicationStatus.SchemaCreated) Then
                Return New StandardResponse(412, "Schema cannot be transferred when the application has data loaded into it.")
            End If
            If request.resetSchema Then
                ResetSchema(db, request)
            End If
            request.ExtractData(request.data)
            If request.dataFormat <> Nothing Then
                If request.dataFormat.ToLower = "json" Then

                    Dim tables As Table()
                    Dim recordTracker As New RecordCountInfo
                    Try
                        tables = DataTools.JSON.Deserialize(Of Table())(request.extractedData)
                        For Each t In tables
                            t.CreateIn(db, recordTracker)
                        Next
                        For Each t In tables
                            If t.relationships IsNot Nothing Then
                                For Each r In t.relationships
                                    r.CreateIn(db, recordTracker)
                                Next
                            End If
                        Next
                        Dim aet As AuditEventType = Nothing
                        aet = SCH_30001_INIT
                        SystemAuditStream.CreateStandardEvent(db, aet)
                    Catch nex As NullReferenceException
                        Return New StandardResponse(nex, request, 1)
                    Catch ex As Exception
                        Return New StandardResponse(ex, request)
                    End Try



                    Dim schema = LoadSchemaFrom(db)
                    processClock.EndClock()
                    Dim schemaString = DataTools.JSON.Serialize(Of Table())(schema.ToArray)
                    Dim sp As New StandardResponse() With {.data = DataTools.DataConversion.EncodeToBase64(schemaString), .recordCount = schema.Count}
                    sp.header = New SchemaProcessInfo With {.version = db.Application.SchemaVersion, .processTime = processClock.processTime, .fieldsAffected = recordTracker.fieldsAffected, .tablesAffected = recordTracker.tablesAffected}
                    Return sp
                Else
                    Return New StandardResponse(406)
                End If
            Else
                Return New StandardResponse(406)
            End If
        End Function

        Public Shared Function Download(ByVal db As Database, ByVal request As SchemaRequest) As StandardResponse
            Try
                Dim aet As AuditEventType = Nothing
                aet = SCH_30003_COPY
                SystemAuditStream.CreateStandardEvent(db, aet)
                Dim schema = LoadSchemaFrom(db)
                Dim schemaString = DataTools.JSON.Serialize(Of Table())(schema.ToArray)
                Dim sp As New StandardResponse() With {.data = DataTools.DataConversion.EncodeToBase64(schemaString), .header = "version=" & db.Application.SchemaVersion, .recordCount = schema.Count}
                Return sp
            Catch ex As Exception
                Return New StandardResponse(ex, request)
            End Try
        End Function

        Public Shared Function DownloadRelationships(ByVal db As Database, ByVal request As SchemaRequest) As StandardResponse
            Dim aet As AuditEventType = Nothing
            aet = SCH_30003_COPY
            SystemAuditStream.CreateStandardEvent(db, aet)
            Dim schema = LoadSchemaFrom(db)
            Dim relationships As New List(Of Relationship)
            For Each t In schema
                For Each r In t.relationships
                    If r.relatedTableId <> 0 Then
                        relationships.Add(r)
                    End If
                Next
            Next
            'Dim relationshipString = DataTools.JSON.Serialize(Of Relationship())(relationships.ToArray)
            Dim sp As New StandardResponse() With {.data = relationships, .header = "version=" & db.Application.SchemaVersion, .recordCount = relationships.Count}
            Return sp
        End Function

        Public Shared Function RelateTables(ByVal db As Database, ByVal request As SchemaRequest) As StandardResponse
            If Not (db.Application.ApplicationStatus = ApplicationStatus.Empty Or db.Application.ApplicationStatus = ApplicationStatus.SchemaCreated) Then
                Return New StandardResponse(412, "Schema relationship cannot be re-built when the application has data loaded into it.")
            End If
            If request.resetRelations Then
                SchemaTransferHelper.ClearRelations(db)
            End If
            If request.dataFormat <> Nothing Then
                If request.dataFormat.ToLower = "json" Then
                    Dim relationships As Relationship()
                    Dim recordTracker As New RecordCountInfo
                    Try
                        relationships = DataTools.JSON.Deserialize(Of Relationship())(request.ExtractData(request.data))

                        For Each r In relationships
                            r.CreateIn(db, recordTracker)
                        Next
                        Dim aet As AuditEventType = Nothing
                        aet = SCH_30002_RELATION
                        SystemAuditStream.CreateStandardEvent(db, aet)
                        'After the relations are created, set the application status as SchemaCreated
                        db.Application.ApplicationStatus = ApplicationStatus.SchemaCreated
                    Catch nex As NullReferenceException
                        Return New StandardResponse(nex, request, 1)
                    Catch ex As Exception
                        Return New StandardResponse(ex, request)
                    End Try

                    Return New StandardResponse() With {.data = ""}
                Else
                    Return New StandardResponse(406)
                End If
            Else
                Return New StandardResponse(406)
            End If
        End Function

        Private Shared Function LoadSchemaFrom(target As Database) As List(Of Table)
            Dim schema As New List(Of Table)
            For Each dr As DataRow In target.GetDataTable("SELECT * FROM DataSourceTable WHERE Name NOT IN ('_neighborhood_profile', '_photo_meta_data', 'DO_NOT_USE') ORDER BY ImportType").Rows
                Dim t As New Table
                t.LoadFromDataRow(target, dr)
                schema.Add(t)
            Next
            Return schema
        End Function

        Public Shared Function UpdateSchema(target As Database, request As SchemaRequest) As StandardResponse
            If (target.Application.ApplicationStatus = ApplicationStatus.Empty) Then
                Return New StandardResponse(412, "No schema has been loaded into this environment yet.")
            End If
            If request.dataFormat <> Nothing Then
                If request.dataFormat.ToLower = "json" Then
                    Dim tables As Table()
                    Dim recordTracker As New RecordCountInfo
                    Try
                        tables = DataTools.JSON.Deserialize(Of Table())(request.ExtractData(request.data))

                        For Each t In tables
                            t.ChangeSchema(target, recordTracker)
                        Next
                        Dim aet As AuditEventType = Nothing
                        aet = SCH_30001_INIT
                        SystemAuditStream.CreateStandardEvent(target, aet)
                    Catch nex As NullReferenceException
                        Return New StandardResponse(nex, request, 1)
                    Catch ex As Exception
                        Return New StandardResponse(ex, request)
                    End Try

                    Return New StandardResponse() With {.data = ""}
                Else
                    Return New StandardResponse(406)
                End If
            Else
                Return New StandardResponse(406)
            End If
        End Function

        Public Shared Function RepairSchemaMissingTables(target As Database, request As SchemaRequest) As StandardResponse
            If (target.Application.ApplicationStatus = ApplicationStatus.Empty) Then
                Return New StandardResponse(412, "No schema has been loaded into this environment yet.")
            End If
            Try
                DataTransferHelper.PrepareTables(target)
                Return New StandardResponse
            Catch ex As Exception
                Return New StandardResponse(ex, request)
            End Try
        End Function
        Public Shared Function UpdateTableProperties(target As Database, request As StandardRequest) As StandardResponse
            If (target.Application.ApplicationStatus = ApplicationStatus.Empty) Then
                Return New StandardResponse(412, "No schema has been loaded into this environment yet.")
            End If
            Try

                If request.dataFormat <> Nothing Then
                    If request.dataFormat.ToLower = "json" Then
                        Dim changes As PropertyValue()
                        changes = DataTools.JSON.Deserialize(Of PropertyValue())(request.ExtractData(request.data))
                        For Each t In changes
                            Table.updateProperty(target, t.TableName, t.PropertyName, t.Value)
                        Next
                        Return New StandardResponse() With {.data = ""}
                    Else
                        Return New StandardResponse(406)
                    End If
                Else
                    Return New StandardResponse(406)
                End If
            Catch ex As Exception
                Return New StandardResponse(ex, request)
            End Try
        End Function
        Public Shared Function UpdateFieldProperties(target As Database, request As StandardRequest) As StandardResponse
            If (target.Application.ApplicationStatus = ApplicationStatus.Empty) Then
                Return New StandardResponse(412, "No schema has been loaded into this environment yet.")
            End If
            Try

                If request.dataFormat <> Nothing Then
                    If request.dataFormat.ToLower = "json" Then
                        Dim changes As PropertyValue()
                        changes = DataTools.JSON.Deserialize(Of PropertyValue())(request.ExtractData(request.data))
                        For Each t In changes
                            Field.updateProperty(target, t.TableName, t.FieldName, t.PropertyName, t.Value)
                        Next
                        Return New StandardResponse() With {.data = ""}
                    Else
                        Return New StandardResponse(406)
                    End If
                Else
                    Return New StandardResponse(406)
                End If
            Catch ex As Exception
                Return New StandardResponse(ex, request)
            End Try
        End Function
    End Class


End Namespace
