﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects
Imports System.IO

Namespace RemoteIntegration

    Public Class ContentTransfer

#Region "Photo Upload"

        Public Shared Function UploadPhotos(ByVal db As Database, ByVal request As PhotoSyncRequest) As StandardResponse

            If request.dataFormat <> Nothing Then
                Dim conflictedPhotos As New List(Of ParcelPhoto)
                Dim uploadedCount As Integer = 0
                Dim rejections As New List(Of String)
                Dim errorCount As Integer = 0
                'Dim dtParcelPhotos As DataTable
                Dim parcelPhotoCount As Integer = 0
                Dim lastErrorMessage As String = ""
                Dim photoList As New List(Of PhotoList)
                Dim isMainImageAdded As Boolean = False
                Dim isPhotoLimitExceeded As Boolean = False
                Dim metaKeys As New Dictionary(Of String, String)
                For i As Integer = 1 To 10
                    Dim fieldName As String = db.GetStringValue("SELECT Name FROM DataSourceField WHERE TableId IN (SELECT Id FROM DataSourceTable WHERE Name = '_photo_meta_data') AND AssignedName = 'PhotoMetaField" & i & "'")
                    If fieldName IsNot Nothing AndAlso fieldName <> String.Empty Then
                        metaKeys.Add(fieldName, "MetaData" & i)
                    End If
                Next
                Dim photoLimitValue As Integer = db.GetIntegerValue("SELECT Value FROM ClientSettings WHERE Name = 'MaxPhotosPerParcel'")
                If (photoLimitValue = 0) Then
                    photoLimitValue = 100
                End If

                Try
                    SystemAuditStream.CreateStandardEvent(db, CNT_80000_START)
                    Dim s3 As New S3FileManager

                    '  ParcelPhoto = DataTools.JSON.Deserialize(Of List(Of ParcelPhoto))(request.photos)
                    For Each t In request.photos
                        Try
                            'dtParcelPhotos = db.GetDataTable("SELECT COUNT(*) FROM ParcelImages WHERE ParcelId = " & t.parcel & " UNION SELECT COUNT(*) FROM ParcelImages WHERE ParcelId = " & t.parcel & " IsPrimary = 1")
                            'parcelPhotoCount = Convert.ToInt32(dtParcelPhotos.Rows(1)(0).ToString())
                            'isMainImageAdded = IIf(Convert.ToInt32(dtParcelPhotos.Rows(0)(0).ToString()) = 0, False, True)
                            'If parcelPhotoCount >= photoLimitValue Then
                            '    isPhotoLimitExceeded = True
                            'End If
                            If (PhotoTransferHelper.UploadPhoto(db, request, t, s3, metaKeys, photoList, photoLimitValue)) Then
                                uploadedCount += 1
                            Else
                                rejections.Add(t.fileName)
                            End If
                        Catch uex As Exception
                            t.failed = True
                            lastErrorMessage = uex.Message
                            rejections.Add(t.fileName)
                        End Try
                    Next

                    For Each t In request.photos.Where(Function(x) x.failed)
                        Try
                            'dtParcelPhotos = db.GetDataTable("SELECT COUNT(*) FROM ParcelImages WHERE ParcelId = " & t.parcel & " UNION SELECT COUNT(*) FROM ParcelImages WHERE ParcelId = " & t.parcel & " AND IsPrimary = 1")
                            'parcelPhotoCount = Convert.ToInt32(dtParcelPhotos.Rows(1)(0).ToString())
                            'isMainImageAdded = IIf(Convert.ToInt32(dtParcelPhotos.Rows(0)(0).ToString()) = 0, False, True)
                            'If parcelPhotoCount >= photoLimitValue Then
                            '    isPhotoLimitExceeded = True
                            'End If
                            If (PhotoTransferHelper.UploadPhoto(db, request, t, s3, metaKeys, photoList, photoLimitValue)) Then
                                rejections.Remove(t.fileName)
                                uploadedCount += 1
                            End If
                        Catch uex As Exception
                            errorCount += 1
                            lastErrorMessage = uex.Message
                            Dim errorResp As New StandardResponse(uex, request)
                        End Try
                    Next
                Catch nex As NullReferenceException
                    Return New StandardResponse(nex, request, 1)
                Catch ex As Exception
                    Return New StandardResponse(ex, request)
                End Try


                Dim rejectionString = DataTools.JSON.Serialize(Of List(Of String))(rejections)
                Dim sp As New StandardResponse() With {.recordCount = request.photos.Count, .data = rejectionString, .header = New PhotoUploadStatus With {.photosUploaded = uploadedCount, .uploadPhotoList = photoList}}

                Dim totalPhotos As Integer = request.photos.Count
                Dim missedPhotos As Integer = totalPhotos - uploadedCount
                Dim rejected As Integer = rejections.Count - errorCount

                If errorCount > 0 Then
                    sp.AddMessage(errorCount & " photos failed due to errors while transporting to cloud storage.")
                    sp.AddMessage("Last error message: " + lastErrorMessage)
                End If

                If rejected > 0 Then
                    sp.AddMessage(rejected & " photos are rejected at the server due to missing parcels or other reasons.")
                    If errorCount = 0 Then
                        sp.AddMessage("Last error message: " + lastErrorMessage)
                    End If
                End If
                SystemAuditStream.CreateStandardEvent(db, CNT_80001_END)
                Return sp

            Else
                Return New StandardResponse(406)
            End If
        End Function

#End Region

#Region "Photo DownSync - Paged"

        Public Shared Function PhotoDownSync_Init(ByVal target As Database, ByVal request As PhotoSyncRequest) As StandardResponse
            Dim type = TransferTypes.PhotoTransfer
            Dim photoResponse As New PhotoDownloadResponse
            Dim jobId As Integer

            If request.dataFormat <> Nothing Then
                If request.dataFormat.ToLower = "json" Then

                    Try
                        If request.pageSize = 0 Then
                            Throw New Exception("Page size is a required parameter to process the request.")
                        End If

                        jobId = TransferJob.Create(target, request, type)
                        photoResponse = PhotoTransferHelper.PhotoDownSync_Init(target, jobId, request)
                    Catch exj As Exceptions.PendingTransferException
                        Dim sp = New StandardResponse(500)
                        sp.AddMessage(exj.Message)
                        sp.AddMessage("Pending job types in response.header")
                        sp.AddMessage("Pending job ids in response.data")
                        sp.data = exj.PendingJobIds
                        sp.header = exj.PendingJobTypeString
                        Return sp
                    Catch nex As NullReferenceException
                        Return New StandardResponse(nex, request, 1)
                    Catch ex As Exception
                        'ErrorMailer.ReportException("API", ex, System.Web.HttpContext.Current.Request)
                        Return New StandardResponse(400, ex.Message)
                    End Try
                    SystemAuditStream.CreateStandardEvent(target, CNT_80004_INIT, jobId)
                    Return New StandardResponse With {.header = photoResponse}


                Else
                    Return New StandardResponse(406)
                End If
            Else
                Return New StandardResponse(406)
            End If
        End Function

        Public Shared Function PhotoDownSync_ProcessData(ByVal target As Database, ByVal request As PhotoSyncRequest) As StandardResponse
            TransferJob.Verify(target, request, TransferTypes.PhotoTransfer)
            request.pageSize = target.GetIntegerValue("SELECT PageSize FROM TransferJob WHERE Id = " & request.jobId)
            Dim parcelphotoList As List(Of ParcelPhoto)
            If request.dataFormat <> Nothing Then
                If request.dataFormat.ToLower = "json" Then
                    Try
                        parcelphotoList = PhotoTransferHelper.PhotoDownSync_Data(target, request, request.pageIndex, request.pageSize, request.jobId)
                    Catch nex As NullReferenceException
                        Return New StandardResponse(nex, request, 1)
                    Catch ex As Exception
                        Return New StandardResponse(400, ex.Message)
                    End Try
                    Dim photoString = DataTools.JSON.Serialize(Of ParcelPhoto())(parcelphotoList.ToArray())

                    TransferJob.UpdateCurrentPage(target, request.pageIndex, request.jobId)
                    SystemAuditStream.CreateStandardEvent(target, CNT_80005_DATA, request.jobId, request.pageIndex)
                    Return New StandardResponse With {.data = photoString, .recordCount = parcelphotoList.Count}
                Else
                    Return New StandardResponse(406)
                End If
            Else
                Return New StandardResponse(406)
            End If
        End Function

        Public Shared Function PhotoDownSync_Commit(ByVal type As BulkDataTransferType, ByVal target As Database, ByVal request As PhotoSyncRequest) As StandardResponse
            Dim parcelCount = 0, totalPhotos = 0
            Try
                TransferJob.Verify(target, request, TransferTypes.PhotoTransfer)
                PhotoTransferHelper.PhotoDownSync_Commit(target, parcelCount, totalPhotos, request)
                TransferJob.Commit(target, request, TransferTypes.PhotoTransfer)
                SystemAuditStream.CreateStandardEvent(target, CNT_80006_COMMIT, request.jobId, totalPhotos)

            Catch nex As NullReferenceException
                Return New StandardResponse(nex, request, 1)
            Catch ex As Exception
                Return New StandardResponse(ex, request)
            End Try
            Return New StandardResponse 'With {.header = New PhotoDownloadResponse With {.totalPhotos = totalPhotos, .totalParcels = parcelCount, .pageCount = request.pageIndex, .jobId = request.jobId}}
        End Function

        Public Shared Function PhotoDownSync_CommitPage(bulkDataTransferType As BulkDataTransferType, target As Database, request As PhotoSyncRequest) As StandardResponse
            TransferJob.Verify(target, request, TransferTypes.PhotoTransfer)
            Dim committedPhotos As Integer = 0
            If request.imageIds.Count > 0 Then
                Dim imageIds As String = String.Join(",", request.imageIds.Select(Function(x) x.ToString))
                Dim sql As String = "UPDATE ParcelImages SET DownSynced = 1,LocalId=NULL, DownSyncedDate = GETUTCDATE() WHERE Id IN (" + imageIds + ")"
                committedPhotos = target.Execute(sql)
                If ClientSettings.PropertyValue("PhotoDeleteAndRecover") = "1" Then
                    Dim dt As DataTable = target.GetDataTable("SELECT Id, Path FROM ParcelImages WHERE Id IN (" + imageIds + ") AND CC_Deleted = 1")
                    If dt.Rows.Count > 0 Then
                        Dim s3 As New S3FileManager()
                        Dim errorList As String = s3.DeleteMultipleFile(dt)
                        target.Execute("DELETE FROM ParcelImages WHERE Id IN (" + imageIds + ") AND CC_Deleted = 1")
                        If errorList <> "" Then
                            target.Execute("INSERT INTO AdditionalLogs(ParcelId, LoginId, Description) VALUES({0}, {1}, {2})".SqlFormatString(1, "PhotoCommit", "Error occured when deleting photos" + errorList + " from S3"))
                        End If
                    End If
                End If
            End If
            Return New StandardResponse With {.header = committedPhotos & " photos committed.", .recordCount = committedPhotos}
        End Function

        Public Shared Function PhotoDownSync_Reject(bulkDataTransferType As BulkDataTransferType, target As Database, request As PhotoSyncRequest) As StandardResponse
            'TransferJob.Verify(target, request, TransferTypes.PhotoTransfer)
            Dim imageIds As String = String.Join(",", request.imageIds.Select(Function(x) x.ToString))
            Dim sql As String = "UPDATE ParcelImages SET DownSynced = 0, DownSyncedDate = NULL WHERE Id IN (" + imageIds + ");"
            sql += "UPDATE Parcel SET FailedOnDownSync = 1, FailedOnDownSyncStatus = 2 WHERE Id IN (SELECT DISTINCT ParcelId FROM ParcelImages WHERE Id IN  (" + imageIds + "));"
            Dim rejectedPhotos As Integer = target.Execute(sql)
            target.Execute(("INSERT INTO ParcelDownsyncErrorLog(ParcelId,ErrorData,ErrorType) (SELECT DISTINCT ParcelId, {0}, {1} FROM ParcelImages WHERE Id IN (" & imageIds & "))").SqlFormatString("Parcel DownSync has failed due to errors.", 0))
            target.Execute("INSERT INTO ParcelAuditTrail (EventDate, EventTime, ParcelId, LoginID, EventType, Description,ApplicationType) SELECT GETUTCDATE(), GETUTCDATE(), ParcelId, NULL, 3, CAST(COUNT(*) AS VARCHAR(6)) + ' photo(s) rejected after download('+STUFF((SELECT ', ' + '#'+CONVERT(VARCHAR(12),Id) FROM ParcelImages t2 WHERE t2.ParcelId = pi.ParcelId AND t2.Id IN (" & imageIds & ") ORDER BY ParcelId FOR XML PATH('')), 1, 2, '') +').' As PhotoCount,'API'  FROM ParcelImages pi  WHERE Id IN (" & imageIds & ") GROUP BY ParcelId")
            Return New StandardResponse With {.header = rejectedPhotos & " photos rejected.", .recordCount = rejectedPhotos}
        End Function

        Public Shared Function PhotoDownSync_Cancel(ByVal type As BulkDataTransferType, ByVal db As Database, ByVal request As PhotoSyncRequest) As StandardResponse
            TransferJob.Verify(db, request, TransferTypes.PhotoTransfer)
            TransferJob.Cancel(db, request, TransferTypes.PhotoTransfer)
            SystemAuditStream.CreateStandardEvent(db, CNT_80007_CANCEL, request.jobId)
            Return New StandardResponse()
        End Function

#End Region

#Region "Photo DownSync - Pageless"

        Public Shared Function DownSync(ByVal target As Data.Database, ByVal request As PhotoSyncRequest) As StandardResponse

            If request.dataFormat <> Nothing Then
                If request.dataFormat.ToLower = "json" Then
                    Dim parcelphotoList As List(Of ParcelPhoto)
                    Dim sp As New StandardResponse
                    Try
                        SystemAuditStream.CreateStandardEvent(target, CNT_80002_START)
                        parcelphotoList = PhotoTransferHelper.DownloadPhoto(target, request.startTime, request.endTime)
                        SystemAuditStream.CreateStandardEvent(target, CNT_80003_END)
                    Catch nex As NullReferenceException
                        Return New StandardResponse(nex, request, 1)
                    Catch ex As Exception
                        'ErrorMailer.ReportException("API", ex, System.Web.HttpContext.Current.Request)
                        Return New StandardResponse(400, ex.Message)
                    End Try

                    Dim photoString = DataTools.JSON.Serialize(Of ParcelPhoto())(parcelphotoList.ToArray())
                    Return New StandardResponse With {.data = photoString, .recordCount = parcelphotoList.Count}

                Else
                    Return New StandardResponse(406)
                End If
            Else
                Return New StandardResponse(406)
            End If

        End Function

#End Region

#Region "PhotoMap - Download & Upload"

        Public Shared Function PhotoMap_Download(ByVal target As Data.Database, ByVal request As PhotoSyncRequest) As StandardResponse

            Dim recordTracker As New RecordCountInfo
            Dim syncedPhotoList As New List(Of PhotoMap)
            If request.dataFormat <> Nothing Then
                If request.dataFormat.ToLower = "json" Then
                    Dim sp As New StandardResponse
                    Try
                        syncedPhotoList = PhotoStorageHelper.DownSyncPhotoMap(target, request.photos, recordTracker)

                    Catch nex As NullReferenceException
                        Return New StandardResponse(nex, request, 1)
                    Catch ex As Exception
                        'ErrorMailer.ReportException("API", ex, System.Web.HttpContext.Current.Request)
                        Return New StandardResponse(400, ex.Message)
                    End Try

                    Dim photoMapString = DataTools.JSON.Serialize(Of List(Of PhotoMap))(syncedPhotoList)
                    Return New StandardResponse With {.data = DataTools.DataConversion.EncodeToBase64(photoMapString), .recordCount = syncedPhotoList.Count}

                Else
                    Return New StandardResponse(406)
                End If
            Else
                Return New StandardResponse(406)
            End If
        End Function

        Public Shared Function PhotoMap_Upload(ByVal target As Data.Database, ByVal request As PhotoSyncRequest) As StandardResponse

            Dim recordTracker As New RecordCountInfo
            Dim conflictList As New List(Of Integer)

            If request.dataFormat <> Nothing Then
                If request.dataFormat.ToLower = "json" Then
                    Dim sp As New StandardResponse
                    Try
                        conflictList = PhotoStorageHelper.UpSyncPhotoMap(target, request.photos, recordTracker)

                    Catch nex As NullReferenceException
                        Return New StandardResponse(nex, request, 1)
                    Catch ex As Exception
                        'ErrorMailer.ReportException("API", ex, System.Web.HttpContext.Current.Request)
                        Return New StandardResponse(400, ex.Message)
                    End Try

                    Dim conflictString = DataTools.JSON.Serialize(Of List(Of Integer))(conflictList)
                    Return New StandardResponse With {.data = conflictString, .header = New PhotoUploadStatus With {.photosUploaded = recordTracker.recordsInserted}}

                Else
                    Return New StandardResponse(406)
                End If
            Else
                Return New StandardResponse(406)
            End If
        End Function
#End Region

#Region "Photo - Flags & Metadata"

        Public Shared Function SetPhotoFlag(ByVal target As Data.Database, ByVal request As PhotoSyncRequest) As StandardResponse

            Dim recordTracker As New RecordCountInfo
            Dim conflictList As New List(Of String)

            If request.dataFormat <> Nothing Then
                If request.dataFormat.ToLower = "json" Then
                    Dim sp As New StandardResponse
                    Try
                        Dim data = DataTools.JSON.Deserialize(Of List(Of PhotoFlag))(request.ExtractData(request.data))
                        conflictList = PhotoStorageHelper.SetMainFlag(target, data)

                    Catch nex As NullReferenceException
                        Return New StandardResponse(nex, request, 1)
                    Catch ex As Exception
                        Return New StandardResponse(400, ex.Message)
                    End Try

                    Dim conflictString = DataTools.JSON.Serialize(Of List(Of String))(conflictList)
                    Return New StandardResponse With {.data = conflictString}

                Else
                    Return New StandardResponse(406)
                End If
            Else
                Return New StandardResponse(406)
            End If
        End Function

        Public Shared Function UpdatePhotoClientReferenceId(ByVal target As Data.Database, ByVal request As PhotoSyncRequest) As StandardResponse

            Dim recordTracker As New RecordCountInfo
            Dim conflictList As New List(Of String)

            If request.dataFormat <> Nothing Then
                If request.dataFormat.ToLower = "json" Then
                    Dim sp As New StandardResponse
                    Try
                        Dim data = DataTools.JSON.Deserialize(Of List(Of PhotoFlag))(request.ExtractData(request.data))
                        PhotoStorageHelper.UpdateClientImageId(target, data, recordTracker)

                    Catch nex As NullReferenceException
                        Return New StandardResponse(nex, request, 1)
                    Catch ex As Exception
                        Return New StandardResponse(400, ex.Message)
                    End Try
                    conflictList.Add(recordTracker.conflictCount)
                    Dim conflictString = DataTools.JSON.Serialize(Of List(Of String))(conflictList)
                    Return New StandardResponse With {.data = conflictString}


                Else
                    Return New StandardResponse(406)
                End If
            Else
                Return New StandardResponse(406)
            End If
        End Function
#End Region

#Region "Photo Store - Backup & Restore"

        Public Shared Function BackupPhotoIndex(db As Database, request As PhotoSyncRequest) As StandardResponse
            Try
                Dim backupCount As Integer = PhotoStorageHelper.BackupPhotoIndex(db)
                Dim sr As New StandardResponse
                sr.recordCount = backupCount
                sr.AddMessage(backupCount & " photos backed up.")
                Return sr
            Catch ex As Exception
                Return New StandardResponse(ex, request)
            End Try
        End Function

        Public Shared Function RestorePhotoIndex(db As Database, request As PhotoSyncRequest) As StandardResponse
            Try
                Dim restoreCount As Integer = PhotoStorageHelper.RestorePhotoIndex(db)
                Dim sr As New StandardResponse
                sr.recordCount = restoreCount
                sr.AddMessage(restoreCount & " photos restored.")
                Return sr
            Catch ex As Exception
                Return New StandardResponse(ex, request)
            End Try
        End Function

        Public Shared Function ListEmptyParcels(db As Database, request As PhotoSyncRequest) As StandardResponse
            Dim pids = PhotoStorageHelper.GetParcelsWithoutPhotos(db)
            Dim sp As New StandardResponse
            sp.data = DataTools.JSON.Serialize(Of List(Of Integer))(pids)
            sp.recordCount = pids.Count
            Return sp
        End Function

#End Region

        Public Shared Function DeletePhotos(db As Database, request As PhotoSyncRequest) As StandardResponse
            If request.imageIds IsNot Nothing AndAlso request.imageIds.Count > 0 Then
            ElseIf request.photos IsNot Nothing AndAlso request.photos.Count > 0 Then
                For Each photo In request.photos

                Next

            End If
        End Function



    End Class

End Namespace
