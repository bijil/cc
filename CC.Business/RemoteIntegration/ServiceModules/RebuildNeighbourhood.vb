﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects
Namespace RemoteIntegration
    Public Class RebuildNeighbourhood

        Public Shared Function BuildNeighbourhood(ByVal db As Database, ByVal request As NeighbourhoodRequest) As StandardResponse
            Try
                Dim status = Neighborhood.RebuildNeighborhoods(db, request.relinkOnly)
                Return New StandardResponse With {.header = status}
            Catch ex As Exception
                Return New StandardResponse("500", ex.Message)
            End Try
        End Function
    End Class
End Namespace
