﻿Imports System.Runtime.Serialization

Namespace RemoteIntegration.DataContracts

    <DataContract(Namespace:="")> _
   Public Class AccessResponse
        <DataMember()>
        Public Property users As UserInfo()
    End Class

End Namespace
