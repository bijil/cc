﻿Imports System.Runtime.Serialization

<DataContract(Namespace:="", Name:="PhotoDownloadResponse")> _
Public Class PhotoDownloadResponse
    <DataMember(Order:=1)>
        Public Property jobId As Integer
    <DataMember(Order:=2)>
    Public Property pageCount As Integer
    <DataMember(order:=3)>
    Public Property totalParcels As Integer
    <DataMember(order:=4)>
    Public Property totalPhotos As Integer
End Class
