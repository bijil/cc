﻿Imports System.Runtime.Serialization
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects

Namespace RemoteIntegration.DataContracts
    <DataContract(Namespace:="", Name:="AuditTrail")> _
    Public Class AuditTrailResponse
        <DataMember()> _
        Public Property auditInfo As AuditTrail()
    End Class
End Namespace
