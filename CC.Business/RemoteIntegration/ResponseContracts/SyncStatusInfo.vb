﻿Imports System.Runtime.Serialization

Namespace RemoteIntegration.DataContracts

    <DataContract(Namespace:="")> _
    Public Class SyncStatusInfo
        <DataMember()> Public Property qcMinDate As Date
        <DataMember()> Public Property qcMaxDate As Date
        <DataMember()> Public Property parcelCount As Integer
        <DataMember()> Public Property changeCount As Integer

    End Class

End Namespace