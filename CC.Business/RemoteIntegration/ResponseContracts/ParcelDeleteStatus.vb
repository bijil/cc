﻿Imports System.Runtime.Serialization
Namespace RemoteIntegration.DataContracts
    <DataContract(Namespace:="", Name:="ParcelDeleteStatus")> _
    Public Class ParcelDeleteStatus
        <DataMember()>
        Public Property parcelsAffected As Integer
        Public Property recordcount As Integer
    End Class
End Namespace

