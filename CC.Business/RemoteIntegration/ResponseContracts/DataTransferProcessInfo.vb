﻿Imports System.Runtime.Serialization
Namespace RemoteIntegration.DataContracts

    <DataContract(Namespace:="")> _
    Public Class DataTransferProcessInfo
        Private _rowsAffected As Integer

        <DataMember()> Public Property tablesAffected As Integer
        <DataMember()> Public Property recordsInserted As Integer
        <DataMember()> Public Property recordsUpdated As Integer
        <DataMember()> Public Property parcelCreated As Integer
        <DataMember()> Public Property parcelModified As Integer
        <DataMember()>
        Public Property rowsAffected As Integer
            Get
                Return _rowsAffected
            End Get
            Set(ByVal value As Integer)
                _rowsAffected = (recordsInserted + recordsUpdated + parcelCreated + parcelModified)
            End Set
        End Property
    End Class

End Namespace
