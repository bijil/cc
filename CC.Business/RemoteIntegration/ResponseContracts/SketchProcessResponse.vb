﻿Imports System.Runtime.Serialization
Namespace RemoteIntegration.DataContracts
    <DataContract(Namespace:="", Name:="SketchProcessResponse")> _
    Public Class SketchProcessResponse
        <DataMember()> Public Property sketchFormat As String
        <DataMember()> Public Property parcelCount As Integer
        <DataMember()> Public Property sketchCount As Integer
        <DataMember()> Public Property failedSketches As Integer
    End Class
End Namespace
