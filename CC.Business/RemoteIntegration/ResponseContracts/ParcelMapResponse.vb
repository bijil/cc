﻿Imports System.Runtime.Serialization
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects

Namespace RemoteIntegration.DataContracts

    <DataContract(Namespace:="")> Public Class ParcelMapResponse
        <DataMember()> Public parcels As ParcelMapInfo()
    End Class

End Namespace

