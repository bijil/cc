﻿Imports System.Runtime.Serialization
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects

Namespace RemoteIntegration.DataContracts
    <DataContract(Namespace:="", Name:="ChangesDownload")> _
    Public Class UpdateTransferDownSyncResponse
        <DataMember(Order:=2)> _
        Public Property periodStart As String
        <DataMember(Order:=3)> _
        Public Property periodEnd As String
        <DataMember(Order:=1)> _
        Public Property processTime As Long
        <DataMember(Order:=4)> _
        Public Property nextQCStartDate As DateTime = DateTime.MinValue
    End Class
End Namespace

