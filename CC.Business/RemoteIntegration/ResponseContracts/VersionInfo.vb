﻿Imports System.Runtime.Serialization

Namespace RemoteIntegration.DataContracts

    <DataContract(Namespace:="")> _
    Public Class VersionInfo

        <DataMember()> Public Property version As String
        <DataMember()> Public Property application As String

        Public Sub New()

        End Sub

    End Class

End Namespace
