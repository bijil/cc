﻿Imports CAMACloud.RemoteIntegration
Imports System.Runtime.Serialization


Namespace RemoteIntegration.Objects
    <DataContract(NameSpace:="", Name:="PhotoUpload")> Public Class PhotoUploadStatus

        <DataMember()> Public Property photosUploaded As Integer
        <DataMember()> Public Property uploadPhotoList As List(Of PhotoList)

    End Class
End Namespace