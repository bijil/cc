﻿Imports System.Runtime.Serialization
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects

Namespace RemoteIntegration.DataContracts

    <DataContract(Namespace:="")> _
    Public Class PhotoSyncResponse
        <DataMember()> Public Property photos As ParcelPhoto()
    End Class

End Namespace

