﻿Imports System.Runtime.Serialization

<DataContract(Namespace:="", Name:="RejectResponse")> _
  Public Class ParcelRejectResponse
    <DataMember()>
    Public Property parcelsAffected As Integer
    <DataMember()>
    Public Property failedCount As Integer

End Class
