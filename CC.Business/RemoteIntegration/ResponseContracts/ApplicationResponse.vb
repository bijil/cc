﻿Imports System.Runtime.Serialization

Namespace RemoteIntegration.DataContracts
    <DataContract(Namespace:="", Name:="ApplicationConfiguration")> _
    Public Class ApplicationResponse
        <DataMember(Order:=1)> _
        Public Property parcelTable As String
        <DataMember(Order:=2)> _
        Public Property neighborhoodTable As String
        <DataMember(Order:=3)> _
        Public Property neighborhoodField As String
        <DataMember(Order:=4)> _
        Public Property neighborhoodNameField As String
        <DataMember(Order:=5)> _
        Public Property neighborhoodNumberField As String
        <DataMember(Order:=6)> _
        Public Property streetAddressTable As String
        <DataMember(Order:=7)> _
        Public Property streetAddressField As String
        <DataMember(Order:=8)> _
        Public Property streetAddressFilter As String
        <DataMember(Order:=9)> _
        Public Property StreetNumberField As String
        <DataMember(Order:=10)> _
        Public Property StreetNameField As String
        <DataMember(Order:=11)> _
        Public Property StreetNameSuffixField As String
        <DataMember(Order:=12)> _
        Public Property keyField1 As String
        <DataMember(Order:=13)> _
        Public Property keyField2 As String
        <DataMember(Order:=14)> _
        Public Property keyField3 As String
        <DataMember(Order:=15)> _
        Public Property keyField4 As String
        <DataMember(Order:=16)> _
        Public Property keyField5 As String
        <DataMember(Order:=17)> _
        Public Property keyField6 As String
        <DataMember(Order:=18)> _
        Public Property keyField7 As String
        <DataMember(Order:=19)> _
        Public Property keyField8 As String
        <DataMember(Order:=20)> _
        Public Property keyField9 As String
        <DataMember(Order:=21)> _
        Public Property keyField10 As String
        <DataMember(Order:=22)> _
        Public Property streetAddressFilterValue As String
        <DataMember()>
        Public Property cityField As String

    End Class

End Namespace
