﻿Imports System.Runtime.Serialization
Namespace RemoteIntegration.DataContracts

    <DataContract(Namespace:="")> _
    Public Class SchemaVersion
        <DataMember()> Public Property version As String
    End Class

End Namespace
