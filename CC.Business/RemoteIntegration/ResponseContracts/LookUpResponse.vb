﻿Imports System.Runtime.Serialization

Namespace RemoteIntegration.DataContracts
    <DataContract(Namespace:="", Name:="Lookup")> _
    Public Class LookupProcessInfo
        <DataMember()>
        Public Property lookupsAffected As Integer
        <DataMember()>
        Public Property valuesInserted As Integer
        <DataMember()>
        Public Property failedCount As Integer
    End Class

End Namespace