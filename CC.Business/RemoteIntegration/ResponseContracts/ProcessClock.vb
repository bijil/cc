﻿Imports System.Runtime.Serialization

Namespace RemoteIntegration.DataContracts


    Public Class ProcessClock
        Private _start As Long
        Private _end As Long

        Public Property processTime As Long
        Public Property StartTime As Long
            Get
                Return _start
            End Get
            Set(ByVal value As Long)
                _start = value
            End Set
        End Property
        Public Property EndTime As Long
            Get
                Return _end
            End Get
            Set(ByVal value As Long)
                _end = value
            End Set
        End Property
        Public Sub StartClock()
            _start = DateTime.Now.Ticks
        End Sub
        Public Sub EndClock()
            _end = DateTime.Now.Ticks
            processTime = (_end - _start)
        End Sub
    End Class
End Namespace
