﻿Imports System.Runtime.Serialization

<DataContract(Namespace:="", Name:="RollBackResponse")> _
Public Class ParcelRollBackResponse
    <DataMember()>
    Public Property parcelsAffected As Integer
    <DataMember()>
    Public Property failedCount As Integer

End Class
