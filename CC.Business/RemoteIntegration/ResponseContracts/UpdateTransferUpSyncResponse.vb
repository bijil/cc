﻿
Imports System.Runtime.Serialization
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects

Namespace RemoteIntegration.DataContracts

    <DataContract(Namespace:="", Name:="UpdateTransferStatus")> _
    Public Class UpdateTransferUpSyncResponse
        <DataMember()> _
        Public Property tablesAffected As Integer
        <DataMember()> _
        Public Property recordsAffected As Integer
        <DataMember()> _
        Public Property failedCount As Integer
        <DataMember()> _
        Public Property processTime As Long

    End Class

End Namespace
