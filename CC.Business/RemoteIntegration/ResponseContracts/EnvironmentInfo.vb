﻿Imports System.Runtime.Serialization

Namespace RemoteIntegration.DataContracts

    <DataContract(Namespace:="")> _
    Public Class EnvironmentInfo

        <DataMember()> Public Property environment As String
        <DataMember()> Public Property parcels As Integer
        <DataMember()> Public Property assignmentGroups As Integer
        <DataMember()> Public Property unassignedParcels As Integer
        <DataMember()> Public Property dbName As String
        <DataMember()> Public Property totalPhotos As String

        Public Sub New()

        End Sub

    End Class

End Namespace
