﻿Namespace RemoteIntegration.DataContracts
    Public Class RecordCountInfo
        Public Property fieldsAffected As Integer
        Public Property tablesAffected As Integer

        Public Property conflictCount As Integer
        Public Property parcelModified As Integer
        Public Property parcelDeleted As Integer
        Public Property abort As Boolean
        Public Property parcelCreated As Integer
        Public Property parcelIgnored As String
        Public Property recordsIgnored As Integer
        Public Property recordsInserted As Integer
        Public Property recordsUpdated As Integer
        Public Property lookUpAffected As Integer


        Public ReadOnly Property rowsAffected As Integer
            Get
                Return recordsInserted + recordsUpdated
            End Get
        End Property

        Public ReadOnly Property parcelsAffected As Integer
            Get
                Return parcelCreated + parcelModified
            End Get
        End Property


    End Class
End Namespace
