﻿Imports System.Runtime.Serialization
Namespace RemoteIntegration.DataContracts
    <DataContract(Namespace:="", Name:="SketchInitResponse")>
    Public Class SketchInitResponse
        <DataMember()> Public Property jobId As Integer
        <DataMember()> Public Property totalSketches As Integer
    End Class
End Namespace