﻿Imports System.Runtime.Serialization
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects

Namespace RemoteIntegration.DataContracts
    <DataContract(Namespace:="")> _
    Public Class UpdateTransferResponse
        <DataMember()> _
        Public Property changes As ParcelChange()
    End Class
End Namespace

