﻿Imports System.Runtime.Serialization

Namespace RemoteIntegration.DataContracts
    <DataContract(Namespace:="")> _
    Public Class GISResponse
        <DataMember()>
        Public Property parcelAffected As Integer
        <DataMember()>
        Public Property rowsAffected As Integer
        <DataMember()>
        Public Property failedCount As Integer

        <DataMember()>
        Public Property ErrorMsg As String

        <DataMember()>
        Public Property invalidParcels As String

    End Class

End Namespace
