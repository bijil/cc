﻿Imports System.Runtime.Serialization

Namespace RemoteIntegration.DataContracts

    <DataContract(Namespace:="")> _
    Public Class SchemaProcessInfo
        <DataMember()> Public Property version As String
        <DataMember()> Public Property processTime As Long
        <DataMember()> Public Property fieldsAffected As Integer
        <DataMember()> Public Property duplicates As Integer
        <DataMember()> Public Property tablesAffected As Integer
    End Class

End Namespace
