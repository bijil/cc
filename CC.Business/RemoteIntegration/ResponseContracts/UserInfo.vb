﻿Imports System.Runtime.Serialization
Imports System.Text.RegularExpressions

Namespace RemoteIntegration.DataContracts
    <DataContract()> Public Class UserInfo

        <DataMember(Order:=1)> Public Property username As String
        <DataMember(Order:=2)> Public Property firstName As String
        <DataMember(Order:=3)> Public Property lastName As String
        <DataMember(Order:=4)> Public Property email As String
        <DataMember(Order:=5)> Public Property roles As New List(Of String)
        <DataMember(Order:=6)> Public Property isLockedOut As Boolean
        <DataMember(Order:=7)> Public Property createdDate As String
        <DataMember(Order:=8)> Public Property lastLoginDate As String

        'Public Sub New()
        '    roles = New List(Of String)
        'End Sub

    End Class
End Namespace
