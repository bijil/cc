﻿Imports System.Runtime.Serialization

Namespace RemoteIntegration.DataContracts
    Public Class CommittedParcel
        Public Property parcelId As String
        Public Property recordsAffected As String
    End Class
End Namespace
