﻿Imports System.ServiceModel
Imports System.Runtime.Serialization
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects

Namespace RemoteIntegration.DataContracts

    <KnownType(GetType(SyncStatusInfo))>
    <KnownType(GetType(PhotoDownloadResponse))>
    <KnownType(GetType(ParcelDeleteStatus))>
    <KnownType(GetType(ParcelRejectResponse))>
    <KnownType(GetType(ParcelRollBackResponse))>
    <KnownType(GetType(ParcelChange))>
    <KnownType(GetType(UpdateTransferUpSyncResponse))>
    <KnownType(GetType(UpdateTransferDownSyncResponse))>
    <KnownType(GetType(PhotoSyncResponse))>
    <KnownType(GetType(AuditTrailResponse))>
    <KnownType(GetType(ParcelMapResponse))>
    <KnownType(GetType(PhotoUploadStatus))>
    <KnownType(GetType(GISResponse))>
    <KnownType(GetType(LookupProcessInfo))>
    <KnownType(GetType(EnvironmentInfo))>
    <KnownType(GetType(VersionInfo))>
    <KnownType(GetType(AccessResponse))>
    <KnownType(GetType(ApplicationResponse))>
    <KnownType(GetType(SchemaVersion))>
    <KnownType(GetType(SchemaProcessInfo))>
    <KnownType(GetType(DataTransferProcessInfo))>
    <KnownType(GetType(SketchInitResponse))>
    <KnownType(GetType(SketchProcessResponse))>
    <KnownType(GetType(List(Of SketchObject)))>
    <KnownType(GetType(List(Of String)))>
    <KnownType(GetType(List(Of KeyValuePair(Of String, String))))>
    <KnownType(GetType(NameValueCollection))>
    <KnownType(GetType(UserInfo))>
    <KnownType(GetType(JobInfo))>
    <KnownType(GetType(List(Of JobInfo)))>
    <KnownType(GetType(List(Of Integer)))>
    <KnownType(GetType(List(Of UserInfo)))>
    <KnownType(GetType(List(Of ParcelMapInfo)))>
    <KnownType(GetType(List(Of Relationship)))>
    <KnownType(GetType(Dictionary(Of String, String)))>
    <KnownType(GetType(Dictionary(Of String, Integer)))>
    <KnownType(GetType(Dictionary(Of Integer, String)))>
    <DataContract(Namespace:="")> _
    Public Class StandardResponse
        Public Sub New()
            Me.messages = New List(Of String)()
            Me.statusCode = "200"
            Me.description = "OK"
            Me.responseFormat = "json"
            Me.CCTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")
        End Sub

        Public Sub New(ByVal statusCode As String)
            Me.messages = New List(Of String)()
            Me.statusCode = statusCode
            Me.CCTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")
            Select Case statusCode
                Case "200"
                    description = "OK"
                    Exit Select
                Case "500"
                    description = "An error occurred while processing your request. Please check messages for more info if available."
                    Exit Select
                Case "400"
                    description = "Bad input parameters!"
                    Exit Select
                Case "401"
                    description = "Authentication failed! Please verify your credentials."
                    Exit Select
                Case "403"
                    description = "Access forbidden!"
                    Exit Select
                Case "404"
                    description = "The content you are trying to access does not exist."
                    Exit Select
                Case "406"
                    description = "You have either specified an invalid dataFormat or the dataFormat is missing in the request body."
                    Exit Select
                Case "412"
                    description = "Pre-condition failed; Operation not allowed."
                    Exit Select
                Case "901"
                    description = "Closed for Maintenance"
                Case "999"
                    description = "Feature not implemented."
                    Exit Select

            End Select
        End Sub

        Public Sub New(ByVal statusCode As String, ByVal message As String)
            MyClass.New(statusCode)

            Me.messages.Add(message)
        End Sub

        Public Sub New(ByVal statusCode As String, ByVal subCode As String, ByVal ex As Exception)
            MyClass.New(statusCode)

            Select Case statusCode
                Case "400"
                    Select Case subCode
                        Case "1"
                            Me.description = "Mismatch in the request data."
                        Case "2"

                    End Select
            End Select
            If ex IsNot Nothing Then
                Me.AddMessage(ex.Message)
                If ex.GetBaseException IsNot Nothing Then
                    Me.AddMessage(ex.GetBaseException.Message)
                End If
            End If
        End Sub

        Public Sub New(ex As Exception, request As StandardRequest, Optional subCode As String = "")
            MyClass.New("400")

            Select Case subCode
                Case "1"
                    Me.description = "The request is not shaped in the expected format. Deserialization failed."
                Case "2"

            End Select

            ErrorMailer.ReportWCFException("API", ex, ServiceModel.OperationContext.Current, Web.HttpContext.Current.Request, request.accessKey)

            Me.AddMessage(ex.Message)
            If ex.GetBaseException IsNot Nothing AndAlso ex.GetBaseException IsNot ex Then
                Me.AddMessage(ex.GetBaseException.Message)
            End If
        End Sub

        'Sub New()
        '    ' TODO: Complete member initialization 
        'End Sub

        <DataMember(Order:=1)> Public Property statusCode As String
        <DataMember(Order:=2)> Public Property description As String
        <DataMember(Order:=3)> Public Property messages As List(Of String)
        <DataMember(Order:=4)> Public Property header As Object
        <DataMember(Order:=5)> Public Property data As Object
        <DataMember(Order:=6)> Public Property responseFormat As String
        <DataMember(Order:=7)> Public Property recordCount As Integer
        <DataMember(Order:=8)> Public Property CCTimeStamp As String
        '<DataMember(Order:=9)> Public Property RequestTimeStamp As String
        <IgnoreDataMember()> Public Property errorMessage As List(Of Object)
        <IgnoreDataMember()> Public Property headerMessage As Object
        Public handled As Boolean = False


        Public Sub AddMessage(ByVal message As String)
            Me.messages.Add(message)
        End Sub
    End Class

End Namespace

