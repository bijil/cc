﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts
Namespace RemoteIntegration
    Public Interface IDatabaseObject

        Sub CreateIn(ByVal target As CAMACloud.Data.Database, ByRef recordTracker As RecordCountInfo)

        Sub LoadFromDataRow(ByVal target As CAMACloud.Data.Database, ByVal dr As DataRow)

    End Interface
End Namespace
