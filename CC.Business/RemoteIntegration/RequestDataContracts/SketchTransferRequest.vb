﻿Imports System.Runtime.Serialization
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects
Imports CAMACloud.BusinessLogic.Sketching

Namespace RemoteIntegration.DataContracts

    <DataContract()>
    Public Class SketchTransferRequest
        Inherits StandardRequest

        <DataMember()> Public Property totalPages As Integer
        <DataMember()> Public Property pageIndex As Integer
        <DataMember()> Public Property pageSize As Integer
        <DataMember()> Public Property direction As String      'UPSYNC or DOWNSYNC
        <DataMember()> Public Property sketchFormat As String
        <DataMember()> Public Property sketches As List(Of SketchObject)
        <DataMember()> Public Property periodStart As String
        <DataMember()> Public Property periodEnd As String
        <IgnoreDataMember()> Public Property sketchProcessor As ISketchProcessor
    End Class

End Namespace
