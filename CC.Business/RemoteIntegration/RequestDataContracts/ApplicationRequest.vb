﻿
Imports System.Runtime.Serialization

Namespace RemoteIntegration.DataContracts

    <DataContract()> Public Class ApplicationRequest
        Inherits StandardRequest
        <DataMember()> Public Property resetSchema As Boolean

    End Class

End Namespace
