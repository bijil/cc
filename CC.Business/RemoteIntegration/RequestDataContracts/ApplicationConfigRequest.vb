﻿Imports System.Runtime.Serialization
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects
Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts

Namespace RemoteIntegration.DataContracts

    <DataContract(Namespace:="", Name:="AppConfig")>
    Public Class ApplicationConfigRequest
        Inherits StandardRequest
        <DataMember()> _
        Public Property parcelTable As String
        <DataMember()> _
        Public Property neighborhoodTable As String
        <DataMember()> _
        Public Property keyField1 As String
        <DataMember()> _
        Public Property keyField2 As String
        <DataMember()> _
        Public Property keyField3 As String
        <DataMember()> _
        Public Property keyField4 As String
        <DataMember()> _
        Public Property keyField5 As String
        <DataMember()> _
        Public Property keyField6 As String
        <DataMember()> _
        Public Property keyField7 As String
        <DataMember()> _
        Public Property keyField8 As String
        <DataMember()> _
        Public Property keyField9 As String
        <DataMember()> _
        Public Property keyField10 As String
        <DataMember()> _
        Public Property neighborhoodField As String
        <DataMember()> _
        Public Property neighborhoodNameField As String
        <DataMember()> _
        Public Property neighborhoodNumberField As String
        <DataMember()> _
        Public Property streetAddressTable As String
        <DataMember()> _
        Public Property streetAddressField As String
        <DataMember()> _
        Public Property streetAddressFilter As String
        <DataMember()> _
        Public Property StreetNumberField As String

        <DataMember()> _
        Public Property StreetNameField As String

        <DataMember()> _
        Public Property StreetNameSuffixField As String

        <DataMember()>
        Public Property streetAddressFilterValue As String
        <DataMember()>
        Public Property cityField As String
    End Class

End Namespace
