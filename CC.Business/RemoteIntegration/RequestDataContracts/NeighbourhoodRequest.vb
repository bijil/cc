﻿Imports System.Runtime.Serialization
Namespace RemoteIntegration.DataContracts

    <DataContract()>
    Public Class NeighbourhoodRequest
        Inherits StandardRequest
        <DataMember()> Public Property relinkOnly As Boolean
    End Class

End Namespace