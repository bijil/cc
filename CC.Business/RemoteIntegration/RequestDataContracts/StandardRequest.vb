﻿Imports CAMACloud.DataTools
Imports System.Runtime.Serialization
Imports System.Web

Namespace RemoteIntegration.DataContracts

    <DataContract()> _
    Public Class StandardRequest

        <DataMember(IsRequired:=True), IgnoreDataMember()> Public Property accessKey As String
        <DataMember(IsRequired:=True)> Public Property passKey As String
        <DataMember()> Public Property MACAddress As String
        <DataMember()> Public Property dataFormat As String
        <DataMember()> Public Property isCompressed As Boolean
        <DataMember()> Public Property dataEncoding As String
        <DataMember()> Public Property compressionFormat As String
        <DataMember()> Public Property data As String
        <DataMember()> Public Property jobId As String
        ' <DataMember()> Public Property RequestTimeStamp As String

        Public Sub New()
            Me.dataFormat = "json"
            'Me.RequestTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")
        End Sub
        Public Property extractedData() As String

        Public Function ExtractData(data As String, Optional packedFormat As String = "base64") As String
            Try
                If dataEncoding Is Nothing Then
                    Throw New Exception("""dataEncoding"" is a required parameter when uploading encoded data. Options are ""ascii"", ""utf8"", ""utf16"".")
                End If

                If isCompressed Then
                    If compressionFormat Is Nothing Then
                        Throw New Exception("""compressionFormat"" is a required parameter when uploading compressed data. Options are ""gzip"", ""zip"".")
                    End If
                End If

                extractedData = DataConversion.ExtractDataFromPack(data, packedFormat, dataEncoding, isCompressed, compressionFormat)
                Return extractedData
            Catch ex As Exception
                Throw New Exceptions.BadRequestException(ex)
            End Try
        End Function

        Public ReadOnly Property OrganizationId As Integer
            Get
                Return Database.System.GetIntegerValue("SELECT OrganizationId FROM ApplicationAccess WHERE AccessKey = " + accessKey.ToSqlValue)
            End Get
        End Property

        Public ReadOnly Property OrganizationName As String
            Get
                Return Database.System.GetStringValue("SELECT Name FROM Organization WHERE Id = " & OrganizationId)
            End Get
        End Property

        Public ReadOnly Property GetTenantKey As String
            Get
                Return "org" + Database.System.GetStringValue("SELECT OrganizationId FROM ApplicationAccess WHERE AccessKey = {0} AND PassKey={1}".SqlFormatString(accessKey, passKey)).PadLeft(3, "0")
            End Get
        End Property

        Public ReadOnly Property WebRequest As HttpRequest
            Get
                Return HttpContext.Current.Request
            End Get
        End Property

    End Class

End Namespace
