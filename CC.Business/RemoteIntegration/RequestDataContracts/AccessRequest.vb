﻿Imports System.Runtime.Serialization
Imports System.ComponentModel

Namespace RemoteIntegration.DataContracts
    <DataContract()>
    Public Class AccessRequest
        Inherits StandardRequest
        <DataMember()> Public Property username As String
        <DataMember()> Public Property firstName As String
        <DataMember()> Public Property lastName As String
        <DataMember()> Public Property password As String
        <DataMember()> Public Property passwordFormat As String
        <DataMember()> Public Property email As String
        <DataMember()> Public Property roles As List(Of String)
        <DataMember()> Public Property status As String
        Public Sub New()
            roles = New List(Of String)
        End Sub
    End Class
End Namespace

