﻿Imports System.Runtime.Serialization
Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts

Namespace RemoteIntegration.DataContracts

    <DataContract()>
    Public Class DataTransferRequest
        Inherits StandardRequest

        <DataMember()> Public Property reason As String
        <DataMember()> Public Property resetAudit As Boolean
        <DataMember()> Public Property resetChanges As Boolean
        <DataMember()> Public Property doNotUnlock As Boolean       'To be used only with commit
        <DataMember()> Public Property totalPages As Integer
        <DataMember()> Public Property totalTables As Integer
        <DataMember()> Public Property tableNames As String()
        <DataMember()> Public Property pageIndex As Integer
        <DataMember()> Public Property changeSpec As String
            Get
                Return data
            End Get
            Set(ByVal value As String)
                data = value
            End Set
        End Property
        <DataMember()> Public Property periodStart As String
        <DataMember()> Public Property periodEnd As String
        <DataMember()> Public Property getAllChanges As Boolean
        <DataMember()> Public Property overwriteConflicts As Boolean
        <DataMember()> Public Property backupPhotos As Boolean
        <DataMember()> Public Property downsyncAlternate As Boolean
        <DataMember()> Public Property scriptAction As String
        <DataMember()> Public Property acceptFields As String
        <DataMember()> Public Property ignoreFields As String
		<DataMember()> Public Property type As String
        <DataMember()> Public Property threadId As Integer = 0
        <DataMember()> Public Property commitOnly As Boolean = False
    End Class

End Namespace
