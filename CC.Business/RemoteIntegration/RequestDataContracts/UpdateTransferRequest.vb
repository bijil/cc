﻿Imports System.Runtime.Serialization
Imports System.Text.RegularExpressions
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects

Namespace RemoteIntegration.DataContracts
    Class ParcelIdentifier
        Public Property ParcelId
        Public Property AuxRowId
    End Class
    <DataContract()>
    Public Class UpdateTransferRequest

        <DataMember()> Public Property periodStart As DateTime
        <DataMember()> Public Property periodEnd As DateTime
        <DataMember()> Public Property changes As ParcelChange()


    End Class

End Namespace
