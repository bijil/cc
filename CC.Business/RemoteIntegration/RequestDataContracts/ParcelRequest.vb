﻿Imports System.Runtime.Serialization
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects
Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts

Namespace RemoteIntegration.DataContracts

    <DataContract()>
    Public Class ParcelRequest
        Inherits StandardRequest
        <DataMember()> _
        Public Property parcels As ParcelMapInfo()
        <DataMember()> Public Property [partial] As Boolean = False
        <DataMember()> Public Property deleteNewRecords As Boolean = False
    End Class

End Namespace
