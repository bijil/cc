﻿Imports System.Runtime.Serialization
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects
Imports System.Collections

Namespace RemoteIntegration.DataContracts
    Public Class PhotoSyncRequest
        Inherits StandardRequest

        <DataMember()> Public Property pageIndex As Integer
        <DataMember()> Public Property pageSize As Integer

        <DataMember()> Public Property startTime As String
        <DataMember()> Public Property endTime As String
        <DataMember()> Public Property photos As List(Of ParcelPhoto)
        <DataMember()> Public Property totalPages As Integer
        <DataMember()> Public Property reason As String
        <DataMember()> Public Property downloadUrlsOnly As Boolean = False
        <DataMember()> Public Property imageIds As List(Of Integer)
        <DataMember()> Public Property committedParcelsOnly As Boolean = False
        <DataMember()> Public Property stopped As Boolean = False
        <DataMember()> Public Property capturedDate As Boolean = False
    End Class
End Namespace
