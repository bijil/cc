﻿Imports System.Runtime.Serialization
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects
Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts

Namespace RemoteIntegration.DataContracts

    <DataContract()>
    Public Class TemplateRequest
        Inherits StandardRequest

        <DataMember()> Public Property dataTemplateTable As String
        <DataMember()> Public Property dataTemplateParentTable As String
        <DataMember()> Public Property parentFilterFields As New List(Of String)
        <DataMember()> Public Property templateData As String
        <DataMember()> Public Property parentData As String
        <DataMember()> Public Property parentKeys As New List(Of String)
        <DataMember()> Public Property templateParentKeys As New List(Of String)
        <DataMember()> Public Property appendTemplate As Boolean

    End Class

End Namespace
