﻿Imports System.Runtime.Serialization

Namespace RemoteIntegration.DataContracts

    <DataContract()> Public Class SystemRequest
        Inherits StandardRequest

        <DataMember()> Public Property startTime As String
        <DataMember()> Public Property endTime As String
    End Class
End Namespace
