﻿Imports System.Runtime.Serialization

Namespace RemoteIntegration.DataContracts

    <DataContract()> Public Class SchemaRequest
        Inherits StandardRequest

        <DataMember()> Public Property resetSchema As Boolean

        <DataMember()> Public Property resetRelations As Boolean

        <DataMember()> Public Property schema As String
            Get
                Return data
            End Get
            Set(value As String)
                data = value
            End Set
        End Property

    End Class
End Namespace
