﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts
Imports CAMACloud.BusinessLogic.RemoteIntegration.Objects

Namespace RemoteIntegration

    Public Class TransferJob

        Shared transferJobSql = <sql>
                                        CREATE TABLE TransferJob
                                        (
	                                        Id INT IDENTITY PRIMARY KEY,
	                                        CreatedBy INT,
	                                        CreatedTime DATETIME DEFAULT GETUTCDATE(),
	                                        SourceIPAddress VARCHAR(100),
	                                        TransferType INT,
	                                        TotalPages INT,
	                                        CurrentPage INT,
	                                        Completed BIT DEFAULT 0,
                                            Cancelled BIT DEFAULT 0,
	                                        Error BIT DEFAULT 0,
	                                        Reason VARCHAR(1000),
	                                        StatusMessage VARCHAR(1000),
	                                        ErrorMessage VARCHAR(MAX)
                                        )
                                </sql>

		Public Shared Function Create(ByVal db As Database, ByVal request As StandardRequest, type As TransferType) As Integer

			''' Cleanup old TransferPage records
			db.Execute("DELETE FROM TransferPage WHERE JobID IN (SELECT Id FROM TransferJob WHERE Completed = 1 OR Cancelled = 1 AND CreatedTime < DATEADD(month, -1, GETUTCDATE()))")

			Dim totalPages As Integer = 0
			Dim reason As String = ""
			If type.TransferType = ContentTransferTypes.DataTransfer Then
				totalPages = CType(request, DataTransferRequest).totalPages
				reason = CType(request, DataTransferRequest).reason
			ElseIf type.TransferType = ContentTransferTypes.PhotoTransfer Then
				totalPages = CType(request, PhotoSyncRequest).totalPages ' totalPages = CType(request, DataTransferRequest).totalPages
				reason = CType(request, PhotoSyncRequest).reason
			ElseIf type.TransferType = ContentTransferTypes.SketchTransfer Then
				totalPages = CType(request, SketchTransferRequest).totalPages
			End If

			If Not db.DoesTableExists("TransferJob") Then
				db.Execute(transferJobSql.Value)
			End If

			Dim pendingCheckSql As String = ""
			If type.LockLevel = TransferSyncLockLevel.All Then
				pendingCheckSql = "SELECT Id, TransferType FROM TransferJob WHERE Completed = 0 AND Cancelled = 0 AND Error = 0"
			ElseIf type.LockLevel = TransferSyncLockLevel.Self Then
				pendingCheckSql = "SELECT Id, TransferType FROM TransferJob WHERE Completed = 0 AND Cancelled = 0 AND Error = 0 AND TransferType = " & type.TransferType.GetHashCode
			End If
			If pendingCheckSql <> "" Then
				Dim dbJobs = db.GetDataTable(pendingCheckSql)

				Dim pendingJobIds = dbJobs.AsEnumerable.Select(Function(x) CInt(x("Id"))).ToList
				Dim pendingJobTypes = dbJobs.AsEnumerable.Select(Function(x) CType(x("TransferType"), ContentTransferTypes).ToString).ToList
				If pendingJobIds.Count > 0 Then
					'Throw New Exceptions.BadRequestException("Another transfer job is in progress. A new job cannot be initiated until the previous one is committed or cancelled.", Nothing, False)
					Throw New Exceptions.PendingTransferException(pendingJobIds, pendingJobTypes, False)
				End If
			End If


			Dim sql As String = "INSERT INTO TransferJob (CreatedBy, SourceIPAddress, TransferType, TotalPages, Reason, StatusMessage) VALUES ({0}, {1}, {2}, {3}, {4}, {5});"

			Dim applicationId As Integer = Database.System.GetIntegerValue("SELECT Id FROM ApplicationAccess WHERE AccessKey = {0}".SqlFormat(False, request.accessKey))
			Dim ipAddress As String = ""
			If System.Web.HttpContext.Current IsNot Nothing Then
				ipAddress = System.Web.HttpContext.Current.Request.ClientIPAddress
			End If
			Dim sqlJob As String = sql.SqlFormat(True, applicationId, ipAddress, type.TransferType.GetHashCode, totalPages, reason, "Initialized")

			Return db.GetIdentityFromInsert(sqlJob)
		End Function

        Public Shared Function Verify(ByVal db As Database, ByVal request As StandardRequest, ByVal type As TransferType) As Integer
            Dim jobId As Integer = -1
            Integer.TryParse(request.jobId, jobId)
            If jobId = -1 Then
                Throw New Exceptions.BadRequestException("A valid jobId is required to transfer data to CAMACloud.", Nothing)
            End If
            If db.GetIntegerValue("SELECT COUNT(*) FROM TransferJob WHERE Completed = 0 AND Error = 0 AND Cancelled = 0 AND Id = " & jobId) = 0 Then
                Throw New Exceptions.BadRequestException("The requested transfer jobId " & request.jobId & " is not valid at current context.", Nothing, False)
            End If
            Return jobId
        End Function

		Public Shared Function VerifyFull(ByVal db As Database, ByVal request As StandardRequest, ByVal type As TransferType) As Integer

			

			Dim jobId As Integer = -1
			Integer.TryParse(request.jobId, jobId)
			If jobId = -1 Then
				Throw New Exceptions.BadRequestException("A valid jobId is required to transfer data to CAMACloud.", Nothing)
			End If

			Dim verifySql As String = "EXEC cc_VerifyTransferJob @JobID = {0}"
			Dim resp = db.GetTopRow(verifySql.SqlFormat(False, jobId))

			If resp.GetInteger("Status") = -1 Then
				Throw New Exceptions.BadRequestException(resp.GetString("Message"), Nothing, False)
			End If

			'If db.GetIntegerValue("SELECT COUNT(*) FROM TransferJob WHERE Completed = 0 AND Error = 0 AND Cancelled = 0 AND Id = " & jobId) = 0 Then
			'	Throw New Exceptions.BadRequestException("The requested transfer jobId " & request.jobId & " is not valid at current context.", Nothing, False)
			'End If
			'If db.GetIntegerValue("SELECT TotalPages - CompletedPages FROM TransferJob WHERE Id = " & jobId) > 0 Then
			'	Throw New Exceptions.BadRequestException("The requested transfer jobId " & request.jobId & " corresponds to an incomplete transfer. Cannot commit job when the completed pages do not equate with the total pages at initialization.", Nothing, False)
			'End If
			Return jobId
		End Function

        Public Shared Sub Commit(ByVal db As Database, ByVal request As StandardRequest, ByVal type As TransferType)
            Dim jobId As Integer = Verify(db, request, type)
            db.Execute("UPDATE TransferJob SET Completed = 1, StatusMessage = 'Completed and committed' WHERE Id = " & jobId)
        End Sub
        
        Public Shared Sub AutoPriorityUpdate(ByVal db As Database, ByVal request As StandardRequest, ByVal type As TransferType)
        	Dim value As Integer =db.GetIntegerValue("SELECT Value FROM ClientSettings WHERE Name = 'EnableAutoPriority'")
        	If value=1 Then
	        	Dim sql = "EXEC api_autoProritySet "
	        	db.Execute(sql)
        	End If
            'Dim sqlInsert = "INSERT INTO ParcelRefreshLog(ParcelID,Refreshtype,RefreshDateTime,APSUpdated,APSReseted) SELECT ParcelID,Refreshtype,RefreshDateTime,APSUpdated,APSReseted FROM ParcelRefreshLogCurrent"
            'db.Execute(sqlInsert)
            'Dim sqlDelete = "DELETE FROM ParcelRefreshLogCurrent"
            'db.Execute(sqlDelete)
            db.Execute("EXEC API_ParcelRefreshLogs")
        End Sub

		Public Shared Sub ProcessMAParcelComparable(ByVal db As Database, ByVal request As StandardRequest, ByVal type As TransferType)
			Dim pcCount As Integer = db.GetIntegerValue("SELECT COUNT(*) FROM MAParcelComparableRefreshLogCurrent")
			If pcCount > 0 Then
				Dim dtRow As DataRow = db.GetTopRow("SELECT top 1 * FROM MAParcelComparableFields")
				Dim Comparable1 As String = dtRow.GetString("Comparable1")
				Dim Comparable2 As String = dtRow.GetString("Comparable2")
				Dim Comparable3 As String = dtRow.GetString("Comparable3")
				Dim Comparable4 As String = dtRow.GetString("Comparable4")
				Dim Comparable5 As String = dtRow.GetString("Comparable5")
				Dim keyField As String = dtRow.GetString("KeyFieldValue")
				Dim keyFd As String = ""
				Dim drow As DataRow = db.GetTopRow("SELECT * FROM Application")
				If keyField = "K" Then
					keyFd = drow.GetString("KeyField1")
					keyFd = "pd." + keyFd
				Else
					keyFd = drow.GetString("Alternatekeyfieldvalue")
					keyFd = "pd." + keyFd
				End If

				Dim sql As String = ""
				Dim iqtemp As String = "ParcelId, "
				Dim vq As String = "CC_ParcelId, "
				Dim up As String = ""
				Dim iq As String = ""
				Dim iqtWhere = ""
				Dim delWhere = ""

				If Comparable1 <> "" Then
					iqtemp += "C1, "
					vq += Comparable1 + ", "
					up += "UPDATE t SET t." + Comparable1 + " = pd.CC_ParcelId FROM #temp_parcel_comparable t JOIN ParcelData pd ON t." + Comparable1 + " = " + keyFd
					iqtWhere += Comparable1 + " IS NOT NULL OR "
					delWhere += " (" + Comparable1 + " Is Not NULL And ISNUMERIC(" + Comparable1 + ") = 0) OR"
				End If
				If Comparable2 <> "" Then
					iqtemp += "C2, "
					vq += Comparable2 + ", "
					up += "; UPDATE t SET t." + Comparable2 + " = pd.CC_ParcelId FROM #temp_parcel_comparable t JOIN ParcelData pd ON t." + Comparable2 + " = " + keyFd
					iqtWhere += Comparable2 + " IS NOT NULL OR "
					delWhere += " (" + Comparable2 + " Is Not NULL And ISNUMERIC(" + Comparable2 + ") = 0) OR"
				End If
				If Comparable3 <> "" Then
					iqtemp += "C3, "
					vq += Comparable3 + ", "
					up += "; UPDATE t SET t." + Comparable3 + " = pd.CC_ParcelId FROM #temp_parcel_comparable t JOIN ParcelData pd ON t." + Comparable3 + " = " + keyFd
					iqtWhere += Comparable3 + " IS NOT NULL OR "
					delWhere += " (" + Comparable3 + " Is Not NULL And ISNUMERIC(" + Comparable3 + ") = 0) OR"
				End If
				If Comparable4 <> "" Then
					iqtemp += "C4, "
					vq += Comparable4 + ", "
					up += "; UPDATE t SET t." + Comparable4 + " = pd.CC_ParcelId FROM #temp_parcel_comparable t JOIN ParcelData pd ON t." + Comparable4 + " = " + keyFd
					iqtWhere += Comparable4 + " IS NOT NULL OR "
					delWhere += " (" + Comparable4 + " Is Not NULL And ISNUMERIC(" + Comparable4 + ") = 0) OR"
				End If
				If Comparable5 <> "" Then
					iqtemp += "C5, "
					vq += Comparable5 + ", "
					up += "; UPDATE t SET t." + Comparable5 + " = pd.CC_ParcelId FROM #temp_parcel_comparable t JOIN ParcelData pd ON t." + Comparable5 + " = " + keyFd
					iqtWhere += Comparable5 + " IS NOT NULL OR "
					delWhere += " (" + Comparable5 + " Is Not NULL And ISNUMERIC(" + Comparable5 + ") = 0) OR"
				End If

				iqtWhere = iqtWhere.Substring(0, iqtWhere.Length - 4)
				vq = vq.Substring(0, vq.Length - 2)
				iqtemp = iqtemp.Substring(0, iqtemp.Length - 2)
				delWhere = delWhere.Substring(0, delWhere.Length - 3)

				sql += "SELECT " + vq + " INTO #temp_parcel_comparable FROM ParcelData pd JOIN MAParcelComparableRefreshLogCurrent mp on pd.CC_ParcelId = mp.ParcelID WHERE " + iqtWhere
				sql += "; " + up
				sql += "; DELETE FROM #temp_parcel_comparable WHERE " + delWhere
				sql += "; DELETE FROM ParcelComparables WHERE ParcelId in (SELECT CC_ParcelId FROM #temp_parcel_comparable)"
				sql += "; INSERT INTO ParcelComparables (" + iqtemp + ") SELECT " + vq + " FROM #temp_parcel_comparable;"
				db.Execute(sql)
				Dim sqlDelete = "DELETE FROM MAParcelComparableRefreshLogCurrent"
				db.Execute(sqlDelete)
			End If

		End Sub


		Public Shared Sub Cancel(ByVal db As Database, ByVal request As StandardRequest, ByVal type As TransferType)
            Dim jobId As Integer = Verify(db, request, type)
            db.Execute("UPDATE TransferJob SET Cancelled = 1, StatusMessage = 'Cancelled' WHERE Id = " & jobId)
			db.Execute("DELETE FROM ParcelRefreshLogCurrent")
			db.Execute("DELETE FROM MAParcelComparableRefreshLogCurrent")
		End Sub

        Public Shared Sub CancelAll(ByVal db As Database, ByVal request As DataTransferRequest)
            db.Execute("UPDATE TransferJob SET Cancelled = 1, StatusMessage = 'Cancelled' WHERE Completed = 0 AND Cancelled = 0 AND Error = 0")
        End Sub

        Public Shared Function GetTotalPages(ByVal db As Database, jobId As Integer)
            Return db.GetIntegerValue("SELECT TotalPages FROM TransferJob WHERE Id = " & jobId)
        End Function

        Public Shared Sub UpdatePagingDetails(ByVal db As Database, pageSize As Integer, totalPages As Integer, jobId As Integer)
            db.Execute("UPDATE TransferJob SET PageSize={1},TotalPages={2} WHERE Id={0} ".SqlFormatString(jobId, pageSize, totalPages))
        End Sub

        Public Shared Sub UpdateCurrentPage(ByVal db As Database, currentPage As Integer, jobId As Integer)
			db.Execute("UPDATE TransferJob SET CurrentPage={1} WHERE Id={0} ".SqlFormatString(jobId, currentPage))
        End Sub

		Public Shared Sub IncrementJobPage(ByVal db As Database, jobId As Integer)
			db.Execute("UPDATE TransferJob SET CompletedPages = ISNULL(CompletedPages, 0) + 1 WHERE Id = {0} ".SqlFormatString(jobId))
		End Sub

		Public Shared Sub RegisterPageDetails(ByVal db As Database, request As DataTransferRequest, ByRef data As DataSet, tracker As RecordCountInfo)
			Dim tableNames As String = String.Join(",", request.tableNames)
			Dim uploadedRecords As Integer = 0
			For Each dt As DataTable In data.Tables
				uploadedRecords += dt.Rows.Count
			Next
			Dim affectedRecords As Integer = tracker.rowsAffected

			Dim sqlPageUpdate = "EXEC cc_UpdateTransferJobPage @JobID = {0}, @ThreadID = {1}, @PageIndex = {2}, @TableNames = {3}, @UploadedRecords = {4}, @ParcelsUpdated = {5}, @ParcelsAdded = {6}, @RecordsUpdated = {7}, @RecordsInserted = {8}"
			Dim sql = sqlPageUpdate.SqlFormat(False, request.jobId, request.threadId, request.pageIndex, tableNames, uploadedRecords, tracker.parcelModified, tracker.parcelCreated, tracker.recordsUpdated, tracker.recordsInserted)
			db.Execute(sql)

		End Sub

        Public Shared Function GetActiveJobs(db As Database) As List(Of JobInfo)
            Dim jobs As New List(Of JobInfo)
            Dim dt As DataTable = db.GetDataTable("SELECT * FROM TransferJob WHERE Completed = 0 AND Cancelled = 0")
            For Each dr As DataRow In dt.Rows
                Dim j As New JobInfo
                j.ID = dr.Get("Id")
                j.CreatedTime = dr.GetDate("CreatedTime")
                j.SourceIPAddress = dr.GetString("SourceIPAddress")
                j.Type = DirectCast(dr.GetInteger("TransferType"), ContentTransferTypes).ToString
                jobs.Add(j)
            Next
            Return jobs
        End Function

		Shared Function GetStartTime(db As Database, request As DataTransferRequest) As DateTime
			Return db.GetTopRow("SELECT CreatedTime FROM TransferJob WHERE Id = " & request.jobId)("CreatedTime")
		End Function

		Shared Function GetData(db As Database, jobId As Integer) As String
			Return db.GetStringValue("SELECT TransferData FROM TransferJob WHERE Id = " & jobId, "")
		End Function

		Shared Function SetData(db As Database, jobId As Integer, data As String) As String
			Return db.Execute("UPDATE TransferJob SET TransferData = " + data.ToSqlValue + " WHERE Id = " & jobId)
		End Function

    End Class




End Namespace



'Public Shared Function CreatePhotoTransferJob(ByVal db As Database, accessKey As String, ByVal type As BulkDataTransferType) As Integer
'    If Not db.DoesTableExists("TransferJob") Then
'        db.Execute(transferJobSql.Value)
'    End If
'    Dim pendingJobIds = db.GetDataTable("SELECT Id FROM TransferJob WHERE Completed = 0 AND Cancelled = 0 AND Error = 0").AsEnumerable.Select(Function(x) CInt(x("Id"))).ToList
'    If pendingJobIds.Count > 0 Then
'        'Throw New Exceptions.BadRequestException("Another transfer job is in progress. A new job cannot be initiated until the previous one is committed or cancelled.", Nothing, False)
'        Throw New Exceptions.PendingTransferException(pendingJobIds, False)
'    End If

'    Dim sql As String = "INSERT INTO TransferJob (CreatedBy, SourceIPAddress, TransferType, StatusMessage) VALUES ({0}, {1}, {2}, {3});"

'    Dim applicationId As Integer = Database.System.GetIntegerValue("SELECT Id FROM ApplicationAccess WHERE AccessKey = {0}".SqlFormat(False, accessKey))
'    Dim ipAddress As String = ""
'    If System.Web.HttpContext.Current IsNot Nothing Then
'        ipAddress = System.Web.HttpContext.Current.Request.ClientIPAddress
'    End If
'    Dim sqlJob As String = sql.SqlFormat(True, applicationId, ipAddress, type.GetHashCode, "Initialized")

'    Return db.GetIdentityFromInsert(sqlJob)
'End Function

'Public Shared Sub UpdatePagingDetails(ByVal db As Database, pageSize As Integer, totalPages As Integer, jobId As Integer)
'    db.Execute("UPDATE TransferJob SET PageSize={1},TotalPages={2} WHERE Id={0} ".SqlFormatString(jobId, pageSize, totalPages))
'End Sub

'Public Shared Sub UpdateCurrentPage(ByVal db As Database, currentPage As Integer, jobId As Integer)
'    db.Execute("UPDATE TransferJob SET CurrentPage={1} WHERE Id={0} ".SqlFormatString(jobId, currentPage))
'End Sub

'Public Shared Function VerifyPhotoTransferJob(ByVal type As BulkDataTransferType, ByVal db As Database, ByVal request As PhotoSyncRequest) As Integer
'    Dim jobId As Integer = -1
'    Integer.TryParse(request.jobId, jobId)
'    If jobId = -1 Then
'        Throw New Exceptions.BadRequestException("A valid jobId is required to transfer data to CAMACloud.", Nothing)
'    End If
'    If db.GetIntegerValue("SELECT COUNT(*) FROM TransferJob WHERE Completed = 0 AND Error = 0 AND Cancelled = 0 AND Id = " & jobId) = 0 Then
'        Throw New Exceptions.BadRequestException("The requested transfer jobId is not valid at current context.", Nothing)
'    End If

'    Dim pageSize = db.GetIntegerValue("SELECT PageSize FROM TransferJob WHERE Id={0}".SqlFormatString(jobId))

'    Return jobId
'End Function

'Public Shared Sub CommitPhotoTransferJob(ByVal type As BulkDataTransferType, ByVal db As Database, ByVal request As PhotoSyncRequest)
'    Dim jobId As Integer = VerifyPhotoTransferJob(type, db, request)
'    db.Execute("UPDATE TransferJob SET Completed = 1, StatusMessage = 'Completed and committed' WHERE Id = " & jobId)
'End Sub
''Public Shared Function GetCurrentPage(ByVal type As BulkDataTransferType, ByVal db As Database, jobId As Integer)
''    Return db.GetIntegerValue("SELECT CurrentPage FROM TransferJob WHERE Id = " & jobId)
''End Function
''Public Shared Function GetPageSize(ByVal type As BulkDataTransferType, ByVal db As Database, jobId As Integer)
''    Return db.GetIntegerValue("SELECT PageSize FROM TransferJob WHERE Id = " & jobId)
''End Function

'Public Shared Sub CancelPhotoTransferJob(ByVal type As BulkDataTransferType, ByVal db As Database, ByVal request As PhotoSyncRequest)
'    Dim jobId As Integer = VerifyPhotoTransferJob(type, db, request)
'    db.Execute("UPDATE TransferJob SET Cancelled = 1, StatusMessage = 'Cancelled' WHERE Id = " & jobId)
'End Sub

'Public Shared Function CreateTransferJob(ByVal db As Database, ByVal request As DataTransferRequest, ByVal type As BulkDataTransferType) As Integer
'    If Not db.DoesTableExists("TransferJob") Then
'        db.Execute(transferJobSql.Value)
'    End If
'    Dim pendingJobIds = db.GetDataTable("SELECT Id FROM TransferJob WHERE Completed = 0 AND Cancelled = 0 AND Error = 0").AsEnumerable.Select(Function(x) CInt(x("Id"))).ToList
'    If pendingJobIds.Count > 0 Then
'        'Throw New Exceptions.BadRequestException("Another transfer job is in progress. A new job cannot be initiated until the previous one is committed or cancelled.", Nothing, False)
'        Throw New Exceptions.PendingTransferException(pendingJobIds, False)
'    End If

'    Dim sql As String = "INSERT INTO TransferJob (CreatedBy, SourceIPAddress, TransferType, TotalPages, Reason, StatusMessage) VALUES ({0}, {1}, {2}, {3}, {4}, {5});"

'    Dim applicationId As Integer = Database.System.GetIntegerValue("SELECT Id FROM ApplicationAccess WHERE AccessKey = {0}".SqlFormat(False, request.accessKey))
'    Dim ipAddress As String = ""
'    If System.Web.HttpContext.Current IsNot Nothing Then
'        ipAddress = System.Web.HttpContext.Current.Request.ClientIPAddress
'    End If
'    Dim sqlJob As String = sql.SqlFormat(True, applicationId, ipAddress, type.GetHashCode, request.totalPages, request.reason, "Initialized")

'    Return db.GetIdentityFromInsert(sqlJob)
'End Function

'Public Shared Function VerifyTransferJob(ByVal type As BulkDataTransferType, ByVal db As Database, ByVal request As DataTransferRequest) As Integer
'    Dim jobId As Integer = -1
'    Integer.TryParse(request.jobId, jobId)
'    If jobId = -1 Then
'        Throw New Exceptions.BadRequestException("A valid jobId is required to transfer data to CAMACloud.", Nothing)
'    End If
'    If db.GetIntegerValue("SELECT COUNT(*) FROM TransferJob WHERE Completed = 0 AND Error = 0 AND Cancelled = 0 AND Id = " & jobId) = 0 Then
'        Throw New Exceptions.BadRequestException("The requested transfer jobId is not valid at current context.", Nothing, False)
'    End If
'    Return jobId
'End Function

'Public Shared Sub CommitTransferJob(ByVal type As BulkDataTransferType, ByVal db As Database, ByVal request As DataTransferRequest)
'    Dim jobId As Integer = VerifyTransferJob(type, db, request)
'    db.Execute("UPDATE TransferJob SET Completed = 1, StatusMessage = 'Completed and committed' WHERE Id = " & jobId)
'End Sub

'Public Shared Sub CancelTransferJob(ByVal type As BulkDataTransferType, ByVal db As Database, ByVal request As DataTransferRequest)
'    Dim jobId As Integer = VerifyTransferJob(type, db, request)
'    db.Execute("UPDATE TransferJob SET Cancelled = 1, StatusMessage = 'Cancelled' WHERE Id = " & jobId)
'End Sub

'Public Shared Sub CancelAllTransferJobs(ByVal db As Database, ByVal request As DataTransferRequest)
'    db.Execute("UPDATE TransferJob SET Cancelled = 1, StatusMessage = 'Cancelled' WHERE Completed = 0 AND Cancelled = 0 AND Error = 0")
'End Sub
