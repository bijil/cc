﻿Namespace RemoteIntegration

    Public Enum ContentTransferTypes
        DataTransfer = 101
        PhotoTransfer = 102
        SketchTransfer = 103
    End Enum

    Public Enum TransferSyncLockLevel
        None = 0
        Self = 1
        All = 2
    End Enum

    Public Class TransferType

        Private _transferType As ContentTransferTypes
        Private _syncLockLevel As TransferSyncLockLevel

        Public Sub New(transferType As ContentTransferTypes, lockLevel As TransferSyncLockLevel)
            _transferType = transferType
            _syncLockLevel = lockLevel
        End Sub

        Public ReadOnly Property TransferType As ContentTransferTypes
            Get
                Return _transferType
            End Get
        End Property

        Public ReadOnly Property LockLevel As TransferSyncLockLevel
            Get
                Return _syncLockLevel
            End Get
        End Property

    End Class


    Public Class TransferTypes
        Public Shared ReadOnly DataTransfer As New TransferType(ContentTransferTypes.DataTransfer, TransferSyncLockLevel.Self)
        Public Shared ReadOnly PhotoTransfer As New TransferType(ContentTransferTypes.PhotoTransfer, TransferSyncLockLevel.All)
        Public Shared ReadOnly SketchTransfer As New TransferType(ContentTransferTypes.SketchTransfer, TransferSyncLockLevel.Self)
    End Class
End Namespace
