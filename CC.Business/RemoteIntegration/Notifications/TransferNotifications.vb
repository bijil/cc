﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts
Imports CAMACloud.BusinessLogic.RemoteIntegration

Public Class TransferNotification

    Private Shared _purgeNotification = <html>
                                            <head></head>
                                            <body>
                                                <h1 style="font-family:Segoe UI,Tahoma,Trebuchet MS,Arial;border-bottom:1px solid #CFCFCF;">'{0}' Data Purged</h1>
                                                <p>All parcel data from '{0}' account has been purged, and the status has been reset to empty. This action indicates that fresh data is being pushed to the account.</p>
                                                <p>Signature: {1} - {2}</p>
                                            </body>
                                        </html>

    Private Shared _resetCommitNotification = <html>
                                                  <head></head>
                                                  <body>
                                                      <h1 style="font-family:Segoe UI,Tahoma,Trebuchet MS,Arial;border-bottom:1px solid #CFCFCF;">'{0}' Data Loaded</h1>
                                                      <p>All parcel data from '{0}' account has been loaded successfully and committed. The further actions may be loaded GIS data and photos.</p>
                                                      <p>Signature: {1} - {2}</p>
                                                  </body>
                                              </html>

    Private Shared _refreshCommitNotification = <html>
                                                    <head></head>
                                                    <body>
                                                        <h1 style="font-family:Segoe UI,Tahoma,Trebuchet MS,Arial;border-bottom:1px solid #CFCFCF;">'{0}' Data Refreshed</h1>
                                                        <p>Parcel data from '{0}' account has been refreshed successfully and committed. </p>
                                                        <p>Signature: {1} - {2}</p>
                                                    </body>
                                                </html>

    Private Shared _changeCommitNotification = <html>
                                                   <head></head>
                                                   <body>
                                                       <h1 style="font-family:Segoe UI,Tahoma,Trebuchet MS,Arial;border-bottom:1px solid #CFCFCF;">'{0}' Data Schema Changed &amp; Refreshed</h1>
                                                       <p>Schema has been modified on '{0}' account and parcel data has been refreshed successfully and committed. </p>
                                                       <p>Signature: {1} - {2}</p>
                                                   </body>
                                               </html>

    Public Shared Function PurgeNotification(request As StandardRequest) As EventNotification
        Dim en As New EventNotification(request, "ACCOUNT-PURGE")
        en.Body = String.Format(_purgeNotification.ToString, en.OrganizationName, request.accessKey, request.WebRequest.UserHostAddress)
        Return en
    End Function

    Public Shared Sub SendPurgeNotification(request As StandardRequest)
        Dim en = PurgeNotification(request)
        en.Send()
    End Sub

    Public Shared Function CommitNotification(request As StandardRequest) As EventNotification
        Dim en As New EventNotification(request, "TRANSFER-COMMIT")
        en.Body = String.Format(_resetCommitNotification.ToString, en.OrganizationName, request.accessKey, request.WebRequest.UserHostAddress)
        Return en
    End Function

    Public Shared Sub SendCommitNotification(type As BulkDataTransferType, request As DataTransferRequest)
        Dim en As EventNotification = New EventNotification(request, "TRANSFER-COMMIT")
        Dim notificationFormat As String = ""
        Select Case type
            Case BulkDataTransferType.Reset
                en = New EventNotification(request, "DATA-LOADED")
                notificationFormat = _resetCommitNotification.ToString
            Case BulkDataTransferType.Refresh
                en = New EventNotification(request, "DATA-REFRESHED")
                notificationFormat = _refreshCommitNotification.ToString
            Case BulkDataTransferType.Change
                en = New EventNotification(request, "DATA-CHANGED")
                notificationFormat = _changeCommitNotification.ToString
        End Select
        en.Body = String.Format(notificationFormat.ToString, en.OrganizationName, request.accessKey, request.WebRequest.UserHostAddress)
        en.Send()
    End Sub
End Class
