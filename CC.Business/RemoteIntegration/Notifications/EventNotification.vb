﻿Imports CAMACloud.BusinessLogic.RemoteIntegration.DataContracts

Public Class EventNotification

    Private _organizationName As String
    Public ReadOnly Property OrganizationName As String
        Get
            Return _organizationName
        End Get
    End Property
    Public Property Subject As String
    Public Property Body As String

    Public Sub New(request As StandardRequest, tag As String)
        _organizationName = request.OrganizationName
        Subject = "[" + tag + "] :: " + _organizationName
    End Sub

    Public Sub Send()
        ServiceMailer.SendEmail(Subject, Body)
    End Sub

End Class
