﻿Imports CAMACloud.BusinessLogic.RemoteIntegration
Imports System.Text
Imports System.Web
Imports System.IO
Imports System.Text.RegularExpressions
Namespace Installation

    Public Enum ConfigFileType
        All
        FieldCategory
        CSEFieldCategory
        ApplicationSettings
        ClientSettings
        SketchSettings
        ClientTemplates
        ClientCustomAction
        ClientValidation
        AppraisalType
        FieldAlertTypes
        GISSettings
        DTRStatusTypes
        DTRTasks
        UI_HeatMap
        UI_HeatMapLookup
        ScreenMenu
        DataSourceTableAction
        TableProperties
        AggregateFieldSettings
        MRATemplates
        HeatmapSettings
        RangeLookups
    End Enum

    Public Class EnvironmentConfiguration

#Region " Public Methods"
        Dim importfrom As String
        Dim target As Database
        Dim OrgId As Integer = -1
        Public Sub ExportSettings(configType As ConfigFileType, response As HttpResponse, tag As String, db As Database)
            target = db
            Dim doc = ExportSettings(configType, db)
            Dim xml As String = doc.ToString(SaveOptions.None)
            'xml = Regex.Replace(xml, "\B""*&lt;""*\B", "<")
            'xml = Regex.Replace(xml, "\B""*&gt;""*\B", ">")
            'xml = Regex.Replace(xml, "\B""*&amp;""*\B", "&")
            Dim exportName = HttpContext.Current.GetCAMASession.OrganizationCodeName + "-" + tag + "-" + Now.ToString("yyMM-dd-HHmmss") + ".xml"
            response.WriteAsAttachment(xml, exportName)
        End Sub

        Public Function ExportSettings(configType As ConfigFileType, db As Database) As XDocument
            target = db
            Dim xdoc As XDocument = GetRootDoc(configType)
            If configType = ConfigFileType.All Or configType = ConfigFileType.FieldCategory Then
                _exportFieldCategory(xdoc)
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.CSEFieldCategory Then
                _exportCSEFieldCategory(xdoc)
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.ApplicationSettings Then
                _exportApplicationSettings(xdoc)
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.ClientTemplates Then
                _exportClientTemplates(xdoc)
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.ClientSettings Then
                _exportClientSettings(xdoc)
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.SketchSettings Then
                _exportSketchSettings(xdoc)
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.ClientCustomAction Then
                _exportClientCustomAction(xdoc)
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.ClientValidation Then
                _exportClientValidation(xdoc)
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.AppraisalType Then
                _exportAppraisalTypes(xdoc)
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.FieldAlertTypes Then
                _exportFieldAlertType(xdoc)
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.GISSettings Then
                _exportGISSettings(xdoc)
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.DTRStatusTypes Then
                _exportDTRStatusTypes(xdoc)
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.DTRTasks Then
                _exportDTRSTasks(xdoc)
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.UI_HeatMap Then
                _exportUI_HeatMap(xdoc)
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.UI_HeatMapLookup Then
                _exportUI_HeatMapLookup(xdoc)
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.ScreenMenu Then
                _exportScreenMenu(xdoc)
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.DataSourceTableAction Then
                _exportTableAction(xdoc)
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.TableProperties Then
                _exportTableProperties(xdoc)
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.AggregateFieldSettings Then
                _exportAggregateFieldSettings(xdoc)
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.MRATemplates Then
                _exportMraTemplates(xdoc)
            End If
            If configType = ConfigFileType.HeatmapSettings Then
            	_exportHeatMapsettings(xdoc)
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.RangeLookups Then
                _exportRangeLookups(xdoc)
            End If
            Return xdoc
        End Function


        Public Function ImportSettings(stream As IO.Stream, configType As ConfigFileType, db As Database, Optional loginId As String = "API", Optional ByVal tag As String = "", Optional Filename As String ="")
            target = db
            Dim strXml As String

            Dim impXML As String
            Dim importXm As String
            Dim node As XElement
            impXML = "SELECT COUNT(*) As TotalCount FROM FieldCategory WHERE SourcetableName in ("


            Using Xmlreader = New StreamReader(stream, Encoding.UTF8)
                strXml = Xmlreader.ReadToEnd()
                'strXml = strXml.Replace("&", "&amp;")  CC_2303 - code change reverted , it causes some conversion issue in xml import
                node = XElement.Parse(strXml)

                If  configType = ConfigFileType.FieldCategory Then 
            		Dim sourceTableNameExists As Boolean = False 'condition added to check any  sourceTable name exists.If not exists then impXML throws error.
            		importXm = "0"

                    For Each cat In node.Descendants("FieldCategory")


                        Dim abc As String = cat.@SourceTableName


                        If cat.@SourceTableName.Trim <> "" Then
                            sourceTableNameExists = True
                            impXML.Append("'" + cat.@SourceTableName.Trim + "'" + " , ")
                        End If
                    Next

                    If sourceTableNameExists Then
            			impXML = impXML.Substring(0,impXML.Length-3)
            			impXML.Append(" ) ")
            			Dim res As DataRow=Database.Tenant.GetTopRow(impXML)
            			importXm = res.GetString("TotalCount")
            		End If
            		If importXm < 1 Then
            			Dim sql As String = "SELECT BackupDocument FROM CC_SettingsBackup WHERE ImportId = (SELECT MAX(ImportId) FROM CC_SettingsBackup WHERE BackupDocument LIKE '%FieldCategory%' AND BackupName LIKE '%Backup%')"
		            	Dim result As String = db.GetStringValue(sql) 
		            	ImportSettings(strXml, configType, db, loginId, tag, Filename)
            			Return 1
            		End If
            	End If
            End Using

            'Dim escapChr As String
            'Dim regArray() As String = {"\B""([^""\/>][^"" \/>][^"" >][^"">][^""]*(>)[^""]*[^>""])""\B", "\B""([^""\/>][^"" \/>][^"" >][^"">][^""]*(<)[^""]*[^<""])""\B", "\B""([^""\/>][^"" \/>][^"" >][^"">][^""]*(&&)[^""]*[^<""])""\B"}
            'For Each repex As String In regArray
            '	Dim matches As MatchCollection = Regex.Matches(strXml, repex)
            '	For Each m As Match In matches
            '		Dim fGroup = m.Groups(1).Value
            '		Dim sGroup = m.Groups(2).Value
            '		If (sGroup = ">") Then
            '			escapChr = "&gt;"
            '		ElseIf (sGroup = "<") Then
            '			escapChr = "&lt;"
            '		ElseIf (sGroup = "&&") Then
            '			escapChr = "&amp;&amp;"
            '		End If

            '		Dim sGroupRep = Regex.Replace(fGroup, sGroup, escapChr)
            '		sGroupRep = """" + sGroupRep + """"
            '		strXml = Regex.Replace(strXml, repex, sGroupRep)
            '	Next
            'Next

            ImportSettings(strXml, configType, db, loginId, tag, Filename)
        End Function


        Public Sub ImportSettings(xmlString As String, configType As ConfigFileType, db As Database, Optional loginId As String = "API", Optional ByVal tag As String = "", Optional Name As String ="", Optional OId As Integer = -1)
            target = db
            OrgId = OId
            importfrom = Name
            Try
            	Dim xDoc = XDocument.Parse(xmlString)
                BackupBeforeImport(xDoc, tag, loginId, configType)
            Catch xex As Xml.XmlException
                Dim DBR As String = vbNewLine + vbNewLine

                Dim lines = xmlString.Replace(vbCrLf, vbLf).Replace(vbCr, vbLf).Split(vbLf)
                Dim errorLine As String = lines(xex.LineNumber - 1)
                Dim startingPos As Integer = errorLine.LastIndexOf("""", xex.LinePosition)
                Dim endingPos As Integer = errorLine.IndexOf("""", xex.LinePosition)
                Dim propertyStartingPos As Integer = errorLine.LastIndexOf(" ", startingPos)

                Dim errorMessage As String = ""
                If endingPos > startingPos Then


                    Dim errorValue As String = errorLine.Substring(startingPos + 1, endingPos - startingPos - 1)
                    Dim errorProperty As String = errorLine.Substring(propertyStartingPos + 1, startingPos - propertyStartingPos - 1 - 1)
                    errorMessage += "Incorrect XML property assignment: (Line: {0}, Position: {1})".FormatString(xex.LineNumber, xex.LinePosition) + DBR + errorProperty + DBR + errorValue + DBR + "Use escaped formats to preserve symbols and special characters."
                Else
                    errorMessage += "Incorrect XML content at Line: {0}, Position: {1}".FormatString(xex.LineNumber, xex.LinePosition) + DBR + errorLine
                End If

                errorMessage += DBR + "Note: This is only the first encountered error in the uploaded file. There might be more errors."


                Throw New Exception(errorMessage)
            Catch ex As Exception
                Throw ex
            End Try

        End Sub



#End Region

#Region "Export private Methods"

        Private Function GetRootDoc(configType As ConfigFileType) As XDocument
            Dim doc = New XDocument(New XDeclaration("1.0", "utf-8", True))
            Dim parentRoot = <CAMACloudConfiguration></CAMACloudConfiguration>
            doc.Add(parentRoot)
            Return doc
        End Function

        Private Sub _exportClientSettings(ByRef xdoc As XDocument)

            Dim clientDatatable = target.GetDataTable("SELECT Name,Value FROM ClientSettings")
            Dim SettingsNode = <settings type="ClientSettings"></settings>
            For Each dataRow In clientDatatable.Rows
                Dim ClientSettingsNode = <ClientSettings/>
                ClientSettingsNode.@Name = dataRow(0).ToString
                ClientSettingsNode.@Value = dataRow(1).ToString
                SettingsNode.Add(ClientSettingsNode)
            Next
            xdoc.Root.Add(SettingsNode)
        End Sub
        Private Sub _exportSketchSettings(ByRef xdoc As XDocument)

            Dim sketchDatatable = target.GetDataTable("SELECT Name,Value FROM SketchSettings")
            Dim SettingsNode = <settings type="SketchSettings"></settings>
            For Each dataRow In sketchDatatable.Rows
                Dim SketchSettingsNode = <SketchSettings/>
                SketchSettingsNode.@Name = dataRow(0).ToString
                SketchSettingsNode.@Value = dataRow(1).ToString
                SettingsNode.Add(SketchSettingsNode)
            Next
            xdoc.Root.Add(SettingsNode)
        End Sub
        Private Sub _exportTableProperties(ByRef xdoc As XDocument)
            Dim Datatable = target.GetDataTable("SELECT Name,disableDeletePCI,DoNotKeepCondition,AutoInsertProperties,YearPartitionField FROM DatasourceTable")
            Dim TablePropertiesNode = <settings type="TableProperties"></settings>
            For Each drow As DataRow In Datatable.Rows
                Dim rec = <TableProperties/>
                rec.@TableName = drow.GetString("Name")
                rec.@disableDeletePCI = drow.GetString("disableDeletePCI")
                rec.@DoNotKeepCondition = drow.GetString("DoNotKeepCondition")
                rec.@AutoInsertProperties = drow.GetString("AutoInsertProperties")
                rec.@YearPartitionField = drow.GetString("YearPartitionField")
                TablePropertiesNode.Add(rec)
            Next
            xdoc.Root.Add(TablePropertiesNode)
        End Sub
        Private Sub _exportApplicationSettings(ByRef xdoc As XDocument)
            Dim db As Database = target
            Dim AppSettings = <settings type="ApplicationSettings"></settings>
            Dim rec As XElement
            Dim dtSettings As DataTable = db.GetDataTable("SELECT ParcelTable, NeighborhoodTable, KeyField1, KeyField2, KeyField3, KeyField4, KeyField5, KeyField6, KeyField7, KeyField8, KeyField9, KeyField10, NeighborhoodField, NeighborhoodNameField, BPPCondition, BPPParentPropertyField, StreetAddressTable, StreetAddressField, StreetNumberField, StreetNameField, StreetNameSuffixField,StreetUnitField, StreetAddressFilter, Alternatekeyfieldvalue, ShowAlternateField, ShowKeyValue1 FROM Application")
            If dtSettings.Rows.Count > 0 Then
                For Each drow As DataRow In dtSettings.Rows
                    rec = <ApplicationSettings/>
                    rec.@Name = "ParcelTable"
                    rec.@Value = drow.GetString("ParcelTable")
                    AppSettings.Add(rec)

                    rec = <ApplicationSettings/>
                    rec.@Name = "NeighborhoodTable"
                    rec.@Value = drow.GetString("NeighborhoodTable")
                    AppSettings.Add(rec)
                    For i = 1 To 10
                        rec = <ApplicationSettings/>
                        Dim key = "KeyField" + i.ToString()
                        Dim val As String = target.Application.PropertyValue(key)
                        If Not String.IsNullOrEmpty(val) Then
                            rec.@Name = key
                            rec.@Value = val
                            AppSettings.Add(rec)
                        End If
                    Next

                    Dim AppSettingsLists() As String = {"Alternatekeyfieldvalue", "ShowAlternateField", "ShowKeyValue1", "NeighborhoodField", "NeighborhoodNameField", "StreetAddressTable", "StreetAddressField", "StreetNumberField", "StreetNameField", "StreetNameSuffixField", "StreetUnitField"}
                    For Each AppList As String In AppSettingsLists
                        rec = <ApplicationSettings/>
                        rec.@Name = AppList
                        rec.@Value = drow.GetString(AppList)
                        AppSettings.Add(rec)
                    Next

                    Dim FColName As String = "", FColCond As String = "Equal To", FColVal As String = ""
                    If drow.GetString("StreetAddressFilter") <> "" Then
                        Dim fltrParts() As String = drow.GetString("StreetAddressFilter").Split(" ")
                        FColName = fltrParts(0)
                        Dim fCond As String = fltrParts(1)
                        Select Case fCond
                            Case ">"
                                FColCond = "Greater Than"
                            Case "<"
                                FColCond = "Less Than"
                            Case Else
                                FColCond = "Equal To"
                        End Select
                        FColVal = fltrParts(2)
                        rec = <ApplicationSettings/>
                        rec.@Name = "FilterColumnName"
                        rec.@Value = FColName
                        AppSettings.Add(rec)
                        rec = <ApplicationSettings/>
                        rec.@Name = "FilterCondition"
                        rec.@Value = FColCond
                        AppSettings.Add(rec)
                        rec = <ApplicationSettings/>
                        rec.@Name = "FilterValue"
                        rec.@Value = FColVal
                        AppSettings.Add(rec)
                    End If

                    Dim dr As DataRow = Database.System.GetTopRow("SELECT * FROM OrganizationSettings WHERE OrganizationId = " & IIf(OrgId = -1, HttpContext.Current.GetCAMASession.OrganizationId, OrgId))
                    If (dr.GetBoolean("BPPEnabled")) Then
                        rec = <ApplicationSettings/>
                        rec.@Name = "BPPCondition"
                        rec.@Value = drow.GetString("BPPCondition")
                        AppSettings.Add(rec)

                        rec = <ApplicationSettings/>
                        rec.@Name = "BPPParentPropertyField"
                        rec.@Value = drow.GetString("BPPParentPropertyField")
                        AppSettings.Add(rec)
                    End If
                Next
            End If
            xdoc.Root.Add(AppSettings)
            Dim FieldTracking = <settings type="FieldTracking"></settings>
            Dim rec2 As XElement
            Dim field As DataTable = db.GetDataTable("select name,sourcetable,TrackingEnabledValue from datasourcefield where TrackingEnabledValue IS NOT NULL")
            For Each fRow As DataRow In field.Rows
                rec2 = <Field/>
                rec2.@FieldName = fRow.GetString("name")
                rec2.@TableName = fRow.GetString("sourcetable")
                rec2.@Value = fRow.GetString("TrackingEnabledValue")
                FieldTracking.Add(rec2)
            Next
            xdoc.Root.Add(FieldTracking)
        End Sub

        Public Sub _exportFieldCategory(ByRef xdoc As XDocument, Optional parentCategoryId As Integer = 0)
            Dim FieldRootSettings = <settings type="FieldCategory"></settings>
            Dim dtFields As DataTable = target.GetDataTable("SELECT * FROM DataSourceField WHERE CategoryId IS NOT NULL OR SourceTable = '_photo_meta_data' ORDER BY CategoryOrdinal")
            __exportFieldCategory(FieldRootSettings, dtFields)
            xdoc.Root.Add(FieldRootSettings)
        End Sub

        Public Sub __exportFieldCategory(ByRef node As XElement, dtFields As DataTable, Optional parentCategoryId As Integer = 0)
            Dim db As Database = target
            Dim filter As String = IIf(parentCategoryId = 0, "ParentCategoryId IS NULL", "ParentCategoryId = " & parentCategoryId)
            Dim categories As DataTable = db.GetDataTable("SELECT * FROM FieldCategory WHERE " + filter)
            Dim drFields As DataRow()
            If parentCategoryId = 0 Then
                Dim photoRow As DataRow = categories.NewRow()
                photoRow("Name") = "_photo_meta_data"
                photoRow("SourceTableName") = "_photo_meta_data"
                photoRow("Id") = -2
                categories.Rows.Add(photoRow)
            End If

            For Each cRow As DataRow In categories.Rows

                Dim xCategory = <FieldCategory></FieldCategory>
                Dim catId As String = cRow.GetString("Id")
                Dim sDescription As String = cRow.GetString("Description")
                Dim sHeaderInfo As String = cRow.GetString("HeaderInfo")
                Dim sGridHeader As String = cRow.GetString("GridHeader")
                xCategory.@Id = cRow.GetString("Id")
                xCategory.@Ordinal = cRow.GetString("Ordinal")
                xCategory.@Name = cRow.GetString("Name")
                xCategory.@TabularData = cRow.GetString("TabularData")

                xCategory.@SourceTableName = cRow.GetString("SourceTableName")
                xCategory.@ShowGridOnly = cRow.GetString("ShowGridOnly")
                xCategory.@FilterFields = cRow.GetString("FilterFields")
                If cRow.GetString("ShowCategory") <> "1" Then
                	xCategory.@Hidden = "True"
                Else 
                	xCategory.@Hidden = "False"
                End If
                xCategory.@SortExpression = cRow.GetString("SortExpression")
                xCategory.@AllowCopy = cRow.GetString("AllowCopy")
                xCategory.@AllowAddDelete = cRow.GetString("AllowAddDelete")
                xCategory.@AllowDeleteOnly = cRow.GetString("AllowDeleteOnly")
                xCategory.@IsReadOnly = cRow.GetString("IsReadOnly")
                xCategory.@AllowMassUpdate = cRow.GetString("AllowMassUpdate")
                xCategory.@DoNotAllowEditExistingRecords = cRow.GetString("DoNotAllowEditExistingRecords")
                xCategory.@UniqueKeys = cRow.GetString("UniqueKeys")
                xCategory.@MultiGridStub = cRow.GetString("MultiGridStub")
                xCategory.@MaxRecords = cRow.GetString("MaxRecords")
                xCategory.@HideExpression = cRow.GetString("HideExpression")
                xCategory.@DoNotAllowDeleteFirstRecord = cRow.GetString("DoNotAllowDeleteFirstRecord")
                xCategory.@MinRecords = cRow.GetString("MinRecords")
                xCategory.@DoNotShowInMultiGrid = cRow.GetString("DoNotShowInMultiGrid")
                xCategory.@DoNotAllowInsertDeleteExpression = cRow.GetString("DoNotAllowInsertDeleteExpression")
                xCategory.@IncludeParentFieldsOnInsert = cRow.GetString("IncludeParentFieldsOnInsert")
                xCategory.@ClassCalculatorParameters = cRow.GetString("ClassCalculatorParameters")
                xCategory.@ReadOnlyExpression = cRow.GetString("ReadOnlyExpression")
                xCategory.@EnableDeleteExpression = cRow.GetString("EnableDeleteExpression")
                xCategory.@UniqueFieldsInSiblings = cRow.GetString("UniqueFieldsInSiblings")
				xCategory.@UniqueSiblingExpression = cRow.GetString("UniqueSiblingExpression")
				xCategory.@UniqueSiblingKeys = cRow.GetString("UniqueSiblingKeys")
				xCategory.@PhotoTag = cRow.GetString("PhotoTag")
				xCategory.@PhotoLinkTableKey = cRow.GetString("PhotoLinkTableKey")
                xCategory.@DoNotDeleteOnHide = IIf(cRow.GetString("DoNotDeleteOnHide") = "", "True", cRow.GetString("DoNotDeleteOnHide"))
                xCategory.@DisplayType = cRow.GetString("DisplayType")
				xCategory.@EnableTemplateInsert = cRow.GetString("EnableTemplateInsert")
				xCategory.@OverrideParentCategory = cRow.GetString("OverrideParentCategory")
				xCategory.@CC_Note = cRow.GetString("CC_Note")
				xCategory.@DisableCopyRecordExpression = cRow.GetString("DisableCopyRecordExpression")
                xCategory.@DisableCopyRecord = cRow.GetString("DisableCopyRecord")
                xCategory.@MassUpdateDescendents = cRow.GetString("MassUpdateDescendents")
                xCategory.@MassUpdateDescendentFields = cRow.GetString("MassUpdateDescendentFields")
                If sDescription <> "" Then
                    Dim xDesc = <Description></Description>
                    xDesc.Value = sDescription
                    xCategory.Add(xDesc)
                End If

                If sHeaderInfo <> "" Then
                    Dim xHead = <HeaderInfo></HeaderInfo>
                    xHead.Add(New XCData(vbNewLine + sHeaderInfo.Trim + vbNewLine))
                    xCategory.Add(xHead)
                End If
                If sGridHeader <> "" Then
                    Dim xGHead = <GridHeader></GridHeader>
                    xGHead.Add(New XCData(vbNewLine + sGridHeader.Trim + vbNewLine))
                    xCategory.Add(xGHead)
                End If
                If cRow.GetString("Name") = "_photo_meta_data" Then
                    drFields = dtFields.Select("SourceTable = '_photo_meta_data'")
                    'dtFields = db.GetDataTable("SELECT * FROM DataSourceField WHERE SourceTable = '_photo_meta_data' ORDER BY CategoryOrdinal")
                Else
                    drFields = dtFields.Select("CategoryId = " & cRow.GetInteger("Id"))
                    'dtFields = db.GetDataTable("SELECT * FROM DataSourceField WHERE CategoryId = " & cRow.GetInteger("Id") & " ORDER BY CategoryOrdinal")
                End If
                For Each dsf As DataRow In drFields
                    Dim field = <Field/>
                    field.@CategoryOrdinal = dsf.GetInteger("CategoryOrdinal")
                    field.@SourceTable = dsf.GetString("SourceTable")
                    field.@Name = dsf.GetString("Name")
                    field.@IsReadOnly = dsf.GetString("IsReadOnly")
                    field.@DoNotShowOnDE = dsf.GetString("DoNotShowOnDE")
                    field.@DisplayLabel = dsf.GetString("DisplayLabel")
                    field.@GridLinkScript = dsf.GetString("GridLinkScript")
                    field.@AssignedName = dsf.GetString("AssignedName")
                    field.@CalculationExpression = dsf.GetString("CalculationExpression")
                    field.@ReadOnlyExpression = dsf.GetString("ReadOnlyExpression")
                    field.@VisibilityExpression = dsf.GetString("VisibilityExpression")
                    field.@RequiredExpression = dsf.GetString("RequiredExpression")
                    field.@CC_Note = dsf.GetString("CC_Note")
                    'field.@AutoSelectFirstitem = dsf.GetString("AutoSelectFirstitem")
                    field.@MustIncludeInDataCopy = dsf.GetString("MustIncludeInDataCopy")
                    field.@CalculationOverrideExpression = dsf.GetString("CalculationOverrideExpression")
                    field.@DefaultValue = dsf.GetString("DefaultValue")
                    If dsf.GetString("DataType") = "5" Or dsf.GetString("DataType") = "11" Then
                        field.@LookupTable = dsf.GetString("LookupTable")
                        If field.@LookupTable <> "$QUERY$" Then
                            If db.GetIntegerValue("SELECT COUNT(*) FROM ParcelDataLookup WHERE LookupName = {0} AND IdValue= {1}".SqlFormatString(field.@LookupTable, field.@DefaultValue)) = 0 Then
                                field.@DefaultValue = ""
                            End If
                        End If
                    Else
                        field.@LookupTable = ""
                        field.@LookupQuery = ""
                        field.@LookupQueryForCache = ""
                    End If

                    field.@IsFavorite = dsf.GetString("IsFavorite")
                    field.@FavoriteOrdinal = dsf.GetString("FavoriteOrdinal")
                    field.@DataType = dsf.GetString("DataType")
                    field.@IsRequired = dsf.GetBoolean("IsRequired")
                    field.@ShowOnGrid = dsf.GetBoolean("ShowOnGrid")

                    field.@SortType = If(dsf.GetString("SortType"), "0")
                    field.@IsMassUpdateField = IIf(dsf.GetString("IsMassUpdateField") = "", False, dsf.GetString("IsMassUpdateField"))
                    field.@IsUniqueInSiblings = dsf.GetBoolean("IsUniqueInSiblings")
                    field.@LookupShowNullValue = dsf.GetBoolean("LookupShowNullValue")
                    field.@LookupQuery = dsf.GetString("LookupQuery")
                    field.@AutoSelectFirstItem = dsf.GetBoolean("AutoSelectFirstItem")
                    field.@AutoNumber = dsf.GetBoolean("autoNumber")
                    field.@MustIncludeInDataCopy = dsf.GetBoolean("MustIncludeInDataCopy")
                    field.@AliasSource = dsf.GetString("AliasSource")
                    field.@DestinationNullReplacement = dsf.GetString("DestinationNullReplacement")
                    field.@DoNotInsertOrUpdateAtDestination = dsf.GetBoolean("DoNotInsertOrUpdateAtDestination")
                    field.@DoNotShowOnDTR = dsf.GetBoolean("DoNotShowOnDTR")
                    field.@MetaDataSourceTable = dsf.GetString("MetaDataSourceTable")
                    field.@IsRequiredDTROnly = dsf.GetString("IsRequiredDTROnly")
                    field.@IsHeatmap = dsf.GetString("isHeatmap")
                    field.@UpdateSiblings = dsf.GetString("UpdateSiblings")
                    field.@RadioFieldValues = dsf.GetString("RadioFieldValues")
                    field.@DoNotAllowSelectNull = dsf.GetBoolean("DoNotAllowSelectNull")
                    field.@IsLargeLookup = dsf.GetBoolean("IsLargeLookup")
'                    field.@MaxLength = dsf.GetString("MaxLength")
'                    If dsf.GetInteger("Precision") > 0 And dsf.GetInteger("NumericScale") = 0 Then
'                    	field.@NumericScale = dsf.GetString("Precision")
'                    Else
'                    	field.@NumericScale = dsf.GetString("NumericScale")
'                    End If
'                    field.@NumericPrecision = dsf.GetString("NumericPrecision")
                    
                    field.@DoNotIncludeInAuditTrail = dsf.GetString("DoNotIncludeInAuditTrail")
                    field.@IsClassCalculatorAttribute = dsf.GetString("IsClassCalculatorAttribute")
                    field.@RequiredIfRecordEdited = dsf.GetString("RequiredIfRecordEdited")
                    field.@UIProperties = dsf.GetString("UIProperties")
                    field.@LookupTrimData = dsf.GetBoolean("LookupTrimData")
                    field.@LookupTrimListData = dsf.GetBoolean("LookupTrimListData")
                    field.@DoNotEditFirstRecord = dsf.GetBoolean("DoNotEditFirstRecord")
                    field.@LookupQueryFilter = dsf.GetString("LookupQueryFilter")
                    field.@ConditionalValidationConfig = dsf.GetString("ConditionalValidationConfig")
                    field.@IsCustomInputType = dsf.GetBoolean("CustomInputType")
                    field.@AllowRadioNull = dsf.GetBoolean("AllowRadioNull")
                    field.@MapToSourceTable = dsf.GetString("MapToSourceTable")
                    field.@IncludeInSearchFilter = dsf.GetString("IncludeInSearchFilter")
                    field.@LookupQueryForCache = dsf.GetString("LookupQueryForCache")
                    field.@LookupQueryCustomFields = dsf.GetString("LookupQueryCustomFields")
                    field.@AllowTextInput = dsf.GetBoolean("AllowTextInput")
                    field.@DoNotAllowSpecialCharacters = dsf.GetBoolean("DoNotAllowSpecialCharacters")
                    field.@DoNotAllowEditExistingValue = dsf.GetString("DoNotAllowEditExistingValue")
                    Dim infoContent As String = dsf.GetString("InfoContent").Replace("&lt;", "<").Replace("&gt;", ">")
                    '  field.@CalculationOverrideExpression = dsf.GetBoolean("CalculationOverrideExpression")

                    If infoContent <> "" Then
                        Dim xInfo = <InfoContent></InfoContent>
                        xInfo.Add(New XCData(vbNewLine + infoContent.Trim + vbNewLine))
                        field.Add(xInfo)
                    End If
                    Dim dtfieldSettings As DataTable = Database.Tenant.GetDataTable("SELECT * FROM DataSourceFieldProperties Where FieldId={0}".SqlFormatString(dsf.GetString("Id")))
                    For Each dfield As DataRow In dtfieldSettings.Rows
                        Dim fieldsettings = <UI_Property></UI_Property>
                        'Dim fieldsettings = <UI_Property/>
                        fieldsettings.@PropertyName = dfield.GetString("PropertyName")
                        fieldsettings.@Value = dfield.GetString("Value")
                        field.Add(fieldsettings)
                    Next
                    xCategory.Add(field)
                Next
                Dim dtCategorySettings As DataTable = Database.Tenant.GetDataTable("SELECT * FROM FieldCategoryProperties Where CategoryId={0}".SqlFormatString(catId))
                For Each catProp As DataRow In dtCategorySettings.Rows
                    Dim CatSettings = <UI_CategoryProperty></UI_CategoryProperty>
                    CatSettings.@PropertyName = catProp.GetString("PropertyName")
                    CatSettings.@Value = catProp.GetString("Value")
                    xCategory.Add(CatSettings)
                Next

                __exportFieldCategory(xCategory, dtFields, catId)
                node.Add(xCategory)
            Next
        End Sub

        Public Sub _exportGISSettings(ByRef xdoc As XDocument)
            Dim db As Database = target
            Dim xElement = <settings type="GISSettings"></settings>
            Dim dtSettings As DataTable = db.GetDataTable("SELECT Name,NameField,CustomField1,CustomField2,CustomField3 FROM GIS_ObjectLayer")
            If dtSettings.Rows.Count > 0 Then
                For Each dr As DataRow In dtSettings.Rows
                    Dim rec = <GISSettings/>
                    rec.@Name = dr.GetString("Name")
                    rec.@NameField = dr.GetString("NameField")
                    rec.@CustomField1 = dr.GetString("CustomField1")
                    rec.@CustomField2 = dr.GetString("CustomField2")
                    rec.@CustomField3 = dr.GetString("CustomField3")
                    xElement.Add(rec)
                Next
            End If
            xdoc.Root.Add(xElement)
        End Sub

        Private Sub _exportClientValidation(ByRef xdoc As XDocument)
            Dim db As Database = target
            Dim xElement = <settings type="ClientValidation"></settings>
            Dim dtSettings As DataTable = db.GetDataTable("SELECT * FROM ClientValidation")
            If dtSettings.Rows.Count > 0 Then
                For Each dr As DataRow In dtSettings.Rows
                    Dim rec = <ClientValidation/>
                    rec.@Name = dr.GetString("Name")
                    If dr.GetString("Condition") <> "" Then
                        Dim xHead = <Condition></Condition>
                        xHead.Add(New XCData(vbNewLine + dr.GetString("Condition").Trim + vbNewLine))
                        rec.Add(xHead)
                    End If
                    rec.@ErrorMessage = dr.GetString("ErrorMessage")
                    rec.@Enabled = dr.GetString("Enabled")
                    rec.@SourceTable = dr.GetString("SourceTable")
                    rec.@ValidateFirstOnly = dr.GetBoolean("ValidateFirstOnly")
                    rec.@IsSoftWarning = dr.GetBoolean("IsSoftWarning")
                    xElement.Add(rec)
                Next
            End If
            xdoc.Root.Add(xElement)
        End Sub

        Private Sub _exportAppraisalTypes(ByRef xdoc As XDocument)
            Dim db As Database = target
            Dim xElement = <settings type="AppraisalType"></settings>
            Dim dtSettings As DataTable = db.GetDataTable("SELECT * FROM  AppraisalType")
            If dtSettings.Rows.Count > 0 Then
                For Each dr As DataRow In dtSettings.Rows
                    Dim rec = <AppraisalType/>
                    rec.@Name = dr.GetString("Name")
                    rec.@Description = dr.GetString("Description")
                    rec.@Code = dr.GetString("Code")
                    xElement.Add(rec)
                Next
            End If
            xdoc.Root.Add(xElement)
        End Sub

        Private Sub _exportDTRStatusTypes(ByRef xdoc As XDocument)
            Dim db As Database = target
            Dim xElement = <settings type="DTRStatusTypes"></settings>
            Dim dtSettings As DataTable = db.GetDataTable("SELECT * FROM  DTR_StatusType")
            If dtSettings.Rows.Count > 0 Then
                For Each dr As DataRow In dtSettings.Rows
                    Dim rec = <DTRStatusType/>
                    rec.@Name = dr.GetString("Name")
                    rec.@Description = dr.GetString("Description")
                    rec.@Code = dr.GetString("Code")
                    rec.@DTRReadOnly = dr.GetString("DTRReadOnly")
                    rec.@DTREditor = dr.GetString("DTREditor")
                    rec.@DTRManager = dr.GetString("DTRManager")
                    rec.@GIS_Flag_Color = dr.GetString("GIS_Flag_Color")
                    xElement.Add(rec)
                Next
            End If
            xdoc.Root.Add(xElement)
        End Sub

        Private Sub _exportDTRSTasks(ByRef xdoc As XDocument)
            Dim db As Database = target
            Dim xElement = <settings type="DTRTasks"></settings>
            Dim dtSettings As DataTable = db.GetDataTable("SELECT * FROM  DTR_Tasks")
            If dtSettings.Rows.Count > 0 Then
                For Each dr As DataRow In dtSettings.Rows
                    Dim rec = <DTRTask/>
                    rec.@Name = dr.GetString("Name")
                    Dim cFT = <FilterData/>
                    cFT.Value = dr.GetString("FilterData")
                    rec.Add(cFT)
                    Dim cCF = <CompiledSQLFilter/>
                    cCF.Value = dr.GetString("CompiledSQLFilter")
                    rec.Add(cCF)
                    xElement.Add(rec)
                Next
            End If
            xdoc.Root.Add(xElement)
        End Sub

        Private Sub _exportFieldAlertType(ByRef xdoc As XDocument)
            Dim db As Database = target
            Dim xElement = <settings type="FieldAlertTypes"></settings>
            Dim dtSettings As DataTable = db.GetDataTable("SELECT * FROM  FieldAlertTypes")
            If dtSettings.Rows.Count > 0 Then
                For Each dr As DataRow In dtSettings.Rows
                    Dim rec = <FieldAlertTypes/>
                    rec.@Name = dr.GetString("Name")
                    xElement.Add(rec)
                Next
            End If
            xdoc.Root.Add(xElement)
        End Sub

        Private Sub _exportClientTemplates(ByRef xdoc As XDocument)
            Dim db As Database = target
            Dim xElement = <settings type="ClientTemplates"></settings>
            Dim dtSettings As DataTable = db.GetDataTable("SELECT * FROM  ClientTemplates")
            If dtSettings.Rows.Count > 0 Then
                For Each dr As DataRow In dtSettings.Rows
                    Dim rec = <ClientTemplates/>
                    rec.@Name = dr.GetString("Name")
                    If dr.GetString("TemplateContent") <> "" Then
                        Dim xHead = <TemplateContent></TemplateContent>
                        xHead.Add(New XCData(vbNewLine + dr.GetString("TemplateContent") + vbNewLine))
                        rec.Add(xHead)
                    End If
                    xElement.Add(rec)
                Next
            End If

            xdoc.Root.Add(xElement)
        End Sub

        Private Sub _exportClientCustomAction(ByRef xdoc As XDocument)
            Dim db As Database = target
            Dim xElement = <settings type="ClientCustomAction"></settings>
            Dim dtSettings As DataTable = db.GetDataTable("SELECT * FROM ClientCustomAction ORDER BY Ordinal, Id")
            If dtSettings.Rows.Count > 0 Then
                For Each drow As DataRow In dtSettings.Rows
                    Dim rec = <ClientCustomAction/>
                    rec.@Id = drow.GetString("Id")
                    rec.@Action = drow.GetString("Action")
                    rec.@TableName = drow.GetString("TableName")
                    rec.@FieldName = drow.GetString("FieldName")
                    Dim fieldId As Integer = 0
                    If Integer.TryParse(drow.GetString("ValueTag"), fieldId) Then
                        rec.@ReferenceField = db.GetStringValue("SELECT SourceTable + '.' + Name As TableField FROM DataSourceField WHERE Id = " & fieldId)
                        rec.@ValueTag = "OtherField"
                    Else
                        rec.@ValueTag = drow.GetString("ValueTag")
                    End If
                    rec.@CustomText = drow.GetString("CustomText")
                    rec.@Enabled = drow.GetBoolean("Enabled")
                    xElement.Add(rec)
                Next
            End If
            xdoc.Root.Add(xElement)
        End Sub

        Private Sub _exportTableAction(ByRef xdoc As XDocument)
            Dim db As Database = target
            Dim xElement = <settings type="DataSourceTableAction"></settings>
            Dim dtSettings As DataTable = db.GetDataTable("SELECT dta.*, t.Name As TableName FROM DataSourceTableAction dta INNER JOIN DataSourceTable t ON dta.TableId = t.Id ORDER BY dta.Ordinal, dta.Id")
            If dtSettings.Rows.Count > 0 Then
                For Each drow As DataRow In dtSettings.Rows
                    Dim rec = <DataSourceTableAction/>
                    rec.@Action = drow.GetString("Action")
                    rec.@TableName = drow.GetString("TableName")
                    rec.@FieldName = drow.GetString("FieldName")
                    rec.@ValueTag = drow.GetString("ValueTag")
                    xElement.Add(rec)
                Next
            End If
            xdoc.Root.Add(xElement)
        End Sub

        Public Sub _exportCSEFieldCategory(ByRef xdoc As XDocument, Optional parentCategoryId As Integer = 0)
            Dim FieldRootSettings = <settings type="CSEFieldCategory"></settings>
            __exportCSEFieldCategory(FieldRootSettings)
            xdoc.Root.Add(FieldRootSettings)
        End Sub
        Public Sub __exportCSEFieldCategory(ByRef node As XElement, Optional parentCategoryId As Integer = 0)
            Dim db As Database = target
            ' Dim filter As String = IIf(parentCategoryId = 0, "ParentCategoryId IS NULL", "ParentCategoryId = " & parentCategoryId)
            Dim categories As DataTable = db.GetDataTable("SELECT * FROM cse.FieldCategory ")
            Dim dtFields As DataTable
            For Each cRow As DataRow In categories.Rows
                Dim xCategory = <CSEFieldCategory></CSEFieldCategory>
                Dim catId As String = cRow.GetString("Id")
                xCategory.@Ordinal = cRow.GetString("Ordinal")
                xCategory.@Name = cRow.GetString("Category")
                dtFields = db.GetDataTable("SELECT * FROM cse.ComparableFields WHERE CategoryId = " & catId & " ORDER BY Ordinal")

                For Each dsf As DataRow In dtFields.Rows
                    Dim field = <Field/>
                    field.@FieldId = dsf.GetInteger("FieldId")
                    field.@Label = dsf.GetString("Label")
                    field.@DataFormatName = dsf.GetString("DataFormat")
                    field.@SpecialQuery = dsf.GetString("SpecialQuery")
                    field.@OutputType = dsf.GetString("OutputType")
                    field.@AuxFilterField = dsf.GetString("AuxFilterField")
                    field.@AuxFilterValue = dsf.GetString("AuxFilterValue")
                    field.@Ordinal = dsf.GetString("Ordinal")
                    field.@FieldType = dsf.GetString("FieldType")
                    field.@CalculationExpression = dsf.GetString("CalculationExpression")
                    field.@FilterQuery = dsf.GetString("FilterQuery")
                    field.@Hidden = If(dsf.GetBoolean("Hidden"), "True", "False")
                    xCategory.Add(field)
                Next
                '  __exportCSEFieldCategory(xCategory, catId)
                node.Add(xCategory)
            Next
            Dim CalcCat = <CSEFieldCategory></CSEFieldCategory>
            CalcCat.@Name = "CC_FooterFields"
            dtFields = db.GetDataTable("SELECT * FROM cse.ComparableFields WHERE CategoryId IS NULL")
            For Each dsf As DataRow In dtFields.Rows
                Dim field = <Field/>
                field.@FieldId = dsf.GetInteger("FieldId")
                field.@Label = dsf.GetString("Label")
                field.@DataFormatName = dsf.GetString("DataFormat")
                field.@SpecialQuery = dsf.GetString("SpecialQuery")
                field.@OutputType = dsf.GetString("OutputType")
                field.@AuxFilterField = dsf.GetString("AuxFilterField")
                field.@AuxFilterValue = dsf.GetString("AuxFilterValue")
                field.@Ordinal = dsf.GetString("Ordinal")
                field.@FieldType = dsf.GetString("FieldType")
                field.@CalculationExpression = dsf.GetString("CalculationExpression")
                field.@FilterQuery = dsf.GetString("FilterQuery")
                field.@Hidden = If(dsf.GetBoolean("Hidden"), "True", "False")
                CalcCat.Add(field)
            Next
            node.Add(CalcCat)
        End Sub
        Public Sub _exportUI_HeatMap(ByRef xdoc As XDocument, Optional parentCategoryId As Integer = 0)
            Dim db As Database = target
            Dim xElement = <settings type="UI_HeatMap"></settings>
            Dim dtSettings As DataTable = db.GetDataTable("SELECT * FROM UI_HeatMap")
            If dtSettings.Rows.Count > 0 Then
                For Each drow As DataRow In dtSettings.Rows
                    Dim rec = <UI_HeatMap/>
                    rec.@Id = drow.GetString("Id")
                    rec.@FieldId = drow.GetString("FieldId")
                    rec.@ComparisonType = drow.GetString("ComparisonType")
                    rec.@IsAggregate = drow.GetString("IsAggregate")
                    xElement.Add(rec)
                Next
            End If
            xdoc.Root.Add(xElement)
        End Sub
        Public Sub _exportUI_HeatMapLookup(ByRef xdoc As XDocument, Optional parentCategoryId As Integer = 0)
            Dim db As Database = target
            Dim xElement = <settings type="UI_HeatMapLookup"></settings>
            Dim dtSettings As DataTable = db.GetDataTable("SELECT * FROM UI_HeatMapLookup")
            If dtSettings.Rows.Count > 0 Then
                For Each drow As DataRow In dtSettings.Rows
                    Dim rec = <UI_HeatMapLookup/>
                    rec.@RowUID = drow.GetString("RowUID")
                    rec.@HeatMapID = drow.GetString("HeatMapID")
                    rec.@Value1 = drow.GetString("Value1")
                    rec.@Value2 = drow.GetString("Value2")
                    rec.@ColorCode = drow.GetString("ColorCode")
                    xElement.Add(rec)
                Next
            End If
            xdoc.Root.Add(xElement)
        End Sub

        Public Sub _exportHeatMapsettings(ByRef xdoc As XDocument, Optional parentCategoryId As Integer = 0)
            Dim db As Database = target
            Dim xElement = <settings type="UI_HeatMap"></settings>
            Dim dtSettings As DataTable = db.GetDataTable("SELECT uh.*,df.SourceTable,df.Name FROM UI_HeatMap uh inner join DatasourceField df on df.Id=uh.FieldId where IsAggregate=0")
            If dtSettings.Rows.Count > 0 Then
                For Each drow As DataRow In dtSettings.Rows
                    Dim rec = <UI_HeatMap/>
                    rec.@Id = drow.GetString("Id")
                    rec.@FieldId = drow.GetString("FieldId")
                    rec.@FieldName = drow.GetString("Name")
                    rec.@SourceTable = drow.GetString("SourceTable")
                    rec.@ComparisonType = drow.GetString("ComparisonType")
                    rec.@IsAggregate = drow.GetString("IsAggregate")
                    xElement.Add(rec)
                Next
            End If
            dtSettings = db.GetDataTable("SELECT * FROM UI_HeatMap where IsAggregate=1")
            If dtSettings.Rows.Count > 0 Then
                For Each drow As DataRow In dtSettings.Rows
                    Dim rec = <UI_HeatMap/>
                    rec.@Id = drow.GetString("Id")
                    rec.@FieldId = drow.GetString("FieldId")
                    rec.@FieldName = "Null"
                    rec.@SourceTable = "Null"
                    rec.@ComparisonType = drow.GetString("ComparisonType")
                    rec.@IsAggregate = drow.GetString("IsAggregate")
                    xElement.Add(rec)
                Next

            End If
            xdoc.Root.Add(xElement)
            _exportUI_HeatMapLookup(xdoc)
            _exportAgggregateForHeatmap(xdoc)
        End Sub
        Private Sub _exportAgggregateForHeatmap(ByRef xdoc As XDocument)
            Dim db As Database = target
            Dim xElement = <settings type="AggregateFieldSettings"></settings>
            Dim dtAggregateSettings As DataTable = db.GetDataTable("SELECT af.* FROM AggregateFieldSettings af JOIN UI_HeatMap hm ON hm.FieldId =af.ROWUID WHERE hm.IsAggregate =1")
            If dtAggregateSettings.Rows.Count > 0 Then
                For Each drow As DataRow In dtAggregateSettings.Rows
                    Dim rec = <AggregateFieldSettings/>
                    rec.@ROWUID = drow.GetString("ROWUID")
                    rec.@TableName = drow.GetString("TableName")
                    rec.@FieldName = drow.GetString("FieldName")
                    rec.@FunctionName = drow.GetString("FunctionName")
                    rec.@IncludeInHeatMap = drow.GetString("IncludeInHeatMap")
                    rec.@Condition = drow.GetString("Condition")
                    xElement.Add(rec)
                Next
            End If
            xdoc.Root.Add(xElement)
        End Sub
        Public Sub _exportScreenMenu(ByRef xdoc As XDocument)
            Dim db As Database = target
            Dim xElement = <settings type="ScreenMenu"></settings>
            Dim dtSettings As DataTable = db.GetDataTable("SELECT * FROM ScreenMenu WHERE IsCustomized = 1")
            If dtSettings.Rows.Count > 0 Then
                For Each drow As DataRow In dtSettings.Rows
                    Dim rec = <ScreenMenu/>
                    rec.@Key = drow.GetString("Key")
                    rec.@Ordinal = drow.GetString("Ordinal")
                    rec.@ScreenId = drow.GetString("ScreenId")
                    rec.@Name = drow.GetString("Name")
                    rec.@Description = drow.GetString("Description")
                    rec.@Action = drow.GetString("Action")
                    rec.@Class = drow.GetString("Class")
                    rec.@ScreenName = drow.GetString("ScreenName")
                    rec.@HiddenCondition = drow.GetString("HiddenCondition")
                    xElement.Add(rec)
                Next
            End If
            xdoc.Root.Add(xElement)
        End Sub

        Public Sub _exportAggregateFieldSettings(ByRef xdoc As XDocument)
            Dim db As Database = target
            Dim xElement = <settings type="AggregateFieldSettings"></settings>
            Dim dtSettings As DataTable = db.GetDataTable("SELECT TableName, FieldName, FunctionName FROM AggregateFieldSettings")
            If dtSettings.Rows.Count > 0 Then
                For Each drow As DataRow In dtSettings.Rows
                    Dim rec = <AggregateFieldSettings/>
                    rec.@TableName = drow.GetString("TableName")
                    rec.@FieldName = drow.GetString("FieldName")
                    rec.@FunctionName = drow.GetString("FunctionName")
                    xElement.Add(rec)
                Next
            End If
            xdoc.Root.Add(xElement)
        End Sub

        Public Sub _exportMraTemplates(ByRef xdoc As XDocument)
            Dim db As Database = target
            Dim xElement = <settings type="MRATemplates"></settings>
            Dim dtSettings As DataTable = db.GetDataTable("SELECT ROW_NUMBER() OVER (ORDER BY t.Name) As Ordinal, t.* FROM MRA_Templates t INNER JOIN DataSourceField f ON t.OutputField = f.Id ORDER BY t.Name")
            If dtSettings.Rows.Count > 0 Then
                For Each drow As DataRow In dtSettings.Rows
                    Dim rec = <MRATemplates/>
                    rec.@ID = drow.GetString("ID")
                    rec.@Name = drow.GetString("Name")
                    rec.@OutputField = drow.GetString("OutputField")
                    rec.@InputFields = drow.GetString("InputFields")
                    rec.@Filters = drow.GetString("Filters")
                    rec.@CreatedBy = drow.GetString("CreatedBy")
                    rec.@CreatedDate = drow.GetString("CreatedDate")
                    rec.@PoolSize = drow.GetString("PoolSize")
                    rec.@LastExecutionTime = drow.GetString("LastExecutionTime")
                    rec.@CostFormula = drow.GetString("CostFormula")
                    rec.@CostFormulaSQL = drow.GetString("CostFormulaSQL")
                    rec.@Groups = drow.GetString("Groups")
                    xElement.Add(rec)
                Next
            End If
            xdoc.Root.Add(xElement)
        End Sub

        Private Sub _exportRangeLookups(ByRef xdoc As XDocument)
            Dim db As Database = target
            Dim xElement = <settings type="RangeLookups"></settings>
            Dim dtSettings As DataTable = db.GetDataTable("SELECT * FROM RangeLookup ORDER BY GroupName,Id")
            If dtSettings.Rows.Count > 0 Then
                For Each drow As DataRow In dtSettings.Rows
                    Dim rec = <RangeLookups/>
                    rec.@GroupName = drow.GetString("GroupName")
                    rec.@ValueFrom = drow.GetString("ValueFrom")
                    rec.@ValueTo = drow.GetString("ValueTo")
                    rec.@RangeValue = drow.GetString("RangeValue")
                    rec.@IncrementFactor = drow.GetString("IncrementFactor")
                    xElement.Add(rec)
                Next
            End If
            xdoc.Root.Add(xElement)
        End Sub

#End Region

#Region "Import Pivate Methods"

        Private Delegate Sub nodeProcessor(node As XElement)
        Private Delegate Sub nodeProcessorForFieldCategory(node As XElement, backupDoc As XElement, loginId As String)

        Private Sub processNodeWithNodeIsolation(doc As XDocument, settingsType As String, processor As nodeProcessor)
            If doc.Root.@type = settingsType Then
                processor(doc.Root)
            Else
                Dim node = doc.Root.Elements.Where(Function(x) x.@type = settingsType).FirstOrDefault()
                If node IsNot Nothing Then
                    processor(node)
                End If
            End If
        End Sub

        Private Sub processNodeWithNodeIsolationFieldCategory(doc As XDocument, settingsType As String, processor As nodeProcessorForFieldCategory, backupDoc As XDocument, loginId As String)
            Dim node = doc.Root.Elements.Where(Function(x) x.@type = settingsType).FirstOrDefault()
            Dim backupDocument = backupDoc.Root.Elements.Where(Function(x) x.@type = settingsType).FirstOrDefault()
            If node IsNot Nothing Then
                processor(node, backupDocument, loginId)
            End If
        End Sub
        '
        Private Sub _importClientTemplates(node As XElement)
            Dim db As Database = target

            db.Execute("TRUNCATE TABLE ClientTemplates; ")
            For Each rowItem In node.Descendants("ClientTemplates")
                Dim templateNode = rowItem.Descendants("TemplateContent")
                If templateNode.Count > 0 Then
                    Dim sqlFCTemplate As String = "INSERT INTO ClientTemplates (Name, TemplateContent) VALUES ({0}, {1})"
                    db.Execute(sqlFCTemplate.SqlFormat(True, rowItem.@Name, templateNode.First().Value()))
                End If
            Next
        End Sub

        Private Sub _importClientCustomAction(node As XElement)
            Dim db As Database = target
            db.Execute("TRUNCATE TABLE ClientCustomAction; ")
            Dim i = 10
            For Each rowItem In node.Descendants("ClientCustomAction")
                Dim valueTag As String = rowItem.@ValueTag
                If rowItem.@ValueTag = "OtherField" Then
                    valueTag = db.GetIntegerValue("SELECT Id FROM DataSourceField WHERE SourceTable + '.' + Name = " + rowItem.@ReferenceField.ToString.ToSqlValue)
                End If
                Dim sqlFCTemplate As String = "INSERT INTO ClientCustomAction (Action, TableName, FieldName, ValueTag,Ordinal,CustomText,Enabled) VALUES ({0},{1},{2},{3},{4},{5},{6})"
                db.Execute(sqlFCTemplate.SqlFormat(True, rowItem.@Action, rowItem.@TableName, rowItem.@FieldName, valueTag, i, rowItem.@CustomText, rowItem.@Enabled))
                i = i + 10
            Next
        End Sub

        Private Sub _importTableAction(node As XElement)
            Dim db As Database = target
            db.Execute("TRUNCATE TABLE DataSourceTableAction; ")
            Dim i = 10
            For Each rowItem In node.Descendants("DataSourceTableAction")
                Dim valueTag As String = rowItem.@ValueTag
                Dim tableName = rowItem.@TableName
                Dim tableId = db.GetIntegerValue("SELECT Id FROM DataSourceTable WHERE Name = " + tableName.ToSqlValue)
                Dim sqlFCTemplate As String = "INSERT INTO DataSourceTableAction (Action, TableId, FieldName, ValueTag,Ordinal) VALUES ({0}, {1}, {2}, {3},{4})"
                db.Execute(sqlFCTemplate.SqlFormat(True, rowItem.@Action, tableId, rowItem.@FieldName, valueTag, i))
                i = i + 10
            Next
        End Sub

        Private Sub _importClientValidation(node As XElement)
            Dim db As Database = target
            db.Execute("TRUNCATE TABLE ClientValidation; ")
            Dim i = 10
            For Each rowItem In node.Descendants("ClientValidation")
                Dim validateFirstOnly = If(rowItem.@ValidateFirstOnly, False)
                Dim IsSoftWarning = If(rowItem.@IsSoftWarning, False)
                Dim ConditionNode = rowItem.Descendants("Condition")
                Dim sqlFCTemplate As String = "INSERT INTO ClientValidation (Name, Condition, ErrorMessage, Ordinal,Enabled,SourceTable,ValidateFirstOnly,IsSoftWarning) VALUES ({0}, {1}, {2}, {3},{4},{5},{6},{7})"
                db.Execute(sqlFCTemplate.SqlFormat(True, rowItem.@Name, ConditionNode.First().Value(), rowItem.@ErrorMessage, i, rowItem.@Enabled, rowItem.@SourceTable, validateFirstOnly, IsSoftWarning))
                i = i + 10
            Next
        End Sub

        Private Sub _importAppraisalType(node As XElement)
            Dim db As Database = target
            Dim items As New List(Of Object())()
            db.Execute("TRUNCATE TABLE AppraisalType; ")
            For Each rowItem In node.Descendants("AppraisalType")
                If Not items.Any(Function(item) item(0).ToString() = rowItem.@Name OrElse item(1).ToString() = rowItem.@Description) AndAlso rowItem.@Name <> "" AndAlso Regex.IsMatch(rowItem.@Name, "^[a-zA-Z0-9_]*$") AndAlso rowItem.@Description <> "" AndAlso Regex.IsMatch(rowItem.@Description, "[a-zA-Z0-9]") Then
                    Dim sqlFCTemplate As String = "INSERT INTO AppraisalType (Name, Description,Code) VALUES ({0}, {1}, {2})"
                    db.Execute(sqlFCTemplate.SqlFormat(True, rowItem.@Name, rowItem.@Description, rowItem.@Code))
                    items.Add({rowItem.@Name, rowItem.@Description})
                End If
            Next
        End Sub

        Private Sub _importFieldAlertTypes(node As XElement)
            Dim db As Database = target
            'db.Execute("TRUNCATE TABLE FieldAlertTypes; ")
            Dim dt As DataTable = db.GetDataTable("SELECT [Name] FROM FieldAlertTypes")
            Dim sqlFCTemplate As String = ""
            For Each rowItem In node.Descendants("FieldAlertTypes")
                Dim resultRows() As DataRow = dt.Select($"Name = '{rowItem.@Name}'")
                If resultRows.Length > 0 Then
                    dt.Rows.Remove(resultRows(0))
                Else
                    sqlFCTemplate += " INSERT INTO FieldAlertTypes (Name) VALUES ({0});".SqlFormat(True, rowItem.@Name)
                End If
            Next
            If dt.Rows.Count > 0 Then
                For Each row As DataRow In dt.Rows
                    sqlFCTemplate += " DELETE FROM FieldAlertTypes WHERE [Name] = {0};".SqlFormat(True, row("Name"))
                Next
            End If

            If sqlFCTemplate <> "" Then
                db.Execute(sqlFCTemplate)
            End If
        End Sub

        Private Sub _importGISSettings(node As XElement)
            Dim db As Database = target
            Dim idVal As String
            db.Execute("TRUNCATE TABLE GIS_ObjectLayer; ")
            For Each rowItem In node.Descendants("GISSettings")
                If rowItem.@Name.Equals("Parcel") Then
                    idVal = 1
                ElseIf rowItem.@Name.Equals("Street") Then
                    idVal = 2
                ElseIf rowItem.@Name.Equals("Neighborhood") Then
                    idVal = 3
                Else
                    idVal = 9
                End If
                db.Execute("INSERT INTO GIS_ObjectLayer (Id,Name,CustomField1,CustomField2,CustomField3) VALUES (" + idVal.ToSqlValue + "," + rowItem.@Name.ToSqlValue + "," + rowItem.@CustomField1.ToSqlValue + "," + rowItem.@CustomField2.ToSqlValue + "," + rowItem.@CustomField3.ToSqlValue + ")")
            Next
        End Sub
        Private Sub _importTableProperties(node As XElement)
            Dim db As Database = target
            For Each item In node.Descendants("TableProperties")
                Dim disableDeletePCI = "0"
                If item.@disableDeletePCI = "1" Or item.@disableDeletePCI.ToLower = "true" Then
                    disableDeletePCI = 1
                Else : disableDeletePCI = 0
                End If
                Dim doNotKeep As String = item.@DoNotKeepCondition
                Dim AutoInsertProperties As String = item.@AutoInsertProperties
                Dim YearPartitionField = item.@YearPartitionField
                Dim sqlFCTemplate As String = "UPDATE DataSourceTable set disableDeletePCI={0},DoNotKeepCondition={1},AutoInsertProperties={2},YearPartitionField ={3} where name ='" + item.@TableName + "'"
                db.Execute(sqlFCTemplate.SqlFormat(True, disableDeletePCI, doNotKeep, AutoInsertProperties, YearPartitionField))
            Next
        End Sub
        Private Sub _importClientSettings(node As XElement)
            Dim db As Database = target
            For Each item In node.Descendants("ClientSettings")
                Dim strVal As New StringBuilder
                strVal.Append(item.@Value)

                If strVal.ToString.Contains("'") Then
                    strVal.Replace("'", "''")
                End If
                Dim isRowExist As Boolean = db.GetIntegerValue("SELECT count(*) FROM  ClientSettings where name ='" + item.@Name + "'")
                If item.@Name = "UseOSM" Then
                    Dim o As DataRow = Database.System.GetTopRow("SELECT EnableAdvancedMap FROM OrganizationSettings WHERE OrganizationId = " & IIf(OrgId = -1, HttpContext.Current.GetCAMASession.OrganizationId, OrgId))
                    If (o.GetBoolean("EnableAdvancedMap")) Then
                        If isRowExist Then
                            db.Execute("Update ClientSettings set Value='" + strVal.ToString + "' where name ='" + item.@Name + "'")
                        Else
                            db.Execute("INSERT INTO ClientSettings (Name,Value) VALUES ('" + item.@Name + "' , '" + strVal.ToString + "')")
                        End If
                        If (strVal.ToString = "0") Then
                            db.Execute("DELETE FROM ClientSettings where name ='EnableEagleViewInMA'")
                            Database.System.Execute("UPDATE OrganizationSettings SET PictometryInMA=0  WHERE OrganizationId= " & IIf(OrgId = -1, HttpContext.Current.GetCAMASession.OrganizationId, OrgId))
                        End If
                    Else
                        db.Execute("DELETE FROM ClientSettings where name in('" + item.@Name + "','EnableEagleViewInMA')")
                        Database.System.Execute("UPDATE OrganizationSettings SET PictometryInMA=0  WHERE OrganizationId= " & IIf(OrgId = -1, HttpContext.Current.GetCAMASession.OrganizationId, OrgId))
                    End If
                    Continue For
                End If
                If item.@Name = "EnableEagleViewInMA" Then
                    Dim o As DataRow = Database.System.GetTopRow("SELECT PictometryInMA FROM OrganizationSettings WHERE OrganizationId = " & IIf(OrgId = -1, HttpContext.Current.GetCAMASession.OrganizationId, OrgId))
                    If (o.GetBoolean("PictometryInMA")) Then
                        If isRowExist Then
                            db.Execute("Update ClientSettings set Value='" + strVal.ToString + "' where name ='" + item.@Name + "'")
                        Else
                            db.Execute("INSERT INTO ClientSettings (Name,Value) VALUES ('" + item.@Name + "' , '" + strVal.ToString + "')")
                        End If
                    Else
                        db.Execute("DELETE FROM ClientSettings where name ='" + item.@Name + "'")
                    End If
                    Continue For
                End If
                If item.@Name = "EnableDisto" Then
                    Dim o As DataRow = Database.System.GetTopRow("SELECT DistoEnabled FROM OrganizationSettings WHERE OrganizationId = " & IIf(OrgId = -1, HttpContext.Current.GetCAMASession.OrganizationId, OrgId))
                    If (o.GetBoolean("DistoEnabled")) Then
                        If isRowExist Then
                            db.Execute("Update ClientSettings set Value='" + strVal.ToString + "' where name ='" + item.@Name + "'")
                        Else
                            db.Execute("INSERT INTO ClientSettings (Name,Value) VALUES ('" + item.@Name + "' , '" + strVal.ToString + "')")
                        End If
                    Else
                        db.Execute("DELETE FROM ClientSettings where name ='" + item.@Name + "'")
                    End If
                    Continue For
                End If

                If item.@Name = "EnableFieldTracking" Then
                    Dim o As DataRow = Database.System.GetTopRow("SELECT PCITrackingEnabled FROM OrganizationSettings WHERE OrganizationId = " & IIf(OrgId = -1, HttpContext.Current.GetCAMASession.OrganizationId, OrgId))
                    If (o.GetBoolean("PCITrackingEnabled")) Then
                        If isRowExist Then
                            db.Execute("Update ClientSettings set Value='" + strVal.ToString + "' where name ='" + item.@Name + "'")
                        Else
                            db.Execute("INSERT INTO ClientSettings (Name,Value) VALUES ('" + item.@Name + "' , '" + strVal.ToString + "')")
                        End If
                    Else
                        db.Execute("DELETE FROM ClientSettings where name ='" + item.@Name + "'")
                    End If
                    Continue For
                End If

                If isRowExist Then
                    db.Execute("Update ClientSettings set Value='" + strVal.ToString + "' where name ='" + item.@Name + "'")
                Else
                    db.Execute("INSERT INTO ClientSettings (Name,Value) VALUES ('" + item.@Name + "' , '" + strVal.ToString + "')")
                End If
                If item.@Name = "TimeZone" And isRowExist Then
                    ClientSettings.AlterDateFunctions("TimeZone", strVal.ToString, db)
                End If
                If item.@Name = "DST" And isRowExist Then
                    ClientSettings.AlterDateFunctions("DST", strVal.ToString, db)
                End If
            Next
        End Sub
        Private Sub _importSketchSettings(node As XElement)
            Dim db As Database = target
            For Each item In node.Descendants("SketchSettings")
                Dim strVal As New StringBuilder
                strVal.Append(item.@Value)

                If strVal.ToString.Contains("'") Then
                    strVal.Replace("'", "''")
                End If
                Dim isRowExist As Boolean = db.GetIntegerValue("SELECT count(*) FROM  SketchSettings where name ='" + item.@Name + "'")

                If isRowExist Then
                    db.Execute("Update SketchSettings set Value='" + strVal.ToString + "' where name ='" + item.@Name + "'")
                Else
                    db.Execute("INSERT INTO SketchSettings (Name,Value) VALUES ('" + item.@Name + "' , '" + strVal.ToString + "')")
                End If
            Next
        End Sub
        Private Sub _importFieldTracking(node As XElement)
            Dim ft = Database.Tenant.GetStringValue("select * from clientsettings where name='EnableFieldTracking'")
            If Not (ft = "1" Or ft = "true") Then
                For Each item In node.Descendants("Field")
                    Database.Tenant.Execute("UPDATE dsf SET dsf.TrackingEnabledValue = '" + item.@Value + "' FROM DataSourceField  dsf INNER JOIN DataSourceTable dst ON  dsf.TableId=dst.id WHERE dst.CC_targetTable='ParcelData' AND  dsf.Name = '" + item.@FieldName + "' AND dsf.SourceTable='" + item.@TableName + "'")
                Next
            Else
                Return
            End If
        End Sub
        Private Sub _importApplicationSettings(node As XElement)
            Dim db As Database = target
            Dim isRowExist As Boolean = db.GetIntegerValue("select count(*) from Application ")
            Dim updateColumns As New StringBuilder()
            Dim updateColumnValues As New StringBuilder()
            Dim FilterColumnName As String = "", FilterCondition As String = "", FilterValue As String = "", StreetAddressFilterValue As String = "''"
            For Each item In node.Descendants("ApplicationSettings")
                If item.@Name = "FilterColumnName" Then
                    FilterColumnName = item.@Value
                ElseIf item.@Name = "FilterCondition" Then
                    If item.@Value = "Greater Than" Or item.@Value = "Less Than" Or item.@Value = "Equal To" Then
                        Select Case item.@Value
                            Case "Greater Than"
                                FilterCondition = ">"
                            Case "Less Than"
                                FilterCondition = "<"
                            Case Else
                                FilterCondition = "="
                        End Select
                    End If
                ElseIf item.@Name = "FilterValue" Then
                    FilterValue = item.@Value
                Else
                    If Not isRowExist Then
                        If updateColumns.Length <> 0 Then
                            updateColumns.Append(", ")
                        End If
                        If updateColumnValues.Length <> 0 Then
                            updateColumnValues.Append(", ")
                        End If
                        updateColumns.Append(item.@Name)
                        updateColumnValues.Append(item.@Value.ToSqlValue)
                    Else
                        If item.@Name = "BPPParentPropertyField" OrElse item.@Name = "BPPCondition" Then
                            Dim dr As DataRow = Database.System.GetTopRow("SELECT * FROM OrganizationSettings WHERE OrganizationId = " & IIf(OrgId = -1, HttpContext.Current.GetCAMASession.OrganizationId, OrgId))
                            If Not (dr.GetBoolean("BPPEnabled")) Then
                                Continue For
                            End If
                        End If
                        If updateColumns.Length <> 0 Then
                            updateColumns.Append(", ")
                        End If
                        updateColumns.Append(item.@Name)
                        updateColumns.Append("=")
                        'updateColumns.Append(item.@Value.ToSqlValue)
                        If item.@Name = "Alternatekeyfieldvalue" And String.IsNullOrEmpty(item.@Value) Then
                            updateColumns.Append(item.@Value.ToSqlValue(True))
                        Else
                            updateColumns.Append(item.@Value.ToSqlValue)
                        End If
                    End If
                End If
            Next

            If FilterColumnName <> "" AndAlso FilterCondition <> "" AndAlso FilterValue <> "" Then
                StreetAddressFilterValue = FilterColumnName + " " + FilterCondition + " " + FilterValue
            End If

            If Not isRowExist Then
                If updateColumns.Length <> 0 Then
                    updateColumns.Append(", ")
                End If
                If updateColumnValues.Length <> 0 Then
                    updateColumnValues.Append(", ")
                End If
                updateColumns.Append("StreetAddressFilterValue")
                updateColumnValues.Append(StreetAddressFilterValue.ToSqlValue)
                db.Execute("INSERT INTO Application (" + updateColumns.ToString + " ) VALUES (" + updateColumns.ToString + ")")
            Else
                If updateColumns.Length <> 0 Then
                    updateColumns.Append(", ")
                End If
                updateColumns.Append("StreetAddressFilterValue")
                updateColumns.Append("=")
                updateColumns.Append(StreetAddressFilterValue.ToSqlValue)
                db.Execute("UPDATE Application SET " + updateColumns.ToString)
            End If

        End Sub
        Public Sub _importHeatmapSettings(doc As XDocument)
            Dim node = doc.Root.Elements.Where(Function(x) x.@type = "UI_HeatMap").FirstOrDefault()
            Dim fieldId As Integer
            If node IsNot Nothing Then
                Dim db As Database = target
                db.Execute("TRUNCATE TABLE UI_HeatMap; ")
                For Each rowItem In node.Descendants("UI_HeatMap")
                    If rowItem.@IsAggregate Then
                        fieldId = rowItem.@FieldId
                    Else
                        fieldId = Database.Tenant.GetIntegerValue("SELECT Id From DataSourceField WHERE Name={0} AND SourceTable={1}".SqlFormat(True, rowItem.@FieldName, rowItem.@SourceTable))
                    End If
                    Dim sqlFCTemplate As String = "SET IDENTITY_INSERT UI_HeatMap ON; INSERT INTO UI_HeatMap (Id,FieldId,ComparisonType,IsAggregate) VALUES ({0}, {1},{2},{3}) SET IDENTITY_INSERT UI_HeatMap OFF; "
                    db.Execute(sqlFCTemplate.SqlFormat(True, rowItem.@Id, fieldId, rowItem.@ComparisonType, rowItem.@IsAggregate))
                Next
            End If
            Dim node1 = doc.Root.Elements.Where(Function(x) x.@type = "UI_HeatMapLookup").FirstOrDefault()
            If node1 IsNot Nothing Then
                _importUI_HeatMapLookup(node1)
            End If
            Dim node2 = doc.Root.Elements.Where(Function(x) x.@type = "AggregateFieldSettings").FirstOrDefault()
            If node2 IsNot Nothing Then
                _aggregateForHeatMap(node2)
            End If
            If node Is Nothing And node1 Is Nothing And node2 Is Nothing Then
                Throw New Exception("Imported File does not contain Heatmap settings.")
            End If
        End Sub
        Private Sub _aggregateForHeatMap(node As XElement)
            Dim db As Database = target
            db.Execute("TRUNCATE TABLE AggregateFieldSettings; ")
            For Each rowItem In node.Descendants("AggregateFieldSettings")
                If Database.Tenant.GetTopRow("SELECT * FROM DatasourceField WHERE Name = {0} and SourceTable = {1}".SqlFormat(True, rowItem.@FieldName, rowItem.@TableName)) IsNot Nothing Then
                    Dim sqlFCTemplate As String = "SET IDENTITY_INSERT AggregateFieldSettings ON; INSERT INTO AggregateFieldSettings (ROWUID,TableName,FieldName,FunctionName,IncludeInHeatMap,Condition) VALUES ({0}, {1},{2},{3},{4},{5}) SET IDENTITY_INSERT AggregateFieldSettings OFF; "
                    db.Execute(sqlFCTemplate.SqlFormat(True, rowItem.@ROWUID, rowItem.@TableName, rowItem.@FieldName, rowItem.@FunctionName, rowItem.@IncludeInHeatMap, rowItem.@Condition))
                End If
            Next
        End Sub
        Private Sub _importUI_HeatMap(node As XElement)
            Dim db As Database = target
            db.Execute("TRUNCATE TABLE UI_HeatMap; ")
            For Each rowItem In node.Descendants("UI_HeatMap")
                Dim sqlFCTemplate As String = "INSERT INTO UI_HeatMap (FieldId,ComparisonType,IsAggregate) VALUES ({0}, {1},{2}) "
                db.Execute(sqlFCTemplate.SqlFormat(True, rowItem.@FieldId, rowItem.@ComparisonType, rowItem.@IsAggregate))
            Next
        End Sub
        Private Sub _importUI_HeatMapLookup(node As XElement)
            Dim db As Database = target
            db.Execute("TRUNCATE TABLE UI_HeatMapLookup; ")
            For Each rowItem In node.Descendants("UI_HeatMapLookup")
                Dim sqlFCTemplate As String = "INSERT INTO UI_HeatMapLookup (HeatMapID,Value1,Value2,ColorCode) VALUES ({0}, {1},{2},{3})"
                db.Execute(sqlFCTemplate.SqlFormat(True, rowItem.@HeatMapID, rowItem.@Value1, rowItem.@Value2, rowItem.@ColorCode))
            Next
        End Sub
        Private Sub _importScreenMenu(node As XElement)
            Dim db As Database = target
            db.Execute("DELETE FROM ScreenMenu WHERE IsCustomized = 1; ")
            For Each rowItem In node.Descendants("ScreenMenu")
                Dim sqlFCTemplate As String = "INSERT INTO ScreenMenu ([Key], Ordinal, ScreenId, Name, [Description], [Action], Class, ScreenName, IsCustomized,HiddenCondition) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, 1,{8})"
                db.Execute(sqlFCTemplate.SqlFormat(True, rowItem.@Key, rowItem.@Ordinal, rowItem.@ScreenId, rowItem.@Name, rowItem.@Description, rowItem.@Action, rowItem.@Class, rowItem.@ScreenName, rowItem.@HiddenCondition))
            Next
        End Sub
        Private Sub _importAggregateFields(node As XElement)
            Dim db As Database = target
            'db.Execute("DELETE FROM AggregateFieldSettings")
            For Each rowItem In node.Descendants("AggregateFieldSettings")
                If db.GetIntegerValue("SELECT COUNT(*) FROM AggregateFieldSettings WHERE TableName = {0} AND FieldName = {1} AND FunctionName = {2}".SqlFormat(True, rowItem.@TableName, rowItem.@FieldName, rowItem.@FunctionName)) = 0 Then
                    Dim sql As String = "INSERT INTO AggregateFieldSettings ([TableName],[FieldName],[FunctionName]) VALUES ({0}, {1}, {2})"
                    db.Execute(sql.SqlFormat(True, rowItem.@TableName, rowItem.@FieldName, rowItem.@FunctionName))
                End If
            Next
        End Sub
        Private Sub _importMraTemplates(node As XElement)
            Dim db As Database = target
            For Each rowItem In node.Descendants("MRATemplates")
                Dim dupCount As Integer = 0
                Dim currDate As String = DateAndTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
                dupCount = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM  MRA_Templates WHERE Name = {0}".SqlFormatString(rowItem.@Name))
                If dupCount = 0 Then
                    Dim sqlFCTemplate As String = "INSERT INTO MRA_Templates (Name,OutputField,InputFields,Filters,CreatedBy,CreatedDate,PoolSize,LastExecutionTime,CostFormula,CostFormulaSQL,Groups) VALUES ({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10})"
                    db.Execute(sqlFCTemplate.SqlFormat(True, rowItem.@Name, rowItem.@OutputField, rowItem.@InputFields, rowItem.@Filters, rowItem.@CreatedBy, currDate, rowItem.@PoolSize, rowItem.@LastExecutionTime, rowItem.@CostFormula, rowItem.@CostFormulaSQL, rowItem.@Groups))
                End If
            Next
            Dim count = db.GetIntegerValue("SELECT COUNT (*) FROM MRA_Templates")
            If count > 20 Then
                db.Execute("DELETE FROM MRA_Templates WHERE ID NOT IN (SELECT TOP 20 ID FROM MRA_Templates ORDER BY ID DESC)")
            End If
        End Sub

        Private Sub __importRangeLookup(node As XElement, db As Database)
            For Each rowItem In node.Descendants("RangeLookups")
                Dim sqlFCTemplate As String = "INSERT INTO RangeLookup (GroupName, ValueFrom,ValueTo,RangeValue,IncrementFactor) VALUES ({0}, {1},{2},{3},{4})"
                db.Execute(sqlFCTemplate.SqlFormat(True, rowItem.@GroupName, rowItem.@ValueFrom, rowItem.@ValueTo, rowItem.@RangeValue, rowItem.@IncrementFactor))
            Next
        End Sub

        Public Function ReadXml(
    fileName As String
    ) As XmlReadMode
        End Function

#Region "Field Category private methods"
        Private Sub _importCSEFieldCategory(node As XElement)
            Dim db As Database = target
            Try
                db.Execute("TRUNCATE TABLE cse.FieldCategory; ")
                db.Execute("TRUNCATE TABLE cse.ComparableFields ")
                _importCSECategory(node, db)
            Catch ex As Xml.XmlException
                Throw New Exception("The uploaded file contains invalid XML. Import process has been aborted.")
            Catch ex As Exception
                Throw New Exception("You have uploaded an invalid Field Category settings file - " + ex.Message, ex)
            End Try
        End Sub
        Private Sub _importCSECategory(node As XElement, db As Database, Optional parentCategoryId As Integer = 0)
            For Each cat In node.Elements("CSEFieldCategory")
                Dim catId = ""
                If (cat.@Name <> "CC_FooterFields") Then
                    Dim checkIfExist = db.GetIntegerValue("SELECT COUNT(*) from cse.FieldCategory where Category = {0}".SqlFormat(True, cat.@Name))
                    If checkIfExist > 0 Then
                        Throw New Exception("The category name already exists. Duplicate name is not allowed.")
                    End If
                    Dim sqlFCTemplate As String = "INSERT INTO cse.FieldCategory (Category,Ordinal) VALUES ({0}, {1}); SELECT CAST(@@IDENTITY AS INT)"
                    catId = db.GetIntegerValue(sqlFCTemplate.SqlFormat(True, cat.@Name, cat.@Ordinal))
                End If
                For Each field In cat.Elements("Field")
                    Dim fieldId = ""
                    If field.@FieldId <> 0 Then
                        fieldId = field.@FieldId
                    End If
                    Dim DataFormatName As String = "", SpecialQuery As String = "", OutputType As String = "", AuxFilterValue As String = "", AuxFilterField As String = "", CalculationExpression = "", FilterQuery = ""
                    If field.Elements("DataFormatName").Count() > 0 Then
                        DataFormatName = field.Elements("DataFormatName").First.Value
                    End If
                    If field.Elements("SpecialQuery").Count() > 0 Then
                        SpecialQuery = field.Elements("SpecialQuery").First.Value
                    End If
                    If field.Elements("AuxFilterValue").Count() > 0 Then
                        AuxFilterValue = field.Elements("AuxFilterValue").First.Value
                    End If
                    If field.Elements("AuxFilterField").Count() > 0 Then
                        AuxFilterField = field.Elements("AuxFilterField").First.Value
                    End If
                    'If field.Elements("CalculationExpression").Count() > 0 Then
                    '    CalculationExpression = field.Elements("CalculationExpression").First.Value
                    'End If
                    'If field.Elements("FilterQuery").Count() > 0 Then
                    '    FilterQuery = field.Elements("FilterQuery").First.Value
                    'End If
                    Dim sqlFUTemplate As String = "insert INTO cse.ComparableFields(categoryID,fieldid,label,dataformat,SpecialQuery,outputtype,AuxFilterField,AuxFiltervalue,ordinal,FieldType,CalculationExpression,FilterQuery, Hidden) values({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12})"
                    db.Execute(sqlFUTemplate.SqlFormat(True, catId, fieldId, field.@Label, DataFormatName, SpecialQuery, field.@OutputType, AuxFilterField, AuxFilterValue, field.@Ordinal, field.@FieldType, field.@CalculationExpression, field.@FilterQuery, Boolean.Parse(field.@Hidden)))

                Next

            Next
        End Sub

        Private Sub _importRangeLookup(node As XElement)
            Dim db As Database = target
            Dim vfrom, vto, value As Single
            Try
                Dim dt As New DataTable
                dt.Columns.Add("GroupName", Type.GetType("System.String"))
                dt.Columns.Add("ValueFrom", Type.GetType("System.String"))
                dt.Columns.Add("ValueTo", Type.GetType("System.String"))
                dt.Columns.Add("RangeValue", Type.GetType("System.String"))
                dt.Columns.Add("IncrementFactor", Type.GetType("System.String"))
                Dim r As New Regex("^[a-zA-Z0-9 ]*$")
                For Each Range In node.Descendants("RangeLookups")
                    Single.TryParse(Range.@ValueFrom, vfrom)
                    Single.TryParse(Range.@ValueTo, vto)
                    Single.TryParse(Range.@RangeValue, value)

                    If Range.@GroupName.Trim() = "" Or Not (r.IsMatch(Range.@GroupName)) Then
                        Throw New Exception("The Group Name is invalid.")
                    End If
                    If Range.@ValueFrom.Trim() = "" Then
                        Throw New Exception("The From value is invalid.")
                    End If
                    If Range.@ValueTo.Trim() = "" Then
                        Throw New Exception("The To value is invalid.")
                    End If
                    If Range.@RangeValue.Trim() = "" Then
                        Throw New Exception("The Range value is invalid.")
                    End If
                    If (Range.@IncrementFactor.Trim() = "") Or (Range.@IncrementFactor.Trim() <> "1" And Range.@IncrementFactor.Trim() <> "0.1" And Range.@IncrementFactor.Trim() <> "0.01" And Range.@IncrementFactor.Trim() <> "0.001") Then
                        Throw New Exception("The Increment Factor is invalid.")
                    End If
                    If vfrom >= vto Then
                        Throw New Exception("The 'To' value should not be less than or equal to 'From' value.")
                    End If
                    Dim dr As DataRow = dt.NewRow
                    dr("GroupName") = Range.@GroupName.Trim()
                    dr("ValueFrom") = Range.@ValueFrom.Trim()
                    dr("ValueTo") = Range.@ValueTo.Trim()
                    dr("RangeValue") = Range.@RangeValue.Trim()
                    dr("IncrementFactor") = Range.@IncrementFactor.Trim()
                    dt.Rows.Add(dr)
                Next
                Dim duplicateVal = dt.AsEnumerable().GroupBy(Function(i) New With {Key .GroupName = i.Field(Of String)("GroupName"), Key .RangeValue = i.Field(Of String)("RangeValue")}).Where(Function(g) g.Count > 1).Select(Function(g) New With {g.Key.GroupName, g.Key.RangeValue})
                Dim ValCount As Integer = duplicateVal.Count
                If ValCount > 0 Then
                    Throw New Exception("The Range values are duplicated.")
                End If

                Dim duplicateRange = dt.AsEnumerable().GroupBy(Function(i) New With {Key .GroupName = i.Field(Of String)("GroupName"), Key .ValueFrom = i.Field(Of String)("ValueFrom"), Key .ValueTo = i.Field(Of String)("ValueTo")}).Where(Function(g) g.Count > 1).Select(Function(g) New With {g.Key.GroupName, g.Key.ValueFrom, g.Key.ValueTo})
                Dim RangeCount As Integer = duplicateRange.Count
                If RangeCount > 0 Then
                    Throw New Exception("The Ranges are duplicated.")
                End If
                db.Execute("TRUNCATE TABLE RangeLookup; ")
                __importRangeLookup(node, db)
            Catch ex As Xml.XmlException
                Throw New Exception("The uploaded file contains invalid XML. Import process has been aborted.")
            Catch ex As Exception
                Throw New Exception("You have uploaded an invalid Rangelookup file - " + ex.Message, ex)
            End Try
        End Sub

        Private Sub _importFieldCategory(node As XElement, Optional backupDoc As XElement = Nothing, Optional loginUser As String = "")
            Dim db As Database = target
            Dim SettingAuditrailData As New ArrayList
            Try
                For Each cat In node.Descendants("FieldCategory")
                    If cat.@TabularData.ToLower = "true" Then
                        If cat.@SourceTableName.Trim = "" Then
                            Throw New Exception("One or more FieldCategory with TabularData=true does not have SourceTableName defined.")
                        End If
                    End If

                    If backupDoc IsNot Nothing Then
                        For Each bCat In backupDoc.Descendants("FieldCategory")
                            If bCat.@Name = cat.@Name AndAlso bCat.@SourceTableName = cat.@SourceTableName Then
                                For Each item As XAttribute In cat.Attributes()
                                    Select Case item.Name
                                        Case "TabularData"
                                            If Not ((cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "1") Or (cat.Attribute(item.Name).Value = "" And bCat.Attribute(item.Name).Value = "") Or (cat.Attribute(item.Name).Value = "1" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "False") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "0") Or (cat.Attribute(item.Name).Value = "0" And bCat.Attribute(item.Name).Value = "False")) Then
                                                SettingAuditrailData.Add(item.Name.ToString() + " changed from " + bCat.Attribute(item.Name).Value + " to " + cat.Attribute(item.Name).Value + " for category " + cat.@Name)
                                            End If
                                        Case "IsReadOnly"
                                            If Not ((cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "1") Or (cat.Attribute(item.Name).Value = "" And bCat.Attribute(item.Name).Value = "") Or (cat.Attribute(item.Name).Value = "1" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "False") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "0") Or (cat.Attribute(item.Name).Value = "0" And bCat.Attribute(item.Name).Value = "False")) Then
                                                SettingAuditrailData.Add(item.Name.ToString() + " changed from " + bCat.Attribute(item.Name).Value + " to " + cat.Attribute(item.Name).Value + " for category " + cat.@Name)
                                            End If
                                        Case "AllowAddDelete"
                                            If Not ((cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "1") Or (cat.Attribute(item.Name).Value = "" And bCat.Attribute(item.Name).Value = "") Or (cat.Attribute(item.Name).Value = "1" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "False") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "0") Or (cat.Attribute(item.Name).Value = "0" And bCat.Attribute(item.Name).Value = "False")) Then
                                                SettingAuditrailData.Add(item.Name.ToString() + " changed from " + bCat.Attribute(item.Name).Value + " to " + cat.Attribute(item.Name).Value + " for category " + cat.@Name)
                                            End If
                                        Case "AllowCopy"
                                            If Not ((cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "1") Or (cat.Attribute(item.Name).Value = "" And bCat.Attribute(item.Name).Value = "") Or (cat.Attribute(item.Name).Value = "1" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "False") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "0") Or (cat.Attribute(item.Name).Value = "0" And bCat.Attribute(item.Name).Value = "False")) Then
                                                SettingAuditrailData.Add(item.Name.ToString() + " changed from " + bCat.Attribute(item.Name).Value + " to " + cat.Attribute(item.Name).Value + " for category " + cat.@Name)
                                            End If
                                        Case "EditExist"
                                            If Not ((cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "1") Or (cat.Attribute(item.Name).Value = "" And bCat.Attribute(item.Name).Value = "") Or (cat.Attribute(item.Name).Value = "1" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "False") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "0") Or (cat.Attribute(item.Name).Value = "0" And bCat.Attribute(item.Name).Value = "False")) Then
                                                SettingAuditrailData.Add(item.Name.ToString() + " changed from " + bCat.Attribute(item.Name).Value + " to " + cat.Attribute(item.Name).Value + " for category " + cat.@Name)
                                            End If
                                        Case "AllowMassUpdate"
                                            If Not ((cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "1") Or (cat.Attribute(item.Name).Value = "" And bCat.Attribute(item.Name).Value = "") Or (cat.Attribute(item.Name).Value = "1" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "False") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "0") Or (cat.Attribute(item.Name).Value = "0" And bCat.Attribute(item.Name).Value = "False")) Then
                                                SettingAuditrailData.Add(item.Name.ToString() + " changed from " + bCat.Attribute(item.Name).Value + " to " + cat.Attribute(item.Name).Value + " for category " + cat.@Name)
                                            End If
                                        Case "MassUpdateDescendents"
                                            If Not ((cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "1") Or (cat.Attribute(item.Name).Value = "" And bCat.Attribute(item.Name).Value = "") Or (cat.Attribute(item.Name).Value = "1" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "False") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "0") Or (cat.Attribute(item.Name).Value = "0" And bCat.Attribute(item.Name).Value = "False")) Then
                                                SettingAuditrailData.Add(item.Name.ToString() + " changed from " + bCat.Attribute(item.Name).Value + " to " + cat.Attribute(item.Name).Value + " for category " + cat.@Name)
                                            End If
                                        Case "DoNotAllowEditExistingRecords"
                                            If Not ((cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "1") Or (cat.Attribute(item.Name).Value = "" And bCat.Attribute(item.Name).Value = "") Or (cat.Attribute(item.Name).Value = "1" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "False") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "0") Or (cat.Attribute(item.Name).Value = "0" And bCat.Attribute(item.Name).Value = "False")) Then
                                                SettingAuditrailData.Add(item.Name.ToString() + " changed from " + bCat.Attribute(item.Name).Value + " to " + cat.Attribute(item.Name).Value + " for category " + cat.@Name)
                                            End If
                                        Case "ShowGridOnly"
                                            If Not ((cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "1") Or (cat.Attribute(item.Name).Value = "" And bCat.Attribute(item.Name).Value = "") Or (cat.Attribute(item.Name).Value = "1" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "False") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "0") Or (cat.Attribute(item.Name).Value = "0" And bCat.Attribute(item.Name).Value = "False")) Then
                                                SettingAuditrailData.Add(item.Name.ToString() + " changed from " + bCat.Attribute(item.Name).Value + " to " + cat.Attribute(item.Name).Value + " for category " + cat.@Name)
                                            End If
                                        Case "MultiGridStub"
                                            If Not ((cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "1") Or (cat.Attribute(item.Name).Value = "" And bCat.Attribute(item.Name).Value = "") Or (cat.Attribute(item.Name).Value = "1" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "False") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "0") Or (cat.Attribute(item.Name).Value = "0" And bCat.Attribute(item.Name).Value = "False")) Then
                                                SettingAuditrailData.Add(item.Name.ToString() + " changed from " + bCat.Attribute(item.Name).Value + " to " + cat.Attribute(item.Name).Value + " for category " + cat.@Name)
                                            End If
                                        Case "DoNotAllowDeleteFirstRecord"
                                            If Not ((cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "1") Or (cat.Attribute(item.Name).Value = "" And bCat.Attribute(item.Name).Value = "") Or (cat.Attribute(item.Name).Value = "1" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "False") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "0") Or (cat.Attribute(item.Name).Value = "0" And bCat.Attribute(item.Name).Value = "False")) Then
                                                SettingAuditrailData.Add(item.Name.ToString() + " changed from " + bCat.Attribute(item.Name).Value + " to " + cat.Attribute(item.Name).Value + " for category " + cat.@Name)
                                            End If
                                        Case "DoNotShowInMultiGrid"
                                            If Not ((cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "1") Or (cat.Attribute(item.Name).Value = "" And bCat.Attribute(item.Name).Value = "") Or (cat.Attribute(item.Name).Value = "1" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "False") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "0") Or (cat.Attribute(item.Name).Value = "0" And bCat.Attribute(item.Name).Value = "False")) Then
                                                SettingAuditrailData.Add(item.Name.ToString() + " changed from " + bCat.Attribute(item.Name).Value + " to " + cat.Attribute(item.Name).Value + " for category " + cat.@Name)
                                            End If
                                        Case "DoNotEditFirstRecord"
                                            If Not ((cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "1") Or (cat.Attribute(item.Name).Value = "" And bCat.Attribute(item.Name).Value = "") Or (cat.Attribute(item.Name).Value = "1" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "False") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "0") Or (cat.Attribute(item.Name).Value = "0" And bCat.Attribute(item.Name).Value = "False")) Then
                                                SettingAuditrailData.Add(item.Name.ToString() + " changed from " + bCat.Attribute(item.Name).Value + " to " + cat.Attribute(item.Name).Value + " for category " + cat.@Name)
                                            End If
                                        Case "DoNotDeleteOnHide"
                                            If Not ((cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "1") Or (cat.Attribute(item.Name).Value = "" And bCat.Attribute(item.Name).Value = "") Or (cat.Attribute(item.Name).Value = "1" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "False") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "0") Or (cat.Attribute(item.Name).Value = "0" And bCat.Attribute(item.Name).Value = "False")) Then
                                                SettingAuditrailData.Add(item.Name.ToString() + " changed from " + bCat.Attribute(item.Name).Value + " to " + cat.Attribute(item.Name).Value + " for category " + cat.@Name)
                                            End If
                                        Case "EnableTemplateInsert"
                                            If Not ((cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "1") Or (cat.Attribute(item.Name).Value = "" And bCat.Attribute(item.Name).Value = "") Or (cat.Attribute(item.Name).Value = "1" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "False") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "0") Or (cat.Attribute(item.Name).Value = "0" And bCat.Attribute(item.Name).Value = "False")) Then
                                                SettingAuditrailData.Add(item.Name.ToString() + " changed from " + bCat.Attribute(item.Name).Value + " to " + cat.Attribute(item.Name).Value + " for category " + cat.@Name)
                                            End If
                                        Case "AllowDeleteOnly"
                                            If Not ((cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "1") Or (cat.Attribute(item.Name).Value = "" And bCat.Attribute(item.Name).Value = "") Or (cat.Attribute(item.Name).Value = "1" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "False") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "0") Or (cat.Attribute(item.Name).Value = "0" And bCat.Attribute(item.Name).Value = "False")) Then
                                                SettingAuditrailData.Add(item.Name.ToString() + " changed from " + bCat.Attribute(item.Name).Value + " to " + cat.Attribute(item.Name).Value + " for category " + cat.@Name)
                                            End If
                                        Case "DisableCopyRecord"
                                            If Not ((cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "1") Or (cat.Attribute(item.Name).Value = "" And bCat.Attribute(item.Name).Value = "") Or (cat.Attribute(item.Name).Value = "1" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "True" And bCat.Attribute(item.Name).Value = "True") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "False") Or (cat.Attribute(item.Name).Value = "False" And bCat.Attribute(item.Name).Value = "0") Or (cat.Attribute(item.Name).Value = "0" And bCat.Attribute(item.Name).Value = "False")) Then
                                                SettingAuditrailData.Add(item.Name.ToString() + " changed from " + bCat.Attribute(item.Name).Value + " to " + cat.Attribute(item.Name).Value + " for category " + cat.@Name)
                                            End If
                                        Case Else
                                            If cat.Attribute(item.Name).Value <> bCat.Attribute(item.Name).Value Then
                                                SettingAuditrailData.Add(item.Name.ToString() + " changed from " + bCat.Attribute(item.Name).Value + " to " + cat.Attribute(item.Name).Value + " for category " + cat.@Name)
                                            End If
                                    End Select
                                Next
                            End If
                        Next
                    End If

                    If cat.@SortExpression IsNot Nothing AndAlso cat.@SortExpression <> "" Then
                        Dim SortExpression = cat.@SortExpression
                        Dim sortFields As New ArrayList
                        If Not (SortExpression.Contains("CASE") And SortExpression.Contains("WHEN")) Then
                            Dim sortingfields As String() = SortExpression.Split(New Char() {","c})
                            For Each fields In sortingfields
                                Dim sFields As String() = fields.Trim().Split(New Char() {" "c})
                                If sFields.Length > 2 Then
                                    Throw New Exception("The given sort expression for the category " + cat.@Name + " is invalid. Please review the expression.")
                                ElseIf sFields.Length > 1 Then
                                    If Not (sFields(1) = "" Or sFields(1).ToLower() = "asc" Or sFields(1).ToLower() = "desc") Then
                                        Throw New Exception("The given sort expression for the category " + cat.@Name + " is invalid. Please review the expression.")
                                    End If
                                End If

                                If sFields(0) Is Nothing Or sFields(0) = "" Then
                                    Throw New Exception("The given sort expression for the category " + cat.@Name + " is invalid. Please review the expression.")
                                End If
                                sortFields.Add(sFields(0))
                            Next

                            If sortFields.Count > 0 Then
                                For Each dField In cat.Elements("Field")
                                    If sortFields.Count = 0 Then
                                        Exit For
                                    End If
                                    For i As Integer = 0 To sortFields.Count - 1
                                        If dField.@Name.ToString().ToLower().IndexOf(sortFields(i).ToLower()) <> -1 Or "rowuid".IndexOf(sortFields(i).ToLower()) <> -1 Or "clientrowuid".IndexOf(sortFields(i).ToLower()) <> -1 Or "parentrowuid".IndexOf(sortFields(i).ToLower()) <> -1 Then
                                            sortFields.RemoveAt(i)
                                            Exit For
                                        End If
                                    Next
                                Next

                                If sortFields.Count > 0 Then
                                    Dim fCount As Integer = 0
                                    For i As Integer = 0 To sortFields.Count - 1
                                        Dim dataRows As DataTable = Database.Tenant.GetDataTable("SELECT Name FROM DataSourceField WHERE Name = '" + sortFields(i) + "' AND CategoryId Is Null AND SourceTable " + If(cat.@SourceTableName.Trim = "", "Is Null", "= '" + cat.@SourceTableName.Trim + "'"))
                                        If dataRows.Rows.Count() > 0 Then
                                            fCount = fCount + 1
                                        End If
                                    Next
                                    If (fCount <> sortFields.Count) Then
                                        Throw New Exception("The given sort expression for the category " + cat.@Name + " is invalid. Please review the expression.")
                                    End If
                                End If
                            End If

                        End If
                    End If

                    Dim count As Integer = 0
                    Dim catName As String = cat.@Name

                    If catName = "_photo_meta_data" Then
                        For Each dField In cat.Elements("Field")
                            count = count + 1
                        Next

                        If (count > 10) Then
                            Throw New Exception("There is a maximum of 10 photo metadata fields allowed; modify the XML to remove any additional lines and import again")
                        End If
                    End If

                Next
            Catch ex As Xml.XmlException
                '        		tables truncate after above codes ,  data is not removed. so no need to import from old backup
                '        		Dim sql As String = "SELECT BackupDocument FROM CC_SettingsBackup WHERE ImportId = (SELECT MAX(ImportId) FROM CC_SettingsBackup WHERE BackupDocument LIKE '%FieldCategory%' AND BackupName LIKE '%Backup%')"
                '            	Dim result As String = db.GetStringValue(sql) 
                '            	ImportSettings(result, ConfigFileType.FieldCategory, db)
                Throw New Exception("The uploaded file contains invalid XML. Import process has been aborted.")
            Catch ex As Exception
                '            	Dim sql As String = "SELECT BackupDocument FROM CC_SettingsBackup WHERE ImportId = (SELECT MAX(ImportId) FROM CC_SettingsBackup WHERE BackupDocument LIKE '%FieldCategory%' AND BackupName LIKE '%Backup%')"
                '            	Dim result As String = db.GetStringValue(sql) 
                '            	ImportSettings(result, ConfigFileType.FieldCategory, db)
                Throw New Exception("You have uploaded an invalid Field Category settings file - " + ex.Message, ex)
            End Try

            Try
                Dim tableFields = db.GetDataTable("SELECT Name, SourceTable FROM DataSourceField").AsEnumerable.Select(Function(x) New With {.FieldName = x.GetString("Name"), .TableName = x.GetString("SourceTable")}).ToList
                Dim tables = tableFields.Select(Function(x) x.TableName).Distinct.Select(Function(x) New With {.Table = x, .Fields = New List(Of String)}).ToList
                tables.ForEach(Sub(x)
                                   x.Fields = tableFields.Where(Function(y) y.TableName = x.Table).Select(Function(z) z.FieldName.ToLower).ToList
                               End Sub)
                Dim tableList As New Dictionary(Of String, List(Of String))

                For Each t In tables
                    tableList.Add(t.Table.ToLower, t.Fields)
                Next

                db.Execute("TRUNCATE TABLE FieldCategory; ")
                db.Execute("TRUNCATE TABLE DataSourceFieldProperties; ")
                db.Execute("TRUNCATE TABLE FieldCategoryProperties; ")
                db.Execute("UPDATE DataSourceField SET CategoryId = NULL, CategoryOrdinal = 0;")
                _importCategory(node, db, tableList)

                Dim o As DataRow = Database.System.GetTopRow("SELECT EnableXMLAuditTrail FROM OrganizationSettings WHERE OrganizationId = " & HttpContext.Current.GetCAMASession().OrganizationId)
                If (o.GetBoolean("EnableXMLAuditTrail")) Then
                    For Each element In SettingAuditrailData
                        db.Execute("INSERT INTO SettingsAuditTrail(EventDate,EventTime,LoginId,Description) VALUES(GETUTCDATE(),GETUTCDATE(),{0},{1})".SqlFormat(True, loginUser.ToString(), element.ToString()))
                    Next
                End If

                Dim sql As String = ""
                sql += vbNewLine + ("UPDATE DataSourceField SET CategoryId = NULL WHERE CategoryId = 0")
                sql += vbNewLine + ("UPDATE DataSourceField SET IsRequired = 0 WHERE (CategoryId IS NULL AND SourceTable <> '_photo_meta_data') AND IsRequired = 1;")
                sql += vbNewLine + ("UPDATE DataSourceField SET IsRequired = 0 WHERE DoNotShowOnDE = 1 AND IsRequired = 1;")
                sql += vbNewLine + ("UPDATE DataSourceField SET IsRequired = 0 WHERE IsReadonly = 1 AND IsRequired = 1;")
                sql += vbNewLine + ("UPDATE DataSourceField SET DoNotShowOnDTR = 0 WHERE DoNotShowOnDTR IS NULL")

                sql += vbNewLine + ("UPDATE DataSourceField SET IsRequiredDTROnly = 0 WHERE (CategoryId IS NULL AND SourceTable <> '_photo_meta_data') AND IsRequiredDTROnly = 1;")
                sql += vbNewLine + ("UPDATE DataSourceField SET IsRequiredDTROnly = 0 WHERE DoNotShowOnDE = 1 AND IsRequiredDTROnly = 1;")
                sql += vbNewLine + ("UPDATE DataSourceField SET IsRequiredDTROnly = 0 WHERE IsReadonly = 1 AND IsRequiredDTROnly = 1;")
                sql += vbNewLine + ("UPDATE DataSourceField SET IsRequiredDTROnly = 0 WHERE DoNotShowOnDTR = 1 AND IsRequiredDTROnly = 1;")
                sql += vbNewLine + ("UPDATE DataSourceField SET IsRequired = 0  WHERE IsRequired = 1 AND IsRequiredDTROnly = 1;")
                sql += vbNewLine + ("UPDATE DataSourceField SET IsRequired = 0  WHERE IsRequiredDTROnly = 1")
                sql += vbNewLine + ("UPDATE DataSourceField SET IsRequiredDTROnly = 0 WHERE IsRequiredDTROnly IS NULL")
                db.Execute(sql)

            Catch ex As Xml.XmlException
                Dim sql As String = "SELECT BackupDocument FROM CC_SettingsBackup WHERE ImportId = (SELECT MAX(ImportId) FROM CC_SettingsBackup WHERE BackupDocument LIKE '%FieldCategory%' AND BackupName LIKE '%Backup%')"
                Dim result As String = db.GetStringValue(sql)
                ImportSettings(result, ConfigFileType.FieldCategory, db)
                Throw New Exception("Previous Backup Restored due to Invalid Field Category settings - " + ex.Message)
            Catch ex As Exception
                Dim sql As String = "SELECT BackupDocument FROM CC_SettingsBackup WHERE ImportId = (SELECT MAX(ImportId) FROM CC_SettingsBackup WHERE BackupDocument LIKE '%FieldCategory%' AND BackupName LIKE '%Backup%')"
                Dim result As String = db.GetStringValue(sql)
                ImportSettings(result, ConfigFileType.FieldCategory, db)
                Throw New Exception("Previous Backup Restored due to Invalid Field Category settings - " + ex.Message, ex)
            End Try
        End Sub

        Private Sub _importCategory(node As XElement, db As Database, tables As Dictionary(Of String, List(Of String)), Optional parentCategoryId As Integer = 0)

            For Each cat In node.Elements("FieldCategory")
                Dim name As String = cat.@Name
                Dim Id As String = cat.@Id
                Dim sqlFCTemplate As String
                Dim eqid As String = 0

                If Id Is Nothing Then
                    sqlFCTemplate = "INSERT INTO FieldCategory (Name, Description, HeaderInfo, Ordinal, ShowCategory, TabularData, SourceTableId, SourceTableName,SortExpression,AllowCopy, AllowMassUpdate, DoNotAllowEditExistingRecords,ShowGridOnly, GridHeader,UniqueKeys, MultiGridStub, MaxRecords,DoNotAllowDeleteFirstRecord,MinRecords,HideExpression,DoNotShowInMultiGrid,DoNotAllowInsertDeleteExpression,IncludeParentFieldsOnInsert,ClassCalculatorParameters,ReadOnlyExpression, EnableDeleteExpression, UniqueSiblingExpression, UniqueSiblingKeys,UniqueFieldsInSiblings,PhotoTag,PhotoLinkTableKey,DoNotDeleteOnHide,DisplayType,EnableTemplateInsert,OverrideParentCategory,CC_Note,DisableCopyRecordExpression,DisableCopyRecord,MassUpdateDescendents,MassUpdateDescendentFields) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7},{8},{9}, {10}, {11},{12}, {13},{14}, {15}, {16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34},{35},{36},{37},{38},{39}); SELECT CAST(@@IDENTITY AS INT);"
                Else
                    If Id <> "" Then
                        eqid = db.GetIntegerValue("SELECT COUNT(Id) FROM FieldCategory WHERE Id = " + Id)
                        If Id <= 0 And Id <> -2 Then
                            Throw New Exception("Id value should be greater than zero.")
                        Else
                            If eqid <> 0 Then
                                Throw New Exception("Id values should be unique. ")
                            Else
                                sqlFCTemplate = "SET IDENTITY_INSERT FieldCategory ON; INSERT INTO FieldCategory (Id,Name, Description, HeaderInfo, Ordinal, ShowCategory, TabularData, SourceTableId, SourceTableName,SortExpression,AllowCopy, AllowMassUpdate, DoNotAllowEditExistingRecords,ShowGridOnly, GridHeader,UniqueKeys, MultiGridStub, MaxRecords,DoNotAllowDeleteFirstRecord,MinRecords,HideExpression,DoNotShowInMultiGrid,DoNotAllowInsertDeleteExpression,IncludeParentFieldsOnInsert,ClassCalculatorParameters,ReadOnlyExpression, EnableDeleteExpression, UniqueSiblingExpression, UniqueSiblingKeys,UniqueFieldsInSiblings,PhotoTag,PhotoLinkTableKey,DoNotDeleteOnHide,DisplayType,EnableTemplateInsert,OverrideParentCategory,CC_Note,DisableCopyRecordExpression,DisableCopyRecord,MassUpdateDescendents,MassUpdateDescendentFields) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7},{8},{9}, {10}, {11},{12}, {13},{14}, {15}, {16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34},{35},{36},{37},{38},{39},{40}); SET IDENTITY_INSERT FieldCategory ON; SELECT CAST(@@IDENTITY AS INT);"
                            End If
                        End If

                    Else
                        Throw New Exception("Id is not given for Category" + " - " + name + ".")
                    End If
                End If

                Dim showCategory As String = "1"
                Dim isReadOnly As String = "0"
                Dim DisableCopyRecord As String = "0"
                Dim allowAddDelete As String = "1"
                Dim AllowDeleteOnly As String = "0"
                Dim SortExpression As String = ""
                Dim allowCopy As String = "0"
                Dim allowMassUpdate As String = "0"
                Dim MassUpdateDescendents As String = "0"
                Dim MassUpdateDescendentFields As String = ""
                Dim DoNotAllowEditExistingRecords As String = "0"
                Dim DoNotAllowDeleteFirstRecord As String = "0"
                Dim DoNotEditFirstRecord1 As String = "0"
                Dim MinRecords As String = "0"
                Dim DoNotDeleteOnHide As String = "1"
                Dim DisplayType As String = ""
                Dim EnableTemplateInsert As String = "0"
                Dim OverrideParentCategory As String = ""
                Dim CC_Note As String = ""
                Dim DisableCopyRecordExpression As String = ""
                Dim headerInfo As String = "", description As String = "", gridHeader As String = "", UniqueKeys As String = ""
                ''For Each prop In cat.Attributes
                ''prop.value = prop.value.Replace("&#xA;", vbNewLine)
                ''Next
                If cat.@Hidden IsNot Nothing Then
                    If cat.@Hidden.ToLower = "true" Then
                        showCategory = "0"
                    Else
                        showCategory = "1"
                    End If
                End If
                If cat.@IsReadOnly IsNot Nothing AndAlso cat.@IsReadOnly.ToLower = "true" Then
                    isReadOnly = "1"
                End If
                If cat.@DisableCopyRecord IsNot Nothing AndAlso cat.@DisableCopyRecord.ToLower = "true" Then
                    DisableCopyRecord = "1"
                End If
                If cat.@AllowAddDelete IsNot Nothing AndAlso cat.@AllowAddDelete.ToLower = "false" Then
                    allowAddDelete = "0"
                End If
                If cat.@AllowDeleteOnly IsNot Nothing AndAlso cat.@AllowDeleteOnly.ToLower = "true" Then
                    AllowDeleteOnly = "1"
                End If
                If cat.@AllowMassUpdate IsNot Nothing AndAlso cat.@AllowMassUpdate.ToLower = "true" Then
                    allowMassUpdate = "1"
                End If
                If cat.@MassUpdateDescendents IsNot Nothing AndAlso cat.@MassUpdateDescendents.ToLower = "true" Then
                    MassUpdateDescendents = "1"
                End If

                If cat.@DoNotAllowEditExistingRecords IsNot Nothing AndAlso cat.@DoNotAllowEditExistingRecords.ToLower = "true" Then
                    DoNotAllowEditExistingRecords = "1"
                End If
                If cat.@DoNotAllowDeleteFirstRecord IsNot Nothing AndAlso cat.@DoNotAllowDeleteFirstRecord.ToLower = "true" Then
                    DoNotAllowDeleteFirstRecord = "1"
                End If
                If cat.@DoNotEditFirstRecord IsNot Nothing AndAlso cat.@DoNotEditFirstRecord.ToLower = "true" Then
                    DoNotEditFirstRecord1 = "1"
                End If
                If cat.@DoNotDeleteOnHide IsNot Nothing AndAlso cat.@DoNotDeleteOnHide.ToLower = "false" Then
                    DoNotDeleteOnHide = "0"
                End If
                If cat.@DisplayType IsNot Nothing Then
                    DisplayType = cat.@DisplayType
                End If
                Dim multiGridStub = getBooleanAttribute(cat, "MultiGridStub")
                Dim sourceTableId As String = ""
                If cat.@SourceTableName <> "" Then
                    sourceTableId = target.GetIntegerValue("SELECT Id FROM DataSourceTable WHERE Name = {0}".SqlFormatString(cat.@SourceTableName))
                End If
                If cat.Elements("HeaderInfo").Count() > 0 Then
                    headerInfo = cat.Elements("HeaderInfo").First.Value
                End If

                If cat.Elements("GridHeader").Count() > 0 Then
                    gridHeader = cat.Elements("GridHeader").First.Value
                End If
                If cat.Elements("Description").Count() > 0 Then
                    description = cat.Elements("Description").First.Value
                End If
                'If cat.Elements("UniqueKeys").Count() > 0 Then
                '    UniqueKeys = cat.Elements("UniqueKeys").First.Value
                'End If
                If cat.@UniqueKeys IsNot Nothing AndAlso cat.@UniqueKeys <> "" Then
                    UniqueKeys = cat.@UniqueKeys
                End If
                If cat.@SortExpression IsNot Nothing AndAlso cat.@SortExpression <> "" Then
                    SortExpression = cat.@SortExpression
                End If
                If cat.@DisableCopyRecordExpression IsNot Nothing AndAlso cat.@DisableCopyRecordExpression <> "" Then
                    DisableCopyRecordExpression = cat.@DisableCopyRecordExpression
                End If
                If cat.@CC_Note IsNot Nothing AndAlso cat.@CC_Note <> "" Then
                    CC_Note = cat.@CC_Note
                End If
                If cat.@MassUpdateDescendentFields IsNot Nothing AndAlso cat.@MassUpdateDescendentFields <> "" Then
                    MassUpdateDescendentFields = cat.@MassUpdateDescendentFields
                End If
                If cat.@AllowCopy IsNot Nothing AndAlso cat.@AllowCopy <> "" Then
                    allowCopy = cat.@AllowCopy
                End If
                Dim valShowGridOnly As Boolean = False
                If cat.@ShowGridOnly.ToLower = "true" Then
                    valShowGridOnly = True
                End If
                Dim catName As String = cat.@Name
                Dim catId As Integer
                If catName = "_photo_meta_data" Then
                    catId = -2
                Else
                    Dim dr As DataRow
                    If cat.@SourceTableName IsNot Nothing AndAlso cat.@SourceTableName <> "" Then
                        dr = db.GetTopRow("SELECT * FROM FieldCategory WHERE Name = {0} AND SourceTableName = {1}".SqlFormat(True, cat.@Name, cat.@SourceTableName))
                    Else
                        dr = db.GetTopRow("SELECT * FROM FieldCategory WHERE Name = {0} AND SourceTableName IS NULL".SqlFormat(True, cat.@Name))
                    End If
                    If dr IsNot Nothing Then
                        Throw New Exception("The category name already exists. Duplicate name is not allowed.")
                    End If
                    If Id Is Nothing Then
                        catId = db.GetIntegerValue(sqlFCTemplate.SqlFormat(True, cat.@Name, description, headerInfo, cat.@Ordinal, showCategory, cat.@TabularData, sourceTableId, cat.@SourceTableName, SortExpression, allowCopy, allowMassUpdate, DoNotAllowEditExistingRecords, valShowGridOnly, gridHeader, UniqueKeys, multiGridStub, cat.@MaxRecords, DoNotAllowDeleteFirstRecord, cat.@MinRecords, cat.@HideExpression, cat.@DoNotShowInMultiGrid, cat.@DoNotAllowInsertDeleteExpression, cat.@IncludeParentFieldsOnInsert, cat.@ClassCalculatorParameters, cat.@ReadOnlyExpression, cat.@EnableDeleteExpression, cat.@UniqueSiblingExpression, cat.@UniqueSiblingKeys, cat.@UniqueFieldsInSiblings, cat.@PhotoTag, cat.@PhotoLinkTableKey, DoNotDeleteOnHide, cat.@DisplayType, cat.@EnableTemplateInsert, cat.@OverrideParentCategory, CC_Note, cat.@DisableCopyRecordExpression, cat.@DisableCopyRecord, MassUpdateDescendents, MassUpdateDescendentFields))
                    Else
                        catId = db.GetIntegerValue(sqlFCTemplate.SqlFormat(True, cat.@Id, cat.@Name, description, headerInfo, cat.@Ordinal, showCategory, cat.@TabularData, sourceTableId, cat.@SourceTableName, SortExpression, allowCopy, allowMassUpdate, DoNotAllowEditExistingRecords, valShowGridOnly, gridHeader, UniqueKeys, multiGridStub, cat.@MaxRecords, DoNotAllowDeleteFirstRecord, cat.@MinRecords, cat.@HideExpression, cat.@DoNotShowInMultiGrid, cat.@DoNotAllowInsertDeleteExpression, cat.@IncludeParentFieldsOnInsert, cat.@ClassCalculatorParameters, cat.@ReadOnlyExpression, cat.@EnableDeleteExpression, cat.@UniqueSiblingExpression, cat.@UniqueSiblingKeys, cat.@UniqueFieldsInSiblings, cat.@PhotoTag, cat.@PhotoLinkTableKey, DoNotDeleteOnHide, cat.@DisplayType, cat.@EnableTemplateInsert, cat.@OverrideParentCategory, CC_Note, cat.@DisableCopyRecordExpression, cat.@DisableCopyRecord, MassUpdateDescendents, MassUpdateDescendentFields))
                    End If

                End If


                Dim bulkSql As String = ""
                Dim fieldOrdinal As Integer = 10
                For Each field In cat.Elements("Field")
                    ''For Each prop In field.Attributes
                    ''prop.value = prop.value.Replace("&#xA;", vbNewLine)
                    ''Next
                    Dim isRequired As String = getBooleanAttribute(field, "IsRequired")
                    Dim showOnGrid As String = getBooleanAttribute(field, "ShowOnGrid")

                    Dim sortType As String = 0
                    If field.@SortType IsNot Nothing Then
                        sortType = field.@SortType
                    End If

                    Dim IsMassUpdateField As Boolean = False
                    If field.@IsMassUpdateField IsNot Nothing Then
                        IsMassUpdateField = field.@IsMassUpdateField
                    End If

                    Dim isUniqueInSiblings As String = getBooleanAttribute(field, "IsUniqueInSiblings")
                    Dim lookupShowNullValue As String = getBooleanAttribute(field, "LookupShowNullValue")
                    Dim autoSelectFirstItem As String = getBooleanAttribute(field, "AutoSelectFirstItem")
                    Dim autoNumber As String = getBooleanAttribute(field, "AutoNumber")
                    Dim MustIncludeInDataCopy As String = getBooleanAttribute(field, "MustIncludeInDataCopy")
                    Dim IsRequiredDTROnly As String = getBooleanAttribute(field, "IsRequiredDTROnly")
                    Dim IsHeatmap As String = getBooleanAttribute(field, "isHeatmap")
                    Dim UpdateSiblings As String = getBooleanAttribute(field, "UpdateSiblings")
                    Dim RadioFieldValues As String = field.@RadioFieldValues
                    Dim DoNotAllowSelectNull As String = getBooleanAttribute(field, "DoNotAllowSelectNull")
                    Dim IsLargeLookup As String = getBooleanAttribute(field, "IsLargeLookup")
                    Dim DoNotIncludeInAuditTrail As String = getBooleanAttribute(field, "DoNotIncludeInAuditTrail")
                    Dim IsClassCalculatorAttribute As String = getBooleanAttribute(field, "IsClassCalculatorAttribute")
                    Dim RequiredIfRecordEdited As String = getBooleanAttribute(field, "RequiredIfRecordEdited")
                    Dim UIProperties As String = field.@UIProperties
                    Dim defaultValue As String = field.@DefaultValue
                    '                    Dim MaxLength As String = field.@MaxLength
                    '                    Dim Precision As String = field.@Precision
                    Dim LookupTrimData As String = getBooleanAttribute(field, "LookupTrimData")
                    Dim LookupTrimListData As String = getBooleanAttribute(field, "LookupTrimListData")
                    Dim DoNotEditFirstRecord As String = getBooleanAttribute(field, "DoNotEditFirstRecord")
                    Dim InfoContent As String = ""
                    Dim DoNotAllowEditExistingValue As String = getBooleanAttribute(field, "DoNotAllowEditExistingValue")
                    '                    If Precision IsNot Nothing AndAlso Precision.Trim() ="" Then
                    '                    	Precision ="NULL"
                    '					End If

                    'Dim MapToSourceTable As String = field.@MapToSourceTable

                    If field.Elements("InfoContent").Count() > 0 Then
                        InfoContent = field.Elements("InfoContent").First.Value
                    End If

                    If (field.@DataType = "5" Or field.@DataType = "11") Then
                        If field.@LookupTable <> "" AndAlso field.@LookupTable <> "$QUERY$" AndAlso defaultValue <> "" Then
                            If db.GetIntegerValue("SELECT COUNT(*) FROM ParcelDataLookup WHERE LookupName = {0} AND IdValue= {1}".SqlFormatString(field.@LookupTable, field.@DefaultValue)) = 0 Then
                                defaultValue = ""
                            End If
                        End If
                    Else
                        If field.@DataType = "3" Then
                            If defaultValue <> "false" And defaultValue <> "true" Then
                                defaultValue = ""
                            End If
                        End If
                        field.@LookupTable = ""
                        field.@LookupQuery = ""
                        field.@LookupQueryForCache = ""
                        field.@LookupQueryCustomFields = ""
                        field.@AllowTextInput = "0"
                    End If

                    If field.@DataType <> "1" Then
                        field.@DoNotAllowSpecialCharacters = "0"
                    End If


                    Dim doNotInsertOrUpdateAtDestination As String = getBooleanAttribute(field, "DoNotInsertOrUpdateAtDestination")
                    Dim doNotShowOnDTR As String = getBooleanAttribute(field, "DoNotShowOnDTR")
                    Dim isCustomInputType As String = getBooleanAttribute(field, "IsCustomInputType")
                    Dim AllowRadioNull As String = getBooleanAttribute(field, "AllowRadioNull")
                    Dim AllowTextInput As String = getBooleanAttribute(field, "AllowTextInput")
                    Dim DoNotAllowSpecialCharacters As String = getBooleanAttribute(field, "DoNotAllowSpecialCharacters")

                    'Dim checkQuery As String = "SELECT COUNT(*) FROM DataSourceField  WHERE SourceTable = {0} AND Name = {1};".SqlFormatString(field.@SourceTable, field.@Name)
                    'If db.GetIntegerValue(checkQuery) = 0 Then

                    'End If

                    If tables.ContainsKey(field.@SourceTable.ToLower) AndAlso tables(field.@SourceTable.ToLower).Contains(field.@Name.ToLower) Then
                    Else
                        Dim sourceTable As String = field.@SourceTable
                        FieldCategory.AddCustomField(field.@Name, "", catId, sourceTable)
                    End If

                    Dim updateCatId As String = catId
                    If catId = -2 Then
                        updateCatId = 0
                    End If

                    '                    Dim numericScale As String = field.@NumericScale
                    '                    If String.IsNullOrEmpty(numericScale) Then
                    '                    	numericScale = Precision
                    '                    End If

                    Dim sqlFUTemplate As String = "UPDATE DataSourceField SET CategoryId = {0}, IsReadOnly = {3}, DoNotShowOnDE = {4}, DisplayLabel = {5}, AssignedName = {6}, CalculationExpression = {7}, LookupTable = {8}, IsFavorite = {9}, FavoriteOrdinal = {10}, DataType = {11}, CategoryOrdinal = {12}, IsRequired = {13}, ShowOnGrid = {14}, DefaultValue = {15},SortType={16},IsMassUpdateField={17}, ReadOnlyExpression = {18}, VisibilityExpression = {19},IsUniqueInSiblings={20},LookupShowNullValue={21}, LookupQuery={22},GridLinkScript={23},RequiredExpression={24},AutoSelectFirstitem={25},MustIncludeInDataCopy={26},CalculationOverrideExpression={27},autoNumber={28}, AliasSource = {29}, DoNotInsertOrUpdateAtDestination = {30}, DestinationNullReplacement = {31}, DoNotShowOnDTR = {32}, MetaDataSourceTable = {33}, IsRequiredDTROnly ={34} ,isHeatmap ={35}, CC_Note={36},UpdateSiblings={37},RadioFieldValues={38},DoNotAllowSelectNull={39},IsLargeLookup={40},DoNotIncludeInAuditTrail={42},IsClassCalculatorAttribute={43},RequiredIfRecordEdited={44}, UIProperties={45}, LookupTrimData={46}, LookupTrimListData={47}, DoNotEditFirstRecord={48}, InfoContent={49}, LookupQueryFilter={50},CustomInputType={51},AllowRadioNull={52},ConditionalValidationConfig ={53}, MapToSourceTable = {54},IncludeInSearchFilter = {55},LookupQueryForCache = {56}, LookupQueryCustomFields = {57}, DoNotAllowEditExistingValue = {58}, AllowTextInput = {59}, DoNotAllowSpecialCharacters = {60}  WHERE SourceTable = {1} AND Name = {2};"
                    bulkSql += vbNewLine + (sqlFUTemplate.SqlFormat(True, updateCatId, field.@SourceTable, field.@Name, field.@IsReadOnly, field.@DoNotShowOnDE, field.@DisplayLabel, field.@AssignedName, field.@CalculationExpression, field.@LookupTable, field.@IsFavorite, field.@FavoriteOrdinal, field.@DataType, IIf(field.@CategoryOrdinal = 0, fieldOrdinal, field.@CategoryOrdinal), isRequired, showOnGrid, defaultValue, sortType, IsMassUpdateField, field.@ReadOnlyExpression, field.@VisibilityExpression, isUniqueInSiblings, lookupShowNullValue, field.@LookupQuery, field.@GridLinkScript, field.@RequiredExpression, autoSelectFirstItem, MustIncludeInDataCopy, field.@CalculationOverrideExpression, autoNumber, field.@AliasSource, doNotInsertOrUpdateAtDestination, field.@DestinationNullReplacement, doNotShowOnDTR, field.@MetaDataSourceTable, IsRequiredDTROnly, IsHeatmap, field.@CC_Note, UpdateSiblings, field.@RadioFieldValues, DoNotAllowSelectNull, IsLargeLookup, 0, DoNotIncludeInAuditTrail, IsClassCalculatorAttribute, RequiredIfRecordEdited, UIProperties, LookupTrimData, LookupTrimListData, DoNotEditFirstRecord, InfoContent, field.@LookupQueryFilter, isCustomInputType, AllowRadioNull, field.@ConditionalValidationConfig, field.@MapToSourceTable, field.@IncludeInSearchFilter, field.@LookupQueryForCache, field.@LookupQueryCustomFields, DoNotAllowEditExistingValue, AllowTextInput, DoNotAllowSpecialCharacters))
                    For Each rowItem In field.Descendants("UI_Property")
                        Dim fid As Integer = Database.Tenant.GetIntegerValue("Select Id from DataSourceField Where SourceTable = {0} AND Name = {1}".SqlFormat(False, field.@SourceTable, field.@Name))
                        Dim PropertyName = rowItem.@PropertyName
                        Dim Value = rowItem.@Value
                        Dim sql1 As String = "INSERT INTO DataSourceFieldProperties (FieldId,PropertyName,Value) VALUES ({0}, {1},{2})"
                        db.Execute(sql1.SqlFormat(False, fid, PropertyName, Value))
                    Next
                    fieldOrdinal += 10
                Next
                For Each catItem In cat.Descendants("UI_CategoryProperty")
                    Dim PropertyName = catItem.@PropertyName
                    Dim Value = catItem.@Value
                    Dim sql1 As String = "INSERT INTO FieldCategoryProperties (CategoryId,PropertyName,Value) VALUES ({0}, {1},{2})"
                    db.Execute(sql1.SqlFormat(False, catId, PropertyName, Value))
                Next
                If catId > 0 Then
                    Dim sqlFUTemplate2 As String = "UPDATE FieldCategory SET ParentCategoryId = {1}, FilterFields = {2}, IsReadOnly = {3}, AllowAddDelete = {4} , DoNotEditFirstRecord = {5} , AllowDeleteOnly = {6}  WHERE Id = {0};"
                    If parentCategoryId = 0 Then
                        bulkSql += vbNewLine + (sqlFUTemplate2.FormatString(catId, "NULL", "NULL", isReadOnly, allowAddDelete, DoNotEditFirstRecord1, AllowDeleteOnly))
                    Else
                        bulkSql += vbNewLine + (sqlFUTemplate2.SqlFormatString(catId, parentCategoryId, cat.@FilterFields, isReadOnly, allowAddDelete, DoNotEditFirstRecord1, AllowDeleteOnly))
                    End If
                    _importCategory(cat, db, tables, catId)
                End If

                If bulkSql <> "" Then
                    db.Execute(bulkSql)
                End If

            Next
        End Sub

        Private Function getBooleanAttribute(x As XElement, name As String) As String
            For Each a In x.Attributes
                If a.Name.LocalName.ToLower = name.ToLower Then
                    If a.Value.ToLower = "true" Or a.Value.ToLower = "1" Then
                        Return "1"
                    End If
                End If
            Next
            Return "0"
        End Function


        Private Sub _importDTRStatusTypes(node As XElement)
            Dim db As Database = target
            db.Execute("TRUNCATE TABLE DTR_StatusType; ")
            For Each rowItem In node.Descendants("DTRStatusType")
                Dim sqlFCTemplate As String = "INSERT INTO DTR_StatusType (Name, Description, Code, DTRReadOnly, DTREditor, DTRManager, GIS_Flag_Color) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6})"
                db.Execute(sqlFCTemplate.SqlFormat(True, rowItem.@Name, rowItem.@Description, rowItem.@Code, rowItem.@DTRReadOnly, rowItem.@DTREditor, rowItem.@DTRManager, rowItem.@GIS_Flag_Color))
            Next
        End Sub
        
        Private Sub _importDTRTasks(node As XElement)
            Dim db As Database = target
            'db.Execute("TRUNCATE TABLE DTR_Tasks; ") - Do Not Truncate, May have assignments
            For Each rowItem In node.Descendants("DTRTask")

                Dim sqlFCTemplate As String = "BEGIN IF NOT EXISTS(SELECT * FROM DTR_Tasks WHERE NAME={0}) BEGIN " + "INSERT INTO DTR_Tasks (Name, FilterData, CompiledSQLFilter) VALUES ({0}, {1},{2}) END ELSE BEGIN UPDATE  DTR_Tasks SET FilterData={1},CompiledSQLFilter={2} WHERE NAME={0} END  END "
                db.Execute(sqlFCTemplate.SqlFormat(True, rowItem.@Name, rowItem...<FilterData>.Value.Trim, rowItem...<CompiledSQLFilter>.Value.Trim))
            Next
        End Sub

#End Region


#End Region
        Public Sub BackupBeforeImport(ByRef xdoc As XDocument, tag As String, loginId As String, configType As ConfigFileType)
            Dim db As Database = target
            Dim count As Integer = 0
            Dim backupDoc As XDocument
            backupDoc = GetRootDoc(configType)

            If (configType = ConfigFileType.All Or configType = ConfigFileType.FieldCategory) Then
                count = 0
                count = db.GetIntegerValue("SELECT COUNT(*) FROM FieldCategory")
                If (count > 0) Then
                    _exportFieldCategory(backupDoc)
                End If
            End If

            If (configType = ConfigFileType.All Or configType = ConfigFileType.CSEFieldCategory) Then
                count = 0
                count = db.GetIntegerValue("SELECT COUNT(*) FROM cse.FieldCategory")
                If (count > 0) Then
                    _exportCSEFieldCategory(backupDoc)
                End If
            End If

            If configType = ConfigFileType.All Or configType = ConfigFileType.ApplicationSettings Then
                count = 0
                count = db.GetIntegerValue("SELECT COUNT(*) FROM Application")
                If (count > 0) Then
                    _exportApplicationSettings(backupDoc)
                End If
            End If

            If (configType = ConfigFileType.All Or configType = ConfigFileType.ClientTemplates) Then
                count = 0
                count = db.GetIntegerValue("SELECT COUNT(*) FROM ClientTemplates")
                If (count > 0) Then
                    _exportClientTemplates(backupDoc)
                End If
            End If

            If configType = ConfigFileType.All Or configType = ConfigFileType.ClientSettings Then
                count = 0
                count = db.GetIntegerValue("SELECT COUNT(*) FROM ClientSettings")
                If (count > 0) Then
                    _exportClientSettings(backupDoc)
                End If
            End If
            
            If configType = ConfigFileType.All Or configType = ConfigFileType.SketchSettings Then
                count = 0
                count = db.GetIntegerValue("SELECT COUNT(*) FROM SketchSettings")
                If (count > 0) Then
                    _exportSketchSettings(backupDoc)
                End If
            End If

            If (configType = ConfigFileType.All Or configType = ConfigFileType.ClientCustomAction) Then
                count = 0
                count = db.GetIntegerValue("SELECT COUNT(*) FROM ClientCustomAction")
                If (count > 0) Then
                    _exportClientCustomAction(backupDoc)
                End If
            End If

            If (configType = ConfigFileType.All Or configType = ConfigFileType.ClientValidation) Then
                count = 0
                count = db.GetIntegerValue("SELECT COUNT(*) FROM ClientValidation")
                If (count > 0) Then
                    _exportClientValidation(backupDoc)
                End If
            End If

            If (configType = ConfigFileType.All Or configType = ConfigFileType.AppraisalType) Then
                count = 0
                count = db.GetIntegerValue("SELECT COUNT(*) FROM AppraisalType")
                If (count > 0) Then
                    _exportAppraisalTypes(backupDoc)
                End If
            End If

            If (configType = ConfigFileType.All Or configType = ConfigFileType.FieldAlertTypes) Then
                count = 0
                count = db.GetIntegerValue("SELECT COUNT(*) FROM FieldAlertTypes")
                If (count > 0) Then
                    _exportFieldAlertType(backupDoc)
                End If
            End If

            If (configType = ConfigFileType.All Or configType = ConfigFileType.GISSettings) Then
                count = 0
                count = db.GetIntegerValue("SELECT COUNT(*) FROM GIS_ObjectLayer")
                If (count > 0) Then
                    _exportGISSettings(backupDoc)
                End If
            End If

            If (configType = ConfigFileType.All Or configType = ConfigFileType.DTRStatusTypes) Then
                count = 0
                count = db.GetIntegerValue("SELECT COUNT(*) FROM DTR_StatusType")
                If (count > 0) Then
                    _exportDTRStatusTypes(backupDoc)
                End If
            End If

            If configType = ConfigFileType.All Or configType = ConfigFileType.DTRTasks Then
                count = 0
                count = db.GetIntegerValue("SELECT COUNT(*) FROM DTR_Tasks")
                If (count > 0) Then
                    _exportDTRSTasks(backupDoc)
                End If
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.UI_HeatMap Then
                count = 0
                count = db.GetIntegerValue("SELECT COUNT(*) FROM UI_HeatMap")
                If (count > 0) Then
                    _exportUI_HeatMap(backupDoc)
                End If
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.UI_HeatMapLookup Then
                count = 0
                count = db.GetIntegerValue("SELECT COUNT(*) FROM UI_HeatMapLookup")
                If (count > 0) Then
                    _exportUI_HeatMapLookup(backupDoc)
                End If
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.ScreenMenu Then
                count = 0
                count = db.GetIntegerValue("SELECT COUNT(*) FROM ScreenMenu WHERE IsCustomized = 1")
                If (count > 0) Then
                    _exportScreenMenu(backupDoc)
                End If
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.DataSourceTableAction Then
                count = 0
                count = db.GetIntegerValue("SELECT COUNT(*) FROM DataSourceTableAction dta INNER JOIN DataSourceTable t ON dta.TableId = t.Id")
                If (count > 0) Then
                    _exportTableAction(backupDoc)
                End If
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.TableProperties Then
                count = 0
                count = db.GetIntegerValue("SELECT COUNT(*) FROM DatasourceTable")
                If (count > 0) Then
                    _exportTableProperties(backupDoc)
                End If
            End If
            
            If configType = ConfigFileType.All Or configType = ConfigFileType.AggregateFieldSettings Then
                count = 0
                count = db.GetIntegerValue("SELECT COUNT(*) FROM AggregateFieldSettings")
                If (count > 0) Then
                    _exportAggregateFieldSettings(backupDoc)
                End If
            End If
            
            If configType = ConfigFileType.All Or configType = ConfigFileType.MRATemplates Then
                count = 0
                count = db.GetIntegerValue("SELECT COUNT(*) FROM MRA_Templates")
                If (count > 0) Then
                    _exportMraTemplates(backupDoc)
                End If
            End If
            
             If (configType = ConfigFileType.All Or configType = ConfigFileType.RangeLookups) Then
                count = 0
                count = db.GetIntegerValue("SELECT COUNT(*) FROM RangeLookup")
                If (count > 0) Then
                    _exportRangeLookups(backupDoc)
                End If
            End If

            Dim backupDocLen = backupDoc.ToString().Length
            If (backupDocLen > 49) Then 
                tag = "-" + tag
                Dim exportName = IIf(OrgId = -1, HttpContext.Current.GetCAMASession.OrganizationCodeName + tag + "-" + Now.ToString("yyMM-dd-HHmmss"), tag + "-" + Now.ToString("yyMM-dd-HHmmss"))
                Dim currDate As String = DateAndTime.Now.ToString("dd-MM-yyyy HH:mm:ss")
                Dim IPAddress = HttpContext.Current.Request.UserHostAddress
                Dim sqlFCTemplate As String = "INSERT INTO dbo.CC_SettingsBackup (BackupDocument, BackupDate, BackupName,LoginId,IPAddress) VALUES ({0}, {1}, {2},{3},{4})"
                db.Execute(sqlFCTemplate.SqlFormat(True, backupDoc, currDate, "Backup-" + exportName.ToString(), loginId, IPAddress))
                sqlFCTemplate = "INSERT INTO dbo.CC_SettingsBackup (BackupDocument, BackupDate, BackupName,LoginId,IPAddress, ImportFrom) VALUES ({0}, {1}, {2},{3},{4},{5})"
                db.Execute(sqlFCTemplate.SqlFormat(True, xdoc, currDate, "Import-" + exportName.ToString(), loginId, IPAddress, importfrom))
            End If

            ImportSettings(xdoc, configType, loginId, backupDoc)
        End Sub

        Public Sub ImportSettings(ByRef xdoc As XDocument, configType As ConfigFileType, loginId As String, Optional backupDoc As XDocument = Nothing)
            Dim db As Database = target
            If configType = ConfigFileType.All Or configType = ConfigFileType.CSEFieldCategory Then
                processNodeWithNodeIsolation(xdoc, "CSEFieldCategory", AddressOf _importCSEFieldCategory)
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.FieldCategory Then
                processNodeWithNodeIsolationFieldCategory(xdoc, "FieldCategory", AddressOf _importFieldCategory, backupDoc, loginId)
                Dim o As DataRow = Database.System.GetTopRow("SELECT EnableXMLAuditTrail FROM OrganizationSettings WHERE OrganizationId = " & HttpContext.Current.GetCAMASession().OrganizationId)
                If (o.GetBoolean("EnableXMLAuditTrail")) Then
                    db.Execute("INSERT INTO SettingsAuditTrail(EventDate,EventTime,LoginId,Description) VALUES(GETUTCDATE(),GETUTCDATE(),'" + loginId.ToString() + "','Field Settings Updated via XML.')")
                End If
            End If

            If configType = ConfigFileType.All Or configType = ConfigFileType.ApplicationSettings Then
                processNodeWithNodeIsolation(xdoc, "ApplicationSettings", AddressOf _importApplicationSettings)
                processNodeWithNodeIsolation(xdoc, "FieldTracking", AddressOf _importFieldTracking)
            End If

            If configType = ConfigFileType.All Or configType = ConfigFileType.ClientTemplates Then
                processNodeWithNodeIsolation(xdoc, "ClientTemplates", AddressOf _importClientTemplates)
            End If

            If configType = ConfigFileType.All Or configType = ConfigFileType.ClientSettings Then
                processNodeWithNodeIsolation(xdoc, "ClientSettings", AddressOf _importClientSettings)
            End If
            
            If configType = ConfigFileType.All Or configType = ConfigFileType.SketchSettings Then
                processNodeWithNodeIsolation(xdoc, "SketchSettings", AddressOf _importSketchSettings)
            End If

            If configType = ConfigFileType.All Or configType = ConfigFileType.ClientCustomAction Then
                processNodeWithNodeIsolation(xdoc, "ClientCustomAction", AddressOf _importClientCustomAction)
            End If

            If configType = ConfigFileType.All Or configType = ConfigFileType.ClientValidation Then
                processNodeWithNodeIsolation(xdoc, "ClientValidation", AddressOf _importClientValidation)
            End If

            If configType = ConfigFileType.All Or configType = ConfigFileType.AppraisalType Then
                processNodeWithNodeIsolation(xdoc, "AppraisalType", AddressOf _importAppraisalType)
            End If

            If configType = ConfigFileType.All Or configType = ConfigFileType.FieldAlertTypes Then
                processNodeWithNodeIsolation(xdoc, "FieldAlertTypes", AddressOf _importFieldAlertTypes)
            End If

            If configType = ConfigFileType.All Or configType = ConfigFileType.GISSettings Then
                processNodeWithNodeIsolation(xdoc, "GISSettings", AddressOf _importGISSettings)
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.DTRTasks Then
                processNodeWithNodeIsolation(xdoc, "DTRTasks", AddressOf _importDTRTasks)
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.DTRStatusTypes Then
                processNodeWithNodeIsolation(xdoc, "DTRStatusTypes", AddressOf _importDTRStatusTypes)
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.UI_HeatMap Then
                processNodeWithNodeIsolation(xdoc, "UI_HeatMap", AddressOf _importUI_HeatMap)
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.UI_HeatMapLookup Then
                processNodeWithNodeIsolation(xdoc, "UI_HeatMapLookup", AddressOf _importUI_HeatMapLookup)
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.ScreenMenu Then
                processNodeWithNodeIsolation(xdoc, "ScreenMenu", AddressOf _importScreenMenu)
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.DataSourceTableAction Then
                processNodeWithNodeIsolation(xdoc, "DataSourceTableAction", AddressOf _importTableAction)
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.TableProperties Then
                processNodeWithNodeIsolation(xdoc, "TableProperties", AddressOf _importTableProperties)
            End If
            If configType = ConfigFileType.All Or configType = ConfigFileType.AggregateFieldSettings Then
                processNodeWithNodeIsolation(xdoc, "AggregateFieldSettings", AddressOf _importAggregateFields)
            End If
             If configType = ConfigFileType.All Or configType = ConfigFileType.MRATemplates Then
             	processNodeWithNodeIsolation(xdoc, "MRATemplates", AddressOf _importMraTemplates)
             End If
             If configType = ConfigFileType.HeatmapSettings Then
             	_importHeatmapSettings (xdoc)
             End If
             
             If configType = ConfigFileType.All Or configType = ConfigFileType.RangeLookups Then
             	Dim node = xdoc.Root.Elements.Where(Function(x) x.@type = "RangeLookups").FirstOrDefault()
                If node IsNot Nothing Then
                	processNodeWithNodeIsolation(xdoc, "RangeLookups", AddressOf _importRangeLookup)
            	Else Throw New Exception("You have uploaded an invalid Rangelookup file")
                End If 
            End If

             
        End Sub
    End Class

End Namespace
