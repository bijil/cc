﻿Namespace DataAnalyzer
    Public Class QueryFilter
        Public ID, Field, FieldName, [Operator], Value As String

        Public Sub New()

        End Sub

        Public Sub New(whole As String)
            Dim x = whole.Split("`")
            ID = x(0)
            Field = x(1)
            FieldName = x(2)
            Me.Operator = x(3)
            Value = x(4)
        End Sub

        Public Overrides Function ToString() As String
            Return ID + "`" + Field + "`" + FieldName + "`" + [Operator] + "`" + Value
        End Function

        Public Shared Function FromListString(list As String) As List(Of QueryFilter)
            Return list.Split(";").Where(Function(x) x <> "").Select(Function(x) New QueryFilter(x)).ToList
        End Function

        Public Shared Function ToListString(filters As List(Of QueryFilter)) As String
            Return String.Join(";", filters.ToArray.Select(Function(x) x.ToString()))
        End Function
    End Class

End Namespace
