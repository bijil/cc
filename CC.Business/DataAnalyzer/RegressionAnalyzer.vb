﻿Imports System.Web.UI.DataVisualization.Charting
Imports System.Web.UI.WebControls
Imports CAMACloud

Namespace DataAnalyzer
    Public Class RegressionAnalyzer

        Public Shared Function CompileTemplate(templateId As Integer) As Integer
            Return Database.Tenant.GetIntegerValue("EXEC MRA_CompileTemplateFilters " & templateId)
        End Function

        Public Shared Function GetMRATemplate(templateId As Integer) As DataRow
            Dim td As DataRow = Database.Tenant.GetTopRow("SELECT t.*, r.*, f.Name As OutputFieldName, f.DisplayLabel As OutputFieldLabel, f.DataType As OutputFieldType FROM MRA_Templates t LEFT OUTER JOIN MRA_Result r ON t.Id = r.TemplateID LEFT OUTER JOIN DataSourceField f ON f.Id = t.OutputField WHERE t.ID = " & templateId)
            Return td
        End Function

        Public Shared Function GetCoefficients(templateId As Integer) As DataTable
            Return Database.Tenant.GetDataTable("SELECT c.*, f.Name As FieldName, f.DisplayLabel, f.DataType FROM MRA_Coefficients c LEFT OUTER JOIN DataSourceField f ON c.FieldId = f.Id WHERE c.TemplateID = " & templateId)
        End Function

        Public Shared Function GetUsability(usabilityIndex As Double) As Usability
            If usabilityIndex < 0.25 Then
                Return New Usability With {.Name = "Unusable", .Color = Drawing.Color.Red}
            ElseIf usabilityIndex < 0.5 Then
                Return New Usability With {.Name = "Weak", .Color = Drawing.Color.Orange}
            ElseIf usabilityIndex < 0.75 Then
                Return New Usability With {.Name = "Moderate", .Color = Drawing.Color.Blue}
            Else
                Return New Usability With {.Name = "Substantial", .Color = Drawing.Color.Green}
            End If
        End Function

        Public Shared Function EvaluateMultipleRegression(templateId As String, data As DataTable, Optional constantIsZero As Boolean = False, Optional stepWise As Boolean = False, Optional stepWiseFields As String = "") As Integer

            Dim td As DataRow = GetMRATemplate(templateId)
            If td Is Nothing Then
                Return 0
            End If

            constantIsZero = td.GetBoolean("NoIntercept")

            'Dim outputField As Integer = td.GetInteger("OutputField")
            Dim inputSpecs As String = td.GetString("InputFields")

            Dim fields = DataSourceField.GetAllFields

            Dim ch As New Chart
            Dim stats As StatisticFormula = ch.DataManipulator.Statistics

            Dim rowCount As Integer = data.Rows.Count

            Dim N As Integer = data.Columns.Count - 3
            Dim itemLabelIndex As Integer = data.Columns.Count - 2
            Dim y(rowCount - 1) As Double
            Dim x(N, rowCount - 1) As Double
            Dim w(rowCount - 1) As Double

            If constantIsZero Then
                ReDim x(N - 1, rowCount - 1)
            End If

            For i As Integer = 0 To rowCount - 1
                If constantIsZero Then
                    For j As Integer = 1 To N
                        x(j - 1, i) = data(i).Get(j)
                    Next
                Else
                    x(0, i) = 1
                    For j As Integer = 1 To N
                        x(j, i) = data(i).Get(j)
                    Next
                End If


                y(i) = data(i).Get(0)
                w(i) = 1
            Next



			Dim lr As New Statistics.LinearRegression
			Dim checkRegress As Integer = lr.Regress(y, x, w) 
            If checkRegress <> 1 Then
                Return checkRegress     ''return errorcode 
            End If

            'Calculate Overall Mean of Y
            Dim totalY As Double = 0, overallMean As Double
            For i As Integer = 0 To rowCount - 1
                totalY += y(i)
            Next
            overallMean = totalY / rowCount
            Dim ssreg As Double = 0
            Dim ssres As Double = 0
            For i As Integer = 0 To rowCount - 1
                If constantIsZero Then
                    ssreg += Math.Pow(lr.CalculatedValues(i), 2)
                Else
                    ssreg += Math.Pow(overallMean - lr.CalculatedValues(i), 2)
                End If

                ssres += Math.Pow(lr.Residuals(i), 2)
            Next

            Dim dfreg, dfres As Integer
            Dim msreg, msres As Double
            Dim rsquare As Double, adjustedR As Double
            Dim multipleR, standerror As Double
            Dim regf As Double, signf As Double

            dfreg = N
            dfres = rowCount - lr.Coefficients.Count
            msreg = ssreg / dfreg
            msres = ssres / dfres
            rsquare = ssreg / (ssreg + ssres)
            multipleR = Math.Sqrt(rsquare)
            standerror = Math.Sqrt(msres)
            regf = msreg / msres
            adjustedR = (1 - ((dfreg + dfres) / dfres) * (ssres / (ssres + ssreg)))
            signf = stats.FDistribution(msreg / msres, dfreg, dfres)

            'lblRSquare.Text = rsquare.ToString("0.########")
            'lblMultipleR.Text = multipleR.ToString("0.########")
            'lblStandardError.Text = standerror.ToString("0.########")
            'lblObservations.Text = rowCount
            'lblAdjustedRSquare.Text = adjustedR.ToString("0.########")

            'lblSSReg.Text = ssreg.ToString("0.###")
            'lblMSReg.Text = msreg.ToString("0.###")
            'lblSSRes.Text = ssres.ToString("0.###")
            'lblMSRes.Text = msres.ToString("0.###")
            'lblDFReg.Text = dfreg
            'lblDFRes.Text = dfres
            'lblFisher.Text = regf.ToString("0.###")
            'lblSignificanceF.Text = signf.ToString("E4")


            'lblDFTotal.Text = rowCount - lr.Coefficients.Count + N
            'lblSSTotal.Text = (ssreg + ssres).ToString("0.###")
			If stepWise = False Then
            	Dim sqlSaveResult As String = "DELETE FROM MRA_Result WHERE TemplateID = {0}; INSERT INTO MRA_Result (TemplateID, Observations, RSquare, MultipleR, AdjustedRSquare, StandardError, RegressionMeanSquare, ResidualMeanSquare, RegressionSumOfSquares, ResidualSumOfSquares, RegressionDegreeOfFreedom, ResidualDegreeOfFreedom, RegressionF, SignificantF) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13});"
            	sqlSaveResult = sqlSaveResult.SqlFormat(False, templateId, rowCount, rsquare, multipleR, adjustedR, standerror, msreg, msres, ssreg, ssres, dfreg, dfres, regf, signf)
            	Database.Tenant.Execute(sqlSaveResult)
            End If	
            'Dim reg As New DataTable
            'reg.Columns.Add("Head")
            'reg.Columns.Add("CE")
            'reg.Columns.Add("SE")
            'reg.Columns.Add("TS")
            'reg.Columns.Add("PV")

            'Dim fieldSpecs = Database.Tenant.GetStringValue("SELECT InputFields FROM MRA_Templates WHERE ID = " & templateId)
            'Dim inputFields = InputField.FromSpecString(fieldSpecs, fields)
            Dim sqlString As String = ""
            If stepWise = False Then
            	sqlString = "SELECT tf.FieldID, ISNULL(tf.Label, f.DisplayLabel) As Label, ISNULL(tf.Expression, f.Name) As Expression  FROM MRA_TemplateFields tf INNER JOIN DataSourceField f ON tf.FieldID = f.Id WHERE TemplateID = {0} AND IsFilter = 0 ORDER BY tf.Ordinal, tf.ID".FormatString(templateId)
            Else
            	sqlString = "SELECT tf.FieldID, ISNULL(tf.Label, f.DisplayLabel) As Label, ISNULL(tf.Expression, f.Name) As Expression  FROM MRA_TemplateFields tf INNER JOIN DataSourceField f ON tf.FieldID = f.Id WHERE TemplateID = {0} AND IsFilter = 0 AND tf.FieldID in ({1})".FormatString(templateId,stepWiseFields)
			End If
			Dim ifields = Database.Tenant.GetDataTable(sqlString).ToRows.Select(Function(ff) New With {.FieldID = ff.GetInteger("FieldID"), .Label = ff.GetString("Label"), .Expression = ff.GetString("Expression")})
				
            Dim sqlSaveCoEffs As String = "DELETE FROM MRA_Coefficients WHERE TemplateID = " & templateId & "; "
            Dim sqlStepWiseCoeff As String = "DELETE FROM MRA_StepWiseCoefficients WHERE TemplateID = " & templateId & "; "

            For i As Integer = 0 To lr.C.Length - 1
                'Dim rr As DataRow = reg.NewRow
                Dim head As String
                If constantIsZero Then
                    head = data.Columns(i + 1).ColumnName
                Else
                    If i = 0 Then
                        head = "Intercept"
                    Else
                        head = data.Columns(i).ColumnName
                    End If
                End If


                Dim c As Double = lr.Coefficients(i)
                Dim se As Double = lr.CoefficientsStandardError(i)
                Dim tstat = c / se
                Dim pval = stats.TDistribution(Math.Abs(tstat), dfres, False)
                'rr("Head") = head
                'rr("CE") = c.ToString("0.########")
                'rr("SE") = se.ToString("0.########")
                'rr("TS") = tstat.ToString("0.########")
                'rr("PV") = pval.ToString("0.########")
                'reg.Rows.Add(rr)

                Dim ci = If(constantIsZero, i + 1, i)
                Dim fieldId As Integer = -1
                Dim fieldName As String = "Intercept"
                Dim fieldSpec As String = ""
                If ci > 0 Then
                    Dim f = ifields(ci - 1) 'inputFields(ci)
                    fieldId = f.FieldID
                    fieldName = f.Label
                    fieldSpec = f.Expression
                End If

                Dim sqlInsert As String = "INSERT INTO MRA_Coefficients (TemplateID, Ordinal, FieldId, Head, FieldSpecification, Coefficient, StandardError, tStat, pValue) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}); ".SqlFormat(False, templateId, ci, fieldId, fieldName, fieldSpec, c, se, tstat, pval)
                sqlSaveCoEffs += sqlInsert
                Dim sqlSWInsert As String = "INSERT INTO MRA_StepWiseCoefficients (TemplateID, Ordinal, FieldId, Head, FieldSpecification, Coefficient, StandardError, tStat, pValue) VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}); ".SqlFormat(False, templateId, ci, fieldId, fieldName, fieldSpec, c, se, tstat, pval)
                sqlStepWiseCoeff += sqlSWInsert	
            Next
            
            If stepWise = False Then
            	Database.Tenant.Execute(sqlSaveCoEffs)
	            
	            'Dim resd As New DataTable
	            'resd.Columns.Add("ID")
	            'resd.Columns.Add("Item")
	            'resd.Columns.Add("ItemId")
	            'resd.Columns.Add("PR")
	            'resd.Columns.Add("RES")
	
	            Dim sqlSaveResiduals As String = "DELETE FROM MRA_ResidualOutput WHERE TemplateID = {0}; ".FormatString(templateId)
	
	
	
	            Dim sqlResidualInserts As New List(Of String)
	            For i As Integer = 0 To lr.CalculatedValues.Length - 1
	                'Dim rr As DataRow = resd.NewRow
	
	                Dim parcelId As Integer = data.Rows(i)("CC_ParcelId")
	                Dim pr As Double = lr.CalculatedValues(i)
	                Dim res As Double = -lr.Residuals(i)
	                'rr("ID") = i + 1
	                'rr("Item") = data.Rows(i)(itemLabelIndex)
	                'rr("ItemId") = parcelId
	                'rr("PR") = pr.ToString("0.########")
	                'rr("RES") = (res).ToString("0.########")
	                'resd.Rows.Add(rr)
	
	                Dim sqlInsert As String = "INSERT INTO MRA_ResidualOutput (TemplateID, ObservationNo, ParcelId, PredictedValue, Residual) VALUES ({0}, {1}, {2}, {3}, {4});".SqlFormat(False, templateId, i + 1, parcelId, pr, res)
	                sqlResidualInserts.Add(sqlInsert)
	            Next
	
	            Database.Tenant.Execute(sqlSaveResiduals)
	            Dim stepSize As Integer = 1000
	            For i As Integer = 0 To lr.CalculatedValues.Length - 1 Step stepSize
	                Dim batchSql As String = sqlResidualInserts.Skip(i).Take(stepSize).Aggregate(Function(s1, s2) s1 + vbNewLine + s2)
	                Database.Tenant.Execute(batchSql)
	            Next
	
	
	            'residuals.Columns(1).HeaderText = data.Columns(itemLabelIndex).ColumnName
	            'residuals.Columns(2).HeaderText = "Predicted " + data.Columns(0).ColumnName
	
	
	
	            'regresult.DataSource = reg
	            'regresult.DataBind()
	
	            'residuals.DataSource = resd
	            'residuals.DataBind()
	
	            Dim costFormula As String = ""
	            Dim inlineQueryForSql As String = "SELECT 0 As __T"
	            Dim pi As Integer = -1, fn As Integer = 0
	            If Not constantIsZero Then
	                pi += 1
	                costFormula += lr.Coefficients(pi).ToString("0.###")
	            End If
	            If inputSpecs <> "" Then
	                For Each fi In inputSpecs.Split(",")
	                    pi += 1
	                    fn += 1
	
	                    'Dim fld = inputFields(fn)
	                    Dim fld = ifields(fn - 1)
	                    If costFormula <> "" Then costFormula += " + "
	                    Dim coeff = Math.Round(lr.Coefficients(pi), 3)
	                    costFormula += If(coeff < 0, "({0})", "{0}").FormatString(coeff) + " * " + If(fld.Expression.Contains(" "), "({0})", "{0}").FormatString(fld.Expression)
	                    inlineQueryForSql += ", CAST(@{0} AS FLOAT) AS {0}".FormatString(fields.Where(Function(xf) xf.Id = fld.FieldID).First.AssignedName)
	                Next
	            End If
	
	            Dim costFormulaSql As String = "SELECT " + costFormula + " FROM (" + inlineQueryForSql + ") x"
	
	            Database.Tenant.Execute("UPDATE MRA_Templates SET CostFormula = {1}, CostFormulaSQL = {2}, LastExecutionTime = GETUTCDATE() WHERE ID = {0}".SqlFormat(True, templateId, costFormula, costFormulaSql))
	
	            'Updating Calculcation Query in MRA_Templates for MRA Calculation
	            Database.Tenant.Execute("EXEC MRA_TemplateQueryBuilder {0}".SqlFormat(True, templateId))
	
	            'Dim modelUrl As String = "javascript:window.open('popup.model.aspx?params=" + modelParam + "', 'popup', 'width=600,height=500,location=no,toolbar=no');"
	            'hlOpenLink.NavigateUrl = modelUrl
	        Else
            	Database.Tenant.Execute(sqlStepWiseCoeff)
			End If   

            Return 1   ''returns true
        End Function

        Public Shared Function DeleteTemplate(datasetId As Integer) As Boolean
            Dim deleteStatements = "DELETE FROM MRA_TemplateParcels WHERE TemplateID = {0};DELETE FROM MRA_TemplateFields WHERE TemplateID = {0};DELETE FROM MRA_Result WHERE TemplateID = {0};DELETE FROM MRA_ResidualOutput WHERE TemplateID = {0};DELETE FROM MRA_Templates WHERE ID = {0};"

            Dim res = Database.Tenant.Execute(deleteStatements.FormatString(datasetId))
            Return res > 0
        End Function

        Public Shared Function GetReportsData(templateId As Integer, reportName As String, ByRef exportGrid As GridView, applyRange As Boolean, rangeMin As Single, rangeMax As Single, rangeInverse As Boolean) As System.Data.DataTable
            Dim sql As String = "EXEC MRA_GetDataForExport {0}, {1}".SqlFormatString(templateId, reportName)
            If applyRange Then
                sql += ", @RangeFiltered = 1, @RangeMin = {0}, @RangeMax = {1}, @RangeInverse = {2}".SqlFormat(False, rangeMin, rangeMax, rangeInverse)
            End If
            Dim ds = Database.Tenant.GetDataSet(sql)
            Dim fields = ds.Tables(0)
            Dim data = ds.Tables(1)

            exportGrid.Columns.Clear()
            For Each field As System.Data.DataRow In fields.Rows
                Dim bf As New BoundField()
                bf.DataField = field.GetString("DataField")
                bf.HeaderText = field.GetString("Name")
                Dim w = field.GetInteger("Length")
                If w > 0 Then
                    bf.ItemStyle.Width = w
                Else
                    bf.ItemStyle.Width = Unit.Empty
                End If
                Select Case field.GetInteger("Align")
                    Case 2
                        bf.ItemStyle.HorizontalAlign = HorizontalAlign.Right
                    Case 1
                        bf.ItemStyle.HorizontalAlign = HorizontalAlign.Center
                    Case Else
                        bf.ItemStyle.HorizontalAlign = HorizontalAlign.Left
                End Select

                exportGrid.Columns.Add(bf)
            Next
            Return data
        End Function

        Public Shared Function GetCalcReport(CalcId As Integer, reportName As String,  ByRef exportGrid As GridView) As System.Data.DataTable
            Dim sql As String = "EXEC MRA_GetCalcForExport {0}, {1}".SqlFormatString(CalcId, reportName)

            Dim ds = Database.Tenant.GetDataSet(sql)
            Dim fields = ds.Tables(0)
            Dim data = ds.Tables(1)

            exportGrid.Columns.Clear()
            For Each field As System.Data.DataRow In fields.Rows
                Dim bf As New BoundField()
                bf.DataField = field.GetString("DataField")
                bf.HeaderText = field.GetString("Name")
                Dim w = field.GetInteger("Length")
                If w > 0 Then
                    bf.ItemStyle.Width = w
                Else
                    bf.ItemStyle.Width = Unit.Empty
                End If
                Select Case field.GetInteger("Align")
                    Case 2
                        bf.ItemStyle.HorizontalAlign = HorizontalAlign.Right
                    Case 1
                        bf.ItemStyle.HorizontalAlign = HorizontalAlign.Center
                    Case Else
                        bf.ItemStyle.HorizontalAlign = HorizontalAlign.Left
                End Select

                exportGrid.Columns.Add(bf)
            Next
            Return data
        End Function

        Public Shared Function SaveLookupValues(TemplateId As Integer, FieldId As Integer, TargetTable As String, LookupName As String, LookupValues() As String)
            Database.Tenant.Execute("DELETE FROM MRA_LookupValues WHERE TemplateId = {0} AND TemplateFieldID = {1}; DELETE FROM MRA_LookupValueResults WHERE TemplateId = {0} AND TemplateFieldID = {1}; ".SqlFormat(True,TemplateId,FieldId))
            Dim batchSize As Integer = 100
            If LookupValues.Length > 0 Then
                Dim its As Integer = LookupValues.Length \ batchSize
                For i As Integer = 0 To its
                    Dim dataSql = LookupValues.Skip(i * batchSize).Take(100).Select(Function(x) "SELECT {0}, {1}, {2}".SqlFormat(False, TemplateId, FieldId, x)).Aggregate(Function(x, y) x + vbNewLine + " UNION " + vbNewLine + y)
                    Dim insertSql = "INSERT INTO MRA_LookupValues (TemplateId, TemplateFieldID, Value)" + vbNewLine + dataSql
                    Database.Tenant.Execute(insertSql)
                Next
            End If
            Database.Tenant.Execute("UPDATE lv SET lv.NumericValue = pl.Value FROM parcelDatalookup pl inner join MRA_LookupValues lv ON lv.value = pl.IdValue AND pl.LookupName  = {0} AND lv.TemplateId = {1} AND lv.TemplateFieldId = {2}".SqlFormat(True,LookupName,TemplateId,FieldId))
            Dim FieldName As String = Database.Tenant.GetStringValue("SELECT Name FROM Datasourcefield WHERE ID = {0}".SqlFormat(True, FieldId))
            Dim DataSetID As Integer = Database.Tenant.GetIntegerValue("Select DatasetID FROM MRA_Templates Where ID = {0}".SqlFormat(True, TemplateId))
            Dim Aggregate As String = Database.Tenant.GetStringValue("Select ISNULL(Aggregate,'MAX') from MRA_TemplateFields Where TemplateID = {0} And FieldId = {1}".SqlFormat(True, TemplateId, FieldId))
            Dim Sql As String = "INSERT INTO MRA_LookupValueResults(TemplateId,TemplateFieldID,ParcelID,Value) SELECT {0}, {1}, p.Id, lv.NumericValue FROM Parcel p INNER JOIN MRA_DataSetParcels dsp ON p.Id = dsp.ParcelId AND dsp.DataSetId = {2}".FormatString(TemplateId, FieldId, DataSetID)
            Sql += " INNER JOIN (SELECT CC_ParcelID, {0}({1}) As {1} FROM {2} GROUP BY CC_ParcelID) x ON p.Id = x.CC_ParcelId".FormatString(Aggregate, FieldName, TargetTable)
            Dim LookupJoin As String = "INNER JOIN MRA_LookupValues lv ON lv.TemplateID  = {0} AND lv.TemplateFieldID = {1} AND lv.Value = CAST(x.{2} As VARCHAR(MAX))".FormatString(TemplateId, FieldId, FieldName)
            Database.Tenant.Execute(Sql + vbNewLine + LookupJoin)
        End Function
        
        Public Shared Function BuildCorrTables(TemplateId As Integer) As String
        	Dim fields = Database.Tenant.GetDataTable("SELECT FieldID, Label FROM MRA_TemplateFields WHERE TemplateId = {0} AND Isfilter = 0 UNION SELECT OutputField, OutputFieldLabel FROM MRA_Templates Where Id = {0}".SqlFormat(True,TemplateId))
        	Dim tableString As String = ""
        	Dim dataTable As String = "<div id='TableParent'><table id='CorrTable'>"
        	Dim colorTable As String = "<table id='CorrColTable'>"
        	Dim HeaderString As String = "<tr><th color=228,234,236></th>"
        	For Each f As DataRow In fields.Rows
        		Dim Name = f.GetString("Label")
        		HeaderString += "<th color=228,234,236 b='TB'>{0}</th>".FormatString(Name)
        	Next
        	HeaderString += "</tr>"
        	Dim rowString As String = ""
        	Dim ColorString As String = ""
        	Dim i As Integer = 1
        	For Each f As DataRow In fields.Rows
        		Dim ID = f.GetInteger("FieldID")
        		Dim Fname = f.GetString("Label")
        		rowString += "<tr><td color=228,234,236 b ='LR'>{0}</td>".FormatString(Fname)
        		ColorString += "<tr><td color=228,234,236 b ='LR'>{0}</td>".FormatString(Fname)
        		Dim ds = Database.Tenant.GetDataTable("EXEC MRA_GetCorrelationReport {0}, {1}".SqlFormat(True,TemplateId,ID))
        		Dim j As Integer  = 1
        		For Each r As DataRow In ds.Rows
		    		Dim rValue = r.Item("r")
		    		Dim per = Math.Floor(Math.Abs(rValue*255)) + 10
					Dim ColorCode As String = ""  
					Dim s As Integer = 255
					If (rValue >= 0) Then
						ColorCode = "{0},{1},{2}".FormatString(Convert.ToInt32((s-per*0.8)),Convert.ToInt32((s-per*0.8)),Convert.ToInt32((s-per*0.2)))
					Else 
						ColorCode = "{0},{1},{2}".FormatString(Convert.ToInt32((s-per*0.2)),Convert.ToInt32((s-per)),Convert.ToInt32((s-per)))
					End If	
					If j <= i Then
						rowString += "<td>{0}</td>".FormatString(rValue)
						ColorString += "<td color={0}></td>".FormatString(ColorCode)
					Else
						rowString += "<td></td>"
						ColorString += "<td></td>"
					End If
					j += 1
		    	Next
		    	rowString += "</tr>"
		    	ColorString += "</tr>"
		    	i += 1
        	Next
        	DataTable += HeaderString + rowString + "</table>"
        	colorTable +=  HeaderString + ColorString + "</table>"
        	Dim legendTable As String = "<table><tr><td>-1</td><td color=202,43,43></td><td color=206,81,98></td><td color=190,129,146></td><td color=255,238,238></td><td>0</td><td color=238,238,255></td><td color=129,146,190></td><td color=81,98,206></td><td color=43,43,202></td><td>1</td></tr></table>"
        	tableString += DataTable + colorTable + legendTable + "</div>"
        	
        	Return tableString
        End Function

        Public Class Usability
            Public Property Name As String
            Public Property Color As Drawing.Color
        End Class

    End Class

End Namespace

