﻿Namespace DataAnalyzer
    Public Class InputField
        Public Property FieldID As Integer
        Public Property Label As String
        Public Property Expression As String
        Public Property Ordinal As Integer
		Public ID, Field, FieldName, DisplayLabel As String

    	Public Sub New()

    	End Sub

    Public Sub New(whole As String)
        Dim x = whole.Split("~")
        ID = x(0)
        DisplayLabel = x(1)
        FieldName = x(2)
       ' Value = x(3)
    End Sub
    
    Public Overrides Function ToString() As String
            Return ID + "~" + DisplayLabel + "~" + FieldName 
        End Function
    
    Public Shared Function FromInputString(list As String) As List(Of InputField)
        Return list.Split(",").Where(Function(x) x <> "").Select(Function(x) New InputField(x)).ToList
    End Function
    
    Public Shared Function ToInputString(inputs As List(Of InputField)) As String
            Return String.Join(",", inputs.ToArray.Select(Function(x) x.ToString()))
        End Function
    
        Public Shared Function FromSpecString(inputSpec As String, fields As List(Of DataSourceField)) As Dictionary(Of Integer, InputField)
            Dim d As New Dictionary(Of Integer, InputField)
            Dim i As Integer = 1
            For Each spec In inputSpec.Split(",")
                Dim parts = spec.Split("~")
                If IsNumeric(parts(0)) Then
                    Dim fid = Integer.Parse(parts(0))
                    Dim label, formula As String
                    If parts.Length = 1 Then
                        Dim f = fields.Where(Function(x) x.Id = fid).First
                        label = f.DisplayLabel
                        formula = f.AssignedName
                    Else
                        label = parts(1)
                        formula = parts(2).Replace("`", ",")
                    End If
                    Dim fld As New InputField With {.Ordinal = i, .FieldID = fid, .Label = label, .Expression = formula}
                    d.Add(i, fld)
                    i += 1
                End If
            Next

            Return d
        End Function
    End Class

End Namespace
