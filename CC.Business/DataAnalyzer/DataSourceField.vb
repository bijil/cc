﻿Namespace DataAnalyzer
    Public Class DataSourceField

        Public Shared Function GetFields() As List(Of DataSourceField)
            Return Database.Tenant.GetDataTable("SELECT f.Id, DisplayLabel, AssignedName, DataType, LookupTable, t.ImportType, t.Id As TableId, t.Name As TableName FROM DataSourceField f LEFT OUTER JOIN DataSourceTable t ON f.TableId = t.Id WHERE CategoryId IS NOT NULL").AsEnumerable.Select(Function(x) New DataSourceField With {.Id = x("Id"), .AssignedName = x("AssignedName"), .DisplayLabel = x("DisplayLabel"), .InputType = x("DataType"), .LookupTable = x.GetString("LookupTable"), .ImportType = x("ImportType"), .TableId = x("TableId"), .TableName = x("TableName")}).ToList
        End Function
        
        Public Shared Function GetAllFields() As List(Of DataSourceField)
            Return Database.Tenant.GetDataTable("SELECT f.Id, DisplayLabel, AssignedName, DataType, LookupTable, t.ImportType, t.Id As TableId, t.Name As TableName FROM DataSourceField f LEFT OUTER JOIN DataSourceTable t ON f.TableId = t.Id").AsEnumerable.Select(Function(x) New DataSourceField With {.Id = x("Id"), .AssignedName = x.GetString("AssignedName"), .DisplayLabel = x.GetString("DisplayLabel"), .InputType = x("DataType"), .LookupTable = x.GetString("LookupTable"), .ImportType = x("ImportType"), .TableId = x("TableId"), .TableName = x.GetString("TableName")}).ToList
        End Function
        
        Public Id, InputType, ImportType, TableId As Integer
        Public DisplayLabel, AssignedName, LookupTable, TableName As String

        Public ReadOnly Property JoinId As String
            Get
                If LookupTable IsNot Nothing Then
                    Return "l" & Id
                End If
                If ImportType = 1 Then
                    Return "au" & TableId
                End If
                Return "f" & Id
            End Get
        End Property

        Public ReadOnly Property AuxJoinId As String
            Get
                Return "au" & TableId
            End Get
        End Property

    End Class

End Namespace

