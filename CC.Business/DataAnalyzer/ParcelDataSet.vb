﻿Namespace DataAnalyzer
    Public Class Dataset

        Public Property ID As Integer
        Public Property Name As String
        Public Property PoolSize As Integer
        Public Property IsEmpty As Boolean
        Public Property [Error] As Boolean
        Public Property Groups As List(Of DataSetGroupItem)

        Public Shared Function Rename(datasetId As Integer, name As String) As String
            Dim currentName As String = Database.Tenant.GetStringValue("SELECT Name FROM MRA_DataSet WHERE ID = " & datasetId)
            If name IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(name) AndAlso name.Trim.ToLower <> currentName.Trim.ToLower Then
                name = name.Trim
                If Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM MRA_DataSet WHERE Name = " + name.ToSqlValue) > 0 Then
                    Throw New Exception("Another dataset exists with the same name.")
                End If
                Database.Tenant.GetStringValue("UPDATE MRA_DataSet SET Name = {1} WHERE ID = {0}".SqlFormat(False, datasetId, name))
                currentName = name
            End If
            Return currentName
        End Function

        Public Shared Function GetDataSet(datasetId As Integer) As Dataset
        	Dim ds = Database.Tenant.GetDataTable("SELECT *, CAST(CASE WHEN PoolSize IS NULL THEN 1 ELSE 0 END AS BIT) As IsEmpty FROM MRA_DataSet WHERE ID = " & datasetId).ConvertToList(Of Dataset).FirstOrDefault
            If ds IsNot Nothing Then
            	ds.Groups = Database.Tenant.GetDataTable("EXEC MRA_GetDataGroupsForSet " & datasetId).ConvertToList(Of DataSetGroupItem)
            End If
            Return ds
        End Function

        Public Shared Function SaveLookupValues(datasetId As Integer, groupId As Integer, selectAction As String, includeNull As String, selection() As String) As Boolean
        	Dim selSize As Integer = 0
        	Dim IncNull As Integer = 0
            If selectAction = "-1" Then
                selSize = -1
            Else
                selSize = selection.Count
            End If
            If includeNull = "1" Then
            	IncNull = 1
            End If

            Database.Tenant.Execute("IF NOT EXISTS(SELECT * FROM MRA_DataSetGroups WHERE DataSetID = {0} AND GroupID = {1}) INSERT INTO MRA_DataSetGroups (DataSetID, GroupID, SelectedSize,IncludeNull) VALUES ({0}, {1}, {2}, {3})".SqlFormat(False, datasetId, groupId, selSize, IncNull))
            Dim dgid As Integer = 0

            dgid = Database.Tenant.GetIntegerValue("SELECT ID FROM MRA_DataSetGroups WHERE DataSetID = {0} AND GroupID = {1};".SqlFormat(False, datasetId, groupId))

            Database.Tenant.Execute("UPDATE MRA_DataSetGroups SET SelectedSize = {1},IncludeNull = {2} WHERE ID = {0}".SqlFormat(True, dgid, selSize,IncNull))

            Database.Tenant.Execute("DELETE FROM MRA_DataSetGroupValues WHERE DataSetGroupID = " & dgid)

            Dim batchSize As Integer = 100

			If selSize = -1 Or selection.Length > 0 Then
                Dim its As Integer = selection.Length \ batchSize
                For i As Integer = 0 To its
                    Dim dataSql = selection.Skip(i * batchSize).Take(100).Select(Function(x) "SELECT {0}, {1}".SqlFormat(False, dgid, x)).Aggregate(Function(x, y) x + vbNewLine + " UNION " + vbNewLine + y)
                    Dim insertSql = "INSERT INTO MRA_DataSetGroupValues (DataSetGroupID, Value) " + vbNewLine + dataSql
                    Database.Tenant.Execute(insertSql)
                Next
            End If

            CompileDataset(datasetId)

            Return True
        End Function

        Public Shared Function CompileDataset(datasetId As Integer) As Integer
        	Dim rowsAffected As Integer
        	Dim lookupJoin As String = vbNewLine + "INNER JOIN ParcelDataLookup pl{0} ON pl{0}.LookupName = {2} AND pd.{1} = pl{0}.IdValue"
            Dim lookupGroupJoin As String = vbNewLine + "LEFT JOIN MRA_DataSetGroupValues gv_{0} ON gv_{0}.DataSetGroupId = {0} AND gv_{0}.Value = pd.{1}"
            Dim neighborhoodJoin As String = vbNewLine + "LEFT JOIN MRA_DataSetGroupValues gv_n ON gv_n.DataSetGroupId = {0} AND gv_n.Value = p.NeighborhoodId"
			Dim CheckForNoneSelect As Integer = Database.Tenant.GetIntegerValue("SELECT count(*) from MRA_DataSetGroups where DatasetId = {0} and SelectedSize = 0 AND IncludeNull = 0".SqlFormat(False, datasetId)) 
			Dim IncludeWhereCondition As String = vbNewLine + "where"
			Dim IncludeNullCondition As String = ""
			Dim NeighborhoodWhere As String = ""
			
			Dim sql As String = "SELECT DISTINCT {0}, p.Id FROM Parcel p INNER JOIN ParcelData pd ON p.Id = pd.CC_ParcelId".SqlFormat(False, datasetId)
			Database.Tenant.Execute("DELETE FROM MRA_DataSetParcels WHERE DataSetId = " & datasetId)
			If CheckForNoneSelect = 0 Then
	            For Each gr As DataRow In Database.Tenant.GetDataTable("SELECT dg.*, g.LookupName, g.FieldID, f.Name As FieldName FROM MRA_DataSetGroups dg LEFT OUTER JOIN MRA_DataGroups g LEFT OUTER JOIN DataSourceField f ON g.FieldID = f.Id ON dg.GroupID = g.ID WHERE dg.DataSetID = " & datasetId).Rows
	                Dim groupId = gr.GetInteger("GroupId")
	                Dim dsgid = gr.GetInteger("ID")
	                Dim IncludNull = gr.getBoolean("IncludeNull")
	                If groupId = -1 Then
	                	sql += neighborhoodJoin.FormatString(dsgid)
	                	If IncludNull = True Then
	  						NeighborhoodWhere + = "(gv_n.Value IS NOT NULL OR p.NeighborhoodId IS NULL)"
	                	Else
	                		NeighborhoodWhere + = "(gv_n.Value IS NOT NULL)"
	                	End If
	                Else
	                    sql += lookupGroupJoin.FormatString(dsgid, gr.GetString("FieldName").Wrap("[]"))
	                End If
	                
	                If groupId <> -1 Then
	                  If IncludNull = True Then
	                	If IncludeNullCondition = "" Then
		               		IncludeNullCondition + = vbNewLine + "( gv_{0}.Value  IS NOT NULL OR  pd.{1} IS NULL)".FormatString(dsgid, gr.GetString("FieldName").Wrap("[]"))
		               	Else
		               		IncludeNullCondition + = vbNewLine + "AND ( gv_{0}.Value  IS NOT NULL OR  pd.{1} IS NULL)".FormatString(dsgid, gr.GetString("FieldName").Wrap("[]"))
		               	End If
	               	  Else
		               	If IncludeNullCondition = "" Then
		               		IncludeNullCondition + = vbNewLine + "( gv_{0}.Value IS NOT NULL)".FormatString(dsgid)
		               	Else
		               		IncludeNullCondition + = vbNewLine + " AND ( gv_{0}.Value IS NOT NULL)".FormatString(dsgid)
		               	End If 
	               	End If
	              End If  	
	            Next
	            If IncludeNullCondition = "" Then
	            	IncludeNullCondition + = vbNewLine + NeighborhoodWhere
	            Else
	            	IncludeNullCondition + = vbNewLine + "AND " + NeighborhoodWhere
	            End If
	            IncludeWhereCondition + = IncludeNullCondition
	            Dim finalSql = "INSERT INTO MRA_DataSetParcels (DataSetId, ParcelId) " + vbNewLine + sql + vbNewLine + IncludeWhereCondition + vbNewLine + "SELECT CAST(@@ROWCOUNT AS INT) As RowsAffected;"
	            rowsAffected = Database.Tenant.GetIntegerValue(finalSql)
			Else
				rowsAffected = 0
			End IF
            Database.Tenant.Execute("UPDATE MRA_DataSet SET PoolSize = {1}, Error = 0 WHERE ID = {0}".SqlFormat(False, datasetId, rowsAffected))

            Return rowsAffected
        End Function

        Public Shared Function DeleteDataset(datasetId As Integer) As Boolean
            Dim deleteStatements = "DELETE FROM MRA_DataSetParcels WHERE DataSetId = {0};
                                    DELETE FROM MRA_DataSetGroupValues WHERE DataSetGroupID IN (SELECT ID FROM MRA_DataSetGroups WHERE DataSetID = {0});
                                    DELETE FROM MRA_DataSetGroups WHERE DataSetID = {0};
                                    DELETE FROM MRA_DataSet WHERE ID = {0};"

            Dim res = Database.Tenant.Execute(deleteStatements.FormatString(datasetId))
            Return res > 0
        End Function

    End Class

    Public Class DataSetGroupItem
        Public Property GroupID As Integer
        Public Property Label As String
        Public Property Items As Integer
        Public Property IncNull As Integer	
        Public Property SelectionText As String
        Public Property LookupName As String
    End Class
End Namespace
