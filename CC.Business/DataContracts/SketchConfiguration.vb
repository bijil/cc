﻿Imports System.Runtime.Serialization

Namespace Installation

    Public Class SketchConfiguration
        <DataMember()> Public Property formatter As String
        <DataMember()> Public Property sources As List(Of SketchConfigSource)
    End Class

    Public Class SketchConfigSource
        <DataMember()> Public Property SketchSource As SketchSource
        <DataMember()> Public Property VectorSource As VectorSource
        <DataMember()> Public Property NotesSource As NotesSource
    End Class

    Public Class SketchSource
        <DataMember()> Public Property Table As String
    End Class

    Public Class VectorSource
        <DataMember()> Public Property Table As String
    End Class

    Public Class NotesSource
        <DataMember()> Public Property Table As String
    End Class

End Namespace
