﻿Imports CAMACloud.Sketching.Converters
Imports System.Threading
Imports System.Drawing.Imaging

Public Class BulkConverter

    'Dim jc As JPEGConverter

    Private Sub BulkConverter_Load(sender As Object, e As EventArgs) Handles Me.Load
        If My.Settings.LastSketch <> "" Then
            Try
                txtSketchString.Text = My.Settings.LastSketch
                PreviewSketch()
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try

        End If

    End Sub

    Private Sub btnConvert_Click(sender As Object, e As EventArgs) Handles btnConvert.Click

        If txtSketchString.Text.Trim <> "" Then
            My.Settings.LastSketch = txtSketchString.Text.Trim
            My.Settings.Save()
            PreviewSketch()
        End If

    End Sub

    Sub PreviewSketch()
        Debug.WriteLine("STARTING CONVERSION")
        Dim bmp = SketchRenderer.RenderBitmap(txtSketchString.Text.Trim, "TEST PARCEL", "IMPRV: 00000")
        preview.Image = bmp

        Dim jpgEncoder As ImageCodecInfo = GetEncoder(ImageFormat.Jpeg)
        Dim encoderParams As New EncoderParameters(1)
        Dim jpegQuality As System.Drawing.Imaging.Encoder = System.Drawing.Imaging.Encoder.Quality
        Dim qualityParameter As New EncoderParameter(jpegQuality, 100L)
        encoderParams.Param(0) = qualityParameter

        bmp.Save("D:\test.jpg", jpgEncoder, encoderParams)

        lblSize.Text = bmp.Width & "x" & bmp.Height
    End Sub

    Private Function GetEncoder(ByVal format As ImageFormat) As ImageCodecInfo

        Dim codecs As ImageCodecInfo() = ImageCodecInfo.GetImageDecoders()
        Dim codec As ImageCodecInfo
        For Each codec In codecs
            If codec.FormatID = format.Guid Then
                Return codec
            End If
        Next codec
        Return Nothing

    End Function

End Class
