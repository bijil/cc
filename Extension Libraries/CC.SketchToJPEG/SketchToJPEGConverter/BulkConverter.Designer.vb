﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BulkConverter
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnConvert = New System.Windows.Forms.Button()
        Me.preview = New System.Windows.Forms.PictureBox()
        Me.txtSketchString = New System.Windows.Forms.TextBox()
        Me.lblSize = New System.Windows.Forms.Label()
        CType(Me.preview, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnConvert
        '
        Me.btnConvert.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnConvert.Location = New System.Drawing.Point(699, 12)
        Me.btnConvert.Name = "btnConvert"
        Me.btnConvert.Size = New System.Drawing.Size(75, 23)
        Me.btnConvert.TabIndex = 0
        Me.btnConvert.Text = "Preview"
        Me.btnConvert.UseVisualStyleBackColor = True
        '
        'preview
        '
        Me.preview.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.preview.BackColor = System.Drawing.Color.Silver
        Me.preview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.preview.Location = New System.Drawing.Point(12, 65)
        Me.preview.Name = "preview"
        Me.preview.Size = New System.Drawing.Size(778, 360)
        Me.preview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.preview.TabIndex = 1
        Me.preview.TabStop = False
        '
        'txtSketchString
        '
        Me.txtSketchString.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSketchString.Location = New System.Drawing.Point(13, 12)
        Me.txtSketchString.Multiline = True
        Me.txtSketchString.Name = "txtSketchString"
        Me.txtSketchString.Size = New System.Drawing.Size(680, 47)
        Me.txtSketchString.TabIndex = 2
        Me.txtSketchString.Text = "SKETCH[-1,-1]:U10/R10 S U20 R20 D20 L20" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'lblSize
        '
        Me.lblSize.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSize.AutoSize = True
        Me.lblSize.Location = New System.Drawing.Point(699, 38)
        Me.lblSize.Name = "lblSize"
        Me.lblSize.Size = New System.Drawing.Size(16, 13)
        Me.lblSize.TabIndex = 3
        Me.lblSize.Text = "..."
        '
        'BulkConverter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(802, 437)
        Me.Controls.Add(Me.lblSize)
        Me.Controls.Add(Me.txtSketchString)
        Me.Controls.Add(Me.preview)
        Me.Controls.Add(Me.btnConvert)
        Me.Name = "BulkConverter"
        Me.Text = "Sketch Preview"
        CType(Me.preview, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnConvert As Button
    Friend WithEvents preview As PictureBox
    Friend WithEvents txtSketchString As TextBox
    Friend WithEvents lblSize As Label
End Class
