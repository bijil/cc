﻿Imports Microsoft.VisualBasic
Imports System.Runtime.CompilerServices
Imports System.Web.UI.WebControls

Friend Module StringExtensions

    Public Function Coalesce(ParamArray objects()) As Object
        For Each o In objects
            If o IsNot Nothing Then
                Return o
            End If
        Next
        Return Nothing
    End Function


    <Extension()> Public Function Wrap(ByVal source As String, wrapper As String) As String
        If wrapper.IsEmpty Then
            Return source
        End If
        Select Case wrapper
            Case "<", "<>"
                Return "<" + source + ">"
            Case "[", "[]"
                Return "[" + source + "]"
            Case "{", "{}"
                Return "{" + source + "}"
            Case "(", "()"
                Return "(" + source + ")"
            Case Else
                Return wrapper + source + wrapper
        End Select
    End Function

    <Extension()> Public Function Append(ByRef source As String, str As String, Optional delimiter As String = "", Optional wrapper As String = "") As String
        source += IIf(String.IsNullOrEmpty(source), "", delimiter) + str.Wrap(wrapper)
        Return source
    End Function

    <Extension()> Public Function AppendLine(ByRef source As String, str As String, Optional indentLevel As Integer = 0) As String
        Dim indent As String = Space(indentLevel).Replace(" ", vbTab)
        source += IIf(String.IsNullOrEmpty(source), "", vbNewLine) + indent + str.Replace(vbNewLine, vbNewLine + indent)
        Return source
    End Function

    <Extension()> Public Function ToSqlValue(ByVal source As String, Optional ByVal allowNull As Boolean = False) As String
        If allowNull AndAlso (source Is Nothing OrElse source.Trim = "") Then
            Return "null"
        End If
        Return "'" + source.Replace("'", "''") + "'"
    End Function

    <Extension()> Public Function ToSqlValue(Of T)(ByVal source As String, Optional ByVal allowNull As Boolean = False) As String
        If allowNull AndAlso source.Trim = "" Then
            Return "null"
        End If
        If GetType(T) Is GetType(Single) Then
            Dim s As Single = 0
            If Not Single.TryParse(source, s) Then
                Return 0
            End If
        End If
        Return "'" + source.Replace("'", "''") + "'"
    End Function

    <Extension()> Public Function ToValue(Of T)(ByVal source As String) As String
        If GetType(T) Is GetType(Single) Then
            Dim s As Single = 0
            If Not Single.TryParse(source, s) Then
                Return 0
            End If
        End If
        Return source
    End Function

    <Extension()> Public Function IsEmpty(ByVal source As String) As Boolean
        Return String.IsNullOrEmpty(source)
    End Function

    <Extension()> Public Function IsNotEmpty(ByVal source As String) As Boolean
        Return Not String.IsNullOrEmpty(source)
    End Function

    <Extension()> Public Function ContainsData(ByVal source As String, ByVal target As String) As Boolean
        Return Not target.IsEmpty AndAlso source.ToLower.Contains(target.ToLower)
    End Function

    <Extension()> Public Function FormatString(ByVal source As String, ByVal ParamArray inputs() As String) As String
        Return String.Format(source, inputs)
    End Function

    <Extension()> Public Function SqlFormat(ByVal source As String, allowNulls As Boolean, ByVal ParamArray inputs() As Object) As String
        Dim params(inputs.Length - 1) As String
        Dim i As Integer = 0
        For Each o In inputs
            If TypeOf o Is TextBox Then
                params(i) = DirectCast(o, TextBox).Text.ToSqlValue(allowNulls)
            ElseIf TypeOf o Is HiddenField Then
                params(i) = DirectCast(o, HiddenField).Value.ToSqlValue(allowNulls)
            ElseIf TypeOf o Is DropDownList Then
                params(i) = DirectCast(o, DropDownList).SelectedValue.ToSqlValue(allowNulls)
            ElseIf TypeOf o Is CheckBox Then
                params(i) = DirectCast(o, CheckBox).Checked.GetHashCode
            ElseIf TypeOf o Is Decimal Then
                params(i) = DirectCast(o, Decimal).ToString()
            ElseIf TypeOf o Is Integer Then
                params(i) = DirectCast(o, Integer).ToString
                'ElseIf TypeOf o Is Date Or TypeOf o Is DateTime Then
                '    params(i) = DirectCast(o, DateTime).ToSqlValue(allowNulls)
            ElseIf TypeOf o Is Boolean Then
                params(i) = DirectCast(o, Boolean).GetHashCode
            ElseIf o Is Nothing Then
                params(i) = "".ToSqlValue(allowNulls)
            Else
                params(i) = o.ToString().ToSqlValue(allowNulls)
            End If

            If (params(i) Is Nothing) Then
                Throw New Exception("Parameter " + (i + 1).ToString() + " is nothing.")
            End If

            i += 1
        Next

        Return String.Format(source, params)
    End Function

    <Extension()> Public Function SqlFormatString(ByVal source As String, ByVal ParamArray inputs() As String) As String
        Dim i
        Try
            Dim params(inputs.Length - 1) As String
            For i = 0 To inputs.Length - 1
                params(i) = inputs(i).ToSqlValue
            Next

            Return String.Format(source, params)
        Catch ex As Exception
            Throw New Exception("Parameter " & i & " is null.")
        End Try
    End Function

    <Extension()> Public Function SqlFormatStringWithNulls(ByVal source As String, ByVal ParamArray inputs() As String) As String
        Dim params(inputs.Length - 1) As String
        For i As Integer = 0 To inputs.Length - 1
            If inputs(i) = "" Then
                params(i) = "NULL"
            Else
                params(i) = inputs(i).ToSqlValue
            End If
        Next

        Return String.Format(source, params)
    End Function
    <Extension()> Public Function NullIf(ByVal source As String, ByVal checkValue As String) As String
        If source = checkValue Then
            Return "null"
        Else
            Return source
        End If
    End Function


    <Extension()> Public Function NullIfBlank(ByVal source As String) As String
        If source = String.Empty Or source.Trim = "" Then
            Return "null"
        Else
            Return source
        End If
    End Function

    <Extension()> Public Function ReverseString(ByVal source As String) As String
        Dim carray = source.ToCharArray
        Dim reverse As String = String.Empty
        For i = carray.Length - 1 To 0 Step -1
            reverse += carray(i)
        Next
        Return reverse
    End Function

End Module
