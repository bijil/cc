﻿Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Drawing.Imaging

Public Module ConversionUnits
    Public Const DEFAULT_PPF As Single = 10
    Public Property PPF As Single

    Public Const MIN_IMAGE_SIZE As Single = 600
    Public Const MAX_IMAGE_SIZE As Single = 800
End Module

Public Class SketchRenderer


    Public Shared RoundOffScale As Integer = 2

    Public Shared Function RenderBitmap(sketchString As String, Optional parcelLabel As String = "", Optional entityLabel As String = "") As Bitmap
        PPF = DEFAULT_PPF

        Dim segments = sketchString.Split(";").Where(Function(x) x.Trim <> "").Select(Function(x) New SketchSegment(x))


        Dim vectorBrush As New SolidBrush(Color.Black)
        Dim linePen As New Pen(vectorBrush, 2)

        Dim crossHairBrush As New SolidBrush(Color.Red)
        Dim chPen As New Pen(crossHairBrush, 1)

        Dim bounds As New RectangleF(0, 0, 0, 0)
        Dim n As Integer = 0
        'Dim debugFormatter = "{0}" + vbTab + "->  P: ({1,4},{2,4})" + vbTab + vbTab + "S: {3,4}x{4,4}" + vbTab + vbTab + "// {5}"
        For Each s In segments
            n += 1
            Dim r = s.GetBounds
            'Debug.WriteLine(String.Format(debugFormatter, n, r.X, r.Y, r.Width, r.Height, s.Label))
            bounds.X = Math.Min(bounds.X, r.X)
            bounds.Y = Math.Max(bounds.Y, r.Y)
            bounds.Width = Math.Max(bounds.Right, r.Right) - Math.Min(bounds.Left, r.Left)
            'bounds.Height = Math.Max(bounds.Bottom, r.Bottom) - Math.Min(bounds.Top, r.Top)
            bounds.Height = Math.Max(bounds.Bottom, r.Bottom) - Math.Max(bounds.Top, r.Top)
            'Debug.WriteLine(String.Format(debugFormatter, "**", bounds.X, bounds.Y, bounds.Width, bounds.Height, "**"))
        Next
        bounds.Width = Math.Max(480, bounds.Width)
        bounds.Height = Math.Max(480, bounds.Height)
        'Debug.WriteLine(bounds.ToString)
        'Debug.WriteLine("=============================================================")
        'Debug.WriteLine(String.Format(debugFormatter, "B", bounds.X, bounds.Y, bounds.Width, bounds.Height, "=="))

        bounds.Width = bounds.Width - bounds.X
        bounds.Height = bounds.Height + bounds.Y

        Dim boundSize As Integer = Math.Max(bounds.Width, bounds.Height)
        Dim reductionPercent As Single = 1.0
        If boundSize > MAX_IMAGE_SIZE Then
            reductionPercent = MAX_IMAGE_SIZE / boundSize

            PPF = PPF * reductionPercent
            bounds.Width = bounds.Width * reductionPercent
            bounds.Height = bounds.Height * reductionPercent
        End If
        Dim fontExtra As Integer = 0
        If boundSize > 1000 Then
            fontExtra = 4
        ElseIf boundSize > 500 Then
            fontExtra = 2
        End If


        Dim margin As New Point(8 * DEFAULT_PPF, 8 * DEFAULT_PPF)
        Dim border As New Point(4 * DEFAULT_PPF, 4 * DEFAULT_PPF)

        Dim size As New Size(bounds.Width + margin.X * 2, bounds.Height + margin.Y * 2 + 20)

        Dim bmp As New Bitmap(size.Width, size.Height)
        Dim gr As Graphics = Graphics.FromImage(bmp)
        gr.FillRectangle(Brushes.White, 0, 0, size.Width, size.Height)
        gr.CompositingQuality = CompositingQuality.HighQuality
        gr.SmoothingMode = SmoothingMode.HighQuality
        gr.DrawRectangle(Pens.LightBlue, border.X, border.Y, (margin.X - border.X) * 2 + bounds.Width, (margin.Y - border.Y) * 2 + bounds.Height)
        gr.DrawRectangle(Pens.LightBlue, border.X + 2, border.Y + 2, (margin.X - border.X) * 2 + bounds.Width - 4, (margin.Y - border.Y) * 2 + bounds.Height - 4)

        gr.DrawString(parcelLabel, getFont(3, True), Brushes.Black, New PointF(border.X + 2, bounds.Height + margin.Y + border.Y + 4))
        gr.DrawString(entityLabel, getFont(2), Brushes.Black, New PointF(border.X + 2, bounds.Height + margin.Y + border.Y + 4 + 20))

        Dim origin = New Point(margin.X - bounds.X, bounds.Height + margin.Y - bounds.Y)

        Dim chLength As Integer = size.Width * 2 / 100
        Dim chLT As New Point(border.X + 2, border.Y + 2)
        Dim chRB As New Point(size.Width - border.X - 2, bounds.Height + margin.Y + border.Y - 2)
        gr.DrawLine(chPen, chLT.X, origin.Y, chLT.X + chLength, origin.Y)
        gr.DrawLine(chPen, chRB.X, origin.Y, chRB.X - chLength, origin.Y)
        gr.DrawLine(chPen, origin.X, chLT.Y, origin.X, chLT.Y + chLength)
        gr.DrawLine(chPen, origin.X, chRB.Y, origin.X, chRB.Y - chLength)
        gr.DrawLine(chPen, origin.X - chLength, origin.Y, origin.X + chLength, origin.Y)
        gr.DrawLine(chPen, origin.X, origin.Y - chLength, origin.X, origin.Y + chLength)
        'gr.DrawLine(Pens.LightCoral, origin.X, border.Y + 2, origin.X, bounds.Height + margin.Y + border.Y - 2)
        'gr.DrawLine(Pens.LightCoral, border.X + 2, origin.Y, size.Width - border.X - 2, origin.Y)

        Dim labelFont = New Font(SystemFonts.DefaultFont.FontFamily, SystemFonts.DefaultFont.Size + 2 + fontExtra, FontStyle.Bold)
        Dim areaFont = New Font(SystemFonts.DefaultFont.FontFamily, SystemFonts.DefaultFont.Size + 1 + fontExtra, FontStyle.Regular)
        Dim dimensionFont = New Font(SystemFonts.DefaultFont.FontFamily, SystemFonts.DefaultFont.Size + 1 + fontExtra, FontStyle.Regular)

        For Each s In segments
            Dim sp As New Point(origin.X, origin.Y)
            For Each vp In s.StartCommands
                sp.Offset(vp.GraphOffset.X, vp.GraphOffset.Y)
            Next

            Dim coordinates As New List(Of PointF)
            coordinates.Add(New PointF(sp.X, sp.Y))

            Dim ep As New Point(sp.X, sp.Y)
            For Each vp In s.ShapeCommands
                ep.Offset(vp.GraphOffset.X, vp.GraphOffset.Y)
                coordinates.Add(New PointF(ep.X, ep.Y))

                'Debug.WriteLine("{0} -> {1}", sp, ep)
                gr.DrawLine(linePen, sp, ep)

                Dim text As String = vp.Length
                Dim textSize As SizeF = gr.MeasureString(text, dimensionFont)
                Dim lineCenter As New PointF((sp.X + ep.X) / 2, (sp.Y + ep.Y) / 2)
                Dim angle = vp.Angle
                'text = angle
                If angle > -45 And angle <= 45 Then
                    'LEFT -> RIGHT
                    lineCenter.X -= textSize.Width / 2
                    lineCenter.Y += 3
                ElseIf angle > 45 And angle <= 135 Then
                    'TOP TO BOTTOM
                    lineCenter.X -= textSize.Width + 2
                    lineCenter.Y -= textSize.Height / 2
                ElseIf angle > 135 Or angle < -135 Then
                    'RIGHT TO LEFT
                    lineCenter.X -= textSize.Width / 2
                    lineCenter.Y -= textSize.Height + 2
                Else
                    'BOTTOM TO TOP
                    lineCenter.X += 2
                    lineCenter.Y -= textSize.Height / 2
                End If

                gr.DrawString(text, dimensionFont, Brushes.Black, lineCenter)
                sp = ep
            Next

            Dim cpoint = New Point(0, 0)
            Dim coords = coordinates.ToArray
            Dim s1 As Single = 0, s2 As Single = 0
            For i As Integer = 0 To coords.Length - 2
                s1 += coords(i).X * coords(i + 1).Y / (PPF * PPF)
                s2 += coords(i).Y * coords(i + 1).X / (PPF * PPF)
                cpoint.X += coords(i).X
                cpoint.Y += coords(i).Y
            Next
            If coords.Length > 1 Then
                cpoint.X /= (coords.Length - 1)
                cpoint.Y /= (coords.Length - 1)
            End If

            Dim area = Math.Abs(s1 - s2) / 2
            Dim areaText As String = "[" + Math.Round(area, RoundOffScale).ToString + "]"


            Dim labelSize = gr.MeasureString(s.Label, labelFont)
            Dim areaSize = gr.MeasureString(areaText, areaFont)
            Dim blockHeight = labelSize.Height + 2 + areaSize.Height
            Dim blockWidth = Math.Max(labelSize.Width, areaSize.Width)
            gr.DrawString(s.Label, labelFont, Brushes.Brown, New PointF(cpoint.X - labelSize.Width / 2, cpoint.Y - blockHeight / 2))
            gr.DrawString(areaText, areaFont, Brushes.Maroon, New PointF(cpoint.X - areaSize.Width / 2, cpoint.Y - blockHeight / 2 + areaSize.Height / 2 + 9))
        Next



        Return bmp
    End Function

    Public Shared Sub SaveJPEG(filePath As String, sketchString As String, Optional parcelLabel As String = "", Optional entityLabel As String = "")
        Dim bmp = RenderBitmap(sketchString, parcelLabel, entityLabel)

        Dim jpgEncoder As ImageCodecInfo = GetEncoder(ImageFormat.Jpeg)
        Dim encoderParams As New EncoderParameters(1)
        Dim jpegQuality As System.Drawing.Imaging.Encoder = System.Drawing.Imaging.Encoder.Quality
        Dim qualityParameter As New EncoderParameter(jpegQuality, 100L)
        encoderParams.Param(0) = qualityParameter

        bmp.Save(filePath, jpgEncoder, encoderParams)

    End Sub

    Private Shared Function GetEncoder(ByVal format As ImageFormat) As ImageCodecInfo

        Dim codecs As ImageCodecInfo() = ImageCodecInfo.GetImageDecoders()
        Dim codec As ImageCodecInfo
        For Each codec In codecs
            If codec.FormatID = format.Guid Then
                Return codec
            End If
        Next codec
        Return Nothing

    End Function

    Private Shared Function getFont(sizeDiff As Integer, Optional bold As Boolean = False)
        Return New Font(SystemFonts.DefaultFont.FontFamily, SystemFonts.DefaultFont.Size + sizeDiff, If(bold, FontStyle.Bold, FontStyle.Regular))
    End Function

    Private Class SketchSegment
        Public Property Label As String
        Public Property Properties As String
        Public Property StartCommands As Vector()
        Public Property ShapeCommands As Vector()

        Public Sub New(vectorString As String)
            Dim parts = vectorString.Split(":".ToCharArray, 2)
            Label = parts(0).Split("[")(0)
            Properties = parts(0).Split("[")(1).Trim("]")
            Dim vparts = parts(1).Split("S"c)
            StartCommands = vparts(0).Split(" "c).Where(Function(x) x.Trim <> "").Select(Function(x) New Vector(x)).ToArray
            ShapeCommands = vparts(1).Split(" "c).Where(Function(x) x.Trim <> "").Select(Function(x) New Vector(x)).ToArray
        End Sub

        Public Function GetBounds() As RectangleF
            Dim o As New PointF(0, 0)
            Dim pmax As New PointF(0, 0)
            Dim pmin As New PointF(0, 0)
            For Each v In StartCommands
                o.X += v.GraphOffset.X
                o.Y += v.GraphOffset.Y

                pmax = New PointF(Math.Max(pmax.X, o.X), Math.Min(pmax.Y, o.Y))
                pmin = New PointF(Math.Min(pmin.X, o.X), Math.Max(pmin.Y, o.Y))
            Next
            For Each v In ShapeCommands
                o.X += v.GraphOffset.X
                o.Y += v.GraphOffset.Y

                pmax = New PointF(Math.Max(pmax.X, o.X), Math.Min(pmax.Y, o.Y))
                pmin = New PointF(Math.Min(pmin.X, o.X), Math.Max(pmin.Y, o.Y))
            Next
            Return New RectangleF(pmin.X, pmin.Y, (pmax.X - pmin.X), (pmin.Y - pmax.Y))
        End Function
    End Class

    Private Class Vector

        Public Property DirectionX As Char
        Public Property DistanceX As Single
        Public Property DirectionY As Char
        Public Property DistanceY As Single

        Public ReadOnly Property Coordinate As PointF
            Get
                Return p
            End Get
        End Property

        Public ReadOnly Property GraphOffset As PointF
            Get
                Return o
            End Get
        End Property

        Public ReadOnly Property Length As Single
            Get
                Return Math.Round(Math.Sqrt(p.X * p.X + p.Y * p.Y), RoundOffScale)
            End Get
        End Property

        Public ReadOnly Property Angle As Single
            Get
                Dim a = Math.Atan2(o.Y, o.X) * 180 / Math.PI
                'If a < 0 Then
                '    a = 360 + a
                'End If
                Return a
            End Get
        End Property

        Private o As PointF, p As PointF

        Public Sub New(command As String)
            o = New PointF(0, 0)
            p = New PointF(0, 0)
            If command.Trim <> "" Then
                For Each dp In command.Split("/"c)
                    Dim dir = dp.Chars(0)
                    Dim length = Math.Round(CSng(dp.Substring(1)), RoundOffScale)
                    Dim pixels = Math.Round(CSng(dp.Substring(1)) * PPF, RoundOffScale)
                    If dir = "U" Or dir = "D" Then
                        DirectionY = dir
                        DistanceY = pixels
                        If dir = "U" Then
                            p.Y -= length
                            o.Y -= pixels
                        Else
                            p.Y += length
                            o.Y += pixels
                        End If
                    End If
                    If dir = "L" Or dir = "R" Then
                        DirectionX = dir
                        DistanceX = pixels
                        If dir = "R" Then
                            p.X += length
                            o.X += pixels
                        Else
                            p.X -= length
                            o.X -= pixels
                        End If
                    End If
                Next
                'Debug.WriteLine("{0} {1}", command, p.ToString)'
            End If
        End Sub
    End Class

End Class

