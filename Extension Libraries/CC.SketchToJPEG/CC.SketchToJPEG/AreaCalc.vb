﻿Namespace DataContracts
    Friend Class AreaCalc
        Public Property label As String
        Public Property area As Single
        Public Property vector As String

    End Class

    Friend Class SketchView
        Public Property areas As List(Of AreaCalc)
        Public Property preview As String
    End Class

    Friend Class PreviewResponse
        Public Property success As Boolean
        Public Property [error] As String
        Public Property data As String
    End Class
End Namespace
