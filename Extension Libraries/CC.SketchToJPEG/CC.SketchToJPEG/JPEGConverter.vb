﻿Imports System.Reflection
Imports CAMACloud.Sketching.Converters.DataContracts

Public Class JPEGConverter

    Dim ready As Boolean = False

    Public Sub CreateScript(path As String, fileName As String)
        Dim assly = Assembly.GetExecutingAssembly
        Using strm = assly.GetManifestResourceStream("CAMACloud.Sketching.Converters." + fileName)
            Using srdr = New IO.StreamReader(strm)
                Dim content = srdr.ReadToEnd
                Dim targetPath As String = IO.Path.Combine(path, fileName)
                IO.File.WriteAllText(targetPath, content)
            End Using
        End Using

    End Sub

    Public Sub New()
        Dim tempDb As String = IO.Path.Combine(IO.Path.GetTempPath, "cc_sketchlib")
        If Not IO.Directory.Exists(tempDb) Then
            IO.Directory.CreateDirectory(tempDb)
        End If

        CreateScript(tempDb, "index.html")
        CreateScript(tempDb, "14-ccsketcheditor.js")
        CreateScript(tempDb, "ccsketch-masketch.js")
        CreateScript(tempDb, "configs.js")
        CreateScript(tempDb, "jquery.js")

        Dim startFile = IO.Path.Combine(tempDb, "index.html")
        InitializeComponent()
        'Dim webUrl = IO.Path.Combine(IO.Path.GetDirectoryName(Application.ExecutablePath), "web/index.html?t=2")
        Dim webUrl = startFile + "?t=" & Now.Ticks
        browser.Navigate("file://" + webUrl)
        'Threading.Thread.Sleep(500)

    End Sub

    Private Sub DocCompleted(sender As Object, e As WebBrowserDocumentCompletedEventArgs) Handles browser.DocumentCompleted
        ready = True
    End Sub

    Public Sub WaitFoReady()
        'While True
        '    Threading.Thread.Sleep(200)
        '    If ready Then
        '        Exit While
        '    End If
        'End While
    End Sub


    Public Function Convert(sketchString As String) As Bitmap

        Dim doc = browser.Document
        Dim ret = browser.Document.InvokeScript("previewSketch", {sketchString})
        Dim resp As PreviewResponse = Newtonsoft.Json.JsonConvert.DeserializeObject(ret, GetType(PreviewResponse))
        If resp.success Then
            Dim data = resp.data.Split(",")(1)
            Dim bytes = System.Convert.FromBase64String(data)
            Dim ms As New IO.MemoryStream(bytes)
            Dim bmp As New Bitmap(ms)
            Return bmp
        Else
            Throw New Exception(resp.error)
        End If
    End Function


End Class