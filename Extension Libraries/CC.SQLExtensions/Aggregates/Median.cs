﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Collections.Generic;

namespace CAMACloud.SQLServer.Extensions.Aggregates
{

    [Serializable]
    [Microsoft.SqlServer.Server.SqlUserDefinedAggregate(
        Format.UserDefined,
        IsInvariantToDuplicates = false,
        IsInvariantToNulls = false,
        IsInvariantToOrder = false,
        MaxByteSize = -1)]
    public struct Median : IBinarySerialize
    {

        private List<double> ld;

        public void Init()
        {
            ld = new List<double>();
        }

        public void Accumulate(SqlDouble Value)
        {
            if (!Value.IsNull)
            {
                ld.Add(Value.Value);
            }
        }


        public void Merge(Median Group)
        {
            this.ld.AddRange(Group.ld.ToArray());
        }

        public SqlDouble Terminate()
        {
            if (ld.Count == 0)
                return SqlDouble.Null;

            ld.Sort();
            int index = (int)ld.Count / 2;

            if (ld.Count % 2 == 0)
            {
                return (SqlDouble)(((double)ld[index] + (double)ld[index - 1]) / 2);
            }
            else
            {
                return (SqlDouble)((double)ld[index]);
            }
        }


        #region IBinarySerialize Members

        public void Read(System.IO.BinaryReader r)
        {
            int cnt = r.ReadInt32();
            this.ld = new List<double>(cnt);
            for (int i = 0; i < cnt; i++)
            {
                this.ld.Add(r.ReadDouble());
            }
        }

        public void Write(System.IO.BinaryWriter w)
        {
            w.Write(this.ld.Count);
            foreach (double d in this.ld)
            {
                w.Write(d);
            }
        }

        #endregion
    }



}
