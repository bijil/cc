﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml.Serialization;
using System.IO;
using System.Text;
using System.Collections;

namespace CAMACloud.SQLServer.Extensions.Aggregates
{

[Serializable]
[Microsoft.SqlServer.Server.SqlUserDefinedAggregate(
    Format.UserDefined,
    IsInvariantToDuplicates = false,
    IsInvariantToNulls = false,
    IsInvariantToOrder = false,
    MaxByteSize = -1)]
public struct Mean : IBinarySerialize
{

    private List<double> elements;
    int counter;
    double sum;
    public void Init()
    {
        sum = 0.0;
        counter = 0;
        elements = new List<double>();
    }

    public void Accumulate(SqlDouble Value)
    {
        if (!Value.IsNull)
        {
            elements.Add(Value.Value);
            counter++;
        }
    }


    public void Merge(Mean Group)
    {
        this.elements.AddRange(Group.elements.ToArray());
    }

   
    public SqlDouble Terminate()
    {
        if (elements.Count == 0)
            return SqlDouble.Null;
        counter = elements.Count;
        for (int i = 0; i < counter; i++)
        {
            sum =sum +elements[i];
        }
        return (SqlDouble)((double)(sum / counter));
       
    }
    


    #region IBinarySerialize Members

    public void Read(System.IO.BinaryReader r)
    {
        int cnt = r.ReadInt32();
        this.elements = new List<double>(cnt);
        for (int i = 0; i < cnt; i++)
        {
            this.elements.Add(r.ReadDouble());
        }
    }

    public void Write(System.IO.BinaryWriter w)
    {
        w.Write(this.elements.Count);
        foreach (double d in this.elements)
        {
            w.Write(d);
        }
    }

    #endregion
}

}
