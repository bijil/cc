﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml.Serialization;
using System.IO;
using System.Text;
using System.Collections;

namespace CAMACloud.SQLServer.Extensions.Aggregates
{
    [Serializable]
    [Microsoft.SqlServer.Server.SqlUserDefinedAggregate(
        Format.UserDefined,
        IsInvariantToDuplicates = false,
        IsInvariantToNulls = false,
        IsInvariantToOrder = false,
        MaxByteSize = -1)]
    public struct RangeMax : IBinarySerialize
    {

        private List<double> element;
        private double max;

        public void Init()
        {
            element = new List<double>();
            max = 0.0;
        }

        public void Accumulate(SqlDouble Value)
        {
            if (!Value.IsNull)
            {
                element.Add(Value.Value);
            }
        }


        public void Merge(RangeMax Group)
        {
            this.element.AddRange(Group.element.ToArray());
        }

        public SqlDouble Terminate()
        {
            if (element.Count == 0)
                return SqlDouble.Null;

            element.Sort();
            max = element[element.Count - 1];

            return (SqlDouble)((double)(max));


        }


        #region IBinarySerialize Members

        public void Read(System.IO.BinaryReader r)
        {
            int cnt = r.ReadInt32();
            this.element = new List<double>(cnt);
            for (int i = 0; i < cnt; i++)
            {
                this.element.Add(r.ReadDouble());
            }
        }

        public void Write(System.IO.BinaryWriter w)
        {
            w.Write(this.element.Count);
            foreach (double d in this.element)
            {
                w.Write(d);
            }
        }

        #endregion
    }
}

