﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Collections.Generic;

namespace CAMACloud.SQLServer.Extensions.Aggregates
{

    [Serializable]
    [Microsoft.SqlServer.Server.SqlUserDefinedAggregate(
        Format.UserDefined,
        IsInvariantToDuplicates = false,
        IsInvariantToNulls = false,
        IsInvariantToOrder = false,
        MaxByteSize = -1)]
    public struct IQR : IBinarySerialize
    {

        private List<double> ld;
        private List<double> ld1;
        private double m;
        private double m1;
        private double m2;
        private int c2 ;
        private int c1 ;
        public void Init()
        {
            m = 0.0;
            m1 = 0.0;
            m2 = 0.0;
            c2 = 0;
            c1 = 0;
            ld = new List<double>();
            ld1 = new List<double>();
        }

        public void Accumulate(SqlDouble Value)
        {
            if (!Value.IsNull)
            {
                ld.Add(Value.Value);
            }
        }


        public void Merge(IQR Group)
        {
            this.ld.AddRange(Group.ld.ToArray());
        }

        public SqlDouble Terminate()
        {
            var s = 0;
            int index, index1, index2;
            index1 = -1;
            index2 = -1;
            index = -1;
            if (ld.Count == 0)
                return SqlDouble.Null;
            if (ld.Count == 1)
                return SqlDouble.Null;

            try
            {

                s = 1;
                ld.Sort();
                index = (int)Math.Floor((double)ld.Count / 2);

                if (ld.Count % 2 == 0)
                {
                    s = 2;
                    m = ((double)ld[index] + (double)ld[index - 1]) / 2;
                }
                else
                {
                    s = 3;
                    m = (double)ld[index];
                }

                s = 4;
                if (ld.IndexOf(m) != -1)
                {
                    int n1 = ld.IndexOf(m);
                    for (int i = 1; i <= n1; i++)
                    {
                        c1++;
                    }
                    s = 5;
                    index1 = (int)Math.Floor((double)c1 / 2);
                    if (c1 == 0)
                    {
                        m1 = (double)ld[index1];
                    }
                    else if (c1 % 2 == 0)
                    {
                        s = 6;
                        m1 = ((double)ld[index1] + (double)ld[index1 - 1]) / 2;
                    }
                    else
                    {
                        s = 7;
                        m1 = (double)ld[index1];
                    }
                    for (int i = n1 + 1; i < ld.Count; i++)
                    {
                        ld1.Add(ld[i]);
                        c2++;
                    }
                    index2 = (int)Math.Floor((double)c2 / 2);
                    s = 8;
                    if (c2 == 0)
                    {
                        m2 = (double)ld1[index2];
                    }
                    else if (c2 % 2 == 0)
                    {
                        s = 9;
                        m2 = ((double)ld1[index2] + (double)ld1[index2 - 1]) / 2;
                    }
                    else
                    {
                        s = 10;
                        m2 = (double)ld1[index2];
                    }

                    return (SqlDouble)(m2 - m1);

                }
                else
                {
                    int counter = 0;
                    int midValue = 0;
                    midValue = ld.Count / 2;
                    for (int i = 0; i < midValue; i++)
                    {
                        counter++;
                    }
                    index1 = (int)Math.Floor((double)counter / 2);
                    s = 11;

                    if (counter == 0)
                    {
                        m1 = (double)ld[index1];
                    }
                    else if (counter % 2 == 0)
                    {
                        s = 12;
                        m1 = ((double)ld[index1] + (double)ld[index1 - 1]) / 2;
                    }
                    else
                    {
                        s = 13;
                        m1 = (double)ld[index1];
                    }
                    for (int i = midValue; i < ld.Count; i++)
                    {
                        ld1.Add(ld[i]);
                        c2++;
                    }
                    index2 = (int)Math.Floor((double)c2 / 2);
                    if (c2 == 0)
                    {
                        m2 = (double)ld1[index2];
                    }
                    else if (c2 % 2 == 0)
                    {
                        s = 14;
                        m2 = ((double)ld1[index2] + (double)ld1[index2 - 1]) / 2;
                    }
                    else
                    {
                        s = 15;
                        m2 = (double)ld1[index2];
                    }

                    return (SqlDouble)(m2 - m1);
                    //return SqlDouble.Null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("LD.Count = {0}, index = {1}, index1 = {2}, index2 = {3}, stage={4}, LD1.Count = {5}\n", ld.Count, index, index1, index2, s, ld1.Count), ex);
            }



        }


        #region IBinarySerialize Members

        public void Read(System.IO.BinaryReader r)
        {
            int cnt = r.ReadInt32();
            this.ld = new List<double>(cnt);
            for (int i = 0; i < cnt; i++)
            {
                this.ld.Add(r.ReadDouble());
            }
            int cnt1 = r.ReadInt32();
            this.ld1 = new List<double>(cnt1);
            for (int i = 0; i < cnt1; i++)
            {
                this.ld1.Add(r.ReadDouble());
            }
        }

        public void Write(System.IO.BinaryWriter w)
        {
            w.Write(this.ld.Count);
            foreach (double d in this.ld)
            {
                w.Write(d);
            }
            w.Write(this.ld1.Count);
            foreach (double d1 in this.ld1)
            {
                w.Write(d1);
            }
        }

        #endregion
    }

}
