﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Collections.Generic;

namespace CAMACloud.SQLServer.Extensions.Aggregates
{

    [Serializable]
    [Microsoft.SqlServer.Server.SqlUserDefinedAggregate(
        Format.UserDefined,
        IsInvariantToDuplicates = false,
        IsInvariantToNulls = false,
        IsInvariantToOrder = false,
        MaxByteSize = -1)]
    public struct IQRMax : IBinarySerialize
    {

        private List<double> firstList;
        private List<double> secondList;
        private double firstMedian;
        private double secondMedian;
        private int counter;
        private int midValue;
        public void Init()
        {
            firstMedian = 0.0;
            secondMedian = 0.0;
            counter = 0;
            midValue = 0;
            firstList = new List<double>();
            secondList = new List<double>();
        }

        public void Accumulate(SqlDouble Value)
        {
            if (!Value.IsNull)
            {
                firstList.Add(Value.Value);
            }
        }


        public void Merge(IQRMax Group)
        {
            this.firstList.AddRange(Group.firstList.ToArray());
        }

        public SqlDouble Terminate()
        {
            if (firstList.Count == 0)
                return SqlDouble.Null;
            if (firstList.Count == 1)
                return SqlDouble.Null;

            firstList.Sort();
            int id = (int)firstList.Count / 2;

            if (firstList.Count % 2 == 0)
            {
                firstMedian = ((double)firstList[id] + (double)firstList[id - 1]) / 2;
            }
            else
            {
                firstMedian = (double)firstList[id];
            }

            if (firstList.IndexOf(firstMedian) != -1)
            {
                int n1 = firstList.IndexOf(firstMedian);
                for (int i = n1 + 1; i < firstList.Count; i++)
                {
                    secondList.Add(firstList[i]);
                    counter++;
                }
                int index2 = (int)counter / 2;

                if (counter == 0) { secondMedian = (double)secondList[index2]; }
                else if (counter % 2 == 0)
                {
                    secondMedian = ((double)secondList[index2] + (double)secondList[index2 - 1]) / 2;
                }
                else
                {
                    secondMedian = (double)secondList[index2];
                }

                return (SqlDouble)(secondMedian);

            }
            else
            {

                midValue = (int)firstList.Count / 2;
                for (int i = midValue; i < firstList.Count; i++)
                {
                    secondList.Add(firstList[i]);
                    counter++;
                }
                int index2 = (int)counter / 2;

                if (counter == 0) { secondMedian = (double)secondList[index2]; }
                else if (counter % 2 == 0)
                {
                    secondMedian = ((double)secondList[index2] + (double)secondList[index2 - 1]) / 2;
                }
                else
                {
                    secondMedian = (double)secondList[index2];
                }

                return (SqlDouble)(secondMedian);

            }

        }


        #region IBinarySerialize Members

        public void Read(System.IO.BinaryReader r)
        {
            int cnt = r.ReadInt32();
            this.firstList = new List<double>(cnt);
            for (int i = 0; i < cnt; i++)
            {
                this.firstList.Add(r.ReadDouble());
            }
            int cnt1 = r.ReadInt32();
            this.secondList = new List<double>(cnt1);
            for (int i = 0; i < cnt1; i++)
            {
                this.secondList.Add(r.ReadDouble());
            }
        }

        public void Write(System.IO.BinaryWriter w)
        {
            w.Write(this.firstList.Count);
            foreach (double d in this.firstList)
            {
                w.Write(d);
            }
            w.Write(this.secondList.Count);
            foreach (double d1 in this.secondList)
            {
                w.Write(d1);
            }
        }

        #endregion
    }

}
