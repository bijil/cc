﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml.Serialization;
using System.IO;
using System.Text;
using System.Collections;

namespace CAMACloud.SQLServer.Extensions.Aggregates
{
    [Serializable]
    [Microsoft.SqlServer.Server.SqlUserDefinedAggregate(
        Format.UserDefined,
        IsInvariantToDuplicates = false,
        IsInvariantToNulls = false,
        IsInvariantToOrder = false,
        MaxByteSize = -1)]
    public struct RangeMin : IBinarySerialize
    {

        private List<double> elements;
        private double min;

        public void Init()
        {
            elements = new List<double>();
            min = 0.0;

        }

        public void Accumulate(SqlDouble Value)
        {
            if (!Value.IsNull)
            {
                elements.Add(Value.Value);
            }
        }


        public void Merge(RangeMin Group)
        {
            this.elements.AddRange(Group.elements.ToArray());
        }

        public SqlDouble Terminate()
        {
            if (elements.Count == 0)
                return SqlDouble.Null;

            elements.Sort();
            min = elements[0];
            return (SqlDouble)((double)(min));
        }


        #region IBinarySerialize Members

        public void Read(System.IO.BinaryReader r)
        {
            int cnt = r.ReadInt32();
            this.elements = new List<double>(cnt);
            for (int i = 0; i < cnt; i++)
            {
                this.elements.Add(r.ReadDouble());
            }
        }

        public void Write(System.IO.BinaryWriter w)
        {
            w.Write(this.elements.Count);
            foreach (double d in this.elements)
            {
                w.Write(d);
            }
        }

        #endregion
    }
}