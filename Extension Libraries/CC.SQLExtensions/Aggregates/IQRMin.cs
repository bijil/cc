﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Collections.Generic;


namespace CAMACloud.SQLServer.Extensions.Aggregates
{

    [Serializable]
    [Microsoft.SqlServer.Server.SqlUserDefinedAggregate(
        Format.UserDefined,
        IsInvariantToDuplicates = false,
        IsInvariantToNulls = false,
        IsInvariantToOrder = false,
        MaxByteSize = -1)]
    public struct IQRMin : IBinarySerialize
    {

        private List<double> elements;
        private double median;
        private double iqr;
        private int c1;
        public void Init()
        {
            median = 0.0;
            iqr = 0.0;
            c1 = 0;
            elements = new List<double>();

        }

        public void Accumulate(SqlDouble Value)
        {
            if (!Value.IsNull)
            {
                elements.Add(Value.Value);
            }
        }


        public void Merge(IQRMin Group)
        {
            this.elements.AddRange(Group.elements.ToArray());
        }

        public SqlDouble Terminate()
        {
            int s = 0;

            try
            {
                if (elements.Count == 0)
                    return SqlDouble.Null;
                if (elements.Count == 1)
                    return SqlDouble.Null;

                elements.Sort();
                int index = (int)Math.Floor((double)elements.Count / 2);

                if (elements.Count % 2 == 0)
                {
                    s = 1;
                    median = ((double)elements[index] + (double)elements[index - 1]) / 2;
                }
                else
                {
                    s = 2;
                    median = (double)elements[index];
                }

                if (elements.IndexOf(median) != -1)
                {
                    int n1 = elements.IndexOf(median);
                    for (int i = 1; i <= n1; i++)
                    {
                        c1++;
                    }
                    int index1 = (int)c1 / 2;


                    if (c1 == 0)
                    {
                        s = 3;
                        iqr = (double)elements[index1];
                    }
                    else if (c1 % 2 == 0)
                    {
                        s = 4;
                        iqr = ((double)elements[index1] + (double)elements[index1 - 1]) / 2;
                    }
                    else
                    {
                        s = 5;
                        iqr = (double)elements[index1];
                    }

                    return (SqlDouble)(iqr);

                }
                else
                {
                    int counter = 0;
                    int midValue = 0;
                    midValue = elements.Count / 2;
                    for (int i = 0; i < midValue; i++)
                    {
                        counter++;
                    }
                    int index1 = (int)Math.Floor((double)counter / 2);

                    if (counter == 0)
                    {
                        s = 6;
                        iqr = (double)elements[index1];
                    }
                    else if (counter % 2 == 0)
                    {
                        s = 7;
                        iqr = ((double)elements[index1] + (double)elements[index1 - 1]) / 2;
                    }
                    else
                    {
                        s = 8;
                        iqr = (double)elements[index1];
                    }

                    return (SqlDouble)(iqr);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("stage={0}, El.Count = {1}\n", s, elements.Count), ex);
            }


        }


        #region IBinarySerialize Members

        public void Read(System.IO.BinaryReader r)
        {
            int cnt = r.ReadInt32();
            this.elements = new List<double>(cnt);
            for (int i = 0; i < cnt; i++)
            {
                this.elements.Add(r.ReadDouble());
            }
        }

        public void Write(System.IO.BinaryWriter w)
        {
            w.Write(this.elements.Count);
            foreach (double d in this.elements)
            {
                w.Write(d);
            }
        }


        #endregion
    }


}
