﻿Imports Microsoft.Reporting.WebForms
Imports CAMACloud.Data
Imports System.Data

Partial Class _default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            'ReportViewer1.LocalReport.ReportPath = "field/AuditTrail.rdlc"
            'ReportViewer1.LocalReport.ReportPath = "field/NeighborhoodStatistics.rdlc"

            Dim dataSetName = "FieldActivityDataset"
            ReportViewer1.LocalReport.ReportPath = "field/FieldActivity.rdlc"
            'Get a datatable here. If you are using the correct datatable/query, you'll see the values in the records, else will no see values, just rows.


            Dim result As DataTable = Connection.GetDataTable("EXEC reports_GetNeighborhoodStats '','','' ")

            ''Add the datasource 
            ReportViewer1.LocalReport.DataSources.Add(New ReportDataSource(dataSetName, result))

            'View in Browser or Refresh browse
          
        End If
    End Sub

    'Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
    '    '        private void As CreatePDF(string fileName)
    '    '{

    '    '    string[] streamIds;
    '    '    string mimeType = string.Empty;
    '    '    string encoding = string.Empty;
    '    '    string extension = string.Empty;

    '    '        'Setup the report viewer object and get the array of bytes
    '    '        ReportViewer1.LocalReport = ReportViewer1.ProcessingMode = ProcessingMode.Local;
    '    '    ReportViewer1.LocalReport.ReportPath = "YourReportHere.rdlc";

    '    '    byte[] bytes = ReportViewer1.LocalReport.Render("PDF", DBNull.Value, out mimeType, out encoding, out extension, out streamIds, out warnings);

    '    '        'Now that you have all the bytes representing the PDF report, buffer it and send it to the client.

    '    '}



    '    Dim display As New 

    'End Sub
    'Sub CreatePDF(ByVal fileName As String)
    '    Dim mimeType As String = String.Empty
    '    Dim encoding As String = String.Empty
    '    Dim extension As String = String.Empty

    '    ' ReportViewer1.LocalReport = ReportViewer1.ProcessingMode = ProcessingMode.Local
    '    ReportViewer1.LocalReport.ReportPath = "NeighborhoodStatistics.rdlc"
    '    Dim Bytes As Byte = ReportViewer1.LocalReport.Render("PDF", DBNull.Value, mimeType, encoding, extension)




    'End Sub

    'Private Sub Warning()
    '    Throw New NotImplementedException
    'End Sub

    'Private Function out() As Object
    '    Throw New NotImplementedException
    'End Function

End Class
