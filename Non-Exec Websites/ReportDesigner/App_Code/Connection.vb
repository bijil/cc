﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient


Public Class Connection
    Shared conn As New SqlConnection("Server=.;database=PACS_BEXAR; integrated security=true")
    Public Shared Function GetDataTable(ByVal query As String) As DataTable
        If conn.State = ConnectionState.Open Then
            conn.Close()
        End If
        If conn.State = ConnectionState.Closed Then
            conn.Open()
        End If
        Dim cmd As New SqlDataAdapter(query, conn)
        Dim dt As New DataTable
        cmd.Fill(dt)
        Return dt
    End Function
End Class
