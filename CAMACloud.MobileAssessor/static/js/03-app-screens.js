﻿//    DATA CLOUD SOLUTIONS, LLC ("COMPANY") CONFIDENTIAL 
//    Unpublished Copyright (c) 2010-2013 
//    DATA CLOUD SOLUTIONS, an Ohio Limited Liability Company, All Rights Reserved.

//    NOTICE: All information contained herein is, and remains the property of COMPANY.
//    The intellectual and technical concepts contained herein are proprietary to COMPANY
//    and may be covered by U.S. and Foreign Patents, patents in process, and are protected
//    by trade secret or copyright law. Dissemination of this information or reproduction
//    of this material is strictly forbidden unless prior written permission is obtained
//    from COMPANY. Access to the source code contained herein is hereby forbidden to
//    anyone except current COMPANY employees, managers or contractors who have executed
//    Confidentiality and Non-disclosure agreements explicitly covering such access.</p>

//    The copyright notice above does not evidence any actual or intended publication
//    or disclosure of this source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
//    DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
//    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
//    IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
//    OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
//    TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
//    ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.




ccma.UI.ActiveScreenId = 'sort-screen'
var firstTimeLoad = false;
var ParcelopenfromMap = false;
var sortScreenScroll;
var enableMultipleNbhd;
var imageLimit = 0;
var downloadLimit = 0;
var showFutureDataInSortscreen = false;
var NbhdLoadStatus = true;
var parcelOpenMode;
var _sketchStringThrowError = false;
var parent_missing_msg = false;
var sketchEditorOpenFirstTime = false;
var showDCCClicked = false; // DCC opened or not , If DCC not opened then load all category when redirecting from PRC
var sketchSaveClicked = false; //sketch change action performed or not,
var prcClick = false; // To identify prc click
function showLoading(showButtons, callback) {
    loadScreen('loading', function () {
        if (showButtons) {
            $('.tools .dashboard').hide();
            $('.tools .sort').hide();
            $('.tools .search').hide();
            $('.tools .sync').show();
            $('.tools .logout').show();
        }
        if (callback) callback();
    });
}


function showLogin() {
    loadScreen('login', function () {
        clearLastActivityTime();
        setLoginProgress(0);
        $('.tools a').hide();
        log('Setting last username');
        $('.login-username').val(localStorage.getItem('username'));
    });
}

function showAgreement(checkUnPw, asPage) {
    var proc = function () {

        const setAgreement = () => {
            if (ccma.UI.AgreementContent != null && ccma.UI.AgreementContent != "") {
                $('.License-Agreement').html(ccma.UI.AgreementContent);
            }
        }

        if (!asPage) {
            getLicenseAgreement(() => {
                $('.login-buttons button').removeAttr('disabled');
                setAgreement();
                loadScreen('agreement');
            });
        } else {
            if ((ccma.UI.ActiveScreenId == "agreement") || (ccma.UI.ActiveScreenId == "viewagreement")) { return; }
            $('.app-href').html(window.location.origin);
            $('.uname').html(ccma.CurrentUser);
            setAgreement();
            loadScreen('viewagreement');
        }

    }
    if (checkUnPw == true) {
        var uname = $('.login-username').val();
        var pword = $('.login-password').val();
        if ((uname == "") || (pword == "")) {
            return;
        }
        checkDbExistence(function (isApproved) {
            if (isApproved) {
                proc()
            }
        });
    }
    else proc();
}


function selectNbhd(callback) {
    loadScreen('neighborhood', function () {
        NbhdLoadStatus = false;
        refreshNeighborhoodsList(function () {
            NbhdLoadStatus = true;
            /* $('.nbhd-selected', '#neighborhood').unbind(touchClickEvent);*/
            $('.nbhd-selected', '#neighborhood').bind(touchClickEvent, function (e) {
                e.preventDefault();
                syncNbhd = [];
                var nbhds;
                if (activeParcel)
                    activeParcel = null
                let dd = EnableNewPriorities == 'True' ? '.nbhdtbl tbody tr' : '.nbhd-div';
                if (enableMultipleNbhd) {
                    var parcelCount = 0;
                    $(dd).each(function (index, nbhd) {
                        if ($(nbhd).attr('selected') == 'selected') {
                            parcelCount += parseInt($(nbhd).attr('parcelCount'));
                            syncNbhd.push($(nbhd).attr('value'));
                        }
                    });

                    //if (EnableNewPriorities == 'True') {
                        imageLimit = parseInt(document.querySelector('input[type="radio"][name="selectImage__Count"]:checked').value.toString().split('||')[1]);
                        downloadLimit = document.querySelector('input[type="radio"][name="selectImage__Count"]:checked').value.toString().split('||')[0];
                    /*}
                    else {
                        imageLimit = parseInt($('.select-image-count').val().toString().split('||')[1]);
                        downloadLimit = $('.select-image-count').val().toString().split('||')[0];
                    }*/

                    if ($('.normalParcelsEnabled').html() == 'true') {
                        imageLimit = 4;
                        downloadLimit = 3000;
                    }
                }
                else {
                    if ($('select.select-nbhd-for-sync').attr('value') != "") {
                        syncNbhd.push($('select.select-nbhd-for-sync').attr('value'));
                        imageLimit = 0;
                        downloadLimit = 350;
                    }
                }
                if (syncNbhd.length > 0) {
                    localStorage.setItem('nbhd', syncNbhd.join('||'));
                    if (callback) callback();
                }
            });
        });
    });
}

function refreshNeighborhoodsList(callback) {

    $('.select-nbhd-for-sync').html('<option value="">loading...</option>');
    $('.select-nbhd-for-sync').attr('disabled', 'disabled');
    $$$('getnbhds/', {}, function (data) {
        var maxwidth = 0;
        var parcelDownloadLimit = (data.ParcelDownloadLimit == '0') ? 1200 : parseInt(data.ParcelDownloadLimit.toString()),
            defaultImgOption = (data.DefaultImageDownloadOption && ["1", "2", "3", "4"].includes(data.DefaultImageDownloadOption)) ? data.DefaultImageDownloadOption : '1';
        enableMultipleNbhd = ((data.EnableMultipleNbhd) ? ((parseInt(data.EnableMultipleNbhd.toString()) == 1) ? true : false) : false);
        var refreshNbhdSelectionStatus = function () {
            var totalParcelCount = 0, totalPriorityCount = 0, nbhdCount = 0;
            if (EnableNewPriorities == 'True') {
                let sumObj = { 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0 }, rs = document.querySelectorAll('.nbhdtbl tr[selected="selected"]');
                rs.forEach((r, ix) => {
                    let cs = r.querySelectorAll('td');
                    for (let i = 2; i < cs.length; i++) {
                        let cl = cs[i]; sumObj[i] = sumObj[i] + parseInt(cl.textContent);
                    }
                });

                let footerRow = document.querySelector('.nbhdtbl tfoot tr'), tfootCells = footerRow.querySelectorAll('td');
                for (let i = 2; i < tfootCells.length; i++) {
                    let cl = tfootCells[i]; cl.textContent = sumObj[i];
                }
            }
            else {
                $('.nbhd-div').each(function (index, nbhd) {
                    if ($(nbhd).attr('selected')) {
                        totalParcelCount += parseInt($(nbhd).attr('parcelCount'));
                        totalPriorityCount += parseInt($(nbhd).attr('prioParcelCount'));
                        ++nbhdCount;
                    }
                });
                $('.totalPriorityParcelCount').html(totalPriorityCount);
                $('.totalParcelCount').html(totalParcelCount);
                $('.totalNbhdCount').html(nbhdCount);
            }
        }

        let selectImageCount = [{ Id: '400', Name: 'Default - All Images' }, { Id: '600', Name: 'Main Image + 2 most recent photos' }, { Id: '800', Name: 'Main Image Only' }, { Id: '1200', Name: 'No Images / Max Parcels' }]
        let nbhdel = EnableNewPriorities == 'True' ? '.nbhdtbl tbody tr' : '.nbhd-div';
        if (enableMultipleNbhd) {
            $('span.select-nbhd-for-sync').show();
            $('select.select-nbhd-for-sync').hide();
            /*if (EnableNewPriorities == 'True') {  }
            else { $('.span-select-image-count').hide(); $('.input-select-image-count').show(); }*/
            $('.span-select-image-count').css('display', 'block');//$('.span-select-image-count').show(); 
            $('.input-select-image-count').hide();
            if (data.EnableWalkingMode && data.EnableWalkingMode == "1") $('.select-route-mode').show();
            else $('.select-route-mode').hide();

            selectImageCount.forEach((x, i) => {
                switch (i) {
                    case 0: x.Id = '400||0'; break;
                    case 1: x.Id = '600||1'; break;
                    case 2: x.Id = '800||2'; break;
                    case 3: x.Id = '1200||3'; break;
                }

                if (parseInt(x.Id) > parcelDownloadLimit) x.Id = parcelDownloadLimit.toString() + '||' + i.toString();
            });


            //$('.select-image-count option').each(function (index, option) {
            //    switch (index) {
            //        case 0: $(option).attr('value', '400||0'); break;
            //        case 1: $(option).attr('value', '600||1'); break;
            //        case 2: $(option).attr('value', '800||2'); break;
            //        case 3: $(option).attr('value', '1200||3'); break;
            //    }
            //    var val = parseInt($(option).attr('value'));
            //    if (val > parcelDownloadLimit)
            //        $(option).attr('value', parcelDownloadLimit.toString() + '||' + index.toString());
            //});
            if (data.Neighborhoods.length == 0) {
                $('span.select-nbhd-for-sync').html('No ' + data.NeighborhoodName.toString() + 's assigned.');
                $('.select-image-count').hide();
                $('.select-route-mode').hide();
                if (callback) callback();
                return;
            }
            var div = document.createElement('div');
            if (EnableNewPriorities == 'True') {
                let nbHtml = '<table class="nbhdtbl"><thead><tr><th></th><th>Assignment Group</th><th style="background-color: #FF0000;">Critical</th><th style="background-color: #FFC000;">Urgent</th><th style="background-color: #FFFF00;">High</th><th style="background-color: #00B0F0;">Medium</th><th style="background-color: #8944FA;">Normal</th><th>Total Priority</th><th>Total</th></tr></thead><tbody>';
                data.Neighborhoods.forEach((dt) => {
                    nbHtml += '<tr value="' + dt.Nbhd + "##" + dt.NbhdId + '" parcelCount = "' + dt.ParcelCount + '" prioParcelCount = "' + dt.PriorityCount + '"><td><label id="' + dt.Nbhd + '" class="selectNbhdCheckbox" style="margin - top: 3px; "></label></td>';
                    nbHtml += '<td>' + dt.Nbhd + '</td>';
                    nbHtml += '<td style="background-color: #edc3c3;">' + dt.CriticalCount + '</td>'; nbHtml += '<td style="background-color: #efd892;">' + dt.UrgentCount + '</td>';
                    nbHtml += '<td style="background-color: #fdfdbb;">' + dt.HighCount + '</td>'; nbHtml += '<td style="background-color: #bde5f3;">' + dt.MediumCount + '</td>';
                    nbHtml += '<td style="background-color: #bf9df7;">' + dt.NormalCount + '</td>'; nbHtml += '<td>' + dt.PriorityCount + '</td>';
                    nbHtml += '<td>' + dt.ParcelCount + '</td></tr>';
                });
                nbHtml += '</tbody><tfoot><tr><td></td><td>Total Selected: </td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr></tfooter></table>';

                $(div).html('<div class="nbhd-list nbhd-list-prty" style=" z-index: 20; margin: 0 auto;">' + nbHtml + '</div>');
            }
            else {
                $(div).html('<div class="nbhd-div" style="display: table; margin: 0 auto; border: 1px solid silver; padding: 3px;" value="${Nbhd}##${NbhdId}" parcelCount = "${ParcelCount}" prioParcelCount = "${PriorityCount}"><label id="${Nbhd}" class="selectNbhdCheckbox" style="margin-top: 3px;"></label>${NbhdPrty}</div>');
                $(div).fillTemplate(data.Neighborhoods);
                $(div).html('<div class="nbhd-list" style="height: 256px; overflow-y: scroll; overflow-x: hidden; -webkit-overflow-scrolling: touch; z-index: 20; border: 1px solid grey; margin: 0 auto;">' + $(div).html() + '</div>');
            }


            $('span.select-nbhd-for-sync').html($(div).html());
            $('.nbhd-div').each(function (index, nbhd) {
                var x = $(nbhd).html().split('</label>')
                var y = x[1].split(')')
                var text = y[0] + ', Total-' + $(nbhd).attr('parcelCount') + ')';
                var html = x[0] + '</label>' + text;
                $(nbhd).html(html);
                if ($(nbhd).width() > maxwidth)
                    maxwidth = $(nbhd).width();
            })
            maxwidth = maxwidth > 340 ? maxwidth : 340;
            $('.nbhd-div').width(maxwidth);
            $('.nbhd-list').width(maxwidth + 2);
            var nbhdHeight = 9
            if (!iPad && !windowsTouch) {
                nbhdHeight = 3;
                $('.nbhd-select-buttons').css('margin-top', '0px')
                if(EnableNewPriorities != 'True') $('.nbhd-list').height('100px')
            }
            if ($('.nbhd-div').length < nbhdHeight && EnableNewPriorities != 'True')
                if ($('.nbhd-div').length < 9)
                    $('.nbhd-list').height($('.nbhd-div').height() * $('.nbhd-div').length);
            //Selected ' + data.NeighborhoodName.toString() + ' Count: <span class="totalNbhdCount"></span><br>Total Selected Priority Parcel Count: <span class="totalPriorityParcelCount"></span><br>Total Selected Parcel Count: <span class="totalParcelCount"></span><span class="normalParcelsEnabled" style= "display: none;"></span><br>

            if (EnableNewPriorities == 'True') {
                $('span.select-nbhd-for-sync').append('<span style="font-size: smaller"><a class="clearSelection" style="color: blue; cursor: pointer;">Clear all selections</a></span>')
                $('.select-image-count-span').css('width', '550px !important');
            }
            else {
                $('span.select-nbhd-for-sync').append('<span style="font-size: smaller">Selected ' + data.NeighborhoodName.toString() + ' Count: <span class="totalNbhdCount"></span><br>Total Selected Priority Parcel Count: <span class="totalPriorityParcelCount"></span><br>Total Selected Parcel Count: <span class="totalParcelCount"></span><span class="normalParcelsEnabled" style= "display: none;"></span><br><a class="clearSelection" style="color: blue; cursor: pointer;">Clear all selections</a></span>')
                
                $('.select-image-count-span').css('width', (maxwidth + 2) + 'px !important');
            }

            let nbhdEl = EnableNewPriorities == 'True' ? '.nbhdtbl tbody tr' : '.nbhd-div';
            $('.clearSelection').unbind(touchClickEvent)
            $('.clearSelection').bind(touchClickEvent, function (e) {
                $(nbhdEl).css('background-color', ''); 
                $(nbhdEl).removeAttr('selected');
                $(nbhdEl).find("label.selectNbhdCheckbox").removeClass('selectedcheckbox');
                checkCount();
            });

            
            var checkCount = function () {
                var count = EnableNewPriorities == 'True' ? $('.nbhdtbl tbody tr[selected="selected"]').length : $('.nbhd-div[selected="selected"]').length;
                var totalParcelCount = 0, totalPriorityCount = 0;

                var imageDownloadLimit = /*EnableNewPriorities == 'True' ?*/ parseInt(document.querySelector('input[type="radio"][name="selectImage__Count"]:checked').value.toString().split('||')[0])// : parseInt($('.input-select-image-count').val().toString().split('||')[0]);
                $('.normalParcelsEnabled').html(false);

                $(nbhdel).each(function (index, nbhd) {
                    if ($(nbhd).attr('selected')) {
                        totalParcelCount += parseInt($(nbhd).attr('parcelCount'));
                        totalPriorityCount += parseInt($(nbhd).attr('prioParcelCount'));
                    }
                });
                if (totalParcelCount >= 3000 || totalPriorityCount >= imageDownloadLimit || count == 15) {
                    $(nbhdel).each(function (index, nbhd) {
                        if (!$(nbhd).attr('selected')) {
                            $(nbhd).attr('disabled', 'disabled');
                            $(nbhd).css('background-color', '#ddd');
                        }
                    });
                }
                else {
                    $(nbhdel).each(function (index, nbhd) {
                        if ($(nbhd).attr('disabled') == 'disabled')
                            $(nbhd).css('background-color', '');
                        $(nbhd).removeAttr('disabled');
                    });
                }
                if (totalParcelCount > 3000)
                    $('.normalParcelsEnabled').html(true);
                refreshNbhdSelectionStatus();
            }

            $(nbhdel).unbind(touchClickEvent);
            $(nbhdel).bind(touchClickEvent, function (e) {
                if (!scrolling) {
                    if ($(this).attr('selected')) {
                        $(this).css('background-color', '');
                        $(this).removeAttr('selected');
                        $(this).find("label.selectNbhdCheckbox").removeClass('selectedcheckbox')
                    }
                    else if ($(this).attr('disabled') != 'disabled') {
                        $(this).css('background-color', '#79bbff')
                        $(this).attr('selected', 'selected');
                        $(this).find("label.selectNbhdCheckbox").addClass('selectedcheckbox')
                    }
                    checkCount();
                }
            });

            //if (EnableNewPriorities == 'True') {
                let irHtml = '';
                selectImageCount.forEach((si, i) => {
                    irHtml += '<span><input id="imgCount' + i + '" type="radio" class="selectImageCount-radio" style="margin-right: 5px;" name="selectImage__Count" value="' + si.Id + '" /><label style="margin-right: 5px;width: 300px;display: inline-block;margin-left: 3px;" for="imgCount' + i + '">' + si.Name + '</label></span>'
                });
                $('.select-image-count-span').html(irHtml);
                $('.select-image-count-span').unbind('change');
                $('.select-image-count-span').bind('change', function () {
                    checkCount();
                });
                let imgRadioButtons = document.querySelectorAll('input[type="radio"][name="selectImage__Count"]');
                imgRadioButtons[parseInt(defaultImgOption) - 1].checked = true;
            /*}
            else {
                $('.input-select-image-count').lookup({ popupView: true, width: 375, searchActive: true, searchActiveAbove: 8, searchAutoFocus: false, onSave: () => { } }, []);
                $('.input-select-image-count')[0].getLookup.setData(selectImageCount);
                $('.input-select-image-count').unbind('change');
                $('.input-select-image-count').bind('change', function () {
                    checkCount();
                });
                $('.input-select-image-count').val(selectImageCount[parseInt(defaultImgOption) - 1].Id);

            }*/
            checkCount();


            $('#nbhd-select-note').html('You are allowed to work on any of the listed ' + data.NeighborhoodName.toString() + 's as per the condition. Please choose the ' + data.NeighborhoodName.toString() + 's to continue');
        }
        else {
            if (data.EnableWalkingMode && data.EnableWalkingMode == "1") $('.SingleNbhd-route-mode').show();
            $('span.select-nbhd-for-sync').hide();
            $('select.select-nbhd-for-sync').show();
            $('.select-image-count').hide();

            if (EnableNewPriorities == 'True')
                $('select.select-nbhd-for-sync').html('<option value="${Nbhd}##${NbhdId}">${Nbhd} Priority - ${PriorityCount} Total Parcel - ${ParcelCount}</option>');
            else {
                $('select.select-nbhd-for-sync').html('<option value="${Nbhd}##${NbhdId}">${NbhdPrty}</option>');
            }
            $('select.select-nbhd-for-sync').fillTemplate(data.Neighborhoods);
            if (data.Neighborhoods.length == 0)
                $('.SingleNbhd-route-mode').hide();
        }
        $('.select-nbhd-for-sync').removeAttr('disabled');
        if (localStorage.getItem('nbhd') != null) {
            var locNbhd = localStorage.getItem('nbhd').split('||');
            if (enableMultipleNbhd) {
                $(nbhdel).each(function (index, nbhd) {
                    for (n in locNbhd) {
                        if (locNbhd[n] == $(nbhd).attr('value')) {
                            $(this).css('background-color', '#79bbff')
                            $(this).attr('selected', 'selected');
                            $(this).find("label.selectNbhdCheckbox").addClass('selectedcheckbox')
                        }
                    }
                });
                refreshNbhdSelectionStatus();
                checkCount();
            }
            else {
                $('select.select-nbhd-for-sync').val(localStorage.getItem('nbhd').split('||')[0]);
            }
        }
        if (callback) callback();
    });

}

function showProgress() {
    loadScreen('progress', function () {
        $('.tools .dashboard').hide();
        $('.tools .sort').hide();
        $('.tools .search').hide();
        $('.tools .sync').hide();
        $('.tools .logout').show();
    });
}

function showError(error) {
    $('div#errordisplay').html($('#' + error).html());
    loadScreen('error', function () {
        $('.tools .image-button').hide();
    });
}
function setSortButton(reset) {
    if (isBPPUser) {
        if (reset == 0) selectedFilters = 0;
        if (selectedFilters && selectedFilters != 0) { $('.sort-view').css('background', 'url(static/css/buttons/bpp-sort-filter.png)'); $('.sort-view').css('background-position-x', '5%'); $('.sort-view').css('width', '74px'); }
        else { $('.sort-view').css('background', 'url(static/css/buttons/bpp-sort-filter.png)'); $('.sort-view').css('background-position-x', '97%'); $('.sort-view').css('width', '74px'); }
    }
    else $('.sort-view').css('background', 'url(static/css/buttons/sort.png)')
}

function showSortScreen(callback, showonly) {
    refreshSortScreen = false;
    activeParcel = null;
    ccma.UI.ActiveFormTabId = null;
    unSyncChanges();
    if (currentNbhd == null && Object.keys(neighborhoods).length > 0)
        currentNbhd = neighborhoods[Object.keys(neighborhoods)[0]][0].NbhdId;
    loadScreen('sort-screen', function () {
        $('.tools .dashboard').show();
        $('.tools .sort').show();
        $('.tools .search').show();
        $('.tools .sync').show();
        $('.tools .logout').show();
        if (callback) callback();
    });


    getData("SELECT * FROM Neighborhood", [], function (results) {
        var agreeNbhd = results[0].ProfileReasonable;
        var neg = (agreeNbhd == "true" ? "false" : "true");
        $('.agree-nbhd[agree="' + neg + '"]').addClass('unselected');
        $('.agree-nbhd[agree="' + agreeNbhd + '"]').removeClass('unselected');

        if (results[0].ProfileReasonable == "true") {
            $('.nbhd-profile-float').removeClass('red');
        } else {
            $('.nbhd-profile-float').addClass('red');
        }
        if (results[0].LastComment != undefined) {
            $('.nbhd-profile-comment').val(results[0].LastComment);
        }
        if (showonly)
            return;
        //if(isBPPUser && combinedView) combinedView = false;
        ShowParcelListWithNoSort();
    });

}

function showDashboard() {
    loadScreen('user-dashboard', function () {
        $('.dashboard-nbhd').html('')
        prod.updateDashboard(currentNbhd);
    });
}

function showSearch() {
    addSearchOptions();
    activeParcel = null;
    loadScreen('search');
    $('.btn-parcel-search').removeAttr('disabled');
    $('.btn-parcel-search').html('Search');
    $('.nbhd-profile-select').val(currentNbhd);
    $('.nbhd-profile-select').trigger('change')
}

function showSearchResults() {
}

function showDigitalPRC() {
    appState.photoCatId = 0;
    $('#digital-prc .create-Bpp-parcel, #digital-prc .mark-as-delete').hide();
    if (activeParcel.CautionMessage != null) {
        var cautionMsg = activeParcel.CautionMessage.replace(/&lt;/g, '<').replace(/&gt;/g, '>') || clientSettings["DefaultCaution"] || 'CAUTION!';
        $('.caution-prc').removeClass('hidden');
        $('.caution-prc').text(cautionMsg);
    }
    else {
        $('.caution-prc').addClass('hidden');
    }
    loadScreen('digital-prc', function () {
        if (isBPPUser && activeParcel.CC_Deleted == 'false' && (!(listType == 3 && (parcelOpenMode == 'RP' || activeParcel.IsRPProperty == 'true')) || ParcelopenfromMap)) { //listType == 1 changed to 3 because need to show the add button FD_18924, listType == 3 not in
            if ($('#digital-prc .fy_selector').css('display') != 'none') {
                $('#digital-prc .create-Bpp-parcel').css('right', '190px');
                $('#digital-prc .mark-as-delete').css('right', '155px')
            }
            $('#digital-prc .create-Bpp-parcel').show();
            if (activeParcel.IsParentParcel != 'true')
                $('#digital-prc .mark-as-delete').show();
        }
        else
            $('#digital-prc .create-Bpp-parcel, #digital-prc .mark-as-delete').hide();
        setActiveScreenTitle('Property Record', activeParcel);
        //$('header span', $('#' + ccma.UI.ActiveScreenId)).html('Property Record - ' + activeParcel.StreetAddress);
        refreshDigitalPRC();
    });

}

function refreshDigitalPRC() {
    if (activeParcel) {
        catArray = []; firstTime = true;
        if (isBPPUser && activeParcel.IsPersonalProperty == 'true')
            $('.prc-card').html($('#prc-template-bpp').html());
        else
            $('.prc-card').html($('#prc-template').html());
        if (EnableNewPriorities == 'True') {
            $('.prc_priority').show();
            let prcolr = getPriorityClr(activeParcel.CC_Priority);
            $('.prc_priority').html(prcolr.prty);
            if (prcolr.clr) { $('.prc_priority').css('background-color', prcolr.clr); }
            else { $('.prc_priority').css('background-color', ""); }
            $('.prc_priority').show();
        }
        else $('.prc_priority').hide();

        let s = prcPreviewSketch(true), sktchImgPrc = $('.prc-card img[srcx="${SketchPreview}"]')[0] ? $('.prc-card img[srcx="${SketchPreview}"]') : $('.prc-card img[src="${SketchPreview}"]'),
            prcFirstImg = $('.prc-card img[srcx="${FirstPhoto}"]')[0] ? $('.prc-card img[srcx="${FirstPhoto}"]') : $('.prc-card img[src="${FirstPhoto}"]');

        if (prcFirstImg) $(prcFirstImg).addClass('fitPrcImg');

        if (sktchImgPrc[0] && s.skArray?.length > 0) {
            let html = '';

            if ($('.prc-card .select-sketch-segments')[0]) $('.prc-card .select-sketch-segments').remove();
            s.skArray.forEach(function (sk, i) { html += '<option val="' + i + '">' + sk + '</option>'; });
            if (s.flg) {
                $(sktchImgPrc).parent().prepend('<div style="display:flex;justify-content: end;"><span class="invalidalerticonprc"></span><span class="select-sketch-segments-prc" style="width: 100px; float: right; height: 24px;"><select onchange="prcPreviewSketch()" style="width: 100px;">' + html + '</select></span></div>')
            }
            else {
                $(sktchImgPrc).parent().prepend('<span class="select-sketch-segments" style="width: 110px; float: right; height: 28px;overflow:hidden; padding:3px;"><select onchange="prcPreviewSketch()" style="width: 100px;">' + html + '</select></span>')
            }
        }
        else if (s.flg) {
            $(sktchImgPrc).parent().prepend('<div style="display:flex;justify-content: end;"><span class="invalidalerticonprc"></span>');
        }

        $('.prc-card').fillTemplate([activeParcel]);
        $('[srcx]', '.prc-card').each((i, x) => {
            $(x).attr('src', $(x).attr('srcx'));
        });

        if (clientSettings?.ShowEmptyCategoryOnPRC != '1') {
            $('.prc-card table:not(:has(tbody))').each((i, el) => {
                let onclickAttributeValue = el.getAttribute('onclick'), ptd = false;
                if (!onclickAttributeValue && $(el).parent('td')[0]?.getAttribute('onclick')) {
                    onclickAttributeValue = $(el).parent('td')[0].getAttribute('onclick');
                    ptd = true;
                }
                if (ptd && onclickAttributeValue?.includes('showDcTab')) $(el).parent('td').hide();
                else if (onclickAttributeValue?.includes('showDcTab')) $(el).hide();
            });
        }
        //refreshScrollable();
        $('[onclick]').each(function () {
            var onclickHideSketch = $(this).attr('onclick');
            if (clientSettings["HideSketchTab"] == "1") {
                if (onclickHideSketch.includes('showSketches')) {
                    $(this).closest('td').hide();
                }
            }
        });
    }
    
}

var cachedSortScreen = {};
function showComparables() {
    loadScreen('comparables', function () {
        //FILLFROMTEMPLATE

        setActiveScreenTitle('Comparables', activeParcel);
        $('.parcel-comparables').html($('#comparables-template').html());
        $('.parcel-comparables').fillTemplate([activeParcel]);

        $('#comp-estimate-chart').html($('#estimate-chart-template').html());
        $('#comp-estimate-chart').fillTemplate([activeParcel], false);
        bindEstimateChartSelection();
        $('.sort-screen-select td[class="select"]', $('#comp-estimate-chart')).removeClass('selected-cell');
        $('.sort-screen-select td[value="' + activeParcel.SelectedAppraisalType + '"]', $('#comp-estimate-chart')).addClass('selected-cell');

        //$('.comp-field-alert-flag').html('<option value="${Id}">${Name}</option>'); $('.comp-field-alert-flag').fillTemplate(fieldAlerts);
        $('.comp-field-alert-flag').removeClass('cc-drop');
        $('.comp-field-alert-flag').empty();
        $('.comp-field-alert-flag').lookup({ popupView: true, width: 288, searchActive: false, searchActiveAbove: 8, searchAutoFocus: false, onSave: () => { } }, []);
        $('.comp-field-alert-flag')[0].getLookup.setData(fieldAlerts);


        $('.comp-field-alert-flag').unbind('change');
        $('.comp-field-alert-flag').bind('change', function () {
            refreshSortScreen = true;
            var flag = $(this).val();
            ccma.Sync.enqueueParcelAttributeChange(activeParcel.Id, 'FieldAlertType', flag, function () {
                $('.select-flag', $('nav[parcelid="' + activeParcel.Id + '"]')).val(flag);
                getData("SELECT * FROM Parcel WHERE  Id = " + activeParcel.Id + "", [], function (p) {
                    if (p[0].Reviewed)
                        executeQueries("UPDATE Parcel SET Reviewed = 'false' WHERE Id = " + activeParcel.Id)
                });
            });
        });

        isBPPUser && (activeParcel.CC_Deleted == 'true' || (activeParcel.IsRPProperty == 'true' && activeParcel.IsParentParcel == 'true' && activeParcel.Reviewed == 'true') || (ccma.Session.RealDataReadOnly == true && activeParcel.IsRPProperty == 'true')) ? $('.comp-field-alert-flag').attr('disabled', 'disabled') : $('.comp-field-alert-flag').removeAttr('disabled');
        $('.comp-field-alert-flag').val(activeParcel.FieldAlertType);
        $('.scrollable .field-alert-message ').html(activeParcel.FieldAlert);

    });

}
function showParcel(parcelId, callback, openfromMap, doNotShowChilds, fromRefreshScreen) {
    if (isBPPUser && (listType != 0 || openfromMap) && ParcelsNotInGroup.some(function (item) { return item === parseInt(parcelId) }) && (clientSettings['PersonalPropertyDownloadType'] == 'BPP' || (ccma.Session.bppRealDataEditable == 0 && !ccma.Session.bppRealDataReadOnly))) {
        messageBox('You only have permission to view the parcel from Sort Screen.');
        return;
    }

    if (openfromMap != undefined) appState.openfromMap = openfromMap;
    else appState.openfromMap = undefined;
    saveAppState();
    ParcelopenfromMap = openfromMap || false;
    _sketchStringThrowError = false; sketchSaveClicked = false; showDCCClicked = false; parent_missing_msg = false;
    $('#parcel-dashboard .create-Bpp-parcel').hide();
    if (ParcelopenfromMap && !fromRefreshScreen) {
        currentMapBounds = UsingOSM ? OsMap.getBounds() : map.getBounds();
        preMacBounds = currentMapBounds;
    }
    ccma.UI.ParcelLinks.OpenedFromLink = false;
    getParcel(parcelId, function (parcel) {
        if (isBPPUser && (listType != 0 || openfromMap) && parcel.IsRPProperty && (clientSettings['PersonalPropertyDownloadType'] == 'BPP' || (ccma.Session.bppRealDataEditable == 0 && !ccma.Session.bppRealDataReadOnly))) {
            messageBox('You only have permission to view the parcel from Sort Screen.');
            return;
        }

        if (isBPPUser && listType == 0 && !doNotShowChilds && !openfromMap && !combinedView) {
            activeParentParcel = activeParcel;
            activeParcel = null;
            listType = 1;
            cachedSortScreen.parcels = $("#my-parcels nav").clone();
            cachedSortScreen.headerText = $('header .headerspan', $('#sort-screen')).html();
            loadParcels();
            $('.select-parcelspanHeader .selectParcelCheckbox').show('selectedcheckbox');
            $('.sort-toggle').hide();
            return;
        }
        else if (isBPPUser && listType == 0 && (!doNotShowChilds || !openfromMap) && !combinedView) {
            parcelOpenMode = 'RP';
            $('#parcel-dashboard .create-Bpp-parcel').attr('onclick', 'createNewParcel(activeParcel.Id)');
            $('#parcel-dashboard .create-Bpp-parcel').show();
        }
        else if (isBPPUser && !combinedView) {
            if (activeParcel.IsParentParcel == 'true') {
                parcelOpenMode = 'RP';
                if (listType == 1 || ParcelopenfromMap) {
                    $('#parcel-dashboard .create-Bpp-parcel').attr('onclick', 'createNewParcel(activeParcel.Id)');
                    $('#parcel-dashboard .create-Bpp-parcel').show();
                }
            }
            else
                parcelOpenMode = 'BPP';
        }
        loadScreen('parcel-dashboard', function () {
            loadParcel = false;
            saveAppState();

            if ($.trim($('#parcel-header-template').html()) != '') {
                $('.parcel-header-temp').removeClass('hidden');
                $('.parcel-header-temp').parent().attr("width", "100%");
                $('.parcel-header-temp').html($('#parcel-header-template').html());
                $('.parcel-header-temp').fillTemplate(activeParcel);
                setActiveScreenTitle('');
            }
            else {
                setActiveScreenTitle('Dashboard', activeParcel);
            }
            //$('header span', $('#' + ccma.UI.ActiveScreenId)).html('Parcel (' + parcel.KeyValue1 + ') - ' + parcel.StreetAddress);
            $('span', '.nav-data-collection').html(parcel.StreetAddress);

            if (callback) callback();
        });
        if (activeParcel) {
            $('#photo-catalogue').html('');
            photos.pid = activeParcel.Id;
            photos.keyvalue = activeParcel.KeyValue1;
            photos.fillCatalogue(activeParcel.Id, '#photo-catalogue', 1);
        }
    });
}

function showDataCollection(tab, recordIndex, childSpec, isPrc, isLoadChild) {
    var newShowfun = function () {
        if (tab == undefined) {
            activeParcel = sortSourceData(activeParcel)
            tab = 0;
            showDCCClicked = true;
            sketchSaveClicked = false;
        }
        var returnDccTrackedValue = trackedValue();
        if (!returnDccTrackedValue) return false;
        var isPrcTrue = isPrc && isPrc.notRefreshAll ? { isPrc, tab } : false;
        $('.record-photos').hide();
        $('.dc-wrapper', '#data-collection').hide();
        if ($.trim($('#parcel-header-template').html()) == '')
            $('header span:first-child', '#data-collection').html('Loading Data Collection Card ...');
        $('.dc-categories-condensed').unbind(touchClickEvent);
        $('.dc-categories-condensed').bind(touchClickEvent, function (e) {
            setTimeout(function () {
                expandCategoryMenu();
            }, 390);
        });
        $('textarea').unbind('touchstart', 'touchmove');
        var ts;
        $('textarea').bind('touchstart', function (e) {
            ts = (iPad ? e.pageY : e.targetTouches[0].pageY);
        });
        $('textarea').bind('touchmove', function (e) {
            if (e.touches.length != 1) return;
            e.stopPropagation();
            e.preventDefault();
            var te = (iPad ? e.pageY : e.targetTouches[0].pageY);
            if (ts < te) {
                $(this).scrollTop($(this).scrollTop() - 5)
            } else {
                $(this).scrollTop($(this).scrollTop() + 5)
            }
        });
        $('#dc-categories a[type="ParentCat"]').show();
        if ((isBPPUser && activeParcel.IsRPProperty == 'true') || isRPUser)
            $('#dc-categories a[BPPType="1"]').hide();
        else if (isBPPUser && activeParcel.IsPersonalProperty == 'true')
            $('#dc-categories a[BPPType="0"]').hide();

        loadScreen('data-collection', function () {
            firstTimeLoad = true;
            loadDataCollectionValues(null, null, null, isPrcTrue);
            if ($.trim($('#parcel-header-template').html()) != '') {
                setActiveScreenTitle('');
                $('.parcel-header-temp').removeClass('hidden');
                $('.parcel-header-temp').parent().attr("width", "100%");
                $('.parcel-header-temp').html($('#parcel-header-template').html());
                $('.parcel-header-temp').fillTemplate(activeParcel);

            } else {
                setActiveScreenTitle('Data Entry', activeParcel);
            }
            //$('header span', $('#' + ccma.UI.ActiveScreenId)).html('Parcel (' + activeParcel.KeyValue1 + ') - ' + activeParcel.StreetAddress);
            //$('#dc-categories').css({ 'left': '-' + 290 + 'px' });
            if (isBPPUser && activeParcel.CC_Deleted != 'true' && (!(listType == 3 && (parcelOpenMode == 'RP' || activeParcel.IsRPProperty == 'true')) || ParcelopenfromMap)) {
                $('#dc-form .create-Bpp-parcel').show();
                if (activeParcel.IsParentParcel != 'true')
                    $('#dc-form .mark-as-delete').css('display', 'inline');
                else $('#dc-form .mark-as-delete').hide();
            } else
                $('#dc-form .create-Bpp-parcel, #dc-form .mark-as-delete').hide();
            $('.record-photos').show();
            $('.dc-wrapper', '#data-collection').show();
            //var timer=(tab==0)?0:1000;
            //setTimeout(function(){ selectDcTab(tab, undefined, recordIndex); }, timer);
            selectDcTab(tab, undefined, recordIndex);
            $('#dc-form-contents section[name="quickreview"]').each(function (i, x) {
                if (x.style.display != 'none') {
                    var txtarea = $('textarea', x);
                    txtarea.each(function (j) {
                        if (txtarea[j].style.display != 'none') {
                            txtarea[j].style.cssText = 'height:auto';
                            txtarea[j].style.cssText = 'height:' + txtarea[j].scrollHeight + 'px';
                            iPad ? txtarea[j].style.maxHeight = '148px' : txtarea[j].style.maxHeight = '144px';
                        }
                    });
                }
            });
            firstTimeLoad = false;
            if (childSpec) loadChild(childSpec, isPrc);

            /*if (prcClick && lookupDataCounter == 0) { 
                $('#maskLayer').hide(); 
                prcClick = false;
            }
            else if (isPrc && !isLoadChild) {
                var timeOutval =  isPrc.notRefreshAll ? 3000: 5000;      
                setTimeout(function() { if (prcClick) { $('#maskLayer').hide(); prcClick = false; lookupDataCounter = 0; } }, timeOutval);
            } */
        });
    }

    if (event?.target && ccma.UI.ActiveScreenId == 'sort-screen' && $(event.target).parents('.parcel-item')[0] && $(event.target).parents('.parcel-item').attr('parcelid')) {
        let pidss = $(event.target).parents('.parcel-item').attr('parcelid');
        getParcel(pidss, () => {
            newShowfun();
        });
    } else { newShowfun(); }
}

function showSketchImages() {
    $('#photo-catalogue').html('');
    loadScreen('photos', function () {
        setActiveScreenTitle('Sketch Images', activeParcel);
        // $( '.delete-selected-photo' ).hide();
        photos.pid = activeParcel.Id;
        photos.keyvalue = activeParcel.KeyValue1;
        photos.fillCatalogue(activeParcel.Id, '#photo-catalogue', 2);
    });

}

function showPhotos(catId) {
    if (!checkPPAccess('dccPhoto')) {
        var msg = "This option is not available for selected parcel"
        if (activeParcel.CC_Deleted == "true")
            msg = "Photos cannot be taken on a property that is flagged to be deleted. Removing the Deleted option for this parcel will allow you to take photos."
        messageBox(msg);
        return;
    }
    appState.photoCatId = 0;
    $('#photo-catalogue').html('');
    loadScreen('photos', function () {
        setActiveScreenTitle('Photos', activeParcel);
        if (activeParcel.FirstPhoto.length == 0) {
            $('.delete-selected-photo').hide();
        } else {
            $('.delete-selected-photo').show();
        }
        //$('header span', $('#' + ccma.UI.ActiveScreenId)).html('Photos (' + activeParcel.KeyValue1 + ') - ' + activeParcel.StreetAddress);
        photos.pid = activeParcel.Id;
        photos.keyvalue = activeParcel.KeyValue1;
        photos.fillCatalogue(activeParcel.Id, '#photo-catalogue', 1);
        $('.mask').hide();
    });
}

function showSketches(vuid) {
    if (vuid != undefined) appState.vuid = vuid;
    else appState.vuid = undefined;
    saveAppState();
    if (vuid && (clientSettings.SketchConfig == 'FTC' || sketchSettings["SketchConfig"] == 'FTC')) {
        var record = activeParcel.IMP.filter(function (apex) { return apex.ROWUID == vuid })[0];
        vuid = record ? (record.IMPAPEXOBJECT[0] ? record.IMPAPEXOBJECT[0].ROWUID : null) : null;
    }

    if (vuid && (clientSettings.SketchConfig == 'Patriot' || sketchSettings["SketchConfig"] == 'Patriot')) {
        var record = activeParcel.CCV_Buildings_BuildingDepreciaton.filter(function (bld) { return bld.ROWUID == vuid })[0];
        vuid = record ? (record.CCV_Sketch[0] ? record.CCV_Sketch[0].ROWUID : null) : null;
    }
    loadScreen('sketchview', function () {
        setActiveScreenTitle('Sketches', activeParcel);
        //$('header span', $('#' + ccma.UI.ActiveScreenId)).html('Sketches (' + activeParcel.KeyValue1 + ') - ' + activeParcel.StreetAddress);

        var formatter = ccma.Sketching.SketchFormatter;
        var config = ccma.Sketching.Config;
        let sketches = [], isErrorSketch = false;
        try {
            sketches = activeParcel.getSketchSegments();
        }
        catch (ex) { isErrorSketch = true; console.log(ex); }

        var openSketchEditor = function (vuid) {
            sketchApp.resetOrigin();

            var ab = sketchRenderer.allbounds();

            sketchApp.refresh();
            sketchApp.zoom(1);
            sketchApp.setZoom(49);
            sketchEditorOpenFirstTime = true;
            //sketchApp.isOpenSketchEditorTrue = true;
            sketchApp.ischangeSketchOriginPosition = false; sketchApp.isInvalidSketchString = false;
            visionPCIs = [];
            try {
                sketchApp.open(sketches, vuid, null, config.AfterSave, config.BeforeSave);
            }
            catch (ex) { isErrorSketch = true; console.log(ex); }
            sketchApp.render();
            sketchApp.addVectorFromCache(localStorage.getItem("sketchCache"));
            sketchApp.panSketchPosition()
            sketchApp.isOpenSketchEditorTrue = true;
            localStorage.setItem("sketchCache", '');
            sketchopened = true;
            if (isErrorSketch) { messageBox('Sketch cannot be rendered due to the wrong sketch data.'); sketchApp.isInvalidSketchString = true; sketchApp.sketches = []; sketchApp.refreshControlList(); sketchApp.render(); }
        }

        previewSketch(vuid);

        $('.btn-open-sketch-editor').unbind(touchClickEvent);
        $('.btn-open-sketch-editor').bind(touchClickEvent, function (e) {
            e.preventDefault();
            var returnTrackedValue = trackedValue();
            if (!returnTrackedValue) return false;
            hideDistoClose();
            $('.ccse-toolbutton').addClass('ccse-tool-inactive');
            $('.zoom-control').hide()
            if (validate_SketchConfig() == false) { alert('Please complete your sketch configuration in admin console before you draw'); return false; }
            $('.overlay-sketch-editor').show();
            openSketchEditor(vuid);

            sketchApp.onUnSave = function () {
                //sketches = activeParcel.getSketchSegments(); 
                showSketches();
            }
            sketchApp.onSave = function (data, Notedata, beforeSave, afterSave, onError, cc_error_msg, auditTrailEntry) {
                if (!onError) onError = function (x) {
                    if ($('.ccse-canvas').hasClass('dimmer')) {
                        $('.ccse-canvas').removeClass('dimmer');
                        $('.dimmer').hide();
                    }
                    console.error(x)
                };
                if (beforeSave)
                    beforeSave();
                if (cc_error_msg)
                    ccma.Sync.enqueueParcelAttributeChange(activeParcel.Id, 'CC_Error', cc_error_msg);
                else
                    ccma.Sync.enqueueParcelAttributeChange(activeParcel.Id, 'CC_Error', '');
                if (auditTrailEntry)
                    ccma.Sync.enqueueEvent(activeParcel.Id, 'AuditTrailEntry', auditTrailEntry);
                var emptyBeforeSaveCallback = function (sd, other, newAuxRowId, callback, failedCallback) {
                    if (callback) callback();
                }

                var createNewRecordMethod = function (sd, pAuxRecord, newAuxRowId, callback, failedCallback) {
                    var newRecordData = {};
                    if (sd.labelField && sd.label) newRecordData[sd.labelField] = sd.label;
                    insertNewAuxRecordForCategory(sd.sourceName, pAuxRecord, newAuxRowId, newRecordData, callback, function () {
                        messageBox('Failed to save new sketch record/note. Invalid configuration.')
                        if (failedCallback) failedCallback();
                    });
                }
                var saveSketchNotes = function (Notedata, onComplete) {
                    var fieldIds, data = '$note$', parentid, ParentRecord = null;
                    if (Notedata.length == 0) { if (onComplete) onComplete(); return; }
                    var nt = Notedata.pop();
                    if (!nt.clientId)
                        nt.clientId = nt.noteid
                    var rowUID = nt.clientId;
                    var beforeSketchSave = emptyBeforeSaveCallback;
                    if (nt.newRecord) {
                        beforeSketchSave = createNewRecordMethod; parentid = nt.parentId;
                        ParentRecord = nt.config.SketchSource.Table ? _.where(activeParcel[nt.config.SketchSource.Table], { ROWUID: parentid })[0] : activeParcel;
                        if (!ParentRecord) {
                            messageBox('Failed to save new Notes. Invalid configuration.')
                            if (onComplete) onComplete(); return;
                        }
                    }
                    beforeSketchSave(nt, ParentRecord, rowUID, function (newid) {
                        if (newid)
                            rowUID = newid.toString();
                        var checkRow = nt.sourceName ? _.where(activeParcel[nt.sourceName], { ROWUID: rowUID })[0] : activeParcel;
                        checkRow[nt.noteField] = nt.noteText;
                        var updateSql = 'UPDATE ' + nt.sourceName + ' SET ';
                        nt.noteText = nt.noteText.replace(/\"/g, '""');
                        updateSql += nt.noteField + ' = "' + nt.noteText + '"';
                        if (nt.xPositionField) {
                            updateSql += ',' + nt.xPositionField + ' = ' + (nt.xPosition || 0) + '';
                            checkRow[nt.xPositionField] = (nt.xPosition || 0);
                        }
                        if (nt.yPositionField) {
                            updateSql += ',' + nt.yPositionField + ' = ' + (nt.yPosition || 0) + '';
                            checkRow[nt.yPositionField] = (nt.yPosition || 0);
                        }
                        var sqlParams = [];
                        updateSql += ' WHERE ROWUID = ' + rowUID;
                        if (nt.noteText == '*DEL*') {
                            updateSql = 'DELETE FROM ' + nt.sourceName + ' WHERE ROWUID = ' + rowUID;
                            if (nt.sourceName)
                                activeParcel[nt.sourceName] = _.reject(activeParcel[nt.sourceName], function (x) { return x.ROWUID == rowUID });
                            fieldIds = nt.noteFieldID; data += nt.noteText;
                        }
                        else {
                            fieldIds = nt.noteFieldID + '|' + nt.xPositionFieldId + '|' + nt.yPositionFieldId;
                            data += nt.noteText + '|' + nt.xPosition + '|' + nt.yPosition;
                        }

                        getData(updateSql, sqlParams, function (ar, d, res) {
                            ccma.Sync.enqueueParcelChange(activeParcel.Id, d.rowUID, null, null, null, 'UPDATESKETCH', d.fieldIds, d.data, null, function () {
                                if (Notedata.length > 0)
                                    saveSketchNotes(Notedata, onComplete);
                                else
                                    if (onComplete) onComplete();
                            });
                        }, {
                            rowUID: rowUID,
                            data: data,
                            fieldIds: fieldIds
                        });
                    }, onComplete)
                }
                var saveSketchItem = function (sdata, onComplete) {
                    var sd = sdata.pop();
                    if (!sd.clientId)
                        sd.clientId = sd.sid
                    if (sd.sid.includes("--")) sd.sid = sd.sid.split("--")[1]
                    var rowUID;
                    var vectorString = sd.vectorString;
                    var fieldId = sd.sourceFieldId;
                    var segments = sd.segments || [];
                    var beforeSketchSave = emptyBeforeSaveCallback;
                    var skhold = null, oldValueObj = {};
                    if (sd.insertRecord) beforeSketchSave = createNewRecordMethod;
                    beforeSketchSave(sd, sd.parentRow, sd.clientId, function (newid) {
                        if (newid)
                            sd.clientId = newid.toString();
                        rowUID = sd.clientId;
                        var checkRow = sd.sourceName ? _.where(activeParcel[sd.sourceName], { ROWUID: rowUID })[0] : activeParcel;
                        checkRow[sd.sourceField] = sd.vectorString;
                        var checkRoworg = sd.sourceName ? _.where(activeParcel.Original[sd.sourceName], { ROWUID: rowUID })[0] : activeParcel.Original;
                        skhold = checkRoworg[sd.sourceField];
                        var updateSql = 'UPDATE ' + sd.sourceName + ' SET ';
                        if (sd.sourceName == null) {
                            updateSql = 'UPDATE Parcel SET ';
                        }

                        var newVectorString = vectorString;
                        try {
                            if (JSON.parse(newVectorString)) {
                                newVectorString = newVectorString.replace(/\'/g, "''");
                            }
                        }
                        catch (e) { }
                        console.log(newVectorString);
                        updateSql += sd.sourceField + ' = \'' + newVectorString + '\'';
                        oldValueObj[sd.sourceField] = skhold;

                        for (var si in segments) {
                            var seg = segments[si];
                            if (seg.field) {
                                updateSql += ", " + seg.field + " = " + (seg.area || 0);
                                checkRow[seg.field] = (seg.area || 0);
                                oldValueObj[seg.field] = checkRoworg[seg.field];
                            }

                        }
                        //segments.forEach(function (seg) { if (seg.field) updateSql += ", " + seg.field + " = " + (seg.area || 0) + ";" });
                        //if (sd.areaField) updateSql += ',' + sd.areaField + ' = ' + (sd.area || 0) + '';
                        if (sd.perimeterField) {
                            updateSql += ',' + sd.perimeterField + ' = ' + (sd.perimeter || 0) + '';
                            checkRow[sd.perimeterField] = (sd.perimeter || 0);
                            oldValueObj[sd.perimeterField] = checkRoworg[sd.perimeterField];
                        }

                        let sketchProStr = '';
                        if (sd.sketchProField) {
                            let skLbl = (checkRow['SOURCE'] ? (checkRow['SOURCE'] + "/") : "") + (checkRow['ACCOUNT_ID'] ? (checkRow['ACCOUNT_ID'] + "/") : "") + (checkRow['NAME'] ? (checkRow['NAME']) : "");
                            if (skLbl != '' && skLbl[skLbl.length - 1] == '/') skLbl = skLbl.slice(0, -1);
                            if (skLbl == '' && activeParcel) skLbl = activeParcel.KeyValue1;
                            sketchProStr = vectorString != '' ? convertHelionToSketchPro(vectorString, skLbl, sd.labelPositions) : '';
                            sketchProStr = sketchProStr.replace(/\'/g, "''");
                            updateSql += ',' + sd.sketchProField + ' = \'' + sketchProStr + '\'';
                            checkRow[sd.sketchProField] = (sketchProStr || "");
                            oldValueObj[sd.sketchProField] = checkRoworg[sd.sketchProField];
                        }

                        if (sd.labelCommandField) {
                            updateSql += ',' + sd.labelCommandField + ' = \'' + (sd.labelCommand || '') + '\'';
                            checkRow[sd.labelCommandField] = (sd.labelCommand || 0);
                            oldValueObj[sd.labelCommandField] = checkRoworg[sd.labelCommandField];
                        }
                        if (sd.labelField && sd.labelFieldId > 1) {
                            updateSql += ',' + sd.labelField + ' = \'' + (sd.label || '') + '\'';
                            checkRow[sd.labelField] = (sd.label || '');
                            oldValueObj[sd.labelField] = checkRoworg[sd.labelField];
                        }
                        if (sd.dimensionCommandField) {
                            updateSql += ',' + sd.dimensionCommandField + ' = \'' + (sd.dimensionCommand || '') + '\'';
                            checkRow[sd.dimensionCommandField] = (sd.dimensionCommand || 0);
                            oldValueObj[sd.dimensionCommandField] = checkRoworg[sd.dimensionCommandField];
                        }
                        if (sd.vectorStartFieldId > -1 && sd.vectorStartField) {
                            updateSql += ',' + sd.vectorStartField + ' = \'' + (sd.vectorStart || '') + '\'';
                            checkRow[sd.vectorStartField] = (sd.vectorStart || 0);
                            oldValueObj[sd.vectorStartField] = checkRoworg[sd.vectorStartField];
                        }
                        if (sd.vectorPageFieldId > -1 && sd.vectorPageField) {
                            updateSql += ',' + sd.vectorPageField + ' = \'' + (sd.vectorPage || 'false') + '\'';
                            checkRow[sd.vectorPageField] = (sd.vectorPage || 'false');
                            oldValueObj[sd.vectorPageField] = checkRoworg[sd.vectorPageField];
                        }
                        if (sd.footModeFieldId > -1 && sd.footModeField) {
                            updateSql += ',' + sd.footModeField + ' = \'' + (sd.footMode || 'false') + '\'';
                            checkRow[sd.footModeField] = (sd.footMode || 'false');
                            oldValueObj[sd.footModeField] = checkRoworg[sd.footModeField];
                        }
                        sd.extraLabels.forEach(function (item) {
                            if (item.FieldId) {
                                updateSql += ',' + item.FieldName + ' = \'' + (item.Value || '') + '\'';
                                checkRow[item.FieldName] = (item.Value || '');
                                oldValueObj[item.FieldName] = checkRoworg[item.FieldName];
                            }
                        })

                        updateSql += ' WHERE ROWUID = ' + rowUID;

                        if (vectorString == '*DEL*') {
                            updateSql = 'DELETE FROM ' + sd.sourceName + ' WHERE ROWUID = ' + rowUID;
                            if (sd.sourceName)
                                activeParcel[sd.sourceName] = _.reject(activeParcel[sd.sourceName], function (x) { return x.ROWUID == rowUID });
                        }

                        var sqlParams = [];
                        var fieldIds = Object.keys(sd.segments).map(function (x) { return sd.segments[x].fieldId }).filter(function (x) { return x > -1 }).reduce(function (x, y) { return x + "|" + y }, fieldId) + '|' + (sd.perimeterFieldId || -1) + '|' + (sd.labelCommandFieldId || -1) + '|' + (sd.dimensionCommandFieldId || -1) + '|' + (sd.labelFieldId || -1) + '|' + (sd.vectorStartFieldId || -1) + '|' + (sd.vectorPageFieldId || -1) + '|' + (sd.footModeFieldId || -1) + '|' + ((sd.extraLabels.map(function (a) { return (a.FieldId || -1) }).length > 0) ? sd.extraLabels.map(function (a) { return (a.FieldId || -1) }).join('|') : '-1') + '|' + (sd.sketchProFieldId || -1);
                        var data = Object.keys(sd.segments).map(function (x) { return sd.segments[x] }).filter(function (x) { return x.fieldId > -1 }).map(function (x) { return x.area || 0 }).reduce(function (x, y) { return x + "|" + y }, vectorString) + '|' + (sd.perimeter || 0) + '|' + (sd.labelCommand || '') + '|' + (sd.dimensionCommand || '') + '|' + (sd.label || '') + '|' + sd.vectorStart + '|' + (sd.vectorPage || 'false') + '|' + (sd.footMode || 'false') + '|' + sd.extraLabels.map(function (a) { return (a.Value || '') }).join('|') + '|' + (sketchProStr || "");
                        var oldValueStr = JSON.stringify(oldValueObj);

                        console.log(updateSql);
                        getData(updateSql, sqlParams, function (ar, d, res) {
                            sqlinsert = 'Deleted';
                            var fIds = d.fieldIds.split('|').filter(function (f) { return f != -1 });
                            fIds.forEach(function (f) {
                                activeParcel.Changes.push({ Action: "E", AuxRowId: d.rowUID, ChangedTime: Date.now(), Field: datafields[f].Name, FieldId: f, NewValue: d.data, ParcelId: activeParcel.Id })
                            });
                            ccma.Sync.enqueueParcelChange(activeParcel.Id, d.rowUID, null, null, d.oldValueStr, 'UPDATESKETCH', d.fieldIds, d.data, null, function () {
                                if (sdata.length > 0) saveSketchItem(sdata, onComplete);
                                else if (onComplete) onComplete();
                            });
                        }, {
                            rowUID: rowUID,
                            data: data,
                            fieldIds: fieldIds,
                            skold: skhold,
                            oldValueStr: oldValueStr
                        });
                    }, onComplete);
                }

                if (data.length > 0) {
                    saveSketchItem(data.reverse(), function () {
                        localStorage.setItem("sketchCache", '')
                        saveSketchNotes(Notedata, function () {
                            afterSketchSave(afterSave);
                        });
                    })
                }
                else {
                    if (Notedata.length == 0) {
                        if (afterSave)
                            afterSave();
                        return;
                    }
                    else {
                        saveSketchNotes(Notedata, function () {
                            afterSketchSave(afterSave);
                        })
                    }
                }




            }
        });

    });

}

function afterSketchSave(afterSave) {
    executeQueries("UPDATE Parcel SET Reviewed = 'false' WHERE Id = " + parseInt(activeParcel.Id))
    let qrs = EnableNewPriorities == 'True' ? ("UPDATE Parcel SET CC_Priority = 3 WHERE CC_Priority = 0 AND Id = " + parseInt(activeParcel.Id)) : ("UPDATE Parcel SET CC_Priority = 1 WHERE CC_Priority = 0 AND Id = " + parseInt(activeParcel.Id));
    executeQueries(qrs);
    previewSketch();
    if (appState.vuid)
        refreshDataCollectionValues(null, (clientSettings["SketchAreaField"] || sketchSettings['SketchAreaField']), (clientSettings["SketchVectorTable"] || sketchSettings['SketchVectorTable']));
    showSketches()
    if (afterSave)
        afterSave();
}

function previewSketch(vuid, ddlId) {
    let sketches = [];

    sketchRenderer.resetOrigin(); sketchRenderer.showOrigin = false; sketchRenderer.viewOnly = true; $('.invalidalerticon').hide();

    try {
        sketches = activeParcel.getSketchSegments(true);
        sketchRenderer.open(sketches, vuid, true);
    }
    catch (ex) {
        $('.invalidalerticon').show(); console.log(ex);
    }

    let blank_image = "data:image/png;base64,R0lGODlhFAAUAIAAAP///wAAACH5BAEAAAAALAAAAAAUABQAAAIRhI+py+0Po5y02ouz3rz7rxUAOw==",
        url = blank_image;

    $('.ccse-img-container').show();
    if (ddlId == undefined || ddlId == '-1') {
        if (ddlId == undefined && clientSettings?.EnableSingleSketchPreviewInMA == "1") {
            let lks = [{ Id: -1, Name: '--- select ---'}];
            sketchRenderer.sketches.forEach(function (sk, i) {
                lks.push({ Id: i, Name: sk.label });
            });

            $('.skpage-ddl').removeClass('cc-drop');
            $('.skpage-ddl').empty();
            $('.skpage-ddl').lookup({ popupView: true, width: 225, searchActive: false, searchActiveAbove: 8, searchAutoFocus: false, onSave: () => { previewSketch(null, $('.skpage-ddl').val()); } }, []);
            $('.skpage-ddl')[0].getLookup.setData(lks);
            $('.skpage-ddl').val('-1');
            $('.skpage-ddl').show();
        }

        let ab = sketchRenderer.allbounds(true);
        sketchRenderer.resizeCanvas((ab.xmax - ab.xmin) * DEFAULT_PPF + 95, (ab.ymax - ab.ymin) * DEFAULT_PPF + 95);
        sketchRenderer.pan(sketchRenderer.formatter.originPosition == "topRight" ? applyRoundToOrigin(-ab.xmax * DEFAULT_PPF + 15) : applyRoundToOrigin(-ab.xmin * DEFAULT_PPF + 15), sketchRenderer.formatter.originPosition == "topRight" || sketchRenderer.formatter.originPosition == "topLeft" ? applyRoundToOrigin(-ab.ymax * DEFAULT_PPF + 15) : applyRoundToOrigin(-ab.ymin * DEFAULT_PPF + 15));
        sketchRenderer.renderAll();
        if ($('.ccse-img-container .ccse-img').height() < 10000 && $('.ccse-img-container .ccse-img').width() < 10000)
            url = sketchRenderer.toDataURL();
        activeParcel.SketchPreview = url == "data:," ? blank_image : url;
    }
    else if (ddlId > -1)  {
        sketchRenderer.currentSketch = sketchRenderer.sketches[ddlId];
        let ab = sketchRenderer.sketchBounds();
        sketchRenderer.resizeCanvas((ab.xmax - ab.xmin) * DEFAULT_PPF + 95, (ab.ymax - ab.ymin) * DEFAULT_PPF + 95);
        sketchRenderer.pan(sketchRenderer.formatter.originPosition == "topRight" ? applyRoundToOrigin(-ab.xmax * DEFAULT_PPF + 15) : applyRoundToOrigin(-ab.xmin * DEFAULT_PPF + 15), sketchRenderer.formatter.originPosition == "topRight" || sketchRenderer.formatter.originPosition == "topLeft" ? applyRoundToOrigin(-ab.ymax * DEFAULT_PPF + 15) : applyRoundToOrigin(-ab.ymin * DEFAULT_PPF + 15));
        sketchRenderer.clearCanvas();
        clearVectorlabelPosition();
        for (var x in sketchRenderer.sketches[ddlId].vectors) {
            sketchRenderer.sketches[ddlId].vectors[x].render();
        }
        sketchRenderer.sketches[ddlId].renderNotes();
        if ($('.ccse-img-container .ccse-img').height() < 10000 && $('.ccse-img-container .ccse-img').width() < 10000)
            url = sketchRenderer.toDataURL();
        url == "data:," ? blank_image : url;
    }

    $('.ccse-img-container').hide();
    $('.sketch-preview').css({ 'background-image': 'url(' + url + ')' });
}


function applyRoundToOrigin(v) {
    if (sketchRenderer?.config?.formatter == 'Vision') v = Math.ceil(v);
    return v;
}

function showGoogleDirections() {
    loadScreen('google-directions', function () {
        setActiveScreenTitle('Directions', activeParcel);
        activeParcel.GetDirections(function (response) {
            $('#directions').html($('#directions-template').html());
            $('#directions').fillTemplate(response);
        });
    });

}

function showNavigon(proceed) {
    if (!ccma.Environment.iPad) {
        messageBox("Navigation assistance using Navigon is not compatible with this device.");
        return false;
    }
    if (clientSettings.GPSNavigationApp == 'Sygic') {
        showSygic(proceed);
        return false;
    }
    messageBox("You are being redirected to Navigon to assist you in reaching this property. Tap OK to proceed.", ["OK", "Cancel"], function () {
        navigon = "navigon://address//////" + escape(activeParcel.StreetAddress) + "/" + activeParcel.Longitude + "/" + activeParcel.Latitude + "";
        $('#hiddentarget').attr('src', navigon);
        window.setTimeout(function () {
            navigon = "navigon://|" + escape(activeParcel.StreetAddress) + "||||||" + activeParcel.Longitude + "|" + activeParcel.Latitude + "||";
            $('#hiddentarget').attr('src', navigon);
        }, 500);
    });
    //if (activeParcel.Latitude && activeParcel.Longitude)
    //    messageBox("You are being redirected to the Sygic navigation app to assist you in reaching the subject property's destination. Tap on the link button to proceed.<br/><br/><a href='com.sygic.aura://coordinate|" + activeParcel.Longitude + '|' + activeParcel.Latitude + "|drive'>Navigate</a>", ["Cancel"], function () {
    //    	return false;
    //    });
    //else messageBox("This parcel does not have any GIS points");
}

function showSygic(proceed) {
    if (!ccma.Environment.iPad) {
        messageBox("Navigation assistance using Sygic is not compatible with this device.");
        return false;
    }
    if (clientSettings.GPSNavigationApp == 'Navigon') {
        showNavigon(proceed);
        return false;
    }
    if (activeParcel.Latitude && activeParcel.Longitude) {
        $('#sygiclick').remove();
        var url = "com.sygic.aura://" + encodeURI("coordinate|" + activeParcel.Longitude + '|' + activeParcel.Latitude + "|drive");
        messageBox("You are being redirected to the Sygic navigation app to assist you in reaching this property. Tap OK to proceed.", ["OK", "Cancel"], function () {
            $('body').append('<a id="sygiclick" href="' + url + '" style="display: none;">&nbsp;</a>');
            $('#sygiclick').trigger('click');
        });
    } else if (activeParcel.StreetAddress) {
        $('#sygiclick').remove();
        var url = "com.sygic.aura://search|" + escape(activeParcel.StreetAddress) + "|drive";
        messageBox("You are being redirected to the Sygic navigation app to assist you in reaching this property. Tap OK to proceed.", ["OK", "Cancel"], function () {
            $('body').append('<a id="sygiclick" href="' + url + '" style="display: none;">&nbsp;</a>');
            $('#sygiclick').trigger('click');
        });
    }
    else messageBox("This parcel does not have any GIS points");
}

function showWaze(proceed) {
    if (!ccma.Environment.iPad) {
        messageBox("Navigation assistance using Waze is not compatible with this device.");
        return false;
    }

    if (activeParcel.Latitude && activeParcel.Longitude) {
        var url = "https://waze.com/ul?q=" + escape(activeParcel.StreetAddress) + "&ll=" + activeParcel.Latitude + "%2C" + activeParcel.Longitude + "&navigate=yes&zoom=15"
        //var url = "waze://ul?ll=" + activeParcel.Latitude + "%2C"+ activeParcel.Longitude +"&navigate=yes&zoom=15"
        messageBox("You are being redirected to the Waze navigation app to assist you in reaching this property.<br><span style='color: blue;font-size: 12px;'>Note: The app should be installed before proceeding.</span>", ["OK", "Cancel"], function () {
            invokeUrl(url);
        });
    }
    else messageBox("Sorry. This parcel does not have any GIS points");
}

function showGISInteractive(callback) {
    hideEagleView(true);
    hideNearmap(true);
    hideNearmapWMS(true);
    hideWoolpert(true);
    hideImagery();
    // omSpider?omSpider.unspiderfy():'';
    $('#mapCanvas').show();
    var screenCheck = activescreen
    $('.map-items').val('');
    if (activeParcel) {
        $('.Setting').hide();
        if (activeParcel.Latitude == null) {
            messageBox('No map coordinates available for this parcel.');
            return false;
        }
        if (OsMap) {
            OsMap.options.maxZoom = 19;
        }
    }
    else if (!activeParcel && UsingOSM) {
        $('.Setting').show();
        OsMap.options.maxZoom = 21;
    }
    clearMap();
    if (OsMap) {
        if (markerGroup)
            markerGroup.clearLayers();
        hideLegendIcon();
    }
    else {
        for (let i = 0; i < gLocationmarker.length; i++) {
            gLocationmarker[i].setMap(null);
        }
        gLocationmarker = [];
    }
    parcelMapMode = false;
    parcelLinksMode = false;
    $('.map-pri-option').hide();
    $('.map-items').hide();
    // $('.Setting').hide();
    appState.prevScreenId = appState.screenId;
    loadScreen('gis-map', function () {
        $('.svMA').hide(); $('.svDropdown').hide(); $('.svRoot').hide(); $('.svDropdown').hide(); $('.sketchOverlay').remove();
        setScreenDimensions();
        if ($('.overlay')[0])
            $('.overlay').parent('div').hide();
        if (clientSettings["EnableSVInMA"] == "1" && OsMap) {
            OsMap.setMinZoom(10);
        }
        if (activeParcel != null) {
            setActiveScreenTitle('GIS Interactive Map', activeParcel);
            //$('header span', $('#' + ccma.UI.ActiveScreenId)).html('GIS Map - ' + activeParcel.StreetAddress);
            $('.Setting').hide();
            activeParcel.ShowOnMap();
            shadowToolbar();
            changedMapBounds = true;
            $('.heatmap-items').hide();
            if (OsMap) {
                hideLegendIcon();
                removeLegend();
                checkPictometry();
                checkEagleView();
                checkNearMap();
                checkNearMapWMS();
                checkImagery();
                checkWoolpert();
            }
            if (clientSettings.EnableqPublic && clientSettings.EnableqPublic == '1' && $('.map-items option').length == 0) {
                $('.q_public').show();
                $('.q_public button').html('Show qPublic');
                $('.q_public button').attr('onclick', 'show_qPublic();');
                $('.q_public button').show();
            } else {
                $('.q_public button').hide();
            }
            if (activeParcel && clientSettings["EnableSVInMA"] == "1") {
                initSVEvents(OsMap);
                initDraggableOverlay();
            }
            if (screenCheck == 'parcel-dashboard') showDefaultImagery();
            setMapItems(true);
        }

        else {
            if (HeatLayer) {
                $('.heatmap-items').show();
                // refreshParcelsInMap();
                showLegendIcon();
                $('#mapCanvas').unbind("touchend");
                $('#mapCanvas').bind("touchend", function (e) {   //ios13 focusout not working when touch on map screen
                    $('.heatmap-items').blur();
                });
            }
            if (clientSettings.EnableNearmap && clientSettings.EnableNearmap.includes('MA') && UsingOSM) {
                $('#alternateMap')[0].innerText = 'Nearmap';
            } else {
                $('#alternateMap')[0].innerText = 'Alternate Map';
            }
            if (clientSettings.EnableNearmapWMS && clientSettings.EnableNearmapWMS.includes('MA') && UsingOSM) {
                $('#alternateMap')[0].innerText = 'NearmapWMS';
                if (!activeParcel && OsMap) OsMap.options.maxZoom = 19;
            } else {
                $('#alternateMap')[0].innerText = 'Alternate Map';
            }






            $('.q_public').hide();
            if (checkWoolpert() && activeParcel != null) {
                if (showHide) {
                    $('.osmapsetup button').hide();
                    $('.map-items option').length > 1 ? $('.map-items').show() : $('.map-items').hide();
                    return;
                }
                $('.map-items').empty();
                var mapItemsCheck = [{ map: 'Nearmap', Enabled: checkNearMap() }, { map: 'NearmapWMS', Enabled: checkNearMapWMS() }, { map: 'Woolpert', Enabled: checkWoolpert() }, { map: 'Eagle View', Enabled: checkPictometry() }, { map: 'Web Service Imagery', Enabled: checkImagery() }, { map: 'qPublic', Enabled: clientSettings.EnableqPublic && clientSettings.EnableqPublic == '1' }];
                if (mapItemsCheck.filter((x) => { return x.Enabled }).length > 0) {
                    $('.map-items').show();
                }
            } else {
                $('.map-items').hide();
            }
            //setActiveScreenTitle('GIS Map View');
            $('header span', $('#gis-map')).html('GIS Interactive Map');
            shadowToolbar();
            if (callback) callback();

            //$('header span', $('#' + ccma.UI.ActiveScreenId)).html('GIS Interactive Map');
        }
    });
}
function shadowToolbar() {
    $('.toolbaricon').css("box-shadow", "");
    if (trackMe == 'true' || trackMe == true)
        $('.map-agent').css("box-shadow", " 5px 5px 5px #3388A7;");
    else
        $('.map-parcel').css("box-shadow", " 5px 5px 5px #3388A7;");
}
function removeLegend() {
    if (legend != undefined && OsMap._controlCorners.bottomright.childNodes.length > 1) {
        legend.remove(OsMap);
    }
}
function showParcelLinks() {
    if (activeParcel == null) {
        messageBox('Select a parcel in order to continue.');
        return false;
    }

    var bounds = {}
    if (google)
        bounds = new google.maps.LatLngBounds();

    getData('SELECT * FROM Parcel WHERE Id IN (SELECT ChildParcelId FROM ParcelLinks WHERE ParcelId = ?)', [activeParcel.Id.toString()], function (results) {
        if (results.length == 0) {
            messageBox("The parcel does not have any valid and existing linked parcels to show on map.");
            return;
        }

        var hasPoints = false;
        for (x in results) {
            var p = results[x];
            if (p.Latitude && p.Longitude) {
                var point = new google.maps.LatLng(p.Latitude, p.Longitude);
                bounds.extend(point);
                hasPoints = true;
            }
        }

        if (hasPoints) {
            map.fitBounds(bounds);
            map.setCenter(bounds.getCenter());
            parcelMapCenter = bounds.getCenter();
            parcelMapBounds = bounds;
        } else {
            messageBox("None of the linked parcels have map coordinates. Map cannot be shown");
            return;
        }

        clearMap();
        parcelMapMode = false;
        parcelLinksMode = true;

        $('.map-pri-option').hide();
        $('.Setting').hide();
        loadScreen('gis-map', function () {
            setActiveScreenTitle('Parcel Links', activeParcel);
            //$('header span', $('#' + ccma.UI.ActiveScreenId)).html('Parcel Links - ' + activeParcel.StreetAddress);
            refreshParcelsInMap(function () {
                map.setCenter(parcelMapCenter);
                map.fitBounds(parcelMapBounds);
            }, true);
        });

    });

}

function showCountyPage() {
    loadScreen('county-page');
}

function showSync() {
    if (localStorage.forceUpdateNbhd == 'true') {
        loadScreen('neighborhood');
        $('.nbhd-cancel').attr('disabled', 'disabled');
    }
    else {
        loadScreen('synchronization');
        $('.nbhd-cancel').removeAttr('disabled');
        $('header span', $('#' + ccma.UI.ActiveScreenId)).html('Update MobileAssessor');
    }
}

function setActiveScreenTitle(title, p) {
    var key;
    p ? ShowAlternateField == "True" ? ShowKeyValue1 == "True" ? key = p.KeyValue1 + " | " + eval("p." + AlternateKey) : eval("p." + AlternateKey) ? key = eval("p." + AlternateKey) : isBPPUser ? key = p.KeyValue1 : key = eval("p." + AlternateKey) : key = p.KeyValue1 : key = '';
    var sTitle = title + (p ? ' - Parcel (' + key + ') ' + ((p.StreetAddress != null && p.StreetAddress != '') ? ' - ' + p.StreetAddress : '') : '');
    if (sTitle.length > 50) {
        if (appState.screenId == 'photos')
            sTitle = sTitle.substr(0, 50) + '...'
        else if ((appState.screenId == 'parcel-dashboard' && isBPPUser) || (appState.screenId == 'gis-map' && activeParcel))
            sTitle = sTitle.substr(0, 57) + '...'
        else if (sTitle.length > 70)
            sTitle = sTitle.substr(0, 70) + '...';
    }

    if (clientSettings.Trackedvalue) {
        $('.data-entry-tracked-content .newvalue').val('');
        var trackedvalueItem = clientSettings.Trackedvalue;
        var trackedmodeField = Object.keys(datafields).filter(function (x) { return datafields[x].Name == trackedvalueItem }).map(function (y) { return datafields[y] })[0]
        var trackname = trackedmodeField.Name;
        var trackfieldactive = activeParcel[trackname];
        var trackedModeVal = '';
        if (trackedmodeField.InputType == 5) {
            if (trackfieldactive != null && getLookupArray(trackedmodeField.Id, null, null, trackedmodeField.Id).filter(function (d) { return d.Id == trackfieldactive; })[0])
                trackedModeVal = getLookupArray(trackedmodeField.Id, null, null, trackedmodeField.Id).filter(function (d) { return d.Id == trackfieldactive; })[0].Name;
            trackedModeVal = trackedModeVal ? trackedModeVal : '';
        }
        else
            trackedModeVal = (trackfieldactive) ? ($('fieldset[field-name="' + trackname + '"] option[value= "' + trackfieldactive + '"]').html()) : '';

        sTitle += (trackedvalueItem) ? ('<div class="tracked_class" style="display:inline-block; margin-left: 0px"><span class ="track-mode" style="display: inline-block; margin: 0 5px; color:red">' + trackedmodeField.Label + '</span>' + '<span style="color:red">' + ':' + '</span>' + '<span class ="fieldtrack-mode" style="display: inline-block; margin: 0 5px; color:red">' + trackedModeVal + '</span></div>') : '';
    }

    sTitle += (fylEnabled && ccma.UI.ActiveScreenId == "data-collection" ? ('<fy class="' + (showFutureData ? 'fystatus flactive' : 'fystatus') + '">' + (showFutureData ? 'FY' : 'CY') + '</fy>') : '')
    $('header span', $('#' + ccma.UI.ActiveScreenId)).html(sTitle);
}

function setSearchResultsTitle(template, count) {
    setSearchResultsTitleText(template.replace('NNN', count));
}

function setSearchResultsTitleText(text) {
    appState.searchTitle = text;
    $('header .headerspan', $('#sort-screen')).html(text);
}

function loadStreetNamesForSearch() {
    getData('SELECT * FROM Streets ORDER BY Id', [], function (streets) {
        $('.select-street-names').html('<option>${Id}</option>')
        $('.select-street-names').fillTemplate(streets);
    });

}

function validate_SketchConfig() {
    var settings = new Array();
    for (y in clientSettings) {
        settings.push(y);
    }
    var result = settings.filter(function (x) { return x.toString().startsWith("Sketch"); });
    if (result.length == 0) {
        settings = new Array();
        for (y in sketchSettings) {
            settings.push(y);
        }
        result = settings.filter(function (x) { return x.toString().startsWith("Sketch"); });
        if (result.length < 5) {
            if (settings.filter(function (x) { return x.toString().startsWith("SketchConfig"); }).length == 1) {
                return true;
            }
            return false;
        }
    }
    else if (result.length < 5) {
        if (settings.filter(function (x) { return x.toString().startsWith("SketchConfig"); }).length == 1) {
            return true;
        }
        return false;
    }
    return true;
}

function restoreScreen(screenId) {
    switch (screenId) {
        case 'search': showSearch(); break;
        case 'county-page': showCountyPage(); break;
        case 'parcel-dashboard': showParcel(parseInt(appState.parcelId)); break;
        case 'synchronization':
        case 'neighborhood': showSync(); break;
        case 'user-dashboard': showDashboard(); break;
        //$('header span', $('#' + screenId)).html('Update Mobile Assessor');      
        case 'gis-map':
            if (appState.parcelId == -1) { showGISInteractive(function () { loadParcelsInMap(); }); break; }
        default:
            if (screenId || screenId != '') {
                if (appState.vuid || appState.vuid != '')
                    var vuid = parseInt(appState.vuid);
                if (appState.catId || appState.catId != '')
                    var catId = appState.catId;
                if (appState.rowuid || appState.rowuid != '')
                    var rowuid = appState.rowuid;
                if (appState.openfromMap || appState.openfromMap != '')
                    var openfromMap = appState.openfromMap;
                if (appState.ignoreCondition || appState.ignoreCondition != '')
                    var ignoreCondition = appState.ignoreCondition;
                var showFunction = showDcTab;
                if (catId && !rowuid) {
                    catId = getTopParentCategory(catId);
                    showFunction = showDataCollection;
                }
                showParcel(parseInt(appState.parcelId), function () {
                    switch (screenId) {
                        case 'digital-prc': showDigitalPRC(); break;
                        case 'data-collection': firstTime = true; showFunction(catId, rowuid); break;
                        case 'photos': showPhotos(); break;
                        case 'sketchview': showSketches(vuid); break;
                        case 'google-directions': showGoogleDirections(); break;
                        case 'gis-map': showGISInteractive(); break;
                        case 'copyParcel': showCopyImprv(catId, ignoreCondition); break;
                        default: return false;
                    }
                }, openfromMap, null, true);
            }
            break;
    }
}
//function addSearchOptions() {
//    var fieldsitems = clientSettings.MASearchFields
//    if (fieldsitems) {
//        var SeacrhItems = fieldsitems.trim().split(',');
//        $.each(SeacrhItems, function (a) {
//            if ($('.parcel-search-type option[value="' + "field-" + SeacrhItems[a] + '"]').length < 1) {
//                $('.parcel-search-type').append($("<option></option>")
//              .attr("value", "field-" + SeacrhItems[a])
//              .text('Search by Field (' + SeacrhItems[a] + ')'));
//            }
//        });

//    }
//}


function addSearchOptions() {
    let data = [{ Id: 'allpriority', Name: 'All priority parcels' }, { Id: 'urgentpriority', Name: 'Urgent Priority' }, { Id: 'highpriority', Name: 'High Priority' },
    { Id: 'proximity', Name: 'All nearby parcels' }, { Id: 'parcel', Name: 'Search by Parcel ID' }, { Id: 'street', Name: 'Search by Street' }
        , { Id: 'address', Name: 'Search by Address' }, { Id: 'alertfromoffice', Name: 'Search by Alert From Office' }, { Id: 'no-address', Name: 'Parcels without GIS/Address' }]

    if (EnableNewPriorities == 'True') {
        data = [{ Id: 'allpriority', Name: 'All priority parcels' }, { Id: 'parcelpriority', Name: 'Parcel Priority' },
        { Id: 'proximity', Name: 'All nearby parcels' }, { Id: 'parcel', Name: 'Search by Parcel ID' }, { Id: 'street', Name: 'Search by Street' }
            , { Id: 'address', Name: 'Search by Address' }, { Id: 'alertfromoffice', Name: 'Search by Alert From Office' }, { Id: 'no-address', Name: 'Parcels without GIS/Address' }]
    }
    var fieldsitems = clientSettings.MASearchFields
    if (fieldsitems) {
        var SeacrhItems = fieldsitems.trim().split(',');
        $.each(SeacrhItems, function (a) {
            data.push({ Id: "field-" + SeacrhItems[a], Name: 'Search by Field (' + SeacrhItems[a] + ')' });
        });
    }
    $('.parcel-search-type').empty();
    $('.parcel-search-type').lookup({ popupView: true, width: 288, searchActive: false, searchActiveAbove: 8, searchAutoFocus: false, onSave: () => { } }, []);
    $('.parcel-search-type')[0].getLookup.setData(data);
}

function showSketchOptions() {
    $('.ccse-option-list').show();
}

function trackedValue() {
    if (clientSettings.Trackedvalue) {
        var trackedvalueItem = clientSettings.Trackedvalue;
        var trackvalueactive = activeParcel[trackedvalueItem];
        if (trackvalueactive || trackvalueactive === 0) {
            trackFlag = true;
            return true;
        }
        var trackedField = Object.keys(datafields).filter(function (x) { return datafields[x].Name == trackedvalueItem }).map(function (y) { return datafields[y] })[0]
        $('.trackedvalue-content').remove();
        $('.trackedvalueFooterDiv').remove();

        if (!trackedField) {
            trackFlag = true;
            return true;
        }
        var trackedContentDiv = document.createElement('div');
        trackedContentDiv.className = 'trackedvalue-content';
        trackedContentDiv.setAttribute("style", "display:none;z-index: 100;position: absolute;float: left;top: 30%;left: 0;right: 0;margin-left: auto;margin-right: auto;background-color: white;border: 1px solid gray;max-width : 500px;height: 200px;")

        $('body').append(trackedContentDiv);

        var trackedvalueHeaderDiv = document.createElement('div');
        trackedvalueHeaderDiv.className = 'trackedvalue-header-content'
        trackedvalueHeaderDiv.setAttribute("style", "text-align: center;font-weight: bold;font-size: 18px;color: gray;background-color: #dfdddd;float: none;margin-bottom: 10px;");
        $(trackedvalueHeaderDiv).html('Tracked Value');
        $(trackedContentDiv).append(trackedvalueHeaderDiv);

        var trackedContent = document.createElement('div');
        trackedContent.className = 'data-entry-tracked-content';
        var fieldSet = getFieldSet(trackedField, null, true)
        $(trackedContent).append(fieldSet);
        $(trackedContentDiv).append(trackedContent);
        //$('.trackedvalue-content .oldvalue').remove();

        var trackedvalueFooterDiv = document.createElement('div');
        trackedvalueFooterDiv.className = 'trackedvalue-footer-content'
        trackedvalueFooterDiv.setAttribute("style", "float: right; width: 96%; padding: 10px;clear: 0;");
        $(trackedvalueFooterDiv).html('<span><button class="tracked-cancel" style="float: right;padding: 5px 30px; margin-top: 50px;margin-left:5px;" >Cancel</button></span><span><button class="tracked-save" style="float: right; padding: 5px 30px; margin-top: 50px;margin-left:5px;">Save</button></span><span class="requiredMsg" style="display: none;color: red;margin-left: 5px">Fill the required field</span>')
        $(trackedContentDiv).append(trackedvalueFooterDiv);

        $('.mask').show();
        trackFlag = false;
        $('.trackedvalue-content').show();
        $('.tracked-cancel').unbind(touchClickEvent);
        $('.tracked-cancel').bind(touchClickEvent, function (e) {
            $('.trackedvalue-content').hide();
            trackFlag = true;
            setTimeout(function () { $('.mask').hide(); }, 100);
        });
        $('.tracked-save').unbind(touchClickEvent);
        $('.tracked-save').bind(touchClickEvent, function (e) {
            var currTrackValue = $('.data-entry-tracked-content .newvalue').val();
            var trackFieldName = $('.data-entry-tracked-content .newvalue').attr('field-name');
            if (currTrackValue != '' && currTrackValue !== null) {
                if (currTrackValue != activeParcel[trackFieldName]) saveInputValue($('.data-entry-tracked-content .newvalue'), null, null, true);
                $('.trackedvalue-content').hide();
                trackFlag = true;
                setTimeout(function () { $('.mask').hide(); }, 100);
            }
            else {
                $('.requiredMsg').show();
            }
        });
    }
    else {
        trackFlag = true;
        return true;
    }
}

function prcPreviewSketch(firstTimePrc) {
    let skIndex = ($(".select-sketch-segments select")[0] && $(".select-sketch-segments select option").length > 0) ? $(".select-sketch-segments select")[0].selectedIndex : -1,
        sketches = [], skArray = [], isSketchError = false,
        blank_image = "data:image/png;base64,R0lGODlhFAAUAIAAAP///wAAACH5BAEAAAAALAAAAAAUABQAAAIRhI+py+0Po5y02ouz3rz7rxUAOw==", url = blank_image;

    sketchRenderer.resetOrigin();

    sketchRenderer.showOrigin = false; sketchRenderer.viewOnly = true; skIndex = firstTimePrc ? 0 : skIndex;

    try { sketches = activeParcel.getSketchSegments(true); } catch (ex) { isSketchError = true; console.log(ex); }

    try { sketchRenderer.open(sketches, null, true); } catch (ex) { isSketchError = true; console.log(ex); }


    if ((skIndex == 0 || skIndex > 0) && sketchRenderer.sketches[skIndex]) {
        $('.ccse-img-container').show();
        sketchRenderer.currentSketch = sketchRenderer.sketches[skIndex];
        let ab = sketchRenderer.sketchBounds();
        sketchRenderer.resizeCanvas((ab.xmax - ab.xmin) * DEFAULT_PPF + 95, (ab.ymax - ab.ymin) * DEFAULT_PPF + 95);
        sketchRenderer.pan(sketchRenderer.formatter.originPosition == "topRight" ? applyRoundToOrigin(-ab.xmax * DEFAULT_PPF + 15) : applyRoundToOrigin(-ab.xmin * DEFAULT_PPF + 15), sketchRenderer.formatter.originPosition == "topRight" || sketchRenderer.formatter.originPosition == "topLeft" ? applyRoundToOrigin(-ab.ymax * DEFAULT_PPF + 15) : applyRoundToOrigin(-ab.ymin * DEFAULT_PPF + 15));
        sketchRenderer.clearCanvas();
        clearVectorlabelPosition();
        for (var x in sketchRenderer.sketches[skIndex].vectors) {
            sketchRenderer.sketches[skIndex].vectors[x].render();
        }
        sketchRenderer.sketches[skIndex].renderNotes();
        if ($('.ccse-img-container .ccse-img').height() < 10000 && $('.ccse-img-container .ccse-img').width() < 10000)
            url = sketchRenderer.toDataURL();
        $('.ccse-img-container').hide();
    }

    if (firstTimePrc)
        activeParcel.SketchPreview = url == "data:," ? blank_image : url;
    else if ($('.select-sketch-segments').siblings('img')[0])
        $('.select-sketch-segments').siblings('img')[0].src = url == "data:," ? blank_image : url;

    sketchRenderer.sketches.forEach(function (sk) { skArray.push(sk.label); });

    if (firstTimePrc) return { skArray: skArray, flg: isSketchError };


}

//Ma error insert and return
function maErrorLogInsert(msg, url, line) {
    let maErrorLog = localStorage.MaErrorLog;
    maErrorLog = maErrorLog ? JSON.parse(maErrorLog) : { Errors: [] };
    if (maErrorLog.Errors.length == 50)
        maErrorLog.Errors.pop();
    maErrorLog.Errors.unshift(_.clone({ Message: msg, Url: (url + ':' + line), Date: new Date().toUTCString() }));
    localStorage.MaErrorLog = JSON.stringify(maErrorLog);
}

function getAppErrors(s, e) {
    let maErrorLog = localStorage.MaErrorLog;
    maErrorLog = maErrorLog ? JSON.parse(maErrorLog) : { Errors: [] };
    s = (!s || isNaN(s)) ? 0 : s;
    e = (!e || isNaN(e) || e < 0) ? 1 : e;
    return maErrorLog.Errors.slice(s, e);
}

//end


function LogoutErrorInsert(msg) {
    localStorage.logOutErrorLog = localStorage.logOutErrorLog ? localStorage.logOutErrorLog + '---;---' + msg : msg;
}

function convertHelionToSketchPro(sketchStr, sketchLbl, lblList) {
    let sktPro = {
        "$schema": "https://schemas.opencamadata.org/0.10/data.schema.json",
        "sketches": [
            {
                "id": 1,
                "label": sketchLbl,
                "segments": [],
                "notes": []
            }
        ],
        "lookupCollection": {
            "standard": {
                "unspecified": {

                }
            }
        }
    };

    let parts = (sketchStr || '').split(";"), k = 0, li = 1, lv = 0, nc = 1;

    for (var i in parts) {
        let part = parts[i]; k = k + 1;
        if (/(.*?):(.*?)/.test(part)) {
            let hi = part.split(':'), head = hi[0], info = hi[1], r1 = /(.*?)\[(.*?)\]/, hparts = r1.exec(head), labels = '', labelText = '', labelDesc = '', ad1 = '', ad2 = '';

            if (head.split('[')[0].trim() == 'Text') {
                let nd = null;
                try {
                    nd = JSON.parse(Base64.decode(hparts[2]));
                }
                catch (ex) { }

                if (nd && info) {
                    let sl = info.split(' '), x = 0, y = 0;
                    for (s in sl) {
                        if (sl[s]?.trim() != '') {
                            let cp = sl[s].trim().match(/[UDLR]|[0-9]+(.[0-9]+)?/g), dir = cp[0], dis = cp[1] || 0;
                            switch (dir) {
                                case "U": y = parseFloat(dis); break;
                                case "D": y = parseFloat(-dis); break;
                                case "L": x = parseFloat(-dis); break;
                                case "R": x = parseFloat(dis); break;
                            }
                        }
                    }

                    sktPro["sketches"][0]["notes"].push({ "keycode": nc.toString(), "text": nd.Text, "position": { "x": x, "y": y }, "page": 1 });
                    nc = nc + 1;
                }
                continue;
            }

            if (hparts != null) {
                labels = hparts[1].replace(/\{(.*?)\}/g, '');
            }

            if (!labels || labels == "") {
                labels = labelText = labelDesc = "MISSING";
            }
            else {
                let lk = lookup['SKETCH_CODES']?.[labels];
                if (lk) {
                    labelText = lk.Name;
                    labelDesc = lk.Description;
                    ad1 = lk.AdditionalValue1;
                    ad2 = lk.AdditionalValue2;
                }
                else {
                    labelText = labels;
                    labelDesc = labels;
                }
            }

            if (info) {
                let ssp = info.split("S"), sps = ssp[0].trim().split(" "), lps = ssp[1].trim().split(" "), sx = 0, sy = 0, ring = { "commands": [] }, segment = {
                    "id": k,
                    "label": {
                        "text": labelText,
                        "values": [
                            {
                                "text": labelText,
                                "lookupCode": labels,
                                "lookupName": "standard"
                            }
                        ],
                        "position": lblList[lv]
                    },
                    "vectors": [{
                        "label": {
                            "text": labelText,
                            "values": [
                                {
                                    "text": labelText,
                                    "lookupCode": labels,
                                    "lookupName": "standard"
                                }
                            ],
                            "position": lblList[lv]
                        },
                        "areaType": "positive",
                        "drawingType": "sketched",
                        "area": {},
                        "perimeter": {},
                        "id": k
                    }
                    ],
                    "boundarySize": 100,
                    "page": 1,
                    "area": {},
                    "perimeter": {},
                    "domain": "unspecified"
                };

                lv = lv + 1;

                for (s in sps) {
                    if (sps[s]?.trim() != '') {
                        let cp = sps[s].trim().match(/[UDLR#]|[0-9]+(.[0-9]+)?/g);
                        let dir = cp[0], distance = cp[1] || 0;
                        switch (dir) {
                            case "U": sy = parseFloat(distance); break;
                            case "D": sy = parseFloat(-distance); break;
                            case "L": sx = parseFloat(-distance); break;
                            case "R": sx = parseFloat(distance); break;
                        }
                    }
                }
                ring["origin"] = { "x": sx, "y": sy };

                for (var j in lps) {
                    let ns = lps[j];
                    if (ns.trim() != '') {
                        let cmds = ns.split('/'), xx = 0, yy = 0;
                        for (var c in cmds) {
                            let cmd = cmds[c];
                            if (cmd == '') continue;
                            let cp = cmd.match(/[ABUDLR#]|[0-9]+(.[0-9]+)?/g);
                            let dir = cp[0], distance = cp[1] || 0;
                            switch (dir) {
                                case "U": yy = parseFloat(distance); break;
                                case "D": yy = parseFloat(-distance); break;
                                case "L": xx = parseFloat(-distance); break;
                                case "R": xx = parseFloat(distance); break;
                            }
                        }
                        ring["commands"].push({ "line": { "x": xx, "y": yy } });
                    }
                }
                segment["vectors"][0]["ring"] = ring;
                sktPro["sketches"][0]["segments"].push(segment);

                if (!sktPro["lookupCollection"]["standard"]["unspecified"][labels]) {
                    sktPro["lookupCollection"]["standard"]["unspecified"][labels] = {
                        "name": labelText,
                        "description": labelDesc,
                        "ordinal": li,
                        "attributes": [
                            {
                                "key": "AdditionalValue1",
                                "value": ad1
                            },
                            {
                                "key": "AdditionalValue2",
                                "value": ad2
                            }
                        ]
                    }
                    li = li + 1;
                }
            }
        }
    }

    return JSON.stringify(sktPro);
}