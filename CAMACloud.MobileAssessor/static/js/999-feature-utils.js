﻿var utils = {};
var mcis_templatecpy = false;
var templateChildInsert = [];

utils.CopyTemplateData = function (targetCategoryId, filterString,options) {
    if (activeParcel == null || !checkPPAccess('copyTemplate')) {
        let msg = activeParcel == null ? "You have to open a parcel to perform this action." : "This option is not available for the selected parcel.";
        messageBox(msg); 
        return;
    }
    copiedCategories = [];
    templateChildInsert = [];
    emptyCopyCategories = [];
    ccma.Data.Controller.CopyTemplateData(activeParcel.Id, parseInt(ccma.UI.ActiveFormTabId), targetCategoryId, filterString,options);
}

utils.CopyTemplateDataMCIS = function (targetCategoryId, filterString,options) {
    if (activeParcel == null || !checkPPAccess('copyTemplate')) {
        let msg = activeParcel == null ? "You have to open a parcel to perform this action." : "This option is not available for the selected parcel.";
        messageBox(msg); 
        return;
    }
    copiedCategories = [];
    emptyCopyCategories = [];
    mcis_templatecpy = true; 
    ccma.Data.Controller.CopyTemplateData(activeParcel.Id, parseInt(ccma.UI.ActiveFormTabId), targetCategoryId, filterString,options.FilterValues,function(){
        ccma.Data.Controller.CopyTemplateData(activeParcel.Id, parseInt(ccma.UI.ActiveFormTabId), targetCategoryId, filterString, options.selectSpecifier, null, true);
    });
}

utils.MarkAsComplete = function () {
    if (activeParcel == null) {
        messageBox("You have to open a parcel to perform this action."); return false;
    }
    blockInputs();
    markParcelAsComplete(activeParcel.Id, function (RPParcel) {
	if(ParcelopenfromMap){ 
        	setTimeout(function(){
              openMapView();
          	  ParcelopenfromMap = false;
          	  allowInputs();
             }, 3000) 
    	}else{
        showSortScreen(function () { reloadParcelsInList(); allowInputs(); setTimeout(function(){ if ( isBPPUser && RPParcel=="true" && activeParentParcel){
                   refreshMiniSS = true;
				$('#sort-screen .page-back').trigger('click');
                       }}, 300)}, true);
        }
    }, function () {
        allowInputs();
    }, function () {
        allowInputs();
    });
}

utils.OpenCamera = function () {
    if (activeParcel == null) {
        messageBox("You have to open a parcel to perform this action."); return false;
    }
    var altKeyValue = ShowAlternateField == "True" ? eval("activeParcel."+AlternateKey) : null;
    photos.openParcel(activeParcel.Id, activeParcel.KeyValue1, { showPrimary: true }, null, altKeyValue);
}

utils.SearchAndOpenParcel = function (keyvalue) {
    var callerParcelId = activeParcel.Id;
    var lastScreen = ccma.UI.ActiveScreenId;
    var lastTab = ccma.UI.ActiveFormTabId;
    getData('SELECT Id, IsComparable FROM Parcel WHERE KeyValue1 = ?', [keyvalue], function (parcels) {
        if (parcels.length == 0) {
            messageBox("Sorry, the selected parcel has not been downloaded yet.");
            return;
        }

        var pid = parcels[0].Id.toString().trim();
        var isExtra = parseInt(parcels[0].IsComparable);
        var msgConfirm = 'Are you sure you want to open this parcel leaving the current one?';
        if (isExtra) {
            msgConfirm = 'You are attempting to open a parcel out of the current group. All data will not be available. Are you sure you want to open this parcel leaving the current one?'
        }
        messageBox(msgConfirm, ["OK", "Cancel"], function () {
        	var openedfmMap = appState.openfromMap? appState.openfromMap: undefined;	
            showParcel(parcels[0].Id.toString().trim(), function () {
                ccma.UI.ParcelLinks.LinkMap[parcels[0].Id.toString().trim()] = { caller: callerParcelId, openedFromLink: true, lastScreen: lastScreen, lastTab: lastTab };
            }, openedfmMap);
        });

    });
}

utils.ShowParcelLinksOnMap = function () {
    if (activeParcel == null) {
        messageBox("You have to open a parcel to perform this action."); return false;
    }
    showParcelLinks();
}

var saveParcelChange = function (callback) { if (callback) callback();}

