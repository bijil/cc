var alertLock
window.messageBox = function (message) {
    var title, okCallback, cancelCallback, options;
    var errormsg = message.replace("Rerun the transaction.", "Please try again.");
    var p2 = arguments[1]; var p3 = arguments[2]; var p4 = arguments[3]; var p5 = arguments[4];
    if (typeof p2 == "string") {
        console.log('P2 is string');
        title = p2;
        if (typeof p3 == "object") {
            options = p3;
            okCallback = p4;
            cancelCallback = p5;
        }
        else {
            okCallback = p3;
            cancelCallback = p4;
        }
    }

    if (typeof p2 == "object") {
        options = p2;
        okCallback = p3;
        cancelCallback = p4;
    }

    if (typeof p2 == "function") {
        okCallback = p2;
        cancelCallback = p3;
    }
    
    var elem = $('section.current .scrollable#dc-form-contents').length == 0? $('section.current .scrollable'): $('section.current .scrollable#dc-form-contents');
	$(elem).addClass('tempScrollable');
	$(elem).removeClass('scrollable');
    

    if (!options) { options = ["OK"]; }
    if (options.length == 0) { options = ["OK"]; }
    disableIcons = false;
    alertLock = true;
    $('.window-message-box .message-box-title').html(title || 'CAMA Cloud<sup>&#174;</sup>');
    $('.window-message-box .message-box-input').hide();
    $('.window-message-box .message-box-body').html(errormsg);
    if (options.length >= 1) {
        $('.window-message-box .message-box-ok').html(options[0]);
    }
    if (options.length > 1) {
        $('.window-message-box .message-box-cancel').html(options[1]);
        $('.window-message-box .message-box-cancel').show();
    } else {
        $('.window-message-box .message-box-cancel').hide();
    }
    //alert(touchClickEvent);
    var touchClickEvent = 'touchend click';
    $('.window-message-box').show();
    $('.mask').show();
    $('.window-message-box .message-box-ok').unbind(touchClickEvent);
    $('.window-message-box .message-box-ok').bind(touchClickEvent, function (e) {
        e.preventDefault();
        setTimeout(function(){  alertLock = false},1000)
        $('.tempScrollable').addClass('scrollable');
		$('.tempScrollable').removeClass('tempScrollable');
        $('.window-message-box').hide();
        $('.mask').hide();
        $("input").blur();
        if (okCallback)
            okCallback();
    });
    $('.window-message-box .message-box-cancel').unbind(touchClickEvent);
    $('.window-message-box .message-box-cancel').bind(touchClickEvent, function (e) {
        e.preventDefault();
        setTimeout(function () { alertLock = false }, 1000)
        $('.tempScrollable').addClass('scrollable');
		$('.tempScrollable').removeClass('tempScrollable');
        $('.window-message-box').hide();
        $('.mask').hide();
        $("input").blur();
        if (cancelCallback)
            cancelCallback();
    });
}

window.input = function (message) {
    var singleInput = true;
    var title, okCallback, cancelCallback, values, options, TextboxText, boxType = null, enableKeyDown = null, noteSpecialKeys = null;
    var p2 = arguments[1]; var p3 = arguments[2]; var p4 = arguments[3]; var p5 = arguments[4]; var p6 = arguments[5]; var p7 = arguments[6]; var p8 = arguments[7]; var p9 = arguments[8];
    if (typeof p2 == "string") {
        title = p2;
        if (typeof p3 == "object") {
            values = p3;
            okCallback = p4;
            cancelCallback = p5;
        }
        else {
            okCallback = p3;
            cancelCallback = p4;
            TextboxText = p5;
        }
    }

    if (typeof p2 == "object") {
        values = p2;
        okCallback = p3;
        cancelCallback = p4;
    }
    if (typeof p2 == "function") {
        okCallback = p2;
        cancelCallback = p3;
    }
    let isBxType = false;
	if( p6 && p6.isSqftBox ) {
        boxType = p6.type;
        p8 = p6.maxLength;
        enableKeyDown = true;
        p6 = null;
        isBxType = true;
    }
	
    options = ["OK", "Cancel"];
    if (cancelCallback == null)
        options = ["OK"];
    if (values) { singleInput = false; }

    var inputElement
    if (!p6)
        inputElement = singleInput || p5 == 'customlookups' ? $('.window-message-box .message-box-input-text') : $('.window-message-box .message-box-input-sel');
    else {
        inputElement = $('.window-message-box .message-box-input-textarea');
    }
    $('.window-message-box .message-box-input-item').hide();
    $(inputElement).show();
    $(inputElement).val('');
    if (TextboxText)
        $(inputElement).val(TextboxText);
    $('.window-message-box .message-box-input').show();

    var selectedValue = null;
    if (values && values.lookupValues) {
        selectedValue = values.selectedValue;
        values = values.lookupValues;
    }

    if (values && p5 != 'customlookups') {
        $(inputElement).html('');
        var numElements = 0;
        var firstElementId = "";
        var firstE;
        for (var x in values) {
            if (typeof values[x] == "object") {
                var opt = document.createElement('option');
                opt.innerHTML = values[x].Name;
                opt.setAttribute('value', values[x].Id);
                inputElement[0].appendChild(opt);
                if (numElements == 0) { firstElementId = values[x].Id; firstE = opt; };
            } else if (typeof values[x] == "string") {
                var opt = document.createElement('option');
                opt.innerHTML = values[x];
                opt.setAttribute('value', values[x]);
                inputElement[0].appendChild(opt);
                if (numElements == 0) { firstElementId = values[x]; firstE = opt; };
            }
            numElements++;
        }
        if (numElements > 0 && cancelCallback == null) {
            if (firstElementId != "") {
                var opt = document.createElement('option');
                opt.innerHTML = "";
                opt.setAttribute('value', "");
                inputElement[0].insertBefore(opt, firstE);
            }
        }

        if (selectedValue) {
            $(inputElement).val(selectedValue);
        }
    }
    else if (p5 == 'customlookups') {
        inputElement.lookup({ popupView: true, searchActive: true, searchActiveAbove: 8, searchAutoFocus: false, onSave: () => { } }, []);
        inputElement[0].getLookup.setData(values);
    }
    if (p7 || isBxType) {
        let mCont = isBxType ? 'Please enter a SQFT to continue.' : 'Please enter a text to continue.'
        $('.window-message-box .message-box-input').append('<span class="validateMessageText" style="display: none;color:Red;">' + mCont +'</span>')
        $('.window-message-box .message-box-input-textarea').css('max-width', '290px');
        $('.window-message-box .message-box-input-textarea').css('padding-left', '2px');
        $('.window-message-box .message-box-input-textarea').css('overflow', 'auto');
        $('.window-message-box .message-box-input').bind('keyup', function(){
            $('.validateMessageText').hide();
        });
    }
    if(p8)
        $('.window-message-box .message-box-input-textarea').attr("maxlength", p8)
     
	if (p9 && p9.specialKeys) {
        noteSpecialKeys = p9.specialKeys;
        $('.message-box-input .message-box-input-textarea').bind('keydown', function(e) {
             var e = e || window.event;
             var k = e.which || e.keyCode;
             if (noteSpecialKeys) {
             	var ntspecial = noteSpecialKeys.filter(function(x) { return x == k })[0];
                if (ntspecial && e.key != '3' && e.key != '.' && e.key != '4'&& (ntspecial != 220 || (ntspecial == 220 && e.key == '|')))
                    return false;
             }
        });
        $('.message-box-input .message-box-input-textarea').bind('keyup', function(e) {
             if(this && this.value)
             	this.value = this.value.replaceAll('|', '');
        });  
    }     
        
    if(boxType) {
        $('.window-message-box .message-box-input-text').prop('type', boxType);
        if(enableKeyDown) {
            $('.window-message-box .message-box-input-text').bind('keydown', function() {
            	 if( $(inputElement).val().length >= 10 ) {
                    var sqfttt = $(inputElement).val().slice(0,-1);
	    			$(inputElement).val(sqfttt);
			     }
            });
        }
    }
        
    var alertLock = true;
    $('.window-message-box .message-box-title').html(title || 'CAMA Cloud<sup>&#174;</sup>');

    $('.window-message-box .message-box-body').html(message);
    if (options.length >= 1) {
        $('.window-message-box .message-box-ok').html(options[0]);
    }
    if (options.length > 1) {
        $('.window-message-box .message-box-cancel').html(options[1]);
        $('.window-message-box .message-box-cancel').show();
    } else {
        $('.window-message-box .message-box-cancel').hide();
    }
    //alert(touchClickEvent);
    var touchClickEvent = 'touchend click';
    $('.window-message-box').show();
    $('.window-message-box .message-box-ok').unbind(touchClickEvent);
    $('.window-message-box .message-box-ok').bind(touchClickEvent, function (e) {
        e.preventDefault();
        if(p8)
            $('.window-message-box .message-box-input-textarea').removeAttr("maxlength")
        if (p9 && p9.specialKeys)
        	$('.message-box-input .message-box-input-textarea').unbind('keydown');
        
        if ((p7 && $(inputElement).val().trim() == '') || (isBxType && $(inputElement).val().trim() == '' && TextboxText != '')){
             $('.validateMessageText').show();
             return false;
        }
        else{
            if (p7 || isBxType){
        		$('.window-message-box .message-box-input').unbind('keyup');
        		$('.window-message-box .message-box-input-textarea').css('max-width', '');
        		$('.validateMessageText').remove();
        	}
            alertLock = false;
            $('.window-message-box').hide();
            $("input").blur();
            if (okCallback)
                okCallback($(inputElement).val());
        }

        if (enableKeyDown) {
            $('.window-message-box .message-box-input-text').unbind('keydown');
            $('.window-message-box .message-box-input-text').prop('type', 'Text');
        }

        // Check if the element with class 'cc-drop' exists
        if ($('input.message-box-input-item').hasClass('cc-drop')) {
            // If it exists, remove the 'cc-drop' class
            $('input.message-box-input-item').removeClass('cc-drop');
        }
    });
    $('.window-message-box .message-box-cancel').unbind(touchClickEvent);
    $('.window-message-box .message-box-cancel').bind(touchClickEvent, function (e) {
        e.preventDefault();
        alertLock = false;
        if (p7 || isBxType){
        	$('.window-message-box .message-box-input').unbind('keyup');
        	$('.window-message-box .message-box-input-textarea').css('max-width', '');
        	$('.validateMessageText').remove();
        }
        if(p8)
        	$('.window-message-box .message-box-input-textarea').removeAttr("maxlength")
        if (p9 && p9.specialKeys)
        	$('.message-box-input .message-box-input-textarea').unbind('keydown');
        if(enableKeyDown) { 
            $('.window-message-box .message-box-input-text').unbind('keydown');
            $('.window-message-box .message-box-input-text').prop('type', 'Text');
        }
        $('.window-message-box').hide();
        $("input").blur();
        if (cancelCallback)
            cancelCallback();

        // Check if the element with class 'cc-drop' exists
        if ($('input.message-box-input-item').hasClass('cc-drop')) {
            // If it exists, remove the 'cc-drop' class
            $('input.message-box-input-item').removeClass('cc-drop');
        }
    });
}

window.onerror = function (msg, url, line, ex) {
    if (window.location.hash == '#repairing') {
        return false;
    }

    ccma.State.LastError = {
        message: msg,
        url: url,
        line: line
    }

    console.error(msg);
    maErrorLogInsert (msg, url, line);
    if (msg.indexOf("TypeError") > -1) { return false; }
    if (msg == "TypeError: 'undefined' is not a constructor") {
        if (!serviceUnavailabilityWarned) {
            messageBox('Some of the online services may be blocked as you are working in offline mode. ');
            serviceUnavailabilityWarned = true;
        }
        return false;
    }
    url = url.substr(url.lastIndexOf("/") + 1);
    if (url.indexOf('?') > -1)
        url = url.substr(url, url.indexOf('?'));
    alert(msg + "\n\nSource: " + url + " @ line " + line);
    if (hideSplash) hideSplash();
}