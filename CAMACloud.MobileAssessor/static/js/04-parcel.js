﻿//    DATA CLOUD SOLUTIONS, LLC ("COMPANY") CONFIDENTIAL 
//    Unpublished Copyright (c) 2010-2013 
//    DATA CLOUD SOLUTIONS, an Ohio Limited Liability Company, All Rights Reserved.

//    NOTICE: All information contained herein is, and remains the property of COMPANY.
//    The intellectual and technical concepts contained herein are proprietary to COMPANY
//    and may be covered by U.S. and Foreign Patents, patents in process, and are protected
//    by trade secret or copyright law. Dissemination of this information or reproduction
//    of this material is strictly forbidden unless prior written permission is obtained
//    from COMPANY. Access to the source code contained herein is hereby forbidden to
//    anyone except current COMPANY employees, managers or contractors who have executed
//    Confidentiality and Non-disclosure agreements explicitly covering such access.</p>

//    The copyright notice above does not evidence any actual or intended publication
//    or disclosure of this source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
//    DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
//    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
//    IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
//    OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
//    TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
//    ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.


// Parcel
var activeParcel = null;
var activeParentParcel = null;
var activeParcelData = null;
var lookup = {};
var fieldAlerts = [];
var parentChild = [];
TableHierarchical = [];
var directions, mapDirec;
var AerialMap;// = true;
var CustomLayer; //= false;
var ShowBoundaries; //= false;
var ShowAll;// = false;
var StreetLayer;// = false;
var HeatLayer;//= false;
var StreetView;
var polygons = [];
var boundaryLine={};
var Heatmapfields = {};
var agFields = {};
var Heatmaplookup = {};
var clientSettings = {};
var sketchSettings = {};
var StreetMapPoints = {};
var neighborhoods = [];
var datafields = {};
var copyVector = null;
var listparcels = [];
var fieldCategories = [];
var tableListGlobal = [];
var markerGroup = null;
var gLocationmarker = [];
function getDataField(key, table) {
	if(key=="Id" && (table === undefined || table == null))
		return null;
    var res = Object.keys(datafields).filter(function (x) { return table ? ((datafields[x].Name == key) && (datafields[x].SourceTable == table)) : ((datafields[x].Name == key) && (datafields[x].CategoryId || (datafields[x].QuickReview == "true")) && datafields[x].SourceTable == null) }).map(function (x) { return datafields[x] });
    if (!table && res.length == 0) {
        res = Object.keys(datafields).filter(function (x) { return (datafields[x].Name == key) }).map(function (x) { return datafields[x] });
    }
    if (res.length > 0)
        return res[0];
    else
        return null;
}

function Parcel(parcelId, x, full) {
    if (full == null || full === undefined) {
        full = true;
    }
    var points = [];
    var shapes = [];
    var bounds = {}
    if (google && !UsingOSM)
        bounds = new google.maps.LatLngBounds();
    else
        bounds = new L.LatLngBounds();
    this.Original = {};
    this.Data = {};
    this.Data.Original = {};
    this.Changes = {};
    this.Photos = [];
    this.Sketches = [];
    this.FirstPhoto = "";
    this.FirstSketch = "";
    this.SketchPreview = "";
    this.SALES = [];
    this.OUTBUILDINGS = [];
    this.Comparables = [];


    this.SPACE = ' ';
    this.CopyFrom = function (obj) {
        for (key in obj) {
            key = key.split(':')[0];
            var js = 'this["' + key + '"] = obj["' + key + '"];';    //'this.' + key + ' = obj.' + key + ";"
            var js2 = 'this.Original["' + key + '"] = obj["' + key + '"];'; //'this.Original.' + key + ' = obj.' + key + ";"
            //            var js5 = 'this.Data.' + key + ' = {};'
            //            var js3 = 'this.Data.' + key + '.Value = obj.' + key + ";"
            //            var js4 = 'this.Data.' + key + '.Original = obj.' + key + ";"
            try {
                eval(js);
                eval(js2);
                //                eval(js5);
                //                eval(js3);
                //                eval(js4);
            } catch (e) {
                console.error(e);
                console.error(js);
            }
        }
        if (this.CCMAMarkupString)
            this.CCMAMarkupString = this.CCMAMarkupString.replace(/\\n/g, '\n');
        eval('this.Id = "' + parcelId + '";');
        eval('this.__ID = "' + parcelId + '";');
    };

    this.that = this;
    this.Points = function () { return points; }
    this.Shapes = function () { return shapes; }
    this.Bounds = function () { return bounds; }
    this.Center = function () { return bounds.getCenter(); }


    this.ShowOnMap = function (locate, whichmap,callback) {
 if (!UsingOSM) {
        if (!google) {
            messageBox('Google Maps failed to load. You cannot locate the parcel on map now.');
            return false;
        }
        }

        if (!whichmap) { whichmap = UsingOSM ? OsMap : map; }
        if (locate == null)
            locate = true;
        if (mapInitialized)
            if (whichmap) {
                if (!UsingOSM) {
                		 var clearLocationMarker = function() {
                		 	for (let i = 0; i < gLocationmarker.length; i++) {
					    gLocationmarker[i].setMap(null);
					  }
					  gLocationmarker = [];
				}
				
				class Popup extends google.maps.OverlayView {
					    constructor(position, content) {
					      super();
					      this.position = position;
					      content.classList.add("popup-bubble"); // This zero-height div is positioned at the bottom of the bubble.
					
					      const bubbleAnchor = document.createElement("div");
					      bubbleAnchor.classList.add("popup-bubble-anchor");
					      bubbleAnchor.appendChild(content); // This zero-height div is positioned at the bottom of the tip.
					
					      this.containerDiv = document.createElement("div");
					      this.containerDiv.classList.add("popup-container");
					      this.containerDiv.appendChild(bubbleAnchor); // Optionally stop clicks, etc., from bubbling up to the map.
					
					      Popup.preventMapHitsAndGesturesFrom(this.containerDiv);
					    }
					    /** Called when the popup is added to the map. */
					
					    onAdd() {
					      this.getPanes().floatPane.appendChild(this.containerDiv);
					    }
					    /** Called when the popup is removed from the map. */
					
					    onRemove() {
					      if (this.containerDiv.parentElement) {
					        this.containerDiv.parentElement.removeChild(this.containerDiv);
					      }
					    }
					    /** Called each frame when the popup needs to draw itself. */
					
					    draw() {
					      const divPosition = this.getProjection().fromLatLngToDivPixel(
					        this.position
					      ); // Hide the popup when it is far out of view.
					
					      const display =
					        Math.abs(divPosition.x) < 4000 && Math.abs(divPosition.y) < 4000
					          ? "block"
					          : "none";
					
					      if (display === "block") {
					        this.containerDiv.style.left = divPosition.x + "px";
					        this.containerDiv.style.top = divPosition.y + "px";
					      }
					
					      if (this.containerDiv.style.display !== display) {
					        this.containerDiv.style.display = display;
					      }
					    }
					  }
				
				var addMarker = function(x, index) {
					if(x.Label){
					var label = document.createElement('span')
					label.innerHTML = x.Label
					var marker = new Popup(
					    new google.maps.LatLng(x.Latitude, x.Longitude),
					   label
					  );
			  		}else{
					  var marker = new google.maps.Marker({
					    position: new google.maps.LatLng( x.Latitude, x.Longitude ),
					    map: map,
					    icon: ParcelLocationIcon
					  });
				  }
				  gLocationmarker.push(marker);
				  gLocationmarker[index].setMap(map);
				}
                		clearLocationMarker();
                		if(this.ParcelGISPoints && this.ParcelGISPoints.length > 0){
					activeParcel.ParcelGISPoints.forEach(function(x, index){
				    		addMarker(x, index);
				    	});
				  var LocCenter = new google.maps.LatLng( x.Latitude, x.Longitude )
				  activeParcel.mapCenter = [LocCenter.lat(), LocCenter.lng()];
				    map.setCenter(LocCenter);
                	}
                  var paths=[];
		          this.Shapes().forEach(function (w) {
		          		if (w != null) {
							paths.push(w);
		                }
		           })
		          polygons.push(new google.maps.Polygon({
		                paths: paths,
		                strokeColor: "#FF0000",
		                strokeOpacity: 0.8,
		                strokeWeight: 2,
		                fillColor: "#FF0000",
		                fillOpacity: 0.1,
		                visible: true
		                }));
		       	  
				  polygons.forEach(function(w) {
            				w.setMap(map);
                  });
							                       
		                    
                  resizeMap();
                    // parcelPolygon.setMap(whichmap);
                  if (locate == true)
                    this.LocateOnMap(whichmap);

                } else {
                		if (markerGroup) {
			              markerGroup.clearLayers();
			        };
			      markerGroup = L.layerGroup().addTo(OsMap);
                		if(this.ParcelGISPoints && this.ParcelGISPoints.length > 0){
                		       this.ParcelGISPoints.forEach(function(x){
                		       	if(x.Label)
                		       		 L.popup({closeButton: false,closeOnClick: false }).setLatLng([x.Latitude , x.Longitude]).setContent('<span>'+x.Label+'</span>').openOn(OsMap);
                		       	else
                		             	L.marker([x.Latitude , x.Longitude], {icon: ParcelLocation }).addTo(markerGroup);
                			});
                		}
	                 		var layers=[];
							 this.Shapes().forEach(function (s) {
	                        if (s != null) {
	                            var lyr = L.polygon(s, { color: 'red' });
	                            layers.push(lyr);
	                            polygonlayer.addLayer(lyr);
	                            OsMap.addLayer(polygonlayer);
	                        }
	                    });
	                    boundaryLine[this.Id]=layers;
                    resizeMap();
                }
            }

        if (locate == true)
            this.LocateOnMap(whichmap);
            if(callback) callback();
    }

    this.LocateOnMap = function (whichmap) {
        if (!UsingOSM) {
            if (!google) {
                messageBox('Google Maps failed to load. You cannot locate the parcel on map now.');
                return false;
            }
            if (!whichmap) whichmap = map;
        } else { if (!whichmap) whichmap = OsMap; }
        parcelMapCenter = this.Center();
        parcelMapBounds = this.Bounds();
        if (!UsingOSM)
            whichmap.setCenter(this.Center());
        else {
        	if(whichmap.getZoom())
          		whichmap.setView(this.Center());
        	else
          		whichmap.setView(this.Center(),18);
        }
        whichmap.fitBounds(this.Bounds());
        trackMe = false;
    }

    this.AddPoint = function (lat, lng, recNo) {
        if (google && !UsingOSM) {
            latlng = new google.maps.LatLng(lat, lng);
            points.push(latlng);
            if (shapes[recNo] == null) {
                shapes[recNo] = [];
            }
            shapes[recNo].push(latlng);
            bounds.extend(latlng);
        }
        else {
            var a = new L.latLng(parseFloat(lat), parseFloat(lng));
            var point = [parseFloat(lng), parseFloat(lat)]
            points.push(a);
            if (shapes[recNo] == null) {
                shapes[recNo] = [];
            }
            shapes[recNo].push(a);
            //            if (OSshapes[recNo] == null) {
            //                OSshapes[recNo] = [];
            //            }
            //            OSshapes[recNo].push(point);
            bounds.extend(a);
        }
    };

    this.GetDirections = function (callback) {
        if (!google) {
            messageBox('Google Directions API failed to load. You cannot get directions now.');
            return false;
        }
        if ((activeParcel.Latitude == null && activeParcel.Longitude == null) || activeParcel.Points().length == 0) {
            $('#directions').html("Sorry, No map coordinates found for this parcel<br />");
            return;
        }
        geo.getCurrentPosition(function () {
            var destination = UsingOSM == true ? activeParcel.Center().wrap() : activeParcel.Center().toUrlValue();
            var request = {
                origin: myLocation.toUrlValue(),
                destination: destination,
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            };

            directions.route(request, function (response, status) {
                if (status == "OK") {
                    if (callback)
                        callback(response);
                }
                else {
                    $('#directions').html("Sorry, Unable to retrieve your route<br />");
                }
            });
        });

    }

    this.UpdateField = function (fieldName, newValue) {
        var pid = parseInt(this.__ID);
        db.transaction(function (tx) {
            tx.executeSql('UPDATE Parcel SET [' + fieldName + '] = ? WHERE Id = ?', [newValue, pid.toString()], function (dx, res) {
                activeParcel[fieldName] = newValue;
            });
        });
    }

    this.CompData = function (compid, expression, datatype, useLookup) {
        var c = this.Comparables[parseInt(compid)];
        if (c == null)
            return "";
        var value = c.EvalText(null, expression, datatype, useLookup);
        if (value == null)
            value = "";
        if (value == "null")
            value = "";
        return value;
    };

    this.getSketchSegments = function (prcView) {
       
        var config = ccma.Sketching.Config;
        var processor = ccma.Sketching.SketchFormatter;
        var isPages = false;
        var sketches = [];
        var _sktLookupArray = ( sketchSettings["sketchLookupQuery"] && sketchSettings["sketchLookupQuery"] != '' )? sketchSettings["sketchLookupQuery"].split('$'): [];
        var sk_count = -1;
        for (var ski in config.sources) {
        	var sk_count = sk_count + 1;
            var sksource = config.sources[ski];
            var keyPrefix = sksource.Key ? (sksource.Key + '--') : '';
            var sketchTable = sksource.SketchSource.Table ? ( activeParcel[sksource.SketchSource.Table] ? ( activeParcel[sksource.SketchSource.Table].filter(function(st) { return eval( sksource.SketchSource.sketchSourceFilter || true ); }) ) : [] ) : [activeParcel];                 //sksource.SketchSource.Table ? eval('activeParcel.' + sksource.SketchSource.Table) : [activeParcel];
            var sketchTableOriginal = sksource.SketchSource.Table ? ( activeParcel.Original[sksource.SketchSource.Table] ? ( activeParcel.Original[sksource.SketchSource.Table].filter(function(st) { return eval( sksource.SketchSource.sketchSourceFilter || true ); }) ) : [] ) : [activeParcel.Original];        //sksource.SketchSource.Table ? eval( 'activeParcel.Original.' + sksource.SketchSource.Table ) : [activeParcel.Original];          
            var notesTable = sksource.NotesSource && sksource.NotesSource.Table ? eval('activeParcel.' + sksource.NotesSource.Table) : [];
            var sketchKeyField = sksource.SketchSource.KeyField || 'ROWUID';
            
			if (sksource.SketchSource.sketchSourceSorting) {
               sketchTable = sketchSorting(sketchTable, sksource.SketchSource.sketchSourceSorting, (sksource.SketchSource.Table? sksource.SketchSource.Table: null));
               var orgSortOrder = sketchTable.map(function(st) { return st.ROWUID });
               var orgmap = orgSortOrder.reduce((r, v, i) => ((r[v] = i), r), {});
               sketchTableOriginal = sketchTableOriginal.sort((a, b) => orgmap[a['ROWUID']] - orgmap[b['ROWUID']]);
           	}  
            
            for (var i in sketchTable) {
                var pi = sketchTable[i];
                var orgTable = sketchTableOriginal[i];
                var sketchKey = keyPrefix + pi[sketchKeyField];
                var labelField = sksource.SketchSource.LabelField ? sksource.SketchSource.LabelField.split('/'): [];
                if (sketchSettings && sketchSettings.SketchDDLOptions)
            		labelField = labelField.concat(sketchSettings.SketchDDLOptions.split(','));
                var labelFieldValue = '', labelFieldDelimter = sksource.SketchSource.LabelFieldDelimter? sksource.SketchSource.LabelFieldDelimter: '/';
                
                for ( var k = 0; k < labelField.length; k++ ) {
                	var splitField = labelField[k].split(' %# ');
                	for ( var sf = 0; sf < splitField.length; sf++ ) {
                		var bracketlblField = splitField[sf].trim(), desc = '', bracketExists = false, lblField = splitField[sf].trim();
                		if (bracketlblField.indexOf('{') > -1) {
                			lblField = bracketlblField.match(/\{.*?\}/g)[0].match(/\{.*?\}/g)[0].replace( '{', '' ).replace( '}', '' );
                			bracketExists = true;
                		}
                		var value = '';

                        if (lblField == 'KeyValue1' || lblField == 'AlternateKey') {
                            if (ShowAlternateField == "True" && AlternateKey != '') desc = eval('activeParcel.' + AlternateKey);
                            if (!desc || desc == '') desc = eval('activeParcel.KeyValue1');
                        }
                        else {
                            if (lblField.indexOf('parentRecord.') > -1 && !pi.parentRecord) {
                                if (!parent_missing_msg) {
                                    parent_missing_msg = true;
                                    messageBox("The sketch is missing its related improvement record. Please contact support with this parcel number and do NOT save the sketch.");
                                }
                            }
                            else
                                value = (eval('pi.' + lblField) || '');
                            var tbl = sksource.SketchSource.Table ? sksource.SketchSource.Table : null;
                            if (lblField.indexOf('parentRecord.') > -1) {
                                lblField = lblField.replace('parentRecord.', '');
                                tbl = parentChild ? (parentChild.filter(function (pc) { return pc.ChildTable == tbl })[0] ? parentChild.filter(function (pc) { return pc.ChildTable == tbl })[0].ParentTable : tbl) : tbl;
                            }
                            var field = getDataField(lblField, tbl);
                            if (field && field.InputType == '5') {
                                desc = evalLookup(lblField, value, null, field);
                                desc = ((desc == '???') || (desc == '') || (desc == null)) ? value : desc;
                            }
                            else
                                desc = value;
                            if (config.isPVDConfig)
                                desc = ((lblField == 'ResidenceType' || lblField == 'CommercialType2') ? (desc == 'None' ? '' : desc) : (lblField == 'YearConstrcuted' ? (desc == '0' || desc == 0 ? '' : desc) : desc));
                        }

                        if (bracketExists) {
	                		bracketlblField = bracketlblField.replace('{' + lblField + '}', desc);
	                		desc = bracketlblField;
	                	}
	                	labelFieldValue = labelFieldValue + desc;
	                	if( sf != (splitField.length -1) && desc && desc != '' )
	                		labelFieldValue = labelFieldValue + labelFieldDelimter;
                	}
                	if (k != (labelField.length -1) && labelFieldValue[labelFieldValue.length - 1] != labelFieldDelimter)
                		labelFieldValue = labelFieldValue + labelFieldDelimter;
                }
                
                var sketch = {
                    uid: sketchKey.toString(),
                    sid: pi[sketchKeyField],
                    label: (sksource.SketchLabelPrefix ? (sksource.SketchLabelPrefix + ' ') : '') + (labelFieldValue) + (eval('pi.' + sksource.SketchSource.KeyFields[0]) ? (' [' + eval('pi.' + sksource.SketchSource.KeyFields[0]) + ']') : ''),
                    sketches: [],
                    notes: [],
                    config: sksource,
                    parentRow: sksource.SketchSource.Table ? pi : null
                }
                
                if(sksource.GroupingField && sksource.GroupingField == 'ParcelId')
                	sketch.GroupingValue = activeParcel.Id;
                else if(sksource.GroupingField && sksource.DGroupingField)
                	sketch.GroupingValue = pi[sksource.GroupingField] || pi['ROWUID'];
                else if(sksource.GroupingField && sksource.OGroupingField)
                	sketch.GroupingValue = pi[sksource.GroupingField] || pi['ParentROWUID'];
                else if (sksource.GroupingField && sksource.SketchSource.Table )
                    sketch.GroupingValue = pi[sksource.GroupingField] || pi['ParentROWUID'];

                let sectParentTable = null;
                if (config.ShowSectNum) {
                	var isBristol = (config.isBristolTable ? true : false);
        			var sectTables = {}, isShowSectNum = false;
        			if (isBristol)
            			sectTables = { Residential: 'CCV_CONSTR_RESDEP', Commercial: 'CCV_CONSTR_COMDEP', CondoMain: 'CCV_CONSTR_CDMDEP', CondoUnit: 'CCV_CONSTR_CDUDEP' }
        			else
            			sectTables = { Residential: 'CCV_CONSTRSECTION_RES', Commercial: 'CCV_CONSTRSECTION_COM', CondoMain: 'CCV_CONSTRSECTION_CDM', CondoUnit: 'CCV_CONSTRSECTION_CDU' }
            			
                    if (pi[sectTables.Residential] && pi[sectTables.Residential].filter(function (res) { return res.CC_Deleted != true }).length > 0) {
            			if(pi[sectTables.Residential].filter(function (res) { return res.CC_Deleted != true }).length > 1)
                            isShowSectNum = true;
                        sectParentTable = sectTables.Residential;
            		}
                    else if (pi[sectTables.Commercial] && pi[sectTables.Commercial].filter(function (comm) { return comm.CC_Deleted != true }).length > 0) {
            			if (pi[sectTables.Commercial].filter(function (comm) { return comm.CC_Deleted != true }).length > 1)
                            isShowSectNum = true;
                        sectParentTable = sectTables.Commercial;
            		}
                    else if (pi[sectTables.CondoMain] && pi[sectTables.CondoMain].filter(function (comn) { return comn.CC_Deleted != true }).length > 0) {
            			if (pi[sectTables.CondoMain].filter(function (comn) { return comn.CC_Deleted != true }).length > 1)
                            isShowSectNum = true;
                        sectParentTable = sectTables.CondoMain;
            		}
                    else if (pi[sectTables.CondoUnit] && pi[sectTables.CondoUnit].filter(function (conut) { return conut.CC_Deleted != true }).length > 0) {
            			if (pi[sectTables.CondoUnit].filter(function (conut) { return conut.CC_Deleted != true }).length > 1)
                            isShowSectNum = true;
                        sectParentTable = sectTables.CondoUnit;
                    }

            		if(isShowSectNum)
            			sketch.isShowSectNum = true;
                }
                
                var query = sketchSettings["sketchLookupQuery"]? _sktLookupArray[sk_count]: ( sksource.LookUpQuery? sksource.LookUpQuery: '');
                if ( query && query != '' ) {
					var testField;
                    if ( query.search( /\{.*?\}/g ) > -1 ){
                        query.match( /\{.*?\}/g ).forEach( function ( fn ) {
                            var testFieldName = fn.replace( '{', '' ).replace( '}', '' );
                            if ( testFieldName.contains('parcel.') )
                            	testField = activeParcel[testFieldName]? activeParcel[testFieldName]: null;
                            else if ( testFieldName.contains('parent.parent.') )
                            	testField = ( pi['parentRecord']? ( pi['parentRecord']['parentRecord']? pi['parentRecord']['parentRecord'][testFieldName]: null ): null );  
							else if ( testFieldName.contains('parent.') )
								testField = ( pi['parentRecord']? pi['parentRecord'][testFieldName]: null);
							else
								testField = pi[testFieldName];
                            query = query.replace( fn, pi[testFieldName] )
                        } );
                    }
                    sketch.lookUp = [];
					if(config.CustomLookupLoad)
						sketch.lookUp =	Bristol_Lookup.filter(function(lkup){ return lkup.MDL == testField });
					else{
                    	getData( query, [], function ( ld, skdata ) {
                    		var lookup = {};
                        	if (ld.length > 0) {
	                            var props = Object.keys(ld[0]);
	                            var IdField = props[0]; var NameField = props[1]; var DescField = props[1]
	                            if (!NameField) NameField = IdField;
	                            if (!DescField) DescField = NameField;
	                            for (var x in ld) {
	                                var d = ld[x];
	                                var v = d[IdField];
	                                if(v && typeof(v) == "string") v = v.replace('&gt;','>').replace('&lt;','<');
	                                if (lookup[v] === undefined)
	                                    lookup[v] = { Name: d[NameField], Description: d[DescField], Ordinal: x, SortType: null, Id: v, Object: d };
	                            }
	
	                            skdata.lookUp = lookup;
	                        }
	                        else
                        		skdata.lookUp = ld;
                    	}, sketch )
					}
                }
                
                for (var vs in sksource.VectorSource) {
                    var vectorSource = sksource.VectorSource[vs];
                    var sketchVectorTable = vectorSource.Table ? eval('activeParcel.' + vectorSource.Table) : [activeParcel];
                    var sketchVectorTableOrginal = vectorSource.Table ? eval('activeParcel.Original.' + vectorSource.Table) : [activeParcel.Original];
                    var sketchVectorKeyField = vectorSource.KeyField || 'ROWUID';
                    var doNotAllowDeleteFirstVector = getCategoryFromSourceTable(vectorSource.Table).Id ? getCategoryFromSourceTable(vectorSource.Table).DoNotAllowDeleteFirstRecord : null;
                    doNotAllowDeleteFirstVector = (doNotAllowDeleteFirstVector == null || doNotAllowDeleteFirstVector == 'false' || doNotAllowDeleteFirstVector == false ? false : true);
                    var labelRequiredTrue = vectorSource.IsLabelRequired == undefined ? true : vectorSource.IsLabelRequired;

                    if (sksource.SketchSource.Table == vectorSource.Table) {
                        var fullVector = eval('pi.' + vectorSource.CommandField);
                        var fullVectorOrg = eval('orgTable.' + vectorSource.CommandField);
                        if (!processor)
                            processor = CAMACloud.Sketching.Formatters.Sigma;
                        if(config.encoder){
                        	fullVector = sketchApp.encoder ? sketchApp.encoder.decode(fullVector) : fullVector;
                        	fullVectorOrg = sketchApp.encoder ? sketchApp.encoder.decode(fullVectorOrg) : fullVectorOrg;
                        } 
                        let customParameters = {};

                        if (config.isHelionRS) {
                            customParameters.isHelionRS = true;
                        }

						try {	
                            var parts = processor.getParts ? processor.getParts(fullVector, config.doNotDecodeEncode, { sectParentTable: sectParentTable, skRecord: pi }, customParameters) : [];
                            var orgParts = fullVectorOrg ? processor.getParts(fullVectorOrg, config.doNotDecodeEncode, { sectParentTable: sectParentTable, skRecord: pi }, customParameters) : null;
                        } 
                        catch (err) {
                            if (prcView) {
                                throw "Sketch cannot be rendered due to the Vector sketch data entered by the user.";
                            }
                            else {
                                if (!_sketchStringThrowError) {
                                    _sketchStringThrowError = true;
                                    messageBox(" Invalid sketch string format!");
                                }
                                return [];
                            }
                        }
                        
                        var pcount = 0, boundaryScale = 0;
                       	var filteredFreeFormTextLabels = processor.getFreeFormTextLabels ? processor.getFreeFormTextLabels(fullVector) : [];
                       	var filteredfreeFormLineEntries = processor.getLineEntries ? processor.getLineEntries(fullVector) : [];
                       	if(config.IsJsonFormat){
                       		var jString = (fullVector && fullVector != '') ? JSON.parse(fullVector) : "";
                       		if(jString == ""){
                       			jString = {
                       				JSON: {
                       					GlobalSettings: {SketchScale: 12, AreaSizeDecimals: 1, AreaSizeSuffix: "sf", DimensionDecimals: 2, DimensionSuffix: "'", DimensionsMatchLineColor: false},
                       					LabelEntries: [],
                       					LineEntries: []
                       				},
                       				DCS: ""
                       			}
                       		}
                       		sketch.jsonString = jString;
                       	}
                        var mvpncount = 0;
                        
                        for (var x in parts) {
                            pcount++;
                            var part = parts[x];
                            var orgPart = orgParts ? orgParts[x] : null;
                            var partSplitfirst = null, orgPartSplitfirst = null;
                            if (part.isMVPNote) {
                            	var mData = part.mvpData;
                            	for(var nt in part.mvpNotes) {
                            		var note = part.mvpNotes[nt];
                            		mvpncount++;
	                            	var n = {
		                            	uid: sketchKey.toString() + "/" + mvpncount,
		                           	 	text: ( note['noteText'] || '' ).replace( /&lt;/g, '' ).replace( /&gt;/g, '' ),
		                            	x: parseFloat( note['xPosition'] || 0 ) * ( 1 ),
		                            	y: parseFloat( note['yPosition'] || 0 ) * ( 1 ),
										fontSize: note['fontSize'],
										mvpData: mData
		                        	}
		                        	sketch.notes.push( n );
	                        	}
                            	continue;
                            }

                            if (part.isVisionNote || part.isHelionNote) {
                                part.note.uid = sketchKey.toString() + "/" + part.note.uid;
                                sketch.notes.push(part.note);
                                continue;
                            }
                            
                            if (config.SkipRowuidInChange) {
                            	var vstring = part.vector;
                            	var partsplit = vstring.split('[')[1].split(']')[0].split(/\,/);
                            	if (partsplit[partsplit.length - 1].indexOf('{') > -1 && partsplit[partsplit.length - 1].indexOf('}') > -1)
                            		partSplitfirst = partsplit.pop();
                            	part.vector = vstring.split('[')[0] + '[' + partsplit.toString() + ']' + vstring.split(']')[1];
                            	if (orgPart) {
                            		var ostring = orgPart.vector;
                            		var opartsplit = ostring.split('[')[1].split(']')[0].split(/\,/);
                            		if (opartsplit[opartsplit.length - 1].indexOf('{') > -1 && opartsplit[opartsplit.length - 1].indexOf('}') > -1)
                            			orgPartSplitfirst = opartsplit.pop();
                            		orgPart.vector = ostring.split('[')[0] + '[' + opartsplit.toString() + ']' + ostring.split(']')[1];
                            	}
                            }
                            
                            var vectorDeleted = orgParts && parts ? orgParts.length > parts.length : false;
                            var isChanged = false;
                            
                            if (part.brightModified) {
                            	isChanged = part.brightModified.isModified? true: false;
                            }
                            else {
	                            if (orgPart && orgPart.vector)
	                                isChanged = !vectorDeleted && part.vector.replace(/ /g, '') != orgPart.vector.replace(/ /g, '');
	                            else isChanged = true;
                            }
                            
                            if(config.EnableBoundary)
                            	boundaryScale = ( part.boundScale > boundaryScale ) ? part.boundScale: boundaryScale;
                            	
                            if (config.isMVP) {
                            	var partFirst = part.vector.split(':')[0], partSecond = part.vector.split(':')[1];
                                var hPartFirst = partFirst.replace(/[\[\]']+/g,'');
                                hPartFirst = hPartFirst.split(",");
                                hPartFirst[41] = part.bbScale;
                                part.vector = '[' + hPartFirst.toString() + ']:' + partSecond; 
                            }	
                            
							if (config.SkipRowuidInChange) {
								if (partSplitfirst) {
									var vstring = part.vector;
                            		var partsplit = vstring.split('[')[1].split(']')[0].split(/\,/);
                            		partsplit.push(partSplitfirst);
                            		part.vector = vstring.split('[')[0] + '[' + partsplit.toString() + ']' + vstring.split(']')[1];
								}
                            	if (orgPartSplitfirst) {
                            		var ostring = orgPart.vector;
                            		var opartsplit = ostring.split('[')[1].split(']')[0].split(/\,/);
                            		opartsplit.push(orgPartSplitfirst);
                            		orgPart.vector = ostring.split('[')[0] + '[' + opartsplit.toString() + ']' + ostring.split(']')[1];
                            	}
                            }

                            var label = [], swapLookupIdName = false, swapName;
                            if(sksource.EnableSwapLookupIdName){
                            	swapLookupIdName = true;
                            	swapName = part.name;
                            }
                            var lkName = ( sketchSettings["sketchLookupQuery"] && sketchSettings["sketchLookupQuery"] != '' && query != '')? null : (clientSettings['SketchLabelLookup'] || sketchSettings['SketchLabelLookup'] || vectorSource.LabelLookup);

                            if (lkName?.contains("/")) {
                                let slashlist = lkName.split("/");
                                for (slt in slashlist) {
                                    if (lookup[slashlist[slt]]) {
                                        lkName = slashlist[slt]; break;
                                    }
                                }
                            }

                            var fLookUp = ccma.Data.LookupMap[lkName];
	                        var isLookUpQueryField = fLookUp ? fLookUp.LookupTable == '$QUERY$' : false;
                            var labelValue = vectorSource.GetLabelValueFromField ? pi[vectorSource.LabelField] : ( sksource.AssignLookUpNameAsLabel ? part[vectorSource.LabelTarget] : ( (config.ApexEditSketchLabel && sketchSettings['ApexEditSketchLabel'] == '1' && part.code) ? part.code: part.label ) );
                            var useLookUpNameAsValue = ( sksource.AssignLookUpNameAsLabel || (config.ApexEditSketchLabel && sketchSettings['ApexEditSketchLabel'] == '1') ) ? true: false;
                            var labelNameValue = ( useLookUpNameAsValue ) ? part.label : part[vectorSource.LabelTarget];
                            var desc = lkName && lookup[lkName] ? (swapLookupIdName ? lookup[lkName][swapName] : lookup[lkName][labelValue]) : null
                            var clrLabelValue = sksource.splitByDelimiter && labelValue.indexOf(sksource.splitByDelimiter) > -1? labelValue.split(sksource.splitByDelimiter)[0]: labelValue;
                            var clrdesc = lkName && lookup[lkName] ? (swapLookupIdName ? lookup[lkName][swapName] : lookup[lkName][clrLabelValue]) : null
                            var lookDesc = ( !sksource.DoNotShowLabelDescription && sketchSettings["DoNotShowLabelDescriptionSketch"] != '1' && clientSettings["DoNotShowLabelDescriptionSketch"] != "1") ? desc : null
                            var labelDetails = {}, BasementFunctionality = false, mvpDisplayLabel = '';
                            labelDetails = part.mvpLabelDetails;
                            if(part.sketchType == 'outBuilding') {
	                        	labelValue = labelValue.replace('&lt;','<');
	                    		labelValue = labelValue.replace('&gt;','>');
	                    		mvpDisplayLabel = part.mvpDisplayLabel;
	                    	}
	                    	var _hlfs = vectorSource.HideLabelFromShow? vectorSource.HideLabelFromShow: ( (config.ApexEditSketchLabel && sketchSettings['ApexEditSketchLabel'] == '1') ? true: false );
                            label.push( { Field: vectorSource.LabelField, Value: labelValue, Description: lookDesc, IsEdited: isChanged, hiddenFromEdit: vectorSource.HideLabelFromEdit, hiddenFromShow: _hlfs, lookup: lkName, Caption: vectorSource.labelCaption, NameValue: labelNameValue, ShowCurrentValueOnly: vectorSource.ShowCurrentValueOnly, lookUpQuery: sksource.LookUpQuery, splitByDelimiter: sksource.splitByDelimiter, Target: vectorSource.LabelTarget, UseLookUpNameAsValue: useLookUpNameAsValue, IsLargeLookup: sksource.IsLargeLookup, IsRequired: labelRequiredTrue, colorCode: ( clrdesc && clrdesc.color ? clrdesc.color : null ), labelDetails: labelDetails, IsSwapLookupIdName: swapLookupIdName, showLabelDescriptionOnly: isLookUpQueryField, EnableFieldValidation: vectorSource.EnableFieldValidation } );
                            if ( vectorSource.ExtraLabelFields && ( !config.ApexEditSketchLabel || ( config.ApexEditSketchLabel && sketchSettings['ApexEditSketchLabel'] == '1' ) ) ){
                                vectorSource.ExtraLabelFields.forEach( function ( item ){
                                    var value = part[item.Target]
                                    if ( item.ValueRegx ){
                                        value = value.match( item.ValueRegx )
                                        if ( value && value[0] ) value = value[0]
                                    }
                                    let lknm = item.LookUp;
                                    if (lknm?.contains("/")) {
                                        let slashlist = lknm.split("/");
                                        for (slt in slashlist) {
                                            if (lookup[slashlist[slt]]) {
                                                lknm = slashlist[slt]; break;
                                            }
                                        }
                                    }

                                    fLookUp = ccma.Data.LookupMap[lknm]
	                                isLookUpQueryField = fLookUp ? fLookUp.LookupTable == '$QUERY$' : false;
                                    label.push({ Field: item.Name, Value: value, lookup: lknm, Target: item.Target, Caption: item.Caption, hiddenFromShow: item.HideLabelFromShow, DoNotShowLabelDescription: item.DoNotShowLabelDescription, IsLargeLookup: item.IsLargeLookup, ValidationRegx: item.ValidationRegx, requiredAny: item.requiredAny, showLabelDescriptionOnly: isLookUpQueryField, EnableFieldValidation: item.EnableFieldValidation } )
                                } )
                            }
                            var hideArea_value = false;
							if(part.isUnSketchedArea) 
								hideArea_value = true;
                            var sk = {
                                uid: sketchKey.toString() + "/" + pcount,
                                label: label,
                                labelPosition: part.labelPosition,
                                referenceIds: part.referenceIds,
                                otherValues: part.otherValues,
                                vector: part.vector,
                                isChanged: isChanged,
                                mvpData: part.mvpData,
                                AreaLabelPosition: part.AreaLabelPosition,
                                hideAreaValue:hideArea_value,
                                isUnSketchedArea: part.isUnSketchedArea,
                                Disable_CopyVector_OI: part.Disable_CopyVector_OI,
                                sketchType: part.sketchType,
                                LabelDelimiter: sksource.LabelDelimiter === undefined ? '/' : '',
                                vectorConfig: vectorSource,
                                doNotAllowDeleteFirstVector: doNotAllowDeleteFirstVector,
                                BasementFunctionality: part.BasementFunctionality,
                                mvpDisplayLabel: mvpDisplayLabel,
                            }

                            var hideSketch = (sksource.HideNullSketches && !sk.vector) || false;
                            if (!hideSketch) sketch.sketches.push(sk);
                        }
                        
                        if(config.EnableBoundary){
                            sketch.boundaryScale = (boundaryScale != 0 && boundaryScale != '') ? boundaryScale : 80;
                        }
                        
                        if(config.NoteMaxLength) {
		                	sketch.maxNotelen = config.NoteMaxLength;
		                }
                        
                        for (var x in filteredfreeFormLineEntries){
                        	var LineEntries = filteredfreeFormLineEntries[x];
                        	var label = [], Line_Type = false;
                        	if(LineEntries['LinePattern'].contains('Dashed'))
                        		Line_Type = true;
                        	var sk = {
                                uid: LineEntries['KeyCode'],
                                label: label,
                                otherValues:[],
                                vector: '[-1,-1]:' +  LineEntries['Path'],
                                referenceIds: '-1,-1',
                                isChanged: false,
                                isFreeFormLineEntries : true,
                                vectorConfig: vectorSource,
                               	PageNum: LineEntries['PageNum'],
                               	Width: LineEntries['Width'],
                               	Color: LineEntries['Color'],
                               	LinePattern: LineEntries['LinePattern'],
                               	Line_Type: Line_Type,
                               	KeyCode: LineEntries['KeyCode'],
                               	hideAreaValue :true  	
                            }
                            var hideSketch = (sksource.HideNullSketches && !sk.vector) || false;
                            if (!hideSketch) sketch.sketches.push(sk);
                        }
                        
                        for (var x in filteredFreeFormTextLabels){
                        	var note = filteredFreeFormTextLabels[x];
                        	var notePosition = note["Position"].split(",");
                        	var n = {
                            	uid: note['KeyCode'],
                           	 	text: ( note['Text'] || '' ).replace( /&lt;/g, '' ).replace( /&gt;/g, '' ),
                            	x: parseInt( notePosition[0] || 0 ) * ( 1 ),
                            	y: parseInt( notePosition[1] || 0 ) * ( 1 ),
                            	//lx: parseInt( note[sksource.NotesSource.LineXField] || 0 ) * ( sksource.NotesSource.ScaleFactor || 1 ),
                            	//ly: parseInt( note[sksource.NotesSource.LineYField] || 0 ) * ( sksource.NotesSource.ScaleFactor || 1 ),
                            	Bold: note['Bold'],
								Color: note['Color'],
								FontFace: note['FontFace'],
								FontSize: note['FontSize'],
								Italic: note['Italic'],
								PageNum: note['PageNum'],
								Rotation: note['Rotation'],
								KeyCode: note['KeyCode']
                        	}
                        	sketch.notes.push( n );
                        }

                    } else {

                        //var filteredDetails = sketchVectorTable.filter(function (ivx) { if (ivx.ParentROWUID) return (ivx.ParentROWUID == pi.ROWUID); else return (eval('ivx.' + sksource.VectorSource.ConnectingFields[0]) == eval('pi.' + sksource.SketchSource.KeyFields[0])); });
                        var filteredDetails = sketchVectorTable && sketchVectorTable.filter( function ( ivx ) { if ( ivx.ParentROWUID ) return ( ivx.ParentROWUID == pi.ROWUID && eval( vectorSource.filter || true ) ); else if ( pi && pi.constructor.name == "Parcel" ) { return ( ivx.CC_ParcelId == pi.Id ); } else return ( eval( 'ivx.' + vectorSource.ConnectingFields[0] ) == eval( 'pi.' + sksource.SketchSource.KeyFields[0] ) && eval( vectorSource.filter || true ) ); } );
                        //var filteredDetailsOriginal = sketchVectorTableOrginal.filter(function (ivx) { if (ivx.ParentROWUID) return (ivx.ParentROWUID == pi.ROWUID); else if (pi && pi.constructor.name == "Parcel") { return (ivx.CC_ParcelId == pi.Id); } else return (eval('ivx.' + sksource.VectorSource.ConnectingFields[0]) == eval('pi.' + sksource.SketchSource.KeyFields[0])); });
                        if( config.IsProvalConfig && ( vectorSource.Table == 'sktsegment' || vectorSource.Table == 'ccv_SktSegment' ) )
                        	filteredDetails = filteredDetails.sort(function(x,y){ return x.CC_segment_id - y.CC_segment_id });
                        if(config.EnableBoundary && config.IsProvalConfig && config.ConfigTables){
                        	var ConfigTables = config.ConfigTables, boundaryScales = 0;
                        	if ( pi[ConfigTables.SktHeader] && pi[ConfigTables.SktHeader][0] && pi[ConfigTables.SktHeader][0]['scale'] )
                        		boundaryScales = pi[ConfigTables.SktHeader][0]['scale'];     
                            sketch.boundaryScale = (boundaryScales != 0 && boundaryScales != '') ? boundaryScales : 100;
                        }
                        if (config.OutBuildingDefinition && vectorSource.type == 'outBuilding' ) {
                            for (var x in filteredDetails) {
                            	var id = filteredDetails[x];
                            	var outB = config.OutBuildingDefinition( id, vectorSource, {sksource});
                            	if(!isEmpty(outB)) {
                            		var outBd = outB.pop();
                            		sketch.sketches.push(outBd);
                            	}
                            }
                        }
                        else if (config.SketchDefinition) {
                        	var sks = config.SketchDefinition(filteredDetails, isChanged);
                        	sks.forEach(function(sk){ sketch.sketches.push(sk); });
                        }
                        else 
	                        for (var x in filteredDetails) {
	                            var readonly = false
	                            var id = filteredDetails[x];
	                            var org = id && id.Original ? id.Original : [];
	                            var vectorKey = id[sketchVectorKeyField];
	                            var labelValue, field, index, vector, vectorDetail, labelPosition, isChanged, areaFieldValue = null, noVectorSegment = false, areaUnit = null, Line_Type = false, isUnSketchedTrueArea = false, perimeterFieldValue = null, BasementFunctionality = false, yearValueToDisplay;
	                            if (config.VectorDefinition) {
	                                vectorDetail = config.VectorDefinition(id, vectorSource);
	                                vector = vectorDetail.vector_string;
	                                isChanged = vectorDetail.isChanged;
	                                noVectorSegment = vectorDetail.noVectorSegment;
	                                Line_Type = vectorDetail.Line_Type;
	                                isUnSketchedTrueArea = vectorDetail.isUnSketchedTrueArea;
	                                areaFieldValue = vectorDetail.areaFieldValue? vectorDetail.areaFieldValue: null;
	                                perimeterFieldValue = vectorDetail.perimeterFieldValue ? vectorDetail.perimeterFieldValue: null
	                                areaUnit = vectorDetail.areaUnit? vectorDetail.areaUnit: null;
                                }
	                            else {
	                             	field = getDataField( vectorSource.LabelField, vectorSource.Table )
	                             	index = sketchVectorTable.indexOf( id )
	                             	if ( field && field.ReadOnlyExpression )
                                 		readonly = activeParcel.EvalValue( vectorSource.Table + '[' + index + ']', field.ReadOnlyExpression );
                                 	vector = eval( 'id.' + vectorSource.CommandField );
                                 	labelPosition = eval( 'id.' + vectorSource.LabelCommandField );
                                 	
                                 	if (sksource.EnableMarkedAreaDrawing) 
                                 		areaFieldValue = eval( 'id.' + vectorSource.AreaField);            
	                                if(config.IsVectorStartCommandField)
	                                	isChanged= (eval('id.' + vectorSource.CommandField) != eval('org.' + vectorSource.CommandField)) || (eval('id.' + vectorSource.LabelField) != eval('org.' + vectorSource.LabelField)) || (eval('id.' + vectorSource.VectorStartCommandField) != eval('org.' + vectorSource.VectorStartCommandField))
	                                else
	                                	isChanged= eval('id.' + vectorSource.CommandField) != eval('org.' + vectorSource.CommandField) || eval('id.' + vectorSource.LabelField) != eval('org.' + vectorSource.LabelField)
	                            }
	                            if (config.LabelPositionDefinition)
	                                labelPosition = config.LabelPositionDefinition(id, vectorSource);
	                            var labelDetails = {};
	                            if (config.LabelDefinition) {
	                            	labelDetails = config.LabelDefinition(id, vectorSource);
	                            	labelValue = labelDetails.labelValue;
	                            	isChanged = isChanged || labelDetails.isChanged;
	                            	BasementFunctionality = labelDetails.BasementFunctionality ? labelDetails.BasementFunctionality : false;
	                            	//if (labelValue == '') labelPosition = null;
                            	}
	                            else labelValue = eval( 'id.' + vectorSource.LabelField )
	                            if(sksource.ShowYearBuilt && clientSettings['ShowYearValue'] == "1" && clientSettings['YearValueToDisplay']){
	                            	var yearFieldValue = clientSettings['YearValueToDisplay'];
	                            	var yearField = getDataField( yearFieldValue, vectorSource.Table);
	                            	if(yearField)
	                            		yearValueToDisplay = eval( 'id.' + yearFieldValue);
	                            }
	                            
	                            let lblLookup = vectorSource.LabelLookup;
		                        if (vectorSource.LabelLookupSelector) {
		                        	let lblkField = getDataField(vectorSource.LabelLookupSelector.FieldName, vectorSource.LabelLookupSelector.Table);
		                        	lblLookup = lblkField? (lookup['#FIELD-'+ lblkField.Id]? '#FIELD-'+ lblkField.Id: lblLookup): lblLookup;
		                        }

                                if (lblLookup?.contains("/")) {
                                    let slashlist = lblLookup.split("/");
                                    for (slt in slashlist) {
                                        if (lookup[slashlist[slt]]) {
                                            lblLookup = slashlist[slt]; break;
                                        }
                                    }
                                }
	                            
	                            var fLookUp = ccma.Data.LookupMap[lblLookup]
	                            var isLookUpQueryField = fLookUp ? fLookUp.LookupTable == '$QUERY$' : ( ( sketchSettings["sketchLookupQuery"] && sketchSettings["sketchLookupQuery"]!= '' )? true: false );
	                            var desc = ( sketchSettings["sketchLookupQuery"] && sketchSettings["sketchLookupQuery"] != '' && query != '')? null : ( lblLookup && lookup[lblLookup] ? lookup[lblLookup][labelValue] : null );
	                            var lookDesc = ( !sksource.DoNotShowLabelDescription && sketchSettings["DoNotShowLabelDescriptionSketch"] != '1' && clientSettings["DoNotShowLabelDescriptionSketch"] != "1") ? desc : null;
	                            var label = []; 
	                            var _hlfs = vectorSource.HideLabelFromShow? vectorSource.HideLabelFromShow: ( (config.ApexEditSketchLabel && sketchSettings['ApexEditSketchLabel'] == '1') ? true: false );
	                            label.push( { Field: vectorSource.LabelField, Value: labelValue, Description: lookDesc, IsEdited: false, lookup: lblLookup, ReadOnly: readonly, labelDetails: labelDetails, Caption: vectorSource.labelCaption, IsLargeLookup: sksource.IsLargeLookup, IsRequired: labelRequiredTrue, colorCode: ( desc && desc.color ? desc.color : null ), showLabelDescriptionOnly: isLookUpQueryField, hiddenFromShow: _hlfs, EnableFieldValidation: vectorSource.EnableFieldValidation } );
	                            if ( vectorSource.ExtraLabelFields && ( !config.ApexEditSketchLabel || ( config.ApexEditSketchLabel && sketchSettings['ApexEditSketchLabel'] == '1' ) ) ) {
	                                vectorSource.ExtraLabelFields.forEach(function (item) {
	                                    field = getDataField(item.Name, vectorSource.Table)
                                        var val = eval('id.' + item.Name)
                                        let lknm = item.LookUp;
                                        if (lknm?.contains("/")) {
                                            let slashlist = lknm.split("/");
                                            for (slt in slashlist) {
                                                if (lookup[slashlist[slt]]) {
                                                    lknm = slashlist[slt]; break;
                                                }
                                            }
                                        }

                                        var t = lknm && lookup[lknm] ? lookup[lknm][val] : val
	                                    if (field && field.ReadOnlyExpression)
	                                        readonly = activeParcel.EvalValue( vectorSource.Table + '[' + index + ']', field.ReadOnlyExpression )
                                        fLookUp = ccma.Data.LookupMap[lknm]
	                                    isLookUpQueryField = fLookUp ? fLookUp.LookupTable == '$QUERY$' : false;
                                        label.push({ Field: item.Name, Value: val, Description: t, lookup: lknm, ReadOnly: readonly, Caption: item.Caption, showLabelDescriptionOnly: isLookUpQueryField, hiddenFromShow: item.HideLabelFromShow, EnableFieldValidation: item.EnableFieldValidation } )
	                                })
	                            } 
	                            var sk = {
	                                uid: vectorKey.toString(),
	                                label: label,
	                                vector: vector,
	                                labelPosition: labelPosition,
	                                isChanged: isChanged,
	                                vectorConfig: vectorSource,
	                                noVectorSegment: noVectorSegment,
	                                Line_Type: Line_Type,
	                                areaFieldValue: areaFieldValue,
	                                isUnSketchedTrueArea: isUnSketchedTrueArea,
	                                perimeterFieldValue: perimeterFieldValue,
	                                areaUnit: areaUnit,
	                                BasementFunctionality: BasementFunctionality,
	                                doNotAllowDeleteFirstVector: doNotAllowDeleteFirstVector,
	                                yearValueToDisplay: yearValueToDisplay,
	                                vectorStart: eval( 'id.' + vectorSource.VectorStartCommandField )
	                            }
	                            if ( vectorSource.FootModeField ){
	                                var footMode = id[vectorSource.FootModeField]
	                                if ( footMode == true || footMode == 'true' ){
	                                    sk.footMode = footMode;
	                                }
	                            }
	                            if (vectorSource.IdField) 
	                                sk.rowId = eval( 'id.' + vectorSource.IdField );
	                            sketch.index = parseInt( i );
	                            if (processor.decodeSketch) 
	                                sk.vector = processor.decodeSketch(sk.vector);
	                            if ( vectorSource.vectorPagingField )                          {
	                                var page = id[vectorSource.vectorPagingField]
	                                if ( page == true || page == 'true' ){
	                                    sk.page = page;
	                                    var s = _.clone( sketch )
	                                    s.sketches = [];
	                                    s.sketches.push( sk )
	                                    sketches.push( s );
	                                    s.isNewPage = true;
	                                    isPages = true;
	                                    s.index = ( sketches.length )
	                                    continue;
	                                }
	                            }
	                            var hideSketch = (sksource.HideNullSketches && !sk.vector) || false;
	                            if (!hideSketch) sketch.sketches.push(sk);
	                        }
                    }
                }
                
                if(config.IsNoteMaxLength) {
                	if(sksource.NotesSource) {
                		var maxNotelen, noteField = sksource.NotesSource.TextField, sNoteTable = sksource.NotesSource.Table;
                		maxNotelen = (getDataField(noteField,sNoteTable) && getDataField(noteField,sNoteTable).MaxLength && getDataField(noteField,sNoteTable).MaxLength != "" && getDataField(noteField,sNoteTable).MaxLength != '0') ? getDataField(noteField,sNoteTable).MaxLength: "34";
                		maxNotelen = maxNotelen.toString();
                		sketch.maxNotelen = maxNotelen;
                	}
                }
                else if (sksource.NotesSource && sksource.NotesSource.Table && sksource.NotesSource.TextField ) {
					let noteFld = getDataField && getDataField(sksource.NotesSource.TextField, sksource.NotesSource.Table)
					if ( noteFld && noteFld.MaxLength)
						sketch.maxNotelen = noteFld.MaxLength;
				}
				
				if (config.NoteDefinition)
                	sketch.notes = config.NoteDefinition(pi);
                else if (notesTable && notesTable.length > 0) {
                    var cf = sksource.NotesSource.ConnectingFields;
                    var filterCondition = "1==1";
                    cf.forEach( function ( f ) {
                        filterCondition += " && ivx." + f + " == pi." + f
                    } );
                    var filteredNotes = notesTable.filter( function ( ivx ) { if ( ivx.ParentROWUID ) return ( ivx.ParentROWUID == pi.ROWUID ); else return ( eval( filterCondition ) ) } );
                    for ( var x in filteredNotes ) {
                        var note = filteredNotes[x];
                        var n = {
                            uid: note[sksource.NotesSource.KeyField || 'ROWUID'],
                            text: ( note[sksource.NotesSource.TextField] || '' ).replace( /&lt;/g, '' ).replace( /&gt;/g, '' ),
                            x: parseInt( note[sksource.NotesSource.PositionXField] || 0 ) * ( sksource.NotesSource.ScaleFactor || 1 ),
                            y: parseInt( note[sksource.NotesSource.PositionYField] || 0 ) * ( sksource.NotesSource.ScaleFactor || 1 ),
                            lx: parseInt( note[sksource.NotesSource.LineXField] || 0 ) * ( sksource.NotesSource.ScaleFactor || 1 ),
                            ly: parseInt( note[sksource.NotesSource.LineYField] || 0 ) * ( sksource.NotesSource.ScaleFactor || 1 )
                        }
                        sketch.notes.push( n );
                    }
                }
                if ((sketch.config.AllowSegmentAddition || sketch.config.AllowSegmentAdditionByCustomButtons) || sketch.sketches.length > 0) {
                    sketches.push(sketch);
                }
            }


        }
        if ( isPages && sketches.length > 1 ){
            sketches = sketches.sort( function ( x, y ) { return x.index - y.index } )
            sketches.forEach( function ( sk,i ){
                sk.label = sk.label + ' - ' + (i + 1);
                if (config.Isccdropitems) {
                    sk.sid = sk.sid + '-' + (i + 1);
                    sk.uid = sk.uid + '-' + (i + 1);
                }
            })
        }
        return sketches;
    }

    if (full) {
        if (x.IsComparable == 0) {
            getData('SELECT * FROM Comparables WHERE ParcelId = ?', [parcelId.toString()], function (comp, parcel) {
                if (comp.length > 0) {
                    getParcelOnly(comp[0].C1, function (p) {
                        parcel.Comparables.push(p);
                        getParcelOnly(comp[0].C2, function (p) {
                            parcel.Comparables.push(p);
                            getParcelOnly(comp[0].C3, function (p) {
                                parcel.Comparables.push(p);
                                getParcelOnly(comp[0].C4, function (p) {
                                    parcel.Comparables.push(p);
                                    getParcelOnly(comp[0].C5, function (p) {
                                        parcel.Comparables.push(p);
                                    }, parcel);
                                }, parcel);
                            }, parcel);
                        }, parcel);
                    }, parcel);
                }
            }, this, true);
        }
    }

    if (x != null) {
        this.CopyFrom(x)
    }
}
$.extend(Parcel.prototype, ccma.Data.Evaluable);

function getParcelOnly(parcelId, callback, otherData) {
    getData("SELECT *, P.Id FROM Parcel P LEFT OUTER JOIN Neighborhood N ON (P.NeighborhoodId = N.Id) LEFT OUTER JOIN AggregateFields ag ON ag.Id = p.Id WHERE P.Id = " + safeParseInt(parcelId), [], function (parcels) {
        if (parcels.length > 0) {
            var p = new Parcel(parcelId, parcels[0], false);
            if (callback) callback(p, otherData);
        }
    });
}

function getParcelWithMap(parcelId, callback) {
    getData("SELECT *, P.Id FROM Parcel P LEFT OUTER JOIN Neighborhood N ON (P.NeighborhoodId = N.Id) LEFT OUTER JOIN AggregateFields ag ON ag.Id=p.Id WHERE P.Id = " + safeParseInt(parcelId), [], function (parcels) {
        if (parcels.length > 0) {
            var p = new Parcel(parcelId, parcels[0], false);

            getData("SELECT * FROM MapPoints WHERE ParcelId = ? ORDER BY (Ordinal * 1.0)", [parcelId.toString()], function (points, parcel) {
                for (var x in points) {
                    parcel.AddPoint(points[x].Latitude, points[x].Longitude, points[x].RecordNumber || 0);
                }

                if (callback) callback(parcel);
            }, p);

            //if (callback) callback(p, otherData);
        }
    });
}

function getParcelWithAllDataAndImages(parcelId, callback, otherData,skipValidationCallback) {
    getData("SELECT *, P.Id,P.CC_Deleted FROM Parcel P LEFT OUTER JOIN Neighborhood N ON (P.NeighborhoodId = N.Id) LEFT OUTER JOIN AggregateFields ag ON ag.Id=p.Id WHERE P.Id = " + safeParseInt(parcelId), [], function (parcels) {
        if (parcels.length > 0) {
            var p = new Parcel(parcelId, parcels[0], false), temp_data = null;
            if(isBPPUser && p.CC_Deleted == 'true'){ skipValidationCallback(); return;}
            var is_img_loaded = false, other_props_loaded = false,isMapPointsLoaded = false;
            var callbackProcessor = function(){
            if (is_img_loaded && other_props_loaded && isMapPointsLoaded && callback) 
            		callback(p, temp_data);
            }
	    p.MapPoints = [];
            getData("SELECT * FROM MapPoints WHERE ParcelId = ? ORDER BY (Ordinal * 1)", [parcelId.toString()], function (points) {
                for (var x in points) {
                    p.AddPoint(points[x].Latitude, points[x].Longitude, points[x].RecordNumber || 0);
                    p.MapPoints.push({ Id: points[x].Id, Latitude: points[x].Latitude, Longitude: points[x].Longitude, Ordinal: points[x].Ordinal, ParcelId: points[x].ParcelId, RecordNumber: points[x].RecordNumber });
                }
                isMapPointsLoaded = true;
                callbackProcessor();
            });
            IDB_getData('Images', ['ParcelId', 'Type'], ['=', '='], [parseInt(parcelId), 1], ['AND'], function (images) {
				images = images.slice(0,ccma.UI.Camera.MaxPhotosPerParcel);
            	var img_count = images.length, counter = 0, parcel_images = images;
                if (img_count == 0) {
					is_img_loaded = true;
					getDataUrl('/static/images/residential.png',function(res){
								 p.FirstPhoto = res;
							});
					callbackProcessor();
                }
				else {
                    for (var x in images) 
                        loadPhotoProperties(images[x], function(){ 
                            if(img_count == ++counter) {
                                is_img_loaded = true;
                                for (var x in parcel_images)
                                    p.Photos.push(parcel_images[x]);
                                if (parcel_images.length > 0) {
                                    var prcimage = (parcel_images.filter(function (x) { return checkIsFalse(x.IsPrimary); }).length > 0) ? parcel_images.filter(function (x) { return checkIsFalse(x.IsPrimary); })[0].Image : parcel_images[0].Image;
                                    p.FirstPhoto = prcimage;
                                }
                                callbackProcessor();
                            }
                        });
                }
            });

      		tableList = tableListGlobal.slice();
            loadAuxData(p, parcelId, tableList, function (p) {
                LoadAuxDataPRC(p, function () {
                    /*IDB_getData('Images', ['ParcelId', 'Type'], ['=', '='], [parseInt( parcelId ), 1], ['AND'], function (images) {
						//images = images.slice(0,ccma.UI.Camera.MaxPhotosPerParcel);
            			//getData("SELECT * FROM Images WHERE ParcelId = ? AND  Type = '1' LIMIT " + ccma.UI.Camera.MaxPhotosPerParcel, [parcelId.toString()], function (images) {
                        for (var x in images)
                            p.Photos.push(images[x]);
                        if (images.length > 0) {
                            var prcimage = (images.filter(function (x) { return checkIsFalse(x.IsPrimary); }).length > 0) ? images.filter(function (x) { return checkIsFalse(x.IsPrimary); })[0].Image : images[0].Image;
                            p.FirstPhoto = prcimage;
                        }*/
                        loadActiveParcelChanges(function(){
                            loadPRCactiveParcel(TableHierarchical, p, function (p) {
                                deleteTempRecords(p, function (t) {
                                	temp_data = t;
                                	other_props_loaded = true;
                                    callbackProcessor();
                                });
                            });
                        },p)
                    //});
                });
            });
        }
    });
}

function getParcelWithAllData(parcelId, callback, otherData,loadAll) {
    getData("SELECT *, P.Id,P.ROWUID FROM Parcel P LEFT OUTER JOIN Neighborhood N ON (P.NeighborhoodId = N.Id) WHERE P.Id = " + safeParseInt(parcelId), [], function (parcels) {
        if ( parcels.length > 0 ){
            //if ( parcels.length == 2 ){
            //    parcels = parcels.filter( function ( p ) { return (p.CC_YearStatus == 'A' || !p.CC_YearStatus) } );
            //    parcels[0].CC_YearStatus = 'C';
            //}
            var p = new Parcel(parcelId, parcels[0], false);
          	tableList = tableListGlobal.slice();
          	loadAuxData( p, parcelId, tableList, function ( p ){
          	    if ( callback ) callback( p, otherData );
          	}, loadAll );
        }
        else{
        	messageBox('Template Data Parcel is not defined. Unable to proceed with Default Template Copy.')
        	return false;
        }
    });
}

function getDataUrl(imgsrc,callback) {
    var img = new Image();
    img.onload = function () {
         var canvas = document.createElement('canvas'),
         ctx = canvas.getContext('2d');
         canvas.height = img.naturalHeight;
         canvas.width = img.naturalWidth;
         ctx.drawImage(img, 0, 0);
         var b64 = canvas.toDataURL('image/png')
         if(callback)
         	callback(b64);
        };
    img.src = imgsrc;
}

function getParcel(parcelId, callback) {
    getData("SELECT * FROM SubjectParcels WHERE Id = ?", [parcelId.toString()], function (parcels) {
        if (parcels.length > 0) {
            activeParcel = new Parcel(parcelId, parcels[0]);
            //  activeParcelData = new Parcel(parcelId, parcels[0]);
            appState.parcelId = parcelId;
	     activeParcel.MapPoints = [];
	     activeParcel.ParcelGISPoints = [];
            getData("SELECT * FROM MapPoints WHERE ParcelId = ? ORDER BY (Ordinal * 1)", [parcelId.toString()], function (points) {
                for (var x in points) {
                    activeParcel.AddPoint(points[x].Latitude, points[x].Longitude, points[x].RecordNumber || 0);
                    activeParcel.MapPoints.push({ Id: points[x].Id, Latitude: points[x].Latitude, Longitude: points[x].Longitude, Ordinal: points[x].Ordinal, ParcelId: points[x].ParcelId, RecordNumber: points[x].RecordNumber });
                }
            });
            getData("SELECT * FROM ParcelGISPoints WHERE ParcelId = ? ", [parcelId.toString()], function (points) {
                for (var x in points) {
                    activeParcel.ParcelGISPoints.push({ Latitude: points[x].Latitude, Longitude: points[x].Longitude, ParcelId: points[x].ParcelId, Label:points[x].Label});
                }
            },null, true);

              tableList = tableListGlobal.slice();
                loadAuxData(activeParcel, parcelId, tableList, function () {
                	//activeParcel =  sortSourceData( activeParcel ); execute after PCI push 
                //    IDB_getData('Images',['ParcelId'],['='],[parseInt(parcelId)],[], function (images) {
                    IDB_getData('Images', ['ParcelId', 'Type'], ['=', '='], [parseInt(parcelId), 1], ['AND'], function (images) {
						images = images.slice(0,ccma.UI.Camera.MaxPhotosPerParcel);
            			//getData("SELECT * FROM Images WHERE ParcelId = ? AND  Type = '1' LIMIT " + ccma.UI.Camera.MaxPhotosPerParcel, [parcelId.toString()], function (images) {
                        for (var x in images) {
                            activeParcel.Photos.push(images[x]);
                            //activeParcel.Data.Photos.push(images[x]);
                        }

                        if (images.length > 0) {
                            var prcimage = (images.filter(function (x) { return checkIsFalse(x.IsPrimary); }).length > 0) ? images.filter(function (x) { return checkIsFalse(x.IsPrimary); })[0].Image : images[0].Image;
                            activeParcel.FirstPhoto = prcimage;
                            //    activeParcelData.FirstPhoto = prcimage;
                        }
                       else {
							getDataUrl('/static/images/residential.png',function(res){
								 if(activeParcel) activeParcel.FirstPhoto = res;
							});
						}
						
                        IDB_getData( 'Images', ['ParcelId', 'Type'], ['=', '='], [parseInt( parcelId ), 2], ['AND'], function ( images ){
                            activeParcel.sketchImages = images;
                            LoadAuxDataPRC( activeParcel, function (){
                                loadActiveParcelChanges( function (){
                                	activeParcel =  sortSourceData( activeParcel ); //MA_1906 change
                                    loadPRCactiveParcel( TableHierarchical, activeParcel, function ( p )  {
                                        if ( callback ) callback( p );
                                    } );
                                }, activeParcel )
                            } );

                        } );
                      

                    });

                });

        }
        else {
            activeParcel = null; activeParcelData = null;
        }

        return activeParcel;
    });
}

function loadPRCactiveParcel(tablelist, parcel, callback) {
    currentActiveParcel = parcel;
    if (tablelist.length == 0) {
        if (callback) callback(parcel);
        return;
    }
    var sourceTable = tablelist.pop().tableName;
    if (sourceTable === undefined) {
        if (callback) callback(parcel);
        return;
    }
    
    var childvalue = eval('parcel.' + sourceTable);
    var parenttable = parentChild.filter(function (s) { return s.ChildTable == sourceTable })[0].ParentTable;
    var Parentvalue = eval('parcel.' + parenttable);
    var filterfieldsArray = fieldCategories.filter(function (x) { return x.SourceTable == sourceTable }).map(function (p) { return p.FilterFields })// fieldCategories.filter(function (x) { return x.SourceTable == sourceTable });
	
    if (childvalue && childvalue.length == 0) { 
		if (Parentvalue) Parentvalue.map(function(p){ p[sourceTable] = []; });
		loadPRCactiveParcel(tablelist, parcel, callback); 
		return; 
	}
	filterfieldsArray.forEach(function(filterfields){
    	if (filterfields && filterfields != "") { filterfields = filterfields.split(','); } else { filterfields = [] }
    	if (Parentvalue) {
        	Parentvalue.forEach(function (x) {
           		var filterCondition = '';
            	var iter_value = 0;
            	while (iter_value < filterfields.length) {
                	var childFilter, parentFilter; parentFilter = childFilter = filterfields[iter_value];
                	if (childFilter.indexOf('/') > -1) { parentFilter = childFilter.split('/')[0]; childFilter = childFilter.split('/')[1]; }
                	var values = eval("x." + parentFilter);
                	filterCondition += 'd.' + childFilter + ' == "' + values + '" ';
                	iter_value += 1;
                	if (iter_value < filterfields.length) {
                    	filterCondition += ' && ';
                	}
            	}
            	if (filterCondition == '') filterCondition = '1==1'
            	var parentROWUID = x.ROWUID;

            	var fiteredSource = eval('childvalue.filter(function (d, i) { return ((!d.ParentROWUID &&(' + filterCondition + ')) || (d.ParentROWUID && d.ParentROWUID == ' + parentROWUID + '))})');
            	fiteredSource.forEach(function (cc, findex) {
                	cc["parentRecord"] = x;
                	cc["ParentROWUID"] = parentROWUID;
                	cc["CC_FIndex"] = findex;
           		});
            	x[sourceTable] = fiteredSource;
        	});
    	}
	});
    loadPRCactiveParcel(tablelist, parcel, callback);
}
function LoadTableHierarchical(TableName) {
    var t = 1;
    var ab = function (child) { var dd = parentChild.filter(function (f) { return f.ChildTable == child }); if (dd.length == 0) { return t } if (dd[0].ParentTable.toLowerCase() == 'parcel') { return t } else { t = t + 1; ab(dd[0].ParentTable) } };
    var dd = ab(TableName); return t;
}
function LoadAuxDataPRC(activeParcel, callback) {
    TableHierarchical = [];
    tableListGlobal.forEach(function (name) {

        var no = LoadTableHierarchical(name);
        if (no > 1) {
            TableHierarchical.push({ 'tableName': name, 'no': no });
        }
    });
    TableHierarchical.sort(function (a, b) {
        return a.no - b.no;
    })
    if (callback) callback(activeParcel);
}
function loadAuxData(parcel, parcelId, tableList, callback,loadAll) {
    if (tableList.length == 0) {
        if (callback) callback(parcel);
        return;
    }
    var sourceTable = tableList.pop();
    if (sourceTable === undefined || sourceTable == null) {
        if (callback) callback(parcel);
        return;
    }
    var filter = '';
    var cat = getCategoryFromSourceTable(sourceTable)
    if ( fylEnabled && !loadAll && cat.YearPartitionField && !(parcelId =='-99') )
        filter = showFutureData ? " AND CC_YearStatus = 'F' " : "AND ( CC_YearStatus ='A' OR CC_YearStatus IS NULL) "
    var sql = "SELECT * FROM " + sourceTable + " WHERE CC_ParcelId = ?  " + filter;
    getData(sql, [parcelId.toString()], function (adata, sourceTable) {
        parcel[sourceTable] = copyDataRows(adata);
        parcel.Original[sourceTable] = copyDataRows(adata);
        var data = parcel[sourceTable];
        if (data) {
            data.forEach(function (ab) {
                ab.Original = {};
                $.each(ab, function (index, element) {
                    if (typeof (element) != 'object')
                        ab.Original[index] = element;
                });
            });
            //    }
        }
        loadAuxData( parcel, parcelId, tableList, callback, loadAll );
    }, sourceTable, true, function () {
        parcel[sourceTable] = [];
        parcel.Original[sourceTable] = [];
        loadAuxData( parcel, parcelId, tableList, callback, loadAll );
    });
}

function loadActiveParcelChanges(callback,p) {
    getData("SELECT pc.*, f.SourceTable FROM ParcelChanges pc LEFT OUTER JOIN Field f ON pc.FieldId = f.Id LEFT OUTER JOIN FieldCategory fc ON f.CategoryId = fc.Id WHERE pc.ParcelId in (?,?)", [p.Id.toString(), safeParseString(p.Id)], function (changes) {
        p.Changes = [];
        if (changes.length == 0) {
            if (callback) callback(p);
        }
        else {
            for (var x in changes) {
                var ch = changes[x];
                var source = ch.SourceTable
                var field = ch.Field;
                var oldValue = ch.OldValue;
                var updateSketchOldVal = null, updateSketchOldFormat = false;
                if (oldValue == "null")
                    oldValue = null;
                if (ch.FieldId == "UPDATESKETCH") {
                    var skhfieldId = ch.OldValue.split("|");
                    if (skhfieldId && datafields[skhfieldId[0]]) {
                    	let flname = datafields[skhfieldId[0]].Name;
                        try {
							if(JSON.parse(field)) {
								oldValue = JSON.parse(field);
								updateSketchOldVal = oldValue;
								oldValue = updateSketchOldVal[flname];
							}
							else {
								oldValue = field;
								updateSketchOldVal = oldValue;
								updateSketchOldFormat = true;
							}
						}
						catch(e) { 
							oldValue = field;
							updateSketchOldVal = oldValue;
							updateSketchOldFormat = true;
						} 
						source = datafields[skhfieldId[0]].SourceTable;
                        field = flname;						
                    }
                }
                 if(!(oldValue == ch.NewValue || (oldValue == null && ch.NewValue == '')))
					p.Changes.push({Action : ch.Action,AuxRowId :ch.AuxRowId? ch.AuxRowId:null,ChangedTime : ch.ChangedTime,DownSynced : ch.Synced,FieldId : ch.FieldId,FieldName : ch.Field,Latitude : ch.Latitude,Longitude : ch.Longitude,NewValue : ch.NewValue,OldValue : ch.OldValue,ParcelId : ch.ParcelId,ParentAuxROWUID :ch.AuxRowId? ch.ParentAuxRowId:null});
				
                if ((ch.AuxRowId == null || source == null) && field) {
                   // p.Changes[field] = ch;
                   		p.Original[field] = oldValue;
                    //activeParcelData[ch.Field] = oldValue;
                	}
                else {
                   // if (p.Changes[source] == null)
                     //   p.Changes[source] = {}
                    //p.Changes[source][field] = ch;
                    	for (ex in p.Original[source]) {
                        	var acx = p.Original[source][ex];
                        	if (acx.ROWUID == ch.AuxRowId && field) {
                            //console.log(acx, updatedDataObject(acx, ch.Field, ch.OldValue));
                           		 if (ch.FieldId == "UPDATESKETCH") {
                                    var fIds = ch.OldValue.split('|'), fvalues = ch.NewValue.split('|');
					                if(fIds.length > 1 && fvalues.length > 1) {
						                for(var i = 0; fIds.length > i; i++) {
						                	acx = p.Original[source][ex];
									        if(fIds[i]!= -1) {
									           	  let fname = datafields[fIds[i]].Name;
									           	  let sktOrgVal = !updateSketchOldFormat? (updateSketchOldVal && updateSketchOldVal[fname]? updateSketchOldVal[fname]: ''): updateSketchOldVal;
									              p.Original[source][ex] = updatedDataObject(acx, fname, sktOrgVal);
	                            	              p[source][ex].Original[fname] = sktOrgVal;
									         }
						                }
					                }
                                 }
                                 else{
                           		     p.Original[source][ex] = updatedDataObject(acx, field, oldValue);
                            	     p[source][ex].Original[field] = oldValue;
                                 }
                        	}
                      	}
                    }
                
            }
            if (callback) callback(p);
        }
    });
}

function applyParcelChanges(callback){
	var ssql = "SELECT pc.*, f.SourceTable FROM ParcelChanges pc LEFT OUTER JOIN Field f ON pc.FieldId = f.Id LEFT OUTER JOIN FieldCategory fc ON f.CategoryId = fc.Id WHERE pc.ParcelId <> '-9999' ORDER BY (pc.ChangedTime*1) ";
	getData(ssql, [], function (changes) {
		var sqls = [];
		for(var i = 0; i < changes.length; i++){
			var x = changes[i];
			if (x.NewValue){
				var chngs = "";
				changes.filter(function(y){return (y.AuxRowId == x.AuxRowId && y.ParcelId == x.ParcelId && y.SourceTable == x.SourceTable && y.NewValue)}).forEach(function(item, index){
					if(chngs.indexOf("["+item.Field+"] ") > -1){
						var itemIndex = chngs.indexOf("["+item.Field+"] "),	
							itemLastIndex = chngs.indexOf("',",itemIndex);
						chngs = chngs.replace(chngs.substring(itemIndex,itemLastIndex+2),'');
					}
					chngs += '[' + item.Field + '] = \'' + item.NewValue.replace(/'/g, "''") + '\','
					changes.splice(changes.indexOf(item),1);
				});
				chngs = chngs.substring(0, chngs.length-1);
				var s = 'UPDATE ' + (x.SourceTable ? x.SourceTable : 'Parcel') + ' SET ' + chngs +' WHERE ' + (x.AuxRowId ? ( ( x.AuxRowId > 0 ? 'ROWUID = ': 'ClientROWUID = ') + x.AuxRowId) : ('Id = ' + x.ParcelId));
				sqls.push(s);
				i--;
			}else if(x.NewValue == null && (x.Action == 'edit' || x.Action == 'AppraisalMethod')){
				var chngs = "";
				changes.filter(function(y){return (y.AuxRowId == x.AuxRowId && y.ParcelId == x.ParcelId && y.SourceTable == x.SourceTable && y.NewValue == null && (y.Action == 'edit' || y.Action == 'AppraisalMethod'))}).forEach(function(item, index){
					if(chngs.indexOf("["+item.Field+"] ") == -1){
						chngs += '[' + x.Field + '] = \'' + '' + '\','
						changes.splice(changes.indexOf(item),1);
					}
				});
				chngs = chngs.substring(0, chngs.length-1);
				var s = 'UPDATE ' + ( x.SourceTable ? x.SourceTable : 'Parcel' ) + ' SET ' + chngs +' WHERE ' + ( x.AuxRowId ? ( ( x.AuxRowId > 0 ? 'ROWUID = ' : 'ClientROWUID = ' ) + x.AuxRowId ) : ( 'Id = ' + x.ParcelId ) );
				sqls.push(s);
				i--;
			}
		}
		executeQueries(sqls, callback, function () {
				if (callback) callback();
			}, function (t, c) {
				setProgressLabel('Merging local changes ... (' + c + '/' + t + ')');
		}, true);
	});
}

function deleteTempRecords(Parcel, callback) {

    var deleteChildRecords = function ( _pRec, onFinish ) {
        if( _pRec.length == 0 ) { onFinish(); return; }
        var recd = _pRec.shift(), rec = recd.record, table = recd.sTable;
        var childTables = parentChild.filter(function (pt) { return pt.ParentTable == table })
        if (childTables.length == 0 || !childTables) { deleteChildRecords(_pRec, onFinish); }
        else {
            childTables.forEach(function (ch) {
                rec[ch.ChildTable].forEach(function (a) { 
                    a.CC_Temp_Deleted = "true"; 
                    _pRec.push( { record: a, sTable: ch.ChildTable } );
                });
            });
            deleteChildRecords( _pRec, onFinish );
        }
    }

    var getRecords = function (cat, callback) {
        var c = cat.pop();
        if (!c) { callback(Parcel); return; }
        var source = _.clone(Parcel[c.SourceTable]), b = 0;
        if (!source || (source && source.length == 0)) { getRecords(cat, callback); return; }
        
        function _updateHidden(source) {
            if(source.length == 0) { getRecords(cat, callback); return; };
            var a = source.shift();  activeParcel = Parcel;
            var t = Parcel.EvalValue(c.SourceTable + '[' + b + ']', c.HideExpression);  b = b + 1;

            if (t == true) {
                var rowuid = a["ROWUID"], _arr = [ {record: a, sTable: c.SourceTable} ];
                HiddenRecords.push( { "rowuid": rowuid, "catid": c.Id, "pid": Parcel.Id } ); 
                a.CC_Temp_Deleted = "true";
                deleteChildRecords(_arr, function () { 
                    if (!c.ParentCategoryId) c.Hidden == true;
                    _updateHidden(source);
                });
            }
            else
            	_updateHidden(source);
        }
        
        _updateHidden(source);
    }

    var childcategories = fieldCategories.filter(function (f) { return (f.HideExpression && f.HideExpression != '' && f.SourceTable && f.SourceTable != '') });

    getRecords(childcategories, function (p) { 
        var t = checkMinRecordExist(p);
        tableListGlobal.forEach(function (t) {
            if (p[t]) p[t] = p[t].filter(function (r) { return r.CC_Temp_Deleted != "true" })
        })
        if (callback) callback(t);
    });
}

function markAsDelete( pId , ssFlag){ 
    pId = pId.toString();
    var msg = 'Do you want to flag this property for deletion? No photos can be added to this parcel if it will be deleted. Existing photos will be deleted from the cloud when this parcel is QC Approved.'
    if($( '.parcel-item[parcelid="' + pId + '"] .mark-as-delete' ).hasClass('Bppdeleted') == true)
    	msg = 'Do you want to remove the deleted flag from this property ?'
    messageBox(msg, ["Yes", "Cancel"], function (){
    if($( '.parcel-item[parcelid="' + pId + '"] .mark-as-delete' ).hasClass('Bppdeleted') == true){
        db.transaction( function ( x ) {
            x.executeSql( "UPDATE PARCEL SET CC_Deleted = 'false' WHERE CC_parcelid = ?", [pId], function () {
                ccma.Sync.enqueueEvent( pId, 'recoverdeletedparcel', '', { updateData: false } );
                $('.parcel-item[parcelid="' + pId + '"] .mark-as-delete').attr('pdeleted', "false");
                $( '.parcel-item[parcelid="' + pId + '"] .mark-as-delete' ).removeClass('Bppdeleted');
                $( '.parcel-item[parcelid="' + pId + '"] .mark-as-delete' ).html('X');
                $( '.parcel-item[parcelid="' + pId + '"] .select-flag' ).removeAttr('disabled');

            } )
        } );
     }
    else{
        db.transaction( function ( x ) {
            x.executeSql( "UPDATE PARCEL SET CC_Deleted = 'true' WHERE CC_parcelid = ?", [pId], function () {
                ccma.Sync.enqueueEvent( pId, 'deleteparcel', '', { updateData: false } );
                //$( '.parcel-item[parcelid="' + pId + '"]' ).remove();
                $('.parcel-item[parcelid="' + pId + '"] .mark-as-delete').attr('pdeleted', "true");
                $( '.parcel-item[parcelid="' + pId + '"] .mark-as-delete' ).addClass('Bppdeleted');
                $( '.parcel-item[parcelid="' + pId + '"] .mark-as-delete' ).html('Deleted');
                $( '.parcel-item[parcelid="' + pId + '"] .select-flag' ).attr('disabled','disabled');
                if(ssFlag){
					if(ParcelopenfromMap){
						ParcelopenfromMap = false;
						openMapView();
						refreshSortScreen = true;
					}
					else{
                    	if (refreshSortScreen){
                    	    showSortScreen( null, false );
                    	}    
                        else {
                        	showSortScreen( null, true ); refreshSortScreen = true;
                        }
                    }
                }
            } )
        } )
   }
    }, function ( x, e )
    {
    } )
}

function createNewParcel(parentId) {
    var proceedParcelCreation = function(parentId, RPParcelId) {
        var newPId = ccTicks().toString()
        if (currentNbhd == null && Object.keys(neighborhoods).length > 0)
            currentNbhd = neighborhoods[Object.keys(neighborhoods)[0]][0].NbhdId || neighborhoods[Object.keys(neighborhoods)[0]][0].Id;
        var msg = 'Do you want to create a new property to be linked to parcel ' + RPParcelId + '?';
        if (!parentId || bppView)
            msg = 'Do you want to create a standalone business personal property?';
        messageBox(msg, ["Yes", "Cancel"], function() {
            var defaults = Object.keys(datafields).filter(function(k) {
                return datafields[k].SourceTable == null && datafields[k].DefaultValue != null
            }).map(function(k) {
                var df = datafields[k];
                return {
                    Id: df.Id,
                    Name: df.Name,
                    DefaultValue: df.DefaultValue
                }
            });
            if (!parentId || bppView) {
                db.transaction(function (x) {
                    let prt = EnableNewPriorities == 'True' == "1" ? "3" : "1";
                    x.executeSql("INSERT INTO Parcel (Id, CC_parcelid, Keyvalue1,NeighborhoodId,priority,CC_Priority,Reviewed,IsPersonalProperty,IsComparable,CC_Deleted) VALUES (?, ?, ?, ?,?, 3 , 'false', 'true' , 0,'false' )", [newPId, newPId, newPId, currentNbhd, prt], function(x, result) {
                        executeQueries(_db_parcelsDrop, function() {
                            createParcelView(function() {
                                ccma.Sync.enqueueParcelChange(newPId, currentNbhd, null, null, 'newparcel', 'newparcel', '', newPId, {
                                    updateData: false
                                });
                                getData("UPDATE NbhdStats set Priorities = Priorities + 1 WHERE Id = " + currentNbhd, [], null);
                                refreshSortScreen = true;
                                defaults.forEach(function(field) {
                                    ccma.Sync.enqueueParcelChange(newPId, null, null, 'E', field.Name, field.Id, null, field.DefaultValue, {
                                        updateData: true,
                                        source: null
                                    }, function() {}, function() {});
                                });
                                if (combinedView || ccma.Session.BPPDownloadType == '0') {
                                    ccma.UI.SortOrderType = "3";
                                    showParcel(newPId);
                                    messageBox("New standalone business personal property created succsessfully.", ["OK"], function() {});
                                } else {
                                    ccma.UI.SortOrderType = "3";
                                    messageBox("New standalone business personal property created succsessfully.");
                                }
                            })
                        })
                    }, function(x, e) {})
                });
            } else {
                var bppCount = $('nav[parcelid="' + parentId + '"]').attr('Bppcount');
                getParcelOnly(parentId, function(p) {
                    db.transaction(function (x) {
                        let prt = EnableNewPriorities == 'True' == "1" ? "3" : "1";
                        x.executeSql("INSERT INTO Parcel (Id, CC_parcelid, Keyvalue1, StreetAddress, Latitude, Longitude, StreetNumber, StreetName, StreetNameSuffix, NeighborhoodId, priority, CC_Priority, Reviewed, IsPersonalProperty, parentParcelId, IsComparable, CC_Deleted) VALUES ( ?, ?, ?, ?, ?, ?, ?, ? ,? ,? ,? ,? , 'false', 'true' , ?,0,'false' )", [newPId, newPId, newPId, p.StreetAddress, p.Latitude, p.Longitude, p.StreetNumber, p.StreetName, p.StreetNameSuffix, p.NeighborhoodId, prt, prt, p.Id], function (x, result) {
                            executeQueries(_db_parcelsDrop, function () {
                                createParcelView(function () {
                                    $('nav[parcelid="' + parentId + '"]').attr('Bppcount', parseInt(bppCount) + 1);
                                    $('span.Bppcount', 'nav[parcelid="' + parentId + '"]').html(parseInt(bppCount) + 1);
                                    getData("UPDATE NbhdStats set Priorities = Priorities + 1 WHERE Id = " + p.NeighborhoodId, [], null);
                                    refreshSortScreen = true;
                                    refreshMiniSS = true;
                                    var copyCategories = [];
                                    ccma.Sync.enqueueParcelChange(parentId, newPId, null, null, 'newparcel', 'newparcel', '', newPId, {
                                        updateData: false
                                    });
                                    defaults.forEach(function (field) {
                                        ccma.Sync.enqueueParcelChange(newPId, null, null, 'E', field.Name, field.Id, null, field.DefaultValue, {
                                            updateData: true,
                                            source: null
                                        }, function () { }, function () { });
                                    });
                                    var cats = fieldCategories.filter(function (x) {
                                        return (x['UI_CateogorySettings'] && x['UI_CateogorySettings'].CopyCategoryOnBPPCreate && x['UI_CateogorySettings'].CopyCategoryOnBPPCreate == 'true')
                                    });
                                    if (cats.length > 0) {
                                        $('.mask').show();
                                        cats.forEach(function (cat) {
                                            var exist = copyCategories.filter(function (p) {
                                                return p.SourceTable == cat.SourceTable
                                            }).length
                                            if (cat.ParentCategoryId == null && cat.SourceTable && exist == 0)
                                                copyCategories.push(cat)
                                        });
                                        var tempActiveParcel = null;
                                        if (activeParcel) tempActiveParcel = activeParcel;
                                        getParcel(newPId, function (a) {
                                            activeParcel = a;
                                            ccma.Data.Controller.CopyFormData(parentId, newPId, copyCategories, function () {
                                                activeParcel = tempActiveParcel;
                                                $('.mask').hide();
                                                ccma.UI.SortOrderType = "3";
                                            }, {
                                                type: 'BPPCopy',
                                                pID: newPId
                                            });
                                        });
                                    } else {
                                        ccma.UI.SortOrderType = "3";
                                        messageBox("New business Personal Property " + newPId + " created successfully");
                                    }
                                })
                            })
                        }, function (x, e) {});
                    });
                })
            }
        }, function() {})
    }
    var bppView = ccma.Session.BPPDownloadType == '0' ? true : false;
    if (parentId && !bppView) {
        getData("SELECT KeyValue1 FROM Parcel WHERE Id = " + parentId.toString(), [], function(res) {
            if (res && res.length == 1) {
                var RPParcelId = res[0].KeyValue1;
                proceedParcelCreation(parentId, RPParcelId);
            } else {
                proceedParcelCreation();
            }
        }, null, null, function() {
            proceedParcelCreation();
        });
    } else
        proceedParcelCreation();
}

var loadSketchSegments = function () { return activeParcel.getSketchSegments(); }

var clearVectorlabelPosition = function () { vectorLabelPositions = []; }
var calculationExpressionSketch = function (calcField, callback) {
    _PCIBatchArray = [];
  
    var update_FinalPCI = function (){
    	ccma.Sync.enqueueBulkParcelChanges( _PCIBatchArray, function() {  
			getParcel(activeParcel.Id, function () {
				_PCIBatchArray = [];
				if (callback) callback(); return;
			});
		});
    }

    var calculation_pci_creation  = function (calcField) {
        var field = calcField.shift(), sourceTable = field.SourceTable, sourceData = activeParcel[sourceTable], fName = field.Name;
        sourceData = sourceData? sourceData: [];
        
        try {
			 for( var i = 0; i < sourceData.length; i++) {
				var source = sourceTable + '[' + i + ']', sData = activeParcel[sourceTable][i];
				var v = activeParcel.Eval(source, fName);

				if (field && field.CalculationExpression && field.CalculationExpression.trim() != '' && field.CalculationExpression.search(childRecCalcPattern) > -1) {
					var tempVal = calculateChildRecords(field, i);
					v = ( (tempVal == null)? v: tempVal );
				}

				if(field && field.CalculationExpression && sourceTable && (v.Value || v.Value === 0) && v.Value.toString() != sData[fName]) 
					_update_PCIChanges(field, v.Value, sData);
			}
        }
        catch(err) {
              console.log(err.message) ;
        }
        
        if( calcField.length > 0 ) calculation_pci_creation(calcField); 
        else if( _PCIBatchArray.length > 0 ) update_FinalPCI();
        else if (callback) { callback(); return; }
    } 
       
    calculation_pci_creation(calcField);     
}

var sketchSorting = function (skRecord, stExps, stable) {
    if (!stExps || !skRecord || skRecord && skRecord.length == 0)
        return skRecord;

    let splitstExp = stExps.split(','), sortResultRecord = [];
    splitstExp.forEach((ex, i) => { if (ex.contains('ROWUID')) { splitstExp.splice(i, 1); splitstExp.unshift(ex); } });

    splitstExp.forEach((splitExp) => {
        let stExp = splitExp.trim(), stField = stExp.split(" ")[0], fld = getDataField(stField, stable),
            fldType = fld ? fld.InputType : (stField == 'ROWUID' ? '2' : null),
            stOrder = stExp.split(" ")[1] && stExp.split(" ")[1] == 'DESC' ? 'DESC' : 'ASC';

        sortResultRecord = skRecord.sort((a, b) => {
            let val1 = (a[stField] ? a[stField] : ((stOrder == 'DESC') ? -99999999 : 99999999)),
                val2 = (b[stField] ? b[stField] : ((stOrder == 'DESC') ? -99999999 : 99999999));

            if (fldType == "2" || fldType == "7" || fldType == "8" || fldType == "9") {
                val1 = val1 ? parseFloat(val1) : val1; val2 = val2 ? parseFloat(val2) : val2;
            }

            if (val1 == val2) { return 0; }
            else if ((val1 == null || val1 == '' || val1 == ' ') && val1 !== 0) { return 1; }
            else if ((val2 == null || val2 == '' || val2 == ' ') && val2 !== 0) { return -1; }
            else if (stOrder == 'DESC') { return val1 < val2 ? 1 : -1; }
            else {
                if (stField == 'ROWUID' && val2 > 0 && val1 < 0) return 0;
                else return val1 < val2 ? -1 : 1;
            }
        });
    });

    return (sortResultRecord ? sortResultRecord : []);
}

var disableAutoInsertFun = function (sourceTable, sourceRecord, autoInsert) {
    let disableInsertOnCopyRecord = false;

    if (autoInsert && autoInsert.DisableInsertOnCopyRecord && autoInsert.DisableInsertOnCopyRecord != '0') {
        if (autoInsert.DisableInsertOnCopyRecord == '1')
            disableInsertOnCopyRecord = true;
        else if (sourceRecord) {
            let idx = activeParcel[sourceTable].indexOf(sourceRecord);
            if (idx > -1) {
                try {
                    disableInsertOnCopyRecord = activeParcel.EvalValue(sourceTable + '[' + idx + ']', decodeHTML(autoInsert.DisableInsertOnCopyRecord));
                }
                catch (ex) { }
            }
        }
    }

    return disableInsertOnCopyRecord;
}
