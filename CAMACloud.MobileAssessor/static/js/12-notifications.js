﻿//    DATA CLOUD SOLUTIONS, LLC ("COMPANY") CONFIDENTIAL 
//    Unpublished Copyright (c) 2010-2013 
//    DATA CLOUD SOLUTIONS, an Ohio Limited Liability Company, All Rights Reserved.

//    NOTICE: All information contained herein is, and remains the property of COMPANY.
//    The intellectual and technical concepts contained herein are proprietary to COMPANY
//    and may be covered by U.S. and Foreign Patents, patents in process, and are protected
//    by trade secret or copyright law. Dissemination of this information or reproduction
//    of this material is strictly forbidden unless prior written permission is obtained
//    from COMPANY. Access to the source code contained herein is hereby forbidden to
//    anyone except current COMPANY employees, managers or contractors who have executed
//    Confidentiality and Non-disclosure agreements explicitly covering such access.</p>

//    The copyright notice above does not evidence any actual or intended publication
//    or disclosure of this source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
//    DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
//    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
//    IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
//    OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
//    TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
//    ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

var notify = function () {
    // check for notification compatibility
    if (!window.Notification) {
        // if browser version is unsupported, be silent
        log('Notifications unsupported.');
        return;
    }
    // log current permission level
    log(Notification.permission);
    // if the user has not been asked to grant or deny notifications from this domain
    if (Notification.permission === 'default') {
        Notification.requestPermission(function () {
            // callback this function once a permission level has been set
            notify();
        });
    }
    // if the user has granted permission for this domain to send notifications
    else if (Notification.permission === 'granted') {
        var n = new Notification(
                    '1 new friend request',
                    {
                        'body': 'Jill would like to add you as a friend',
                        // prevent duplicate notifications
                        'tag': 'unique string'
                    }
                );
        // remove the notification from Notification Center when it is clicked
        n.onclick = function () {
            this.close();
        };
        // callback function when the notification is closed
        n.onclose = function () {
            console.log('Notification closed');
        };
    }
    // if the user does not want notifications to come from this domain
    else if (Notification.permission === 'denied') {
        // be silent
        log('Notifications denied.');
        return;
    }
};