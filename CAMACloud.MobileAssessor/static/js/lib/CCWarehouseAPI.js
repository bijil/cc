var CCWarehouseAPI = function ()
{


    this.version = "0.16";
    this.onError = function () { this.logListen( "onError()" ); };
    this.onWarehouseBackground = function () { this.logListen( "onWarehouseBackground()" ); };
    this.onWarehouseForeground = function () { this.logListen( "onWarehouseForeground()" ); };
    this.onLog = null;

    this.onSketchVectorsReceiveClose = function () { this.logListen( "onSketchReceiveClose()" ); };
    this.onSketchVectorsReceiveOpen = function () { this.logListen( "onSketchReceiveOpen()" ); };
    this.onSketchMoveNode = function () { this.logListen( "onSketchMoveNode()" ); };

    this.onSketchClose = function () { this.logListen( "onSketchClose()" ); };
    this.onSketchOpen = function () { this.logListen( "onSketchOpen()" ); };
    this.onSketchMinimize = function () { this.logListen( "onSketchMinimize()" ); };
    this.onSketchMaximize = function () { this.logListen( "onSketchMaximize()" ); };
    //this.onCaptureImage = function() {this.logListen("onCaptureImage()");};
    //this.onResizeImage = function() {this.logListen("onResizeImage()");};

    this.logging = true;

    this.Callbacks = [];

    this.log( "Welcome to CCWarehouse API Version: " + this.version );
    this.logging = false;
};

CCWarehouseAPI.prototype.CaptureImage = function ( captureOptions, callback )
{
    var packet = new CCWarehouseAPI.CCWPacket();
    packet.call = 8;
    packet.data = JSON.stringify( captureOptions );

    this.Send( packet, callback );
};

CCWarehouseAPI.prototype.ResizeImage = function ( resizeOptions, callback )
{
    var packet = new CCWarehouseAPI.CCWPacket();
    packet.call = 9;

    packet.data = JSON.stringify( resizeOptions );

    this.Send( packet, callback );
};

CCWarehouseAPI.prototype.DeletePath = function ( path, callback )
{
    var packet = new CCWarehouseAPI.CCWPacket();
    packet.call = 10;

    var ioInfo = new CCWarehouseAPI.CCWIOInfo();
    ioInfo.Path = path;

    packet.data = JSON.stringify( ioInfo );

    this.Send( packet, callback );
};

CCWarehouseAPI.prototype.WriteFile = function ( path, data, callback )
{
    var packet = new CCWarehouseAPI.CCWPacket();
    packet.call = 11;

    var ioInfo = new CCWarehouseAPI.CCWIOInfo();
    ioInfo.Path = path;
    ioInfo.Data = data;

    packet.data = JSON.stringify( ioInfo );

    this.Send( packet, callback );
};

CCWarehouseAPI.prototype.ReadFile = function ( path, callback )
{
    var packet = new CCWarehouseAPI.CCWPacket();
    packet.call = 12;

    var ioInfo = new CCWarehouseAPI.CCWIOInfo();
    ioInfo.Path = path;

    packet.data = JSON.stringify( ioInfo );

    this.Send( packet, callback );
};

CCWarehouseAPI.prototype.GetFileList = function ( path, includeSubDirectories, callback )
{
    var packet = new CCWarehouseAPI.CCWPacket();
    packet.call = 13;

    var ioInfo = new CCWarehouseAPI.CCWIOInfo();
    ioInfo.Path = path;
    ioInfo.IncludeSubDirectories = includeSubDirectories;

    packet.data = JSON.stringify( ioInfo );
    this.Send( packet, callback );
};

CCWarehouseAPI.CCWPacket = function ()
{
    this.call = 0;
    this.data = "";
    this.callbackId = -1;
    this.from = "";
};

CCWarehouseAPI.CCWPacket.prototype.GenerateId = function ()
{
    this.callbackId = Math.random();
};

CCWarehouseAPI.CCWImage = function ()
{
    this.Width = -1;
    this.Height = -1;
    this.Data = "";
};

CCWarehouseAPI.CCWImageCaptureOptions = function ()
{
    this.Watermark = "";
    this.Width = -1;
    this.Height = -1;
    this.MenuX = 0;
    this.MenuY = 0;
    this.CameraOnly = true;
};

CCWarehouseAPI.CCWImageResizeOptions = function ()
{
    this.Width = -1;
    this.Height = -1;
    this.Data = "";
};

CCWarehouseAPI.CCWIOInfo = function ()
{
    this.Path = "";
    this.Data = "";
    this.IncludeSubDirectories = false;
};

CCWarehouseAPI.WSQuery = function (cmd, query, params) {
    this.cmd = cmd;
    this.query = query || "";
    this.params = params || [];
};

CCWarehouseAPI.prototype.log = function ( msg )
{
    if ( this.logging === true )
    {
        msg = '[CCWarehouseAPI] ' + msg;
        if ( this.onLog !== null )
            this.onLog( msg );
        console.log( msg );
    }
};

CCWarehouseAPI.prototype.logListen = function ( msg )
{
    this.log( "[LISTENING] " + msg );
};

CCWarehouseAPI.prototype.raiseError = function ( errorMessage )
{
    var msg = '[ERROR] ' + errorMessage;
    this.log( msg );
    if ( this.onError !== null )
        this.onError( msg );
};

CCWarehouseAPI.prototype.MAClose = function ()
{
    var packet = new CCWarehouseAPI.CCWPacket();
    packet.call = 3;
    packet.data = "close";

    this.Send( packet );
};

CCWarehouseAPI.prototype.ReOpenMA = function () {
    var packet = new CCWarehouseAPI.CCWPacket();
    packet.call = 3;
    packet.data = "reopenma";

    this.Send(packet);
};

CCWarehouseAPI.prototype.SketchOpen = function ()
{
    var packet = new CCWarehouseAPI.CCWPacket();
    packet.call = 4;
    packet.data = "open";

    this.Send( packet );
};

CCWarehouseAPI.prototype.SketchClose = function ()
{
    var packet = new CCWarehouseAPI.CCWPacket();
    packet.call = 4;
    packet.data = "close";

    this.Send( packet );
};

CCWarehouseAPI.prototype.SketchClear = function ()
{
    var packet = new CCWarehouseAPI.CCWPacket();
    packet.call = 4;
    packet.data = "clear";

    this.Send( packet );
};

CCWarehouseAPI.prototype.SketchMinimize = function ()
{
    var packet = new CCWarehouseAPI.CCWPacket();
    packet.call = 4;
    packet.data = "mini";

    this.Send( packet );
};

CCWarehouseAPI.prototype.SketchMaximize = function ()
{
    var packet = new CCWarehouseAPI.CCWPacket();
    packet.call = 4;
    packet.data = "max";

    this.Send( packet );
};

CCWarehouseAPI.prototype.OpenDatabase = function (dbName, callback)
{
    var q = new CCWarehouseAPI.WSQuery('connect', dbName);
    this.Post(14, q, callback);
};

CCWarehouseAPI.prototype.ExecuteSql = function (query, parameters, callback)
{
    var q = new CCWarehouseAPI.WSQuery('query', query, parameters);
    this.Post(14, q, callback);
};

CCWarehouseAPI.prototype.CleanUp = function (query, parameters, callback) {
    this.Post(99, "", callback);
};

CCWarehouseAPI.prototype.CallbackExists = function ( callbackId )
{
    return ( typeof CCWarehouse.Callbacks[callbackId] !== 'undefined' );
};

CCWarehouseAPI.prototype.Post = function (call, data, callback) {
    var packet = new CCWarehouseAPI.CCWPacket();
    packet.call = call;
    packet.data = (typeof data == "object") ? JSON.stringify(data) : data;
    this.Send(packet, callback);
}


CCWarehouseAPI.prototype.Send = function ( CCWPacket, callback )
{
    CCWPacket.from = "client";

    if ( typeof callback !== 'undefined' )
    {
        var done = false;
        while ( done === false )
        {

            CCWPacket.GenerateId();

            if ( this.CallbackExists( CCWPacket.callbackId ) === false )
            {
                this.Callbacks[CCWPacket.callbackId] = callback;
                done = true;
            }

        }
    }

    var data = JSON.stringify( CCWPacket );
    this.log( "[REQUEST] " + data );
    window.webkit.messageHandlers.CCWarehouse.postMessage( data );
};

CCWarehouseAPI.prototype.ReceivedMessage = function ( packet )
{
    //var packet = JSON.parse(json);
    packet.from = "server";

    var json = JSON.stringify( packet );
    this.log( '[RESPONSE] ' + json );


    var runCallback = function ( api, data, errorMessage )
    {
        if ( api.CallbackExists( packet.callbackId ) )
        {
            api.Callbacks[packet.callbackId]( data, errorMessage );
            delete api.Callbacks[packet.callbackId];
        }
    };

    var call = packet.call;
    switch ( call )
    {
        case 0:
            this.log( "[PING] Server Time: " + packet.data );
            break;
        case 1:

            switch ( packet.data )
            {
                case "back":
                    if ( this.onWarehouseBackground !== null )
                        this.onWarehouseBackground();
                    break;

                case "fore":

                    if ( this.onWarehouseForeground !== null )
                        this.onWarehouseForeground();
                    break;

                default:
                    this.raiseError( "Unkown app state: " + packet.data );
            }
            break;

        case 2:
            this.raiseError( "[API] " + packet.data );

            runCallback( this, null, packet.data );

            break;

        case 3:
            switch ( packet.data )
            {
                case "open":
                    if ( this.onMAOpen !== null )
                        this.onMAOpen();
                    break;

                case "close":

                    if ( this.onMAClose !== null )
                        this.onMAClose();
                    break;

                default:
                    this.raiseError( "Unkown app state: " + packet.data );
            }
            break;

        case 4:

            switch ( packet.data )
            {
                case "close":
                    if ( this.onSketchClose !== null )
                        this.onSketchClose();
                    break;

                case "open":

                    if ( this.onSketchOpen !== null )
                        this.onSketchOpen();
                    break;

                case "mini":

                    if ( this.onSketchMinimize !== null )
                        this.onSketchMinimize();
                    break;

                case "max":

                    if ( this.onSketchMaximize !== null )
                        this.onSketchMaximize();
                    break;

                default:
                    this.raiseError( "Unkown app state: " + packet.data );
            }

            break;

        case 5:
            if ( this.onSketchVectorsReceiveClose !== null )
                this.onSketchVectorsReceiveClose( packet.data );
            break;

        case 6:
            if ( this.onSketchMoveNode !== null )
                this.onSketchMoveNode( packet.data );
            break;

        case 7:
            if ( this.onSketchVectorsReceiveOpen !== null )
                this.onSketchVectorsReceiveOpen( packet.data );
            break;

        case 8:
            var captured_image = JSON.parse( packet.data );
            /*if(this.onCaptureImage !== null)
                this.onCaptureImage(captured_image);*/


            runCallback( this, captured_image );

            break;

        case 9:
            var resized_image = JSON.parse( packet.data );
            /*if(this.onResizeImage !== null)
                this.onResizeImage(resized_image);*/


            runCallback( this, resized_image );

            break;

        case 10:

            runCallback( this, JSON.parse( packet.data ) );

            break;

        case 11:

            runCallback( this, JSON.parse( packet.data ) );

            break;

        case 12:

            runCallback( this, JSON.parse( packet.data ) );

            break;

        case 13:

            runCallback( this, JSON.parse( packet.data ) );

            break;
		case 14:
			runCallback( this, JSON.parse(packet.data) );
            break;


        case 99:
            runCallback(this, JSON.parse(packet.data));
            break;
        default:
            this.raiseError( "Unknown call: " + call );
    }


};
