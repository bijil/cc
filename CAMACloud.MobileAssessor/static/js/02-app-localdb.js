﻿//    DATA CLOUD SOLUTIONS, LLC ("COMPANY") CONFIDENTIAL 
//    Unpublished Copyright (c) 2010-2013 
//    DATA CLOUD SOLUTIONS, an Ohio Limited Liability Company, All Rights Reserved.

//    NOTICE: All information contained herein is, and remains the property of COMPANY.
//    The intellectual and technical concepts contained herein are proprietary to COMPANY
//    and may be covered by U.S. and Foreign Patents, patents in process, and are protected
//    by trade secret or copyright law. Dissemination of this information or reproduction
//    of this material is strictly forbidden unless prior written permission is obtained
//    from COMPANY. Access to the source code contained herein is hereby forbidden to
//    anyone except current COMPANY employees, managers or contractors who have executed
//    Confidentiality and Non-disclosure agreements explicitly covering such access.</p>

//    The copyright notice above does not evidence any actual or intended publication
//    or disclosure of this source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
//    DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
//    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
//    IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
//    OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
//    TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
//    ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.


var db;
var todoDB = {};
var aggregate_fields = [];
let ldbname, db_conn_error;
var indxDB;
var errorInWebSql = false, errorInIDB = false, clearDB = false, preventSchemaUpdate = false;
var _db_create = {
    Images: "CREATE TABLE IF NOT EXISTS Images (Id, ParcelId, Image, Path, Type, LocalId, Accepted, Synced, IsPrimary, MetaData1, MetaData2, MetaData3, MetaData4, MetaData5, MetaData6, MetaData7, MetaData8, MetaData9, MetaData10,UploadTime,DownSynced,SmallImage);",
    Comparables: "CREATE TABLE IF NOT EXISTS Comparables (Id, ParcelId, C1, C2, C3, C4, C5);",
    ParcelChanges: "CREATE TABLE IF NOT EXISTS ParcelChanges (ParcelId, AuxRowId, ParentAuxRowId, Action, Field, FieldId, OldValue, NewValue, ChangedTime, Latitude, Longitude, Synced);",
    CompletedParcels: "CREATE TABLE IF NOT EXISTS CompletedParcels (ParcelId, SelectedAppraisalType, ChangedTime, Latitude, Longitude, Synced);",
    Location: "CREATE TABLE IF NOT EXISTS Location (UpdateTime, Latitude, Longitude, Accuracy, Synced);",
    ClientValidation: "CREATE TABLE IF NOT EXISTS ClientValidation (Name, Condition, Enabled, SourceTable, ErrorMessage, Ordinal);",
    LookupValue: "CREATE TABLE IF NOT EXISTS LookupValue (Id, Source, Value, Name, Description, Ordinal, NumericValue);",
    StreetMapPoints: "CREATE TABLE IF NOT EXISTS StreetMapPoints(streetid,Name,Points);",
    UI_HeatMap: "CREATE TABLE IF NOT EXISTS UI_HeatMap(Id,FieldId ,ComparisonType,IsAggregate);",
    UI_HeatMapLookup: "CREATE TABLE IF NOT EXISTS UI_HeatMapLookup(RowUID,HeatMapID,Value1,Value2 ,ColorCode);",
    AggregateFieldSettings: "CREATE TABLE IF NOT EXISTS AggregateFieldSettings(RowUID,TableName,FieldName,FunctionName,IncludeInHeatMap);",
    FieldSettings: "CREATE TABLE IF NOT EXISTS FieldSettings(Id,FieldId,PropertyName,Value);",
    CategorySettings: "CREATE TABLE IF NOT EXISTS CategorySettings(Id,CategoryId,PropertyName,Value);"
}
var _db_drop = {
    Location: "DROP TABLE IF EXISTS Location;",
    ParcelChanges: "DROP TABLE IF EXISTS ParcelChanges;",
    SubjectParcelsView: "DROP VIEW IF EXISTS SubjectParcels;",
    PendingParcelsView: "DROP VIEW IF EXISTS PendingParcels;",
    AggregateFieldSettings: "DROP VIEW IF EXISTS AggregateFields;",
    FYLTableSettings: "DROP VIEW IF EXISTS FYLTable;",
    EmptyImages: "DELETE FROM Images WHERE Image = ''"
}


var _db_adjustments = [
    //'ALTER TABLE Images ADD SmallImage TEXT',
    //'ALTER TABLE Images ADD UploadTime TEXT',
    //'ALTER TABLE Images ADD DownSynced TEXT',
    //'ALTER TABLE LookupValue ADD NumericValue TEXT',
    //'ALTER TABLE AggregateFieldSettings ADD IncludeInHeatMap TEXT',
    //'ALTER TABLE FieldCategory ADD DoNotDeleteOnHide TEXT',
    //'ALTER TABLE Parcel ADD ParentParcelID TEXT',
    //'ALTER TABLE Parcel ADD IsPersonalProperty TEXT',
    //'ALTER TABLE FieldCategory ADD Type TEXT',
    //'ALTER TABLE Parcel ADD IsParentParcel  TEXT'
];


var _db_check = {
    ParcelChangesCompatibility: "SELECT ParentAuxRowId FROM ParcelChanges LIMIT 1",
    FieldCompatibility: "SELECT SourceTable, AssignedName FROM Field LIMIT 1",
    AllTextFieldsCompatibility: "SELECT COUNT(*) FROM FieldCategory WHERE ShowCategory = 1"
}

var _db_comp_ParcelChanges = [_db_drop.ParcelChanges, _db_create.ParcelChanges]
var _db_comp_FieldCategory = ['DROP TABLE IF EXISTS FieldCategory', 'DROP TABLE IF EXISTS Field']

var _db_proc_changeLogin = [
    "DROP TABLE IF EXISTS MapPoints",
    "DROP TABLE IF EXISTS Images",
    "DROP TABLE IF EXISTS Parcel",
    "DELETE FROM ParcelChanges"
]
var _db_parcelsDrop = [
    _db_drop.FYLTableSettings,
    _db_drop.SubjectParcelsView,
    _db_drop.PendingParcelsView,
    _db_drop.AggregateFieldSettings
];

function openWebSqlliteDatabase(lDBName, ocallback) {
    initIndexDbSqllite(lDBName, (recData) => {
        db = recData ? new SQL.Database(recData) : new SQL.Database();
        db.transaction = function (callback) {
            var tx = {
                executeSql: function (query, params, success, failure) {
                    var _tx = this;
                    var resp = {
                        rows: {
                            length: 0,
                            item: function (i) {
                                return this[i];
                            }
                        }
                    };
                    try {
                        if (/^\s*SELECT\b/i.test(query) && params?.length > 0) {
                            let stmt = db.prepare(query, params)
                            resp = {
                                rows: {
                                    length: 0,
                                    item: function (i) {
                                        return this[i];
                                    }
                                }
                            };
                            let cn = 0;
                            while (stmt.step()) {
                                let row = stmt.getAsObject();
                                resp.rows[cn] = row;
                                cn = cn + 1;
                            }
                            resp.rows.length = cn;
                        }
                        else if (params?.length == 0) {
                            var raw = db.exec(query)[0];
                            if (raw) {
                                var values = raw.values;
                                resp = {
                                    rows: {
                                        length: values.length,
                                        item: function (i) {
                                            return this[i];
                                        }
                                    }
                                };

                                for (var i = 0; i < values.length; i++) {
                                    resp.rows[i] = {};
                                    var k = 0;
                                    for (var c of raw.columns) {
                                        resp.rows[i][c] = values[i][k];
                                        k++;
                                    }
                                }

                            }
                        }
                        else {
                            db.run(query, params);
                        }
                    }
                    catch (ex) {
                        failure && failure(_tx, ex);
                        return;
                    }
                    setTimeout(function () { success && success(_tx, resp); }, 10);
                }
            }

            callback && callback(tx);
        }

        if (ocallback) ocallback(db);
    });
}

function openWinSqlliteDatabase(dbName, callback, failedCallback) {

    var db = new (function WinWebSqllite(dbName) {
        var _db = this;
        var _dbName = dbName;
        var _connected = false;

        Object.defineProperties(this, {
            name: {
                value: _dbName
            }
        })

        window.chrome.webview.hostObjects.ExternalObject.InitSqlLiteDb(_dbName).then(result => {
            if (result == "Success") {
                _connected = true;
                if (callback) callback(_db);
            }
            else {
                failedCallback && failedCallback(result);
            }
        }).catch(error => {
            failedCallback && failedCallback(error.message);
        });

        this.transaction = function (callback) {
            var tx = {
                executeSql: function (query, params, success, failure) {
                    var _tx = this;

                    window.chrome.webview.hostObjects.ExternalObject.ExecuteQuery(query, params).then(result => {
                        if (result.contains("Error occurred in SQLite query execution")) {
                            failure && failure(_tx, { message: result });
                            return;
                        }

                        result = JSON.parse(result);
                        var resp = {
                            rows: {
                                length: result.length,
                                item: function (i) {
                                    return this[i];
                                }
                            }
                        };

                        for (var i = 0; i < result.length; i++) {
                            resp.rows[i] = result[i];
                        }

                        success && success(_tx, resp);
                    }).catch(error => {
                        failure && failure(_tx, error);
                    });
                }
            }

            callback && callback(tx);
        }

    })(dbName)

}

function initIndexDbSqllite(dbName, callback) {
    let sqldbName = dbName + '_sqllite';
    indxDB = indexedDB.open(sqldbName, 1);

    indxDB.onupgradeneeded = function (event) {
        let dbx = indxDB.db = event.target.result;

        if (!dbx.objectStoreNames.contains('sqliteData')) {
            let objectStore = dbx.createObjectStore('sqliteData', { keyPath: 'id', autoIncrement: true });
        }
    };

    indxDB.onsuccess = function (event) {
        let dbx = indxDB.db = event.target.result;

        if (dbx.objectStoreNames.contains('sqliteData')) {
            let transaction = dbx.transaction(['sqliteData'], 'readonly');
            let objectStore = transaction.objectStore('sqliteData');

            objectStore.openCursor().onsuccess = function (event) {
                let cursor = event.target.result;
                if (cursor?.value) {
                    if (callback) callback(cursor?.value?.data);
                }
                else {
                    if (callback) callback();
                }
            };
        }
        else {
            if (callback) callback();
        }
    };

    indxDB.onerror = function (event) {
        if (callback) callback();
    };
}

function getFirstIndexedSqlLiteId(objectStore, callback) {
    objectStore.openCursor().onsuccess = function (event) {
        let cursor = event.target.result;
        let firstRowId = cursor ? cursor.primaryKey : null;
        if (callback) callback(firstRowId);
    };
}

function clearSqlLiteIndexDB(ccallback) {
    if (ccma.Environment.ExternalWebSql || typeof (openDatabase) != 'undefined' || (typeof (WinWrapperVersion) != 'undefined' && window?.chrome?.webview)) {
        if (ccallback) ccallback();
    }
    else {
        var p1 = performance.now();
        if (indxDB && ldbname) {
            var req = indexedDB.deleteDatabase(ldbname + "_sqllite");
            indxDB.db.close();
            req.onsuccess = function () {
                console.log('dropped sqllite db within ' + Math.round((performance.now() - p1)));
                if (ccallback) ccallback();
            }
            req.onblocked = function (e) {
                if (ccallback) ccallback();
                console.log('sqllite db drop blocked: ');
            }
        } else if (ccallback) ccallback();
    }
}

function updateSqlLiteDataInIndexedDB(callback) {
    if (!ccma.Environment.ExternalWebSql && typeof (openDatabase) == 'undefined' && typeof (WinWrapperVersion) == 'undefined' && !window?.chrome?.webview) {
        let transaction = indxDB.db.transaction(['sqliteData'], 'readwrite');
        let objectStore = transaction.objectStore('sqliteData');
        let exportedData = db.export();
        let newUint8Array = new Uint8Array(exportedData);
        getFirstIndexedSqlLiteId(objectStore, (ixId) => {
            if (ixId) {
                let putRequest = objectStore.put({ id: ixId, data: newUint8Array });
                putRequest.onsuccess = function () {
                    if (callback) callback();
                };
                putRequest.onerror = function (event) {
                    console.error(`Error updating data in sqliteData object store:`, event.target.error);
                    if (callback) callback();
                };
            }
            else {
                objectStore.add({ data: newUint8Array });
                if (callback) callback();
            }
        });
    }
    else if (callback) callback();
}

function openWebSqlDatabase(dbName, callback, failedCallback) {
    if (typeof (WinWrapperVersion) != 'undefined' && window?.chrome?.webview) {
        openWinSqlliteDatabase(dbName, (_sqDb) => {
            if (callback) callback(_sqDb);
        }, (ex) => {
            console.log(ex);
            failedCallback && failedCallback();
        });
    }
    else if (typeof (openDatabase) != 'function') {
        openWebSqlliteDatabase(dbName, (_sqDb) => {
            if (callback) callback(_sqDb);
        });       
    }
    else {
        try {
            var dbSize = 49;
            if (ccma.Environment.OS == 'iPad')
                if (!navigator.standalone)
                    dbSize = 50;
            var _db = openDatabase(dbName, '1.0', 'CAMA - MobileAssessor', dbSize * 1024 * 1024);
            if (callback) callback(_db);
        } catch (ex) {
            console.log(ex);
            failedCallback && failedCallback();
        }
    }
}

function openWHDatabase(dbName, callback, failedCallback) {

    var db = new (function WHWebSql(dbName) {
        var _db = this;
        var _dbName = dbName;
        var _connected = false;

        Object.defineProperties(this, {
            name: {
                value: _dbName
            }
        })

        CCWarehouse.OpenDatabase(_dbName + ".db", function (d) {
            if (d.success) {
                _connected = true;
                if (callback) callback(_db);
            } else {
                failedCallback && failedCallback(d.message, d.errorCode);
            }
        });

        this.transaction = function (callback) {
            var tx = {
                executeSql: function (query, params, success, failure) {
                    var _tx = this;
                    CCWarehouse.ExecuteSql(query, params, function (wd) {
                        if (wd.success) {
                            var raw = wd.data;
                            var resp = {
                                rows: {
                                    length: raw.length,
                                    item: function (i) {
                                        return this[i];
                                    }
                                }
                            };
                            for (var i = 0; i < raw.length; i++) {
                                resp.rows[i] = {};
								for(var c of wd.columns){
									resp.rows[i][c] = raw[i][c];
								}
                            }
                            success && success(_tx, resp);
                        } else {
                            failure && failure(_tx, wd.message);
                        }
                    })

                }
            }

            callback && callback(tx);
        }

    })(dbName)

    return db;

}

//switching call to use browser feature or external feature
function openDB(dbName, callback, failedCallback) {
	if(localStorage.getItem('iOSVersion') && parseInt(localStorage.getItem('iOSVersion').split('.')[0])>=13) {
		ccma.Environment.ExternalWebSql=true;
	}
    var dbOpener = (ccma.Environment.ExternalWebSql) ? openWHDatabase : openWebSqlDatabase;
    dbOpener(dbName, callback, failedCallback);
}

function initLocalDatabase(callback) {

    var uname = ccma.Session.User;
    if (uname == "" || !uname) {
        hideSplash();
        LogoutErrorInsert('sl: logout182');
        showLogin();
        return;
    }
    ldbname = uname ? (localDBName + '_' + uname.replace(/\s/g, '').replace(/-/g, '_')) : localDBName;
    if (localStorage.getItem('dbName')) ldbname = localStorage.getItem('dbName');

    openDB(ldbname, function (dbx) {
        localStorage.setItem('dbName', ldbname);
        db = dbx;


        var _db_essentials = [
            _db_create.ParcelChanges,
            _db_create.CompletedParcels,
            _db_drop.Location,
            _db_create.Location,
            _db_create.Images,
            _db_create.ClientValidation,
            _db_create.LookupValue,
            _db_create.StreetMapPoints,
            _db_create.UI_HeatMap,
            _db_create.UI_HeatMapLookup,
            _db_create.AggregateFieldSettings,
            _db_create.FieldSettings,
            _db_create.CategorySettings,
            _db_drop.SubjectParcelsView,
            _db_drop.PendingParcelsView,
            _db_drop.AggregateFieldSettings,
            _db_drop.FYLTableSettings
        ];

        var initIDB = function () {
            initLocalIDBdatabase(ldbname, function () {
                if (!todoDB.indexedDB.db.objectStoreNames.contains('Images') || !todoDB.indexedDB.db.objectStoreNames.contains('UnsyncedImages'))
                    todoDB.indexedDB.createTable(function () {
                        console.log('Image table created');
                        if (callback) callback();
                    })
                else if (callback) callback();
            });
        }

        executeQueries(_db_essentials, function () {
            checkCompatibility(function () {
                executeQueries(_db_adjustments, initIDB, null, null, true);
            });
        }, null, null, true);


    }, function (e) {
        LogoutErrorInsert("page: initLocalDatabase");
        Logout();
        messageBox("Error occurred while connecting to the database '" + ldbname + "'. You have been logged out.");
        console.error(e);
        db_conn_error = (e ? (e + '\n') : '') + logCurrentStack();
    });

}

function initLocalIDBdatabase(dbName, callback, errorCallback) {
    var dbVersion = 1.0;
    var tblIndexes = {
        Images: "Id,ParcelId,Image,Path,Type,LocalId,Accepted,Synced,IsPrimary,MetaData1,MetaData2,MetaData3,MetaData4,MetaData5,MetaData6,MetaData7,MetaData8,MetaData9,MetaData10,UploadTime,DownSynced,SmallImage".split(','),
        UnsyncedImages: ['LocalId', 'ParcelId', 'NbhdId', 'Synced', 'Type']
    }
    var indexedDB = window.indexedDB;

    todoDB.indexedDB = {};
    todoDB.indexedDB.db = null;
    todoDB.indexedDB.objectStores = ['Images', 'UnsyncedImages'];
    todoDB.indexedDB.onerror = function (e) {
        console.log(e);
    };

    todoDB.indexedDB.open = function () {
        var request = indexedDB.open(dbName);
        this.req = request;
        request.onsuccess = function (e) {
            todoDB.indexedDB.db = e.target.result;
            if (callback) callback();
        }
        request.onupgradeneeded = function (e) {
            todoDB.indexedDB.db = e.target.result;
            todoDB.indexedDB.objectStores.forEach(function (tbl) {
                if (!todoDB.indexedDB.db.objectStoreNames.contains(tbl)) {
                    var objStore = todoDB.indexedDB.db.createObjectStore(tbl, { keyPath: tbl + 'Key', autoIncrement: true });
                    tblIndexes[tbl].forEach(function (index) {
                        objStore.createIndex(index.trim(), index.trim().replace(/[#|$]/g, ''), { unique: false, multiEntry: true });
                    });
                }
            })
        }
        request.onfailure = todoDB.indexedDB.onerror;
        request.onerror = function (e) {
            console.error("An error occured while opening the db: ", e);
            if (errorCallback) errorCallback();
        }
    };
    todoDB.indexedDB.open();

    todoDB.indexedDB.createTable = function (callback) {
        var recreateDB = false, missingIndex = { Images: [], UnsyncedImages: [] }, checkedCount = 0;
        var createProcessor = function () {
            if (!recreateDB) { callback(); return; }
            var version = todoDB.indexedDB.db.version;
            todoDB.indexedDB.db.close();
            var request1 = indexedDB.open(todoDB.indexedDB.db.name, ++version);
            request1.onupgradeneeded = function (e) {
                todoDB.indexedDB.db = e.target.result;
                todoDB.indexedDB.objectStores.forEach(function (tbl) {
                    if (!todoDB.indexedDB.db.objectStoreNames.contains(tbl)) {
                        var objStore = todoDB.indexedDB.db.createObjectStore(tbl, { keyPath: tbl + 'Key', autoIncrement: true });
                        tblIndexes[tbl].forEach(function (index) {
                            objStore.createIndex(index, index, { unique: false, multiEntry: true });
                        });
                    }
                })
                if (callback) callback();
            }
        }
        todoDB.indexedDB.objectStores.forEach(function (tbl) {
            if (todoDB.indexedDB.db.objectStoreNames.contains(tbl)) {
                getTransaction([tbl], false, function (tx) {
                    var imgObjStore = tx.objectStore(tbl);
                    tblIndexes[tbl].forEach(function (imgIdx) {
                        if (!imgObjStore.indexNames.contains(imgIdx)) {
                            recreateDB = true;
                        }
                    })
                    checkedCount++;
                    if (checkedCount == 2) createProcessor();
                })
            } else { recreateDB = true; checkedCount++; if (checkedCount == 2) createProcessor(); }
        })
    }

    todoDB.indexedDB.Insert = function (tableName, Data, callback, otherData, errorCallback) {
        getTransaction([tableName], false, function (tx) {
            var decimalColumns = [];
            if (tableName == 'Images')
                decimalColumns = "Id,ParcelId,Type,Accepted,Synced,IsPrimary,DownSynced".split(',');
            var objectStore = tx.objectStore(tableName), rec, count = Data.length;
            var insert = function (data, callback) {
                if (data.length == 0) { }
                else {
                    rec = data.pop();
                    if (tableName == 'Images') {
                        tempIndex = tblIndexes['Images'].filter(function (idx) { return Object.keys(rec).indexOf(idx) == -1; }).map(function (tid) { var t = {}; t[tid] = null; return t; });
                        tempIndex.forEach(function (tIndex) { rec = $.extend(rec, tIndex); });
                    }
                    if (rec[tableName + 'Key'] !== undefined)
                        delete rec[tableName + 'Key'];
                    var t = {};
                    for (var key in rec) {
                        if (decimalColumns.indexOf(key) > -1)
                            t[key] = (!isNaN(parseInt(rec[key])) ? parseInt(rec[key]) : rec[key])
                        else
                            t[key] = rec[key]
                    }
                    var req = objectStore.add(t);
                    req.onsuccess = function () { insert(data, callback); }
                    req.onerror = function (e) { console.error('An error occured while inserting to the indexDb: ', req.error); console.log(e.target.error); }
                }
            }
            insert(Data, callback);
            tx.oncomplete = function () {
                if (callback) callback('success', otherData);
            }
        });
    }

    todoDB.indexedDB.update = function (tableName, setValues, indexes, operators, values, idxJoiners, callback, otherData, errorCallback) {
        IDB_getData(tableName, indexes, operators, values, idxJoiners, function (records) {
            var updateRecord = function () {
                if (records.length == 0) { if (callback) callback(otherData); return; }
                var record = records.pop();
                var key = IDBKeyRange.only(record[tableName + 'Key'])
                getTransaction([tableName], false, function (tx) {
                    var index = tx.objectStore(tableName);
                    index.openCursor(key).onsuccess = function (evt) {
                        var cursor = evt.target.result;
                        var data = cursor.value;
                        setValues.forEach(function (set) {
                            data[set.name] = set.value;
                        });
                        var req = cursor.update(data);
                        req.onsuccess = function () { }
                        req.onerror = function (e) { console.error('An error occured while updating to the indexDb: ', req.error); console.log(e.target.error); }
                    }
                    tx.oncomplete = function () {
                        updateRecord();
                    }
                });
            }
            updateRecord();
        });
    }

    todoDB.indexedDB.deleteRecord = function (tableName, indexes, operators, values, idxJoiners, callback, otherData, errorCallback) {
        IDB_getData(tableName, indexes, operators, values, idxJoiners, function (records) {
            var deleteRecord = function () {
                if (records.length == 0) { if (callback) callback(otherData); return; }
                var record = records.pop();
                var key = IDBKeyRange.only(record[tableName + 'Key'])
                getTransaction([tableName], false, function (tx) {
                    var del = tx.objectStore(tableName).delete(key);
                    del.onsuccess = function (evt) {
                        console.log('deleted');
                    }
                    del.onerror = function (e) { console.error('An error occured while deleting from the indexDb: ', del.error); console.log(e.target.error); }
                    tx.oncomplete = function () {
                        deleteRecord();
                    }
                });
            }
            deleteRecord();
        });
    }

}

function executeQueries(queries, callback, failedCallback, progressCallback, proceedWithFailed) {
    var total = 0, completed = 0;
    var _exec = function (sql, callback) {
        db.transaction(function (tx) {
            var query = sql, data = [];
            if (sql.query) { query = sql.query; data = sql.data; };
            tx.executeSql(query, data, function (tx1, res) {
                completed += 1;
                if (progressCallback) progressCallback(total, completed);
                if (callback) callback();
            }, function (x, e) {
                console.warn(sql);
                console.error(e);
                if (proceedWithFailed) _recExec(list, callback);
                else if (failedCallback) failedCallback();
            });
        });
    }

    var _recExec = function (list, callback) {
        if (list.length == 0) {
            if (callback) callback();
        } else {
            var first = list.shift();
            _exec(first, function () {
                _recExec(list, callback);
            });
        }
    }

    var list = queries.join ? queries : [queries];
    total = list.length;
    _recExec(list, callback);
}


function processQueue(list, processMethod, callback, failedCallback, progressCallback, proceedWithFailed) {
    var total = 0, completed = 0;
    var _exec = function (d, callback) {
        if (processMethod) {
            processMethod(d, function () {
                completed += 1;
                if (progressCallback) progressCallback(total, completed);
                if (callback) callback();
            }, function (e) {
                console.error(e);
                if (proceedWithFailed) _recExec(list, callback);
                else if (failedCallback) failedCallback();
            });
        } else {
            callback && callback();
        }
    }

    var _recExec = function (list, callback) {
        if (list.length == 0) {
            if (callback) callback();
        } else {
            var first = list.shift();
            _exec(first, function () {
                _recExec(list, callback);
            });
        }
    }

    list = list || [];
    total = list.length;
    _recExec(list, callback);
}

function checkIfImageTableExists(callback) {
    executeQueries(_db_create.Images, callback);
}

function checkIfComparablesTableExists(callback) {
    executeQueries(_db_create.Comparables, callback);
}

function createAggregateFields(callback, fetchCS) {
    getData("select * from AggregateFieldSettings order by tablename,FunctionName='FIRST', FunctionName='LAST'", null, function (list) {
        aggregate_fields = list;
        var tables = '(select parcel.id from parcel) parcel'; var fields = 'p.Id';
        var fieldselect = '', innerQuery = '', count = 0, convertionquery = '';
        list.forEach(function (a) {
            a.Condition = a && a.Condition && a.Condition.replace('&lt;', '<').replace('&gt;', '>');
            if (!a.TableName || a.TableName == '' || !a.FunctionName || !a.FieldName || (a.FunctionName == 'MEDIAN' && !iPad)) return
            convertionquery = (a.FunctionName == 'MIN' || a.FunctionName == 'MAX' || a.FunctionName == 'SUM' || a.FunctionName == 'AVG' || a.FunctionName == 'MEDIAN') ? convertionquery = '* 1' : '';
            if (count >= 0 || innerQuery == '' || (a.UniqueName != null && a.UniqueName != '')) innerQuery += ' LEFT OUTER JOIN( SELECT cc_parcelid '
            if (a.UniqueName != null && a.UniqueName != '')
                fieldselect += ',' + a.UniqueName
            else
                fieldselect += ',' + a.FunctionName + '_' + a.TableName + '_' + a.FieldName;
            if (a.FunctionName == 'FIRST' || a.FunctionName == 'LAST') {
                if (a.UniqueName != null && a.UniqueName != '') {
                    innerQuery += ' , ' + '(' + a.TableName + '.' + a.FieldName + ')' + ' as ' + a.UniqueName;
                    innerQuery += ' from ' + a.TableName + (a.Condition ? (fylEnabled ? (showFutureDataInSortscreen ? " WHERE  CC_YearStatus = 'F' AND " + a.Condition + "" : " WHERE ( CC_YearStatus ='A' OR CC_YearStatus IS NULL) AND " + a.Condition + "") : a.Condition ? " WHERE " + a.Condition + "" : '') : fylEnabled ? (showFutureDataInSortscreen ? " WHERE  CC_YearStatus = 'F' " : " WHERE ( CC_YearStatus ='A' OR CC_YearStatus IS NULL) ") : '') + ' group by cc_parcelid) ' + a.UniqueName + ' ON parcel.Id = ' + a.UniqueName + '.cc_parcelid'
                }
                else {
                    innerQuery += ' , ' + '(' + a.TableName + '.' + a.FieldName + ')' + ' as ' + a.FunctionName + '_' + a.TableName + '_' + a.FieldName;
                    innerQuery += ' from ' + a.TableName + (a.Condition ? (fylEnabled ? (showFutureDataInSortscreen ? " WHERE  CC_YearStatus = 'F' AND " + a.Condition + "" : " WHERE ( CC_YearStatus ='A' OR CC_YearStatus IS NULL) AND " + a.Condition + "") : a.Condition ? " WHERE " + a.Condition + "" : '') : fylEnabled ? (showFutureDataInSortscreen ? " WHERE  CC_YearStatus = 'F' " : " WHERE ( CC_YearStatus ='A' OR CC_YearStatus IS NULL) ") : '') + ' group by cc_parcelid) ' + a.TableName + '_' + a.FieldName + '_' + a.FunctionName + ' ON parcel.Id = ' + a.TableName + '_' + a.FieldName + '_' + a.FunctionName + '.cc_parcelid'
                }
                count = 0;
            }
            else if (list.filter(function (l) { return (l.TableName == a.TableName && l.FunctionName != 'FIRST' && l.FunctionName != 'LAST') }).length == count + 1) {
                if (a.UniqueName != null && a.UniqueName != '') {
                    innerQuery += ' , ' + (a.FunctionName == 'MEDIAN' ? 'AVG' : a.FunctionName) + '(' + (a.FunctionName == 'COUNT' ? '*' : (a.FunctionName == 'MEDIAN' ? ('Median_Value') : a.TableName + '.' + a.FieldName) + convertionquery) + ')' + ' as ' + a.UniqueName;

                    if (a.FunctionName == 'MEDIAN')
                        innerQuery += ' FROM ( SELECT cc_parcelid,' + a.TableName + '.' + a.FieldName + ' AS Median_Value, COUNT(*) OVER (PARTITION  BY cc_parcelid) AS c, ROW_NUMBER() OVER ( PARTITION  BY cc_parcelid ORDER BY CAST( ' + a.TableName + '.' + a.FieldName + ' AS NUMERIC ) ) AS rn FROM ' + a.TableName + (a.Condition ? (fylEnabled ? (showFutureDataInSortscreen ? " WHERE  CC_YearStatus = 'F' AND " + a.Condition + "" : " WHERE ( CC_YearStatus ='A' OR CC_YearStatus IS NULL) AND " + a.Condition + "") : a.Condition ? " WHERE " + a.Condition + "" : '') : fylEnabled ? (showFutureDataInSortscreen ? " WHERE  CC_YearStatus = 'F' " : " WHERE ( CC_YearStatus ='A' OR CC_YearStatus IS NULL) ") : '') + ' ) AS dummy_table WHERE rn IN ( (c + 1)/2, (c + 2)/2 ) group by cc_parcelid ) ' + a.UniqueName + ' ON parcel.Id = ' + a.UniqueName + '.cc_parcelid';
                    else
                        innerQuery += ' from ' + a.TableName + (a.Condition ? (fylEnabled ? (showFutureDataInSortscreen ? " WHERE  CC_YearStatus = 'F' AND " + a.Condition + "" : " WHERE ( CC_YearStatus ='A' OR CC_YearStatus IS NULL) AND " + a.Condition + "") : a.Condition ? " WHERE " + a.Condition + "" : '') : fylEnabled ? (showFutureDataInSortscreen ? " WHERE  CC_YearStatus = 'F' " : " WHERE ( CC_YearStatus ='A' OR CC_YearStatus IS NULL) ") : '') + ' group by cc_parcelid) ' + a.UniqueName + ' ON parcel.Id = ' + a.UniqueName + '.cc_parcelid';
                }
                else {
                    innerQuery += ' , ' + (a.FunctionName == 'MEDIAN' ? 'AVG' : a.FunctionName) + '(' + (a.FunctionName == 'COUNT' ? '*' : (a.FunctionName == 'MEDIAN' ? ('Median_Value') : a.TableName + '.' + a.FieldName) + convertionquery) + ')' + ' as ' + a.FunctionName + '_' + a.TableName + '_' + a.FieldName;

                    if (a.FunctionName == 'MEDIAN')
                        innerQuery += ' FROM ( SELECT cc_parcelid,' + a.TableName + '.' + a.FieldName + ' AS Median_Value, COUNT(*) OVER (PARTITION  BY cc_parcelid) AS c, ROW_NUMBER() OVER ( PARTITION  BY cc_parcelid ORDER BY CAST( ' + a.TableName + '.' + a.FieldName + ' AS NUMERIC ) ) AS rn FROM ' + a.TableName + (a.Condition ? (fylEnabled ? (showFutureDataInSortscreen ? " WHERE  CC_YearStatus = 'F' AND " + a.Condition + "" : " WHERE ( CC_YearStatus ='A' OR CC_YearStatus IS NULL) AND " + a.Condition + "") : a.Condition ? " WHERE " + a.Condition + "" : '') : fylEnabled ? (showFutureDataInSortscreen ? " WHERE  CC_YearStatus = 'F' " : " WHERE ( CC_YearStatus ='A' OR CC_YearStatus IS NULL) ") : '') + ' ) AS dummy_table WHERE rn IN ( (c + 1)/2, (c + 2)/2 ) group by cc_parcelid ) ' + a.TableName + '_' + a.FieldName + '_' + a.FunctionName + ' ON parcel.Id = ' + a.TableName + '_' + a.FieldName + '_' + a.FunctionName + '.cc_parcelid';
                    else
                        innerQuery += ' from ' + a.TableName + (a.Condition ? (fylEnabled ? (showFutureDataInSortscreen ? " WHERE  CC_YearStatus = 'F' AND " + a.Condition + "" : " WHERE ( CC_YearStatus ='A' OR CC_YearStatus IS NULL) AND " + a.Condition + "") : a.Condition ? " WHERE " + a.Condition + "" : '') : fylEnabled ? (showFutureDataInSortscreen ? " WHERE  CC_YearStatus = 'F' " : " WHERE ( CC_YearStatus ='A' OR CC_YearStatus IS NULL) ") : '') + ' group by cc_parcelid) ' + a.TableName + '_' + a.FieldName + '_' + a.FunctionName + ' ON parcel.Id = ' + a.TableName + '_' + a.FieldName + '_' + a.FunctionName + '.cc_parcelid';
                }
                count = 0;
            }
            else {
                if (a.UniqueName != null && a.UniqueName != '') {
                    innerQuery += ' , ' + (a.FunctionName == 'MEDIAN' ? 'AVG' : a.FunctionName) + '(' + (a.FunctionName == 'COUNT' ? '*' : (a.FunctionName == 'MEDIAN' ? ('Median_Value') : a.TableName + '.' + a.FieldName) + convertionquery) + ')' + ' as ' + a.UniqueName;

                    if (a.FunctionName == 'MEDIAN')
                        innerQuery += ' FROM ( SELECT cc_parcelid,' + a.TableName + '.' + a.FieldName + ' AS Median_Value, COUNT(*) OVER (PARTITION  BY cc_parcelid) AS c, ROW_NUMBER() OVER ( PARTITION  BY cc_parcelid ORDER BY CAST( ' + a.TableName + '.' + a.FieldName + ' AS NUMERIC ) ) AS rn FROM ' + a.TableName + (a.Condition ? (fylEnabled ? (showFutureDataInSortscreen ? " WHERE  CC_YearStatus = 'F' AND " + a.Condition + "" : " WHERE ( CC_YearStatus ='A' OR CC_YearStatus IS NULL) AND " + a.Condition + "") : a.Condition ? " WHERE " + a.Condition + "" : '') : fylEnabled ? (showFutureDataInSortscreen ? " WHERE  CC_YearStatus = 'F' " : " WHERE ( CC_YearStatus ='A' OR CC_YearStatus IS NULL) ") : '') + ' ) AS dummy_table WHERE rn IN ( (c + 1)/2, (c + 2)/2 ) group by cc_parcelid ) ' + a.UniqueName + ' ON parcel.Id = ' + a.UniqueName + '.cc_parcelid';
                    else
                        innerQuery += ' from ' + a.TableName + (a.Condition ? (fylEnabled ? (showFutureDataInSortscreen ? " WHERE  CC_YearStatus = 'F' AND " + a.Condition + "" : " WHERE ( CC_YearStatus ='A' OR CC_YearStatus IS NULL) AND " + a.Condition + "") : a.Condition ? " WHERE " + a.Condition + "" : '') : fylEnabled ? (showFutureDataInSortscreen ? " WHERE  CC_YearStatus = 'F' " : " WHERE ( CC_YearStatus ='A' OR CC_YearStatus IS NULL) ") : '') + ' group by cc_parcelid) ' + a.UniqueName + ' ON parcel.Id = ' + a.UniqueName + '.cc_parcelid';
                }
                else {
                    innerQuery += ' , ' + (a.FunctionName == 'MEDIAN' ? 'AVG' : a.FunctionName) + '(' + (a.FunctionName == 'COUNT' ? '*' : (a.FunctionName == 'MEDIAN' ? ('Median_Value') : a.TableName + '.' + a.FieldName) + convertionquery) + ')' + ' as ' + a.FunctionName + '_' + a.TableName + '_' + a.FieldName;

                    if (a.FunctionName == 'MEDIAN')
                        innerQuery += ' FROM ( SELECT cc_parcelid,' + a.TableName + '.' + a.FieldName + ' AS Median_Value, COUNT(*) OVER (PARTITION  BY cc_parcelid) AS c, ROW_NUMBER() OVER ( PARTITION  BY cc_parcelid ORDER BY CAST( ' + a.TableName + '.' + a.FieldName + ' AS NUMERIC ) ) AS rn FROM ' + a.TableName + (a.Condition ? (fylEnabled ? (showFutureDataInSortscreen ? " WHERE  CC_YearStatus = 'F' AND " + a.Condition + "" : " WHERE ( CC_YearStatus ='A' OR CC_YearStatus IS NULL) AND " + a.Condition + "") : a.Condition ? " WHERE " + a.Condition + "" : '') : fylEnabled ? (showFutureDataInSortscreen ? " WHERE  CC_YearStatus = 'F' " : " WHERE ( CC_YearStatus ='A' OR CC_YearStatus IS NULL) ") : '') + ' ) AS dummy_table WHERE rn IN ( (c + 1)/2, (c + 2)/2 ) group by cc_parcelid ) ' + a.TableName + '_' + a.FieldName + '_' + a.FunctionName + ' ON parcel.Id = ' + a.TableName + '_' + a.FieldName + '_' + a.FunctionName + '.cc_parcelid';
                    else
                        innerQuery += ' from ' + a.TableName + (a.Condition ? (fylEnabled ? (showFutureDataInSortscreen ? " WHERE  CC_YearStatus = 'F' AND " + a.Condition + "" : " WHERE ( CC_YearStatus ='A' OR CC_YearStatus IS NULL) AND " + a.Condition + "") : a.Condition ? " WHERE " + a.Condition + "" : '') : fylEnabled ? (showFutureDataInSortscreen ? " WHERE  CC_YearStatus = 'F' " : " WHERE ( CC_YearStatus ='A' OR CC_YearStatus IS NULL) ") : '') + ' group by cc_parcelid) ' + a.TableName + '_' + a.FieldName + '_' + a.FunctionName + ' ON parcel.Id = ' + a.TableName + '_' + a.FieldName + '_' + a.FunctionName + '.cc_parcelid';
                }
                count += 1;
            }
        });

        let sqlAggregatequery = innerQuery ? 'CREATE VIEW IF NOT EXISTS AggregateFields AS SELECT parcel.ID' + fieldselect + ' FROM ' + tables + innerQuery : 'CREATE VIEW IF NOT EXISTS AggregateFields AS SELECT 1 as ID', fylQuery = '', table = null;

        let processView = function () {
            let viewExec = function () {
                executeQueries(['SELECT parcel.ID' + fieldselect + ' FROM ' + tables + innerQuery, sqlAggregatequery, fylQuery], callback, function () { executeQueries(["CREATE VIEW IF NOT EXISTS AggregateFields AS SELECT 1 AS ID", 'CREATE VIEW IF NOT EXISTS FYLTable AS SELECT 1 as CC_parcelid'], callback) });
            }

            if (table) {
                executeQueries(['SELECT * FROM ' + table], () => {
                    fylQuery = 'CREATE VIEW IF NOT EXISTS FYLTable AS SELECT * FROM ' + table + ' WHERE ' + (showFutureDataInSortscreen ? "  CC_YearStatus = 'F' " : " ( CC_YearStatus ='A' OR CC_YearStatus IS NULL) ") + ' GROUP BY CC_parcelid HAVING MIN(ROWID)'; viewExec();
                }, () => {
                    fylQuery = 'CREATE VIEW IF NOT EXISTS FYLTable AS SELECT 1 as CC_parcelid'; viewExec();
                });
            }
            else {
                fylQuery = 'CREATE VIEW IF NOT EXISTS FYLTable AS SELECT 1 as CC_parcelid';
                viewExec();
            }
        }

        if (fetchCS) {
            getData("SELECT Name, Value FROM ImportSettings WHERE Name = 'FYLtableinSortScreenTemplate'", [], (cls) => {
                if (cls?.length > 0) table = cls[0].Value; processView();
            }, () => { processView(); });
        }
        else {
            table = clientSettings.FYLtableinSortScreenTemplate; processView();
        }
    });
}

function createParcelView(callback, fetchCS) {

    var _db_views = [
        "CREATE VIEW IF NOT EXISTS SubjectParcels AS SELECT p.Id, p.*, n.*,ag.*, n.Number AS NeighborhoodNo, n.Number AS NBHDNO, p.Id AS SubjectParcelId,  CASE WHEN n.Number IS NULL THEN 'false' ELSE 'true' END as ParcelInGroup, fyl.* FROM Parcel p LEFT OUTER JOIN Neighborhood n ON p.NeighborhoodId = n.Id LEFT OUTER JOIN AggregateFields ag ON ag.Id=p.Id LEFT OUTER JOIN FYLTable fyl on fyl.cc_parcelid=p.Id WHERE IsComparable = 0 AND p.Id <> -99;",
        "CREATE VIEW IF NOT EXISTS PendingParcels AS SELECT p.Id, p.*, n.*,ag.*, n.Number AS NeighborhoodNo, n.Number AS NBHDNO, p.Id AS SubjectParcelId,  CASE WHEN n.Number IS NULL THEN 'false' ELSE 'true' END as ParcelInGroup, fyl.* FROM Parcel p LEFT OUTER JOIN Neighborhood n ON p.NeighborhoodId = n.Id LEFT OUTER JOIN AggregateFields ag ON ag.Id=p.Id LEFT OUTER JOIN FYLTable fyl on fyl.cc_parcelid=p.Id WHERE Reviewed = 'false' AND IsComparable = 0 AND p.Id <> -99;"
    ];
    createAggregateFields(function () {
        executeQueries(_db_views, callback);
    }, fetchCS);
}

function deleteEmptyImages(callback) {
    todoDB.indexedDB.deleteRecord('Images', ['Image'], ['='], [''], [''], callback);
    //executeQueries(_db_drop.EmptyImages, callback);
}

function checkCompatibility(callback) {
    db.transaction(function (x) {
        executeSql(x, _db_check.FieldCompatibility, [], function () {
            getData(_db_check.AllTextFieldsCompatibility, [], function (cats) {
                if (cats.length > 0) {
                    if (callback) callback();
                } else {
                    executeQueries(_db_comp_FieldCategory, callback);
                }
            }, null, true, function () { executeQueries(_db_comp_FieldCategory, callback); });

        }, function () { executeQueries(_db_comp_FieldCategory, callback) });
    });
}

function resetTablesForUserChange(callback) {
    executeQueries(_db_proc_changeLogin, callback);
}

function clearDatabase(callback) {
    var listsql = "SELECT tbl_name FROM sqlite_master WHERE type = 'table' AND name NOT LIKE '#_%' ESCAPE '#'";
    getData(listsql, [], function (names) {
        var sqls = names.map(function (x) { return "DROP TABLE " + x.tbl_name + ";" })
        executeQueries(sqls, function () {
            console.log('All websql tables cleared.');
            clearSqlLiteIndexDB(function () {
                if (callback) callback();
            });
        });
    });
}

var logQuery = false;
function enableDebugging() {
    localDev = true;
    quickDebug = true;
    logQuery = true;
}
var lastQueryOutput;
function getData(query, params, callback, otherData, ignoreError, errorCallback) {
    if (params == null)
        params = [];
    var data = [];
    db.transaction(function (x) {
        if (logQuery) console.log(query);
        x.executeSql(query, params, function (x, results) {
            var len = results.rows.length, i;
            for (i = 0; i < len; i++) {
                data.push(results.rows.item(i));
            }
            if (callback) callback(data, otherData, results); else { lastQueryOutput = data; if (data.length > 0) console.log(data[0]); };
        }, function (x, e) {
            if (errorCallback) errorCallback();
            if (!ignoreError) {
                console.error(e.message);
                console.error(query);
                alert(e.message);
                log(e.message);
                log(query);
            }
            else {
                console.error(e.message);
                console.error(query);
            }
        });
    });
}

function testQuery(query) {
    if (!db || !ccma.Session.IsLoggedIn) {
        console.log('Database is not ready! Please login with your credentials.');
        return false;
    }
    getData(query, [], function (data) {
        if (data.length == 0) {
            console.log('Command executed successfully! (' + data.length + ' records found.)');
        } else {
            console.log(data.length + ' records found.');
            var reduced = [];
            for (var i in data) {
                var o = data[i], p = {}, fi = 0;
                for (var x in o) {
                    fi += 1;
                    p[x] = o[x];
                    if (fi > 12) break;
                };
                reduced.push(p);
                if (i > 50) break;
            };
            console.log(reduced);
        }

    }, null, null, function (e) { console.error(e) });
}

function getTransaction(tables, isReadOnly, callback) {
    var processor = function (tempCallback) {
        if (tempCallback) tempCallback();
    }
    if (navigator.standalone && iOS10)
        processor = function (tempCallback) {
            var dbName = todoDB.indexedDB.db.name;
            todoDB.indexedDB.db.close();
            var open = indexedDB.open(dbName);
            open.onsuccess = function (evt) {
                todoDB.indexedDB.db = evt.target.result;
                if (tempCallback) tempCallback();
            }
        }
    processor(function () {
        if (todoDB.indexedDB.db.objectStoreNames.contains(tables[0])) {
            var tr = todoDB.indexedDB.db.transaction(tables, (isReadOnly ? 'readonly' : 'readwrite'));
            if (callback) callback(tr);
        } else {
            if ((tables.indexOf('SubjectParcels') == -1) && (tables.indexOf('PendingParcels') == -1)) console.log(tables);
            if (callback) callback('error', tables);
        }
    });
}

function IDB_getData(tableName, indexes, operators, values, idxJoiners, callback, otherData, ignoreError, errorCallback, isCount) {
    var result = [];
    getTransaction([tableName], true, function (tx) {
        if (tx != 'error') {
            try {
                var obj = tx.objectStore(tableName), isIndexed = false;
                if (indexes.length == 1 && operators[0] == '=') {
                    var key = IDBKeyRange.only(values[0])
                    if (isCount) obj = obj.index(indexes[0]).count(key);
                    else obj = obj.index(indexes[0]).getAll(key);
                    isIndexed = true;
                }
                else if (indexes.length > 0 && operators[0] == '=') {
                    //if (isCount) obj = obj.index(indexes.shift()).count(values.shift());
                    var key = IDBKeyRange.only(values.shift())
                    obj = obj.index(indexes.shift()).getAll(key);
                    operators.shift();
                    //idxJoiners.shift();
                }
                else if (indexes.length == 0) {
                    if (isCount) obj = obj.count();
                    else obj = obj.getAll();
                    isIndexed = true;
                }
                else obj = obj.getAll();
                var isTrue = true, rec;
                tx.oncomplete = function () {
                    if (callback) callback(result, otherData);
                }
                obj.onsuccess = function (evt) {
                    var cursor = evt.target.result;
                    if (isIndexed) { result = cursor; }
                    else if (cursor) {
                        var records = cursor;
                        records.forEach(function (rec) {
                            isTrue = true;
                            indexes.forEach(function (idx, i) {
                                var joiner = idxJoiners[i], oper = operators[i], val = values[i], flag = false;
                                switch (oper.toLowerCase()) {
                                    case '=': if (rec[idx] == val) flag = true; break;
                                    case '!=': if (rec[idx] != val) flag = true; break;
                                    case '<': if (rec[idx] < val) flag = true; break;
                                    case '>': if (rec[idx] > val) flag = true; break;
                                    case '<=': if (rec[idx] <= val) flag = true; break;
                                    case '>=': if (rec[idx] >= val) flag = true; break;
                                    case '%like': if (rec[idx].endsWith(val)) flag = true; break;
                                    case 'like%': if (rec[idx].startsWith(val)) flag = true; break;
                                    case '%like%': if (rec[idx].search(val)) flag = true; break;
                                    case 'between': if (rec[idx] < val && rec[idx] > values.shift()) flag = true; break;
                                    case 'in': if (val.indexOf(rec[idx]) > -1) flag = true; break;
                                }
                                if (joiner == 'AND') isTrue = isTrue && flag;
                                else isTrue = isTrue || flag;
                            });
                            if (isTrue) result.push(rec);
                        });
                        //cursor.continue();
                    }
                }
                obj.onerror = function (ex) {
                	console.error(obj.error);
                    if (errorCallback) errorCallback();
                }

            } catch (ex) {
                console.error(ex);
                console.log(tableName, indexes, operators, values, idxJoiners)
            }
        }
        else if (errorCallback) errorCallback();
    });
}

function executeSql(tx, query, params, success, failure) {
    tx.executeSql(query, params, function (x, results) { if (success) success(x, results) }, function (x, e) { if (failure) failure(x, e); else console.error(query, e.message) });
}

function _getColumnInformation(firstRow, callBack, tableName) {
    var cKeys = [];
    var columns = "";
    var createColumns = "";
    var phs = "";
    for (key in firstRow) {
        var df = getDataField(key, tableName);

        cKeys.push(key);
        var type = "TEXT"

        ////        if (key == "Index") {
        ////            key = "SortIndex"
        ////            type = "INTEGER";
        ////        }
        //        if (key == "Id" || /^.*?Id$/.test(key) || /^.*?_id$/.test(key) || /^.*?UID$/.test(key)) {
        //            type = "INTEGER"
        //        }
        //        if (key == "Synced") {
        //            type = "INTEGER"
        //        }
        //        if (firstRow[key] != null) {
        //            if ((firstRow[key].toString() == 'true') || (firstRow[key].toString() == 'false')) {
        //                type = "BOOL";
        //            }
        //        }
        //        if (df) {
        //            switch (parseInt(df.InputType)) {
        //                case 1: type = "TEXT"; break;
        //                case 2: type = "FLOAT"; break;
        //                case 3: type = "BOOL"; break;
        //                case 4: type = "DATETIME"; break;
        //                case 7: type = "FLOAT"; break;
        //                case 8: type = "INT"; break;
        //                case 9: type = "INT"; break;
        //                case 10: type = "INT"; break;
        //                default: type = "TEXT"; break;
        //            }
        //        }

        if (columns == "") {
            columns = key;
            createColumns = "[" + key + "]" + " " + type;
            phs = "?";
        }
        else {
            columns += ", " + key;
            createColumns += ", " + "[" + key + "]" + " " + type;
            phs += ", ?"
        }
    }

    var r = new Object();
    r.Columns = columns;
    r.CreateColumns = createColumns;
    r.PlaceHolders = phs;
    r.ColumnKeys = cKeys;

    if (callBack) callBack(r);
}

function _parseDataForInsert(parcel, firstRow, cinfo) {
    var pa = [];
    for (key in parcel) {
        if (cinfo.ColumnKeys.indexOf(key) > -1) {
            var dat;
            dat = parcel[key];
            //console.log("TrueFalsePCI in MA =", TrueFalseInPCI);
            if (TrueFalseInPCI == "False") {
                if (dat == "True" || dat == "true") dat = true;
                if (dat == "False" || dat == 'false') dat = false;
            }
            if (key == "Index") {
                dat = parseInt(dat);
            }
            dat = dat === null ? dat : dat.toString();
            pa.push(dat);
        }
    }
    return pa;
}

function _loadParcelColumnInformation(data, callback, tableName) {
    getData('SELECT * FROM Field', [], function (fields) {
        var columns = [];
        var cKeys = [];
        for (x in fields) {
            f = fields[x];
            if ((f.ImportTypeId === undefined) || (f.ImportTypeId == 0)) {
                cKeys.push(f.Name);
                var type = " TEXT";
                //                switch (f.InputType) {
                //                    case "2":
                //                        type = " FLOAT";
                //                        break;
                //                    case "3":
                //                        type = " BOOL";
                //                        break;
                //                }
                columns["___" + f.Name] = type;
            }
        }
        for (key in data) {
            var dkey = key;
            dkey = "___" + key;
            if (cKeys.indexOf(key) == -1)
                cKeys.push(key);
            if (columns[dkey] === undefined) {
                var type = " TEXT"
                //                if (key == "Id")
                //                    type == " INTEGER KEY"
                columns[dkey] = type;
            }
        }

        var createColumns = "";
        var colcheck = [];
        for (key in columns) {
            key = key.replace("___", "")
            if (colcheck.indexOf(key.toLowerCase()) == -1) {
                if (createColumns != "") createColumns += ", ";
                createColumns += "[" + key + "]" + columns["___" + key];
                colcheck.push(key.toLowerCase());
            }
        }

        createColumns += ",DistanceFromMe FLOAT,DistanceIndex INTEGER"
        var r = new Object();
        r.CreateColumns = createColumns;
        r.ColumnKeys = cKeys;

        if (callback) callback(r);
    });
}

var subTables = [];
function _syncSubTables(tableName, data, callback) {
    if (data.length == 0) {
        //log('No data downloaded as sub tables.');
        if (callback) callback();
    }
    var filtered = {};
    for (x in data) {
        var item = data[x];
        var dsName = item.DataSourceName;
        if (filtered[dsName] == null) {
            filtered[dsName] = [];
        }
        filtered[dsName].push(item);
    }
    _syncSubTableFromList(filtered, callback, (tableName == "*ld*"? true: false));
}

function _syncSubTableFromList(filtered, callback, islkTbl) {
    if (Object.keys(filtered).length == 0) {
        if (callback) callback();
    }
    for (var key in filtered) {
        _syncTable(key, filtered[key], function () {
            delete filtered[key];
            _syncSubTableFromList(filtered, callback, islkTbl);
        }, islkTbl);
        break;
    }
}

var columnDir = {};

function IDB_syncTable(tableName, Data, callback) {
    console.log("Downloaded table: " + tableName + ", " + Data.length + " rows.");
    var insertData = function (data) {
        data.forEach(function (d) {
            d.IsPrimary = (d.IsPrimary === true ? 1 : 0);
            d.DownSynced = (d.DownSynced === true ? 1 : 0);
        });
        todoDB.indexedDB.Insert('Images', data, function () {
            if (callback) callback();
        })
    }
    if (Data.length == 0) {
        syncProgressList.push(tableName);
        if (callback) callback();
        return;
    }
    insertData(Data);
}

function _syncTable(tableName, data, callback, islkTbl) {
    console.log("Downloaded table: " + tableName + ", " + data.length + " rows.");
    if (data.length == 0) {
        syncProgressList.push(tableName);
        if (callback) callback();
        return;
    }
    if (syncProgressList.indexOf(tableName) > -1) {
        syncProgressList.push(tableName);
        _syncData(tableName, data, columnDir[tableName], callback, islkTbl);
        return;
    }

    if (tableName == "Parcel") {
        parcelCount = data.length;
        localStorage.setItem("pcount", parcelCount);
    }
    if (data.length > 0) {
        db.transaction(function (x) {
            var dropAction = "DROP TABLE IF EXISTS " + tableName;
            var dropConfirm = tableName + " dropped.";
            if (tableName == "ParcelChanges") {
                dropAction = "DELETE FROM ParcelChanges WHERE Synced = '1'"; dropConfirm = 'Synced parcel cleared.'
            }
            else if (tableName == "Images") {
                dropAction = "DELETE FROM Images WHERE Synced = '1'"; dropConfirm = 'Synced Images cleared.'
                todoDB.indexedDB.deleteRecord('Images', ['Synced'], ['='], [1], [])
            }
            x.executeSql(dropAction, [], function (x) {
                console.log(dropConfirm);
                if (data.length > 0) {
                    var firstRow = data[0];
                    var tableRenderer;
                    if (tableName == "Parcel")
                        tableRenderer = _loadParcelColumnInformation;
                    else
                        tableRenderer = _getColumnInformation;
                    tableRenderer(firstRow, function (cInfo) {
                        columnDir[tableName] = cInfo;
                        syncProgressList.push(tableName);
                        _syncData(tableName, data, cInfo, callback, islkTbl);
                    }, tableName);
                }
            }, function (x, e) {
            });
        });

    }
}

function _syncData(tableName, data, cinfo, callback, islkTbl) {
    //CreateStore(tableName, data, function () {
    //    if (tableName == "StreetMapPoints") {
    //        deleteDB();
    //        CreateStore(tableName, data, callback);
    //    }
    //    else {
    var createSql = "CREATE TABLE IF NOT EXISTS " + tableName + " (" + cinfo.CreateColumns + ");";
    db.transaction(function (x) {
        executeSql(x, createSql, [], function (tx) {
            //log("Loading local table " + tableName + ".");
            if (islkTbl && data.length == 1 && data[0].ROWUID == -9999) {
                log("Finished creating local table " + tableName + ". Only -9999 rows exists and ignored.");
                if (callback) callback();
                return;
            }

            var firstRow = data[0];
            var insertQueries = [];

            var columns = "";
            var phs = "";
            for (key in firstRow) {
                if (cinfo.ColumnKeys.indexOf(key) > -1) {
                    if (key == "Index") key = "SortIndex";
                    if (columns == "") { columns = "[" + key + "]"; phs = "?"; }
                    else { columns += ", " + "[" + key + "]"; phs += ", ?"; }
                }
            }
            //var insertSql = "INSERT INTO " + tableName + " (" + columns + ") VALUES (" + phs + ");", selectSql;
            var insertHeader = "INSERT INTO " + tableName + " (" + columns + ") ";
            var selectSql = "SELECT " + phs + " ";


			// Max 999 parameters are allowed on some systems. So lets make the batch size, with respect to the number of fields.
			
            var batchSize = Math.floor(999 / cinfo.ColumnKeys.length);
            var total = data.length;
            var progress = 0;
            var batchTotal = Math.ceil(total / batchSize);
            for (var i = 0; i < data.length; i = i + batchSize) {
                var batchSelect = '', batchData = [];
                for (var j = 0; j < batchSize; j++) {
                    var ri = i + j;
                    if (ri >= total) break;
                    var parcel = data[ri];
                    var pa = _parseDataForInsert(parcel, firstRow, cinfo);

                    batchSelect += (j > 0 ? ' UNION ALL ' : ' ') + selectSql;
                    batchData = batchData.concat(pa);
                }

                var insertSql = insertHeader + batchSelect;

                executeSql(tx, insertSql, batchData, function (tx, res) {
                    progress += 1;
                    if (progress == batchTotal) {
                        log("Finished creating local table " + tableName + ".");
                        if (callback) callback();
                    }
                }, function (e, m) {
                    log(m.message, true);
                });
                
                

                //executeSql(tx, insertSql, pa, function (tx, res) {
                //    progress += 1;
                //    if (progress == total) {
                //        log("Finished creating local table " + tableName + ".");
                //        if (callback) callback();
                //    }
                //}, function (e, m) {
                //    log(m.message, true);
                //});
            }

            //executeQueries(insertQueries, callback, function (e, m) {
            //    log(m.message, true);
            //    callback && callback();
            //}, null, true);
        });
    });
    //});
    //   }
}

function dropDatabase(callback) {
    console.log('dropping db');
    var p1 = performance.now();
    var req = indexedDB.deleteDatabase(todoDB.indexedDB.db.name);
    todoDB.indexedDB.db.close();
    req.onsuccess = function () {
        console.log('dropped db within ' + Math.round((performance.now() - p1)));
        if (callback) callback();
    }
    req.onblocked = function (e) {
        console.log('db drop blocked: ');
    }
}

function checkDbExistence(callback) {
    var existingUser = localStorage.getItem('username');
    var loggedUser = $('.login-username').val();
    var prevVersionNo = localStorage.getItem('schema_version');
    var currVersionNo = localDBName.split('MA')[1];
    var processor = function (isAgreed) {
        callback(isAgreed); return;
    }
    clearDB = false;
    var initDB = function (callback) {


        var dbName = localStorage.getItem('dbName')
        openDB(dbName, function (dbx) {
            db = dbx;

            if (todoDB.indexedDB && todoDB.indexedDB.db) todoDB.indexedDB.db.close();
            initLocalIDBdatabase(dbName, callback);
        }, function (e) {
            processor(false);
            messageBox("Error occurred while connecting to the database '" + dbName + "'. You have been logged out.");
            console.error(e);
            db_conn_error = (e ? (e + '\n') : '') + logCurrentStack();
        })

    }
    var checkUnsyncedStatus = function (existingUser, callback, isUserChanged, isEnvChanged) {
        var clear = function () {
            clearDB = true; callback(true);
        }
        var prompt = function (table) {
            if(isEnvChanged)
            	messageBox('The environment has been changed and there are some pending changes in ' + table + '. Click "Cancel" to login with the previous environment  '+ getCookie('PrevURL'), ['Discard changes', 'Cancel'], () => {
                    clear();
                }, () => {
                    callback(false);
                });
            else if (isUserChanged)
                messageBox('Contains unsynced ' + table + ' created by the previous user(' + existingUser + '). Click Cancel to login as ' + existingUser, ['Discard changes', 'Cancel'], function () {
                    clear();
                }, function () { callback(false); });
            else
                messageBox('The schema version of this environment has been changed and there are some pending changes in ' + table + '. Click "Cancel" to login with the existing database.', ['Discard changes', 'Cancel'], function () {
                    clear();
                }, function () { preventSchemaUpdate = true; callback(true); });
        }
        var checkIDBstat = function () {
            errorInIDB = false;
            if (todoDB.indexedDB.db.objectStoreNames.contains('Images')) {
                IDB_getData('Images', ['Synced'], ['='], [0], [], function (count) {
                    if (count > 0) prompt('Images');
                    else { console.log('Will drop db since the ' + (isUserChanged ? 'username' : 'schema version') + ' is mismatched with the previous one.'); clear(); }
                }, null, null, null, true)
            }
            else { clear(); }
        }
        initDB(function () {
            db.transaction(function (tx) {
                tx.executeSql('SELECT COUNT(*) FROM ParcelChanges WHERE Synced IN (0,"0")', [], function (x, pciCount) {
                    errorInWebSql = false;
                    if (pciCount.rows.item(0)['COUNT(*)'] > 0) prompt('ParcelChanges');
                    else checkIDBstat();
                }, function () { errorInWebSql = true; checkIDBstat(); });
            });
        }, function () { errorInIDB = true; });
    }

    if (existingUser === undefined || existingUser === null) { localStorage.setItem('dbName', 'CC_MA'); processor(true); }

    else if (!prevVersionNo) {
        if (existingUser == loggedUser) {
            var dbName = localDBName + '_' + existingUser.replace(/\s/g, '').replace(/-/g, '_');
            openDB(dbName, function (dbx) {
                db = dbx;
                clearDB = false;
                processor(true);
            }, function () {
                messageBox('The schema version has been changed. Please delete this instance and create new.');
                processor(false);
            })
        }
        else messageBox('Please login as the previous user: ' + existingUser, function () { processor(false); })
    }
    else {
        if (existingUser == loggedUser) {
            if(getCookie('PrevURL') && getCookie('PrevURL') !== "" && getCookie('PrevURL').toLowerCase().indexOf(window.location.hostname.toLowerCase()) == -1)
            		checkUnsyncedStatus(existingUser, processor, false, true);
            else if (prevVersionNo == currVersionNo) { clearDB = false; processor(true); }
            else checkUnsyncedStatus(existingUser, processor, false);
        }
        else checkUnsyncedStatus(existingUser, processor, true);
    }
}

function insert_unsyncedImages(callback) {
    var imgdata;
    var processor = function () {
        IDB_getData('Images', ['Synced'], ['='], [0], [], function (images) {
            var insert = function () {
                if (images.length == 0) { if (callback) callback(); return; }
                var im = images.pop();
                IDB_getData('UnsyncedImages', ['LocalId'], ['='], [im.LocalId], [], function (imCount) {
                    if (imCount != 0) insert();
                    else {
                        imgdata = [{ ParcelId: parseInt(im.ParcelId), Image: im.Image, UploadTime: im.UploadTime, Synced: 0, LocalId: im.LocalId, Type: im.Type }];
                        todoDB.indexedDB.Insert('UnsyncedImages', imgdata, function () {
                            insert();
                        })
                    }
                }, null, null, null, true);
            }
            insert();
        })
    }
    IDB_getData('Images', ['Synced'], ['='], [0], [], function (imgCount) {
        IDB_getData('UnsyncedImages', [], [], [], [], function (unsyncedCount) {
            if (imgCount == unsyncedCount) { if (callback) callback(); return; }
            else processor();
        }, null, null, null, true);
    }, null, null, null, true);
}

function windowsAppClose() {
    window.chrome.webview.hostObjects.ExternalObject.CloseMA();
}
