﻿Object.defineProperties(window, {
    'who': {
        get: function () { return ccma.Session.User + ' @ ' + org_county_name; }
    },
    'platform': {
        get: function () { return ccma.Environment.OS; }
    },
    'useragent': { value: navigator.userAgent },
    'dashboard': {
        get: function () {
            return $('.dashboard-table')[0].outerHTML;
        }
    },
    'runningsince': {
        get: function () { return (new Date(performance.timing.domLoading)).toLocaleString(); }
    },
    'lasterror': {
        get: function () { return ccma.State.LastError; }
    },
    'activeparcel': {
        get: function () { if (activeParcel) return activeParcel.KeyValue1; else return 'None' }
    },
    'activescreen': {
        get: function () { return ccma.UI.ActiveScreenId; }
    },
    'user': {
        get: function () { return ccma.Session.User; }
    },
    'currenttitle': {
        get: function () {
            if (ccma.UI.ActiveScreenId) {
                $('header span', 'section#' + ccma.UI.ActiveScreenId).html();
            }
        }
    }
});


function _pass(p) {
    $('.login-password').val(p);
    $('.login-signin-button').trigger('click');
}

function helpobj(o) {
    if (o === null || o === undefined)
        return 'NULL';
    var str = '';
    for (var x in o) {
        str += x + '\n';
    }
    return str;
}

function _debugger_processor(data) {
    if (!data) return false;
    var udata = data.trim().toUpperCase();
    if (udata.startsWith("SELECT ") || udata.startsWith("UPDATE ") || udata.startsWith("INSERT ") || udata.startsWith("DELETE ") || udata.startsWith("CREATE ") || udata.startsWith("DROP ") ) {
        testQuery(data);
        return true;
    }
    return false;
}