﻿//    DATA CLOUD SOLUTIONS, LLC ("COMPANY") CONFIDENTIAL 
//    Unpublished Copyright (c) 2010-2013 
//    DATA CLOUD SOLUTIONS, an Ohio Limited Liability Company, All Rights Reserved.

//    NOTICE: All information contained herein is, and remains the property of COMPANY.
//    The intellectual and technical concepts contained herein are proprietary to COMPANY
//    and may be covered by U.S. and Foreign Patents, patents in process, and are protected
//    by trade secret or copyright law. Dissemination of this information or reproduction
//    of this material is strictly forbidden unless prior written permission is obtained
//    from COMPANY. Access to the source code contained herein is hereby forbidden to
//    anyone except current COMPANY employees, managers or contractors who have executed
//    Confidentiality and Non-disclosure agreements explicitly covering such access.</p>

//    The copyright notice above does not evidence any actual or intended publication
//    or disclosure of this source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
//    DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
//    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
//    IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
//    OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
//    TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
//    ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.


function ProductivityStats() {
    stats = this;

    function ParcelStatistics() {
        this.TotalParcels = 0;
        this.AlertParcels = 0;
        this.PriorityParcels = 0;
        this.AlertsRemaining = 0;
        this.PriorityRemaining = 0;
        this.OtherParcels = 0;
        this.CompletedParcels = 0;
        this.TotalAlertsRemaining = 0;
        this.TotalPriorityRemaining = 0;
    }
    this.currNbhdId = "";
    this.Neighborhood = "";
    this.UserID = "";
    this.LoginTime = "";

    this.NbhdStats = new ParcelStatistics();
    this.ApprStats = new ParcelStatistics();

    this.LastCAMAUpdate = null;
    this.LastDataSync = null;
    this.CompletedCount = 0;
    this.PendingChanges = 0;
    this.PendingLocations = 0;
    this.PendingPhotos = 0;

    this.refreshLocal = function (currNbhd, callback) {
        var nbhd = localStorage.getItem('nbhd').toString().replace(/'/g, '').split('||');
        //var nbhdId = localStorage.getItem('nbhdId').toString().replace(/'/g, '').split('||');
        stats.Neighborhood = [];
        for (i in nbhd)
            stats.Neighborhood.push({ 'Nbhd': nbhd[i].split('##')[0], 'NbhdId': nbhd[i].split('##')[1] });
        stats.UserID = localStorage.getItem('username');
        if (localStorage.getItem('last-cama-update') != null) {
            var lcu = parseInt(localStorage.getItem('last-cama-update'));
            stats.LastCAMAUpdate = new Date(lcu);
        }
        if (localStorage.getItem('last-data-sync') != null) {
            var lcu = parseInt(localStorage.getItem('last-data-sync'));
            stats.LastDataSync = new Date(lcu);
        }
        stats.currNbhdId = (currNbhd ? currNbhd : ($('.dashboard-nbhd-select').val() ? $('.dashboard-nbhd-select').val() : (nbhd[0].split('##')[1] ? nbhd[0].split('##')[1] : null)));
        changedNbhd = stats.currNbhdId;
        if (db) {
            db.transaction(function (tx) {
                stats.ApprStats.TotalParcels = 0;
                stats.ApprStats.AlertParcels = 0;
                stats.ApprStats.PriorityParcels = 0;
                stats.ApprStats.AlertParcels = 0;
                stats.ApprStats.CriticalParcels = 0;
                stats.ApprStats.MediumParcels = 0;
                stats.ApprStats.NormalParcels = 0;
                stats.ApprStats.OtherParcels = 0;
                stats.ApprStats.CompletedParcels = 0;
                stats.NbhdStats.TotalParcels = 0;
                stats.NbhdStats.AlertParcels = 0;
                stats.NbhdStats.PriorityParcels = 0;
                stats.NbhdStats.OtherParcels = 0;
                stats.NbhdStats.CompletedParcels = 0;
                stats.ApprStats.AlertsRemaining = 0;
                stats.ApprStats.PriorityRemaining = 0;
                stats.ApprStats.CriticalRemaining = 0;
                stats.ApprStats.MediumRemaining = 0;
                stats.ApprStats.NormalRemaining = 0;
		        stats.PendingLocations = 0;
                stats.PendingPhotos = 0;
                stats.ApprStats.TotalAlertsRemaining = 0;
                stats.ApprStats.TotalPriorityRemaining = 0;
                stats.ApprStats.TotalCriticalRemaining = 0;
                stats.ApprStats.TotalMediumRemaining = 0;
                stats.ApprStats.TotalNormalRemaining = 0;
                var query = "SELECT * FROM NbhdStats"
                if (stats.currNbhdId != null)
                    query += " WHERE Id = '" + stats.currNbhdId + "'"
                tx.executeSql(query, [], function (tx, res) {
                    if (res.rows.length > 0) {
                        var s = res.rows.item(0);
                        stats.NbhdStats.TotalParcels = parseInt(s.Total);
                        stats.NbhdStats.AlertParcels = parseInt(s.Alerts);
                        stats.NbhdStats.PriorityParcels = parseInt(s.Priorities);
                        if (EnableNewPriorities == 'True') {
                            stats.NbhdStats.CriticalParcels = parseInt(s.Critical);
                            stats.NbhdStats.MediumParcels = parseInt(s.Medium);
                            stats.NbhdStats.NormalParcels = parseInt(s.Normal);
                        }
                        stats.NbhdStats.OtherParcels = parseInt(s.Others);
                        stats.NbhdStats.CompletedParcels = parseInt(s.Completed);
                        stats.NbhdStats.PriorityRemaining = parseInt(s.PriorityRemains);
                        stats.NbhdStats.AlertsRemaining = parseInt(s.AlertRemains);
                        if (EnableNewPriorities == 'True') {
                            stats.NbhdStats.CriticalRemaining = parseInt(s.CriticalRemains);
                            stats.NbhdStats.MediumRemaining = parseInt(s.MediumRemains);
                            stats.NbhdStats.NormalRemaining = parseInt(s.NormalRemains);
                        }
                    }
                }, function (tx, err) {

                }); 
                query = "SELECT * FROM ParcelChanges WHERE ";
                if (stats.currNbhdId != null)
                    query += "ParcelId IN (SELECT Id FROM Parcel WHERE NeighborhoodId = '" + stats.currNbhdId + "') AND "
                query += "(Synced = '0' OR Synced = 0)"
                tx.executeSql(query, [], function (tx, res) {
                    stats.PendingChanges = res.rows.length;
                });
                
				var afterImage = function() {
					db.transaction(function(tx) {
		                tx.executeSql("SELECT * FROM Location WHERE Synced = 0 OR Synced = '0'", [], function (tx, res) {
		                    stats.PendingLocations = res.rows.length;
		                });
		                query = "SELECT * FROM SubjectParcels";
		                if (stats.currNbhdId != null)
		                    query += " WHERE NeighborhoodId = '" + stats.currNbhdId + "'"
		                tx.executeSql(query, [], function (tx, res) {
		                    stats.ApprStats.TotalParcels = res.rows.length;
		                    var count = 0;
		                    var total = res.rows.length;
		                    for (i = 0; i < total; i++) {
                                var p = res.rows.item(i);
                                if (EnableNewPriorities == 'True') {
                                    if (p.CC_Priority == 4) {
                                        stats.ApprStats.AlertParcels += 1;
                                    }
                                    else if (p.CC_Priority == 3) {
                                        stats.ApprStats.PriorityParcels += 1;
                                    }
                                    else if (p.CC_Priority == 5) {
                                        stats.ApprStats.CriticalParcels += 1;
                                    }
                                    else if (p.CC_Priority == 2) {
                                        stats.ApprStats.MediumParcels += 1;
                                    }
                                    else if (p.CC_Priority == 1) {
                                        stats.ApprStats.NormalParcels += 1;
                                    }
                                    else {
                                        stats.ApprStats.OtherParcels += 1;
                                    }
                                    
                                }
                                else {
                                    if (p.CC_Priority == 2) {
                                        stats.ApprStats.AlertParcels += 1;
                                    } else if (p.CC_Priority == 1) {
                                        stats.ApprStats.PriorityParcels += 1;
                                    } else {
                                        stats.ApprStats.OtherParcels += 1;
                                    }
                                }
                                if (p.Reviewed == 'true') {
                                    stats.ApprStats.CompletedParcels += 1;
                                }
		                        count += 1;
		                        if (count == total) {
		                            query = "SELECT * FROM PendingParcels";
		                            if (stats.currNbhdId != null)
		                                query += " WHERE NeighborhoodId = '" + stats.currNbhdId + "'"
		                            tx.executeSql(query, [], function (tx, res) {
		                                stats.ApprStats.AlertsRemaining = 0;
		                                stats.ApprStats.PriorityRemaining = 0;
                                        stats.ApprStats.OtherParcels = 0;
                                        stats.ApprStats.CriticalRemaining = 0;
                                        stats.ApprStats.MediumRemaining = 0;
                                        stats.ApprStats.NormalRemaining = 0;
		                                //var count = 0;
		                                var total = res.rows.length;
		                                for (i = 0; i < total; i++) {
                                            var p = res.rows.item(i);
                                            if (EnableNewPriorities == 'True') {
                                                if (p.CC_Priority == 4) {
                                                    stats.ApprStats.AlertsRemaining += 1;
                                                }
                                                else if (p.CC_Priority == 3) {
                                                    stats.ApprStats.PriorityRemaining += 1;
                                                }
                                                else if (p.CC_Priority == 5) {
                                                    stats.ApprStats.CriticalRemaining += 1;
                                                }
                                                else if (p.CC_Priority == 2) {
                                                    stats.ApprStats.MediumRemaining += 1;
                                                }
                                                else if (p.CC_Priority == 1) {
                                                    stats.ApprStats.NormalRemaining += 1;
                                                }
                                                else {
                                                    stats.ApprStats.OtherParcels += 1;
                                                }
                                            }
                                            else {
                                                if (p.CC_Priority == 2) {
                                                    stats.ApprStats.AlertsRemaining += 1;
                                                } else if (p.CC_Priority == 1) {
                                                    stats.ApprStats.PriorityRemaining += 1;
                                                } else {
                                                    stats.ApprStats.OtherParcels += 1;
                                                }
                                            }
		                                    //                                    count += 1;
		                                    //                                    if (count == total) {
		                                    //                                        
		                                    //                                    }
		                                }
		                                stats.ApprStats.TotalAlertsRemaining = stats.ApprStats.AlertsRemaining + stats.NbhdStats.AlertsRemaining;
                                        stats.ApprStats.TotalPriorityRemaining = stats.ApprStats.PriorityRemaining + stats.NbhdStats.PriorityRemaining;
                                        if (EnableNewPriorities == 'True') {
                                            stats.ApprStats.TotalCriticalRemaining = stats.ApprStats.CriticalRemaining + stats.NbhdStats.CriticalRemaining;
                                            stats.ApprStats.TotalMediumRemaining = stats.ApprStats.MediumRemaining + stats.NbhdStats.MediumRemaining;
                                            stats.ApprStats.TotalNormalRemaining = stats.ApprStats.NormalRemaining + stats.NbhdStats.NormalRemaining;
                                        }
		                                if (callback) { callback(); }
		                            }, function (tx, err) {
		
		                            });
		
		                            //                            tx.executeSql("SELECT * FROM Parcel WHERE Reviewed = 'true'", [], function (tx, res) {
		                            //                                stats.ApprStats.AlertsRemaining = 0;
		                            //                                stats.ApprStats.PriorityRemaining = 0;
		                            //                                stats.ApprStats.OtherParcels = 0;
		                            //                                //var count = 0;
		                            //                                var total = res.rows.length;
		                            //                                for (i = 0; i < total; i++) {
		                            //                                    var p = res.rows.item(i);
		                            //                                    if (p.OnAlert == "true") {
		                            //                                        stats.ApprStats.AlertsRemaining += 1;
		                            //                                    } else if (p.Priority == 1) {
		                            //                                        stats.ApprStats.PriorityRemaining += 1;
		                            //                                    } else {
		                            //                                        stats.ApprStats.OtherParcels += 1;
		                            //                                    }
		                            //                                    //                                    count += 1;
		                            //                                    //                                    if (count == total) {
		                            //                                    //                                        
		                            //                                    //                                    }
		                            //                                }
		                            //                                if (callback) { callback(); }
		                            //                            }, function (tx, err) {
		
		                            //                            });
		                        }
		                    }
		                }, function (tx, err) {
		
		                });
	                });
	                if (callback) { callback(); }
                }
                
                var indexes = [], operators = [], values = [];
                var getImages = function(tx) { 
                	var tx = todoDB.indexedDB.db.transaction('UnsyncedImages', 'readonly')
					var objStore = tx.objectStore('UnsyncedImages').count(), res;
					objStore.onsuccess = function (e) {
						stats.PendingPhotos = e.target.result;
					}
					tx.oncomplete = function() { afterImage(); }
            	}
				if (enableMultipleNbhd && stats.currNbhdId != null && Object.keys(neighborhoods).length > 1) {
					tx.executeSql("SELECT Id FROM Parcel WHERE NeighborhoodId = '" + stats.currNbhdId + "'", [], function (tx, parcels) {					
						indexes.push('ParcelId');
						operators.push('IN');
						var data = [];
						for (i = 0; i < parcels.rows.length; i++) {
			                data.push(parcels.rows.item(i));
			            }
						values.push(data.map(function(p){ return parseInt(p.Id); }));
						getImages(tx);
					})
				}
				else getImages(tx);
            });
        }
    }

    this.updateDashboard = function (nbhd) {
        $("sync-message").hide();
        stats.refreshLocal(nbhd, function () {
            var processor = function () {
                $('.dashboard-user').html(stats.UserID);
                $('.nbhd-total-parcels').html(stats.NbhdStats.TotalParcels);
                $('.nbhd-alert-parcels').html(stats.NbhdStats.AlertParcels);
                $('.nbhd-priority-parcels').html(stats.NbhdStats.PriorityParcels);
                $('.nbhd-proximity-parcels').html(stats.NbhdStats.OtherParcels);
                $('.nbhd-completed-parcels').html(stats.NbhdStats.CompletedParcels);

                $('.appr-total-parcels').html(stats.ApprStats.TotalParcels);
                $('.appr-alert-parcels').html(stats.ApprStats.AlertParcels);
                $('.appr-priority-parcels').html(stats.ApprStats.PriorityParcels);
                $('.appr-alert-parcels-rem').html(stats.ApprStats.TotalAlertsRemaining);
                $('.appr-priority-parcels-rem').html(stats.ApprStats.TotalPriorityRemaining);
                $('.appr-proximity-parcels').html(stats.ApprStats.OtherParcels);
                $('.appr-completed-parcels').html(stats.ApprStats.CompletedParcels);
                $('.appr-last-cama-update').html(toDateLiteral(stats.LastCAMAUpdate));
                $('.appr-last-data-sync').html(toDateLiteral(stats.LastCAMAUpdate));
                $('.appr-pending-sync').html(stats.PendingChanges);
                $('.appr-pending-loc').html(stats.PendingLocations);
                $('.appr-pending-photos').html(stats.PendingPhotos);

                if (EnableNewPriorities == 'True') {
                    let tblUpdate = [
                        [stats.NbhdStats.CriticalParcels, (stats.NbhdStats.CriticalParcels - stats.ApprStats.TotalCriticalRemaining), stats.ApprStats.TotalCriticalRemaining],
                        [stats.NbhdStats.AlertParcels, (stats.NbhdStats.AlertParcels - stats.ApprStats.TotalAlertsRemaining), stats.ApprStats.TotalAlertsRemaining],
                        [stats.NbhdStats.PriorityParcels, (stats.NbhdStats.PriorityParcels - stats.ApprStats.TotalPriorityRemaining), stats.ApprStats.TotalPriorityRemaining],
                        [stats.NbhdStats.MediumParcels, (stats.NbhdStats.MediumParcels - stats.ApprStats.TotalMediumRemaining), stats.ApprStats.TotalMediumRemaining],
                        [stats.NbhdStats.NormalParcels, (stats.NbhdStats.NormalParcels - stats.ApprStats.TotalNormalRemaining), stats.ApprStats.TotalNormalRemaining]
                    ]

                    rs = $('.productivity-table tbody tr');
                    rs.forEach((r, ix) => {
                        let cs = r.querySelectorAll('td');
                        for (let i = 1; i < cs.length; i++) {
                            let cl = cs[i]; cl.textContent = tblUpdate[ix][i - 1];
                        }
                    });
                }
                let {LastComment,ProfileReasonable} = neighborhoods[stats.Neighborhood.filter((x)=>{return x.NbhdId == stats.currNbhdId})[0]?.Nbhd][0]
                $('.nbhd-profile-comment').val(LastComment)
                //$('.agree-nbhd[agree="' + ProfileReasonable + '"]').click();

                $('.agree-nbhd[agree="' + ProfileReasonable + '"]').trigger(touchClickEvent);
                if (navigator.onLine && (stats.PendingPhotos > 0 || stats.PendingChanges > 0))
                    $('.showSyncBtn').show();
                else $('.showSyncBtn').hide();
                $(".sync-message").hide()
            }
            if (stats.Neighborhood.length > 1) {
                $('.dashboard-nbhd').html('<input style="width: 150px;" class="dashboard-nbhd-select"/>');
                //$('.dashboard-nbhd-select').html('<option value="${NbhdId}">${Nbhd}</option>');
                //$('.dashboard-nbhd-select').fillTemplate(stats.Neighborhood);
                let nssArr = [];
                stats.Neighborhood.forEach((nss) => {
                    nssArr.push({ Id: nss.NbhdId, Name: nss.Nbhd });
                });
                $('.dashboard-nbhd-select').removeClass('cc-drop');
                $('.dashboard-nbhd-select').empty();
                $('.dashboard-nbhd-select').lookup({ popupView: true, width: 166, searchActive: false, searchActiveAbove: 8, searchAutoFocus: false, onSave: () => { } }, []);
                $('.dashboard-nbhd-select')[0].getLookup.setData(nssArr);

                if (nbhd) $('.dashboard-nbhd-select').val(nbhd);
                processor();
                $('.dashboard-nbhd-select').change(function () {
                    stats.refreshLocal(null, function () {
                        processor();
                    });
                });
            }
            else {
                $('.dashboard-nbhd').html(stats.Neighborhood[0].Nbhd);
                processor();
            }
        });

    }
}

var prod = new ProductivityStats();

function viewSyncStatus() {
	if(stats && stats.PendingPhotos > 0 && $('.sync-status-message').html() == 'No photos to sync ..'){
		$('.sync-status-message').html('');
	}
    $(".sync-message").show();
}