﻿function loadPhotoPropertiesUI(callback) {
    var pfdiv = $('.overlay-photo-properties .photo-meta-fields');
    $(pfdiv).html('');
    getData('SELECT * FROM Field WHERE SourceTable = \'_photo_meta_data\' ORDER BY AssignedName', [], function (fields) {
        if (fields.length == 0) {
            $('.photo-box-tools .pm-props').hide();
        }
        for (var fi in fields) {
            var field = fields[fi];
            var fieldSet = getFieldSet(field, {
                trackChange: false,
                changeHandler: savePhotoProperty,
                category: { Id: "-2", Name: "_photo_properties" }
            });
            $(pfdiv).append(fieldSet);
        }
        $(pfdiv).append("<div class='spacer' style=' height: 200px'></div>")
		bindInfoButtonClick();
        if (callback) callback();
    });
}

function loadPhotoProperties(img, callback) {
    ccma.UI.ActivePhoto = img;
    let pfdiv = $('.overlay-photo-properties .photo-meta-fields');
    let fieldsetloop = () => {
        $('fieldset', pfdiv).forEach(function(fs) {
            let assignedName = $(fs).attr('assigned-name');
            $(fs).removeAttr('readonlyfieldset');
            let metaField = assignedName.replace('PhotoMetaField', 'MetaData');
            setPhotoFieldValue(fs, null, null);
        });
        if (callback) callback();
    }
    let specialQuery = false;
    if (ccma.UI.ActiveScreenId == "sort-screen" || ccma.UI.ActiveScreenId == "gis-map") {
        $('fieldset', pfdiv).forEach(function(fs) {
            let fieldId = $(fs).attr('field-id');
            let field = datafields[fieldId];
            if ((field.InputType == 5) && (field.LookupTable == '$QUERY$')) {
                if (field.LookupQuery && (field.LookupQuery.contains('parent.') || field.LookupQuery.contains('parcel.'))) {
                    specialQuery = true;
                }
            }
        });
    }
    if (specialQuery) {
        getParcel(img.ParcelId, function() {
            fieldsetloop();
        });
    } else {
        fieldsetloop();
    }
}

function savePhotoDefaultProperties(img, callback, photoMetaDataValues, isAcceptPhoto) {

    ccma.UI.ActivePhoto = img;
    var pfdiv = $('.overlay-photo-properties .photo-meta-fields');
    $('fieldset', pfdiv).forEach(function (fs) {
        var fieldId = $(fs).attr('field-id');
        var field = datafields[fieldId];
        var value = field.DefaultValue;
        var assignedName = field.AssignedName;
        var metaField = assignedName.replace('PhotoMetaField', 'MetaData');
        if(_.has(photoMetaDataValues, metaField) == true)
        	value = _.values(_.pick(photoMetaDataValues, metaField))[0];
        if (field.DefaultValue == null && field.AutoSelectFirstitem == "true" && field.InputType == "5") {
            getLookupData(field, { category: -2, source: img, metaField: metaField }, function (ld, img, o) {
                var metaField = o.metaField;
                if (ld.length == 0) { if (callback) callback(); else return false; }
                value = ld[1]? ld[1].Id : ld[0].Id;
                if(_.has(photoMetaDataValues, metaField) == true)
        			value = _.values(_.pick(photoMetaDataValues, metaField))[0];
                img[metaField] = value;
                ccma.UI.ActivePhoto[metaField] = value;
                photos.images[photos.index][metaField] = value;
                if (clientSettings?.PhotoTagMetaField && clientSettings?.PhotoTagMetaField != '' && clientSettings?.PhotoTableLinkMetaField && clientSettings?.PhotoTableLinkMetaField != '') photos.metadataedit = true;
                ccma.Sync.enqueueCommand(img.Id || img.LocalId, 'PhotoMetaData', metaField, value, { updateData: true, source: 'Images', sourceKey: (img.Id != null ? 'Id' : 'LocalId'), sourceKeyValue: img.Id || img.LocalId, sourceField: metaField }, function () {
                    if (callback) callback();
                });
            });
        }
        else {
        	value = getDefaultValue(field, value);
        	if (!value && field.InputType == '12') {
        		let location = UsingOSM? OSMmyLocation: myLocation;    	
    			if (geo && geo.location && geo.location.longitude && geo.location.latitude)
    				location = UsingOSM ? new L.latLng(geo.location.latitude, geo.location.longitude) : new google.maps.LatLng(geo.location.latitude, geo.location.longitude);
    			if (location)
    				value = UsingOSM? (location.lat.toFixed(8) + "," + location.lng.toFixed(8)): (location.lat().toFixed(8)  + "," +  location.lng().toFixed(8)); 
        	}
            ccma.UI.ActivePhoto[metaField] = value;
            photos.images[photos.index][metaField] = value;
            if (value !== null) {
                if (clientSettings?.PhotoTagMetaField && clientSettings?.PhotoTagMetaField != '' && clientSettings?.PhotoTableLinkMetaField && clientSettings?.PhotoTableLinkMetaField != '') photos.metadataedit = true;
                ccma.Sync.enqueueCommand(ccma.UI.ActivePhoto.Id || ccma.UI.ActivePhoto.LocalId, 'PhotoMetaData', metaField, value, { updateData: true, source: 'Images', sourceKey: (ccma.UI.ActivePhoto.Id != null ? 'Id' : 'LocalId'), sourceKeyValue: ccma.UI.ActivePhoto.Id || ccma.UI.ActivePhoto.LocalId, sourceField: metaField }, function () {
                    if (callback) callback();
                });
            }
        }
        //        var save = function () {
        //            ccma.UI.ActivePhoto[metaField] = value;
        //            photos.images[photos.index][metaField] = value;
        //            ccma.Sync.enqueueCommand(ccma.UI.ActivePhoto.Id || ccma.UI.ActivePhoto.LocalId, 'PhotoMetaData', metaField, value, { updateData: true, source: 'Images', sourceKey: (ccma.UI.ActivePhoto.Id != null ? 'Id' : 'LocalId'), sourceKeyValue: ccma.UI.ActivePhoto.Id || ccma.UI.ActivePhoto.LocalId, sourceField: metaField }, function () {
        //                if (callback) callback();
        //            });

        //        }
    });


}
function savePhotoProperty(el, callback) {
    var enqueue = true;
    var fieldId = $(el).attr('field-id');
    var field = datafields[fieldId];
    var value = $(el).val();
    if (field.InputType == 5) {
        let tValue = value;
        try {
            value = decodeURI(tValue);
        } catch {
            value = tValue;
        }
    }
    if(field.InputType == 13)
    	var value = $('input:checked',el).val();
    var assignedName = field.AssignedName;
    var metaField = assignedName.replace('PhotoMetaField', 'MetaData');
    if (ccma.UI.ActivePhoto[metaField] == value) {
        enqueue = false;
    }
    ccma.UI.ActivePhoto[metaField] = value;
    if(photos.images.length){photos.index = photos.index || 0; photos.images[photos.index][metaField] = value;}
     if(field && field.IsUniqueInSiblings == "true"){
        if(field.InputType == '5') {
            let lp = $(el) && $(el)[0] && $(el)[0].getLookup;
            if(!lp) return false;
            for (var k of Object.keys(lp.rows)) {
                lp.rows[k]._disabled =false;
            }
            photos.images.forEach(function(opt){
                let sid = opt[metaField];
                for (var k of Object.keys(lp.rows)) {
                    if(lp.rows[k].Id == sid) lp.rows[k]._disabled = true;
                }
            });
        } else {
            $('select option',el).removeAttr('disabled');
            photos.images.forEach(function(opt){
                $('select option[value = "'+opt[metaField]+'"]', el).attr('disabled','disabled');
            });
        }
    }
    if (enqueue) {
        if (clientSettings?.PhotoTagMetaField && clientSettings?.PhotoTagMetaField != '' && clientSettings?.PhotoTableLinkMetaField && clientSettings?.PhotoTableLinkMetaField != '') photos.metadataedit = true;
        ccma.Sync.enqueueCommand(ccma.UI.ActivePhoto.Id || ccma.UI.ActivePhoto.LocalId, 'PhotoMetaData', metaField, value, { updateData: true, source: 'Images', sourceKey: (ccma.UI.ActivePhoto.Id != null ? 'Id' : 'LocalId'), sourceKeyValue: ccma.UI.ActivePhoto.Id || ccma.UI.ActivePhoto.LocalId, sourceField: metaField }, function () {
            getData("SELECT * FROM Parcel WHERE  Id = " + ccma.UI.ActivePhoto.ParcelId + "", [], function (p) {
                refreshSortScreen = true;
                executeQueries("UPDATE Parcel SET Reviewed = 'false' WHERE Id = " + ccma.UI.ActivePhoto.ParcelId)
            });
        });
    }
    refreshRelatedPhotoFields(field, savePhotoProperty);

    if (callback) callback();
}

function refreshRelatedPhotoFields(field, saveMethod) {
    var rfields = ccma.UI.RelatedFields[field.Id];
    if ((rfields == undefined) || (rfields == null)) {
        return false;
    }

    for (var x in rfields) {
        var sfield = datafields[rfields[x]];
        $('fieldset[field-id="' + sfield.Id + '"]').each(function () {
            var el = $('.newvalue', this);
            setPhotoFieldValue(this, null, null, function (sfield) {
                if (el.length > 0) {
                    saveMethod(el[0], function () {
                        refreshRelatedPhotoFields(sfield, saveMethod);
                    });
                } else {
                    refreshRelatedPhotoFields(sfield, saveMethod);
                }
            }, sfield, '');
        });

    }
}


function setPhotoFieldValue(fieldset, source, customddl, callback, callbackdata, defaultValue) {
    var fieldProcessor = function (callback) { if (callback) callback(); }
    var fval = "";
    var fname = $(fieldset).attr('field-name');
    var ftype = $(fieldset).attr('field-type');
    var fieldId = $(fieldset).attr('field-id');

    var field = datafields[fieldId];
    var assignedName = field.AssignedName;
    var metaField = assignedName.replace('PhotoMetaField', 'MetaData');
    
    if (field) {
        if ((field.InputType == 5) /*&& (field.LookupTable == '$QUERY$')*/) {
            var cat = { Id: -2 }
            ds = ccma.UI.ActivePhoto;
            getLookupData(field, { category: cat, source: ds }, function (ld, source) {
                var elem = $('.newvalue', $(fieldset));
                $(elem)[0].getLookup.setData(ld);
                
                if(field && field.IsUniqueInSiblings == "true") {
                    let lp = $(elem)[0].getLookup;
                    for (var k of Object.keys(lp.rows)) {
                        lp.rows[k]._disabled = false;
                    }
                    photos.images.forEach(function(opt){
                        let sid = opt[metaField];
                        for (var k of Object.keys(lp.rows)) {
                            if(lp.rows[k].Id == sid) lp.rows[k]._disabled = true;
                        }
                    });
                }

                //$(elem).html('<option value="${Id}">${Name}</option>');
                //$(elem).fillTemplate(ld);
                //isCustomddl = false;
                //if (ld && ld.length > 25) {
                   // $(elem).attr('customddl', '');
                //}
                
                ccma.UI.ActivePhoto = source;
                var v = { Value: ccma.UI.ActivePhoto[metaField] || null };
                var lkVal = v.Value? (ld.filter(function(x) { return encodeURI(x.Id) == encodeURI(v.Value.toString()); })[0]? ld.filter(function(x) { return encodeURI(x.Id) == encodeURI(v.Value.toString()); })[0]: null): null;
				var nonexists = !lkVal? true: false;
				var disVal = lkVal && lkVal.Name ? lkVal.Name: (lkVal? v.Value: '');
				$(elem).show();
				$(elem).siblings('.cusDpdown-arrow').show();
				//$(elem).parent('.cusDpdown').show();
                // $('.newvalue', $(fieldset)).html(disVal);

                refreshRelatedPhotoFields(field, savePhotoProperty);
                $('label', $(fieldset)).attr({ 'value': '', 'oldvalue': '' });
                //var v = { Value: ccma.UI.ActivePhoto[metaField] || null };
                setElementValue('.newvalue', fieldset, v.Value, ftype, field.DefaultValue, field.AutoSelectFirstitem, function () {
                    if (callback) callback(callbackdata);
                }, ld, nonexists);
            });

        }
        else if(parseInt(ftype) == 13) {
            var radioOptions = [];
            if (field.RadioFieldValues && field.RadioFieldValues != "") {
                var values = []; 
                values = field.RadioFieldValues.trim().split(',');
                radioOptions = [{ name : field.Id, text: 'Yes', value: values[0] }, { name : field.Id, text: 'No', value: values[1] }];
            }
            else
                radioOptions = [{ name : field.Id, text: 'Yes', value: 'true' }, { name : field.Id, text: 'No', value: 'false' }];
            $('radiogroup', $(fieldset)).css({'width': '131px'});
            if(field.AllowRadioNull == "true"){
                var nullOption = { name : field.Id, text: 'Blank', value: '' }
                radioOptions.push(nullOption);
                $('radiogroup', $(fieldset)).css({'width':'210px'});
            }
            var rbutton = $('.RadioButtonGroup',$(fieldset))[0];
            $(rbutton).html('<input type="radio" class="radioButton" name=${name} value=${value}>${text}</input>&nbsp;');
            $(rbutton).fillTemplate(radioOptions)
            $(fieldset).append(rbutton);
            $('input', $(fieldset)).removeAttr('checked'); 
            var v = { Value: ccma.UI.ActivePhoto[metaField] || null };
            $('label', $(fieldset)).attr({ 'value': '', 'oldvalue': '' });
            setElementValue('.newvalue', fieldset, v.Value, ftype, field.DefaultValue, field.AutoSelectFirstitem, function () {
                    if (callback) callback(callbackdata);
            });
        }
        else if (fname != null) {
            fieldProcessor(function () {
                $('label', $(fieldset)).attr({ 'value': '', 'oldvalue': '' });
                var v = { Value: ccma.UI.ActivePhoto[metaField] || null };
	            if(field && field.IsUniqueInSiblings == "true") {
		            $('select option',fieldset).removeAttr('disabled');
						photos.images.forEach(function(opt){
							$('select option[value = "'+opt[metaField]+'"]',fieldset).attr('disabled','disabled');
						});
		        }
                setElementValue('.newvalue', fieldset, v.Value, ftype, field.DefaultValue, field.AutoSelectFirstitem, function () {
                    if (callback) callback(callbackdata);
                });
            });
        }
        //FD_26439
        if (field.ReadOnly == "true" || field.IsReadOnlyMA=="true") {
        	$(fieldset).attr('disabled', 'disabled');
        	if (parseInt(ftype) == 12) { $('.LoadGeolocationIcon', $(fieldset)).hide(); $('.ClickGeoLocation', $(fieldset)).hide(); }
        }
        else {
        	$(fieldset).removeAttr('disabled');
        	if (parseInt(ftype) == 12) { $('.LoadGeolocationIcon', $(fieldset)).show(); $('.ClickGeoLocation', $(fieldset)).show(); };
        }
     }

}

function setElementValue(selector, fieldset, value, ftype, defaultValue, AutoSelectFirstitem, callback, lkdVals, nonexists) {
    var el = $(selector, fieldset)[0];
    if (parseInt(ftype) == 11){
        ccma.Data.Evaluable.setLookupList(el, ((value) ? value : '').split(','));
    } else {
    	if (parseInt(ftype) == 5 && nonexists)
    		$(el).val('');
    	else
        	$(el).val(value)
    }
    if ((value == '' || !value) && ftype == 12) {
        if ($('.ClickGeoLocation', fieldset).length == 0) {
            var cLink = document.createElement('a');
            $(cLink).html("Click to add Geo Location");
            $(cLink).addClass("ClickGeoLocation");
            $(cLink).attr({ 'onclick': 'GeolocationLoad(this);' });
            $(cLink).attr({ 'href': '#' });
            $(el).before(cLink);
            $(el).hide();
        }
        $('.ClickGeoLocation', fieldset).show();
        $(el).hide();
    }
    if (value != '' && ftype == 12 && value) {
        $('.ClickGeoLocation', fieldset).hide();
        $(el).show();
    } 
    if (parseInt(ftype) == 11 || parseInt(ftype) == 5 || parseInt(ftype) == 3) {
        if ( ( (value == null) || ( ( parseInt(ftype) == 5 && lkdVals && lkdVals.findIndex(element => element.Id == value) === -1 ) || ( parseInt(ftype) != 5 && $(el).html().indexOf('value="' + value + '"') == -1 ) ) ) && defaultValue == null && AutoSelectFirstitem=='true' ) {
            if (parseInt(ftype) == 5 && lkdVals ) {
            	if(!lkdVals[0]) {
            		$(el).val(''); $(el).html('');
            	}
            	else {
	            	var lkv = ((lkdVals[0].Id == '' || lkdVals[0].Id == '<blank>' || lkdVals[0].Value == '<blank>') && lkdVals[1])? lkdVals[1]: lkdVals[0];
	            	var lId = !lkv.Id && lkv.Value ? (lkv.Value == '<blank>'? '': lkv.Value): lkv.Id;
	            	$(el).val(lId);
	            	var disNm = lkv.Name? lkv.Name : (lkv.Id? lkv.Id: '');
	            	// $(el).html(disNm);
            	}
            }
            else {
            	if ($(el).find('option:first-child').val() == '')
                	$(el)[0].selectedIndex = 1;
            	else
                	$(el)[0].selectedIndex = 0;
            }
            /*if ($(el).find('option:first-child').val() == '')
                $(el)[0].selectedIndex = 1;
            else
                $(el)[0].selectedIndex = 0;*/
            savePhotoProperty(el, callback);
        }
    }
    if (parseInt(ftype) == 13){ 
    	$('input', $(el)).removeAttr('checked');
        $('input[value = "'+ value +'"]', $(el)).prop( "checked", true );
        savePhotoProperty(el, callback);
    }
    if (value == null && defaultValue != null) {
    	let fieldId = $(el).attr('field-id');
        let field = datafields[fieldId];
        defaultValue = getDefaultValue(field, defaultValue)
        $(el).val(defaultValue);
        if (parseInt(ftype) == 5 && lkdVals ) {
        	var lkv = lkdVals.filter(function(ld) { return ld.Id == defaultValue })[0];
        	var disNm = lkv && lkv.Name ? lkv.Name: (lkv? defaultValue: '');
        	var cval = lkv ? defaultValue: '';
        	$(el).val(cval);
        	// $(el).html(disNm);
        }
        else if(parseInt(ftype) == 5) {
        	$(el).val('');
        	$(el).html('');
        }
        savePhotoProperty(el, callback);
    } else{
        if (callback) callback();
    }
}
function setFirstElementInLookup(el) {

}