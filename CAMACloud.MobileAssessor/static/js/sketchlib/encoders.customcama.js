﻿CAMACloud.Sketching.Encoders.CustomCAMATraverse = {
    name: 'CustomCAMATraverse',
    decode: function (sketch) {
        if (!sketch) return sketch;
        if (sketch.indexOf('[-1,-1') > -1) return sketch;          //temporary pass-through for saved MA-Sketches, as no Save to CCT is done yet.
        var label;
        var segments = [], sindex = 0;

        function _fc() { return sketch[0] || false; }
        function _read(l) { var x = sketch.substr(0, l); sketch = sketch.substr(l).trim(); return x; }
        function _2v(vs) { var dir = vs[0], mag = parseFloat(vs.substr(1)); return { dir: dir, len: mag, veer: "NEWS".indexOf(dir) > -1 }; }
        function _isl(v) { return v && v.match(/[A-Z]{3}/); }

        function _2xy(v) {
            var xy = { x: 0, y: 0 }
            switch (v.dir) {
                case "U": case "N": xy.y -= v.len; break;
                case "D": case "S": xy.y += v.len; break;
                case "L": case "W": xy.x -= v.len; break;
                case "R": case "E": xy.x += v.len; break;
            }
            xy.veer = "NEWS".indexOf(v.dir) > -1;
            return xy;
        }

        function _2ma(xy) {
            var LR = (xy.x && ((xy.x < 0 ? 'L' : 'R') + Math.abs(xy.x)) || '');
            var UD = (xy.y && ((xy.y < 0 ? 'U' : 'D') + Math.abs(xy.y)) || '');
            if (LR && UD) return LR + ' ' + UD;
            return LR || UD;
        }

        function _readSegment(label, sp) {
            var segnum = sindex++;
            sp = sp || { x: 0, y: 0 };
            var seg = { label: label, start: { x: sp.x, y: sp.y }, vectors: [] };

            if (_fc() == '(') {
                _read(1);		//read and remove the opening bracket.
                var lv = null, lparts = null;
                while (_fc() != ')') {
                    var m = /(([A-Z]{3}[0-9]*[;]?)+)|([A-Z]\d+(\.\d+)?)/g.exec(sketch);
                    if (m && m[0]) {
                        var vector = _read(m[0].length);
                        if (_isl(vector)) {
                            _readSegment(vector, sp);
                        } else {
                            var re = /([A-Z]\d+(\.\d+)?)/g
                            while (mv = re.exec(vector)) {
                                var vpart = mv[1], vx = _2v(vpart), xy = _2xy(vx);
                                sp.x += xy.x; sp.y += xy.y;
                                if (!lv || !lv.veer || !vx.veer) { lparts = []; seg.vectors.push(lparts); }
                                lparts.push(_2ma(xy));
                                lv = vx;
                            }
                        }
                    }
                }
                _read(1);		//read and remove the closing bracket.
                seg.masketch = Base64.encode((seg.label || '').replace(';', '\n')) + '[-1,-1]:' + _2ma(seg.start) + ' S' + seg.vectors.map(function (x) { return x.join('/'); }).reduce((x, y) => { return x + ' ' + y }, '');
                segments[segnum] = seg;		//set the segment on the proper index, as recursion is being used.
            }
        }

        var iter = 0;
        var re = /(([A-Z]{3}[0-9]*[;]?)+)/g
        var m;
        while (sketch.length > 0) {
            m = re.exec(sketch);
            if (m && _isl(m[0])) {
                label = _read(m[0].length);
                _readSegment(label);
            }

            if (sketch == '.' || sketch == '') break;
            if (iter++ > 100) break;
        }

        var masketch = segments.map((x) => { return x.masketch }).join(';');
        return masketch;
    },
    encode: function (masketch) {
        return masketch;
    }
}