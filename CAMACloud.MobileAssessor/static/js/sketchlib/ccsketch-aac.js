﻿
CAMACloud.Sketching.Formatters.AAC = {
    name: "CAMACloud.SketchFormat",
    saveAllSegments: true,
    allowSegmentAddition: false,
    allowSegmentDeletion: false,
    arcMode: 'ARC',
    vectorSeperator: ";",
    newVectorHeaderSuffix: '[-1,-1]:',
    nodeToString: function (n) {
        var str = "";
        if (n.arcLength > 0) {
            str += "A" + sRound(n.arcLength);
        }
        if (n.arcLength < 0) {
            str += "B" + Math.abs(sRound(n.arcLength));
        }
        if (n.dy > 0) {
            if (n.isStart) {
                if (str != "") str += " ";
            } else {
                if (str != "") str += "/";
            }
            str += n.sdy + sRound(n.dy);
        }
        if (n.dx > 0) {
            if (n.isStart) {
                if (str != "") str += " ";
            } else {
                if (str != "") str += "/";
            }
            str += n.sdx + sRound(n.dx);
        }
        if (n.hideDimensions) {
            if (str != "") str += "/";
            str += "#";
        }
        if (n.isStart) {
            str += " S";
        }
        if (n.vector.placeHolder && n.isStart) {
            str += " P" + n.vector.placeHolderSize.toString();
        }
        return str;
    },
    vectorToString: function (v) {
        var str = "";
        var label =  v.name || '';
        if (v.startNode != null) {
            if (!v.labelEdited && !v.newRecord) {
                if (v.header) {
                    var r1 = /(.*?)\[(.*?)\]/;
                    var headpart = r1.exec(v.header);
                    var labels = "", refString = "[-1,-1]";
                    if (headpart != null) {
                        labels = headpart[1];
                        refString = "[" + headpart[2] + "]"
                    }
                    var headerValue = labels + refString + ":";
                    str += headerValue;
                } else {
                    if (v.referenceIds) {
                        str += label + "[" + v.referenceIds + "]:";
                    } else {
                        if (v.rowId)
                            str += label + "[" + v.rowId + ",-1]:";
                        else
                            str += label + "[-1,-1]:";
                    }

                };
            }
            else {
            	if(!v.referenceIds)
                   v.referenceIds = '-1,-1';
                str += label + "[" + v.referenceIds + "," + this.labelPositionToString(v.labelPosition) + "," + label + "," + (v.sketchType == 'outBuilding'? '1': '0') + "," + (v.isUnSketchedArea? '1': '0') + "," + (v.isUnSketchedArea? v.fixedAreaTrue: v.area()) + "," + (v.isUnSketchedArea? v.fixedPerimeterTrue: v.perimeter()) + "]:";
            }
            str += v.startNode.vectorString();
            var nn = v.startNode.nextNode;
            while (nn != null) {
                str += " ";
                str += nn.vectorString();
                nn = nn.nextNode;
            }
        }
        return str;
    },
    vectorFromString: function (editor, s) {
        if ((s || '').trim() == '') {
            var nv = new Vector(editor);
            nv.isClosed = false;
            nv.label = "Blank"
            nv.uid = -1;
            return nv;
        }

        var o = editor.origin;
        var p = o.copy();

        var v = new Vector(editor);
        this.updateVectorFromString(editor, v, s);
        v.isModified = false;
        return v;
    },
    updateVectorFromString: function (editor, v, s) {
        v.startNode = null;
        v.endNode = null;
        var o = editor.origin;
        var p = o.copy();

        var hi = s.split(':');
        var head = hi[0];
        var lineString = hi[1] || '';
        var r1 = /(.*?)\[(.*?)\]/;
        var headpart = r1.exec(head);
        var labels = "", refString = "[-1,-1]";
        if (headpart != null) {
            labels = headpart[1];
            refString = "[" + headpart[2] + "]"
        }
        v.header = labels + refString + ":";
        var isStart = false;
        var hasStarted = false;
        var isVeer = false;
        var veerSteps = 0;
        var nodeStrings = lineString.split(' ');
        for (var i in nodeStrings) {
            var arcLength = 0;
            var hideDimensions = false;
            var isPlaceholder = false;
            var ns = nodeStrings[i];
            if (ns.trim() != '') {
                var cmds = ns.split('/');
                for (var c in cmds) {
                    var cmd = cmds[c];
                    if (cmd == '') continue;
                    if (cmd == 'S') {
                        isStart = true;
                    } else {
                        var cp = cmd.match(/[ABUDLR#PC]|[0-9]+(.[0-9]+)?/g);
                        var dir = cp[0];
                        distance = cp[1] || 0;
                        var pix = CAMACloud.Sketching.Utilities.toPixel(distance);
                        switch (dir) {
                            case "A": arcLength = Number(distance); break;
                            case "B": arcLength = -Number(distance); break;
                            case "U": p.moveBy(0, pix); break;
                            case "D": p.moveBy(0, -pix); break;
                            case "L": p.moveBy(-pix, 0); break;
                            case "R": p.moveBy(pix, 0); break;
                            case "#": hideDimensions = true; break;
                            case "P": isPlaceholder = true; break;
                        }
                    }
                }

                p = p.copy();

                if (isPlaceholder) {
                    v.placeHolder = true;
                    v.placeHolderSize = Number(distance);
                    v.terminateNode(v.startNode);
                }
                else if (isStart) {
                    v.start(p.copy());
                    hasStarted = true;
                    isStart = false;
                } else if (hasStarted) {
                    var q = p.copy().alignToGrid(editor);
                    if (!v.startNode.overlaps(q))
                        v.connect(p.copy());
                    else
                        v.terminateNode(v.startNode);
                }

                if (v.endNode) {
                    v.endNode.hideDimensions = hideDimensions;
                    v.endNode.arcLength = arcLength;
                    if (arcLength != 0)
                        v.endNode.isArc = true;
                }

            }
        }

        if (v.startNode)
            if (v.startNode.overlaps(v.endNode.p.copy()))
                v.isClosed = true;
        return v;
    },
    labelPositionFromString: function (editor, l) {
        if (!l)
            return null
        l = l.replace(' ', '');
        var ins = l.match(/[UDLR]|[0-9.]*/g);
        if (ins.length > 3) {
            var x = ins[0] == "R" ? ins[1] : -ins[1];
            var y = ins[2] == "U" ? ins[3] : -ins[3];
            return new PointX(parseFloat(x), (parseFloat(y)))
        }

    },
    labelPositionToString: function (p) {
        var x = p.x > 0 ? "R" + p.x : "L" + -p.x;
        var y = p.y > 0 ? "U" + p.y : "D" + -p.y;
        return x + " " + y
    },
    open: function (editor, data) {
        editor.vectors = [];
        editor.sketches = [];
        let isError = false;

        for (var x in data) {
            var sketch = new Sketch(editor);
            var s = data[x];
            sketch.parentRow = s.parentRow;
            sketch.uid = s.uid;
            sketch.label = s.label;
            sketch.vectors = [];
            sketch.config = s.config || {};
            sketch.sid = s.sid;
            sketch.lookUp = s.lookUp;
            sketch.GroupingValue = s.GroupingValue;
            sketch.boundaryScale = s.boundaryScale;
            sketch.maxNotelen = s.maxNotelen;
            var counter = 0;
            try {
                for (var i in s.sketches) {
                    var sk = s.sketches[i];
                    var segments = (sk.vector || '').split(';');
                    var scount = 0;
                    if (segments.length < 2) scount = -1;
                    for (var si in segments) {
                        counter += 1;
                        scount += 1;
                        var sv = segments[si];
                        var v = this.vectorFromString(editor, sv);
                        v.sketch = sketch;
                        v.uid = sk.uid + (scount ? '/' + scount : '');
                        v.index = counter;
                        v.name = sk.label && sk.label[0].Value ? sk.label[0].Value : (sk.imp_Type || '');
                        v.label = sk.label && sk.label[0].Value ? '[' + counter + '] ' + sk.label.map(function (a) { return ((a.Description && sketchSettings["DoNotShowLabelDescriptionSketch"] != '1' && clientSettings["DoNotShowLabelDescriptionSketch"] != '1' && a.Description.Name) ? ((sketchSettings["DoNotShowLabelCode"] == '1' || a.showLabelDescriptionOnly) ? a.Description.Name : a.Value + '-' + a.Description.Name) : a.Value) }).filter(function (a) { return a }).join('/') : '[' + counter + '] ' + (sk.imp_Type || '')
                        v.labelFields = sk.label;
                        if (sk.label[0] && sk.label[0].colorCode) {
                            var clrcode = sk.label[0].colorCode.split('~');
                            v.colorCode = clrcode[0] ? clrcode[0] : null;
                            v.newLineColor = (clrcode[1] && sketchSettings['SketchLineColorCustomizations'] && sketchSettings['SketchLineColorCustomizations'].contains('MA')) ? clrcode[1] : null;
                            v.newLabelColorCode = (clrcode[2] && sketchSettings['SketchTextColorCustomizations'] && sketchSettings['SketchTextColorCustomizations'].contains('MA')) ? clrcode[2] : null;
                        }
                        v.vectorString = sv;
                        v.referenceIds = sk.referenceIds;
                        v.rowId = sk.rowId;
                        v.isChanged = sk.isChanged;
                        v.vectorConfig = sk.vectorConfig;
                        v.noVectorSegment = sk.noVectorSegment;
                        this.AreaUnit = sk.areaUnit;
                        v.labelPosition = this.labelPositionFromString(editor, sk.labelPosition);
                        if (sk.otherValues) {
                            for (var keys in sk.otherValues) {
                                v[keys] = sk.otherValues[keys];
                            }
                        }
                        editor.vectors.pop();
                        sketch.vectors.push(v);
                    }
                }
            }
            catch (e) {
                editor.vectors = []; sketch.vectors = []; isError = true;
            }
            sketch.isModified = false;
            editor.loadNotesForSketch(sketch, s.notes);
            editor.sketches.push(sketch);
        }

        if (isError) {
            editor.sketches.forEach((sk) => { sk.vectors = []; });
            throw "Sketch cannot be rendered due to the wrong sketch data.";
        }
    },
    getParts: function (vectorString) {
        var p = [];
        var parts = (vectorString || '').split(";");
        for (var i in parts) {
            var part = parts[i];
            if (/(.*?):(.*?)/.test(part)) {
                var hi = part.split(':');
                var head = hi[0];
                var info = hi[1];
                var labels, refString, labelPosition = null, otherValues = {};

                var r1 = /(.*?)\[(.*?)\]/;
                var hparts = r1.exec(head);
                if (hparts == null) {
                    labels = "";
                    refString = "-1,-1";
                } else {
                    labels = hparts[1].replace(/\{(.*?)\}/g, '');
                    refString = hparts[2];
                    var t = refString.split(',')
                    if (t.length > 2) { 
                    	refString = t[0] + ',' + t[1]; 
                    	labelPosition = t[2];
						//otherValues.code =  t[3]? t[3]: '';
						otherValues.sketchType = t[4] && t[4] == '1'? 'outBuilding': 'sketch';
						otherValues.isUnSketchedArea = t[5] && t[5] == '1'? true: false;
						otherValues.currentVectorArea = t[6] || t[6] == 0? t[6]: null;
						if (otherValues.isUnSketchedArea && (t[6] || t[6] == 0) && (t[7] || t[7] == 0)) {
							otherValues.fixedAreaTrue =  parseFloat(t[6]);
							otherValues.fixedPerimeterTrue = parseFloat(t[7]);
						}					
                    }
                }
                p.push({
                    label: labels,
                    vector: part,
                    referenceIds: refString,
                    labelPosition: labelPosition,
                    otherValues: otherValues
                });
            }
        }

        return p;
    }
}

var outBuildingAACLabelDefinition = function (data, config, options) {
	let head = options.head;
	let items = options.items;
	let type = options.action;
	let callback = options.callback;
    if(head == "New Vector - Labels")
		head = "New Outbuilding - Labels";
    else if (type == 'edit') 
        head = "Edit Outbuilding - Labels";
    $('.skNote').hide(); $('.Current_vector_details .vectorPage , .Current_vector_details .footradio,.Current_vector_details .calculatedradio,.Current_vector_details .obsketch').parent().remove()
    $('.assocValidation, .UnSketched, .linked-div, .SectNumDropDown, .PnANewValue').remove();
    $('.dynamic_prop div').remove();
    $('.Current_vector_details .head').html(head)
    $('.mask').show();
    

    $( '.Current_vector_details .head' ).after( '<div class ="outbdiv"> <div class="obsketch"><label class ="checkboxlabel" style="float: left; display: none;"><input class="obcheckbox" type="checkbox"  style="width: 20px;margin-right:7px;margin-top: -3px;"/>Draw Sketch</label><span class="oarea"><label style="float: left; width: 98px;">Sqft</label><input class="outarea" type="number" style="width: 65px;margin-top: -3px;float: right; height: 25px;"></span><span class="operi" style="margin-left: 40px;"><label style="float: left; width: 80px;">Perimeter</label><input class="peri" type="number" style="width: 65px;margin-right:7px;margin-top: -3px;float: right;height: 25px;"></span></div><div><label style="float: left;width: 98px;"> Label</label><select class="obddl" style="display: none;"></select><input class = "outTypeText" type = "textbox" style="display: none; width: 267px"/><div><span class = "identM" style = "display : none; color: #e00909; min-width: 12px; margin: 4px; margin-left: 100px;">Please Check all Values</span></div></div></div>' );
    $( '.obddl' ).empty();
    $( '.obddl' ).append('<option></option>');
    
    var values = sketchApp.currentSketch.config.SketchSource.Table == 'res_building_tb'? lookup['res_sketch_lookup']: lookup['com_sketch_lookup'];
    var isOutLookup = false;
    if (values && !_.isEmpty(values)) {
        isOutLookup = true;
        values = Object.keys(values).map(function (x) {
            return values[x]
        }).sort(function (a, b) {
            return a.Ordinal - b.Ordinal
        });
        for (var x in values) {
            $( '.obddl' ).append( '<option value="' + values[x].Id + '">' + ( (sketchSettings['DoNotShowLabelCodeInDropDown'] == '1' && values[x].Name)? values[x].Name: (sketchSettings['DoNotShowLabelDescription']? values[x].Id: (values[x].Id + '-' + values[x].Name)) ) + '</option>' );
        }
    }
    $( '.Current_vector_details' ).css( 'width', 480 );
    $( '.Current_vector_details' ).show();

    if (isOutLookup)
        $('.obddl').show();
    else
        $('.outTypeText').show();

    if (type == 'edit') {
        isOutLookup? $( '.obddl' ).val(items[0].Value): $('.outTypeText').val(items[0].Value);
        if (!sketchApp.currentVector.isUnSketchedArea) {
            $( ".obcheckbox" ).prop( 'checked' , true)
            $('.obsketch').hide();
            $( ".obcheckbox" ).hide();
        }
        else {
            $('.outarea').val(sketchApp.currentVector.fixedAreaTrue);
            $('.peri').val(sketchApp.currentVector.fixedPerimeterTrue)
        }
    }
    $( '.Current_vector_details #Btn_cancel_vector_properties' ).unbind(touchClickEvent)
    $( '.Current_vector_details #Btn_cancel_vector_properties' ).bind(touchClickEvent, function () {
        $( '.Current_vector_details' ).hide();
        $( '.Current_vector_details .outbdiv' ).remove();
        if ( !sketchApp.isScreenLocked ) $( '.mask' ).hide();
        return false;
    });
    
    $('.obddl').unbind('change');
    $('.obddl').bind('change', function () {
        $('.identM').hide();
    });

    $('.obcheckbox').unbind('change');
    $('.obcheckbox').bind('change', function () {
        if ($( ".obcheckbox" ).prop( 'checked' )) {
            $('.oarea').hide(); $('.operi').hide();            
        }
        else {
            $('.oarea').show(); $('.operi').show();
            $('.outarea').css('border', ''); $('.peri').css('border', ''); 
			$('.outarea').val(""); $('.peri').val("");
        }
    });
	
    $('.oarea').on('change keyup', function (t) {
		$('.outarea').css('border', '');
		var sqfttt = $('.outarea').val();
		if (sqfttt && sqfttt.length > 10) {
			sqfttt = sqfttt.slice(0, -1);
			$('.outarea').val(sqfttt);
		}
	});
	
	$('.peri').on('change keyup', function (t) {
		var peri = $('.peri').val();
		$('.peri').css('border', '');
		if (peri && peri.length > 10) {
			peri = peri.slice(0, -1);
			$('.peri').val(peri);
		}
	});

    $( '.Current_vector_details #Btn_Save_vector_properties' ).unbind( touchClickEvent )
    $( '.Current_vector_details #Btn_Save_vector_properties' ).bind( touchClickEvent, function () {
        var val = isOutLookup? $( '.obddl' ).val(): $('.outTypeText').val();
        var lkval = isOutLookup? values.filter(function(x) { return x.Id == val })[0]: null;
        var name = lkval && lkval.Name? lkval.Name: (!isOutLookup? val: null);
        var desc = lkval && lkval.Description? lkval.Description: null; 
        var area = $('.outarea').val()? parseFloat($('.outarea').val()): 0;
        var peri = $('.peri').val()? parseFloat($('.peri').val()): 0;
        if (val == '') {
            $('.identM').show();
            return false;
        }
        
        var draw_outbuilding = $( ".obcheckbox" ).prop( 'checked' );
		if (!draw_outbuilding) {
			let areaSketch = true;
			let sqftvalue = $('.outarea').val() ? $('.outarea').val(): 0;
			let perivalue = $('.peri').val() ? $('.peri').val() : 0;
			if (!sqftvalue || parseFloat(sqftvalue) < 1) {
            	$('.outarea').css('border', '1px solid red');
            	return false;
            }
            else if (parseFloat(perivalue) < 0) {
                $('.peri').css('border', '1px solid red');
            	return false;
            }
		}       

        items[0].Value = val;
        items[0].NameValue = name;
        items[0].Description = lkval? lkval: null;

        if ( type == 'edit' && draw_outbuilding && !sketchApp.currentVector.isUnSketchedArea) {
            sketchApp.currentVector.labelEdited = true;
            sketchApp.currentVector.isUnSketchedArea = false;
            sketchApp.currentVector.label = '[' + sketchApp.currentVector.index + '] ' + (items[0].NameValue? items[0].Value + '-' + items[0].NameValue: items[0].Value);
        }
        else if ( type == 'edit' && draw_outbuilding && sketchApp.currentVector.isUnSketchedArea) {
            sketchApp.currentVector.vectorString = null;
            sketchApp.currentVector.startNode = null;
            sketchApp.currentVector.endNode = null;
            sketchApp.currentVector.commands = [];
            sketchApp.currentVector.endNode = null;
            sketchApp.currentVector.placeHolder = false;
            sketchApp.render()
            sketchApp.startNewVector( true );
            sketchApp.currentVector.isUnSketchedArea = false;
            sketchApp.currentVector.hideAreaValue = false;
            sketchApp.currentVector.name = name? name: val;
            sketchApp.currentVector.labelEdited = true;
            sketchApp.currentVector.isClosed = false;
            sketchApp.currentVector.fixedAreaTrue = null;
            sketchApp.currentVector.fixedPerimeterTrue = null;
            sketchApp.currentVector.label = '[' + sketchApp.currentVector.index + '] ' + (items[0].NameValue? items[0].Value + '-' + items[0].NameValue: items[0].Value);
        }
        else if( type == 'edit' && !draw_outbuilding && sketchApp.currentVector.isUnSketchedArea){
            sketchApp.currentVector.labelEdited = true;
            sketchApp.currentVector.fixedAreaTrue = area;
            sketchApp.currentVector.fixedPerimeterTrue = peri;
            sketchApp.currentVector.label = '[' + sketchApp.currentVector.index + '] ' + (items[0].NameValue? items[0].Value + '-' + items[0].NameValue: items[0].Value);
        }

        if ( callback ) {
            callback( items, { 'IsOutbuilding': true, 'NewOBdrawSketch': ($( ".obcheckbox" ).prop( 'checked' )? false: true), 'outArea': area, 'outPerimeter': peri } )
        }

        if ( !sketchApp.isScreenLocked ) $( '.mask' ).hide();
        $( '.Current_vector_details' ).hide();
        $( '.Current_vector_details .outbdiv' ).remove();
        return false;
    });

}