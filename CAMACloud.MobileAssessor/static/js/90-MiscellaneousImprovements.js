﻿

function loadValidationTablesForMI(callBack) {
    var arr = [];
    ccma.MIFieldValidations = {};
    getData("select * from ccv_tAA_Misc_Catalog ", [], function (results) {
        for (var x in results) {
            var n = results[x]
            arr.push(n);
        }
        ccma.MIFieldValidations['MICATALOG'] = arr;
        callBack && callBack();
    });
}


function LoadRelatedFieldsForMI(field, expType, expression) {

    var MIfields = [], MIrelfields = [];
   // var MIallFields = Object.keys(datafields).map(function (x) { return datafields[x] }).filter(function (x) { return x.CategoryId == field.CategoryId });
    var MIallFields = Object.keys(datafields)
        .map(function (key) {
            return datafields[key];
        })
        .filter(function (x) {
            return (x.CategoryId == field.CategoryId &&
                (x['UI_Settings'] &&
                x['UI_Settings'].MIFields &&
                    x['UI_Settings'].MIFields === "true") && x.Name != "RollYear");
        });
    if (expType == 'LookupQuery') {
        MIallFields.forEach(function (x) {
            if (ccma.UI.RelatedFields[field.Id] == undefined) ccma.UI.RelatedFields[x.Id] = [];
            if (!_.contains(ccma.UI.RelatedFields[field.Id], x.Id) && (x.Id != field.Id)) ccma.UI.RelatedFields[field.Id].push(x.Id);
        });
    }
    else if (expType == 'CalculationExpression')
    {
        MIfields = expression.match(/([a-zA-Z_][a-zA-Z0-9_]+)/g)
        MIrelfields = MIallFields.filter(function (x) { return _.contains(MIfields, x.Name) });

        //if (MIfields !== null && typeof MIfields !== 'undefined') {
        //    if (Array.isArray(MIfields) && MIfields.includes('Quality')) 
        //        {
        //            MIrelfields = MIallFields.filter(function (x) { return _.contains(MIfields, x.Label) });
        //        }
        //    }

        MIrelfields.forEach(function (x) {
            if (ccma.UI.RelatedFields[x.Id] == undefined) ccma.UI.RelatedFields[x.Id] = [];
           if (!_.contains(ccma.UI.RelatedFields[x.Id], field.Id) && (x.Id != field.Id)) ccma.UI.RelatedFields[x.Id].push(field.Id);
       });
    }
}
function getConditionalValidationForMI(field, source) {
    var validations = [];
    var MICode;
    var MIddl;
    var MIDecimal;
    var MIFormula;
    var MILabel;
    var MIRequire;
    var fieldname = field.Name;
    var col = fieldname.match(/\d+/g);

    var MICodeid ; 
    var RollYear ;

    if (source.MICodeID != null && source.RollYear != null) {
        MICodeid = source.MICodeID;
        RollYear = source.RollYear;
        var MIFieldValidations = ccma.MIFieldValidations["MICATALOG"].filter(function (thisObject) {
            return (thisObject.MICodeID == MICodeid && thisObject.RollYear == RollYear);
        }).shift();
    } else {
         // RollYear = 2017;
        var MIFieldValidations = ccma.MIFieldValidations["MICATALOG"].filter(function (thisObject) {
            return (thisObject.RollYear == RollYear);
        }).shift();
    }

    for (x in MIFieldValidations) {
        MICode = MIFieldValidations.MICode;
        switch (col[0]) {
            case '01':
                MIddl = MIFieldValidations.C01;
                MIDecimal = MIFieldValidations.D01;
                MIFormula = MIFieldValidations.F01;
                MILabel = MIFieldValidations.L01;
                MIRequire = MIFieldValidations.S01;
                break;
            case '02':
                MIddl = MIFieldValidations.C02;
                MIDecimal = MIFieldValidations.D02;
                MIFormula = MIFieldValidations.F02;
                MILabel = MIFieldValidations.L02;
                MIRequire = MIFieldValidations.S02;
                break;
            case '03':
                MIddl = MIFieldValidations.C03;
                MIDecimal = MIFieldValidations.D03;
                MIFormula = MIFieldValidations.F03;
                MILabel = MIFieldValidations.L03;
                MIRequire = MIFieldValidations.S03;
                break;
            case '04':
                MIddl = MIFieldValidations.C04;
                MIDecimal = MIFieldValidations.D04;
                MIFormula = MIFieldValidations.F04;
                MILabel = MIFieldValidations.L04;
                MIRequire = MIFieldValidations.S04;
                break;
            case '05':
                MIddl = MIFieldValidations.C05;
                MIDecimal = MIFieldValidations.D05;
                MIFormula = MIFieldValidations.F05;
                MILabel = MIFieldValidations.L05;
                MIRequire = MIFieldValidations.S05;
                break;
            case '06':
                MIddl = MIFieldValidations.C06;
                MIDecimal = MIFieldValidations.D06;
                MIFormula = MIFieldValidations.F06;
                MILabel = MIFieldValidations.L06;
                MIRequire = MIFieldValidations.S06;
                break;
            case '07':
                MIddl = MIFieldValidations.C07;
                MIDecimal = MIFieldValidations.D07;
                MIFormula = MIFieldValidations.F07;
                MILabel = MIFieldValidations.L07;
                MIRequire = MIFieldValidations.S07;
                break;
            case '08':
                MIddl = MIFieldValidations.C08;
                MIDecimal = MIFieldValidations.D08;
                MIFormula = MIFieldValidations.F08;
                MILabel = MIFieldValidations.L08;
                MIRequire = MIFieldValidations.S08;
                break;
            case '09':
                MIddl = MIFieldValidations.C09;
                MIDecimal = MIFieldValidations.D09;
                MIFormula = MIFieldValidations.F09;
                MILabel = MIFieldValidations.L09;
                MIRequire = MIFieldValidations.S09;
                break;
            case '10':
                MIddl = MIFieldValidations.C10;
                MIDecimal = MIFieldValidations.D10;
                MIFormula = MIFieldValidations.F10;
                MILabel = MIFieldValidations.L10;
                MIRequire = MIFieldValidations.S10;
                break;
            case '11':
                MIddl = MIFieldValidations.C11;
                MIDecimal = MIFieldValidations.D11;
                MIFormula = MIFieldValidations.F11;
                MILabel = MIFieldValidations.L11;
                MIRequire = MIFieldValidations.S11;
                break;
            case '12':
                MIddl = MIFieldValidations.C12;
                MIDecimal = MIFieldValidations.D12;
                MIFormula = MIFieldValidations.F12;
                MILabel = MIFieldValidations.L12;
                MIRequire = MIFieldValidations.S12;
                break;

        }        
        validations.push({ txtDDL: MIddl, txtDecimal: MIDecimal, txtFormula: MIFormula, txtLabel: MILabel, txtRequire: MIRequire });
        return validations;
    }
    return validations;
    
}
function applyConditionalValidationForMI(field, fieldset, source) {
    var fieldname = field.Name;
    var stringcol = fieldname.match(/[a-z]+/gi);
    var numcol = fieldname.match(/\d+/g); //match(/[a-z]+/gi)
    if (numcol != null) {
        var validations = getConditionalValidationForMI(field, source);
        if (validations.length > 0) {
            validations = validations[0];
            if (validations.txtLabel != null) {
                $('.legend', fieldset).text(validations.txtLabel);

                field.CalculationExpression = null;
                // $('.newvalue', fieldset).val("");
                // $('.oldvalue', fieldset).val("");


                if (validations.txtDecimal != "") {
                    $('.newvalue', fieldset).removeAttr('scale');
                    $('.newvalue', fieldset).attr({ scale: validations.txtDecimal })
                    field.NumericScale = validations.txtDecimal;
                }
                if (validations.txtRequire != 0) {

                    var reqd = document.createElement('span');
                    $(reqd).addClass('reqd');
                    reqd.innerHTML = '*';
                    if (($(fieldset).find('.reqd').length === 0)) {
                        $('.newvalue[conditional-req]', fieldset).removeAttr('required');
                        $(fieldset).prepend(reqd);
                        $('input, textarea, select, .cusDpdownSpan', $(fieldset)).attr({ 'required': true });
                    }

                }
                else {
                    $('.newvalue', fieldset).removeAttr('required');
                    $('.reqd', $(fieldset)).remove();
                }
                if (validations.txtFormula != null) {
                    var Formula = decodeHTML(validations.txtFormula).replace(/[\[\]']+/g, '');
                    //console.log(Formula);
                    if (stringcol == "V") {
                        $(fieldset).removeAttr('calc-exp');
                        $(fieldset).attr({ "ro": true });
                        $(fieldset).attr({ "field-type": 8 });
                        $(fieldset).removeAttr('hiddenfieldset');
                        $(fieldset).removeAttr('readonlyfieldset');
                        $(fieldset).attr({ 'readonlyfieldset': 1 });
                        $('.newvalue', fieldset).attr({ "readonly": "readonly" });

                       // var Formulafield = /.*?([a-zA-Z][a-zA-Z0-9_#.]+)(?=[^']*(?:'[^']*'[^']*)*$).*?/i.exec(Formula);
                        var MIFormulaFields
                        if (typeof Formula === 'string') {
                            var Formulafield = Formula.match(/([a-zA-Z_][a-zA-Z0-9_]+)/g);
                            
                            if (Formulafield !== null && typeof Formulafield !== 'undefined' && Array.isArray(Formulafield)) {
                                var ffieldid = Formulafield.filter((item, index) => Formulafield.indexOf(item) === index);
                                ffieldid.forEach(function (y) {
                                    if (y.toUpperCase() != "IF" ) {

                                         MIFormulaFields = Object.keys(datafields)
                                            .map(function (key) {
                                                return datafields[key];
                                            })
                                            .filter(function (x) {
                                                return (x.CategoryId == field.CategoryId &&
                                                    (x['UI_Settings'] &&
                                                        x['UI_Settings'].MIFields &&
                                                        x['UI_Settings'].MIFields === "true") && x.Name == y);
                                            });

                                        if (MIFormulaFields.length == 0) {

                                            MIFormulaFields = Object.keys(datafields)
                                                .map(function (key) {
                                                    return datafields[key];
                                                })
                                                .filter(function (x) {
                                                    return (x.CategoryId == field.CategoryId &&
                                                        (x['UI_Settings'] &&
                                                            x['UI_Settings'].MIFields &&
                                                            x['UI_Settings'].MIFields === "true") && x.Label == y);
                                                });


                                            if (MIFormulaFields.length > 0) {
                                                if (y != MIFormulaFields[0].Name) {
                                                    Formula = Formula.replaceAll(y, MIFormulaFields[0].Name)
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var Formulavalidations = getConditionalValidationForMI(MIFormulaFields[0], source);
                                            var fname = MIFormulaFields[0].Name.match(/\d+/g);
                                            if (Formulavalidations[0].txtDDL != 0) {
                                                Formula = Formula.replaceAll('V' + fname, 'MIChoiceID' + fname)
                                            }
                                        }
                                 }

                                 });
                            }
                        }
                        var MIexpression = Formula;

                        $(fieldset).attr({ 'calc-exp': Formula });
                        field.CalculationExpression = MIexpression;
                        var MIexpType = 'CalculationExpression';

                        LoadRelatedFieldsForMI(field, MIexpType, MIexpression);

                    }
                    else {
                        $(fieldset).attr({ "ro": true });
                        $(fieldset).attr({ 'hiddenfieldset': 1 });
                        $(fieldset).removeAttr('calc-exp');
                        $('.newvalue', fieldset).removeAttr('required');
                        $('.reqd', $(fieldset)).remove();

                    }
                }
                else if (validations.txtDDL != 0) {
                    if (stringcol == "MIChoiceID") {
                        $(fieldset).attr({ "ro": false });
                        $(fieldset).removeAttr('hiddenfieldset');
                        //field.LookupQuery = "select ItemName from ccv_tAA_Misc_CatalogChoice_{parcel.CC_MiscRollYear} where FieldNum =" + numcol + " and MICodeID = '{MICodeID}' and RollYear = '{RollYear}'  order by ItemName";
                    }
                    else {
                        $(fieldset).attr({ "ro": true });
                        $(fieldset).attr({ 'hiddenfieldset': 1 });
                        $(fieldset).removeAttr('calc-exp');
                        $('.newvalue', fieldset).removeAttr('required');
                        $('.reqd', $(fieldset)).remove();
                    }

                }
                else {
                    if (stringcol == "V") {
                        field.CalculationExpression = null;
                        $(fieldset).removeAttr('calc-exp');
                        $(fieldset).removeAttr('readonlyfieldset');
                        $('.newvalue', fieldset).removeAttr("readonly");
                        $(fieldset).removeAttr('calculated');
                        $(fieldset).removeAttr('hiddenfieldset');
                        $(fieldset).attr({ "ro": false });
                    }
                    else if (stringcol == "MIChoiceID") {
                        $(fieldset).attr({ "ro": true });
                        $(fieldset).attr({ 'hiddenfieldset': 1 });
                        $('.newvalue', fieldset).removeAttr('required');
                        $('.reqd', $(fieldset)).remove();
                    }
                }

            }
            else {
                field.CalculationExpression = null;
                $(fieldset).removeAttr('calc-exp');
                $(fieldset).attr({ "ro": true });
                 $(fieldset).attr({ 'hiddenfieldset': 1 });
                $('.newvalue', fieldset).removeAttr('required');
                $('.reqd', $(fieldset)).remove();
            }

            if ($(fieldset).attr('hiddenfieldset') == 1) {
                $('.newvalue', fieldset).val("");
            }

        }
    }
    else {
        //if (fieldname == "RollYear") {
           // $(fieldset).removeAttr('hiddenfieldset');

           // field.LookupTable = '$QUERY$';
           // field.LookupQuery = 'select RollYear from ccv_tAA_Misc_Catalog group by RollYear';
        //}
        if (fieldname == "MICodeID") {
           // field.LookupQuery = "select MICodeID, MICode || ' - ' || Description Description from ccv_tAA_Misc_Catalog where  RollYear = case when '{RollYear}' = 'null' then '{parcel.RollYear}' else '{RollYear}' end";
            var MIexpType = 'LookupQuery';
            var MIexpression = field.LookupQuery;
            LoadRelatedFieldsForMI(field, MIexpType, MIexpression);            
        }
    } 
    
}

function IF(value, exp1, exp2)
{
    return value? exp1: exp2;
}


