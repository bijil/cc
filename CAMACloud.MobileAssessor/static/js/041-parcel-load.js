﻿//    DATA CLOUD SOLUTIONS, LLC ("COMPANY") CONFIDENTIAL 
//    Unpublished Copyright (c) 2010-2013 
//    DATA CLOUD SOLUTIONS, an Ohio Limited Liability Company, All Rights Reserved.

//    NOTICE: All information contained herein is, and remains the property of COMPANY.
//    The intellectual and technical concepts contained herein are proprietary to COMPANY
//    and may be covered by U.S. and Foreign Patents, patents in process, and are protected
//    by trade secret or copyright law. Dissemination of this information or reproduction
//    of this material is strictly forbidden unless prior written permission is obtained
//    from COMPANY. Access to the source code contained herein is hereby forbidden to
//    anyone except current COMPANY employees, managers or contractors who have executed
//    Confidentiality and Non-disclosure agreements explicitly covering such access.</p>

//    The copyright notice above does not evidence any actual or intended publication
//    or disclosure of this source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
//    DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
//    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
//    IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
//    OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
//    TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
//    ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
var currentNbhd = null;
var changedNbhd = null;
var refreshNbhdProfile = false;
var ipa, pictometryUrl, showPictometry = false, isIpaLoaded = false, defaultZoom = 19;
function ShowParcelListWithNoSort() {
    ShowParcelList(false);
}


function ShowParcelList(geosort,selectedFilters) {
    $( 'a.page-back', $( '#sort-screen' ) ).hide();
    if(isBPPUser){ 
    	if(ccma.Session.BPPDownloadType != '0') $('.sort-toggle').show();
    	combinedView ? $('.standalone-bpp').show() : $('.standalone-bpp').hide()
    	if(ccma.Session.BPPDownloadType == '0')
    		$('.standalone-bpp').show();
    }
    log('Sorting parcels to visit ...');
    unsortedParcels = {};
    var parcels;
    var modeFilter = "", selectQuery = "";

    if (geosort == null)
        geosort = true;
    if (sort == null)
        sort = '';
    var head = "";
    if (listMode == 0) {
        head = 'Proximity Parcels';
        $('header .sort-view', $('#sort-screen')).show();
        if (!(isBPPUser && !combinedView)) modeFilter += 'CC_Priority = ' + listMode + ' AND ';
        $('#ss-view-type').removeClass('csv-proximity');
        $('#ss-view-type').addClass('csv-priority');
    }
    else if (listMode == 1) {
        head = 'Priority Parcels';
        $('header .sort-view', $('#sort-screen')).show();
        if (!(isBPPUser && !combinedView)) modeFilter += EnableNewPriorities == 'True' ? 'CC_Priority IN (1, 2, 3, 4, 5) AND' : 'CC_Priority IN (1, 2) AND';
        $('#ss-view-type').removeClass('csv-priority');
        $('#ss-view-type').addClass('csv-proximity');
    }
    else
        head = 'Parcels with Alerts';
    if ( isBPPUser ) {        
        if ( listType == 0 ) {
			if(selectedFilters && selectedFilters != 0) {
				if(selectedFilters == "1" )	modeFilter += " (IsParentParcel = 'true' ) AND ";
				else if(selectedFilters == "2")	modeFilter +=  " ((IsParentParcel ='false' AND IsPersonalProperty = 'false') OR (IsParentParcel ='true')) AND " ;
				else if(selectedFilters == "4") modeFilter += " (IsParentParcel ='true') AND ";
		    }
		    else {
                modeFilter += " IsParentParcel ='true' AND "; head += ' (REAL)';
		    }
            selectQuery = ',(Select count(*) from PendingParcels where parentParcelId = p.Id) BPPCount , (SELECT CASE WHEN Count(*) > 0  THEN "auto" ELSE "none" END from SubjectParcels where parentParcelId = p.Id and reviewed ="true") AnyChildReviewed '
      
        }
        else if ( listType == 1 )  {
			if(selectedFilters && selectedFilters != 0 && !activeParentParcel) {
				if(selectedFilters == "1" ){
					if(combinedView) modeFilter += '';
					else modeFilter += " (IsParentParcel = 'true' ) AND ";
				}
				else if(selectedFilters == "2") modeFilter +=  " ((IsParentParcel ='false' AND IsPersonalProperty = 'false') OR (IsParentParcel ='true')) AND " ;
				else if(selectedFilters == "3" ) modeFilter += " ((IsPersonalProperty = 'true' AND parentParcelId IS NULL) OR (IsPersonalProperty = 'true' AND parentParcelId IS NOT NULL))  AND  ";
				else if(selectedFilters == "4")	modeFilter += " (IsParentParcel ='true') AND ";
			}
	else {
			if(activeParentParcel){
            modeFilter += " IsPersonalProperty = 'true' AND ParentParcelID = " + activeParentParcel.Id + " AND "; head += ' 		(BPP) -- ' + activeParentParcel.KeyValue1;
            }
	}
        }
        else if ( listType == 2 ){
            modeFilter += " IsPersonalProperty = 'true' AND "
        }
    }
    $( 'header .headerspan', $( '#sort-screen' ) ).html( head );
    myParcels = document.getElementById('my-parcels');
    myParcels.innerHTML = '<div class="sort-screen-message">Please wait while parcels are sorted and loaded ...</div>';
    let tbl = 'PendingParcels', rvcond = " Reviewed = 'false'";
    if (isBPPUser && !combinedView) { tbl = 'SubjectParcels'; rvcond = ' 1 = 1'; }
    var sql = "SELECT *" + selectQuery + " FROM  " + tbl + " p WHERE " + modeFilter + rvcond;
    getData(sql, [], function (parcels) {
        let dsql = isBPPUser && !combinedView ? "SELECT * FROM SubjectParcels WHERE IsPersonalProperty = 'true' AND parentParcelId IS NOT NULL AND Reviewed = 'false' AND " + (listMode == 0 ? "CC_Priority = 0" : (EnableNewPriorities == 'True' ? "CC_Priority IN (1, 2, 3, 4 , 5)" : "CC_Priority IN (1, 2)")): "SELECT COUNT(*) FROM SubjectParcels";
        getData(dsql, [], function (bparcels) {
            if (isBPPUser && combinedView) {
                for (let i = 0; i < parcels.length; i++) {
                    if (ParcelsNotInGroup.includes(parseInt(parcels[i].Id))) {
                        parcels.splice(i, 1);
                        i--;
                    }
                }
            }
            if (isBPPUser && !combinedView) {
                for (let i = 0; i < parcels.length; i++) {
                    let pri = parcels[i].CC_Priority;
                    if ((listMode == 0 && pri != '0') || (listMode == 1 && pri == '0')) {
                        let bp = bparcels.filter((bk) => { return bk.ParentParcelID == parcels[i].CC_ParcelId });
                        if (!(bp?.length > 0)) {
                            parcels.splice(i, 1);
                            i--;
                        }
                    }
                }
            }
            if (bparcels && bparcels.length)

            if (parcels.length == 0 && !(isBPPUser && listType == 1 && !combinedView)) {
                showMultipleMACButton();
                if (isFirstTime) {
                    if (listMode == 2) {
                        listMode = 1;
                        ShowParcelList(geosort);
                        return;
                    } else if (listMode == 1) {
                        listMode = 0;
                        if (listType == 0) {
                            listMode = 1
                            toggleWithBPP();
                            return;
                        }
                        else {
                            ShowParcelList(geosort);
                            return;
                        }
                    } else {
                        isFirstTime = false;
                        if (listType == 0) {
                            listMode = 1
                            toggleWithBPP();
                            return;
                        }
                    }
                }
                myParcels.innerHTML = '<div class="sort-screen-message">No parcels found in this section.</div>';
                setSortButton(0);
                if (isBPPUser && listType == 1 && !combinedView) {
                    $('a.page-back', $('#sort-screen')).show();
                    $('.sort-toggle').hide();
                }
                selectNbhdInHomescreen();
                return;
            }
            isFirstTime = false;

            //        if (listMode == 1)
            //            geosort = false;

            log('Geosort: ' + geosort);
            if (!geosort) {
                log('Skip Google Sort');
                appState.homeParcelSelectedIndex = 0;
                appState.homeParentParcelSelectedIndex = 0;
                loadParcels(undefined, selectedFilters, bparcels);
            }
            else {
                if (listMode == 0) {
                    ComputeProximityRoutes(loadParcels);
                } else {
                    ComputePriorityAndAlertRoutes(loadParcels);
                }


            }
        });
    });
    if(isBPPUser){ 
   		if(selectedFilters && selectedFilters !=0) {  $('.sort-view').css('background-position-x','5%');} 
    	else {$('.sort-view').css('background-position-x','97%');}     
    }
}

function loadParcels(callback, selectedFilters, bparcels) {
    $( 'a.page-back', $( '#sort-screen' ) ).hide();
    if(isBPPUser && ( listType == 0 || combinedView )) $('.sort-toggle').show();
	else $('.sort-toggle').hide();
	//if(isBPPUser && ( combinedView || listType == 0 )) $('.select-parcelspanHeader .selectParcelCheckbox').hide();
	//else $('.select-parcelspanHeader .selectParcelCheckbox').show();
	var parentFetchSql = '';
    var modeFilter = 'CC_Priority = "' + listMode + '" AND ', selectQuery = '';
    var limit = 10, haveNbhd = true; head = "";
    switch (listMode) {
        case 0:
            //$('header span', $('#sort-screen')).html('Proximity Parcels');
            modeFilter = 'CC_Priority = ' + listMode + ' AND ';
            head = "Nearby NNN Parcels";
			limit = 25;
            break;
        case 1:
            //$('header span', $('#sort-screen')).html('Priority Parcels');
            if (listType != 1 || (activeParentParcel && listType && listType == 1) || combinedView){
                modeFilter = EnableNewPriorities == 'True' ? 'CC_Priority IN (1,2, 3, 4, 5) AND ' : 'CC_Priority IN (1,2) AND ';
                head = "Nearby NNN Priority Parcels"
            }
			else{
				 modeFilter = ''; head = "Nearby NNN Parcels"
			}
            limit = 15;
            break;
    }

    if (isBPPUser && !activeParentParcel && !combinedView) modeFilter = '';

    if ( isBPPUser) {
        if ( listType == 0 ) {
            $( '.select-parcelspanHeader .selectParcelCheckbox' ).hide( 'selectedcheckbox' );
			if(selectedFilters && selectedFilters != 0) {
				    if(selectedFilters == "1" )	modeFilter += " (IsParentParcel = 'true' ) AND ";
				    else if(selectedFilters == "2")	modeFilter +=  " ((IsParentParcel ='false' AND IsPersonalProperty = 'false') OR (IsParentParcel ='true')) AND " ;
				    else if(selectedFilters == "4") modeFilter += " (IsParentParcel ='true') AND ";
		    }
		    else		
                modeFilter += " IsParentParcel ='true' AND ";
	        head += '(REAL)';
            $( '.map-view' ).show();   $('.change-sort-view').show();selectQuery = ',(Select count(*) from PendingParcels where parentParcelId = p.Id) BPPCount , (SELECT CASE WHEN Count(*) > 0  THEN "auto" ELSE "none" END from SubjectParcels where parentParcelId = p.Id and reviewed ="true") AnyChildReviewed '
        }
        else if ( listType == 1 )  {
            if(combinedView)  $( '.select-parcelspanHeader .selectParcelCheckbox' ).show( 'selectedcheckbox' );
		    if(selectedFilters && selectedFilters != 0 && !activeParentParcel) {
			    if(selectedFilters == "1" ){
				    if(combinedView) modeFilter += '';
				    else modeFilter += " (IsParentParcel = 'true' ) AND ";
			    }
			    else if(selectedFilters == "2") modeFilter += " ((IsParentParcel ='false' AND IsPersonalProperty = 'false') OR (IsParentParcel ='true')) AND " ;
			    else if(selectedFilters == "3" ) modeFilter += " ((IsPersonalProperty = 'true' AND parentParcelId IS NULL) OR (IsPersonalProperty = 'true' AND parentParcelId IS NOT NULL))  AND  ";
			    else if(selectedFilters == "4")	modeFilter += " (IsParentParcel ='true') AND ";
		    }
		    else {
			    if(activeParentParcel){
                    modeFilter += " IsPersonalProperty = 'true' AND ParentParcelID = " + activeParentParcel.Id + " AND "; head += '(BPP) - ' + activeParentParcel.KeyValue1 +' '+ (activeParentParcel.StreetAddress ? (activeParentParcel.StreetAddress.length > 12 ? activeParentParcel.StreetAddress.substring(0,12) + '..' :activeParentParcel.StreetAddress) : '' );
           
                    $( '.map-view' ).hide();
			          $('.change-sort-view').hide();  
			        selectQuery = ',(Select count(*) from PendingParcels where parentParcelId = p.Id) BPPCount , (SELECT CASE WHEN Count(*) > 0  THEN "auto" ELSE "none" END from SubjectParcels where parentParcelId = p.Id and reviewed ="true") AnyChildReviewed '
			        if(!combinedView)
                        parentFetchSql = 'SELECT *' + selectQuery + ' FROM SubjectParcels p  WHERE Id = ' + activeParentParcel.Id
                }			
            }
        }
        else if ( listType == 2 ) {
            modeFilter += " IsPersonalProperty = 'true' AND ";head += '(BPP)'
        }
    }
    getData("select KeyValue1 from Parcel where KeyValue1!=-99 limit 1",[],function(result){
    	var keyVal = result[0].KeyValue1;
    	var sortquery = '';
	    var SortOrderType = ccma.UI.SortOrderType;
	    if (SortOrderType == 1)
	        sortquery = ',DistanceIndex*1';
	    else if (SortOrderType == 2)
	        sortquery = ',-DistanceIndex*1';
	    else if (SortOrderType == 3)
	        isNaN(keyVal) ? sortquery = ',keyvalue1 ' : sortquery = ',keyvalue1*1'
	    else if (SortOrderType == 4)
	        isNaN(keyVal) ? sortquery = ',keyvalue1 desc' : sortquery = ',-keyvalue1*1'
	    else if (SortOrderType.indexOf("-Asc") > -1) {
	        SortOrderType = SortOrderType.replace("-Asc", "");
	        sortquery = ',' + SortOrderType;
	    }
	    else if (SortOrderType.indexOf("-Desc") > -1) {
	        SortOrderType = SortOrderType.replace("-Desc", "");
	        sortquery = ',' + SortOrderType + ' DESC';
	    }
	    
	    //if (currentNbhd == null) {
	        
	    //}
	    //else
	    //    modeFilter += " NbhdId = '" + (changedNbhd ? changedNbhd : currentNbhd) + "' AND";
        let tbl = 'PendingParcels', rvcond = " Reviewed = 'false'", sql = '';
        if (isBPPUser && !activeParentParcel && !combinedView) { tbl = 'SubjectParcels'; rvcond = ' 1 = 1'; }

        if (isBPPUser && !activeParentParcel && combinedView)
            sql = 'SELECT *' + selectQuery + ' FROM ' + tbl + ' p  WHERE ' + modeFilter + rvcond + ' ORDER BY -CC_Priority' + sortquery; // + ' LIMIT ' + limit;
        else
            sql = 'SELECT *' + selectQuery + ' FROM ' + tbl + ' p  WHERE ' + modeFilter + rvcond + ' ORDER BY -CC_Priority' + sortquery + ' LIMIT ' + limit;
        //FD_30176
        //var bpparcels = bparcels;
        //loadParcelsFromQuery(sql, bpparcels, 'my-parcels', callback, function (parcels) {
            loadParcelsFromQuery(sql,'my-parcels', callback, function (parcels) {
	        var locParcels = [];
	        var noLocParcels = [];
	        var newParcels = [];
	        if(isBPPUser && combinedView){
		         for(let i=0; i<parcels.length; i++){
				    if(ParcelsNotInGroup.includes(parseInt(parcels[i].Id))){
					    parcels.splice(i, 1);
					    i--;
				    }
			    }
            }

            if (isBPPUser && !activeParentParcel && combinedView) parcels.splice(limit);

            if (isBPPUser && !activeParentParcel && !combinedView && bparcels) {
                for (let i = 0; i < parcels.length; i++) {
                    let pri = parcels[i].CC_Priority;
                    if ((listMode == 0 && pri != '0') || (listMode == 1 && pri == '0')) {
                        let bp = bparcels.filter((bk) => { return bk.ParentParcelID == parcels[i].CC_ParcelId });
                        if (!(bp?.length > 0)) {
                            parcels.splice(i, 1);
                            i--;
                        }
                    }
                }
            }

	        for (var i = 0; i < parcels.length; i++) {
	            if (parcels[i].Latitude == null || parcels[i].Latitude == ' ' || parcels[i].Latitude == undefined)
	                noLocParcels[i] = parcels[i];
	            else
	                locParcels[i] = parcels[i];
	        }
	        if (noLocParcels.length > 0)
	            newParcels = _.union( locParcels, noLocParcels );
	        setSearchResultsTitle( head, parcels.length )
	        return true;
	    }, null, parentFetchSql);
    
    });
}
//FD_30176
//function loadParcelsFromQuery(sql, bpparcels, target, callback, resultcallback, parcelId, parentFetchSql, combinedViewFilter) {
function loadParcelsFromQuery(sql, target, callback, resultcallback, parcelId, parentFetchSql, combinedViewFilter) {

    $('.multiple-mac').hide();
    if ( fylEnabled ) $( '.fy_selector' ).show();
    var startTime = ticks();
    nbhdLoaded = false;
    if (parcelId)
        sql = 'SELECT * FROM PendingParcels WHERE Id = ' + parcelId;
    if (sql == '')
        console.warn('Last AppQuery is blank')
    getData(sql, null, function (parcels) {
        if (resultcallback) {
            if (!resultcallback(parcels)) {
                return false;
            }
        }

        if (combinedViewFilter) {
            if (isBPPUser && combinedView && !parcelId) {
                for (let i = 0; i < parcels.length; i++) {
                    if (ParcelsNotInGroup.includes(parseInt(parcels[i].Id))) {
                        parcels.splice(i, 1);
                        i--;
                    }
                }
            }

            if (isBPPUser && !activeParentParcel && combinedView && !parcelId) parcels.splice(15);
        }

		getParentParcel(parentFetchSql,function(pParcel){
        if (!parcelId)
            appState.parcelQuery = sql;
        appState.parentFetchSql = parentFetchSql;
        var myParcels;
        //console.log('Loading ' + parcels.length + ' parcels on home screen');
        myParcels = document.getElementById(target);
        //console.log(myParcels, parcelId, parcels);
        if (!parcelId) {
            myParcels.innerHTML = ( isBPPUser && listType == 0 && !combinedView ) ? document.getElementById( 'my-parentparcels-template' ).innerHTML : (isBPPUser && (listType == 1 ||listType == 2) && !combinedView) ? document.getElementById( 'my-parcels-template-bpp' ).innerHTML:
            document.getElementById( 'my-parcels-template' ).innerHTML;
            if ( isBPPUser && ( listType == 1 || listType == 2) && !combinedView){
            	$('.normal-view-bpp', myParcels).removeClass('hidden')
            	$('.normal-view', myParcels).addClass('hidden')
                $( '.mark-complete', myParcels ).after( '<a class="mark-as-delete text-button" pid="${Id}" pdeleted="${CC_Deleted}" >X</a>' )
                }
           	else if( isBPPUser && listType == 0 && !combinedView){
				$('.parcel-item-new .normal-view', myParcels).removeClass('hidden');
				}
           	else if(!isBPPUser || combinedView){
               	$('.normal-view-bpp', myParcels).addClass('hidden')
            	$('.normal-view' , myParcels).removeClass('hidden')
            	$('.expanded-view', myParcels).addClass('hidden')
            	$( '.mark-complete', myParcels ).after( '<a class="mark-as-delete text-button" rp="${IsRPProperty}" pid="${Id}" pdeleted="${CC_Deleted}" style="display:none" ></a>' )
               }
            $('[template="estimate-chart"]', myParcels).html($('#estimate-chart-template').html());
            listparcels = [];

            if (isBPPUser && combinedView && parcels.length > 0) {
	            $(myParcels).html('');
	            parcels.forEach((item) => {
				    if (item.IsPersonalProperty == "true") {
				        let pTemplate = document.createElement('div')
                        pTemplate.innerHTML = document.getElementById('my-parcels-template-bpp').innerHTML;
                        convertSortScreenSelectToInput(pTemplate);
				        $('[template="estimate-chart"]', pTemplate).html($('#estimate-chart-template').html());
				        $( pTemplate ).fillTemplate( [item] );
				        $('.normal-view-bpp', pTemplate).removeClass('hidden');
				        $( myParcels ).append($('nav',pTemplate));
				    }
				    else {
				        let pTemplate = document.createElement('div')
                        pTemplate.innerHTML = document.getElementById('my-parcels-template').innerHTML;
                        convertSortScreenSelectToInput(pTemplate);
				        $('[template="estimate-chart"]', pTemplate).html($('#estimate-chart-template').html());
				        $( pTemplate ).fillTemplate( [item] );
				        $('.normal-view', pTemplate).removeClass('hidden');
				        $( myParcels ).append($('nav',pTemplate));
				    }
			    });
            }
            else {
                //FD_30176
                //if ((typeof bpparcels != 'undefined' && bpparcels) && !combinedView && listMode == 1 && ccma.Session.bppRealDataReadOnly && ccma.Session.RealDataReadOnly) {
                //    const matchingParcels = parcels.filter(parcel =>
                //        bpparcels.some(bpp => bpp.ParentParcelID == parcel.Id)
                //    );
                //    //console.log('parentParcels', parcels);
                //    //console.log('matchingparentParcels', matchingParcels);
                //    //console.log('BpParcels:', bpparcels);
                //    convertSortScreenSelectToInput(myParcels);
                //    $(myParcels).fillTemplate(matchingParcels);
                //}
                //else {
                //    convertSortScreenSelectToInput(myParcels);
                //    $(myParcels).fillTemplate(parcels);
                //}
                convertSortScreenSelectToInput(myParcels);
                $(myParcels).fillTemplate(parcels);
            }

			if (pParcel.length >= 1) {
                var pTemplate = document.createElement('div');
                pTemplate.innerHTML = document.getElementById('my-parentparcels-template').innerHTML;
                convertSortScreenSelectToInput(pTemplate);
				$( pTemplate ).fillTemplate( pParcel[0] );
				$( myParcels ).prepend(pTemplate);
				$('#my-parcels .parcel-item-new .nav-buttons a:nth-child(1)').css('display','none');
				$('.parcel-item-new .normal-view').removeClass('hidden');
            }	
        } else {
            var oneParcel = $('nav[parcelid="' + parcelId + '"]')[0];
            oneParcel.innerHTML = $('#my-parcels-template nav').html();
            convertSortScreenSelectToInput(oneParcel);
            $('[template="estimate-chart"]', oneParcel).html($('#estimate-chart-template').html());
            $(oneParcel).fillTemplate(parcels);
            //  console.log('Refreshed one parcel', oneParcel, parcels[0].LandOpinion)
        }

        var endTime = ticks();
        //console.log('Sortscreen refreshed in ' + (endTime - startTime) + ' ticks');
		$('.parcel-item-new', $( myParcels ) ).each(function(){
			let macbutton = false;
			$('.ssc-norm-status .comp32', this).parent().each(function(){
				if(macbutton) $(this).css('display', 'none');
				if($(this).css('display') == 'inline')
					macbutton = true;
			});
		});
		setSortButton(0)
        refreshImagesInSortScreen( target )
        unSyncChanges();
            if (!(EnableNewPriorities == 'True')) $('.alert32').css('background', 'url(../static/css/images/parcel-list.png) left center no-repeat !important;');
            if (EnableNewPriorities == 'True') $('.priority32').css('background', 'url(../static/css/images/alert_high.png) left center no-repeat !important;');
        sortScreenEvents( parcels );
        if (callback) callback();
		});
    });
}
function sortScreenEvents( parcels ) {
    $( '.parcel-item .select-parcelspan' ).remove();
    $( '.parcel-item' ).each( function ()    {
        $( this ).prepend( '<span class="select-parcelspan"><span class="selectParcelCheckbox"></span></span>' );
    } );
    $( '.select-parcelspanHeader .selectParcelCheckbox' ).removeClass( 'selectedcheckbox' );
    showMultipleMACButton();
    $.onswipe( '.overlay-photo-box', 'both', function ( pi, leftSwiped )    {
		if(leftSwiped)
        	photos.showNext();
        else
        	photos.showPrevious();
    } );
    /*$.onswipe( '.overlay-photo-box', 'right', function ( pi )    {

        photos.showPrevious();
    } );*/

    $.onswipe( '.parcel-item,.parcel-item-new', 'left', function ( pi, leftSwiped )    {
        showParcel( parseInt( $( pi ).attr( 'parcelid' ) ) );
    } );

    bindEstimateChartSelection();
    bindSortScreenEvents();
    if ( parcels.length > 0 )
        selectParcelInHomeScreen(( isBPPUser && listType == 0 && !combinedView) ? $( '.parcel-item-new' )[appState.homeParentParcelSelectedIndex] : ( isBPPUser && !combinedView && listType == 1 ? $( '.parcel-item-new' )[0]: $( '.parcel-item' )[appState.homeParcelSelectedIndex]) );
    //refreshScrollable();
    if ( isBPPUser && listType == 1 && !combinedView )
        $( 'a.page-back', $( '#sort-screen' ) ).show();
    //if ( isBPPUser && combinedView){
    	//$('.parcel-item .select-parcelspan').hide();
    	//}
}
function refreshImagesInSortScreen( target, ParcelId, callback ){
    var filter = '';
    if ( ParcelId )
        filter = '="' + ParcelId + '"'
    var myParcels = document.getElementById( target );
    var divimgs = []
    $( 'div.sort-screen-photo-large[imgpid' + filter + ']', $( myParcels ) ).each( function (){
        divimgs.push( this );
    } );
 
    var imageProcess = function() {
    	if (divimgs.length == 0) { if (callback) callback(); return; }
    	var img = divimgs.pop();
    	var pid = $(img).attr('imgpid');
        var pkey = $(img).attr('pkey');
        //IDB_getData('Images',['ParcelId'],['='],[parseInt(pid)],[], function (im) {    
        IDB_getData('Images',['ParcelId', 'Type'],['=', '='],[parseInt(pid), 1],['AND'], function (im) {
            if (im.length > 0) {
                var obimage = (im.filter(function (x) { return checkIsFalse( x.IsPrimary ); } ).length > 0 ) ? im.filter(function (x) { return checkIsFalse(x.IsPrimary); })[0].Image : im[0].Image;
                $( 'div.sort-screen-photo[imgpid="' + pid + '"]', $( myParcels ) ).css( 'background-image', "url('" + obimage + "')" );
            }
            else $( 'div.sort-screen-photo[imgpid="' + pid + '"]', $( myParcels ) ).css( 'background-image', 'url("static/css/images/parcel.png")' );
			imageProcess();
        });

        $(img).unbind(touchClickEvent);
        $(img).bind(touchClickEvent, function (e){
            e.preventDefault();
			if (!scrolling) {
			    if(isBPPUser) {
			        if( $('.parcel-item[parcelid="' + pid + '"] .mark-as-delete').hasClass('Bppdeleted') == true) {
			            messageBox("Photos cannot be taken on a property that is flagged to be deleted. Removing the Deleted option for this parcel will allow you to take photos.")
			            return;
			        }
			        var isParent ;
			        var sql = "select IsParentParcel from parcel where Id= "+ pid;
			        if (ShowAlternateField == "True" && AlternateKey)
			            sql = "select IsParentParcel, "+ AlternateKey +" from parcel where Id= "+ pid;
			        getData(sql, [], function(d) { 
			            isParent = d[0].IsParentParcel ;
			            var altKeyValue = d[0].AlternateKey ? d[0].AlternateKey : pkey;	
                        if ((isParent == true || isParent == "true") && (ccma.Session.RealDataReadOnly || (ParcelsNotInGroup.some(function (item) { return item === parseInt(pid) }) && clientSettings['PersonalPropertyDownloadType'] == 'BPP') || (ccma.Session.bppRealDataEditable == 0 && !ccma.Session.bppRealDataReadOnly))) {
			                messageBox("This option is not available for selected parcel.");
			                return;
			            }
			            else photos.openParcel(pid, pkey, { showPrimary: true },null,altKeyValue)
			        });
			    }
			    else {
			        if (ShowAlternateField == "True" && AlternateKey) {
			            var sql = "select "+ AlternateKey +" as val from parcel where Id= "+ pid;
			            getData(sql,[],function(d) {
			                var altKeyValue = d[0].val ? d[0].val : pkey;	
			                photos.openParcel(pid, pkey, { showPrimary: true },null,altKeyValue)
			            });
			        }
			        else 
			            photos.openParcel(pid, pkey, { showPrimary: true });   
			   } 
			}			
        });
    }
    
    imageProcess();
}
    var imgUnSync = [];
    function unSyncChanges() {
        var expandType = (isBPPUser && listType == 1 && !combinedView) ?  ".expanded-view-bpp" :".expanded-view"
        getData("SELECT * FROM ParcelChanges pc LEFT OUTER JOIN Parcel p on p.Id=pc.ParcelId WHERE p.REVIEWED !='true' AND (pc.Field != 'newparcel' OR IFNULL(pc.Field,'') = '') AND pc.FieldId NOT IN (SELECT id from Field WHERE IsCustomField = 'true' OR DoNotIncludeInAuditTrail = 'true' ) AND pc.Action !='PhotoFlagMain'", [], function (UnSynced) {
            IDB_getData('UnsyncedImages', [], [], [], [], function(ui) {
                imgUnSync = ui;
                let unsyncedMetaPids = [];
                getData("Select * from ParcelChanges pc Where Field = 'PhotoMetaData'", [], function (UnSyncedMeta) {
                    IDB_getData('Images', [], [], [], [], function(imgs) {
                        UnSyncedMeta.forEach(function(c){
                            let pids = imgs.filter(function(x){return x.Id  == c.ParcelId || x.LocalId == c.ParcelId })[0].ParcelId
                            unsyncedMetaPids.push(pids.toString());
                        });
                        $('.parcel-item').each(function () {
                            var pid = $(this).attr('parcelId');          
                            var unSyncedChanges = UnSynced.filter(function (x) { return x.ParcelId == pid }).length > 0 || imgUnSync.filter(function (x) { return x.ParcelId == pid }).length > 0 ; 
                            if(unsyncedMetaPids.includes(pid)){
                                unSyncedChanges = !unSyncedChanges ? true : unSyncedChanges;
                            }
                            if (unSyncedChanges) {
                                $(this).find('.unsync32').remove();
                                $(this).find(expandType).css('padding-bottom', '29px');
                                $(this).append('<span class="unsync32"></span>');
                            }
                            else {
                                $(this).find(expandType).css('padding-bottom', '0px');
                                $(this).find('.unsync32').remove();
                            }
                        });
                    });
                });      
            });
        });
    }

    function bindSortScreenEvents() {
        $('.parcel-item, .parcel-item-new').unbind(touchClickEvent);
        $('.parcel-item, .parcel-item-new').bind(touchClickEvent, function (e) {
            e.preventDefault();        
            if (!scrolling) {
                if ($('.normal-view', this).hasClass('hidden')) {
                	if ( $(this).hasClass('parcel-item-new') && !$(this).hasClass('selected') && !combinedView && isBPPUser) {
                		$( '.parcel-item,.parcel-item-new' ).removeClass('selected');   
                		$(this).addClass('selected');
                		if (listType == 0) appState.homeParentParcelSelectedIndex = $('.parcel-item-new').index($(this));
                	}
                	
                    return false;
                }
                else {
                	if ($(this).hasClass('parcel-item-new') && listType == 0 && !combinedView && isBPPUser)
                		appState.homeParentParcelSelectedIndex = $('.parcel-item-new').index($(this));
                	else if ($(this).hasClass('parcel-item-new') && listType != 0 && !combinedView && isBPPUser)
                		appState.homeParcelSelectedIndex = '0';
                	else
                    	appState.homeParcelSelectedIndex = $('.parcel-item').index($(this));
                }
                selectParcelInHomeScreen(this);
                //refreshScrollable(undefined,true)
            }
        });
        $('.select-parcelspan').unbind(touchClickEvent);
        $('.select-parcelspan').bind(touchClickEvent, function (e) { if ($(this).find('.selectParcelCheckbox').hasClass('selectedcheckbox')) { $('.select-parcelspanHeader .selectParcelCheckbox').removeClass("selectedcheckbox"); $(this).find('.selectParcelCheckbox').removeClass("selectedcheckbox") } else $(this).find('.selectParcelCheckbox').addClass("selectedcheckbox"); showMultipleMACButton(); e.stopPropagation(); });
        $('.parcel-item .close32, .parcel-item-new .close32').unbind(touchClickEvent);
        $('.parcel-item .close32, .parcel-item-new .close32').bind(touchClickEvent, function (e) {
            e.preventDefault();
            let ssClass = $(this).parents('.parcel-item-new')[0]? '.parcel-item-new': '.parcel-item';
            var pi = $(this).parents(ssClass);
            //pi.removeClass('selected');
            if ( isBPPUser && (listType == 1 || listType == 2) && !combinedView){
             	$('.normal-view-bpp', $(pi)).removeClass('hidden');
            	$('.expanded-view-bpp', $(pi)).addClass('hidden');
            	$('.normal-view', $(pi)).removeClass('hidden');
            	$('.expanded-view', $(pi)).addClass('hidden');
            }
            else if ( isBPPUser && combinedView){
            	$('.normal-view', $(pi)).removeClass('hidden');
            	$('.expanded-view', $(pi)).addClass('hidden');
            	$('.normal-view-bpp', $(pi)).removeClass('hidden');
            	$('.expanded-view-bpp', $(pi)).addClass('hidden');
            }
			else {
				$('.normal-view', $(pi)).removeClass('hidden');
            	$('.expanded-view', $(pi)).addClass('hidden');
			}
        });
        $('.select-parcelspanHeader').unbind(touchClickEvent);
        $('.select-parcelspanHeader').bind(touchClickEvent, function (e) {
            if ($(this).find('.selectParcelCheckbox').hasClass('selectedcheckbox')) {
                $(this).find('.selectParcelCheckbox').removeClass("selectedcheckbox");
                $('.select-parcelspan').each(function (i, obj) {
                    $(obj).find('.selectParcelCheckbox').hasClass('selectedcheckbox') ? $(this).find('.selectParcelCheckbox').removeClass("selectedcheckbox") : '';
                });
            } else {
                $(this).find('.selectParcelCheckbox').addClass("selectedcheckbox");
                $('.select-parcelspan').each(function (i, obj) {
                    $(obj).find('.selectParcelCheckbox').hasClass('selectedcheckbox') ? '' : $(this).find('.selectParcelCheckbox').addClass("selectedcheckbox");
                });
            }
            showMultipleMACButton();
        });
        $('.parcel-item .mark-complete').unbind(touchClickEvent);
        $('.parcel-item .mark-complete').bind(touchClickEvent, function (e) {
            e.preventDefault();
            var pid = $(this).attr('pid');
            loadParcel = true;
            markParcelAsComplete(pid, function(RPParcel){
            	if(isBPPUser && listType == 1) //in combined view we need to refresh the parcels after MAC.
               		showSortScreen(function () { reloadParcelsInList() }, false);
            });
        });
        
        $('.parcel-item-new .normal-view .mini-sort-screen-bpp').unbind(touchClickEvent);
        $('.parcel-item-new .normal-view .mini-sort-screen-bpp').bind(touchClickEvent, function ( e ) {
            e.preventDefault();
            let pid = $(this).attr('pid');
            createNewParcel(pid);
        });
        
        $( '.parcel-item .mark-as-delete' ).unbind( touchClickEvent );
        $( '.parcel-item .mark-as-delete' ).bind( touchClickEvent, function ( e ){
            e.preventDefault();
            var pid = $( this ).attr( 'pid' );
            loadParcel = true;
            markAsDelete( pid );
        } );

        if ($('.select-flag')[0].tagName.toLowerCase() == 'select') {
            if (!(isBPPUser)) {
                $('.select-flag').html('<option value="${Id}">${Name}</option>'); $('.select-flag').fillTemplate(fieldAlerts);
            }
        }
        else {
            $('.select-flag').each((i, el) => {
                if (!$(el).hasClass('.cc-drop')) {
                    $(el).lookup({ popupView: true, width: 162, searchActive: false, searchActiveAbove: 8, searchAutoFocus: false, onSave: () => { } }, []);
                    $(el)[0].getLookup.setData(fieldAlerts);
                }
            });
        }

        if (windowsTouch) {
            $('.page-wrapper,.expanded-view').bind(touchClickEvent, function (e) {
                if (e.target.type != 'select-one') {
                    $('select').blur();
                }
            });
            $('.page-wrapper,.expanded-view-bpp').bind(touchClickEvent, function (e) {
                if (e.target.type != 'select-one') {
                    $('select').blur();
                }
            });
        }

		$('.select-flag').unbind('change');
        $('.select-flag').bind('change', function (e) {
        	e.preventDefault();
            var pid = $(this).attr('pid');
            $(this).attr('selected', $(this).val());
            ccma.Sync.enqueueEvent(pid, 'FieldAlertType', $(this).val(), { updateData: true }, () => {
            	getData("SELECT * FROM Parcel WHERE  Id = " + pid + "", [], function (p) {
		            if (p[0].Reviewed) {
		                refreshSortScreen = true;
		            	executeQueries("UPDATE Parcel SET Reviewed = 'false' WHERE Id = " + pid)
		            	if(isBPPUser) ShowParcelList(false, selectedFilters);
		            	/*else refreshParcelInList();*/
		            }
		        });
            });
        });

        $('.select-flag[selected]').each(function () {
            $(this).val($(this).attr('selected'));
        });
        
        $('#my-parcels .parcel-item').each(function(){
			var pId = $(this).attr('parcelid'); 
			if($('.mark-as-delete', this).attr('pdeleted') == "true"){
				$( '.parcel-item[parcelid="' + pId + '"] .mark-as-delete' ).addClass('Bppdeleted');
		        $( '.parcel-item[parcelid="' + pId + '"] .mark-as-delete' ).html('Deleted');
		        $( '.parcel-item[parcelid="' + pId + '"] .select-flag' ).attr('disabled','disabled')
		    }
            if ($('.mark-as-delete', this).attr('rp') == "true" && ccma.Session.RealDataReadOnly == true) $('.parcel-item[parcelid="' + pId + '"] .select-flag').attr('disabled', 'disabled');
		});

        $('.parcel-item .sort-screen-select').each(function () {
            $('td.select', $(this)).removeClass('selected-cell');
            $('td[value="' + $(this).attr('selectedtype') + '"]', $(this)).addClass('selected-cell');
        });

    }
    function showMultipleMACButton() {
        if ($(".page-content span.selectParcelCheckbox.selectedcheckbox").length > 0){
            $('.multiple-mac').show();
            if ( fylEnabled ) $( '#sort-screen .fy_selector' ).hide();
            }
        else{
            $('.multiple-mac').hide();
             if ( fylEnabled ) $( '.fy_selector' ).show();
            }
    }
    function refreshParcelInList(parcelId, callback) {

        if (isEmpty(appState.parcelQuery)) {
            if (callback) callback();
        }

        loadParcelsFromQuery(appState.parcelQuery, "my-parcels", callback, null, parcelId,appState.parentFetchSql, true);
        //loadParcelsFromQuery(appState.parcelQuery, "my-parcels", callback, null, null);

    }



    function reloadParcelsInList(callback) {
        loadParcelsFromQuery(appState.parcelQuery, "my-parcels", callback, null, null, appState.parentFetchSql, true);
    }

    //function to show only priority parcels
    function loadAllParcelsInMap() {
		if(activeParcel) activeParcel=null;
        priMapMode = 0;
        changedMapBounds = true;
        loadParcelsInMap(true, true);
    }
    function BoundariesONOFF() {
        if(activeParcel) activeParcel=null;
        if (!UsingOSM) {
            ShowBoundaries == true ? ShowBoundaries = false : ShowBoundaries = true;
        }
        loadParcelsInMap(true, true);
    }

    function loadPrioritiesOnlyInMap() {
        if(activeParcel) activeParcel=null;
        priMapMode = 1;
        changedMapBounds = true;
        loadParcelsInMap(true, true);

    }
    var UnSynced = [];
    var imagesUnsynced=[];
    function loadParcelsInMap(prireload, notSetZoomBounds) {
        var filter=''
        hidePictometryInMap();
        //if (currentNbhd != null)
        //    filter = " AND NbhdId = '" + currentNbhd + "'";
        if(google && typeof clientSettings != 'undefined' && typeof clientSettings.UseOSM != 'undefined' && (clientSettings.UseOSM == 1 || clientSettings.UseOSM == true) ) {
		  UsingOSM = true; 
		}
        if(!google && !UsingOSM){
			messageBox("Aerial Imagery cache is cleared when resetting the application while the device was offline. Street and Parcel information will still be provided. Once the iPad has internet connectivity, reset the application again to have aerial imagery reloaded.",["OK","Reset"], ()=>{return;}, ()=>{
				if(navigator.onLine)
					window.resetApp(true);
				else
					messageBox("The iPad is still in offline.");
				})
			return;	
        }
        var query = "SELECT * FROM PendingParcels WHERE DistanceIndex > -1" + filter + " ORDER BY -CC_Priority, DistanceIndex";
        if (mapInitialized) {
            showGISInteractive();
            parcelMapMode = true;
            parcelLinksMode = false;

            var bounds = {}
            if (google && !UsingOSM)
                bounds = new google.maps.LatLngBounds();
            else
                bounds = new L.LatLngBounds();
            getData(query, [], function (results) {
                var hasPoints;
                for (x in results) {
                    var p = results[x];
                    if (p.Latitude && p.Longitude) {
                        var point = UsingOSM ? new L.latLng(p.Latitude, p.Longitude) : new google.maps.LatLng(p.Latitude, p.Longitude);
                        bounds.extend(point);
                        hasPoints = true;
                    }
                }
                if (hasPoints) {
                	parcelMapCenter = bounds.getCenter();
                    parcelMapBounds = bounds;
                    if (UsingOSM) {
                        if (!notSetZoomBounds && changedMapBounds == true) {
                            if(Object.keys(preMacBounds).length > 0) bounds = preMacBounds;
                            OsMap.fitBounds(bounds);
                            OsMap.getZoom() ? OsMap.setView(bounds.getCenter()) : OsMap.setView(bounds.getCenter(),19);

                        }
                    }
                    else {
                        if (!notSetZoomBounds && changedMapBounds == true) {
                            if(Object.keys(preMacBounds).length > 0) bounds = preMacBounds;
                            map.fitBounds(bounds);
                            map.setCenter(bounds.getCenter());
                        }
                    }
                } else {
                    if (!UsingOSM)
                        map.setCenter(myLocation)
                    else
                        if (OSMmyLocation)
                            OsMap.setView(OSMmyLocation,17, {animate: false});
                }
                changedMapBounds = false;
                preMacBounds = {};
                if (!prireload) {
               		 if(UsingOSM)
                    	$('.Setting').show();
                    if (listMode == 1) {
                        $('#showall').show();
                        $('.map-pri-option').show();
                        $('.map-pri-only').hide();
                        if(!ShowAll)
                        	priMapMode = 1;
                    } else {
                        $('#showall').hide();
                        $('.map-pri-option').hide();

                    }
                    $('.map-Boundaries').show();
                } else {
                	if(UsingOSM)
                    	$('.Setting').show();
                    $('.map-pri-option').show();
                    if (priMapMode == 0) {
                        $('.map-show-all').hide();
                        // $('#myonoffswitch3').removeAttr('checked');
                    } else {
                        $('.map-pri-only').hide();
                    }
                    if (listMode == 0) {
                        $('.map-pri-only').hide();
                        $('.map-show-all').hide();
                    }

                }
            	if(UsingOSM && trackMe && OSMmyLocation)
                     OsMap.setView(OSMmyLocation,19);
				IDB_getData('UnsyncedImages',[],[],[],[],function(ui){imagesUnsynced=ui;}) ;                    
                getData("SELECT * FROM ParcelChanges pc LEFT OUTER JOIN Parcel p on p.Id=pc.ParcelId WHERE p.REVIEWED !='true' AND pc.FieldId NOT IN (SELECT id from Field WHERE IsCustomField = 'true' OR DoNotIncludeInAuditTrail = 'true') AND pc.Action !='PhotoFlagMain' ", [], function (parcelsUnsynced) {
                    UnSynced = parcelsUnsynced;
                     clearMap();
                    refreshParcelsInMap();
                });
                if (ShowBoundaries == true)
                    $('.map-Boundaries').removeClass('map-Boundaries-line').addClass('map-Boundaries-Noline');
                else
                    $('.map-Boundaries').removeClass('map-Boundaries-Noline').addClass('map-Boundaries-line');
                checkPictometry();
                checkEagleView();
                checkNearMap();
                checkNearMapWMS();
                checkWoolpert();
                checkImagery();
               	if(UsingOSM && HeatLayer) {
                	$('.heatmap-items').show();
                	showLegendIcon();
                	}
                
            });

        }
    }

function getIPALoadUrl() {
	return clientSettings['EagleViewInMA.IPALoadURL'] || ''
}

function getSecretKey() {
    return clientSettings['EagleViewInMA.SecretKey'] || ''

}

function getApiKey() {
    return clientSettings['EagleViewInMA.APIKey'] || ''
}

function checkPictometry() {		
	if (getApiKey() && getIPALoadUrl() && getSecretKey() && EnableEagleViewInMA && UsingOSM) {
		if (activeParcel && $('.map-items option').length == 0) $('.map-show-pictometry').show();
		else $('.map-show-pictometry').hide();
		showPictometry = true;
	}
	else {
		$('.map-pictometry').hide();
		showPictometry = false;
	}
	return showPictometry;
}

var nbhdLoaded = false;

function loadPictometryIPA () {
	if (!navigator.onLine) {
		console.error("Failed to connect to the internet.");
		return;
	}
	else if (checkPictometry()) {
		var loadScript = function (url, callback) {
			var head = document.getElementsByTagName('head')[0];
	        var script = document.createElement('script'), loaded;
	        script.type = 'text/javascript';
	        script.src = url;
	        if (callback) {
	            script.onreadystatechange = script.onload = function () {
	                if (!loaded) {
	                    callback();
	                }
	                loaded = true;
	            };
	        }
	        head.appendChild(script);
		}
	    var getFrameId = function () {
	        return "pictometry_ipa";
	    }
        function signedUrl() {
        
            var d = new Date();
            var n = d.getTime();
            var ts = Math.trunc(Math.floor(n / 1000))

            // create the url
            var url = getIPALoadUrl() + "?apikey=" + getApiKey() + "&ts=" + ts
            // generate the hash
            var hash = CryptoJS.HmacMD5(url, getSecretKey() );

            // convert hash to digital signature string
            var signature = hash.toString().replace("-", "").toLowerCase()

            // create the signed url
            finalUrl = url + "&ds=" + signature + "&app_id=" + getFrameId()
			pictometryUrl = finalUrl;
            return finalUrl
        } 
        if (getApiKey() != '')
            loadScript("https://pol.pictometry.com/ipa/v1/embed/host.php?apikey=" + getApiKey(), function () {
            
				if (!ipa) ipa = new PictometryHost(getFrameId(), 'https://pol.pictometry.com/ipa/v1/load.php');
                
                // set the iframe src to load the IPA
                var iframe = document.getElementById(getFrameId());
                iframe.setAttribute('src', signedUrl());

                ipa.ready = function () {
                	isIpaLoaded = true;
                    ipa.addListener('location', function (location) {
                        alert(location.y + ", " + location.x);
                    });
                    ipa.addListener("error", function (error) {
            			$('#maskLayer').hide();
	                    console.error("EagleView Error: " + JSON.stringify(error));
	                });
	                ipa.showDashboard({ exportImage : false, dualpane :false, exportPdf :false, annotations :false });
	                ipa.setImageFilterLatest();
                }
            });
    }
}


function loadPictometryInMap (loadCallback) {
	if (!navigator.onLine) {
		if (loadCallback) loadCallback();
		console.error("Failed to connect to the internet.");
		return;
	}
	else if (checkPictometry()) {
		if (loadCallback) loadCallback();
		$('#maskLayer').hide();
 	}     
    else { 
    	console.warn("EagleView is not enabled.");
    	if (loadCallback) loadCallback();
	}
}

function showPictometryInMap () {        
	if (!isIpaLoaded) {
		$('.map-items').val('');
		hidePictometryInMap();
		if (!isIpaLoaded) messageBox("EagleView is not configured at this time. Please contact support with your EagleView keys for troubleshooting or setup.");
		return false;
	}
	if (!navigator.onLine) {
		$('.map-items').val('');
		hidePictometryInMap();
		console.error("Failed to connect to the internet.");
		messageBox("Sorry, EagleView map won't be available in offline mode.", function () {
			return false;
		});
	} 
	else {
		showMask('Loading..');
		hideAllMapViews();
		function SetParcelCenter(callback) {     
            if (UsingOSM) {
            	var coord = bounds.getCenter();
            	var defaultZoom = 19;
            	if(clientSettings['EagleViewDefaultZoom'])
					defaultZoom = clientSettings['EagleViewDefaultZoom'];
				if(defaultZoom > 23){
					defaultZoom =23;
				}else if(defaultZoom < 15){
					defaultZoom =15;	
				}	
                ipa.setLocation({
                    y: coord.lat,       				// Latitude
                    x: coord.lng,      					// Longitude
                    zoom: (activeParcel ? defaultZoom : 17)      // Optional Zoom level
                });
            }
            if(callback) callback();
        }
		var polygon = [], shapeIds = [], shapes = [], bounds = new L.LatLngBounds();
		var query = "SELECT * FROM MapPoints WHERE ParcelId <> '-9999'";
		if (activeParcel) query += " AND ParcelId = '" + activeParcel.Id + "' ORDER BY Ordinal*1" ;
		getData(query, [], function (results) {
			for (x in results) {
                var p = results[x];
				if (shapes[p.RecordNumber || 0] == null) {
                    shapes[p.RecordNumber || 0] = [];
                }
                shapes[p.RecordNumber || 0].push({ y: p.Latitude, x: p.Longitude });
                if (p.Latitude && p.Longitude) {
                    var point = UsingOSM ? new L.latLng(p.Latitude, p.Longitude) : new google.maps.LatLng(p.Latitude, p.Longitude);
                    bounds.extend(point);
                }
            }
            SetParcelCenter(function () {
            	if(!activeParcel) shapes = [];
				shapes.forEach(function (w) {
					polygon.push({
						type: ipa.SHAPE_TYPE.POLYGON,
						coordinates: w,
						strokeColor: "#FFFF00",
						strokeOpacity: 0.75,
						strokeWeight: 2,
						visible: true
					});
				});
				ipa.addShapes(polygon, function (result) {
					for (var i = 0; i < result.length; i++) {
						if (result[i].success === false) 
							alert(result[i].error);
						else 
							shapeIds.push(result[i].shapeId);
					}
					$('#maskLayer').hide();
					checkNearMap();
                    $('.map-hide-nearmap').hide();
                    checkNearMapWMS();
                    $('.map-hide-nearmapwms').hide();
					checkImagery();
					$('.map-hide-Imagery').hide();
					if(clientSettings.EnableqPublic && clientSettings.EnableqPublic == '1'){
						$('.q_public button').html('Show qPublic');
						$('.q_public button').attr('onclick','show_qPublic();');
					}
					hideSvOverlay();
					$('#mapCanvas').hide();
					$('#pictometry').show();
					$('.map-show-pictometry').hide()
					if($('.map-items option').length == 0) $('.map-hide-pictometry').show();
					$('#lblpop').hide();
					$('.heatmap-items').hide();
				});
            });
        });
    }
}

function hidePictometryInMap(doNotRefresh) {
	if($('.map-items option').length == 0 || ($('.map-items option').length >  0 && !activeParcel)) $('#mapCanvas').show();
	$('#pictometry').hide();
	if(checkPictometry()) {
		$('.map-hide-pictometry').hide();
		if (ipa) ipa.removeAllShapes();
		if (!doNotRefresh) {
			if (activeParcel) {
				$('#lblpop').hide();
				showSvOverlay();
                showGISInteractive();
			}
			else {
				$('#lblpop').show();
				//$('header a.map-view', $('#sort-screen')).trigger('click');
				if (HeatLayer) $('.heatmap-items').show();
			}
		}
	}
}

function hideEagleInMap(doNotRefresh) {
    if ($('.map-items option').length == 0 || ($('.map-items option').length > 0 && !activeParcel)) $('#mapCanvas').show();
    $('#eagleView').hide();
    if (checkEagleView()) {
        $('.map-hide-ev').hide();
        removeEVScripts();
        if (!doNotRefresh) {
            if (activeParcel) {
                $('#lblpop').hide();
                showSvOverlay();
                showGISInteractive();
            }
            else {
                $('#lblpop').show();
                if (HeatLayer) $('.heatmap-items').show();
            }
        }
    }
}
	
    function selectParcelInHomeScreen(that) {
    	let isMiniSS = $(that).hasClass('parcel-item-new')? true: false;
        $( '.parcel-item,.parcel-item-new' ).removeClass('selected');        
        if(listType != 0 || combinedView || isMiniSS)
            $(that).addClass('selected');
	
        var mode = 1 - parseInt($(that).attr('mode'));
        $(that).attr('mode', mode);
        if (isMiniSS && mode != "0") {
        	$('.normal-view', $(that)).addClass('hidden');        	
        	$('.normal-view-bpp', $(that)).addClass('hidden');
        	$('.expanded-view-bpp', $(that)).addClass('hidden');
        	$('.expanded-view', $(that)).removeClass('hidden');
        }
        else if ( mode != "0" && (listType != 0 || combinedView )) {
        	if ( isBPPUser &&( listType == 1 || listType == 2 ) && !combinedView){
        		$( '.normal-view-bpp', $( that ) ).addClass( 'hidden' );
            	$( '.expanded-view-bpp', $( that ) ).removeClass( 'hidden' );
            	if(listType != 1) $( '.normal-view', $( that ) ).addClass( 'hidden' );
            	$( '.expanded-view', $( that ) ).removeClass( 'hidden' );
           }
           else {
           		$( '.normal-view', $( that ) ).addClass( 'hidden' );
            	$( '.expanded-view', $( that ) ).removeClass( 'hidden' );
            	$( '.normal-view-bpp', $( that ) ).addClass( 'hidden' );
            	$( '.expanded-view-bpp', $( that ) ).removeClass( 'hidden' );
           }
        }



        var pid = parseInt($(that).attr('parcelid'));
        //var EnableGroupProfile = clientSettings["EnableGroupProfileView"] || '0';
         var EnableGroupProfile = '0';
    	if (clientSettings["EnableGroupProfileView"])
	  		EnableGroupProfile = (clientSettings["EnableGroupProfileView"].toUpperCase().indexOf("MA") > -1 ||  clientSettings["EnableGroupProfileView"] == '1') ? '1' : '0';
		else
			EnableGroupProfile = clientSettings["EnableGroupProfileView"] == undefined ? '1': clientSettings["EnableGroupProfileView"] == '0' ? '0' : '0';
        if (EnableGroupProfile == '1') {
            $('.nbhd-profile-float').removeClass("hidden");
        }
        else {
            $('.nbhd-profile-float').addClass("hidden");
        }
        if (enableMultipleNbhd === undefined)
            enableMultipleNbhd = (clientSettings["EnableMultipleNbhd"] ? ('1' ? true : false) : false) || false;
        if (!nbhdLoaded) {

            try {
                if (enableMultipleNbhd && Object.keys(neighborhoods).length > 1) {
                    if (!currentNbhd || refreshNbhdProfile) {
                        fillNbhdProfileForMultiNbhd();
                    }
                }
                else {
                    var nbhd = $(that).attr('nbhd');
                    var n = neighborhoods[nbhd.toString()];
                    if (!n) {
                        n = [{ Name: nbhd, Number: nbhd }];
                    }
                    if (n) {
                        nb = new Parcel(nbhd, n[0]);
                        $('.nbhd-profile-float').html($('#nbhd-profile-template').html());
                        $('.nbhd-profile-float').fillTemplate([nb]);
                    }
                }
                //refreshScrollable( null, true );
            } catch (e) {
                console.error(e);
                $( '.nbhd-profile-float' ).html( e );
                //refreshScrollable();
            }

        }

        bindSortScreenEvents();

    }

    function fillNbhdProfileForMultiNbhd() {
        var template = $('#nbhd-profile-template').html(), spanTemplate = '';
        template = template.replace('${Number}', '<input style="width: 150px;" class = "nbhd-profile-select"/>')
        template = template.split('<table ')
        if (template[1].indexOf('span') > -1) {
        	spanTemplate = template[1].split('<span>')[1].split('</span>')[0]
        	var span = template[1].split('<span')
        	template[1] = span.join('<span class="aggregateField" ')
        }
        var profileSelectTemplate = '<table ' + template[1];
        var grpProfileTemplate = '<table style="display:none;"' + template[2];
        var grpProfile;
        var profileGrp = document.createElement("span");
        var n = [], nbs = [];
        for (i = 0; i < Object.keys(neighborhoods).length; i++) {
            n.push({ NbhdId: neighborhoods[Object.keys(neighborhoods)[i]][0].Id, Number: neighborhoods[Object.keys(neighborhoods)[i]][0].Number, Name: neighborhoods[Object.keys(neighborhoods)[i]][0].Number });
            nbs.push({ Id: neighborhoods[Object.keys(neighborhoods)[i]][0].Id, Name: neighborhoods[Object.keys(neighborhoods)[i]][0].Number });
        }
        var spans;
        for (i = 0; i < n.length; i++) {
            nb = new Parcel(n[i].Name, neighborhoods[Object.keys(neighborhoods)[i]][0]);
            grpProfile = document.createElement("span")
            $(grpProfile).html(grpProfileTemplate);
            $(grpProfile).fillTemplate([nb]);
            $($(grpProfile).children()[0]).addClass('nbhd-profile-table');
            $($(grpProfile).children()[0]).addClass('nbhd-' + n[i].NbhdId);
            $(profileGrp).append($(grpProfile).html());
        }

        //var createElem = document.createElement("select")
        //$(createElem).html('<option value="${NbhdId}">${Number}</option>')
        //$(createElem).fillTemplate(n)

        $('.nbhd-profile-float').html(profileSelectTemplate)
        $('.nbhd-profile-float').append($(profileGrp).html())
        //$('.nbhd-profile-select').html($(createElem).html());
        $('.nbhd-profile-select').each((i, el) => {
            $(el).removeClass('cc-drop');
            $(el).empty();
            $(el).lookup({ popupView: true, width: 166, searchActive: false, searchActiveAbove: 8, searchAutoFocus: false, onSave: () => { } }, []);
            $(el)[0].getLookup.setData(nbs);
            $(el).val(nbs[0].Id);
        });
        
        

		if (spanTemplate != '') {
        	$('.aggregateField').html('')
			for (i = 0; i < n.length; i++) {
				nb = new Parcel(n[i].Name, neighborhoods[Object.keys(neighborhoods)[i]][0]);
				spans = document.createElement('span');
				$(spans).html(spanTemplate)
				$(spans).fillTemplate([nb])
				$('.aggregateField').append('<span style="display: none;" class="aggrField nbhd-' + n[i].NbhdId +'">' + $(spans).html() + '</span>');
			}
		}
        var index = (appState.screenId == 'search') ? 1 : 0;
        if (changedNbhd) {
            $('.nbhd-' + changedNbhd).show();
            $('.nbhd-profile-select').val(changedNbhd);
            currentNbhd = changedNbhd;
            changedNbhd = null;
        }
        else
            $('.nbhd-' + n[0].NbhdId).show();
        $('.nbhd-profile-select').change(function () {
            $('.nbhd-profile-table').hide();
            $('.aggrField').hide()
            var index = (appState.screenId == 'search') ? 1 : 0;
            currentNbhd = $(this).val().toString();
            $('.nbhd-' + currentNbhd).show();
            changedNbhd = (appState.screenId == 'search') ? currentNbhd : null;
            //changedMapBounds = true;
            //ShowParcelList(false)
        });
        refreshNbhdProfile = false;
        return;
    }

    /*
    SEARCH FUNCTIONALITY
    */


    function ClearSearch() {
        $('.search-fld').val('');
    }

    var searchedCount = 0;

    function SearchParcels(hidevisited) {
        var assr = $('.search-assr').val();
        var parc = $('.search-parcel').val();
        var addr = $('.search-street').val();
        if ((assr == "") && (parc == "") && (addr == "")) {
            messageBox('Please fill up any one of the fields to search.');
            return;
        }

        $('#search-parcels').html('<div class="sort-screen-message">Please wait ...</div>');

        var visitedFilter = "";
        if (hidevisited == true) {
            visitedFilter = " AND Reviewed = 'false'"
        }

        var sql = "SELECT * FROM SubjectParcels WHERE (KeyValue1 LIKE '" + parc + "%') AND (StreetAddress LIKE '%" + addr.replace("*", "%") + "%')" + visitedFilter + " ORDER BY -CC_Priority, StreetAddress LIMIT 30";
        loadParcelsFromQuery(sql, "search-parcels", null, function (parcels) {
            searchedCount = parcels.length;
            if (parcels.length == 0) {
                $('#search-parcels').html('<div class="sort-screen-message">No parcels found!</div>');
                return false;
            }
            return true;
        });
    }

    function SetSearchType(type) {
        if (type) {
            $('.parcel-search-type').val(type);
        }
        var t = $('.parcel-search-type').val();
        if (t.contains('-'))
            t = t.slice(0, t.indexOf('-'))
        console.log(t);
        $('.search-optional').hide();
        $('.option-' + t).show();

        $('.pq-optiroute')[0].checked = false;
        $('.pq-uselocation')[0].checked = true;
        $('.pq-hidevisited')[0].checked = true;
        $('.pq-showprionly')[0].checked = false;
        $('.pq-nogis')[0].checked = true;
        $('.pq-noaddress')[0].checked = true;
        $('input[type="checkbox"][name="search-priority__radio"]').forEach(cb => cb.checked = false);
    }

    function OnSelectSearchStreetDirection() {
        var direction = $('.pq-direction').val();
        switch (direction) {
            case "0": //Forward by Number
            case "1": //Reverse by Number
                $('.pq-addrsel').removeAttr('disabled');
                break;
            case "2": //Forward by Parcel ID
            case "3": //Reverse by Parcel ID
                $('.pq-addrsel').val("0");
                $('.pq-addrsel').attr('disabled', 'disabled');
                break;
        }
    }

    function PerformSearch() {
        //    pq - type
        //    pq - pids
        //    pq - street
        //    pq - direction
        //    pq - addrsel
        //    pq - address
        //    pq - optiroute
        //    pq - uselocation

        var pqtype = $('.pq-type').val();
        var pids = $('.pq-pids').val();
        var street = $('.pq-street').val();
        var direction = $('.pq-direction').val();
        var addrsel = $('.pq-addrsel').val();
        var address = $('.pq-address').val();
        var alert = $('.pq-alertfromoffice').val();
        var optiroute = $('.pq-optiroute')[0].checked;
        var uselocation = $('.pq-uselocation')[0].checked;
        var hidevisited = $('.pq-hidevisited')[0].checked;
        var showprionly = $('.pq-showprionly')[0].checked;
        var nogis = $('.pq-nogis')[0].checked;
        var noaddress = $('.pq-noaddress')[0].checked;
        var fieldids = $('.pq-field').val();
        var filter = '';
        var orderBy = '';
        var listTitle = '';

        if (pqtype == 'parcelpriority' && $('input[type="checkbox"][name="search-priority__radio"]:checked').length == 0) return;

        switch (pqtype) {
            case "allpriority":
                listTitle = 'Nearby NNN Priority Parcels';
                filter = EnableNewPriorities == 'True'? 'CC_Priority IN (1,2,3,4,5)' : 'CC_Priority IN (1,2)';
                orderBy = '-CC_Priority * 1.0, DistanceIndex * 1.0';
                break;
            case "urgentpriority":
                listTitle = 'Nearby NNN Urgent Parcels';
                filter = EnableNewPriorities == 'True' ? 'CC_Priority IN (4)' : 'CC_Priority IN (2)';
                orderBy = 'DistanceIndex * 1.0';
                break;
            case "highpriority":
                listTitle = 'Nearby NNN High Parcels';
                filter = EnableNewPriorities == 'True' ? 'CC_Priority = 3' : 'CC_Priority = 1';
                orderBy = 'DistanceIndex * 1.0';
                break;
            case "parcelpriority":
                let pr = ''; $('input[type="checkbox"][name="search-priority__radio"]:checked').forEach((el) => { pr = pr + $(el).val() + ','; }); pr = pr.slice(0, -1);
                listTitle = 'Nearby NNN Priority Parcels';
                filter = 'CC_Priority IN (' + pr + ')';
                orderBy = '-CC_Priority * 1.0, DistanceIndex * 1.0';
                break;
            case "proximity":
                listTitle = 'Nearby NNN Parcels';
                filter = 'CC_Priority = 0';
                orderBy = 'DistanceIndex * 1.0';
                break;
            case "parcel":
                listTitle = 'NNN Parcels found';
                if (pids.toString().trim() == '') return;
                var apid = pids.toString().split(",");
                if (apid.length > 1) {
                    var spid = '';
                    for (x in apid) {
                        var pid = apid[x].toString().trim();
                        if (spid == '')
                            spid += "'" + pid + "'";
                        else
                            spid += ", '" + pid + "'";
                    }
                    ShowAlternateField == "True" ? isBPPUser ? filter = AlternateKey+' IN (' + spid + ') OR KeyValue1 IN (' + spid + ')' : filter = AlternateKey+' IN (' + spid + ') OR KeyValue1 IN (' + spid + ')' : filter = 'KeyValue1 IN (' + spid + ')';
                } else {
                    ShowAlternateField == "True" ? isBPPUser ? filter = AlternateKey+' LIKE \'%' + pids + '%\' OR KeyValue1 LIKE \'%' + pids + '%\'' : filter = AlternateKey+' LIKE \'%' + pids + '%\' OR KeyValue1 LIKE \'%' + pids + '%\'' : filter = 'KeyValue1 LIKE \'%' + pids + '%\'';
                }
                orderBy = 'DistanceIndex * 1.0';
                hidevisited = false;
                break;
            case "street":
                listTitle = 'Parcels on ' + street;
                filter = "(StreetAddress LIKE '%" + street + "%')";
                optiroute = false;
                switch (direction) {
                    case "0": //Forward by Number
                        orderBy = 'CAST(StreetNumber AS INT)';
                        break;
                    case "1": //Reverse by Number
                        orderBy = '-CAST(StreetNumber AS INT)';
                        break;
                    case "2": //Forward by Parcel ID
                        orderBy = 'KeyValue1';
                        break;
                    case "3": //Reverse by Parcel ID
                        orderBy = '-KeyValue1';
                        break;
                }
                switch (addrsel) {
                    case "0":
                        break;
                    case "1":
                        filter += " AND CAST(StreetNumber AS INT) % 2 = 1"
                        break;
                    case "2":
                        filter += " AND CAST(StreetNumber AS INT) % 2 = 0"
                        break;
                }
                if (showprionly) {
                    filter += ' AND CC_Priority IN (1,2)';
                }
                break;
            case "address":
                listTitle = 'NNN Parcels found';
                //filter = "(StreetAddress LIKE '%" + address.replace("*", "%") + "%')";
                filter = address.trim() == '' ? "(StreetAddress IS NULL)":"(StreetAddress LIKE '%" + address.replace("*", "%") + "%')";
                orderBy = 'StreetAddress'
                hidevisited = false;
                break;
           case "alertfromoffice":
                listTitle = 'NNN Parcels found';
                filter = alert.trim() == '' ? "(ParcelAlert IS NULL)": "(ParcelAlert LIKE '%" + alert.replace("*", "%") + "%')";
                orderBy = 'ParcelAlert'
                hidevisited = false;
                break;
            case "no-address":
                listTitle = 'NNN Parcels found';
                filter = " 1 = 1";
                if (nogis) {
                    filter += " AND Id NOT IN (SELECT DISTINCT ParcelId FROM MapPoints)"
                }

                if (noaddress) {
                    filter += " AND (StreetAddress IS NULL OR StreetAddress = '')";
                }
                if (!nogis && !noaddress) {
                    filter = "0 = 1";
                }
                orderBy = 'KeyValue1'
                break;
            default:
                pqtype = pqtype.slice(pqtype.indexOf('-') + 1)
                listTitle = 'NNN Parcels found';
                if (fieldids.toString().trim() == '') return;
                var apid = fieldids.toString().split(",");
                if (apid.length > 1) {
                    var spid = '';
                    for (x in apid) {
                        var pid = apid[x].toString().trim();
                        if (spid == '')
                            spid += "'" + pid + "'";
                        else
                            spid += ", '" + pid + "'";
                    }
                    filter = pqtype + ' IN (' + spid + ')';
                } else {
                    filter = pqtype + ' LIKE \'%' + fieldids + '%\'';
                }
                orderBy = 'DistanceIndex * 1.0';
                hidevisited = false;
                break;
        }
        var selectQuery = '';
        if ( isBPPUser && !combinedView ){
           if ( listType == 0 || (listType == 1 && parcelMapMode) ){
                listType = 0;
                filter += " AND IsParentParcel ='true' ";
                $( '.map-view' ).show();   $('.change-sort-view').show(); selectQuery = ',(Select count(*) from PendingParcels where parentParcelId = p.Id) BPPCount, (SELECT CASE WHEN Count(*) > 0  THEN "auto" ELSE "none" END from SubjectParcels where parentParcelId = p.Id and reviewed ="true") AnyChildReviewed '
            }
            else if ( listType == 1 )            {
                filter += " AND IsPersonalProperty = 'true' AND ParentParcelID = " + activeParentParcel.Id + " ";
                $( '.map-view' ).hide();
				  $('.change-sort-view').hide();                
            }
            else if ( listType == 2 )
                filter += " AND IsPersonalProperty = 'true' ";
        }
        if (hidevisited) {
            filter += " AND Reviewed = 'false'";
        }

        var allSql = 'SELECT *' + selectQuery + ' FROM SubjectParcels p ' + ( filter != '' ? ' WHERE ' + filter : '' ) + ( orderBy != '' ? ' ORDER BY ' + orderBy : orderBy );
        var showSql = allSql + ' LIMIT 30';
        changedNbhd = $('.nbhd-profile-select').val();
        refreshNbhdProfile = true;
        appState.homeParcelSelectedIndex = 0;
        appState.homeParentParcelSelectedIndex = 0;
        
        var loadResultsInSortScreen = function (routingCall) {
            loadParcelsFromQuery(showSql, 'my-parcels', null, function (parcels) {
                if (isBPPUser && combinedView) {
                    for (let i = 0; i < parcels.length; i++) {
                        if (ParcelsNotInGroup.includes(parseInt(parcels[i].Id))) {
                            parcels.splice(i, 1);
                            i--;
                        }
                    }
                }

                if (parcels.length == 0) {
                    $('.nbhd-profile-select').val(changedNbhd);
                    $('.nbhd-profile-select').trigger('change');
                }

                if (routingCall) {

                    routingCall(parcels, function () {
                        setSearchResultsTitle(listTitle, parcels.length);
                        showSortScreen(null, true);
                    });
                } else {
                    setSearchResultsTitle(listTitle, parcels.length);
                    showSortScreen(null, true);
                }
                return true;
            });
        }

        $('.btn-parcel-search').attr('disabled', 'disabled');
        $('.btn-parcel-search').html('Searching ...');
        //$('.btn-parcel-search').attr('disabled', 'disabled');
        if (optiroute) {
            if (pqtype == "allpriority") {
                ComputePriorityAndAlertRoutes(loadResultsInSortScreen);
            } else {
                loadResultsInSortScreen(function (parcels, callback) {
                    routing.calculateRoute("mapquest", parcels, myLocation, null, callback);
                });
            }
        } else {
            loadResultsInSortScreen();
        }
    }
    
    function showMask(info, hideLoadText) {
        $('#maskLayer').show();
        $('#loadText').html(info);
        if (hideLoadText) $('#loadText').parent().hide();
        else $('#loadText').parent().show();
    }

    $(document).on(touchClickEvent, function (e) {
        if (!$(e.target).hasClass('future-year') && $(e.target).closest(".Setting").length === 0 && $(e.target).closest(".poptable").length === 0)
            HideMapPopup();
        if ( $( e.target ).closest( ".ccse-options" ).length === 0 && $( e.target ).closest( ".ccse-option-list" ).length === 0 )
        $( '.ccse-option-list' ).hide();
    });
    
    function clearMapStore() {
        currentHeatMapField = null;
        currentHeatMapFieldId = null;
        var mapCached = ["AerialMap", "ShowLines", "ShowAllCached", "HeatMap", "StreetMap", "StreetView", "CLayer", "currentHeatMapField", "currentHeatMapFieldId"];
        for (i = 0; i < mapCached.length; i++) {
            localStorage.removeItem(mapCached[i]);
        }
    }

function createAggrFieldsForNbhd(callback) {
	if (!clientSettings.EnableGroupProfileView || clientSettings.EnableGroupProfileView != "1") {
		if (callback) callback();
		return;
	}
    getData("SELECT * FROM AggregateFieldSettings ORDER BY tablename,FunctionName='FIRST', FunctionName='LAST'", null, function (list) {
        var tables = '(select parcel.id,parcel.NeighborhoodId from parcel WHERE parcel.NeighborhoodId IS NOT NULL) parcel';
        var fieldselect = '', innerQuery = '', count = 0, convertionquery = '', fName = '', firstQuery = [], firstFieldSelect = [], medianQuery = [], parcelNbhdRes;
        list.forEach(function (a) {
            if (!a.TableName || a.TableName == '' || !a.FunctionName || !a.FieldName || ( a.FunctionName == 'MEDIAN' && !iPad ) ) return
            convertionquery = a.FunctionName == 'MIN' || a.FunctionName == 'MAX' || a.FunctionName == 'SUM' || a.FunctionName == 'AVG' ? convertionquery = '* 1' : '';
            fName = a.FunctionName + '_' + a.TableName + '_' + a.FieldName;
            if (a.FunctionName == 'FIRST' || a.FunctionName == 'LAST') {
                firstFieldSelect.push(fName);
                var sql = 'SELECT par.NeighborhoodId nbhdId'
                sql += ' , ' + '(' + a.TableName + '.' + a.FieldName + ')' + ' as ' + fName;
                sql += ' from ' + a.TableName + ' INNER JOIN Parcel par ON par.Id = ' + a.TableName + '.' + 'CC_ParcelId group by par.NeighborhoodId ' + (a.FunctionName == 'FIRST' ? 'HAVING MAX(1000000000000- ' + a.TableName + '.' + 'ROWUID)' : '');
                firstQuery.push(sql);
                count = 0;
            }
            else if( a.FunctionName == 'MEDIAN' ) {
                var medianSql = 'SELECT cc_parcelid, AVG(Median_Value * 1) AS '+ fName;
                medianSql += ' FROM ( SELECT cc_parcelid, ' + a.TableName + '.' + a.FieldName + ' AS Median_Value, COUNT(*) OVER (PARTITION  BY cc_parcelid) AS c, ROW_NUMBER() OVER ( PARTITION  BY cc_parcelid ORDER BY CAST( ' + a.TableName + '.' + a.FieldName + ' AS NUMERIC ) ) AS rn FROM ' + a.TableName + ') AS dummy_table WHERE rn IN ( (c + 1)/2, (c + 2)/2 ) group by cc_parcelid ';
                medianQuery.push({ fieldSet: fName, fieldQuery: medianSql }); 
            }
            else {
                fieldselect += (',' + (a.FunctionName == 'COUNT' ? 'SUM' + '(' + fName + ')' : a.FunctionName + '(' + fName + ')'));
                if (count == 0 || innerQuery == '') innerQuery += ' LEFT OUTER JOIN( SELECT ' + a.TableName + '.' + 'cc_parcelid '
                if (list.filter(function (l) { return (l.TableName == a.TableName && l.FunctionName != 'FIRST' && l.FunctionName != 'LAST' && l.FunctionName != 'MEDIAN') }).length == count + 1) {
                    innerQuery += ' , ' + a.FunctionName + '(' + (a.FunctionName == 'COUNT' ? '*' : a.TableName + '.' + a.FieldName + convertionquery) + ')' + ' as ' + fName;
                    innerQuery += ' from ' + a.TableName + ' group by cc_parcelid) ' + a.TableName + ' ON parcel.Id = ' + a.TableName + '.cc_parcelid'
                    count = 0;
                }
                else {
                    innerQuery += ' , ' + a.FunctionName + '(' + (a.FunctionName == 'COUNT' ? '*' : a.TableName + '.' + a.FieldName + convertionquery) + ')' + ' as ' + fName;
                    count += 1;
                }
            }
        });
        
        var sqlAggregatequery = innerQuery ? 'SELECT parcel.NeighborhoodId nbhdId' + fieldselect + ' FROM ' + tables + innerQuery + ' GROUP BY parcel.NeighborhoodId' : 'SELECT 1 as ID';
        var result = {};
        for (var i in neighborhoods) 
          if(neighborhoods[i][0]["NbhdId"])
            {
              result[parseInt(neighborhoods[i][0]["NbhdId"])] = {};    
            }
           else  
        	  result[parseInt(neighborhoods[i][0]["Id"])] = {};
    	
        getData(sqlAggregatequery, [], function (res) {
            var executeFirstQuery = function (callback) {
                if (firstQuery.length > 0) {
                    var query = firstQuery.pop();
                    getData(query, [], function (r) {
                        r.forEach(function (res) {
                            var field = Object.keys(res).filter(function (f) { return f != 'nbhdId' })
                            if (result[res['nbhdId']] === undefined) result[res['nbhdId']] = {};
                            result[res['nbhdId']][field] = res[field];
                        });
                        executeFirstQuery(callback);
                    })
                }
                else if (callback) callback();
            }

            var medianCalculation = function (mcallback) {
                if (medianQuery.length > 0) {
                    var medResult = medianQuery.pop();
                    var mQuery = medResult.fieldQuery, mField = medResult.fieldSet;
                    getData(mQuery, [], function (mdResult) {
                        Object.keys(neighborhoods).forEach(function (nhd) {
                            var nhdId = nhd[0].Id, medianResult = null;
                            var medianNbhdResult = mdResult.filter( mdres => parcelNbhdRes.some( pnd => ( pnd.NeighborhoodId == nhdId &&  pnd.id == mdres.cc_parcelid ) ) ).map( mNhres => mNhres.mField? parseFloat(mNhres.mField): -9999 );
                            medianNbhdResult = medianNbhdResult.sort();
                            var mCount = medianNbhdResult.length;
                            medianResult = ( mCount > 0? ( mCount.length % 2 == 0 ? ( ( medianNbhdResult[(mCount / 2) - 1] + medianNbhdResult[(Math.ceil((mCount + 1) / 2)) - 1] ) / 2 ): medianNbhdResult[((mCount + 1) / 2) - 1]  ): null );
                            medianResult = medianResult == -9999? null: medianResult;
                            neighborhoods[nhd][0][mField] = medianResult;
                        });
                        medianCalculation(mcallback);
                    });
                }
                else if (mcallback) mcallback();
            }
    
            executeFirstQuery(function () {
                getData("select parcel.id,parcel.NeighborhoodId from parcel WHERE parcel.NeighborhoodId IS NOT NULL", [], function (pnRes) {
                    parcelNbhdRes = pnRes;
                    medianCalculation(function () {
                        var fields = fieldselect.substring(1).split(',');
                        Object.keys(neighborhoods).forEach(function (n) {
                            var nbhdId = neighborhoods[n][0]['Id'];
                            var values = res.filter(function (r) { return r['nbhdId'] == nbhdId })[0];
                            fields.forEach(function (f) {
                                if (f != "" && f.indexOf('FIRST') == -1 && f.indexOf('LAST') == -1) {
                                    var field = f.split('(')[1].split(')')[0];
                                    if (f.indexOf('AVG') > -1) values[f] = Math.round(parseInt(values[f]) * 100) / 100
                                    neighborhoods[n][0][field] = values[f];
                                }
                            });
                            firstFieldSelect.forEach(function (f) {
                                neighborhoods[n][0][f] = result[nbhdId][f];
                            })
                        });
                        if (callback) callback();
                    });
                });
            });
        });
    });
}

function show_qPublic() {
	if (!clientSettings.qPublicKeyField || !clientSettings.qPublicKeyField.toString()) return;
	hideAllMapViews();
	$('#qPublic').show();
	$('.map-items').val('qPublic');
	var url = "https://qpublic.schneidercorp.com/application.aspx?app=" + org_county_name;
	url += "County" + orgaddress.split(',')[1] + "&layer=Parcels&PageType=Map&KeyValue=";
	url += encodeURI(activeParcel[clientSettings.qPublicKeyField]);
	checkPictometry();
	$('.map-hide-pictometry').hide();
    checkNearMap();
    $('.map-hide-ev').hide();
    checkEagleView();
    removeEVScripts();
    $('.map-hide-nearmap').hide();
    checkNearMapWMS();
    $('.map-hide-nearmapwms').hide();
	checkImagery();
	$('.map-hide-Imagery').hide();
	hideSvOverlay();
	$('#maskLayer').hide();
    $('#mapCanvas').hide();
	$('#qPublic_frame').attr('src', url);
	$('#qPublic_frame').height(ccma.UI.Layout.Screen.UsableHeight);
	$('.q_public button').html('Hide qPublic');
	$('.q_public button').attr('onclick','hide_qPublic();');
}

function hide_qPublic(doNotRefresh) {
    $('#mapCanvas').show();
	$('#qPublic').hide();
	$('.q_public button').html('Show qPublic');
    $('.q_public button').attr('onclick', 'show_qPublic();');
    if (!doNotRefresh && activeParcel) {
        showSvOverlay();
        showGISInteractive();
    }
}

function getParentParcel(parentFetchSql,callback){
	if(parentFetchSql){
		getData(parentFetchSql,[],function(pParcel){
			if(callback) callback(pParcel);
		});
    }
	else{
		if(callback) callback([]);
    }
}

function selectNbhdInHomescreen()
{      
        var EnableGroupProfile = '0';
    	if (clientSettings["EnableGroupProfileView"])
	  		EnableGroupProfile = (clientSettings["EnableGroupProfileView"].toUpperCase().indexOf("MA") > -1 ||  clientSettings["EnableGroupProfileView"] == '1') ? '1' : '0';
		else
			EnableGroupProfile = clientSettings["EnableGroupProfileView"] == undefined ? '1': clientSettings["EnableGroupProfileView"] == '0' ? '0' : '0';
        if (EnableGroupProfile == '1') {
            $('.nbhd-profile-float').removeClass("hidden");
        }
        else {
            $('.nbhd-profile-float').addClass("hidden");
        }
        if (enableMultipleNbhd === undefined)
            enableMultipleNbhd = (clientSettings["EnableMultipleNbhd"] ? ('1' ? true : false) : false) || false;
        if (!nbhdLoaded) {

            try {
                if (enableMultipleNbhd && Object.keys(neighborhoods).length > 1) {
                    refreshNbhdProfile=true
					if (!currentNbhd || refreshNbhdProfile) {
                        fillNbhdProfileForMultiNbhd();
                    }
                }
                else {
                    var nbhd = Object.keys(neighborhoods)[0];
                    var n = neighborhoods[Object.keys(neighborhoods)[0]];
                    if (!n) {
                        n = [{ Name: nbhd, Number: nbhd }];
                    }
                    if (n) {
                        nb = new Parcel(nbhd, n[0]);
                        $('.nbhd-profile-float').html($('#nbhd-profile-template').html());
                        $('.nbhd-profile-float').fillTemplate([nb]);
                    }
                }                
            } catch (e) {
                console.error(e);
                $( '.nbhd-profile-float' ).html( e );                  
	    }
        }
}

function convertSortScreenSelectToInput(el) {
    let selFlagHtml = $('.select-flag', el)[0]?.outerHTML;
    if (selFlagHtml && selFlagHtml.contains('<select')) {
        $('.select-flag', el).removeClass(".cc-drop");
        $('.select-flag', el)[0].innerHTML = "";
        selFlagHtml = $('.select-flag', el)[0].outerHTML.replace('<select', '<input type="text"').replace('></select>', "/>");
        let selectRegex = new RegExp(`<select\\s+[^>]*class=["']([^"']*)\\bselect-flag\\b([^"']*)["'][^>]*>(.*?)</select>`, 'g'); //new RegExp(`<select\\s+class="select-flag"(.*?)</select>`, 'g');
        el.innerHTML = el.innerHTML.replace(selectRegex, selFlagHtml);
        $('.select-flag', el).css('width', '145px');
    }
}

function provalResSorting(arr, order) {
    let field = order.split(" ")[0].trim();
    arr.sort((a, b) => {
        if (a[field] === 'B') {
            return -1;
        } else if (b[field]  === 'B') {
            return 1;
        } else if (a[field]  === 'L') {
            return -1;
        } else if (b[field]  === 'L') {
            return 1;
        } else if (a[field]  === 'C') {
            return -1;
        } else if (b[field]  === 'C') {
            return 1;
        } else if (a.key === '' || a.key === null || a.key === undefined) {
            return 1;
        } else if (b.key === '' || b.key === null || a.key === undefined) {
            return -1;
        } else if (a[field]  === 'A') {
            return 1;
        } else if (b[field]  === 'A') {
            return -1;
        } else {
            const aVal = parseFloat(a[field] );
            const bVal = parseFloat(b[field] );
            return aVal - bVal;
        }
    });

    return arr;
}
