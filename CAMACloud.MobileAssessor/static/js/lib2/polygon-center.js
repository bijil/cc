﻿function TinyQueue(t, e) {
    if (!(this instanceof TinyQueue)) return new TinyQueue(t, e);
    if (this.data = t || [], this.length = this.data.length, this.compare = e || defaultCompare, this.length > 0)
        for (var n = (this.length >> 1) - 1; n >= 0; n--) this._down(n)
}

function defaultCompare(t, e) {
    return t < e ? -1 : t > e ? 1 : 0
}

function centerMarker(t, e, n) {
    var i, r, a, h;
    e = e || 1;
    for (var o = 0; o < t[0].length; o++) {
        var s = t[0][o];
        (!o || s[0] < i) && (i = s[0]), (!o || s[1] < r) && (r = s[1]), (!o || s[0] > a) && (a = s[0]), (!o || s[1] > h) && (h = s[1])
    }
    var l = a - i,
        u = h - r,
        f = Math.min(l, u),
        d = f / 2,
        p = new TinyQueue(null, compareMax);
    if (0 === f) return [i, r];
    for (var g = i; g < a; g += f)
        for (var c = r; c < h; c += f) p.push(new Cell(g + d, c + d, d, t));
    var v = getCentroidCell(t),
        y = new Cell(i + l / 2, r + u / 2, 0, t);
    y.d > v.d && (v = y);
    for (var C = p.length; p.length;) {
        var m = p.pop();
        m.d > v.d && (v = m, n && console.log("found best %d after %d probes", Math.round(1e4 * m.d) / 1e4, C)), m.max - v.d <= e || (d = m.h / 2, p.push(new Cell(m.x - d, m.y - d, d, t)), p.push(new Cell(m.x + d, m.y - d, d, t)), p.push(new Cell(m.x - d, m.y + d, d, t)), p.push(new Cell(m.x + d, m.y + d, d, t)), C += 4)
    }
    return n && (console.log("num probes: " + C), console.log("best distance: " + v.d)), [v.x, v.y]
}

function compareMax(t, e) {
    return e.max - t.max
}

function Cell(t, e, n, i) {
    this.x = t, this.y = e, this.h = n, this.d = pointToPolygonDist(t, e, i), this.max = this.d + this.h * Math.SQRT2
}

function pointToPolygonDist(t, e, n) {
    for (var i = !1, r = 1 / 0, a = 0; a < n.length; a++)
        for (var h = n[a], o = 0, s = h.length, l = s - 1; o < s; l = o++) {
            var u = h[o],
                f = h[l];
            u[1] > e != f[1] > e && t < (f[0] - u[0]) * (e - u[1]) / (f[1] - u[1]) + u[0] && (i = !i), r = Math.min(r, getSegDistSq(t, e, u, f))
        }
    return (i ? 1 : -1) * Math.sqrt(r)
}

function getCentroidCell(t) {
    for (var e = 0, n = 0, i = 0, r = t[0], a = 0, h = r.length, o = h - 1; a < h; o = a++) {
        var s = r[a],
            l = r[o],
            u = s[0] * l[1] - l[0] * s[1];
        n += (s[0] + l[0]) * u, i += (s[1] + l[1]) * u, e += 3 * u
    }
    return 0 === e ? new Cell(r[0][0], r[0][1], 0, t) : new Cell(n / e, i / e, 0, t)
}

function getSegDistSq(t, e, n, i) {
    var r = n[0],
        a = n[1],
        h = i[0] - r,
        o = i[1] - a;
    if (0 !== h || 0 !== o) {
        var s = ((t - r) * h + (e - a) * o) / (h * h + o * o);
        s > 1 ? (r = i[0], a = i[1]) : s > 0 && (r += h * s, a += o * s)
    }
    return (h = t - r) * h + (o = e - a) * o
}
TinyQueue.prototype = {
    push: function(t) {
        this.data.push(t), this.length++, this._up(this.length - 1)
    },
    pop: function() {
        if (0 !== this.length) {
            var t = this.data[0];
            return this.length--, this.length > 0 && (this.data[0] = this.data[this.length], this._down(0)), this.data.pop(), t
        }
    },
    peek: function() {
        return this.data[0]
    },
    _up: function(t) {
        for (var e = this.data, n = this.compare, i = e[t]; t > 0;) {
            var r = t - 1 >> 1,
                a = e[r];
            if (n(i, a) >= 0) break;
            e[t] = a, t = r
        }
        e[t] = i
    },
    _down: function(t) {
        for (var e = this.data, n = this.compare, i = this.length >> 1, r = e[t]; t < i;) {
            var a = 1 + (t << 1),
                h = a + 1,
                o = e[a];
            if (h < this.length && n(e[h], o) < 0 && (a = h, o = e[h]), n(o, r) >= 0) break;
            e[t] = o, t = a
        }
        e[t] = r
    }
};