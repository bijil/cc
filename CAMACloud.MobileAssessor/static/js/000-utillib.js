﻿//    DATA CLOUD SOLUTIONS, LLC ("COMPANY") CONFIDENTIAL 
//    Unpublished Copyright (c) 2010-2013 
//    DATA CLOUD SOLUTIONS, an Ohio Limited Liability Company, All Rights Reserved.

//    NOTICE: All information contained herein is, and remains the property of COMPANY.
//    The intellectual and technical concepts contained herein are proprietary to COMPANY
//    and may be covered by U.S. and Foreign Patents, patents in process, and are protected
//    by trade secret or copyright law. Dissemination of this information or reproduction
//    of this material is strictly forbidden unless prior written permission is obtained
//    from COMPANY. Access to the source code contained herein is hereby forbidden to
//    anyone except current COMPANY employees, managers or contractors who have executed
//    Confidentiality and Non-disclosure agreements explicitly covering such access.</p>

//    The copyright notice above does not evidence any actual or intended publication
//    or disclosure of this source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
//    DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
//    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
//    IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
//    OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
//    TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
//    ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.


String.prototype.trim = function () { return this.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); };


var lastTicks = null;
var lastccTicks = null;
function ticks() {
    var t = (new Date()).getTime();
    while (t == lastTicks) {
        t = (new Date()).getTime();
    }
    lastTicks = t;
    return t;
}

function tick() {
    return ticks();
}

Date.prototype.tickSeconds = function () {
    return parseInt(this.getTime() / 1000);
}

Date.prototype.ticks = function () {
    return parseInt(this.getTime());
}


var startts = ticks();

function getTs() {
    var cts = (ticks() - startts).padLeft(10);
    return cts;
}

function __(o) {
    console.log(o);
}

function error(e) {
    console.warn(e);
}

Number.prototype.padLeft = function (width, ch) {
    if (!ch) {
        ch = " ";
    }

    if (("" + this).length >= width) {
        return "" + this;
    }
    else {
        return arguments.callee.call(
      ch + this,
      width,
      ch
    );
    }
};

String.prototype.padLeft = function (width, ch) {
    if (!ch) {
        ch = " ";
    }

    if (("" + this).length >= width) {
        return "" + this;
    }
    else {
        return arguments.callee.call(
      ch + this,
      width,
      ch
    );
    }
};

String.prototype.padRight = function (width, ch) {
    if (!ch) {
        ch = " ";
    }

    if (("" + this).length >= width) {
        return "" + this;
    }
    else {
        return arguments.callee.call(
      this + ch,
      width,
      ch
    );
    }
};

String.prototype.listItem = function () {
    return "<li>" + this + "</li>"
};

String.prototype.ul = function () {
    return "<ul>" + this + "</ul>"
};

String.prototype.ol = function () {
    return "<ol>" + this + "</ol>"
};
String.prototype.startsWith = function (str)
{ return (this.match("^" + str) == str) }

function log(e, error) {
    if (!localDev) {
        return false;
    }

    if (e === undefined) { return false; }
    //if (!iPad) {return false;}
    var now = new Date();
    var msg;
    if (e) {
        e = e.toString();
    } else { e = "{null}"; }
    if (e.substr) {
        if (e.substr(0, 3) != "***") {
            msg = now.toLocaleTimeString() + " - " + getTs() + " - " + e;
        } else {
            msg = e.substr(3);
        }
    } else {
        msg = e.toString();
    }

    if (quickDebug) {
        if (error)
            console.error(msg)
        else
            console.log(msg)
    }

    if (iPad)
        remoteLog(msg);

}

function remoteLog(msg) {
    //return false;
    var data = {
        DebugMessage: msg
    };
    $.ajax({
        url: debugHostUrl + '?zt=' + ticks(),
        type: "POST",
        dataType: 'json',
        data: data,
        success: function (data, status) {

        },
        error: function (resp, stat, errorMessage) {

        }
    });
}

function xlog(e, error) {
    var now = new Date();

    var msg = now.toLocaleTimeString() + " - " + getTs() + " - " + e;
    if (error)
        console.error(msg)
    else
        console.log(msg)
}

function logObj(o) {
    log('***Oject Dictionary - ' + o.toString());
    for (x in o) { log("***" + x + ': ' + o[x]); }
}

function loga(arr) {
    for (x in arr) {
        e = arr[x];
        var now = new Date();
        var msg = now.toLocaleTimeString() + " - " + getTs() + " - " + e;
        if (quickDebug) {
            console.log(msg)
        }
    }
}

function evalLookup(group, id, getDescription, field, getNumericValue, options) {
    var fieldId = field ? field.Id : null;
	var Id = id;

    if (getFieldLookup(group, fieldId) == null) {
        console.warn('Missing lookup table - ' + group);
        return "???";
    }
    if ((id == undefined) || (id == null) || (id == "" && id !== 0)) {
        return "";
    }
    id = Id.toString();
    var item = getFieldLookup(group, fieldId, options)[id];
    if (item === undefined){
        id = Id.toString().trim();
        var item = getFieldLookup(group, fieldId, options)[id];
    }
    if (item == null) {
        //        if (field == null)
        //            return "";
        //        if (field.LookupTable && field.LookupTable != '$QUERY$') {
        //            item = lookup[(field.LookupTable)][id]
        //            if (item == null)
        //                return "";
        //            else {
        //                if (getDescription)
        //                    return item.Description;
        //                return item.Name
        //            }
        //        }
        //  console.warn('Missing lookup value "' + id + '" in table "' + group + '"');
        if (fieldId != null && datafields[fieldId].ReadOnly)
            return id;
        else
            return "";
    }
    if (getDescription === 'SHORTDESC')
        return item.Name;
    else if (getDescription === 'LONGDESC')
        return item.Description;
    else if (getDescription === 'SHORTDESC, LONGDESC')
        return item.Name + ' - ' + item.Description;
    else if (getDescription) 
        return item.Description;

    if (getNumericValue)
        return item.NumericValue;
    return (item.Name  ||  item.Id)
}

function evalYesNo(value) {
    return value == 'true' || value == '1' || value == 1 ? 'Yes' : value == 'false' || value == '0' || value == 0 ? 'No' : value;
}

function Point(lat, lng) {
    this.Latitude = lat;
    this.Longitude = lng;

    this.ToLatLng = function () {
        if (google)
            return new google.maps.LatLng(this.Latitude, this.Longitude);
        else
            return this;
    }

    this.lat = function () {
        return this.Latitude;
    }

    this.lng = function () {
        return this.Longitude;
    }

}
function GISPoint(lat, lng) {
    this.Latitude = lat;
    this.Longitude = lng;
    this.lat = function () {
        return this.Latitude;
    }
    this.lng = function () {
        return this.Longitude;
    }
    if (google)
        return new google.maps.LatLng(this.Latitude, this.Longitude);
}
function screenCoords(e) {
    try {
        var touchPoints;
        var touches = 1;
        var ax, ay;

        if (e.changedTouches != undefined) {
            touchPoints = e.changedTouches
            touches = touchPoints.length;
            e = touchPoints[0];
            if (touches > 1) {
                var sx = 0, sy = 0;
                for (i = 0; i < touches; i++) {
                    sx = sx + touchPoints[i].clientX;
                    sy += touchPoints[i].clientY;
                }
                ax = Math.floor(sx / touches);
                ay = Math.floor(sy / touches);

            }
        }
        var ex, ey;
        var r = 1;
        ex = Math.round((ax ? ax : e.clientX) / r) * r;
        ey = Math.round(((ay ? ay : e.clientY)) / r) * r;
        var p = new Object();
        p.x = ex;
        p.y = ey;
        p.touches = touches;

        return p;
    }
    catch (ex) {
        messageBox(ex);
    }
}


function updatedDataObject(o, field, value) {
    var copy = new Object();
    for (x in o) {
        copy[x] = o[x];
    }
    copy[field] = value;
    return copy;
}

function copyDataRows(a) {
    var copy = [];
    for (x in a) {
        copy.push(copyDataObject(a[x]));
    }
    return copy;
}

function copyDataObject(o) {
    var copy = new Object();
    for (x in o) {
        copy[x] = o[x];
    }
    return copy;
}

function valueOfMultipleSelection(sm) {
    var value = "";
    for (x = 0; x < sm.selectedOptions.length; x++) {
        var op = sm.selectedOptions[x];
        var val = op.attributes['value'];
        if (val) {
            if (value != "")
                value += ",";
            value += val.value;
        }
    }
    return value;
}

var sm1, value1;

function setValueOfMultiSelection(sm, value) {
    if (value)
        if (value != "") {
            sm1 = sm;
            value1 = value;
        }
    var array = [];
    if (value)
        array = value.split(",");
    array = array.map(function (x) { return (x ? x.trim() : x) });
    for (x = 0; x < sm.options.length; x++) {
        var op = sm.options[x];
        var val = op.attributes['value'];
        if (val) {
            if (array.indexOf(val.value) > -1) {
                op.selected = true;
            } else {
                op.selected = false;
            }
        }

    }
}

function normallizeMultiSelectionString(str) { return (str ? str.split(",") : []).map(function (x) { return x.trim() }).join(",") }

function ccTicks() {
	var currentYr = new Date().getFullYear(), nextYr =  Math.ceil(currentYr/ 10) * 10;
	nextYr = (((nextYr - currentYr) < 3) ? (nextYr + 10): nextYr);
    var d1 = new Date(nextYr, 12, 31);
    var d2 = (new Date()).getTime();
    while (d2 == lastccTicks) {
        d2 = (new Date()).getTime();
    }
    lastccTicks = d2;
    var diff = d2 - d1.getTime();
    return diff;
}

function decodeHTML(encoded) {
    var div = document.createElement('div');
    div.innerHTML = encoded;
    var decoded = div.firstChild.nodeValue;
    return decoded;
}

function resetCookies() {
    var cookies = document.cookie.split(";");

    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        if (name != "MACID")
            document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }


    ajaxRequest({
        url: baseUrl + 'maauth/reset.jrq?zt=' + ticks(),
        dataType: 'json',
        type: 'POST',
        data: {},
        success: function (data) {
            if (data.status) {
                if (data.status == "OK") {
                    messageBox("Authentication cache cleared.");
                }
                else {
                    messageBox(data.message);
                }
            }
        },
        error: function (req, e, s) {
            messageBox(e);
        }
    })
    return false;
}

function resetAllCookies() {
    var cookies = document.cookie.split(";");

    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }


    ajaxRequest({
        url: baseUrl + 'maauth/resetdevice.jrq?zt=' + ticks(),
        dataType: 'json',
        type: 'POST',
        data: {},
        success: function (data) {
            if (data.status) {
                if (data.status == "OK") {
                    window.location.reload();
                }
                else {
                    messageBox(data.message);
                }
            }
        },
        error: function (req, e, s) {
            messageBox(e);
        }
    })
    return false;
}
