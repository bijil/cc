﻿//    DATA CLOUD SOLUTIONS, LLC ("COMPANY") CONFIDENTIAL 
//    Unpublished Copyright (c) 2010-2013 
//    DATA CLOUD SOLUTIONS, an Ohio Limited Liability Company, All Rights Reserved.

//    NOTICE: All information contained herein is, and remains the property of COMPANY.
//    The intellectual and technical concepts contained herein are proprietary to COMPANY
//    and may be covered by U.S. and Foreign Patents, patents in process, and are protected
//    by trade secret or copyright law. Dissemination of this information or reproduction
//    of this material is strictly forbidden unless prior written permission is obtained
//    from COMPANY. Access to the source code contained herein is hereby forbidden to
//    anyone except current COMPANY employees, managers or contractors who have executed
//    Confidentiality and Non-disclosure agreements explicitly covering such access.</p>

//    The copyright notice above does not evidence any actual or intended publication
//    or disclosure of this source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
//    DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
//    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
//    IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
//    OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
//    TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
//    ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.


//ROUTING

var googledirections;
if (google)
try { googledirections = new google.maps.DirectionsService(); } catch (e) { }

function ComputePriorityAndAlertRoutes(callback) {
    log('Getting list of alerts');
    geo.getCurrentPosition(function (loc) {
        let sqry = EnableNewPriorities == 'True' ? "UPDATE Parcel SET DistanceIndex = 9999 WHERE CC_Priority IN (1,2,3,4,5)" : "UPDATE Parcel SET DistanceIndex = 9999 WHERE CC_Priority IN (1,2)"
        getData(sqry, [], function () {
            getData("SELECT Id,Number FROM Neighborhood", [], function (nbhd) {
                loadPriorityParcelForRouting(nbhd, loc, function () {
                    if (callback) callback();
                });
            });
        });

    });
}

function loadPriorityParcelForRouting(nbhds, loc, callback) {
    var splitNeighborhood = "";
    nbhds.forEach(function(x){ splitNeighborhood += x.Id +","  });
    splitNeighborhood = splitNeighborhood.slice(0,splitNeighborhood.length-1);
    var alertSql = EnableNewPriorities == 'True' ? "SELECT * FROM PendingParcels WHERE CC_Priority IN (3,4,5) AND NbhdId IN (" + splitNeighborhood + ")" : "SELECT * FROM PendingParcels WHERE CC_Priority IN (2) AND NbhdId IN (" + splitNeighborhood + ")";
    getData(alertSql, [], function (alerts) {
        routing.calculateRoute("mapquest", alerts, loc || myLocation, 0, null, function (np, fp, l) {
            log('Getting list of priorities');
            var startLoc = loc || myLocation;
            if (fp)
                startLoc = fp.Location;
            if ((l == undefined) || (l == null) || isNaN(l)) {
                l = 0;
            }
            var prioritySql = EnableNewPriorities == 'True' ? "SELECT * FROM PendingParcels WHERE CC_Priority IN (1,2) AND NbhdId IN (" + splitNeighborhood + ")" : "SELECT * FROM PendingParcels WHERE CC_Priority IN (1) AND NbhdId IN (" + splitNeighborhood + ")";
            getData(prioritySql, [], function (priorities) {
                routing.calculateRoute("mapquest", priorities, startLoc, l, null, function (np1, fp1, l1) {
                   if (callback) callback();
                });
            });
        });
    });
}

function ComputeProximityRoutes(callback) {
    log('Getting list of proximity');
    geo.getCurrentPosition(function (loc) {
        getData("UPDATE Parcel SET DistanceIndex = 9999 WHERE CC_Priority IN (0)", [], function () {
            getData("SELECT Id,Number FROM Neighborhood", [], function (nbhd) {
                loadProximityParcelForRouting(nbhd, loc, function () {
                    if (callback) callback();
                });
            });
        });

    });
}

function loadProximityParcelForRouting(nbhds, loc, callback) {
    var nbhd = nbhds.pop();
    getData("SELECT * FROM PendingParcels WHERE CC_Priority IN (0) AND NbhdId = ?", [nbhd.Id], function (parcels) {
        routing.calculateRoute("mapquest", parcels, loc || myLocation, 0, nbhd.Number.toString(), function (np1, fp1) {
            if (nbhds.length > 0) loadProximityParcelForRouting(nbhds, loc, callback);
            else if (callback) callback();
        });
    });
}

function routing() {

};


routing.calculateRoute = function (method, population, reference, startn, nbhd, callback) {
    if ( ( isTestingCounty || location.hostname === "localhost" || location.hostname.contains( "192.168" ) ) && ( clientSettings.forceRoutingRequest != '1' ) ){
        console.log( 'Map request is blocked as it is a testing Environment' )
        if ( callback ) callback(); return;
    }
    switch ( method ) {
        case "mapquest":
            routing.routeUsingMapQuest(population, reference, startn, nbhd, callback);
            break;
        case "google":
            routing.routeUsingGoogle(population, reference, startn, callback);
            break;
    }
}

routing.sortParcelsByDistance = function (parcels, reference) {
    if (!reference)
        reference = myLocation;

    var unsortedParcels = {};

    for (x in parcels) {
        var parcel = parcels[x];
        var pid = parcel.Id.toString();
        var loc = new google.maps.LatLng(parseFloat(parcel.Latitude), parseFloat(parcel.Longitude));
        if (unsortedParcels[pid] == null) {
            unsortedParcels[pid] = { ParcelId: pid, Distance: 0.0, Location: loc, SortIndex: 9999, CC_Priority: parcel.CC_Priority, StreetAddress: parcel.StreetAddress };
        }
    }

    var nearestParcel = null;
    var nearestDistance = 10000000000000;
    var farthestParcel = null;
    var farthestDistance = 0;

    for (x in unsortedParcels) {
        parcel = unsortedParcels[x];
        parcel.Distance = google.maps.geometry.spherical.computeDistanceBetween(reference, parcel.Location) * 0.9144;
        if (parcel.Distance < nearestDistance) {
            nearestDistance = parcel.Distance;
            nearestParcel = parcel;
        }
        if (parcel.Distance > farthestDistance) {
            farthestDistance = parcel.Distance;
            farthestParcel = parcel;
        }
        unsortedParcels[x] = parcel;
    }

    var unsortedList = [];
    for (x in unsortedParcels) {
        unsortedList.push(unsortedParcels[x]);
    }

    var sortedList = [];
    while (unsortedList.length > 0) {
        var min = 100000000000000;
        var np = [];
        var minindex = -1;
        for (var i = 0; i < unsortedList.length; i++) {
            var parcel = unsortedList[i];
            if (parcel.Distance < min) {
                min = parcel.Distance;
                minindex = i;
            }
        }
        np = unsortedList.splice(minindex, 1);
        sortedList.push(np[0]);
    }

    for (var i = 0; i < sortedList.length; i++) {
        sortedList[i].SortIndex = i;
    }

    return {
        sortedList: sortedList,
        nearestParcel: nearestParcel,
        farthestParcel: farthestParcel
    };
}

routing.routeUsingGoogle = function (population, reference, startn, callback) {
    log('Sorting via Google Sort');
    var o = sortParcelsByDistance(population);
    var sortedList = o.sortedList;
    var nearestParcel = o.nearestParcel;
    var farthestParcel = o.farthestParcel;
    var topN = Math.min(sortedList.length, 9);
    var topParcels = [];
    for (var i = 1; i < topN; i++) {
        var parcel = sortedList[i];
        if (parcel != farthestParcel) topParcels.push(parcel);
    }

    var waypoints = [];
    for (var i = 0; i < topParcels.length; i++) {
        var parcel = topParcels[i];
        waypoints.push({ location: parcel.Location });
    }

    var request = {
        origin: nearestParcel.Location,
        destination: farthestParcel.Location,
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.IMPERIAL,
        waypoints: waypoints,
        optimizeWaypoints: true,
        provideRouteAlternatives: false
    };

    log('Google DIRECTIONS/ROUTE request');
    googledirections.route(request, function (response, status) {
        if (status == "OK") {
            sortedList[0].SortIndex = 0;
            var wpo = response.routes[0].waypoint_order;

            for (var i = 0; i < wpo.length - 1; i++) {
                var parcel = sortedList[i + 1];
                parcel.SortIndex = wpo[i] + 1;
            }
            var totalSorted = sortedList.length;
            var count = 0;
            db.transaction(function (tx) {
                for (var i = 0; i < sortedList.length; i++) {
                    var updateSql = "UPDATE Parcel SET DistanceFromMe = ?, DistanceIndex = ? WHERE Id = ?";
                    var p = sortedList[i];
                    tx.executeSql(updateSql, [p.Distance, p.SortIndex, parseInt(p.ParcelId)], function (tx, res) {
                        count++;
                        if (count == totalSorted) {
                            appState.homeParcelSelectedIndex = 0;
                            appState.homeParentParcelSelectedIndex = 0;
                            log("Parcels sorted");
                            if (callback) callback();
                        }
                    }, function (tx, ex) {
                        log(ex.message, true);
                    });
                }
            });
        }
    }, function () {
        alert('Error');
    });
}


routing.routeUsingMapQuest = function (parcels, reference, startn, nbhd, callback) {
    log( 'Prepare route using MapQuest' );
    var o = routing.sortParcelsByDistance(parcels, reference);
    routing.runMapQuestQuery(o.sortedList, reference, o.farthestParcel, startn, nbhd, function () {
        if (callback) callback(o.nearestParcel, o.farthestParcel, o.sortedList.length);
    });
}

routing.runMapQuestQuery = function (longlist, reference, target, startindex, nbhd, callback) {
    if (clientSettings.UseNewRoutingMethod == "1" || clientSettings.UseNewRoutingMethod == "true" || (_.isEmpty(clientSettings) && NewRoutingMethod)) {
        if (longlist.length == 0) {
            if (callback) callback();
            return
        }
        routing.__routeUsingMapQuest_New(longlist, callback)
        return;
    }
    log('MapQuest query for subset ...');
    var sublist;
    var extend = false;
    var maxlength = 24;

    if (longlist.length == 0) {
        if (callback) callback();
    }

    if (longlist.length > maxlength) {
        sublist = longlist.splice(0, maxlength);
        extend = true;
    } else {
        sublist = longlist;
    }

    routing.__routeUsingMapQuest(sublist, reference, nbhd, function (sequence, points, resp) {
        if (!sequence) {
            if (callback) callback();
            return false;
        }

        console.log('__routeUsingMapQuest - Entry')

        var total = sublist.length;
        var count = 0;

        db.transaction(function (tx) {
            for (i = 0; i < sequence.length; i++) {
                var seq = sequence[i];
                var p = sublist[seq];
                var index = (startindex + i);
                console.log(startindex, index, p.StreetAddress);
                tx.executeSql("UPDATE Parcel SET DistanceFromMe = " + (isNaN(p.Distance) ? 9999 : p.Distance) + ", DistanceIndex = " + index + " WHERE Id = " + p.ParcelId, [], function (tx, res) {
                    count++;
                    if (count == total) {
                        if (extend) {
                            var nextReference = sublist[sublist.length - 1].Location;
                            for (var vi = sublist.length - 1; i >= 0; i--) {
                                nextReference = sublist[sublist.length - 1].Location;
                                if (isFinite(nextReference.lat()))
                                    break;
                            }
                            if (isNaN(nextReference.lat())) {
                                nextReference = reference;
                            }
                            var nextIndex = startindex + sublist.length;
                            routing.runMapQuestQuery(longlist, nextReference, target, nextIndex, nbhd, callback);
                        } else {
                            log('Route calculated successfully.');
                            if (callback) callback();
                        }
                    }
                }, function (tx, ex) {
                    log(ex.message, true);
                });
            }
        });
    });
}

routing.__routeUsingMapQuest = function (population, reference, nbhd, callback) {
    var locations = [];
    var lstring = "";
    var nullpoint = new google.maps.LatLng(myLocation.lat() + 0.25, myLocation.lng() + 0.25);
    console.log('Sending for routing ..');
    var parcelIds = [];
    for (var i = 0; i < population.length; i++) {
        if (lstring != "") {
            lstring += ","
        }
        var lline = '';
        p = population[i];
        var firstChar = p.StreetAddress ? p.StreetAddress.toString().trim().charAt(0) : null;
        if (p.StreetAddress && p.StreetAddress != '' && firstChar != '0' && !isNaN(firstChar)) {
            lline = clientSettings.IncludeCountyAddressInRouting == 'true' || clientSettings.IncludeCountyAddressInRouting == '1' ? '{street: \'' + p.StreetAddress + ',' + orgaddress + '\'}' : '{street: \'' + p.StreetAddress + '\'}';
        } else
            if (p.Location && !(isNaN(p.Location.lat()))) {
                var ll = isNaN(p.Location.lat()) ? nullpoint.lat() : (Math.round(p.Location.lat() * 1E6) / 1E6);
                var lg = isNaN(p.Location.lng()) ? nullpoint.lng() : (Math.round(p.Location.lng() * 1E6) / 1E6);
                lline = '{latLng:{lat:' + ll + ',lng:' + lg + '}}';
            } else {
                lline = clientSettings.IncludeCountyAddressInRouting == 'true' || clientSettings.IncludeCountyAddressInRouting == '1' ? '{street: \'' + p.StreetAddress + ',' + orgaddress + '\'}' : '{street: \'' + p.StreetAddress + '\'}';
            }

        lstring += lline;
        parcelIds.push(p.Id)
        log(lline);
    }

    if (population.length <= 3) {
        var seq = [];
        if (population.length == 1) seq = [0];
        if (population.length == 2) seq = [0, 1];
        if (population.length == 3) seq = [0, 1, 2];
        if (callback) callback(seq, [], null);
        if (!navigator.onLine) 
 			messageBox("The Geo Location sorting options require cell service as they re-route the properties in the Assignment Group. Please try again when the iPad is connected to the internet")
        return;
    }
    var routeMethod = 'SHORTEST';
    var routeSelect = enableMultipleNbhd ? '.select-route-mode' : '.SingleNbhd-route-mode'
    if($(routeSelect).val() == "1") 
    	routeMethod = 'PEDESTRIAN'
    lstring = "{options:{routeType:'"+routeMethod+"'}, mapState:{height:3000, width:4000, scale:1000  }, locations:[" + lstring + "]}";
    var url = '/mobileassessor/route.jrq?zt=' + ticks();
    var data = {
        key: unescape('Fmjtd%7Cluua2168n5%2C8s%3Do5-h4bsl'),
        json: lstring,
        nbhd: nbhd
    }
    $.ajax({
        url: url,
        data: data,
        dataType: 'json',
        type: 'POST',
        success: function (resp) {
            console.log(resp);

            if (!resp.info) {
                console.log(resp);
                messageBox("Routing request failed! Sorting parcels in default or last sorted order. Please use Search interface for better help, and try sorting again later.");
                if (callback) callback();
                return false;
            }
            var msgs = resp.info.messages;
            if (msgs.length > 0) {
                for (x in msgs) {
                    log(msgs[x]);
                }
                if (callback) callback();
            }
            else {
                var seq = resp.route.locationSequence;

                var locs = resp.route.locations;
                var pind = 0;
                if (callback) callback(seq, locs, resp.route);
            }
        },
        error: function (e) {
        	console.error(e)
        	if (!navigator.onLine)
 			messageBox("The Geo Location sorting options require cell service as they re-route the properties in the Assignment Group. Please try again when the iPad is connected to the internet")
        	else
            messageBox("Routing request failed! Sorting parcels in default or last sorted order. Please use Search interface for better help, and try sorting again later.");
            if (callback) callback();
            __('Error');
        }
    });
}

routing.__routeUsingMapQuest_New = function (parcels, callback) {
	var routeMethod = 0;
	var routeSelect = enableMultipleNbhd ? '.select-route-mode' : '.SingleNbhd-route-mode'
      if($(routeSelect).val() == "1") 
		routeMethod = 1;
    var url = '/mobileassessor/route_new.jrq?zt=' + ticks();
    var mapPointParcels = parcels.filter(function(x){ return (!(isNaN(x.Distance))) }); //filter gispoint parcels or parceladdress parcels 
    if (mapPointParcels.length > 0) {
	    var data = {
	        parcels: mapPointParcels.map(function (a) { return a.ParcelId }).join(','),
	        currentLocation_Latitude: myLocation.lat(),
	        currentLocation_Longitude: myLocation.lng(),
	        routeMethod : routeMethod
	    }
	    $.ajax({
	        url: url,
	        data: data,
	        dataType: 'json',
	        type: 'POST',
	        success: function (resp) {
	            console.log(resp);
	            if (resp.status != 'True') {
	                console.log(resp);
	                messageBox("Routing request failed! Sorting parcels in default or last sorted order. Please use Search interface for better help, and try sorting again later.");
	                if (callback) callback();
	                return false;
	            }
	            else {
	            	var nanDistanceParcels = parcels.filter(function(x){ return (isNaN(x.Distance)) }); //insert non gispoint parcel and non address parcel to results array.
	            	if (nanDistanceParcels.length > 0) {
	            		var maxIndex = ( ( resp.Sequence.length > 0 )? ( Math.max.apply(Math, resp.Sequence.map(function(o){ return o.Index; })) ): 0 );
	            		for (var np in nanDistanceParcels) {
	            			maxIndex = maxIndex + 1;
	            			resp.Sequence.push({ ParcelId: nanDistanceParcels[np].ParcelId, Index: maxIndex });
	            		}
	            	}
	            	
	                db.transaction(function (tx) {
	                    var count = 0;
	                    for (i = 0; i < resp.Sequence.length; i++) {
	                        var p = resp.Sequence[i]
	                        tx.executeSql("UPDATE Parcel SET DistanceFromMe = " + (isNaN(p.Distance) ? 9999 : p.Distance) + ", DistanceIndex = " + p.Index + " WHERE Id = " + p.ParcelId, [], function (tx, res) {
	                            count++;
	                            if (count == resp.Sequence.length)
	                                if (callback) callback();
	                        })
	                    }
	                })
	            }
	        },
	        error: function () {
	            messageBox("Routing request failed! Sorting parcels in default or last sorted order. Please use Search interface for better help, and try sorting again later.");
	            if (callback) callback();
	            __('Error');
	        }
	    });
    }
    else {
        db.transaction(function (tx) {
            var count = 0;
            for (i = 0; i < parcels.length; i++) {
                var p = parcels[i];
                tx.executeSql("UPDATE Parcel SET DistanceFromMe = " + (isNaN(p.Distance) ? 9999 : p.Distance) + ", DistanceIndex = " + p.SortIndex + " WHERE Id = " + p.ParcelId, [], function (tx, res) {
                    count++;
                    if (count == parcels.length)
                        if (callback) callback();
                })
            }
        });
    }
}