﻿var debugLocal = (self.location.hostname == "localhost");
const shclients = [];
const executors = {};
debugLocal = false;
{
    function WSURL(domain, path, port) {
        path = path || ''; if (path[0] == '/') path = path.substr(1);
        return (debugLocal ? 'ws://' : 'wss://') + (debugLocal ? ('localhost:' + (port || 80)) : domain) + '/' + path;
    }

    function WebSocketDriver(name, url, options) {
        options = options || {};
        var _queue = [];
        var _t = this;
        var window = 'window' in self ? self[window] : null;
        var _name = name;

        var ss, lt, ac = true;
        function addItemToQueue(msg) { _queue.push({ message: msg }); }
        function pushItemsInQueue() {
            if (ss.readyState == WebSocket.CLOSED) { _t.connect(pushItemsInQueue); return false; }
            if (ss.readyState != WebSocket.OPEN) return false;
            while (_queue.length > 0) { ss.send(JSON.stringify(_queue.shift().message)); }
            return true;
        }
        function keepAlive(ka) {
            lt && clearTimeout(lt);
            if (!ss || ss.readyState != WebSocket.OPEN) { lt = setTimeout(function () { keepAlive(true); }, 2000); return false; }
            if (ka) { lt = setTimeout(function () { pushItemsInQueue() && ss.send('\x07'); keepAlive(true); }, 42 * 1000) }
        }

        this.connect = function (callback) {
            _t.beforeConnect && _t.beforeConnect();
            ss = new WebSocket(url);
            ss.onopen = function () { keepAlive(true); _t.onopen && _t.onopen.apply && _t.onopen.apply(_t); callback && callback(); };
            ss.onmessage = function (m) { var d = JSON.parse(m.data); d.sender = _name; _t.onmessage && _t.onmessage.apply && _t.onmessage.apply(_t, [d]); keepAlive(true); };
            ss.onerror = function (e, f, g) { console.error(e, f, g); };
            ss.onclose = function () { lt = setTimeout(function () { ac && _t.connect(); }, 1000); }
            if (window) { window.onclose = function () { keepAlive(false); ac = false; ss.close(); } }
        }

        var _send = function (message) {
            addItemToQueue(message);
            pushItemsInQueue();
        }

        this.send = function (message) {
            if (_t.beforeSend) message = _t.beforeSend.apply(_t, [message])
            message && _send(message);
        }

        this.onopen = options.onopen;
        this.onmessage = options.onmessage;
        this.beforeConnect = options.beforeConnect;
        this.beforeSend = options.beforeSend || function (m) { return m };

        Object.defineProperties(this, {
            name: { value: _name }
        })

        if (!WebSocketDriver.drivers) WebSocketDriver.drivers = {};
        WebSocketDriver.drivers[_name] = this;
    }
}

var handleMessageFromBrowser = function (m) {
    var d = m.data;
    if (executors[d.sender]) {
        executors[d.sender](d);
    } else {
        if (!m.sender) console.error('Message requires valid sender property.');
        else console.error('Invalid sender tag on message.');
    }
}

function sendMessageToBrowsers(m) {
    if ('registration' in self) {
        clients.matchAll().then(function (clients) { clients.forEach(function (c) { c.postMessage(m) }) })
    } else if (shclients.length > 0) {
        shclients.forEach(function (c) { c.postMessage(m) })
    } else {
        self.postMessage(m)
    }
}

var onMessageReceived = function (m) {
    var notify = function () { };
    if ('registration' in self) { notify = function (a, b) { self.registration.showNotification(a, b); }; }
}




//Worker Installation
{
    // Install Service Worker
    self.addEventListener('install', function (event) {
        event.waitUntil(self.skipWaiting());
    });

    // Service Worker Active
    self.addEventListener('activate', function (event) {
        event.waitUntil(self.clients.claim());
    });

    //Message Event Handler for "ServiceWorker"
    self.addEventListener('message', handleMessageFromBrowser);

    // Connect Event Handler for "SharedWorker"
    self.addEventListener('connect', function (c) {
        var port = c.ports[0];
        port.onmessage = handleMessageFromBrowser;
        shclients.push(port);
    });

    self.addEventListener('error', function (e, f, g) {
        console.log('Error', e, f, g)
    });
}

var _debugger_key = '';

var _rwi = new WebSocketDriver('rwi', WSURL('rdb.camacloud.com', 'inspector/', 1020), {
    onmessage: function (m) {
        sendMessageToBrowsers(m);
    },
    onopen: function () { if (_debugger_key) this.send('auth ' + _debugger_key); },
    beforeSend: function (message) {
        if (message.cmd == 'setkey') {
            if (message.value) {
                _debugger_key = message.value;
                return ('auth ' + _debugger_key);
            }
        } else {
            return (message);
        }
    }
});

executors['rwi'] = function (m) {
    _rwi.send(m)
}

_rwi.connect();