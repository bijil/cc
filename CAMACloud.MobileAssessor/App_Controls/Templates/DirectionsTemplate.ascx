﻿<%@ Control Language="VB" ClassName="DirectionsTemplate" %>
<div id="directions-template" style="display: none;">
    <div context="routes">
        <div class="summary">
            Route Summary: ${summary}</div>
        <div context="legs">
            <table cellspacing="0" class="dist-dur">
                <tr>
                    <td style="width: 140px;">
                        Distance: <span context="distance">${text}</span>
                    </td>
                    <td style="width: 140px;">
                        Duration: <span context="duration">${text}</span>
                    </td>
                </tr>
            </table>
            <table class="fw b" cellspacing="0" style="margin-top: 10px;">
                <tr>
                    <td>
                        Starting: ${start_address}
                    </td>
                </tr>
                <tr>
                    <td>
                        Destination: ${end_address}
                    </td>
                </tr>
            </table>
            <table class="fw dir-steps" cellspacing="0">
                <tr context="steps">
                    <td class="num">
                        #{}.
                    </td>
                    <td class="inst">
                        ${instructions}
                    </td>
                    <td context="distance">
                        ${text}
                    </td>
                    <td context="duration">
                        ${text}
                    </td>
                </tr>
            </table>
        </div>
        <div style="margin-top: 10px; font-size: smaller; color: Gray;">
            ${copyrights}</div>
    </div>
</div>
