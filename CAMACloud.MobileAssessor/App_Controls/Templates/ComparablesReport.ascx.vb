﻿
Partial Class App_Controls_ComparablesReport
	Inherits System.Web.UI.UserControl

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
			LoadComparables()
		End If
	End Sub

    Sub LoadComparables()
        Try
            gvComparables.DataSource = Database.Tenant.GetDataTable("SELECT * FROM cc_ComparableFields ORDER BY Ordinal")
            gvComparables.DataBind()
        Catch ex As Exception
            'Throw New Exception(HttpContext.Current.GetCAMASession().OrganizationId)
            ' Alert(Me.Page, ex.Message.ToString())
        End Try
    End Sub


	Public Function EvalSubjectField() As String
		Dim c As Object = Eval("DefaultFieldName")
		If c Is DBNull.Value Then
			Return "&nbsp;"
		ElseIf c.ToString.Trim = "-" OrElse c.ToString.Trim = "" Then
			Return "&nbsp;"
		Else
			Return "${" + c.ToString() + ":Eval}"
		End If

	End Function

	Public Function EvalComparableField(ord As Integer) As String
		Dim c As Object = Eval("NonDefaultCompField" & ord)
		If c IsNot DBNull.Value Then
			If c.ToString.Trim = "-" Then
				Return "&nbsp;"
			Else
				Return "${" + c.ToString() + ":Eval}"
			End If
		ElseIf Eval("DefaultFieldName") IsNot DBNull.Value Then
			Return "${" + Eval("DefaultFieldName") + ":CompData," & (ord - 1) & "}"
		Else
			Return "&nbsp;"
		End If
	End Function

End Class
