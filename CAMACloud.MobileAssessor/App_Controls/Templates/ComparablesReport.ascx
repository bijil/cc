﻿<%@ Control Language="VB" AutoEventWireup="false"
	Inherits="CAMACloud.MobileAssessor.App_Controls_ComparablesReport" Codebehind="ComparablesReport.ascx.vb" %>
<div id="comparables-template">
	<asp:GridView runat="server" ID="gvComparables" ShowHeader="true" AutoGenerateColumns="false"
		Width="100%">
		<HeaderStyle CssClass="comp-header" />
		<Columns>
			<asp:BoundField DataField="Label" HeaderText="Description" ItemStyle-Width="220px"
				ItemStyle-CssClass="comp-field-desc" />
			<asp:TemplateField HeaderText="Subject" ItemStyle-CssClass="comp-column">
				<ItemTemplate>
					<%# EvalSubjectField() %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Comp #1" ItemStyle-CssClass="comp-column">
				<ItemTemplate>
					<%# EvalComparableField(1) %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Comp #2" ItemStyle-CssClass="comp-column">
				<ItemTemplate>
					<%# EvalComparableField(2)%>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Comp #3" ItemStyle-CssClass="comp-column">
				<ItemTemplate>
					<%# EvalComparableField(3) %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Comp #4" ItemStyle-CssClass="comp-column">
				<ItemTemplate>
					<%# EvalComparableField(4) %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Comp #5" ItemStyle-CssClass="comp-column">
				<ItemTemplate>
					<%# EvalComparableField(5) %>
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</asp:GridView>
</div>
