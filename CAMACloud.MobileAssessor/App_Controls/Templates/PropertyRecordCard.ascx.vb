﻿
Partial Class App_Controls_PropertyRecordCard
	Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadContentTemplate("property-record-card", ContentTemplate)
            LoadContentTemplate("property-record-card-PersonalProperty", ContentTemplate2)
        End If
    End Sub

    Private Sub LoadContentTemplate(templateId As String, ph As PlaceHolder)
        Dim template As String = ClientSettings.ContentTemplate(templateId)

        If template <> "" Then
            template = Replace(template, " src=", " srcx=", ,, CompareMethod.Text)

            ph.Controls.Clear()
                ph.Controls.Add(New LiteralControl(template))
            End If
    End Sub
End Class
