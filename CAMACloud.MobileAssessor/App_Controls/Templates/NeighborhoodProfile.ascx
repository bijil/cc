﻿<%@ Control Language="VB" AutoEventWireup="false"
	Inherits="CAMACloud.MobileAssessor.App_Controls_NeighborhoodProfile" Codebehind="NeighborhoodProfile.ascx.vb" %>
<asp:TextBox runat="server" ID="txt1" Visible="false" />
<div id="nbhd-profile-template" style="display: none;">
    <asp:PlaceHolder runat="server" ID="ContentTemplate">
        <table class="profile-title fw">
		    <tr>
			    <td style="text-align: left; width: 33%;">Nbhd Profile: ${Number} </td>
			    <td style="text-align: center; width: 33%;"></td>
			    <td style="text-align: right; width: 33%;">Market Trend: --</td>
		    </tr>
	    </table>
	    <table class="fw bordered-table" border="1">
		    <tr class="b c">
			    <td></td>
			    <td>Age </td>
			    <td>Sq. Ft. </td>
			    <td>Class </td>
			    <td>Appraisal </td>
			    <td>$/SF</td>
			    <td>Selling Price </td>
			    <td>$/SF</td>
		    </tr>
		    <tr class="c">
			    <td class="b">Mode </td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		    </tr>
            <tr class="c">
			    <td class="b">Median </td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		    </tr>
	    </table>
    </asp:PlaceHolder>
	
</div>
