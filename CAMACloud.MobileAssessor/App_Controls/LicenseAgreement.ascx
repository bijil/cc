﻿<div class="text-content">
    <div style="height: 2px;">
    </div>
    <h2 style="text-align: center;">
        Terms of Use and Confidentiality Statement</h2>
   <div class="License-Agreement">
    <p>
        By logging in to this device, opening and using this software and clicking the “I
        Agree” button at the end of this statement (on logging in), I (the Recipient) hereby
        agree as follows:</p>
    <p>
        1. To hold Harris Local Government Solutions, Inc. and its’ Licensor, Data Cloud Solutions,
        LLC (the Disclosers) Disclosing Party’s Confidential Information in strict confidence
        and to take reasonable precautions to protect such Confidential Information (including,
        without limitation, all precautions I as the Recipient employ with respect to my
        own confidential information); and,</p>
    <p>
        For the purposes of this agreement, “Confidential Information” shall mean any confidential,
        non-public, proprietary or confidential information of Disclosers or their customers
        or licensors, including, without limitation, information concerning product plans,
        services, customers, markets, business, technology, marketing plans, employees and
        consultants, technical data, trade secrets, know-how, ideas, research, prototypes,
        samples, formulas, software, inventions (whether patentable or not), processes,
        designs, drawings, and schematics; and,</p>
    <p>
        2. I will not without the express prior written permission of Disclosers, divulge
        any such Confidential Information, to others except fellow employees with a need-to-know,
        provided such fellow employees are likewise bound to the obligations herein; and,</p>
    <p>
        3. Not to copy, decompile or attempt to reverse engineer any such Confidential Information.</p>
        </div>
</div>
