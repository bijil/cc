﻿<%@ Control Language="VB" ClassName="LoginPage" %>
<section id="login" class="screen" style="background: White;">
    <div class="login-box">
        <div class="title">
        </div>
        <div class="content">
            <table style="width: 100%">
                <tr>
                    <td>Username: </td>
                    <td style="text-align: right;">
                        <input type="text" class="login-username" autocorrect="off" autocapitalize="off"
                            pattern="[a-z_0-9]" />
                    </td>
                </tr>
                <tr>
                    <td>Password: </td>
                    <td style="text-align: right;">
                        <input type="password" class="login-password" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: left;padding-left: 145px;">
                        <button class="login-signin-button" onclick="showAgreement(true);" >Sign In</button>
                        <button class="disto_close hidden" onclick="CCWarehouse.MAClose()" >Close</button>
                        <button class="winapp_close hidden" onclick="windowsAppClose()" >Close</button>
                    </td>
                </tr>
            </table>
        </div>
        <div class="bottom">
        </div>
    </div>
    <div style="margin-top: 30px; text-align: center;">
        <a style="font-size: larger;" href="about:blank" onclick="return resetCookies();">Reset authentication
            cache</a></div>
</section>