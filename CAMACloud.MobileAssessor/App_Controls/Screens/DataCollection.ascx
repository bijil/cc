﻿<%@ Control Language="VB" ClassName="DataCollection" %>
<section class="screen" id="data-collection">
    <header><div>
   <table style="width:100%"><tr>
        <td><span>Parcel Data Collection</span></td> 
                      <td> <div class="parcel-header-temp hidden"></div> </td> 
                  <td> <a class="page-back"></a></td></tr>
        </table>
        </div>
    </header>
    <div class="record-photos" style="position: absolute;right: 80px;z-index: 9;top: -2px;">
    	<a class="record-photo-preview" style="height: 29px;float: right;margin-right: 5px;margin-top: 10px;display: none;"><span class="photocount" 
    	style="right: 1px;padding: 1px;">0</span></a>
    	<a class="record-add-photo" style="height: 29px;float: right;margin-right: 8px;margin-top: 10px;display: none;"><span class="photocount" 
    	style="right: 1px;padding: 1px;">0</span></a>
	</div>
    <div class="dc-wrapper boxed">
        <div id="dc-categories" class="scrollable">
            <div>
            </div>
        </div>
        <div id="dc-categories-condensed" class='dc-categories-condensed'>
            <a></a><span>Category Name</span>
        </div>
        <div id="dc-form" class="extend-panel">
            <div id="dc-form-contents" class="scrollable" style="position: relative; overflow-x: hidden;">
            </div>
        </div>
        <div id="dc-form-template" style="display: none;">
            <section name="quickreview">
                            <div class="vspace">
            </div>
           <%--<div class="cautionfield" style="padding: 0px 20px 3px 34px;">--%>
            <div class="cautionalert-qr hidden" style="margin-bottom: 8px;"></div>
          <%--  </div>--%>
            <% If ClientSettings.PropertyValue("EnableNewPriorities") = "1" Then %>
                <div style="padding: 0px 20px 3px 20px; font-size: 15px; font-weight: bold;">
                    Priority: <span class="dcc_priority"></span>
                </div>
            <%End If %>
            <fieldset style="padding: 0px 20px 3px 20px;">
                <legend>Alert from Office</legend>
                <div>
                    Type: <span class="officealerttype"></span>
                </div>
                <textarea style="width: 100%; font-size: 14pt; color: Red; margin-bottom: 8px;" rows="3"
                    class="officealert" disabled="disabled"></textarea>
            </fieldset>
            <fieldset style="padding: 0px 20px 3px 20px;">
                <legend style="padding-top:25px;">Alert from Field</legend>
                <div style="margin-top:-20px;">
                    <input type="text" class="dc-field-alert-flag">
<%--                    <select class="dc-field-alert-flag" pid="${Id}" style="width: auto; min-width: auto;">
                        <option value="0">No Flag</option>
                        <option value="1">New Construction</option>
                        <option value="2">Demolition</option>
                        <option value="3">Data Discrepancy</option>
                    </select></div>--%>


                <div class="vspace">
                </div>
                <textarea class="field-alert-message" style="width: 100%; font-size: 14pt;" rows="3"></textarea>
                <div class="vspace">
                </div>
                <div id="dc-estimate-chart" style="margin-bottom: 8px;">
                </div>
                <div style="text-align: right; padding: 8px;">
                	<a class="create-Bpp-parcel create-Bpp-parcel-photo" pid="${Id}" onclick="createNewParcel(activeParcel.IsParentParcel == 'true' ?  activeParcel.Id : activeParcel.ParentParcelID)" style="float: none;display:none;border: 0px;"></a> 
                    <a class="mark-as-delete delete-Bpp-parcel-photo" pid="${Id}" onclick=" markAsDelete(activeParcel.Id,true)" style="margin-top: 8px;margin-right: 10px;display: inline;padding: 3px 16px !important;border: 0px;border-radius: 0%;"></a>
                    <button style="padding: 6px 25px; font-size: 13pt;" class="dc-mark-complete">
                        Mark as Complete</button>
                </div>
            </fieldset>
            </section>
        </div>
    </div>
</section>