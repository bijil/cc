﻿<%@ Control Language="VB" ClassName="PhotoAlbum" %>
<section class="screen" id="photos">
    <header>
        <span>Parcel Photos</span> <a class="page-back"></a><a class="add-photo" onclick="photos.addNewFromParcel();"><input type="file" id="camera1" name="camera" style="width: 138px; float: left;height: 98%;font-size: 5px;opacity: 0"/>
        </a><a class="delete-selected-photo"  onclick="photos.deletemultiple();"></a>
    </header>
    <div class="scrollable">
        <div id="photo-catalogue-container">
            <div id="photo-catalogue">
            </div>
            <div style="clear: both">
            </div>
        </div>
    </div>
</section>