﻿<%@ Control Language="VB" ClassName="AppraiserDashboard" %>
<section class="screen hidden" id="user-dashboard">
    <header>
        <span>Appraiser Dashboard</span> <a class="page-back"></a>
    </header>
    <div style="text-align: center;" class="scrollable">
        <div style="text-align: left; width: 700px; margin-left: auto; margin-right: auto;padding-bottom:50px;">
            <div class="neighborhood-dashboard">
                <table>
                    <tr>
                        <td>
                            <%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName()%>:
                        </td>
                        <td style="text-align: right;">Logged in as:
                        </td>
                    </tr>
                    <tr style="font-size: 16pt; font-weight: bold;">
                        <td class="dashboard-nbhd"></td>
                        <td style="text-align: right;" class="dashboard-user"></td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td style="font-size: 12pt;">The <%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName()%>s Profile Stats seem reasonable
                        </td>
                        <td style="text-align: right;">
                            <button class="agree-nbhd" agree="true" style="padding: 6px; width: 60px; font-size: 12pt;">
                                YES</button>
                            <button class="agree-nbhd" agree="false" style="padding: 6px; width: 60px; font-size: 12pt;">
                                NO</button>
                        </td>
                    </tr>
                </table>
                <div class="vspace">
                </div>
                <fieldset style="padding: 0px; margin: 0px; border: 0px none; background: none;">
                    <legend style="font-weight: normal;">Comment on <%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName()%>:</legend>
                    <textarea style="width: 100%;" rows="4" class="nbhd-profile-comment" maxlength="480"></textarea>
                    <div style="text-align: right; margin: 6px 0px;">
                        <button style="font-size: 12pt; padding: 6px 30px;" class="nbhd-profile-comment-update">
                            Update</button>
                    </div>
                </fieldset>
            </div>
            <table style="border: 1px solid #999999; height: 30px; display: none;">
                <tr>
                    <td class="nbhd-perc-complete"></td>
                    <td class="nbhd-perc-alert"></td>
                    <td class="nbhd-perc-priority"></td>
                    <td class="nbhd-perc-proximity"></td>
                </tr>
            </table>
            <table class="dashboard-table">
                <thead>
                    <tr>
                        <th colspan="2">Appraiser Statistics : <span class="dashboard-user"></span>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <% If ClientSettings.PropertyValue("EnableNewPriorities") = "1" Then %>
                        <tr>
                            <td colspan="2">
                                <table class="productivity-table">
                                    <thead>
                                        <tr>
                                            <th>Productivity: </th>
                                            <th>Total </th>
                                            <th>Completed </th>
                                            <th>Remaining </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="background-color: #FF0000;">Critical</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #FFC000;">Urgent</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color:  #FFFF00;">High</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #00B0F0;">Medium</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: #8944FA;">Normal</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    <%Else %>
                        <tr>
                            <td>Total Urgent Parcels:
                            </td>
                            <td class="nbhd-alert-parcels">0
                            </td>
                        </tr>
                        <tr>
                            <td>Total High Priority Parcels:
                            </td>
                            <td class="nbhd-priority-parcels">0
                            </td>
                        </tr>
                        <tr>
                            <td>Urgent Parcels Remaining:
                            </td>
                            <td class="appr-alert-parcels-rem">0
                            </td>
                        </tr>
                        <tr>
                            <td>High Priority Parcels Remaining:
                            </td>
                            <td class="appr-priority-parcels-rem">0
                            </td>
                        </tr>
                    <%End If %>
                    <tr class="hidden">
                        <td>Other Proximity Parcels for Review:
                        </td>
                        <td class="appr-proximity-parcels">0
                        </td>
                    </tr>
                    <% If ClientSettings.PropertyValue("EnableNewPriorities") = "1" Then %>
                       <tr style="height: 50px;"><td style="border: none !important; font-weight: bold;">Data Sync Updates</td></tr>
                    <%End If %>
                    <tr>
                        <td>Parcels reviewed since last CAMA update:
                        </td>
                        <td class="appr-completed-parcels">0
                        </td>
                    </tr>
                    <tr>
                        <td>Last CAMA Update:
                        </td>
                        <td class="appr-last-cama-update"></td>
                    </tr>
                    <tr>
                        <td>Last Data Sync:
                        </td>
                        <td class="appr-last-data-sync"></td>
                    </tr>
                    <% If ClientSettings.PropertyValue("EnableNewPriorities") = "1" Then %>
                        <tr style="height: 50px;"><td style="border: none !important; font-weight: bold;">Pending Changes on Device</td></tr>
                    <%End If %>
                    <tr>
                        <td>Changes pending for sync:
                        </td>
                        <td class="appr-pending-sync"></td>
                    </tr>
                    <tr>
                        <td>Pending location updates:
                        </td>
                        <td class="appr-pending-loc"></td>
                    </tr>
                    <tr>
                        <td>Photos pending for upload:
                        </td>
                        <td class="appr-pending-photos"></td>
                    </tr>
                </tbody>
            </table>
            <p style="font-weight: bold; margin-top: 20px; font-size: larger;">
                <a style="float: right;" class="showSyncBtn" onclick="viewSyncStatus();return false;"
                    href="#">Show sync status</a>
            </p>
            <p style="margin-top: 5px; color: #808080; display: none;" class="sync-message">Sync status: <span class="sync-status-message">--</span></p>
        </div>
    </div>
</section>
