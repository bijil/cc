﻿<section class="screen" id="synchronization">
    <header>
        <span>Update MobileAssessor</span>
        <a class="page-back"></a>
    </header>
    <div class="scrollable">
        <menu>
            <cc:ScreenMenu runat="server" ID="smSyncAll" Action="RunSync(0);" CssClass="nav-sync nav-sync-all" Title="Update Everything" Info="Update program, and download CAMA data." />
            <cc:ScreenMenu runat="server" ID="smSyncApp" Action="RunSync(1);" CssClass="nav-sync nav-sync-app" Title="Update Application" Info="Download and install program updates." />
            <cc:ScreenMenu runat="server" ID="smSyncCAMA" Action="RunSync(2);" CssClass="nav-sync nav-sync-cama" Title="Update CAMA Data" Info="Download updated CAMA assignments and data." />
            <cc:ScreenMenu runat="server" ID="smReset" Action="clearAppState();window.location.reload();" CssClass="nav-sync" Title="Reset Application" Info="Reset and reload MobileAssessor" />
        </menu>
    </div>
</section>