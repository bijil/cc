﻿<%@ Control Language="VB" ClassName="ParcelSearchScreen" %>
<script runat="server">

</script>
<section class="screen" id="search">
 <header>
        <a class="page-back"></a>
    </header>
    <div class="nbhd-profile-float hidden">
    </div>
    <div class="parcel-search-panel">
        <table class="parcel-search-params">
            <tr>
                <td colspan="2" style="text-align: center; font-size: 2em; padding: 8px;">
                    Search Parcels
                </td>
            </tr>
            <tr>
                <td>
                    Search Type:
                </td>
                <td>
                    <input type="text" class="parcel-search-type pq-type" onchange="SetSearchType();">
<%--                    <select class="parcel-search-type pq-type" onchange="SetSearchType();">
                        <option value="allpriority">All priority parcels</option>
                        <option value="urgentpriority">Urgent Priority</option>
                        <option value="highpriority">High Priority</option>
                        <option value="proximity">All nearby parcels</option>
                        <option value="parcel">Search by Parcel ID</option>
                        <option value="street">Search by Street</option>
                        <option value="address">Search by Address</option>
                        <option value="alertfromoffice">Search by Alert From Office</option>
                        <option value="no-address">Parcels without GIS/Address</option>
                    </select>--%>
                </td>
            </tr>
            <tr class="search-optional option-parcel">
                <td>
                    Parcel IDs:
                </td>
                <td>
                    <textarea class="pq-pids" style="width:250px;resize:none;padding:2px;" rows="3" autocorrect="off" autocapitalize="off" autocomplete="off"></textarea>
                        <br />
                    <span style="font-size:10pt;color:Navy;">Use commas as separator if searching for multiple parcels.</span>
                </td>
            </tr>
             <tr class="search-optional option-field">
                <td>
                    Field's IDs:
                </td>
                <td>
                    <textarea class="pq-field" style="width:250px; resize:none;padding:2px;" rows="3" autocorrect="off" autocapitalize="off" autocomplete="off"></textarea>
                        <br />
                    <span style="font-size:10pt;color:Navy;">Use commas as separator if searching for multiple parcels.</span>
                </td>
            </tr>
            <tr class="search-optional option-street">
                <td>
                    Select Street:
                </td>
                <td>
                    <select class="pq-street select-street-names" style="width:243px;">
                        <option value="1">No streets nearby</option>
                    </select>
                </td>
            </tr>
            <tr class="search-optional option-street">
                <td>
                    Direction:
                </td>
                <td>
                    <select class="pq-direction" style="width:243px;" onchange="OnSelectSearchStreetDirection();">
                        <option value="0">Forward by Address#</option>
                        <option value="1">Reverse by Address#</option>
                        <option value="2">Forward by Parcel ID</option>
                        <option value="3">Reverse by Parcel ID</option>
                    </select>
                </td>
            </tr>
            <tr class="search-optional option-street">
                <td>
                    Address Selection:
                </td>
                <td>
                    <select class="pq-addrsel" style="width:243px;">
                        <option value="0">All Numbers</option>
                        <option value="1">Odd Numbers Only</option>
                        <option value="2">Even Numbers Only</option>
                    </select>
                </td>
            </tr>
            <tr class="search-optional option-address">
                <td>
                    Address Contains:
                </td>
                <td>
                    <input type="text" style="width: 250px" maxlength="20" autocorrect="off" autocapitalize="off"
                        autocomplete="off" class="pq-address" />
                </td>
            </tr>
             <tr class="search-optional option-alertfromoffice">
                <td>
                    Alert Contains:
                </td>
                <td>
                    <input type="text" style="width: 250px" maxlength="20" autocorrect="off" autocapitalize="off"
                        autocomplete="off" class="pq-alertfromoffice" />
                </td>
            </tr>
            <tr class="search-optional option-parcelpriority">
                <td colspan="2">
                    <input id="critical-parcelpriority" type="checkbox" class="search-priority-radio" style="margin-right: 5px;" name="search-priority__radio" value="5" />
                    <label style="margin-right: 5px;" for="critical-parcelpriority">Critical</label>
                    <input id="urgent-parcelpriority" type="checkbox" class="search-priority-radio" style="margin-right: 5px;" name="search-priority__radio" value="4" />
                    <label style="margin-right: 5px;" for="urgent-parcelpriority">Urgent</label>
                    <input id="high-parcelpriority" type="checkbox" class="search-priority-radio" style="margin-right: 5px;" name="search-priority__radio" value="3" />
                    <label style="margin-right: 5px;" for="high-parcelpriority">High</label>
                    <input id="medium-parcelpriority" type="checkbox" class="search-priority-radio" style="margin-right: 5px;" name="search-priority__radio" value="2" />
                    <label style="margin-right: 5px;" for="medium-parcelpriority">Medium</label>
                    <input id="normal-parcelpriority" type="checkbox" class="search-priority-radio" style="margin-right: 5px;" name="search-priority__radio" value="1" />
                    <label style="margin-right: 5px;" for="normal-parcelpriority">Normal</label>
                </td>
            </tr>
            <tr class="search-optional option-allpriority option-parcelpriority option-highpriority option-urgentpriority option-proximity">
                <td colspan="2">
                    <input type="checkbox" id="chkroute"  name="chkroute" class="pq-optiroute" />
                    <label for="chkroute">Sort parcels for optimized travel route (Refresh last sort)</label>
                </td>
            </tr>
            <tr class="search-optional no-option-street">
                <td colspan="2">
                    <input type="checkbox" id="chklocation" name="chklocation" class="pq-uselocation" />
                    <label for="chklocation">Search with respect to my current location.</label>
                </td>
            </tr>
            <tr class="search-optional option-street">
                <td colspan="2">
                    <input type="checkbox" id="chkprionly" name="chkprionly" class="pq-showprionly" />
                    <label for="chkprionly">Show priority parcels only.</label>
                </td>
            </tr>
            <tr class="search-optional option-no-address">
                <td colspan="2">
                    <input type="checkbox" id="chknogis" name="chknogis" class="pq-nogis" />
                    <label for="chknogis">Show parcels without GIS.</label>
                </td>
            </tr>
            <tr class="search-optional option-no-address">
                <td colspan="2">
                    <input type="checkbox" id="chknoaddress" name="chknoaddress" class="pq-noaddress" />
                    <label for="chknoaddress">Show parcels with missing or incomplete address.</label>
                </td>
            </tr>
            <tr class="search-optional option-allpriority option-parcelpriority option-highpriority option-urgentpriority option-proximity option-street option-no-address">
                <td colspan="2">
                    <input type="checkbox" id="chkreviewed" name="chkreviewed" class="pq-hidevisited" />
                    <label for="chkreviewed">Do not show completed parcels in list.</label>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align:center;">
                    <button onclick="PerformSearch();" class="btn-parcel-search">Search</button>
                </td>
            </tr>
        </table>
    </div>
    <%--<div id="search-panel">
        <table>
            <tr>
                <td class="lbl">
                    Parcel ID:
                </td>
                <td class="inp">
                    <input type="search" class="search-fld search-parcel" maxlength="8" style="width: 140px"
                        autocorrect="off" autocapitalize="off" />
                </td>
                <td class="lbl">
                    Street:
                </td>
                <td class="inp">
                    <input type="search" class="search-fld search-street" maxlength="18" style="width: 120px"
                        autocorrect="off" autocapitalize="off" />
                </td>
                <td>
                    <button onclick="SearchParcels();">
                        Search</button>
                    <button onclick="ClearSearch();">
                        Clear</button>
                </td>
            </tr>
        </table>
    </div>
    <header>
        <span>Search Results</span> <a class="page-back"></a><a class="hide-visited"></a>
    </header>
    <div id="search-results" class="scrollable">
        <div id="search-parcels">
        </div>
    </div>--%>
</section>
