﻿<%@ Control Language="VB" ClassName="SelectNeighborhood" %>
<section class="screen" id="neighborhood">

     
  
    <header>Download Parcels</header>
    <div class="scrollable">
        <div class="text-content">
            <div style="height: 2px;">
                <h1 style="margin-left: 0px;">
                    Select <%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName() %></h1>
                <p>
                    By update data, you are downloading the necessary parcel and supporting data to
                    your iPad for offline use.
                </p>
                <p id="nbhd-select-note">
                    You are allowed to work on any one of the assigned  <%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName() %>s. Please choose
                    the <%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName() %> to continue.</p>
                <p style="text-align: center; font-size: 15px;">
                     
                        <%--<input type = "text" Class="select-image-count input-select-image-count" style="display: none; text-align: left; font-size: 16pt; padding: 3px 13px; width: 350px; margin-bottom: 15px;">
                     
                      <option value="400">Default - All Images</option>
                        <option value = "600" > Main Image + 2 most recent photos</Option>
                        <option value = "800" > Main Image Only</Option>
                        <option value = "1200" > No Images / Max Parcels</Option>
                    </select>--%>
                    <select class="select-route-mode" style="display: none;text-align: center;font-size: 16pt;padding: 3px 13px;width: 184px;margin-bottom: 15px;margin-left: 10px;">
                        <option value="0">Driving Mode</option>
                        <option value="1">Walking Mode</option>
                    </select>
                    <select style="font-size: 14pt; padding: 3px 13px; width: 350px; display: none;" class="select-nbhd-for-sync">
                    </select>
                    <span class="select-nbhd-for-sync" style="text-align: left; font-size: 14pt;"></span>

                    <select class="SingleNbhd-route-mode" style="display: none;text-align: center;font-size: 16pt;padding: 3px 13px;width: 184px;margin-bottom: 15px;margin-left: 10px;">
                        <option value="0">Driving Mode</option>
                        <option value="1">Walking Mode</option>
                    </select>
                    <br/>
                    
                    <span class="select-image-count span-select-image-count" style="display: none; text-align: left; font-size: 14pt; padding: 3px 0px; width: 350px; margin-bottom: 10px; width: 100%;">
                        <label style="display:block; font: bold 18px 'Helvetica Neue', Helvetica; color: #5c6d7b; text-shadow: #e8ebee 0 1px 0;">Photo Download Options</label>
                        <label style="display:block; font-size: 14px; margin-top: 10px;">Select an option for downloading photos below. Choosing to download less photos will result in more properties downloaded.</label>
                        <span class="select-image-count-span">

                        </span>
                    </span>
                    
                </p>
                <p style="width: 100%; text-align: center; margin-top: 50px;" class="nbhd-select-buttons">
                    <button style="padding: 10px 30px; margin-right: 20px;" class='nbhd-list-refresh' onclick="RefreshNbhds();">
                        Refresh</button>
                    <button style="padding: 10px 30px; margin-right: 20px;" class="nbhd-selected">
                        OK</button>
                    <button style="padding: 10px 30px; margin-left: 20px;" class="nbhd-cancel" onclick="CancelSync()">
                        Cancel</button>
                </p>
            </div>
        </div>
    </div>
</section>