﻿<%@ Control Language="vb" ClassName="MassUpdateScreen" %>
<section class="screen" id="mass-update-div">
    <header>
        <span>Mass Update Categories</span> <a class="page-back"></a>
    </header>
    <div class="scrollable" style="overflow-x:hidden;">
        <div class="dc-massupdate mass-update-padding" >
        </div>
    </div>
</section>
