﻿<%@ Control Language="VB" ClassName="SketchViewer" %>
<script runat="server">

</script>
<section class="screen sketch-viewer" id="sketchview">
    <header>
        <span>Parcel Sketches</span> <input type="text" class="skpage-ddl"> <a class="page-back"></a>
    </header>
    <div class="scrollable">
        <div style="padding-bottom:50px;">
            <div class="sketch-preview" style="background-size: contain; background-position: center center; background-repeat: no-repeat;width:750px;height:750px;margin:40px auto;"></div>
            <div style="text-align:center; margin-bottom:25px;"><span class="invalidalerticon" style="display: none"></span><button class="btn-open-sketch-editor">Open Sketch Editor</button></div>
        </div>
    </div>
</section>