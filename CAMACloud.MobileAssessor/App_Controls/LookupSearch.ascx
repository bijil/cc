﻿<%@ Control Language="vb" AutoEventWireup="false" ClassName="LookupSearch" %>
<div class="mask">
</div>
<div class="customddl">
    <div class="customddl_main_block">
        <div class="customddl_header">
            <div controls="">
                <table>
                    <tr>
                        <td colspan="3"><span legend></span>:&nbsp;&nbsp;<span selectedcustomddl=""></span></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" onkeyup="return FillLookupList(this);" /></td>
                        <td>
                            <button class="customddlbtnadd" onclick="return setLookupValue(this);">Ok </button>
                        </td>
                        <td>
                            <button onclick="return closeddlDefault();">Cancel</button></td>
                    </tr>
                </table>
            </div>
            <div headerright>
            </div>
        </div>
        <div id="lookupout" style="clear: both;">
        	<div class="lkShowAll">
	        	<span class="lkShowAllStatus" style="float: left;"> Showing 0 records out of 0 </span>
	        	<span class="lkShowAllRec" style="display: none; float: Right; margin-right: 40px"> Showing All Records </span>
	        	<span class="lkShowAlllink" onclick="return fillAllLookupList();" style="text-decoration: underline; float: Right; margin-right: 40px">Show All Records</span>
        	</div>
            <div dummyscroll class="">
                <div class="customddl_desc scrollable">
	                <ul id="loolupul" >
	                
	                </ul>
                </div>
            </div>
        </div>
    </div>
</div>
