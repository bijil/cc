﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="OverlaySketchControl.ascx.vb"
    Inherits="CAMACloud.MobileAssessor.OverlaySketchControl" %>
<div class="ccsketched overlay-sketch-editor full-frame hidden unselectable" unselectable="on">
    <div class="ccse-sketchtoolbar" style="height: 40px;">
        <div class="ccse-toolbar toolbar">
            <div class="ccse-other-tools">
                <span>Sketch/Segment: </span>
                <%--<select class="ccse-sketch-select" style="width: 100px; height: 24px;">
                </select>--%>
                <input type="text" class="ccse-sketch-select" style="width: 100px; height: 24px;">
                <select class="ccse-page-select" style="width: 40px; height: 24px; display: none;">
                </select>
                <%--<select class="ccse-vector-select" style="width: 100px; height: 24px;">
                </select>--%>
                <input type="text" class="ccse-vector-select" style="width: 100px; height: 24px;">
                <span class="ccse-add-page" style="display: none;">+</span>
                <span class="ccse-delete-page" style="display: none;">x</span>
                <span class="sketch-label" style="font-size: 12pt; margin: 6pt; display: none;">Status</span>
                <span class="ccse-zoom-value" style="font-size: 12pt; display: inline-block; text-align: center; width: 60px;"><span>100</span>%</span>
            </div>
            <a class="ccse-toolbutton ccse-zoom ccse-tool-active"></a>
            <a class="ccse-toolbutton ccse-pan ccse-tool-active"></a>
            <a class="ccse-toolbutton ccse-angle-lock ccse-tool-active"></a>
            <a class="ccse-toolbutton ccse-dcc-photo" style="display: none;"></a>
            <a class="ccse-toolbutton ccse-disto" style="display: none;"></a>
          	<input type="text" id="color_picker">
           	<a class="ccse-toolbutton ccse-options ccse-tool-inactive" onclick="showSketchOptions()" style=""></a>
            <a class="ccse-toolbutton ccse-screen-lock" style="float: right;" islocked="false"></a>
            <div class="fvSwitch" style="display: none;"><label class="switch"><input type="checkbox" class="fv-switch-input" onchange="return enableFullView();"><span class="slider"></span></label></div>
            <div class="ccse-option-list hidden">
                <a class="list ccse-close-all" >Close All Vectors</a>
                <a class="list ccse-undo-all" >Undo All Changes</a>
             	<a class="list ccse-flip-v" >Flip Vertical</a>
                <a class="list ccse-flip-h" >Flip Horizontal</a>
            </div>
        </div>
        <div class="ccse-toolbar toolbar">
            <div style="float: left" class="ccse-mode-buttons">
                <div >
                    <button class="ccse-create-newpage" style="display: none;">
                        Add Page</button>
                    <button  class="ccse-delete-newpage" style="display: none;">
                        Delete Page</button>

                </div>            
            </div>
            <div style="float: right" class="ccse-mode-buttons">
                <div class="ccse-mode ccse-mode-scale" >
                	Scale : 
                    <input class="ccse-scale" readonly="readonly" style="width: 50px;" />
                </div>
                <div class="ccse-mode ccse-mode-line" >
                    Length:
                    <input class="ccse-meter-length" readonly="readonly" style="width: 55px;" />
                    Angle:
                    <input class="ccse-meter-angle" readonly="readonly" style="width: 55px;" />
                </div>
                <div class="ccse-mode ccse-mode-rotate-sketch">
                    <button class="ccse-rotate-sketch">
                    </button>
                </div>
                <div class="ccse-mode ccse-mode-manual-sketch">
                    <button class="ccse-manual-box">
                        Add'l Areas
                    </button>
                </div>
                <div class="ccse-mode ccse-mode-undo-segment">
                    <button class="ccse-undo-segment">
                    </button>
                </div>
                <div class="ccse-mode ccse-mode-sketch-details">
                    <button class="ccse-sketch-details">
                        Details
                    </button>
                </div>
                <div class="ccse-mode ccse-mode-select-line">
			        <button class="ccse-party-wall">
                        Party Wall
                    </button>
			    </div>
                <div class="ccse-mode ccse-mode-select-node">
                    <button class="ccse-show-pad">
                        Keys</button>
                    <button class="ccse-delete-node">
                        Delete Node</button>
                    <button class="ccse-add-node-before">
                        Add Node before</button>
                     <button class="ccse-delete-line">
                        Delete Line</button>
                </div>
                <div class="ccse-mode ccse-mode-transfer-segment">
                    <button class="ccse-transfer-vector">
                        Transfer Segment</button>
                </div>
                <div class="ccse-mode ccse-mode-multi-segment">
                    <button class="ccse-add-segment">
                        Add Segment</button>
                </div>
                <div class="ccse-mode ccse-mode-select-vector">
                    <button class="ccse-delete-vector">
                        Delete</button>
                </div>

                <div class="ccse-mode ccse-mode-copy-vector">
                    <button class="ccse-copy-vector">
                        Copy Segment</button>
                </div>
                <div class="ccse-mode ccse-mode-paste-vector" style="display: none">
                    <button class="ccse-paste-vector">
                        Paste Segment</button>
                </div>
                <div class="ccse-mode ccse-mode-select-vector-plus">
                    <button class="ccse-edit-label">
                        Edit Label</button>
                </div>
                <div class="ccse-mode ccse-mode-select-label-move">
                    <button class="ccse-move-label">
                        Move Label</button>
                </div>
                <div class="ccse-custom-buttons">

                </div>
                <div class="ccse-mode ccse-mode-select-sketch-plus">
                    <button class="ccse-add-vector">
                        Add Segment</button>
                </div>
                 <div class="ccse-mode ccse-mode-outbuilding-plus">
                    <button class="ccse-add-outbuilding">
                        Add OutBuilding</button>
                </div>
                <div class="ccse-mode ccse-mode-select-sketch">
<%--                    <button class="ccse-flip-v">
                        Flip-V</button>
                    <button class="ccse-flip-h">
                        Flip-H</button>--%>
                    <button class="ccse-sketch-move" status="off">
                        Move</button>
                </div>

                <div class="ccse-mode ccse-mode-add-segment">
                    <button class="ccse-show-pad">
                        Keys</button>
                    <button class="ccse-undo">
                        Undo</button>
                     <button class="ccse-redo">
                        Redo</button>
                </div>
                <div class="ccse-mode ccse-mode-notes">
                    <button class="ccse-notes-toggle">
                        Hide Notes</button>
                </div>
                <div class="ccse-mode ccse-mode-notes-edit">
                    <button class="ccse-notes-edit">
                        Edit Note</button>
                </div>
                <div class="ccse-mode ccse-mode-notes-new">
                    <button class="ccse-notes-new">
                        New Note</button>
                </div>
                <div class="ccse-mode ccse-mode-notes-delete">
                    <button class="ccse-notes-delete">
                        Delete Note</button>
                </div>
                <div class="ccse-mode ccse-mode-sqft-tool">
                    <button class="ccse-sqft-tool">
                       SQFT</button>
                </div>
                <div class="ccse-mode ccse-mode-visible">
                    <button class="ccse-save">
                        Save </button>
                    <button class="ccse-close">
                        Close</button>
                   
                </div>
                 
            </div>
			<span class="testspan" style="color:white"></span>
        </div>
        <div class="pan-control pan-normal control-window" style="display: none;">
        </div>
        <div class="zoom-control control-window">
            <a class="zoom-minus zoom-button minus">-</a>
            <div class="zoom-range range">
            </div>
            <a class="zoom-plus zoom-button plus">+</a>
        </div>
    </div>
    <div class="sketch-pad" style="display: none;">
        <div class="movable-title-bar">
            Edit Sketch Segment <a class="hide-sketch-pad"><span></span></a>
        </div>
        <div class="sketch-pad-key-pad">
            <table>
                <tr>
                    <td>
                        <span class="sp-text-box box-length" selected="" max="9999" min="0"><span>0</span>'</span>
                    </td>
                    <td>
                        <a class="sketch-pad-btn sketch-pad-cmd sp-add" cmd="+"><span></span></a><a class="sketch-pad-btn sketch-pad-cmd sp-up"
                            cmd="U" style = "margin-left: 3px"><span></span></a><a class="sketch-pad-btn sketch-pad-cmd sp-angle" cmd="A" style = "margin-left: 3px"><span></span></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="sp-text-box box-angle" max="180" min="-180"><span>0</span>&deg;</span>
                    </td>
                    <td>
                        <a class="sketch-pad-btn sketch-pad-cmd sp-left" cmd="L"><span></span></a><a class="sketch-pad-btn sketch-pad-cmd sp-down"
                            cmd="D" style = "margin-left: 3px"><span></span></a><a class="sketch-pad-btn sketch-pad-cmd sp-right" cmd="R" style = "margin-left: 3px"><span></span></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a class="sketch-pad-btn sketch-pad-btn-wide sketch-pad-cmd sp-circle" cmd="ELL">ELL</a>
                    </td>
                    <td>
                        <a class="sketch-pad-btn sketch-pad-num sp-n7">7</a> <a class="sketch-pad-btn sketch-pad-num sp-n8">8</a> <a class="sketch-pad-btn sketch-pad-num sp-n9">9</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a class="sketch-pad-btn sketch-pad-btn-wide sketch-pad-cmd sp-rect" cmd="BOX">BOX</a>
                    </td>
                    <td>
                        <a class="sketch-pad-btn sketch-pad-num sp-n4">4</a> <a class="sketch-pad-btn sketch-pad-num sp-n5">5</a> <a class="sketch-pad-btn sketch-pad-num sp-n6">6</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a class="sketch-pad-btn sketch-pad-btn-wide sketch-pad-num sp-nc">CLEAR</a>
                    </td>
                    <td>
                        <a class="sketch-pad-btn sketch-pad-num sp-n1">1</a> <a class="sketch-pad-btn sketch-pad-num sp-n2">2</a> <a class="sketch-pad-btn sketch-pad-num sp-n3">3</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a class="sketch-pad-btn sketch-pad-btn-wide sketch-pad-cmd sp-ok" cmd="OK">OK</a>                     
                    </td>
                    <td>
                        <a class="sketch-pad-btn sketch-pad-num sp-nc">-</a> <a class="sketch-pad-btn sketch-pad-num sp-n0">0</a> <a class="sketch-pad-btn sketch-pad-num sp-np">.</a>
                    </td>
                </tr>
                <tr>
                    <td>
                    	<a class="sketch-pad-btn sketch-pad-cmd sp-recty" cmd="RECT" style ="width:84px">Rectangle</a>
                    </td>
                    <td>
                        <span class="sp-text-box box-lengthR" selected="" max="999" min="0" style ="width:57px; font-size: 16pt; text-align: left">L:<span>0</span></span>
                    	<span class="sp-text-box box-width" selected="" max="999" min="0" style ="width:57px; font-size: 16pt; text-align: left">W:<span>0</span></span>
                	</td>
                </tr>
                <tr>
                    <td>
                        <select class="sketch-pad-ddl sketch-poly-sides">
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </td>
                    <td>
                        <a class="sketch-pad-btn sketch-pad-btn-wide sketch-pad-cmd sp-poly" cmd="POLY">POLY</a>
                        <a class="sketch-pad-btn sketch-pad-cmd sp-rectx" cmd="BOX">BX</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <canvas id="sketch-canvas" class="ccse-canvas" width="600" height="600" style="width: 600px; height: 600px;"></canvas>
</div>


<div class="ccse-img-container full-frame  hidden">
    <canvas class="ccse-img" width="1000" height="1000" style="background: White; height: 1000px; width: 1000px;"></canvas>
</div>
<div class="Current_vector_details hidden">
    <div class="head">Edit Labels</div>
    <div class="dynamic_prop">
    </div>
    <div class="skNote" style="width: 92%; margin: 0px 15px;"></div>
    <div style="float: right; padding: 5px 45px">
        <button id="Btn_Save_vector_properties">Save </button>
        <button id="Btn_cancel_vector_properties">Cancel </button>
    </div>
</div>

<div class="manual-entry-container hidden"  id="manual-entry">
  <button class="manual-hide-button" id="manual-hideBtn">&#9650;</button>
  <div class="manual-popup-header">
    <div class="manual-popup-title">Manually Entered Areas</div>
  </div>
  <div class="manual-entery-popup-content">
  </div>
</div>

<div class="customNote">
    <div class="note-content">
        <div class="note-header">New Sketch Note</div>
        <div class="custom-note-options"></div>
        <textarea class="note-textarea"></textarea>
        <div class="note-button-container">
            <button class="note-button note-ok">OK</button>
            <button class="note-button note-cancel">Cancel</button>
        </div>
    </div>
</div>
