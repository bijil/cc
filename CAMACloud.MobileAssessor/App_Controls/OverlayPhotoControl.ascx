﻿<%@ Control Language="VB" ClassName="OverlayPhotoControl" %>
<div class="overlay-photo-backlay dimmer full-frame hidden">
</div>
<div class="overlay-photo-viewer full-frame" style="display: none;">
    <div class="photo-box-tools">
     
        
	   <a class="pm-btn pm-left" ></a>
       <a class="pm-btn pm-right" ></a>
       <a class="pm-btn pm-add" ><input type="file" 
		accept="image/*" id="camera" name="camera" style="width: 45px; float: left;height: 98%;font-size: 5px;opacity: 0"/></a>
       <a class="pm-btn pm-accept"></a>
       <a class="pm-btn pm-trash" ></a>
       <a class="pm-btn pm-prmflag"  style="width: 193px;"></a>
       <a class="pm-btn pm-props" ></a>
       <a class="pm-btn pm-close pm-hide" ></a>
    </div>
    <div class="overlay-photo-box">
    </div>
    <div class="photo-index">
    </div>
    
</div>


<div id="_winCameraDiv" class="full-frame" style="display: none; background: rgba(0,0,0,0.8);">
    <div id="_divWinCameraMain"></div>
</div>
<div class="overlay-photo-properties full-frame" style="display: none;">
    <div class="photo-box-tools" style="text-align:right;">
       <a class="pm-btn pm-close pm-hidepro"></a>
    </div>
    <div class = "photo-meta-photoPreview" style="background-size: contain;">
    </div>
   	<div class="photo-meta-fields scrollable" style="max-height: 55%; overflow: auto;">
	</div>
    <div class="photo-box-tools" style="text-align: center; position:absolute; bottom:5px; width:100%;">
    <a class="pm-btn pm-leftpro" ></a>
    <span class="photo-index unselectable" unselectable="on"></span>
       <a class="pm-btn pm-rightpro" ></a>
    </div>
</div>
<div class="overlay-sketchimage-viewer full-frame" style="display: none;">
   
     <div class="overlay-sketchimage-box">
          <div class="markup">

          </div>
    </div>

</div>