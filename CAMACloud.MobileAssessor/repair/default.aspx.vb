﻿Public Class _default1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetNoStore()
        Dim repairId As String = Date.UtcNow.Ticks.ToString
        Session("REPAIRCACHE1") = repairId
        Session("REPAIRCACHE2") = repairId
        Response.Redirect("~/#repairing")
    End Sub

End Class