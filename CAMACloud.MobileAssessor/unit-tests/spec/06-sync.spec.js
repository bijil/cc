import { importMobileAssessorGlobal } from './util/setup.js';
import { createGlobalFixture } from './util/sync.js';

describe('06-sync', () => {
  /** @type {jasmine.Clock} */
  let clock;
  /** @type {jasmine.Spy} */
  let checkNetworkSpy;
  /** @type {jasmine.Spy} */
  let _uploadParcelChangesSpy;

  beforeEach(() => {
    clock = jasmine.clock().install();

    // Reset global fixture
    createGlobalFixture();

    // Imported before each to reset the globals defined by the script
    importMobileAssessorGlobal('static/js/06-sync.js');

    checkNetworkSpy = spyOn(global, 'checkNetwork').and.callFake((success, _failure) => {
      setTimeout(success);
    });
    _uploadParcelChangesSpy = spyOn(global, '_uploadParcelChanges').and.callFake((success, _failure, exit) => {
      setTimeout(() => {
        uploadInProgress = false;
        (exit || success)();
      }, 200);
    });
  });

  afterEach(() => {
    clock.uninstall();
  });

  describe('RunSync', () => {
    /** @type {jasmine.Spy} */
    let startApplicationSpy;
    /** @type {jasmine.Spy} */
    let DownloadDataSpy;
    /** @type {jasmine.Spy} */
    let DownloadSystemDataSpy;
    /** @type {jasmine.Spy} */
    let DownloadAppDataSpy;
    /** @type {jasmine.Spy} */
    let DownloadRefreshDataSpy;

    beforeEach(() => {
      DownloadDataSpy = spyOn(global, 'DownloadData').and.callFake((callback) => setTimeout(callback));
      DownloadSystemDataSpy = spyOn(global, 'DownloadSystemData').and.callFake((callback) => setTimeout(callback));
      DownloadAppDataSpy = spyOn(global, 'DownloadAppData').and.callFake((callback) => setTimeout(callback));
      DownloadRefreshDataSpy = spyOn(global, 'DownloadRefreshData').and.callFake((callback) => setTimeout(callback));
      startApplicationSpy = spyOn(global, 'startApplication').and.callFake((callback) => {
        callback(() => {
          syncInProgress = false;
        });
      });
    });

    it('is created', () => {
      expect(global.RunSync).toBeDefined();
    });

    it(`doesn't sync when prior upload in progress`, () => {
      const messageBoxSpy = spyOn(global, 'messageBox');

      UploadData();
      clock.tick(100);
      RunSync(1);
      clock.tick(300);

      expect(messageBoxSpy).toHaveBeenCalledTimes(1);
      expect(_uploadParcelChangesSpy).toHaveBeenCalledTimes(1);
      expect(_uploadParcelChangesSpy).not.toHaveBeenCalledWith(null, null, jasmine.any(Function), jasmine.anything());
    });

    it('uploads parcel changes', () => {
      RunSync(1);
      clock.tick(300);

      expect(_uploadParcelChangesSpy).toHaveBeenCalledOnceWith(null, null, jasmine.any(Function), false);
    });

    it('prevents reentrancy', () => {
      RunSync(1);
      RunSync(1);
      clock.tick(300);

      expect(_uploadParcelChangesSpy).toHaveBeenCalledTimes(1);

      RunSync(1);
      clock.tick(300);

      expect(_uploadParcelChangesSpy).toHaveBeenCalledTimes(2);
    });

    it(`prevents upload parcel changes reentrancy`, () => {
      _uploadParcelChangesSpy.and.callFake((success, _failure, exit) => {
        setTimeout(exit || success, 1000);
      });

      UploadData();
      RunSync(1);
      clock.tick(300);

      expect(_uploadParcelChangesSpy).toHaveBeenCalledTimes(1);
    });

    it('starts application', () => {
      for (const [type, callback] of [[0, DownloadDataSpy], [1, DownloadSystemDataSpy], [2, DownloadAppDataSpy], [4, DownloadRefreshDataSpy]]) {
        startApplicationSpy.calls.reset();
        callback.calls.reset();

        RunSync(type);
        clock.tick(300);

        const context = `when type is ${type}`;
        expect(startApplicationSpy).withContext(context).toHaveBeenCalledTimes(1);
        expect(callback).withContext(context).toHaveBeenCalledTimes(1);
      }
    });

    it(`doesn't start application`, () => {
      const callbacks = [DownloadDataSpy, DownloadSystemDataSpy, DownloadAppDataSpy, DownloadRefreshDataSpy];
      for (const type of [null, undefined, -1, 3, 5]) {
        startApplicationSpy.calls.reset();

        RunSync(type);
        clock.tick(300);

        const context = `when type is ${type}`;
        expect(startApplicationSpy).withContext(context).not.toHaveBeenCalled();
        callbacks.forEach(callback => {
          expect(callback).withContext(context).not.toHaveBeenCalled();
        });
      }
    });
  });

  describe('UploadData', () => {
    /** @type {jasmine.Spy} */
    let _restartSyncSpy;
    /** @type {jasmine.Spy} */
    let checkSchemaUpdateSpy;
    /** @type {jasmine.Spy} */
    let messageBoxSpy;

    beforeEach(() => {
      messageBoxSpy = spyOn(global, 'messageBox');
      checkSchemaUpdateSpy = spyOn(global, 'checkSchemaUpdate').and.callFake(exit => {
        setTimeout(exit);
      });
      _restartSyncSpy = spyOn(global, '_restartSync');
      spyOn(global, 'startApplication').and.callFake((_callback) => {
        syncInProgress = false;
      });
    });

    afterEach(() => {
      // UploadData is a recurring background process not expected to create message boxes;
      // exceptions include location cleanup failure (as tested further below).
      expect(messageBoxSpy).not.toHaveBeenCalled();
    });

    it('is created', () => {
      expect(global.UploadData).toBeDefined();
    });

    it(`doesn't upload when not logged in`, () => {
      ccma.Session.IsLoggedIn = false;

      UploadData();
      clock.tick(300);

      expect(_uploadParcelChangesSpy).not.toHaveBeenCalled();
      expect(checkSchemaUpdateSpy).not.toHaveBeenCalled();
      expect(_restartSyncSpy).not.toHaveBeenCalled();
    });

    it(`doesn't upload when sync in progress`, () => {
      RunSync(1);

      UploadData();
      clock.tick(500);

      expect(_uploadParcelChangesSpy).toHaveBeenCalledTimes(1);
      expect(checkSchemaUpdateSpy).not.toHaveBeenCalled();
      expect(_restartSyncSpy).toHaveBeenCalled();
    });

    it('restarts sync timer after check network failure', () => {
      checkNetworkSpy.and.callFake((_success, failure) => {
        setTimeout(failure);
      });

      UploadData();
      clock.tick(300);

      expect(_restartSyncSpy).toHaveBeenCalled();
    });

    it('restarts sync timer after execute sql failure', () => {
      executeSqlSpy.and.callFake((_sql, _params, _success, failure) => {
        setTimeout(failure);
      });

      UploadData();
      clock.tick(300);

      expect(executeSqlSpy).toHaveBeenCalledTimes(1);
      expect(_restartSyncSpy).toHaveBeenCalled();
    });

    it('restarts sync timer after location update failure', () => {
      executeSqlSpy.and.callFake((_sql, _params, success, _failure) => {
        const response = {
          rows: {
            length: 1,
            item: (_i) => ({ UpdateTime: new Date(), Latitude: 0, Longitude: 0, Accuracy: 0 }),
          }
        };
        setTimeout(() => success({}, response));
      });
      const ajaxRequestSpy = spyOn(global, 'ajaxRequest').and.callFake((params) => {
        setTimeout(() => params.error());
      });

      UploadData();
      clock.tick(300);

      expect(ajaxRequestSpy).toHaveBeenCalledTimes(1);
      expect(_restartSyncSpy).toHaveBeenCalled();
    });

    it('displays message box after location cleanup failure', () => {
      // Before location cleanup can occur, have to setup a successful sql select and
      // subsequent successful ajax request.
      executeSqlSpy.and.callFake((sql, _params, success, failure) => {
        if (sql.startsWith('SELECT')) {
          const response = {
            rows: {
              length: 1,
              item: (_i) => ({ UpdateTime: new Date(), Latitude: 0, Longitude: 0, Accuracy: 0 }),
            }
          };
          setTimeout(() => success({}, response));
        } else {
          expect(sql).toMatch(/^DELETE/);
          setTimeout(() => failure({}, { message: 'oof' }));
        }
      });
      const ajaxRequestSpy = spyOn(global, 'ajaxRequest').and.callFake((params) => {
        params.success({});
      });

      UploadData();
      clock.tick(300);

      expect(messageBoxSpy).toHaveBeenCalledWith('oof');
      messageBoxSpy.calls.reset();

      expect(executeSqlSpy).toHaveBeenCalledTimes(2);
      expect(ajaxRequestSpy).toHaveBeenCalledTimes(1);
      expect(_uploadParcelChangesSpy).toHaveBeenCalledTimes(1);
      expect(_restartSyncSpy).toHaveBeenCalled();
    });

    it(`prevents reentrancy`, () => {
      UploadData();
      UploadData();
      clock.tick(300);

      expect(_uploadParcelChangesSpy).toHaveBeenCalledTimes(1);

      UploadData();
      clock.tick(300);

      expect(_uploadParcelChangesSpy).toHaveBeenCalledTimes(2);
    });

    it(`prevents upload parcel changes reentrancy`, () => {
      RunSync(1);

      UploadData();
      clock.tick(300);

      expect(_uploadParcelChangesSpy).toHaveBeenCalledTimes(1);
    });
  });

  describe('_restartSync', () => {
    it('is created', () => {
      expect(global._restartSync).toBeDefined();
    });

    it('calls UploadData', () => {
      const UploadDataSpy = spyOn(global, 'UploadData');

      _restartSync();
      clock.tick(30000);

      expect(UploadDataSpy).toHaveBeenCalledTimes(1);
    });

    it(`doesn't stack`, () => {
      const UploadDataSpy = spyOn(global, 'UploadData').and.callThrough();

      _restartSync();
      _restartSync();
      _restartSync();
      clock.tick(30000);

      expect(UploadDataSpy).toHaveBeenCalledTimes(1);
    });
  });

  describe('_uploadParcelChanges', () => {
    /** @type {jasmine.Spy} */
    let successCallbackSpy;
    /** @type {jasmine.Spy} */
    let failedCallbackSpy;
    /** @type {jasmine.Spy} */
    let exitCallbackSpy;
    /** @type {jasmine.Spy} */
    let _restartSyncSpy;
    /** @type {jasmine.Spy} */
    let _uploadPhotosSpy;
    /** @type {jasmine.Spy} */
    let getDataSpy;
    /** @type {jasmine.Spy} */
    let ajaxRequestSpy;

    beforeEach(() => {
      uploadInProgress = true;
      _uploadParcelChangesSpy.and.callThrough();
      successCallbackSpy = jasmine.createSpy('success');
      failedCallbackSpy = jasmine.createSpy('failure');
      exitCallbackSpy = jasmine.createSpy('exit');
      _restartSyncSpy = spyOn(global, '_restartSync');
      _uploadPhotosSpy = spyOn(global, '_uploadPhotos').and.callFake((success, _failure) => {
        setTimeout(success);
      });
      getDataSpy = spyOn(global, 'getData').and.callFake((_sql, _params, success, _failure) => {
        // Limit results so there's no recursion
        const results = ajaxRequestSpy.calls.count() ? [] : [{}];
        setTimeout(() => success(results));
      });
      ajaxRequestSpy = spyOn(global, 'ajaxRequest').and.callFake((params) => {
        setTimeout(() => {
          params.success({ status: 'OK' });
        });
      });
    });

    it('is created', () => {
      expect(global._uploadParcelChanges).toBeDefined();
    });

    it('restarts sync timer', () => {
      ccma.Session.IsLoggedIn = false;

      _uploadParcelChangesSpy(successCallbackSpy, failedCallbackSpy, null, false);

      expect(_restartSyncSpy).toHaveBeenCalledTimes(1);
      expect(successCallbackSpy).not.toHaveBeenCalled();
      expect(failedCallbackSpy).not.toHaveBeenCalled();
      // expects _restartSync to reset uploadInProgress
    });

    it(`doesn't prevent reentrancy`, () => {
      _uploadParcelChangesSpy(successCallbackSpy, failedCallbackSpy, exitCallbackSpy, false);
      _uploadParcelChangesSpy(successCallbackSpy, failedCallbackSpy, exitCallbackSpy, false);
      clock.tick(300);

      expect(ajaxRequestSpy).toHaveBeenCalledTimes(2);
    });

    it('drains parcel changes', () => {
      const results = [{}, {}, {}];
      getDataSpy.and.callFake((_sql, _params, success, _failure) => {
        // Return results for each request until there aren't anymore
        const uploadIndex = ajaxRequestSpy.calls.count();
        const resultsSlice = results.slice(uploadIndex, uploadIndex + 1);
        setTimeout(() => success(resultsSlice));
      });

      _uploadParcelChangesSpy(successCallbackSpy, failedCallbackSpy, exitCallbackSpy, false);
      clock.tick(300);

      expect(_uploadParcelChangesSpy).toHaveBeenCalledTimes(results.length);
    });

    it('does not retry requests', () => {
      ajaxRequestSpy.and.callFake((params) => {
        setTimeout(() => params.success({ status: 'Error' }));
      });

      _uploadParcelChangesSpy(successCallbackSpy, failedCallbackSpy, exitCallbackSpy, false);
      clock.tick(3000);

      expect(ajaxRequestSpy).toHaveBeenCalledOnceWith(jasmine.objectContaining({
        url: jasmine.stringMatching(/mobileassessor\/parcelchanges.jrq\?zt=\d+$/),
      }));
      expect(_uploadParcelChangesSpy).toHaveBeenCalledTimes(1);
      expect(_restartSyncSpy).not.toHaveBeenCalled();
    });

    describe('exitCallback', () => {
      afterEach(() => {
        // exitCallback should always be called if supplied and is called
        // instead of successCallback and failedCallback.
        expect(successCallbackSpy).not.toHaveBeenCalled();
        expect(failedCallbackSpy).not.toHaveBeenCalled();
        expect(exitCallbackSpy).toHaveBeenCalledTimes(1);
        expect(_restartSyncSpy).not.toHaveBeenCalled();

        // uploadInProgress reset is inconsistent; when not logged in, it expects
        // the exitCallback to perform the reset.
        if (ccma.Session.IsLoggedIn) {
          expect(uploadInProgress).withContext('resets uploadInProgress').toBeFalse();
        } else {
          expect(uploadInProgress).withContext(`doesn't reset uploadInProgress`).toBeTrue();
        }
      });

      it('called if not logged in', () => {
        ccma.Session.IsLoggedIn = false;

        _uploadParcelChangesSpy(successCallbackSpy, failedCallbackSpy, exitCallbackSpy, false);
        clock.tick(300);

        expect(getDataSpy).toHaveBeenCalledTimes(0);
        expect(ajaxRequestSpy).toHaveBeenCalledTimes(0);
        expect(_uploadParcelChangesSpy).toHaveBeenCalledTimes(1);
      });

      it('called after getData returns results and sends parcel changes', () => {
        _uploadParcelChangesSpy(successCallbackSpy, failedCallbackSpy, exitCallbackSpy, false);
        clock.tick(300);

        expect(getDataSpy).toHaveBeenCalledTimes(2);
        expect(ajaxRequestSpy).toHaveBeenCalledTimes(1);
        expect(_uploadParcelChangesSpy).toHaveBeenCalledTimes(1);
      });

      it('called after getData returns no results', () => {
        getDataSpy.and.callFake((_sql, _params, success, _failure) => {
          setTimeout(() => success([]));
        });
        _uploadParcelChangesSpy(successCallbackSpy, failedCallbackSpy, exitCallbackSpy, false);
        clock.tick(300);

        expect(getDataSpy).toHaveBeenCalledTimes(1);
      });

      it('called after getData fails', () => {
        getDataSpy.and.callFake((_sql, _params, _success, failure) => {
          setTimeout(() => failure?.call(this));
        });
        _uploadParcelChangesSpy(successCallbackSpy, failedCallbackSpy, exitCallbackSpy, false);
        clock.tick(3000);

        expect(getDataSpy).toHaveBeenCalledTimes(1);
      });

      it('called after parcel changes returns error', () => {
        ajaxRequestSpy.and.callFake((params) => {
          setTimeout(() => params.success({ status: 'Error' }));
        });

        _uploadParcelChangesSpy(successCallbackSpy, failedCallbackSpy, exitCallbackSpy, false);
        clock.tick(300);

        expect(getDataSpy).toHaveBeenCalledTimes(1);
        expect(ajaxRequestSpy).toHaveBeenCalledTimes(1);
        expect(_uploadParcelChangesSpy).toHaveBeenCalledTimes(1);
      });

      it('called after parcel changes returns unexpected error', () => {
        ajaxRequestSpy.and.callFake((params) => {
          setTimeout(() => params.success({}));
        });

        _uploadParcelChangesSpy(successCallbackSpy, failedCallbackSpy, exitCallbackSpy, false);
        clock.tick(300);

        expect(getDataSpy).toHaveBeenCalledTimes(1);
        expect(ajaxRequestSpy).toHaveBeenCalledTimes(1);
        expect(_uploadParcelChangesSpy).toHaveBeenCalledTimes(1);
      });

      it('called after parcel changes request error', () => {
        ajaxRequestSpy.and.callFake((params) => {
          setTimeout(() => params.error());
        });

        _uploadParcelChangesSpy(successCallbackSpy, failedCallbackSpy, exitCallbackSpy, false);
        clock.tick(300);

        expect(getDataSpy).toHaveBeenCalledTimes(1);
        expect(ajaxRequestSpy).toHaveBeenCalledTimes(1);
        expect(_uploadParcelChangesSpy).toHaveBeenCalledTimes(1);
      });

      it('called after check network fails', () => {
        checkNetworkSpy.and.callFake((_success, failure) => {
          setTimeout(failure);
        });

        _uploadParcelChangesSpy(successCallbackSpy, failedCallbackSpy, exitCallbackSpy, false);
        clock.tick(300);

        expect(getDataSpy).not.toHaveBeenCalled();
        expect(ajaxRequestSpy).not.toHaveBeenCalled();
        expect(_uploadParcelChangesSpy).toHaveBeenCalledTimes(1);
      });

      it('called after upload photos fails', () => {
        _uploadPhotosSpy.and.callFake((_success, failure) => {
          setTimeout(failure);
        });

        _uploadParcelChangesSpy(successCallbackSpy, failedCallbackSpy, exitCallbackSpy, false);
        clock.tick(300);

        expect(getDataSpy).not.toHaveBeenCalled();
        expect(ajaxRequestSpy).not.toHaveBeenCalled();
        expect(_uploadParcelChangesSpy).toHaveBeenCalledTimes(1);
      });
    });

    describe('successCallback', () => {
      beforeEach(() => {
        // successCallback isn't called if an exit callback is supplied
        exitCallbackSpy = null;
        uploadInProgress = true;
      });

      afterEach(() => {
        expect(successCallbackSpy).toHaveBeenCalledTimes(1);
        expect(failedCallbackSpy).not.toHaveBeenCalled();
        expect(_restartSyncSpy).not.toHaveBeenCalled();
        expect(uploadInProgress).withContext(`doesn't reset uploadInProgress`).toBeTrue();
      });

      it('called after getData returns results and sends parcel changes', () => {
        _uploadParcelChangesSpy(successCallbackSpy, failedCallbackSpy, exitCallbackSpy, false);
        clock.tick(300);

        expect(getDataSpy).toHaveBeenCalledTimes(2);
        expect(ajaxRequestSpy).toHaveBeenCalledTimes(1);
        expect(_uploadParcelChangesSpy).toHaveBeenCalledTimes(1);
      });

      it('called after getData returns no results', () => {
        getDataSpy.and.callFake((_sql, _params, success, _failure) => {
          setTimeout(() => success([]));
        });
        _uploadParcelChangesSpy(successCallbackSpy, failedCallbackSpy, exitCallbackSpy, false);
        clock.tick(300);

        expect(getDataSpy).toHaveBeenCalledTimes(1);
      });
    });

    describe('failedCallback', () => {
      beforeEach(() => {
        // failedCallback isn't called if an exit callback is supplied
        exitCallbackSpy = null;
      });

      afterEach(() => {
        expect(successCallbackSpy).not.toHaveBeenCalled();
        expect(failedCallbackSpy).toHaveBeenCalledTimes(1);
        expect(_restartSyncSpy).not.toHaveBeenCalled();
        expect(uploadInProgress).withContext('resets uploadInProgress').toBeFalse();
      });

      it('called after parcel changes returns error', () => {
        ajaxRequestSpy.and.callFake((params) => {
          setTimeout(() => params.success({ status: 'Error' }));
        });

        _uploadParcelChangesSpy(successCallbackSpy, failedCallbackSpy, exitCallbackSpy, false);
        clock.tick(300);

        expect(getDataSpy).toHaveBeenCalledTimes(1);
        expect(ajaxRequestSpy).toHaveBeenCalledTimes(1);
        expect(_uploadParcelChangesSpy).toHaveBeenCalledTimes(1);
      });

      it('called after parcel changes returns unexpected error', () => {
        ajaxRequestSpy.and.callFake((params) => {
          setTimeout(() => params.success({}));
        });

        _uploadParcelChangesSpy(successCallbackSpy, failedCallbackSpy, exitCallbackSpy, false);
        clock.tick(300);

        expect(getDataSpy).toHaveBeenCalledTimes(1);
        expect(ajaxRequestSpy).toHaveBeenCalledTimes(1);
        expect(_uploadParcelChangesSpy).toHaveBeenCalledTimes(1);
      });

      it('called after parcel changes request errors', () => {
        ajaxRequestSpy.and.callFake((params) => {
          setTimeout(() => params.error());
        });

        _uploadParcelChangesSpy(successCallbackSpy, failedCallbackSpy, exitCallbackSpy, false);
        clock.tick(300);

        expect(getDataSpy).toHaveBeenCalledTimes(1);
        expect(ajaxRequestSpy).toHaveBeenCalledTimes(1);
        expect(_uploadParcelChangesSpy).toHaveBeenCalledTimes(1);
      });

      it('called after check network fails', () => {
        checkNetworkSpy.and.callFake((_success, failure) => {
          setTimeout(failure);
        });

        _uploadParcelChangesSpy(successCallbackSpy, failedCallbackSpy, exitCallbackSpy, false);
        clock.tick(300);

        expect(getDataSpy).not.toHaveBeenCalled();
        expect(ajaxRequestSpy).not.toHaveBeenCalled();
        expect(_uploadParcelChangesSpy).toHaveBeenCalledTimes(1);
      });

      it('called after upload photos fails', () => {
        _uploadPhotosSpy.and.callFake((_success, failure) => {
          setTimeout(failure);
        });

        _uploadParcelChangesSpy(successCallbackSpy, failedCallbackSpy, exitCallbackSpy, false);
        clock.tick(300);

        expect(getDataSpy).not.toHaveBeenCalled();
        expect(ajaxRequestSpy).not.toHaveBeenCalled();
        expect(_uploadParcelChangesSpy).toHaveBeenCalledTimes(1);
      });

      it('called after getData fails', () => {
        getDataSpy.and.callFake((_sql, _params, _success, failure) => {
          setTimeout(() => failure?.call(this));
        });
        _uploadParcelChangesSpy(successCallbackSpy, failedCallbackSpy, exitCallbackSpy, false);
        clock.tick(3000);

        expect(getDataSpy).toHaveBeenCalledTimes(1);
      });
    });
  });
});
