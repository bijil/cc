// Source: https://stackoverflow.com/a/64785113
/** @type {<T, K extends keyof T>(spy: jasmine.SpyObj<T>, propertyName: K): jasmine.Spy<() => T[K]>} */
export const spyPropertyGetter = (spy, propertyName) => {
  return Object.getOwnPropertyDescriptor(spy, propertyName)?.get;
};
