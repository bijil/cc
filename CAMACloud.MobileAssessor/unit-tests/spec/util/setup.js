import * as fs from 'fs';
import * as path from 'path';

const mobileAssessorProjectRoot = '../';

/** Imports script into the global scope */
export function importGlobal(file) {
  const fileResolved = path.resolve(process.cwd(), file);
  const script = fs.readFileSync(fileResolved).toString('utf8');
  eval.apply(null, [script]);
}

/** Imports script from mobile assessor into the global scope */
export function importMobileAssessorGlobal(file) {
  importGlobal(path.join(mobileAssessorProjectRoot, file));
}
