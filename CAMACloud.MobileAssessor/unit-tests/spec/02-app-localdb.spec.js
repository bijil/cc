import { spyPropertyGetter } from './helpers/jasmine.js';
import { importMobileAssessorGlobal } from './util/setup.js';
import { createGlobalFixture } from './util/app-localdb.js';

describe('02-app-localdb', () => {
  /** @type {jasmine.Clock} */
  let clock;

  beforeEach(() => {
    clock = jasmine.clock().install();

    // Reset global fixture
    createGlobalFixture();

    // Imported before each to reset the globals defined by the script
    importMobileAssessorGlobal('static/js/02-app-localdb.js');
  });

  afterEach(() => {
    clock.uninstall();
  });

  describe('getData', () => {
    /** @type {jasmine.Spy} */
    let errorSpy;
    /** @type {jasmine.spyObj} */
    let dbSpy;
    /** @type {jasmine.spyObj} */
    let transactionSpy;
    /** @type {jasmine.spyObj} */
    let rowsSpy;

    beforeEach(() => {
      errorSpy = spyOn(global.console, 'error');
      rowsSpy = jasmine.createSpyObj('rowsSpy', ['item'], { length: 0 });
      transactionSpy = jasmine.createSpyObj('transactionSpy', ['executeSql']);
      dbSpy = jasmine.createSpyObj('db', ['transaction']);
      dbSpy.transaction.and.callFake((callback) => {
        setTimeout(() => {
          callback(transactionSpy);
        });
      });
      transactionSpy.executeSql.and.callFake((_query, _params, success, _failure) => {
        setTimeout(() => {
          success(transactionSpy, { rows: rowsSpy });
        });
      });
      global.db = dbSpy;
    });

    it('is created', () => {
      expect(global.getData).toBeDefined();
    });

    it('executes query', () => {
      const callbackSpy = jasmine.createSpy('callback');

      getData('SELECT * FROM foo WHERE bar = ?', ['baz'], callbackSpy);
      clock.tick(300);

      expect(transactionSpy.executeSql).toHaveBeenCalledOnceWith(
        'SELECT * FROM foo WHERE bar = ?',
        ['baz'],
        jasmine.any(Function),
        jasmine.any(Function)
      );

      // returns results via success callback
      expect(callbackSpy).toHaveBeenCalledOnceWith(
        jasmine.any(Array),
        undefined,
        { rows: { item: jasmine.any(Function), length: 0 } }
      );
    });

    it('returns data', () => {
      const items = [{ foo: 5 }, { bar: 1 }, { baz: 3 }];
      const callbackSpy = jasmine.createSpy('callback');
      rowsSpy.item.and.callFake((index) => items[index]);
      spyPropertyGetter(rowsSpy, 'length').and.returnValue(items.length);

      getData('SELECT * FROM foo WHERE bar = ?', ['baz'], callbackSpy);
      clock.tick(300);

      expect(callbackSpy).toHaveBeenCalledOnceWith(
        items,
        undefined,
        { rows: { item: jasmine.any(Function), length: items.length } }
      );
    });

    it('returns otherData', () => {
      const otherData = { foo: { bar: 123 }, baz: '456', qux: new Set([6, 7, 8]) };
      const callbackSpy = jasmine.createSpy('callback');

      getData('SELECT * FROM foo WHERE bar = ?', ['baz'], callbackSpy, otherData);
      clock.tick(300);

      expect(callbackSpy).toHaveBeenCalledOnceWith(
        jasmine.any(Array),
        otherData,
        { rows: { item: jasmine.any(Function), length: 0 } }
      );
    });

    it('calls error callback', () => {
      const callbackSpy = jasmine.createSpy('callback');
      const errorCallbackSpy = jasmine.createSpy('errorCallback');
      transactionSpy.executeSql.and.callFake((_query, _params, _success, failure) => {
        // error callback is called on failures
        setTimeout(() => {
          failure(transactionSpy, { message: 'oof' });
        });
      });

      // error callback is called regardless of how ignoreError is specified
      for (const ignoreError of [null, undefined, true, false]) {
        const context = `ignoreError is ${ignoreError}`;
        errorSpy.calls.reset();
        callbackSpy.calls.reset();
        errorCallbackSpy.calls.reset();
        transactionSpy.executeSql.calls.reset();

        getData('SELECT * FROM foo WHERE bar = ?', ['baz'], callbackSpy, undefined, ignoreError, errorCallbackSpy);
        clock.tick(300);

        expect(errorSpy).withContext(context).toHaveBeenCalled();
        expect(transactionSpy.executeSql).withContext(context).toHaveBeenCalledTimes(1);
        expect(errorCallbackSpy).withContext(context).toHaveBeenCalledOnceWith();
        expect(callbackSpy).withContext(context).not.toHaveBeenCalled();
      }
    });

    it('does not call callback', () => {
      const callbackSpy = jasmine.createSpy();
      transactionSpy.executeSql.and.callFake((_query, _params, _success, failure) => {
        // callback is not called on failures
        setTimeout(() => {
          failure(transactionSpy, { message: 'oof' });
        });
      });

      getData('SELECT * FROM foo WHERE bar = ?', ['baz'], callbackSpy);
      clock.tick(300);

      expect(transactionSpy.executeSql).toHaveBeenCalledTimes(1);

      // returns results via success callback
      expect(callbackSpy).not.toHaveBeenCalled();
    });

    it('announces errors', () => {
      const alertSpy = spyOn(global, 'alert');
      const logSpy = spyOn(global, 'log');
      transactionSpy.executeSql.and.callFake((_query, _params, _success, failure) => {
        setTimeout(() => {
          failure(transactionSpy, { message: 'oof' });
        });
      });

      for (const ignoreError of [null, undefined, false]) {
        const context = `ignoreError is ${ignoreError}`;
        alertSpy.calls.reset();
        logSpy.calls.reset();

        getData('SELECT * FROM foo WHERE bar = ?', ['baz'], jasmine.createSpy(), undefined, ignoreError, jasmine.createSpy());
        clock.tick(300);

        expect(alertSpy).withContext(context).toHaveBeenCalledOnceWith('oof');
        expect(logSpy).withContext(context).toHaveBeenCalled();
      }
    });

    it('quiets errors', () => {
      const alertSpy = spyOn(global, 'alert');
      const logSpy = spyOn(global, 'log');
      transactionSpy.executeSql.and.callFake((_query, _params, _success, failure) => {
        setTimeout(() => {
          failure(transactionSpy, { message: 'oof' });
        });
      });

      getData('SELECT * FROM foo WHERE bar = ?', ['baz'], jasmine.createSpy(), undefined, true, jasmine.createSpy());
      clock.tick(300);

      expect(alertSpy).not.toHaveBeenCalled();
      expect(logSpy).not.toHaveBeenCalled();
    });
  });

});
