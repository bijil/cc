﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="CAMACloud.MobileAssessor._default"
    EnableViewState="false" CodeBehind="Default.aspx.vb" %>

<!DOCTYPE html>
<html <%= CacheManifest %>>
<head>
    <title>
        <%=ApplicationTitle %>
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="static/app/apple-touch-icon-precomposed-72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="static/app/apple-touch-icon-precomposed-144.png" />
    <meta name="apple-mobile-web-app-title" content="<%=ApplicationTitle %>" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="user-scalable=no,initial-scale=1.0,maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <link rel="Shortcut icon" href="/static/app/favicon.ico" />
    <link rel="apple-touch-startup-image" href="images/splash/MAminiAir-min.png" media="(min-device-width: 768px) and (max-device-width: 1024px) and (-webkit-min-device-pixel-ratio: 2) and (orientation: portrait)">
    <link rel="apple-touch-startup-image" href="images/splash/MA10.5-min.png" media="(min-device-width: 834px) and (max-device-width: 834px) and (-webkit-min-device-pixel-ratio: 2) and (orientation: portrait)">
    <link rel="apple-touch-startup-image" href="images/splash/MA12-min.png" media="(min-device-width: 1024px) and (max-device-width: 1024px) and (-webkit-min-device-pixel-ratio: 2) and (orientation: portrait)">
    <cc:Style runat="server" IncludeFolder="~/static/themes/css" />
    <cc:Style runat="server" IncludeFolder="~/static/css" />
    <script type="text/javascript" src="static/js/lib/zepto.min.js"></script>
    <script type="text/javascript" src="static/js/lib/CCWarehouseAPI.js"></script>



    <script type="text/javascript">
        
        var macid = '<%=macID%>';
        if ( macid != "" )
            localStorage.setItem( "macid", macid);
        /*function getApiKey() {
           return "<%=APIKey%>"
        }*/
        var EnableEagleViewInMA = (('<%=  ClientSettings.PropertyValue("EnableEagleViewInMA")  %>' == '1')? true: false);
        var UsingOSM = (('<%=  ClientSettings.PropertyValue("UseOSM")  %>' == '1')? true: false);
        var _debugger_key = '<%= CAMACloud.MobileAssessor.General.DebuggerKey %>';
        var _client_ip = '<% = Request.ClientIPAddress%>';
        var WORKER_URL = '/rwi-worker.js';
        var EnableNewPriorities = '<%= EnableNewPriorities %>'
        var TrueFalseInPCI = '<%= DoNotChangeTrueFalseInPCI %>'
        var NewRoutingMethod = (('<%=  ClientSettings.PropertyValue("UseNewRoutingMethod")  %>' == '1') ? true : false);
        /*function getIPALoadUrl() {
            return "<%=IPALoadUrl%>"
        }
        function getSecretKey() {
            return "<%=SecretKey%>"

        }*/

        function getFrameId() {
            return "pictometry_ipa";
        }

        var google = false;
        var L = false;
        var CAMACloud = {};
        var touchClickEvent = 'click';
        var pageTimeout = setTimeout('window.location.reload();', 12000);
        var serviceUnavailabilityWarned = false;
        var localDBName = 'MA<%=Right(Database.Tenant.Application.SchemaVersion.ToString.PadLeft(6, "0"), 6) %>'
        var CALAuthority = '<%=ApplicationSettings.CALAuthority %>';
        var orgaddress = '<%= HttpContext.Current.GetCAMASession.OrganizationAddress%>'
        var org_county_name = '<%= HttpContext.Current.GetCAMASession.OrganizationCountyName%>'
        var isTestingCounty = <%= HttpContext.Current.GetCAMASession.IsTestingCounty%>
        var AlternateKey = '<%=Database.Tenant.Application.AlternateKeyfield%>'
        var ShowKeyValue1 = '<%=Database.Tenant.Application.ShowKeyValue1%>'
        var ShowAlternateField = '<%=Database.Tenant.Application.ShowAlternateField%>'
        var macacheVersion = '<%=cacheVersion%>';
        function sleep(seconds) {
            var e = new Date().getTime() + (seconds * 1000);
            while (new Date().getTime() <= e) { }
        }

        if ( external && external.console != undefined ){
            window.console.log = function () {
                var a = arguments;
                for (var x in a) {
                    var b = a[x];
                    if (typeof b == "object") {
                        a[x] = toJSON(b);
                    }
                }
                external.console.log(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9])
            };

            window.console.error = function () {
                var a = arguments;
                for (var x in a) {
                    var b = a[x];
                    if (typeof b == "object") {
                        a[x] = toJSON(b);
                    }
                }
                external.console.error(a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9])
            };

            window.onkeydown = function (e) {
                if (e.keyCode == 65) {
                    alert(e.keyCode);
                }
            }

            console.log({ a: "Hello", b: "World" });
            console.log("navigator.geolocation " + (navigator.geolocation ? "works." : "do not work."));


            function toJSON(o) {
                var level = 0;
                function copyO(x) {
                    level++;
                    if (level > 5) return {};
                    var r = {};
                    for (var x in o) {
                        if (typeof o[x] == "string" || typeof o[x] == "number" || typeof o[x] == "boolean") {
                            r[x] = o[x];
                        } else if (typeof o[x] == "object") {
                            r[x] = copyO(o[x]);
                        }
                    }
                    level--;
                    return r;
                }
                return JSON.stringify(copyO(o));
            }
        }

        function MapTouch() {
            return true == ("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch);
        }
        if (MapTouch() === true && (/iPad/.test(window.navigator.userAgent) || /iPhone/.test(window.navigator.userAgent)) == false) {
            navigator = navigator || {};
            navigator.msMaxTouchPoints = navigator.msMaxTouchPoints || 2;
        }
    </script>

    <%If DesignMode Then%>
    <!-- All scripts are loaded on runtime. If anything needs to be loaded for local tests, please use test pages, or load here under design mode. -->
    <%Else%>
    <cc:JavaScript runat="server" IncludeFolder="~/static/js/crypto" />
    <cc:JavaScript runat="server" IncludeFolder="~/static/js/lib2" />
    <cc:JavaScript runat="server" IncludeFolder="~/static/js/" />
    <cc:JavaScript runat="server" IncludeFolder="~/static/js/sketchlib/" />
    <%End If%>
    <script type="text/javascript">
        // Essential scripts which should be in the page. This will not work if any other script file has an error, so the page timeout will not cleared. Causing page to reload.
        // Even in case of all scripts not loaded due to network issues, this script will not clear the pageTimeout, causing page to reload.
        clearTimeout(pageTimeout);
        console.log('All scripts loaded successfully. Page timeout cleared.');
        
        
        /*
		var new_worker;
 		if ('serviceWorker' in navigator) {
     		var refreshing = false;
     		navigator.serviceWorker.addEventListener('controllerchange', () => {
         	if (refreshing)
             	return;
         	window.location.reload();
         	refreshing = true;
     		});
     	
     	
     	navigator.serviceWorker.register('service-worker.js').then(registration => {
         if(registration.waiting) new_worker = registration.waiting;
         registration.onupdatefound = function(){
             new_worker = registration.installing;
             if(new_worker){
             new_worker.addEventListener('statechange', () => {
                 if (new_worker.state == 'installed' && navigator.serviceWorker.controller) {
                     messageBox('An updated version of CAMA Cloud is now available. Update now?', ["OK", "Not Now"], function () {
                     new_worker.postMessage( { action: 'skipWaiting' } );
                     showSplash();
            		 setSplashProgress('Updating application..', 80);
        			 setScreenDimensions();
                     }, function () {
                    	appUpdating = false;
                    	hideSplash(); 
                		});
                 	}
             	});
             	}
         	};
     	}).catch(err => {
         console.log('Something went wrong during the registration');
     	});
 	}     	
     	*/

        //loadPictometryIPA();
        if (window.location.hash == '#repairing' && false) {

            window.onload = function () {
                document.getElementsByClassName('splash-progress-window')[0].getElementsByTagName('label')[0].innerHTML = 'Please wait. Repair action in progress...';
                document.getElementsByClassName('btn-repair-app')[0].style.display = 'none';                
            }


            applicationCache.addEventListener('cached', function () {
                window.location.href = '/';
            });

            applicationCache.addEventListener('updateready', function () {
                window.location.href = '/';
            });

            applicationCache.addEventListener('progress', function (e, f, g) {
                var percent = Math.round(e.loaded / e.total * 100);
                var spwa = document.getElementsByClassName('splash-progress-window');
                if (spwa.length > 0) {
                    var pa = spwa[0].getElementsByClassName('progress');
                    if (pa.length > 0) {
                        var pw = Math.round(504 * (percent / 100));
                        pa[0].style.backgroundPositionX = pw + 'px';
                    } else {
                        spwa[0].getElementsByTagName('label')[0].innerHTML = 'Repairing (' + percent + '%)';
                    }
                }

            })
        }
    </script>
</head>
<body>
    <form runat="server" id="cc" onsubmit="return false;" novalidate>
        <div>
            <div class="app-title-line unselectable" style="display: none;" unselectable="on">
                <%=ApplicationTitle %>
            </div>
            <div class="page-wrapper">
                <div class="page-container">
                    <header class="page-header unselectable" unselectable="on">
                        <section class="app-top-logo app-top-left-logo">
                            <section class="app-top-logo app-top-right-logo">
                            </section>
                        </section>
                    </header>
                    <div class="page-content">
                        <div id="jqt">
                            <screens:AppScreens runat="server" />
                            <screens:LoginPage runat="server" />
                            <!-- Has Text Input-->
                            <screens:LoginAgreement runat="server" />
                            <screens:SelectNeighborhood runat="server" />
                            <screens:SortScreen runat="server" />
                            <screens:SearchParcels runat="server" />
                            <!-- Has Text Input-->
                            <screens:ParcelCopy runat="server" />
                            <!-- Has Text Input-->
                            <screens:AppraiserDashboard runat="server" />
                            <screens:ParcelDashboard runat="server" />
                            <!-- Has Text Input-->
                            <screens:DigitalPRC runat="server" />
                            <screens:ParcelComparables runat="server" />
                            <screens:DataCollection runat="server" />
                            <!-- Has Text Input-->
                            <screens:PhotoAlbum runat="server" />
                            <screens:SketchViewer runat="server" />
                            <screens:GoogleDirections runat="server" />
                            <screens:GoogleMap runat="server" />
                            <screens:Synchronization runat="server" />
                            <screens:ViewAgreement runat="server" />
                            <screens:MassUpdateScreen runat="server" />
                            <screens:ClassCalculatorScreen runat="server" />

                            <!-- Has Text Input-->
                        </div>
                    </div>
                    <cc:AppFooter runat="server" />
                </div>
            </div>
        </div>
        <iframe id="hiddentarget" style="display: none; position: absolute; top: 20px; width: 300px; left: 20px; height: 300px;"
            src="about:blank"></iframe>
        <div class="templates-panel" style="display: none;">
            <templates:SortScreenView runat="server" ID="tplSS" />
            <templates:EstimateChart runat="server" ID="tplEC" />
            <templates:NeighborhoodProfile runat="server" ID="tplNP" />
            <templates:ParcelHeader runat="server" ID="tplPH" />
            <templates:ComparablesReport runat="server" ID="tplCR" />
            <templates:PropertyRecordCard runat="server" ID="tplPRC" />
            <templates:DirectionsTemplate runat="server" ID="tplDT" />
            <templates:ErrorTemplates runat="server" ID="tplError" />
            <div class="data-navigator-template">
                <div class="data-navigator">
                    <a class="record-button" action="list" state="grid"></a><a class="record-move in-record-op"
                        ddir="first"></a><a class="record-move in-record-op" ddir="prev"></a><span class="records-indicator">Showing record <span class="aux-index">0</span> of <span class="aux-records">0</span></span>
                    <a class="record-move in-record-op" ddir="next"></a><a class="record-move in-record-op"
                        ddir="last"></a><a class="record-move record-new" ddir="new"></a><a class="record-move in-record-op record-delete "
                            ddir="del"></a><a class="record-massupdate"><a class="record-futurecopy"></a><a class="record-classcalculator"></a>
                                <a action="jump" class="hidden"></a>
                </div>
            </div>
            <div class="aux-header-template">
                <div class="aux-header">
                    <table style="width: 100%">
                        <tr>
                            <td class="aux-nav-back-col" style="${showback}">
                                <button class="aux-nav-back" target="${ParentCategoryId}">
                                    Up</button>
                            </td>
                            <td style="vertical-align: top; padding: 10px; text-align: right;">
                                <div context="subcats" class="sub-cat-item">
                                    <a class="sub-cat-opener" bpptype="${Type}" catid='${Id}' targetdata='${SourceTable}' filters="${FilterFields}"
                                        filtervalues="">
                                        <!--View -->
                                        <strong>${Name}</strong>
                                        <label>
                                            (<span class="aux-records"><strong>-1</strong></span>)
                                        </label>
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div style="display: none;">
            <canvas id="resizer" />
            <input type="text" id="hiddentextbox" />
        </div>
        <cc:OnscreenKeyboard runat="server" />
        <div class="input-blocker dimmer full-frame hidden">
        </div>
        <cc:OverlayPhotoControl runat="server" />
        <cc:OverlaySketchControl runat="server" />
        <cc:LookupSearch runat="server" />
        <cc:GeoLocationMap runat="server" />
        <div class="splash-screen full-frame">
            <div class="installation-instructions">
                Install CAMA Cloud<sup>&#174;</sup> on your Home Screen by clicking on action button (<img
                    src="static/help/ipad-safari-action-icon.png" style="vertical-align: text-bottom" />)
            on the browser toolbar, then selection <b>Add to Home Screen</b> option.
            </div>
            <div class="splash-progress-window">
                <label>
                    Starting MobileAssessor ...</label>
                <div class="progress" value="50"></div>
                <div style="font-weight: bold; text-align: center; font-size: larger; color: Blue;"
                    class="btn-repair-app">
                    <a href="/repair/">Repair Application</a>
                </div>
            </div>
            <div class="splash-progress-continuous" style="display: none;">
                <label>Please wait. It may take several seconds ...</label>
                <div class="sp-continuous">
                    <div class="sp-sub-continuous"></div>
                </div>
            </div> 
        </div>
        <div class="update-box" style="display: none;">
            <span id="update-icon"></span>

            <div id="notification-text">
                <p>
                    <b>Updates are available</b>
                    <br />
                    An update to CAMA Cloud is now available. This notification is to indicate a settings change has been made.
                </p>
            </div>
            <div id="btn-container">
                <input type="button" onclick="closeAlert()" value="Remind Later" class="btn-notification" id="btn_remainder" />
                <input type="button" onclick="updateSchema()" value="Update Now" class="btn-notification" id="btn_update" />
            </div>
        </div>
        <cc:MessageBox runat="server" />

        <div id="maskLayer">
            <span style="margin-left: 47%; margin-top: 22%; float: left">

                <img id="imgMask" src="/static/css/images/loading-photo.gif" />
                <span id="loadText">Loading...</span></span>
        </div>
        <div class="cc-drop-pop" style="width: 300px;display:none;">
            <input type="text" class="cc-drop-customtext" />
            <input type="text" class="cc-drop-search" />
            <div class="cc-drop-items"></div>
        </div>
        <div class="infoContentContainer">
            <div class="infoContentHeader">
                Information - <span class="info_fieldName"></span><span style="float: right; margin: 2px; cursor: pointer;" onclick="hideInfoContent()">X</span>
            </div>
            <div class="infoContent scrollable"></div>
            <div class="infoContentFooter">&nbsp
                <span type="button" value="Close" style="float: right; margin-right: 10px; cursor: pointer;" onclick="hideInfoContent()">Close</span>
            </div>
        </div>
        <div class="compalertmsgContainer">
            <div class="compalertmsgHeader">
                <span>Alert from Field</span><span style="float: right; margin: 2px; cursor: pointer;" onclick="hidecompalertmsg(); ">X</span>
            </div>
            <div class="compalertmsg"></div>
            <div class="compalertmsgFooter">&nbsp
                <span type="button" value="Close" style="float: right; margin-right: 10px; cursor: pointer;" onclick="savecompalertmsg(this)">Update</span>
            </div>
        </div>
        <div style="display:none">
            <a class="url-invoker" href="#" target="_blank">Open URL</a>
        </div>
    </form>

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=geometry&amp;sensor=false&amp;region=GB&amp;key=AIzaSyC7e5YrZlKqWtaY4ehsDwMgCis4IfNs7Xw"></script>	
    <script type="text/javascript" src="/static/js/lib2/oms.min.js"></script>
</body>
</html>

