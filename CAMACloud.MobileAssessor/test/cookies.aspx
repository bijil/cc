﻿<%@ Page Language="vb" AutoEventWireup="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Cookies Test</title>
    <style>
        body {
            font-family: monospace;
            font-size: 10pt;
        }

            body * {
                box-sizing: border-box;
            }

        table {
            border-collapse: collapse;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width: 100%">
                <tr>
                    <th style="width: 120px;">Name</th>
                    <th style="width: 200px;">Domain</th>
                    <th>Value</th>
                </tr>
                <% For Each cks As String In Request.Cookies.AllKeys
                        Dim ck = Request.Cookies(cks)
                        %>

                <tr>
                    <td><%= ck.Name %></td>
                    <td><%= ck.Domain %></td>
                    <td><%= ck.Value %></td>
                </tr>
                <% Next %>
            </table>
        </div>
    </form>
</body>
</html>
