﻿var cache_version = 'ver-4745';
var cache_name = 'CC-' + cache_version;
//files to be cached
var cache_files = [
'.',
'/static/css/app/ccma-small.png',
'/static/css/buttons/add-photo.png',
'/static/css/buttons/back.png',
'/static/css/buttons/bpp-sort-filter.png',
'/static/css/buttons/btn-delete.png',
'/static/css/buttons/btn-mac.png',
'/static/css/buttons/calculator.png',
'/static/css/buttons/createBpp.png',
'/static/css/buttons/data-nav.png',
'/static/css/buttons/dcc-photo.png',
'/static/css/buttons/deleteBpp.png',
'/static/css/buttons/futureYear.png',
'/static/css/buttons/Hideline.png',
'/static/css/buttons/hide-visited.png',
'/static/css/buttons/info.png',
'/static/css/buttons/map.png',
'/static/css/buttons/map-pri-only.png',
'/static/css/buttons/map-show-all.png',
'/static/css/buttons/massupdate.png',
'/static/css/buttons/photo-preview.png',
'/static/css/buttons/record-photo.png',
'/static/css/buttons/setting.png',
'/static/css/buttons/Showline.png',
'/static/css/buttons/sort.png',
'/static/css/buttons/sort-toggle.png',
'/static/css/buttons/standalone-create-Bpp.png',
'/static/css/buttons/view-change.png',
'/static/css/buttons/view-priority.png',
'/static/css/buttons/view-proximity.png',
'/static/css/colors/ffffff.png',
'/static/css/colors/ffffff-bar.png',
'/static/css/images/checkbox.png',
'/static/css/images/close.png',
'/static/css/images/disto1.png',
'/static/css/images/favorite.png',
'/static/css/images/futureYear.png',
'/static/css/images/g10.png',
'/static/css/images/g20.png',
'/static/css/images/gps.gif',
'/static/css/images/gpserror.gif',
'/static/css/images/ico_imprvCopy.png',
'/static/css/images/legendIcon.png',
'/static/css/images/loading-photo.gif',
'/static/css/images/Lock_unlock.png',
'/static/css/images/map_icon.png',
'/static/css/images/map-aerial-icon.png',
'/static/css/images/map-icons.png',
'/static/css/images/map-street-icon.png',
'/static/css/images/nav-sprite.png',
'/static/css/images/next.png',
'/static/css/images/pageloading.gif',
'/static/css/images/pan-buttons.png',
'/static/css/images/parcel.png',
'/static/css/images/parcel-list.png',
'/static/css/images/photomgr.png',
'/static/css/images/processing.gif',
'/static/css/images/progress.jpg',
'/static/css/images/right-button.png',
'/static/css/images/Search_Icon.png',
'/static/css/images/sidepanel.png',
'/static/css/images/sketch-toolbar-bg.png',
'/static/css/images/sketch-tools-sprite.png',
'/static/css/images/tick.png',
'/static/images/accept.png',
'/static/images/back.png',
'/static/images/camera.png',
'/static/images/capture.png',
'/static/images/folder.png',
'/static/images/parcel.png',
'/static/images/rotate.png',
'/static/images/sketch-cursor.png',
'/static/maps/anchor.png',
'/static/maps/anchorGray.png',
'/static/maps/bpp-parcel.png',
'/static/maps/bpp-parcel-alert.png',
'/static/maps/bpp-parcel-done.png',
'/static/maps/bpp-parcel-priority.png',
'/static/maps/bpp-Unsync-alert.png',
'/static/maps/bpp-Unsync-parcel.png',
'/static/maps/bpp-Unsync-priority.png',
'/static/maps/parcel_first.png',
'/static/maps/parcel-alert.png',
'/static/maps/parcel-alert_first.png',
'/static/maps/parcel-done.png',
'/static/maps/parcel-done_first.png',
'/static/maps/parcel-priority.png',
'/static/maps/parcel-priority_first.png',
'/static/maps/Unsync-alert.png',
'/static/maps/Unsync-alert_first.png',
'/static/maps/Unsync-done.png',
'/static/maps/Unsync-parcel.png',
'/static/maps/Unsync-parcel_first.png',
'/static/maps/Unsync-priority.png',
'/static/maps/Unsync-priority_first.png',
'/static/css/tools/home.png',
'/static/css/tools/logout.png',
'/static/css/tools/message.png',
'/static/css/tools/search.png',
'/static/css/tools/sort.png',
'/static/css/tools/stats.png',
'/static/css/tools/sync.png',
'/static/css/nav/county.png',
'/static/css/nav/dashboard.png',
'/static/css/nav/data-collection.png',
'/static/css/nav/gis-map.png',
'/static/css/nav/google-map.png',
'/static/css/nav/navigon.png',
'/static/css/nav/photos.png',
'/static/css/nav/search.png',
'/static/css/nav/sketches.png',
'/static/css/nav/sync64.png',
'/static/css/sketchpad/sp-add.png',
'/static/css/sketchpad/sp-angle.png',
'/static/css/sketchpad/sp-del.png',
'/static/css/sketchpad/sp-down.png',
'/static/css/sketchpad/sp-left.png',
'/static/css/sketchpad/sp-ok.png',
'/static/css/sketchpad/sp-right.png',
'/static/css/sketchpad/sp-up.png'
];
 //message from DOM
self.addEventListener( 'message', evt => {
   if ( evt.data.action == 'skipWaiting' )
       self.skipWaiting();
} );

self.addEventListener( 'install', evt => {
   evt.waitUntil( caches.open( cache_name ).then( cache => {
       return cache.addAll( cache_files );
   } ) );
} );

self.addEventListener( 'activate', evt => {
   evt.waitUntil( caches.keys().then( key_list => {
       return Promise.all( key_list.map( key =>{
           if ( cache_name.indexOf( key ) === -1 )
               return caches.delete( key );
       } ) );
   } ) );
} );
//fetch from cache first
self.addEventListener( 'fetch', evt => {
   evt.respondWith(
caches.match( evt.request ).then( cache_response => {
   return cache_response || fetch( evt.request ).then( response => {
       return caches.open( cache_name ).then( cache => {
           cache.put( evt.request, response.clone() );
           return response;
       } );
   } );
} )
);
} );
