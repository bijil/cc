﻿Imports System.Net
Imports System.Text
Imports System.Text.Encoding
Imports Newtonsoft.Json
Imports System.Xml

Public Class MapQuest

    Shared ErrorMessages As New List(Of String)
    Public Const MAPQUEST_KEY = "TbrubRuJmy19ytFhyA1iGbEpg9c9P3A0"
    Public Shared Function Query(path As String, jsonData As String, Optional ByRef success As Boolean = False) As String
        ErrorMessages.Clear()
        Dim url As String = "http://www.mapquestapi.com/" + path + "?key=" + MAPQUEST_KEY + "&outFormat=json&inFormat=json"
        Dim request = HttpWebRequest.Create(url)
        request.Method = "POST"
        request.ContentType = "text/json"
        request.Timeout = 216000
        Dim dataBytes = ASCII.GetBytes(jsonData.ToString)
        request.GetRequestStream.Write(dataBytes, 0, dataBytes.Length)
        Dim processStartTime = DateTime.Now
        Dim response = request.GetResponse()
        Dim rstrm = response.GetResponseStream()
        Dim ms As New IO.MemoryStream
        rstrm.CopyTo(ms)
        Dim respBytes = ms.ToArray
        Dim xOut = ASCII.GetString(respBytes)
        Dim elapsedTime = (Date.Now - processStartTime).TotalSeconds
        success = True
        'Console.WriteLine("MapQuest request completed in {0}", elapsedTime)
        Return xOut
    End Function
 
    Public Shared Function ProcessRoute(ByRef stats As RequestStatistics, refLocation As Location, ByRef wayPoints As List(Of Location), Optional exitPoint As Location = Nothing, Optional routeMethod As Integer = 0) As List(Of Integer)
        Dim sequence As New List(Of Integer)
        'Console.WriteLine("Computing route for " & wayPoints.Count & " points.")
        Dim op As New Options
        Dim routeJ As New MQRequest
        op.routeType = "SHORTEST"
        op.narrativeType = "none"
        op.enhancedNarrative = False
        op.doReverseGeocode = False
        If routeMethod = 1 Then
            op.routeType = "PEDESTRIAN"
        End If
        routeJ.options = op
        Dim lo As New List(Of Locs)
        If refLocation IsNot Nothing Then
            Dim locss As New Locs
            Dim latslngs As New LatLng
            latslngs.lat = refLocation.Latitude
            latslngs.lng = refLocation.Longitude
            locss.latLng = latslngs
            lo.Add(locss)
        End If
        For Each l In wayPoints
            Dim locss As New Locs
            Dim latslngs As New LatLng
            latslngs.lat = l.Latitude.ToString
            latslngs.lng = l.Longitude.ToString
            locss.latLng = latslngs
            lo.Add(locss)
        Next

        If exitPoint IsNot Nothing Then
            Dim locss As New Locs
            Dim latslngs As New LatLng
            latslngs.lat = exitPoint.Latitude
            latslngs.lng = exitPoint.Longitude
            locss.latLng = latslngs
            lo.Add(locss)
        End If
        routeJ.locations = lo
        Dim jsonStr As String = JsonConvert.SerializeObject(routeJ)
        Dim success As Boolean = False
        Dim routeResp = MapQuest.Query("directions/v2/optimizedroute", jsonStr, success)
        stats.RoutingRequestCount += 1
 		Dim rx = Newtonsoft.Json.JsonConvert.DeserializeObject(Of MQResponse)(routeResp)
        If rx.info.statuscode = 0 Then
            Dim li As Integer = 0
            If rx IsNot Nothing Then
                Dim mqLocs = MQLocation.FromJson(rx)
                stats.RoutingParcelsCount += mqLocs.Count
                Dim seqText As String = String.Join(",",rx.route.locationSequence)
                Dim newSequence As New List(Of Integer)
                If seqText = "" Then
                    Return newSequence
                End If
                Dim seqArray = seqText.Split(",").Select(Function(x) CInt(x)).ToList
                If exitPoint IsNot Nothing Then
                    seqArray.RemoveAt(seqArray.Count - 1)
                End If
                For i As Integer = 1 To seqArray.Count - 1
                    Dim si = seqArray.IndexOf(i)
                    Dim mql = mqLocs(si)
                    Dim wi As Integer = 0
                    newSequence.Add(si)
                    'newSequence.Add(seqArray(i))
                    Dim wp = wayPoints(i - 1)

                    Debug.WriteLine(wp.StreetAddress + " " + mql.Street + " " + mql.City + " " + mql.State + " " + mql.Country)
                    'wp.StreetAddress = mql.Street
                    wp.ResolvedLocation = mql
                Next
                Return newSequence
            End If
        Else
        End If


        Dim index As Integer = 0
        For Each l In wayPoints
            index += 1
            sequence.Add(index)
        Next
        Return sequence
    End Function

    Public Class RequestStatistics
        Public Property GeoCodeAttemptedParcels As Integer = 0
        Public Property GeoCodeSucceededParcels As Integer = 0
        Public Property GeoCodeRequestCount As Integer = 0
        Public Property RoutingRequestCount As Integer = 0
        Public Property RoutingParcelsCount As Integer = 0

        'RoutingRequestCount = {2}, RoutingParcelsCount = {3}, GeoCodeRequestCount = {4}, GeoCodeAttemptedParcels = {5}, GeoCodeSucceededParcels = {6}
    End Class


    Public Shared Sub ListLocations(locations As List(Of Location))
        Dim i As Integer = 0
        For Each l In locations
            Debug.WriteLine(i.ToString + vbTab + l.StreetAddress)
            i += 1
        Next
    End Sub

    Public Shared Sub ListMQLocations(locations As List(Of MQLocation))
        Dim i As Integer = 0
        For Each l In locations
            Debug.WriteLine(i.ToString + vbTab + l.ToLongString)
            i += 1
        Next
    End Sub

    'Public Shared Sub ClearOldDBLog()
    '    CAMACloud.Data.Database.Tenant.Execute("DELETE FROM RoutingMQLog WHERE QueryTime < DATEADD(hour, -6, GETUTCDATE())")
    'End Sub

    'Public Shared Sub LogIntoDB(request As String, response As String)
    '    CAMACloud.Data.Database.Tenant.Execute("INSERT INTO RoutingMQLog (Request, Response) VALUES ({0}, {1})".SqlFormat(False, request, response))
    'End Sub
End Class
