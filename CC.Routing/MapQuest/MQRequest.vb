﻿	Public Class LatLng
        Public Property lat As String
        Public Property lng As String
    End Class

    Public Class Locs 
        Public Property latLng As LatLng
    End Class

    Public Class Options
        Public Property routeType As String
        Public Property narrativeType As String
        Public Property enhancedNarrative As String
        Public Property doReverseGeocode As String
    End Class

Public Class MQRequest
        Public Property locations As List(Of Locs)
        Public Property options As Options
    End Class
