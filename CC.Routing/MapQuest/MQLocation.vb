﻿Public Class MQLocation
    Public Property Street As String
    Public Property City As String
    Public Property County As String
    Public Property State As String
    Public Property ZIPCode As String
    Public Property Country As String

    Public Property Latitude As Double
    Public Property Longitude As Double
    Public Property Quality As String
    Public Property QualityCode As String

    Public Shared Function FromJson(resp As MQResponse) As List(Of MQLocation)
        Dim mqL As New List(Of MQLocation)
        'Dim lc As MQLocation
        For Each l In resp.route.locations
            'Dim locss As New Locs
            Dim ml As New MQLocation
            ml.Latitude = l.latLng.lat
            ml.Longitude = l.latLng.lng
            ml.Street = l.street
            ml.City = l.adminArea5
            ml.County = l.adminArea4
            ml.State = l.adminArea3
            ml.ZIPCode = l.postalCode
            ml.Country = l.adminArea1

            ml.Quality = l.geocodeQuality
            ml.QualityCode = l.geocodeQualityCode
            mqL.Add(ml)


        Next



        Return mqL
    End Function

    Public Function ToLongString() As String
        Return String.Format("{0,-10}{1,-40}{2,-15}{3,-5}", Quality, Street, City, State)
    End Function

    Public Function ToSqlUpdateString() As String
        Return "@StreetAddress = {0}, @City = {1}, @ZipCode = {2}, @Resolution = {3}, @Latitude = {4}, @Longitude = {5}, @QualityDescriptor = {6}".SqlFormat(True, Street, City, ZIPCode, Quality, Latitude, Longitude, QualityCode)
    End Function
End Class
