﻿    Public Class GeoInfo
        Public Property statuscode As Integer
        Public Property messages As Object()
    End Class

    Public Class GeoOptions
        Public Property maxResults As Integer
        Public Property thumbMaps As Boolean
        Public Property ignoreLatLngInput As Boolean
    End Class

    Public Class ProvidedLocation
        Public Property city As String
        Public Property state As String
    End Class

    Public Class GeoLatLng
        Public Property lat As Double
        Public Property lng As Double
    End Class

    Public Class GeoDisplayLatLng
        Public Property lat As Double
        Public Property lng As Double
    End Class

    Public Class GeoLocation
        Public Property street As String
        Public Property adminArea6 As String
        Public Property adminArea6Type As String
        Public Property adminArea5 As String
        Public Property adminArea5Type As String
        Public Property adminArea4 As String
        Public Property adminArea4Type As String
        Public Property adminArea3 As String
        Public Property adminArea3Type As String
        Public Property adminArea1 As String
        Public Property adminArea1Type As String
        Public Property postalCode As String
        Public Property geocodeQualityCode As String
        Public Property geocodeQuality As String
        Public Property dragPoint As Boolean
        Public Property sideOfStreet As String
        Public Property linkId As String
        Public Property unknownInput As String
        Public Property type As String
        Public Property latLng As GeoLatLng
        Public Property displayLatLng As GeoDisplayLatLng
        Public Property mapUrl As String
    End Class

    Public Class GeoResult
        Public Property providedLocation As ProvidedLocation
        Public Property locations As GeoLocation()
    End Class

    Public Class GeocodeResponse
        Public Property info As GeoInfo
        Public Property options As GeoOptions
        Public Property results As GeoResult()
    End Class
	