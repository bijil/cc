﻿'Public Class MQResponse
'
'
'  
'
'    Public Class LocationSequence
'        Inherits Dictionary(Of Integer, Integer)
'
'    End Class
'
'End Class



    Public Class Lr
        Public Property lng As Double
        Public Property lat As Double
    End Class

    Public Class Ul
        Public Property lng As Double
        Public Property lat As Double
    End Class

    Public Class BoundingBox
        Public Property lr As Lr
        Public Property ul As Ul
    End Class

    Public Class RouteError
        Public Property errorCode As Integer
        Public Property message As String
    End Class

    Public Class StartPoint
        Public Property lng As Double
        Public Property lat As Double
    End Class

    Public Class Sign
        Public Property extraText As String
        Public Property text As String
        Public Property type As Integer
        Public Property url As String
        Public Property direction As Integer
    End Class

    Public Class Maneuver
        Public Property distance As Double
        Public Property streets As String()
        Public Property narrative As String
        Public Property turnType As Integer
        Public Property startPoint As StartPoint
        Public Property index As Integer
        Public Property formattedTime As String
        Public Property directionName As String
        Public Property maneuverNotes As Object()
        Public Property linkIds As Object()
        Public Property signs As Sign()
        Public Property mapUrl As String
        Public Property transportMode As String
        Public Property attributes As Integer
        Public Property time As Integer
        Public Property iconUrl As String
        Public Property direction As Integer
    End Class

    Public Class Leg
        Public Property hasTollRoad As Boolean
        Public Property hasBridge As Boolean
        Public Property destNarrative As String
        Public Property distance As Double
        Public Property hasTimedRestriction As Boolean
        Public Property hasTunnel As Boolean
        Public Property hasHighway As Boolean
        Public Property index As Integer
        Public Property formattedTime As String
        Public Property origIndex As Integer
        Public Property hasAccessRestriction As Boolean
        Public Property hasSeasonalClosure As Boolean
        Public Property hasCountryCross As Boolean
        Public Property roadGradeStrategy As Object()()
        Public Property destIndex As Integer
        Public Property time As Integer
        Public Property hasUnpaved As Boolean
        Public Property origNarrative As String
        Public Property maneuvers As Maneuver()
        Public Property hasFerry As Boolean
    End Class

    Public Class Optionss
        Public Property arteryWeights As Object()
        Public Property cyclingRoadFactor As Integer
        Public Property timeType As Integer
        Public Property useTraffic As Boolean
        Public Property returnLinkDirections As Boolean
        Public Property countryBoundaryDisplay As Boolean
        Public Property enhancedNarrative As Boolean
        Public Property locale As String
        Public Property tryAvoidLinkIds As Object()
        Public Property drivingStyle As Integer
        Public Property doReverseGeocode As Boolean
        Public Property generalize As Integer
        Public Property mustAvoidLinkIds As Object()
        Public Property sideOfStreetDisplay As Boolean
        Public Property routeType As String
        Public Property avoidTimedConditions As Boolean
        Public Property routeNumber As Integer
        Public Property shapeFormat As String
        Public Property maxWalkingDistance As Integer
        Public Property destinationManeuverDisplay As Boolean
        Public Property transferPenalty As Integer
        Public Property narrativeType As String
        Public Property walkingSpeed As Integer
        Public Property urbanAvoidFactor As Integer
        Public Property stateBoundaryDisplay As Boolean
        Public Property unit As String
        Public Property highwayEfficiency As Integer
        Public Property maxLinkId As Integer
        Public Property maneuverPenalty As Integer
        Public Property avoidTripIds As Object()
        Public Property filterZoneFactor As Integer
        Public Property manmaps As String
    End Class

    Public Class DisplayLatLng
        Public Property lng As Double
        Public Property lat As Double
    End Class

    Public Class LatLngs
        Public Property lng As Double
        Public Property lat As Double
    End Class

    Public Class Locationss
        Public Property dragPoint As Boolean
        Public Property displayLatLng As DisplayLatLng
        Public Property adminArea4 As String
        Public Property adminArea5 As String
        Public Property postalCode As String
        Public Property adminArea1 As String
        Public Property adminArea3 As String
        Public Property type As String
        Public Property sideOfStreet As String
        Public Property geocodeQualityCode As String
        Public Property adminArea4Type As String
        Public Property linkId As Integer
        Public Property street As String
        Public Property adminArea5Type As String
        Public Property geocodeQuality As String
        Public Property adminArea1Type As String
        Public Property adminArea3Type As String
        Public Property latLng As LatLng
    End Class

    Public Class Route
        Public Property hasTollRoad As Boolean
        Public Property hasBridge As Boolean
        Public Property boundingBox As BoundingBox
        Public Property distance As Double
        Public Property hasTimedRestriction As Boolean
        Public Property hasTunnel As Boolean
        Public Property hasHighway As Boolean
        Public Property computedWaypoints As Object()
        Public Property routeError As RouteError
        Public Property formattedTime As String
        Public Property sessionId As String
        Public Property hasAccessRestriction As Boolean
        Public Property realTime As Integer
        Public Property hasSeasonalClosure As Boolean
        Public Property hasCountryCross As Boolean
        Public Property fuelUsed As Double
        Public Property legs As Leg()
        Public Property options As Options
	    Public Property locations As Locationss()
	    Public Property time As Integer
	    Public Property hasUnpaved As Boolean
	    Public Property locationSequence As Integer()
        Public Property hasFerry As Boolean
    End Class

    Public Class Info
        Public Property statuscode As Integer
    End Class

    Public Class MQResponse
        Public Property route As Route
        Public Property info As Info
    End Class

