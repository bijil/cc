﻿Public Class GeocodeRequest
	Public Property locations As List(Of String)
	Public Property options As GeoOptions
		
	Public Shared Function toGeoLocationList1(ByRef missingLocs As List(Of Location)) As List(Of String)
		Dim GLocationList As New List(Of String)
		For Each l In missingLocs
				Dim GLocs As String = New String("")
	            If l.HasFullAddress Then
	            	GLocs = l.StreetAddress
	        	ElseIf l.HasAddress Then
	            	GLocs = l.StreetNumber + " " + l.StreetName
	        	End If
	        	If IsNotEmpty(l.City) Then
	        		GLocs = GLocs + "," + l.City
		        End If
		        If IsNotEmpty(l.County) Then
		        	GLocs = GLocs + "," + l.County
		        End If
		        If IsNotEmpty(l.State) Then
		            GLocs = GLocs + "," + l.State
		        End If
		        If IsNotEmpty(l.Country) Then
		            GLocs = GLocs + "," + l.Country
		        End If
		       	GLocationList.Add(GLocs)
		Next
		Return GLocationList
	End Function	
		
		
		
		
		
		
	Public Shared Function toGeoLocationList(ByRef missingLocs As List(Of Location)) As List(Of GeoRequestLocation)
		Dim GLocationList As New List(Of GeoRequestLocation)
		For Each l In missingLocs
				Dim GLoc As New GeoRequestLocation
				If l.HasGIS Then
		            Dim GLatlng As New LatLng
		            GLatlng.lat = l.Latitude.ToString
		            GLatlng.lng = l.Longitude.ToString
		            GLoc.Latlng = GLatlng
		            GLocationList.Add(GLoc)
		            Continue For
		        End If
	            If l.HasFullAddress Then
	            	GLoc.Street = l.StreetAddress
	        	ElseIf l.HasAddress Then
	            	GLoc.Street = l.StreetNumber + " " + l.StreetName
	        	End If
	        	If IsNotEmpty(l.City) Then
	        		GLoc.adminArea5Type = "City"
		            GLoc.adminArea5 = l.City
		        End If
		        If IsNotEmpty(l.County) Then
		        	GLoc.adminArea4Type = "County"
		        	GLoc.adminArea4= l.County
		        End If
		        If IsNotEmpty(l.State) Then
		        	GLoc.adminArea3Type = "State"
		            GLoc.adminArea3= l.State
		        End If
		        If IsNotEmpty(l.ZIPCode) Then
		            GLoc.postalCode= l.ZIPCode
		        End If
		        If IsNotEmpty(l.Country) Then
		        	GLoc.adminArea1Type = "Country"
		            GLoc.adminArea1= l.Country
		        End If
		       	GLocationList.Add(GLoc)
		Next
		Return GLocationList
	End Function		
End Class
Public Class GeoRequestLocation
	Public Property latlng As LatLng
    Public Property street As string
    Public Property adminArea6 As String
    Public Property adminArea6Type As String
    Public Property adminArea5 As String
    Public Property adminArea5Type As String
    Public Property adminArea4 As String
    Public Property adminArea4Type As String
    Public Property adminArea3 As String
    Public Property adminArea3Type As String
    Public Property adminArea1 As String
    Public Property adminArea1Type As String
    Public Property postalCode As String
End Class

