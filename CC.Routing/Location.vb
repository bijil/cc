﻿Imports System.Reflection
Imports System.Text.RegularExpressions


Public Class Location

    Public Property Id As Integer
    Public Property KeyValue1 As String
    Public Property StreetAddress As String
    Public Property StreetNumber As String
    Public Property StreetName As String
    Public Property City As String
    Public Property County As String
    Public Property State As String
    Public Property ZIPCode As String
    Public Property Country As String

    Public Property Latitude As Decimal
    Public Property Longitude As Decimal
    Public Property IsLargeParcel As Boolean

    Public Property DistanceFromMe As Integer
    Public Property DistanceIndex As Integer = 9999
    Public Property BlockIndex As Integer
    Public Property HasNewGIS As Boolean
    Public Property GeoCodePointFailed As Boolean
    Public ReadOnly Property HasFullAddress As Boolean
        Get
            If IsNotEmpty(StreetAddress) AndAlso Regex.IsMatch(StreetAddress, "^[1-9][0-9]*") Then
                Return True
            End If
            Return False
        End Get
    End Property

    Public ReadOnly Property HasAddress As Boolean
        Get
            If IsNumeric(StreetNumber) AndAlso IsNotEmpty(StreetName) AndAlso IsNotEmpty(ZIPCode) Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property

    Public ReadOnly Property HasGIS As Boolean
        Get
            If Latitude = 0 And Longitude = 0 Then
                Return False
            Else
                Return True
            End If
        End Get
    End Property

    Public ReadOnly Property IsCertain As Boolean
        Get
            Return HasGIS OrElse HasFullAddress OrElse HasAddress
        End Get
    End Property

    Public ReadOnly Property IsBad As Boolean
        Get
            Return Not IsCertain
        End Get
    End Property

    Public Property IsGCResolved As Boolean
    Public Property ResolvedLocation As MQLocation

    Public Sub New()

    End Sub
    Public Sub New(dr As DataRow)
        Dim propNames = Me.GetType.GetProperties().Where(Function(x) x.CanWrite)
        For Each dc As DataColumn In dr.Table.Columns
            Dim prop = propNames.Where(Function(x) x.Name.ToLower = dc.ColumnName.ToLower).First
            If prop IsNot Nothing Then
                prop.SetValue(Me, dr(prop.Name))
            End If
        Next
    End Sub

    Public Shared Function GetListFromTable(dt As DataTable) As List(Of Location)
        Dim writableProperties As New List(Of PropertyInfo)
        Dim propNames = (New Location).GetType.GetProperties().Where(Function(x) x.CanWrite)
        For Each dc As DataColumn In dt.Columns
            Dim prop = propNames.Where(Function(x) x.Name.ToLower = dc.ColumnName.ToLower).FirstOrDefault
            If prop IsNot Nothing Then
                writableProperties.Add(prop)
            End If
        Next

        Dim li As New List(Of Location)
        For Each dr In dt.Rows
            Dim p As New Location
            For Each prop In writableProperties
                prop.SetValue(p, If(dr(prop.Name) Is DBNull.Value, Nothing, dr(prop.Name)))
            Next
            li.Add(p)
        Next
        Return li
    End Function

    Public Function GetBlock(level As Integer, Optional altLng As Boolean = False, Optional lngScale As Integer = 1) As MapBlock
        If altLng Then
            Return New MapBlock() With {.Latitude = Math.Round(Me.Latitude, level), .Longitude = Math.Round(Me.Longitude, level + lngScale)}
        Else
            Return New MapBlock() With {.Latitude = Math.Round(Me.Latitude, level), .Longitude = Math.Round(Me.Longitude, level)}
        End If
    End Function

    Private Function IsNotEmpty(str As String)
        Return Not String.IsNullOrWhiteSpace(str)
    End Function

    Public Function ToLocationXML() As XElement
        Dim xloc = <location></location>
        If HasGIS Then
            Dim latLng = <latLng><lat></lat><lng></lng></latLng>
            latLng...<lat>.Value = Latitude
            latLng...<lng>.Value = Longitude
            xloc.Add(latLng)
            Return xloc
        End If
        Dim street = <street></street>
        If HasFullAddress Then
            street.Value = StreetAddress
        ElseIf HasAddress Then
            street.Value = StreetNumber + " " + StreetName
        End If
        xloc.Add(street)
        If IsNotEmpty(City) Then
            Dim xCity = <adminArea5 type="City"></adminArea5>
            xCity.Value = City
            xloc.Add(xCity)
        End If
        If IsNotEmpty(County) Then
            Dim xCounty = <adminArea4 type="County"></adminArea4>
            xCounty.Value = County
            xloc.Add(xCounty)
        End If
        If IsNotEmpty(State) Then
            Dim xState = <adminArea3 type="State"></adminArea3>
            xState.Value = State
            xloc.Add(xState)
        End If
        Dim xCountry = <country></country>
        xCountry.Value = Country
        xloc.Add(xCountry)
        Return xloc
    End Function

    Public Shared Function ToLocationListXML(locs As List(Of Location)) As XElement
        Dim xlocs = <locations></locations>
        For Each p In locs
            xlocs.Add(p.ToLocationXML)
        Next
        Return xlocs
    End Function

    Public Function ToAddressString() As String
        If IsCertain Then
            If HasFullAddress Then
                Return StreetAddress + ", " + State + ", " + Country
            ElseIf HasAddress Then
                Return StreetNumber & " " & StreetName & ", " + +State + ", " + Country
            ElseIf HasGIS Then
                Return Latitude & "," & Longitude
            End If
        End If
        Return ""
    End Function

End Class

Public Class MapBlock
    Public ReadOnly Property Code As String
        Get
            Return (Latitude.ToString("00.000000") + If(Math.Sign(Longitude) < 0, "-", "+") + Math.Abs(Longitude).ToString("000.000000")).Replace(".", "")
        End Get
    End Property
    Public Property Latitude As Single
    Public Property Longitude As Single
    Public Property Index As Integer
    Public ReadOnly Property ParcelCount As Integer
        Get
            Return Locations.Count
        End Get
    End Property
    Public Property Locations As New List(Of Location)

    Public Function ToLocation() As Location
        Return New Location With {.Latitude = Latitude, .Longitude = Longitude, .KeyValue1 = Code, .DistanceIndex = Index}
    End Function

    Sub SetIndex(i As Integer)
        Index = i
        For Each l In Locations
            l.BlockIndex = i
        Next
    End Sub

End Class