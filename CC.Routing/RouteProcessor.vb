﻿Imports CAMACloud.Data
Imports System.Net
Imports System.Text.Encoding
Imports Newtonsoft.Json
Public Class RouteProcessor

    Public Const MAPQUEST_KEY = "TbrubRuJmy19ytFhyA1iGbEpg9c9P3A0"
    Public Const MAX_BATCH_SIZE As Integer = 24
    Public Shared LastBlocks As List(Of MapBlock)


    Public Shared Function CalculateRoute(loginId As String, db As Database, referenceLocation As Location, parcelIds() As Integer, Optional routeType As Integer = 0) As RouteResponse
        Dim stats As New MapQuest.RequestStatistics

        Dim parcelCSV = String.Join(",", parcelIds)

        'parcelCSV = "16086,16124,16125,16322,16326,16327,16328,16330,16334,16383,16385,16387,16389,16390,16400,16402,16418,16419,16429,16430,16433,16435,16438,16443,16444,16360,16361,16364,16372,16375,16377,16450,16452,16453,16454,16457,16459,16463,16466,16468,16472,16473,17977,17981,17984,17985,17987,17992,17993,17994,17998,18000,18001,18002,18003,18004,18006,18007,18008,18010,18011,18018,18019,18021,18026,18028,18029,18031,18035,18036,18038,18039,18137,18165,18170,18201,18206,18208,18209,18877,18879,18880,18881"
        Dim parcelData = db.GetDataTable("EXEC gis_GetParcels @ParcelCSV = " + parcelCSV.ToSqlValue)

        Dim jobId As Integer = db.GetIntegerValue("INSERT INTO RoutingJob (CreatedBy, ParcelCount, CurrentLatitude, CurrentLongitude) VALUES ({0}, {1}, {2}, {3}); SELECT CAST(@@IDENTITY AS INT) As NewId;".SqlFormat(False, loginId, parcelIds.Length, referenceLocation.Latitude, referenceLocation.Longitude))

        Dim resp As New RouteResponse
        Dim processStartTime = DateTime.Now
        Dim refLocation = referenceLocation

        'Get StreetAddress, Latitude, Longitude, and other properties of the parcels
        Dim parcels As List(Of Location) = Location.GetListFromTable(parcelData)
        For Each p In parcels
            p.County = refLocation.County
            p.State = refLocation.State
            p.Country = refLocation.Country
        Next

        Dim sqlInsert As String = ""
        For Each p In parcels
            sqlInsert += "INSERT INTO RoutingJobData (JobId, ParcelId) VALUES ({0}, {1});".SqlFormat(False, jobId, p.Id)
        Next
        db.Execute(sqlInsert)
        'For Each p In parcels
        '    p.Latitude = 0
        '    p.Longitude = 0
        'Next

        Dim badParcels = parcels.Where(Function(x) x.IsBad).ToList
        parcels = parcels.Where(Function(x) x.IsCertain).ToList
        Dim missingGisParcels = parcels.Where(Function(x) Not x.HasGIS AndAlso (x.HasAddress Or x.HasFullAddress)).ToList
        GeoCodeGISMissingLocations(stats, missingGisParcels)

        'Compute basic properties - distance from reference
        For Each p In parcels
            p.DistanceFromMe = GeoUtilities.GetDistanceBetweenPoints(refLocation.Latitude, refLocation.Longitude, p.Latitude, p.Longitude)
        Next

        parcels = parcels.OrderBy(Function(x) x.DistanceFromMe).ToList

        If parcels.Count < MAX_BATCH_SIZE Then
            Dim sequence = MapQuest.ProcessRoute(stats, refLocation, parcels, Nothing, routeType)
            If (sequence.Count > 0) Then
                For i As Integer = 0 To parcels.Count - 1
                    parcels(i).DistanceIndex = sequence(i)
                Next
            End If

        Else
            Dim blocks = ComputeAndSortBlocks(stats, refLocation, parcels, 2, 0, 0, routeType)
            Dim startLoc = refLocation
            Dim maxSequence = -1

            'For bi As Integer = 0 To blocks.Count - 1
            '    Dim b = blocks(bi)
            '    Dim sqlInsertBlock As String = "INSERT INTO RoutingBlocks (JobId, Latitude, Longitude, BlockInfo, DistanceIndex, Parcels) VALUES ({0}, {1}, {2}, {3}, {4}, {5});".SqlFormat(False, jobId, b.Latitude, b.Longitude, b.Locations.Count & " parcels", b.Index, b.Locations.Count)
            '    Database.Tenant.Execute(sqlInsertBlock)
            'Next

            'SaveBlockRouteKML(blocks)

            'For Each b In blocks
            '    Console.WriteLine("{0,-5}{1}", b.Index, b.Code)
            'Next
            For bi As Integer = 0 To blocks.Count - 1
                Dim b = blocks(bi)
                Dim nextBlock As Location = Nothing
                If bi < blocks.Count - 1 Then
                    nextBlock = blocks(bi + 1).ToLocation
                End If
                maxSequence += 1
                Dim sequence = MapQuest.ProcessRoute(stats, startLoc, b.Locations, nextBlock, routeType)
                For i As Integer = 0 To b.Locations.Count - 1
                    b.Locations(i).DistanceIndex = maxSequence + sequence(i)
                Next
                maxSequence = b.Locations.Select(Function(x) x.DistanceIndex).Max
                Dim lastLoc = b.Locations.Where(Function(x) x.DistanceIndex = maxSequence).First
                startLoc = lastLoc
            Next
        End If

        For Each bp In badParcels
            parcels.Add(bp)
        Next

        Dim elapsedTime = (Date.Now - processStartTime).TotalSeconds


        Dim orderedParcels = parcels.OrderBy(Function(x) x.DistanceIndex).ToList

        resp.locationSequence = orderedParcels.ToDictionary(Function(x) x.Id, Function(y) y.DistanceIndex)
        resp.processingTime = elapsedTime
        resp.success = True
        resp.locations = orderedParcels

        Dim sqlUpdate As String = ""
        For Each ls In resp.locationSequence
            sqlUpdate += ("UPDATE RoutingJobData SET DistanceIndex = {2} WHERE JobID = {0} AND ParcelId = {1};".SqlFormatString(jobId, ls.Key, ls.Value))
        Next
        db.Execute(sqlUpdate)

        db.Execute("UPDATE RoutingJob SET CompletedTime = GETUTCDATE(), IgnoredCount = {1}, RoutingRequestCount = {2}, RoutingParcelsCount = {3}, GeoCodeRequestCount = {4}, GeoCodeAttemptedParcels = {5}, GeoCodeSucceededParcels = {6}  WHERE Id = {0};".SqlFormat(False, jobId, badParcels.Count, stats.RoutingRequestCount, stats.RoutingParcelsCount, stats.GeoCodeRequestCount, stats.GeoCodeAttemptedParcels, stats.GeoCodeSucceededParcels))


        Dim newResolvedParcels = resp.locations.Where(Function(x) Not x.IsGCResolved And (x.ResolvedLocation IsNot Nothing AndAlso (x.ResolvedLocation.Quality = "POINT" Or x.ResolvedLocation.Quality = "ADDRESS")))
        For i As Integer = 0 To newResolvedParcels.Count - 1 Step 100
            Dim updateSql = ""
            newResolvedParcels.Skip(i).Take(100).ToList.ForEach(Sub(x)
                                                                    updateSql += "EXEC gis_UpdateGCResolution @ParcelId = " & x.Id & ", " + x.ResolvedLocation.ToSqlUpdateString + ";" + vbNewLine
                                                                End Sub)
            db.Execute(updateSql)
        Next

        'Console.WriteLine("Time elasped: {0}", elapsedTime)
        Return resp
    End Function

    'Private Shared Function GetParcelList(parcelIds As List(Of Integer)) As List(Of Location)
    '    Throw New NotImplementedException
    'End Function
    Private Shared Sub GeoCodeGISMissingLocations(ByRef stats As MapQuest.RequestStatistics, places As List(Of Location))
        stats.GeoCodeAttemptedParcels += places.Count
        Dim successCount As Integer = 0
        For i As Integer = 0 To places.Count - 1 Step 100
            Dim batch = places.Skip(i).Take(100).ToList
            'Console.WriteLine("Gettings GIS locations for parcels {0} - {1}", i + 1, i + batch.Count)
            Dim gRequest As New GeocodeRequest
            Dim gOptions As New GeoOptions
            gRequest.locations =  GeocodeRequest.toGeoLocationList1(batch)
            'gOptions.ignoreLatLngInput = True
            gOptions.thumbMaps = False
            gOptions.maxResults = -1
            gRequest.options = gOptions
            Dim gRequestJson As String = JsonConvert.SerializeObject(gRequest)
            Dim gResult = MapQuest.Query("geocoding/v1/batch", gRequestJson)
            Dim gRespCollection = Newtonsoft.Json.JsonConvert.DeserializeObject(Of GeocodeResponse)(gResult)
            If gRespCollection.info.statuscode <> 0 Then
            	Exit For
            End If
            stats.GeoCodeRequestCount += 1
            Dim pi As Integer = 0
            For Each result In gRespCollection.results
            	Dim geoResp = New GeoResult
            	geoResp = result
            	Dim pa = batch(pi)
            	For Each respLocation In geoResp.locations
            		pa.Latitude = respLocation.latLng.lat
	                pa.Longitude = respLocation.latLng.lng
            		If respLocation.geocodeQuality = "POINT"  And respLocation.adminArea3 = pa.State And respLocation.adminArea4 = pa.County  Then
            			pa.HasNewGIS = True
            			successCount += 1
            			Exit For
					Else
	                    pa.GeoCodePointFailed = True  
            		End If
            	Next
            	pi += 1
            Next
         Next   
        stats.GeoCodeSucceededParcels += successCount
    End Sub


    Private Shared Function ComputeAndSortBlocks(ByRef stats As MapQuest.RequestStatistics, refLocation As Location, ByRef wayPoints As List(Of Location), Optional startingBlockSize As Integer = 2, Optional inputBlockCount As Integer = 0, Optional startIndex As Integer = 0, Optional routeType As Integer = 0) As List(Of MapBlock)
        Debug.Indent()
        Debug.WriteLine("Computing route for block with " & wayPoints.Count & " points")
        LOG_ClearOld()

        Dim blocks As New Dictionary(Of String, MapBlock)
        Dim blockOptimized As Boolean = False
        Dim blockSize As Integer = startingBlockSize
        Dim useAltLng As Boolean = False
        Dim lngScale As Integer = 1
        While Not blockOptimized
            blocks.Clear()
            For Each p In wayPoints
                Dim blk = p.GetBlock(blockSize, useAltLng, lngScale)
                If Not blocks.ContainsKey(blk.Code) Then
                    blocks.Add(blk.Code, blk)
                Else
                    blk = blocks(blk.Code)
                End If
                blk.Locations.Add(p)
            Next
            If inputBlockCount > 0 And blocks.Count = inputBlockCount Then
                If useAltLng Then
                    If (lngScale > 10) Then lngScale = 10
                    lngScale += 1
                Else
                    useAltLng = True
                    blockSize += 1
                End If

            ElseIf blockSize = 0 OrElse blocks.Count < MAX_BATCH_SIZE Then
                blockOptimized = True
            Else
                blockSize -= 1

            End If
        End While

        Dim blockList = blocks.Values.ToList
        renumberBlocks(blockList)

        displayBlockStats("Initial", blockList)
        'For Each b In blockList
        '    b.Locations.Clear()
        'Next
        'For Each p In wayPoints
        '    Dim pblk = p.GetBlock(blockSize, useAltLng, lngScale)
        '    Dim rblk = blockList.Where(Function(x) x.Code = pblk.Code).First
        '    If rblk IsNot Nothing Then
        '        p.BlockIndex = rblk.Index
        '        rblk.Locations.Add(p)
        '    End If
        'Next


        SortBlocks(stats, blockList, refLocation, startIndex, routeType)
        renumberBlocks(blockList)

        displayBlockStats("Sorted", blockList)
        'Dim index As Integer = 0
        'For Each blk In blockList
        '    index += 1
        '    blk.Index = index
        'Next


        While blockList.Count > 0
            Dim mergeFound As Boolean = False
            Dim lastRefLocation As Location = refLocation
            For i = 0 To blockList.Count - 1
                Dim b = blockList(i)

                'Debug.WriteLine(vbTab + vbTab + "Evaluating block size {0}", b.Locations.Count)
                If i < blockList.Count - 1 Then
                    Dim nb = blockList(i + 1)

                    If b.ParcelCount < MAX_BATCH_SIZE AndAlso b.ParcelCount + nb.ParcelCount < MAX_BATCH_SIZE Then
                        'Debug.Print("This: {0}, Next: {1}", b.ParcelCount, nb.ParcelCount)
                        Dim changeIndex = nb.Index
                        For Each p In wayPoints
                            If nb.Locations.Contains(p) Then
                                'p.BlockIndex = b.Index
                                nb.Locations.Remove(p)
                                b.Locations.Add(p)
                            End If

                        Next
                        nb.Index = b.Index
                        blockList.RemoveAt(i + 1)

                        LOG_InsertDBLog("---", String.Join(",", String.Format("Block merged and removed. New count: {0}. This block size: {1}", blockList.Count, b.ParcelCount)))
                        'Debug.Print("Block merged and removed. New count: {0}. This block size: {1}", blockList.Count, b.ParcelCount)
                        mergeFound = True
                        Exit For
                    End If
                End If

                If b.ParcelCount >= MAX_BATCH_SIZE Then
                    If i > 0 Then
                        lastRefLocation = New Location With {.Latitude = blockList(i - 1).Latitude, .Longitude = blockList(i - 1).Longitude, .State = refLocation.State, .Country = refLocation.Country}
                    End If
                    'Debug.WriteLine("Old locations: {0} [{1}]", blockList.Select(Function(x) x.ParcelCount).Sum(), blockList.Select(Function(x) x.ParcelCount.ToString).Aggregate(Function(x, y) x + ", " + y))
                    Dim subBlocks = ComputeAndSortBlocks(stats, lastRefLocation, b.Locations, startingBlockSize, 1, b.Index, routeType)
                    'Debug.Unindent()
                    'Debug.Print("Sub-blocks: [{0}]", subBlocks.Select(Function(x) x.ParcelCount.ToString).Aggregate(Function(x, y) x + ", " + y))
                    'Debug.Print("Removing element: {0}", i)
                    blockList.Remove(b)
                    For j As Integer = 0 To subBlocks.Count - 1
                        Dim sb = subBlocks(j)
                        blockList.Insert(i + j, sb)


                        LOG_InsertDBLog("---", String.Format("Block of size {1} inserted at {0}", i + j, sb.ParcelCount))
                        'Debug.Print("Block of size {1} inserted at {0}", i + j, sb.ParcelCount)
                        sb.SetIndex(i + j)
                    Next
                    For j As Integer = i + subBlocks.Count To blockList.Count - 1
                        Dim bl = blockList(j)
                        bl.SetIndex(j)
                    Next

                    'Dim newb = New MapBlock With {.Latitude = b.Latitude, .Longitude = b.Longitude}
                    'For j As Integer = blockList.Count - 1 To i + 1 Step -1
                    '    Dim cb = blockList(j)
                    '    For Each p In wayPoints
                    '        If p.BlockIndex = cb.Index Then
                    '            p.BlockIndex = cb.Index + 1
                    '        End If
                    '    Next
                    '    cb.Index = cb.Index + 1
                    'Next
                    'newb.Index = b.Index + 1
                    'blockList.Insert(blockList.IndexOf(b) + 1, newb)

                    'Dim fillLevel = 0
                    'For Each p In wayPoints
                    '    If p.BlockIndex = b.Index Then
                    '        fillLevel += 1
                    '        If fillLevel >= MAX_BATCH_SIZE Then
                    '            p.BlockIndex = newb.Index
                    '            newb.ParcelCount += 1
                    '            b.ParcelCount -= 1
                    '        End If
                    '    End If
                    'Next



                    mergeFound = True
                    Exit For
                End If
            Next
            If Not mergeFound Then
                Exit While
            End If

            'Debug.WriteLine("New locations: {0} [{1}]", blockList.Select(Function(x) x.ParcelCount).Sum(), blockList.Select(Function(x) x.ParcelCount.ToString).Aggregate(Function(x, y) x + ", " + y))
        End While


        'For Each b In blockList
        '    b.Locations.Clear()
        'Next
        'For Each p In wayPoints
        '    For Each b In blockList
        '        If b.Index = p.BlockIndex Then
        '            b.Locations.Add(p)
        '        End If
        '    Next
        'Next

        renumberBlocks(blockList)
        displayBlockStats("Optimized", blockList)

        LastBlocks = blockList

        Debug.Unindent()
        Return blockList
        'wayPoints.Select()
    End Function

    Private Shared Sub SortBlocks(ByRef stats As MapQuest.RequestStatistics, ByRef blockList As List(Of MapBlock), refLocation As Location, startIndex As Integer, Optional routeType As Integer = 0)
        Dim blockLoc = blockList.Select(Function(x) x.ToLocation).ToList
        For Each p In blockLoc
            p.DistanceFromMe = GeoUtilities.GetDistanceBetweenPoints(refLocation.Latitude, refLocation.Longitude, p.Latitude, p.Longitude)
        Next
        'blockLoc = blockLoc.OrderBy(Function(x) x.DistanceFromMe).ToList

        Dim sequence = MapQuest.ProcessRoute(stats, refLocation, blockLoc, Nothing, routeType).ToArray

        LOG_InsertDBLog("---", String.Join(",", sequence))

        For i As Integer = 0 To blockLoc.Count - 1
            Dim si = sequence(i) - 1
            'blockList(si).SetIndex(startIndex + i)
            blockList(i).SetIndex(startIndex + sequence(i) - 1)
        Next

        blockList = blockList.OrderBy(Function(x) x.Index).ToList
    End Sub

    Private Shared Sub renumberBlocks(ByRef blocks As List(Of MapBlock))
        Dim bnum As Integer = 0
        For Each b In blocks
            b.SetIndex(bnum)
            bnum += 1
        Next
    End Sub

    Private Shared Sub SaveBlockRouteKML(locations As List(Of MapBlock))
        Dim kml = <kml>
                      <Document>

                      </Document>
                  </kml>

        Dim routeLine = <Placemark>
                            <name>Routing Test</name>
                            <LineString>
                                <coordinates></coordinates>
                            </LineString>
                        </Placemark>

        Dim lineCoordinates = ""

        For Each p In locations.OrderBy(Function(x) x.Index).Select(Function(x) x.ToLocation)
            'Console.WriteLine(p.StreetAddress)
            Dim coord = p.Longitude & "," & p.Latitude & ",0"
            lineCoordinates += coord + vbNewLine

            Dim kp = <Placemark><name></name><Point><coordinates></coordinates></Point><address>123 N Mada Street</address><description></description></Placemark>
            kp...<name>.Value = p.DistanceIndex ' & " - " & p.ToAddressString
            kp...<Point>...<coordinates>.Value = coord
            kp...<address>.Value = p.ToAddressString
            kp...<description>.Value = p.ToAddressString

            kml...<Document>.First.Add(kp)
        Next
        routeLine...<LineString>...<coordinates>.Value = lineCoordinates
        kml...<Document>.First.Add(routeLine)
        Dim kmlPath As String = "D:\Cloud Storage\OneDrive\Desktop\block.kml"
        IO.File.WriteAllText(kmlPath, kml.ToString)
        Process.Start(kmlPath)
    End Sub

    Private Shared Sub displayBlockStats(status As String, blockList As List(Of MapBlock))
        Debug.WriteLine(status + " Blocks: {0}, Locations: {1} [{2}]", blockList.Count, blockList.Select(Function(x) x.ParcelCount).Sum(), blockList.Select(Function(x) x.ParcelCount.ToString).Aggregate(Function(x, y) x + ", " + y))
        Dim testPositions As String = ""
        For Each b In blockList
            For Each p In b.Locations
                If p.Id = 12971 Then
                    testPositions += "P12971: " & b.Index & " "
                End If
                If p.Id = 12935 Then
                    testPositions += "P12935: " & b.Index & " "
                End If
            Next
        Next
        Debug.WriteLineIf(testPositions <> "", testPositions)
    End Sub

    Public Shared Sub LOG_ClearOld()
        'Database.Tenant.Execute("DELETE FROM RoutingMQLog WHERE QueryTime < DATEADD(hour, -6, GETUTCDATE())")
    End Sub

    Public Shared Sub LOG_InsertDBLog(request As String, response As String)
        'Database.Tenant.Execute("INSERT INTO RoutingMQLog (Request, Response) VALUES ({0}, {1})".SqlFormat(False, request, response))
    End Sub

    Public Class RouteResponse
        Public Property success As Boolean
        Public Property errorMessages As List(Of String)
        Public Property locationSequence As Dictionary(Of Integer, Integer)
        Public Property processingTime As Single

        Public Property locations As List(Of Location)

        Public Sub New()
            errorMessages = New List(Of String)
            locationSequence = New Dictionary(Of Integer, Integer)
            processingTime = -1

        End Sub
    End Class
End Class


