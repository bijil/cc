﻿Public Class GeoUtilities

    Public Shared Function GetDistanceBetweenPoints(lat1 As Double, long1 As Double, lat2 As Double, long2 As Double) As Double
        Dim distance As Double = 0

        Dim dLat As Double = (lat2 - lat1) / 180 * Math.PI
        Dim dLong As Double = (long2 - long1) / 180 * Math.PI

        Dim a As Double = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) + Math.Cos(lat2) * Math.Sin(dLong / 2) * Math.Sin(dLong / 2)
        Dim c As Double = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a))

        'Calculate radius of earth
        ' For this you can assume any of the two points.
        Dim radiusE As Double = 6378135
        ' Equatorial radius, in metres
        Dim radiusP As Double = 6356750
        ' Polar Radius
        Dim rLat1 As Double = lat1 / 180 * Math.PI
        Dim nr As Double = Math.Pow(radiusE * radiusE * Math.Cos(rLat1), 2) + Math.Pow(radiusP * radiusP * Math.Sin(rLat1), 2)
        Dim dr As Double = Math.Pow(radiusE * Math.Cos(rLat1), 2) + Math.Pow(radiusP * Math.Sin(rLat1), 2)
        Dim radius As Double = Math.Sqrt(nr / dr)

        If Double.IsInfinity(c) Or Double.IsNaN(c) Then
            Return 999999
        End If

        'Calaculate distance in metres.
        distance = radius * c
        Return distance
    End Function


End Class
