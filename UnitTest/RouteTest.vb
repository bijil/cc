﻿Imports CAMACloud
Imports CAMACloud.Routing
Imports CAMACloud.Data
Imports System.Runtime.Serialization.Json
Module RouteTest

    Sub Main()

        Dim outputPath As String = IO.Path.GetFullPath(IO.Path.Combine(My.Application.Info.DirectoryPath, "..\..\..\CAMACloud.MobileAssessor\test\"))

        Dim jsonFile As String = IO.Path.Combine(outputPath, "parcels.json")


        Dim db As New Database("Server=.;Integrated Security=True;Database=CAMACLOUD;")
        Dim parcels = db.GetDataTable("SELECT * FROM test_route").AsEnumerable.Select(
            Function(x) New Parcel With {
                .pid = x("ParcelId"), .street = x("StreetAddress"), .latitude = x.Get("Latitude"), .longitude = x.Get("Longitude")
             }
        ).ToList

        'Dim locs = Location.GetListFromTable(ptable)
        Dim ptable() As Integer = parcels.Select(Function(x) x.pid).ToArray
        Console.WriteLine("Routing " & ptable.Count & " parcels.")

        '45.4, -63.0
        '39.9675344,-82.9908442
        'Dim currentLocation = New Location With {.Latitude = 45.4, .Longitude = -63.0, .State = "Nova Scotia", .County = "Truro", .Country = "Canada"}
        Dim currentLocation = New Location With {.Latitude = 45.2654744, .Longitude = -63.2818688, .City = "Truro", .State = "Nova Scotia", .Country = "Canada"}
        'Dim currentLocation = New Location With {.Latitude = 39.9675344, .Longitude = -82.9908442, .County = "NS", .Country = "Canada"}
        Dim routes = RouteProcessor.CalculateRoute("unit_tester", db, currentLocation, ptable)

        For Each lk In routes.locationSequence.Keys
            Dim li = routes.locationSequence(lk)
            Console.WriteLine("{0}  {1}", lk, li)
            parcels.Where(Function(x) x.pid = lk).First.o = li
        Next

        For Each parcel In parcels.OrderBy(Function(x) x.o).ToList
            Console.WriteLine(parcel.street)
        Next

        Dim ms As New IO.MemoryStream
        Dim ser As New DataContractJsonSerializer(GetType(List(Of Parcel)))
        ser.WriteObject(ms, parcels)
        Dim json = System.Text.Encoding.ASCII.GetString(ms.ToArray)
        IO.File.WriteAllText(jsonFile, json)
        'SaveResultAsKml(routes)

        'Dim addressData As String = ""
        'For Each p In routes.locations
        '    If p.IsCertain Then
        '        addressData += p.ToAddressString + vbNewLine
        '    End If
        'Next
        'IO.File.WriteAllText("D:\Cloud Storage\OneDrive\Desktop\route.txt", addressData)

        '
        'For Each rk In routes.Keys
        'LogTable({-8, 4}, rk, routes(rk))
        'Next

        'Dim locs = Location.ToLocationListXML(Location.GetListFromTable(ptable))
        'For Each l In locs
        '    Log(l.ToLocationXML)
        'Next
        'Log(locs.ToString)
        'If RouteProcessor.LastBlocks IsNot Nothing Then
        '    For Each b In RouteProcessor.LastBlocks
        '        LogTable({-14, 7, 7}, b.Code, b.Index, b.ParcelCount)
        '    Next
        'End If

        Console.ReadKey(True)
        Exit Sub
    End Sub

    Sub SaveResultAsKml(routes As RouteProcessor.RouteResponse)


        Dim kml = <kml>
                      <Document>

                      </Document>
                  </kml>

        Dim routeLine = <Placemark>
                            <name>Routing Test</name>
                            <LineString>
                                <coordinates></coordinates>
                            </LineString>
                        </Placemark>

        Dim lineCoordinates = ""

        For Each p In routes.locations.Where(Function(x) x.HasGIS).OrderBy(Function(x) x.DistanceIndex)
            'Console.WriteLine(p.StreetAddress)
            Dim coord = p.Longitude & "," & p.Latitude & ",0"
            lineCoordinates += coord + vbNewLine

            Dim kp = <Placemark><name></name><Point><coordinates></coordinates></Point><address>123 N Mada Street</address><description></description></Placemark>
            kp...<name>.Value = p.BlockIndex & "/" & p.DistanceIndex ' & " - " & p.ToAddressString
            kp...<Point>...<coordinates>.Value = coord
            kp...<address>.Value = p.ToAddressString
            kp...<description>.Value = String.Format("ParcelId: {0} <br/>Block Index: {1}", p.Id, p.BlockIndex)

            kml...<Document>.First.Add(kp)
        Next
        routeLine...<LineString>...<coordinates>.Value = lineCoordinates
        kml...<Document>.First.Add(routeLine)
        Dim kmlPath As String = "D:\Cloud Storage\OneDrive\Desktop\route.kml"
        IO.File.WriteAllText(kmlPath, kml.ToString)
        Process.Start(kmlPath)
    End Sub

    Sub Log(ParamArray s() As Object)
        Dim lStr = s.Aggregate(Function(x, y) x.ToString() + " " + y.ToString())
        Console.WriteLine(lStr)
    End Sub

    Sub LogTable(colWidths() As Integer, ParamArray s() As Object)
        Dim formatString = ""
        If colWidths.Length <= s.Length Then
            For i As Integer = 0 To s.Length - 1
                If colWidths.Length - 1 < i Then
                    formatString += " {" & i & "}"
                Else
                    formatString += "{" & i & "," & colWidths(i) & "}"
                End If
            Next
        Else
            For i As Integer = 0 To s.Length - 1
                formatString += "{" & i & "," & colWidths(i) & "}"
            Next
        End If

        'Dim lStr = s.Aggregate(Function(x, y) x.ToString() + " " + y.ToString())
        Console.WriteLine(formatString, s)
    End Sub

End Module

Public Class Parcel
    Public Property o As Integer
    Public Property pid As Integer
    Public Property street As String
    Public Property latitude As Single
    Public Property longitude As Single
End Class
