﻿function setTableField(ddl, field, type) {
	$(ddl).addClass('wait');
	$(ddl).attr('disabled', 'disabled');
	
	var row = $(ddl).parents('.dst-row');
	var tid = $('#TID', row).val();
	var value = $(ddl).val();
	if (type == "checkbox") {
		value = ddl.checked;
	}
	$ds('updatetable', {
		TableId: tid,
		ColumnName: field,
		Value: value
	}, function (resp) {
		$(ddl).removeClass('wait');
		$(ddl).removeAttr('disabled');
		if (resp.status == "OK") {

		} else {
			alert(resp.message);
			if ((field == "LookupReferredTableField") || (field == "LookupGroupField")) {
				$(ddl).val('');
			}
		}
		setSelectColors();
	});
}

function setSelectColors() {
	$('select').each(function () {
		if ($(this).val() == "") {
			$(this).addClass('none-selected');
		} else {
			$(this).removeClass('none-selected');
		}
	});
}