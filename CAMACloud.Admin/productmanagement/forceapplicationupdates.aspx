﻿<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false"  MasterPageFile="~/App_MasterPages/ClientAdmin.master" CodeBehind="forceapplicationupdates.aspx.vb" Inherits="CAMACloud.Admin.forceapplicationupdates" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.min.css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
	<script type="text/javascript" src="/App_Static/jslib/jquery.min.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>

    <h1>
        Force Application Updates
    </h1>

    <style type="text/css">
        select {
            width: 200px;
        }

        input[type="text"] {
            width: 196px;
        }

        input[type="submit"] {
            margin-top: 35px;
        }

        td span[sep] {
            margin-left: 15px;
            float: right;
        }

        .select2-container {
          min-width: 400px;
        }
        
        .select2-results__option {
          padding-right: 20px;
          vertical-align: middle;
        }

        .select2-results__option:before {
          content: "";
          display: inline-block;
          position: relative;
          height: 20px;
          width: 20px;
          border: 2px solid #e9e9e9;
          border-radius: 4px;
          background-color: #fff;
          margin-right: 20px;
          vertical-align: middle;
        }
        .select2-results__option[aria-selected=true]:before {
          font-family:fontAwesome;
          content: "\f00c";
          color: #fff;
          background-color: #f77750;
          border: 0;
          display: inline-block;
          padding-left: 5px;
          padding-top: 5px;
        }

        .select2-container--default .select2-results__option[aria-selected=true] {
        	background-color: #fff;
        }

        .select2-container--default .select2-results__option--highlighted[aria-selected] {
        	background-color: #eaeaeb;
        	color: #272727;
        }

        .select2-container--default .select2-selection--multiple {
        	margin-bottom: 10px;
        }
        .select2-container--default.select2-container--open.select2-container--below .select2-selection--multiple {
        	border-radius: 4px;
        }

        .select2-container--default.select2-container--focus .select2-selection--multiple {
        	border-color: #f77750;
        	border-width: 2px;
        }

        .select2-container--default .select2-selection--multiple {
        	border-width: 2px;
        }

        .select2-container--open .select2-dropdown--below {
        	
        	border-radius: 6px;
        	box-shadow: 0 0 10px rgba(0,0,0,0.5);
        
        }

        .select2-selection .select2-selection--multiple:after {
        	content: 'hhghgh';
        }
        
        .select-icon .select2-selection__placeholder .badge {
        	display: none;
        }

        .select-icon .select2-results__option:before,
        .select-icon .select2-results__option[aria-selected=true]:before {
        	display: none !important;
        }

        .select-icon  .select2-search--dropdown {
        	display: none;
        }

        .validate {
            border: 1px solid red !important;
        }

        .info {
            width: 800px !important;
        }

        .ddl-countyfilter {
            height: 30px;
            border: 2px solid #aaa;
            border-radius: 4px;
            width: 110px;
            margin-right: 8px;
        }

        .cf-input-text {
            height: 25px;
            border: 2px solid #aaa;
            border-radius: 4px;
            width: 270px !important;
        }

        .cf-ddl-vendor, .cf-ddl-camasystem {
            height: 30px;
            border: 2px solid #aaa;
            border-radius: 4px;
            width: 278px !important;
        }

        .force-save {
            background-color: #1f9be3;
            width: 90px;
            height: 35px;
            text-align: center;
            color: #fff;
            border: none;
            border-radius: 5px;
        }

        .validate {
            border: 1px solid red !important;
        }
        
    </style>

    <script type="text/javascript">
        var countyList = [], camaSystemList = [], vendorList = [], stageList = [];

        function hideMask() {
            $('.masklayer').hide();
        }

        $('document').ready(function () {
            showMask();
            $(".ddl-UpdateType").val("").select2();
            $(".ddl-UpdateStage").select2({ multiple: true, placeholder: { id: '', text: 'None Selected' } }).val([]).trigger('change');
            $(".ddl-county").select2({ multiple: true, placeholder: { id: '', text: 'None Selected' } }).val([]).trigger('change');

            $$$fpAjax("forceapplicationupdates.aspx/getCounties", {}, (res) => {
                countyList = res?.d?.length > 0 ? res.d : [];
                $$$fpAjax("forceapplicationupdates.aspx/getCamaSystem", {}, (res) => {
                    camaSystemList = res?.d?.length > 0 ? res.d : [];
                    let html = "<option value=''>-- Select --</option>";
                    camaSystemList.forEach((u) => { html += "<option value=" + u.Id + ">" + u.Name + "</option>"; }); $('.cf-ddl-camasystem').append(html);
                    $$$fpAjax("forceapplicationupdates.aspx/getVendor", {}, (res) => {
                        vendorList = res?.d?.length > 0 ? res.d : [];
                        let html = "<option value=''>-- Select --</option>";
                        vendorList.forEach((u) => { html += "<option value=" + u.Id + ">" + u.Name + "</option>"; }); $('.cf-ddl-vendor').append(html);
                        $$$fpAjax("forceapplicationupdates.aspx/getStage", {}, (res) => {
                            stageList = res?.d?.length > 0 ? res.d : [];
                            let html = "<option value=''>-- Select --</option>";
                            stageList.forEach((u) => { html += "<option value=" + u.Id + ">" + u.StageDescription + "</option>"; }); $('.ddl-UpdateStage').append(html);
                            setTimeout(hideMask, 3000);
                        }, (err) => {
                            console.log(err.message);
                            });
                    }, (err) => {
                        console.log(err.message);
                    });
                }, (err) => {
                    console.log(err.message);
                });
            }, (err) => {
                console.log(err.message);
            });

            
            $('.ddl-countyfilter').bind('change', function () {
                if ($(this).val() == 1 || $(this).val() == 2) {
                    $('.cf-input-text').val('').show();
                    $('.cf-ddl-vendor, .cf-ddl-camasystem').hide();
                }
                else if ($(this).val() == 3) {
                    $('.cf-ddl-vendor').val('').show();
                    $('.cf-input-text, .cf-ddl-camasystem').hide();
                }
                else if ($(this).val() == 4) {
                    $('.cf-ddl-camasystem').val('').show();
                    $('.cf-input-text, .cf-ddl-vendor').hide();
                }

                bindCounties();
            });

            $('.ddl-UpdateStage, .cf-ddl-camasystem, .cf-ddl-vendor').bind('change', function () {
                bindCounties();
            });

            $('.ddl-UpdateType, .ddl-county').bind('change', function () {
                $('.select2-selection', $(this).siblings('.select2-container')).removeClass('validate');
            });

            $('.select-all').bind('change', function () {
                if ($(this)[0].checked) {
                    $('.ddl-UpdateStage').val([]).trigger('change');
                    $('.ddl-countyfilter, .ddl-UpdateStage, .cf-ddl-camasystem, .cf-ddl-vendor, .cf-input-text, .ddl-county').attr('disabled', 'disabled');
                }
                else {
                    $('.ddl-UpdateStage').val([]).trigger('change');
                    $('.ddl-countyfilter, .ddl-UpdateStage, .cf-ddl-camasystem, .cf-ddl-vendor, .cf-input-text, .ddl-county').removeAttr('disabled');
                }
            });

            $('.cf-input-text').bind('keyup', function () {
                bindCounties();
            });
        });

        function saveForceUpdates() {
            let st = $('.ddl-UpdateStage').val(), tp = $('.ddl-UpdateType').val(), sa = $('.select-all')[0].checked, cl = $('.ddl-county').val(),
                fl = $('.ddl-countyfilter').val(), cm = $('.cf-ddl-camasystem').val(), vr = $('.cf-ddl-vendor').val(), sr = $('.cf-input-text').val(), validate = true, up = '';

            if (!tp) {
                validate = false;
                $('.select2-selection', $('.ddl-UpdateType').siblings('.select2-container')).addClass('validate');
            }

            if (!sa && cl && cl.length == 0) {
                validate = false;
                $('.select2-selection', $('.ddl-county').siblings('.select2-container')).addClass('validate');
            }

            if (!validate) return false;
            let cf = confirm("Are you sure, you want to Continue?");

            if (cf) {
                showMask();
                //st?.forEach((x) => {
                //    switch (x) {
                //        case "1": up = up + 'Sales, '; break;
                //        case "2": up = up + 'Implementations, '; break;
                //        case "3": up = up + 'Integrations, '; break;
                //        case "4": up = up + 'Production, '; break;
                //        case "5": up = up + 'Sandboxes, '; break;
                //    }
                //});
                let stageIds = $('.ddl-UpdateStage').val() || [];
                let upStages = [];

                stageIds.forEach(stageId => {
                    let stage = stageList.find(stage => stage.Id === parseInt(stageId));
                    if (stage) {
                        upStages.push(stage.StageDescription);
                    }
                });
                up = upStages.join(', ');
                tp = tp == 1 ? 'Update Application/Update System Data' : 'Update Data & Assignment Groups';
                let ds = { updateType: tp, updateStage: up, county: (cl?.length > 0 ? cl.toString() : ''), allEnv: sa ? 1 : 0 };

                $$$fpAjax("forceapplicationupdates.aspx/saveForceUpdate", ds, (res) => {
                    if (res.d == "Sucess") {
                        alert("The force update successfully created!");
                        $('.ddl-UpdateStage').val([]).trigger('change'); $(".ddl-UpdateType").val("").trigger('change'); $('.ddl-countyfilter').val("1").trigger('change');
                        if (sa) {
                            $('.select-all')[0].checked = false; $('.select-all').trigger('change');
                        }
                    }
                    else {
                        alert("Error occured! " + res.d);
                    }
                    $('.masklayer').hide();
                }, (err) => {
                    alert("Error occured! " + err.message); $('.masklayer').hide();
                });
            }
            return false;
        }

        function bindCounties() {
            let v = $(".ddl-UpdateStage").val();
            if (v.length == 0) {
                $('.ddl-county').html('');
            }
            else {
                let sltr = $('.ddl-countyfilter').val(), srval = '', fc = '';
                if (sltr != '') {
                    switch (sltr) {
                        case "1":
                        case "2":
                            srval = $('.cf-input-text').val(); if (srval) { let tp = sltr == 1 ? 'Name' : 'State'; fc = "x['" + tp + "'] && x['" + tp + "'].toUpperCase().includes('" + srval.toUpperCase() + "')"; } break;
                        case "3":
                            srval = $('.cf-ddl-vendor').val(); if (srval) fc = "x['VendorId'] == '" + srval + "'"; break;
                        case "4":
                            srval = $('.cf-ddl-camasystem').val(); if (srval) fc = "x['CAMASystem'] == '" + srval + "'"; break;
                    }
                }

                if (!fc) fc = '1 == 1';

                let cList = countyList.filter((x) => { return v.indexOf(x['Stage'] && x['Stage'].toString()) > -1 && eval(fc) });
                let html = "";
                cList.forEach((u) => { html += "<option value=" + u.Id + ">" + u.Name + "</option>"; }); $('.ddl-county').html('').append(html);
            }
        }

        var $$$fpAjax = (path, d, scallback, fcallback) => {
            $.ajax({
                type: "POST",
                url: path,
                data: JSON.stringify(d),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: (data) => {
                    if (scallback) scallback(data);
                },
                error: (e) => {
                    if (fcallback) fcallback(e);
                }
            });
        }
    </script>

    <p class="info">
        "Force Appication updates"
    </p>
    <table class="tbl-force-update" style="border-spacing: 20px;">
        <tbody>
            <tr>
                <td colspan="2">
                    <input type="checkbox" id="allcounty" class="select-all" style="margin: 0; height: 16px; width: 16px;" />
                    <label for="allcounty" style="font-size: 19px; margin-left: 10px;">Select All Enivorments</label>
                </td>
            </tr>
            <tr>
                <td>
                    Update Type<span >:</span>
                </td>
                <td class="td-UpdateType">
                    <select class="ddl-UpdateType">
                        <option value="">--- Select ---</option>
                        <option value="1">Update Application/Update System Data</option>
                        <option value="2">Update Data & Assignment Groups</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    Update by Stages<span >:</span>
                </td>
                <td  class="td-UpdateStage">
                    <select class="ddl-UpdateStage"></select>
                </td>
            </tr>
            <tr>
                <td>
                   
                </td>
                <td>
                    <select class="ddl-countyfilter">
                        <option value="1">Name</option>
                        <option value="2">State</option>
                        <option value="3">Vendor</option>
                        <option value="4">CAMA System</option>
                    </select>
                    <input type="text" class="cf-input-text" placeholder="Search options...">
                    <select class="cf-ddl-vendor" style="display:none;"></select>
                    <select class="cf-ddl-camasystem" style="display:none;"></select>
                </td>
            </tr>
            <tr>
                <td>
                    County Name<span >:</span>
                </td>
                <td>
                    <select class="ddl-county"></select>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center; vertical-align: middle;">
                    <button class="force-save" onclick="return saveForceUpdates();"> Save</button>
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>