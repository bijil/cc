﻿<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" MasterPageFile="~/App_MasterPages/ClientAdmin.master" CodeBehind="forceupdateaudittrail.aspx.vb" Inherits="CAMACloud.Admin.forceupdateaudittrail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <link href="/App_Static/css/datatables/jquery.dataTables.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src = "/App_Static/js/datatables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src = "/App_Static/js/datatables/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src = "/App_Static/js/datatables/buttons.html5.min.js"></script>
    <script type="text/javascript" src = "/App_Static/js/datatables/moment.min.js"></script>

    <h1>
        Force Application Updates AuditTrail
    </h1>
    <style>
        body {
            overflow-y: auto !important;
        }

        .left-col {
            float: left;
            width: 25%;
        }


        .right-col {
            float: right;
        }

        table.dataTable tbody td {
            padding: 5px 9px;
        }

        .dataTables_filter {
            margin-top: 20px;
            margin-bottom: 20px;
        }

        .buttons-excel {
            float: right;
        }

        .max-width-200 {
            max-width: 200px;
            word-wrap: break-word;
        }

        .break {
            word-break: break-all;
        }

        .hiddencol {
            display: none;
        }
      
        .udag-btn {
            border: 1px solid #aed0ea;
            background: #b2daf5 url(images/ui-bg_glass_80_b2daf5_1x400.png) 50% 50% repeat-x;
            color: #2779aa;
            margin-right: 5px;
            border-radius: 5px;
            width: 75px;
            height: 25px;
        }

        .udag-btn:hover {
            background-color: #000 !important;
            color: #fff !important;
        }

        table.dataTable.udag-table {
            box-shadow: #bbbbbb 0px 0px 5px 0px;
            max-height: 650px !important;
            overflow-y: auto !important;
        }

            table.dataTable.udag-table thead {
                border: 1px solid #aed0ea;
                background: #b2daf5 url(images/ui-bg_glass_80_b2daf5_1x400.png) 50% 50% repeat-x;
                font-weight: bold;
                color: #2779aa;
                text-align: center;
            }

            table.dataTable.udag-table thead, table.dataTable.udag-table tr, table.dataTable.udag-table td {
                text-align: center;
            }

            table.dataTable.udag-table td {
                color: #000;
                max-width:200px;
                word-break: break-word;
            }

            table.dataTable.udag-table thead th, table.dataTable.udag-table thead td {
                border-bottom: 0px solid #111 !important;
            }

            table.dataTable.udag-table tr {
                outline: 0.3px solid rgb(243, 233, 233);
            }

        .dataTables_wrapper > div {
            margin: 5px;
        }


        table.dataTable.udag-table thead th {
            position: relative;
            background-image: none !important;
        }

        table.dataTable.udag-table tbody tr:hover {
            background-color: #f2f2f2 !important;
            color: #000;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.current,
        .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
            border-radius: 50px;
            background: #b2daf5 url(images/ui-bg_glass_80_b2daf5_1x400.png) 50% 50% repeat-x;
            font-weight: bold;
            color: #2779aa !important;
        }

        .paginate_button.current:hover {
            background: none !important;
            border-radius: 50px;
            background-color: #7f49b4 !important;
            color: #fff !important;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover, .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            border: 1px solid #979797;
            background: none !important;
            border-radius: 50px !important;
            background-color: #000 !important;
            color: #fff !important;
        }

        .dataTables_length select {
            width: 100px !important;
        }

        .dataTables_filter input {
            width: 200px !important;
        }

        .popup-mask {
            position: absolute;
            background-color: #0c0b0b;
            z-index: 20;
            height: 100%;
            width: 100%;
            opacity: 0.5;
            top: 0%;
            bottom: 0%;
            right: 1px;
            left: 0px;
        }

        .udag-popup {
            z-index: 100;
            top: 20%;
            left: 25%;
            z-index: 200;
            min-width: 700px;
            max-width: 850px;
            min-height: 350px;
            max-height: 450px;
            border-radius: 8px;
            position: absolute;
            padding: 10px;
            background-color: #f2f2f2;
            box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%), 0 6px 20px 0 rgb(0 0 0 / 19%);
        }

        .udag-close {
            background-color: #f4eded;
            border: 3px solid #999;
            border-radius: 50px;
            cursor: pointer;
            display: inline-block;
            font-family: arial;
            font-weight: bold;
            position: absolute;
            top: -20px;
            right: -20px;
            font-size: 30px;
            line-height: 26px;
            width: 30px;
            height: 30px;
            text-align: center;
        }

        .udag-popup-table {
          width: 100%;
          box-shadow: #bbbbbb 0px 0px 5px 0px;
        }

            .udag-popup-table thead {
                border: 1px solid #aed0ea;
                background: #b2daf5 url(images/ui-bg_glass_80_b2daf5_1x400.png) 50% 50% repeat-x;
                font-weight: bold;
                color: #2779aa;
                text-align: center;
                height: 35px;
            }

            .udag-popup-table thead, .udag-popup-table tr, .udag-popup-table td {
                text-align: center;
            }

            .udag-popup-table td {
                color: #000;
            }

            .udag-popup-table thead th, .udag-popup-table thead td {
                border-bottom: 0px solid #111 !important;
            }

            .udag-popup-table tr {
                outline: 0.3px solid rgb(243, 233, 233);
                height: 30px;
            }

            .udag-popup-table tbody tr:nth-child(even) {
              background-color: #f9f9f9;
            }

        .udag-popup-div {
            margin-left: 5px;
            max-height: 395px;
            overflow-y: auto;
            max-width: 825px;
            overflow-x: auto;
        }
        .scroll{
             overflow-x: auto;
             max-width:91vw;
        }
    </style>

    <script type="text/javascript">
        $(function () {
            showMask();
            $$$ajax("forceupdateaudittrail.aspx/GetForceUpdateTbl", {}, (res) => {
                bindForceUpdateTable(res);
                $('.udag-div').show();
                setTimeout(hideMask, 3000);
            }, (e) => {
                console.log(e);
            });
        });

        function hideMask() {
            $('.masklayer').hide();
        }

        function bindForceUpdateTable(data) {
            $('#udag-table').dataTable().fnDestroy();
            var udagTable = $("#udag-table").DataTable({
                bLengthChange: true,
                pageLength: 20,
                lengthMenu: [[10, 20, 50, 100], [10, 20, 50, 100]],
                bFilter: true,
                bPaginate: true,
                data: data.d,
                ordering: true,
                fixedColumns: true,
                "order": [],
                columns: [
                    {
                        'data': 'Id',
                        'searchable': false,
                        'sortable': true,
                        visible: false
                    },
                    {
                        'data': 'EventDate',
                        'searchable': true,
                        'sortable': true,
                        //'render': function (data, type, row) { return moment(data).format("MM/DD/YYYY hh:mm:ss A"); },
                        'width': '20%'
                    },
                    {
                        'data': 'LoginId',
                        'searchable': true,
                        'sortable': false,
                        'width': '15%'
                    },
                    {
                        'data': 'UpdateType',
                        'sortable': false,
                        'searchable': false
                    },
                    {
                        'data': 'Stage',
                        'sortable': false,
                        'searchable': false
                    },
                    {
                        'data': 'UpdateAllEnv',
                        'sortable': false,
                        'searchable': false
                    },
                    {
                        'data': '',
                        'sortable': false,
                        'searchable': false,
                        'width': '20%'
                    }
                ],
                columnDefs: [
                    {
                        width: 200,
                        targets: -1,
                        render: function (data, type, row, meta) {
                            return '<button class="show-btn udag-btn" onclick="showList(' + row.Id + '); return false;">Show</button>';
                        },

                    }
                ]
            });
        }

        var showList = (rId) => {
            showMask();
            $$$ajax("forceupdateaudittrail.aspx/GetStatusData", { RowId: rId }, (res) => {
                if (res?.d?.length > 0) {
                    let data = res.d, table = document.createElement("table"),
                        thead = document.createElement("thead"), headerRow = document.createElement("tr"),
                        tbody = document.createElement("tbody");

                    $(table).addClass('udag-popup-table');

                    Object.keys(data[0]).forEach(key => {
                        let th = document.createElement("th");
                        th.textContent = key;
                        headerRow.appendChild(th);
                    });

                    thead.appendChild(headerRow); table.appendChild(thead);
                    data.forEach(item => {
                        let row = document.createElement("tr");
                        Object.values(item).forEach(value => {
                            let cell = document.createElement("td");
                            cell.textContent = value;
                            row.appendChild(cell);
                        });
                        tbody.appendChild(row);
                    });

                    table.appendChild(tbody);
                    $('.udag-popup-div').html('').append(table);
                    hideMask();
                    $('.popup-mask').height($('body').height());

                    $('.popup-mask, .udag-popup').show();
                }
                else hideMask();
            }, (e) => {
                hideMask();
            });
            return false;
        }

        var convertDate = (res) => {
            return res.map(obj => ({ ...obj, Date: moment(obj.Date).format("MM/DD/YYYY hh:mm:ss A") }));
        }

        var popupClose = () => {
            $('.popup-mask, .udag-popup').hide();
        }

        var $$$ajax = (path, d, scallback, fcallback) => {
            $.ajax({
                type: "POST",
                url: path,
                data: JSON.stringify(d),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: (data) => {
                    if (scallback) scallback(data);
                },
                error: (e) => {
                    if (fcallback) fcallback(e);
                }
            });
        }
    </script>

    <p class="info">
        "Force Appication Updates AuditTrail"
    </p>
     
    <div class="udag-div" style="width:99%;">
        <table id="udag-table" class="udag-table">
            <thead>
                <tr>
                    <th data-orderable="true">Id</th>
                    <th>Created Date</th>
                    <th data-orderable="false">LoginId</th>
                    <th data-orderable="false">Update Type</th>
                    <th data-orderable="false" >Stage</th>
                    <th data-orderable="false">All Env</th>
                    <th data-orderable="false">Enviornment Status</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div class="popup-mask" style="display: none;"></div>
    <div class="udag-popup" style="display: none;">
        <div style="padding:10px; font-size: 17px;">
            <span style="font-weight: bold; color: #2779aa">Enviornment Status</span>
            <div class="udag-close" onclick="popupClose();">&times;</div>
        </div>
        <hr style="height: 2px; margin: 5px; background-color: #aca7a7; margin-bottom: 10px">
        <div class="udag-popup-div">
                   
        </div>
    </div></div>
</asp:Content>
