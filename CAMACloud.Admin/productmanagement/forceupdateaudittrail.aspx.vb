﻿Public Class forceupdateaudittrail
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    <System.Web.Services.WebMethod()>
    Public Shared Function GetForceUpdateTbl() As JSONTable
        Dim dt As DataTable = Database.System.GetDataTable("Select Id, Format(dbo.GetLocalDate(EventDate),'MM-dd-yyyy hh:mm:ss tt') as EventDate, LoginId, UpdateType, Stage, UpdateAllEnv FROM ForceUpdateApplication ORDER BY Id desc")
        Return dt.ConvertToList()
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function GetStatusData(ByVal RowId As String) As JSONTable
        Dim dt As DataTable = Database.System.GetDataTable("Select o.Name [County Name], f.status [Downloaded], Format(dbo.GetLocalDate(UpdateDate),'MM-dd-yyyy hh:mm:ss tt') as [Download Time], f.QCStatus, Format(dbo.GetLocalDate(QCUpdateDate),'MM-dd-yyyy hh:mm:ss tt') as [QC Update Time], f.DTRStatus, Format(dbo.GetLocalDate(DTRUpdateDate),'MM-dd-yyyy hh:mm:ss tt') as [DTR Update Time]  FROM ForceUpdateApplicationStatus f Join Organization o on f.orgId = o.Id WHERE f.UpdateId = " + RowId)
        Return dt.ConvertToList()
    End Function

End Class