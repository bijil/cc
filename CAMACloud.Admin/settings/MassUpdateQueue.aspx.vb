﻿Public Class MassUpdateQueue
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            RefreshGrid()

            'Checking the Logged user and change the permissions
            If Session("loggedUserType") Is Nothing Then
                Alert("Session Experied, Please LogIn again")
                Response.Redirect("~/auth/Default.aspx")
            Else
                Dim loggedUserType As String = Session("loggedUserType").ToString()

                If loggedUserType = "support" Then
                    Dim lnkBtnUpdate As LinkButton = New LinkButton()
                    Dim lnlBtnUpdateAll As LinkButton = New LinkButton()

                    For i As Integer = 0 To gridPending.Rows.Count - 1
                        lnkBtnUpdate = DirectCast(gridPending.Rows(i).FindControl("lnkBtnUpdate"), LinkButton)
                        lnkBtnUpdate.Visible = False

                        lnlBtnUpdateAll = DirectCast(gridPending.Rows(i).FindControl("lnlBtnUpdateAll"), LinkButton)
                        lnlBtnUpdateAll.Visible = False
                    Next
                End If
            End If
        End If
    End Sub

    Private Sub gridPending_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gridPending.RowCommand
        lblStatus.Text = ""
        Dim updateId As Integer = e.CommandArgument
        Select Case e.CommandName
            Case "UpdatePending"
                chlHosts.DataSource = Database.System.GetDataTable("SELECT * FROM ApplicationHost WHERE Id IN (SELECT ApplicationHostId FROM EnvironmentUpdatesPending WHERE EnvironmentUpdateId = " & updateId & ")")
                chlHosts.DataValueField = "Id"
                chlHosts.DataTextField = "Name"
                chlHosts.DataBind()
                hdnUpdateId.Value = updateId
                gridPending.Visible = False
                pnlUpdatePending.Visible = True
            Case "UpdateAll"
                ProcessPendingUpdates(updateId, "Id IN (SELECT ApplicationHostId FROM EnvironmentUpdatesPending WHERE EnvironmentUpdateId = " & updateId & ")")
        End Select
    End Sub


    Sub ProcessPendingUpdates(updateId As String, hostFilter As String)
        lblStatus.Text = ""

        Dim sql As String = Database.System.GetStringValue("SELECT UpdateSql FROM EnvironmentUpdates WHERE Id = " & updateId)
        Dim sqls() As String = GetSQLBatches(sql)

        Dim success As Integer = 0
        Dim failed As Integer = 0
        Dim lastError As String = ""

        Dim orgs As DataTable = Database.System.GetDataTable("SELECT * FROM Organization WHERE ApplicationHostId IN (SELECT Id FROM ApplicationHost WHERE IsDemo = 0 AND " & hostFilter & ")")
        ExecuteSQL(orgs, sqls, False, success, failed, lastError)

        Dim demoOrgs As DataTable = Database.System.GetDataTable("SELECT * FROM Organization WHERE ApplicationHostId IN (SELECT Id FROM ApplicationHost WHERE IsDemo = 1 AND " & hostFilter & ")")
        ExecuteSQL(demoOrgs, sqls, True, success, failed, lastError)


        If success > 0 Then
            lblStatus.Text = success & " environments updated successfully. "
        End If
        If failed > 0 Then
            lblStatus.Text += failed.ToString + " environments failed. "
        End If

        If failed > 0 Then
            lblError.Text = lastError
        End If

        Database.System.Execute("DELETE FROM EnvironmentUpdatesPending WHERE EnvironmentUpdateId = " & updateId & " AND ApplicationHostId IN (SELECT Id FROM ApplicationHost WHERE " & hostFilter & ")")
        RefreshGrid()
        pnlUpdatePending.Visible = False
    End Sub

    Sub ExecuteSQL(orgs As DataTable, sqls() As String, useDemo As Boolean, ByRef success As Integer, ByRef failed As Integer, ByRef lastError As String)

        

        For Each org As DataRow In orgs.Rows
            Dim connStr As String = Database.GetConnectionStringFromOrganization(org.GetInteger("Id"))

            Try
                Dim conn As New SqlClient.SqlConnection(connStr)
                conn.Open()
                For Each ssql As String In sqls
                    Dim cmd As New SqlClient.SqlCommand(ssql, conn)
                    cmd.ExecuteNonQuery()
                Next
                conn.Close()
                success += 1
            Catch sqlex As SqlClient.SqlException
                failed += 1
                lastError = sqlex.Message
            Catch ex As Exception
                failed += 1
            End Try
        Next

        
    End Sub


    Function GetSQLBatches(sql As String) As String()
        Dim batches As New List(Of String)
        Dim currentSql As String = ""
        Dim sqlLines As String() = sql.Split(vbLf)
        For Each line In sqlLines
            If line.Trim.ToLower = "go" Then
                batches.Add(currentSql)
                currentSql = ""
            Else
                currentSql += line + vbLf
            End If
        Next
        If currentSql <> "" Then
            batches.Add(currentSql)
        End If
        Return batches.ToArray
    End Function

    Private Sub btnUpdate_Click(sender As Object, e As System.EventArgs) Handles btnUpdate.Click

        Dim hostIds As String = "0"
        For Each li As ListItem In chlHosts.Items
            If li.Selected Then
                hostIds.Append(li.Value, ", ")
            End If
        Next

        Dim hostFilter As String = "Id IN (" + hostIds + ")"
        ProcessPendingUpdates(hdnUpdateId.Value, hostFilter)
    End Sub

    Sub RefreshGrid()
        gridPending.DataSource = Database.System.GetDataTable("SELECT * FROM EnvironmentUpdates WHERE Id IN (SELECT EnvironmentUpdateId FROM EnvironmentUpdatesPending) ORDER BY EventDate")
        gridPending.DataBind()
        gridPending.Visible = True
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        RefreshGrid()
        pnlUpdatePending.Visible = False
    End Sub
End Class