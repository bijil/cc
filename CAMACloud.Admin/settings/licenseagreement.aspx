﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ClientAdmin.master"
    CodeBehind="licenseagreement.aspx.vb" Inherits="CAMACloud.Admin.AgreementContent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        .hidden
{
    display:none;
}
        .ma-vendor-app-logo
        {
            background-position:center; 
        }
        .ma-vendor-logo
        {
            height: 100px;
            width: 318px;
            background-repeat: no-repeat;
            border:1px solid #CFCFCF;
           
        }
    </style>

    <script type="text/javascript">
        $(function () {
            $('.upload').change(function () {
                $('.btn', $(this).parent()).click();
            });
        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <asp:HiddenField runat="server" ID="tickValue" />
    <h1>
        MobileAssessor - License Agreement Content</h1>
    <table>
        <tbody>
            <tr>
                <td>
                    Vendor Name:
                </td>
                <td>
                    <asp:DropDownList ID="ddlVendor" runat="server" Width="250px" AutoPostBack =true >
                    </asp:DropDownList>
                </td>
            </tr>
             <tr>
                <td colspan="2">
                    Vendor Information:
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:TextBox ID="txtVendorInfo" TextMode="MultiLine" runat="server" Rows="3" Width="500px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                   Vendor Agreement:
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:TextBox ID="txtAgreement" TextMode="MultiLine" runat="server" Rows="10" Width="500px"></asp:TextBox>
                </td>
            </tr>
             <tr>
                  <td>
                Vendor Logo: 
                <div class="ma-vendor-logo ma-vendor-app-logo" runat="server" id="logoMA5" ></div>
                    
                <asp:FileUpload runat="server"  Width="350px" CssClass="upload" ID="FileMA5" />
                <div class="hidden">  <asp:Button ID="Button1" runat="server" CommandName="MA5" CssClass="btn" OnClick="Button1_Click"/></div>  
                      <br />
                </td>
                <td class="info">
                    <br />
                    <br />
                    Specifications:<br />
                Format: PNG Preferred<br />
                Height:100px <br />
                Width:318px <br />
                Background: Transparent
            </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <asp:Button ID="btnSave" runat="server" Text="Save Changes" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnReset" runat="server" Text="Reset" />
                </td>
                <td>
                    <br />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>
