﻿Public Class users
    Inherits System.Web.UI.Page

    Dim FullName As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Not IsPostBack Then
        'If Not Roles.RoleExists("Administrator") Then
        '    Roles.CreateRole("Administrator")
        'End If
        'If Not Roles.RoleExists("LimitedAdmin") Then
        '    Roles.CreateRole("LimitedAdmin")
        'End If
        'If Not Roles.IsUserInRole("admin", "Administrator") Then
        '    Roles.AddUserToRole("admin", "Administrator")
        'End If
        'grid.DataSource = Membership.GetAllUsers()
        'grid.DataBind()
        'End If

        If Not IsPostBack Then
            BindGridView()
        End If
    End Sub

    'Public Function GetUserRolesAsString()
    '    Dim userName As String = Eval("UserName")
    '    Dim uroles() As String = Roles.GetRolesForUser(userName)
    '    Dim str As String = ""
    '    For Each r In uroles
    '        str += IIf(str <> "", ", ", "") + r
    '    Next
    '    Return str
    'End Function

    Protected Sub BindGridView()
        For Each u As MembershipUser In Membership.GetAllUsers
            If Database.System.GetIntegerValue("SELECT COUNT(*) FROM AdminUserSettings WHERE LoginId = {0}".SqlFormatString(u.UserName)) = 0 Then
                Database.System.Execute("INSERT INTO AdminUserSettings (LoginId, FirstName) VALUES ({0}, {0})".SqlFormatString(u.UserName))
            End If
        Next

        If UserName() = "support" Then
            grid.DataSource = Database.System.GetDataTable("SELECT * FROM AdminUserSettings WHERE LogedId NOT IN ('admin') ORDER BY FirstName")
        Else
            grid.DataSource = Database.System.GetDataTable("SELECT * FROM AdminUserSettings WHERE LoginId NOT IN ('admin') ORDER BY FirstName")
        End If

        grid.DataBind()
    End Sub

    Public Function UserStatus() As String
        Dim loginId As String = Eval("LoginId")
        Dim u As MembershipUser = Membership.GetUser(loginId)
        If u Is Nothing Then
            Return "invalid"
        End If
        If u.IsLockedOut Then
            Return "locked"
        End If
        If Not u.IsApproved Then
            Return "pending"
        End If
        Return "ok"
    End Function

    Public ReadOnly Property UserLastLoginDate As String
        Get
            If Membership.GetUser(Eval("LoginId").ToString) Is Nothing Then
                Return String.Empty
            End If
            Return Database.System.GetStringValue("SELECT dbo.GetLocalDate('" + Membership.GetUser(Eval("LoginId").ToString).LastActivityDate.ToString("yyyy-MM-dd HH:mm:ss") + "')")
        End Get
    End Property

    Public Function IsUserLocked() As Boolean
        Dim loginId As String = Eval("LoginId")
        Dim u As MembershipUser = Membership.GetUser(loginId)
        If u Is Nothing Then
            Return False
        End If
        If u.IsLockedOut Then
            Return True
        End If
        Return False
    End Function

    Sub ClearForm()
        hdnUserId.Value = ""
        txtFirstName.Text = ""
        txtMiddleName.Text = ""
        txtLastName.Text = ""
        txtMobileNumber.Text = ""
        'ddlMobileCarrier.SelectedIndex = 0
        txtUserMail.Text = ""
        txtLoginId.Text = ""
        txtPassword.Text = ""
        txtPassword2.Text = ""
        txtLoginId.Enabled = True
        rfvPassword.Enabled = True
        'For Each li As ListItem In cblRoles.Items
        '    li.Selected = False
        'Next
        btnCancel.Text = "Cancel"
    End Sub

    Protected Sub btnSaveUser_Click(sender As Object, e As EventArgs) Handles btnSaveUser.Click
        Dim res As MembershipCreateStatus
        Dim userName As String = txtLoginId.Text

        FullName = txtFirstName.Text + " " + txtMiddleName.Text + " " + txtLastName.Text

        'Change Membership Password without using Old Password
        If txtNewPassword.Text <> "" Then
            Dim mu As MembershipUser = Membership.GetUser(userName)
            Dim ResPassword = mu.ResetPassword()
            mu.ChangePassword(ResPassword, txtNewPassword.Text)
        End If

        Try
            If hdnUserId.Value = "" Then
                If rblRoles4.SelectedValue = "" Then
                    Alert("Please select one Role")
                    Return
                End If

                Membership.CreateUser(userName, txtPassword.Text, txtUserMail.Text, "@", "#", True, res)

                If res = MembershipCreateStatus.Success Then
                    Database.System.Execute("INSERT INTO AdminUserSettings(LoginId,FirstName,MiddleName,LastName,Email,Mobile,UserType) VALUES({0},{1},{2},{3},{4},{5},{6})".SqlFormatString(userName, txtFirstName.Text,
                                            IIf(txtMiddleName.Text = "", "", txtMiddleName.Text), IIf(txtLastName.Text = "", "", txtLastName.Text), txtUserMail.Text, txtMobileNumber.Text, rblRoles4.SelectedValue))
                    'ddlMobileCarrier.SelectedValue, 
                    'IIf(cbNotifybyEmail.Checked, 1, 0), IIf(cbNotifybySMS.Checked, 1, 0), IIf(cbAutoQC.Checked, 1, 0)
                    'AssignRolesFromList(userName)

                    ClearForm()

                    BindGridView()

                    RunScript("hidePopup();")
                Else
                    If res = MembershipCreateStatus.DuplicateEmail Then
                        Alert("This email address has been used for another account. Please try with a different email address.")
                        Return
                    End If
                    If res = MembershipCreateStatus.DuplicateUserName Then
                        Alert("Log in ID already exists. Please choose another one.")
                        Return
                    End If
                    If res = MembershipCreateStatus.InvalidUserName Then
                        Alert("Log in ID is invalid. Please choose another one.")
                        Return
                    End If
                    If res = MembershipCreateStatus.InvalidPassword Then
                        Alert("Password must be minimum 6 characters in length\nMust contain at least one special symbol.")
                        Return
                    End If
                    Alert("Error: " + res.ToString)
                    Return
                End If
            Else
                Dim u As MembershipUser = Membership.GetUser(userName)

                If txtPassword.Text.IsNotEmpty AndAlso txtPassword.Text = txtPassword2.Text Then
                    If txtPassword.Text.Length < 6 Then
                        Alert("Password must be minimum 6 characters in length\nMust contain at least one special symbol.")
                        Return
                    End If
                    u.ChangePassword(u.ResetPassword(), txtPassword.Text)
                End If

                u.Email = txtUserMail.Text
                Membership.UpdateUser(u)
                Dim lookUpId As Integer
                lookUpId = Database.System.Execute("UPDATE AdminUserSettings SET FirstName={1},MiddleName={2},LastName={3},Email={4},Mobile={5},UserType={6} WHERE LoginId={0};SELECT CAST(@@ROWCOUNT AS INT) As NeewId ".SqlFormatString(userName, txtFirstName.Text,
                                            IIf(txtMiddleName.Text = "", "", txtMiddleName.Text), IIf(txtLastName.Text = "", "", txtLastName.Text), txtUserMail.Text, txtMobileNumber.Text, rblRoles4.SelectedValue)) 'ddlMobileCarrier.SelectedValue, , IIf(cbNotifybyEmail.Checked, 1, 0), IIf(cbNotifybySMS.Checked, 1, 0), IIf(cbAutoQC.Checked, 1, 0)
                If (lookUpId = 0) Then
                    Database.System.Execute("INSERT INTO AdminUserSettings(LoginId,FirstName,MiddleName,LastName,Email,Mobile,UserType) VALUES({0},{1},{2},{3},{4},{5},{6})".SqlFormatString(userName, txtFirstName.Text,
                                          IIf(txtMiddleName.Text = "", "", txtMiddleName.Text), IIf(txtLastName.Text = "", "", txtLastName.Text), txtUserMail.Text, txtMobileNumber.Text, rblRoles4.SelectedValue)) 'ddlMobileCarrier.SelectedValue, , IIf(cbNotifybyEmail.Checked, 1, 0), IIf(cbNotifybySMS.Checked, 1, 0), IIf(cbAutoQC.Checked, 1, 0)
                End If


                'AssignRolesFromList(userName)

                BindGridView()

                Alert("Changes updated successfully.\n\nClick Close to go back to the list.")

                'If chkLocked.Visible AndAlso chkLocked.Checked = False Then
                '    u.UnlockUser()
                'End If
            End If

        Catch ex As Exception
            Alert(ex.Message)
        End Try
    End Sub

    Sub LoadUserForEdit(userName As String)
        hdnUserId.Value = userName
        Dim u As MembershipUser = Membership.GetUser(userName)
        If u Is Nothing Then
            Dim tr As DataRow = Database.System.GetTopRow("SELECT * FROM AdminUserSettings WHERE LoginId='" + userName + "'")
            Membership.CreateUser(userName, "123456", tr.Get("Email"))
            u = Membership.GetUser(userName)
        End If

        txtUserMail.Text = u.Email
        txtLoginId.Text = u.UserName
        txtLoginId.Enabled = False
        rfvPassword.Enabled = False
        txtPassword.Enabled = False
        txtPassword2.Enabled = False
        newPwd.Visible = True

        Dim dr As DataRow = Database.System.GetTopRow("SELECT * FROM AdminUserSettings WHERE LoginId='" + u.UserName + "'")

        If dr IsNot Nothing Then
            txtFirstName.Text = dr.GetString("FirstName")
            txtMiddleName.Text = dr.GetString("MiddleName")
            txtLastName.Text = dr.GetString("LastName")
            txtMobileNumber.Text = dr.GetString("Mobile")

            If dr.GetString("UserType") = "admin" Then
                rblRoles4.SelectedIndex = 0
            ElseIf dr.GetString("UserType") = "support" Then
                rblRoles4.SelectedIndex = 1
            ElseIf dr.GetString("UserType") = "ProductManagement" Then
                rblRoles4.SelectedIndex = 2
            End If

            ' ddlMobileCarrier.SelectedValue = IIf(dt.Rows(0)("MobileCarrierId") IsNot Nothing, ddlMobileCarrier.Items.IndexOf(ddlMobileCarrier.Items.FindByValue(dt.Rows(0)("MobileCarrierId").ToString())), ddlMobileCarrier.Items.IndexOf(ddlMobileCarrier.Items.FindByValue("15")))
            'ddlMobileCarrier.SelectedValue = dr.GetString("MobileCarrierId")

            'cbNotifybyEmail.Checked = dr.GetBoolean("NotifyByEmail")
            'cbNotifybySMS.Checked = dr.GetBoolean("NotifyBySMS")
            'cbAutoQC.Checked = dr.GetBoolean("AutoQC")
        Else
            txtFirstName.Text = ""
        End If
        'For Each li As ListItem In cblRoles.Items
        '    If Roles.IsUserInRole(userName, li.Value) Then
        '        li.Selected = True
        '    Else
        '        li.Selected = False
        '    End If
        'Next
        btnCancel.Text = "Close"

        'If u.IsLockedOut Then
        '    chkLocked.Checked = True
        'Else
        '    chkLocked.Visible = False
        'End If
    End Sub

    Protected Sub grid_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grid.RowCommand
        hdnUserId.Value = e.CommandArgument
        Select Case e.CommandName
            Case "DeleteUser"
                If e.CommandArgument = Membership.GetUser().UserName Then
                    Alert("You cannot delete your own user account.")
                    Return
                End If
                If e.CommandArgument = "admin" Then
                    Alert("You cannot delete the default 'admin' user.")
                    Return
                End If
                If e.CommandArgument = "support" Then
                    Alert("You cannot delete the default 'support' user.")
                    Return
                End If
                Membership.DeleteUser(e.CommandArgument)
                'Database.Tenant.Execute("DELETE FROM UserTrackingCurrent WHERE LoginId = " + e.CommandArgument.ToString.ToSqlValue)
                Database.System.Execute("DELETE FROM AdminUserSettings WHERE LoginId = " + e.CommandArgument.ToString.ToSqlValue)
                BindGridView()
            Case "EditUser"
                Try
                    ClearForm()
                    LoadUserForEdit(e.CommandArgument)
                Catch ex As Exception
                    Alert(ex.Message)
                End Try

            Case "UnlockUser"
                Dim u As MembershipUser = Membership.GetUser(e.CommandArgument.ToString())
                If u IsNot Nothing Then
                    u.UnlockUser()
                End If
                BindGridView()
            Case Else

        End Select
        'BindGridView()
    End Sub

    Protected Sub lbNewUser_Click(sender As Object, e As EventArgs) Handles lbNewUser.Click
        'userPwd.Visible = True
        'userCPwd.Visible = True
        txtLoginId.Text = ""
        txtLoginId.Enabled = True
        newPwd.Visible = False
        txtPassword.Enabled = True
        txtPassword2.Enabled = True
    End Sub
End Class