﻿Imports System.Web.Services
Public Class camasystem
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

        End If
    End Sub


    Public Class CamaSystemSettings

        Public Property Id As Integer
        Public Property VId As Integer
        Public Property CamaSystem As String
        Public Property VendorName As String

    End Class


    <WebMethod>
    Public Shared Function GetCamaRecords() As List(Of CamaSystemSettings)
        Dim CamaSystemrecord As List(Of CamaSystemSettings) = New List(Of CamaSystemSettings)()


        Dim dt As DataTable = Database.System.GetDataTable("SELECT C.id, C.Name AS CamaSystem,Vendorid AS VendorId,V.Name AS VendorName FROM CAMASystem C JOIN Vendor V ON C.VendorId = V.Id ORDER BY V.Name")

        For Each q In dt.Rows

            CamaSystemrecord.Add(New CamaSystemSettings With {
                            .Id = q("Id"),
                            .VId = q("VendorId"),
                            .CamaSystem = q("CamaSystem"),
                            .VendorName = q("VendorName")
                        })
        Next

        Return CamaSystemrecord
    End Function


End Class