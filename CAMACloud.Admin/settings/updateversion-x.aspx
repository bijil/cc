﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ClientAdmin.master" CodeBehind="updateversion-x.aspx.vb" Inherits="CAMACloud.Admin.updateversion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Update Mobile Accessor Version</h1>
    <p class="info">This action will create a new version notification for MobileAssessor.</p>
    <asp:Button runat="server" ID="btnUpdate" Text=" Release MobileAssessor Version " />
</asp:Content>
