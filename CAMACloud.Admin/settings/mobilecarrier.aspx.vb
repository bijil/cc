﻿Imports System.Data
Partial Class settings_mobilecarrier
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGridView()

            'Checking the Logged user and change the permissions
            If Session("loggedUserType") Is Nothing Then
                Alert("Session Experied, Please LogIn again")
                Response.Redirect("~/auth/Default.aspx")
            Else
                Dim loggedUserType As String = Session("loggedUserType").ToString()

                If loggedUserType = "support" Then
                    btnAdd.Visible = False

                    Dim lnkBtnEdit As LinkButton = New LinkButton()
                    Dim lnlBtnDelete As LinkButton = New LinkButton()

                    For i As Integer = 0 To gdvUserDetails.Rows.Count - 1
                        lnkBtnEdit = DirectCast(gdvUserDetails.Rows(i).FindControl("LinkButton1"), LinkButton)
                        lnkBtnEdit.Visible = False

                        lnlBtnDelete = DirectCast(gdvUserDetails.Rows(i).FindControl("lbtnDeleteUser"), LinkButton)
                        lnlBtnDelete.Visible = False
                    Next
                End If
            End If
        End If
    End Sub

    'Protected Sub gdvUserDetails_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
    '    Dim loggedUser As String = Session("loggedUser").ToString()

    '    If loggedUser = "dcs-support" Then
    '        Dim theEditLinkButton As LinkButton = CType(e.Row.FindControl("lbtnDeleteUser"), LinkButton)
    '        theEditLinkButton.Enabled = False
    '    End If
    'End Sub

    Protected Sub BindGridView()
        Dim _dtCarrier As DataTable
        _dtCarrier = Database.System.GetDataTable("SELECT *  FROM MobileCarrier")
        gdvUserDetails.DataSource = _dtCarrier
        gdvUserDetails.DataBind()
    End Sub

    Protected Sub gdvUserDetails_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gdvUserDetails.RowCommand
        Select Case e.CommandName
            Case "DeleteUser"
                Database.System.Execute("DELETE FROM MobileCarrier  WHERE ID =" + e.CommandArgument.ToString())
                BindGridView()
            Case "EditUser"
                hdnID.Value = e.CommandArgument.ToString()
                lblHeading.Text = "Edit Mobile Carrier"
                btnSave.Text = "Update"
                _fillFields(e.CommandArgument.ToString())
                mvMobileCarrier.ActiveViewIndex = 1
            Case Else

        End Select
    End Sub

    Protected Sub btnbtnAdd_Click(sender As Object, e As System.EventArgs) Handles btnAdd.Click
        _clearFields()
        lblHeading.Text = "Add Mobile Carrier"
        btnSave.Text = "Save"
        mvMobileCarrier.ActiveViewIndex = 1
    End Sub

    Protected Sub btnbtnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        _clearFields()
       
        mvMobileCarrier.ActiveViewIndex = 0
    End Sub

    Protected Sub btnbtnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        If btnSave.Text = "Save" Then
            If (Database.System.Execute("INSERT INTO MobileCarrier(CarrierName,Gateway)VALUES({0},{1})".SqlFormatString(txtName.Text, txtGateway.Text)) > 0) Then
                BindGridView()
                'mvMobileCarrier.ActiveViewIndex = 0
                RunScript("alertOnSave()")
            Else
                Alert("Insertion Failed")
            End If
        ElseIf btnSave.Text = "Update" Then

            If (Database.System.Execute("UPDATE MobileCarrier SET CarrierName={1},Gateway={2}  WHERE id={0}".SqlFormatString(hdnID.Value, txtName.Text, txtGateway.Text)) > 0) Then
                BindGridView()
                RunScript("alertOnUpdate()")
            Else
                Alert("Insertion Failed")
            End If
        End If
    End Sub

    Private Sub _clearFields()
        txtName.Text = ""
        txtGateway.Text = ""
    End Sub

    Private Sub _fillFields(id As String)
        Dim _dtCarrier As DataTable
        _dtCarrier = Database.System.GetDataTable("SELECT *  FROM MobileCarrier where ID=" + id)
        If (_dtCarrier.Rows.Count > 0) Then
            txtName.Text = _dtCarrier.Rows(0)("CarrierName").ToString()
            txtGateway.Text = _dtCarrier.Rows(0)("Gateway").ToString()
        End If
    End Sub
End Class
