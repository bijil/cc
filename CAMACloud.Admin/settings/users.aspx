﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ClientAdmin.master" CodeBehind="users.aspx.vb" Inherits="CAMACloud.Admin.users" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function showPopup(title, isEdit) {
            $('.user-edit-panel').html('');
            
            $('.edit-popup').dialog({
                modal: true,
                width: 825,
                height: 500,
                resizable: false,
                title: title,
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                    setTimeout(() => {
                        let confirmRow = $('.confirmRow'), passwordRow = $('.passwordRow'), newPassRow = $('.newPassRow');

                        if (isEdit == 1) {
                            passwordRow.hide();
                            confirmRow.hide();
                            newPassRow.show();
                        } else {
                            passwordRow.show();
                            confirmRow.show();
                            newPassRow.hide();
                        }
                    }, 0);
                }
            });
        }

        function hidePopup() {
            $('.edit-popup').dialog('close');
        }

        function checkValid(evt) {
            const value = evt.target.value;
            
            if (value != $('.confirmPassword').val()) {
                $('.confirmPassword + .password-indicator').addClass("pass__invalid");
                $('.confirmPassword + .password-indicator').removeClass("pass__valid");
            }
            else if (value == '') {
                $('.confirmPassword + .password-indicator').removeClass("pass__invalid");
                $('.confirmPassword + .password-indicator').removeClass("pass__valid");
            }
            else {
                $('.confirmPassword + .password-indicator').removeClass("pass__invalid");
                $('.confirmPassword + .password-indicator').addClass("pass__valid");
            }

            updateConditions(value.trim());

            if (checkPwdRegex(value)) {
                $('.userPassword + .password-indicator').addClass("pass__valid").removeClass("pass__invalid");
                $('.confirmPassword').prop("disabled", false);
            } else {
                $('.userPassword + .password-indicator').addClass("pass__invalid").removeClass("pass__valid");
                $('.confirmPassword').prop("disabled", true);
            }

            if (!value) {
                $('.userPassword + .password-indicator').removeClass('pass__invalid');
                $('.confirmPassword + .password-indicator').removeClass('pass__invalid');
            }

            if (value != '') {
                $('#MainContent_MainContent_rfvPassword').css('display', 'none');
            }
            else {
                $('#MainContent_MainContent_rfvPassword').css('display', 'inline');
            }
        }

        function checkPwdRegex(value) {
            const pwdRegex = /^(?=.{8,}$)(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])(?=.*?\W).*$/;
            return pwdRegex.test(value);
        }

        function updateConditions(value) {
            const minreg = /^.{8,}$/;
            const upreg = /^(?=.*?[A-Z]).*$/;
            const lowreg = /^(?=.*?[a-z]).*$/;
            const numreg = /^(?=.*?[0-9]).*$/;
            const spreg = /^(?=.*?\W).*$/;

            minreg.test(value) ? $('#min8 .req__icon').addClass('req__icon--valid') : $('#min8 .req__icon').removeClass('req__icon--valid');
            upreg.test(value) ? $('#atUp .req__icon').addClass('req__icon--valid') : $('#atUp .req__icon').removeClass('req__icon--valid');
            numreg.test(value) ? $('#atNum .req__icon').addClass('req__icon--valid') : $('#atNum .req__icon').removeClass('req__icon--valid');
            lowreg.test(value) ? $('#atLow .req__icon').addClass('req__icon--valid') : $('#atLow .req__icon').removeClass('req__icon--valid');
            spreg.test(value) ? $('#atSp .req__icon').addClass('req__icon--valid') : $('#atSp .req__icon').removeClass('req__icon--valid');
        }

        function checkInputConfirm(evt) {
            const value = $('.userPassword').val();
            console.log("Password=" + value);
            console.log("Confirm Password=" + evt.target.value);
            if (value != evt.target.value) {
                $('.confirmPassword + .password-indicator').addClass("pass__invalid").removeClass("pass__valid");
            } else {
                $('.confirmPassword + .password-indicator').addClass("pass__valid").removeClass("pass__invalid");
            }
        }

        function checkNewPassword(evt) {
            const value = evt.target.value;
            const password = $(evt.target), passIcon = $('.newPassword + .password-indicator');

            updateConditions(value.trim());

            if (checkPwdRegex(value)) {
                passIcon.addClass("pass__valid").removeClass("pass__invalid");
            } else {
                passIcon.addClass("pass__invalid").removeClass("pass__valid");
            }

            if (!value) {
                passIcon.removeClass('pass__invalid');
            }
        }

        function validatePassword() {
            let isEdit = $('.userPassword').prop('disabled') && $('.confirmPassword').prop('disabled')
            let isValid;
            if (isEdit)
                isValid = !$('.newPassword + .password-indicator').hasClass('pass__invalid')
            else {
                isValid = $('.userPassword + .password-indicator').hasClass('pass__valid') && $('.confirmPassword + .password-indicator').hasClass('pass__valid')
                if (!$('.userPassword + .password-indicator').hasClass('pass__valid')) $('.userPassword + .password-indicator').addClass('pass__invalid')
                if (!$('.confirmPassword + .password-indicator').hasClass('pass__valid')) $('.confirmPassword + .password-indicator').addClass('pass__invalid')
            }
            return isValid;
        }

        $(document).on('keyup', '.userPassword', checkValid);
        $(document).on('keyup', '.confirmPassword', checkInputConfirm);
        $(document).on('keyup', '.newPassword', checkNewPassword);
    </script>
    <style>
        .user-edit-div {
            position: relative;
        }
        .reqs {
            border: 1px solid #ddd;
            /* background: #fff; */
            padding: 4px 6px;
            bottom: 0;
            right: 0;
        }

        .reqs p {
            margin: 0px 0 5px;
            display: flex;
        }

        .reqs p:last-child {
            margin: 0;
        }
        
        .req__icon {
            position: relative;
            display: block;
            width: 15px;
            margin-right: 6px;
            box-sizing: border-box;
            background: #d50000;
            border-radius: 100%;
            padding: 8px;
        }
        
        .req__icon:before, .req__icon:after {
            position: absolute;
            left: 0;
            top: 3px;
            content: ' ';
            margin: 0 auto;
            right: 0;
            height: 10px;
            width: 2px;
            background-color: #ffffff;
            display: block;
        }

        .req__icon:before {
            transform: rotate(45deg);
        }

        .req__icon:after {
            transform: rotate(-45deg);
        }

        .req__icon--valid {
            background-color: #00b94c;
        }
        
        .req__icon--valid::before {
            transform: rotate(45deg);
            height: 8px;
            width: 4px;
            border-bottom: 2px solid #ffffff;
            border-right: 2px solid #ffffff;
            top: 2px;
            background: transparent;
        }

        .req__icon--valid::after {
            display: none;
        }

        .password-indicator{
            position: relative;
            width: 18px;
            height: 10px;
            margin-top: 0;
            display: inline-block;
        }

        .password-indicator::before, 
        .password-indicator::after {
            position: absolute;           
        }

        .pass__invalid {
            /* position: relative;
            display: block;
            width: 15px;
            margin-right: 6px;
            box-sizing: border-box;
            background: #d50000;
            border-radius: 100%;
            padding: 8px; */
        }
        
        .pass__invalid::before,
        .pass__invalid::after {
            content: ' '; 
            top: 0;
            margin: 0 auto;
            left: 0;
            height: 14px;
            width: 2px;
            background-color: #d50000;
            display: inline-block;
        }

        .pass__invalid:before {
            transform: rotate(45deg);
        }

        .pass__invalid:after {
            transform: rotate(-45deg);
        }

        .pass__valid {       
            height: 12px; 
        }

        .pass__valid::before {
            content: '';
            transform: rotate(45deg);
            height: 10px;
            width: 5px;
            border-bottom: 3px solid #00b94c;
            border-right: 3px solid #00b94c;
            top: 0px;
            left: -2px;
        }
        

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Manage Users</h1>
    <asp:UpdatePanel runat="server" ID="uplGrid">
        <ContentTemplate>
            <div class="link-panel">
                <asp:LinkButton runat="server" ID="lbNewUser" Text="Add New User" OnClientClick="showPopup('Create new CAMA user',0);" />
            </div>
            <asp:GridView runat="server" ID="grid" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField>
                        <ItemStyle Width="25px" />
                        <ItemTemplate>
                            <%# Container.DataItemIndex + 1 %>.
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Full Name">
                        <ItemStyle Width="200px" />
                        <ItemTemplate>
                            <%# Eval("FirstName") + " " + Eval("LastName")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Login Id" DataField="LoginId">
                        <ItemStyle Width="120px"></ItemStyle>
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Status">
                        <ItemStyle Width="80px" />
                        <ItemTemplate>
                            <%#UserStatus() %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Last Login">
                        <ItemStyle Width="160px" />
                        <ItemTemplate>
                            <%#UserLastLoginDate %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Last Login IP" ItemStyle-Width="120px" Visible="false">
                        <ItemTemplate>
                            <%# Eval("LoginId")%>
                        </ItemTemplate>
                        <ItemStyle Width="120px"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemStyle Width="140px" />
                        <ItemTemplate>
                            <asp:LinkButton runat="server" CommandName="EditUser" Text="Edit" CommandArgument='<%# Eval("LoginId") %>' OnClientClick="showPopup('Edit User Properties',1)" />
                            <asp:LinkButton ID="lbtnDeleteUser" Text="Delete" runat="server" CommandName="DeleteUser"  OnClientClick="return confirm('Are you sure you want to delete this user?');"
                                CommandArgument='<%# Eval("LoginId")%>' CausesValidation="false"></asp:LinkButton>
                            <asp:LinkButton ID="lbtnUnlock" Text="Unlock" runat="server" CommandName="UnlockUser" CommandArgument='<%# Eval("LoginId")%>' CausesValidation="false" Visible='<%#IsUserLocked() %>'></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    No users found.
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="edit-popup" style="display: none;">
        <asp:UpdatePanel ID="uplEdit" runat="server">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlUserEdit" CssClass="user-edit-panel" Style="width: 800px; height: 400px;">
                    <asp:HiddenField ID="hdnUserId" runat="server" />
                    <table class="user-edit-table">
                        <tr>
                            <td style="width: 8px;"></td>
                            <td>
                                <h3 style="margin: 8px 0;">User Properties</h3>
                                <div class="user-edit-div">
                                    <table class="user-edit-form">
                                        <tr>
                                            <td>First Name: </td>
                                            <td>
                                                <asp:TextBox ID="txtFirstName" runat="server" autocomplete="off"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvtxtFirstName" runat="server" ForeColor="Red" Display="Dynamic" ErrorMessage="*" ControlToValidate="txtFirstName"
                                                    ValidationGroup="UserProp"></asp:RequiredFieldValidator>
                                                <%-- <asp:RequiredFieldValidator
                                                    ID="rfvtxtFirstNameForEdit" runat="server" ForeColor="Red" Display="Dynamic" ErrorMessage="*" ControlToValidate="txtFirstName"
                                                    ValidationGroup="UserProp2"></asp:RequiredFieldValidator>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Middle Name </td>
                                            <td>
                                                <asp:TextBox ID="txtMiddleName" runat="server"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator
												    ID="MiddleNameValidator" runat="server" ForeColor="Red" ErrorMessage="*" ControlToValidate="txtMiddleName"
												    ValidationGroup="UserProp"></asp:RequiredFieldValidator>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Last Name </td>
                                            <td>
                                                <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Email: </td>
                                            <td>
                                                <asp:TextBox ID="txtUserMail" runat="server" autocomplete="off"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvtxtUserMail" runat="server" ForeColor="Red" ErrorMessage="*" ControlToValidate="txtUserMail" Display="Dynamic"
                                                    ValidationGroup="UserProp"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revtxtUserMail" runat="server" ControlToValidate="txtUserMail" ErrorMessage="Invalid Email!" ForeColor="#D20B0E"
                                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic" ValidationGroup="UserProp"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Mobile Number </td>
                                            <td>
                                                <asp:TextBox ID="txtMobileNumber" runat="server"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="revtxtMobileNumber" runat="server" ForeColor="#D20B0E" ControlToValidate="txtMobileNumber" ErrorMessage="Invalid MobileNo"
                                                    ValidationExpression="\d{10,11}" ValidationGroup="UserProp" Display="Dynamic"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <%--<tr>
                                            <td>Mobile Carrier </td>
                                            <td>
                                                <asp:DropDownList ID="ddlMobileCarrier" runat="server" Width="188px" Height="22px">
                                                    <asp:ListItem Text="AT & T" Value="at&t"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="rfvddlMobileCarrier" runat="server" ForeColor="Red" ErrorMessage="*" ControlToValidate="ddlMobileCarrier" Display="Dynamic"
                                                    InitialValue="--Select--" ValidationGroup="UserProp"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>--%>
                                        <tr>
                                            <td>Login ID: </td>
                                            <td>
                                                <asp:TextBox ID="txtLoginId" runat="server" autocomplete="off"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="UserNameRequiredFieldValidator" runat="server" ForeColor="Red" ErrorMessage="*" ControlToValidate="txtLoginId"
                                                    Display="Dynamic" ValidationGroup="UserProp"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr id="userPwd" runat="server" class="passwordRow">
                                                <td>Password: </td>
                                                <td style="position: relative;">
                                                    <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" autocomplete="off" CssClass="userPassword" MaxLength="30"> </asp:TextBox>
                                                    <span class="password-indicator"></span>
                                                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ForeColor="Red" ErrorMessage="" ControlToValidate="txtPassword" Display="Dynamic" ValidationGroup="UserProp" Style="margin-left:-21px;"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr id="userCPwd" runat="server" class="confirmRow">
                                                <td>Confirm Password: </td>
                                                <td style="position: relative;">
                                                    <asp:TextBox ID="txtPassword2" TextMode="Password" runat="server" autocomplete="off" CssClass="confirmPassword" MaxLength="30"></asp:TextBox>
                                                    <span class="password-indicator"></span>
                                                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassword" ControlToValidate="txtPassword2" ErrorMessage="" Display="Dynamic" ForeColor="#D20B0E" ValidationGroup="UserProp"></asp:CompareValidator>
                                                </td>
                                            </tr>
                                            <tr id="newPwd" runat="server" class="newPassRow">
                                                <td>New Password: </td>
                                                <td style="position: relative;">
                                                    <asp:TextBox ID="txtNewPassword" CssClass="txtNP newPassword" TextMode="Password" runat="server" autocomplete="off" MaxLength="30"></asp:TextBox>
                                                    <span class="password-indicator"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                
                                                </td>
                                            </tr>
                                     </table>
                                     <div class="reqs">
                                        <p>Password must satisfy:</p>
                                        <p id = "min8">
                                            <i class="req__icon"></i>
                                            <span>Minimum 8 Characters</span>
                                        </p>
                                        <p id = "atUp" >
                                            <i class="req__icon"></i>
                                            <span>At least 1 Uppercase alphabet</span>
                                        </p>
                                        <p id = "atLow">
                                            <i class="req__icon"></i>
                                            <span>At least 1 Lowercase alphabet</span>
                                        </p>
                                        <p id = "atNum">
                                            <i class="req__icon"></i>
                                            <span>At least 1 Digit</span>
                                        </p>
                                        <p id = "atSp">
                                            <i class="req__icon"></i>
                                            <span>At least 1 Special character</span>
                                        </p>
                                    </div>
                                </div>
                                <div style="margin-top: 10px; margin-bottom: 15px;">
                                    <asp:Button runat="server" ID="btnSaveUser" Text="Save User" OnClientClick="return validatePassword()" ValidationGroup="UserProp2" />
                                    <asp:Button runat="server" ID="btnCancel" Text="Cancel" OnClientClick="hidePopup();" />
                                </div>
                            </td>
                            <td class="v-split" style="width: 12px;"></td>
                            <td style="width: 300px;">
                                <table>
                                    <tr>
                                        <td>
                                            <h3>
                                                Select Roles</h3>
                                            <%--<asp:CheckBoxList ID="cblRoles1" runat="server" Style="margin-left: -3px">
                                                <asp:ListItem Text="Admin" Value="admin"></asp:ListItem>
                                                <asp:ListItem Text="Dcs-support" Value="dcs-support"></asp:ListItem>
                                            </asp:CheckBoxList>--%>
                                            <asp:RadioButtonList ID="rblRoles4" runat="server" Style="margin-left: -3px">
                                                <asp:ListItem Text="Admin" Value="admin"></asp:ListItem>
                                                <asp:ListItem Text="Support" Value="support"></asp:ListItem>
                                                <asp:ListItem Text="Product Management" Value="ProductManagement"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <%--<tr>
                                        <td style="margin-top: 20px;">
                                            <h3>
                                                Other Options</h3>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="cbNotifybyEmail" Text="Send Notification Via Email" Checked="true" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="cbNotifybySMS" Text="Send Notification Via SMS" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="cbAutoQC" Text="Compliance For Auto-QC" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chkLocked" Text="User access is locked" runat="server" />
                                        </td>
                                    </tr>--%>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
