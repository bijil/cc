﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/ClientAdmin.master" AutoEventWireup="false"
    Inherits="CAMACloud.Admin.settings_mobilecarrier" Codebehind="mobilecarrier.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript">
        function alertOnSave() {
            alert('New carrier has been successfully added.');
            document.getElementById('btnCancel').click();
        }
        function alertOnUpdate() {
            alert('Carrier gateway has been updated.');
            document.getElementById('btnCancel').click();
        }
    </script>
    <div>
        <asp:UpdatePanel ID="updl" runat="server">
            <ContentTemplate>
                <asp:MultiView ID="mvMobileCarrier" runat="server" ActiveViewIndex="0">
                    <asp:View ID="View1" runat="server">
                        <h1>
                            Mobile Carriers Registered as SMS Gateways</h1>
                        <table>
                            <tr>
                                <td>
                                    <asp:Button ID="btnAdd" Text="Add New" runat="server" Style="float: right;" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="gdvUserDetails" runat="server" AutoGenerateColumns="false">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemStyle Width="18px" />
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>.
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Name" DataField="CarrierName" ItemStyle-Width="140px">
                                                <ItemStyle Width="120px"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Gateway" DataField="Gateway" ItemStyle-Width="220px">
                                                <ItemStyle Width="120px"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemStyle Width="90px" />
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EditUser" Text="Edit" CommandArgument='<%# Eval("ID") %>' />
                                                    <asp:LinkButton ID="lbtnDeleteUser" Text="Delete" runat="server" CommandName="DeleteUser" CommandArgument='<%# Eval("ID")%>' CausesValidation="false" OnClientClick="return confirm('Are you sure you want to delete this carrier gateway?')"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <h1>
                            <asp:Label ID="lblHeading" runat="server" Text="Label"></asp:Label>
                        </h1>
                        <asp:HiddenField ID="hdnID" runat="server" />
                        <table>
                            <tr>
                                <td>Name</td>
                                <td>
                                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox>&nbsp;
                                    <asp:RequiredFieldValidator ID="rfvtxtName" runat="server" ForeColor="Red" Display="Dynamic" ErrorMessage="*" ControlToValidate="txtName" ValidationGroup="UserProp"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>Gateway</td>
                                <td>
                                    <asp:TextBox ID="txtGateway" runat="server"></asp:TextBox>&nbsp;
                                    <asp:RequiredFieldValidator ID="rfvtxtGateway" runat="server" ForeColor="Red" Display="Dynamic" ErrorMessage="*" ControlToValidate="txtGateway"
                                        ValidationGroup="UserProp"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="UserProp" />
                                </td>
                                <td>
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" ClientIDMode="Static" />
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
