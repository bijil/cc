﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ClientAdmin.master" CodeBehind="sqlexec.aspx.vb" Inherits="CAMACloud.Admin.sqlexec" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.min.css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
	<script type="text/javascript" src="/App_Static/jslib/jquery.min.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
    <script type="text/javascript">
        $('document').ready(function () {
            $('#<%= countyList.ClientID %>').select2({ multiple: true, placeholder: { id: '', text: 'None Selected' } }).val([]).trigger('change');
            $('#<%= chlCAMASystem.ClientID %>').select2({ multiple: true, placeholder: { id: '', text: 'None Selected' } }).val([]).trigger('change');
        });

        function validateList() {
            $('#MainContent_MainContent_selectedcountyList').val('');
            if ($('#MainContent_MainContent_rblTarget input[type=radio]:checked').val() == 'E') {
                let v = $('.ddl-county').val().toString();
                if (v == "") {
                    alert("Please select at least one county!");
                    return false;
                }
                $('#MainContent_MainContent_selectedcountyList').val(v);
            }
            else if ($('#MainContent_MainContent_rblTarget input[type=radio]:checked').val() == 'C') {
                let v = $('.ddl-camasystem').val().toString();
                if (v == "") {
                    alert("Please select at least one CAMASystem!");
                    return false;
                }
                $('#MainContent_MainContent_CAMASystemList').val(v);
            }
            return true;
        }
    </script>
    <style type="text/css">
        select {
            width: 200px;
        }

        input[type="text"] {
            width: 196px;
        }

        input[type="submit"] {
            margin-top: 35px;
        }

        td span[sep] {
            margin-left: 15px;
            float: right;
        }

        .select2-container {
          min-width: 400px;
        }
        
        .select2-results__option {
          padding-right: 20px;
          vertical-align: middle;
        }

        .select2-results__option:before {
          content: "";
          display: inline-block;
          position: relative;
          height: 20px;
          width: 20px;
          border: 2px solid #e9e9e9;
          border-radius: 4px;
          background-color: #fff;
          margin-right: 20px;
          vertical-align: middle;
        }

        .select2-results__option[aria-selected=true]:before {
          font-family:fontAwesome;
          content: "\f00c";
          color: #fff;
          background-color: #f77750;
          border: 0;
          display: inline-block;
          padding-left: 5px;
          padding-top: 5px;
        }

        .select2-container--default .select2-results__option[aria-selected=true] {
        	background-color: #fff;
        }

        .select2-container--default .select2-results__option--highlighted[aria-selected] {
        	background-color: #eaeaeb;
        	color: #272727;
        }

        .select2-container--default .select2-selection--multiple {
        	margin-bottom: 10px;
        }
        .select2-container--default.select2-container--open.select2-container--below .select2-selection--multiple {
        	border-radius: 4px;
        }

        .select2-container--default.select2-container--focus .select2-selection--multiple {
        	border-color: #f77750;
        	border-width: 2px;
        }

        .select2-container--default .select2-selection--multiple {
        	border-width: 2px;
        }

        .select2-container--open .select2-dropdown--below {
        	
        	border-radius: 6px;
        	box-shadow: 0 0 10px rgba(0,0,0,0.5);
        
        }

        .select2-selection .select2-selection--multiple:after {
        	content: 'hhghgh';
        }
        
        .select-icon .select2-selection__placeholder .badge {
        	display: none;
        }

        .select-icon .select2-results__option:before,
        .select-icon .select2-results__option[aria-selected=true]:before {
        	display: none !important;
        }

        .select-icon  .select2-search--dropdown {
        	display: none;
        }

        .validate {
            border: 1px solid red !important;
        }

        .info {
            width: 800px !important;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Mass Update All Environments</h1>
    <table>
        <tr>
            <td style="width:600px;">
                <asp:RadioButtonList runat="server" ID="rblTarget" RepeatDirection="Horizontal" AutoPostBack="true">
                    <asp:ListItem Value="*" Text="All" Selected="True"/>
                    <asp:ListItem Value="D" Text="Beta Only" />
                    <asp:ListItem Value="T" Text="Samples" />
                    <asp:ListItem Value="SB" Text="SandBoxes" />
                    <asp:ListItem Value="S" Text="Selected" />
                    <asp:ListItem Value="E" Text="Environments" />
                    <asp:ListItem Value="C" Text="CAMASystem" />
                </asp:RadioButtonList>
                <div style="padding:3px 15px">
                    <asp:CheckBoxList runat="server" ID="chlHosts" />
                </div>
                <div style="padding:3px 15px">
                    <asp:DropDownList ID="chlCAMASystem" runat="server" CssClass="ddl-camasystem"></asp:DropDownList>
                    <asp:HiddenField ID="CAMASystemList" runat="server" />
                </div>
                <div style="padding:3px 15px">
                    <asp:DropDownList ID="countyList" runat="server" CssClass="ddl-county"></asp:DropDownList>
                    <asp:HiddenField ID="selectedcountyList" runat="server" />
                </div>
                <asp:CheckBox runat="server" ID="chkQueueOnly" AutoPostBack="true" Text="Queue only (no-execution)" /><br />
                <asp:CheckBox runat="server" ID="chkDoNotQueue" AutoPostBack="true" Text="Do not queue" />
            </td>
            <td>
                <table>
                    <tr>
                        <td style="width:100px;padding-top:4px;">
                            Provided by: <asp:RequiredFieldValidator runat="server" ControlToValidate="txtUpdatedBy" ErrorMessage="*"  ValidationGroup="Enqueue" />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtUpdatedBy" Width="180px"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            Notes: <asp:RequiredFieldValidator runat="server" ControlToValidate="txtNotes" ErrorMessage="*"  ValidationGroup="Enqueue"/> <br />
                            <asp:TextBox runat="server" ID="txtNotes" TextMode="MultiLine" Width="300px" Rows="4" />
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            <asp:Button runat="server" ID="btnUpdate" Text=" Update Environments " ValidationGroup="Enqueue" OnClientClick="return validateList();" />
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblStatus" Font-Bold="true" /><br /><br />
                            <asp:Label runat="server" ID="lblError" ForeColor="Red" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:TextBox runat="server" ID="txtSql" TextMode="MultiLine" Rows="40" Columns="120" Font-Names="Lucida Console" /><br />
</asp:Content>
