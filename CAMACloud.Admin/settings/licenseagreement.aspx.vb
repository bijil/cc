﻿Public Class AgreementContent
    Inherits System.Web.UI.Page
    Dim fileName As String
    Dim vendorId As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ddlVendor.DataSource = Database.System.GetDataTable("select Id,Name from Vendor")
            ddlVendor.DataTextField = "Name"
            ddlVendor.DataValueField = "Id"
            ddlVendor.DataBind()
            FillVendor()
            ViewAgreement()
            tickValue.Value = Now.Ticks
        End If

    End Sub

    Private Sub FillVendor()
        Dim vendorId As Integer = Request.QueryString("Vendorid")
        Try
            If vendorId = 0 Then
               
            Else
                ddlVendor.SelectedValue = vendorId
            End If
        Catch ex As Exception

        End Try
    End Sub


    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Try
            If Database.System.Execute("UPDATE Vendor SET AgreementContent={0},VendorInfo={1} where id={2}".SqlFormatString(txtAgreement.Text, txtVendorInfo.Text, ddlVendor.SelectedValue)) > 0 Then
                Alert("Changes has been updated successfully.")
                ViewAgreement()
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnReset_Click(sender As Object, e As System.EventArgs) Handles btnReset.Click
        txtAgreement.Text = ""
        logoMA5.Controls.Clear()
    End Sub
    Private Sub ViewAgreement()
        txtAgreement.Text = Database.System.GetDataTable("select  AgreementContent from Vendor where id={0}".SqlFormatString(ddlVendor.SelectedValue)).Rows(0)("AgreementContent").ToString()
        txtVendorInfo.Text = Database.System.GetDataTable("select  VendorInfo from Vendor where id={0}".SqlFormatString(ddlVendor.SelectedValue)).Rows(0)("VendorInfo").ToString()
        Dim dbFileName As String = Database.System.GetDataTable("select VendorLogoPath from Vendor where id={0}".SqlFormatString(ddlVendor.SelectedValue)).Rows(0)("VendorLogoPath").ToString()
        If dbFileName IsNot Nothing And dbFileName <> "" Then
            Dim pathSplit As String() = dbFileName.Split("/")
            fileName = pathSplit(3)
        End If
    End Sub
    Private Sub ddlVendor_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlVendor.SelectedIndexChanged
        ViewAgreement()
    End Sub
    Protected Sub txtAgreement_TextChanged(sender As Object, e As EventArgs) Handles txtAgreement.TextChanged
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        VendorInfo._id = ddlVendor.SelectedValue
        Dim btn As Button = sender
        Dim upload As FileUpload = Nothing
        Dim tag As String = Nothing
        Dim minWidth = 0, minHeight = 0, maxWidth = 0, maxHeight = 0
        Dim s3Path As String = HttpContext.Current.GetCAMASession.VendorId + "/assets/logo-"
        upload = FileMA5
        tag = "ma-vendor-logo"
        'change size based on req testing purpose given by haresh
        minHeight = 10
        minWidth = 31
        maxHeight = 1000
        maxWidth = 3180
        If Not String.IsNullOrEmpty(tag) AndAlso upload IsNot Nothing AndAlso upload.HasFile Then
            fileName = upload.FileName
            Dim ext As String = IO.Path.GetExtension(fileName)
            'change by haresh s3path for testing
            s3Path = s3Path + tag + "/" + fileName
            Dim s3 As New S3FileManager
            Try
                Dim bmp As New Drawing.Bitmap(upload.FileContent)
                If (minWidth > 0 AndAlso bmp.Width < minWidth) Or (minHeight > 0 AndAlso bmp.Height < minHeight) Or (maxHeight > 0 AndAlso bmp.Height > maxHeight) Or (maxWidth > 0 AndAlso bmp.Width > maxWidth) Then
                    Throw New Exception("Uploaded image does not match the dimension conditions.")
                End If

                FileMA5.SaveAs(Server.MapPath("/App_Static/images/" + fileName))

                s3.UploadFile(upload.FileContent, s3Path)
                VendorInfo.MAVendorLogo = s3Path
            Catch ex As Exception
                Alert(ex.Message)
            End Try
        Else
        End If

    End Sub
    Protected Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        logoMA5.Style.Add("background-image", "url(/App_Static/images/" + fileName + ")")

    End Sub

End Class