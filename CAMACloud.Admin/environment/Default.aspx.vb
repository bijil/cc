﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Microsoft.SqlServer.Management.Smo
Imports Microsoft.SqlServer.Management.Common
Imports System.IO
Imports System.Data.SqlClient
Imports Microsoft.SqlServer.Management.Sdk.Sfc
Imports System.Text
Imports System.Collections.Specialized
Imports System.Data.SqlTypes


Public Class _Default2
    Inherits System.Web.UI.Page
    'Dim myServer As New Server()
    'Dim myDatabase As Database = myServer.Databases("CCMODEL")
    Shared myServer As New Server()
    Shared myDatabase As Database
    Public Shared envName, county, city, state, vendor, hostKey, dbServer, dbName, initializeSize, growth, repeat, day, time, hostingStage, CAMAsystem, notes, isProduction As String
    Public Shared creationId As Integer
    Public Shared path, tablePath, Host, ServerId, DBUser, DBPassword As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            bindVendor()
            bindServer()
            bindHost()
            ddlRepeat.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlDay.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlTime.Items.Insert(0, New ListItem("--Select--", "0"))
        End If
    End Sub
    Private Sub bindVendor()
        ddlVendor.DataSource = Data.Database.System.GetDataTable("SELECT Id,Name FROM Vendor")
        ddlVendor.DataTextField = "Name"
        ddlVendor.DataValueField = "Id"
        ddlVendor.DataBind()
        ddlVendor.Items.Insert(0, New ListItem("--Select Vendor--", "0"))
    End Sub

    Private Sub LoadCAMASystem() Handles ddlVendor.SelectedIndexChanged
        ddlCAMASystem.FillFromSqlWithDatabase(Data.Database.System, "SELECT c.ID,c.Name FROM Vendor v LEFT JOIN CAMASystem c ON v.id=c.VendorId WHERE c.VendorId =" + ddlVendor.SelectedItem.Value + " ORDER BY Name", True, "-- Select CAMASystem --", ID)
    End Sub
    Private Sub bindServer()
        ddlDBServer.DataSource = Data.Database.System.GetDataTable("SELECT Id,Name FROM DatabaseServer")
        ddlDBServer.DataTextField = "Name"
        ddlDBServer.DataValueField = "Id"
        ddlDBServer.DataBind()
        ddlDBServer.Items.Insert(0, New ListItem("--Select Server--", "0"))
    End Sub
    Private Sub bindHost()
        ddlHostingStage.DataSource = Data.Database.System.GetDataTable("SELECT Id,Name FROM ApplicationHost WHERE HostGroup IS NOT NULL")
        ddlHostingStage.DataTextField = "Name"
        ddlHostingStage.DataValueField = "Id"
        ddlHostingStage.DataBind()
        ddlHostingStage.Items.Insert(0, New ListItem("--Select Stage--", "0"))
    End Sub

    <System.Web.Services.WebMethod()>
    Public Shared Function CheckEnvName(ByVal envName As String) As Integer
        Dim EnvironmentNameCount As Integer = Data.Database.System.GetIntegerValue("SELECT COUNT(*) FROM Organization WHERE UPPER(Name)=UPPER('" + envName + "')")
        Return EnvironmentNameCount
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function CheckCounty(ByVal county As String) As Integer
        Dim countyCount As Integer = Data.Database.System.GetIntegerValue("select count(*) from Organization where upper(County) = upper('" + county + "')")
        Return countyCount
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function IsDbExists(ByVal databaseName As String) As Boolean
        Dim host As String
        Dim dt As DataTable = Data.Database.System.GetDataTable("SELECT Host FROM DatabaseServer")
        Dim count As Integer = 0

        If (Not dt Is Nothing) Then
            For Each dr As DataRow In dt.Rows
                host = dr.Item(0)
                count = count + Data.Database.System.GetIntegerValue("SELECT Count(*) FROM [" & host & "].[Master].[dbo].sysdatabases WHERE ('[' + name + ']' ={0} OR name ={0})".SqlFormatString(databaseName))
            Next
        End If
        If (count > 0) Then
            Return True
        Else
            Return False
        End If
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function IsHostKeyExists(ByVal hostKey As String) As Integer
        Dim keyCount As Integer = Data.Database.System.GetIntegerValue("select count(*) from Organization where MAHostKey = '" & hostKey & "'")
        Return keyCount
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function CheckEnvironmentCreationProcess(ByVal json As String) As String
        Dim query As String
        Try
            Dim jsonArry() As String
            jsonArry = json.Split("|")
            envName = jsonArry(0)
            county = jsonArry(1)
            city = jsonArry(2)
            state = jsonArry(3)
            vendor = jsonArry(4)
            hostKey = jsonArry(5)
            dbServer = jsonArry(6)
            dbName = jsonArry(7)
            initializeSize = jsonArry(8)
            growth = jsonArry(9)
            repeat = jsonArry(10)
            day = jsonArry(11)
            time = jsonArry(12)
            hostingStage = jsonArry(13)
            CAMAsystem = jsonArry(14)
            notes = jsonArry(15)
            isProduction = jsonArry(16)
            query = "SELECT COUNT(*) FROM EnvironmentCreationDetails WHERE( DatabaseName={0} OR EnvironmentName={1} OR CountyName={2}) AND STATUS='P'".SqlFormatString(dbName, envName, county)
            Dim count As Integer = Data.Database.System.GetIntegerValue(query)
            If count = 0 Then
                query = "INSERT INTO EnvironmentCreationDetails (EnvironmentName,CountyName,DatabaseServer,DatabaseName,ProcessStartTime,Status )VALUES({0},{1},{2},{3},{4},'P')".SqlFormatString(envName, county, dbServer, dbName, Date.UtcNow.ToString("yyyy-MM-dd HH:mm:ss"))
                Data.Database.System.Execute(query)
                creationId = Data.Database.System.GetIntegerValue("SELECT Id FROM EnvironmentCreationDetails WHERE EnvironmentName={0} AND CountyName={1} AND DatabaseServer={2} AND DatabaseName={3} AND Status='P' ".SqlFormatString(envName, county, dbServer, dbName))
                Return "notExist"
            Else
                Return "exist"
            End If
        Catch ex As Exception
            updateProcessError(ex.Message)
            Return ex.Message
        End Try
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function SetServerDetails() As String
        dbName = dbName.Trim()
        Try
            Host = Data.Database.System.GetStringValue("SELECT Host FROM DatabaseServer WHERE id={0}".SqlFormatString(dbServer))
            Dim serverDetails As DataRow = Data.Database.System.GetTopRow("SELECT id,DBUser, DBPassword FROM DatabaseServer WHERE host='" & Host & "'")
            If (Not serverDetails Is Nothing) Then
                ServerId = serverDetails.Item(0)
                DBUser = serverDetails.Item(1)
                DBPassword = serverDetails.Item(2)
            End If
            Return "True"
        Catch ex As Exception
            updateProcessError(ex.Message)
            Return ex.Message
        End Try
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function deleteFile() As String
        Try
            Dim rootFolderPath As String = "D:\SQLScript"
            Dim filesToDelete As String = "script*.sql"
            Dim fileList As String() = System.IO.Directory.GetFiles(rootFolderPath, filesToDelete)
            For Each file As String In fileList
                System.IO.File.Delete(file)
            Next
            Return "True"
        Catch ex As Exception
            updateProcessError(ex.Message)
            Return ex.Message
        End Try
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function GenerateScript() As String
        'Dim sourceConnectionString As String = "Server=10.0.0.100;Database=master;User Id=sa;Password=Cama@2014;"
        Dim systemConnection = ConfigurationManager.ConnectionStrings("ControlDB")
        If systemConnection Is Nothing Then
            Throw New Exception("ControlDB ConnectionString is not initialized. Check machine.config.")
        End If
        Dim controlDbConnectionString As String = systemConnection.ConnectionString
        'Dim sourceConnectionString As String = "Data Source=TN01DEVDW7007;Initial Catalog=master;Integrated Security=True" ''For Local Server
        Dim conn As New SqlConnection(controlDbConnectionString)
        Try
            Dim dbName As String
            dbName = "CCMODEL"
            If conn.State = ConnectionState.Closed Then
                conn.Open()
                conn.ChangeDatabase(dbName)
                Dim sc As New ServerConnection(conn)
                myServer = New Server(sc)
                myDatabase = myServer.Databases("CCMODEL")
            End If
            Return "True"
        Catch ex As Exception
            updateProcessError(ex.Message)
            Return ex.Message
        Finally
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function GenerateDBScript() As String
        Try
            Dim str As String
            str = "USE [master]" + System.Environment.NewLine + "GO"
            WriteToFile(str)
            Dim scripter As New Scripter(myServer)
            scripter.Options.IncludeHeaders = True
            Dim scriptCollection As StringCollection = scripter.Script(New Urn() {myDatabase.Urn})
            str = "GO"
            For Each script As String In scriptCollection
                WriteToFile(script)
                WriteToFile(str)
            Next
            Return "True"
        Catch ex As Exception
            updateProcessError(ex.Message)
            Return ex.Message
        End Try
    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function GenerateAssembliesScript() As String
        Try
            Dim str As String
            str = "USE [CCMODEL]" & System.Environment.NewLine & "GO"
            WriteToFile(str)
            Dim scriptOption As New ScriptingOptions()
            scriptOption.IncludeHeaders = True
            For Each assemblie As Microsoft.SqlServer.Management.Smo.SqlAssembly In myDatabase.Assemblies
                If Not assemblie.IsSystemObject Then
                    Dim sc As StringCollection = assemblie.Script(scriptOption)
                    Genarate(sc)
                End If
            Next
            Return "True"
        Catch ex As Exception
            updateProcessError(ex.Message)
            Return ex.Message
        End Try
    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function GenerateSchemasScript() As String
        Try
            Dim scripter As New Scripter(myServer)
            Dim myAdventureWorks As Database = myServer.Databases("CCMODEL")
            Dim scriptOption As New ScriptingOptions()
            scriptOption.IncludeHeaders = True
            For Each schemas As Microsoft.SqlServer.Management.Smo.Schema In myAdventureWorks.Schemas
                If Not schemas.IsSystemObject Then
                    Dim sc As StringCollection = schemas.Script(scriptOption)
                    Genarate(sc)
                End If
            Next
            Return "True"
        Catch ex As Exception
            updateProcessError(ex.Message)
            Return ex.Message
        End Try
    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function GenerateUserDefinedTableTypeScript() As String
        Try
            Dim scripter As New Scripter(myServer)
            Dim myAdventureWorks As Database = myServer.Databases("CCMODEL")
            Dim scriptOption As New ScriptingOptions()
            scriptOption.IncludeHeaders = True
            For Each UserDefinedTableType As Microsoft.SqlServer.Management.Smo.UserDefinedTableType In myAdventureWorks.UserDefinedTableTypes
                Dim sc As StringCollection = UserDefinedTableType.Script(scriptOption)
                Genarate(sc)
            Next
            Return "True"
        Catch ex As Exception
            updateProcessError(ex.Message)
            Return ex.Message
        End Try
    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function GenerateUserDefinedAggregatesScript() As String
        Try
            Dim scripter As New Scripter(myServer)
            Dim myAdventureWorks As Database = myServer.Databases("CCMODEL")
            Dim scriptOption As New ScriptingOptions()
            scriptOption.IncludeHeaders = True
            For Each userdefinedaggregate As Microsoft.SqlServer.Management.Smo.UserDefinedAggregate In myAdventureWorks.UserDefinedAggregates
                Dim sc As StringCollection = userdefinedaggregate.Script(scriptOption)
                Genarate(sc)
            Next
            Return "True"
        Catch ex As Exception
            updateProcessError(ex.Message)
            Return ex.Message
        End Try
    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function GenerateStoredProceduresScript() As String
        Try
            Dim scripter As New Scripter(myServer)
            Dim scriptOption As New ScriptingOptions()
            scriptOption.IncludeHeaders = True
            Dim sqlQuery As String = String.Format(" SELECT object_id FROM [CCMODEL].sys.procedures ORDER BY create_date")
            Dim dt As DataTable = Data.Database.System.GetDataTable(sqlQuery, 6000)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each dr As DataRow In dt.Rows
                    Dim id As String = dr("object_id").ToString()
                    Dim sp As Microsoft.SqlServer.Management.Smo.StoredProcedure = myDatabase.StoredProcedures.ItemById(id)
                    If Not sp.IsSystemObject Then
                        Dim sc As StringCollection = sp.Script(scriptOption)
                        Genarate(sc)
                    End If
                Next
            End If
            Return "True"
        Catch ex As Exception
            updateProcessError(ex.Message)
            Return ex.Message
        End Try
    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function GenerateUserDefinedFunctionsScript() As String
        Try
            Dim scripter As New Scripter(myServer)
            Dim scriptOption As New ScriptingOptions()
            scriptOption.IncludeHeaders = True
            For Each userdefinedfunction As Microsoft.SqlServer.Management.Smo.UserDefinedFunction In myDatabase.UserDefinedFunctions
                If Not userdefinedfunction.IsSystemObject Then
                    Dim sc As StringCollection = userdefinedfunction.Script(scriptOption)

                    Genarate(sc)
                End If
            Next
            Return "True"
        Catch ex As Exception
            updateProcessError(ex.Message)
            Return ex.Message
        End Try
    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function GenerateTableScriptWithDependencies() As String
        Try
            Dim scripter As New Scripter(myServer)
            Dim scriptOptions As New ScriptingOptions()
            scriptOptions.IncludeHeaders = True
            scriptOptions.AnsiPadding = True
            scriptOptions.DriPrimaryKey = True
            scriptOptions.DriUniqueKeys = True
            scriptOptions.DriAllConstraints = False
            scriptOptions.DriDefaults = False
            scriptOptions.NoCollation = True
            For Each myTable As Microsoft.SqlServer.Management.Smo.Table In myDatabase.Tables
                If Not myTable.IsSystemObject Then
                    Dim sc As StringCollection = myTable.Script(scriptOptions)

                    Genarate(sc)
                End If
            Next
            Return "True"
        Catch ex As Exception
            updateProcessError(ex.Message)
            Return ex.Message
        End Try
    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function GenerateViewsScript() As String
        Try
            Dim scriptOptions As New ScriptingOptions()
            scriptOptions.IncludeHeaders = True
            Dim sqlQuery As String = String.Format("SELECT object_id FROM [CCMODEL].sys.views ORDER BY create_date")
            Dim dt As DataTable = Data.Database.System.GetDataTable(sqlQuery, 6000)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each dr As DataRow In dt.Rows
                    Dim id As String = dr("object_id").ToString()
                    Dim vw As Microsoft.SqlServer.Management.Smo.View = myDatabase.Views.ItemById(id)

                    If Not vw.IsSystemObject Then
                        Dim sc As StringCollection = vw.Script(scriptOptions)
                        Genarate(sc)
                    End If
                Next
            End If
            Return "True"
        Catch ex As Exception
            updateProcessError(ex.Message)
            Return ex.Message
        End Try
    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function GenerateTableIndexesScript() As String
        Try
            Dim scripter As New Scripter(myServer)
            Dim scriptOption As New ScriptingOptions()
            scriptOption.IncludeHeaders = True
            scriptOption.AnsiPadding = True
            scriptOption.NonClusteredIndexes = True
            scriptOption.ClusteredIndexes = False
            For Each myTable As Microsoft.SqlServer.Management.Smo.Table In myDatabase.Tables
                For Each myIndex As Index In myTable.Indexes
                    ' Generating CREATE INDEX command for table indexes 
                    If Not myIndex.IsSystemObject And Not myIndex.IsClustered And Not myIndex.IsUnique Then
                        Dim sc As StringCollection = myIndex.Script(scriptOption)
                        Genarate(sc)
                    End If
                Next
            Next
            Return "True"
        Catch ex As Exception
            updateProcessError(ex.Message)
            Return ex.Message
        End Try
    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function GenerateTableScript() As String
        Try
            Dim scripter As New Scripter(myServer)
            Dim scriptOptions As New ScriptingOptions()
            scriptOptions.DriPrimaryKey = False
            scriptOptions.DriUniqueKeys = False
            scriptOptions.DriAllConstraints = False
            scriptOptions.DriDefaults = True
            scriptOptions.DriForeignKeys = True
            scriptOptions.DriChecks = True
            scriptOptions.Default = False
            For Each myTable As Microsoft.SqlServer.Management.Smo.Table In myDatabase.Tables
                If Not myTable.IsSystemObject Then
                    Dim sc As StringCollection = myTable.Script(scriptOptions)
                    Genarate(sc)
                End If
            Next
            Return "True"
        Catch ex As Exception
            updateProcessError(ex.Message)
            Return ex.Message
        End Try
    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function GenerateExtendedPropertiesScript() As String
        Try
            Dim scripter As New Scripter(myServer)
            Dim scriptOptions As New ScriptingOptions()
            scriptOptions.Default = False
            scriptOptions.ExtendedProperties = True
            For Each myTable As Microsoft.SqlServer.Management.Smo.Table In myDatabase.Tables
                If Not myTable.IsSystemObject Then
                    Dim sc As StringCollection = myTable.Script(scriptOptions)
                    Genarate(sc)
                End If
            Next
            Dim sqlQuery As String = String.Format("SELECT object_id FROM [CCMODEL].sys.views ORDER BY create_date")
            Dim dt As DataTable = Data.Database.System.GetDataTable(sqlQuery, 6000)
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each dr As DataRow In dt.Rows
                    Dim id As String = dr("object_id").ToString()
                    Dim vw As Microsoft.SqlServer.Management.Smo.View = myDatabase.Views.ItemById(id)
                    If Not vw.IsSystemObject Then
                        Dim sc As StringCollection = vw.Script(scriptOptions)
                        Genarate(sc)
                    End If
                Next
            End If
            Return "True"
        Catch ex As Exception
            updateProcessError(ex.Message)
            Return ex.Message
        End Try
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function ReplaceContentInsideScript() As String
        Try
            Dim rootFolderPath As String = "D:\SQLScript"
            Dim files As String = "script*.sql"
            Dim fileList As String() = System.IO.Directory.GetFiles(rootFolderPath, files)
            For Each file As String In fileList
                path = System.IO.Path.GetFullPath(file)
            Next
            Dim Source, newSource As String
            Dim sDelimStart, sDelimEnd, res As String
            Dim nIndexStart, nIndexEnd As String
            'Dim str As String = "FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\CCMODEL_log.ldf' , SIZE =" ''Local Server
            Dim str As String = "FILENAME = N'D:\CAMACloud Data\CCMODEL_log.ldf' , SIZE ="
            Using reader As New StreamReader(path)
                While Not reader.EndOfStream
                    Dim line As String = reader.ReadLine()
                    If line.Contains(str) Then
                        Source = line
                        Exit While
                    End If
                End While
            End Using
            If initializeSize <> "" Then
                Dim newInitialDBSize = initializeSize & "MB"
                sDelimStart = "SIZE ="
                sDelimEnd = " , MAXSIZE ="
                nIndexStart = Source.IndexOf(sDelimStart)
                nIndexEnd = Source.IndexOf(sDelimEnd)
                If nIndexStart > -1 AndAlso nIndexEnd > -1 Then
                    res = Strings.Mid(Source, nIndexStart + sDelimStart.Length + 1, nIndexEnd - nIndexStart - sDelimStart.Length)
                    newSource = Source.Replace(res, newInitialDBSize)
                    My.Computer.FileSystem.WriteAllText(path, My.Computer.FileSystem.ReadAllText(path).Replace(Source, newSource), False)
                    Source = newSource
                End If
            End If
            If growth <> "" Then
                sDelimStart = "FILEGROWTH ="
                sDelimEnd = "%)"
                nIndexStart = Source.IndexOf(sDelimStart)
                nIndexEnd = Source.IndexOf(sDelimEnd)
                If nIndexStart > -1 AndAlso nIndexEnd > -1 Then
                    res = Strings.Mid(Source, nIndexStart + sDelimStart.Length + 1, nIndexEnd - nIndexStart - sDelimStart.Length)
                    newSource = Source.Replace(res, growth)
                    My.Computer.FileSystem.WriteAllText(path, My.Computer.FileSystem.ReadAllText(path).Replace(Source, newSource), False)
                End If
            End If

            My.Computer.FileSystem.WriteAllText(path, My.Computer.FileSystem.ReadAllText(path).Replace("CCMODEL", dbName), False)
            Return "True"
        Catch ex As Exception
            updateProcessError(ex.Message)
            Return ex.Message
        End Try
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function CreateDB() As String
        Try
            Dim script As String
            Using streamReader = New StreamReader(path, Encoding.UTF8)
                script = streamReader.ReadToEnd()
            End Using
            Dim sqlConnectionString As String = "Server=" & Host & ";Database=master;User Id=sa;Password=" & DBPassword & ";"
            Dim conn As New SqlConnection(sqlConnectionString)
            Dim server As New Server(New ServerConnection(conn))
            server.ConnectionContext.ExecuteNonQuery(script)
            conn.Close()
            Return "True"
        Catch ex As Exception
            updateProcessError(ex.Message)
            Return ex.Message
        End Try
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function UpdateOrganization() As String
        Dim query As String
        Try
            query = "EXEC [CreateCounties]@Name={0},@City={1},@County= {2},@State={3},@ApplicationHostId={4},@DBHost= {5},@DBName={6},@DBUser={7},@DBPassword={8},@VendorId={9},@MAHostKey={10},@CAMASystem={11},@Notes={12},@isProduction={13}".SqlFormat(True, envName, city, county, state, hostingStage, Host, dbName, DBUser, DBPassword, vendor, hostKey, CAMAsystem, notes, isProduction)
            Data.Database.System.Execute(query)
            Dim OrganizationId As String
            ServerId = Data.Database.System.GetStringValue("SELECT Id FROM DatabaseServer WHERE host='" & Host & "'")
            OrganizationId = Data.Database.System.GetStringValue("SELECT Id FROM Organization WHERE DBHost='" & Host & "' and DBName='" & dbName & "'")
            query = String.Format("INSERT INTO BackupSchedule(ServerId,OrganizationId,DatabaseName,RepeatFactor,ScheduleDay,ScheduleTime) VALUES('" & ServerId & "','" & OrganizationId & "','" & dbName & "','" & repeat & "','" & day & "','" & time & "')")
            Data.Database.System.Execute(query)
            Return "True"
        Catch ex As Exception
            updateProcessError(ex.Message)
            Return ex.Message
        End Try
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function InsertTable() As String
        Try
            Dim vendorName As String = Data.Database.System.GetStringValue("SELECT name FROM Vendor WHERE id={0}".SqlFormatString(vendor))
            vendorName = vendorName.Trim()
            vendorName = vendorName.Replace(" ", "")
            Dim rootFolderPath As String = "D:\SQLScript"
            Dim files As String = "*script.sql"
            Dim fileList As String()
            Dim script As String
            fileList = System.IO.Directory.GetFiles(rootFolderPath, files)
            For Each file As String In fileList
                tablePath = System.IO.Path.GetFullPath(file)
                If tablePath.Contains(vendorName) Then
                    Exit For
                End If
            Next
            My.Computer.FileSystem.WriteAllText(tablePath, My.Computer.FileSystem.ReadAllText(tablePath).Replace("CCMODEL", dbName), False)
            Using streamReader = New StreamReader(tablePath, Encoding.UTF8)
                script = streamReader.ReadToEnd()
            End Using
            If script.IsEmpty = False Then
                Dim sqlConnString As String
                sqlConnString = "Server=" & Host & ";Database=" & dbName & ";User Id=sa;Password=" & DBPassword & ";"
                Dim con As New SqlConnection(sqlConnString)
                Dim NewServer As New Server(New ServerConnection(con))
                NewServer.ConnectionContext.ExecuteNonQuery(script)
            End If
            Return "True"
        Catch ex As Exception
            updateProcessError(ex.Message)
            Return ex.Message
        Finally
            My.Computer.FileSystem.WriteAllText(tablePath, My.Computer.FileSystem.ReadAllText(tablePath).Replace(dbName, "CCMODEL"), False)
        End Try
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function UpdateStatus() As String
        Dim query As String
        Try
            query = "UPDATE EnvironmentCreationDetails  SET ProcessEndTime=GETUTCDATE(),Status='C'  WHERE EnvironmentName={0} AND CountyName={1} AND DatabaseServer={2} AND DatabaseName={3} AND Status='P' ".SqlFormatString(envName, county, dbServer, dbName)
            Data.Database.System.Execute(query)
            Dim OrganizationId As String, Username As String
            OrganizationId = Data.Database.System.GetStringValue("SELECT Id FROM Organization WHERE DBHost='" & Host & "' and DBName='" & dbName & "'")
            Username = Membership.GetUser().ToString()
            query = "EXEC [Sp_EnvSettingsAuditTrail]@Name={0},@City={1},@County= {2},@State={3},@ApplicationHostId={4},@DBHost= {5},@DBName={6},@VendorId={7},@MAHostKey={8},@CAMASystem={9},@Notes={10},@isProduction={11},@OrgID={12},@LoginID={13},@repeat={14},@day={15},@time={16}".SqlFormat(True, envName, city, county, state, hostingStage, Host, dbName, vendor, hostKey, CAMAsystem, notes, isProduction, OrganizationId, Username, repeat, day, time)
            Data.Database.System.Execute(query)

            Return "True"
        Catch ex As Exception
            updateProcessError(ex.Message)
            Return ex.Message
        End Try
    End Function

    Public Shared Sub updateProcessError(ByVal errorMessage As String)
        Dim query As String
        query = "UPDATE EnvironmentCreationDetails  SET ProcessEndTime=GETUTCDATE(), Status='F', ErrorMessage= {0}  WHERE EnvironmentName={1} AND CountyName={2} AND DatabaseServer={3} AND DatabaseName={4} AND Status='P' ".SqlFormatString(errorMessage, envName, county, dbServer, dbName)
        Data.Database.System.Execute(query)
    End Sub

    Public Shared Sub Genarate(StringCollection As StringCollection)
        Dim str As String
        str = "GO"
        For Each Script As String In StringCollection
            WriteToFile(Script.Trim())
            WriteToFile(str)
        Next
        str = ""
    End Sub

    Public Shared Sub WriteToFile(text As String)
        If (text IsNot Nothing) Then
            Dim folderPath As String = "D:\SQLScript"
            If Not Directory.Exists(folderPath) Then
                Directory.CreateDirectory(folderPath)
            End If
            Dim path As String = "D:\SQLScript\script_" & DateTime.Now.ToString("dd'_'MM'_'yyyy") & ".sql"
            Using writer As New StreamWriter(path, True)
                Try
                    writer.WriteLine(text)
                Catch ex As Exception
                    Throw ex
                End Try
                writer.Close()
            End Using
        End If
    End Sub
End Class