﻿<%@ Page Title="" Language="vb" Async="true" AutoEventWireup="true" MasterPageFile="~/App_MasterPages/ClientAdmin.master" CodeBehind="Default.aspx.vb" Inherits="CAMACloud.Admin._Default2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .tblEnvironment { width: 100%; }
        .txtLSide { width: 87%; } .txtRSide { width: 98%; }
        .ddlLSide { width: 88%; } .ddlRSide { width: 99%; }
        .lLabel { width: 12%; } .RLabel { width: 15%; }
        .txtArea {width: 100%;}
        .txtLContainer { width: 35%; } .txtRContainer { width: 31%; }
        .txtCity { width: 44%; margin-right: 11%; } .txtState { width: 20%; }
        .txtInitialSize { width: 47.5%; margin-right: 12%; } .txtGrowth { width: 15%; }
        .ddlRepeat { width: 30%; } .ddlDay { width: 31%; } .ddlTime { width: 25%; }
        .txtAlign { margin-bottom: 2%; }
        .dropFiled { color: initial; font: 13.3333px Arial; }
        .progress {height: 20px; margin-bottom: 20px; overflow: hidden;background-color: #f5f5f5; border-radius: 4px;}
        .progress-bar{float: left; width: 0%; height: 100%; font-size: 12px; line-height: 20px; color: #fff;text-align: center; background-color: #337ab7;box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);transition: width .6s ease;}
        .update-progress {margin-top: 1%; margin-right: 20.5%; margin-left: 12.5%;}
        .IsProduction {margin-left: -5px}
        </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.update-progress').css('display', 'none');
            function openWindow() {
                var data = null;
                data.name = "asd";
            }
            $('.txtInitialSize, .txtGrowth').keydown(function (e) {
                -1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || /65|67|86|88/.test(e.keyCode) && (!0 === e.ctrlKey || !0 === e.metaKey) || 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey || 48 > e.keyCode || 57 < e.keyCode) && (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault()
            });
            $(".IsProduction input[type='checkbox']").prop( "checked", false );
            $('.btnCreateEnvironment').click(function () {
                var envName = $('.txtEnvName').val().trim();
                var dbServer = $('.ddlDBServer option:selected').val();
                var county = $('.txtCounty').val().trim();
                var dbName = $('.txtDBName').val().trim();
                var city = $('.txtCity').val().trim();
                var state = $('.txtState').val().trim();
                var initializeSize = $('.txtInitialSize').val().trim();
                var growth = $('.txtGrowth').val().trim();
                var hostKey = $('.txtHostKey').val().trim();
                var vendor = $('.ddlVendor option:selected').val();
                var repeat = $('.ddlRepeat option:selected').val();
                var day = $('.ddlDay option:selected').val();
                var time = $('.ddlTime option:selected').val();
                var hostingStage = $('.ddlHostStage option:selected').val();
                var camaSystem = $('.ddlCAMASystem option:selected').val();
                var notes = $('.txtNotes').val().trim();
                var isProd = $(".IsProduction input[type='checkbox']")[0]? $(".IsProduction input[type='checkbox']").is(':checked'): false;
                if (envName === '' || dbServer === '0' || county === '' || dbName === '' || city === '' || state === '' || vendor === '0' || !camaSystem || repeat === '0' || day === '0' || time === '0' || hostKey === '' || hostingStage === '0') {
                    if (envName === '') { alert('Environment Name cannot be blank'); return false; }
                    if (dbServer === '0') { alert('Database Server cannot be blank'); return false; }
                    if (county === '') { alert('County cannot be blank'); return false; }
                    if (dbName === '') { alert('Database Name cannot be blank'); return false; }
                    if (city === '') { alert('City cannot be blank'); return false; }
                    if (state === '') { alert('State cannot be blank'); return false; }
                    if (vendor === '0') { alert('CC Vendor cannot be blank'); return false; }
                    if (!camaSystem) { alert('CAMA System cannot be blank'); return false; }
                    if (repeat === '0' || day === '0' || time === '0') { alert('Backup Plan cannot be blank'); return false; }
                    if (hostKey === '') { alert('Host Key cannot be blank'); return false; }
                    if (hostingStage === '0') { alert('Hosting Stage cannot be blank'); return false; }
                }
                var environmentDetails = new Array();
                environmentDetails.push( envName, county, city, state, vendor, hostKey, dbServer, dbName, initializeSize, growth, repeat, day, time, hostingStage, camaSystem, notes, isProd );
                StartProcess(environmentDetails)
            });

            $('.txtEnvName').focusout(function () {
                if ($(this).val() !== "") {
                    $.ajax({
                        type: "POST",
                        url: "Default.aspx/CheckEnvName",
                        data: "{ 'envName':'" + $('.txtEnvName').val().trim() + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var count = data.d;

                            if (count >= 1) {
                                alert('Environment Name already exists');
                                $('.txtEnvName').focus();
                                return false;
                            }
                        }
                    });
                }
            });

            $('.txtCounty').focusout(function () {
                if ($(this).val() !== "") {
                    $.ajax({
                        type: "POST",
                        url: "Default.aspx/CheckCounty",
                        data: "{ 'county':'" + $('.txtCounty').val().trim() + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var count = data.d;

                            if (count >= 1) {
                                alert('County already exists');
                                $('.txtCounty').focus();
                                return false;
                            }
                        }
                    });
                }
            });

            $('.txtDBName').focusout(function () {
                $.ajax({
                    type: "POST",
                    url: "Default.aspx/IsDbExists",
                    data: "{'databaseName':'" + $('.txtDBName').val().trim() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var isExists = data.d;

                        if (isExists == true) {
                            alert('Database already exists');
                            $('.txtDBName').focus();
                            return false;
                        }
                    }
                });
            });

            $('.txtGrowth').focusout(function () {
                var growth = $('.txtGrowth').val().trim();
                if (growth !== '') {
                    if (growth < 10) {
                        alert('Growth(%) cannot be less then 10'); return false;
                    }

                    if (growth > 100) {
                        alert('Growth(%) cannot be more then 100'); return false;
                    }
                }
            });

            $('.txtHostKey').focusout(function () {
                $.ajax({
                    type: "POST",
                    url: "Default.aspx/IsHostKeyExists",
                    data: "{'hostKey':'" + $('.txtHostKey').val().trim() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var count = data.d;
                        if (count >= 1) {
                            alert('HostKey already exists');
                            $('.txtHostKey').focus();
                            return false;
                        }
                    }
                });
            });

        });
        function StartProcess(environmentDetails) {
            $('#btnSaveEnvironment').attr('disabled', true);
            $('.update-progress').css('display', 'block');         
            var json = environmentDetails.join("|")
            var url= "Default.aspx/CheckEnvironmentCreationProcess"
            var data="{ 'json':'" + json + "'}"
            ajaxCall(url, data, function (x) {
                if (x == 'notExist')
                {
                    $('#pBar').css("width", "2%");
                    $('.sr-only').text("2% Complete");
                    SetServerDetails()               
                }
                else if (x == 'exist') {
                    alert("The given environment is on under creationing.")
                }
                else
                {
                    alert(x);
                    clearData();
                }
                
            })
        }

        function SetServerDetails() {
            var url= "Default.aspx/SetServerDetails"
            ajaxCall(url,"", function (x) {
                if (x == 'True') {
                    $('#pBar').css("width", "10%");
                    $('.sr-only').text("10% Complete");
                    deleteOldScripts()
                }
                else {
                    alert(x);
                    clearData();
                }
            })
        }

        function deleteOldScripts() {
            var url= "Default.aspx/DeleteFile"
            ajaxCall(url, "", function (x) {
                if (x == 'True') {
                    $('#pBar').css("width", "15%");
                    $('.sr-only').text("15% Complete");
                    generateScript()
                }
                else {
                    alert(x);
                    clearData();
                }
            })
        }

        function generateScript() {
            var url = "Default.aspx/GenerateScript"
            ajaxCall(url, "", function (x) {
                if (x == 'True') {
                    $('#pBar').css("width", "18%");
                    $('.sr-only').text("18% Complete");
                    generateDBScript()
                }
                else {
                    alert(x);
                    clearData();
                }
            })
        }

        function generateDBScript() {
            var url = "Default.aspx/GenerateDBScript"
            ajaxCall(url, "", function (x) {
                if (x == 'True') {
                    $('#pBar').css("width", "22%");
                    $('.sr-only').text("22% Complete");
                    generateAssembliesScript()
                }
                else {
                    alert(x);
                    clearData();
                }
            })
        }

        function generateAssembliesScript() {
            var url = "Default.aspx/GenerateAssembliesScript"
            ajaxCall(url, "", function (x) {
                if (x == 'True') {
                    $('#pBar').css("width", "25%");
                    $('.sr-only').text("25% Complete");
                    generateSchemasScript()
                }
                else {
                    alert(x);
                    clearData();
                }
            })
        }

        function generateSchemasScript() {
            var url = "Default.aspx/GenerateSchemasScript"
            ajaxCall(url, "", function (x) {
                if (x == 'True') {
                    $('#pBar').css("width", "27%");
                    $('.sr-only').text("27% Complete");
                    generateUserDefinedTableTypeScript()
                }
                else {
                    alert(x);
                    clearData();
                }
            })
        }

        function generateUserDefinedTableTypeScript() {
            var url = "Default.aspx/GenerateUserDefinedTableTypeScript"
            ajaxCall(url, "", function (x) {
                if (x == 'True') {
                    $('#pBar').css("width", "30%");
                    $('.sr-only').text("30% Complete");
                    generateUserDefinedAggregatesScript()
                }
                else {
                    alert(x);
                    clearData();
                }
            })
        }

        function generateUserDefinedAggregatesScript() {
            var url = "Default.aspx/GenerateUserDefinedAggregatesScript"
            ajaxCall(url, "", function (x) {
                if (x == 'True') {
                    $('#pBar').css("width", "32%");
                    $('.sr-only').text("32% Complete");
                    generateStoredProceduresScript()
                }
                else {
                    alert(x);
                    clearData();
                }
            })
        }

        function generateStoredProceduresScript() {
            var url = "Default.aspx/GenerateStoredProceduresScript"
            ajaxCall(url, "", function (x) {
                if (x == 'True') {
                    $('#pBar').css("width", "38%");
                    $('.sr-only').text("38% Complete");
                    generateUserDefinedFunctionsScript()
                }
                else {
                    alert(x);
                    clearData();
                }
            })
        }

        function generateUserDefinedFunctionsScript() {
            var url = "Default.aspx/GenerateUserDefinedFunctionsScript"
            ajaxCall(url, "", function (x) {
                if (x == 'True') {
                    $('#pBar').css("width", "40%");
                    $('.sr-only').text("40% Complete");
                    generateTableScriptWithDependencies()
                }
                else {
                    alert(x);
                    clearData();
                }
            })
        }

        function generateTableScriptWithDependencies() {
            var url = "Default.aspx/GenerateTableScriptWithDependencies"
            ajaxCall(url, "", function (x) {
                if (x == 'True') {
                    $('#pBar').css("width", "44%");
                    $('.sr-only').text("44% Complete");
                    generateViewsScript()
                }
                else {
                    alert(x);
                    clearData();
                }
            })
        }

        function generateViewsScript() {
            var url = "Default.aspx/GenerateViewsScript"
            ajaxCall(url, "", function (x) {
                if (x == 'True') {
                    $('#pBar').css("width", "47%");
                    $('.sr-only').text("47% Complete");
                    generateTableIndexesScript()
                }
                else {
                    alert(x);
                    clearData();
                }
            })
        }

        function generateTableIndexesScript() {
            var url = "Default.aspx/GenerateTableIndexesScript"
            ajaxCall(url, "", function (x) {
                if (x == 'True') {
                    $('#pBar').css("width", "49%");
                    $('.sr-only').text("49% Complete");
                    generateTableScript()
                }
                else {
                    alert(x);
                    clearData();
                }
            })
        }

        function generateTableScript() {
            var url = "Default.aspx/GenerateTableScript"
            ajaxCall(url, "", function (x) {
                if (x == 'True') {
                    $('#pBar').css("width", "52%");
                    $('.sr-only').text("52% Complete");
                    generateExtendedPropertiesScript()
                }
                else {
                    alert(x);
                    clearData();
                }
            })
        }

        function generateExtendedPropertiesScript() {
            var url = "Default.aspx/GenerateExtendedPropertiesScript"
            ajaxCall(url, "", function (x) {
                if (x == 'True') {
                    $('#pBar').css("width", "55%");
                    $('.sr-only').text("55% Complete");
                    replaceContentInsideScript()
                }
                else {
                    alert(x);
                    clearData();
                }
            })
        }

        function replaceContentInsideScript() {
            var url = "Default.aspx/ReplaceContentInsideScript"
            ajaxCall(url, "", function (x) {
                if (x == 'True') {
                    $('#pBar').css("width", "65%");
                    $('.sr-only').text("65% Complete");
                    createDB()
                }
                else {
                    alert(x);
                    clearData();
                }
            })
        }

        function createDB() {
            var url = "Default.aspx/CreateDB"
            ajaxCall(url, "", function (x) {
                if (x == 'True') {
                    $('#pBar').css("width", "75%");
                    $('.sr-only').text("75% Complete");
                    updateOrganization()
                }
                else {
                    alert(x);
                    clearData();
                }
            })
        }
        function updateOrganization() {
            var url = "Default.aspx/UpdateOrganization"
            ajaxCall(url, "", function (x) {
                if (x == 'True') {
                    $('#pBar').css("width", "80%");
                    $('.sr-only').text("80% Complete");
                    insertTable()
                }
                else {
                    alert(x)
                    clearData();
                }
            })
        }
        function insertTable() {
            var url = "Default.aspx/InsertTable"
            ajaxCall(url, "", function (x) {
                if (x == 'True') {
                    $('#pBar').css("width", "90%");
                    $('.sr-only').text("90% Complete");
                    updateStatus()
                }
                else {
                    alert(x);
                    clearData();
                }
            })
        }

        function updateStatus() {
            var url = "Default.aspx/UpdateStatus"
            ajaxCall(url, "", function (x) {
                if (x == 'True') {
                    $('#pBar').css("width", "100%");
                    $('.sr-only').text("100% Complete");
                    setTimeout(updateStatusMessage, 5000);  
                }
                else {
                    alert(x);
                    clearData();
                }
            })
        }

        updateStatusMessage = () => {
            alert("New environment created successfully ")
            clearData();
        }

        function ajaxCall(url, data, callback, errorCallback) {
            $.ajax({
                url: url,
                type: "POST",
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                data: data,
                success: function (data, status) {
                    if (callback) callback(data.d);
                }
                , error: function (xhr, ajaxOptions, thrownError) {
                    if (errorCallback)
                        errorCallback(thrownError);
                    else
                        console.log(xhr);
                }
            });
        }
        function clearData() {
           $('#btnSaveEnvironment').attr('disabled', false);
           $('.update-progress').css('display', 'none');
           $('.txtEnvName').val('');
           $('.ddlDBServer').prop('selectedIndex', 0);
           $('.txtCounty').val('');
           $('.txtDBName').val('');
           $('.txtCity').val('');
           $('.txtState').val('');
           $('.txtInitialSize').val('');
           $('.txtGrowth').val('');
           $('.txtHostKey').val('');
           $('.txtNotes').val('');
           $('.ddlVendor').prop('selectedIndex', 0);
           $('.ddlRepeat').prop('selectedIndex', 0);
           $('.ddlDay').prop('selectedIndex', 0);
           $('.ddlTime').prop('selectedIndex', 0);
           $('.ddlHostStage').prop('selectedIndex', 0);
           $('.ddlCAMASystem').prop('selectedIndex', 0);


        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Create New Client Environment</h1>
    <div style="width: 95%;">
        <table class="tblEnvironment">
            <tr>
                <td class="lLabel">Environment Name</td>
                <td class="txtLContainer">
                    <asp:TextBox runat="server" ID="txtEnvName" CssClass="txtEnvName txtLSide txtAlign" /></td>
                <td class="RLabel">Select Database Server</td>
                <td>
                    <asp:DropDownList ID="ddlDBServer" runat="server" AutoPostBack="False" CssClass="ddlDBServer ddlRSide txtAlign dropFiled"></asp:DropDownList></td>
            </tr>
            <tr>
                <td class="lLabel">County</td>
                <td class="txtContainer">
                    <asp:TextBox runat="server" ID="txtCounty" CssClass="txtCounty txtLSide txtAlign" /></td>
                <td class="RLabel">Database Name</td>
                <td class="txtRContainer">
                    <asp:TextBox runat="server" ID="txtDBName" CssClass="txtDBName txtRSide txtAlign" /></td>
            </tr>
            <tr>
                <td class="lLabel">City</td>
                <td>
                    <asp:TextBox runat="server" ID="txtCity" CssClass="txtCity txtAlign" />
                    State
                <asp:TextBox runat="server" ID="txtState" CssClass="txtState txtAlign" /></td>
                <td class="RLabel">Initial DB Size(MB)</td>
                <td>
                    <asp:TextBox runat="server" ID="txtInitialSize" CssClass="txtInitialSize txtAlign" />
                    Growth(%)
                <asp:TextBox runat="server" ID="txtGrowth" CssClass="txtGrowth txtAlign" MaxLength="3" /></td>
            </tr>
            <tr>
                <td>CC Vendor</td>
                <td>
                    <asp:DropDownList ID="ddlVendor" runat="server" AutoPostBack="True" CssClass="ddlVendor ddlLSide txtAlign dropFiled"></asp:DropDownList></td>
				<td>CAMA System</td>
                <td>
                    <asp:DropDownList ID="ddlCAMASystem" runat="server" AutoPostBack="False" CssClass="ddlCAMASystem ddlRSide txtAlign dropFiled"></asp:DropDownList></td>
            </tr>
            <tr>
                <td>Host Key</td>
                <td>
                    <asp:TextBox runat="server" ID="txtHostKey" CssClass="txtHostKey txtLSide txtAlign" /></td>
                <td>Hosting Stage</td>
                <td>
                    <asp:DropDownList ID="ddlHostingStage" runat="server" AutoPostBack="False" CssClass="ddlHostStage ddlRSide txtAlign dropFiled"></asp:DropDownList></td>
            </tr>
            <tr>
             	<td>Backup Plan</td>
                <td>
                    <asp:DropDownList ID="ddlRepeat" runat="server" AutoPostBack="False" CssClass="ddlRepeat txtAlign dropFiled">
                        <asp:ListItem Text="Every" Value="Every"></asp:ListItem>
                        <asp:ListItem Text="First" Value="First"></asp:ListItem>
                        <asp:ListItem Text="Second" Value="Second"></asp:ListItem>
                        <asp:ListItem Text="Third" Value="Third"></asp:ListItem>
                        <asp:ListItem Text="Fourth" Value="Fourth"></asp:ListItem>
                        <asp:ListItem Text="Fifth" Value="Fifth"></asp:ListItem>
                        <asp:ListItem Text="Last" Value="Last"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="ddlDay" runat="server" AutoPostBack="False" CssClass="ddlDay txtAlign dropFiled">
                        <asp:ListItem Text="Sunday" Value="Sunday"></asp:ListItem>
                        <asp:ListItem Text="Monday" Value="Monday"></asp:ListItem>
                        <asp:ListItem Text="Tuesday" Value="Tuesday"></asp:ListItem>
                        <asp:ListItem Text="Wednesday" Value="Wednesday"></asp:ListItem>
                        <asp:ListItem Text="Thursday" Value="Thursday"></asp:ListItem>
                        <asp:ListItem Text="Friday" Value="Friday"></asp:ListItem>
                        <asp:ListItem Text="Saturday" Value="Saturday"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="ddlTime" runat="server" AutoPostBack="False" CssClass="ddlTime txtAlign dropFiled">
                        <asp:ListItem Text="00:00" Value="00:00"></asp:ListItem>
                        <asp:ListItem Text="06:00" Value="06:00"></asp:ListItem>
                        <asp:ListItem Text="08:00" Value="08:00"></asp:ListItem>
                        <asp:ListItem Text="12:00" Value="12:00"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>Production</td>
                <td> <asp:CheckBox ID="checkProduction" CssClass="IsProduction" runat="server" Text="Yes" /> </td>
            </tr>
            <tr>
             	<td>Notes</td>
                <td colspan="3">
                  	<asp:TextBox ID="txtNotes"  CssClass="txtNotes txtArea txtAlign"  TextMode="MultiLine" Rows="7" style="overflow:auto;" runat="server"></asp:TextBox></td>
            </tr>
        </table>
    </div>
    <div style="margin-top: 1%; float: right; margin-right: 5.5%;">
        <input type="button" id="btnSaveEnvironment" class="btnCreateEnvironment" value="Create Environment" />
    </div>
    <div class="update-progress">
        <div class="progress">
            <div class="progress-bar" id="pBar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">
                <span class="sr-only">0% Complete</span>
            </div>
        </div>
    </div>    
</asp:Content>

