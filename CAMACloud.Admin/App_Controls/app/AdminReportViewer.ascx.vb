﻿Imports System.IO

Public Class AdminReportViewer
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Public Sub showReport()
        exportOptions.Visible = True
        reportHead.Visible = True
        reportViewer.Visible = True

    End Sub

    Public Sub GenerateGrid(dtReportData As DataTable, reportTitle As String)
        lblReportHead.Text = reportTitle
        ViewState("dtReportData") = dtReportData
        Dim bfieldName As String
        Dim bHeaderText As String
        gvCustomReport.Columns.Clear()
        For Each column As DataColumn In dtReportData.Columns
            bfieldName = column.ColumnName
            bHeaderText = column.ColumnName
            Dim bfield As New BoundField()
            bfield.HeaderText = bHeaderText
            bfield.DataField = bfieldName
            If (column.ColumnName = "Date" Or (column.ColumnName = "EventDate" And reportTitle = "Settings Audit Trail Report")) Then
                bfield.DataFormatString = "{0:d}"
            End If
            If (column.ColumnName = "EventTime" And reportTitle = "Settings Audit Trail Report") Then
                bfield.DataFormatString = "{0:T}"
            End If
            bfield.ItemStyle.CssClass = "gvNonCountItemStyle"
            bfield.HeaderStyle.CssClass = "gvHeaderStyle"
            gvCustomReport.Columns.Add(bfield)
        Next
        gvCustomReport.PageIndex = 0
        gvCustomReport.ShowFooter = False
        bindGrid(dtReportData)

    End Sub
    Public Sub bindGrid(dtReportData As DataTable)
        gvCustomReport.DataSource = Nothing
        gvCustomReport.DataSource = dtReportData
        gvCustomReport.DataBind()
    End Sub
    Protected Sub gvCustomReportPageChanging(sender As Object, e As GridViewPageEventArgs)
        gvCustomReport.PageIndex = e.NewPageIndex
        bindGrid(ViewState("dtReportData"))
    End Sub
    Private Sub ExportGridView(senderId As String)
        Dim dt As DataTable = ViewState("dtReportData")
        Dim GridView1 As New GridView()
        GridView1.AllowPaging = False
        GridView1.AutoGenerateColumns = False
        Dim bfieldName As String
        Dim bHeaderText As String
        GridView1.Columns.Clear()
        For Each column As DataColumn In dt.Columns
            bfieldName = column.ColumnName
            bHeaderText = column.ColumnName
            Dim bfield As New BoundField()
            bfield.HeaderText = bHeaderText
            bfield.DataField = bfieldName
            If (column.ColumnName = "Date") Then
                bfield.DataFormatString = "{0:d}"
            End If
            bfield.ItemStyle.CssClass = "gvNonCountItemStyle"
            bfield.HeaderStyle.CssClass = "gvHeaderStyle"
            bfield.ItemStyle.Width = 130
            GridView1.Columns.Add(bfield)
        Next
        GridView1.DataSource = Nothing
        GridView1.DataSource = dt
        GridView1.DataBind()
        If (senderId = "ibtnExportToExcel") Then
            ExportToExcel(GridView1)
        End If
    End Sub
    Private Sub ExportToExcel(GridView1 As GridView)
        Response.Clear()
        Response.Buffer = True
        Response.ClearContent()
        Response.ClearHeaders()
        Dim fileName As String = Server.UrlEncode(lblReportHead.Text.Replace(" ", "") + ".xlsx")
        Response.Clear()
        Response.Buffer = True
        Response.ClearContent()
        Response.ClearHeaders()
        Response.AddHeader("Content-Disposition", "attachment;filename=" + fileName)
        Response.Charset = ""
        Response.ContentType = "application/vnd.openxml.formats-officedocument.spreadsheetml.sheet"
        GridView1.ControlStyle.Width = 130
        Dim excelStream = ExcelGenerator.ExportGrid(GridView1, ViewState("dtReportData"), lblReportHead.Text, lblReportHead.Text)
        excelStream.CopyTo(Response.OutputStream)
        Response.End()
    End Sub

    Protected Sub ibtnExportToExcel_Click(sender As Object, e As ImageClickEventArgs) Handles ibtnExportToExcel.Click
        ExportGridView(sender.Id)
    End Sub

    Protected Sub ddlPageSize_selectedIdexChanged(sender As Object, e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
        gvCustomReport.PageSize = ddlPageSize.SelectedValue
        gvCustomReport.PageIndex = 0
        bindGrid(ViewState("dtReportData"))
    End Sub



End Class