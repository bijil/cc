﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AdminReportViewer.ascx.vb" Inherits="CAMACloud.Admin.AdminReportViewer" %>
<%@ Import Namespace="CAMACloud"%>
<%@ Import Namespace ="CAMACloud.Data" %>
<style type="text/css">
        .custom-report-viewer {
            width: 98%;
            overflow-x: auto;
            /*position: absolute;*/
            padding: 0 5px 0px 5px;
        }       
        .report-head{
            font-family: Cambria, Georgia, serif;
	        font-size: 26px;       
            text-align: center;
        }  
       .exportIcon{    
            text-align: right;
            background-color: #ECE9D8;
        }
        .gvHeaderStyle{
            white-space:nowrap; 
            /*text-align: center !important;*/
            border-right: 1px solid !important;
            border-right-style: inset !important;
        }
        .gvItemStyle{
            white-space:nowrap;
            text-align: left;
            min-width: 130px;
        }
        .gvNonCountItemStyle{       
             text-align: left;
             white-space:nowrap;
             /*min-width: 130px;*/
        }
        .gvFooterStyle{
            text-align: left;
        }
        .highlighted{
            background-color:yellow;
        }
</style>
<script type="text/javascript">
    function filterTable(Stxt, table) {
        dehighlight(document.getElementById(table));
        if (Stxt.value.length > 0)
            highlight(Stxt.value.toLowerCase(), document.getElementById(table));
    }
    function dehighlight(container) {
        for (var i = 0; i < container.childNodes.length; i++) {
            var node = container.childNodes[i];
            if (node.attributes && node.attributes['class'] && node.attributes['class'].value == 'highlighted') {
                node.parentNode.parentNode.replaceChild(
                document.createTextNode(
                node.parentNode.innerHTML.replace(/<[^>]+>/g, "").replace('&amp;', '&')),
                node.parentNode);
                // Stop here and process next parent
                return;
            } else if (node.nodeType != 3) {
                // Keep going onto other elements
                dehighlight(node);
            }
        }
    }

    function highlight(Stxt, container) {
        for (var i = 0; i < container.childNodes.length; i++) {
            var node = container.childNodes[i];
            if (node.className == "ui-state-default") {
                continue;
            }
            if (node.nodeType == 3) {
                // Text node
                var data = node.data.replace('&amp', '&');
                var data_low = data.toLowerCase();
                if (data_low.indexOf(Stxt) >= 0) {
                    //Stxt found!
                    var new_node = document.createElement('span');
                    node.parentNode.replaceChild(new_node, node);
                    var result;
                    while ((result = data_low.indexOf(Stxt)) != -1) {
                        new_node.appendChild(document.createTextNode(data.substr(0, result)));
                        new_node.appendChild(create_node(document.createTextNode(data.substr(result, Stxt.length))));
                        data = data.substr(result + Stxt.length);
                        data_low = data_low.substr(result + Stxt.length);
                    }
                    new_node.appendChild(document.createTextNode(data));
                }
            } else {
                // Keep going onto other elements
                highlight(Stxt, node);
            }
        }
    }
    function create_node(child) {
        var node = document.createElement('span');
        node.setAttribute('class', 'highlighted');
        node.attributes['class'].value = 'highlighted';
        node.appendChild(child);
        return node;
    }
</script>
<div style ="overflow-x:auto">
     <div Id="exportOptions" class="exportIcon" runat="server" Visible ="False">
         <div style ="float:left;margin-left:2%">
             <span>Page Size:</span>
             <asp:DropDownList runat ="server" ID ="ddlPageSize" AutoPostBack ="true" Width="50px">
             <asp:ListItem value="15"></asp:ListItem>
             <asp:ListItem Value="30"></asp:ListItem>
             <asp:ListItem Value="50"></asp:ListItem>
         </asp:DropDownList>
             <span style="margin-left:20px">Find :</span>
             <input type="text" id ="Stxt" onkeyup="filterTable(this,'MainContent_Uc_CustomReport_gvCustomReport')" style ="width:100px"/>
         </div>
         <asp:ImageButton ID="ibtnExportToExcel" ImageUrl="~/App_Static/images/reportexcel.png" width="28px" height="25px" style="margin-right:3px;" ToolTip="Export To Excel" runat="server" />
     </div>
     <div ID="reportHead"  class="report-head"  runat="server" Visible ="False">
        <asp:Label ID="lblReportHead" runat="server" Text="Label"></asp:Label>
     </div>
     <div class="custom-report-viewer" Id="reportViewer" runat="server" Visible ="False">
         <asp:GridView ID="gvCustomReport"  runat="server" width="100%" AllowPaging="true" AutoGenerateColumns="false"  ShowFooter = "true" OnPageIndexChanging = "gvCustomReportPageChanging" PageSize="19"  >
         </asp:GridView>			
     </div>
   </div>