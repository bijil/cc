﻿Imports System.Linq

Partial Class App_Controls_app_ModuleNavigation
	Inherits System.Web.UI.UserControl

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            '2,	Device License Manager,	/license/	,LincenseManger
            Dim sql As String = "SELECT * FROM AppRoles WHERE Path IS NOT NULL AND ID=2"
            menu.DataSource = Database.System.GetDataTable(sql)
            menu.DataTextField = "Name"
            menu.DataValueField = "Path"
            menu.DataBind()
            '			menu.DataSource = CAMACloud.data
        End If
	End Sub
End Class
