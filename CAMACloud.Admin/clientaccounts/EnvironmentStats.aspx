﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ClientAdmin.master" CodeBehind="EnvironmentStats.aspx.vb" Inherits="CAMACloud.Admin.EnvironmentStats" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .tblEnvironmentStats {
            width: 100%;
        }

        .txtLSide {
            width: 87%;
        }

        .txtRSide {
            width: 87%;
        }

        .ddlLSide {
            width: 88%;
            height: 25px;
        }

        .ddlRSide {
            width: 88%;
            height: 25px;
        }

        .txtLContainer {
            width: 35%;
        }

        .txtRContainer {
            width: 35%;
        }

        .lLabel {
            width: 12%;
        }

        .RLabel {
            width: 15%;
        }

        .txtAlign {
            margin-bottom: 3%;
        }

        .dropFiled {
            color: initial;
            font: 13.3333px Arial;
        }

        .btnGenerate{float: right; margin-right: 95px;}

        #MainContent_MainContent_gvTotalMACbyUser td {
            border-right: solid 1px #0ab0c4 !important;
        }

       #MainContent_MainContent_gvTotalMACbyUser .pgr td { border-left: none; }
        
		.masklayer {
	    background: black;
	    opacity: 0.5;
	    top: 0%;
	    width: 100%;
	    height: 100%;
	    bottom: 0%;
	    position: absolute;
	    display: none;
		}
		.info {
            width: 850px;
            border-left: 8px solid #CFCFCF;
            padding-left: 10px;
            line-height: 15pt;
            padding-top: 5px;
            padding-bottom: 5px;
        }
        .btnDownload{
        	background-image: url(../App_Static/css/icon16/download.png);
		    background-repeat: no-repeat;
		    background-position: center left 5px;
		    width: 110px;
		    float: right;
		    margin-bottom:10px;
        }

    </style>

    <script type="text/javascript">
        function searchValidation ()
        { //Search button click - data search and data vilidation
            var startDate = $( '.txtFromDate' ).val();
            var endDate = $( '.txtToDate' ).val();
            var vendorId = $( '.ddlVendor' ).val();
            if (vendorId == '') { alert('Please select a vendor'); return false; } //Null filed validation
            if ( startDate != '' && endDate != '' )
            { //Date field validation
                var start = new Date( startDate );
                var end = new Date( endDate );
                if (start > end) { alert('Start date cannot be greater than End date'); return false; }
                var min_date = new Date(1990, 0, 1);
                var max_date = new Date(2078, 0, 1);
                if (start < min_date || start > max_date) { alert('Start date is invalid'); return false; }
                if (end < min_date || end > max_date) { alert('End date is invalid'); return false; }
            }

            if ( startDate != '' && endDate == '' )
            {
                var todayDate = new Date();
                var month = todayDate.getMonth() + 1;
                var day = todayDate.getDate();
                var year = todayDate.getFullYear();
                if ( month < 10 )
                    month = '0' + month.toString();
                if ( day < 10 )
                    day = '0' + day.toString();
                var maxDate = year + '-' + month + '-' + day;
                $( '.txtToDate' ).val( maxDate );
                var start = new Date( $( '.txtFromDate' ).val() );
                var end = new Date( $( '.txtToDate' ).val() );
                if (start > end) { alert('End date is Mandatory'); return false; }
                var min_date = new Date(1990, 0, 1);
                var max_date = new Date(2099, 0, 1);
                if (start < min_date || start > max_date) { alert('Start date is invalid'); return false; }
                if (end < min_date || end > max_date) { alert('End date is invalid'); return false; }
                
            } //End date field validation
            //$( '.masklayer' ).css( "display", "block" );
            dateValidate(function (valid) {
                if (valid == true) {
                    $('body').scrollTop(0);
                    $('body').css('overflow', 'hidden');
                    showMask('Please wait ...');
                    return true;
                }
                else {
                    return false;
                }
            });
        }
        function dateValidate(callback) {
            var pattern = /^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/;
            var dtRegex = new RegExp(pattern);
            var validat = true;
            $("input:[type='date']").each(function () {
                if (!this.validity.valid || (!dtRegex.test($(this).val()) && $(this).val() != "")) {
                    validat = false;
                }
            });
            if (callback) callback(validat);
        };
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Environment Stats</h1>
    <p class="info">
  		<strong>Notes :</strong>Please be patient, it may take a few minutes to prepare the report.
    </p>
    <div>
        <div  style="width: 95%;" >
            <table class="tblEnvironmentStats">
                <tr>
                    <td class="lLabel">Vendor Name</td>
                    <td class="txtLContainer">
                        <asp:DropDownList ID="ddlVendor" runat="server" AutoPostBack="True" CssClass="ddlVendor ddlLSide txtAlign dropFiled"></asp:DropDownList></td>

                    <td class="RLabel">Environment Name</td>
                    <td class="txtRContainer">
                        <asp:DropDownList ID="ddlEnvironment" runat="server" AutoPostBack="false" CssClass="ddlEnvironment ddlRSide txtAlign dropFiled"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td class="lLabel">From Date</td>
                    <td class="txtContainer">
                        <input type="date" id="txtFromDate" name="FromDate" runat="server" class="txtLSide txtFromDate" /></td>
                    <td class="RLabel">To Date</td>
                    <td class="txtRContainer">
                        <input type="date" id="txtToDate" name="FromDate" runat="server" class="txtRSide txtToDate" /></td>
                </tr>
            </table>
        </div>
        <div style="margin-top: 1%;width:100%;float:left;">
            <asp:Button ID="btnGenerate" CssClass="btnGenerate" runat="server" OnClientClick="return searchValidation();" Text="Generate" />
        </div>
    </div>

    <div id="StatsReport"  runat="server" visible="false" style="width: 99%;">
    <div style="float:left;height:auto;width: 100%;">
            <div style="width: 49%; min-height: 356px; float: left;">
            <h4>Device License Stats</h4>
            <div style="font-weight: bold; width: 100%;float: left;">
                <span id="PageCountDeviceLicense" runat="server"></span>
                <asp:Button ID="btnDLSDownload"  CssClass="btnDownload" runat="server" Text="Download" />
            </div>
            <asp:GridView ID="gvDeviceLicense" AutoGenerateColumns="false" runat="server" ShowHeaderWhenEmpty="True" EmptyDataText="No record found" Width="100%" AllowPaging="true" OnPageIndexChanging="gvDeviceLicensePageChanging" PageSize="10">
                <Columns>
                    <asp:BoundField DataField="Organization" HeaderText="Organization" />
                    <asp:BoundField DataField="Console" HeaderText="Console" />
                    <asp:BoundField DataField="MA" HeaderText="MA" />
                    <asp:BoundField DataField="MAChrome" HeaderText="MAChrome" />
                    <asp:BoundField DataField="MACommon" HeaderText="MACommon" />
                </Columns>
            </asp:GridView>
        </div>
        <div style="width: 49%; min-height: 356px; float: right;">
            <h4>Total MAC By User</h4>
            <div style="font-weight: bold; width: 100%;float: left;">
                <span id="PageCountTotalMACbyUser" runat="server"></span>
                 <asp:Button ID="btnMACUserDownload"  CssClass="btnDownload" runat="server" Text="Download" />
            </div>
            <asp:GridView ID="gvTotalMACbyUser"  runat="server" AutoGenerateColumns="False" CellPadding="4" ShowHeaderWhenEmpty="True" EmptyDataText="No record found" ShowFooter="true" AllowPaging="true" OnPageIndexChanging="gvTotalMACbyUserPageChanging" PageSize="10" Width="100%">
                <Columns>
                    <asp:BoundField DataField="Organization" HeaderText="Organization" SortExpression="Organization" />
                    <asp:BoundField DataField="UserName" HeaderText="UserName" SortExpression="UserName" />
                    <asp:BoundField DataField="TotalMACbyUser" HeaderText="TotalMACbyUser" SortExpression="TotalMACbyUser" />
                </Columns>
            </asp:GridView>
        </div>

    </div>
    <div>
        <div style="width: 49%; min-height: 356px; float: left;">
            <h4>Photos Taken MA</h4>
            <div style="font-weight: bold; width: 100%;float: left;">
                <span id="PageCountPhotosTakenMA" runat="server"></span>
                 <asp:Button ID="btnPhotoDownload"  CssClass="btnDownload" runat="server" Text="Download" />
            </div>
            <asp:GridView ID="gvPhotosTakenMA" AutoGenerateColumns="false" runat="server" ShowHeaderWhenEmpty="True" EmptyDataText="No record found" Width="100%" AllowPaging="true" OnPageIndexChanging="gvPhotosTakenMAPageChanging" PageSize="10">
                <Columns>
                    <asp:BoundField DataField="Organization" HeaderText="Organization" />
                    <asp:BoundField DataField="Counts" HeaderText="Counts" />
                </Columns>
            </asp:GridView>
        </div>
        <div style="width: 49%; min-height: 356px; float: right;">
            <h4>Most Recent MAC</h4>
            <div style="font-weight: bold; width: 100%;float: left;">
                <span id="PageCountMostRecentMAC" runat="server"></span>
                 <asp:Button ID="btnMACDownload"  CssClass="btnDownload" runat="server" Text="Download" />
            </div>
            <asp:GridView ID="gvMostRecentMAC" runat="server" AutoGenerateColumns="False" CellPadding="4" ShowHeaderWhenEmpty="True" EmptyDataText="No record found"  AllowPaging="true" OnPageIndexChanging="gvMostRecentMACPageChanging" PageSize="10" Width="100%">
                <Columns>
                    <asp:BoundField DataField="Organization" HeaderText="Organization" SortExpression="Organization" />
                    <asp:BoundField DataField="LastReviewedDate" HeaderText="LastReviewedDate" SortExpression="LastReviewedDate" DataFormatString="{0:MM/dd/yyyy hh:mm tt}" />
                </Columns>
            </asp:GridView>
        </div>
    </div>

    </div>
</asp:Content>
