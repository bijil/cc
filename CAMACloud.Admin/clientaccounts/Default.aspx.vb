﻿
Partial Class clientaccounts_Account
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadGrid()
            LoadCurrentActivities()
        End If
    End Sub
    Sub LoadGrid(Optional ByVal pageIndex As Integer = 0)
        grdlicense.PageIndex = pageIndex
        grdlicense.DataSource = Database.System.GetDataTable("EXEC GetLicenseDetails ")
        grdlicense.DataBind()
    End Sub
    Sub LoadCurrentActivities()
        Dim dtToday As DataTable = Database.System.GetDataTable("SELECT Name FROM Organization WHERE CAST(dbo.GetLocalDate(CreatedDate) AS DATE) = CAST(dbo.GetLocalDate(GETUTCDATE()) AS DATE)")
        Dim dtLast As DataTable = Database.System.GetDataTable("SELECT Name FROM Organization WHERE CAST(dbo.GetLocalDate(CreatedDate) AS DATE) = (SELECT CAST(dbo.GetLocalDate(MAX(CreatedDate)) As DATE) FROM Organization WHERE CAST(dbo.GetLocalDate(CreatedDate) AS DATE) < CAST(dbo.GetLocalDate(GETUTCDATE()) AS DATE))")
        rpToday.DataSource = dtToday
        rpToday.DataBind()
        rpLast.DataSource = dtLast
        rpLast.DataBind()
        If rpToday.Items.Count > 0 Then
            lblNoneToday.Visible = False
        Else
            lblNoneToday.Visible = True
        End If

        If rpLast.Items.Count > 0 Then
            lblNoneLast.Visible = False
        Else
            lblNoneLast.Visible = True
        End If


    End Sub
End Class
