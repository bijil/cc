﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/ClientAdmin.master" AutoEventWireup="false" EnableEventValidation="false" 
    CodeBehind="backupsch.aspx.vb" Inherits="CAMACloud.Admin.backupsch" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.min.css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
	<script type="text/javascript" src="/App_Static/jslib/jquery.min.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>

    <h1>
        Backup Scheduler
    </h1>

    <style type="text/css">
        select {
            width: 200px;
        }

        input[type="text"] {
            width: 196px;
        }

        input[type="submit"] {
            margin-top: 35px;
        }

        td span[sep] {
            margin-left: 15px;
            float: right;
        }

        .select2-container {
          min-width: 400px;
        }
        
        .select2-results__option {
          padding-right: 20px;
          vertical-align: middle;
        }

        .select2-results__option:before {
          content: "";
          display: inline-block;
          position: relative;
          height: 20px;
          width: 20px;
          border: 2px solid #e9e9e9;
          border-radius: 4px;
          background-color: #fff;
          margin-right: 20px;
          vertical-align: middle;
        }
        .select2-results__option[aria-selected=true]:before {
          font-family:fontAwesome;
          content: "\f00c";
          color: #fff;
          background-color: #f77750;
          border: 0;
          display: inline-block;
          padding-left: 5px;
          padding-top: 5px;
        }

        .select2-container--default .select2-results__option[aria-selected=true] {
        	background-color: #fff;
        }

        .select2-container--default .select2-results__option--highlighted[aria-selected] {
        	background-color: #eaeaeb;
        	color: #272727;
        }

        .select2-container--default .select2-selection--multiple {
        	margin-bottom: 10px;
        }
        .select2-container--default.select2-container--open.select2-container--below .select2-selection--multiple {
        	border-radius: 4px;
        }

        .select2-container--default.select2-container--focus .select2-selection--multiple {
        	border-color: #f77750;
        	border-width: 2px;
        }

        .select2-container--default .select2-selection--multiple {
        	border-width: 2px;
        }

        .select2-container--open .select2-dropdown--below {
        	
        	border-radius: 6px;
        	box-shadow: 0 0 10px rgba(0,0,0,0.5);
        
        }

        .select2-selection .select2-selection--multiple:after {
        	content: 'hhghgh';
        }
        /* select with icons badges single*/
        .select-icon .select2-selection__placeholder .badge {
        	display: none;
        }

        .select-icon .placeholder {
        /* 	display: none; */
        }

        .select-icon .select2-results__option:before,
        .select-icon .select2-results__option[aria-selected=true]:before {
        	display: none !important;
        	/* content: "" !important; */
        }

        .select-icon  .select2-search--dropdown {
        	display: none;
        }

        .validate {
            border: 1px solid red !important;
        }

        .info {
            width: 800px !important;
        }
    </style>

    <script type="text/javascript">

        var countyList = [];
        $('document').ready(function () {
            $("#ddlServer").val("").select2(); $("#ddlScheduleDay").select2({ multiple: true, placeholder: { id: '', text: 'None Selected' } }); $("#ddlScheduleTime").select2(); $("#ddlRepeatfactor").select2(); clearDropDowns();
            $("#ddlCountyName").select2({ multiple: true, placeholder: { id: '', text: 'None Selected' }}).val([]).trigger('change');

            $$$bkAjax("backupsch.aspx/getCounties", {}, (res) => {
                countyList = res?.d?.length > 0 ? res.d: [];
            }, (err) => {
                console.log(err.message);
            });

            $("#ddlServer").bind("change", function () {
                let dh = $(this).val(); $('#ddlCountyName').empty();
                if (dh && dh != '-- Select --') {
                    let html = "<option value=''>-- Select --</option>", clist = countyList.filter((x) => { return x.DBHost == dh });
                    clist.forEach((u) => {
                        html += "<option value=" + u.ID + ">" + u.Name + "</option>";
                    });

                    $('#ddlCountyName').append(html);
                }
            });

            $('#ddlScheduleDay, #ddlScheduleTime, #ddlRepeatfactor').bind("change", function () {
                $('.select2-selection', $(this).siblings('.select2-container')).removeClass('validate');
            });
            });

        var clearDropDowns = () => {
            $("#ddlServer").val("").trigger('change'); $("#ddlScheduleDay").val("").trigger('change');
            $("#ddlScheduleTime").val("----Select-----").trigger('change'); $("#ddlRepeatfactor").val("----Select-----").trigger('change');
        }

        var saveBackupSchedule = () => {
            let s = $('#ddlServer').val(), cl = $('#ddlCountyName').val(), d = $('#ddlScheduleDay').val(), t = $('#ddlScheduleTime').val(), r = $('#ddlRepeatfactor').val(), v = true;

            if (d?.length == 0) {
                $('.select2-selection', $('.ddlScheduleDay').siblings('.select2-container')).addClass('validate'); v = false;
            }

            if (t == '----Select-----') {
                $('.select2-selection', $('.ddlScheduleTime').siblings('.select2-container')).addClass('validate'); v = false;
            }

            if (!r || r == '----Select-----') {
                $('.select2-selection', $('.ddlRepeatfactor').siblings('.select2-container')).addClass('validate'); v = false;
            }

            if (!v) return false;
            let cf = confirm("Are you sure, you want create new backup schedule?");

            if (cf) {
                showMask();
                let ds = { server: (s && s != '-- Select --'? s: ''), county: (cl?.length > 0 ? cl.toString() : ''), bkDays: d.toString(), bkTime: t, repeat: r };

                $$$bkAjax("backupsch.aspx/saveBackup", ds, (res) => {
                    if (res.d == "Sucess") alert("The backup schedule created successfully!"); clearDropDowns(); $('.masklayer').hide();
                }, (err) => {
                    alert("Error occured! " + err.message); $('.masklayer').hide();
                });
            }
            return false;
        }

        var $$$bkAjax = (path, d, scallback, fcallback) => {
            $.ajax({
                type: "POST",
                url: path,
                data: JSON.stringify(d),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: (data) => {
                    if (scallback) scallback(data); 
                },
                error: (e) => {
                    if (fcallback) fcallback(e); 
                }
            });
        }
    </script>

    <p class="info">
        The "save changes" deletes existing backup schedule and created with new one. <br>
        Server and county not selected, then new backup schedule will be created in all enviornments.<br>
        Server selected and no county selected, then new backup schedule will be created for all enviorments for the selected server.
    </p>
    <table>
        <tbody>
            <tr>
                <td>
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    Server<span >:</span>
                                </td>
                                <td  class="ddlHostServer">
                                     <asp:DropDownList ID="ddlServer" runat="server" AutoPostBack="False" CssClass="ddlServer"   ClientIDMode="Static">
                                         </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   County Name<span >:</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlCountyName" runat="server" AutoPostBack="False"  CssClass="ddlCountyName"  ClientIDMode="Static">
                                         </asp:DropDownList>
                                </td>
                                                  <tr>
                                                                                 
                                </tr>  <tr>
                                <td>
                                   Schedule Day<span >:</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlScheduleDay" runat="server" AutoPostBack="False"  CssClass="ddlScheduleDay"  ClientIDMode="Static">
                                  
                                         </asp:DropDownList>
                                </td>
                                                 

                                   </tr>
                                <td>
                                   Schedule Time<span >:</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlScheduleTime" runat="server" AutoPostBack="False"  CssClass="ddlScheduleTime"  ClientIDMode="Static">
                                  
                                         </asp:DropDownList>
                                </td>
                                                 
                                <tr>
                                <td>
                                    Repeat<span >:</span>
                                </td>
                                <td  class="ddlRepeatfactor">
                                     <asp:DropDownList ID="ddlRepeatfactor" runat="server" AutoPostBack="False" CssClass="ddlRepeatfactor"     ClientIDMode="Static">
                                         </asp:DropDownList>
                                </td>
                            </tr>
            <%-- <tr id="trHideSchedule" runat="server">
                                        <td colspan = "3" align="center">
                                            <strong>Schedule Plan</strong>
                                            <asp:UpdatePanel ID = "UpdatePanel7" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView ID = "grid" runat="server" ShowHeaderWhenEmpty="True" AutoGenerateColumns="False"
                                                        ShowFooter = "True" Width="100%">
                                                        <RowStyle Height = "20px" />
                                                        <Columns>
                                                            <asp:BoundField DataField="BackupId" ReadOnly="True" SortExpression="BackupId" ShowHeader="False"
                                                                Visible="False"/>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:HiddenField ID="hfBackupId" runat="server"/>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Repeat">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="gvlblRepeatFactor" runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:DropDownList ID="gvddlRepeatFactor" AppendDataBoundItems="True" runat="server">
                                                                    </asp:DropDownList>
                                                                </EditItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:DropDownList ID="gvddlAddRepeatFactor" runat="server">
                                                                    </asp:DropDownList>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Day">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="gvlblScheduleDay" runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:DropDownList ID="gvddlScheduleDay" AppendDataBoundItems="True" runat="server">
                                                                    </asp:DropDownList>
                                                                </EditItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:DropDownList ID="gvddlAddScheduleDay" runat="server">
                                                                    </asp:DropDownList>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Time">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="gvlblScheduleTime" runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:DropDownList ID="gvddlScheduleTime" AppendDataBoundItems="True" runat="server">
                                                                    </asp:DropDownList>
                                                                </EditItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:DropDownList ID="gvddlAddScheduleTime" runat="server">
                                                                    </asp:DropDownList>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>

                                                            <%-- <asp:TemplateField ShowHeader="False">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="EditButton" runat="server" CommandName="Edit" AlternateText="Edit"/>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:LinkButton ID="lnkUpdate" runat="server" CausesValidation="True"
                                                                        CommandName="Update" Text="Update"></asp:LinkButton>
                                                                    &nbsp;<asp:LinkButton ID="lnkCancel" runat="server" CausesValidation="False"
                                                                              CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                                </EditItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField ShowHeader="False">
                                                                    <%--<ItemTemplate>
                                                                        <asp:ImageButton ID="DeleteButton" runat="server"
                                                                            CommandName="Delete" OnClientClick="return confirm('Are you sure you want to delete this event?');" '
                                                                        AlternateText="Delete" />
                                                                </ItemTemplate>
                                                                        <FooterTemplate>
                                                                            <asp:LinkButton ID="lnkAdd" runat="server" CommandName="ADD" Text="Add"></asp:LinkButton>
                                                                        </FooterTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                                                </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                        </td>
                                    </tr>
                                </table>

                            <tr>--%>
                                <td colspan="2">
                                    <asp:Button ID="btnSave" runat="server" TabIndex="3" Text="Save Changes"  ClientIDMode="Static" OnClientClick="return saveBackupSchedule();" class="btSave"/>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td>
                    <asp:UpdatePanel runat="server" ID="dd" >
                        <ContentTemplate>
                            <asp:Label ID="lblCurrentTime" runat="server" Style="font-weight: bold; font-size: 60px;
                                margin-left: 178px; color: #34AEFA; width:100%"></asp:Label>
                            <asp:Timer ID="Timer1" runat="server">
                            </asp:Timer>
                            <asp:Button runat="server" ID="refreshBtn" Visible="False" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>

