﻿Public Class OnlineUsers
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not IsPostBack) Then
            loadGrid()
        End If
    End Sub
    Private Sub loadGrid(Optional ByVal pageIndex As Integer = 0)
        Dim dt As New DataTable
        Dim filter = ""
        If txtdate.Text = "" Then
        Else
            Filter = txtdate.Text
        End If
        If filter = "" Then
            dt = Database.System.GetDataTable("EXEC temp_GetOnlineUsers")
        Else
            dt = Database.System.GetDataTable("EXEC temp_GetOnlineUsers {0}".SqlFormatString(filter))
        End If
        results.PageIndex = pageIndex
        results.DataSource = dt
        results.DataBind()
    End Sub

    Protected Sub results_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles results.PageIndexChanging
        LoadGrid(e.NewPageIndex)
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click

        Try
          
            loadGrid()

        Catch ex As Exception

        End Try
    
    End Sub
End Class