﻿Imports System.IO
Public Class EnvironmentStats
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            dllBindVendor()
            dllEnvironment()
            txtFromDate.Attributes.Add("min","1900-01-01")
            txtFromDate.Attributes.Add("max", "2078-06-06")
            txtToDate.Attributes.Add("max", "2078-06-06")
            txtToDate.Attributes.Add("min","1900-01-01")
        End If

    End Sub
    Private Sub dllBindVendor()
        Dim dtVendor As DataTable = Database.System.GetDataTable("SELECT  Id, Name FROM Vendor ORDER BY Name")
        If dtVendor.Rows.Count > 0 Then
            ddlVendor.FillFromTable(dtVendor, True, "--- Select Vendor ---")
        End If
    End Sub

    Private Sub dllEnvironment(Optional ByVal vendorId As String = "")
        Dim dtEnvironment As DataTable = Database.System.GetDataTable("SELECT id,Name FROM Organization WHERE " & IIf(vendorId = "", "1=1", "vendorId =" + vendorId) & " AND ( IsProduction = 'true' Or IsProduction = 1 )  ORDER BY Name")
        If dtEnvironment.Rows.Count > 0 Then
            ddlEnvironment.FillFromTable(dtEnvironment, True, "--- ALL ---")

        Else
            ddlEnvironment.FillFromTable(dtEnvironment, True, "--- No Environments ---")
        End If
    End Sub
    Private Sub ddlVendor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlVendor.SelectedIndexChanged
        dllEnvironment(ddlVendor.SelectedValue)
          StatsReport.Visible = false
    End Sub

    Protected Sub btnGenerate_Click(sender As Object, e As EventArgs) Handles btnGenerate.Click
        gvPhotosTakenMA.PageIndex = 0
        gvDeviceLicense.PageIndex = 0
        gvMostRecentMAC.PageIndex = 0
        gvTotalMACbyUser.PageIndex = 0
        loadDeviceLicense()
        StatsReport.Visible = True
        RunScript("$('.masklayer').hide();")
    End Sub

    Private Sub loadDeviceLicense() 'Function for Data grid bindings
        Dim vendorId As String = ddlVendor.SelectedValue
        Dim orgId As String = ddlEnvironment.SelectedValue
        Dim FromDate As String = txtFromDate.Value
        Dim ToDate As String = txtToDate.Value

        Dim dsEnvStats As DataSet = Database.System.GetDataSet("EXEC sp_GetEnvironmentStats {0},{1},{2},{3}".SqlFormatString(vendorId, orgId, FromDate, ToDate))

        ViewState("EnvironmentStats") = dsEnvStats
        BindDeviceLicense(dsEnvStats.Tables(0))
        BindTotalMACbyUser(dsEnvStats.Tables(1))
        BindPhotosTakenMA(dsEnvStats.Tables(2))
        BindMostRecentMAC(dsEnvStats.Tables(3))
    End Sub
    Public Sub BindDeviceLicense(ByVal dt As DataTable)
        gvDeviceLicense.DataSource = dt
        gvDeviceLicense.DataBind()
        If gvDeviceLicense.Rows.Count > 0 Then
            PageCountDeviceLicense.InnerHtml = "Displaying records {0} to {1} of {2}".FormatString(gvDeviceLicense.PageIndex * gvDeviceLicense.PageSize + 1, gvDeviceLicense.PageIndex * gvDeviceLicense.PageSize + gvDeviceLicense.Rows.Count, dt.Rows.Count)
        	btnDLSDownload.Visible = True
        Else
            PageCountDeviceLicense.InnerHtml = "&nbsp"
            btnDLSDownload.Visible = False
        End If
    End Sub

    Public Sub BindTotalMACbyUser(ByVal dt As DataTable)
        gvTotalMACbyUser.DataSource = dt
        gvTotalMACbyUser.DataBind()
        Dim total As Integer
        If gvTotalMACbyUser.Rows.Count > 0 Then
            PageCountTotalMACbyUser.InnerHtml = "Displaying records {0} to {1} of {2}".FormatString(gvTotalMACbyUser.PageIndex * gvTotalMACbyUser.PageSize + 1, gvTotalMACbyUser.PageIndex * gvTotalMACbyUser.PageSize + gvTotalMACbyUser.Rows.Count, dt.Rows.Count)
        	btnMACUserDownload.Visible = True
	        gvTotalMACbyUser.FooterRow.Cells(1).Text="TOTAL"
	        gvTotalMACbyUser.FooterRow.Cells(1).Font.Bold="true"
	        gvTotalMACbyUser.FooterRow.Cells(1).Attributes.Add("style","color:black")
	        For k As Integer = 0 To gvTotalMACbyUser.Rows.Count-1 
	        	Dim cRow As GridViewRow = gvTotalMACbyUser.Rows(k)
	        	total += cRow.Cells(2).Text
	        Next 
	        gvTotalMACbyUser.FooterRow.Cells(2).Text =total.ToString()
	        gvTotalMACbyUser.FooterRow.Cells(2).Font.Bold="true"
	        gvTotalMACbyUser.FooterRow.Cells(2).Attributes.Add("style","color:black")
        Else
            PageCountTotalMACbyUser.InnerHtml = "&nbsp"
            btnMACUserDownload.Visible = False
        End If
        If dt.Rows.Count > 0 Then
        	GrandTotal = 0
        	For i As Integer = 0 To dt.Rows.Count-1
        		GrandTotal += dt.Rows(i).Item(3)
        	Next
        End If
        Dim RowSpan As Integer = 2
        For i As Integer = gvTotalMACbyUser.Rows.Count - 2 To 0 Step -1
            Dim currRow As GridViewRow = gvTotalMACbyUser.Rows(i)
            Dim prevRow As GridViewRow = gvTotalMACbyUser.Rows(i + 1)
            If currRow.Cells(0).Text = prevRow.Cells(0).Text Then
                currRow.Cells(0).RowSpan = RowSpan
                prevRow.Cells(0).Visible = False
                RowSpan += 1
            Else
                RowSpan = 2
            End If
        Next

    End Sub

    Public Sub BindPhotosTakenMA(ByVal dt As DataTable)
        gvPhotosTakenMA.DataSource = dt
        gvPhotosTakenMA.DataBind()
        If gvPhotosTakenMA.Rows.Count > 0 Then
            PageCountPhotosTakenMA.InnerHtml = "Displaying records {0} to {1} of {2}".FormatString(gvPhotosTakenMA.PageIndex * gvPhotosTakenMA.PageSize + 1, gvPhotosTakenMA.PageIndex * gvPhotosTakenMA.PageSize + gvPhotosTakenMA.Rows.Count, dt.Rows.Count)
        	btnPhotoDownload.Visible = True
        Else
            PageCountPhotosTakenMA.InnerHtml = "&nbsp"
            btnPhotoDownload.Visible = False
        End If
    End Sub

    Public Sub BindMostRecentMAC(ByVal dt As DataTable)
        gvMostRecentMAC.DataSource = dt
        gvMostRecentMAC.DataBind()
        If gvMostRecentMAC.Rows.Count > 0 Then
            PageCountMostRecentMAC.InnerHtml = "Displaying records {0} to {1} of {2}".FormatString(gvMostRecentMAC.PageIndex * gvMostRecentMAC.PageSize + 1, gvMostRecentMAC.PageIndex * gvMostRecentMAC.PageSize + gvMostRecentMAC.Rows.Count, dt.Rows.Count)
            btnMACDownload.Visible = True
        Else
            PageCountMostRecentMAC.InnerHtml = "&nbsp"
            btnMACDownload.Visible = False
        End If
    End Sub
    Protected Sub gvDeviceLicensePageChanging(sender As Object, e As GridViewPageEventArgs)
        gvDeviceLicense.PageIndex = e.NewPageIndex
        Dim ds As DataSet = ViewState("EnvironmentStats")
        BindDeviceLicense(ds.Tables(0))
    End Sub


    Protected Sub gvTotalMACbyUserPageChanging(sender As Object, e As GridViewPageEventArgs)
        gvTotalMACbyUser.PageIndex = e.NewPageIndex
        Dim ds As DataSet = ViewState("EnvironmentStats")
        BindTotalMACbyUser(ds.Tables(1))
    End Sub

    Protected Sub gvPhotosTakenMAPageChanging(sender As Object, e As GridViewPageEventArgs)
        gvPhotosTakenMA.PageIndex = e.NewPageIndex
        Dim ds As DataSet = ViewState("EnvironmentStats")
        BindPhotosTakenMA(ds.Tables(2))
    End Sub

    Protected Sub gvMostRecentMACPageChanging(sender As Object, e As GridViewPageEventArgs)
        gvMostRecentMAC.PageIndex = e.NewPageIndex
        Dim ds As DataSet = ViewState("EnvironmentStats")
        BindMostRecentMAC(ds.Tables(3))
    End Sub
    
    Public Function ExportGridView(ByVal dt As DataTable, Optional ByVal formatColumn As String = Nothing, Optional ByVal format As String = Nothing,Optional ByVal removeColumn As String = Nothing,Optional ByVal formatCell As Boolean = false) As GridView
		'Create a dummy GridView
		Dim GridView1 As New GridView()
		GridView1.AllowPaging = False
		GridView1.AutoGenerateColumns = False
		Dim bfieldName As String
		Dim bHeaderText As String
		GridView1.Columns.Clear()
		For Each column As DataColumn In dt.Columns
		    bfieldName = column.ColumnName
		    bHeaderText = column.ColumnName
		    Dim bfield As New BoundField()
		    bfield.HeaderText = bHeaderText
            bfield.DataField = bfieldName
            If bfieldName = "Organization" Or bfieldName = "LastReviewedDate" Then
                bfield.ItemStyle.Width = 200
            ElseIf bfieldName = "UserName" Or bfieldName = "TotalMACbyUser" Then
                bfield.ItemStyle.Width = 150
            Else
                bfield.ItemStyle.Width = 125
            End If

            If (column.ColumnName = formatColumn) Then
		        bfield.DataFormatString = format
		    End If
		    If (column.ColumnName <> removeColumn) Then
		        GridView1.Columns.Add(bfield)
		    End If
		Next
		GridView1.DataSource = Nothing
		GridView1.DataSource = dt
		GridView1.DataBind()
		If formatCell= True Then
			Dim RowSpan As Integer = 2
	        For i As Integer = GridView1.Rows.Count - 2 To 0 Step -1
	            Dim currRow As GridViewRow = GridView1.Rows(i)
	            Dim prevRow As GridViewRow = GridView1.Rows(i + 1)
	            If currRow.Cells(0).Text = prevRow.Cells(0).Text Then
	                currRow.Cells(0).RowSpan = RowSpan
	                prevRow.Cells(0).Visible = False
	                RowSpan += 1
	            Else
	                RowSpan = 2
	            End If
	        Next
		End If

		Return GridView1
	End Function
	Protected Sub btnDLSDownload_Click(sender As Object, e As EventArgs) Handles btnDLSDownload.Click
        Dim ds As DataSet = ViewState("EnvironmentStats")
        Dim GridView1 As GridView = ExportGridView(ds.Tables(0))
        ExportToExcel(GridView1, "DeviceLicenseStats.xlsx", "Device License Stats", ds.Tables(0))
    End Sub
    Protected Sub btnMACUserDownload_Click(sender As Object, e As EventArgs) Handles btnMACUserDownload.Click
		Dim ds As DataSet = ViewState("EnvironmentStats")
        Dim GridView1 As GridView = ExportGridView(ds.Tables(1),,,"OrgId",true)
        ExportToExcel(GridView1, "TotalMACByUser.xlsx", "Total MAC By User", ds.Tables(1), True, "TotalMACbyUser")
    End Sub
    
    Protected Sub btnPhotoDownload_Click(sender As Object, e As EventArgs) Handles btnPhotoDownload.Click
        Dim ds As DataSet = ViewState("EnvironmentStats")
        Dim GridView1 As GridView = ExportGridView(ds.Tables(2),,,"OrgId")
        ExportToExcel(GridView1, "PhotosTakenMA.xlsx", "Photos Taken MA", ds.Tables(2))
    End Sub
    
    Protected Sub btnMACDownload_Click(sender As Object, e As EventArgs) Handles btnMACDownload.Click
		Dim ds As DataSet = ViewState("EnvironmentStats")
        Dim GridView1 As GridView = ExportGridView(ds.Tables(3), "LastReviewedDate", "{0:MM/dd/yyyy h:mm tt}","OrgId")
        ExportToExcel(GridView1, "MostRecentMAC.xlsx", "Most Recent MAC", ds.Tables(3),)
    End Sub
    Public Sub ExportToExcel(GridView1 As GridView, ByVal filename As String, title As String, Optional dt As DataTable = Nothing, Optional showTotal As Boolean = False, Optional columnName As String = "", Optional sheetName As String = "data")
        Response.Clear()
        Response.Buffer = True
        Response.ClearContent()
        Response.ClearHeaders()
        Response.AddHeader("Content-Disposition", "attachment;filename=" + filename)
        Response.Charset = ""
        Response.ContentType = "application/vnd.openxml.formats-officedocument.spreadsheetml.sheet"
        Dim excelStream = ExcelGenerator.ExportGrid(GridView1, dt, title, sheetName, showTotal, columnName)
        excelStream.CopyTo(Response.OutputStream)
        Response.End()
    End Sub
    Public Shared Property GrandTotal As Integer

End Class