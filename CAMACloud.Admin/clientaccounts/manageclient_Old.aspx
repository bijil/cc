﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ClientAdmin.master" CodeBehind="manageclient_Old.aspx.vb" Inherits="CAMACloud.Admin.manageclient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .form-table
        {
        }
        
        .form-table .caption-cell
        {
            padding-bottom: 3px;
            padding-right: 25px;
            vertical-align: bottom;
            
        }
        
        
        .form-table .caption
        {
        }
        
        .form-table .caption:after
        {
            content: ":";
        }
        
        .form-table .input
        {
            padding: 4px;
            border-radius: 4px;
            border: 1px solid #CFCFCF;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField runat="server" ID="hdnOrganizationName" />
    <asp:HiddenField runat="server" ID="hdnOrganizationId" />
    <div style="text-align: left; padding-right: 10px;">
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/clientaccounts/list.aspx">Select another county</asp:HyperLink></div>
    <h1 runat="server" id="clientTitle">
        Client Organization Name</h1>
    <table style="width: 100%">
        <tr>
            <td>
                
                <table class="form-table">
                    <tr>
                        <td class="caption-cell"><span class="caption">Organization</span> </td>
                        <td colspan="5">
                            <asp:TextBox runat="server" ID="txtName" Width="570px" CssClass="input" /></td>
                    </tr>
                    <tr>
                        <td class="caption-cell"><span class="caption">City</span> </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtCity" CssClass="input" Width="180px" /></td>
                        <td class="caption-cell"><span class="caption">County</span> </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtCounty" CssClass="input" Width="180px" /></td>
                        <td class="caption-cell"><span class="caption">State</span> </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtState" CssClass="input" Width="60px" /></td>
                    </tr>
                </table>
                <h2>Server & Database Settings</h2>
                <table class="form-table">
                    <tr>
                        <td class="caption-cell">Main Host Server: </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlHostServer" width="200px" CssClass="input"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="caption-cell">Mobile Assessor Host Key: </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtMAHostKey" Width="170px" CssClass="input" />
                        </td>
                    </tr>
                </table>
                <h2>Quota Settings</h2>
                <table class="form-table">
                    <tr>
                        <td class="caption-cell">Maximum parcels downloadable to MobileAssessor: </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtMaxMAParcels" Width="60px" CssClass="input" />
                        </td>
                    </tr>
                </table>
                <asp:Button runat="server" ID="btnSave" Text="Update Client Account" Height="26px" />
            </td>
            <td style="width: 250px;">
                <asp:GridView runat="server" ID="gridModules" Width="240px" ShowHeader="true">
                    <Columns>
                        <asp:TemplateField>
                            <ItemStyle Width="20px" />
                            <ItemTemplate>
                                <asp:HiddenField runat="server" ID="MID" Value='<%# Eval("RoleId") %>' />
                                <asp:CheckBox runat="server" ID="Selected" Checked='<% #Eval("Selected") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Name" HeaderText="Allowed Modules" />
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
