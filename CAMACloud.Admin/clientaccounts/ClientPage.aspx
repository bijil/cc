﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/ClientAdmin.master" AutoEventWireup="false" Inherits="CAMACloud.Admin.clientaccounts_ClientPage" Codebehind="ClientPage.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   
    <style type="text/css">
        
         #dashboard-content
             { 
                overflow:auto; 
                width:1000px; 
            
             }
         #leftcontent, #rightcontent
             {
                 margin :5px; 
            
             }
         #leftcontent
             {
                  float:left;
                  width:600px;
             }
         #rightcontent
             { 
                 float:right;
                 width:300px;
   
             }  
         #rght_top_content
            {  
                margin-top:6px;
                margin-left:5px;
                float:right;
                width:300px;
                height:200px;
            }
        #lf_list_panel
            {
                 margin-top:5px;
                 width:680px;
                 float:left;
            } 
        #listshortcut
            {
             margin-left:-45px;
            }
        
        #lf_list_panel ul li
            {
              display: inline;
              margin-left:0px; 
            }
       
        li.trace_item img 
           {
            background-color : transparent;
            margin-left:11px;
           }  
       .heading_panel
           {
             border:1px solid #CFCFCF;
             border-top-left-radius:4px;
             border-top-right-radius:4px;
             border-bottom-left-radius:4px;
             border-bottom-right-radius:4px; 
             background-color:#e0eaf0;
             height:25px;
             margin-left:0px;
             margin-right:0px;
           }
       #accountinfo
           {
            padding :2px;   
           }
      #centercontent
          {
            margin-top:155px;
            width:680px;
          }
        #license-tab
          {
           padding:2px;
           margin-top:200px;   
          }
          .searchText
          {
           border-radius:4px;   
           }
          

    </style>
   
    <script type="text/javascript">
     
        function showPopup(title, classname,source) {
            if ($(source).attr('menu').trim() == 'new') {
                document.getElementById("<%=btnClear.ClientID%>").click();
            }

          
            $(classname).dialog({
                modal: true,
                width: 420,
                height: 298,
                resizable: false,
                title: title,
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        }

        function hidePopup(classname) {
            $(classname).dialog('close');
        }
    
    </script>
   
  </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<div id="dashboard-content">
  <asp:HiddenField ID="hdnOrgid" runat="server" />
    <asp:HiddenField ID="hdnOrgname" runat="server" />
 <%--   <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>--%>
<div id="leftcontent">
  <div id="lf_list_panel">
     <div class="ui-state-default">
         <table>
           <tr>
             <td class="style1">
                 <asp:Label ID="lblMainTitle" runat="server"></asp:Label>
&nbsp;</td>
            </tr>
          </table>
      </div> 
    <ul id="listshortcut">
       <li class="trace_item"><a href="#" menu="new" onclick="showPopup('New County Registration','.newaccount',this);"><img style="margin-left:5px;" src="../App_Static/images/Dashboard/new-account.png" alt="" /></a></li>
       <li class="trace_item"><a href="#" menu="server" onclick="showPopup('Server Settings','.Serverconfig-tab',this);"><img src="../App_Static/images/Dashboard/server-config.png" alt="" /></a></li>
       <li class="trace_item"><a href="#" menu="edit" onclick ="showPopup('Edit Organization-','.newaccount',this);"><img src="../App_Static/images/Dashboard/edit-account.png" alt="" /></a></li>
       <li class="trace_item"><a href="#" menu="roles" onclick="showPopup('Select Roles','.roles',this);"><img src="../App_Static/images/Dashboard/roles.png" alt="" /></a></li>
       <li class="trace_item"><a href="#" menu="license" onclick="showPopup('License Keys','.license-tab',this);"><img src="../App_Static/images/Dashboard/license-key.png" alt="" /></a></li>
       <li class="trace_item"><a href="#" menu="license"><img src="../App_Static/images/Dashboard/user-info.png" alt="" /></a></li>
    </ul> 
 </div> 
 <div id="centercontent">  
 <div id="search-panel">
      <div class="ui-state-default">  
      <table>
            <tr>
                <td>
                    <label>Client-Details</label>
                </td>
            </tr>
       </table>
     </div>

<div id="gridcontents">
 <%-- <asp:UpdatePanel runat="server">
    <ContentTemplate>--%>
    <div id="right-menu" style="margin-right:0px;">
       <table style="margin-left:-3px;">
          <tr>
            <td>
                <asp:ImageButton ID="imagActive" ImageUrl="~/App_Static/images/Dashboard/active-sort-icon.png" runat="server" /></td>
            <td><asp:ImageButton ID="imgPending" ImageUrl="~/App_Static/images/Dashboard/pending-sort-icon.png" runat="server" /></td>
             <td><asp:ImageButton ID="imgAll" ImageUrl="~/App_Static/images/Dashboard/viewall-sort-icon.png" runat="server" /></td>
             <td style="width:175px;"></td>
                <td style="margin-bottom:0px">
                    <asp:TextBox CssClass="searchText"  runat="server" id="txtSearch" Width="200px" Height="20px" MaxLength="30"  placeholder="County Name" />
                </td>
               
                <td> 
                   <asp:ImageButton ID="imgSearch" ImageUrl="~/App_Static/images/Dashboard/search-sort-icon.png" runat="server" />
                </td>
            </tr>
        </table></div> 
   
   <div id="grid-data" style="margin-top:-8px;">
     <asp:GridView runat="server" ID="results" AllowPaging="True" PageSize="8">
        <Columns>
            <asp:TemplateField HeaderText="County/Organization">
             <ItemStyle Width="200px"></ItemStyle>
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="lbSelect" Text='<%# Eval("Name") %>' 
                        CommandArgument='<%# Eval("Id") %>' Font-Size="9pt" 
                        Font-Bold="True" oncommand="lbSelect_Command" />
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate" >
             <ItemStyle Width="170px"></ItemStyle>
            </asp:BoundField>
            <asp:TemplateField HeaderText="Status"  ItemStyle-Width="140px">
                <ItemTemplate>
                  <asp:Image ID="Image1" runat="server" ImageUrl='<%# IIf(Eval("status") = "Active", "~/App_Static/Images/Dashboard/active-icon.png", "~/App_Static/Images/Dashboard/pending-icon.png")%>' />
                </ItemTemplate>

<ItemStyle Width="140px"></ItemStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Actions" ItemStyle-Width="160px">
                <ItemTemplate>
                  <asp:ImageButton ID="Imgedit" runat="server" 
                        ImageUrl="~/App_Static/images/Dashboard/edit-icon.png" 
                        oncommand="Imgedit_Command"  CommandArgument='<%# Eval("Id") %>' />
                   <asp:ImageButton ID="Imglicense" runat="server" 
                        ImageUrl="~/App_Static/images/Dashboard/gd-license-icon.png" 
                        oncommand="Imglicense_Command" CommandArgument='<%# Eval("UID") %>'  />
                  </ItemTemplate>
                               
<ItemStyle Width="160px"></ItemStyle>
                               
            </asp:TemplateField>
             </Columns>
              <EmptyDataTemplate>
                    <div class="info">
                        No Counties found.
                    </div>
                </EmptyDataTemplate>
          </asp:GridView>
      </div>
    </div>
 <%-- </ContentTemplate>
 </asp:UpdatePanel>--%>
</div>
  <div id="accountinfo"  style="display:none;">
    <table>
      <tr> 
           <td>Account-Info :</td>
           <td><asp:Label ID="lblorgname" runat="server" Text=""></asp:Label></td>
      </tr>
      <tr>
          <td>City</td>
          <td><asp:Label ID="lblcity" runat="server" Text=""></asp:Label></td>
      </tr>
      <tr>
          <td>State</td>
          <td><asp:Label ID="lblstate" runat="server" Text=""></asp:Label></td>
      </tr>
      <tr>
          <td>County</td>
          <td><asp:Label ID="lblcounty" runat="server" Text=""></asp:Label></td>
      </tr>
   </table>
 </div>  
 <div class="newaccount" style="display:none">
  <asp:UpdatePanel ID="UpdatePanel4" runat="server">
  <ContentTemplate>
  <table id="accountcontent" style="padding:2px;">
      <tr> 
           <td>Name:</td>
           <td>
               <asp:TextBox ID="txtorgname" runat="server" Width="200px"></asp:TextBox></td>
      </tr>
      <tr>
          <td>City:</td>
          <td> <asp:TextBox ID="txtcity" runat="server" Width="200px"></asp:TextBox></td>
      </tr>
      <tr>
          <td>State:</td>
          <td> <asp:TextBox ID="txtstate" runat="server" Width="50px"></asp:TextBox></td>
      </tr>
      <tr>
          <td>County:</td>
          <td> <asp:TextBox ID="txtcounty" runat="server" Width="200px"></asp:TextBox></td>
      </tr>
      <tr>
      <td>
          <%--<asp:Button ID="btnClear" class="accclear"  runat="server" Text="Cancel"  />--%></td>
      <td>
          <asp:Button ID="btnSaveAccount" runat="server" Text="Save Account" /> <asp:Button ID="btnClear" class="accclear"  runat="server" Text="Cancel"  /></td>    
      </tr>
   </table>
   </ContentTemplate>
   </asp:UpdatePanel> 
 </div>      
     
     
  </div>
 
  
  </div>
  <div id="rightcontent">
  <div id="rght_top_content">
      <div class="ui-state-default ui-corner-all">
            <table>
               <tr>
                  <td> 
                      <asp:Label ID="lbltitle" runat="server" Font-Bold="true"  Text="New Account"></asp:Label>
                   </td>
                </tr>
             </table>
       </div>
       <table id="lefttab"><tr><td></td></tr></table>
   </div>
  <div id="license-tab"> 
      <div class="ui-state-default ui-corner-all">
           <table>
               <tr>
                  <td> 
                      <asp:Label ID="Label1" runat="server" Font-Bold="true"  Text="License-Status"></asp:Label>
                   </td>
                </tr>
             </table>
      </div> 
  
   </div>
 </div>
 </div>
 <div class="roles" style="display:none;">
 <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                          <ContentTemplate>
                          <asp:Panel runat="server" ID="pnlUserEdit" CssClass="user-edit-panel" style="width:200px;height:110px;">
                           <table class="comparable-edit" style="margin-left:20px;">
                        <tr>
                            <td class="v-split" style="width: 12px;"></td>
							<td style="width:200px;vertical-align:top;">
								<h3>
									Select Roles</h3>
								<asp:CheckBoxList ID="cblRoles" runat="server">
								</asp:CheckBoxList>
							</td>
                        </tr>
                        <tr> <td> </td><td><div style="margin-top: 10px; margin-bottom: 15px;">
									<asp:Button runat="server" ID="btnRoles" Text="Save" 
                                        />
				  
                            </table></asp:Panel></ContentTemplate> </asp:UpdatePanel> 
 
 </div>
 <div class="Serverconfig-tab"style="display:none;" >
  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                          <ContentTemplate>
                           <asp:Panel runat="server" ID="Panel1" CssClass="user-edit-panel" style="width:400px;height:210px;">
                            <table class="comparable-edit" style="margin-left:15px;">
                                <tr>   <td class="dentry">
                                        Host Name:</td>
                                    <td>
                                        <asp:DropDownList ID="DrpHost" runat="server" Width="195px">
                                        </asp:DropDownList>
                                        <%--<asp:RequiredFieldValidator ID="HostNameValidator" runat="server" 
                                            ControlToValidate="DrpHost" ErrorMessage="*" ForeColor="Red" InitialValue="0" 
                                            ValidationGroup="serverConfig"></asp:RequiredFieldValidator>--%>
                                    </td>
                                   
                                </tr>
                                <tr>
                                  <td class="dentry">
                                        Server&nbsp; Name:</td>
                                    <td>
                                        <asp:DropDownList ID="drpServer" runat="server" AutoPostBack="True" 
                                            Width="195px">
                                        </asp:DropDownList>
                                     <%--   <asp:RequiredFieldValidator ID="ServerValidator" runat="server" 
                                            ControlToValidate="drpServer" ErrorMessage="*" ForeColor="Red" InitialValue="0" 
                                            ValidationGroup="serverConfig" />--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="dentry">
                                        Database Name :</td>
                                    <td>
                                        <asp:TextBox ID="txtDbName" runat="server" Width="190px">
                                        </asp:TextBox><asp:RequiredFieldValidator
                                            ID="dbReqValidator" runat="server" ControlToValidate="txtDbName" ErrorMessage="*" ValidationGroup="serverConfig"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="dentry">
                                        User Name:</td>
                                    <td>
                                        <asp:TextBox ID="txtUserName" runat="server" ReadOnly="True" Width="190px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="dentry">
                                        Password:</td>
                                    <td>
                                        <asp:TextBox ID="txtPwd" runat="server" ReadOnly="True" TextMode="Password" 
                                            Width="190px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="dentry" >
                                         <asp:UpdateProgress ID="updProgress"
                                                    AssociatedUpdatePanelID="UpdatePanel1"
                                                    runat="server">
                                                        <ProgressTemplate>            
                                                        <img alt="progress" src="../App_Static/images/progress.gif"/>
                                                           Configuring...            
                                                        </ProgressTemplate>
                                          </asp:UpdateProgress></td>
                                    <td>

                                      <div style="margin-top: 10px; margin-bottom: 15px; margin-left:-2px;">
                                            <asp:Button ID="btnServerConfig" ValidationGroup="serverConfig" runat="server" Text="Config Server" 
                                                />
                                            <asp:Button ID="btnCancel" runat="server" Text="Cancel"   OnClientClick="hidePopup('.Serverconfig-tab');" />
                                            <asp:Label ID="lblalert" runat="server" Visible="False"></asp:Label>
                                        </div>
                                    </td>
                                </tr>
                            </table></asp:Panel>  </ContentTemplate>
        </asp:UpdatePanel>
  </div>
   <div class="license-tab" style="display:none;" >
                          <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                          <ContentTemplate>
                            <table class="comparable-edit"style="margin-top:50px;margin-left:20px;">
                                <tr>
                                      <td class="dentry" colspan="2">
                                          Generate New License
                                           <asp:DropDownList runat="server" ID="ddlNumber" Width="50px" />
                                          <%-- <asp:Button ID="btnCreate" runat="server" Text="Generate" />--%>
                                        </td>
                                        <td> </td>
                                </tr>
                                <tr>
                                 <td style="text-align: right;">
                        <asp:TextBox runat="server" ID="txtEmail" Width="200px" CssClass="txt-email"/></td>
                         <td>     
                            <asp:Button Text="Email Licenses " runat="server" ID="btnEmail"/>
                    </td></tr>
                            </table></ContentTemplate> </asp:UpdatePanel> 
                       
                    </div> 
                    
                    </div> 
  </asp:Content>
 