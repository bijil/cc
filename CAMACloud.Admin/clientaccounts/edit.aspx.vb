﻿Imports System.IO
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.Net.Mail
Partial Class clientaccounts_edit
    Inherits System.Web.UI.Page

    Dim dbstatus As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            LoadHost()
            ddlNumber.FillNumbers(1, 10)
            LoadRolesList()
            LoadServerList()
            LoadClientDetailsForEdit()
            SetFormTitle()
        End If
    End Sub

    Sub SetFormTitle()
        If hdnOrgname.Value = "" Then
            lbltiltle.Text = "Create New Account"
        Else
            lbltiltle.Text = "Account Settings - " + hdnOrgname.Value
        End If
    End Sub
  

    Private Sub LoadRolesList()
        Dim dtOrgRoles As DataTable = Database.System.GetDataTable("SELECT Name, Id FROM AppRoles WHERE ID IN (SELECT RoleId FROM OrganizationRoles )")
        cblRoles.DataSource = dtOrgRoles
        cblRoles.DataTextField = "Name"
        cblRoles.DataValueField = "Id"
        cblRoles.DataBind()
    End Sub

    Private Sub LoadHost()
        'Dim dthost As DataTable = Database.System.GetDataTable("Select id,HostName from HostSettings")
        Dim dthost As DataTable = Database.System.GetDataTable("Select id,Name from Organization")
        DrpHost.DataSource = dthost
        'DrpHost.DataTextField = "HostName"
        DrpHost.DataTextField = "Name"
        DrpHost.DataValueField = "id"
        DrpHost.DataBind()
        DrpHost.InsertItem("--Select--", "0", 0)
    End Sub

    Private Sub LoadServerList()
        Dim dtserver As DataTable = Database.System.GetDataTable("Select id,Name from Database")
        drpServer.DataSource = dtserver
        drpServer.DataTextField = "Name"
        drpServer.DataValueField = "id"
        drpServer.DataBind()
        drpServer.InsertItem("--Select--", "0", 0)
    End Sub

    Protected Sub drpServer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpServer.SelectedIndexChanged
        Dim dtserver As DataTable = Database.System.GetDataTable("Select HostName,DBUser,DBPassword from Database where id=" + drpServer.SelectedItem.Value + "")
        If (dtserver.Rows.Count > 0) Then
            txtUserName.Text = dtserver.Rows(0)("DBUser").ToString()
            txtPwd.Text = dtserver.Rows(0)("DBPassword").ToString()
        End If
    End Sub

    Private Sub ClearForm()
        hdnOrgid.Value = ""
        txtName.Text = String.Empty
        txtCity.Text = String.Empty
        txtCounty.Text = String.Empty
        txtState.Text = String.Empty
        txtDbName.Text = String.Empty
        txtUserName.Text = String.Empty
        txtPwd.Text = String.Empty
        drpServer.SelectedIndex = 0
        DrpHost.SelectedIndex = 0
        For Each li As ListItem In cblRoles.Items
            li.Selected = False
        Next
    End Sub

    Sub AssignRolesFromList(ByVal Orgid As String)
        For Each li As ListItem In cblRoles.Items
            If li.Selected Then
                If Not IsOrganizationInRole(Orgid, li.Value) Then
                    AddOrgToRole(Orgid, li.Value)
                End If
            Else
                If IsOrganizationInRole(Orgid, li.Value) Then
                    RemoveOrganizationFromRole(Orgid, li.Value)
                End If
            End If
        Next
    End Sub

    Private Sub LoadClientDetailsForEdit()
        If Request("ouid") Is Nothing Then
            hdnOrgid.Value = ""
            lblalert.Text = "Account created successfully.."
        Else
            hdnOrgid.Value = Database.System.GetIntegerValueOrInvalid("SELECT ID FROM Organization WHERE UID = {0}".SqlFormatString(Request("ouid")))
            hdnOrgname.Value = Database.System.GetStringValue("SELECT Name FROM Organization WHERE ID = {0}".SqlFormatString(hdnOrgid.Value))
            Dim dt As DataTable = Database.System.GetDataTable("SELECT * FROM Organization where Id=" + hdnOrgid.Value + "")
            If (dt.Rows.Count > 0) Then
                txtName.Text = dt.Rows(0)("Name").ToString()
                txtCounty.Text = dt.Rows(0)("county").ToString()
                txtState.Text = dt.Rows(0)("State").ToString()
                txtCity.Text = dt.Rows(0)("city").ToString()
                'dbstatus = dt.Rows(0)("status").ToString()
                'If (dbstatus = "New") Then
                '    GenerateDbName(txtName.Text)
                'Else
                '    txtDbName.Text = dt.Rows(0)("DBName").ToString()
                '    drpServer.SelectedIndex = drpServer.Items.IndexOf(drpServer.Items.FindByText(dt.Rows(0)("DBHost").ToString()))
                '    DrpHost.SelectedIndex = DrpHost.Items.IndexOf(DrpHost.Items.FindByText(dt.Rows(0)("CCHost").ToString()))
                '    txtUserName.Text = dt.Rows(0)("DBUser").ToString()
                '    txtPwd.Text = dt.Rows(0)("DBPassword").ToString()
                '    txtDbName.ReadOnly = True
                'End If

            End If
            For Each li As ListItem In cblRoles.Items
                If IsOrganizationInRole(hdnOrgid.Value, li.Value) Then
                    li.Selected = True
                Else
                    li.Selected = False
                End If
            Next
            lblalert.Text = "Updated changes successfully.."
        End If
    End Sub

    Function ConfigServer() As Boolean
        Dim Isdbcreate As Boolean
        Try
            If (hdnOrgid.Value <> "") Then
                Isdbcreate = False
                Database.System.Execute("create database " + txtDbName.Text)
                DBCreation("D:\Workspace\DCS Projects\CAMACloud II\Documents\DB Scripts\ClientDBScript\CCMODEL.sql")
                Isdbcreate = True
            Else
                Isdbcreate = False
            End If
        Catch ex As Exception
            Alert(ex.Message)
        End Try
        Return Isdbcreate
    End Function

    Sub DBCreation(ByVal fileName As String)
        Dim scriptfile As String = System.IO.File.ReadAllText(fileName)
        Dim connStr As String = "Server=" + drpServer.SelectedItem.Text + ";Database=" + txtDbName.Text + ";"
        If txtUserName.Text = "" Then
            connStr += "Integrated Security=True;"
        Else
            connStr += "User Id=" + txtUserName.Text + ";Password=" + txtPwd.Text + ";"
        End If
        connStr += "Connection Timeout=600;"
        Try
            Dim conn As SqlConnection = New SqlConnection(connStr)
            If conn IsNot Nothing Then
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                End If
            End If

            'Dim server As Microsoft.SqlServer.Management.Smo.Server = New Microsoft.SqlServer.Management.Smo.Server(New Microsoft.SqlServer.Management.Common.ServerConnection(conn))
            'server.ConnectionContext.ExecuteNonQuery(scriptfile)
            'conn.Close()


        Catch ex As Exception
            Throw New Exception("Error on process script")
        End Try

    End Sub

    Function IsOrganizationInRole(ByVal orgid As String, ByVal rolid As String) As Boolean
        Dim dt As DataTable = Database.System.GetDataTable("SELECT * FROM OrganizationRoles WHERE OrganizationId=" + orgid + " AND Roleid=" + rolid + "")
        If (dt.Rows.Count > 0) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub AddOrgToRole(ByVal orgid As String, ByVal roleid As String)
        Dim sql As String = String.Format("INSERT INTO OrganizationRoles (OrganizationId,RoleId) VALUES({0},{1})", orgid, roleid)
        Database.System.Execute(sql)
    End Sub

    Private Sub RemoveOrganizationFromRole(ByVal orgid As String, ByVal roleid As String)
        Dim sql As String = String.Format("DELETE FROM OrganizationRoles WHERE OrganizationId={0} AND RoleId={1}", orgid, roleid)
        Database.System.Execute(sql)
    End Sub

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        'hidLastTab.Value = 2
        'hdnOrgid.Value = lblHidden.Text
        'If hdnOrgid.Value <> "" Then
        '    For i = 1 To CInt(ddlNumber.SelectedValue)
        '        Dim license As String = RandomString(16)
        '        Dim otp As Integer = RandomNumber()
        '        Dim sql As String = String.Format("INSERT INTO DeviceLicense (OrganizationId,LicenseKey,OTP,CreatedDate) VALUES({0},'{1}',{2},'{3}')", hdnOrgid.Value, license, otp, Date.UtcNow.ToString("yyyy-MM-dd HH:mm:ss"))
        '        Database.System.Execute(sql)
        '        LicenseTable(license, otp)
        '    Next
        'Else
        '    Alert("Invalid attempt")
        'End If
    End Sub

    Sub LicenseTable(ByVal licensekey As String, ByVal otp As String)
        Dim dtLicense As New DataTable()
        If (ViewState("dtLicense") Is Nothing) Then
            dtLicense.Columns.Add("LicenseKey")
            dtLicense.Columns.Add("OTP")
            Dim dr = dtLicense.NewRow()
            dr("LicenseKey") = licensekey
            dr("OTP") = otp
            dtLicense.Rows.Add(dr)
        Else
            dtLicense = ViewState("dtLicense")
            Dim dr = dtLicense.NewRow()
            dr("LicenseKey") = licensekey
            dr("OTP") = otp
            dtLicense.Rows.Add(dr)
        End If
        ViewState("dtLicense") = dtLicense
    End Sub

    Private Function RandomString(ByVal size As Integer) As String
        Dim builder As New StringBuilder()
        Dim licenseKey As String
        Dim random As New Random()
        Dim ch As Char
        Dim i As Integer
        For i = 0 To size - 1
            ch = Convert.ToChar(Convert.ToInt32((25 * random.NextDouble() + 65)))
            builder.Append(ch)
        Next
        licenseKey = builder.ToString()
        Dim sql As String = String.Format("SELECT count(*) FROM DeviceLicense WHERE LicenseKey = '{0}'", licenseKey)
        If (Database.System.GetIntegerValue(sql) > 0) Then
            licenseKey = RandomString(16)
        End If

        Return licenseKey
    End Function

    Private Function RandomNumber() As Integer
        Dim random As New Random()
        Dim otp = random.Next(100000, 999999)
        Return otp
    End Function

    Protected Sub btnEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmail.Click
        'generate key starts
        hidLastTab.Value = 2
        hdnOrgid.Value = lblHidden.Text
        If hdnOrgid.Value <> "" Then
            For i = 1 To CInt(ddlNumber.SelectedValue)
                Dim license As String = RandomString(16)
                Dim otp As Integer = RandomNumber()
                Dim sql As String = String.Format("INSERT INTO DeviceLicense (OrganizationId,LicenseKey,OTP,CreatedDate) VALUES({0},'{1}',{2},'{3}')", hdnOrgid.Value, license, otp, Date.UtcNow.ToString("yyyy-MM-dd HH:mm:ss"))
                Database.System.Execute(sql)
                LicenseTable(license, otp)
            Next
        Else
            Alert("Invalid attempt")
        End If
        'generate key ends

        hidLastTab.Value = 2
        If (GetAccountStatus() = "Active") Then
            If txtEmail.Text.Trim = "" Then
                Return
            End If
            Try
                Dim ta As New MailAddress(txtEmail.Text)
            Catch ex As Exception
                Alert("Invalid email address.")
                Return
            End Try

            Dim mail As New MailMessage
            mail.From = New MailAddress(ClientSettings.DefaultSender, "Access@CAMACloud")
            mail.To.Add(txtEmail.Text)
            '  mail.Bcc.Add("consultsarath@gmail.com")
            mail.Subject = Now.ToString("yyyyMMdd\HHHmmss") + " : CAMA Cloud License Keys for " + hdnOrgname.Value
            mail.Body = composeEmail()
            mail.IsBodyHtml = True

            AWSMailer.SendMail(mail)
            Alert("An email has been sent to " + txtEmail.Text + " with the selected license keys and their OTPs.")

        Else
            Alert("Inactive client account")
        End If


    End Sub

    Protected Sub btnSaveAccount_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAccount.Click
        hdnOrgname.Value = txtName.Text
        SaveOrganization()

        Alert("Account details saved successfully")
    End Sub

    Sub SaveOrganization()
        'Dim sql As String
        'If hdnOrgid.Value = "" Then
        '    dbstatus = "New"
        '    sql = "insert into Organization(Name,City,County,State,status) values ({1}, {2},{3},{4},{5}); select cast(@@identity as int) As NeewId;"
        '    hdnOrgid.Value = Database.System.GetIntegerValue(sql.SqlFormat(False, hdnOrgid, txtName, txtCity, txtCounty, txtState, dbstatus))
        '    lblHidden.Text = hdnOrgid.Value
        '    Dim k As String
        '    k = ""
        'Else
        '    sql = "update Organization set Name={1},city={2},County={3},State={4} Where Id = {0};SELECT {0} As Id;"
        '    hdnOrgid.Value = Database.System.GetIntegerValue(sql.SqlFormat(False, hdnOrgid, txtName, txtCity, txtCounty, txtState))
        '    lblHidden.Text = hdnOrgid.Value
        'End If
        _clearAccountFiedls()
    End Sub

    Function GetAccountStatus() As String
        hdnOrgid.Value = lblHidden.Text
        dbstatus = Database.System.GetStringValue("select status from Organization where id='" + hdnOrgid.Value + "'")
        If dbstatus = "" Then
            dbstatus = "0"
        End If
        Return dbstatus
    End Function

    Protected Sub btnServerConfig_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnServerConfig.Click
        hidLastTab.Value = 0
        Select Case GetAccountStatus()
            Case "New"
                If (ConfigServer()) Then
                    System.Threading.Thread.Sleep(2000)
                    Alert("Server configuration completed successfully")
                    Database.System.Execute("update Organization set CCHost='" + DrpHost.SelectedItem.Text + "', DBName='" + txtDbName.Text + "',DBHost='" + drpServer.SelectedItem.Text + "' ,DBUser='" + txtUserName.Text + "' ,DBPassword='" + txtPwd.Text + "',status='Active' where id='" + hdnOrgid.Value + "'")
                Else
                    Alert("Error occurred in server configuration")
                End If
            Case "Active"
                Database.System.Execute("update Organization set CCHost='" + DrpHost.SelectedItem.Text + "' ,DBHost='" + drpServer.SelectedItem.Text + "' ,DBUser='" + txtUserName.Text + "' ,DBPassword='" + txtPwd.Text + "' where id='" + hdnOrgid.Value + "'")
                Alert("Changes made successfully")
            Case "0"
                Alert("Please create account before configuring server")
        End Select

    End Sub

    Protected Sub txtName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        'If hdnOrgid.Value = "" Then
        '    GenerateDbName(txtName.Text)
        'End If
        Dim nameCount As Integer

        nameCount = Database.System.GetIntegerValue("SELECT count(*) FROM Organization where Name=" + txtName.Text.ToSqlValue())
        If (nameCount > 0) Then
            lblNameNotValid.Visible = True
            txtName.Text = ""
        Else
            GenerateDbName(txtName.Text)
            lblNameNotValid.Visible = False
        End If
    End Sub

    Sub GenerateDbName(ByVal Name As String)
        Dim s As String() = Name.Split(New Char() {" "c})
        If (s.Length > 1) Then
            txtDbName.Text = s(0)
        Else
            txtDbName.Text = Name
        End If
    End Sub

    Private Function composeEmail() As String
        Dim mailContent As XElement = <html>
                                          <body>
                                              <div style="font-family:Arial;width:600px;">
                                                  <h1 style="border-bottom:1px solid #075297;color:#075297;">CAMA Cloud - Access License</h1>
                                                  <h2></h2>
                                                  <p style="font-size:10pt;">Greetings from CAMA Cloud,</p>
                                                  <p style="font-size:10pt;">The following license keys can be used to access CAMA Cloud applications for <b>Organization</b>.</p>
                                                  <table style="font-size:10pt;font-family:Arial;">
                                                      <thead>
                                                          <tr>
                                                              <td style='font-weight:bold;width:200px;'>License Key</td>
                                                              <td style='font-weight:bold;width:80px;'>OTP</td>
                                                          </tr>
                                                      </thead>
                                                      <tbody>

                                                      </tbody>
                                                  </table>
                                                  <p style="font-size:10pt;">The OTP or One-Time-Password can be used only for authenticating a new device/browser. This implies that each license/OTP pair is valid to be used on one target device only.</p>
                                                  <p style="font-size:10pt;">Sincererly, <br/>CAMA Cloud</p>
                                                  <hr/>
                                                  <p style="font-size:8pt;margin-top:0px;">
                                                      <a href="http://www.camacloud.com/">CAMA Cloud<sup style="font-size:6pt;">SM</sup></a> powered by <a href="http://www.datacloudsolutions.net/">Data Cloud Solutions, LLC.</a>
                                                  </p>
                                              </div>
                                          </body>
                                      </html>

        Dim dataFormat As XElement = <tr>
                                         <td></td>
                                         <td></td>
                                     </tr>

        mailContent...<body>...<div>...<p>(1)...<b>.Value = hdnOrgname.Value
        mailContent...<body>...<div>...<h2>.Value = hdnOrgname.Value
        Dim dtLicense As DataTable = ViewState("dtLicense")
        For Each dr In dtLicense.Rows
            Dim dataLine As XElement = XElement.Parse(dataFormat.ToString)
            dataLine...<td>(0).Value = dr("LicenseKey")
            dataLine...<td>(1).Value = dr("OTP")
            mailContent...<body>...<div>...<table>...<tbody>(0).Add(dataLine)
        Next
        Return mailContent.ToString
    End Function

    Protected Sub btnRoles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRoles.Click
        hidLastTab.Value = 1
        If hdnOrgid.Value <> "" Then
            AssignRolesFromList(hdnOrgid.Value)
            Alert("Roles updated successfully ")
        Else
            Alert("Create account before assigning roles")
        End If


    End Sub

    Private Sub _clearAccountFiedls()
        txtName.Text = ""
        txtCounty.Text = ""
        txtState.Text = ""
        txtCity.Text = ""
    End Sub
End Class
