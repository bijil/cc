﻿Imports System.Web
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Timers
Imports System.IO
Public Class EditClient
    Inherits System.Web.UI.Page
    Private mdtmTarget As Date
    ReadOnly Property OrganizationId As Integer
        Get
            If hdnOrganizationId.Value = "" Then
                If Request("ouid") Is Nothing Then
                    Response.Redirect("~/clientaccounts/list.aspx")
                Else

                    Dim org As DataRow = Database.System.GetTopRow("SELECT ID,Name FROM Organization WHERE UID = {0}".SqlFormatString(Request("ouid")))
                    hdnOrganizationId.Value = org.GetString("Id")
                    hdnOrganizationName.Value = org.GetString("Name")
                End If
                If hdnOrganizationId.Value.ToString = "-1" Then
                    Response.Redirect("~/clientaccounts/list.aspx")
                    Response.End()
                End If
            End If
            Return hdnOrganizationId.Value
        End Get
    End Property


    ReadOnly Property UserName As String
        Get
            Dim UserNm As String = Membership.GetUser().ToString()
            Return UserNm
        End Get
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            LoadOrganization(OrganizationId)
            'gvBind(OrganizationId)
            LoadRolesList(OrganizationId)
            'LoadScreenName()
            LoadIndexRebuild()
            txtCounty.Visible = False
            txtCity.Visible = False
            txtState.Visible = False
            txtName.Visible = False
            txtMAHostKey.Visible = False
            ddlApplicnGroup.Visible = False
            ddlHostServer.Visible = False
            ddlDBName.Visible = False
            ddlDemoHostServr.Visible = False
            ddlStage.Visible = False
            ddlDemoDBname.Visible = False
            ddlVendorID.Visible = False
            ddlSupportVendorID.Visible = False
            ddlCAMASystem.Visible = False
            txtNotes.Enabled = False
            ProductionEnvYes.Visible = False
            ProductionEnvNo.Visible = False
            If lnkEdit.Text = "Edit" Then
                hfEdit.Value = 0
            ElseIf lnkEdit.Text = "Update" Then
                hfEdit.Value = 1
            End If

            If lnkEditLoc.Text = "Edit" Then
                hfEditLoc.Value = 0
            ElseIf lnkEditLoc.Text = "Update" Then
                hfEditLoc.Value = 1
            End If

            If lnkEditServer.Text = "Edit" Then
                hfEditServer.Value = 0
            ElseIf lnkEditServer.Text = "Update" Then
                hfEditServer.Value = 1
            End If

            If lnkEditVendor.Text = "Edit" Then
                hfEditVendor.Value = 0
            ElseIf lnkEditVendor.Text = "Update" Then
                hfEditVendor.Value = 1
            End If

            'Checking the Logged user and change the permissions
            If Session("loggedUserType") Is Nothing Then
                Alert("Session Experied, Please LogIn again")
                Response.Redirect("~/auth/Default.aspx")
            Else
                Dim loggedUserType As String = Session("loggedUserType").ToString()

                If loggedUserType = "support" Then
                    lnkEditServer.Visible = False
                    trHideLocation.Style("Display") = "None"
                    trHideSchedule.Style("display") = "None"
                End If
            End If
        End If
    End Sub
    Sub LoadOrganization(ByVal id As Integer)
        Dim Apphostid As Integer, strDBHost As String, vendorId As Integer, strUsehttps As String, HostGrp As String, supportVendorId As String
        Dim o As DataRow = Database.System.GetTopRow("select case when ( IsProduction = 'true' Or IsProduction = 1 ) then 1 else 0 end as ProEnv,* FROM Organization WHERE Id = " & id)
        Dim dt As New DataTable
        Dim DemoDBHostId As String = ""
        lblClient.Text = o.GetString("Name")
        lblCID.Text = BaseCharMap(id)
        lblOrganiznId.Text = o.GetString("Id")
        lblCounty.Text = o.GetString("County")
        txtCounty.Text = o.GetString("County")
        lblName.Text = o.GetString("Name")
        txtName.Text = o.GetString("Name")
        lblState.Text = o.GetString("State")
        txtState.Text = o.GetString("State")
        lblCity.Text = o.GetString("City")
        txtCity.Text = o.GetString("City")
        txtNotes.Text = o.GetString("Notes")

        Dim stageId As Integer = o.GetInteger("Stage")
        Dim stageDescription As String = Database.System.GetStringValue("SELECT StageDescription FROM OrganizationStage WHERE Id = " & stageId)
        lblStage.Text = stageDescription

        'Dim sd As DataRow = Database.System.GetTopRow("select * from OrganizationStage WHERE Id = " & o.GetString("Stage"))
        'lblStage.Text = sd.GetString("StageDescription")

        'Select Case o.GetString("Stage")
        '    Case "1"
        '        lblStage.Text = "Sales"
        '    Case "2"
        '        lblStage.Text = "Implementations"
        '    Case "3"
        '        lblStage.Text = "Integrations"
        '    Case "4"
        '        lblStage.Text = "Production"
        '    Case "5"
        '        lblStage.Text = "Sandboxes"
        '    Case "6"
        '        lblStage.Text = "Internal Sandboxes"
        '    Case "7"
        '        lblStage.Text = "L3 Support"
        '    Case Else
        '        lblStage.Text = "N/A"
        'End Select

        If o.GetString("ProEnv") = "1" Then
        	lblProEnv.Text = "Yes"
        	ProductionEnvYes.Checked = True
        Else
        	lblProEnv.Text = "No"
        	ProductionEnvNo.Checked = True
        End If
        If Not o.GetString("DBName") = "" Then
            lblDBName.Text = o.GetString("DBName")
        Else
            lblDBName.Text = "N/A"
        End If
        lblCreatedDate.Text = Format(Convert.ToDateTime(o.GetDate("CreatedDate")), "MM/dd/yyyy")
        If Not o.GetString("DemoDBHost") = "" Then
            lblDemoHostServr.Text = o.GetString("DemoDBHost")
        Else
            lblDemoHostServr.Text = "N/A"
            DemoDBHostId = ""
        End If
        If Not o.GetString("DemoDBName") = "" Then
            lblDemoDBname.Text = o.GetString("DemoDBName")
        Else
            lblDemoDBname.Text = "N/A"
        End If
        vendorId = o.GetString("VendorId")
        supportVendorId = o.GetString("SupportVendorId")
        Dim vendor As DataRow = Database.System.GetTopRow("SELECT * FROM vendor WHERE Id = " & vendorId)
        lblVendorID.Text = vendor.GetString("Name")
        If supportVendorId = "" Then
            lblSupportVendor.Text = "None"
        Else
            lblSupportVendor.Text = Database.System.GetStringValue("SELECT Name FROM vendor WHERE Id = " & supportVendorId)
        End If

        If Not o.GetString("CAMASystem") = "" Then
            lblCAMASystem.Text = Database.System.GetStringValue("SELECT Name FROM CAMASystem WHERE Id= " & o.GetString("CAMASystem"))
        Else
            lblCAMASystem.Text = "N/A"
        End If

        lblMAHostKey.Text = o.GetString("MAHostkey")
        txtMAHostKey.Text = o.GetString("MAHostkey")
        Apphostid = o.GetString("ApplicationHostId")
        Dim AppHost As DataRow = Database.System.GetTopRow("SELECT * FROM ApplicationHost WHERE Id = " & Apphostid)
        lblApplicnGroup.Text = AppHost.GetString("Name")
        If AppHost.GetString("UseHTTPS") = "True" Then
            strUsehttps = "https"
        Else
            strUsehttps = "http"
        End If
        HostGrp = AppHost.GetString("HostGroup")
        strDBHost = o.GetString("DBHost")
        If lblDBName.Text = "" Or lblDBName.Text = "N/A" Then
            lblDatabaseSize.Text = "N/A"
        Else
            dt = Database.System.GetDataTable("EXEC sp_DatabaseSize {0},{1}".SqlFormatString(lblDBName.Text, strDBHost))
            If dt.Rows.Count > 0 AndAlso dt.Columns.Count > 0 Then
                lblDatabaseSize.Text = dt.Rows(0)(0).ToString()
            Else
                lblDatabaseSize.Text = "N/A"
            End If
        End If

        Dim DBHost As DataRow = Database.System.GetTopRow("SELECT * FROM [DatabaseServer] where Host = '" & strDBHost & "'")
        lblHostServer.Text = DBHost.GetString("Name")
        lblMA.Text = strUsehttps + "://" + o.GetString("MAHostKey") + ".camacloud.com" '--Reomoved Host group from MA url as per the request from Joseph--
        lblAPI.Text = strUsehttps + "://" + "api" + ".camacloud.com"
        lblCC.Text = strUsehttps + "://" + "console" + ".camacloud.com" '--Reomoved Host group from MA url as per the request from Joseph--
        If lblDemoHostServr.Text <> "N/A" Then
            Dim DemoDBHost As DataRow = Database.System.GetTopRow("SELECT * FROM [DatabaseServer] where Host = '" & lblDemoHostServr.Text & "'")
            DemoDBHostId = DemoDBHost.GetString("Id")
        End If
        If IsPostBack Then
            If ddlDemoHostServr.SelectedValue = "" Then
                lblDemoHostServr.Text = "N/A"
            Else
                lblDemoHostServr.Text = ddlDemoHostServr.SelectedItem.Text
            End If
        End If
        bindDropdownList(DBHost.GetString("Id"), Apphostid, DemoDBHostId, vendorId, supportVendorId)
        ddlStage.SelectedValue = o.GetString("Stage")
        GetCloudDataStatistics(id)
        LastBackUp(id)
        gvBind(id)
    End Sub
    Private Sub bindDropdownList(Optional ByVal HostId As String = "", Optional ByVal AppId As String = "", Optional ByVal DemoHostId As String = "", Optional ByVal VendorId As String = "", Optional ByVal supportVendorId As String = "")
        ddlApplicnGroup.FillFromSqlWithDatabase(Database.System, "SELECT Id,Name FROM ApplicationHost ORDER BY Name", False, , AppId)
        ddlHostServer.FillFromSqlWithDatabase(Database.System, "SELECT Id,Name FROM [dbo].[DatabaseServer]", False, , HostId)
        fillddlDBname(ddlHostServer.SelectedValue, ddlDBName, lblDBName.Text)
        ddlDemoHostServr.FillFromSqlWithDatabase(Database.System, "SELECT Id,Name FROM [dbo].[DatabaseServer]", True, , DemoHostId)
        If ddlDemoHostServr.SelectedValue <> "" Then
            fillddlDBname(ddlDemoHostServr.SelectedValue, ddlDemoDBname, lblDemoDBname.Text)
        End If
        ddlVendorID.FillFromSqlWithDatabase(Database.System, "SELECT Id,CommonName FROM vendor ORDER BY CommonName", False, , VendorId)
        ddlSupportVendorID.FillFromSqlWithDatabase(Database.System, "SELECT Id,CommonName FROM vendor ORDER BY CommonName", True, "-- None --", supportVendorId)
        ddlCAMASystem.FillFromSqlWithDatabase(Database.System, "SELECT Id,Name FROM CAMASystem WHERE vendorId=" + VendorId + " ORDER BY Name", False, , ID)
        fillStage()
    End Sub
    Sub fillStage()
        'ddlStage.InsertItem("Sales", "1", 0)
        'ddlStage.InsertItem("Implementations", 2, 1)
        'ddlStage.InsertItem("Integrations", 3, 2)
        'ddlStage.InsertItem("Production", 4, 3)
        'ddlStage.InsertItem("Sandboxes", 5, 4)
        'ddlStage.InsertItem("Internal Sandboxes", 6, 5)
        'ddlStage.InsertItem("L3 Support", 7, 6)
        Dim stages As DataTable = Database.System.GetDataTable("SELECT Id, StageDescription FROM OrganizationStage")
        For Each stageRow As DataRow In stages.Rows
            ddlStage.InsertItem(stageRow("StageDescription").ToString(), stageRow("Id").ToString(), ddlStage.Items.Count)
        Next

    End Sub

    Protected Sub ddlHostServer_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlHostServer.SelectedIndexChanged
        fillddlDBname(ddlHostServer.SelectedValue, ddlDBName)
    End Sub
    Sub fillddlDBname(ByVal HostServer As String, ddlName As DropDownList, Optional ByVal DBName As String = "")
        Dim DBHost As String
        If HostServer <> "" Then
            Dim drDBHost As DataRow = Database.System.GetTopRow("SELECT * FROM [dbo].[DatabaseServer] where Id= '" & HostServer & "'")
            DBHost = drDBHost.GetString("Host")

            Dim dtDataBase As New DataTable
            dtDataBase = Database.System.GetDataTable("EXEC [" & DBHost & "].[Master].[dbo].[sp_databases]")
            dtDataBase.Columns.Remove("REMARKS")
            dtDataBase.Columns.Remove("DATABASE_SIZE")
            ddlName.FillFromTable(dtDataBase, True, , DBName)
        Else
            ddlName.Items.Clear()
        End If

    End Sub
    Protected Sub ddlDBName_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDBName.SelectedIndexChanged
        Dim DBHost As String
        Dim drDBHost As DataRow = Database.System.GetTopRow("SELECT * FROM [dbo].[DatabaseServer] where Id= '" & ddlHostServer.SelectedValue & "'")
        DBHost = drDBHost.GetString("Host")

        If ddlDBName.SelectedValue <> "" Then
            Dim dt As New DataTable
            dt = Database.System.GetDataTable("EXEC sp_DatabaseSize {0},{1}".SqlFormatString(ddlDBName.SelectedValue.ToString(), DBHost))
            lblDatabaseSize.Text = dt.Rows(0)(0).ToString
        Else
            lblDatabaseSize.Text = "N/A"
        End If

    End Sub
    Protected Sub ddlDemoHostServr_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDemoHostServr.SelectedIndexChanged
        fillddlDBname(ddlDemoHostServr.SelectedValue, ddlDemoDBname)
    End Sub
    
    Function ValidateOrganization() As Boolean
    	If(txtState.Text.Length > 2) Then
    		Alert("Invalid entry, State field has been allowed only two digits !")
    		Return False
    	End If
    	
    	Return True
    End Function
    Sub UpdateOrganization(ByVal id As Integer)

        Dim UpdateQuery As String = ""
        Dim o As DataRow = Database.System.GetTopRow("SELECT Name,County,State,City FROM Organization WHERE Id = " & id)
        Dim CountyNm As String = o.GetString("Name")
        Dim County As String = o.GetString("County")
        Dim State As String = o.GetString("State")
        Dim City As String = o.GetString("City")

        Dim sqlQuery As String = "UPDATE Organization SET Name= '" & Replace(txtName.Text.ToString, "'", "''") & "' , County= '" & Replace(txtCounty.Text.ToString, "'", "''") & "' , State= '" & Replace(txtState.Text.ToString, "'", "''") & "' , City = '" & Replace(txtCity.Text.ToString, "'", "''") & "' WHERE Id= " & id
        Database.System.Execute(sqlQuery)

        If txtName.Text <> CountyNm Then
            UpdateQuery += "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserName & "','" & CountyNm & "', 'Environment Name is updated from " & CountyNm & " to " & txtName.Text & "' )"
        End If
        If txtCounty.Text <> County Then
            UpdateQuery += "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserName & "','" & CountyNm & "', 'County is updated from " & County & " to " & txtCounty.Text & "' )"
        End If
        If txtState.Text <> State Then
            UpdateQuery += "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserName & "','" & CountyNm & "', 'State is updated from " & State & " to " & txtState.Text & "' )"
        End If
        If txtCity.Text <> City Then
            UpdateQuery += "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserName & "','" & CountyNm & "', 'City is updated from " & City & " to " & txtCity.Text & "' )"
        End If

        Try
            Database.System.Execute(UpdateQuery)
        Catch e As Exception

        End Try

        LoadOrganization(id)

    End Sub

    Sub UpdateServer(ByVal id As Integer)
        Dim ApplicationHostId As Integer
        Dim DBHost As String
        Dim DBName As String
        Dim DBUser As String
        Dim DBPassword As String
        Dim DemoDBHost As String
        Dim DemoDBName As String
        Dim DemoDBUser As String
        Dim DemoDBPassword As String
        Dim isPrd As Boolean
        Dim ProdStatus As String = "NULL"
        Dim PrStatus As String = "NULL"
        Dim stage As String = ddlStage.SelectedValue
        Dim drAppHostId As DataRow = Database.System.GetTopRow("SELECT * FROM ApplicationHost WHERE Id= '" & ddlApplicnGroup.SelectedValue & "'")
        ApplicationHostId = drAppHostId.GetString("Id")
        Dim ApplicationHostName = drAppHostId.GetString("Name")
        If ddlHostServer.SelectedValue <> "" Then
            Dim drDBHost As DataRow = Database.System.GetTopRow("SELECT * FROM [dbo].[DatabaseServer] WHERE Id= '" & ddlHostServer.SelectedValue & "'")
            DBHost = drDBHost.GetString("Host").ToSqlValue()
            DBName = ddlDBName.SelectedValue.ToSqlValue()
            DBUser = drDBHost.GetString("DBUser").ToSqlValue()
            DBPassword = drDBHost.GetString("DBPassword").ToSqlValue()
        Else
            DBHost = "NULL"
            DBName = "NULL"
            DBUser = "NULL"
            DBPassword = "NULL"
        End If

        If ddlDBName.SelectedValue = "" Then
            DBName = "NULL"
        End If

        If ddlDemoHostServr.SelectedValue <> "" Then
            Dim drDemoDBHost As DataRow = Database.System.GetTopRow("SELECT * FROM [dbo].[DatabaseServer] WHERE Id= '" & ddlDemoHostServr.SelectedValue & "'")
            DemoDBHost = drDemoDBHost.GetString("Host").ToSqlValue()
            DemoDBName = ddlDemoDBname.SelectedValue.ToSqlValue()
            DemoDBUser = drDemoDBHost.GetString("DBUser").ToSqlValue()
            DemoDBPassword = drDemoDBHost.GetString("DBPassword").ToSqlValue()
        Else
            DemoDBHost = "NULL"
            DemoDBName = "NULL"
            DemoDBUser = "NULL"
            DemoDBPassword = "NULL"
        End If

        If ddlDemoDBname.SelectedValue = "" Then
            DemoDBName = "NULL"
        End If

        If ProductionEnvYes.Checked Then
            isPrd = True
            ProdStatus = "1"
            PrStatus = "Enabled"
        ElseIf ProductionEnvNo.Checked Then
            isPrd = False
            ProdStatus = "0"
            PrStatus = "disabled"
        End If

        Dim UpdateQuery As String = ""
        Dim o As DataRow = Database.System.GetTopRow("select case when (IsProduction = 'true' Or IsProduction = 1 ) then 1 else 0 end as ProEnv,* FROM Organization WHERE Id = " & id)
        Dim CountyNm As String = o.GetString("Name").ToSqlValue()
        Dim AppHostId As Integer = o.GetInteger("ApplicationHostId")
        Dim OrgAppHost As DataRow = Database.System.GetTopRow("SELECT * FROM ApplicationHost WHERE Id= '" & AppHostId & "'")
        Dim OrgAppHostName As String = OrgAppHost.GetString("Name")
        Dim OrgDBHost As String = o.GetString("DBHost")
        Dim OrgDBName As String = o.GetString("DBName")
        Dim OrgDemoDBHost As String = o.GetString("DemoDBHost")
        Dim OrgDemoDBName As String = o.GetString("DemoDBName")
        Dim MaHostKey = txtMAHostKey.Text
        Dim OrgMaHostKey As String = o.GetString("MAHostKey")
        Dim OrgProdStatus As String = o.GetString("ProEnv")

        Dim query As String
        query = "UPDATE Organization SET ApplicationHostId = '" & ApplicationHostId & "', DBHost=" & DBHost & ", DBName= " & DBName & ", DBUser = " & DBUser & ", DBPassword = " & DBPassword & ","
        query += "DemoDBHost=" & DemoDBHost & ", DemoDBName=" & DemoDBName & ", DemoDBUser = " & DemoDBUser & ", DemoDBPassword = " & DemoDBPassword & ", MAHostKey = '" & txtMAHostKey.Text & "' , Stage = '" & stage & "', IsProduction = '" & isPrd & "' WHERE Id= " & id.ToString
        Dim sqlQuery As String = String.Format(query)
        Database.System.Execute(sqlQuery)

        Dim DBHostUp As String = DBHost.Replace("'", "").Trim()
        Dim DBNameUp As String = DBName.Replace("'", "").Trim()
        Dim DemoDBHostUp As String = DemoDBHost.Replace("'", "").Trim()
        Dim DemoDBNameUp As String = DemoDBName.Replace("'", "").Trim()
        Dim MaHostKeyUp As String = MaHostKey.Replace("'", "").Trim()

        If ApplicationHostId <> AppHostId Then
            UpdateQuery += "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserName & "'," & CountyNm & ", 'Application Group is updated from " & OrgAppHostName & " to " & ApplicationHostName & "' )"
        End If
        If DBHostUp <> OrgDBHost Then
            UpdateQuery += "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserName & "'," & CountyNm & ", 'Host Server is updated from " & OrgDBHost & " to " & DBHostUp & "')"
        End If
        If DBNameUp <> OrgDBName Then
            UpdateQuery += "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserName & "'," & CountyNm & ", 'Database is updated from " & OrgDBName & " to " & DBNameUp & "')"
        End If
        If DemoDBHostUp <> OrgDemoDBHost Then
            UpdateQuery += "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserName & "'," & CountyNm & ", 'Demo Host Server is updated from " & OrgDemoDBHost & " to " & DemoDBHostUp & "')"
        End If
        If DemoDBNameUp <> OrgDemoDBName Then
            UpdateQuery += "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserName & "'," & CountyNm & ", 'Demo Database is updated from " & OrgDemoDBName & " to " & DemoDBNameUp & "')"
        End If
        If MaHostKeyUp <> OrgMaHostKey Then
            UpdateQuery += "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserName & "'," & CountyNm & ", 'MA Hostkey is updated from " & OrgMaHostKey & " to " & MaHostKeyUp & "')"
        End If
        If ProdStatus <> OrgProdStatus Then
            UpdateQuery += "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserName & "'," & CountyNm & ", 'Production status " & PrStatus & "' )"
        End If

        Try
            Database.System.Execute(UpdateQuery)
        Catch e As Exception

        End Try
        LoadOrganization(id)

    End Sub

    Sub UpdateVendor(ByVal id As Integer)
        'Dim query
        'query = "UPDATE Organization SET VendorId = " & ddlVendorID.SelectedValue & ", SupportVendorId = " & If(ddlSupportVendorID.SelectedValue = "", "NULL", ddlSupportVendorID.SelectedValue) & " , CAMASystem = " & IIf(ddlCAMASystem.SelectedValue = "", "NULL", ddlCAMASystem.SelectedValue) & ", Notes= '" & txtNotes.Text & "' WHERE Id= " & id.ToString       
        'Dim sqlQuery As String = String.Format(query)

        Dim UpdateQuery As String = ""
        Dim o As DataRow = Database.System.GetTopRow("SELECT O.VendorId,SupportVendorId,CAMASystem,Notes,V.Name AS VendorName,C.Name AS CamaName,o.Name FROM Organization o LEFT JOIN Vendor V ON V.ID  = O.VendorId LEFT JOIN  CAMASystem c ON c.ID  = O.CAMASystem WHERE o.Id = " & id)
        Dim CountyNm As String = o.GetString("Name").ToSqlValue()
        Dim OrgNotes As String = o.GetString("Notes")
        Dim OrgVendorName As String = o.GetString("VendorName")
        Dim OrgSupportVendorId As String = o.GetString("SupportVendorId")
        Dim OrgSupportVendorName = Database.System.GetStringValue("SELECT NAME FROM VENDOR WHERE ID = '" & OrgSupportVendorId & "'")
        Dim OrgCamaName As String = o.GetString("CamaName")

        Dim VendorName = Database.System.GetStringValue("SELECT NAME FROM VENDOR WHERE ID = " & ddlVendorID.SelectedValue)
        Dim CamaName As String = Database.System.GetStringValue("SELECT NAME FROM CAMASystem WHERE ID = '" & ddlCAMASystem.SelectedValue & "'")
        Dim SupportVendorName = Database.System.GetStringValue("SELECT NAME FROM VENDOR WHERE ID = '" & ddlSupportVendorID.SelectedValue & "'")

        Database.System.Execute("UPDATE Organization SET VendorId = {0}, SupportVendorId = {1}, CAMASystem = {2}, Notes = {3} WHERE Id = {4}".SqlFormat(True, ddlVendorID.SelectedValue, ddlSupportVendorID.SelectedValue, ddlCAMASystem.SelectedValue, txtNotes.Text, id.ToString))

        If OrgVendorName <> VendorName Then
            UpdateQuery += "INSERT INTO [AdminSettingsAuditTrail](EventTime, LoginId, County, Description) VALUES (GETUTCDATE(),'" & UserName & "'," & CountyNm & ", 'Vendor is updated from " & OrgVendorName & " to " & VendorName & "' )"
        End If
        If OrgSupportVendorName <> SupportVendorName Then
            UpdateQuery += "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserName & "'," & CountyNm & ", 'Support Vendor is updated from   " & If([String].IsNullOrEmpty(OrgSupportVendorName), "NULL", OrgSupportVendorName) & " to " & If([String].IsNullOrEmpty(SupportVendorName), "NULL", SupportVendorName) & "')"
        End If
        If OrgCamaName <> CamaName Then
            UpdateQuery += "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserName & "'," & CountyNm & ", 'CAMASystem is updated from " & OrgCamaName & " to " & CamaName & "' )"
        End If
        If txtNotes.Text <> OrgNotes Then
            UpdateQuery += "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserName & "'," & CountyNm & ", 'Notes field is updated to """ & txtNotes.Text & """' )"
        End If

        Try
            Database.System.Execute(UpdateQuery)
        Catch e As Exception

        End Try
        LoadOrganization(id)
    End Sub
    Public Sub LastBackUp(ByVal Id As Integer)
        Dim HostServer As String
        Dim DBHost As String
        HostServer = lblHostServer.Text
        If HostServer <> "" Then
            Dim drDBHost As DataRow = Database.System.GetTopRow("SELECT * FROM [dbo].[DatabaseServer] where Name= '" & HostServer & "'")
            DBHost = drDBHost.GetString("Host")
            Dim query As String
            query = " select top 1 A.backup_finish_date AS LastBackupTime, CAST(CAST(A.backup_size / 1000000 AS INT) AS VARCHAR(14)) + ' ' + 'MB' AS Size, B.physical_device_name AS Location,"
            query += "CONVERT(varchar(6), DATEDIFF(second, A.backup_start_date, A.backup_finish_date)/3600)+ ':'+ RIGHT('0' + CONVERT(varchar(2), (DATEDIFF(second, A.backup_start_date, A.backup_finish_date) % 3600) / 60), 2)+ ':'+ RIGHT('0' + CONVERT(varchar(2), DATEDIFF(second, A.backup_start_date, A.backup_finish_date) % 60), 2) AS TimeTaken"
            query += " from  [" & DBHost & "].[msdb].[dbo].backupset A left join [" & DBHost & "].[msdb].[dbo].backupmediafamily B on B.media_set_id = A.media_set_id where database_name='" & lblDBName.Text & "' order by A.backup_finish_date desc"
            Dim dtBackUpTime As New DataTable
            dtBackUpTime = Database.System.GetDataTable(query)
            If dtBackUpTime IsNot Nothing AndAlso dtBackUpTime.Rows.Count > 0 Then
                lblLastBackupTime.Text = dtBackUpTime.Rows(0)(0).ToString
                lblSize.Text = dtBackUpTime.Rows(0)(1).ToString
                txtLocation.Text = dtBackUpTime.Rows(0)(2).ToString
                lblTimeTaken.Text = dtBackUpTime.Rows(0)(3).ToString
            Else
                lblLastBackupTime.Text = "N/A"
                lblSize.Text = "N/A"
                txtLocation.Text = "N/A"
                lblTimeTaken.Text = "N/A"
            End If
        End If
    End Sub
    Private Sub LoadRolesList(orgId As Integer)
        Dim dtOrgRoles As DataTable = Database.System.GetDataTable("SELECT ap.Id,ap.Name,os.SeatCount FROM	AppRoles ap LEFT JOIN ( SELECT AppRoleId,SeatCount FROM OrganizationSeats WHERE OrgId = {0})os ON os.AppRoleId = ap.Id UNION SELECT 999 as Id, 'SketchPro' as Name, ISNULL(MIN(SeatCount), NULL) as SeatCount FROM OrganizationSeats WHERE OrgId = {0} and AppRoleId = 999".SqlFormatString(orgId))
        rptRoles.DataSource = dtOrgRoles
        rptRoles.DataBind()
        Dim dtRoles As DataTable
        dtRoles = Database.System.GetDataTable("SELECT * FROM OrganizationRoles WHERE OrganizationId = " + orgId.ToString())
        If Not dtRoles Is Nothing AndAlso dtRoles.Rows.Count > 0 Then
            For Each dr As DataRow In dtRoles.Rows
                For Each item As RepeaterItem In rptRoles.Items
                    Dim chk As WebControl = CType(item.FindControl("chkRole"), WebControl)
                    Dim textbox As WebControl = CType(item.FindControl("txtSeat"), WebControl)
                    Dim hdnField As HiddenField = CType(item.FindControl("hdnRole"), HiddenField)
                    If hdnField.Value = dr("RoleId") Then
                        CType(chk, CheckBox).Checked = True
                    End If
                    chk.Enabled = False
                    textbox.Enabled = False
                Next
            Next
        End If
    End Sub

    Public Sub LoadIndexRebuild()
        Dim CurrentTimeZone As TimeZone
        CurrentTimeZone = TimeZone.CurrentTimeZone
        Dim Index As DataRow = Database.System.GetTopRow("SELECT TOP 1 name AS Stats,STATS_DATE(object_id, stats_id) AS LastStatsUpdate FROM sys.stats WHERE auto_created=0 ORDER BY LastStatsUpdate desc")
        If CurrentTimeZone.StandardName = "India Standard Time" Then
            lblRebuildIndex.Text = Format(Convert.ToDateTime(Index.GetString("LastStatsUpdate")).ToLocalTime(), "dd-MM-yyyy hh:mm tt")
        Else
            lblRebuildIndex.Text = Format(Convert.ToDateTime(Index.GetString("LastStatsUpdate")), "dd-MM-yyyy hh:mm tt")
        End If
    End Sub
    Protected Sub lbLicence_Click(sender As Object, e As EventArgs) Handles lbLicence.Click
        Dim var As String
        Dim org As DataRow = Database.System.GetTopRow("SELECT * FROM Organization WHERE Id=" & Val(lblOrganiznId.Text))
        var = org.GetString("UID")
        Response.Redirect("~/license/?ouid=" + var)
    End Sub
    Protected Sub lbAPIAccess_Click(sender As Object, e As EventArgs) Handles lbAPIAccess.Click
        Dim var As String
        Dim org As DataRow = Database.System.GetTopRow("SELECT * FROM Organization WHERE Id=" & Val(lblOrganiznId.Text))
        var = org.GetString("UID")
        Response.Redirect("~/license/applicationaccess.aspx?ouid=" + var)
    End Sub

    Protected Sub btnGo_Click(sender As Object, e As EventArgs) Handles btnGo.Click
        Dim org As DataRow
        If Not txtCountyName.Text = "" Then
            org = Database.System.GetTopRow("SELECT * FROM Organization WHERE Name like '%" & txtCountyName.Text & "%'")
            If Not org Is Nothing Then
                Response.Redirect("~/clientaccounts/ManageClient.aspx?ouid=" + org.GetString("UID"))
                'LoadOrganization(org.GetString("Id"))
                'GetCloudDataStatistics(org.GetString("Id"))
            End If
        End If
    End Sub

    Sub AssignRolesFromList(ByVal id As Integer)
        For Each item As RepeaterItem In rptRoles.Items
            Dim chkbx As CheckBox = CType(item.FindControl("chkRole"), CheckBox)
            If chkbx.Text = "SketchPro" Then
                Dim seat As String = CType(item.FindControl("txtSeat"), TextBox).Text
                Dim currentSeat As DataRow = Database.System.GetTopRow("SELECT * FROM OrganizationSeats WHERE AppRoleId = 999 AND OrgId = " + OrganizationId.ToString())
                Dim oldSeat As String = ""
                If Not IsNothing(currentSeat) Then
                    oldSeat = currentSeat.GetString("SeatCount")
                End If
                Dim countyName As String = Database.System.GetStringValue("SELECT Name FROM Organization WHERE Id = " & OrganizationId.ToString())
                If oldSeat <> seat Then
                    If seat = "" Or seat = "0" Then
                        Dim qry As String = "UPDATE DeviceLicense SET EnabledSketchPro = 0 WHERE EnabledSketchPro = 1 AND OrganizationId = " + OrganizationId.ToString()
                        qry += "; DELETE FROM DeviceLicense WHERE DeviceFilter = 'SketchPro' AND OrganizationId = " + OrganizationId.ToString()
                        qry += "; INSERT INTO LicenseAudittrail SELECT OrganizationId, '" + UserName + "' AS LoginId, GETUTCDATE() AS EventTime, DeviceFilter, LicenseKey, '" + HttpContext.Current.Request.ClientIPAddress + "' AS IPAddress, 'SketchPro access removed By SketchPro seat removal' AS Comment FROM DeviceLicense WHERE Id IN (SELECT Id FROM DeviceLicense WHERE EnabledSketchPro = 1 AND OrganizationId = " + OrganizationId.ToString() + ")"
                        qry += "; INSERT INTO LicenseAudittrail SELECT OrganizationId, '" + UserName + "' AS LoginId, GETUTCDATE() AS EventTime, DeviceFilter, LicenseKey, '" + HttpContext.Current.Request.ClientIPAddress + "' AS IPAddress, 'License Key Removed By SketchPro seat removal' AS Comment FROM DeviceLicense WHERE Id IN (SELECT Id FROM DeviceLicense WHERE DeviceFilter = 'SketchPro' AND OrganizationId = " + OrganizationId.ToString() + ")"
                        qry += "; DELETE FROM OrganizationSeats WHERE AppRoleId = 999 AND OrgId = " + OrganizationId.ToString()
                        qry += "; INSERT INTO [AdminSettingsAuditTrail](EventTime, LoginId, County, Description ) VALUES (GETUTCDATE(), '" + UserName + "', '" + countyName + "', 'User seat of SketchPro is removed.')"
                        Database.System.Execute(qry)
                    Else
                        Dim lcnt As Integer = Database.System.GetIntegerValue("SELECT COUNT(*) FROM DeviceLicense WHERE (DeviceFilter = 'SketchPro' OR EnabledSketchPro = 1) AND OrganizationId = " + OrganizationId.ToString())
                        If lcnt > CInt(seat) Then
                            Throw New Exception("The new seat count is less than the currently using SketchPro licenses. License Count: " + lcnt.ToString())
                        Else
                            If (currentSeat Is Nothing) Then
                                Dim qry As String = "INSERT INTO OrganizationSeats(OrgId, AppRoleId, SeatCount, ConsumedSeat) VALUES (" + OrganizationId.ToString() + ", 999, " + seat + ", 0)"
                                qry += "; INSERT INTO [AdminSettingsAuditTrail](EventTime, LoginId, County, Description ) VALUES (GETUTCDATE(), '" + UserName + "', '" + countyName + "', 'User seat of SketchPro is added as " + seat + "')"
                                Database.System.Execute(qry)
                            Else
                                Dim qry As String = "UPDATE	OrganizationSeats SET SeatCount = " + seat + " WHERE AppRoleId = 999 AND OrgId = " + OrganizationId.ToString()
                                qry += "; INSERT INTO [AdminSettingsAuditTrail](EventTime, LoginId, County, Description ) VALUES (GETUTCDATE(), '" + UserName + "', '" + countyName + "', 'User seat of SketchPro is updated as " + seat + "')"
                                Database.System.Execute(qry)
                            End If
                        End If
                    End If
                End If
            End If
        Next
        For Each item As RepeaterItem In rptRoles.Items
            Dim chkbx As CheckBox = CType(item.FindControl("chkRole"), CheckBox)
            If chkbx.Text <> "SketchPro" Then
                Dim textbox As WebControl = CType(item.FindControl("txtSeat"), WebControl)
                Dim hdnField As HiddenField = CType(item.FindControl("hdnRole"), HiddenField)
                Dim seat As String = CType(textbox, TextBox).Text
                If chkbx.Checked Then
                    If Not IsOrganizationInRole(OrganizationId, hdnField.Value) Then
                        AddOrgToRole(OrganizationId, hdnField.Value, CType(textbox, TextBox).Text)
                    End If
                    UpdateSeat(seat, OrganizationId, hdnField.Value)
                Else
                    If IsOrganizationInRole(OrganizationId, hdnField.Value) Then
                        RemoveOrganizationFromRole(OrganizationId, hdnField.Value)
                    End If
                    UpdateSeat(seat, OrganizationId, hdnField.Value, 1)
                End If
            End If
        Next
    End Sub


    Function IsOrganizationInRole(ByVal orgid As String, ByVal rolid As String) As Boolean
        Dim dt As DataTable = Database.System.GetDataTable("SELECT * FROM OrganizationRoles WHERE OrganizationId=" + orgid + " AND Roleid=" + rolid + "")
        If (dt.Rows.Count > 0) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub AddOrgToRole(ByVal orgid As String, ByVal roleid As String, Optional ByVal seat As String = "")
        Dim sql As String = String.Format("INSERT INTO OrganizationRoles (OrganizationId,RoleId) VALUES({0},{1})", orgid, roleid)
        Database.System.Execute(sql)

        Dim UpdateQuery As String = ""
        Dim o As DataRow = Database.System.GetTopRow("SELECT Name,County,State,City FROM Organization WHERE Id = " & orgid)
        Dim CountyNm As String = o.GetString("Name").ToSqlValue()
        Dim roleNm As String = Database.System.GetStringValue("SELECT  ap.Name FROM AppRoles ap LEFT JOIN (Select AppRoleId,SeatCount FROM OrganizationSeats where orgid  = " & orgid & ") os On os.AppRoleId = ap.Id where ap.id  = " & roleid)

        UpdateQuery = "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserName & "'," & CountyNm & ", '" & roleNm & " of Application Roles is enabled' )"
        Try
            Database.System.Execute(UpdateQuery)
        Catch e As Exception

        End Try

        If seat <> "" Then
            Dim Seatsql As String = String.Format("INSERT INTO OrganizationSeats (OrgId,AppRoleId,SeatCount) VALUES({0},{1},{2})", orgid, roleid, seat)
            Seatsql += "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserName & "'," & CountyNm & ",'User seat of " & roleNm & " is added as " & seat & " ')"
            Database.System.Execute(Seatsql)
        End If
    End Sub

    Private Sub RemoveOrganizationFromRole(ByVal orgid As String, ByVal roleid As String)
        Dim sql As String = String.Format("DELETE FROM OrganizationRoles WHERE OrganizationId={0} AND RoleId={1}", orgid, roleid)
        Database.System.Execute(sql)

        Dim UpdateQuery As String = ""
        Dim o As DataRow = Database.System.GetTopRow("SELECT Name,County,State,City FROM Organization WHERE Id = " & orgid)
        Dim CountyNm As String = o.GetString("Name").ToSqlValue()
        Dim roleNm As String = Database.System.GetStringValue("SELECT ap.Name FROM AppRoles ap LEFT JOIN (Select AppRoleId,SeatCount FROM OrganizationSeats where orgid  = " & orgid & ") os On os.AppRoleId = ap.Id where ap.id  = " & roleid)

        UpdateQuery = "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserName & "'," & CountyNm & ", '" & roleNm & " of Application Roles is disabled' )"
        Try
            Database.System.Execute(UpdateQuery)
        Catch e As Exception

        End Try

    End Sub

    Sub GetCloudDataStatistics(ByVal orgId As Integer)
        Dim connStr As String = Database.GetConnectionStringFromOrganization(orgId)
        Dim conn As New SqlClient.SqlConnection(connStr)
        conn.Open()
        ''Total Parcels
        Dim cmd As New SqlClient.SqlCommand("SELECT COUNT(*) FROM Parcel WHERE Id > -1", conn)
        Dim Totparcel As Object = cmd.ExecuteScalar()
        ''Total Assignment Groups
        Dim cmd1 As New SqlClient.SqlCommand("SELECT COUNT(*) FROM Neighborhood", conn)
        Dim Tot As Object = cmd1.ExecuteScalar()
        lblTotParcels.Text = Convert.ToInt32(Totparcel).ToString("N0")
        lblTotal.Text = Convert.ToInt32(Tot).ToString("N0")
        Dim TotUsers As Integer
        TotUsers = Database.System.GetIntegerValue("SELECT COUNT(*) FROM aspnet_Users WHERE ApplicationId in (SELECT ApplicationId FROM aspnet_Applications WHERE ApplicationName = 'org" + OrganizationId.ToString("D3") + "')")
        lblUsers.Text = TotUsers

        ''Total OnlineUsers 
        Dim TotOnlineUsers As Integer
        TotOnlineUsers = Database.System.GetIntegerValue("SELECT COUNT(*) FROM aspnet_Users WHERE ApplicationId in (SELECT ApplicationId FROM aspnet_Applications WHERE ApplicationName = 'org" + OrganizationId.ToString("D3") + "' and (LastActivityDate between DATEADD(HOUR, -8,  GETUTCDATE()) and GETUTCDATE()))")
        lblOnlineUsers.Text = TotOnlineUsers
        conn.Close()

    End Sub
    Protected Sub btnRebuild_Click(sender As Object, e As EventArgs) Handles btnRebuild.Click
        Dim connStr As String = Database.GetConnectionStringFromOrganization(hdnOrganizationId.Value)
        Dim sql As String = "RebuildIndex"
        Try
            Dim conn As New SqlClient.SqlConnection(connStr)
            conn.Open()
            Dim cmd As New SqlClient.SqlCommand(sql, conn)
            cmd.ExecuteReader()
            Alert("Rebuild Index Completed.")
            conn.Close()
        Catch sqlex As SqlClient.SqlException
        Catch ex As Exception
        End Try
        LoadIndexRebuild()
    End Sub

    Protected Sub btnCreate_Click(sender As Object, e As EventArgs) Handles btnCreate.Click
        Dim strsql As String
        strsql =
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_Parcel_Keyvalue1' AND object_id = OBJECT_ID('Parcel'))" &
        " CREATE INDEX IX_Parcel_Keyvalue1 ON Parcel (keyvalue1) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_Parcel_ReviewDate' AND object_id = OBJECT_ID('Parcel'))" &
        " CREATE INDEX IX_Parcel_ReviewDate ON Parcel (ReviewDate) ; " &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_Parcel_FieldAlert' AND object_id = OBJECT_ID('Parcel'))" &
        " CREATE INDEX IX_Parcel_FieldAlert ON Parcel (FieldAlert) ; " &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_Parcel_NeighborhoodId' AND object_id = OBJECT_ID('Parcel'))" &
        " CREATE INDEX IX_Parcel_NeighborhoodId ON Parcel (NeighborhoodId) ; " &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_ParcelChanges_ParcelId' AND object_id = OBJECT_ID('ParcelChanges'))" &
        " CREATE INDEX IX_ParcelChanges_ParcelId ON ParcelChanges (ParcelId) ; " &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_ParcelChanges_FieldId' AND object_id = OBJECT_ID('ParcelChanges'))" &
        " CREATE INDEX IX_ParcelChanges_FieldId ON ParcelChanges (fieldid) ; " &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_ParcelChanges_ReviewedBy' AND object_id = OBJECT_ID('ParcelChanges'))" &
        " CREATE INDEX IX_ParcelChanges_ReviewedBy ON ParcelChanges (ReviewedBy) ; " &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_ParcelAuditTrail_LoginID' AND object_id = OBJECT_ID('ParcelAuditTrail'))" &
        " CREATE INDEX IX_ParcelAuditTrail_LoginID ON ParcelAuditTrail (LoginID) ; " &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_ParcelAuditTrail_EventTime' AND object_id = OBJECT_ID('ParcelAuditTrail'))" &
        " CREATE INDEX IX_ParcelAuditTrail_EventTime ON ParcelAuditTrail (EventTime) ; " &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_ParcelAuditTrail_EventType_LoginID' AND object_id = OBJECT_ID('ParcelAuditTrail'))" &
        " CREATE INDEX IX_ParcelAuditTrail_EventType_LoginID ON ParcelAuditTrail (EventType,LoginID) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_SystemAuditTrail_LoginId' AND object_id = OBJECT_ID('SystemAuditTrail'))" &
        " CREATE INDEX IX_SystemAuditTrail_LoginId ON SystemAuditTrail (LoginId) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_SystemAuditTrail_EventTime' AND object_id = OBJECT_ID('SystemAuditTrail'))" &
        " CREATE INDEX IX_SystemAuditTrail_EventTime ON SystemAuditTrail (EventTime) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_SystemAuditTrail_EventDate' AND object_id = OBJECT_ID('SystemAuditTrail'))" &
        " CREATE INDEX IX_SystemAuditTrail_EventDate ON SystemAuditTrail (EventDate) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_SystemAuditTrail_EventCode' AND object_id = OBJECT_ID('SystemAuditTrail'))" &
        " CREATE INDEX IX_SystemAuditTrail_EventCode ON SystemAuditTrail (EventCode) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_SystemAuditTrail_LoginId_EventTime' AND object_id = OBJECT_ID('SystemAuditTrail'))" &
        " CREATE INDEX IX_SystemAuditTrail_LoginId_EventTime ON SystemAuditTrail(LoginId,EventTime) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_ParcelImages_ParcelId' AND object_id = OBJECT_ID('ParcelImages'))" &
        " CREATE INDEX IX_ParcelImages_ParcelId ON ParcelImages(ParcelId) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_ParcelImages_Path' AND object_id = OBJECT_ID('ParcelImages'))" &
        " CREATE INDEX IX_ParcelImages_Path ON ParcelImages (Path) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_ParcelReviewLog_ParcelId' AND object_id = OBJECT_ID('ParcelReviewLog'))" &
        " CREATE INDEX IX_ParcelReviewLog_ParcelId ON ParcelReviewLog (Parcelid) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_ParcelReviewLog_ReviewedBy' AND object_id = OBJECT_ID('ParcelReviewLog'))" &
        " CREATE INDEX IX_ParcelReviewLog_ReviewedBy ON ParcelReviewLog (ReviewedBy) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_ParcelDataLookup_LookupName' AND object_id = OBJECT_ID('ParcelDataLookup'))" &
        " CREATE INDEX IX_ParcelDataLookup_LookupName ON ParcelDataLookup(LookupName) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_ParcelDataLookup_IdValue' AND object_id = OBJECT_ID('ParcelDataLookup'))" &
        " CREATE INDEX IX_ParcelDataLookup_IdValue ON ParcelDataLookup(IdValue) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_ParcelData_CC_LastUpdateTime' AND object_id = OBJECT_ID('ParcelData'))" &
        " CREATE INDEX IX_ParcelData_CC_LastUpdateTime ON ParcelData(CC_LastUpdateTime) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_ParcelData_mobile_assignment_group_id' AND object_id = OBJECT_ID('ParcelData'))" &
        " CREATE INDEX IX_ParcelData_mobile_assignment_group_id ON ParcelData(mobile_assignment_group_id) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_DataSourceRelationships_PrtTalId_Relship' AND object_id = OBJECT_ID('DataSourceRelationships'))" &
        " CREATE INDEX IX_DataSourceRelationships_PrtTalId_Relship ON DataSourceRelationships(ParentTableId,Relationship) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_XT_sales_history_seller_id' AND object_id = OBJECT_ID('XT_sales_history'))" &
        " CREATE INDEX IX_XT_sales_history_seller_id ON XT_sales_history(seller_id) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_XT_sales_history_chg_of_owner_id' AND object_id = OBJECT_ID('XT_sales_history'))" &
        " CREATE INDEX IX_XT_sales_history_chg_of_owner_id ON XT_sales_history(chg_of_owner_id) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_XT_sales_history_prop_ct' AND object_id = OBJECT_ID('XT_sales_history'))" &
        " CREATE INDEX IX_XT_sales_history_prop_ct ON XT_sales_history(prop_ct) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_XT_value_history_CC_LastUpdateTime' AND object_id = OBJECT_ID('XT_value_history'))" &
        " CREATE INDEX IX_XT_value_history_CC_LastUpdateTime ON XT_value_history(CC_LastUpdateTime) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_XT_prop_event_assoc_event_type' AND object_id = OBJECT_ID('XT_prop_event_assoc'))" &
        " CREATE INDEX IX_XT_prop_event_assoc_event_type ON XT_prop_event_assoc(event_type) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_XT_prop_event_assoc_event_date' AND object_id = OBJECT_ID('XT_prop_event_assoc'))" &
        " CREATE INDEX IX_XT_prop_event_assoc_event_date ON XT_prop_event_assoc(event_date) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_XT_permit_history_bldg_permit_appraiser_id' AND object_id = OBJECT_ID('XT_permit_history'))" &
        " CREATE INDEX IX_XT_permit_history_bldg_permit_appraiser_id ON XT_permit_history(bldg_permit_appraiser_id) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_XT_permit_history_bldg_permit_sub_type_cd' AND object_id = OBJECT_ID('XT_permit_history'))" &
        " CREATE INDEX IX_XT_permit_history_bldg_permit_sub_type_cd ON XT_permit_history(bldg_permit_sub_type_cd) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_XT_permit_history_street_num' AND object_id = OBJECT_ID('XT_permit_history'))" &
        " CREATE INDEX IX_XT_permit_history_street_num ON XT_permit_history(bldg_permit_street_num) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_XT_land_mass_adjustment_land_pct' AND object_id = OBJECT_ID('XT_land_mass_adjustment'))" &
        " CREATE INDEX IX_XT_land_mass_adjustment_land_pct ON XT_land_mass_adjustment(land_pct) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_XT_land_detail_ag_land_type_cd' AND object_id = OBJECT_ID('XT_land_detail'))" &
        " CREATE INDEX IX_XT_land_detail_ag_land_type_cd ON XT_land_detail(ag_land_type_cd) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_XT_land_detail_land_seg_id' AND object_id = OBJECT_ID('XT_land_detail'))" &
        " CREATE INDEX IX_XT_land_detail_land_seg_id ON XT_land_detail(land_seg_id) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_XT_land_detail_ls_mkt_id' AND object_id = OBJECT_ID('XT_land_detail'))" &
        " CREATE INDEX IX_XT_land_detail_ls_mkt_id ON XT_land_detail(ls_mkt_id) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_XT_imprv_CC_LastUpdateTime' AND object_id = OBJECT_ID('XT_imprv'))" &
        " CREATE INDEX IX_XT_imprv_CC_LastUpdateTime ON XT_imprv(CC_LastUpdateTime) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_XT_imprv_imprv_state_cd' AND object_id = OBJECT_ID('XT_imprv'))" &
        " CREATE INDEX IX_XT_imprv_imprv_state_cd ON XT_imprv(imprv_state_cd) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_XT_imprv_attr_i_attr_val_id' AND object_id = OBJECT_ID('XT_imprv_attr'))" &
        " CREATE INDEX IX_XT_imprv_attr_i_attr_val_id ON XT_imprv_attr(i_attr_val_id) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_XT_imprv_detail_prop_val_yr' AND object_id = OBJECT_ID('XT_imprv_detail'))" &
        " CREATE INDEX IX_XT_imprv_detail_prop_val_yr ON XT_imprv_detail(prop_val_yr) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_XT_imprv_detail_imprv_det_sub_class_cd' AND object_id = OBJECT_ID('XT_imprv_detail'))" &
        " CREATE INDEX IX_XT_imprv_detail_imprv_det_sub_class_cd ON XT_imprv_detail(imprv_det_sub_class_cd) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_XT_land_adj_land_seg_adj_seq' AND object_id = OBJECT_ID('XT_land_adj'))" &
        " CREATE INDEX IX_XT_land_adj_land_seg_adj_seq ON XT_land_adj(land_seg_adj_seq) ;" &
        "If Not Exists(SELECT * FROM sys.indexes WHERE name='IX_XT_land_adj_land_seg_adj_dt' AND object_id = OBJECT_ID('XT_land_adj'))" &
        " CREATE INDEX IX_XT_land_adj_land_seg_adj_dt ON XT_land_adj(land_seg_adj_dt) "

        ''Database.Tenant.Exec(strsql)'commented by JJ
        'Database.Tenant.Execute(strsql)
        'Alert("Index Created.")
        'LoadIndexRebuild()

        '''" CREATE INDEX IX_XT_imprv_detail_sketch_cmds ON XT_imprv_detail(sketch_cmds) ;" & _

        Dim connStr As String = Database.GetConnectionStringFromOrganization(hdnOrganizationId.Value) 'commented by JJ
        Try
            Dim conn As New SqlClient.SqlConnection(connStr)
            conn.Open()
            Dim cmd As New SqlClient.SqlCommand(strsql, conn)
            cmd.ExecuteNonQuery()
            Alert("Index Created.")
            conn.Close()
        Catch sqlex As SqlClient.SqlException
        Catch ex As Exception
        End Try
        LoadIndexRebuild()
    End Sub

    Protected Sub lnkEdit_Click(sender As Object, e As EventArgs) Handles lnkEdit.Click
        If hfEdit.Value = 0 Then
            For Each item As RepeaterItem In rptRoles.Items
                Dim chk As CheckBox = item.FindControl("chkRole")
                If chk.Text <> "SketchPro" Then
                    chk.Enabled = True
                End If
                Dim textbox As WebControl = CType(item.FindControl("txtSeat"), WebControl)
                textbox.Enabled = True
            Next

            lnkEdit.Text = "Update"
            hfEdit.Value = 1
        ElseIf hfEdit.Value = 1 Then
            lnkEdit.Text = "Edit"
            hfEdit.Value = 0
            Try
                AssignRolesFromList(hdnOrganizationId.Value)
            Catch ex As Exception
                LoadRolesList(OrganizationId)
                Alert(ex.Message)
                Return
            End Try
            LoadRolesList(OrganizationId)
            Alert("Roles updated successfully ")
        End If
    End Sub

    Protected Sub lnkEditLoc_Click(sender As Object, e As EventArgs) Handles lnkEditLoc.Click
        If hfEditLoc.Value = 0 Then
            lblCounty.Visible = False
            lblName.Visible = False
            lblCity.Visible = False
            lblState.Visible = False
            txtCounty.Visible = True
            txtName.Visible = True
            txtCity.Visible = True
            txtState.Visible = True
            lnkEditLoc.Text = "Update"
            hfEditLoc.Value = 1

        ElseIf hfEditLoc.Value = 1 Then

            If Not ValidateOrganization() Then Return

            txtCounty.Visible = False
            txtName.Visible = False
            txtCity.Visible = False
            txtState.Visible = False
            lblCounty.Visible = True
            lblName.Visible = True
            lblCity.Visible = True
            lblState.Visible = True
            lnkEditLoc.Text = "Edit"
            hfEditLoc.Value = 0
            UpdateOrganization(OrganizationId)
            Alert("Location Updated Successfully ")
        End If
    End Sub

    Protected Sub lnkEditServer_Click(sender As Object, e As EventArgs) Handles lnkEditServer.Click
        If hfEditServer.Value = 0 Then
            lblApplicnGroup.Visible = False
            lblHostServer.Visible = False
            lblDBName.Visible = False
            lblDemoHostServr.Visible = False
            lblStage.Visible = False
            lblDemoDBname.Visible = False
            lblMAHostKey.Visible = False
            ddlApplicnGroup.Visible = True
            ddlHostServer.Visible = True
            ddlDBName.Visible = True
            ddlDemoHostServr.Visible = True
            ddlStage.Visible = True
            ddlDemoDBname.Visible = True
            txtMAHostKey.Visible = True
            ProductionEnvYes.Visible = True
            ProductionEnvNo.Visible = True
            lblProEnv.Visible = False
            lnkEditServer.Text = "Update"
            hfEditServer.Value = 1
        ElseIf hfEditServer.Value = 1 Then
            ddlApplicnGroup.Visible = False
            ddlHostServer.Visible = False
            ddlDBName.Visible = False
            ddlDemoHostServr.Visible = False
            ddlStage.Visible = False
            ddlDemoDBname.Visible = False
            txtMAHostKey.Visible = False
            lblApplicnGroup.Visible = True
            lblHostServer.Visible = True
            lblDBName.Visible = True
            lblDemoHostServr.Visible = True
            lblStage.Visible = True
            lblDemoDBname.Visible = True
            lblMAHostKey.Visible = True
            ProductionEnvYes.Visible = False
            ProductionEnvNo.Visible = False
            lblProEnv.Visible = True
            lnkEditServer.Text = "Edit"
            hfEditServer.Value = 0
            UpdateServer(hdnOrganizationId.Value)
            Alert("Server Configured Successfully ")
        End If
    End Sub

    Protected Sub lnkEditVendor_Click(sender As Object, e As EventArgs) Handles lnkEditVendor.Click
        If hfEditVendor.Value = 0 Then
            lblVendorID.Visible = False
            lblSupportVendor.Visible = False
            lblCAMASystem.Visible = False
            ddlVendorID.Visible = True
            ddlSupportVendorID.Visible = True
            ddlCAMASystem.Visible = True
            txtNotes.Enabled = True
            lnkEditVendor.Text = "Update"
            hfEditVendor.Value = 1
        ElseIf hfEditVendor.Value = 1 Then
            ddlVendorID.Visible = False
            ddlSupportVendorID.Visible = False
            ddlCAMASystem.Visible = False
            lblVendorID.Visible = True
            lblSupportVendor.Visible = True
            lblCAMASystem.Visible = True
            txtNotes.Enabled = False
            lnkEditVendor.Text = "Edit"
            hfEditVendor.Value = 0
            UpdateVendor(hdnOrganizationId.Value)
            Alert("Vendor Information Updated Successfully ")
        End If
    End Sub


    Public Sub gvBind(ByVal id As Integer)
        Dim dtBackupSchedule As New DataTable
        dtBackupSchedule.Clear()
        dtBackupSchedule = Database.System.GetDataTable("SELECT  BackupId,RepeatFactor,ScheduleDay,ScheduleTime FROM BackupSchedule WHERE OrganizationId = " & id)
        Try
            If dtBackupSchedule.Rows.Count = 0 Then
                dtBackupSchedule.Rows.Add(dtBackupSchedule.NewRow())
                gvExecutionPlan.DataSource = dtBackupSchedule
                gvExecutionPlan.DataBind()
            Else
                gvExecutionPlan.DataSource = dtBackupSchedule
                gvExecutionPlan.DataBind()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub gvExecutionPlan_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvExecutionPlan.RowDataBound
        If e.Row.RowType = DataControlRowType.Footer Then
            Dim ddlAddRepeatFactor As DropDownList = DirectCast(e.Row.FindControl("gvddlAddRepeatFactor"), DropDownList)
            Dim ddlAddScheduleDay As DropDownList = DirectCast(e.Row.FindControl("gvddlAddScheduleDay"), DropDownList)
            Dim ddlAddScheduleTime As DropDownList = DirectCast(e.Row.FindControl("gvddlAddScheduleTime"), DropDownList)

            ddlAddRepeatFactor.Items.Clear()
            Dim dtAddRepeatFactor As DataTable = RepeatFactorTable()
            ddlAddRepeatFactor.FillFromTable(dtAddRepeatFactor, True, )

            ddlAddScheduleDay.Items.Clear()
            Dim dtAddScheduleDay As DataTable = SchedulDayTable()
            ddlAddScheduleDay.FillFromTable(dtAddScheduleDay, True, )

            ddlAddScheduleTime.Items.Clear()
            Dim dtAddScheduleTime As DataTable = SchedulTimeTable()
            ddlAddScheduleTime.FillFromTable(dtAddScheduleTime, True, )
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            If (e.Row.RowState And DataControlRowState.Edit) > 0 Then
                Dim ddlRepeatFactor As DropDownList = DirectCast(e.Row.FindControl("gvddlRepeatFactor"), DropDownList)
                Dim ddlScheduleDay As DropDownList = DirectCast(e.Row.FindControl("gvddlScheduleDay"), DropDownList)
                Dim ddlScheduleTime As DropDownList = DirectCast(e.Row.FindControl("gvddlScheduleTime"), DropDownList)

                ddlRepeatFactor.Items.Clear()
                Dim dtRepeatFactor As DataTable = RepeatFactorTable()
                ddlRepeatFactor.FillFromTable(dtRepeatFactor, True, )

                ddlScheduleDay.Items.Clear()
                Dim dtScheduleDay As DataTable = SchedulDayTable()
                ddlScheduleDay.FillFromTable(dtScheduleDay, True, )

                ddlScheduleTime.Items.Clear()
                Dim dtScheduleTime As DataTable = SchedulTimeTable()
                ddlScheduleTime.FillFromTable(dtScheduleTime, True, )

                Dim dr As DataRowView = TryCast(e.Row.DataItem, DataRowView)
                ddlRepeatFactor.SelectedItem.Text = dr("RepeatFactor").ToString()
                ddlScheduleDay.SelectedItem.Text = dr("ScheduleDay").ToString()
                ddlScheduleTime.SelectedItem.Text = dr("ScheduleTime").ToString()
            End If
        End If
    End Sub
    Protected Sub gvExecutionPlan_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvExecutionPlan.RowCommand
        If e.CommandName.Equals("ADD") Then
            Dim HostServer As String
            Dim ServerId As Integer
            Dim OrganizationId As Integer
            Dim DatabaseName As String
            HostServer = lblHostServer.Text
            OrganizationId = lblOrganiznId.Text
            DatabaseName = lblDBName.Text
            If HostServer <> "" Then
                Dim drDBHost As DataRow = Database.System.GetTopRow("SELECT * FROM [dbo].[DatabaseServer] where Name= '" & HostServer & "'")
                ServerId = drDBHost.GetString("Id")
            End If

            Dim RepeatFactor As String = DirectCast(gvExecutionPlan.FooterRow.FindControl("gvddlAddRepeatFactor"), DropDownList).SelectedItem.Text
            Dim BackupDay As String = DirectCast(gvExecutionPlan.FooterRow.FindControl("gvddlAddScheduleDay"), DropDownList).SelectedItem.Text
            Dim BackupTime As String = DirectCast(gvExecutionPlan.FooterRow.FindControl("gvddlAddScheduleTime"), DropDownList).SelectedItem.Text

            Dim UpdateQuery As String = ""
            Dim o As DataRow = Database.System.GetTopRow("SELECT Name FROM Organization WHERE Id = " & OrganizationId)
            Dim CountyNm As String = o.GetString("Name").ToSqlValue()

            If RepeatFactor <> "-- Select --" And BackupDay <> "-- Select --" And BackupTime <> "-- Select --" Then
                Dim sql As String = "SELECT COUNT(*) FROM BackupSchedule WHERE OrganizationId = " & OrganizationId
                If Database.System.GetIntegerValue(sql) < 5 Then
                    Dim sqlQueryInsert As String = String.Format("INSERT INTO BackupSchedule(ServerId,OrganizationId,DatabaseName,RepeatFactor,ScheduleDay,ScheduleTime) VALUES('" & ServerId & "','" & OrganizationId & "','" & DatabaseName & "','" & RepeatFactor & "','" & BackupDay & "','" & BackupTime & "')")
                    Database.System.Execute(sqlQueryInsert)
                    UpdateQuery = "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserName & "'," & CountyNm & ", 'Backup schedule added on " & RepeatFactor & " " & BackupDay & " at " & BackupTime & "' )"
                    Try
                        Database.System.Execute(UpdateQuery)
                    Catch ex As Exception

                    End Try
                Else
                    Alert("You have exceeded the maximum number (3) of Apex scheduled jobs.. You can update existing schedule.")
                    gvBind(lblOrganiznId.Text)
                End If
            Else
                Alert("You should select value in all dropdown list.")
            End If
            gvBind(lblOrganiznId.Text)
        End If
    End Sub
    Protected Sub gvExecutionPlan_RowEditing(sender As Object, e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvExecutionPlan.RowEditing
        gvExecutionPlan.EditIndex = e.NewEditIndex
        gvBind(lblOrganiznId.Text)
    End Sub
    Protected Sub gvExecutionPlan_RowCancelingEdit(sender As Object, e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles gvExecutionPlan.RowCancelingEdit
        gvExecutionPlan.EditIndex = -1
        gvBind(lblOrganiznId.Text)
    End Sub

    Protected Sub gvExecutionPlan_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvExecutionPlan.RowDeleting
        Dim BackupId As Integer = Integer.Parse(DirectCast(gvExecutionPlan.Rows(e.RowIndex).FindControl("hfBackupId"), HiddenField).Value)
        Dim row As GridViewRow = DirectCast(gvExecutionPlan.Rows(e.RowIndex), GridViewRow)

        Dim UpdateQuery As String = ""
        Dim o As DataRow = Database.System.GetTopRow("SELECT Name FROM Organization WHERE Id = " & OrganizationId)
        Dim CountyNm As String = o.GetString("Name").ToSqlValue()

        Dim b As DataRow = Database.System.GetTopRow("SELECT RepeatFactor,ScheduleDay,ScheduleTime FROM BackupSchedule WHERE BackupId = " & BackupId)
        Dim RepeatFactor As String = b.GetString("RepeatFactor")
        Dim ScheduleDay As String = b.GetString("ScheduleDay")
        Dim ScheduleTime As String = b.GetString("ScheduleTime")

        Database.System.Execute("DELETE FROM BackupSchedule WHERE BackupId= " & BackupId)
        gvBind(lblOrganiznId.Text)

        UpdateQuery = "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserName & "'," & CountyNm & ", 'Backup schedule for " & RepeatFactor & " " & ScheduleDay & " at " & ScheduleTime & " is removed' )"
        Try
            Database.System.Execute(UpdateQuery)
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvExecutionPlan_RowUpdating(sender As Object, e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles gvExecutionPlan.RowUpdating
        Dim BackupId As Integer = Integer.Parse(DirectCast(gvExecutionPlan.Rows(e.RowIndex).FindControl("hfBackupId"), HiddenField).Value)
        Dim RepeatFactor As String = DirectCast(gvExecutionPlan.Rows(e.RowIndex).FindControl("gvddlRepeatFactor"), DropDownList).SelectedItem.Text
        Dim BackupDay As String = DirectCast(gvExecutionPlan.Rows(e.RowIndex).FindControl("gvddlScheduleDay"), DropDownList).SelectedItem.Text
        Dim BackupTime As String = DirectCast(gvExecutionPlan.Rows(e.RowIndex).FindControl("gvddlScheduleTime"), DropDownList).SelectedItem.Text
        Dim quary As String
        Dim b As DataRow = Database.System.GetTopRow("SELECT OrganizationId,RepeatFactor,ScheduleDay,ScheduleTime FROM BackupSchedule WHERE BackupId = " & BackupId)
        Dim OldRF As String = b.GetString("RepeatFactor")
        Dim OldSD As String = b.GetString("ScheduleDay")
        Dim OldST As String = b.GetString("ScheduleTime")

        Dim UpdateQuery As String = ""
        Dim o As DataRow = Database.System.GetTopRow("SELECT Name FROM Organization WHERE Id = " & OrganizationId)
        Dim CountyNm As String = o.GetString("Name").ToSqlValue()

        quary = "UPDATE BackupSchedule SET  RepeatFactor = '" & RepeatFactor & "', ScheduleDay ='" & BackupDay & "', ScheduleTime= '" & BackupTime & "' WHERE BackupId= " & BackupId
        Dim sqlQuery As String = String.Format(quary)
        Database.System.Execute(sqlQuery)

        If (RepeatFactor <> OldRF Or BackupDay <> OldSD Or BackupTime <> OldST) Then
            UpdateQuery = "INSERT INTO [AdminSettingsAuditTrail](EventTime,LoginId,County,Description ) VALUES (GETUTCDATE(),'" & UserName & "'," & CountyNm & ", 'Backup schedule is updated from " & OldRF & " " & OldSD & " at " & OldST & "  to " & RepeatFactor & " " & BackupDay & " at " & BackupTime & "' )"
            Try
                Database.System.Execute(UpdateQuery)
            Catch ex As Exception

            End Try
        End If

        gvExecutionPlan.EditIndex = -1
        gvBind(lblOrganiznId.Text)
    End Sub

    Protected Sub gvExecutionPlan_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvExecutionPlan.PageIndexChanging
        gvExecutionPlan.PageIndex = e.NewPageIndex
        gvBind(lblOrganiznId.Text)
    End Sub

    Public Function RepeatFactorTable() As DataTable
        Dim dtR As New DataTable
        dtR.Columns.Add("RID")
        dtR.Columns.Add("RepeatFactor")
        Dim repeats As String() = {"Every", "First", "Second", "Third", "Fourth", "Fifth", "Last"}
        For i As Integer = 0 To 6
            Dim dr As DataRow = dtR.NewRow
            dr(0) = i
            dr(1) = repeats(i)
            dtR.Rows.Add(dr)
        Next
        Return dtR
    End Function
    Public Function SchedulDayTable() As DataTable
        Dim dtW As New DataTable
        dtW.Columns.Add("WeekDay")
        dtW.Columns.Add("ScheduleDay")
        For i As Integer = 0 To 6
            Dim w As System.DayOfWeek = i
            Dim dr As DataRow = dtW.NewRow
            dr("WeekDay") = i
            dr("ScheduleDay") = w.ToString
            dtW.Rows.Add(dr)
        Next
        Return dtW
    End Function
    Public Function SchedulTimeTable() As DataTable
        Dim StartTime As DateTime = DateTime.ParseExact("00:00", "HH:mm", Nothing)
        Dim EndTime As DateTime = DateTime.ParseExact("23:00", "HH:mm", Nothing)
        Dim Interval As New TimeSpan(6, 0, 0)
        'Dim interval As TimeSpan = _
        '    TimeSpan.FromMinutes(10)
        Dim timeInterval As String = Interval.ToString()
        Dim dtR As New DataTable
        dtR.Columns.Add("ROWID")
        dtR.Columns.Add("ScheduleTime")
        Dim i As Integer = 0
        While StartTime <= EndTime
            Dim dr As DataRow = dtR.NewRow
            dr(0) = i
            dr(1) = StartTime.ToString("HH:mm")
            dtR.Rows.Add(dr)
            StartTime = StartTime.Add(Interval)
            i = i + 1
        End While
        Return dtR
    End Function

    Protected Sub btnBackupNow_Click(sender As Object, e As EventArgs) Handles btnBackupNow.Click
        Dim HostServer As String
        Dim DBHost As String
        HostServer = lblHostServer.Text
        If HostServer <> "" Then

            Dim drDBHost As DataRow = Database.System.GetTopRow("SELECT * FROM [dbo].[DatabaseServer] where Name= '" & HostServer & "'")
            DBHost = drDBHost.GetString("Host")

            Dim dtDataBase As New DataTable
            Dim sqlQuery As String = String.Format("EXEC [" & DBHost & "].[Master].[dbo].[sp_BackupDatabase] {0}".SqlFormatString(lblDBName.Text))
            Database.System.Execute(sqlQuery, 6000)
            Alert("Backup Database successfully processed")
            LastBackUp(lblOrganiznId.Text)
        End If
    End Sub
   
    Protected Sub ddlVendorID_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlVendorID.SelectedIndexChanged
        Dim dtCAMASystem As New DataTable
        dtCAMASystem = Database.System.GetDataTable("SELECT Id,Name FROM CAMASystem WHERE VendorId=" & ddlVendorID.SelectedValue & " ORDER BY Name")
        ddlCAMASystem.FillFromTable(dtCAMASystem, True, "-- Select CAMASystem --", ID)
    End Sub
    Sub UpdateSeat(ByVal seat As String, ByVal orgId As Integer, ByVal roleId As Integer, Optional ByVal Removed As Integer = 0)
        Database.System.Execute("EXEC SP_UserSeat @OrgId = {0},@RoleId = {1},@SeatCount = {2},@RemoveSeat = {3},@UserNm = {4}".SqlFormatString(orgId, roleId, seat, Removed, UserName))
    End Sub
End Class