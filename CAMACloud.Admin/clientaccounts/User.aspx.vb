﻿Public Class User
    Inherits System.Web.UI.Page
    Dim FullName As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim o As DataRow = Database.System.GetTopRow("SELECT * FROM Organization WHERE Id = " & HttpContext.Current.GetCAMASession.OrganizationId.ToString)
            lblClient.Text = o.GetString("Name")
            BindGridView()
            LoadRolesList()
            ddlMobileCarrierLoad()
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If (hdnUserId.Value <> "") Then
            btnSaveUser.ValidationGroup = "UserProp2"
            rfvtxtFirstName.ValidationGroup = "UserProp2"
            rfvtxtUserMail.ValidationGroup = "UserProp2"
            revtxtUserMail.ValidationGroup = "UserProp2"
            revtxtMobileNumber.ValidationGroup = "UserProp2"
            ' rfvtxtMobileNumber.ValidationGroup = "UserProp2"
            rfvddlMobileCarrier.ValidationGroup = "UserProp2"
            '  rfvtxtMobileNumber.ValidationGroup = "UserProp2"

        Else
            btnSaveUser.ValidationGroup = "UserProp"
            rfvtxtFirstName.ValidationGroup = "UserProp"
            rfvtxtUserMail.ValidationGroup = "UserProp"
            revtxtUserMail.ValidationGroup = "UserProp"
            revtxtMobileNumber.ValidationGroup = "UserProp"
            ' rfvtxtMobileNumber.ValidationGroup = "UserProp"
            rfvddlMobileCarrier.ValidationGroup = "UserProp"
            'rfvtxtMobileNumber.ValidationGroup = "UserProp"

        End If
    End Sub

    Protected Sub BindGridView()
        For Each u As MembershipUser In Membership.GetAllUsers
            If Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM UserSettings WHERE LoginId = {0}".SqlFormatString(u.UserName)) = 0 Then
                Database.Tenant.Execute("INSERT INTO UserSettings (LoginId, FirstName) VALUES ({0}, {0})".SqlFormatString(u.UserName))
            End If
        Next
        gdvUserDetails.DataSource = Database.Tenant.GetDataTable("SELECT * FROM UserSettings ORDER BY FirstName")
        gdvUserDetails.DataBind()
    End Sub

    Protected Sub gdvUserDetails_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gdvUserDetails.RowCommand
        hdnUserId.Value = e.CommandArgument
        Select Case e.CommandName
            Case "DeleteUser"
                If e.CommandArgument = Membership.GetUser().UserName Then
                    Alert("You cannot delete your own user account.")
                    Return
                End If
                If e.CommandArgument = "admin" Then
                    Alert("You cannot delete the default 'admin' user.")
                    Return
                End If
                Membership.DeleteUser(e.CommandArgument)
                Database.Tenant.Execute("DELETE FROM UserTrackingCurrent WHERE LoginId = " + e.CommandArgument.ToString.ToSqlValue)
                Database.Tenant.Execute("DELETE FROM UserSettings WHERE LoginId = " + e.CommandArgument.ToString.ToSqlValue)
                BindGridView()
            Case "EditUser"
                Try
                    ClearForm()
                    LoadUserForEdit(e.CommandArgument)
                Catch ex As Exception
                    Alert(ex.Message)
                End Try

            Case "UnlockUser"
                Dim u As MembershipUser = Membership.GetUser(e.CommandArgument)
                If u IsNot Nothing Then
                    u.UnlockUser()
                End If
                BindGridView()
            Case Else

        End Select
        'BindGridView()
    End Sub

    Protected Sub lbNewUser_Click(sender As Object, e As System.EventArgs) Handles lbNewUser.Click
        ClearForm()
    End Sub

    Sub ClearForm()
        hdnUserId.Value = ""
        txtFirstName.Text = ""
        txtMiddleName.Text = ""
        txtLastName.Text = ""
        txtMobileNumber.Text = ""
        ddlMobileCarrier.SelectedIndex = 0
        txtUserMail.Text = ""
        txtLoginId.Text = ""
        txtPassword.Text = ""
        txtPassword2.Text = ""
        txtLoginId.Enabled = True
        rfvPassword.Enabled = True
        For Each li As ListItem In cblRoles.Items
            li.Selected = False
        Next
        btnCancel.Text = "Cancel"
    End Sub

    Protected Sub btnSaveUser_Click(sender As Object, e As System.EventArgs) Handles btnSaveUser.Click
        Dim res As MembershipCreateStatus
        Dim userName As String = txtLoginId.Text
        FullName = txtFirstName.Text + " " + txtMiddleName.Text + " " + txtLastName.Text
        Try
            If hdnUserId.Value = "" Then

                Membership.CreateUser(userName, txtPassword.Text, txtUserMail.Text, "@", "#", True, res)
                If res = MembershipCreateStatus.Success Then
                    Database.Tenant.Execute("INSERT INTO UserSettings(LoginId,FirstName,MiddleName,LastName,Email,Mobile,MobileCarrierId,NotifyByEmail,NotifyBySMS,AutoQC) VALUES({0},{1},{2},{3},{4},{5},{6},{7},{8},{9})".SqlFormatString(userName, txtFirstName.Text,
                                            IIf(txtMiddleName.Text = "", "", txtMiddleName.Text), IIf(txtLastName.Text = "", "", txtLastName.Text), txtUserMail.Text, txtMobileNumber.Text, ddlMobileCarrier.SelectedValue, IIf(cbNotifybyEmail.Checked, 1, 0), IIf(cbNotifybySMS.Checked, 1, 0), IIf(cbAutoQC.Checked, 1, 0)))
                    AssignRolesFromList(userName)
                    ClearForm()
                    BindGridView()
                    RunScript("hidePopup();")
                Else
                    If res = MembershipCreateStatus.DuplicateEmail Then
                        Alert("This email address has been used for another account. Please try with a different email address.")
                        Return
                    End If
                    If res = MembershipCreateStatus.DuplicateUserName Then
                        Alert("Log in ID already exists. Please choose another one.")
                        Return
                    End If
                    If res = MembershipCreateStatus.InvalidUserName Then
                        Alert("Log in ID is invalid. Please choose another one.")
                        Return
                    End If
                    If res = MembershipCreateStatus.InvalidPassword Then
                        Alert("Password must be minimum 6 characters in length.")
                        Return
                    End If
                    Alert("Error: " + res.ToString)
                    Return
                End If

            Else
                Dim u As MembershipUser = Membership.GetUser(userName)

                If txtPassword.Text.IsNotEmpty AndAlso txtPassword.Text = txtPassword2.Text Then
                    If txtPassword.Text.Length < 6 Then
                        Alert("Password must be minimum 6 characters in length.")
                        Return
                    End If
                    u.ChangePassword(u.ResetPassword(), txtPassword.Text)
                End If

                u.Email = txtUserMail.Text
                Membership.UpdateUser(u)
                Dim lookUpId As Integer
                lookUpId = Database.Tenant.Execute("UPDATE UserSettings SET FirstName={1},MiddleName={2},LastName={3},Email={4},Mobile={5},MobileCarrierId={6},NotifyByEmail={7},NotifyBySMS={8},AutoQC={9} WHERE LoginId={0};SELECT CAST(@@ROWCOUNT AS INT) As NeewId ".SqlFormatString(userName, txtFirstName.Text,
                                            IIf(txtMiddleName.Text = "", "", txtMiddleName.Text), IIf(txtLastName.Text = "", "", txtLastName.Text), txtUserMail.Text, txtMobileNumber.Text, ddlMobileCarrier.SelectedValue, IIf(cbNotifybyEmail.Checked, 1, 0), IIf(cbNotifybySMS.Checked, 1, 0), IIf(cbAutoQC.Checked, 1, 0)))
                If (lookUpId = 0) Then
                    Database.Tenant.Execute("INSERT INTO UserSettings(LoginId,FirstName,MiddleName,LastName,Email,Mobile,MobileCarrierId,NotifyByEmail,NotifyBySMS,AutoQC) VALUES({0},{1},{2},{3},{4},{5},{6},{7},{8},{9})".SqlFormatString(userName, txtFirstName.Text,
                                          IIf(txtMiddleName.Text = "", "", txtMiddleName.Text), IIf(txtLastName.Text = "", "", txtLastName.Text), txtUserMail.Text, txtMobileNumber.Text, ddlMobileCarrier.SelectedValue, IIf(cbNotifybyEmail.Checked, 1, 0), IIf(cbNotifybySMS.Checked, 1, 0), IIf(cbAutoQC.Checked, 1, 0)))
                End If


                AssignRolesFromList(userName)
                BindGridView()
                Alert("Changes updated successfully.\n\nClick Close to go back to the list.")
                If chkLocked.Visible AndAlso chkLocked.Checked = False Then
                    u.UnlockUser()
                End If
            End If

        Catch ex As Exception
            Alert(ex.Message)
        End Try
    End Sub

    Sub AssignRolesFromList(userName As String)
        For Each li As ListItem In cblRoles.Items
            If li.Selected Then
                If Not Roles.IsUserInRole(userName, li.Value) Then
                    Roles.AddUserToRole(userName, li.Value)
                End If
            Else
                If Roles.IsUserInRole(userName, li.Value) Then
                    Roles.RemoveUserFromRole(userName, li.Value)
                End If
            End If
        Next
    End Sub

    Sub LoadRolesList()
        Dim dtOrgRoles As DataTable = Database.System.GetDataTable("EXEC GetRoles 0, " + HttpContext.Current.GetCAMASession.OrganizationId.ToString)
        Dim roleNames As String() = dtOrgRoles.AsEnumerable.Select(Function(x) x.GetString("ASPRoleName")).ToArray
        For Each role In roleNames
            If Not Roles.RoleExists(role) Then
                Roles.CreateRole(role)
            End If
        Next
        For Each role In Roles.GetAllRoles()
            If Not roleNames.Contains(role) Then
                If Roles.GetUsersInRole(role).Count > 0 Then
                    Roles.RemoveUsersFromRole(Roles.GetUsersInRole(role), role)
                End If
                Roles.DeleteRole(role)
            End If
        Next
        cblRoles.DataSource = Database.System.GetDataTable("EXEC GetRoles 1, " + HttpContext.Current.GetCAMASession.OrganizationId.ToString)
        cblRoles.DataTextField = "Name"
        cblRoles.DataValueField = "ASPRoleName"
        cblRoles.DataBind()
    End Sub

    Sub ddlMobileCarrierLoad()
        FillFromSqlWithDatabase(ddlMobileCarrier, Database.System, "SELECT ID, CarrierName, Gateway FROM MobileCarrier ORDER BY CarrierName", True)
    End Sub

    Sub LoadUserForEdit(userName As String)
        hdnUserId.Value = userName
        Dim u As MembershipUser = Membership.GetUser(userName)
        If u Is Nothing Then
            Dim tr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM UserSettings WHERE LoginId='" + userName + "'")
            Membership.CreateUser(userName, "123456", tr.Get("Email"))
            u = Membership.GetUser(userName)
        End If

        txtUserMail.Text = u.Email
        txtLoginId.Text = u.UserName
        txtLoginId.Enabled = False
        rfvPassword.Enabled = False
        Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM UserSettings WHERE LoginId='" + u.UserName + "'")
        If dr IsNot Nothing Then
            txtFirstName.Text = dr.GetString("FirstName")
            txtMiddleName.Text = dr.GetString("MiddleName")
            txtLastName.Text = dr.GetString("LastName")
            txtMobileNumber.Text = dr.GetString("Mobile")

            ' ddlMobileCarrier.SelectedValue = IIf(dt.Rows(0)("MobileCarrierId") IsNot Nothing, ddlMobileCarrier.Items.IndexOf(ddlMobileCarrier.Items.FindByValue(dt.Rows(0)("MobileCarrierId").ToString())), ddlMobileCarrier.Items.IndexOf(ddlMobileCarrier.Items.FindByValue("15")))
            ddlMobileCarrier.SelectedValue = dr.GetString("MobileCarrierId")

            cbNotifybyEmail.Checked = dr.GetBoolean("NotifyByEmail")
            cbNotifybySMS.Checked = dr.GetBoolean("NotifyBySMS")
            cbAutoQC.Checked = dr.GetBoolean("AutoQC")
        Else
            txtFirstName.Text = ""
        End If
        For Each li As ListItem In cblRoles.Items
            If Roles.IsUserInRole(userName, li.Value) Then
                li.Selected = True
            Else
                li.Selected = False
            End If
        Next
        btnCancel.Text = "Close"

        If u.IsLockedOut Then
            chkLocked.Checked = True
        Else
            chkLocked.Visible = False
        End If
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        RunScript("hidePopup();")
    End Sub

    Public ReadOnly Property UserLastLoginDate As String
        Get
            If Membership.GetUser(Eval("LoginId").ToString) Is Nothing Then
                Return String.Empty
            End If
            Return Database.Tenant.GetStringValue("SELECT dbo.GetLocalDate('" + Membership.GetUser(Eval("LoginId").ToString).LastActivityDate.ToString("yyyy-MM-dd HH:mm:ss") + "')")
        End Get
    End Property

    Public Function IsUserLocked() As Boolean
        Dim loginId As String = Eval("LoginId")
        Dim u As MembershipUser = Membership.GetUser(loginId)
        If u Is Nothing Then
            Return False
        End If
        If u.IsLockedOut Then
            Return True
        End If
        Return False
    End Function

    Public Function UserStatus() As String
        Dim loginId As String = Eval("LoginId")
        Dim u As MembershipUser = Membership.GetUser(loginId)
        If u Is Nothing Then
            Return "invalid"
        End If
        If u.IsLockedOut Then
            Return "locked"
        End If
        If Not u.IsApproved Then
            Return "pending"
        End If
        Return "ok"
    End Function
End Class