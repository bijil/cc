﻿Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Data.SqlClient
Imports System.IO
'Imports System.Web
'Imports System.Data.SqlClient
'Imports System.Collections.Generic
'Imports System.Web.Services
'Imports System.Web.Script.Services

Public Class list
	Inherits System.Web.UI.Page
	
	ReadOnly Property PageAction As String
		Get
			Dim sPageAction As String = Coalesce(Request("pageaction"), "")
			Select Case sPageAction.ToLower
				Case "license"
					Return "license"
				Case "edit"
					Return "edit"
				Case "appaccess"
					Return "appaccess"
				Case Else
					Return "edit"
			End Select
		End Get
	End Property
	
	Sub LoadGrid(Optional ByVal pageIndex As Integer = 0)
		results.PageSize = ddlPageSize.SelectedValue
		Dim sqlFilter As String = ""
		Dim sortOrder As String = "org.state, org.Name"
		Dim SearchText As String = txtName.Text
		Dim sqlSettings As String = ""
		SearchText = SearchText.Replace("%", "[%]")
		Select Case ddlOrderBy.SelectedValue
			Case "Name"
				sortOrder = "org.Name"
			Case "Id"
				sortOrder = "org.Id"
			Case "VendorName"
				sortOrder = "vd.Name"
			Case "CamaName"
				sortOrder = "ca.Name"
				
		End Select
		If chkProEnv.Checked Then
			sqlFilter = " where ( org.IsProduction = 'true' Or org.IsProduction = 1 ) "
		Else
			sqlFilter = " WHERE 1 = 1 "
		End If
		Select Case ddlFilter.SelectedValue
			Case "1"
				sqlFilter = sqlFilter + " AND (org.Name Like '%{0}%' OR County LIKE '%{0}%')".FormatString(SearchText.Trim.ToSqlValue.Trim("'"))
			Case "2"
				sqlFilter = sqlFilter + IIf(ddlState.SelectedValue = "", "", " AND org.state ='{0}'".FormatString(ddlState.SelectedValue.ToSqlValue.Trim("'")))
			Case "3"
				sqlFilter = sqlFilter + " AND vd.Name LIKE '%{0}%'".FormatString(SearchText.Trim.ToSqlValue.Trim("'"))
            Case "4"
                sqlFilter = sqlFilter + IIf(ddlCamaSystem.SelectedValue = "", "", " AND ca.Id = {0} ".FormatString(ddlCamaSystem.SelectedValue))
            Case "5"
				
				Dim dts As DataRow =  Database.System.GetTopRow("SELECT Name from AdvancedSettings where RowID = {0}".FormatString(ddlSettings.SelectedIndex))
				If Not IsNothing(dts) Then
					sqlSettings = dts.GetString("Name")
					sqlFilter = sqlFilter + IIf(ddlSettings.SelectedValue = "", "", " AND os."+sqlSettings+" = 1 ") 
				End If
				
				
		End Select

        results.PageIndex = pageIndex
        Dim dt As DataTable = Database.System.GetDataTable("SELECT org.Id,org.UID,org.Name,org.State,CASE WHEN org.IsProduction = 1 THEN 'Yes' ELSE 'No' END as IsProduction,org.MAHostKey,org.CreatedDate,org.ApplicationHostId,vd1.Name as SupportVendor,vd.Id AS vendor,vd.Name AS vendorName,ca.Id AS camasysId,ca.name AS camsysName,D.Name Hostserver,CASE WHEN a.UseHTTPS ='True' THEN CONCAT('https://',org.MAHostKey,'.camacloud.com') ELSE CONCAT('http://',org.MAHostKey,'.camacloud.com') END AS MAHostUrl,CONVERT(VARCHAR,CAST(org.CreatedDate AS DATE), 101) AS Created_date FROM organization org LEFT JOIN Vendor vd ON vd.Id = org.VendorId LEFT JOIN Vendor vd1 ON vd1.Id = org.SupportVendorId  LEFT JOIN CAMASystem ca ON ca.id = org.CAMASystem LEFT JOIN OrganizationSettings os ON org.id = os.OrganizationId LEFT JOIN [DatabaseServer] D ON org.DBHost=D.Host LEFT JOIN ApplicationHost a ON a.Id = org.ApplicationHostId" + sqlFilter + " ORDER BY " + sortOrder)
		
		results.DataSource = dt
		results.DataBind()
		gvPrint.DataSource = dt
		ViewState("gridData") = dt
		gvPrint.DataBind()
		If results.Rows.Count = 0 Then
			ibtnExportToExcel.Visible = False
		Else
			ibtnExportToExcel.Visible = True
		End If
	End Sub
	
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		
		
		Select Case ddlFilter.SelectedValue
			Case "1"
				txtName.Visible = True
				btnSearch.Visible = True
				ddlState.Visible = False
                ddlSettings.Visible = False
                ddlCamaSystem.Visible = False
            Case "2"
				txtName.Text = ""
				txtName.Visible = False
				btnSearch.Visible = False
				ddlState.Visible = True
                ddlSettings.Visible = False
                ddlCamaSystem.Visible = False
            Case "5"
                txtName.Text = ""
				txtName.Visible = False
				btnSearch.Visible = False
				ddlState.Visible = False
                ddlSettings.Visible = True
                ddlCamaSystem.Visible = False
            Case "4"
                txtName.Text = ""
                txtName.Visible = False
                btnSearch.Visible = False
				ddlState.Visible = False
				ddlSettings.Visible = False
				ddlCamaSystem.Visible = True
            Case Else
				txtName.Visible = True
				btnSearch.Visible = True
				ddlState.Visible = False
                ddlSettings.Visible = False
                ddlCamaSystem.Visible = False
        End Select
		If Not IsPostBack Then
			LoadGrid()
			Dim dt As DataTable = Database.System.GetDataTable("SELECT DISTINCT State  FROM Organization  ORDER BY state")
			ddlState.FillFromTable(dt, True, "--Select--")
            LoadSettingInfo()
            LoadCamaSystem()
        End If
	End Sub

    Sub LoadSettingInfo()
        Dim dtTable As DataTable = Database.system.GetDataTable("select DisplayName from AdvancedSettings ")
        If dtTable.Rows.Count > 0 Then
            ddlSettings.FillFromTable(dtTable, True, "--- Select ---")
        End If

    End Sub
    Sub LoadCamaSystem()
        Dim dt As DataTable = Database.System.GetDataTable("SELECT Id, Name FROM CAMASystem ORDER BY ID")
        If dt IsNot Nothing Then
            ddlCamaSystem.FillFromTable(dt, True, "--- Select ---")
        End If
    End Sub


    Protected Sub results_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles results.PageIndexChanging
		LoadGrid(e.NewPageIndex)
	End Sub
	
	Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
		LoadGrid()
	End Sub
	
	Protected Sub results_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles results.RowCommand
		Select Case e.CommandName
			Case "SelectAccount"
				Select Case PageAction
					Case "license"
						Response.Redirect("~/license/?ouid=" + e.CommandArgument)
					Case "edit"
						Response.Redirect("~/clientaccounts/ManageClient.aspx?ouid=" + e.CommandArgument)
						'Response.Redirect("~/clientaccounts/ManageClient.aspx?ouid=" + e.CommandArgument)
					Case "appaccess"
						Response.Redirect("~/license/applicationaccess.aspx?ouid=" + e.CommandArgument)
				End Select
		End Select
	End Sub
	
	Public Function MAHostUrl() As String
		If Eval("MAHostKey") IsNot DBNull.Value Then
			If Eval("ApplicationHostId") IsNot DBNull.Value Then
				Dim Apphostid As Integer
				Dim strUsehttps As String ', HostGrp As String  --Reomoved Host group from MA url as per the request from Joseph--
				Apphostid = Eval("ApplicationHostId")
				Dim AppHost As DataRow = Database.System.GetTopRow("SELECT * FROM ApplicationHost WHERE Id = " & Apphostid)
				If AppHost.GetString("UseHTTPS") = "True" Then
					strUsehttps = "https"
				Else
					strUsehttps = "http"
				End If
				'HostGrp = AppHost.GetString("HostGroup")
				Return strUsehttps + "://" + Eval("MAHostKey") + ".camacloud.com" '--Reomoved Host group from MA url as per the request from Joseph--
			End If
		End If
		Return ""
	End Function
	
	<WebMethod()>
		<ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
	Public Shared Function Getcounty(pre As String, sear As String, proEnv As String) As List(Of String)
		Dim Counties As New List(Of String)()
		Dim conn As New SqlConnection
		conn.ConnectionString = ConfigurationManager.ConnectionStrings("ControlDB").ConnectionString
		Dim cmd As New SqlCommand()
		Dim condition As String
		If proEnv = "1" Then
			condition = " where ( IsProduction = 'true' Or IsProduction = 1 ) "
		Else
			condition = " where 1 = 1 "
		End If
		Dim query As String = "select Name, Id from Organization " + condition + " and " & "Name Like @SearchText + '%'"
		Select Case sear
			Case "3"
				query = "select Name, Id from Vendor where " & "Name like @SearchText + '%'"
			Case "4"
				query = "select Name, Id from camasystem where " & "Name like @SearchText + '%'"
		End Select
		cmd.CommandText = query
		
		cmd.Parameters.AddWithValue("@SearchText", pre)
		cmd.Connection = conn
		conn.Open()
		Dim sdr As SqlDataReader = cmd.ExecuteReader()
		While sdr.Read()
			Counties.Add(sdr("Name"))
		End While
		conn.Close()
		Return Counties
	End Function

    Protected Sub ddlPageSize_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged, ddlOrderBy.SelectedIndexChanged, ddlSettings.SelectedIndexChanged, ddlState.SelectedIndexChanged, ddlCamaSystem.SelectedIndexChanged
        LoadGrid()
    End Sub
    Protected Sub ddlfilter_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlFilter.SelectedIndexChanged
        ddlCamaSystem.SelectedIndex = 0
        ddlSettings.SelectedIndex = 0
        ddlState.SelectedIndex = 0
        LoadGrid()
    End Sub


    Public Overrides Sub VerifyRenderingInServerForm(control As System.Web.UI.Control)
		If control Is pnlExcel Or control Is gvPrint Then
			Return
		End If
		MyBase.VerifyRenderingInServerForm(control)
	End Sub
	Protected Sub ibtnExportToExcel_Click(sender As Object, e As System.EventArgs) Handles ibtnExportToExcel.Click
		Dim sb As New StringBuilder
		Dim tw As New StringWriter(sb)
		Dim hw As New HtmlTextWriter(tw)
		pnlExcel.Visible = True
		pnlExcel.RenderControl(hw)
		Dim html As String = sb.ToString
		pnlExcel.Visible = False
		Dim fileName As String = "ClientAccountList.xlsx"
		Response.Clear()
		Response.Buffer = True
		Response.ClearContent()
		Response.ClearHeaders()
		Response.AddHeader("Content-Disposition", "attachment;filename=" + fileName)
		Response.Charset = ""
		Response.ContentType = "application/vnd.openxml.formats-officedocument.spreadsheetml.sheet"
		Dim dt As DataTable = ViewState("gridData")
		Dim grdv As New GridView()
		Dim headerList As New Dictionary(Of String, String)()
		headerList.Add("CID", "CID")
		headerList.Add("ID", "ID")
		headerList.Add("State", "State")
		headerList.Add("Name", "County Name")
		headerList.Add("camsysName", "CAMA System")
		headerList.Add("vendorName", "Support Vendor")
		headerList.Add("SupportVendor", "Services Vendor")
		headerList.Add("MAHostUrl", "MobileAssessor URL")
		headerList.Add("Hostserver", "Host Server")
		headerList.Add("Created_date", "Created Date")
		headerList.Add("IsProduction", "Production")
		
		For Each keyvalue As KeyValuePair(Of String, String) In headerList
			Dim gridfield As New BoundField()
			gridfield.DataField = keyvalue.Key
			gridfield.HeaderText = keyvalue.Value
			gridfield.ItemStyle.Width = 200
			If keyvalue.Key = "CID" Or keyvalue.Key = "ID" Or keyvalue.Key = "State" Then
				gridfield.ItemStyle.Width = 50
			End If
			If keyvalue.Key = "Created_date" Then
				gridfield.ItemStyle.Width = 90
			End If
			grdv.Columns.Add(gridfield)
		Next
		dt.Columns.Add("CID", GetType(String))
		For Each dr As DataRow In dt.Rows
			dr("CID") = BaseCharMap(dr("Id"))
		Next
		grdv.DataSource = dt
		grdv.DataBind()
		Dim excelStream = ExcelGenerator.ExportGrid(grdv, dt, "Client Account", "Client Account List.")
		excelStream.CopyTo(Response.OutputStream)
		Response.End()
	End Sub
	
	Protected Sub chkProEnv_CheckedChanged(sender As Object, e As EventArgs) Handles chkProEnv.CheckedChanged
		LoadGrid()
	End Sub
End Class