﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ClientAdmin.master"
    CodeBehind="ManageClient.aspx.vb" Inherits="CAMACloud.Admin.EditClient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script src="../App_Static/js/system.js?t=123"></script>
    <style type="text/css">
        

		.ui-autocomplete{
			overflow: auto;
			max-height: 500px;
			}
            
        
    </style>
    <script type="text/javascript">
        $(function () {
            $('#<%=txtCountyName.ClientID%>').autocomplete({
                source: function (request, response) {
					var term = request.term;
                    if (!term) { return false; }
                   $admin( 'getcounty', { pre: term }, function ( data )
                   {
                       var list = []
                       data.forEach( function (d)
                       {
                           list.push(d.Name)
                       } )
                       response( $.map( list, function ( item )
                       {
                           return {
                               value: item
                           }
                       }));
                    },function (XMLHttpRequest, textStatus, errorThrown) {
                        alert(JSON.stringify(XMLHttpRequest));
                    }); 
                },
            });
            
        	$('.SettingsContainer').on('keydown', '.number', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
    	
	    	$('.btnSave').click(function () {
	    	    if ( validation() )
	    	    {
	    	        SaveAdvancedSettings();
	    	    }
	    	} );
	    	
	    	 $('.btnCancel').click(function () {
	    	     $('body').css('overflow', 'scroll');
	    	} );
	    	
	    	

	    	$( '.clearXmlAudit' ).click( function ()
	    	{
	    	    clearAudit();
	    	    return false;
	    	} );

	    	$( '.cbEnableAdvancedMap input' ).click( function ()
	    	{
	    	    if ( this.checked == false )
	    	    {
	    	        $( '.cbUseOSM Input' ).prop( 'checked',false);
	    	        $( '.cbPictometryInMA Input' ).prop( 'checked',false);
	    	    }
	    	} );

        });
        
       function showAdvanceSettings(){
           try {
               var scrollheight = document.documentElement.scrollHeight;
               document.documentElement.scrollTop = 0;
               $('body').css('overflow', 'hidden');
                var orgName = $('#<%= hdnOrganizationName.ClientID%>').val();
                var orgId = $('#<%= hdnOrganizationId.ClientID%>').val();
                $admin('getadvancedsettings',{orgId: orgId},function(data){
						var objdata = data;
						console.log(objdata);
                        AdvancedSettingsDialogBox(orgName + ' - Advanced Settings')
                        $('.ui-widget-overlay').css('height',scrollheight);
                        $('.ui-dialog .ui-dialog-titlebar-close').click(function () {
                            $('.ui-dialog').draggable({revert:'invalid'});
                            $('body').css('overflow', 'auto');
                        });
                    $('.txtMaxPhotosParcel').val(objdata.MaxPhotosPerParcel)
                    $('.txtVendorCDSMail' ).val( objdata.VendorCDSEmail )
                    $( '.cbEnableAdvancedMap Input' ).prop( 'checked', $.parseJSON( objdata.EnableAdvancedMap ) );
                    $( '.cbPictometryInMA' ).closest( "tr" ).toggle( objdata.EnableAdvancedMap.toString() == 'true' && objdata.UseOSM.toString() == 'true' );
                    if (objdata.EnableAdvancedMap.toString() == 'false' || objdata.UseOSM.toString() == 'false' )
                        $( '.cbPictometryInMA Input' ).prop( 'checked', false );
                    else
                        $( '.cbPictometryInMA Input' ).prop( 'checked', $.parseJSON( objdata.EagleViewInMA ));

                    $( '.cbDisto  Input' ).prop( 'checked', $.parseJSON( objdata.DistoEnabled ) );
                    $( '.cbPCITrackingEnabled  Input' ).prop( 'checked', $.parseJSON( objdata.PCITrackingEnabled ) );
                    $( '.cbEnableXMLAuditTrail Input' ).prop( 'checked', $.parseJSON( objdata.EnableXMLAuditTrail ) );
                    $( '.cbEnableFYL Input' ).prop( 'checked', $.parseJSON( objdata.FYLEnabled) );
                    $('.bppEnable Input').prop('checked', $.parseJSON(objdata.bppEnabled));
                    $('.cbInternalView Input').prop('checked', $.parseJSON(objdata.EnableInternalView));
                    $('.PRCReportView Input').prop('checked', $.parseJSON(objdata.PRCReport));
                    
                    enableClearLink();
                    $('.ui-dialog').draggable('destroy');

                    },function (XMLHttpRequest, textStatus, errorThrown) {
                        alert(JSON.stringify(XMLHttpRequest));
                    }); 
            } catch (e) {

            }
            return false;
       }
      function AdvancedSettingsDialogBox(title) { // Function for binding All Release Note dialog box
           $('body').scrollTop(0);
            $('body').css('overflow', 'hidden');
            var dialog = $('.SettingsContainer').dialog({
                modal: true,
                height: 'auto',
                width: 625,
                top: '120.5px !important',
                resizable: false,
                title: title
            });
            //dialog.parent().appendTo(jQuery('form:first')); //Setting the dialog containing Div to the start of the Form tag
      }
		
      function validation()
      {
          var isValid = true;
      		$('.validator').each(function(i,el) {
      			if($(el).hasClass('required')) {
      				if($(el).val() == null || $(el).val() == '') {
      					alert('Please enter a email address.');
      					isValid = false;
      				}
      				/*
	      				if($(el).next(".red").length==0) {
							$(el).after('<span class="red" style="color:Red;font-weight: bolder;font-size: large;position: absolute;">*</span>');
	      				}
	      				isValid = false;
      				}
      				else{
      					$(el).next(".red").remove();
      				}*/
      			}
      			
      			if (isValid && $( el ).hasClass( 'email' ) )
      			{
      			    if ( !ValidateEmail( $( el ).val() ) )
      			    {
      			        alert( 'Please enter a valid email address.' );
      			        isValid = false;
      			    }
      			}
      		});
      		return isValid;
      }
      
      function ValidateEmail( email )
      {
          var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
          return expr.test( email );
      };

  	function SaveAdvancedSettings() {
  	    var orgId= $('#<%= hdnOrganizationId.ClientID%>').val();
  	    var maxPhotosParcel = $('.txtMaxPhotosParcel').val();
        var UserNm = "<%= UserName %>";
  	    var vendorCDSMail = $('.txtVendorCDSMail' ).val();
  	    var enableAdvancedMap = $( '.cbEnableAdvancedMap Input' ).is( ':checked' ) ? 1 : 0
  	    var pictometryInMA = $( '.cbPictometryInMA Input' ).is( ':checked' ) ? 1 : 0
  	    var distoEnabled = $( '.cbDisto Input' ).is( ':checked' ) ? 1 : 0
  	    var pciTrackingEnabled = $( '.cbPCITrackingEnabled Input' ).is( ':checked' ) ? 1 : 0
  	    var enableXMLAuditTrail =$('.cbEnableXMLAuditTrail Input' ).is( ':checked') ? 1: 0 
		var fylEnabled =$('.cbEnableFYL Input' ).is( ':checked') ? 1: 0   
        var bppEnabled = $('.bppEnable Input').is(':checked') ? 1 : 0;
            let cbInternalView = $('.cbInternalView Input').is(':checked') ? 1 : 0;
            var PRCReport = $('.PRCReportView Input').is(':checked') ? 1 : 0;

            $admin('saveadvancedsettings', { orgId: orgId, maxPhotosParcel: maxPhotosParcel, vendorCDSMail: vendorCDSMail, enableAdvancedMap: enableAdvancedMap, pictometryInMA: pictometryInMA, distoEnabled: distoEnabled, FYLEnabled: fylEnabled, pciTrackingEnabled: pciTrackingEnabled, enableXMLAuditTrail: enableXMLAuditTrail, bppEnabled: bppEnabled, cbInternalView: cbInternalView, UserNm: UserNm, PRCReport: PRCReport}, function ( data ) {
			alert( "Advanced Settings saved successfully.. " )
			showAdvanceSettings();
			enableClearLink();
		},function (XMLHttpRequest, textStatus, errorThrown) {
		    alert(JSON.stringify(XMLHttpRequest));
		}); 
  	    return false;
  	}

        function enableClearLink()
        {
            if ( $( '.cbEnableXMLAuditTrail Input' ).is( ':checked' ) )
            {
                $( '.clearXmlAudit' ).attr( 'style', 'display:inline-block;' );
            }
            else
            {
                $( '.clearXmlAudit' ).attr( 'style', 'display:none;' );
             
            }
        }

  	function hidePopup()
  	{
  	    $('.SettingsContainer').dialog( 'close' );
  	    return false;
  	}

        function clearAudit()
        {
            if ( confirm( "Are you sure you want to clear SettingsAuditTrail ?" ) )
            {
                var orgId= $('#<%= hdnOrganizationId.ClientID%>' ).val();
  	         	$admin('clearsettingsaudittrail',{orgId: orgId},function(data){
				 alert( "XML AuditTrail has been successfully cleared. " )
				    	enableClearLink();
				},function (XMLHttpRequest, textStatus, errorThrown) {
				alert(JSON.stringify(XMLHttpRequest));
				});
            }     
        }
        function allowOnlyNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
    </script>
    <style type="text/css">
        .content_tab2{
            margin-left: 8px;
            margin-top: 7px;
            border: 1px solid #bfbdad;
            border-top-left-radius: 4px;
            border-bottom-right-radius: 4px;
            border-bottom-left-radius: 4px;
            border-top-right-radius: 4px;
            height: Auto;
        }
        .content_tab_head2{
            margin-left: 8px;
            margin-right: 0px;
            padding: 7px;
            background: #e0eaf0;
            border: 1px solid #bfbdad;
            border-top-left-radius: 3px 9px;
            border-bottom-right-radius: 3px 9px;
            border-bottom-left-radius: 3px 9px;
            border-top-right-radius: 3px 9px;
        }

        body {
            margin: 0px;
            padding: 0px;
        }

        table td {
            padding: 5px;
        }

        .style1 {
            color: #030303;
        }

        a {
            text-decoration: none;
        }

        .search-panel {
            padding: 7px;
            border: 1px solid #CFCFCF;
            width: 97%;
            background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#CFCFCF), to(#F8F8F8));
        }

            .search-panel label {
                margin-right: 5px;
            }

        .style2 {
            width: 34px;
            padding: 0px !important;
        }
        
        .SettingsContainer { 
            display: none; 
            min-height: 50px  !important;
        }
            
		.advanced {
		    background-image: url(../App_Static/css/icon16/settings.png);
		    background-repeat: no-repeat;
		    background-position: center left 5px;
		    width: 110px;
		    float: right;
		  }
      	.clearXmlAudit{
      		color: #167ccf !important;
      	}
      
      .ui-button-icon-primary.ui-icon.ui-icon-closethick {
		    position: relative !important;
		    margin-top: -8px  !important; 
		    margin-left: -8px  !important;
		} 
      
		.ddlsize{
		    width: 170px;
		}
		.txtsize{
			width: 166px;
		}
		.left-panel{
			padding : 0 ;
		}
		.ui-accordion .ui-accordion-icons {
			padding-left : 0;
	     }
	     .ui-accordion .ui-accordion-header {
		   	padding  : 0
          }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField runat="server" ID="hdnOrganizationName" />
    <asp:HiddenField runat="server" ID="hdnOrganizationId" />
    <div class="search-panel">
        <table width="100%">
            <tr>
                <td style="width: 65%">
                    <label>
                        Select County :
                    </label>
                    <asp:TextBox ID="txtCountyName" runat="server" Width="300px"
                        MaxLength="30" CssClass="textboxAuto"></asp:TextBox>
                    <asp:Button ID="btnGo" runat="server" Text="GO" CssClass="style2" />
                </td>
                <td class="a">
                    <asp:LinkButton ID="lbLicence" runat="server" CommandName="License" Text="License Keys"
                        Style="float: left" />
                    <asp:LinkButton ID="lbAPIAccess" runat="server" CommandName="API Access" Text="API Access Keys" Style="margin-left: 20px;"/>
                    <button class="advanced" OnClick="return showAdvanceSettings();">Advanced</button>
                </td>
            </tr>
        </table>
    </div>
    <table style="width:100%">
        <tr>
            <td>
                <div style="width: 100%; float: left; height: 60px">
                    <h1 runat="server" id="pageTitle">&nbsp;<asp:Label ID="lblClient" runat="server" Text="Label"></asp:Label>
                    </h1>
                    <div style="position: relative; top: -54px;">
                        <asp:Button ID="btnBackupNow" runat="server" Text="Backup Database" Style="margin-left: 5px; float: right" />
                        <asp:Button ID="btnRebuild" runat="server" Text="Rebuild Index" Style="margin-left: 5px; float: right" />
                        <asp:Button ID="btnCreate" runat="server" Text="Create Index" Style="float: right" />
                    </div>

                </div>
            </td>
        </tr>
    </table>
    <div id="tabcontent">
        <table>
            <tr>
                <td class="style1">
                    <div class="content_tab_head2">
                        <strong>Server Configuration </strong>
                        <asp:LinkButton ID="lnkEditServer" runat="server" Text="Edit"
                            Style="float: right" />
                        <asp:HiddenField runat="server" ID="hfEditServer" />
                    </div>
                    <div class="content_tab2">
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                <table class="comparable-edit">
                                    <tr>
                                        <td>Application Group
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblApplicnGroup" runat="server" Text="Label"></asp:Label>
                                            <asp:DropDownList ID="ddlApplicnGroup" class="ddlsize"  runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Host Server
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblHostServer" runat="server" Text="Label"></asp:Label>
                                            <asp:DropDownList ID="ddlHostServer" runat="server"  class="ddlsize" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Database
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblDBName" runat="server" Text="Label"></asp:Label>
                                            <asp:DropDownList ID="ddlDBName"  class="ddlsize"  runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Database Size
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblDatabaseSize" runat="server" Text="Label"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Created Date
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCreatedDate" runat="server" Text="Label"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Demo Host Server
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblDemoHostServr" runat="server" Text="Label"></asp:Label>
                                            <asp:DropDownList ID="ddlDemoHostServr"  class="ddlsize"  runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Demo Database
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblDemoDBname" runat="server" Text="Label"></asp:Label>
                                            <asp:DropDownList ID="ddlDemoDBname"  class="ddlsize"   runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>MA Hostkey
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblMAHostKey" runat="server" Text="Label"></asp:Label>
                                            <asp:TextBox ID="txtMAHostKey" runat="server" class="txtsize" ></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Production</td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>
                                            <asp:Label runat ="server" ID="lblProEnv"></asp:Label>
                                            <asp:RadioButton ID="ProductionEnvYes" GroupName="ProductionEnv" Text="Yes" Value="1"  runat="server"  />
  											<asp:RadioButton ID="ProductionEnvNo" GroupName="ProductionEnv" Text="No" Value="0" runat="server"  />
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td>Stage
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblStage" runat="server" Text="Label"></asp:Label>
                                            <asp:DropDownList ID="ddlStage"  class="ddlStage"  runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div> 
                    <div class="content_tab_head2" style="margin-top: 10px">
                        <strong>Backup Schedule</strong>
                    </div>
                    <div class="content_tab2">
                        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                            <ContentTemplate>
                                <table class="comparable-edit" style="width: 100%">
                                    <tr>
                                        <td style="width: 130px">Last Backup Time
                                        </td>

                                        <td>
                                            <strong>:</strong>
                                        </td>

                                        <td>
                                            <asp:Label ID="lblLastBackupTime" runat="server" Text="Label"></asp:Label>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>Time Taken
                                        </td>

                                        <td>
                                            <strong>:</strong>
                                        </td>

                                        <td>
                                            <asp:Label ID="lblTimeTaken" runat="server" Text="Label"></asp:Label>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>Backup Size
                                        </td>

                                        <td>
                                            <strong>:</strong>
                                        </td>

                                        <td>
                                            <asp:Label ID="lblSize" runat="server" Text="Label"></asp:Label>
                                        </td>

                                    </tr>
                                    <tr id="trHideLocation" runat="server">
                                        <td colspan="3">Location <strong>:</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                               <asp:TextBox ID="txtLocation" TextMode="MultiLine" Enabled="false" Rows="2" style="overflow:auto;width: 98%; resize:none;" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="trHideSchedule" runat="server">
                                        <td colspan="3" align="center">
                                            <strong>Schedule Plan</strong>
                                            <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView ID="gvExecutionPlan" runat="server" ShowHeaderWhenEmpty="True" AutoGenerateColumns="False"
                                                        ShowFooter="True" Width="100%">
                                                        <RowStyle Height="20px" />
                                                        <Columns>
                                                            <asp:BoundField DataField="BackupId" ReadOnly="True" SortExpression="BackupId" ShowHeader="False"
                                                                Visible="False" />
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:HiddenField ID="hfBackupId" runat="server" Value='<%# Bind("BackupId")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Repeat">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="gvlblRepeatFactor" runat="server" Text='<%# Bind("RepeatFactor")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:DropDownList ID="gvddlRepeatFactor" AppendDataBoundItems="True" runat="server">
                                                                    </asp:DropDownList>
                                                                </EditItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:DropDownList ID="gvddlAddRepeatFactor" runat="server">
                                                                    </asp:DropDownList>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Day">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="gvlblScheduleDay" runat="server" Text='<%# Bind("ScheduleDay")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:DropDownList ID="gvddlScheduleDay" AppendDataBoundItems="True" runat="server">
                                                                    </asp:DropDownList>
                                                                </EditItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:DropDownList ID="gvddlAddScheduleDay" runat="server">
                                                                    </asp:DropDownList>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Time">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="gvlblScheduleTime" runat="server" Text='<%# Bind("ScheduleTime")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:DropDownList ID="gvddlScheduleTime" AppendDataBoundItems="True" runat="server">
                                                                    </asp:DropDownList>
                                                                </EditItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:DropDownList ID="gvddlAddScheduleTime" runat="server">
                                                                    </asp:DropDownList>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField ShowHeader="False">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="EditButton" runat="server" CommandName="Edit" AlternateText="Edit" Visible='<%# IIf(Eval("BackupId") Is DBNull.Value, "False", "True") %>'
                                                                        ImageUrl="~/App_Static/css/icon16/edit.png" />
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:LinkButton ID="lnkUpdate" runat="server" CausesValidation="True"
                                                                        CommandName="Update" Text="Update"></asp:LinkButton>
                                                                    &nbsp;<asp:LinkButton ID="lnkCancel" runat="server" CausesValidation="False"
                                                                        CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                                </EditItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField ShowHeader="False">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="DeleteButton" runat="server" ImageUrl="~/App_Static/css/icon16/delete.png"
                                                                        CommandName="Delete" OnClientClick="return confirm('Are you sure you want to delete this event?');" Visible='<%# IIf(Eval("BackupId") Is DBNull.Value, "False", "True") %>'
                                                                        AlternateText="Delete" />
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:LinkButton ID="lnkAdd" runat="server" CommandName="ADD" Text="Add"></asp:LinkButton>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                        </td>
                                    </tr>
                                </table>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </td>

                <td class="style1">
                    <div class="content_tab_head2">
                        <strong>Client Location</strong>
                        <asp:LinkButton ID="lnkEditLoc" runat="server" Text="Edit"
                            Style="float: right" />
                        <asp:HiddenField runat="server" ID="hfEditLoc" />
                    </div>
                    <div class="content_tab2">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <table class="comparable-edit" style="width:100%">
                                    <tr>
                                        <td>
                                            CID
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCID" runat="server" Text="Label"></asp:Label>
                                        </td
                                    </tr>
                                    <tr>
                                        <td>
                                            Organization Id
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblOrganiznId" runat="server" Text="Label"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            County
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCounty" runat="server" Text="Label"></asp:Label>
                                            <asp:TextBox ID="txtCounty" runat="server" class="txtsize" ></asp:TextBox>
                                        </td>
                                    </tr>
                                     <tr>
                                        <td>
                                            Name
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblName" runat="server" Text="Label"></asp:Label>
                                            <asp:TextBox ID="txtName" runat="server" class="txtsize" ></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            City
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCity" runat="server" Text="Label"></asp:Label>
                                            <asp:TextBox ID="txtCity" runat="server" class="txtsize" ></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            State
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblState" runat="server" Text="Label"></asp:Label>
                                            <asp:TextBox ID="txtState" runat="server" class="txtsize" ></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="content_tab_head2" style="margin-top: 10px">
                        <strong>Client URLs </strong>
                    </div>
                    <div class="content_tab2">
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <table class="comparable-edit" style="width:100%">
                                    <tr>
                                        <td>API
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>

                                            <asp:Label ID="lblAPI" runat="server" Text="Label"></asp:Label>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>MA
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblMA" runat="server" Text="Label"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>CC
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCC" runat="server" Text="Label"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                      <div class="content_tab_head2" style="margin-top: 10px">
                        <strong>Vendor Information</strong>
                        <asp:LinkButton ID="lnkEditVendor" runat="server" Text="Edit"
                            Style="float: right" />
                        <asp:HiddenField runat="server" ID="hfEditVendor" />
                    </div>
                    <div class="content_tab2">
                        <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                            <ContentTemplate>
                                <table class="comparable-edit" style="width: 100%">
                                    <tr>
                                        <td>Vendor:
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblVendorID" runat="server" Text="Label"></asp:Label>
                                            <asp:DropDownList ID="ddlVendorID" runat="server"  class="ddlsize"  AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Support Vendor:
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblSupportVendor" runat="server" Text="Label"></asp:Label>
                                            <asp:DropDownList ID="ddlSupportVendorID" runat="server"  class="ddlsize"  AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>CAMASystem
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCAMASystem" runat="server" Text="Label"></asp:Label>
                                            <asp:DropDownList ID="ddlCAMASystem" class="ddlsize" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">Notes <strong>:</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <asp:TextBox ID="txtNotes" TextMode="MultiLine" Rows="6" style="overflow:auto;width: 98%; resize:none;" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </td>


                <td class="style1">
                <div class="content_tab_head2">
                        <strong>Cloud Data Statistics </strong>
                    </div>
                    <div class="content_tab2">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <table class="comparable-edit" style="width:100%">
                                    <tr>
                                        <td>Total Parcels
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td class="style11">
                                            <asp:Label ID="lblTotParcels" runat="server" Text="Label"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Total Assignment Groups
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblTotal" runat="server" Text="Label"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <%--<asp:LinkButton ID="lnkUsers" runat="server"  Text="Total Users"></asp:LinkButton>--%> 
                                            Total Users                                          
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblUsers" runat="server" Text="Label"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Online Users
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblOnlineUsers" runat="server" Text="Label"></asp:Label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Last Index Rebuild
                                        </td>
                                        <td>
                                            <strong>:</strong>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblRebuildIndex" runat="server" Text="Label"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="content_tab_head2" style="margin-top: 10px">
                        <strong>Application Roles</strong>
                        <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit"
                            Style="float: right" />
                        <asp:HiddenField runat="server" ID="hfEdit" />
                    </div>
                    <div class="content_tab2">
                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                            <ContentTemplate>
                                <table class="comparable-edit">
                                    <asp:Repeater runat="server" ID="rptRoles">
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox runat="server" ID ="chkRole" Text='<%#Eval("Name")%>' Width="200px" Enabled="false" />
                                                    <asp:HiddenField runat="server" ID ="hdnRole" Value ='<%#Eval("Id")%>'/>
                                                </td>
                                                <td>
                                                    <asp:TextBox runat="server" ID="txtSeat" Text='<%#Eval("SeatCount") %>'  Width="50px" Enabled="false" onkeypress="return allowOnlyNumber(event);" MaxLength="6" onpaste="return false"></asp:TextBox>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </ItemTemplate>

                                    </asp:Repeater>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </td>
               </tr>
        </table>
    </div>
        <div class="SettingsContainer">
        <table>
            <tr>
                <td style="margin-left: 8px; width: 330px;">
                    <table>
                        <tr>
                            <td>Max Photos/Parcel: </td>
                            <td>
                                <asp:TextBox ID="txtMaxPhotosParcel" Class="txtMaxPhotosParcel validator number" runat="server" autocomplete="off"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>Vendor-CDS Email: </td>
                            <td>
                                <asp:TextBox ID="txtVendorCDSMail" Class="txtVendorCDSMail validator required email" runat="server" autocomplete="off"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="v-split" style="width: 12px;"></td>
                <td style="width: 200px;">
                    <table>
                        <tr>
                            <td>
                                <asp:CheckBox ID="cbEnableAdvancedMap1" Class="cbEnableAdvancedMap" Text="Advanced Map" Checked="true" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="cbPictometryInMA" Class="cbPictometryInMA" Text="Enable EagleView in MA" Checked="true" runat="server" />
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <asp:CheckBox ID="cbDisto"  Class="cbDisto" Text="Disto" runat="server"  />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <asp:CheckBox ID="cbPCITrackingEnabled" Class="cbPCITrackingEnabled"     Text="Enable PCI Field Tracking" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="cbEnableXMLAuditTrail"  Class="cbEnableXMLAuditTrail" Text="XML AuditTrail" runat="server"  />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="cbEnableFYL"  Class="cbEnableFYL" Text="FYL" runat="server"  />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="bppEnable"  Class="bppEnable" Text="Personal Property" runat="server"  />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="cbInternalView"  Class="cbInternalView" Text="Internal View" runat="server"  />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="PRCReport"  Class="PRCReportView" Text="PRC Report" runat="server"  />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                    <a href="#" class="clearXmlAudit" >Clear XML AuditTrail</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="margin-top: 10px;">
                <td colspan="3" style="text-align:right;">
                    <div id="btnContainer" style="margin-top: 10px; margin-bottom: 15px;">
                        <asp:Button runat="server" ID="btnSave" CssClass="btnSave" Text="Save" />
                        <asp:Button runat="server" ID="btnCancel" CssClass="btnCancel" OnClientClick="return hidePopup()" Text="Cancel" />
                    </div>
                </td>
            </tr>
        </table>
    </div> 
</asp:Content>
