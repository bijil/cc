﻿Public Class manageclient
    Inherits System.Web.UI.Page

    'http://localhost:302/clientaccounts/manageclient.aspx?ouid=9269c834-846d-4d4e-b299-3f0e9faca6c9

    ReadOnly Property OrganizationId As Integer
        Get
            If hdnOrganizationId.Value = "" Then
                If Request("ouid") Is Nothing Then
                    Response.Redirect("~/clientaccounts/list.aspx")
                Else
                    hdnOrganizationId.Value = Database.System.GetIntegerValueOrInvalid("SELECT ID FROM Organization WHERE UID = {0}".SqlFormatString(Request("ouid")))
                End If
                If hdnOrganizationId.Value.ToString = "-1" Then
                    Response.Redirect("~/clientaccounts/list.aspx")
                    Response.End()
                End If
            End If
                Return hdnOrganizationId.Value
        End Get
    End Property

    Private _tenantDb As Database
    Public ReadOnly Property TenantDatabase As Database
        Get
            If _tenantDb Is Nothing Then
                _tenantDb = New Database(Database.GetConnectionStringFromOrganization(OrganizationId))
            End If
            Return _tenantDb
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'ddlHostServer.FillFromSqlWithDatabase(Database.System, "SELECT HostName FROM CAMACloudHost", True, "-- Default Host --")
            LoadOrganization()
        End If
    End Sub

    Sub LoadOrganization()
        Dim o As DataRow = Database.System.GetTopRow("SELECT * FROM Organization WHERE Id = " & OrganizationId)
        clientTitle.InnerHtml = o.GetString("Name")
        txtName.Text = o.GetString("Name")
        txtCity.Text = o.GetString("City")
        txtState.Text = o.GetString("State")
        txtCounty.Text = o.GetString("County")
        ddlHostServer.SelectedValue = o.GetString("DBHost")
        txtMAHostKey.Text = o.GetString("MAHostKey")

        gridModules.DataSource = Database.System.GetDataTable("SELECT ar.Id As RoleId, ar.Name, CAST(CASE WHEN ol.Id IS NULL THEN 0 ELSE 1 END AS BIT) As Selected FROM AppRoles ar LEFT OUTER JOIN OrganizationRoles ol ON ar.Id = ol.RoleId AND ol.OrganizationId = " & OrganizationId)
        gridModules.DataBind()

        txtMaxMAParcels.Text = TenantDatabase.GetIntegerValue("SELECT dbo.GetParcelDownloadMaxCount()")
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click


        For Each mr As GridViewRow In gridModules.Rows
            Dim roleId As Integer = mr.GetHiddenValue("MID")
            Dim sql As String
            If mr.GetChecked("Selected") Then
                sql = "INSERT INTO OrganizationRoles SELECT {0}, Id FROM AppRoles WHERE Id NOT IN (SELECT RoleId FROM OrganizationRoles WHERE OrganizationId = {0}) AND Id = {1}"
            Else
                sql = "DELETE FROM OrganizationRoles WHERE OrganizationId = {0} AND RoleId = {1}"
            End If
            Database.System.Execute(sql, OrganizationId, roleId)
        Next

        Dim maxMAParcels As Integer
        If Integer.TryParse(txtMaxMAParcels.Text, maxMAParcels) Then
            TenantDatabase.Execute("DROP FUNCTION GetParcelDownloadMaxCount")
            TenantDatabase.Execute("CREATE FUNCTION GetParcelDownloadMaxCount() RETURNS INT BEGIN RETURN " & maxMAParcels & " END")
        End If

        LoadOrganization()
    End Sub
End Class