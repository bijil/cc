﻿Imports System.IO
Imports System.Drawing
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Web.Services

Public Class EnvironmentAuditTrail
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then

            Dim dummy As DataTable = New DataTable()
            dummy.Columns.Add("Id")
            dummy.Columns.Add("EventTime")
            dummy.Columns.Add("LoginId")
            dummy.Columns.Add("County")
            dummy.Columns.Add("Description")

            dummy.Rows.Add()
            gvSettingsAuditTrail.DataSource = dummy
            gvSettingsAuditTrail.DataBind()

            'Required for jQuery DataTables to work.
            gvSettingsAuditTrail.UseAccessibleHeader = True
            gvSettingsAuditTrail.HeaderRow.TableSection = TableRowSection.TableHeader

        End If
    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub

    Public Class satReportRecord

        Public Property Id As Integer
        Public Property EventTime As DateTime
        Public Property LoginId As String
        Public Property County As String
        Public Property Description As String

    End Class


    <WebMethod>
    Public Shared Function GetsatReport() As List(Of satReportRecord)
        Dim settingaudittrailrecord As List(Of satReportRecord) = New List(Of satReportRecord)()


        Dim dt As DataTable = Database.System.GetDataTable("SELECT Id,EventTime,LoginId,County,Description FROM AdminSettingsAuditTrail")

        For Each q In dt.Rows

            settingaudittrailrecord.Add(New satReportRecord With {
                            .Id = q("Id"),
                            .EventTime = q("EventTime"),
                            .LoginId = q("LoginId").ToString(),
                            .County = q("County"),
                            .Description = q("Description").ToString()
                        })
        Next

        Return settingaudittrailrecord
    End Function

End Class