﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ClientAdmin.master" CodeBehind="User.aspx.vb" Inherits="CAMACloud.Admin.User" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
    function showPopup(title) {
        $('.user-edit-panel').html('');
        $('.edit-popup').dialog({
            modal: true,
            width: 825,
            height: 455,
            resizable: false,
            title: title,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            }
        });
    }

    function hidePopup() {
        $('.edit-popup').dialog('close');
    }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<h1>
        Manage Users
        -&nbsp;
        <asp:Label ID="lblClient" runat="server" Text="Label"></asp:Label>
    </h1>
    <asp:UpdatePanel runat="server" ID="uplGrid">
        <ContentTemplate>
            <div class="link-panel">
                <asp:LinkButton runat="server" ID="lbNewUser" Text="Add New User" OnClientClick="showPopup('Create new CAMA user');" />
            </div>
            <asp:GridView ID="gdvUserDetails" runat="server" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField>
                        <ItemStyle Width="25px" />
                        <ItemTemplate>
                            <%# Container.DataItemIndex + 1 %>.
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Full Name">
                        <ItemStyle Width="200px" />
                        <ItemTemplate>
                            <%# Eval("FirstName") + " " + Eval("LastName")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Login Id" DataField="LoginId">
                        <ItemStyle Width="120px"></ItemStyle>
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Status">
                        <ItemStyle Width="80px" />
                        <ItemTemplate>
                            <%#UserStatus() %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Last Login">
                        <ItemStyle Width="160px" />
                        <ItemTemplate>
                            <%#UserLastLoginDate %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Last Login IP" ItemStyle-Width="120px" Visible="false">
                        <ItemTemplate>
                            <%# Eval("LoginId")%>
                        </ItemTemplate>
                        <ItemStyle Width="120px"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemStyle Width="140px" />
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EditUser" Text="Edit" CommandArgument='<%# Eval("LoginId") %>' OnClientClick="showPopup('Edit User Properties')" />
                            <asp:LinkButton ID="lbtnDeleteUser" Text="Delete" runat="server" CommandName="DeleteUser" CommandArgument='<%# Eval("LoginId")%>' CausesValidation="false"></asp:LinkButton>
                            <asp:LinkButton ID="lbtnUnlock" Text="Unlock" runat="server" CommandName="UnlockUser" CommandArgument='<%# Eval("LoginId")%>' CausesValidation="false" Visible='<%#IsUserLocked() %>'></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="edit-popup" style="display: none;">
        <asp:UpdatePanel ID="uplEdit" runat="server">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlUserEdit" CssClass="user-edit-panel" Style="width: 800px; height: 410px;">
                    <asp:HiddenField ID="hdnUserId" runat="server" />
                    <table class="user-edit-table">
                        <tr>
                            <td style="width: 8px;"></td>
                            <td>
                                <h3>
                                    User Properties</h3>
                                <table class="user-edit-form">
                                    <tr>
                                        <td>First Name: </td>
                                        <td>
                                            <asp:TextBox ID="txtFirstName" runat="server" autocomplete="off"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtFirstName" runat="server" ForeColor="Red" Display="Dynamic" ErrorMessage="*" ControlToValidate="txtFirstName"
                                                ValidationGroup="UserProp"></asp:RequiredFieldValidator>
                                            <%-- <asp:RequiredFieldValidator
                                                ID="rfvtxtFirstNameForEdit" runat="server" ForeColor="Red" Display="Dynamic" ErrorMessage="*" ControlToValidate="txtFirstName"
                                                ValidationGroup="UserProp2"></asp:RequiredFieldValidator>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Middle Name </td>
                                        <td>
                                            <asp:TextBox ID="txtMiddleName" runat="server"></asp:TextBox>
                                            <%--<asp:RequiredFieldValidator
												ID="MiddleNameValidator" runat="server" ForeColor="Red" ErrorMessage="*" ControlToValidate="txtMiddleName"
												ValidationGroup="UserProp"></asp:RequiredFieldValidator>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Last Name </td>
                                        <td>
                                            <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Email: </td>
                                        <td>
                                            <asp:TextBox ID="txtUserMail" runat="server" autocomplete="off"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtUserMail" runat="server" ForeColor="Red" ErrorMessage="*" ControlToValidate="txtUserMail" Display="Dynamic"
                                                ValidationGroup="UserProp"></asp:RequiredFieldValidator>
                                            <%--  <asp:RequiredFieldValidator
                                                ID="RequiredFieldValidator3" runat="server" ForeColor="Red" ErrorMessage="*"
                                                ControlToValidate="txtUserMail" Display="Dynamic" ValidationGroup="UserProp2"></asp:RequiredFieldValidator>--%>
                                            <asp:RegularExpressionValidator ID="revtxtUserMail" runat="server" ControlToValidate="txtUserMail" ErrorMessage="Invalid Email!" ForeColor="#D20B0E"
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic" ValidationGroup="UserProp"></asp:RegularExpressionValidator>
                                            <%--  <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtUserMail"
                                                ErrorMessage="Invalid Email!" ForeColor="#D20B0E" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                Display="Dynamic" ValidationGroup="UserProp2"></asp:RegularExpressionValidator>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Mobile Number </td>
                                        <td>
                                            <asp:TextBox ID="txtMobileNumber" runat="server"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="revtxtMobileNumber" runat="server" ForeColor="#D20B0E" ControlToValidate="txtMobileNumber" ErrorMessage="Invalid MobileNo"
                                                ValidationExpression="\d{10,11}" ValidationGroup="UserProp" Display="Dynamic"></asp:RegularExpressionValidator>
                                           <%-- <asp:RequiredFieldValidator ID="rfvtxtMobileNumber" runat="server" ForeColor="Red" ErrorMessage="*" ControlToValidate="txtMobileNumber" Display="Dynamic"
                                                ValidationGroup="UserProp"></asp:RequiredFieldValidator>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Mobile Carrier </td>
                                        <td>
                                            <asp:DropDownList ID="ddlMobileCarrier" runat="server" Width="188px" Height="22px">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rfvddlMobileCarrier" runat="server" ForeColor="Red" ErrorMessage="*" ControlToValidate="ddlMobileCarrier" Display="Dynamic"
                                                InitialValue="--Select--" ValidationGroup="UserProp"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Login ID: </td>
                                        <td>
                                            <asp:TextBox ID="txtLoginId" runat="server" autocomplete="off"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="UserNameRequiredFieldValidator" runat="server" ForeColor="Red" ErrorMessage="*" ControlToValidate="txtLoginId"
                                                Display="Dynamic" ValidationGroup="UserProp"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Password: </td>
                                        <td>
                                            <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" autocomplete="off"></asp:TextBox><asp:RequiredFieldValidator ID="rfvPassword"
                                                runat="server" ForeColor="Red" ErrorMessage="*" ControlToValidate="txtPassword" Display="Dynamic" ValidationGroup="UserProp"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Confirm Password: </td>
                                        <td>
                                            <asp:TextBox ID="txtPassword2" TextMode="Password" runat="server" autocomplete="off"></asp:TextBox>
                                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassword" ControlToValidate="txtPassword2" ErrorMessage="Mismatch!"
                                                Display="Dynamic" ForeColor="#D20B0E" ValidationGroup="UserProp"></asp:CompareValidator>
                                        </td>
                                    </tr>
                                </table>
                                <div style="margin-top: 10px; margin-bottom: 15px;">
                                    <asp:Button runat="server" ID="btnSaveUser" Text="Save User" ValidationGroup="UserProp2" />
                                    <asp:Button runat="server" ID="btnCancel" Text="Cancel" />
                                </div>
                            </td>
                            <td class="v-split" style="width: 12px;"></td>
                            <td style="width: 300px;">
                                <table>
                                    <tr>
                                        <td>
                                            <h3>
                                                Select Roles</h3>
                                            <asp:CheckBoxList ID="cblRoles" runat="server" Style="margin-left: -3px">
                                            </asp:CheckBoxList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="margin-top: 20px;">
                                            <h3>
                                                Other Options</h3>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="cbNotifybyEmail" Text="Send Notification Via Email" Checked="true" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="cbNotifybySMS" Text="Send Notification Via SMS" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="cbAutoQC" Text="Compliance For Auto-QC" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chkLocked" Text="User access is locked" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

</asp:Content>
