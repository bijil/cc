﻿Public Class reports_Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadReportsHome()
            rfv_ddlReport.ErrorMessage = " *"
            pnl_nofile.Visible = False
            If Not IsNothing(Page.PreviousPage) Then
                ddlReport.SelectedValue = 1
                GetReportParameters(1)
            End If
        Else
            Dim sid As String = ddlReport.SelectedValue
            If ViewState("reportid") IsNot Nothing AndAlso ViewState("reportid") <> sid Then
                Dim wc As WebControl = ph.FindControl("report_filter_Vendor")
                Dim wc1 As WebControl = ph.FindControl("report_filter_Org")
                Dim wc4 As WebControl = ph.FindControl("report_filter_State")
                If wc IsNot Nothing And wc1 IsNot Nothing Then
                    CType(wc, DropDownList).FillFromSqlWithDatabase(Database.System, "SELECT  Id, Name FROM Vendor ORDER BY Name", True)
                    CType(wc1, DropDownList).FillFromSqlWithDatabase(Database.System, "SELECT Id,Name FROM Organization ORDER BY Name", True)
                    CType(wc4, DropDownList).FillFromSqlWithDatabase(Database.System, "SELECT Distinct State FROM Organization where State <> '*'", True)
                    CType(wc, DropDownList).ClearSelection()
                    CType(wc1, DropDownList).ClearSelection()
                    CType(wc4, DropDownList).ClearSelection()
                End If
                Dim wc3 As WebControl = ph.FindControl("report_filter_TimeSelector")
                If wc3 IsNot Nothing Then
                    AddHandler CType(wc3, DropDownList).SelectedIndexChanged, AddressOf ddl_SelectedIndexChanged
                End If
                ViewState("reportid") = sid
            Else
                ViewState("reportid") = sid
            End If
            Dim target As String = Request.Params("__EVENTTARGET").ToString()
            If target = "ctl00$LeftContent$report_filter_Vendor" Then
                Dim wc As WebControl = ph.FindControl("report_filter_Vendor")
                AddHandler CType(wc, DropDownList).SelectedIndexChanged, AddressOf ddl_SelectedIndexChanged
            End If
            If target = "ctl00$LeftContent$report_filter_TimeSelector" Then
                Dim wc1 As WebControl = ph.FindControl("report_filter_TimeSelector")
                AddHandler CType(wc1, DropDownList).SelectedIndexChanged, AddressOf ddl_SelectedIndexChanged
            End If
            If target = "ctl00$MainContent$Uc_CustomReport$gvCustomReport" Then
                Dim wc As WebControl = ph.FindControl("report_filter_Vendor")
                Dim wc1 As WebControl = ph.FindControl("report_filter_TimeSelector")
                RetainDropdownValues(wc)
                If wc1 IsNot Nothing Then
                    RetainDropdownValues(wc1)
                End If
            End If
        End If
        If ddlReport.SelectedIndex > 0 And HasFilters.Value = "1" Then
            reportParams.Visible = True
            lblReportParameters.Visible = True
        Else
            reportParams.Visible = False
            lblReportParameters.Visible = False
        End If

    End Sub
    Protected Sub btnGenerate_Click(sender As Object, e As System.EventArgs) Handles btnGenerate.Click
        LoadReport(ddlReport.SelectedValue)
        Uc_CustomReport.showReport()
        Dim wcn As WebControl = ph.FindControl("report_filter_TimeSelector")
        Dim wcn1 As WebControl = ph.FindControl("report_filter_Vendor")
        If wcn IsNot Nothing Then
            RetainDropdownValues(wcn)
        End If
        If wcn1 IsNot Nothing Then
            RetainDropdownValues(wcn1)
        End If
    End Sub


    Private Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        If Not IsPostBack Then
            LoadReportSelection()
            ph.Controls.Clear()
            reportParams.Visible = False
            lblReportParameters.Visible = False
            pnl_nofile.Visible = False
        Else
            reportParams.Visible = True
            lblReportParameters.Visible = True
        End If
        Dim srid As String = Request("ctl00$LeftContent$ddlReport")
        If srid Is Nothing OrElse srid = "" Then
            ph.Controls.Clear()
            ph.Controls.Add(New LiteralControl("No parameters"))
            reportParams.Visible = False
            lblReportParameters.Visible = False
        Else
            ph.Controls.Clear()
            ph.EnableViewState = "false"
            reportParams.Visible = True
            lblReportParameters.Visible = True
            Dim rid As Integer = srid
            Dim reportType As String
            Dim response As XElement = XElement.Load(Server.MapPath("config/Reports.xml"))
            Dim temp = From uu In response.Elements("Reports") From yy In uu.Elements("Id") Where yy.Value = rid.ToString Select uu
            reportType = temp.Elements("ReportType").Value
            If (reportType = "Custom") Then
                rid = Request("ctl00$LeftContent$ddlReport")
                GetReportParameters(rid)
            End If
        End If
    End Sub

    Sub LoadReportSelection()
        Dim response As XElement = XElement.Load(Server.MapPath("config/Reports.xml"))
        Dim temp = From uu In response.Descendants("Reports") Where uu.Element("RoleId") = "2" Select Name = uu.Element("Name").Value, ID = uu.Element("Id").Value, Description = uu.Element("Description").Value.Replace("\n", "<br />").Replace("\b", "<b>").Replace("/b", "</b>")
        ddlReport.DataSource = temp.ToList()
        ddlReport.DataTextField = "Name"
        ddlReport.DataValueField = "ID"
        ddlReport.DataBind()
        ddlReport.Items.Insert(0, New ListItem("-- Select --", ""))
    End Sub
    Sub GetReportParameters(rid As String)
        ph.Controls.Clear()
        ph.EnableViewState = "false"
        reportParams.Visible = True
        lblReportParameters.Visible = True
        Dim reportType As String
        Dim response As XElement = XElement.Load(Server.MapPath("config/Reports.xml"))
        Dim temp = From uu In response.Elements("Reports") From yy In uu.Elements("Id") Where yy.Value = rid.ToString Select uu
        reportType = temp.Elements("ReportType").Value
        Dim params As XElement = XElement.Load(Server.MapPath("config/ReportParameters.xml"))
        Dim temp2 = From uu In params.Elements("ReportParameters") From yy In uu.Elements("ReportId") Where yy.Value = rid.ToString Select uu
        For Each xe As XElement In temp2
            Dim name As String = xe.Element("Name")
            Dim label As String = xe.Element("Label")
            Dim inputType As Integer = xe.Element("InputType")
            Dim source As String = xe.Element("Source")
            Dim required As String = xe.Element("Required")
            Dim Validationfield As String = xe.Element("Validationfield")
            Dim ValidationOperator As String = xe.Element("ValidationOperator")
            Dim ip As Panel = GetInputControl(name, label, inputType, source, Validationfield, ValidationOperator, required, rid)
            ph.Controls.Add(ip)
        Next
        If temp2.Count = 0 Then
            ph.Controls.Add(New LiteralControl("No parameters"))
            HasFilters.Value = 0
        Else
            HasFilters.Value = 1
        End If
    End Sub
    Function GetInputControl(name As String, label As String, type As Integer, source As String, ByVal Validationfield As String, ByVal ValidationOperator As String, Optional mandatory As Boolean = False, Optional reportId As String = Nothing) As Panel
        Dim container As New Panel
        Dim labelContainer As New Panel
        Dim labelSpan As New Label
        labelSpan.Text = label
        labelSpan.ID = "lbl_" + name
        labelSpan.CssClass = "leftSpan"
        labelContainer.Controls.Add(labelSpan)
        If name = "TimeFrame" Then
            labelContainer.ID = "div_lb_TimeFrame"
            Dim lblSapan As New Label
            lblSapan.Text = "Year"
            lblSapan.CssClass = "rightSpan"
            lblSapan.ID = "lbl_year"
            labelContainer.Controls.Add(lblSapan)
        End If
        labelContainer.CssClass = "b"
        container.Controls.Add(labelContainer)

        Dim c As WebControl = New TextBox
        Dim d As WebControl = New DropDownList()
        Select Case type
            Case 0
                c = New TextBox
                c.Width = New Unit(214)
            Case 1
                c = New DropDownList
                If name = "Year" Then
                    CType(c, DropDownList).FillNumbers(2013, DateAndTime.Year(Now), 1, "{0}", 0, True, "-- Select --")
                Else
                    CType(c, DropDownList).FillFromSqlWithDatabase(Database.System, source, True)
                End If
                c.Width = New Unit(220)
            Case 2
                c = New TextBox
                c.Attributes.Add("type", "date")
                c.Width = New Unit(214)
                Dim validator As String = "dateValidate(LeftContent_report_filter_" + name + ")"
                c.Attributes.Add("onblur", validator)
            Case 3
                c = New CheckBox
                CType(c, CheckBox).Text = label
                labelContainer.Visible = False
                c.Width = New Unit(220)
                mandatory = False
            Case 4
                c = New RadioButtonList
                CType(c, RadioButtonList).Items.Add("Date Ascending")
                CType(c, RadioButtonList).Items.Add("Date Descending")
                c.Width = New Unit(220)
            Case 6
                c = New DropDownList()
                CType(c, DropDownList).AutoPostBack = True
                CType(c, DropDownList).FillFromSqlWithDatabase(Database.System, source, True)
                c.Width = New Unit(220)
            Case 7
                c = New DropDownList
                CType(c, DropDownList).FillFromSqlWithDatabase(Database.System, source, True)
                c.Attributes("style") = [String].Format("width:40%")
        End Select
        c.ID = "report_filter_" + name
        Dim inputContainer As New Panel
        inputContainer.Controls.Add(c)
        If name = "TimeFrame" Then
            inputContainer.ID = "div_TimeFrame"
            CType(d, DropDownList).FillNumbers(2013, DateAndTime.Year(Now), 1, "{0}", 0, True, "-- Select --")
            d.ID = "report_filter_Year"
            d.Attributes("style") = [String].Format("margin-left: 5px;width:40%")
            inputContainer.Controls.Add(d)
        End If


        If mandatory Then
            inputContainer.Controls.Add(New LiteralControl(" "))
            Dim rfv As New RequiredFieldValidator
            rfv.ControlToValidate = c.ID
            rfv.ValidationGroup = "reportquery"
            inputContainer.Controls.Add(rfv)
        End If
        If Validationfield <> "" Then
            inputContainer.Controls.Add(New LiteralControl(" "))
            Dim cfv As New CompareValidator
            cfv.ControlToValidate = c.ID
            cfv.Operator = GetValidationType(ValidationOperator)
            cfv.ControlToCompare = "report_filter_" + Validationfield
            cfv.ValidationGroup = "reportquery"
            cfv.ErrorMessage = "Please check dates"
            cfv.ForeColor = Drawing.Color.Red
            inputContainer.Controls.Add(cfv)
        End If

        container.Controls.Add(inputContainer)

        container.CssClass = "report-filter-container"
        Return container
    End Function
    Private Function GetValidationType(ByVal type As String) As Integer
        Dim value As Integer
        If type = "=" Then
            value = 0
        ElseIf type = ">" Then
            value = 2
        ElseIf type = "<" Then
            value = 4
        ElseIf type = ">=" Then
            value = 3
        ElseIf type = "<=" Then
            value = 5
        Else : value = 1
        End If
        Return value
    End Function


    Protected Sub ddl_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim ddl As DropDownList = DirectCast(sender, DropDownList)
        Dim element = ddl.ID
        If element = "report_filter_Vendor" Then
        	Dim fc As WebControl = ph.FindControl("report_filter_Org")
            Dim fc1 As WebControl = ph.FindControl("report_filter_State")
            Dim fc2 As WebControl = ph.FindControl("report_filter_CamaSystem")
            CType(fc2, DropDownList).FillFromSqlWithDatabase(Database.System, "SELECT Id,Name FROM CAMASystem WHERE VendorId =" + ddl.SelectedValue.ToString() + " ORDER BY Name", True)
            CType(fc, DropDownList).FillFromSqlWithDatabase(Database.System, "SELECT Id,Name FROM Organization WHERE vendorId = " + ddl.SelectedValue.ToString() + "  ORDER BY Name", True)
            CType(fc1, DropDownList).FillFromSqlWithDatabase(Database.System, "SELECT Distinct State FROM Organization WHERE State <> '*' AND vendorId = " + ddl.SelectedValue.ToString() + " ", True)
        End If
        If element = "report_filter_TimeSelector" Then
            Dim cntrl As WebControl = ph.FindControl("report_filter_TimeFrame")
            Dim lblCntrl As WebControl = ph.FindControl("lbl_TimeFrame")
            Dim lblYearCntrl As WebControl = ph.FindControl("lbl_year")
            Select Case ddl.SelectedIndex
                Case 1
                    CType(cntrl, DropDownList).FillFromSqlWithDatabase(Database.System, "SELECT 0 As Id ,'Jan' As Name UNION SELECT 1 As Id ,'Feb' As Name UNION SELECT 2 As Id ,'Mar' As Name  UNION SELECT 3 AS Id,'Apr' AS Name UNION SELECT 4 AS Id,'MAY' AS Name UNION SELECT 5 AS Id,'Jun' AS Name UNION SELECT 6 AS Id,'Jul' AS Name UNION SELECT 7 AS Id,'Aug' AS Name UNION SELECT 8 AS Id,'Sep' AS Name UNION SELECT 9 AS Id,'Oct' AS NAME UNION SELECT 10 AS Id,'Nov' AS Name UNION SELECT 11 AS Id,'Dec' AS Name ORDER BY Id", True)
                Case 2
                    CType(lblCntrl, Label).Text = "Quarter"
                    CType(cntrl, DropDownList).FillFromSqlWithDatabase(Database.System, "SELECT 0 As Id ,'First' As Name UNION SELECT 1 As Id ,'Second' As Name UNION SELECT 2 As Id ,'Third' As Name  ORDER BY Id", True)
                Case 3
                    CType(lblCntrl, Label).Text = "Year"
                    CType(lblYearCntrl, Label).Attributes("style") = [String].Format("display:none")
                    Dim YearCntrl As WebControl = ph.FindControl("report_filter_year")
                    CType(cntrl, DropDownList).Attributes("style") = [String].Format("display:none")
                    CType(YearCntrl, DropDownList).Attributes("style") = [String].Format("margin-left: 0px")
            End Select
        End If
        If element = "report_filter_CamaSystem" Then
            Dim fc As WebControl = ph.FindControl("report_filter_Org")
            CType(fc, DropDownList).FillFromSqlWithDatabase(Database.System, "SELECT Id,Name FROM Organization WHERE CAMASystem = " + ddl.SelectedValue.ToString() + " AND  ORDER BY Name", True)
        End If

    End Sub

    Sub LoadReportsHome()
        Dim response As XElement = XElement.Load(Server.MapPath("config/Reports.xml"))
        Dim temp1 = From uu In response.Descendants("Reports") Where uu.Element("RoleId") = "2" Select Name = uu.Element("Name").Value, ID = uu.Element("Id").Value, Description = uu.Element("Description").Value.Replace("\n", "<br />").Replace("\b", "<b>").Replace("/b", "</b>")
        rptReports.DataSource = temp1.ToList
        rptReports.DataBind()
    End Sub

    Sub LoadReport(reportId As Integer)
        Dim response As XElement = XElement.Load(Server.MapPath("config/Reports.xml"))
        Dim reportTitle, dataSource As String
        Dim reportParameters As New NameValueCollection
        Dim temp = From uu In response.Elements("Reports") From yy In uu.Elements("Id") Where yy.Value = reportId.ToString() Select uu
        reportTitle = temp.Elements("ReportTitle").Value
        dataSource = temp.Elements("DataSource").Value
        Dim reportData As DataTable
        Dim parameters As New NameValueCollection
        Dim params As XElement = XElement.Load(Server.MapPath("config/ReportParameters.xml"))
        Dim temp2 = From uu In params.Elements("ReportParameters") From yy In uu.Elements("ReportId") Where yy.Value = reportId.ToString Select uu
        Dim paramstring As String = ""
        For Each x As XElement In temp2
            Dim name As String = x.Element("Name").Value
            Dim inputType As Integer = x.Element("InputType").Value
            Dim required As String = x.Element("Required").Value
            Dim value As String = ""
            Dim fc As WebControl = ph.FindControl("report_filter_" + name)
            If fc IsNot Nothing Then
                Select Case inputType
                    Case 0
                        value = CType(fc, TextBox).Text
                    Case 1
                        value = CType(fc, DropDownList).SelectedValue
                    Case 2
                        value = CType(fc, TextBox).Text
                    Case 3
                        value = CType(fc, CheckBox).Checked.GetHashCode
                    Case 4
                        value = CType(fc, RadioButtonList).SelectedValue
                    Case 6
                        value = CType(fc, DropDownList).SelectedValue
                        If name = "TimeSelector" Then
                            Dim ts As WebControl = ph.FindControl("report_filter_TimeSelector")
                            Dim tsValue As String = CType(ts, DropDownList).SelectedValue
                            If tsValue = "" Then
                                parameters.Add("@Month", Nothing)
                                parameters.Add("@Year", Nothing)
                            Else
                                Dim dc As WebControl = ph.FindControl("report_filter_TimeFrame")
                                Dim yc As WebControl = ph.FindControl("report_filter_Year")
                                Dim value1 As String = ""
                                Dim value2 As String = ""
                                Dim name1 As String = ""
                                value1 = CType(dc, DropDownList).SelectedValue
                                If yc IsNot Nothing Then
                                    value2 = CType(yc, DropDownList).SelectedValue
                                End If
                                If value1 = "" Then
                                    parameters.Add("@Month", Nothing)
                                Else
                                    parameters.Add("@Month", value1)
                                End If
                                If value2 = "" Then
                                    parameters.Add("@Year", Nothing)
                                Else
                                    parameters.Add("@Year", value2)
                                End If
                            End If
                        End If
                End Select
            End If
            If value = "" And name <> "TimeFrame" Then
                parameters.Add("@" + name, Nothing)
            Else
                parameters.Add("@" + name, value)
            End If

        Next
        Try
            reportData = Database.System.GetDataTable(dataSource, CommandType.Text, parameters, 3000)
        Catch ex As Exception
            If ex.Message.Contains("OutOfMemoryException") Then
                reportsHome.Visible = False
                lblnoFile.Text = "Sorry, it appears there is too much data matching your criteria. Please adjust your search parameters and try again."
                pnl_nofile.Visible = True
                customReportPanel.Visible = False
            Else
                lblnoFile.Text = "Sorry, it appears there is no data matching your criteria. Please adjust your search parameters and try again."
                reportsHome.Visible = False
                pnl_nofile.Visible = True
                customReportPanel.Visible = False
            End If
            Exit Sub
        End Try
        If (reportData.Rows.Count <= 0) Then
            reportsHome.Visible = False
            pnl_nofile.Visible = True
            customReportPanel.Visible = False
        Else
            reportsHome.Visible = False
            pnl_nofile.Visible = False
            customReportPanel.Visible = True
            Uc_CustomReport.showReport()
            Uc_CustomReport.GenerateGrid(reportData, reportTitle)
        End If
    End Sub

    Protected Sub RetainDropdownValues(ByVal sender As Object)
        Dim ddl As DropDownList = DirectCast(sender, DropDownList)
        If ddl IsNot Nothing Then
            Dim element = ddl.ID
            If element = "report_filter_Vendor" Then
                Dim fc As WebControl = ph.FindControl("report_filter_Org")
                Dim val = CType(fc, DropDownList).SelectedValue
                If ddl.SelectedValue.ToString() <> "" Then
                    CType(fc, DropDownList).FillFromSqlWithDatabase(Database.System, "SELECT Id,Name FROM Organization WHERE vendorId = " + ddl.SelectedValue.ToString() + " ORDER BY Name", True)
                    CType(fc, DropDownList).ClearSelection()
                    CType(fc, DropDownList).Items.FindByValue(val).Selected = True
                End If
                Dim fc1 As WebControl = ph.FindControl("report_filter_State")
                Dim val1 = CType(fc1, DropDownList).SelectedValue
                If ddl.SelectedValue.ToString() <> "" Then
                    CType(fc1, DropDownList).FillFromSqlWithDatabase(Database.System, "SELECT Distinct State FROM Organization WHERE vendorId = " + ddl.SelectedValue.ToString() + "", True)
                    CType(fc1, DropDownList).ClearSelection()
                    CType(fc1, DropDownList).Items.FindByValue(val1).Selected = True
                End If
                
            End If
            If element = "report_filter_TimeSelector" Then
                Dim cntrl As WebControl = ph.FindControl("report_filter_TimeFrame")
                Dim lblCntrl As WebControl = ph.FindControl("lbl_TimeFrame")
                Dim lblYearCntrl As WebControl = ph.FindControl("lbl_year")
                Select Case ddl.SelectedIndex
                    Case 2
                        CType(lblCntrl, Label).Text = "Quarter"
                        Dim index = CType(cntrl, DropDownList).SelectedIndex
                        CType(cntrl, DropDownList).FillFromSqlWithDatabase(Database.System, "SELECT 0 As Id ,'First' As Name UNION SELECT 1 As Id ,'Second' As Name UNION SELECT 2 As Id ,'Third' As Name  ORDER BY Id", True)
                        CType(cntrl, DropDownList).SelectedIndex = index
                    Case 3
                        Dim YearCntrl As WebControl = ph.FindControl("report_filter_year")
                        CType(YearCntrl, DropDownList).Attributes("style") = [String].Format("margin-left: 0px;")
                        CType(lblCntrl, Label).Attributes("style") = [String].Format("display:none")
                        CType(cntrl, DropDownList).Attributes("style") = [String].Format("display:none")
                        CType(lblYearCntrl, Label).Attributes("style") = [String].Format("margin-left: 0px;")
                End Select
            End If
        End If
    End Sub

End Class


