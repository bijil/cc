﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/ClientAdmin.master"
    AutoEventWireup="false" Inherits="CAMACloud.Admin.CAMACloudAdmin_license_LicenseManager"
    CodeBehind="Default.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .license-filters
        {
            margin: 5px;
            margin-top: 15px;
            margin-bottom: 15px;
        }
        
        .license-filters a
        {
            padding-left: 15px;
        }
        
        .license-filters span
        {
        }
        
        .link-button
        {
            font-weight: bold;
            text-decoration: none;
            padding: 5px 15px;
            margin: 5px;
            margin-left: 0px;
            margin-right: 15px;
            background: #CFCFCF;
            border-radius: 8px;
            display: inline-block;
            cursor: pointer;
        }
        .link-button:hover
        {
            color: White;
            background: #065a9f;
        }
        
        .keys pre
        {
            margin: 0px !important;
            padding: 0px !important;
            font-weight: bold;
            font-family: Lucida Sans Typewriter;
        }
        
        .simple-form tr td
        {
            vertical-align: middle;
        }
        
        .mGrid td a
        {
            margin-right: 4px !important;
        }
        
        .sf
        {
            font-size: smaller;
        }
        .licenceTypeContainer { 
            display: none; 
            min-height: 50px  !important;
}
        .lcount {
            float: left;
            margin-left: 10px;
            margin-right: 15px;
            padding-top: 5px;
        }
        .count{
            color:black;font-weight:bold;
        }
        .anchorclass{
					   display: inline-block;
					   width: 100px;
					   white-space: nowrap;
					   overflow: hidden !important;
					   text-overflow: ellipsis;
					   
					    
					    text-decoration: none;
					   color: #717171;
}
    </style>
    <script type="text/javascript">
        function checkSelection() {
            var totalChecked = 0;
            $('.grid-selection input').each(function () {
                if (!$(this).parent('span').hasClass('skcheckbx') && this.checked)
                    totalChecked++;
            });
            if (totalChecked == 0) {
                alert('You have not selected any license row.');
                return false
            }
            return true;
        }
        
        $(document).ready(function () {
            $("#<%=txtNote.ClientID%>").keyup(function () {
                var note = $("#<%=txtNote.ClientID%>").val();
                if (note.length > 500) {
                    alert('Exceeding maximum number of characters.Please short your notes to 500 characters.');
                };
            });
        });
       
        function confirmMultiDelete() {
            if (!checkSelection()) return false;
            return confirm("Are you sure you want to delete selected device licenses permanently?")
        }

        function confirmMultiRevoke() {
            if (!checkSelection()) return false;
            return confirm("You are cancelling the selected installed device licenses and allowing it to be used again. Click OK to continue.")
        }
        var gridcheckselect = false;
        function confirmMultiChange() {
            if (!checkSelection()) return false;
            var dialog = $('.licenceTypeContainer').dialog({
                modal: true,
                height: 'auto',
                width: 'auto',
                top: '120.5px !important',
                resizable: false,
                title: "Licence Types"
            });
            if ($('#chkHead')[0].checked) gridcheckselect = true;
            dialog.parent().appendTo(jQuery('form:first')); //Setting the dialog containing Div to the start of the Form tag
        }

        $(document).ready(function() {
            $('.licenceTypeContainer').on('dialogclose', function (event) {
                $('.licenceTypeContainer .licenceType input').prop('checked', false);
            });
        });

        function confirmChangeLicenses() {
            var selectedLicenseType = $('input:checked', '.licenceTypeContainer .licenceType').closest("td").find("label").html();
            if (selectedLicenseType != '') {
                var msg = "Are you sure you want to change the selected device licenses type to " + selectedLicenseType + ". Click OK to continue."
                return confirm(msg);
            }             
        }
        
        function confirmEmail() {
            if (!checkSelection()) return false;
            if ($('.txt-email').val() == '') {
                alert('Please enter a valid email address to email the licenses.');
                return false;
            }
            if ($('#chkHead')[0].checked) gridcheckselect = true;
            return true;
        }

        function runaftersave() {
            if (gridcheckselect) $('#chkHead')[0].checked = true;
            gridcheckselect = false;
            $('.skcheckbx').unbind("change").bind("change", function () { if ($('#chkHead')[0].checked) gridcheckselect = true; });
        }

        function copyToClipboard() {
            if (window.clipboardData) {

            }
        }

        function bulkSelect(s) {
            $('.grid-selection input').each(function (i, x) { if (!$(x).parent('span').hasClass('skcheckbx')) x.checked = s; });
        }
        
        function ValidateNote() {
            var note = $("#<%=txtNote.ClientID%>").val();
            if (note.length > 500) {
                alert('Please short your notes to 500 characters.');
                return false;
            }
            else{
                return true;
            }

        }
        function checkboxcheck() {
            var chk = $('.grid-selection:not(.skcheckbx) input[type="checkbox"]');
            var chkChecked = $('.grid-selection:not(.skcheckbx) input[type="checkbox"]:checked');
           

            if (chk.length== chkChecked.length) {
                $('#chkHead').prop("checked", true);
            }
            else {
                $('#chkHead').prop("checked", false);
            }

        }
        
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
        <table style="width: 100%;">
        <tr>
            <td>
                <div style="width: 100%; float: left; height: 60px">
                <h1 runat="server" id="pageTitle">
                    Manage License Keys</h1>
                    <div style="position: relative; top: -54px;">
                        <asp:LinkButton ID="lbAPIAccess" runat="server" CommandName="API Access" Text="API Access Keys"
                            Style="margin-left: 5px; float: right; text-decoration: None" />
                        <asp:LinkButton ID="lblClientDtls" runat="server" CommandName="Client Details" Text="Client Details"
                            Style="float: right; text-decoration: None; margin-right: 15px" />
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <p class="info">
        Any client user can access the CAMA Cloud application and data only after validating
        their browser with a unique license key and one time password.</p>
    <asp:HiddenField runat="server" ID="hdnOrganizationName" />
    <asp:HiddenField runat="server" ID="hdnOrganizationId" />
    <asp:HiddenField runat="server" ID="hdnUseState" Value="0" />
    <div class="license-filters">
        <span>License Status:</span>
        <asp:LinkButton runat="server" ID="lbShowUnused" Text="Unused licenses" CommandArgument="0"
            Font-Bold="true" />
        <asp:LinkButton runat="server" ID="lbShowInUse" Text="Licenses in-use" CommandArgument="1" />
        <asp:LinkButton runat="server" ID="lbShowAll" Text="All" CommandArgument="" />
    </div>
    
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="simple-form">
                <tr runat="server" id="trNewLicense">
                    <td>
                        Generate new licenses:
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlNumber" Width="50px" />
                        <asp:DropDownList runat="server" ID="ddlLicenseType" Width="101px">
                            <asp:ListItem Value="" Text="Common" />
                            <asp:ListItem Value="iPad" Text="iPad" />
                            <asp:ListItem Value="Chrome" Text="Console" />
                            <asp:ListItem Value="MAChrome" Text="MA Chrome" />
                            <asp:ListItem Value="MACommon" Text="MA Common" />
                            <asp:ListItem Value="SketchPro" Text="SketchPro" />
                        </asp:DropDownList>
                        <asp:TextBox ID="txtNote" runat="server" ToolTip="Give some notes" placeholder="Note"></asp:TextBox>
                        <asp:Button ID="btnCreate" runat="server" Text="Generate" />
                    </td>
                </tr>
            </table>
            <table style="width: 100%;" runat="server" id="tLicenseOps">
                <tr style="width: 920px;">
                    <td class="link-panel">
                        <asp:LinkButton runat="server" ID="lbDeleteSelected" Text="Delete Selected" OnClientClick="return confirmMultiDelete()" />
                        <asp:LinkButton runat="server" ID="lbRevokeSelected" Text="Revoke Selected" OnClientClick="return confirmMultiRevoke()" />
                        <asp:LinkButton runat="server" ID="lbChngeSelected" Text="Change Selected" OnClientClick="return confirmMultiChange()" />
                    </td>
                    <td style="text-align: right;">
                        <asp:TextBox runat="server" ID="txtEmail" type="email" Width="200px" CssClass="txt-email" />
                        <asp:Button Text=" Email Licenses " runat="server" ID="btnEmail" OnClientClick="return confirmEmail();" />
                    </td>
                </tr>
                <tr style="width: 100%;">
                    <td><span class="lcount"><strong>Total Licenses: </strong>
                        <asp:Label ID="lblTotal" CssClass="count" runat="server"></asp:Label>
                    </span>
                        <span class="lcount"><strong>Common: </strong>
                            <asp:Label ID="lblCommonCount" CssClass="count" runat="server"></asp:Label>
                        </span>
                        <span class="lcount"><strong>iPad: </strong>
                            <asp:Label ID="lbliPadCount" CssClass="count" runat="server"></asp:Label>
                        </span>
                        <span class="lcount"><strong>Console: </strong>
                            <asp:Label ID="lblConsoleCount" CssClass="count" runat="server"></asp:Label>
                        </span>
                        <span class="lcount"><strong>MA Chrome: </strong>
                            <asp:Label ID="lblMAChromeCount" CssClass="count" runat="server"></asp:Label>
                        </span>
                        <span class="lcount"><strong>MA Common: </strong>
                            <asp:Label ID="lblMACommonCount" CssClass="count" runat="server"></asp:Label>
                        </span>
                        <span class="lcount"><strong>SketchPro: </strong>
                            <asp:Label ID="lblSketchPro" CssClass="count" runat="server"></asp:Label>
                        </span>
                    </td>
                    <td style="text-align: right;">
                        <asp:ImageButton ID="ibtnExportToExcel" ImageUrl="~/App_Static/images/excel.png" Width="28px" Height="25px" Style="margin-right: 3px;" ToolTip="Export To Excel" runat="server" />
                        <asp:ImageButton ID="ibtnExportToCSV" ImageUrl="~/App_Static/images/csv.png" Width="28px" Height="25px" Style="margin-right: 3px;" ToolTip="Export To CSV" runat="server" />
                    </td>
                </tr>
            </table>
            <asp:GridView ID="gvLicense" runat="server">
                <Columns>
                    <asp:TemplateField>
                        <ItemStyle Width="18px" />
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSel" runat="server" Checked="false" CssClass="grid-selection" onchange="checkboxcheck()"/>
                            <asp:HiddenField ID="LID" runat="server" Value='<%# Eval("Id") %>' />
                            <asp:HiddenField ID="LKEY" runat="server" Value='<%# EvalLicenseKey() %>' />
                            <asp:HiddenField ID="OTP" runat="server" Value='<%# Eval("OTP") %>' />
                            <asp:HiddenField ID="DEVICE" runat="server" Value='<%# Eval("TargetDevice") %>' />
                        </ItemTemplate>
                        <HeaderTemplate>
                            <input type="checkbox" onchange='bulkSelect(this.checked);'  id="chkHead"/>
                        </HeaderTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="TargetDevice" HeaderText="Device" ItemStyle-Width="110px" />
                    <asp:TemplateField HeaderText="License Key">
                        <ItemStyle CssClass="keys" Width="180px" />
                        <ItemTemplate>
                            <pre style=""><%# EvalLicenseKey() %></pre>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="SCODE" HeaderText="SCODE" ItemStyle-Width="60px" ItemStyle-Font-Bold="true" />
                    <asp:BoundField DataField="OTP" HeaderText="OTP" ItemStyle-Width="60px" ItemStyle-Font-Bold="true" />
                    <asp:BoundField DataField="RegisteredEmail" HeaderText="Registered Email" ItemStyle-Width="250px" />
                    <asp:BoundField DataField="RegisteredNick" HeaderText="Nickname" ItemStyle-Width="150px" />
                    <asp:TemplateField>
                        <ItemStyle Width="96px" />
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="lbDelete" Text="Delete" CommandName="DeleteLicense"
                                CommandArgument='<%# Eval("Id") %>' OnClientClick='return confirm("Are you sure you want to delete this device license permanently?")' />&nbsp;
                            <asp:LinkButton runat="server" ID="lbRevoke" Text="Revoke" CommandName="RevokeLicense"
                                CommandArgument='<%# Eval("Id") %>' Visible='<%# Eval("InUse") %>' OnClientClick='return confirm("You are cancelling an installed device license and allowing it to be used again. Click OK to continue.")' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Note">
                        <ItemStyle  Width="200px" />
                        <ItemTemplate>
                        
                        <a href="" runat="server" title='<%# Eval("Note") %>' class ="anchorclass">
                               <%# Eval("Note") %>   
                                </a>
                               
                            </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemStyle Width="18px" />
                        <ItemTemplate>
                            <asp:Label runat="server" CssClass="a16 lock16" Visible='<%# Eval("InUse") %>' ToolTip="License Key in use" />
                            <asp:Label runat="server" CssClass="a16 newborn16" Visible='<%# ((System.DateTime.UtcNow - CDate(Eval("CreatedDate"))).TotalSeconds < 30) And Not Eval("InUse") %>'
                                ToolTip="Newly created" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="LastUsedLoginId" HeaderText="CAMA User" ItemStyle-Width="90px"
                        ItemStyle-CssClass="sf" />
                    <asp:BoundField DataField="LastUsedIPAddress" HeaderText="IP Address" ItemStyle-Width="85px"
                        ItemStyle-CssClass="sf" />
                    <asp:TemplateField HeaderText="Last Access">
                        <ItemStyle CssClass="keys sf" Width="90px" />
                        <ItemTemplate>
                            <%# Eval("LastAccessTime", "{0:MM/dd/yyyy hh:mm tt}") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                <asp:BoundField DataField="CreatedDate" HeaderText="Created Date" ItemStyle-Width="160px" />
                <asp:BoundField DataField="CreatedBy" HeaderText="Created By" ItemStyle-Width="80px" />
                <asp:TemplateField HeaderText="SketchPro">
                    <ItemStyle  Width="90px" />
                    <ItemTemplate>
                        <asp:CheckBox ID="chkSketchProSel"  runat="server" AutoPostBack="true" oncheckedchanged="chkSketchProSel_CheckedChanged" Checked='<%# Eval("EnabledSketchPro") %>' Enabled='<%# IIf((Not IsDBNull(Eval("TargetDevice")) AndAlso (Eval("TargetDevice") = "Console" Or Eval("TargetDevice") = "iPad")), "true", "false") %>' CssClass="grid-selection skcheckbx"/>
                    </ItemTemplate>
                </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    <div class="info">
                        No licenses found.
                    </div>
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div style="display: none;">
        <textarea id="clipboardAgent"></textarea>
    </div>
    <div class="licenceTypeContainer">
        <asp:RadioButtonList ID="rblLicenceType" runat="server" CssClass="licenceType" RepeatDirection="Horizontal">
            <asp:ListItem Value="">Common</asp:ListItem>
            <asp:ListItem Value="iPad">iPad</asp:ListItem>
            <asp:ListItem Value="Chrome">Console</asp:ListItem>
            <asp:ListItem Value="MAChrome">MA Chrome</asp:ListItem>
            <asp:ListItem Value="MACommon">MA Common</asp:ListItem>
            <asp:ListItem Value="SketchPro">Sketch Pro</asp:ListItem>
        </asp:RadioButtonList>

        <div style="text-align: center;margin-top:20px">
             <asp:Button Text="Change" runat="server" ID="btnLicenceChange" OnClientClick="return confirmChangeLicenses();" />
        </div>
           

    </div>   
</asp:Content>
