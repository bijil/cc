﻿Imports System.Net.Mail
Imports CAMACloud.Security
Imports System.IO

Partial Class CAMACloudAdmin_license_LicenseManager
    Inherits System.Web.UI.Page

    'Private _organizationId As Integer = -9
    Private sketchLicenseFlag As Integer = 0

    ReadOnly Property OrganizationId As Integer
        Get
            If hdnOrganizationId.Value = "" Then
                If Request("ouid") Is Nothing Then
                    'Response.Redirect("~/clientaccounts/search.aspx?pageaction=license")
                    Response.Redirect("~/clientaccounts/list.aspx")
                    hdnOrganizationId.Value = -1
                Else
                    hdnOrganizationId.Value = Database.System.GetIntegerValueOrInvalid("SELECT ID FROM Organization WHERE UID = {0}".SqlFormatString(Request("ouid")))
                    hdnOrganizationName.Value = Database.System.GetStringValue("SELECT Name FROM Organization WHERE UID = {0}".SqlFormatString(Request("ouid")))
                End If
                If hdnOrganizationId.Value.ToString = "-1" Then
                    'Response.Redirect("~/clientaccounts/search.aspx?pageaction=license")
                    Response.Redirect("~/clientaccounts/list.aspx")
                    Response.End()
                End If
            End If
            Return hdnOrganizationId.Value
        End Get
    End Property

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim scriptManager__1 As ScriptManager = ScriptManager.GetCurrent(Me.Page)
        scriptManager__1.RegisterPostBackControl(Me.ibtnExportToExcel)
        scriptManager__1.RegisterPostBackControl(Me.ibtnExportToCSV)
        sketchLicenseFlag = 0
        If Not IsPostBack Then
            ddlNumber.FillNumbers(1, 50)
            RefreshLicenseGrid()

            pageTitle.InnerHtml = "License Keys - " + hdnOrganizationName.Value

            'Checking the Logged user and change the permissions
            If Session("loggedUserType") Is Nothing Then
                Alert("Session Experied, Please LogIn again")
                Response.Redirect("~/auth/Default.aspx")
            Else
                Dim loggedUserType As String = Session("loggedUserType").ToString()

                If loggedUserType = "support" Then
                    lbDeleteSelected.Visible = False

                    Dim lnkBtnDelete As LinkButton = New LinkButton()

                    For i As Integer = 0 To gvLicense.Rows.Count - 1
                        lnkBtnDelete = DirectCast(gvLicense.Rows(i).FindControl("lbDelete"), LinkButton)
                        lnkBtnDelete.Visible = False
                    Next

                    'gvLicense.Columns(0).Visible = False
                End If
            End If
        End If
        RunScript("runaftersave();")
    End Sub

    Private Sub bindOrganizations()
        'ddlOrganization.FillFromSqlWithDatabase(Database.System, "SELECT Id, Name FROM Organization ORDER BY Name", True)

    End Sub

    Private Sub RefreshLicenseGrid()
        If OrganizationId <> -1 Then
            trNewLicense.Visible = True
            tLicenseOps.Visible = True
            gvLicense.Visible = True
        Else
            trNewLicense.Visible = False
            tLicenseOps.Visible = False
            gvLicense.Visible = False
        End If
        lbRevokeSelected.Visible = hdnUseState.Value <> "0"
        Dim inUse As String = IIf(hdnUseState.Value <> "", " AND InUse = " + hdnUseState.Value, "")
        Dim sql As String = String.Format("SELECT *, CASE DeviceFilter WHEN 'Chrome' THEN 'Console' WHEN 'MAChrome' THEN 'MA Chrome' WHEN 'MACommon' THEN 'MA Common' WHEN 'SketchPro' THEN 'Sketch Pro' ELSE DeviceFilter END As TargetDevice FROM DeviceLicense WHERE OrganizationId  = {0} AND IsSuper = 0 AND IsVendor = 0 " + inUse + " ORDER BY InUse DESC, CreatedDate DESC", OrganizationId)
        Dim dt As DataTable = Database.System.GetDataTable(sql)
        lblTotal.Text = dt.Rows.Count
        lblCommonCount.Text = dt.AsEnumerable().Where(Function(x) x("TargetDevice").ToString() = "").ToList().Count
        lbliPadCount.Text = dt.AsEnumerable().Where(Function(x) x("TargetDevice").ToString() = "iPad").ToList().Count
        lblConsoleCount.Text = dt.AsEnumerable().Where(Function(x) x("TargetDevice").ToString() = "Console").ToList().Count
        lblMAChromeCount.Text = dt.AsEnumerable().Where(Function(x) x("TargetDevice").ToString() = "MA Chrome").ToList().Count
        lblMACommonCount.Text = dt.AsEnumerable().Where(Function(x) x("TargetDevice").ToString() = "MA Common").ToList().Count
        lblSketchPro.Text = dt.AsEnumerable().Where(Function(x) x("TargetDevice").ToString() = "Sketch Pro").ToList().Count

        gvLicense.DataSource = dt
        gvLicense.DataBind()
        Dim sql1 As String = String.Format("SELECT  CASE DeviceFilter WHEN 'Chrome' THEN 'Console' WHEN 'MAChrome' THEN 'MA Chrome' WHEN 'MACommon' THEN 'MA Common'  ELSE DeviceFilter END As Device,LicenseKey AS [License Key], SCODE, OTP,RegisteredEmail AS[Registered Email],RegisteredNick AS [Nickname],CreatedDate AS [Created Date],CreatedBy AS [Created By],LastUsedLoginId AS [CAMA User],LastUsedIPAddress AS [IP Address],LastAccessTime AS [Last Access], Note AS Note, EnabledSketchPro AS [Enabled SketchPro] FROM DeviceLicense WHERE OrganizationId  = {0} AND IsSuper = 0 AND IsVendor = 0 " + inUse + " ORDER BY InUse DESC, CreatedDate DESC", OrganizationId)
        Dim dt1 As DataTable = Database.System.GetDataTable(sql1)
        ViewState("dtData1") = dt1

        If gvLicense.Rows.Count > 0 Then
            tLicenseOps.Visible = True
        Else
            tLicenseOps.Visible = False
        End If
    End Sub

    Protected Sub btnGenerateLicense_Click(sender As Object, e As System.EventArgs) Handles btnCreate.Click
        If (ddlLicenseType.SelectedValue = "SketchPro") Then
            Dim alSeat As Integer = GetAvailableSketchProCOunt()
            If (alSeat < CInt(ddlNumber.SelectedValue)) Then
                Alert("The no.of selected license is more than the available seat count. Available Seat Count: " + alSeat.ToString())
                Return
            End If
        End If
        For i = 1 To CInt(ddlNumber.SelectedValue)
            Dim license As String = DeviceLicense.GenerateNewLicenseKey(16)
            Dim otp As Integer = DeviceLicense.GenerateNewOTP()
            Dim sql As String = String.Format("INSERT INTO DeviceLicense (OrganizationId, LicenseKey, OTP, CreatedDate, DeviceFilter,Note,CreatedBy) VALUES({0}, '{1}', {2}, '{3}', {4}, '{5}','{6}')Select @@IDENTITY", OrganizationId, license, otp, Date.UtcNow.ToString("yyyy-MM-dd HH:mm:ss"), ddlLicenseType.SelectedValue.ToSqlValue(True), txtNote.Text, Membership.GetUser().ToString())
            Dim dvlId = Database.System.GetIntegerValue(sql)
            Dim Scode As String = DeviceLicense.GenerateScode(dvlId)
            Database.System.Execute("UPDATE DeviceLicense SET SCODE = '" + Scode + "' WHERE Id = " + dvlId.ToString())
            Database.System.Execute("INSERT INTO LicenseAudittrail SELECT OrganizationId,'" + Membership.GetUser().ToString() + "' AS LoginId,GETUTCDATE() AS EventTime,DeviceFilter,LicenseKey,'" + HttpContext.Current.Request.ClientIPAddress + "' AS IPAddress,'New License key generated' AS Comment FROM DeviceLicense where id = " + dvlId.ToString())
        Next
        If ddlLicenseType.SelectedValue = "SketchPro" Then
            Dim qry As String = "UPDATE	OrganizationSeats SET ConsumedSeat = ConsumedSeat + " + CInt(ddlNumber.SelectedValue).ToString() + " WHERE AppRoleId = 999 AND OrgId = " + OrganizationId.ToString()
            Database.System.Execute(qry)
        End If
        hdnUseState.Value = 0

        RefreshLicenseGrid()
        txtNote.Text = ""
        'Checking the Logged user and change the permissions
        If Session("loggedUserType") Is Nothing Then
            Alert("Session Experied, Please LogIn again")
            Response.Redirect("~/auth/Default.aspx")
        Else
            Dim loggedUserType As String = Session("loggedUserType").ToString()

            If loggedUserType = "support" Then
                lbDeleteSelected.Visible = False

                Dim lnkBtnDelete As LinkButton = New LinkButton()

                For i As Integer = 0 To gvLicense.Rows.Count - 1
                    lnkBtnDelete = DirectCast(gvLicense.Rows(i).FindControl("lbDelete"), LinkButton)
                    lnkBtnDelete.Visible = False
                Next
                'gvLicense.Columns(0).Visible = False
            End If
        End If
    End Sub

    Public Function GetAvailableSketchProCOunt() As Integer
        Dim currentSeat As Integer = Database.System.GetIntegerValue("SELECT SeatCount FROM OrganizationSeats WHERE AppRoleId = 999 AND OrgId = " + OrganizationId.ToString())
        Dim usedCount As Integer = Database.System.GetIntegerValue("SELECT COUNT(*) FROM DeviceLicense WHERE OrganizationId = " + OrganizationId.ToString() + " AND (DeviceFilter = 'SketchPro' OR EnabledSketchPro = 1)")
        Dim availableCount As Integer = IIf(currentSeat - usedCount > 0, currentSeat - usedCount, 0)
        Return availableCount
    End Function

    Public Function EvalLicenseKey() As String
        Dim key As String = Eval("LicenseKey")
        If key.Length = 16 Then
            key = key.Substring(0, 4) + "-" + key.Substring(4, 4) + "-" + key.Substring(8, 4) + "-" + key.Substring(12, 4)
        End If
        Return key
    End Function

    Protected Sub gvLicense_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvLicense.RowCommand
        Select Case e.CommandName
            Case "DeleteLicense"
                DeleteLicenses(e.CommandArgument)
            Case "RevokeLicense"
                RevokeLicenses(e.CommandArgument)
        End Select
    End Sub

    Protected Function GetSelectedIds() As String
        Dim selection As String = "0"
        For Each gr As GridViewRow In gvLicense.Rows
            If gr.GetChecked("chkSel") Then
                selection += "," + gr.GetHiddenValue("LID")
            End If
        Next
        Return selection
    End Function

    Protected Sub lbDeleteSelected_Click(sender As Object, e As System.EventArgs) Handles lbDeleteSelected.Click
        DeleteLicenses(GetSelectedIds)
    End Sub

    Protected Sub lbRevokeSelected_Click(sender As Object, e As System.EventArgs) Handles lbRevokeSelected.Click
        RevokeLicenses(GetSelectedIds)
    End Sub
    Protected Sub btnLicenceChange_Click(sender As Object, e As System.EventArgs) Handles btnLicenceChange.Click
        ChangeLicenses(GetSelectedIds)
    End Sub

    Sub DeleteLicenses(ids As String)
		Database.System.Execute("INSERT INTO LicenseAudittrail SELECT OrganizationId,'" + Membership.GetUser().ToString() +"' AS LoginId,GETUTCDATE() AS EventTime,DeviceFilter,LicenseKey,'" + HttpContext.Current.Request.ClientIPAddress + "' AS IPAddress,'License Key Removed' AS Comment FROM DeviceLicense WHERE Id IN (" & ids & ")" )
        Dim cnt As Integer = Database.System.GetIntegerValue("SELECT COUNT(*) FROM DeviceLicense WHERE OrganizationId = " + OrganizationId.ToString() + " AND (DeviceFilter = 'SketchPro' OR EnabledSketchPro = 1) AND Id IN (" & ids & ")")
        Database.System.Execute("DELETE FROM DeviceLicense WHERE Id IN (" & ids & ")")
        Database.System.Execute("UPDATE OrganizationSeats SET ConsumedSeat = ConsumedSeat - " + (cnt).ToString() + " WHERE AppRoleId = 999 AND OrgId = " + OrganizationId.ToString())
        RefreshLicenseGrid()
    End Sub

    Sub RevokeLicenses(ids As String)
        Database.System.Execute("UPDATE DeviceLicense SET InUse = 0, MachineKey = NULL, SessionKey = NULL WHERE Id IN (" & ids & ")")
        Database.System.Execute("INSERT INTO LicenseAudittrail SELECT OrganizationId,'" + Membership.GetUser().ToString() +"' AS LoginId,GETUTCDATE() AS EventTime,DeviceFilter,LicenseKey,'" + HttpContext.Current.Request.ClientIPAddress + "' AS IPAddress,'License Key Revoked' AS Comment FROM DeviceLicense WHERE Id IN (" & ids & ")" )
        RefreshLicenseGrid()
    End Sub
    Sub ChangeLicenses(ids As String)
        Dim qry As String = ""
        If (rblLicenceType.SelectedValue = "SketchPro") Then
            Dim alSeat As Integer = GetAvailableSketchProCOunt()
            Dim st As Integer = Database.System.GetIntegerValue("SELECT COUNT(*) FROM DeviceLicense WHERE OrganizationId = " + OrganizationId.ToString() + " AND (DeviceFilter = 'SketchPro' OR EnabledSketchPro = 1) AND Id IN (" & ids & ")")
            Dim cnt As Integer = Split(ids, ",").Length
            If ((alSeat + st) < (cnt - 1)) Then
                Alert("Requested Sketch Pro license is not available!")
                RefreshLicenseGrid()
                Return
            End If
            qry = "UPDATE DeviceLicense SET EnabledSketchPro = 0 WHERE Id IN (" & ids & ")"
            qry += "; UPDATE OrganizationSeats SET ConsumedSeat = ConsumedSeat + " + (cnt - 1).ToString() + " WHERE AppRoleId = 999 AND OrgId = " + OrganizationId.ToString()
        ElseIf (rblLicenceType.SelectedValue = "Chrome" Or rblLicenceType.SelectedValue = "iPad") Then
            Dim cnt As Integer = Database.System.GetIntegerValue("SELECT COUNT(*) FROM DeviceLicense WHERE OrganizationId = " + OrganizationId.ToString() + " AND DeviceFilter = 'SketchPro' AND Id IN (" & ids & ")")
            qry += "; UPDATE OrganizationSeats SET ConsumedSeat = ConsumedSeat - " + (cnt - 1).ToString() + " WHERE AppRoleId = 999 AND OrgId = " + OrganizationId.ToString()
        Else
            Dim cnt As Integer = Database.System.GetIntegerValue("SELECT COUNT(*) FROM DeviceLicense WHERE OrganizationId = " + OrganizationId.ToString() + " AND (DeviceFilter = 'SketchPro' OR EnabledSketchPro = 1) AND Id IN (" & ids & ")")
            qry = "UPDATE DeviceLicense SET EnabledSketchPro = 0 WHERE Id IN (" & ids & ")"
            qry += "; UPDATE OrganizationSeats SET ConsumedSeat = ConsumedSeat - " + (cnt - 1).ToString() + " WHERE AppRoleId = 999 AND OrgId = " + OrganizationId.ToString()
        End If

        Dim sql As String = String.Format("UPDATE DeviceLicense SET DeviceFilter={0} WHERE Id IN (" & ids & ")", rblLicenceType.SelectedValue.ToSqlValue(True))
        Database.System.Execute(sql)
        If qry <> "" Then
            Database.System.Execute(qry)
        End If
        rblLicenceType.SelectedIndex = -1
        RefreshLicenseGrid()
    End Sub

    'Protected Sub ddlOrganization_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlOrganization.SelectedIndexChanged
    '    ddlUsageStatus.SelectedValue = "0"
    '    RefreshLicenseGrid()
    'End Sub

    Protected Sub btnEmail_Click(sender As Object, e As System.EventArgs) Handles btnEmail.Click
        CAMACloud.Security.DeviceLicense.SendLicenseEmail(Me, gvLicense, txtEmail.Text, hdnOrganizationName.Value, False)
        RunScript("runaftersave();")
    End Sub

    Private Sub lbShowAll_Click(sender As Object, e As System.EventArgs) Handles lbShowAll.Click, lbShowInUse.Click, lbShowUnused.Click
        Dim lb As LinkButton = sender
        lbShowAll.Font.Bold = False
        lbShowInUse.Font.Bold = False
        lbShowUnused.Font.Bold = False
        lb.Font.Bold = True
        hdnUseState.Value = lb.CommandArgument
        RefreshLicenseGrid()

        'Checking the Logged user and change the permissions
        If Session("loggedUserType") Is Nothing Then
            Alert("Session Experied, Please LogIn again")
            Response.Redirect("~/auth/Default.aspx")
        Else
            Dim loggedUserType As String = Session("loggedUserType").ToString()

            If loggedUserType = "support" Then
                lbDeleteSelected.Visible = False

                Dim lnkBtnDelete As LinkButton = New LinkButton()

                For i As Integer = 0 To gvLicense.Rows.Count - 1
                    lnkBtnDelete = DirectCast(gvLicense.Rows(i).FindControl("lbDelete"), LinkButton)
                    lnkBtnDelete.Visible = False
                Next

                'gvLicense.Columns(0).Visible = False
            End If
        End If
    End Sub

    Protected Sub lblClientDtls_Click(sender As Object, e As EventArgs) Handles lblClientDtls.Click
        Dim var As String
        Dim org As DataRow = Database.System.GetTopRow("SELECT * FROM Organization WHERE Id=" & Val(OrganizationId))
        var = org.GetString("UID")
        Response.Redirect("~/clientaccounts/manageclient.aspx/?ouid=" + var)
    End Sub

    Protected Sub lbAPIAccess_Click(sender As Object, e As EventArgs) Handles lbAPIAccess.Click
        Dim var As String
        Dim org As DataRow = Database.System.GetTopRow("SELECT * FROM Organization WHERE Id=" & Val(OrganizationId))
        var = org.GetString("UID")
        Response.Redirect("~/license/applicationaccess.aspx?ouid=" + var)
    End Sub
    Protected Sub ibtnExportToExcel_Click(sender As Object, e As System.EventArgs) Handles ibtnExportToExcel.Click
        Dim dt As DataTable = ViewState("dtData1")
        Dim GridView1 As GridView = ExportGridView(dt, "Last Access", "{0:MM/dd/yyyy h:mm tt}")
        ExportToExcel(GridView1, "LicenseKeys.xlsx", dt)
    End Sub

    Public Function ExportGridView(ByVal dt As DataTable, Optional ByVal formatColumn As String = Nothing, Optional ByVal format As String = Nothing) As GridView
        'Create a dummy GridView
        Dim GridView1 As New GridView()
        GridView1.AllowPaging = False
        GridView1.AutoGenerateColumns = False
        Dim bfieldName As String
        Dim bHeaderText As String
        GridView1.Columns.Clear()
        For Each column As DataColumn In dt.Columns
            bfieldName = column.ColumnName
            bHeaderText = column.ColumnName
            Dim bfield As New BoundField()
            bfield.HeaderText = bHeaderText
            bfield.DataField = bfieldName
            If bfieldName = "OTP" Then
                bfield.ItemStyle.Width = 60
            End If
            If bfieldName = "Device" Then
                bfield.ItemStyle.Width = 110
            End If
            Select Case bfieldName
                Case "OTP"
                    bfield.ItemStyle.Width = 60
                Case "SCODE"
                    bfield.ItemStyle.Width = 60
                Case "Device"
                    bfield.ItemStyle.Width = 110
                Case "Registered Email"
                    bfield.ItemStyle.Width = 250
                Case "Nickname"
                    bfield.ItemStyle.Width = 150
                Case "Created Date"
                    bfield.ItemStyle.Width = 160
                Case "CAMA User"
                    bfield.ItemStyle.Width = 90
                Case "IP Address"
                    bfield.ItemStyle.Width = 85
                Case "Last Access"
                    bfield.ItemStyle.Width = 90
                Case "Note"
                    bfield.ItemStyle.Width = 200
                Case "License Key"
                    bfield.ItemStyle.Width = 180
                Case "Enabled SketchPro"
                    bfield.ItemStyle.Width = 120
            End Select
            If (column.ColumnName = formatColumn) Then
                bfield.DataFormatString = format
            End If
            GridView1.Columns.Add(bfield)
        Next
        GridView1.DataSource = Nothing
        GridView1.DataSource = dt
        GridView1.DataBind()
        Return GridView1
    End Function

    Public Sub ExportToExcel(GridView1 As GridView, ByVal filename As String, dt As DataTable)
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        Response.Clear()
        Response.Buffer = True
        Response.ClearContent()
        Response.ClearHeaders()
        Response.AddHeader("Content-Disposition", "attachment;filename=" + filename)
        Response.Charset = ""
        Response.ContentType = "application/vnd.openxml.formats-officedocument.spreadsheetml.sheet"
        Dim excelStream = ExcelGenerator.ExportGrid(GridView1, dt, "License Keys", "License Keys")
        excelStream.CopyTo(Response.OutputStream)
        Response.End()
    End Sub

    Protected Sub ibtnExportToCSV_Click(sender As Object, e As System.EventArgs) Handles ibtnExportToCSV.Click
        Dim dt As DataTable = ViewState("dtData1")
        Dim GridView1 As GridView = ExportGridView(dt, "Last Access", "{0:MM/dd/yyyy h:mm tt}")
        ExportToCSV(GridView1, "LicenseKeys.csv")
    End Sub

    Protected Sub ExportToCSV(ByVal GridView1 As GridView, ByVal filename As String)
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("Content-Disposition", "attachment;filename=" + filename)
        Response.Charset = ""
        Response.ContentType = "application/text"
        Dim sBuilder As StringBuilder = New System.Text.StringBuilder()
        For index As Integer = 0 To GridView1.Columns.Count - 1
            sBuilder.Append(GridView1.Columns(index).HeaderText + ","c)
        Next
        sBuilder.Append(vbCr & vbLf)
        For i As Integer = 0 To GridView1.Rows.Count - 1
            For k As Integer = 0 To GridView1.HeaderRow.Cells.Count - 1
                sBuilder.Append(GridView1.Rows(i).Cells(k).Text.Replace("&nbsp;", "") + ",")
            Next
            sBuilder.Append(vbCr & vbLf)
        Next
        Response.Output.Write(sBuilder.ToString())
        Response.Flush()
        Response.[End]()
    End Sub

    Protected Sub chkSketchProSel_CheckedChanged(sender As Object, e As EventArgs)
        Dim chkStatus As CheckBox = DirectCast(sender, CheckBox)
        Dim Id As Integer = DirectCast(chkStatus.NamingContainer, GridViewRow).GetHiddenValue("LID")

        If chkStatus.Checked = "True" Then
            Dim alSeat As Integer = GetAvailableSketchProCOunt()
            If (alSeat < 1) Then
                chkStatus.Checked = "False"
                If sketchLicenseFlag = 0 Then
                    Alert("The maximum license count have been reached!")
                    sketchLicenseFlag = 1

                End If

                Return
            End If

            Dim qry As String = "UPDATE DeviceLicense SET EnabledSketchPro = 1 WHERE Id = " + Id.ToString()
            qry += "; INSERT INTO LicenseAudittrail SELECT OrganizationId,'" + Membership.GetUser().ToString() + "' AS LoginId, GETUTCDATE() AS EventTime, DeviceFilter, LicenseKey ,'" + HttpContext.Current.Request.ClientIPAddress + "' AS IPAddress, 'Enabled Sketch Pro' FROM DeviceLicense WHERE Id = " + Id.ToString()
            qry += "; UPDATE OrganizationSeats SET ConsumedSeat = ConsumedSeat + 1 WHERE AppRoleId = 999 AND OrgId = " + OrganizationId.ToString()
            Database.System.Execute(qry)
        Else
            Dim qry As String = "UPDATE DeviceLicense SET EnabledSketchPro = 0 WHERE Id = " + Id.ToString()
            qry += "; INSERT INTO LicenseAudittrail SELECT OrganizationId,'" + Membership.GetUser().ToString() + "' AS LoginId, GETUTCDATE() AS EventTime, DeviceFilter, LicenseKey ,'" + HttpContext.Current.Request.ClientIPAddress + "' AS IPAddress, 'Disabled Sketch Pro' FROM DeviceLicense WHERE Id = " + Id.ToString()
            qry += "; UPDATE OrganizationSeats SET ConsumedSeat = ConsumedSeat - 1 WHERE AppRoleId = 999 AND OrgId = " + OrganizationId.ToString()
            Database.System.Execute(qry)
        End If
    End Sub
End Class
