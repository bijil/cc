﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ClientAdmin.master"
    CodeBehind="query.aspx.vb" Inherits="CAMACloud.Admin.query" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .link-button
        {
            font-weight: bold;
            text-decoration: none;
            padding: 5px 15px;
            margin: 5px;
            margin-left: 0px;
            margin-right: 15px;
            background: #CFCFCF;
            border-radius: 8px;
            display: inline-block;
            cursor: pointer;
        }
        .link-button:hover
        {
            color: White;
            background: #065a9f;
        }
        
        .keys pre
        {
            margin: 0px !important;
            padding: 0px !important;
            font-weight: bold;
            font-family: Lucida Sans Typewriter;
        }
        
        .simple-form tr td
        {
            vertical-align: middle;
        }
    </style>
    <script type="text/javascript">
        function bulkSelect(s) {
            $('.grid-selection input').each(function (i,x) { x.checked = s; });
        }
        function checkboxcheck() {
            var chk = $('.grid-selection input[type = checkbox]');
            var chkChecked = $('.grid-selection input[type = checkbox]:checked');
            if (chk.length == chkChecked.length) {
                $('#chkHead').prop("checked", true);
            }
            else {
                $('#chkHead').prop("checked", false);
            }

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
        Search Licenses</h1>
    <p class="info">
        Search for licenses using the license keys or registered email addresses or nicknames.</p>
    <p>
        License Key/Email/Nickname:
        <asp:HiddenField runat="server" ID="hdnFilter" />
        <asp:TextBox runat="server" ID="txtKey" Width="220px" MaxLength="50" />
        <asp:RequiredFieldValidator runat="server" ID="rfv" ControlToValidate="txtKey" ErrorMessage="*"
            ValidationGroup="query" />
        <asp:Button runat="server" ID="btnSearch" Text=" Search " ValidationGroup="query" />
    </p>
     <table style="width: 100%;" runat="server" id="tLicenseOps" visible="false">
                <tr style="width: 920px;">
                    <td class="link-panel">
                        <asp:LinkButton runat="server" ID="lbDeleteSelected" Text="Delete Selected" OnClientClick="return confirmMultiDelete()" />
                        <asp:LinkButton runat="server" ID="lbRevokeSelected" Text="Revoke Selected" OnClientClick="return confirmMultiRevoke()" />
                    </td>
                    <td style="text-align: right;">
                        <asp:TextBox runat="server" ID="txtEmail" type="email" Width="200px" CssClass="txt-email" />
                        <asp:Button Text=" Email Licenses " runat="server" ID="btnEmail" OnClientClick="return confirmEmail();" />
                    </td>
                </tr>
                <tr style="width: 100%;">
                    <td style="text-align: right;" colspan="2">
                        <asp:ImageButton ID="ibtnExportToExcel" ImageUrl="~/App_Static/images/excel.png" Width="28px" Height="25px" Style="margin-right: 3px;" ToolTip="Export To Excel" runat="server" />
                        <asp:ImageButton ID="ibtnExportToCSV" ImageUrl="~/App_Static/images/csv.png" Width="28px" Height="25px" Style="margin-right: 3px;" ToolTip="Export To CSV" runat="server" />
                    </td>
                </tr>
            </table>
    <asp:GridView ID="gvLicense" runat="server">
        <Columns>
            <asp:TemplateField>
                <ItemStyle Width="18px" />
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" CssClass="a16 lock16" Visible='<%# Eval("InUse") %>'
                        ToolTip="License Key in use" />
                    <asp:Label ID="Label2" runat="server" CssClass="a16 newborn16" Visible='<%# ((System.DateTime.UtcNow - CDate(Eval("CreatedDate"))).TotalSeconds < 30) And Not Eval("InUse") %>'
                        ToolTip="Newly created" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemStyle Width="18px" />
                <ItemTemplate>
                    <asp:CheckBox ID="chkSel" runat="server" Checked="false" CssClass="grid-selection" onchange="checkboxcheck()"/>
                    <asp:HiddenField ID="LID" runat="server" Value='<%# Eval("Id") %>' />
                    <asp:HiddenField ID="LKEY" runat="server" Value='<%# EvalLicenseKey() %>' />
                    <asp:HiddenField ID="OTP" runat="server" Value='<%# Eval("OTP") %>' />
                    <asp:HiddenField ID="DEVICE" runat="server" Value='<%# Eval("TargetDevice") %>' />
                </ItemTemplate>
                <HeaderTemplate>
                    <input type="checkbox" onchange='bulkSelect(this.checked);'  id="chkHead"/>
                </HeaderTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="TargetDevice" HeaderText="Device" ItemStyle-Width="100px" />
            <asp:TemplateField HeaderText="License Key">
                <ItemStyle CssClass="keys" Width="160px" />
                <ItemTemplate>
                    <pre style=""><%# EvalLicenseKey() %></pre>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="OTP" HeaderText="OTP" ItemStyle-Width="60px" ItemStyle-Font-Bold="true" />
            <asp:BoundField DataField="IssuedUnder" HeaderText="Issued For" ItemStyle-Width="250px" />
            <asp:BoundField DataField="RegisteredEmail" HeaderText="Registered Email" ItemStyle-Width="250px" />
            <asp:BoundField DataField="RegisteredNick" HeaderText="Nickname" ItemStyle-Width="120px" />
            <asp:BoundField DataField="LastUsedLoginId" HeaderText="CAMA User" ItemStyle-Width="90px"
                ItemStyle-CssClass="sf" />
                <asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy" ItemStyle-Width="80px" />
            <asp:TemplateField>
                <ItemStyle Width="60px" />
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="lbDelete" Text="Delete" CommandName="DeleteLicense"
                        CommandArgument='<%# Eval("Id") %>' OnClientClick='return confirm("Are you sure you want to delete this device license permanently?")' />&nbsp;
                    <asp:LinkButton runat="server" ID="lbRevoke" Text="Revoke" CommandName="RevokeLicense"
                        CommandArgument='<%# Eval("Id") %>' Visible='<%# Eval("InUse") %>' OnClientClick='return confirm("You are cancelling an installed device license and allowing it to be used again. Click OK to continue.")' />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            <div class="info">
                No licenses found.
            </div>
        </EmptyDataTemplate>
    </asp:GridView>
</asp:Content>
