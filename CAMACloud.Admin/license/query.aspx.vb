﻿Imports System.Net.Mail
Imports System.IO

Public Class query
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim scriptManager__1 As ScriptManager = ScriptManager.GetCurrent(Me.Page)
        scriptManager__1.RegisterPostBackControl(Me.ibtnExportToExcel)
        scriptManager__1.RegisterPostBackControl(Me.ibtnExportToCSV)
        If Not IsPostBack Then
            'Checking the Logged user and change the permissions
            If Session("loggedUserType") Is Nothing Then
                Alert("Session Experied, Please LogIn again")
                Response.Redirect("~/auth/Default.aspx")
            Else
                Dim loggedUserType As String = Session("loggedUserType").ToString()

                If loggedUserType = "support" Then
                    lbDeleteSelected.Visible = False

                    Dim lnkBtnDelete As LinkButton = New LinkButton()

                    For i As Integer = 0 To gvLicense.Rows.Count - 1
                        lnkBtnDelete = DirectCast(gvLicense.Rows(i).FindControl("lbDelete"), LinkButton)
                        lnkBtnDelete.Visible = False
                    Next

                    'gvLicense.Columns(1).Visible = False
                End If
            End If
        End If
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click
        Dim filter As String = ""
        filter = "RegisteredEmail LIKE '%" + txtKey.Text + "%' OR LicenseKey LIKE '%" + txtKey.Text.Trim.Replace(" ", "").Replace("-", "") + "%' OR RegisteredNick LIKE '%" + txtKey.Text + "%'"
        hdnFilter.Value = filter

        RefreshLicenseGrid()

        'Checking the Logged user and change the permissions
        If Session("loggedUserType") Is Nothing Then
            Alert("Session Experied, Please LogIn again")
            Response.Redirect("~/auth/Default.aspx")
        Else
            Dim loggedUserType As String = Session("loggedUserType").ToString()

            If loggedUserType = "support" Then
                lbDeleteSelected.Visible = False

                Dim lnkBtnDelete As LinkButton = New LinkButton()

                For i As Integer = 0 To gvLicense.Rows.Count - 1
                    lnkBtnDelete = DirectCast(gvLicense.Rows(i).FindControl("lbDelete"), LinkButton)
                    lnkBtnDelete.Visible = False
                Next

                'gvLicense.Columns(1).Visible = False
            End If
        End If
    End Sub

    Sub RefreshLicenseGrid()
        Dim filter As String = hdnFilter.Value
        Dim sql As String = String.Format("SELECT dl.*, CASE dl.DeviceFilter WHEN 'Chrome' THEN 'Console' WHEN 'MAChrome' THEN 'MA Chrome' WHEN 'MACommon' THEN 'MA Common'   ELSE DeviceFilter END As TargetDevice, o.Name As OrganizationName, v.Name As VendorName, CASE WHEN IsSuper = 1 THEN '[S] ' WHEN IsVendor = 1 THEN '[V] ' ELSE '' END + CASE WHEN v.Name IS NOT NULL THEN 'Vendor: ' + v.CommonName ELSE o.Name END AS IssuedUnder FROM DeviceLicense dl LEFT OUTER JOIN Organization o ON DL.OrganizationId = o.Id LEFT OUTER JOIN Vendor v ON  dl.VendorId = v.Id WHERE {0} ORDER BY LicenseKey", filter)
        Dim dt As DataTable = Database.System.GetDataTable(sql)
        Dim sql1 As String = String.Format("SELECT CASE dl.DeviceFilter WHEN 'Chrome' THEN 'Console' WHEN 'MAChrome' THEN 'MA Chrome' WHEN 'MACommon' THEN 'MA Common'  ELSE DeviceFilter END As Device, dl.LicenseKey AS [License Key],dl.OTP,CASE WHEN IsSuper = 1 THEN '[S] ' WHEN IsVendor = 1 THEN '[V] ' ELSE '' END + CASE WHEN v.Name IS NOT NULL THEN 'Vendor: ' + v.CommonName ELSE o.Name END AS [Issued For],dl.RegisteredEmail AS [Registered Email],dl.RegisteredNick AS NickName,dl.LastUsedLoginId As [CAMA User],dl.CreatedBy As [Created By] FROM DeviceLicense dl LEFT OUTER JOIN Organization o ON DL.OrganizationId = o.Id LEFT OUTER JOIN Vendor v ON  dl.VendorId = v.Id WHERE {0} ORDER BY LicenseKey", filter)
        Dim dt1 As DataTable = Database.System.GetDataTable(sql1)
        gvLicense.DataSource = dt
        gvLicense.DataBind()
        ViewState("dtData") = dt1

        If gvLicense.Rows.Count > 0 Then
            tLicenseOps.Visible = True
        Else
            tLicenseOps.Visible = False
        End If
    End Sub


    Public Function EvalLicenseKey() As String
        Dim key As String = Eval("LicenseKey")
        If key.Length = 16 Then
            key = key.Substring(0, 4) + "-" + key.Substring(4, 4) + "-" + key.Substring(8, 4) + "-" + key.Substring(12, 4)
        End If
        Return key
    End Function

    Protected Sub gvLicense_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvLicense.RowCommand
        Select Case e.CommandName
            Case "DeleteLicense"
                DeleteLicenses(e.CommandArgument)
            Case "RevokeLicense"
                RevokeLicenses(e.CommandArgument)
        End Select
    End Sub

    Protected Function GetSelectedIds() As String
        Dim selection As String = "0"
        For Each gr As GridViewRow In gvLicense.Rows
            If gr.GetChecked("chkSel") Then
                selection += "," + gr.GetHiddenValue("LID")
            End If
        Next
        Return selection
    End Function

    Protected Sub lbDeleteSelected_Click(sender As Object, e As System.EventArgs) Handles lbDeleteSelected.Click
        DeleteLicenses(GetSelectedIds)
    End Sub

    Protected Sub lbRevokeSelected_Click(sender As Object, e As System.EventArgs) Handles lbRevokeSelected.Click
        RevokeLicenses(GetSelectedIds)

        'Checking the Logged user and change the permissions
        If Session("loggedUserType") Is Nothing Then
            Alert("Session Experied, Please LogIn again")
            Response.Redirect("~/auth/Default.aspx")
        Else
            Dim loggedUserType As String = Session("loggedUserType").ToString()

            If loggedUserType = "support" Then
                lbDeleteSelected.Visible = False

                Dim lnkBtnDelete As LinkButton = New LinkButton()

                For i As Integer = 0 To gvLicense.Rows.Count - 1
                    lnkBtnDelete = DirectCast(gvLicense.Rows(i).FindControl("lbDelete"), LinkButton)
                    lnkBtnDelete.Visible = False
                Next

                'gvLicense.Columns(1).Visible = False
            End If
        End If
    End Sub

    Sub DeleteLicenses(ids As String)
    	Database.System.Execute("INSERT INTO LicenseAudittrail SELECT OrganizationId,'" + Membership.GetUser().ToString() +"' AS LoginId,GETUTCDATE() AS EventTime,DeviceFilter,LicenseKey,'" + HttpContext.Current.Request.ClientIPAddress + "' AS IPAddress,'License Key Removed' AS Comment FROM DeviceLicense WHERE Id IN (" & ids & ")" )
        Database.System.Execute("DELETE FROM DeviceLicense WHERE Id IN (" & ids & ")")
        RefreshLicenseGrid()
    End Sub

    Sub RevokeLicenses(ids As String)
        Database.System.Execute("UPDATE DeviceLicense SET InUse = 0, MachineKey = NULL, SessionKey = NULL WHERE Id IN (" & ids & ")")
        Database.System.Execute("INSERT INTO LicenseAudittrail SELECT OrganizationId,'" + Membership.GetUser().ToString() +"' AS LoginId,GETUTCDATE() AS EventTime,DeviceFilter,LicenseKey,'" + HttpContext.Current.Request.ClientIPAddress + "' AS IPAddress,'License Key Revoked' AS Comment FROM DeviceLicense WHERE Id IN (" & ids & ")" )
        RefreshLicenseGrid()
    End Sub

    Protected Sub btnEmail_Click(sender As Object, e As System.EventArgs) Handles btnEmail.Click
        CAMACloud.Security.DeviceLicense.SendLicenseEmail(Me, gvLicense, txtEmail.Text, "Device Access", False)
    End Sub
    Public Function ExportGridView(ByVal dt As DataTable, Optional ByVal formatColumn As String = Nothing, Optional ByVal format As String = Nothing) As GridView
        'Create a dummy GridView
        Dim GridView1 As New GridView()
        GridView1.AllowPaging = False
        GridView1.AutoGenerateColumns = False
        Dim bfieldName As String
        Dim bHeaderText As String
        GridView1.Columns.Clear()
        For Each column As DataColumn In dt.Columns
            bfieldName = column.ColumnName
            bHeaderText = column.ColumnName
            Dim bfield As New BoundField()
            bfield.HeaderText = bHeaderText
            bfield.DataField = bfieldName
            If (column.ColumnName = formatColumn) Then
                bfield.DataFormatString = format
            End If
            GridView1.Columns.Add(bfield)
        Next
        GridView1.DataSource = Nothing
        GridView1.DataSource = dt
        GridView1.DataBind()
        Return GridView1
    End Function
    Protected Sub ibtnExportToExcel_Click(sender As Object, e As System.EventArgs) Handles ibtnExportToExcel.Click
        Dim dt As DataTable = ViewState("dtData")
        Dim GridView1 As GridView = ExportGridView(dt)
        ExportToExcel(GridView1, "UserLicenseKeys.xls")
    End Sub
    Public Sub ExportToExcel(GridView1 As GridView, ByVal filename As String)
        Response.Clear()
        Response.Buffer = True
        Response.ClearContent()
        Response.ClearHeaders()
        Response.AddHeader("Content-Disposition", "attachment;filename=" + filename)
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim sw As New StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        GridView1.RenderControl(hw)
        ''style to format numbers to string
        'Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
        'Response.Write(style)
        Response.Output.Write(sw.ToString())
        Response.Flush()
        Response.End()
    End Sub
    Protected Sub ibtnExportToCSV_Click(sender As Object, e As System.EventArgs) Handles ibtnExportToCSV.Click
        Dim dt As DataTable = ViewState("dtData")
        Dim GridView1 As GridView = ExportGridView(dt)
        ExportToCSV(GridView1, "UserLicenseKeys.csv")
    End Sub

    Protected Sub ExportToCSV(ByVal GridView1 As GridView, ByVal filename As String)
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("Content-Disposition", "attachment;filename=" + filename)
        Response.Charset = ""
        Response.ContentType = "application/text"
        Dim sBuilder As StringBuilder = New System.Text.StringBuilder()
        For index As Integer = 0 To GridView1.Columns.Count - 1
            sBuilder.Append(GridView1.Columns(index).HeaderText + ","c)
        Next
        sBuilder.Append(vbCr & vbLf)
        For i As Integer = 0 To GridView1.Rows.Count - 1
            For k As Integer = 0 To GridView1.HeaderRow.Cells.Count - 1
                sBuilder.Append(GridView1.Rows(i).Cells(k).Text.Replace("&nbsp;", "") + ",")
            Next
            sBuilder.Append(vbCr & vbLf)
        Next
        Response.Output.Write(sBuilder.ToString())
        Response.Flush()
        Response.[End]()
    End Sub
End Class