﻿Imports System.Net.Mail
Imports CAMACloud.Security
Imports System.IO

Public Class superaccess
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim scriptManager__1 As ScriptManager = ScriptManager.GetCurrent(Me.Page)
        scriptManager__1.RegisterPostBackControl(Me.ibtnExportToExcel)
        scriptManager__1.RegisterPostBackControl(Me.ibtnExportToCSV)
        If Not IsPostBack Then
        	
        	ddlNumber.FillNumbers(1, 3)
            RefreshLicenseGrid()

            'Checking the Logged user and change the permissions
            If Session("loggedUserType") Is Nothing Then
                Alert("Session Experied, Please LogIn again")
                Response.Redirect("~/auth/Default.aspx")
            Else
                Dim loggedUserType As String = Session("loggedUserType").ToString()
                
                If loggedUserType = "support" Then
                    lbDeleteSelected.Visible = False

                    Dim lnkBtnDelete As LinkButton = New LinkButton()

                    For i As Integer = 0 To gvLicense.Rows.Count - 1
                        lnkBtnDelete = DirectCast(gvLicense.Rows(i).FindControl("lbDelete"), LinkButton)
                        lnkBtnDelete.Visible = False
                    Next

                    'gvLicense.Columns(0).Visible = False
                End If
            End If
        End If
    End Sub

    Private Sub bindOrganizations()
        'ddlOrganization.FillFromSqlWithDatabase(Database.System, "SELECT Id, Name FROM Organization ORDER BY Name", True)

    End Sub    
   
   Private Sub RefreshLicenseGrid() Handles ddlUsageStatus.SelectedIndexChanged, btnRefresh.Click
   		trNewLicense.Visible = True
        trLicenseStatus.Visible = True
        tLicenseOps.Visible = True
        gvLicense.Visible = True
        lbRevokeSelected.Visible = ddlUsageStatus.SelectedValue <> "0"
        Dim inUse As String = IIf(ddlUsageStatus.SelectedValue <> "", " AND InUse = " + ddlUsageStatus.SelectedValue, "")
        Dim sql As String = String.Format("SELECT *, 'All' As TargetDevice FROM DeviceLicense WHERE IsVendor = 0 AND (OrganizationId  IS NULL OR IsSuper = 1) " + inUse + " ORDER BY InUse DESC, CreatedDate")
        gvLicense.DataSource = Database.System.GetDataTable(sql)
        gvLicense.DataBind()
        Dim sql1 As String = String.Format("SELECT LicenseKey AS [License Key], SCODE, OTP, LastUsedLoginId AS [CAMA User],LastUsedIPAddress AS [IP Address],RegisteredEmail AS[Registered Email],RegisteredNick AS [Nickname],(Select FORMAT(LastAccessTime,'MM-dd-yyyy hh:mm tt')) AS [Last Access],(Select FORMAT(CreatedDate,'MM-dd-yyyy hh:mm:s tt')) AS [Created Date],CreatedBy AS [Created By]  FROM DeviceLicense WHERE IsVendor = 0 AND (OrganizationId  IS NULL OR IsSuper = 1) " + inUse + " ORDER BY InUse DESC, CreatedDate")
        Dim dt1 As DataTable = Database.System.GetDataTable(sql1)
	        ViewState("dtData1") = dt1
	        	runscript("hideMask();")
	        If gvLicense.Rows.Count > 0 Then
            tLicenseOps.Visible = True
        Else
            tLicenseOps.Visible = False
        End If

        'Checking the Logged user and change the permissions
        If Session("loggedUserType") Is Nothing Then
            Alert("Session Experied, Please LogIn again")
            Response.Redirect("~/auth/Default.aspx")
        Else
            Dim loggedUserType As String = Session("loggedUserType").ToString()

            If loggedUserType = "support" Then
                lbDeleteSelected.Visible = False

                Dim lnkBtnDelete As LinkButton = New LinkButton()

                For i As Integer = 0 To gvLicense.Rows.Count - 1
                    lnkBtnDelete = DirectCast(gvLicense.Rows(i).FindControl("lbDelete"), LinkButton)
                    lnkBtnDelete.Visible = False
                Next

                'gvLicense.Columns(0).Visible = False
            End If
        End If 
   End Sub

    Protected Sub btnGenerateLicense_Click(sender As Object, e As System.EventArgs) Handles btnCreate.Click
        For i = 1 To CInt(ddlNumber.SelectedValue)
        	Dim license As String = DeviceLicense.GenerateNewLicenseKey(16)
            Dim otp As Integer = DeviceLicense.GenerateNewOTP()
            Dim sql As String = String.Format("INSERT INTO DeviceLicense (OrganizationId,LicenseKey,OTP,CreatedDate,CreatedBy, IsSuper) VALUES(NULL,'{0}',{1},'{2}','{3}', 1)Select @@IDENTITY", license, otp, Date.UtcNow.ToString("yyyy-MM-dd HH:mm:ss"),Membership.GetUser().ToString())
            Dim dvlId = Database.System.GetIntegerValue(sql)
            Dim Scode As String = DeviceLicense.GenerateScode(dvlId)
            Database.System.Execute("UPDATE DeviceLicense SET SCODE = '" + Scode + "' WHERE Id = " + dvlId.ToString())
            Database.System.Execute("INSERT INTO LicenseAudittrail SELECT OrganizationId,'" + Membership.GetUser().ToString() +"' AS LoginId,GETUTCDATE() AS EventTime,DeviceFilter,LicenseKey,'" + HttpContext.Current.Request.ClientIPAddress + "' AS IPAddress,'New License Key generated' AS Comment FROM DeviceLicense where id = "+dvlId.ToString()  )    
        Next
        ddlUsageStatus.SelectedValue = 0
        RefreshLicenseGrid()

        'Checking the Logged user and change the permissions
        If Session("loggedUserType") Is Nothing Then
            Alert("Session Experied, Please LogIn again")
            Response.Redirect("~/auth/Default.aspx")
        Else
            Dim loggedUserType As String = Session("loggedUserType").ToString()

            If loggedUserType = "support" Then
                lbDeleteSelected.Visible = False

                Dim lnkBtnDelete As LinkButton = New LinkButton()

                For i As Integer = 0 To gvLicense.Rows.Count - 1
                    lnkBtnDelete = DirectCast(gvLicense.Rows(i).FindControl("lbDelete"), LinkButton)
                    lnkBtnDelete.Visible = False
                Next

                'gvLicense.Columns(0).Visible = False
            End If
        End If
    End Sub

    Public Function EvalLicenseKey() As String
        Dim key As String = Eval("LicenseKey")
        If key.Length = 16 Then
            key = key.Substring(0, 4) + "-" + key.Substring(4, 4) + "-" + key.Substring(8, 4) + "-" + key.Substring(12, 4)
        End If
        Return key
    End Function

    Protected Sub gvLicense_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvLicense.RowCommand
        Select Case e.CommandName
            Case "DeleteLicense"
                DeleteLicenses(e.CommandArgument)
            Case "RevokeLicense"
                RevokeLicenses(e.CommandArgument)
        End Select
    End Sub

    Protected Function GetSelectedIds() As String
        Dim selection As String = "0"
        For Each gr As GridViewRow In gvLicense.Rows
            If gr.GetChecked("chkSel") Then
                selection += "," + gr.GetHiddenValue("LID")
            End If
        Next
        Return selection
    End Function

    Protected Sub lbDeleteSelected_Click(sender As Object, e As System.EventArgs) Handles lbDeleteSelected.Click
    	DeleteLicenses(GetSelectedIds)
    End Sub

    Protected Sub lbRevokeSelected_Click(sender As Object, e As System.EventArgs) Handles lbRevokeSelected.Click
        RevokeLicenses(GetSelectedIds)
    End Sub

    Sub DeleteLicenses(ids As String)
    	Database.System.Execute("INSERT INTO LicenseAudittrail SELECT OrganizationId,'" + Membership.GetUser().ToString() +"' AS LoginId,GETUTCDATE() AS EventTime,DeviceFilter,LicenseKey,'" + HttpContext.Current.Request.ClientIPAddress + "' AS IPAddress,'License Key Removed' AS Comment FROM DeviceLicense WHERE Id IN (" & ids & ")" )
        Database.System.Execute("DELETE FROM DeviceLicense WHERE Id IN (" & ids & ")")
        RefreshLicenseGrid()
    End Sub

    Sub RevokeLicenses(ids As String)
        Database.System.Execute("UPDATE DeviceLicense SET InUse = 0, MachineKey = NULL, SessionKey = NULL WHERE Id IN (" & ids & ")")
        Database.System.Execute("INSERT INTO LicenseAudittrail SELECT OrganizationId,'" + Membership.GetUser().ToString() +"' AS LoginId,GETUTCDATE() AS EventTime,DeviceFilter,LicenseKey,'" + HttpContext.Current.Request.ClientIPAddress + "' AS IPAddress,'License Key Revoked' AS Comment FROM DeviceLicense WHERE Id IN (" & ids & ")" )
        RefreshLicenseGrid()
    End Sub

    'Protected Sub ddlOrganization_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlOrganization.SelectedIndexChanged
    '    ddlUsageStatus.SelectedValue = "0"
    '    RefreshLicenseGrid()
    'End Sub

    Protected Sub btnEmail_Click(sender As Object, e As System.EventArgs) Handles btnEmail.Click
        CAMACloud.Security.DeviceLicense.SendLicenseEmail(Me, gvLicense, txtEmail.Text, "Super Administration", False)
    End Sub



    Protected Sub RefreshLicenseGrid(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUsageStatus.SelectedIndexChanged, btnRefresh.Click
    	
    End Sub
    
    Protected Sub ibtnExportToExcel_Click(sender As Object, e As System.EventArgs) Handles ibtnExportToExcel.Click
        Dim dt As DataTable = ViewState("dtData1")
        Dim GridView1 As GridView = ExportGridView(dt, "Last Access", "{0:MM/dd/yyyy h:mm tt}","Created Date", "{0:MM/dd/yyyy h:mm tt}")
        ExportToExcel(GridView1, "SuperAdminKeys.xlsx")

    End Sub

    Public Function ExportGridView(ByVal dt As DataTable, Optional ByVal formatColumn As String = Nothing, Optional ByVal format As String = Nothing,Optional ByVal formatColumnCreateDt As String = Nothing, Optional ByVal formatCreateDt As String = Nothing) As GridView
        'Create a dummy GridView
        Dim GridView1 As New GridView()
        GridView1.AllowPaging = False
        GridView1.AutoGenerateColumns = False
        Dim bfieldName As String
        Dim bHeaderText As String
        GridView1.Columns.Clear()
        For Each column As DataColumn In dt.Columns
            bfieldName = column.ColumnName
            bHeaderText = column.ColumnName
            Dim bfield As New BoundField()
            Select Case bfieldName
                Case "License Key"
                    bfield.ItemStyle.Width = 250
                Case "OTP"
                    bfield.ItemStyle.Width = 80
                Case "SCODE"
                    bfield.ItemStyle.Width = 80
                Case "RegisteredEmail"
                    bfield.ItemStyle.Width = 250
                Case "CreatedDate"
                    bfield.ItemStyle.Width = 180
                Case Else
                    bfield.ItemStyle.Width = 120

            End Select
            bfield.HeaderText = bHeaderText
            bfield.DataField = bfieldName
            If (column.ColumnName = formatColumn) Then
            	bfield.DataFormatString = format
            ElseIf (column.ColumnName = formatColumnCreateDt) Then
            	bfield.DataFormatString = formatCreateDt   	
            
            End If
            GridView1.Columns.Add(bfield)
        Next
        GridView1.DataSource = Nothing
        GridView1.DataSource = dt
        GridView1.DataBind()
        Return GridView1
    End Function

    Public Sub ExportToExcel(GridView1 As GridView, ByVal filename As String)
        Response.Clear()
        Response.Buffer = True
        Response.ClearContent()
        Response.ClearHeaders()
        Response.AddHeader("Content-Disposition", "attachment;filename=" + filename)
        Response.Charset = ""
        Response.ContentType = "application/vnd.openxml.formats-officedocument.spreadsheetml.sheet"
        Dim excelStream = ExcelGenerator.ExportGrid(GridView1, ViewState("dtData1"), "Super Admin Keys", "Super Admin Keys")
        excelStream.CopyTo(Response.OutputStream)
        Response.End()
    End Sub
    Protected Sub ibtnExportToCSV_Click(sender As Object, e As System.EventArgs) Handles ibtnExportToCSV.Click
        Dim dt As DataTable = ViewState("dtData1")
        Dim GridView1 As GridView = ExportGridView(dt, "Last Access", "{0:MM/dd/yyyy h:mm tt}")
        ExportToCSV(GridView1, "SuperAdminKeys.csv")
    End Sub

    Protected Sub ExportToCSV(ByVal GridView1 As GridView, ByVal filename As String)
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("Content-Disposition", "attachment;filename=" + filename)
        Response.Charset = ""
        Response.ContentType = "application/text"
        Dim sBuilder As StringBuilder = New System.Text.StringBuilder()
        For index As Integer = 0 To GridView1.Columns.Count - 1
            sBuilder.Append(GridView1.Columns(index).HeaderText + ","c)
        Next
        sBuilder.Append(vbCr & vbLf)
        For i As Integer = 0 To GridView1.Rows.Count - 1
            For k As Integer = 0 To GridView1.HeaderRow.Cells.Count - 1
                sBuilder.Append(GridView1.Rows(i).Cells(k).Text.Replace("&nbsp;", "") + ",")
            Next
            sBuilder.Append(vbCr & vbLf)
        Next
        Response.Output.Write(sBuilder.ToString())
        Response.Flush()
        Response.[End]()
    End Sub
End Class