﻿Public Class _default1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadGrid()
        End If
    End Sub
    Protected Sub results_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles results.PageIndexChanging
        LoadGrid(e.NewPageIndex)
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        LoadGrid()
    End Sub
    Sub LoadGrid(Optional ByVal pageIndex As Integer = 0)
        Dim sqlFilter As String = ""
        If txtName.Text.Trim <> "" Then
            sqlFilter = "Where Name LIKE '%{0}%'".FormatString(txtName.Text.Trim.ToSqlValue.Trim("'"))
        End If
        results.PageIndex = pageIndex
        results.DataSource = Database.System.GetDataTable("SELECT * FROM dbo.Vendor " + sqlFilter)
        results.DataBind()
    End Sub

End Class
