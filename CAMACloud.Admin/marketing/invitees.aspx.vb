﻿Imports System.Net.Mail

Partial Class marketing_invitees
	Inherits System.Web.UI.Page

	Public ReadOnly Property GroupId As Integer
		Get
			If Request("uid") Is Nothing Then
				Response.Redirect("campaigns.aspx")
				Return -1
			End If
			Return Database.System.GetIntegerValue("SELECT Id FROM InvitationGroup WHERE UID = " + Request("uid").ToSqlValue)
		End Get
	End Property

	Sub LoadGrid()
		grid.DataSource = Database.System.GetDataTable("SELECT * FROM InviteeLicenses WHERE InvitationGroupId = " & GroupId & " ORDER BY COALESCE(AcceptedDate, '2099-12-31'), Email")
		grid.DataBind()
	End Sub

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
			LoadGrid()
			lblLink.Text = "http://sv.camacloud.com/auth/acceptinvitation.aspx?uid=" + Request("uid")
			lblLink.NavigateUrl = "http://sv.camacloud.com/auth/acceptinvitation.aspx?uid=" + Request("uid")
		End If
	End Sub

	Protected Sub grid_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grid.PageIndexChanging
		grid.PageIndex = e.NewPageIndex
		LoadGrid()
	End Sub

	Protected Sub btnAdd_Click(sender As Object, e As System.EventArgs) Handles btnAdd.Click
		Dim gid As Integer = GroupId
		Dim errorMails As New List(Of String)
		Dim duplicates As New List(Of String)
		Dim processedCount As Integer = 0
		If txtEmails.Text.Trim <> "" Then
			Dim emailString = txtEmails.Text.Trim
			Dim lines = emailString.Split(vbLf)
			For Each line In lines
				Dim tline = line.Trim
				Dim emails() As String = tline.Split(" ,;".ToCharArray)
				For Each email As String In emails
					If email.Trim <> "" Then
						Try
							Dim m As New MailAddress(email)
							If Database.System.GetIntegerValue("SELECT COUNT(*) FROM InviteeLicenses WHERE Email = {0} AND InvitationGroupId = {1}".SqlFormat(False, email, gid)) > 0 Then
								duplicates.Add(email)
							Else
								Dim sql As String = "INSERT INTO InviteeLicenses (Email, InvitationGroupId) VALUES ({0}, {1})".SqlFormat(False, email, gid)
								Database.System.Execute(sql)
								processedCount += 1
							End If
						Catch ex As Exception
							errorMails.Add(email)
						End Try
					End If
					
				Next
			Next

			txtEmails.Text = ""

			Dim message As String = ""
			If duplicates.Count > 0 Then
				message = duplicates.Count & " duplicated entries are ignored. "
			End If
			If errorMails.Count > 0 Then
				message += errorMails.Count & " entries contain errors and are left in the input text area for correction. "
				txtEmails.Text = String.Join(vbCrLf, errorMails.ToArray)
			End If
			If processedCount > 0 Then
				message += processedCount & " records added into invitees list. "
			End If

			Alert(message)
			LoadGrid()
		End If
	End Sub
End Class
