﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ClientAdmin.master" CodeBehind="restoredatabase.aspx.vb" Inherits="CAMACloud.Admin.restoredatabase" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<script type="text/javascript" src="/App_Static/jslib/jquery-ui-1.8.20.custom.min.js"></script>
<link href="/App_Static/css/select2/select2.css" rel="stylesheet" />
<script type="text/javascript" src="/App_Static/js/select2/select2.js"></script>

     <style type="text/css">
        .tblEnvironment { width: 100%; }
        .txtLSide { width: 98%;color:initial } .txtRSide { width: 98%;color:initial }
        .ddlLSide { width: 50%; } .ddlRSide { width: 70%; }
        .lLabel { width: 18%;font-weight: bold;color:initial} .RLabel { width: 15%;font-weight: bold;color:black}
        .txtArea {width: 100%;}
        .txtLContainer { width: 35%;} .txtRContainer { width: 31%;}
        .trHeight { height: 25px}
        .ddlRepeat { width: 30%; } .ddlDay { width: 31%; } .ddlTime { width: 25%; }
        .txtAlign { margin-bottom: 2%; }
        .lblAlign { margin-bottom: 2%; }
        .dropFiled { color: initial; font: 13.3333px Arial; }
        .progress {height: 20px; margin-bottom: 20px; overflow: hidden;background-color: #f5f5f5; border-radius: 4px;}
        .progress-bar{float: left; width: 0%; height: 100%; font-size: 12px; line-height: 20px; color: #fff;text-align: center; background-color: #337ab7;box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);transition: width .6s ease;}
        .update-progress {margin-top: 1%; margin-right: 20.5%; margin-left: 12.5%;}
		.box {border:2px solid #0094ff;}
		.box h2 {background:#0094ff;color:white;padding:10px;}
		.box p {color:#333;padding:10px;}
		.box {
		    -moz-border-radius-topright:5px;
		    -moz-border-radius-topleft:5px;
		    -webkit-border-top-right-radius:5px;
		    -webkit-border-top-left-radius:5px;
		}
		.red {color:#ff0000;}
		.RBL label
		{
		   display: block;
		}

		.RBL td
		{
		    text-align: center;
		    width: 20px;
		}
		#progressBar {
			background: #dcdcdc;
			height: 15px;
		}
		#progressBar2 {
			background: #dcdcdc;
			height: 15px;
		}
		.masklayer {
		    background: black;
		    opacity: 0.5;
		    top: 0%;
		    width: 100%;
		    height: 100%;
		    bottom: 0%;
		    position: absolute;
		    display: none;
		}
    </style>
    <script  type="text/javascript">
		$(document).ready(function () {
        $("#ddlSEnvironment, #ddlDEnvironment").select2();
	    $("#<%=txtRequestBy.ClientID%>").keyup(function(){
	    	var name=$("#<%=txtRequestBy.ClientID%>").val();
		    if(name.length > 0){
		    	$('#<%=txtRequestBy.ClientID%>').next(".red").remove();
		    };
		});
        
	 	 $("#<%=txtRequestedBy.ClientID%>").keyup(function(){
	    	var name=$("#<%=txtRequestedBy.ClientID%>").val();
		    if(name.length > 0){
		    	$('#<%=txtRequestedBy.ClientID%>').next(".red").remove();
		    };
	 	});
  	});
  	 
                	  
	    function ConfirmRestore() {	
    		
		 	
    		var gv = $("#<%=gdvUserBackUpList.ClientID%>");
    		var chk = $('input[type="checkbox"]:checked', gv);
    		if(chk.length ==0){
    		alert('Select a backup file');
    		return false;
    		}
    		var name=$("#<%=txtRequestBy.ClientID%>").val();
			if(name.length == 0){
			   alert('Fill the required fields')
			   $('#nameSpan').remove();
			   $('#<%=txtRequestBy.ClientID%>').after('<span id ="nameSpan" class="red" style ="margin-left:5px;">Name is Required</span>');
			   return false;
			    }
			 else {
			 	$('#<%=txtRequestBy.ClientID%>').next(".red").remove();
			    var Destination = $( "#<%=HdfDestination.ClientID%>" ).val();
		    	var Source = $( "#<%=HdfSource.ClientID%>" ).val();
		    	if(window.confirm('Do you want to restore '+ Destination +' with ' + Source)){
			       $( "#<%=HiddenField1.ClientID%>" ).val( location.href );
					var v = "1"
                	showMask()
                
              		Status(v);
                	 
					return true;
					
			    }
			    else{
					return false;
				}
			}
    	}
    	function showPopup()
    	{
    		var Des = $( "#<%=HdfDestination.ClientID%>" ).val();
		    var Sour = $( "#<%=HdfSource.ClientID%>" ).val();
		    var name=$("#<%=txtRequestedBy.ClientID%>").val();
		    if(name.length == 0){
			   alert('Fill the required fields')
			   $('#nameSpan').remove();
			   $('#<%=txtRequestedBy.ClientID%>').after('<span id ="nameSpan" class="red" style ="margin-left:5px;">Name is Required</span>');
			   return false;
			    }
			 else {
			 	$('#<%=txtRequestedBy.ClientID%>').next(".red").remove();
    		if(window.confirm('Do you want to restore '+ Des +' with ' + Sour)){
			       $( "#<%=HiddenField1.ClientID%>" ).val( location.href );
					showMask();
					BacUPStatus("1");
					return true;
			    }
			    else{
					return false;
				}
    	
    		}
    	}
    	
	function CheckBoxCheck(rb) {    	
        var gv = $("#<%=gdvUserBackUpList.ClientID%>");
        var chk = $('input[type="checkbox"]', gv);
        $(chk).removeAttr('checked');
        $(rb).attr('checked','checked');
    } 
    function Status(va,BackendCall)
        {
        	if(BackendCall == 1){
        		
        		var file = $('#<%=HdnFileName.ClientID%>').val();
        		findgridcheckBox(file);
        	}
        	var gv = $("#<%=gdvUserBackUpList.ClientID%>");
        	var chk = $('input[type="checkbox"][checked="checked"]', gv);
        	var value = $('td', $(chk).parents('tr').eq(0)).eq(3).html();
        	
        	var Destination = $( "#<%=HdfDestinationDbId.ClientID%>" ).val();
        	 var Source = $( "#<%=HdfSourceDbId.ClientID%>" ).val();
        	 var request = $("#<%=txtRequestBy.ClientID%>").val();
        	 var b = va
        	 var dt = new Date();
				var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
			showMask();
        	$.ajax({
	       		url: '/admin/oldbackup.jrq',
	       		type:'POST',
	       		dataType: 'json',
	       		data:{'DesDB': Destination,'SourDB':Source,'ZipFileName':value,'Requested':request,'opVariable':b,},
	       		async: true,
	       		success: function(Result){
	       		
		       		var resultArray = Result.message.split(',')
		       		if(resultArray[1] == 'notcompleted'){
	          	 		procProgress(resultArray[0]);
	          	 		$('#restoreSpan').html('Restoration is going on...');
		       			Status("0");
		       			
		       		}
		       		
		       		else if(resultArray[1] == 'completed'){
		       		alert('Database Restored successfully.');
		       		$('#<%=ddlDEnvironment.ClientID%> option:eq(0)').attr('selected','selected');
	    			$('#<%=ddlSEnvironment.ClientID%> option:eq(0)').attr('selected','selected');
	    			$('#mainDiv').hide();
	    			hideMask();
	    			$("#<%=txtRequestedBy.ClientID%>").val('');
	    			$("#<%=txtRequestBy.ClientID%>").val('');
		       		}
		       		else if(resultArray[1] == 'error'){
		       		alert('Restoration operation failed \n Error message:' +resultArray[2]);
		       		$('#<%=ddlDEnvironment.ClientID%> option:eq(0)').attr('selected','selected');
	    			$('#<%=ddlSEnvironment.ClientID%> option:eq(0)').attr('selected','selected');
	    			$('#mainDiv').hide();
	    			hideMask();
	    			$("#<%=txtRequestedBy.ClientID%>").val('');
	    			$("#<%=txtRequestBy.ClientID%>").val('');
		       		}
		      
	       		},
	       		error: function(err){
	       		alert(err.message);
	       		}
	       	}) 
        
        } 
       
        function BacUPStatus(bst,test){
        	var Destination = $( "#<%=HdfDestinationDbId.ClientID%>" ).val();
        	var Source = $( "#<%=HdfSourceDbId.ClientID%>" ).val();
        	var request = $("#<%=txtRequestedBy.ClientID%>").val();
        	var b = bst;
        	showMask();
        	$.ajax({
        		url: '/admin/newbackup.jrq',
	       		type:'POST',
	       		dataType: 'json',
	       		data:{'DesDB': Destination,'SourDB':Source,'opVariable':b,'RequestedBy':request},
	       		async: true,
	       		success: function(Result){
	       		var resultArray = Result.message.split(',')
	       		console.log(Result.message);
	       		if(resultArray[1] == 'notcompleted'){
	          	 		procProgress(resultArray[0]);
	          	 		$('#restoreSpan').html('Backup is going on...');
		       			BacUPStatus("0");
		       			
		       	}
		       	else if(resultArray[1] == 'restoreotcompleted'){
		       		
		       		procProgress2(resultArray[0]);
		       		$('#restoreSpan').html('Backup completed');
		       		$('#restoreSpan2').html('Restoration is going on...');
		       		BacUPStatus("0");
		       	}
		       	else if(resultArray[1] == 'completed'){
		       	alert('Database Restored successfully.');
		       	$('#<%=ddlDEnvironment.ClientID%> option:eq(0)').attr('selected','selected');
	    		$('#<%=ddlSEnvironment.ClientID%> option:eq(0)').attr('selected','selected');
	    		$('#mainDiv').hide();
	    		hideMask();
	    		$("#<%=txtRequestedBy.ClientID%>").val('');
	    		$("#<%=txtRequestBy.ClientID%>").val('');
		       	}
		       	else if(resultArray[1] == 'error'){
		       		alert('Restoration operation failed.\n Error message:' +resultArray[2]);
		       		$('#<%=ddlDEnvironment.ClientID%> option:eq(0)').attr('selected','selected');
	    			$('#<%=ddlSEnvironment.ClientID%> option:eq(0)').attr('selected','selected');
	    			$('#mainDiv').hide();
	    			hideMask();
	    			$("#<%=txtRequestedBy.ClientID%>").val('');
	    			$("#<%=txtRequestBy.ClientID%>").val('');
		       		}
		       
	       		},
	       		error: function(err){
	       		alert(err.message);
	       		
	       		}
	       	}) 
	       	
        }
        
        
		function showMask(info, error, errorActionName, callback) {
			$('.masklayer').height($(document).height())
		    $('.masklayer').show();
		    if (!info)
		        info = 'Please wait ...'
		    var helplink = ''
		    if (callback && errorActionName) {
		        helplink = '<a href="#" class="app-mask-error-help-link">Click here<a/> to ' + errorActionName;
		    }
		    if (error) {
		        info = '<span style="color:Red;">' + info + '</span> ' + helplink;
		    }
		
		    $('.app-mask-layer-info').html(info);
		}
		function procProgress(progress)
        {
            $( '#progressBar' ).show();
            $( '#currentProgress' ).width( progress + '%' );
            $('#TimeSpan').html(progress + '%');
            
        } 
        function procProgress2(progress)
        {
            $( '#progressBar2' ).show();
            $( '#currentProgress2' ).width( progress + '%' );
            $('#TimeSpan2').html(progress + '%');
        } 
        function CancelOperation(){
        	if(window.confirm('Do you want to cancel')){
        		location.href= $('#<%=HiddenField1.ClientID%>').val();
        		return true;
        	}
        	else{
        	return false;
        	}
        	
        }
        function findgridcheckBox(backupFile){
         	$("#<%=gdvUserBackUpList.ClientID%> tr:has(td)").each(function() {
			var cell = $(this).find("td:eq(3)");
      		if(cell.html() == backupFile){
      		var chk = $(this).find('input[type="checkbox"]')
      		 $(chk).attr('checked','checked');
      		}

        });
	    } 
	   function hideMask() {
	    $('.masklayer').hide();
	}
	    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Restore Environment</h1>
    <div style ="width:100%" id ="Step1" runat="server" visible="false">
    <div style="width: 95%;">
        <table class="tblEnvironment">
            <tr class="trHeight">
                <td class="lLabel">Destination Environment</td>
                <td class="txtContainer">
                    <asp:DropDownList ID="ddlDEnvironment" runat="server" AutoPostBack="True" CssClass="ddlDEnvironment ddlLSide txtAlign dropFiled" ClientIDMode="Static"></asp:DropDownList></td>
                <td class="RLabel">Source Environment</td>
                <td class="txtRContainer">
                    <asp:DropDownList ID="ddlSEnvironment" runat="server" AutoPostBack="True" CssClass="ddlSEnvironment  ddlRSide txtAlign dropFiled" ClientIDMode="Static"></asp:DropDownList></td>
            </tr>

        </table>
         <asp:HiddenField runat="server" ID="HiddenField1" Value="" />
        <asp:HiddenField runat="server" ID="HdfDestination" Value="" />
        <asp:HiddenField runat="server" ID="HdfDestinationDbId" Value="" />
        <asp:HiddenField runat="server" ID="HdfSourceDbId" Value="" />
        <asp:HiddenField runat="server" ID="HdfSource" Value="" />
        <asp:HiddenField runat="server" ID="HdnFileName" Value="" />
    </div>
    <div id ="mainDiv">
      <div style="width: 95%;"  runat="server" id="destinationInfo" visible="false">
        <fieldset>
            <legend style="color: blue; font-weight: bold;">Destination Information</legend>
            <table class="tblEnvironment">
                <tr class="trHeight">
                    <td class="lLabel">Database Name</td>
                    <td class="txtContainer">
                        <asp:Label ID="lblDBName" runat="server" Text="" CssClass=" txtRSide lblAlign"></asp:Label></td>
                    <td class="RLabel">Source DataBase Name</td>
                    <td class="txtRContainer">
                        <asp:Label ID="lblSourceName" runat="server" Text="" CssClass=" txtLSide lblAlign"></asp:Label>
                </tr>
                <tr class="trHeight">
                    <td class="lLabel">Requested Date</td>
                    <td class="txtContainer">
                        <asp:Label ID="lblRequestedDate" runat="server" Text="" CssClass=" txtRSide lblAlign"></asp:Label></td>
                    <td class="RLabel">Requested By</td>
                    <td class="txtRContainer">
                        <asp:Label ID="lblRequestedBy" runat="server" Text="" CssClass=" txtLSide lblAlign"></asp:Label>
                </tr>
                <tr class="trHeight">
                	<td class="lLabel">DB Server</td>
                    <td class="txtContainer">
                        <asp:Label ID="lblDbServer" runat="server" Text="" CssClass=" txtRSide lblAlign"></asp:Label></td>
                   
                    <td class="lLabel">Status</td>
                    <td class="txtContainer">
                        <asp:Label ID="lblStatus" runat="server" Text="" CssClass=" txtRSide lblAlign"></asp:Label></td>  
                </tr>
            </table>
        </fieldset>
    </div>
     <%--<asp:Button ID="BtnDelete" Text="Delete Sample" runat="server" style ="float:right;margin-right:53px;margin-top:10px" visible ="false" ></asp:Button>--%>
    
    
     <div style="width: 95%;" runat="server" id="sourceInfo" visible="false">
        <fieldset>
            <legend style="color: blue; font-weight: bold;">Source Information</legend>
            <table class="tblEnvironment">
                <tr class="trHeight">
                    <td class="lLabel">Database Name</td>
                    <td class="txtContainer">
                        <asp:Label ID="lblSDBName" runat="server" Text="" CssClass=" txtRSide lblAlign"></asp:Label></td>
                    <td class="RLabel">Database Size</td>
                    <td class="txtRContainer">
                        <asp:Label ID="lblSDbSize" runat="server" Text="" CssClass=" txtLSide lblAlign"></asp:Label>
                </tr>
                <tr class="trHeight">
                    <td class="lLabel">DB Server</td>
                    <td class="txtContainer">
                        <asp:Label ID="lblServer" runat="server" Text="" CssClass=" txtRSide lblAlign"></asp:Label></td>
                    <td class="RLabel">Backup Date</td>
                    <td class="txtRContainer">
                        <asp:Label ID="lblSBackupDate" runat="server" Text="" CssClass=" txtLSide lblAlign"></asp:Label>
                </tr>
                <tr class="trHeight">
                    <td class="lLabel">Free Space in Drive</td>
                    <td class="txtContainer">
                        <asp:Label ID="lblFreeSpace" runat="server" Text="" CssClass=" txtRSide lblAlign"></asp:Label></td>
                   
            </table>
        </fieldset>
    </div>
    
     <div style="width: 95%;" runat="server" id="BackUpSelection" visible ="false">
        
    	<asp:RadioButtonList ID="RblBackupSelection" runat="server" RepeatDirection="Horizontal" TextAlign="right" AutoPostBack = "True">
		    <asp:ListItem Text="Restore with Old BackUp" Value="old" class="lLabel" style = "margin-left:5px"/>
		    <asp:ListItem Text="Restore with New BackUp" Value="new" class="lLabel"/>
		</asp:RadioButtonList>
		
    </div> 
   
   <div id="bakDiv" runat="server" visible = "False" style ="margin-top:25px;width: 90%">
	 	<table class="tblEnvironment">
                <tr class="trHeight">
                    <td width ="12%" style ="color:black">Restore Requested By:</td>
                    <td width ="50%">
                    <asp:TextBox id="txtRequestedBy"  runat="server" ></asp:TextBox></td>
                    
                </tr>
                <tr class="trHeight" width = "90%">
                    <td class="txtContainer" colspan = "3" style = "padding-left:70%">
	 				<input type="button" value="Cancel" onClick ="return CancelOperation()" style ="float:right;margin-top:10px;margin-left:5px" >
	 				<input type="button" value="BackUp and Restore" onClick ="return showPopup()" style ="float:right;margin-top:10px">
	 				</td>
	 			</tr>
	 	</table>
	</div>	 	


<div style ="width:95%" id="grdvDiv" runat="server" visible = "false">
		<asp:UpdatePanel ID="UpdatePanelgrd" runat="server">
          <ContentTemplate>
 		  <asp:Panel>
			<asp:GridView runat="server" ID="gdvUserBackUpList" AllowPaging="true" PageSize="5" Width="95%"  >
				<Columns>
					<asp:TemplateField ItemStyle-Width = "25px">
					<ItemTemplate >
					    <asp:CheckBox ID="chkBakList" runat="server"  class='cbVersionControl'  onclick = "CheckBoxCheck(this);" />
					    <asp:HiddenField ID="HiddenField2" runat="server" Value = '<%#Eval("FileName")%>'/>
					</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField DataField="DBName" HeaderText="DataBase Name"  ItemStyle-Width="100px" />
					<asp:BoundField DataField="BackUpDate" HeaderText="BackUp Date"  ItemStyle-Width="100px" />
					<asp:BoundField DataField="FileName" HeaderText="File name"  ItemStyle-Width="200px"/>
					<asp:BoundField DataField="BackupSize" HeaderText="Size"  ItemStyle-Width="50px"/>
					<asp:TemplateField ItemStyle-Width="50px">
					<ItemTemplate>
					</ItemTemplate>
					</asp:TemplateField>
				</Columns>
			</asp:GridView>
		</asp:Panel>
		</ContentTemplate>
		</asp:UpdatePanel>
</div>
	<div runat="server" style ="width:90%;margin-top:10px;" id ="RestoreDiv" visible="false" >
		<table class="tblEnvironment">
                <tr class="trHeight">
                    <td width ="12%%" style ="color:black">Restore Requested By:</td>
                    <td width ="50%">
                    <asp:TextBox id="txtRequestBy"  runat="server" ></asp:TextBox>
                    </td>
                    
                </tr>
                <tr class="trHeight" width = "90%">
                    <td class="txtContainer" colspan = "3" style = "padding-left:70%">
					<input type="button" value="Cancel" onClick ="return CancelOperation()" style ="float:right;margin-top:10px;margin-left:5px">               
                    <input type="button" value="CopyToSample" onClick ="return ConfirmRestore()" style ="float:right;margin-top:10px"></td>
                  
                </tr>
        </table>
       
	</div>
	
	
	 <div id="progressBar" style="position: relative; width: 95%; display: none; margin-bottom : 20px">
                		<div id="currentProgress" style="position: relative; width: 0%; height: 100%; background: #2164df;">
                		
                		</div>
			 <span id ="restoreSpan" style ="float:left; margin-right:5px;">Restoring...</span> 
			 <span id ="TimeSpan" style ="float:right; margin-left:5px;"></span> 			 
   </div>
   
   
    <div id="progressBar2" style=" position: relative; width: 95%;margin-top:10px; display: none; margin-bottom : 20px">
                		<div id="currentProgress2" style="position: relative; width: 0%; height: 100%; background: #2164df; "></div>
                		<span id ="restoreSpan2" style ="float:left; margin-right:5px;"></span> 
			 			<span id ="TimeSpan2" style ="float:right; margin-left:5px;"></span> 	
   </div>
   </div>
   </div>
</asp:Content>
