﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ClientAdmin.master"
    CodeBehind="OrphansList.aspx.vb" Inherits="CAMACloud.Admin.OrphanList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .search-panel {
            padding: 7px;
            border: 1px solid #CFCFCF;
            width: 95.3%;
            background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#CFCFCF), to(#F8F8F8));
        }

        #results .Shorter {
            float: left;
            word-wrap: inherit;
        }

        .ui-autocomplete {
            overflow: auto;
            max-height: 400px;
        }

        .search-panel label {
            margin-right: 5px;
        }

        .mGrid .empty-row-template TD {
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <h1>CAMA Cloud - Client Orphaned Records</h1>

    <div class="search-panel">
        <table style="width: 100%;">
            <tr>
                <td id="SearchArea" runat="server" style="width: 67%;">
                    <label>Search By:</label>
                    &nbsp;
                        <asp:DropDownList runat="server" ID="ddlFilter" AutoPostBack="true">
                            <asp:ListItem Text="Name" Value="1" />
                            <asp:ListItem Text="State" Value="2" />
                            <asp:ListItem Text="Vendor" Value="3" />
                            <asp:ListItem Text="Cama system" Value="4" />
                        </asp:DropDownList>
                    &nbsp;
                        <asp:TextBox runat="server" ID="txtName" Width="33%" MaxLength="30" CssClass="textboxAuto" />
                    <asp:DropDownList ID="ddlState" runat="server" AutoPostBack="true" Visible="false" Style="display: inline-block;" />
                    &nbsp;
                    <asp:Button runat="server" ID="btnSearch" Text=" Search " />
                    &nbsp;
                    <label for="proEnv">Production Env</label>
                    <asp:CheckBox runat="server" ID="chkProEnv" AutoPostBack="true" name="proEnv" />

                </td>
                <td>
                    <div style="float: right">
                        Sort By:
		                    <asp:DropDownList runat="server" ID="ddlOrderBy" CssClass="SortDdl" Width="108px" AutoPostBack="true">
                                <asp:ListItem Text="State" Value="State" />
                                <asp:ListItem Text="Name" Value="Name" />
                                <asp:ListItem Text="Vendor" Value="VendorName" />
                                <asp:ListItem Text="Cama System" Value="CamaName" />
                                <asp:ListItem Text="Last Updated On" Value="LastUpdatedOn" />
                            </asp:DropDownList>
                        &nbsp;

                            <label>Page Size:</label>
                        <asp:DropDownList runat="server" ID="ddlPageSize" AutoPostBack="true" Style="display: inline-block;">
                            <asp:ListItem Value="25" />
                            <asp:ListItem Value="50" />
                        </asp:DropDownList>
                    </div>
                </td>
                <%--<td style="text-align: right;">
                    <asp:ImageButton ID="ibtnExportToExcel" ImageUrl="~/App_Static/images/excel.png" Width="28px" Height="25px" Style="margin-right: 3px;" ToolTip="Export To Excel" runat="server" />
                </td>--%>
            </tr>
        </table>
    </div>
    <asp:GridView runat="server" ID="results" Width="97%" AllowPaging="True" ShowHeaderWhenEmpty="True">
        <Columns>
            <asp:BoundField DataField="State" HeaderText="State" ItemStyle-Width="40px">
                <ItemStyle Width="40px"></ItemStyle>
            </asp:BoundField>
            <asp:TemplateField>
                <HeaderTemplate>
                    County Name
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="lbSelect" Text='<%# Eval("Name") %>' 
                         Font-Size="9pt" Font-Bold="true" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField ItemStyle-Width="400px" DataField="OrphanTablesWithCount" HeaderText="Table Wise Count" ItemStyle-CssClass="Shorter" />
            <asp:TemplateField>
                <HeaderTemplate>
                    Details
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="lbSelectDetails" Text="Download" CommandName="SelectAccount" Visible='<%# Eval("OrphansExist") %>'
                       CommandArgument='<%#Eval("Id").ToString() + ";" + Eval("Name")%>' Font-Size="9pt" Font-Bold="true" />
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField>
               
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="lbRefreshDetails" Text='Refresh' CommandName="Refresh"  
                        CommandArgument='<%# Eval("Id") %>' Font-Size="9pt" Font-Bold="true" />
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:BoundField ItemStyle-Width="500px" DataField="OrphanRecordDetails" HeaderText="Table Wise Deatils" ItemStyle-CssClass="Shorter" />--%>
            <asp:TemplateField>
                <ItemStyle Width="140px" />
                <HeaderTemplate>
                    Last Updated
                </HeaderTemplate>
                <ItemTemplate>
                    <%# Eval("LastUpdatedOn")%>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            <label style="color: Red; font-weight: bold">No Results Found !</label>
        </EmptyDataTemplate>
    </asp:GridView>
    <asp:Panel runat="server" ID="pnlExcel" Visible="false">
        <asp:GridView runat="server" ID="gvPrint" AllowPaging="false" ShowHeaderWhenEmpty="False">
            <Columns>
                <asp:TemplateField>
                    <ItemStyle Width="40px" />
                    <HeaderTemplate>
                        CID
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# BaseCharMap(Eval("Id"))%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Id" HeaderText="ID" ItemStyle-Width="60px">
                    <ItemStyle Width="60px"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="State" HeaderText="State" ItemStyle-Width="40px">
                    <ItemStyle Width="40px"></ItemStyle>
                </asp:BoundField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        County Name
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("Name") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="camsysName" HeaderText="CAMA System" ItemStyle-Width="200px">
                    <ItemStyle Width="200px"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="vendorName" HeaderText="Support Vendor" ItemStyle-Width="200px">
                    <ItemStyle Width="200px"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="SupportVendor" HeaderText="Services Vendor" ItemStyle-Width="200px">
                    <ItemStyle Width="200px"></ItemStyle>
                </asp:BoundField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        MobileAssessor URL
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# MAHostUrl()%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        Host Server
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("Hostserver")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemStyle Width="90px" />
                    <HeaderTemplate>
                        Created Date
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("CreatedDate", "{0:MM\/dd\/yyyy}")%>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                <label style="color: Red; font-weight: bold">No Results Found !</label>
            </EmptyDataTemplate>
        </asp:GridView>
    </asp:Panel>

    <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script language="javascript" type="text/javascript">
        jQuery.noConflict()(function ($) {
            $(document).ready(function () {
                $('#<%=txtName.ClientID%>').autocomplete({
            source: function (request, response) {
                var search = $('#<%=ddlFilter.ClientID%>').val();
                        var term = request.term.replace(/[\'\\]/g, "");
                        if (!term) { return false; }
                        $.ajax({
                            url: "OrphansList.aspx/Getcounty",
                            data: "{ 'pre':'" + term + "','sear' :'" + search + "','proEnv':'" + ($('#<%=chkProEnv.ClientID%>').prop('checked') ? '1' : '0') + "'}",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            success: function (data) {
                                response($.map(data.d, function (item) {
                                    return {
                                        value: item
                                    }
                                }))
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {

                                if (XMLHttpRequest.readyState == 4 && XMLHttpRequest.status == 401 && XMLHttpRequest.statusText == "Unauthorized") {

                                    alert("Your session has been expired.\n\n Please login again.");
                                    location.reload();
                                }
                                else {
                                    alert(JSON.stringify(XMLHttpRequest));
                                }
                            }
                        });
                    },
            });
    });
         });
    </script>
</asp:Content>

