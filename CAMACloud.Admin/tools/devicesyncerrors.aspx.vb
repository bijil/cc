﻿Imports System.Data
Imports System.Data.SqlClient
Public Class devicesyncerrors
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dt As New DataTable
        dt = Database.System.GetDataTable("select o.id, o.Name,dl.LicenseKey,LoginId as CurrentLoginId,PendingPCIs,PendingImages,ErrorMessage,lastSyncTime from DeviceSyncStatus ds left join DeviceLicense dl on dl.MachineKey=ds.MachineKey left outer join Organization o on o.id=ds.OrganizationId where  (PendingPCIs > 0 OR PendingImages > 0 ) and DATEDIFF(HOUR, lastSyncTime, UpdatedTime) >=2")
        gvErrorslist.DataSource = dt
        gvErrorslist.DataBind()
    End Sub


End Class