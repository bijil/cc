﻿Imports System.IO
Imports System.Data.SqlClient
Imports System.Web.Script.Services
Imports System.Web.Services

Public Class restoredatabase
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Step1.Visible = True
            bindServer()
           
        End If
    End Sub

    Private Sub bindServer()
         ddlDEnvironment.DataSource = Data.Database.System.GetDataTable("SELECT Id,DbName FROM TestingEnvironments")
         ddlDEnvironment.DataTextField = "DbName"
         ddlDEnvironment.DataValueField = "Id"
         ddlDEnvironment.DataBind()
         ddlDEnvironment.Items.Insert(0,New ListItem("--Select Sample County--", "0"))
         ddlSEnvironment.DataSource = Data.Database.System.GetDataTable("SELECT Id,DbName FROM Organization")
         ddlSEnvironment.DataTextField = "DbName"
         ddlSEnvironment.DataValueField = "Id"
         ddlSEnvironment.DataBind()
         ddlSEnvironment.Items.Insert(0,New ListItem("--Select Sample County--", "0"))
    End Sub
    Protected Sub ddlDEnvironment_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDEnvironment.SelectedIndexChanged
        If ddlDEnvironment.SelectedValue <> 0 Then
            Dim s As String= "SELECT * FROM TestingEnvironments WHERE Id ="+ ddlDEnvironment.SelectedValue
            Dim data As DataRow = Database.System.GetTopRow("SELECT * FROM TestingEnvironments WHERE Id ="+ ddlDEnvironment.SelectedValue)
            Dim d As Integer = data.GetBoolean("PreocessStatus")
            If data.GetBoolean("PreocessStatus")  Then
                Dim rest As String = data.GetString("RestoreStatus")
                Dim back As String = data.GetString("BackupStatus")
                Dim k As String = data.GetString("SourceOrg_Id")
                If data.GetString("restoreType") = "old" Then
                    ddlSEnvironment.SelectedValue = data.GetInteger("SourceOrg_Id")
                    source()
                    sourceInfo.Visible = True
                    RblBackupSelection.SelectedValue = "old"
                    radiolist()
                    txtRequestBy.Text = data.GetString("RequestedBy")
                    HdnFileName.Value = data.GetString("Zipefile")
                 RunScript("Status(0,1)")
                Else If data.GetString("restoreType") = "new" Then
                	ddlSEnvironment.SelectedValue = data.GetInteger("SourceOrg_Id")
                	source()
                	sourceInfo.Visible = True
                	RblBackupSelection.SelectedValue = "new"
                	txtRequestedBy.Text = data.GetString("RequestedBy")
                	bakDiv.Visible = True
            		grdvDiv.Visible = False
            		RestoreDiv.Visible = False
                	RunScript("BacUPStatus('0')")
            	End If
            End If
        End If
        If ddlDEnvironment.SelectedValue <> 0 And ddlSEnvironment.SelectedValue <> 0 Then
            BackUpSelection.Visible = True
        Else If ddlDEnvironment.SelectedValue <> 0 And ddlSEnvironment.SelectedItem.Value = 0 Then
            'BtnDelete.Visible = True
            BackUpSelection.Visible = False
            sourceInfo.Visible = False
            grdvDiv.Visible = False
            RestoreDiv.Visible = False
            bakDiv.Visible = False
        Else If ddlDEnvironment.SelectedItem.Value = 0 And ddlSEnvironment.SelectedItem.Value <> 0 Then
            sourceInfo.Visible = True
            'BtnDelete.Visible = False
            BackUpSelection.Visible = False
            grdvDiv.Visible = False
            RestoreDiv.Visible = False
            bakDiv.Visible = False
        End If
        If ddlDEnvironment.SelectedValue <> 0 Then
            destinationInfo.Visible = True
            Dim drDBHost As DataRow = Database.System.GetTopRow("SELECT * FROM TestingEnvironments WHERE Id ='" & ddlDEnvironment.SelectedValue & "'")
            HdfDestination.Value = drDBHost.GetString("DbName")
            HdfDestinationDbId.Value = drDBHost.GetInteger("Id")
            lblDBName.Text = drDBHost.GetString("DbName")
            lblSourceName.Text = drDBHost.GetString("SourceDb")
            lblRequestedDate.Text = drDBHost.GetString("RequestedDate")
            lblRequestedBy.Text = drDBHost.GetString("RequestedBy")
            lblStatus.Text = drDBHost.GetString("Status")
            lblDbServer.Text = drDBHost.GetString("Servername")
        Else
            destinationInfo.Visible = False
            'BtnDelete.Visible = False
        End If
    End Sub
    Protected Sub ddlSEnvironment_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSEnvironment.SelectedIndexChanged
        If ddlSEnvironment.SelectedItem.Value <> 0 And ddlDEnvironment.SelectedItem.Value <> 0 Then
            bakDiv.Visible = False
            BackUpSelection.Visible = True
            sourceInfo.Visible = True
            RblBackupSelection.ClearSelection()
            'BtnDelete.Visible = False
        Else If ddlSEnvironment.SelectedItem.Value <> 0 And ddlDEnvironment.SelectedItem.Value = 0 Then
            sourceInfo.Visible = True
            bakDiv.Visible = False
            BackUpSelection.Visible = False
        Else If ddlDEnvironment.SelectedItem.Value <> 0 And ddlSEnvironment.SelectedItem.Value = 0 Then
            'BtnDelete.Visible = True
            BackUpSelection.Visible = False
            grdvDiv.Visible = False
            RestoreDiv.Visible = False
            bakDiv.Visible = False
        End If
     If ddlSEnvironment.SelectedItem.Value <> 0 Then
            source()
      Else
          sourceInfo.Visible =  False
      End If
    End Sub
    Protected Sub RblBackupSelection_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RblBackupSelection.SelectedIndexChanged
        If RblBackupSelection.SelectedValue = "new" Then
            sourceInfo.Visible = True
            bakDiv.Visible = True
            grdvDiv.Visible = False
            RestoreDiv.Visible = False
        Else If RblBackupSelection.SelectedValue = "old" Then
        		radiolist()
                bakDiv.Visible =  False
        End If
    End Sub

    
    Protected Sub BindGridView(DBHost As String,SourceDbName As String)
        Dim sql As String = String.Format("EXEC [" & DBHost & "].[Master].[dbo].[sp_BackUpFileInfo]{0}".SqlFormatString(SourceDbName))
        Dim dt As DataTable = New DataTable()
        dt = Database.System.GetDataTable(sql)
        If dt.Rows.Count = 0 Then
        	Alert("No old Backups available for this county.Please choose restore with new backup;")
        	Return
        Else If dt.Rows.Count > 0 Then 
        	 gdvUserBackUpList.DataSource = dt
		     gdvUserBackUpList.DataBind()
		     grdvDiv.Visible = True
		     RestoreDiv.Visible = True
        End If
       
    End Sub
    

    Protected Sub source ()
         Dim DBHost As String
        Dim drDBHost As DataRow = Database.System.GetTopRow("SELECT * FROM [dbo].[Organization] where Id= '" & ddlSEnvironment.SelectedValue & "'")
        DBHost = drDBHost.GetString("DBHost")
        Dim DBname As String
        DBname = drDBHost.GetString("DBName")
        Dim DBServer As String = Database.System.GetStringValue("SELECT Name FROM DatabaseServer WHERE Host = '" & DBHost & "'")
        HdfSource.Value = DBname
        HdfSourceDbId.Value = ddlSEnvironment.SelectedValue
            Dim d As New DataTable
            Dim sqlQuery As String = String.Format("EXEC [" & DBHost & "].[Master].[dbo].[sp_helpdb] {0}".SqlFormatString(DBname))
            Dim ds As DataSet = Database.System.GetDataSet(sqlQuery)
            Dim sql As String = String.Format("EXEC [" & DBHost & "].[Master].[dbo].[sp_DriverInfo]")
            lblFreeSpace.Text = Database.System.GetStringValue(sql)
            lblSDBName.Text = DBname
            lblSDbSize.Text = Trim(ds.Tables(0).Rows(0)(1))
            lblServer.Text = DBServer
            sqlQuery = String.Format("Select * FROM(SELECT ROW_NUMBER() OVER (PARTITION BY  bs.database_Name ORDER BY bs.backup_finish_date DESC) rn,bs.database_Name,CONCAT (CAST(backup_size / 1048576 As Decimal(10, 2) ),' MB') AS [BackupSize],bmf.physical_device_name AS FileName,bs.backup_finish_date AS BackupDate FROM [" & DBHost & "].msdb.dbo.backupset bs INNER JOIN [" & DBHost & "].msdb.dbo.backupmediafamily bmf ON [bs].[media_set_id] = [bmf].[media_set_id]) tmp WHERE rn = 1 AND database_Name = {0} ORDER BY tmp.BackupDate DESC ".SqlFormatString(DBname))
            Dim dr As DataRow = Database.System.GetTopRow(sqlQuery)
            If dr IsNot Nothing Then
                lblSBackupDate.Text = Format(Convert.ToDateTime(dr.GetString("BackupDate")), "yyyy/MM/dd hh:mm:ss tt")
            Else
                lblSBackupDate.Text = "N/A"
            End If
    End Sub
    
    Protected Sub radiolist()
        Dim DBHost As String
        Dim drDBHost As DataRow = Database.System.GetTopRow("SELECT * FROM [dbo].[Organization] where Id= '" & ddlSEnvironment.SelectedValue & "'")
        DBHost = drDBHost.GetString("DBHost")
        Dim DBname As String
        DBname = drDBHost.GetString("DBName")
        BindGridView(DBHost,DBname)
    End Sub
    Private Sub gdvUserBackUpList_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gdvUserBackUpList.PageIndexChanging
    	gdvUserBackUpList.PageIndex = e.NewPageIndex
    	Dim DBHost As String
        Dim drDBHost As DataRow = Database.System.GetTopRow("SELECT * FROM [dbo].[Organization] where Id= '" & ddlSEnvironment.SelectedValue & "'")
        DBHost = drDBHost.GetString("DBHost")
        Dim DBname As String
        DBname = drDBHost.GetString("DBName")
        BindGridView(DBHost,DBname)
    End Sub
End Class 