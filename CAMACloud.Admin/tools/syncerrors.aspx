﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ClientAdmin.master" CodeBehind="syncerrors.aspx.vb" Inherits="CAMACloud.Admin.syncerrors" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <script type="text/javascript">
       $(function () {
           $("[src*=minus]").each(function () {
               $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>");
               $(this).next().remove()
           });
       });
    </script>
    <style type="text/css">
        #gvOrganizations td
        {
            line-height: 200% !important;
            border-bottom: 0;
        }
        #gvOrganizations .pgr td {
            line-height: 12px !important;
        }
        #gvErrors td,th {
               line-height: 120% !important;
        }
        #gvErrors  .Shorter {
            float:left;
            word-wrap: break-word; 
        }
       .spacer
		{
			display: inline-block;
			width: 12px;
		}
		.masklayer {
		    background: black;
		    opacity: 0.5;
		    top: 0%;
		    width: 100%;
		    height: 100%;
		    bottom: 0%;
		    position: fixed;
		    overflow:hidden;
		    display: none;
		}
    </style>

    <script type="text/javascript">		
		function hideMask() {
   		$('.masklayer').hide();
		} 
    </script>     
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Outstanding DownSync Errors - Need Attention </h1>
    <div  id="Filter" runat="server"  style="width: 100%;">
        <div style="width: 42%; padding-bottom: 10px; float: left;">
            Keyword:
            <asp:HiddenField runat="server" ID="hdnFilter" />
            <asp:TextBox runat="server" ID="txtKey" Width="220px" MaxLength="50" />
            <asp:RequiredFieldValidator runat="server" ID="rfv" ControlToValidate="txtKey" ErrorMessage="*"
                ValidationGroup="query" />
            <asp:Button runat="server" ID="btnSearch" Text=" Search " ValidationGroup="query" />
        </div>
        <div style="display: inline-block; width: 58%; float: left;">
            <span class="spacer"></span>No. of items per page:
		    <asp:DropDownList runat="server" ID="ddlPageSize" AutoPostBack="true">
                <asp:ListItem Value="10" />
                <asp:ListItem Value="25" />
                <asp:ListItem Value="50" />
                <asp:ListItem Value="100" />
                <asp:ListItem Value="250" />
            </asp:DropDownList>
            <span class="spacer"></span>Sort by:
		    <asp:DropDownList runat="server" ID="ddlOrderBy" AutoPostBack="true">
                <asp:ListItem Text="Org Id" Value="Id" />
                <asp:ListItem Text="Vendor" Value="VendorName" />
                <asp:ListItem Text="CamaSystem" Value="CamaSystemName" />
                <asp:ListItem Text="Count of Errors" Value="TotalErrors" />
            </asp:DropDownList>
            <span class="spacer"></span>
            <label for ="proEnv">Production Env</label>
            <asp:CheckBox  runat ="server" ID="chkPro" AutoPostBack="true" name ="proEnv"/>
             <span class="spacer"></span>
            <asp:ImageButton ID="ibtnExportToExcel" ImageUrl="~/App_Static/images/excel.png" Width="28px" Height="25px" Style="position: absolute;" ToolTip="Export To Excel" runat="server" />
        </div>
    </div>
    <asp:GridView ID="gvOrganizations" runat="server" AutoGenerateColumns="false" PagerStyle-Height="10px" DataKeyNames="ID" PageSize="10" Style="border-collapse: separate; width:96%;" AllowPaging="true" OnPageIndexChanging="OnParentGrid_PageIndexChanging" ClientIDMode="Static">
        <Columns>
            <asp:TemplateField>
              <ItemStyle Width="10px" />
                <ItemTemplate>               
                    <asp:ImageButton ID="imgShow" runat="server" OnClick="Show_Hide_ChildGrid" ImageUrl="~/App_Static/images/plus.png" CommandArgument="Show" />
                    <asp:Panel ID="pnlErrors" runat="server" Visible="false" Style="position: relative;">
                        <asp:GridView ID="gvErrors" runat="server" AutoGenerateColumns="false" Width="610px" PageSize="10" AllowPaging="true" OnPageIndexChanging="OnChildGrid_PageIndexChanging" ClientIDMode="Static">
                            <Columns>
                                <asp:BoundField ItemStyle-Width="680px" DataField="DownSyncRejectReason" HeaderText="Reject Reason" ItemStyle-CssClass="Shorter" />
                                <asp:BoundField ItemStyle-Width="60px" DataField="ErrorCount" HeaderText="Count" />
                            </Columns>
                        </asp:GridView>
                    </asp:Panel>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="VendorName" HeaderText="Vendor" HtmlEncode="false" ItemStyle-Width="270px" />
            <asp:BoundField DataField="Id" HeaderText="Id" ItemStyle-Width="60px" />
            <asp:BoundField DataField="Name" HeaderText="Environment" HtmlEncode="false" ItemStyle-Width="370px" />
            <asp:BoundField DataField="CamaSystemName" HeaderText="CamaSystem" HtmlEncode="false" ItemStyle-Width="200px" />
            <asp:BoundField DataField="TotalErrors" HeaderText="Errors" ItemStyle-Width="60px" />
        </Columns>
        <EmptyDataTemplate>
            No counties having failed DownSync attempts
        </EmptyDataTemplate>
    </asp:GridView>
</asp:Content>
 