﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ClientAdmin.master" CodeBehind="devicesyncerrors.aspx.vb" Inherits="CAMACloud.Admin.devicesyncerrors" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Device Sync Errors - Need Attention</h1>
    <asp:GridView runat="server" ID="gvErrorslist">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Org. Id" ItemStyle-Width="50px" >
			
            </asp:BoundField>
            <asp:BoundField DataField="Name" HeaderText="Environment" 
                ItemStyle-Width="300px" >
 
            </asp:BoundField>
             <asp:BoundField DataField="LicenseKey" HeaderText="License Key" 
                ItemStyle-Width="200px" >
 
            </asp:BoundField>
            <asp:BoundField DataField="CurrentLoginId" HeaderText="Current LoginId" 
                ItemStyle-Width="100px" >

            </asp:BoundField>

            <asp:BoundField DataField="PendingPCIs" HeaderText="PCIs count" 
                ItemStyle-Width="50px" >

            </asp:BoundField>

             <asp:BoundField DataField="PendingImages" HeaderText="Images Count" 
                ItemStyle-Width="50px" >

            </asp:BoundField>
              <asp:BoundField DataField="ErrorMessage" HeaderText="Error Message" 
                ItemStyle-Width="250px" >

            </asp:BoundField>
              <asp:BoundField DataField="lastSyncTime" HeaderText="Last Sync Time" 
                ItemStyle-Width="80px" >

            </asp:BoundField>
            <%--<asp:TemplateField>
                <HeaderTemplate>
                    Error Counts</HeaderTemplate>
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="lbSelect" Text='<%# Eval("ErrorCount") %>' CommandName="SelectError"
                        CommandArgument='<%# Eval("Id") %>' Font-Size="9pt" Font-Bold="true" />
                </ItemTemplate>
            </asp:TemplateField>--%>
           
        </Columns>
        <EmptyDataTemplate>
            No counties having failed device Sync attempts
        </EmptyDataTemplate>
    </asp:GridView>

    
</asp:Content>
