﻿Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Data.SqlClient
Imports System.IO
Imports System.Xml
'Imports System.Web
'Imports System.Data.SqlClient
'Imports System.Collections.Generic
'Imports System.Web.Services
'Imports System.Web.Script.Services

Public Class OrphanList
    Inherits System.Web.UI.Page

    ReadOnly Property PageAction As String
        Get
            Dim sPageAction As String = Coalesce(Request("pageaction"), "")
            Select Case sPageAction.ToLower
                Case "license"
                    Return "license"
                Case "edit"
                    Return "edit"
                Case "appaccess"
                    Return "appaccess"
                Case Else
                    Return "edit"
            End Select
        End Get
    End Property

    Sub LoadGrid(Optional ByVal pageIndex As Integer = 0)
        results.PageSize = ddlPageSize.SelectedValue
        Dim sqlFilter As String = ""
        Dim sortOrder As String = "org.state, org.Name"

        Select Case ddlOrderBy.SelectedValue
            Case "Name"
                sortOrder = "org.Name"
            Case "Id"
                sortOrder = "org.Id"
            Case "VendorName"
                sortOrder = "vd.Name"
            Case "CamaName"
                sortOrder = "ca.Name"
            Case "LastUpdatedOn"
                sortOrder = "oo.LastUpdatedOn DESC"
        End Select
        If chkProEnv.Checked Then
            sqlFilter = " where ( org.IsProduction = 'true' Or org.IsProduction = 1 ) "
        Else
            sqlFilter = " WHERE 1 = 1 "
        End If
        Select Case ddlFilter.SelectedValue
            Case "1"
                sqlFilter = sqlFilter + " AND (org.Name Like '%{0}%' OR County LIKE '%{0}%')".FormatString(txtName.Text.Trim.ToSqlValue.Trim("'"))
            Case "2"
                sqlFilter = sqlFilter + IIf(ddlState.SelectedValue = "", "", " AND org.state ='{0}'".FormatString(ddlState.SelectedValue.ToSqlValue.Trim("'")))
            Case "3"
                sqlFilter = sqlFilter + " AND vd.Name LIKE '%{0}%'".FormatString(txtName.Text.Trim.ToSqlValue.Trim("'"))
            Case "4"
                sqlFilter = sqlFilter + " AND ca.Name LIKE '%{0}%'".FormatString(txtName.Text.Trim.ToSqlValue.Trim("'"))
        End Select

        results.PageIndex = pageIndex
        Dim dt As DataTable = Database.System.GetDataTable("SELECT org.*,CASE WHEN oo.OrphanTablesWithCount IS NULL THEN '0 Records' ELSE oo.OrphanTablesWithCount END AS OrphanTablesWithCount,CASE WHEN oo.OrphanTablesWithCount IS NOT NULL THEN 1 ELSE 0 END AS OrphansExist, oo.LastUpdatedOn,vd1.Name as SupportVendor,vd.Id AS vendor,vd.Name AS vendorName,ca.Id AS camasysId,ca.name AS camsysName,D.Name Hostserver FROM organization org LEFT JOIN OrganizationOrphanRecords oo ON org.Id=oo.OrganizationID  LEFT JOIN Vendor vd ON vd.Id = org.VendorId LEFT JOIN Vendor vd1 ON vd1.Id = org.SupportVendorId  LEFT JOIN CAMASystem ca ON ca.id = org.CAMASystem LEFT JOIN [DatabaseServer] D on org.DBHost=D.Host" + sqlFilter + " ORDER BY " + sortOrder)
        results.DataSource = dt
        results.DataBind()
        gvPrint.DataSource = dt
        gvPrint.DataBind()
        'If results.Rows.Count = 0 Then
        '    ibtnExportToExcel.Visible = False
        'Else
        '    ibtnExportToExcel.Visible = True
        'End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Select Case ddlFilter.SelectedValue
            Case "1"
                txtName.Visible = True
                btnSearch.Visible = True
                ddlState.Visible = False
            Case "2"
                txtName.Text = ""
                txtName.Visible = False
                btnSearch.Visible = False
                ddlState.Visible = True
            Case Else
                txtName.Visible = True
                btnSearch.Visible = True
                ddlState.Visible = False
        End Select
        If Not IsPostBack Then
            Dim dt As DataTable = Database.System.GetDataTable("SELECT DISTINCT State  FROM Organization  ORDER BY state")
            ddlState.FillFromTable(dt, True, "--Select--")
            LoadGrid()
        End If
    End Sub

    Protected Sub results_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles results.PageIndexChanging
        LoadGrid(e.NewPageIndex)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        LoadGrid()
    End Sub



    Protected Sub results_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles results.RowCommand
        Select Case e.CommandName
            Case "SelectAccount"
                Try
                    Dim arg(1) As String
                    Dim id As String
                    Dim Cname As String
                    arg = e.CommandArgument.ToString().Split(";")
                    id = arg(0)
                    Cname = arg(1)
                    Dim xmlData As String = Database.System.GetStringValue("Select OrphanRecordDetails from OrganizationOrphanRecords where OrganizationID=" + id.ToString())

                    If IsNotEmpty(xmlData) Then
                        Dim sb As New StringBuilder
                        Dim tw As New StringWriter(sb)
                        Dim hw As New HtmlTextWriter(tw)


                        Dim strArr() As String
                        Dim count As Integer
                        xmlData = xmlData.Replace("<Root>", "")
                        xmlData = xmlData.Replace("</Root>", "")
                        xmlData = xmlData.Replace("<Rows>", "")
                        xmlData = xmlData.Replace("<OrphansText>", "")
                        xmlData = xmlData.Replace("</OrphansText>", "")
                        xmlData = xmlData.Replace("</Rows>", "$")
                        strArr = xmlData.Split("$")
                        For count = 0 To strArr.Length - 1
                            If IsNotEmpty(strArr(count).Trim()) Then
                                Dim dt As New DataTable
                                dt = stam(strArr(count))
                                Dim dg As New DataGrid()
                                dg.DataSource = dt
                                dg.DataBind()
                                hw.Write(dt.TableName)
                                dg.RenderControl(hw)
                            End If

                        Next

                        Dim html As String = sb.ToString()
                        Response.Clear()
                        Response.Buffer = True
                        Response.ClearContent()
                        Response.ClearHeaders()
                        Response.AddHeader("Content-Disposition", "attachment;filename=" + Cname + "_OrphanedRecords.xlsx")
                        Response.Charset = ""
                        Response.ContentType = "application/vnd.openxml.formats-officedocument.spreadsheetml.sheet"
                        Dim temp As TableToExcel = New TableToExcel()
                        'Dim excelStream = temp.Process("<div id = ""test_table"">" + sb.ToString() + "</div>")
                        Dim excelStream = temp.Process("<div id = ""test_table"">" + sb.ToString() + "</div>", True)
                        excelStream.CopyTo(Response.OutputStream)
                        Response.End()
                    End If

                Catch ex As Exception
                    Throw ex
                End Try

            Case "Refresh"
                Database.System.Execute("exec GetOrganizationOrphanRecords " + e.CommandArgument.ToString())
                LoadGrid()
        End Select
    End Sub

    Public Function stam(xmlData As String) As DataTable

        Dim x As XElement = XElement.Parse(xmlData)
        Dim dt As DataTable = New DataTable()
        Dim setup As XElement = (From p In x.Descendants() Select p).First()
        Dim table = setup.Parent.Name.LocalName
        For Each xe As XElement In setup.Descendants()
            dt.Columns.Add(New DataColumn(xe.Name.ToString(), GetType(String)))
        Next

        Dim all = From p In x.Descendants(setup.Name.ToString()) Select p

        For Each xe As XElement In all
            Dim dr As DataRow = dt.NewRow()

            For Each xe2 As XElement In xe.Descendants()
                dr(xe2.Name.ToString()) = xe2.Value
            Next

            dt.Rows.Add(dr)
        Next
        dt.TableName = table
        Return dt
    End Function

    Public Function MAHostUrl() As String
        If Eval("MAHostKey") IsNot DBNull.Value Then
            If Eval("ApplicationHostId") IsNot DBNull.Value Then
                Dim Apphostid As Integer
                Dim strUsehttps As String ', HostGrp As String  --Reomoved Host group from MA url as per the request from Joseph--
                Apphostid = Eval("ApplicationHostId")
                Dim AppHost As DataRow = Database.System.GetTopRow("SELECT * FROM ApplicationHost WHERE Id = " & Apphostid)
                If AppHost.GetString("UseHTTPS") = "True" Then
                    strUsehttps = "https"
                Else
                    strUsehttps = "http"
                End If
                'HostGrp = AppHost.GetString("HostGroup")
                Return strUsehttps + "://" + Eval("MAHostKey") + ".camacloud.com" '--Reomoved Host group from MA url as per the request from Joseph--
            End If
        End If
        Return ""
    End Function

    <WebMethod()>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Shared Function Getcounty(pre As String, sear As String, proEnv As String) As List(Of String)
        Dim Counties As New List(Of String)()
        Dim conn As New SqlConnection
        conn.ConnectionString = ConfigurationManager.ConnectionStrings("ControlDB").ConnectionString
        Dim cmd As New SqlCommand()
        Dim condition As String
        If proEnv = "1" Then
            condition = " where ( IsProduction = 'true' Or IsProduction = 1 ) "
        Else
            condition = " where 1 = 1 "
        End If
        Dim query As String = "select Name, Id from Organization " + condition + " and " & "Name Like @SearchText + '%'"
        Select Case sear
            Case "3"
                query = "select Name, Id from Vendor where " & "Name like @SearchText + '%'"
            Case "4"
                query = "select Name, Id from camasystem where " & "Name like @SearchText + '%'"
        End Select
        cmd.CommandText = query

        cmd.Parameters.AddWithValue("@SearchText", pre)
        cmd.Connection = conn
        conn.Open()
        Dim sdr As SqlDataReader = cmd.ExecuteReader()
        While sdr.Read()
            Counties.Add(sdr("Name"))
        End While
        conn.Close()
        Return Counties
    End Function

    Protected Sub ddlPageSize_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged, ddlOrderBy.SelectedIndexChanged, ddlState.SelectedIndexChanged
        LoadGrid()
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(control As System.Web.UI.Control)
        If control Is pnlExcel Or control Is gvPrint Then
            Return
        End If
        MyBase.VerifyRenderingInServerForm(control)
    End Sub
    'Protected Sub ibtnExportToExcel_Click(sender As Object, e As System.EventArgs) Handles ibtnExportToExcel.Click
    '    Dim sb As New StringBuilder
    '    Dim tw As New StringWriter(sb)
    '    Dim hw As New HtmlTextWriter(tw)
    '    pnlExcel.Visible = True
    '    pnlExcel.RenderControl(hw)
    '    Dim html As String = sb.ToString
    '    pnlExcel.Visible = False
    '    Response.Clear()
    '    Dim fileName As String = "ClientAccountList.xls"
    '    Response.ContentType = "application/vnd.ms-excel"
    '    Response.AddHeader("Content-Disposition", "attachment;filename=""" & fileName & "")
    '    Response.Write(html)
    '    Response.End()
    'End Sub

    Protected Sub chkProEnv_CheckedChanged(sender As Object, e As EventArgs) Handles chkProEnv.CheckedChanged
        LoadGrid()
    End Sub
End Class