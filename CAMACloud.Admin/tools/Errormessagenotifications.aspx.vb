﻿Imports System.Data
Imports System.Data.SqlClient

Public Class Errormessagenotifications
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadGrid()
            CancelForm()
        End If

        If Session("loggedUserType") Is Nothing Then
            Alert("Session Experied, Please LogIn again")
            Response.Redirect("~/auth/Default.aspx")
        Else
            Dim loggedUserType As String = Session("loggedUserType").ToString()

            If loggedUserType = "support" Then
                lblHeadText.Visible = False
                table1.Visible = False
                Dim lnkBtnedit As LinkButton = New LinkButton()
                Dim lnkBtndelete As LinkButton = New LinkButton()
                For i As Integer = 0 To grid.Rows.Count - 1
                    lnkBtnedit = DirectCast(grid.Rows(i).FindControl("lbEdit"), LinkButton)
                    lnkBtnedit.Visible = False
                    lnkBtndelete = DirectCast(grid.Rows(i).FindControl("lbDelete"), LinkButton)
                    lnkBtndelete.Visible = False
                Next
            End If
        End If
    End Sub

    Sub LoadGrid()
        Dim Sql As String
        Sql = "SELECT * FROM ErrorMsgEmails ORDER BY id"
        grid.DataSource = Database.System.GetDataTable(Sql)
        grid.DataBind()
    End Sub

    Private Sub grid_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grid.RowCommand
        Select Case e.CommandName
            Case "EditItem"
                Dim type As String = e.CommandArgument
                Dim dr As DataRow = Database.System.GetTopRow("SELECT * FROM  ErrorMsgEmails WHERE Email = '" & type & "'")
                If dr Is Nothing Then
                    Alert("Email has been modified.Please reload and try again.")
                Else
                    hname.Value = dr.GetString("Name")
                    Hemail.Value = dr.GetString("Email")
                    txtName.Text = dr.GetString("Name")
                    txtEmail.Text = dr.GetString("Email")
                    btnSave.Text = "Save "
                    btnCancel.Visible = True
                    lblHeadText.Text = "Edit Settings"
                End If
            Case "DeleteItem"
                Dim clnt As String = e.CommandArgument
                clnt = clnt.Replace("'", "''")
                Database.System.Execute("DELETE FROM  ErrorMsgEmails  WHERE Email = '" & clnt & "'")
                CancelForm()
                LoadGrid()
        End Select
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Dim isRowExist As Boolean = False
        Dim type As String = txtName.Text.Trim()
        Dim sValue As String = txtEmail.Text.Trim()
        Dim temphname As String = hname.Value
        Dim temphemail As String = Hemail.Value

        If (temphname = "") Then
            isRowExist = Database.System.GetIntegerValue("SELECT COUNT(*) FROM ErrorMsgEmails WHERE Name='" & type & "' and email = '" & sValue & "'")

        Else
            isRowExist = Database.System.GetIntegerValue("SELECT COUNT(*) FROM ErrorMsgEmails WHERE Name='" & temphname & "' and email = '" & temphemail & "'")
        End If
        UpdateValue(isRowExist)
        LoadGrid()
        CancelForm()
    End Sub

    Private Sub UpdateValue(isRowExist As Boolean)
        Dim Sql As String = ""
        If isRowExist Then
            If ((hname.Value = txtName.Text.Trim() And Hemail.Value = txtEmail.Text.Trim()) Or hname.Value = "") Then
                Alert("The Email already Exists")
            Else
                Sql = "UPDATE ErrorMsgEmails Set Name ={2},Email = {3}  WHERE Name = {0} and Email = {1} "
                Database.System.Execute(Sql.SqlFormat(False, hname.Value, Hemail.Value, txtName.Text.Trim(), txtEmail.Text.Trim()))
            End If
        Else
            If (hname.Value = txtName.Text.Trim() And Hemail.Value = txtEmail.Text.Trim()) Then
                Alert("The Email already Exists")
            Else
                Sql = "INSERT INTO ErrorMsgEmails (Name, Email) VALUES ( {0}, {1} )"
                Database.System.Execute(Sql.SqlFormat(False, txtName.Text.Trim(), txtEmail.Text.Trim()))
                Alert("Email saved successfully")
            End If
        End If
    End Sub


    Private Sub CancelForm() Handles btnCancel.Click
        hname.Value = ""
        txtName.Text = ""
        txtEmail.Text = ""
        btnSave.Text = "Add "
        btnCancel.Visible = False
        lblHeadText.Text = "Add New Email"
    End Sub
End Class