﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="gethandler.aspx.vb" Inherits=".gethandler" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CAMA Cloud - Integration API Service</title>
    <style type="text/css">
        BODY
        {
            color: #000000;
            background-color: white;
            font-family: Segoe UI,Verdana;
            margin: 0px;
        }
        #content
        {
            font-size: 10pt;
            padding:15px;
            padding-top:0px;
        }
        A:link
        {
            color: #336699;
            font-weight: bold;
            text-decoration: underline;
        }
        A:visited
        {
            color: #6699cc;
            font-weight: bold;
            text-decoration: underline;
        }
        A:active
        {
            color: #336699;
            font-weight: bold;
            text-decoration: underline;
        }
        .heading1
        {
            background-color: #003366;
            border-bottom: #336699 6px solid;
            color: #ffffff;
            font-family: Tahoma;
            font-size: 26px;
            font-weight: normal;
            margin: 0px;
            padding-bottom: 12px;
            padding-left: 10px;
            padding-top: 16px;
        }
        pre
        {
            font-size: small;
            background-color: #e5e5cc;
            padding: 5px;
            font-family: Courier New;
            margin-top: 0px;
            border: 1px #f0f0e0 solid;
            white-space: pre-wrap;
            white-space: -pre-wrap;
            word-wrap: break-word;
        }
        table
        {
            border-collapse: collapse;
            border-spacing: 0px;
            font-family: Verdana;
        }
        table th
        {
            border-right: 2px white solid;
            border-bottom: 2px white solid;
            font-weight: bold;
            background-color: #cecf9c;
        }
        table td
        {
            border-right: 2px white solid;
            border-bottom: 2px white solid;
            background-color: #e5e5cc;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <h1 class="heading1">
        CAMA Cloud Data Integration API
    </h1>
    <div id="content">
        <h2>The requested content is not available on your browser.</h2>
        <p>
            The requested content can be accessed only custom client tools, programs or AJAX objects via POST method only.</p>
    </div>
    </form>
</body>
</html>
