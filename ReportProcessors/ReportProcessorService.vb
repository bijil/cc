﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Configuration
Imports System.Threading
Imports System.Text
Imports System.Runtime.CompilerServices
Imports System.Timers
Imports System.Collections.Specialized
Imports System.Web
Imports System.Net.Mail
Imports System.Reflection
Imports System.Net
Imports CAMACloud

Public Class ReportProcessorService

    Protected Overrides Sub OnStart(ByVal args() As String)
        Try
            DeleteOldServiceLog()
            Me.WriteToFile("Background Report Service started at " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))
            Me.FetchAndProcessorJob()
        Catch ex As Exception
            WriteToFile(ex.Message)
        End Try
    End Sub

    Protected Overrides Sub OnStop()
        Try
            Me.WriteToFile("Background Report Service stopped at " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))
        Catch ex As Exception
            WriteToFile(ex.Message)
        End Try
    End Sub
    Public Sub FetchAndProcessorJob()
        Me.WriteToFile("Started Job for Processing...")
        Dim processRunning As Boolean = False
        Dim status As Integer = 0
        Try
            'Dim conString As String = ConfigurationManager.ConnectionStrings("CCMAINConnectionString").ConnectionString
            'Dim conn1 As New SqlConnection(conString)
            Dim queryStr = "SELECT * FROM ReportJobs WHERE Status = 0"
            Dim datatable As New DataTable
            datatable = GetDataTable(queryStr, CommandType.Text)
            ' Dim timeOut As DateTime = Now.AddMilliseconds(3000)
            If datatable.Rows.Count > 0 Then
                Try
                    Do While (processRunning = False)
                        Me.WriteToFile("Processing...")
                        Dim task As New Thread(AddressOf ScheduleJobs)
                        task.Start()
                        task.Join(300000)
                        If task.IsAlive Then
                            Me.WriteToFile("Timeout expired, aborting Job.")
                            task.Abort()
                        Else
                            Me.WriteToFile("Current Job is completed successfully")
                        End If
                        Console.Read()
                        processRunning = False
                        Thread.Sleep(250)
                    Loop
                Catch ex As ThreadAbortException
                    'thread timeout 
                    Me.WriteToFile(ex.Message)
                End Try
            End If
        Catch ex As Exception
        End Try
        processRunning = False
    End Sub
    Public Sub ScheduleJobs()
        Dim status As Integer = 1
        Dim sqlquery As String = ""
        ' processRunning = True
        Dim queryString = "SELECT TOP 1 * FROM ReportJobs WHERE Status = 0"
        Dim dt As New DataTable
        dt = GetDataTable(queryString, CommandType.Text)
        If dt.Rows.Count > 0 Then
            Try
                sqlquery = "UPDATE ReportJobs SET Status =" & status & "WHERE Id =" & CInt(dt.Rows(0).Item("Id")) & ""
                Execute(sqlquery)
                Dim data As DataTable = CType(GetData(dt, 0), DataTable)
                Dim format As String = dt.Rows(0).Item("Format").ToString().Replace(" ", "")
                Dim orgId As Integer = CInt(dt.Rows(0).Item("OrganizationID"))
                Dim reportId As Integer = CInt(dt.Rows(0).Item("ReportID"))
                Dim jobId As Integer = CInt(dt.Rows(0).Item("Id"))
                Dim requestedQuery As String = dt.Rows(0).Item("Query").ToString()
                Dim orgName As String = GetStringValue("SELECT Name FROM Organization WHERE Id =" + orgId.ToString())
                Dim reportName As String = dt.Rows(0).Item("ReportDescription").ToString()
                Dim reportDate As String = dt.Rows(0).Item("CreatedDate").ToString()
                Dim Email As String = dt.Rows(0).Item("Email").ToString()
                Dim Parameters As String = ""
                Dim query As Array = dt.Rows(0).Item("Query").ToString().Split(CChar("@"))
                For Each el In query
                    If el.ToString().Contains("=") Then
                        Parameters += el.ToString() & vbNewLine
                    End If
                Next
                Dim downloadURL As String = dt.Rows(0).Item("DownloadhostUrl").ToString()

                ' If format.ToUpper() = "EXCEL" Then
                'If data.Rows.Count > 0 Then
                '    Try
                '        Dim wr As New StreamWriter("C:\\" & dt.Rows(0).Item("ReportDescription").ToString() & "-" & DateTime.Now.Ticks.ToString() & ".xls")
                '        For i As Integer = 0 To data.Columns.Count - 1
                '            wr.Write(data.Columns(i).ToString().ToUpper() & vbTab)
                '        Next
                '        wr.WriteLine()
                '        For i As Integer = 0 To (data.Rows.Count) - 1
                '            For j As Integer = 0 To data.Columns.Count - 1
                '                If data.Rows(i)(j) IsNot Nothing Then
                '                    wr.Write(Convert.ToString(data.Rows(i)(j)) & vbTab)
                '                Else
                '                    wr.Write(vbTab)
                '                End If
                '            Next
                '            wr.WriteLine()
                '        Next
                '        wr.Close()
                '        status = 2
                '    Catch ex As Exception
                '        status = 3
                '        Throw ex
                '    End Try

                'End If
                'ElseIf format.ToUpper() = "WORD" Then
                '    If data.Rows.Count > 0 Then
                '        Try

                '            status = 2
                '        Catch ex As Exception
                '            status = 3
                '            Throw ex
                '        End Try

                '    End If
                'Else
                '    If data.Rows.Count > 0 Then
                '        Try
                '            Dim sb As New StringBuilder
                '            Dim header As String = ""
                '            For Each dc As DataColumn In data.Columns
                '                header += dc.ColumnName + ","
                '            Next
                '            sb.AppendLine(header)
                '            For Each dr As DataRow In data.Rows
                '                Dim line As String = ""
                '                For Each dc As DataColumn In data.Columns
                '                    line += dr.GetString(dc.ColumnName) + ","
                '                Next
                '                sb.AppendLine(line)
                '            Next
                '            Dim stream As New MemoryStream(ASCIIEncoding.Default.GetBytes(sb.ToString))

                '            Dim Email As String = dt.Rows(0).Item("Email").ToString()
                '            Try
                '                Dim mail As New MailMessage
                '                mail.From = New MailAddress(ClientSettings.NotificationSender)
                '                mail.To.Add(Email)
                '                mail.Subject = dt.Rows(0).Item("ReportDescription") & Space(1) + "Report"
                '                mail.Attachments.Add(New Attachment(stream, dt.Rows(0).Item("ReportDescription").ToString() + ".txt", "text/plain"))
                '                mail.Body = "Please find the requested " + dt.Rows(0).Item("ReportDescription") + " report on " + dt.Rows(0).Item("CreatedDate") + "."
                '                mail.IsBodyHtml = True
                '                AWSMailer.SendMail(mail)
                '            Catch ex1 As Exception
                '                Throw New Exception("Mail not send ...", ex1)
                '            End Try
                '            status = 2
                '        Catch ex As Exception
                '            status = 3
                '            Throw ex
                '        End Try

                '    End If
                'End If

                If data.Rows.Count > 0 Then
                    Try
                        Dim sb As New StringBuilder
                        Dim header As String = ""
                        For Each dc As DataColumn In data.Columns
                            header += dc.ColumnName + ","
                        Next
                        sb.AppendLine(header)
                        For Each dr As DataRow In data.Rows
                            Dim line As String = ""
                            For Each dc As DataColumn In data.Columns
                                line += dr.GetString(dc.ColumnName) + ","
                            Next
                            sb.AppendLine(line)
                        Next
                        Dim stream As New MemoryStream(ASCIIEncoding.Default.GetBytes(sb.ToString))

                        Dim s3mgr As New S3FileManager

                        Dim bytes() As Byte = ASCIIEncoding.Default.GetBytes(sb.ToString)

                        Dim ms As New MemoryStream(bytes)
                        Dim s3Path As String = "/Report/" + reportName.Replace(" ", "") + "-" + DateTime.Now.Ticks.ToString()
                        Dim s3Exception As Exception = Nothing
                        Try
                            Try
                                s3mgr.UploadFile(ms, s3Path)
                            Catch ex As Exception
                                s3Exception = ex
                                ' Throw New Exception("Error on uploading file '" + s3Path + "' with " & ms.ToArray.Length.ToString & " bytes")
                            End Try
                        Catch ex As Exception
                            Dim originalErrorMessage As String = ""
                            If s3Exception IsNot Nothing Then
                                originalErrorMessage = "; Original Message: " + s3Exception.Message
                            End If
                            Throw New Exception("S3 Upload Error; Path : " + s3Path + originalErrorMessage, ex)
                        End Try
                        sqlquery = "UPDATE ReportJobs SET Path='" & s3Path.ToString() & "' WHERE Status = 1 AND Id =" + dt.Rows(0).Item("Id").ToString() + ""
                        Execute(sqlquery)
                        Dim fileURL As String = s3mgr.GetDownloadURL(s3Path)

                        Try
                            Dim mail As New MailMessage
                            mail.From = New MailAddress(ClientSettings.NotificationSender)
                            mail.To.Add(Email)
                            mail.Subject = reportName & Space(1) + "Report"
                            'mail.Attachments.Add(New Attachment(stream, reportName + ".txt", "text/plain"))
                            Dim strb As New StringBuilder
                            Dim txt As String = "Please Click below link to login and get start downloading requested report on " + reportDate.ToString() + "."
                            strb.Append("<h4 style='font-family:Segoe UI,Tahoma,Trebuchet MS,Arial;'> " + txt.Replace("'", "") + "</h4>")
                            strb.AppendLine()
                            strb.Append("<h4 style='font-family:Segoe UI,Tahoma,Trebuchet MS,Arial;color:blue;'>" + orgName.ToString() + " - " + downloadURL.ToString() + " </h4>")
                            strb.AppendLine()
                            strb.Append("<p style='font-family:Segoe UI,Tahoma,Trebuchet MS,Arial'>Report Name :  " + reportName + "</p>")
                            strb.AppendLine()
                            strb.Append("<h4 style='font-family:Segoe UI,Tahoma,Trebuchet MS,Arial'>Report Parameters :</h4>")
                            strb.Append("<table width='100%' align='center border-collapse:collapse;border-color:#CFCFCF;' cellspacing='0' cellpadding='5' border='1' style='color:white;'")
                            Dim body As String
                            Dim seperate As String() = requestedQuery.Split(CChar("@"))
                            Dim sp As String = seperate(0)
                            Dim index As Integer = 1
                            strb.Append("<tr bgcolor='#800000' '><th> Parameter Name </th><th> Parameter Value </th></tr>")
                            While (index <= seperate.Length() - 1)
                                Dim para As String() = seperate(index).Split(CChar("="))
                                strb.Append("<tr><td>'" + para(0) + "'</td><td>'" + para(1).Replace(",", "").Replace("' '", "''") + "'</td></tr>")
                                index = index + 1
                            End While
                            strb.Append("</table>")

                            body = strb.ToString()
                            mail.Body = body
                            mail.IsBodyHtml = True
                            AWSMailer.SendMail(mail)
                        Catch ex1 As Exception
                            Throw New Exception("Mail not send ...", ex1)
                        End Try
                        status = 2
                    Catch ex As Exception
                        status = 3
                        Throw ex
                    End Try
                End If
                If Not IsNothing(sqlquery) Then
                    sqlquery = "UPDATE ReportJobs SET Status =" & status & "WHERE Id =" & CInt(dt.Rows(0).Item("Id")) & ""
                    Execute(sqlquery)
                End If

            Catch ex As ThreadAbortException
                'thread timeout 
                sqlquery = "UPDATE ReportJobs SET Status = 3 WHERE Id =" & CInt(dt.Rows(0).Item("Id")) & ""
                Execute(sqlquery)
            End Try
        End If
    End Sub
    Public Function GetData(source As DataTable, i As Integer) As DataTable
        Dim conString As String = GetConnectionStringFromOrganization(CInt(source.Rows(i).Item("OrganizationID")))
        Dim conn As New SqlConnection(conString)
        If conn.State = ConnectionState.Closed Then conn.Open()
        Dim parameters As New NameValueCollection
        Dim queryString As String = CStr(source.Rows(i).Item("Query"))
        Dim seperate As String() = queryString.Split(CChar("@"))
        Dim sp As String = seperate(0)
        Dim command As New SqlCommand(queryString, conn)
        command.CommandText = queryString
        command.CommandType = CommandType.Text
        Dim index As Integer = 1
        'While (index <= seperate.Length() - 1)
        '    Dim para As Array = seperate(index).Split("=")
        '    MsgBox(para(0))
        '    If para(1) = "" Then
        '        parameters.Add("@" + para(0), Nothing)
        '    Else
        '        parameters.Add("@" + para(0), para(1).Replace(",", ""))
        '    End If
        '    index = index + 1
        'End While
        Dim dt As New DataTable
        Dim ada As New SqlDataAdapter(command)
        ada.Fill(dt)
        Return dt
    End Function
    Public Function GetConnectionStringFromOrganization(oid As Integer, Optional useDemo As Boolean = False) As String
        Dim connStr As String = "Server={0};Database={1};"
        Dim conString As String = ConfigurationManager.ConnectionStrings("CCMAINConnectionString").ConnectionString
        Dim conn As New SqlConnection(conString)
        Dim queryString As String = "SELECT * FROM Organization WHERE Id = " & oid
        Dim cmd As New SqlCommand(queryString, conn)
        cmd.CommandType = CommandType.Text
        Dim dt As New DataTable
        Dim ada As New SqlDataAdapter(cmd)
        ada.Fill(dt)
        Dim org As DataRow = dt.Rows(0)
        If org Is Nothing Then
            Throw New Exception("Invalid database connection attempt. Tenant connection might have been invoked from a stateless application.")
        End If
        connStr = String.Format(connStr, org.Item("DBHost"), org.Item("DBName"))
        If IsNothing(org.Item("DBUser")) Then
            connStr += "Integrated Security=True;"
        Else
            connStr += "User Id={0};Password={1};".FormatString(CStr(org.Item("DBUser")), CStr(org.Item("DBPassword")))
        End If
        connStr += "Connection Timeout=1200;"
        Return connStr
    End Function
    Public Function Execute(ByVal sql As String, Optional ByVal timeout As Integer = 1200) As Integer
        Dim conString As String = ConfigurationManager.ConnectionStrings("CCMAINConnectionString").ConnectionString
        Dim conn1 As New SqlConnection(conString)
        Dim st As DateTime = DateTime.Now
        If conn1.State = ConnectionState.Closed Then
            conn1.Open()
        End If
        Dim cmd As New SqlCommand(sql, conn1)
        Try
            Dim c As Integer = cmd.ExecuteNonQuery
            Return c
        Catch ex As Exception
            Dim msg As String = "Error on executing command - [" & sql & "]. Error Message is - " & ex.Message
            Me.WriteToFile(msg)
            Throw New Exception(msg)
        End Try
    End Function
    Public Function GetDataTable(ByVal commandText As String, ByVal commandType As CommandType, Optional ByVal timeout As Integer = 6200) As DataTable
        Dim conString As String = ConfigurationManager.ConnectionStrings("CCMAINConnectionString").ConnectionString
        Dim con As New SqlConnection(conString)
        If con.State = ConnectionState.Closed Then
            con.Open()
        End If
        Dim LastExecutedQuery As String = commandText
        Dim st As DateTime = DateTime.Now
        Dim cmd As New SqlCommand(commandText, con)
        cmd.CommandType = commandType
        cmd.CommandTimeout = timeout
        Dim dt As New DataTable
        Dim ada As New SqlDataAdapter(cmd)
        Try
            ada.Fill(dt)
            Return dt
        Catch ex As Exception
            Dim msg As String = "Error on executing command - [" & cmd.CommandText & "]. Error Message is - " & ex.Message
            Me.WriteToFile(msg)
            Throw New Exception(msg)
        End Try
    End Function

    'Private Sub SetTimer()
    '    timer = New System.Timers.Timer(3000)
    '    AddHandler timer.Elapsed, AddressOf OnTimedEvent
    '    timer.AutoReset = True
    '    timer.Enabled = True
    'End Sub
    'Private Sub OnTimedEvent(source As Object, e As ElapsedEventArgs)
    '    Console.WriteLine("The Elapsed event was raised at {0:HH:mm:ss.fff}", e.SignalTime)
    'End Sub
    Public Function GetStringValue(ByVal query As String, Optional nullValue As String = "") As String
        Dim conString As String = ConfigurationManager.ConnectionStrings("CCMAINConnectionString").ConnectionString
        Dim _conn As New SqlConnection(conString)
        If _conn.State = ConnectionState.Closed Then _conn.Open()
        Dim LastExecutedQuery As String = query
        Dim st As DateTime = DateTime.Now
        Dim cmd As New SqlCommand(query, _conn)
        Try
            Dim res As Object = cmd.ExecuteScalar()
            If res Is DBNull.Value Then
                Return nullValue
            ElseIf res Is Nothing Then
                Return nullValue
            Else
                Return res.ToString
            End If
        Catch ex As Exception

            Dim msg As String = "Error on executing query - [" & query & "]. Error Message is - " & ex.Message
            Me.WriteToFile(msg)
            Throw New Exception(msg)
        End Try
    End Function
    Private Sub WriteToFile(text As String)
        If (text IsNot Nothing) Then
            Dim folderPath As String = "C:\BackupDatabaseServiceLog"
            If Not Directory.Exists(folderPath) Then
                Directory.CreateDirectory(folderPath)
            End If
            Dim path As String = "C:\BackupDatabaseServiceLog\BackupDatabaseServiceLog" & DateTime.Now.ToString("dd'_'MM'_'yyyy") & ".txt"
            Using writer As New StreamWriter(path, True)
                writer.WriteLine(String.Format(text, DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")))
                writer.Close()
            End Using
        End If
    End Sub
    Private Sub DeleteOldServiceLog()
        Dim folderPath As String = "C:\BackupDatabaseServiceLog"
        Dim files As String() = Directory.GetFiles(folderPath)
        For Each file As String In files
            Dim fi As New FileInfo(file)
            If fi.CreationTime < DateTime.Now.AddDays(-30) Then
                fi.Delete()
            End If
        Next
    End Sub
End Class
