﻿Imports System.Runtime.CompilerServices

Module Module1
    <Extension()> Public Function FormatString(ByVal source As String, ByVal ParamArray inputs() As String) As String
        Return String.Format(source, inputs)
    End Function
    <Extension()> Public Function GetString(ByVal source As DataRow, ByVal fieldName As String) As String
        Dim o As Object = source(fieldName)
        If o Is DBNull.Value Then
            Return ""
        Else
            Return o.ToString
        End If
    End Function
End Module
