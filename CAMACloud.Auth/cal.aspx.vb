﻿Imports CAMACloud
Imports CAMACloud.Security
Imports CAMACloud.Exceptions
Imports System.Net.Mail

Partial Class cal
    Inherits System.Web.UI.Page

    Protected Sub lbActivate_Click(sender As Object, e As System.EventArgs) Handles lbActivate.Click
        Dim licenseKey As String = txtLicenseKey.Text
        Dim otp As String = txtOTP.Text
        Dim email As String = txtEmail.Text

        Try
            Dim toEmail As New MailAddress(txtEmail.Text)
        Catch ex As Exception
            pError.InnerHtml = "Invalid email address!"
            pError.Visible = True
            Return
        End Try

        If txtNick.Text.Length < 5 Then
            pError.InnerHtml = "The device nickname should have more than 5 characters."
            pError.Visible = True
            Return
        End If

        pError.Visible = False

        Try
        	DeviceLicense.Authenticate(licenseKey, otp, email, txtNick.Text, HttpContext.Current)
        	Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""))
            RedirectToApplication()
        Catch lex As InvalidLicenseException
            ErrorMailer.ReportException("license", lex, Request)
            pError.InnerHtml = lex.Message
            pError.Visible = True
        Catch tex As Threading.ThreadAbortException
            'Do Nothing; This is caused by the Response.Redirect
        Catch ex As Exception
            ErrorMailer.ReportException("license-unknown", ex, Request)
            pError.InnerHtml = ex.Message
            pError.Visible = True
        End Try
    End Sub

    Sub RedirectToApplication()
        Dim schema As String = "http://"
        If Request("s") = "1" Then
            schema = "https://"
        End If
        Dim appHost As String = Request("return")
        If appHost = "" Then
            appHost = "console.camacloud.com"
        End If

        Dim port As Integer = 80
        If appHost.Contains(":") Then
            port = CInt(appHost.Split(":")(1).Trim)
            If Not (port >= 300 And port <= 310) Then
                appHost = appHost.Split(":")(0)
            End If
        End If
        If appHost.Contains("sketchapp") Or appHost.Contains("sketcher") Then '' sketchApp not need / in url
        	schema = "https://"
        	Response.Redirect(schema + appHost)
        Else
        	Response.Redirect(schema + appHost + "/")
        End If
		
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.AddHeader("Access-Control-Allow-Origin", "*")
        If Not IsPostBack Then
            DeviceLicense.ClearLicenseCache(HttpContext.Current)
            Try
                Dim dl As DeviceLicense = DeviceLicense.GetLicense
                If dl IsNot Nothing Then
                    dl.VerifyUserInfo()
                End If
            Catch ex As Exceptions.LicenseMissingInfoException
                Response.Redirect(ApplicationSettings.GetCALUserInfoPageUrl(HttpContext.Current))
            Catch ex As Exceptions.UnlicensedAccessException
            Catch ex As Exceptions.LicenseBreachException
            Catch ex As Exception
            End Try
        End If
    End Sub
End Class
