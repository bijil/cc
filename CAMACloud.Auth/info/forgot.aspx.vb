﻿Imports CAMACloud.Data
Imports System.Net.Mail
Imports CAMACloud.Security

Public Class forgot
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.AddHeader("Access-Control-Allow-Origin", "*")
        DeviceLicense.ClearLicenseCache(HttpContext.Current)
    End Sub

    Private Sub lbRecover_Click(sender As Object, e As System.EventArgs) Handles lbRecover.Click
        Try
            Dim toEmail As New MailAddress(txtEmail.Text)
        Catch ex As Exception
            pError.InnerHtml = "Invalid email address!"
            pError.Visible = True
            Return
        End Try

        Dim dt As DataTable = Database.System.GetDataTable("SELECT COALESCE(o.Name, '** Master **') As OrgName, LicenseKey, OTP FROM DeviceLicense dl LEFT OUTER JOIN Organization o ON dl.OrganizationId = o.Id WHERE dl.IsEnabled = 1 AND dl.RegisteredEmail = " + txtEmail.Text.ToSqlValue + " AND dl.RegisteredNick = " + txtNick.Text.ToSqlValue)
        If dt.Rows.Count = 0 Then
            pError.InnerHtml = "No license keys found for the given recovery information.!"
            pError.Visible = True
            Return
        End If

        pError.Visible = False

        Dim updateSql As String = "UPDATE DeviceLicense SET MachineKey = NULL, SessionKey = NULL, InUse = 0 WHERE IsEnabled = 1 AND RegisteredEmail = " + txtEmail.Text.ToSqlValue + " AND RegisteredNick = " + txtNick.Text.ToSqlValue
        Database.System.Execute(updateSql)

        Dim mail As New MailMessage
        mail.From = New MailAddress(ClientSettings.DefaultSender, "Access@CAMACloud")
        mail.To.Add(txtEmail.Text)
        mail.Bcc.Add("consultsarath@gmail.com")
        mail.Subject = Now.ToString("yyyyMMdd\HHHmmss") + " : CAMA Cloud License Keys"
        mail.Body = composeEmail(dt)
        mail.IsBodyHtml = True

        AWSMailer.SendMail(mail)
        txtEmail.Text = ""
        Alert("Your license credentials has been emailed to the registered email address.")

    End Sub


    Private Function composeEmail(dtLicense As DataTable) As String
        Dim mailContent As XElement = <html>
                                          <body>
                                              <div style="font-family:Arial;width:600px;">
                                                  <h1 style="border-bottom:1px solid #075297;color:#075297;">CAMA Cloud - Access License</h1>
                                                  <p style="font-size:10pt;">Greetings from CAMA Cloud,</p>
                                                  <p style="font-size:10pt;">Your CAMA Cloud license credentials are recovered and listed as follows:</p>
                                                  <table style="font-size:10pt;font-family:Arial;">
                                                      <thead>
                                                          <tr>
                                                              <td style='font-weight:bold;width:250px;'>Account Name</td>
                                                              <td style='font-weight:bold;width:200px;'>License Key</td>
                                                              <td style='font-weight:bold;width:80px;'>OTP</td>
                                                          </tr>
                                                      </thead>
                                                      <tbody>

                                                      </tbody>
                                                  </table>
                                                  <p style="font-size:10pt;">The OTP or One-Time-Password can be used only for authenticating a new device/browser. This implies that each license/OTP pair is valid to be used on one target device only.</p>
                                                  <p style="font-size:10pt;">Sincererly, <br/>CAMA Cloud</p>
                                                  <hr/>
                                                  <p style="font-size:8pt;margin-top:0px;">
                                                      <a href="http://www.camacloud.com/">CAMA Cloud<sup style="font-size:6pt;">SM</sup></a> powered by <a href="http://www.datacloudsolutions.net/">Data Cloud Solutions, LLC.</a>
                                                  </p>
                                              </div>
                                          </body>
                                      </html>

        Dim dataFormat As XElement = <tr>
                                         <td style="vertical-align:top;"></td>
                                         <td style="vertical-align:top;"></td>
                                         <td style="vertical-align:top;"></td>
                                     </tr>

        For Each gr As DataRow In dtLicense.Rows
            Dim dataLine As XElement = XElement.Parse(dataFormat.ToString)
            dataLine...<td>(0).Value = gr.GetString("OrgName")
            dataLine...<td>(1).Value = FormatLicenseKey(gr.GetString("LicenseKey"))
            dataLine...<td>(2).Value = gr.GetString("OTP")
            mailContent...<body>...<div>...<table>...<tbody>(0).Add(dataLine)
        Next

        Return mailContent.ToString
    End Function

    Private Function newPara(line As String) As String
        Return "<p>" + line + "</p>"
    End Function


    Public Function FormatLicenseKey(key As String) As String
        If key.Length = 16 Then
            key = key.Substring(0, 4) + "-" + key.Substring(4, 4) + "-" + key.Substring(8, 4) + "-" + key.Substring(12, 4)
        End If
        Return key
    End Function


End Class