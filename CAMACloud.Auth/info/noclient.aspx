﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/SecurityPage.master" CodeBehind="noclient.aspx.vb"
    Inherits="CAMACloud.Auth.noclient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div style="color: White; font-size: 18pt; padding: 50px 0px; text-align: center;"  id="MultiOrganizationLicense" runat="server">
        <div style="padding: 25px 0px; font-size: 23pt; font-weight: bold;">
            Multi-Organization License
        </div>
        <div style="font-size:14pt;line-height:24px;">
            No default account available for this URL on this device. Use client host-key URLs to access specific client accounts.
        </div>
    </div>
    <div style="color: White; font-size: 18pt; padding: 50px 0px; text-align: center;display: none;"  id="WrongMAUrl" runat="server">
        <div style="padding: 25px 0px; font-size: 23pt; font-weight: bold;">
           Cannot Verify Environment
        </div>
        <div style="font-size:14pt;line-height:24px;">
            Please review the URL that was entered when installing MobileAssessor. There is a likely a typo in the environment name which prevents this license key from being authenticated with the appropriate environment.<br><br>If the URL is correct please contact your license administrator regarding this issue for further assistance.
        </div>
    </div>
    <div style="color: White; font-size: 18pt; padding: 50px 0px; text-align: center; display: none;" id="UnLicensed" runat="server"  >
        <div style="padding: 25px 0px; font-size: 23pt; font-weight: bold;">
         Application Access Denied
        </div>
        <div style="font-size: 14pt; line-height: 24px;">
           Your browser is not licensed to access this application.
        </div>
    </div>
</asp:Content>
