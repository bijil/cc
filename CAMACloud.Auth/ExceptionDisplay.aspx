﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ExceptionDisplay.aspx.vb" Inherits="CAMACloud.Auth.ExceptionDisplay" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CAMA Cloud - Exception</title>
</head>
<body style="background-image:url(App_Themes/CloudAuth/images/cloud_bg.jpg)">
    <form id="form1" runat="server">
        <style>
          #myTable {
            position:absolute;
           top:30%;
           left:30%; 
           width:800px;
          }
        </style>
        <div>
            <table id="myTable">
              <tr><td><asp:Label ID="lblException" runat="server" ForeColor="White" Font-Size="Larger"></asp:Label></td></tr>
            </table>
        </div>
    </form>
</body>
</html>