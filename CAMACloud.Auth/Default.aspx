﻿<%@ Page Title="CAMA Cloud&#174; - Login" Language="VB" MasterPageFile="~/App_MasterPages/SecurityPage.master"
	AutoEventWireup="false" Inherits="CAMACloud.Auth._Default" Codebehind="Default.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta name="viewport" content="user-scalable=no,initial-scale=1.0,maximum-scale=1.0" />
	<meta name="apple-mobile-web-app-capable" content="no"/>
    <style type="text/css">
        a{color:white;  }
        a:link { text-decoration: none;    }
        a:visited { text-decoration: none;}                     
        a:hover { text-decoration: underline;}
        a:active {text-decoration: underline;}
    </style>
	<script type="text/javascript">
		$(function () {
			$(':text').keypress(function (e) {
				if (e.which == 13) {
					$(':password').focus();
					$(':password').select();
				}
			});

			$(':password').keypress(function (e) {
				if (e.which == 13) {
					doLogin();
					e.preventDefault();
					//var href = $('.login-button').attr('href').replace('javascript:', '');
					//eval(href);
					return false;
				}

			});
			$(':text').focus();
			$(':text').select();
			if($('#customAgreement').html() != "")
				$('.License-Agreement').html($('#customAgreement').html());
				
		});
		function doLogin(logoutCall){
			if(logoutCall){
				$('#agreement').hide();
				$('#MainContent_divError').show();
				$('#Footer_LkChgOrg').show();
				$('#loginContent').show();
			} else {
				if($('#MainContent_Login_UserName').val() != "" && $('#MainContent_Login_Password').val()  != ""){
					$('#loginContent').hide();
					$('#MainContent_divError').hide();
					$('#Footer_LkChgOrg').hide();
					$('#agreement').show();
					$('.License-Agreement').scrollTop(0);
				}
			}
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<div id="customAgreement" style="display:none;"><%=agr %></div>
    <div class="error" runat="server" id="divError" visible="false">
	</div>
	<asp:Login runat="server" ID="Login">
		<LayoutTemplate>
			<div id="loginContent">
				<div class="login-inputs">
					<div class="input-box">
						<asp:TextBox ID="UserName" runat="server" CssClass="text" watermark="Your Login ID" />
						<asp:RequiredFieldValidator ID="UsernameValidate" runat="server" ErrorMessage="*" ControlToValidate="UserName"  ForeColor="Red"></asp:RequiredFieldValidator> 
					</div>
					<div class="input-box">
						<asp:TextBox ID="Password" TextMode="Password" runat="server" CssClass="text" watermark="Password" />
						<asp:RequiredFieldValidator ID="PasswordValidate" runat="server" ErrorMessage="*" ControlToValidate="Password"  ForeColor="Red"></asp:RequiredFieldValidator>
					</div>
				</div>
				<div style="text-align: center;">
					<asp:LinkButton runat="server" CssClass="login-button" ID="ProceedLogin" OnClientClick="doLogin(); return false;"><span>Log in</span></asp:LinkButton>
				</div>
			</div>
		<div id="agreement" style="display: none;">
		<div class="text-content" style="padding: 10px 0px;">
		    <h2 style="text-align: center; color:white;">
		        Terms of Use and Confidentiality Statement</h2>
		   <div class="License-Agreement" style="height: 280px; overflow-y: scroll;">
		    <p>
		        By logging in to this device, opening and using this software and clicking the “I
		        Agree” button at the end of this statement (on logging in), I (the Recipient) hereby
		        agree as follows:</p>
		    <p>
		        1. To hold Harris Local Government Solutions, Inc. and its’ Licensor, Data Cloud Solutions,
		        LLC (the Disclosers) Disclosing Party’s Confidential Information in strict confidence
		        and to take reasonable precautions to protect such Confidential Information (including,
		        without limitation, all precautions I as the Recipient employ with respect to my
		        own confidential information); and,</p>
		    <p>
		        For the purposes of this agreement, “Confidential Information” shall mean any confidential,
		        non-public, proprietary or confidential information of Disclosers or their customers
		        or licensors, including, without limitation, information concerning product plans,
		        services, customers, markets, business, technology, marketing plans, employees and
		        consultants, technical data, trade secrets, know-how, ideas, research, prototypes,
		        samples, formulas, software, inventions (whether patentable or not), processes,
		        designs, drawings, and schematics; and,</p>
		    <p>
		        2. I will not without the express prior written permission of Disclosers, divulge
		        any such Confidential Information, to others except fellow employees with a need-to-know,
		        provided such fellow employees are likewise bound to the obligations herein; and,</p>
		    <p>
		        3. Not to copy, decompile or attempt to reverse engineer any such Confidential Information.</p>
		        </div>
		</div>
		<div style="text-align: center;">
			<asp:LinkButton runat="server" CssClass="login-button" ID="Login" CommandName="Login"><span>I Agree</span></asp:LinkButton>
			<asp:LinkButton runat="server" CssClass="login-button" ID="Logout" OnClientClick="doLogin(true); return false;"><span>Logout</span></asp:LinkButton>
		</div>
		</div>
		</LayoutTemplate>
	</asp:Login>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Footer" runat="server">
	<div>
             <strong><span class="link"><asp:LinkButton runat="server" ID="LkChgOrg" CommandName="LkChgOrg" ValidationGroup="license" Text=""></asp:LinkButton></span></strong>
	</div>
</asp:Content>
