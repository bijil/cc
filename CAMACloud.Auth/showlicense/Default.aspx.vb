﻿Imports CAMACloud.Security

Partial Class showlicense_Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        DeviceLicense.ClearLicenseCache(HttpContext.Current)
        If Request.Cookies("MACID") IsNot Nothing Then
            Dim dr As DataRow = CAMACloud.Data.Database.System.GetTopRow("SELECT * FROM DeviceLicense WHERE MachineKey = '" + Request.Cookies("MACID").Value + "'")
            If dr IsNot Nothing Then
                Dim isSuper As Integer = dr.GetBoolean("IsSuper")
                Dim isVendor As Integer = dr.GetBoolean("IsVendor")
                Dim lkey As String = dr.GetString("LicenseKey")
                Dim nick As String = dr.GetString("RegisteredNick")
                Dim mailid As String = dr.GetString("RegisteredEmail")
                If lkey.Length = 16 Then
                    license.Text = lkey.Substring(0, 4) + "-" + lkey.Substring(4, 4) + "-" + lkey.Substring(8, 4) + "-" + lkey.Substring(12, 4)
                ElseIf lkey <> "" Then
                    license.Text = lkey
                Else
                    Try
                        Throw New Exception("Unidentified MACID - " + Request.Cookies("MACID").Value)
                    Catch ex As Exception
                        ErrorMailer.ReportException("License-Error", ex, Request)
                    End Try
                End If
                If mailid = "NULL" Then
                    lblmailid.Text = " "
                Else
                    lblmailid.Text = mailid
                End If
                lblNick.Text = nick
                If isSuper Then
                    lblLicenseType.Text = "Super Administrator"
                ElseIf isVendor Then
                    lblLicenseType.Text = "Vendor"
                Else
                    lblLicenseType.Text = ""
                End If
            End If

        End If

    End Sub
End Class
