﻿Imports CAMACloud.Data
Public Class _default2
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Request.QueryString("callUrl") IsNot Nothing Then
                ViewState("callUrl") = Request.QueryString("callUrl").ToString()
            End If
            'If Not IsNothing(Request.UrlReferrer) Then
            '    ViewState("prevPage") = Request.UrlReferrer.ToString()
            'End If
            If Request.Cookies("MACID") IsNot Nothing Then
                ListOrganizations()
            Else
                Response.Redirect("~/showlicense")
            End If
        End If
    End Sub

    Sub ListOrganizations()

        Dim dr As DataRow = CAMACloud.Data.Database.System.GetTopRow("SELECT Id, LicenseKey, IsSuper, IsVendor, VendorId, OrganizationId FROM DeviceLicense WHERE MachineKey = '" + Request.Cookies("MACID").Value + "'")
        If dr IsNot Nothing Then
            LID.Value = dr.GetInteger("Id")
            If dr.GetBoolean("IsSuper") Then
                ddlOrganization.FillFromSqlWithDatabase(Database.System, "SELECT Id, COALESCE(Name+' ('+ State +')', Name) as Name FROM Organization WHERE DBName <> 'CCMODEL' ORDER BY Name", dr.GetString("OrganizationId") = "", , dr.GetString("OrganizationId"))
            ElseIf dr.GetBoolean("IsVendor") Then
                ddlOrganization.FillFromSqlWithDatabase(Database.System, "SELECT Id, COALESCE(Name+' ('+ State +')', Name) as Name FROM Organization WHERE DBName <> 'CCMODEL'  AND (VendorId = " + dr.GetString("VendorId").ToSqlValue + " OR SupportVendorId = " + dr.GetString("VendorId").ToSqlValue + ") ORDER BY Name", dr.GetString("OrganizationId") = "", , dr.GetString("OrganizationId"))
            Else
                Response.Redirect("~/showlicense")
            End If
        Else
            Response.Redirect("~/showlicense")
        End If
    End Sub
    Sub returnUrl()
        Dim prevPage As String = ViewState("callUrl")
        Dim returnUrl As String
        If Not IsNothing(prevPage) Then
            Select Case prevPage
                Case "livecamaCloud"
                    returnUrl = "https://auth.camacloud.com/"
                Case "beta2camaCloud"
                    returnUrl = "https://auth.beta2.camacloud.com/default.aspx"
                Case "beta1camaCloud"
                    returnUrl = "https://auth.beta1.camacloud.com/default.aspx"
                Case "pacsDemocamaCloud"
                    returnUrl = "https://auth.pacsdemo.camacloud.com/default.aspx"
                Case "pacsbetacamaCloud"
                    returnUrl = "https://auth.pacsbeta.camacloud.com/default.aspx"
                Case "pacscamaCloud"
                    returnUrl = "https://auth.pacs.camacloud.com/default.aspx"
                Case "vgsicamaCloud"
                	returnUrl = "https://auth.vgsi.camacloud.com/default.aspx"
            	Case "pvtcamaCloud"
                	returnUrl = "https://auth.pvt.camacloud.com/default.aspx"
                Case "alphacamaCloud"
                    returnUrl = "https://auth.alpha.camacloud.com/default.aspx"
                Case "cacamaCloud"
                    returnUrl = "https://auth.ca.camacloud.com/default.aspx"
                Case "cabetacamaCloud"
                    returnUrl = "https://auth.ca-beta.camacloud.com/default.aspx"
                Case "localHost"
                    returnUrl = "http://localhost:301/"
            End Select
            Response.Redirect(returnUrl)
        End If
    End Sub
    Private Sub lbSwitch_Click(sender As Object, e As System.EventArgs) Handles lbSwitch.Click
        Database.System.Execute("UPDATE DeviceLicense SET OrganizationId = " & ddlOrganization.SelectedValue & " WHERE Id = " & LID.Value)
        Session.Clear()
        CAMACloud.Security.DeviceLicense.ClearLicenseAuthCookies()
        'Dim prevPage As String = ViewState("prevPage")
        'If Not IsNothing(prevPage) Then
        '    Response.Redirect(prevPage)
        'Else
        '    Response.Redirect("~/showlicense")
        'End If
        returnUrl()
    End Sub
End Class