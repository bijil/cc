﻿<%@ Page Title="Change Client Account" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/SecurityPage.master" CodeBehind="default.aspx.vb" Inherits="CAMACloud.Auth._default2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
	<meta name="viewport" content="user-scalable=no,initial-scale=1.0,maximum-scale=1.0" />
	<meta name="apple-mobile-web-app-capable" content="no">
    <style type="text/css">
        .reqd
        {
            padding-left:5px;
            color:White;
            font-size:24pt;
            vertical-align:text-bottom;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">   
    <h3 style="color:White;">Change Organization</h3>                                                                                                    
	<p>
		Select a county account to access using your current license.</p>
	<p class="error" runat="server" visible="false" id="pError">Invalid email address.</p>
    <asp:HiddenField runat="server" ID="LID"/>
	<div class="login-inputs">
        <div class="input-box">
            <asp:DropDownList runat="server" ID="ddlOrganization" CssClass="text" ValidationGroup="license" />
            <asp:RequiredFieldValidator runat="server" ID="rfv3" ControlToValidate="ddlOrganization" ValidationGroup="license" Display="Dynamic" CssClass="reqd" ErrorMessage="*" />
		</div>
	</div>
	<div style="text-align: center;">
		<asp:LinkButton runat="server" CssClass="login-button" ID="lbSwitch"  ValidationGroup="license"><span>Switch Organization</span></asp:LinkButton>
	</div>
</asp:Content>
