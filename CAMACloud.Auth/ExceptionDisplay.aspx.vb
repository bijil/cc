﻿Public Class ExceptionDisplay
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            lblException.Text = "OOPS! An error occurred."
        End If
    End Sub

End Class