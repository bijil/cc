﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/CAMACloud.master"
	AutoEventWireup="false" Inherits="CAMACloud.Console.web_Error" Codebehind="Error.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField runat="server" ID="ErrorID" />
    <asp:HiddenField runat="server" ID="ErrorMessage" />
	<h1>
		OOPS! An error occurred.</h1>
	<div style="width: 800px;">
		<p>
			We're sorry for the inconvenience occurred. The page or operation you have attempted
			has failed unfortunately. A detailed error report has been sent to the technical
			team. The problem will be rectified within 8 hours.</p>
		<p>Optionally, you may provide more detailed information about this particular error to help us resolve the issue faster.</p>
		<p>
			<strong>Describe your experience here:</strong><br />
			<asp:TextBox runat="server" ID="txtErrorDesc" TextMode="MultiLine" Rows="14" Columns="80" /><br />
			<asp:Button runat="server" ID="btnSend" Text="  Send to Technical Team  " />
		</p>
	</div>
</asp:Content>