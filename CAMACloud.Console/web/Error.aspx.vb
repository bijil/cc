﻿Imports System.Net.Mail

Partial Class web_Error
	Inherits System.Web.UI.Page

	Protected Sub btnSend_Click(sender As Object, e As System.EventArgs) Handles btnSend.Click
		If txtErrorDesc.Text.Trim <> "" Then
			Dim msg As New MailMessage
			Dim dts As DataTable = New DataTable
			Dim Sql = "SELECT * FROM ErrorMsgEmails ORDER BY id"
			dts = Database.System.GetDataTable(Sql)
			msg.From = New MailAddress("camacloud@datacloudsolutions.net", "Feedback@CAMACloud")
			For Each q In dts.Rows
				Dim ErrorReceps As String = q("Email").ToString
				msg.To.Add(ErrorReceps)
			Next
			msg.Subject = "Error Feedback from " + UserName() + " [" + HttpContext.Current.GetCAMASession.OrganizationName + "]"
			msg.Body = txtErrorDesc.Text
			msg.IsBodyHtml = False
			msg.Priority = MailPriority.High

			Dim messageBody As String = txtErrorDesc.Text
			If ErrorID.Value IsNot Nothing Then
				messageBody += vbNewLine + vbNewLine + "Error ID: " + ErrorID.Value
			End If
			If ErrorMessage.Value IsNot Nothing Then
				messageBody += vbNewLine + "Error: " + ErrorMessage.Value
			End If
			Try
				AWSMailer.SendMail(msg)
				txtErrorDesc.Text = ""
				Alert("Your message has been sent to the technical team.")
			Catch ex As Exception
				Alert("Your message cannot be sent now.")
			End Try

		End If
	End Sub
	
    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim ex = Server.GetLastError
            If ex IsNot Nothing Then
                ErrorMessage.Value = ex.Message
                ErrorID.Value = ErrorMailer.ReportException("CONSOLE", ex, Request)
            End If
        End If
    End Sub
End Class
