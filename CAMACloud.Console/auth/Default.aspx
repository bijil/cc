﻿<%@ Page Title="CAMA Cloud&#174; - Login" Language="VB" MasterPageFile="~/App_MasterPages/SecurityPage.master"
	AutoEventWireup="false" Inherits="CAMACloud.Console._Default" Codebehind="Default.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
	<meta name="viewport" content="user-scalable=no,initial-scale=1.2,maximum-scale=1.2" />
	<meta name="apple-mobile-web-app-capable" content="yes">
    <style type="text/css">
        a{color:white;  }
        a:link { text-decoration: none;    }
        a:visited { text-decoration: none;}                     
        a:hover { text-decoration: underline;}
        a:active {text-decoration: underline;}
    </style>
	<script type="text/javascript">
		$(function () {
			$(':text').keypress(function (e) {
				if (e.which == 13) {
					$(':password').focus();
					$(':password').select();
				}
			});

			$(':password').keypress(function (e) {
				if (e.which == 13) {
					var href = $('.login-button').attr('href').replace('javascript:', '');
					eval(href);
				}

			});
			$(':text').focus();
			$(':text').select();
});
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<div class="error" runat="server" id="divError" visible="false">
	</div>
	<asp:Login runat="server" ID="Login">
		<LayoutTemplate>
			<div class="login-inputs">
				<div class="input-box">
					<asp:TextBox ID="UserName" runat="server" CssClass="text" watermark="Your Login ID" />
				</div>
				<div class="input-box">
					<asp:TextBox ID="Password" TextMode="Password" runat="server" CssClass="text" watermark="Password" />
				</div>
			</div>
			<div style="text-align: center;">
				<asp:LinkButton runat="server" CssClass="login-button" ID="Login" CommandName="Login"><span>Log in</span></asp:LinkButton>     
			</div>
		</LayoutTemplate>
	</asp:Login>
</asp:Content>
<asp:Content ContentPlaceHolderID="Footer" runat="server">
	<div>
		<strong>
			<asp:Label runat="server" ID="lblOrgName" /></strong>
          <strong><span class="link"><asp:LinkButton runat="server" ID="LkChgOrg" CommandName="LkChgOrg" ValidationGroup="license"><span></span></asp:LinkButton></span></strong>
	</div>
</asp:Content>
