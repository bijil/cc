﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/CAMACloud.master"
	AutoEventWireup="false" Inherits="CAMACloud.Console.FeatureTestPage" Codebehind="FeatureTestPage.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
    .style1
    {
        width: 100%;
    }
</style>
</asp:Content>
<asp:Content ContentPlaceHolderID="LeftContent" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <cc:ClientDashboard runat="server" />
    
	<table class="style1">
        <tr>
            <td>
                Enter Subject :</td>
            <td>
                <asp:TextBox ID="txtSubject" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Enter Message :</td>
            <td>
                <asp:TextBox ID="txtMessage" runat="server"></asp:TextBox>
            </td>
        </tr>
</table>
    
	<asp:Button runat="server" ID="btnTest" Text="Send Notification" />
	<asp:Label runat="server" ID="r" />
</asp:Content>
