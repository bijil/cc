﻿<%@ Control Language="VB" AutoEventWireup="false"
	Inherits="CAMACloud.Console.App_Controls_cama_ImportSettings" Codebehind="ImportSettings.ascx.vb" %>
<script type="text/javascript">
	function showSettings() {
		$('#divImportSettings').dialog({
			modal: true,
			width: 400,
			resizable: false,
			title: "Import Settings",
			open: function (type, data) {
				$(this).parent().appendTo("form");
			}
		});
	}

	function hideSettings() {
		$('#divImportSettings').dialog('close');
		window.location.reload();
	}

</script>
<div class="link-panel ca" style="margin-top: -50px;">
	<div class="fr">
		<a onclick="showSettings();">Import Settings</a>
	</div>
</div>
<div id="divImportSettings" class="popup-win asp-form f200 l150" style="width: 580px;">
	<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
		<ContentTemplate>
			<div style="padding-top: 10px;">
				<label>
					Select Primary Table:</label>
				<asp:DropDownList runat="server" ID="ddlPrimaryTable" ClientIDMode="Static" CssClass="input-element"
					AutoPostBack="true" />
				<label>
					Common Key Field #1:</label>
				<asp:DropDownList runat="server" ID="ddlCommonKeyField1" ClientIDMode="Static" CssClass="input-element" />
				<label>
					Common Key Field #2:</label>
				<asp:DropDownList runat="server" ID="ddlCommonKeyField2" ClientIDMode="Static" CssClass="input-element" />
				<label>
					Common Key Field #3:</label>
				<asp:DropDownList runat="server" ID="ddlCommonKeyField3" ClientIDMode="Static" CssClass="input-element" />
				<div class="buttons l">
					<asp:Button runat="server" ID="btnSaveSettings" Text="Save Settings" />
					<asp:Button runat="server" ID="btnCancel" Text="Close" />
				</div>
			</div>
		</ContentTemplate>
	</asp:UpdatePanel>
</div>
