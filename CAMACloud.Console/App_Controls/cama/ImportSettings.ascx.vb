﻿
Partial Class App_Controls_cama_ImportSettings
    Inherits System.Web.UI.UserControl



	Sub LoadImportSettingsForm()
		ddlPrimaryTable.FillFromSql("SELECT Name FROM DataSourceTable WHERE ImportType = 0 AND DoNotImport = 0", ClientSettings.PrimaryTable = "", , ClientSettings.PrimaryTable)
		LoadCommonKeyFields(ClientSettings.PrimaryTable)
	End Sub

	Sub LoadCommonKeyFields(tableName As String)
		Dim cfs = {ddlCommonKeyField1, ddlCommonKeyField2, ddlCommonKeyField3}
		For Each c In cfs
			c.Enabled = tableName.IsNotEmpty
		Next
		If tableName.IsNotEmpty Then
			ddlCommonKeyField1.FillFromSql("SELECT DSF.Name FROM DataSourceField DSF INNER JOIN DataSourceTable DST ON (DSF.TableId = DST.Id) WHERE DST.Name = " + tableName.ToSqlValue, True, "None", ClientSettings.CommonKeyField1)
			ddlCommonKeyField2.FillFromSql("SELECT DSF.Name FROM DataSourceField DSF INNER JOIN DataSourceTable DST ON (DSF.TableId = DST.Id) WHERE DST.Name = " + tableName.ToSqlValue, True, "None", ClientSettings.CommonKeyField2)
			ddlCommonKeyField3.FillFromSql("SELECT DSF.Name FROM DataSourceField DSF INNER JOIN DataSourceTable DST ON (DSF.TableId = DST.Id) WHERE DST.Name = " + tableName.ToSqlValue, True, "None", ClientSettings.CommonKeyField3)
		End If
	End Sub

	Protected Sub btnSaveSettings_Click(sender As Object, e As System.EventArgs) Handles btnSaveSettings.Click
		If ClientSettings.PrimaryTable <> ddlPrimaryTable.SelectedValue Then
			ClientSettings.PrimaryTable = ddlPrimaryTable.SelectedValue
			LoadImportSettingsForm()
		Else
			ClientSettings.CommonKeyField1 = ddlCommonKeyField1.SelectedValue
			ClientSettings.CommonKeyField2 = ddlCommonKeyField2.SelectedValue
			ClientSettings.CommonKeyField3 = ddlCommonKeyField3.SelectedValue
		End If
	End Sub

	Protected Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
		LoadImportSettingsForm()
		Page.RunScript("hideSettings();")
	End Sub

	Protected Sub ddlPrimaryTable_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlPrimaryTable.SelectedIndexChanged
		LoadCommonKeyFields(ddlPrimaryTable.SelectedValue)
	End Sub

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
			LoadImportSettingsForm()
		End If
	End Sub
End Class
