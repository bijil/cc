﻿Imports System.IO
Imports System.Text
'Imports iTextSharp.text
'Imports iTextSharp.text.pdf
'Imports iTextSharp.text.html
'Imports iTextSharp.text.html.simpleparser

Public Class CustomReportViewer
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Public Sub showReport()
        exportOptions.Visible = True
        reportHead.Visible = True
        reportViewer.Visible = True

    End Sub
    Public Sub GenerateGrid(dtReportData As DataTable, reportTitle As String, ByVal ShowOnGridList As ArrayList)
        lblReportHead.Text = reportTitle
        ViewState("dtReportData") = dtReportData
        ViewState("ShowOnGridList") = ShowOnGridList
        Dim bfieldName As String
        Dim bHeaderText As String
        gvCustomReport.Columns.Clear()
        For Each column As DataColumn In dtReportData.Columns
            bfieldName = column.ColumnName
            bHeaderText = column.ColumnName
            Dim bfield As New BoundField()
            bfield.HeaderText = bHeaderText
            bfield.DataField = bfieldName
            If (column.ColumnName = "Date" Or (column.ColumnName = "EventDate" And reportTitle = "Settings Audit Trail Report")) Then
                bfield.DataFormatString = "{0:d}"
            End If
            If (column.ColumnName = "EventTime" And reportTitle = "Settings Audit Trail Report") Then
                bfield.DataFormatString = "{0:T}"
            End If
            If reportTitle = "Settings Audit Trail Report" Then
                gvCustomReport.ShowFooter = False
            End If
            If reportTitle = "BPP Report" Then
                gvCustomReport.ShowFooter = False
            End If
            
            bfield.ItemStyle.CssClass = "gvNonCountItemStyle"
            bfield.HeaderStyle.CssClass = "gvHeaderStyle"
            For i As Integer = 0 To ShowOnGridList.Count - 1
                Dim columnName As String = ShowOnGridList(i).ToString()
                If (bfieldName = columnName) Then
                    bfield.ItemStyle.CssClass = "gvItemStyle"
                    Exit For
                End If
            Next
            If reportTitle = "PRC Report" And bfieldName = "Path" Then
                Dim hfield As New HyperLinkField()
                hfield.DataTextField = bfieldName
                hfield.HeaderText = bfieldName
                hfield.DataNavigateUrlFields = {bfieldName}
                hfield.DataNavigateUrlFormatString = "~/protected/reports/ViewPRC.aspx?Params={0}"
                hfield.Target = "_blank"
                gvCustomReport.Columns.Add(hfield)
            Else
                gvCustomReport.Columns.Add(bfield)
            End If
        Next
        gvCustomReport.PageIndex = 0
        bindGrid(dtReportData, ShowOnGridList)

    End Sub
    Public Sub bindGrid(dtReportData As DataTable, ShowOnGridList As ArrayList)
        gvCustomReport.DataSource = Nothing
        gvCustomReport.DataSource = dtReportData
        gvCustomReport.DataBind()
        gvCustomReport.FooterRow.Cells(0).Text = "Total"
        gvCustomReport.FooterRow.Cells(0).HorizontalAlign = HorizontalAlign.Right
        
        For i As Integer = 0 To ShowOnGridList.Count - 1
            Dim columnName As String = ShowOnGridList(i).ToString()
            Dim t As Integer = 0
            For Each column As DataControlField In gvCustomReport.Columns
                Dim headerCaptionText As String = column.HeaderText
                If (headerCaptionText = columnName) Then
                    Dim total As Integer = dtReportData.AsEnumerable().Sum(Function(row) row.Field(Of Integer)(columnName))
                    gvCustomReport.FooterRow.Cells(t).Text = total.ToString()
                    gvCustomReport.FooterStyle.CssClass = "gvFooterStyle"

                ElseIf headerCaptionText = "Path" Then

                    gvCustomReport.FooterRow.Cells(1).Text = dtReportData.Rows.Count
                    gvCustomReport.FooterStyle.CssClass = "gvFooterStyle"
                End If

                t += 1
            Next
        Next
    End Sub

    Protected Sub gvCustomReportPageChanging(sender As Object, e As GridViewPageEventArgs)
        gvCustomReport.PageIndex = e.NewPageIndex
        bindGrid(ViewState("dtReportData"), ViewState("ShowOnGridList"))
    End Sub
    Private Sub ExportGridView(senderId As String)
        Dim dt As DataTable = ViewState("dtReportData")
        Dim ShowOnGridList As ArrayList = ViewState("ShowOnGridList")
        'Create a dummy GridView
        Dim GridView1 As New GridView()
        GridView1.AllowPaging = False
        GridView1.AutoGenerateColumns = False
        GridView1.ShowFooter = True
        If(lblReportHead.Text = "BPP Report") Then
        	GridView1.ShowFooter = False
        End If
        Dim bfieldName As String
        Dim bHeaderText As String
        GridView1.Columns.Clear()
        For Each column As DataColumn In dt.Columns
            bfieldName = column.ColumnName
            bHeaderText = column.ColumnName
            Dim bfield As New BoundField()
            bfield.HeaderText = bHeaderText
            bfield.DataField = bfieldName
            If (column.ColumnName = "Date") Then
                bfield.DataFormatString = "{0:d}"
            End If
            bfield.ItemStyle.CssClass = "gvNonCountItemStyle"
            bfield.HeaderStyle.CssClass = "gvHeaderStyle"
            bfield.ItemStyle.Width = 130
            For i As Integer = 0 To ShowOnGridList.Count - 1
                Dim columnName As String = ShowOnGridList(i).ToString()
                If (bfieldName = columnName) Then
                    bfield.ItemStyle.CssClass = "gvItemStyle"
                    Exit For
                End If
            Next
            GridView1.Columns.Add(bfield)
        Next
        GridView1.DataSource = Nothing
        GridView1.DataSource = dt
        GridView1.DataBind()
        GridView1.FooterRow.Cells(0).Text = "Total"
        GridView1.FooterRow.Cells(0).HorizontalAlign = HorizontalAlign.Right
        For i As Integer = 0 To ShowOnGridList.Count - 1
            Dim columnName As String = ShowOnGridList(i).ToString()
            Dim t As Integer = 0
            For Each column As DataControlField In GridView1.Columns
                Dim headerCaptionText As String = column.HeaderText
                If (headerCaptionText = columnName) Then
                    Dim total As Integer = dt.AsEnumerable().Sum(Function(row) row.Field(Of Integer)(columnName))
                    GridView1.FooterRow.Cells(t).Text = total.ToString()
                    GridView1.FooterStyle.CssClass = "gvFooterStyle"
                End If
                t += 1
            Next
        Next
        'If (senderId = "ibtnExportToPdf") Then
        '    ExportToPDF(GridView1)
        'End If
        If (senderId = "ibtnExportToExcel") Then
            ExportToExcel(GridView1)
        End If
        'If (senderId = "ibtnExportToWord") Then
        '    ExportToWord(GridView1)
        'End If

    End Sub
    'Private Sub ExportToWord(GridView1 As GridView)
    '    Response.Clear()
    '    Response.Buffer = True
    '    Response.ClearContent()
    '    Response.ClearHeaders()
    '    Dim FileName As String = Server.UrlEncode(lblReportHead.Text.Replace(" ", "") + ".doc")
    '    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName)
    '    Response.Cache.SetCacheability(HttpCacheability.NoCache)
    '    Response.Charset = ""
    '    Response.ContentType = "application/vnd.ms-word "
    '    Dim sw As New StringWriter()
    '    Dim hw As New HtmlTextWriter(sw)
    '    GridView1.RenderControl(hw)
    '    Response.Output.Write(sw.ToString())
    '    Response.Flush()
    '    Response.End()
    'End Sub
    Private Sub ExportToExcel(GridView1 As GridView)
        Response.Clear()
        Response.Buffer = True
        Response.ClearContent()
        Response.ClearHeaders()
        Dim FileName As String = Server.UrlEncode(lblReportHead.Text.Replace(" ", "") + ".xlsx")
        Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName)
        Response.Charset = ""
        Response.ContentType = "application/vnd.openxml.formats-officedocument.spreadsheetml.sheet"
        GridView1.ControlStyle.Width = 130
        Dim showTotal As Boolean = False
        Dim columnName As String = Nothing
        Dim columnNameCol As New List(Of String)
        If lblReportHead.Text.ToLower() = "dtr production by reviewed" Or lblReportHead.Text.ToLower() = "dtr production by status change" Then
            showTotal = True
            columnName = "Completed"
            Dim dt As DataTable = ViewState("dtReportData")
            Dim dtcolumn = dt.Columns
            For Each dc As DataColumn In dtcolumn
                columnNameCol.Add(dc.ColumnName)
            Next
        End If
        Dim excelStream = ExcelGenerator.ExportGrid(GridView1, ViewState("dtReportData"), lblReportHead.Text, lblReportHead.Text, showTotal, columnName, columnNameCol)
        excelStream.CopyTo(Response.OutputStream)
        Response.End()

    End Sub
    'Private Sub ExportToPDF(GridView1 As GridView)
    '    Response.ContentType = "application/pdf"
    '    Dim FileName As String = Server.UrlEncode(lblReportHead.Text.Replace(" ", "") + ".pdf")
    '    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName)
    '    Response.Cache.SetCacheability(HttpCacheability.NoCache)
    '    Dim sw As New StringWriter()
    '    Dim hw As New HtmlTextWriter(sw)
    '    GridView1.RenderControl(hw)
    '    Dim sr As New StringReader(sw.ToString())
    '    Dim pdfDoc As New Document(PageSize.A4, 25, 20, 30, 15)
    '    Dim htmlparser As New HTMLWorker(pdfDoc)
    '    PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
    '    pdfDoc.Open()
    '    htmlparser.Parse(sr)
    '    pdfDoc.Close()
    '    Response.Write(pdfDoc)
    '    Response.End()
    'End Sub
    Protected Sub ibtnExportToExcel_Click(sender As Object, e As ImageClickEventArgs) Handles ibtnExportToExcel.Click
        ExportGridView(sender.Id)
    End Sub

    'Protected Sub ibtnExportToWord_Click(sender As Object, e As ImageClickEventArgs) Handles ibtnExportToWord.Click
    '    ExportGridView(sender.Id)
    'End Sub

    'Private Sub ibtnExportToPdf_Click(sender As Object, e As ImageClickEventArgs) Handles ibtnExportToPdf.Click
    '    ExportGridView(sender.Id)
    'End Sub
End Class