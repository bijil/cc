﻿<%@ Control Language="VB" AutoEventWireup="false"
	Inherits="CAMACloud.Console.App_Controls_app_ClientDashboard" Codebehind="ClientDashboard.ascx.vb" %>
<%@ Import Namespace="CAMACloud"%>
<%@ Import Namespace="CAMACloud.Data" %>
<style type="text/css">
	.vtop
	{
		border-spacing: 0px;
	}
	
	.vtop td
	{
		vertical-align: top;
	}
	
	.vtop td:first-child
	{
		width: 160px;
	}

    .redirect {
        font-weight: bold;
        text-decoration: none;
        cursor: pointer;
    }
</style>
<h1 style="font-size: 32pt; margin-bottom: 5px;">
	<%= o.GetString("Name")%></h1>
<h1 style="display: none;">
	Welcome,
	<%= Page.UserDisplayName%></h1>
<table style="border-spacing: 0px; width: 100%;">
	<tr>
		<td style="width: 50%; vertical-align: top;">
			<h2>
				Welcome,
				<%= Page.UserDisplayName%></h2>
			<div style="margin-bottom: 10px;">
				<strong>Location</strong>: 
				<%= o.GetString("City")%>
				<%= o.GetString("County")%>
				<%= o.GetString("State")%>
			</div>
			<table class="vtop">
				<tr>
					<td style="width: 50%;">       
                    <strong>Your last login</strong>: <asp:label  runat="server" id = "lblLogin"></asp:label>
                    </td>
				</tr>
			</table>
		</td>
		<td style="width: 50%; vertical-align: top;">
			<h2>
				Cloud Data Statistics</h2>
			<table class="vtop">
				<tr>
					<td>Total Parcels: </td>
					<td>
						<%= Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM Parcel WHERE Id > -1").ToString("N0")%>
					</td>
				</tr>
				<tr>
					<td <asp:label  runat="server" id = "BppCnt"></asp:label> </td>
					<td
						<asp:label  runat="server" id = "lblBppc"></asp:label>
					</td>
				</tr>
				<tr>
					<td <asp:label  runat="server" id = "RpCnt"></asp:label> </td>
					<td>
						 <asp:label  runat="server" id = "lblRpc"></asp:label>
					</td>
				</tr>
				<tr>
					<td>Total <%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName()%>s: </td>
					<td>
						<%= Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM Neighborhood").ToString("N0")%>
					</td>
				</tr>
<%--				<tr>
					<td>Total Groups: </td>
					<td>
						<%= Database.Tenant.GetIntegerValue("SELECT COUNT(DISTINCT GroupNo) FROM Neighborhood").ToString("N0")%>
					</td>
				</tr>--%>
                <tr>
                    <td colspan="2">
                    <asp:LinkButton ID="lbRedirect" class="redirect" runat="server">View count of records</asp:LinkButton>
                    </td>
                </tr>
			</table>
			<h2>
				User Pool</h2>
			<table class="vtop">
				<tr>
					<td>Total Users: </td>
					<td>
						<%= Membership.GetAllUsers.Count%>
					</td>
				</tr>
				<tr>
					<td>Online Users: </td>
					<td>
						<%= Membership.GetNumberOfUsersOnline%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
