﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewPRC.aspx.vb" Inherits="CAMACloud.Console.ViewPRC" %>

<!DOCTYPE html>
 
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <title></title>
</head>
<body>
  <style>
    .EditedValue {
        color: red;
           }
 </style>

    <script type="text/javascript">
        function downloadPrcHtml(prcId, callback) {

            $.ajax({
                url: '/quality/downloadhtmlfile.jrq',
                dataType: 'html',
                data: { PrcId: prcId },
                success: function (response) {
                    $('#viewPRC').html(response);
                    $("button[onclick='utils.MarkAsComplete()']").hide();                   
                    if (callback) callback();
                },
                error: function (xhr, status, error) {
                    console.log(error);
                    if (callback) callback();
                }
            });
        }
    </script>
    <form id="form1" runat="server">
        <div id="viewPRC">
        </div>
    </form>
    <div> </div>
</body>
</html>
