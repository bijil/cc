﻿Imports System.Linq
Imports CAMACloud.Data

Partial Class App_Controls_app_ModuleNavigation
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim roleString As String = String.Join(",", Roles.GetRolesForUser().Select(Function(x) x.ToSqlValue()))
            If roleString = "" Then
                roleString = "'datasetup'"
            End If

            'Dim role As String = roleString ''Commented by JJ

            If roleString.Contains("'BasicSettings'") Then
            	Session("roleLink") = "BasicSettings"
            Else If Session("roleLink") = "BasicSettings"
        		Session("roleLink") = Nothing
        	End If

            Dim sql As String = "SELECT * FROM AppRoles WHERE Path IS NOT NULL AND Id IN (SELECT RoleId FROM OrganizationRoles WHERE OrganizationId = " & HttpContext.Current.GetCAMASession.OrganizationId & ") AND ASPRoleName IN (" + roleString + ") ORDER BY Ordinal"
            menu.DataSource = Database.System.GetDataTable(sql)
            menu.DataTextField = "Name"
            menu.DataValueField = "Path"
            menu.DataBind()
            'menu.DataSource = CAMACloud.data
        End If
    End Sub
End Class
