﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="MRA_NavigationControl.ascx.vb" Inherits="CAMACloud.Console.MRA_NavigationControl" %>
<style type="text/css">
.link-panel a {
    background: linear-gradient(#88c2f1,#326b84);
    color: white;
}
.link-panel a {
font-weight: bold;
margin: 2px;
border-radius: 0px;
cursor: pointer;
font-family: sans-serif;
font-size: 12px;
text-align: center;
}
.link-panel a:hover{
background: linear-gradient(#60a2d8,#21617d);
}
.Labl {
float: left;
margin-top: 5px;
font-size: 15px;
font-family: sans-serif;
color: #383737;
width: 120px;
}
.Left_lbl
{
    float: left;
    width: 35%;
    margin-left:33px;
}
.Rght_linkdiv
{
float: right;
    width: 55%;
    margin-right:18px;
}
.ddlModel {
min-width: 150px;
margin-top: 3px;
}
</style>
<div class="Left_lbl">
<label class="Labl">Select Model:</label>
	<asp:DropDownList ID="ddlModel" runat="server" Class="ddlModel" AutoPostBack="true"></asp:DropDownList>
	</div>
	<div class="Rght_linkdiv">
	 <asp:Panel runat="server" ID="pnlLinks" Style="float: right;" CssClass="link-panel">
		<asp:HyperLink runat="server" ID="hldataView" Text="Data View" cssClass="dataView"/>
		<asp:HyperLink  runat="server" ID="hleditView" Text="Edit Model" cssClass="editView"/>
		<asp:HyperLink runat="server" ID="hlcalculator" Text="Calculator" cssClass="calculator" />
		<asp:HyperLink  runat="server" ID="hlanalysisResult" Text="Analysis Result" cssClass="analysisResult" />	
		<asp:HyperLink  runat="server" ID="hlgraph" Text="Graph View" cssClass="graph"/>
	</asp:Panel>
</div>