﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="MRAResultView.ascx.vb" Inherits="CAMACloud.Console.MRAResultView" %>

<script>
$(function () {
$("#spanMainHeading").html( $(".lblModelName").html() +'- Regression Analysis Result');
})
</script>

    <asp:Label runat="server" ID="lblModelName" class="lblModelName" style="display:none" />

<table >
    <tr>
        <td >
            <asp:GridView runat="server" ID="gvStats">
                <Columns>
                    <asp:BoundField DataField="Field" ItemStyle-Width="120px" HeaderText="Statistics" />
                    <asp:BoundField DataField="Value" ItemStyle-Width="120px" ItemStyle-HorizontalAlign="Right" />
                </Columns>
            </asp:GridView>


            <table class="mGrid app-content" style="width:100%;">
                <tr class="ui-state-default">
                    <th>
                        Computed Regression Model
                    </th>
                </tr>
                <tr>
                    <td style="font-size: 1.4em; text-align: left; padding: 10px 20px; margin: 0px;background-color:#fff5bf;font-weight:bold;">
                        <asp:Label runat="server" ID="lblFormula" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<h3>Analysis of Variance (ANOVA)</h3>
<asp:GridView runat="server" ID="gvANOVA">
    <Columns>
        <asp:BoundField DataField="Caption" ItemStyle-Width="120px" />
        <asp:BoundField DataField="dF" HeaderText="dF" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="c" />
        <asp:BoundField DataField="SS" HeaderText="SS" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
        <asp:BoundField DataField="MS" HeaderText="MS" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
        <asp:BoundField DataField="F" HeaderText="Regression-F" ItemStyle-Width="120px" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
        <asp:BoundField DataField="SigF" HeaderText="Significance-F" ItemStyle-Width="120px" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
    </Columns>
</asp:GridView>
<h3>Regression Coefficients</h3>
<asp:GridView runat="server" ID="gvCoefficients">
    <Columns>
        <asp:BoundField DataField="Head" ItemStyle-Width="160px" />
        <asp:BoundField DataField="Coefficient" HeaderText="Coefficient" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
        <asp:BoundField DataField="StandardError" HeaderText="Standard Error" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
        <asp:BoundField DataField="tStat" DataFormatString="{0:0.########}" HeaderText="t-Stat" ItemStyle-Width="170px" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
        <asp:BoundField DataField="pValue" DataFormatString="{0:0.########}" HeaderText="p-Value" ItemStyle-Width="170px" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
    </Columns>
</asp:GridView>
