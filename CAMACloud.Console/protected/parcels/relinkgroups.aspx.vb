﻿Public Class relinkgroups
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim NeighborhoodName As String = CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName()
        If Database.Tenant.DoesTableExists("NeighborhoodData") Then
            lb_All_RelinkNeighborhoods.Text = "Relink All " & NeighborhoodName & "s"
            lbRelinkNeighborhoods.Text = "Relink " & NeighborhoodName & "s"
        Else
            lb_All_RelinkNeighborhoods.Text = "Link All " & NeighborhoodName & "s"
            lbRelinkNeighborhoods.Text = "Link " & NeighborhoodName & "s"
        End If
    End Sub
    Private Sub lb_All_RelinkNeighborhoods_Click(sender As Object, e As System.EventArgs) Handles lb_All_RelinkNeighborhoods.Click
        Try
            Dim statusMessage As String = CAMACloud.BusinessLogic.Neighborhood.RebuildNeighborhoods(Database.Tenant, True, True, "", Membership.GetUser().ToString())
            Alert(statusMessage)
        Catch ex As Exception
            Alert(ex.Message)
        End Try
    End Sub

    Protected Sub lbRelinkNeighborhoods_Click(sender As Object, e As EventArgs) Handles lbRelinkNeighborhoods.Click
        Try
            Dim statusMessage As String = CAMACloud.BusinessLogic.Neighborhood.RebuildNeighborhoods(Database.Tenant, True, False, "", Membership.GetUser().ToString())
            Alert(statusMessage)
        Catch ex As Exception
            Alert(ex.Message)
        End Try
    End Sub
End Class