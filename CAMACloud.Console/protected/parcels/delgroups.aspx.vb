﻿Public Class manageneighborhood
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then

            RunScript("toggleAscDesc();")
        End If

        If Not IsPostBack Then
            gdvEmptyNeighborhoods.Columns(2).HeaderText = CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName("shortname") & "."
            LoadEmptyNeighborhoods()
            'Dim tooltipon As Boolean = Session("tooltipon")
            'If tooltipon Then
            '    image1.ImageUrl = "~/App_Static/images/images.jpg"
            'Else
            '    image1.ImageUrl = "~/App_Static/images/ibtn.png"
            'End If
        End If
    End Sub

    Private Sub LoadEmptyNeighborhoods()

        Database.Tenant.Execute("EXEC cc_UpdateNeighborhoodStatistics NULL, 0")

        Dim dt As DataTable ' Declare dt variable outside the block

        Dim sort As String = IIf(btnAscDesc.Text = "Ascending", "ASC", "DESC")
        Dim queryWithoutFilter As String = "SELECT Id, Name, Number FROM Neighborhood WHERE TotalParcels = 0 ORDER BY Name " & sort
        dt = Database.Tenant.GetDataTable(queryWithoutFilter)

        ' Check if search text is not empty
        If tbSearchText.Text <> "" Then
            Dim sortby = ddlOrderBy.SelectedValue
            Dim searchtxt = tbSearchText.Text.Trim()

            Dim filterCondition As String = ""
            Select Case ddlFilterOperator.SelectedValue
                Case "1"
                    filterCondition = "REPLACE(" & sortby & ", ' ', '') = REPLACE('" & searchtxt & "', ' ', '')"
                Case "2"
                    filterCondition = sortby & " LIKE '%" & searchtxt & "%'"
                Case "3"
                    filterCondition = sortby & " LIKE '" & searchtxt & "%'"
                Case "4"
                    filterCondition = sortby & " LIKE '%" & searchtxt & "'"
            End Select

            Dim query As String = "SELECT Id, Name, Number FROM Neighborhood WHERE TotalParcels = 0 AND " & filterCondition & " ORDER BY " & sortby & " " & sort
            dt = Database.Tenant.GetDataTable(query)
        Else

        End If

        gdvEmptyNeighborhoods.DataSource = dt
        gdvEmptyNeighborhoods.DataBind()

    End Sub

    Protected Function GetSelectedIds() As String
        Dim selection As String = "0"
        For Each gr As GridViewRow In gdvEmptyNeighborhoods.Rows
            If gr.GetChecked("chkSelect") Then
                selection += "," + gr.GetHiddenValue("NID")
            End If
        Next
        Return selection
    End Function

    Sub DeleteNeighborhoods(ids As String)
        Database.Tenant.Execute("DELETE FROM NeighborhoodData WHERE NbhdId IN (" & ids & ")")
        Database.Tenant.Execute("DELETE FROM Neighborhood WHERE Id IN (" & ids & ")")
        LoadEmptyNeighborhoods()
        ddlFilterOperator.SelectedValue = "0"
        tbSearchText.Visible = False ' Hide the search box
        btnSearch.Visible = False
        tbSearchText.Text = ""
    End Sub

    Private Sub lbDeleteNbhd_Click(sender As Object, e As System.EventArgs) Handles lbDeleteNbhd.Click
        DeleteNeighborhoods(GetSelectedIds)
        Alert("The selected group(s) has been successfully deleted")
    End Sub

    Private Sub lnkDeleteAll_Click(sender As Object, e As System.EventArgs) Handles lnkDeleteAll.Click
        CAMACloud.BusinessLogic.Neighborhood.DeleteAllEmptyGroups(Database.Tenant)
        LoadEmptyNeighborhoods()
        Alert("All Empty Groups has been successfully deleted")
    End Sub

    Private Sub gdvEmptyNeighborhoods_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gdvEmptyNeighborhoods.PageIndexChanging
        gdvEmptyNeighborhoods.PageIndex = e.NewPageIndex
        LoadEmptyNeighborhoods()
    End Sub

    Protected Sub ddlFilterOperator_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFilterOperator.SelectedIndexChanged
        tbSearchText.Text = ""
        If ddlFilterOperator.SelectedValue <> "0" Then
            tbSearchText.Visible = True ' Show the search box
            btnSearch.Visible = True
        Else
            tbSearchText.Visible = False ' Hide the search box
            btnSearch.Visible = False
            LoadEmptyNeighborhoods()
        End If
    End Sub

    Protected Sub ddlOrderBy_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlOrderBy.SelectedIndexChanged
        tbSearchText.Text = ""
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click

        LoadEmptyNeighborhoods()
        'Dim sortby = ddlOrderBy.SelectedValue
        'Dim searchtxt = tbSearchText.Text.Trim()
        'Dim dt As DataTable
        'If (tbSearchText.Text = "") Then
        '    Alert("You have not entered any text to filter in below results.")
        '    LoadEmptyNeighborhoods()
        'Else
        '    If ddlFilterOperator.SelectedValue = "1" Then
        '        dt = Database.Tenant.GetDataTable("SELECT Id, Name, Number FROM Neighborhood WHERE TotalParcels = 0 AND REPLACE(" & sortby & ", ' ', '') = '" & Replace(searchtxt, " ", "") & "'")
        '        'dt = Database.Tenant.GetDataTable("SELECT Id, Name,Number FROM Neighborhood WHERE TotalParcels=0 AND " & sortby & "='" & searchtxt & "' ") ' AND LastComment IS NULL AND FirstVisitedDate IS NULL")           
        '    ElseIf ddlFilterOperator.SelectedValue = "2" Then
        '        dt = Database.Tenant.GetDataTable("SELECT Id, Name,Number FROM Neighborhood WHERE TotalParcels=0 AND " & sortby & " LIKE '%" & searchtxt & "%' ") ' AND LastComment IS NULL AND FirstVisitedDate IS NULL")            
        '    ElseIf ddlFilterOperator.SelectedValue = "3" Then
        '        dt = Database.Tenant.GetDataTable("SELECT Id, Name,Number FROM Neighborhood WHERE TotalParcels=0 AND " & sortby & " LIKE '" & searchtxt & "%' ") ' AND LastComment IS NULL AND FirstVisitedDate IS NULL")            
        '    ElseIf ddlFilterOperator.SelectedValue = "4" Then
        '        dt = Database.Tenant.GetDataTable("SELECT Id, Name,Number FROM Neighborhood WHERE TotalParcels=0 AND " & sortby & " LIKE '%" & searchtxt & "' ") ' AND LastComment IS NULL AND FirstVisitedDate IS NULL")
        '    End If
        '    gdvEmptyNeighborhoods.DataSource = dt
        '    gdvEmptyNeighborhoods.DataBind()
        'End If

    End Sub

    Protected Sub btnAscDesc_Click(sender As Object, e As EventArgs)
        Dim sort As String
        Dim sortby = ddlOrderBy.SelectedValue
        Dim searchtxt = tbSearchText.Text.Trim()
        Dim dt As DataTable
        If btnAscDesc.Text = "Ascending" Then
            'btnAscDesc.Style("background-position-x") = "-12px"
            btnAscDesc.Text = "Descending"
            sort = "ORDER BY " & sortby & " DESC"
        Else
            'btnAscDesc.Style("background-position-x") = "1px"
            btnAscDesc.Text = "Ascending"
            sort = "ORDER BY " & sortby & " ASC"
        End If
        If (tbSearchText.Text = "") Then
            sortby = "1"
            searchtxt = "1"
        End If
        If ddlFilterOperator.SelectedValue = "1" Then
            dt = Database.Tenant.GetDataTable("SELECT Id, Name,Number FROM Neighborhood WHERE TotalParcels=0 AND " & sortby & "='" & searchtxt & "' " & sort & " ") ' AND LastComment IS NULL AND FirstVisitedDate IS NULL")           
        ElseIf ddlFilterOperator.SelectedValue = "2" Then
            dt = Database.Tenant.GetDataTable("SELECT Id, Name,Number FROM Neighborhood WHERE TotalParcels=0 AND " & sortby & " LIKE '%" & searchtxt & "%' " & sort & " ") ' AND LastComment IS NULL AND FirstVisitedDate IS NULL")            
        ElseIf ddlFilterOperator.SelectedValue = "3" Then
            dt = Database.Tenant.GetDataTable("SELECT Id, Name,Number FROM Neighborhood WHERE TotalParcels=0 AND " & sortby & " LIKE '" & searchtxt & "%' " & sort & " ") ' AND LastComment IS NULL AND FirstVisitedDate IS NULL")            
        ElseIf ddlFilterOperator.SelectedValue = "4" Then
            dt = Database.Tenant.GetDataTable("SELECT Id, Name,Number FROM Neighborhood WHERE TotalParcels=0 AND " & sortby & " LIKE '%" & searchtxt & "' " & sort & " ") ' AND LastComment IS NULL AND FirstVisitedDate IS NULL")
        Else
            dt = Database.Tenant.GetDataTable("SELECT Id, Name,Number FROM Neighborhood WHERE TotalParcels=0 " & sort & " ") ' AND LastComment IS NULL AND FirstVisitedDate IS NULL")
        End If
        gdvEmptyNeighborhoods.DataSource = dt
        gdvEmptyNeighborhoods.DataBind()
    End Sub


    'Protected Sub image1_Click(sender As Object, e As ImageClickEventArgs) Handles image1.Click
    '    If image1.ImageUrl = "~/App_Static/images/ibtn.png" Then
    '        Session("tooltipon") = True
    '    Else
    '        Session("tooltipon") = False
    '    End If
    '    Dim tooltipon As Boolean = Session("tooltipon")
    '    If tooltipon Then
    '        image1.ImageUrl = "~/App_Static/images/images.jpg"
    '    Else
    '        image1.ImageUrl = "~/App_Static/images/ibtn.png"
    '    End If
    'End Sub
End Class