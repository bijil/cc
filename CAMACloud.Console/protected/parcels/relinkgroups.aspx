﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ParcelManager.master" CodeBehind="relinkgroups.aspx.vb" Inherits="CAMACloud.Console.relinkgroups" %>

<%@ Import Namespace="CAMACloud" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <script type="text/javascript">
            var msg_RelinkAll = 'Relinking all <%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName()%>s will reset association of all parcels. All ad hoc <%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName()%>s will be emptied, and the parcels will be linked to their original <%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName()%>s. Are you sure you want to continue?'
            var msg_Relink = 'You are about to refresh association of all parcels, except those listed under ad hoc <%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName()%>s, to their current data specified <%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName()%>s. Are you sure you want to continue?'
        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Link <%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName()%>s</h1>
<p class="info" style="width:auto;">The <b> Relink All <%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName()%>s</b> button will re-link your <b><%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName().ToLower%></b> table to the parcels' data in the case such data has been recently modified, and all systems' synchronization cycles have not completed yet <b style="color:red;">(CAUTION: This will undo all ad-hoc assignment groups)</b>. Use the <b>Relink <%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName()%>s</b> button if you'd like to keep the ad-hoc groups intact, but relink all other parcels to any recent parcel data revisions affecting your groups in the event that all systems' synchronization cycles have not completed yet.
                   </p>
                     <div class="link-panel">
                    <asp:LinkButton runat="server" ID="lb_All_RelinkNeighborhoods" OnClientClick="return confirm(msg_RelinkAll);"   />
                     <asp:LinkButton runat="server" ID="lbRelinkNeighborhoods" OnClientClick="return confirm(msg_Relink);"   />
                </div>
</asp:Content>
