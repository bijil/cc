﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/ParcelManager.master"
    AutoEventWireup="false" Inherits="CAMACloud.Console.parcels_cautionlist" CodeBehind="cautionlist.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
     <link rel="Stylesheet" href="/App_Static/css/parcelmanager.css?<%=Now.Ticks%>>" />
    <script type="text/javascript" src="/App_Static/js/parcelmanager.js?<%=Now.Ticks%>>"></script>
   <script type="text/javascript">
       function uploadList() {
           if ($('input[type="file"]').val() != "") {
               $('#currProgress').show();
               $('#currProgress').width('100px')
               setInterval(progressBar, 1000);
           }
       }
       function progressBar() {
           var width = $('#currProgress').width() + 100;
           if (width < 1000)
               $('#currProgress').width(width + 'px');
       }
   </script>        
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h1>
        Caution Alert List</h1>
    <p class="info">
        You can provide a Caution Message to parcels by uploading a control list. The control list should be in CSV format, with the
        following fields.
    </p>
    <p runat="server" id="pHaveCK">
        <%=KeyLists()%>,<strong>CAUTIONFLAG,CAUTIONMESSAGE</strong>
    </p>
    <p runat="server" id="pNoCK">
        Please set the common key fields before using this feature.
    </p>
    <p class="info">
        In <strong>CAUTIONFLAG</strong> - You can set 1,0 for setting and unsetting
        caution alerts respectively.<br />
        <strong>CAUTIONMESSAGE</strong> includes the caution text for parcels.<br />
        <br/>If the particular Caution Message is not provided, a default text can be set as Caution Message.
        <br /><strong>Default text for Caution:</strong>
        <asp:TextBox runat="server" ID="txtcaution" style="margin-left: 23px;" ></asp:TextBox>
        <asp:Button runat="server" ID="btncaution" Text="Save"/>
    </p>
    <p>
        <strong>Note :</strong> Select control file of (*.csv) format. The maximum size
        of uploaded file should be 4MB.<br />
        <br />
    </p>
    <p style="margin:5px 0px;padding:12px 5px;background:#DFDFDF;border:1px solid #CFCFCF;width:auto;">
        <asp:FileUpload runat="server" ID="ControlFile" Width="400px" accept=".csv" />
    </p>
      <div id="currProgress" class="curr_Progress"style="display:none;width: 0px; height: 20px; background: #2164df;"></div>
    <p>
        <asp:Button runat="server" ID="Uploadbtn" Text=" Upload " Height="40px" Width="120px" OnClientClick="return uploadList();"
            Font-Bold="true" />
    </p>
    <asp:Panel runat="server" ID="prioResults" Visible="false">
        <p>
            Total parcels affected:
            <asp:Label runat="server" ID="lblTotal" />
        </p>
        <table style="border-spacing: 0px; width: 600px;">
            <tr>
                <td>
                   Caution Alert(s) Set:
                    <asp:Label runat="server" ID="lblSets" />
                </td>
                <td>
                   Caution Alert(s) Unset:
                    <asp:Label runat="server" ID="lblUnsets" />
                </td>
               </tr>
        </table>
    </asp:Panel>
     
</asp:Content>
