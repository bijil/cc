﻿Imports System.IO
Imports CAMACloud.BusinessLogic

Partial Class parcels_priorities
    Inherits System.Web.UI.Page
    Dim keys As New List(Of String)
    Dim noErrors As Boolean = True
    Public group As List(Of parcels_priorities)
    Public Property grName As String
    Public Property assn As String
    Private _context As HttpContext
    Public Property keyCount As Integer

    Public ReadOnly Property EnableNewPriorities As Boolean
        Get
            Dim EnableNewPriority = False
            Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM ClientSettings WHERE Name = 'EnableNewPriorities' AND Value = 1")
            If dr IsNot Nothing Then
                EnableNewPriority = True
            End If
            Return EnableNewPriority
        End Get
    End Property

    Function KeyList() As String
        Dim keys As New List(Of String)
        For i = 1 To 10
            If Database.Tenant.Application.KeyField(i).IsNotEmpty Then
                keys.Add(Database.Tenant.Application.KeyField(i))
            End If
        Next
        If Database.Tenant.Application.ShowKeyValue1 = False Then
            keys.Remove(Database.Tenant.Application.KeyField(1))
        End If
        If Database.Tenant.Application.ShowAlternateField = True Then
            keys.Add(Database.Tenant.Application.AlternateKeyfield)
        End If
        If keys.Count > 0 Then
            pNoCK.Visible = True
        Else
            pNoCK.Visible = False
        End If

        pHaveCK.Visible = keys.Count > 0
        hdnKeys.Value = keys.Count
        Return String.Join(", ", keys.ToArray.Select(Function(x) "<strong>" + x + "</strong>"))
    End Function

    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim dType As String = ""
            If ClientSettings.EnableAdhocAssignment Then
                'hdnAdHoc.Value = If((chkCreateNbhd.Checked OrElse chkCreateMultiNbhd.Checked), "1", "0")
                phAdhoc.Visible = True
            Else
                phAdhoc.Visible = False
                hdnAdHoc.Value = "0"

            End If
            Dim ColumnHeader As New List(Of String)()
            ColumnHeader.Add("1")
            For i = 1 To 10
                If Database.Tenant.Application.KeyField(i).IsNotEmpty Then
                    ColumnHeader.Add(Database.Tenant.Application.KeyField(i))
                End If
            Next
            If Database.Tenant.Application.ShowKeyValue1 = False Then
                ColumnHeader.Remove(Database.Tenant.Application.KeyField(1))
            End If
            If Database.Tenant.Application.ShowAlternateField = True Then
                ColumnHeader.Add(Database.Tenant.Application.AlternateKeyfield)
            End If
            ColumnHeader.Add("PRIORITY")
            ColumnHeader.Add("MESSAGE")
            If ClientSettings.EnableAdhocAssignment Then
                ColumnHeader.Add("GROUPNAME")
            End If
            lbnHeaderInfo.Attributes.Add("keyfields", String.Join(",", ColumnHeader))
            hdnViewInQCNbhdIds.Value = ""
            hdnRemoveNbhdIds.Value = ""
            'txtNewGroupNumber.Attributes("onBlur") = "verifyAdHocOnCreate()"
            Dim drEx As DataRow = Database.Tenant.GetTopRow("EXEC Sp_PriorityUploadFileInfo")
            lblCorrect.Text = GetString(drEx, "Correct")
            lblInCorrect.Text = GetString(drEx, "InCorrect")
            'LoadUserList()
            If Request.QueryString("Name") IsNot Nothing And Request.QueryString("Name") = "QCRedirect" Then
                ViewState("priorityTableIds") = System.Text.ASCIIEncoding.ASCII.GetString(System.Convert.FromBase64String(Request.QueryString("PrioList")))
                step1.Attributes.Add("style", "display:none")
                restoreFile(System.Text.ASCIIEncoding.ASCII.GetString(System.Convert.FromBase64String(Request.QueryString("fileName"))))
                RunScript("reloadFromQC();")
            ElseIf Request.QueryString("Name") IsNot Nothing And Request.QueryString("Name") = "CreateAdhoc" Then
                ViewState("priorityTableIds") = HttpContext.Current.Session("ptid")
                hfIsCreateAdhoc.Value = "1"
                step1.Attributes.Add("style", "display:none")
                Dim t = HttpContext.Current.Session("ptid")
                If t = Nothing Then
                	RunScript("closeAdhocCreation();")	
                Else
                	RunScript("reloadSecondStep(" + t.ToString() + ");")
                End If
            Else
                RunScript("reoveIsCreateAdhoc();")
            End If
            If (Not IsNothing(Cache("NotApprovedParcels"))) Then
                Cache.Remove("NotApprovedParcels")
            End If
        Else
            If hdnIsMultipleAdhocs.Value <> "1" Then
    			hdnmultinbhdprocess.Value="0"
    		End If
        End If
    End Sub


    Public Sub ExportCSV(sender As Object, e As EventArgs)

        Dim keys As New List(Of String)
        For i = 1 To 10
            If Database.Tenant.Application.KeyField(i).IsNotEmpty Then
                keys.Add(Database.Tenant.Application.KeyField(i))
            End If
        Next
        If Database.Tenant.Application.ShowKeyValue1 = False Then
            keys.Remove(Database.Tenant.Application.KeyField(1))
        End If
        If Database.Tenant.Application.ShowAlternateField = True Then
            keys.Add(Database.Tenant.Application.AlternateKeyfield)
        End If
        Dim dt As DataTable
		Try
	        Dim keysFields As String = String.Join(",", keys.ToArray())
	        Dim ca As New ClientApplication(Database.Tenant)
	        Dim neighborhoodNumberField As String = ca.NeighborhoodField
	        Dim dType = Database.Tenant.GetStringValue("Select Data_Type FROM information_schema.columns WHERE table_name = 'NeighborhoodData' AND column_name='" + neighborhoodNumberField + "'")
            'Dim adhocName As String =iif (dType = "int",0001,"TestAdhoc")
            Dim sqlquery As String
            If EnableNewPriorities Then
                sqlquery = "SELECT " & keysFields & ", PRIORITY = CASE WHEN RowNumber = 1 THEN 0 WHEN RowNumber = 2 THEN 1 WHEN RowNumber = 3 THEN 2 WHEN RowNumber = 4 THEN 3 WHEN RowNumber = 5 THEN 4 WHEN RowNumber = 6 THEN 5 END ,'sample field alert message' AS MESSAGE FROM (SELECT TOP 6 " & keysFields & ",ROW_NUMBER() OVER(ORDER BY RowUID ASC) AS RowNumber FROM parceldata pd inner join parcel p on p.id=pd.CC_ParcelId) a ORDER BY PRIORITY"
            Else
                sqlquery = "SELECT " & keysFields & ", PRIORITY = CASE WHEN RowNumber in (1,4)THEN 0 WHEN RowNumber in (2,5) THEN 1 WHEN RowNumber in (3,6) THEN 2 END ,'sample field alert message' AS MESSAGE FROM (SELECT TOP 6 " & keysFields & ",ROW_NUMBER() OVER(ORDER BY RowUID ASC) AS RowNumber FROM parceldata pd inner join parcel p on p.id=pd.CC_ParcelId) a ORDER BY PRIORITY"
            End If
            dt = Database.Tenant.GetDataTable(sqlquery)
        Catch ex As Exception
	    	Alert("An unexpected error occurred, couldn't complete the download process.")
	    	Return
        End Try

        Dim csv As String = String.Empty
        For Each column As DataColumn In dt.Columns
            csv += column.ColumnName + ","c
        Next
        csv = csv.Trim().Remove(csv.LastIndexOf(","))
        csv += vbCr & vbLf

        For Each row As DataRow In dt.Rows
            For Each column As DataColumn In dt.Columns
                csv += row(column.ColumnName).ToString().Replace(",", ";") + ","c
            Next
            csv = csv.Trim().Remove(csv.LastIndexOf(","))
            csv += vbCr & vbLf
        Next

        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=SamplePriorityList.csv")
        Response.Charset = ""
        Response.ContentType = "application/text"
        Response.Output.Write(csv)
        Response.Flush()
        Response.End()
    End Sub


    Sub restoreFile(optional fname As String = "" )
   	Dim Filename As String
   	If fname <> "" Then
   		Filename = fname
   		hdnFilePath.Value= fname
   	Else
   		Filename = hdnFilePath.Value()
   	End If	
    	If Filename<>"" Then
    		Dim FileInfo As New FileInfo(Filename)
        	txtFileName.Text = FileInfo.Name
    	End If
   End Sub


    <System.Web.Services.WebMethod()>
    Public Shared Function UserDetails() As Array
        Dim uc = Membership.GetAllUsers
        Dim users(uc.Count - 1) As MembershipUser
        Membership.GetAllUsers().CopyTo(users, 0)
        Dim fielduser = users.Where(Function(x) Roles.IsUserInRole(x.UserName, "MobileAssessor"))
        Dim dataUsers As New DataTable()
        dataUsers.Columns.Add("UserName")
        For Each u As MembershipUser In fielduser
            Dim dr As DataRow = dataUsers.NewRow
            dr("UserName") = u.UserName
            dataUsers.Rows.Add(dr)
        Next

        Dim userFromSettings As DataTable = Database.Tenant.GetDataTable("SELECT FirstName,LastName,LoginId FROM UserSettings")
        Dim query = From A In userFromSettings.AsEnumerable
                    Join B In dataUsers.AsEnumerable On
              A.Field(Of String)("LoginId") Equals B.Field(Of String)("UserName")
                    Order By If((A.Field(Of String)("FirstName") + " " + A.Field(Of String)("LastName")), B.Field(Of String)("UserName"))
                    Select New With {
              .LoginId = B.Field(Of String)("UserName"),
              .UserName = If((A.Field(Of String)("FirstName") + " " + A.Field(Of String)("LastName")), B.Field(Of String)("UserName"))
        }

        Return query.ToArray()
    End Function

End Class
