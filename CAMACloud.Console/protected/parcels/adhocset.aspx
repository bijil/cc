﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ParcelManager.master" CodeBehind="adhocset.aspx.vb" Inherits="CAMACloud.Console.adhocset" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <h1>Upload Adhoc List (Temporary)</h1>
    <p>Number of flagged ad hoc groups: <asp:Label runat="server" ID="lblAdHocs" /></p>
    <p class="info">Upload TXT list of Ad hoc groups to be flagged as per new method. The content should contain no header line, and one group per line.</p> 
    <p style="margin: 5px 0px; padding: 12px 5px; background: #DFDFDF; border: 1px solid #CFCFCF; width: 900px;">
        <asp:FileUpload runat="server" ID="ControlFile" Width="400px" accept=".txt" />
    </p>
    <p>
        <asp:Button runat="server" ID="btnUploadList" Text=" Upload List " />&nbsp;
        <asp:Button runat="server" ID="btnSwitch" Text=" Switch to New Ad hoc Method " />
    </p>
</asp:Content>
