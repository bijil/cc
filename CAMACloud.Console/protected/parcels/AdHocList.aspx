﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ParcelManager.master" CodeBehind="AdHocList.aspx.vb" Inherits="CAMACloud.Console.AdHocList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<style type="text/css">		
        .asc {
        	width: 20px;
        	height: 20px;
        	cursor: pointer;
        	border: 1px solid black;
    	    position: absolute;
        }
        .col-nbhd
		{
			width: 120px;
			word-break: break-all;
		}

        .master-prompt {
            display: none;
            position: fixed;
            z-index: 100;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            overflow: auto;
            background-color: rgba(0,0,0,0.4);
        }

        .master-prompt-main {
            background-color: #e1e1e1;
            margin: 10% auto;
            padding: 20px;
            border: 3px solid #348eeb;
            width: 300px;
			border-radius: 10px;
			height: 545px;

        }

        .master-prompt-content {
            height: 510px;
        }

        .radio-cls {
            align-items: center;
            padding: 15px 0px 0px 25px;
            font-size: 18px;
        }

        .radio-box {
            display: flex;
            align-items: center;
            margin-top: 15px;
        }

            .radio-box table {
                border-collapse: separate;
                border-spacing: 2px 0px;
            }
        
            .radio-box td {
                border: 1px solid white;
                padding: 10px;
                border-radius: 5px;
                width: 75px;
            }

                .radio-box td input {
                    cursor: pointer;
	            }

	            .radio-box td label {
                    margin-left: 5px;
                    cursor: pointer;
	            }

        .master-prompt-buttons {
            display: flex;
            justify-content: center; 
            align-items: center;
        }

		.master-prompt-ok {
			background-color: #348eeb;
			width: 85px;
			border: none;
			border-radius: 5px;
			height: 40px;
			color: white;
            float: right;
		}

        .master-prompt-cancel {
            color: #348eeb;
            float: right;
            font-size: 30px;
            font-weight: bold;
            margin-top: -22px;
        }

        .master-prompt-cancel:hover,
        .master-prompt-cancel:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
        }

        .master-prompt-ok:hover,
        .master-prompt-ok:focus {
            background-color: #255689;
            cursor: pointer;
        }
	</style>
	
    <script type="text/javascript">


        var selectedItems = [];

        $("[id*=chkHeader]").live("click", function () {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("input[type=checkbox]", grid).each(function () {
                if (chkHeader.is(":checked")) {
                    $(this).attr("checked", "checked");
                } else {
                    $(this).removeAttr("checked");
                }
                bindGridSelectionEvent()
            });
           toggleButtons();
        });

        $('.select-item input').live('click', function () {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeader]", grid);
            if (!$(this).is(":checked")) {
                chkHeader.removeAttr("checked");
            } else {
                if ($("[id*=Selected]", grid).length == $("[id*=Selected]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }
            bindGridSelectionEvent();
            toggleButtons();
        })
        function bindGridSelectionEvent() {
            $('.select-item input').each(function () {
                var gid = $(this).parent().attr('gid');
                var selected = $(this).attr('checked') == 'checked';
                if (selected) {
                    if (selectedItems.indexOf(gid) == -1)
                        selectedItems.push(gid);
                } else {
                    if (selectedItems.indexOf(gid) != -1)
                        selectedItems.splice(selectedItems.indexOf(gid), 1);
                }
            })
            $('#<%= hfCount.ClientID%>').val(selectedItems);
            $('#<%= lblSelectedAGCount.ClientID%>').text(selectedItems.length);
        }
	function toggleButtons(){
		if(selectedItems.length > 0){
		$('.relink-all').hide();
		$('.delete-all').hide();
		$('.relink-selected').show();
		$('.delete-selected').show();
		}
		else{
		$('.relink-all').show();
		$('.delete-all').show();
		$('.relink-selected').hide();
		$('.delete-selected').hide();
		}
	}
        function selectedAdHocItems() {
            selectedItems = $('#<%= hfCount.ClientID%>').val().split(',');
            $('#<%= lblSelectedAGCount.ClientID%>').text(selectedItems.length);
        }

        function GetselectedAdHocItems() {
            if ($(".select-item input:checkbox:not(:checked)").length == 0)
                $("[id*=chkHeader]").attr("checked", "checked");
        }

        function clearSelection() {
            selectedItems.splice(0);
            $('.selected-count').html(selectedItems.length);
        }

        function validateSelection(action, val) {
        	if(action == 'deleteCompleted') {
        		var _res = confirm('Are you sure you want to delete the completed group(s)?');
        		if( !_res )
        			return false;
    			showMask();
        	}
        	else if(action == 'relinkCompleted') {
        		var _res = confirm('Are you sure you want to relink the completed group(s)?');
        		if( !_res )
        			return false;
        		showMask();
    		}
        	else {
	            if (selectedItems.length == 0) {
	                alert('You have not selected any groups from the list.');
	                return false;
	            }
	            else if(action == 'deleteSelected'){
	            	var _res = confirm('Are you sure you want to delete the selected group(s)?');
        			if( !_res )
        				return false;
	            }
	            showMask();
            }

            showMasterPopup(val);
            return true;
        }

        function showMasterPopup(v) {
            $('#MainContent_MainContent_reAction').val(v);
            $('.master-radio input[type="radio"][value="0"]').each((i, el) => {
                $(el)[0].checked = true;
            });

            $('.master-prompt').show();
            return false;
        }

        function hideMasterPopup(hidePromt) {
            $('.master-prompt').hide();
            if (hidePromt != '1') hideMask();
            return true;
        }

        function validateClearSelection(action) {
        	$('.relink-all').show();
			$('.delete-all').show();
			$('.relink-selected').hide();
			$('.delete-selected').hide();
            if (selectedItems.length == 0) {
                return false;
            }
        }

        function toggleAscDesc(click) {
        	var val = $('#<%= btnAscDesc.ClientID%>').val();
        	if ($('#<%= ddlOrderBy.ClientID%>').val() == "TotalParcels") $('.asc').hide();
        	else $('.asc').show();
        	
        	debugger;
        	if (click) {
            	if (val == "Descending") {
            		$('.asc').css('background-position-x','1px');
            	}
            	else if (val == "Ascending") {
            		$('.asc').css('background-position-x','-18px');
            	}
            	if (click) $('#<%= btnAscDesc.ClientID%>').click();
        	}
        	else {
        		if (val == "Descending") {
            		$('.asc').css('background-position-x','-18px');
            	}
            	else if (val == "Ascending") {
            		$('.asc').css('background-position-x','1px');
            	}
        	}
        }
        function showResultMask() {
            $('.mask').show();
            $('.mask').height($(document).height());
            $('.mask').width('100%');
            window.scrollTo(0, 0);
            $('body').css('overflow', 'hidden');
          }

        function hideResultMask() {
            $('.masklayer').hide();
            $('.mask').hide();
            $('body').css('overflow', 'auto');
        }
        $(function () {
            bindGridSelectionEvent();
        })
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Manage Ad Hoc Groups</h1>
    <div>
		<span class="spacer"></span>No. of items per page:
		<asp:DropDownList runat="server" ID="ddlPageSize" AutoPostBack="true">
			<asp:ListItem Value="25" />
			<asp:ListItem Value="50" />
			<asp:ListItem Value="100" />
			<asp:ListItem Value="250" />
		</asp:DropDownList>
		<span class="spacer" style="margin-left:10px"></span>Sort by:
		<asp:DropDownList runat="server" ID="ddlOrderBy" AutoPostBack="true">
			<asp:ListItem Text="Appraiser" Value="AssignedTo" />
			<asp:ListItem Text="Total Parcels (Max First)" Value="TotalParcels" />
			<asp:ListItem Text="Start Date" Value="FirstVisitedDate" />
		</asp:DropDownList>
        &nbsp;
        <asp:DropDownList runat="server" ID="ddlFilterOperator" AutoPostBack="true" >
            <asp:ListItem Text="-- All --" Value="0" />
			<asp:ListItem Text="Equal to" Value="1" />
			<asp:ListItem Text="Contains" Value="2" />
			<asp:ListItem Text="Starts with" Value="3" />
            <asp:ListItem Text="Ends with" Value="4" />
		</asp:DropDownList>
        &nbsp;
        <asp:TextBox ID="tbSearchText" runat="server" Visible ="false" Width="120px" placeholder="Enter text forGetselectedAdHocItems() search"></asp:TextBox>  
        &nbsp;   
           <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" Visible="false" />
        &nbsp;   
           <asp:Button ID="btnAscDesc" runat="server" style="display: none;" Text="Ascending" OnClick="btnAscDesc_Click" />           
           <input type="button" class="asc" style="background:url(/App_Static/images/asc-desc-new.png); background-repeat: no-repeat; background-position-x: 1px; background-size: 206%;" onclick="toggleAscDesc(true);" />
	</div>
    <div style="margin-top:5px">
        <asp:HiddenField ID="hfCount" runat="server"  />
        <span ID="selectedCount" runat="server">Selected Assignment Groups Count: </span><asp:Label ID="lblSelectedAGCount" class="selected-count" Text="0" runat="server" ></asp:Label>
    </div>
    <div style="margin-top: 15px;">
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <table style="border-spacing: 0px; width: 99%; font-weight: bold;">
                    <tr>
                        <td>
					        <asp:Label runat="server" ID="lblPage" style="margin-right:10px"/><span class="spacer"></span>
                            <asp:LinkButton runat="server" cssClass = "tip clear-selected" abbr="Clear all the selected Assignment Groups" ID="lbClearSel" Text="Clear Selection" OnClientClick="return validateClearSelection();"/>
                        </td>
                        <td style="text-align: right;">
                            <asp:LinkButton runat="server" cssClass = "tip delete-all" style="padding-right:4px" abbr="Delete all Ad-Hoc Groups with 0 pending priorities." ID="lbDeleteAll" tip-margin-left="130"  Text="Delete Completed Groups" OnClientClick="return validateSelection('deleteCompleted', 1);" />
                            <asp:LinkButton runat="server" cssClass = "tip delete-selected" style="padding-right:4px;display:none;" abbr="Delete Selected Ad-Hoc Groups." ID="lbDelete" Text="Delete Selected" tip-margin-left="150" OnClientClick="return validateSelection('deleteSelected', 2);" />
                            <asp:LinkButton runat="server" cssClass = "tip relink-all" ID="lbRelinkAll" style="padding-right:4px" abbr="Relink all Ad-Hoc Groups with 0 pending priorities." tip-margin-left="290" Text="Relink Completed Groups" OnClientClick="return validateSelection('relinkCompleted', 3);"/>
                            <asp:LinkButton runat="server" cssClass = "tip relink-selected" ID="lbRelink" style="padding-right:4px;display:none;"  abbr="Relink Selected Ad-Hoc Groups." Text="Relink Selected" tip-margin-left="260" OnClientClick="return validateSelection('relinkCompleted', 4);"  />
                        </td>
                    </tr>
                </table>
                <%If EnableNewPriorities Then %>
                    <asp:GridView runat="server" ID="gvNbhdNew" AllowPaging="true" PageSize="25" Width = "99%">
                        <Columns>
                            <asp:TemplateField ItemStyle-Width="20px" HeaderText=" ">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkHeader" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="Selected" runat="server" gid='<%# Eval("Id")%>' class='select-item' />
                                    <asp:HiddenField ID="GID" runat="server" Value='<%# Eval("Id")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Number" HeaderText="Assgn Grp#" ItemStyle-CssClass="col-nbhd" />
                            <asp:BoundField DataField="AssignedTo" HeaderText="Appraiser" ItemStyle-Width="120px" />
                            <asp:BoundField DataField="TotalParcels" HeaderText="Total Parcels" ItemStyle-CssClass="count-col col-total"
                                HeaderStyle-CssClass="count-head head-total" />
                            <asp:BoundField DataField="TotalCritical" HeaderText="Total Critical" ItemStyle-CssClass="count-col col-total"
					            HeaderStyle-CssClass="count-head head-total" />
                            <asp:BoundField DataField="TotalAlerts" HeaderText="Total Urgent" ItemStyle-CssClass="count-col col-total"
					            HeaderStyle-CssClass="count-head head-total" />
                            <asp:BoundField DataField="TotalPriorities" HeaderText="Total High" ItemStyle-CssClass="count-col col-total"
					            HeaderStyle-CssClass="count-head head-total" />
                            <asp:BoundField DataField="TotalMedium" HeaderText="Total Medium" ItemStyle-CssClass="count-col col-total"
					            HeaderStyle-CssClass="count-head head-total" />
                            <asp:BoundField DataField="TotalNormal" HeaderText="Total Normal" ItemStyle-CssClass="count-col col-total"
					            HeaderStyle-CssClass="count-head head-total" />
                            <asp:BoundField DataField="VisitedParcels" HeaderText="Visited Parcels" ItemStyle-CssClass="count-col col-visited"
                                HeaderStyle-CssClass="count-head head-visited" />
                            <asp:BoundField DataField="VisitedCritical" HeaderText="Visited Critical " ItemStyle-CssClass="count-col col-visited"
					            HeaderStyle-CssClass="count-head head-visited" />
                            <asp:BoundField DataField="VisitedAlerts" HeaderText="Visited Urgent" ItemStyle-CssClass="count-col col-visited"
					            HeaderStyle-CssClass="count-head head-visited" />
                            <asp:BoundField DataField="VisitedPriorities" HeaderText="Visited High" ItemStyle-CssClass="count-col col-visited"
					            HeaderStyle-CssClass="count-head head-visited" />
                            <asp:BoundField DataField="VisitedMedium" HeaderText="Visited Medium" ItemStyle-CssClass="count-col col-visited"
					            HeaderStyle-CssClass="count-head head-visited" />
                            <asp:BoundField DataField="VisitedNormal" HeaderText="Visited Normal" ItemStyle-CssClass="count-col col-visited"
					            HeaderStyle-CssClass="count-head head-visited" />
                            <asp:BoundField DataField="PendingPriorities" HeaderText="Pending Priorities" ItemStyle-CssClass="count-col col-visited"
                                HeaderStyle-CssClass="count-head head-visited" />
                            <asp:BoundField DataField="LastVisitedDate" HeaderText="Last Activity" DataFormatString="{0:MM/dd/yyyy}"
                                ItemStyle-CssClass="date-col" HeaderStyle-CssClass="date-head" ItemStyle-Width="82px"/>
                        </Columns>
                    </asp:GridView>
                <%Else %>
                    <asp:GridView runat="server" ID="gvNbhd" AllowPaging="true" PageSize="25" Width = "99%">
                        <Columns>
                            <asp:TemplateField ItemStyle-Width="20px" HeaderText=" ">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkHeader" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="Selected" runat="server" gid='<%# Eval("Id")%>' class='select-item' />
                                    <asp:HiddenField ID="GID" runat="server" Value='<%# Eval("Id")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Number" HeaderText="Assgn Grp#" ItemStyle-CssClass="col-nbhd" />
                            <asp:BoundField DataField="AssignedTo" HeaderText="Appraiser" ItemStyle-Width="120px" />
                            <asp:BoundField DataField="TotalParcels" HeaderText="Total Parcels" ItemStyle-CssClass="count-col col-total"
                                HeaderStyle-CssClass="count-head head-total" />
                             <asp:BoundField DataField="TotalAlerts" HeaderText="Total Urgent Priorities" ItemStyle-CssClass="count-col col-total"
                                HeaderStyle-CssClass="count-head head-total" />
                            <asp:BoundField DataField="TotalPriorities" HeaderText="Total High Priorities " ItemStyle-CssClass="count-col col-total"
                                HeaderStyle-CssClass="count-head head-total" />
                            <asp:BoundField DataField="VisitedParcels" HeaderText="Visited Parcels" ItemStyle-CssClass="count-col col-visited"
                                HeaderStyle-CssClass="count-head head-visited" />
                            <asp:BoundField DataField="VisitedAlerts" HeaderText="Visited Urgent Priorities" ItemStyle-CssClass="count-col col-visited"
                                HeaderStyle-CssClass="count-head head-visited" />
                            <asp:BoundField DataField="VisitedPriorities" HeaderText="Visited High Priorities" ItemStyle-CssClass="count-col col-visited"
                                HeaderStyle-CssClass="count-head head-visited" />
                            <asp:BoundField DataField="PendingPriorities" HeaderText="Pending Priorities" ItemStyle-CssClass="count-col col-visited"
                                HeaderStyle-CssClass="count-head head-visited" />
                            <asp:BoundField DataField="LastVisitedDate" HeaderText="Last Activity" DataFormatString="{0:MM/dd/yyyy}"
                                ItemStyle-CssClass="date-col" HeaderStyle-CssClass="date-head" ItemStyle-Width="82px"/>
                        </Columns>
                    </asp:GridView>
                <%End If %>
            </ContentTemplate>

        </asp:UpdatePanel>

    </div>
    <div id="masterprompt" class="master-prompt">
		<div class="master-prompt-main">
            <div class="master-prompt-content">
                <span class="master-prompt-cancel" onclick="return hideMasterPopup();">&times;</span>
                <asp:HiddenField ID="reAction" runat="server" Value="0"  />
                <div class="radio-cls">
                    <asp:Label ID="lblClearParcelPriority" runat="server" Text="Clear the Parcel’s Priority?" AssociatedControlID="rblClearParcelPriority"></asp:Label>
                    <div class="radio-box">
                        <asp:RadioButtonList ID="rblClearParcelPriority" runat="server" RepeatDirection="Horizontal" abbr="This will set the Priority back to 0." CssClass="tip master-radio">
                            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                            <asp:ListItem Text="No" Value="0" Selected="True"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="radio-cls">
                    <asp:Label ID="lblClearParcelNotes" runat="server" Text="Clear the Parcel's Alert from Field Notes?" AssociatedControlID="rblClearParcelNotes"></asp:Label>
                    <div class="radio-box">
                        <asp:RadioButtonList ID="rblClearParcelNotes" runat="server" RepeatDirection="Horizontal" abbr="This will clear the ‘Notes’ on the parcel that appear on the Quick Review screen in mobile, and Parcel Dashboard in QC." CssClass="tip master-radio">
                            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                            <asp:ListItem Text="No" Value="0" Selected="True"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="radio-cls ">
                    <asp:Label ID="lblClearParcelFieldAlert" runat="server" Text="Clear the Field Alert Flag?" AssociatedControlID="rblClearParcelFieldAlert"></asp:Label>
                    <div class="radio-box">
                        <asp:RadioButtonList ID="rblClearParcelFieldAlert" runat="server" RepeatDirection="Horizontal" abbr="This will clear the Alert from Field flag set by the appraiser in the last visit." CssClass="tip master-radio">
                            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                            <asp:ListItem Text="No" Value="0" Selected="True"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    
                </div>
                <div class="radio-cls">
                    <asp:Label ID="lblClearParcelOfficeAlert" runat="server" Text="Clear the Alert from Office?" AssociatedControlID="rblClearParcelOfficeAlert"></asp:Label>
                    <div class="radio-box">
                        <asp:RadioButtonList ID="rblClearParcelOfficeAlert" runat="server" RepeatDirection="Horizontal" abbr="This will clear the Alert from Office, the reason for the last field visit." CssClass="tip master-radio">
                            <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                            <asp:ListItem Text="No" Value="0" Selected="True"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
                <p style="font-size: 15px; font-weight: 600;">If you do not want to update all parcels,<br> you can update individually through the Quality Control module.</p>
            </div>
			<div class="master-prompt-buttons">
                <asp:Button ID="masterOkBtn" runat="server" CssClass="master-prompt-ok" title="If you do not want to update all parcels, you can update through the Quality Control module." Text="Ok" OnClientClick="return hideMasterPopup(1);" />  
            </div>
		</div>
	</div>
</asp:Content>
