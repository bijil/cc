﻿Public Class AdHocList
    Inherits System.Web.UI.Page

    Public ReadOnly Property EnableNewPriorities As Boolean
        Get
            Dim EnableNewPriority = False
            Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM ClientSettings WHERE Name = 'EnableNewPriorities' AND Value = 1")
            If dr IsNot Nothing Then
                EnableNewPriority = True
            End If
            Return EnableNewPriority
        End Get
    End Property

    Sub LoadGrid()
    	Dim orderBy As String = ddlOrderBy.SelectedValue
    	Dim filterField As String = ""
    	Dim filterOperatorVal As Integer = Convert.ToInt16(ddlFilterOperator.SelectedValue.ToString())
    	Dim searchText As String = tbSearchText.Text
        Select Case ddlOrderBy.SelectedValue
        	Case "Number"
        		orderBy = "Number"
        		If searchText <> Nothing AndAlso searchText <> "" Then        			
	        		Select Case filterOperatorVal
	        		    Case 1
	        		    	filterField = " AND Number = '" + searchText + "'"
	        		    Case 2
	        		    	filterField = " AND Number Like '%" + searchText + "%'"
	        		    Case 3
	        		    	filterField = " AND Number like '" + searchText + "%'"
	        		    Case 4
	        		    	filterField = " AND Number like '%" + searchText + "'"        				
	        		End Select
        		End If
            Case "TotalParcels"
                orderBy = "TotalParcels DESC"
            Case "AssignedTo"
                orderBy = "COALESCE(AssignedTo, 'ZZZZZZ')"
            Case "FirstVisitedDate"
                orderBy = "COALESCE(FirstVisitedDate, '2999-12-31')"
        End Select
        Dim sortBy As String = IIf(ddlOrderBy.SelectedValue = "TotalParcels", "", IIf(btnAscDesc.Text = "Ascending", " ASC", " DESC"))
        Dim dt As DataTable
        If EnableNewPriorities Then
            dt = d_("SELECT nd.Id, Number, CONCAT(us.FirstName,' ',us.LastName) AS AssignedTo, TotalParcels, TotalAlerts, TotalPriorities, VisitedParcels, VisitedAlerts, VisitedPriorities, PendingPriorities, TotalPriorityparcels, TotalVisitedPriorityParcels, TotalCritical, TotalMedium, TotalNormal, VisitedCritical, VisitedMedium, VisitedNormal, dbo.GetLocalDate(LastVisitedDate) AS LastVisitedDate FROM Neighborhood nd LEFT JOIN UserSettings us ON	us.LoginId = nd.AssignedTo WHERE IsAdhoc = 1" + filterField + " ORDER BY " + orderBy + sortBy)
        Else
            dt = d_("SELECT nd.Id,Number,CONCAT(us.FirstName,' ',us.LastName) AS AssignedTo,TotalParcels,TotalAlerts,TotalPriorities,VisitedParcels,VisitedAlerts,VisitedPriorities,PendingPriorities,dbo.GetLocalDate(LastVisitedDate) AS LastVisitedDate FROM Neighborhood nd LEFT JOIN UserSettings us ON	us.LoginId = nd.AssignedTo WHERE IsAdhoc = 1" + filterField + " ORDER BY " + orderBy + sortBy)
        End If
        If EnableNewPriorities Then
            gvNbhdNew.DataSource = dt
            gvNbhdNew.DataBind()
        Else
            gvNbhd.DataSource = dt
            gvNbhd.DataBind()
        End If
        If (SelectionState.Count <> 0) Then
            RunScript("selectedAdHocItems();")
            RunScript("GetselectedAdHocItems();")
        End If
        If EnableNewPriorities Then
            gvNbhdNew.Visible = True
            gvNbhd.Visible = False
        Else
            gvNbhd.Visible = True
            gvNbhdNew.Visible = False
        End If

        lblPage.Visible = True
        selectedCount.Visible = True
        lblSelectedAGCount.Visible = True
        If EnableNewPriorities Then
            If (gvNbhdNew.Rows.Count > 0) Then
                lbRelinkAll.Visible = True
                lbDeleteAll.Visible = True
                lblPage.Text = "Displaying records {0} to {1} of {2}".FormatString(gvNbhdNew.PageIndex * gvNbhdNew.PageSize + 1, gvNbhdNew.PageIndex * gvNbhdNew.PageSize + gvNbhdNew.Rows.Count, dt.Rows.Count)
            Else
                lbRelinkAll.Visible = False
                lbDeleteAll.Visible = False
                lblPage.Text = "Displaying records {0} to {1} of {2}".FormatString(gvNbhdNew.PageIndex * gvNbhdNew.PageSize, gvNbhdNew.PageIndex * gvNbhdNew.PageSize + gvNbhdNew.Rows.Count, dt.Rows.Count)
            End If
        Else
            If (gvNbhd.Rows.Count > 0) Then
                lbRelinkAll.Visible = True
                lbDeleteAll.Visible = True
                lblPage.Text = "Displaying records {0} to {1} of {2}".FormatString(gvNbhd.PageIndex * gvNbhd.PageSize + 1, gvNbhd.PageIndex * gvNbhd.PageSize + gvNbhd.Rows.Count, dt.Rows.Count)
            Else
                lbRelinkAll.Visible = False
                lbDeleteAll.Visible = False
                lblPage.Text = "Displaying records {0} to {1} of {2}".FormatString(gvNbhd.PageIndex * gvNbhd.PageSize, gvNbhd.PageIndex * gvNbhd.PageSize + gvNbhd.Rows.Count, dt.Rows.Count)
            End If
        End If

        RunScript ("$('.tip').tipr()")
        RestoreSelectionState()
    End Sub

Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            RunScript("toggleAscDesc();")
        End If
        If EnableNewPriorities Then
            gvNbhdNew.Visible = True
            gvNbhd.Visible = False
        Else
            gvNbhdNew.Visible = False
            gvNbhd.Visible = True
        End If
        If Not IsPostBack Then
        	Database.Tenant.Execute("EXEC cc_UpdateNeighborhoodStatistics NULL, 0")
            ddlOrderBy.InsertItem(CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName(), "Number", 0)
            ddlOrderBy.SelectedIndex = 0
            LoadGrid()
        End If
        If (ddlOrderBy.SelectedIndex = 0) Then
            ddlFilterOperator.Visible = True
        Else
            ddlFilterOperator.SelectedIndex = 0
            ddlFilterOperator.Visible = False
            tbSearchText.Text = ""
            tbSearchText.Visible = False
            btnSearch.Visible = False
        End If
    End Sub

    Private Sub gvNbhd_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvNbhd.PageIndexChanging
        SaveSelectionState()
        gvNbhd.PageIndex = e.NewPageIndex
        LoadGrid()
    End Sub
    Private Sub gvNbhdNew_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvNbhdNew.PageIndexChanging
        SaveSelectionState()
        gvNbhdNew.PageIndex = e.NewPageIndex
        LoadGrid()
    End Sub

    Public ReadOnly Property SelectionState As List(Of Integer)
		Get
            If ViewState("GridSelection") Is Nothing Then
                ViewState("GridSelection") = New List(Of Integer)
            End If
			Return ViewState("GridSelection")
		End Get
	End Property

    Sub SaveSelectionState()
        If EnableNewPriorities Then
            For Each gr As GridViewRow In gvNbhdNew.Rows
                Dim gid As Integer = gr.GetHiddenValue("GID")
                If gr.GetChecked("Selected") Then
                    If Not SelectionState.Contains(gid) Then
                        SelectionState.Add(gid)
                    End If
                Else
                    If SelectionState.Contains(gid) Then
                        SelectionState.Remove(gid)
                    End If
                End If
            Next
        Else
            For Each gr As GridViewRow In gvNbhd.Rows
                Dim gid As Integer = gr.GetHiddenValue("GID")
                If gr.GetChecked("Selected") Then
                    If Not SelectionState.Contains(gid) Then
                        SelectionState.Add(gid)
                    End If
                Else
                    If SelectionState.Contains(gid) Then
                        SelectionState.Remove(gid)
                    End If
                End If
            Next
        End If

    End Sub

    Sub RestoreSelectionState()
        If EnableNewPriorities Then
            For Each gr As GridViewRow In gvNbhdNew.Rows
                Dim gid As Integer = gr.GetHiddenValue("GID")
                If SelectionState.Contains(gid) Then
                    gr.GetControl(Of CheckBox)("Selected").Checked = True
                End If
            Next
        Else
            For Each gr As GridViewRow In gvNbhd.Rows
                Dim gid As Integer = gr.GetHiddenValue("GID")
                If SelectionState.Contains(gid) Then
                    gr.GetControl(Of CheckBox)("Selected").Checked = True
                End If
            Next
        End If

    End Sub

    Protected Sub ddlPageSize_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged, ddlOrderBy.SelectedIndexChanged
        SaveSelectionState()
        If EnableNewPriorities Then
            gvNbhdNew.PageSize = ddlPageSize.SelectedValue
            gvNbhdNew.PageIndex = 0
        Else
            gvNbhd.PageSize = ddlPageSize.SelectedValue
            gvNbhd.PageIndex = 0
        End If
        LoadGrid()
    End Sub

	Sub ClearSelectionState()
        SelectionState.Clear()
        If EnableNewPriorities Then
            For Each gr As GridViewRow In gvNbhdNew.Rows
                gr.GetControl(Of CheckBox)("Selected").Checked = False
            Next
            gvNbhdNew.HeaderRow.GetControl(Of CheckBox)("chkHeader").Checked = False
        Else
            For Each gr As GridViewRow In gvNbhd.Rows
                gr.GetControl(Of CheckBox)("Selected").Checked = False
            Next
            gvNbhd.HeaderRow.GetControl(Of CheckBox)("chkHeader").Checked = False
        End If
        RunScript("clearSelection();")
	End Sub

	Private Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
		If Page.IsPostBack AndAlso Request.IsAjaxRequest Then
			RunScript("bindGridSelectionEvent();")
		End If
	End Sub

	Private Sub lbClearSel_Click(sender As Object, e As EventArgs) Handles lbClearSel.Click
		ClearSelectionState()

	End Sub

    Sub lbDelete_Click(optionStr As String)
        SaveSelectionState()
        Dim processId As String = Guid.NewGuid.ToString
        Dim listInsertSql As String = ""
        Dim NBHDId As String = ""
        Dim numberField As String = Database.Tenant.Application.NeighborhoodField
        For Each i In SelectionState
            If Database.Tenant.DoRowsExist("ParcelData pv LEFT OUTER JOIN Neighborhood n ON pv." + numberField + " = n.Number", "n.Id =" + i.ToString) Then
                NBHDId += i.ToString + ","
            End If
            ParcelEventScript("Delete", i.ToString)
            listInsertSql += "INSERT INTO ProcessList (ListID, ItemId) VALUES ({0}, {1});".SqlFormat(False, processId, i) + vbNewLine
        Next
        If listInsertSql <> "" Then
            Database.Tenant.Execute(listInsertSql)
            CAMACloud.BusinessLogic.Neighborhood.RebuildNeighborhoods(Database.Tenant, True, True, processId, "", optionStr)
            If NBHDId <> "" Then
                NBHDId = NBHDId.Trim().Remove(NBHDId.Length - 1)
                Database.Tenant.Execute("UPDATE Neighborhood SET IsAdhoc =0 WHERE Id IN (" + NBHDId + ")")
                Database.Tenant.Execute("DELETE FROM ProcessList WHERE ItemId IN (" + NBHDId + ")")
            End If
            Dim filter As String = "(SELECT ItemId FROM ProcessList WHERE ListId = {0})".SqlFormat(False, processId)
            'Database.Tenant.Execute("UPDATE Parcel SET NeighborhoodId = NULL WHERE NeighborhoodId IN " + filter)
            Database.Tenant.Execute("DELETE FROM Assignment WHERE NeighborhoodId IN " + filter)
            Database.Tenant.Execute("DELETE FROM NeighborhoodData WHERE NbhdId IN " + filter)
            Database.Tenant.Execute("DELETE FROM Neighborhood WHERE Id IN " + filter)
            Database.Tenant.Execute("DELETE FROM ProcessList WHERE ListID = {0}".SqlFormat(False, processId))
            Alert(SelectionState.Count & " groups deleted.")
        End If
        ClearSelectionState()
        LoadGrid()
        ''RunScript("hideResultMask();")
    End Sub
    Sub lbDeleteAll_Click(optionStr As String)
        Dim listInsertSql As String = ""
        Dim processId As String = Guid.NewGuid.ToString
        Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT Id FROM Neighborhood WHERE Isadhoc = 1 AND Pendingpriorities = 0")
        Dim c As Integer = dt.Rows.Count
        Dim NBHDId As String = ""
        If c > 0 Then
            For i = 0 To c - 1
                NBHDId += dt.Rows(i)("Id").ToString() + ","
                ParcelEventScript("Delete", dt.Rows(i)("Id").ToString())
                listInsertSql += "INSERT INTO ProcessList (ListID, ItemId) VALUES ({0}, {1});".SqlFormat(False, processId, dt.Rows(i)("Id")) + vbNewLine
            Next
        End If
        If listInsertSql <> "" Then
            Database.Tenant.Execute(listInsertSql)
            CAMACloud.BusinessLogic.Neighborhood.RebuildNeighborhoods(Database.Tenant, True, True, processId, "", optionStr)
            If NBHDId <> "" Then
                NBHDId = NBHDId.Trim().Remove(NBHDId.Length - 1)
                Database.Tenant.Execute("UPDATE Neighborhood SET IsAdhoc = 0 WHERE Id IN (" + NBHDId + ")")
                'Database.Tenant.Execute("DELETE FROM ProcessList WHERE ItemId IN (" + NBHDId + ")")
            End If
            Dim filter As String = "(SELECT ItemId FROM ProcessList WHERE ListId = {0})".SqlFormat(False, processId)
            'Database.Tenant.Execute("UPDATE Parcel SET NeighborhoodId = NULL WHERE NeighborhoodId IN " + filter)
            Database.Tenant.Execute("DELETE FROM Assignment WHERE NeighborhoodId IN " + filter)
            Database.Tenant.Execute("DELETE FROM NeighborhoodData WHERE NbhdId IN " + filter)
            Database.Tenant.Execute("DELETE FROM Neighborhood WHERE Id IN " + filter)
            Database.Tenant.Execute("DELETE FROM ProcessList WHERE ListID = {0}".SqlFormat(False, processId))
            Alert(c & " groups deleted.")
        Else
            Alert("There are no completed assignment group to delete.")
        End If
        'ClearSelectionState()
        LoadGrid()
        ''RunScript("hideResultMask();")
    End Sub
    Sub lbRelink_Click(optionStr As String)
        SaveSelectionState()
        Dim processId As String = Guid.NewGuid.ToString
        Dim listInsertSql As String = ""
        For Each i In SelectionState
            ParcelEventScript("Relink", i.ToString)
            listInsertSql += "INSERT INTO ProcessList (ListID, ItemId) VALUES ({0}, {1});".SqlFormat(False, processId, i) + vbNewLine
        Next
        Database.Tenant.Execute(listInsertSql)
        Dim status As String = CAMACloud.BusinessLogic.Neighborhood.RebuildNeighborhoods(Database.Tenant, True, True, processId, "", optionStr)
        Alert(status)
        Database.Tenant.Execute("DELETE FROM ProcessList WHERE ListID = {0}".SqlFormat(False, processId))
        ClearSelectionState()
        LoadGrid()
        ''RunScript("hideResultMask();")
    End Sub
    Sub lbRelinkAll_Click(optionStr As String)
        Dim processId As String = Guid.NewGuid.ToString
        Dim listInsertSql As String = ""
        Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT Id FROM Neighborhood WHERE Isadhoc = 1 AND Pendingpriorities = 0")
        Dim c As Integer = dt.Rows.Count
        If c > 0 Then
            For i = 0 To c - 1
                ParcelEventScript("Relink", dt.Rows(i)("Id").ToString())
                listInsertSql += "INSERT INTO ProcessList (ListID, ItemId) VALUES ({0}, {1});".SqlFormat(False, processId, dt.Rows(i)("Id")) + vbNewLine
            Next
        End If
        If listInsertSql <> "" Then
            Database.Tenant.Execute(listInsertSql)
            Dim status As String = CAMACloud.BusinessLogic.Neighborhood.RebuildNeighborhoods(Database.Tenant, True, True, processId, "", optionStr)
            Alert(status)
            Database.Tenant.Execute("DELETE FROM ProcessList WHERE ListID = {0}".SqlFormat(False, processId))
        Else
            Alert("0 parcels relinked with Assignment Groups.")
        End If
        LoadGrid()
        ''RunScript("hideResultMask();")
    End Sub

    Private Sub masterOkBtn_Click(sender As Object, e As EventArgs) Handles masterOkBtn.Click
        Dim clickVal As String = reAction.Value
        Dim optionStr As String = rblClearParcelPriority.SelectedValue + "," + rblClearParcelNotes.SelectedValue + "," + rblClearParcelFieldAlert.SelectedValue + "," + rblClearParcelOfficeAlert.SelectedValue + "," + Membership.GetUser().ToString()
        If clickVal = "1" Then
            lbDeleteAll_Click(optionStr)
        ElseIf clickVal = "2" Then
            lbDelete_Click(optionStr)
        ElseIf clickVal = "3" Then
            lbRelinkAll_Click(optionStr)
        ElseIf clickVal = "4" Then
            lbRelink_Click(optionStr)
        End If
    End Sub

    Private Sub ddlFilterOperator_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFilterOperator.SelectedIndexChanged
        tbSearchText.Text = ""
        If (ddlFilterOperator.SelectedValue > 0) Then
            tbSearchText.Visible = True
            btnSearch.Visible = True
        Else
            tbSearchText.Visible = False
            btnSearch.Visible = False
        End If
        LoadGrid()
    End Sub
    Private Sub ddlOrderBy_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlOrderBy.SelectedIndexChanged
    	If ddlOrderBy.SelectedValue = "TotalParcels" Then
    		btnAscDesc.Visible = False
    	Else
    		btnAscDesc.Visible = True
        End If
    End Sub
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs)
        If (tbSearchText.Text = "") Then
        	Alert("You have not entered any text to filter in below results.")
        End If
		LoadGrid()
    End Sub
    Protected Sub btnAscDesc_Click(sender As Object, e As EventArgs)
    	If btnAscDesc.Text = "Ascending" Then
    		btnAscDesc.Text = "Descending"
    	Else
    		btnAscDesc.Text = "Ascending"
    	End If
    	LoadGrid()
    End Sub
    Sub ParcelEventScript(entryType As String,NBHID As String )
    	Dim numberField As String = Database.Tenant.Application.NeighborhoodField
    	Dim entry As String ="" 
    	Dim originalNbhd As String =""
    	Dim currentAdhoc As String =""
    	 Dim CurrentDate As String = Database.Tenant.GetStringValue ("SELECT GETUTCDATE()")
    	Dim dt As DataTable = Database .Tenant .GetDataTable("SELECT * FROM Parcel WHERE NeighborhoodId = {0} ".SqlFormat(False, NBHID))
    	If dt.Rows.Count > 0 Then
    		For i = 0 To dt.Rows.Count - 1
    			Dim pid = dt.Rows(i)("Id").ToString()
    			originalNbhd = Database.Tenant.GetStringValue("SELECT "+ numberField +" FROM ParcelData WHERE cc_parcelId = {0} ".SqlFormat(False, pid))
    			currentAdhoc = Database.Tenant.GetStringValue("SELECT Number FROM Neighborhood WHERE Id= {0}".SqlFormat(False,NBHID))
    			If entryType = "Relink" Then
    				entry = "Parcel relinked through Manage Ad-Hoc Groups. Parcel moved from "+ currentAdhoc +" back to "+ originalNbhd +"."
    			Else
    				entry = "Ad-Hoc Group "+ currentAdhoc +" deleted through Manage Ad-Hoc Groups. Parcel moved from "+ currentAdhoc +" back to "+ originalNbhd +"."
    			End If
                'Database.Tenant.Execute("INSERT INTO ParcelAuditTrail (EventTime,EventDate,ParcelId, LoginID, EventType, Description) Values ({0},{1},{2},{3},{4},{5})".SqlFormatString( CurrentDate, CurrentDate,pid,Membership.GetUser().ToString(),0,entry))
                ParcelAuditStream.CreateEvent(Database.Tenant, CurrentDate, pid, Membership.GetUser().ToString(), "0", entry, "Desktop")
            Next
    	End If
    	 
    End Sub
    
End Class