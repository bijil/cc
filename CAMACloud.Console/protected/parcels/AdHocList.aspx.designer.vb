﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class AdHocList

    '''<summary>
    '''ddlPageSize control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlPageSize As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlOrderBy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlOrderBy As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlFilterOperator control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlFilterOperator As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''tbSearchText control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tbSearchText As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSearch As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnAscDesc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAscDesc As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''hfCount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfCount As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''selectedCount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents selectedCount As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''lblSelectedAGCount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSelectedAGCount As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lbClearSel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbClearSel As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lbDeleteAll control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbDeleteAll As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lbDelete control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbDelete As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lbRelinkAll control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbRelinkAll As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lbRelink control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbRelink As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''gvNbhdNew control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvNbhdNew As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''gvNbhd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvNbhd As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''reAction control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents reAction As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''lblClearParcelPriority control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblClearParcelPriority As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''rblClearParcelPriority control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rblClearParcelPriority As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''lblClearParcelNotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblClearParcelNotes As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''rblClearParcelNotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rblClearParcelNotes As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''lblClearParcelFieldAlert control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblClearParcelFieldAlert As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''rblClearParcelFieldAlert control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rblClearParcelFieldAlert As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''lblClearParcelOfficeAlert control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblClearParcelOfficeAlert As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''rblClearParcelOfficeAlert control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rblClearParcelOfficeAlert As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''masterOkBtn control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents masterOkBtn As Global.System.Web.UI.WebControls.Button
End Class
