﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="autopriorityrules.aspx.vb" Inherits="CAMACloud.Console.autopriorityrules" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        $(finalizePage);
        function showMask(info, error, errorActionName, callback) {
            //window.scrollTo(0, 0);
            //theForm.elements['__SCROLLPOSITIONY'].value = 0;
            $('.masklayer').show();
            $('.masklayer').height($(document).height());
            $('body').css('overflow', 'hidden');
            if (!info)
                info = 'Please wait ...'
            var helplink = ''
            if (callback && errorActionName) {
                helplink = '<a href="#" class="app-mask-error-help-link">Click here<a/> to ' + errorActionName
            }
            if (error) {
                info = '<span style="color:Red;">' + info + '</span> ' + helplink
            }

            $('.app-mask-layer-info').html(info);
        }
        function showResultMask() {

            $('.mask').show();                 
            $('.mask').height($(document).height());
            window.scrollTo(0, 0);
            $('body').css('overflow', 'hidden');
        }
        function clroverflow() {
            $('body').css('overflow', 'auto');
        }
        function automask() {
            //theForm.elements['__SCROLLPOSITIONY'].value = 0;
            $('body').css('overflow', 'hidden');
            $('.windowMask').height($(document).height());
            //$('.windowMask').width($(window).width());

            $('.windowMask').show();
            setTimeout(function(){window.scrollTo(0, 0);},1000);

        }
        function hideautomask() {
            $('body').css('overflow', 'auto');
            $('.windowMask').hide();
        }
        function hideMask() {
            $('.masklayer').hide();
            $('body').css('overflow', 'auto');
            makeFieldsDraggable();
            $(finalizePage);
        }
        function notificationandhideMask(msg) {
            $('.masklayer').hide();
            notification(msg);
            makeFieldsDraggable();
            $(finalizePage);
        }
        function ListBoxValid(sender, args) {
            var ctlDropDown = document.getElementById(sender.controltovalidate);
            args.IsValid = ctlDropDown.options.length > 0;
        }

        function Confirm(editedCount, prevCount) {
            //hideMask();
            $('#MainContent_MainContent_FinalResult').hide();
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("The previous Rule identified " + prevCount + " property(s) and the newly edited Rule identifies " + editedCount + " property(s). Press OK to proceed with saving your changes or Cancel to go back and review the Rule.")) {
                confirm_value.value = "Yes";
                $("#<%=HideLinkBt.ClientID%>").click();
                __doPostBack('updatesave', 'OnClick');
                $('#MainContent_MainContent_FinalResult').show();
                hideMask();
                return true;

            } else {
                confirm_value.value = "No";
                $('#MainContent_MainContent_FinalResult').show();
                hideMask();
            }
            document.forms[0].appendChild(confirm_value);

        }

        function alertMsg(msg) {
            if ((msg != null) && (msg != "")) {
                setTimeout(function(){alert(msg);}, 1000);
            }
            else {
                setTimeout(function(){alert("An error occurred while trying to execute the rule. Please review the Rule Conditions.");}, 1000);
            }
        }

        function showPopup(title) {
            $('.edit-panel').html('');
            $('.edit-popup').dialog({
                modal: true,
                width: 675,
                height: 'auto',
                minHeight: "auto",
                position: ['center', 70],
                resizable: false,
                title: title,
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        }


        function hidePopup() {
            $('.edit-popup').dialog('close');
        }

        function editProperties(response) {
            try {
                showPopup('Resulted parcels')

            } catch (e) {

            }
            return false;
        }

        var ddlText, ddlValue, ddl;

        function CacheItems() {

            ddlText = new Array();

            ddlValue = new Array();

            ddl = document.getElementById("<%=ddlCondition.ClientID %>");

            if (ddl) {
                for (var i = 0; i < ddl.options.length; i++) {

                    ddlText[ddlText.length] = ddl.options[i].text;

                    ddlValue[ddlValue.length] = ddl.options[i].value;

                }
            }

        }

        window.onload = CacheItems;

        function FilterItems() {
            ddl.options.length = 0;

            for (var i = 0; i < ddlText.length; i++) {
                if ((ddlValue[i] != "1") && (ddlValue[i] != "7") && (ddlValue[i] != "8") && (ddlValue[i] != "9")) {
                    ddl.options.remove(ddlValue[i]);

                }

            }

            if (ddl.options.length == 0) {
                AddItem("No items found.", "");
            }
        }

        function AddItem(text, value) {
            var opt = document.createElement("option");
            opt.text = text;
            opt.value = value;
            ddl.options.add(opt);
        }

        var reOrderList = [];
        function reOrderRows() {
            $('.reOrderCls').hide(); $('.scCls').show();
            reOrderList = [];
            $("#MainContent_MainContent_gvRuleList tbody").sortable({
                items: "tr:not(.ui-state-default)",
                cursor: "move",
                opacity: 0.6
            });
        }

        function saveReOrderRows() {
            var newOrder = [];
            showMask();
            $("#MainContent_MainContent_gvRuleList tbody tr:not(.ui-state-default)").each(function () {
                let tdel = $(this).find(".Idcls"); if (tdel[0]) newOrder.push($(tdel).val());
            });
            $$$priRules('autopriorityrules.aspx/updateExecutionOrder', { IdList: newOrder.toString() }, (res) => {
                console.log(res.d);
                refreshSortButtons();
                hideMask();
                window.location.href = window.location.href;
            }, (f) => {
                console.log(f);
                refreshSortButtons();
                hideMask();
                window.location.href = window.location.href;
            });
            return false;
        }

        function refreshSortButtons() {
            $("#MainContent_MainContent_gvRuleList tbody").sortable("destroy");
            $('.scCls').hide(); $('.reOrderCls').show();
        }

        function $$$priRules(path, d, scallback, fcallback) {
            $.ajax({
                type: "POST",
                url: path,
                data: JSON.stringify(d),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: (data) => {
                    if (scallback) scallback(data);
                },
                error: (e) => {
                    if (fcallback) fcallback(e);
                }
            });
        }

        function RemoveItem(text, value) {
            var opt = document.createElement("option");
            opt.text = text;
            opt.value = value;
            ddl.options.remove(opt);

        }
        function showCustMsg() {
            automask();
            $('#CustMessBox').show();
        }
        function closeCustmsg() {
            clrCstMsg();
            hideautomask();
            $('#CustMessBox').hide();
        }

        function closefinalmsg() {
            $('#MainContent_MainContent_FinalResult').hide();
            hideMask();
        }

        function clrCstMsg() {
            $('#MainContent_MainContent_txtAlert').val("");
        }
        function SubmitCstMsg() {
            var boxMsg = $('#MainContent_MainContent_txtAlert').val();
            $('#MainContent_MainContent_txtAlert1').val(boxMsg);
            closeCustmsg();
        }
        function updateFlag() {
            RuleId = document.getElementById("<%= hdnRuleId.ClientID %>").value;
            if (RuleId != "0") {
                $("#<%=hdnUpdateFlag.ClientID%>").val("1")
            }
            $(finalizePage);
        }

        function __doPostBack(eventTarget, eventArgument) {
            theForm.__EVENTTARGET.value = eventTarget;
            theForm.__EVENTARGUMENT.value = eventArgument;
            theForm.submit();
        }


        function notification(message, title, callback) {
            $('#webmessage').remove();
            if (title == null)
                title = "CAMA Cloud Success notification";
            var wm = '<div id="webmessage" style="display: none;">' + "<h4>" + title + "<span style='float:right; margin-right:10px;color:gray;margin-top:-10px;cursor:pointer' onclick='closenotification()'>x</span></h4><p>" + message + "</p></div>"
            $('.mask').after(wm);
            $('#webmessage').fadeIn(1000, "swing", function () {
                $('#webmessage').delay(4000).fadeOut(2000, "swing", callback);
            });
        }

       function isAlphaNumericKey(evt) {
            var charCode = evt.keyCode
            if ((charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122) || (charCode >= 48 && charCode <= 57) || (charCode == 45 && charCode.length!=1) || (charCode == 46) || (charCode == 32) || (charCode ==  44)) {
                return true;
            }            
            return false;
        }
        
        function closenotification() {
            $('#webmessage').remove();
        }
        
        function GetConfirmation()
 		{
     		 var reply = confirm("Ary you sure you want to delete this?");
     		 if(reply)
      			{
        		 return true;
      			}
      		 else
     			{
        		 return false;
     			}
  		}
        
        function confirmAlert() {
            var valuesString = $("#" + '<%= txtValuesString.ClientID%>').val();
            var valuesNumber = $("#" + '<%= txtValuesNumber.ClientID%>').val();
            var valuesDate = $("#" + '<%= txtValuesDate.ClientID%>').val();
            var valuesDate2 = $("#" + '<%= txtValuesDate2.ClientID%>').val()
            var ddlCondition = $("#" + '<%=  ddlCondition.ClientID%>').val();
            var ddlField = $("#" + '<%=  ddlField.ClientID%>').val();
            var title = $("#" + '<%= txtTitle.ClientID%>').val();
            var regex = new RegExp("^[a-zA-Z0-9,.-]+$");
            var pattern=/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/;
            var dtRegex = new RegExp(pattern);
            if (!title) {
                alert("Title is required, please enter the Title!");
                return false;
            }
            if (title.indexOf("'") != -1){
              	alert("Avoid single quotes in the Title");
                return false;
               }
            if ((ddlField == "") || ddlField == null) {
                alert("Field is required, please select a Field!");
                return false;
            }

            if (ddlCondition == "0") {
                alert("Condition  is required, please select a Condition!");
                return false;
            }
            if (ddlCondition != "") {
                if ((ddlCondition != "8") && (ddlCondition != "9")) {
                    if ((!valuesString) && (!valuesNumber) && (!valuesDate)) {
                        alert("Values  is required, please enter your Values!");
                        return false;

                    }
                    else {
                        if (!regex.test(valuesString)) {
                            alert("please enter valid Values!");
                            return false;
                        }
                    }
                   
                }
               
               
            }
          
           if($('#MainContent_MainContent_txtValuesDate').is(":visible")){
              if(!dtRegex.test(valuesDate) && ddlCondition != "8" && ddlCondition != "9" )
               {
                  alert("Please enter a valid date.");
                     return false;
                }
              }
           if($('#MainContent_MainContent_txtValuesDate2').is(":visible")){
                if(!valuesDate2){ alert("Values  is required, please enter your Values!"); return false;}
                if(!dtRegex.test(valuesDate2))
                 {
                   alert("Please enter a valid date.");
                     return false
                     }
                if(!(valuesDate2>valuesDate)){
                   alert("Please check the 'From' and 'To' dates in the Date Field.");
	               return false;
                 }
              
            }
            showMask();
            $(finalizePage);
        }

        function confirmAlertOnSave() {
            var title = $("#" + '<%= txtTitle.ClientID%>').val();
            if (!title) {
                alert("Title is required, please enter the Title!");
                return false;
            }
            if (title.indexOf("'") != -1){
              	alert("Avoid single quotes in the Title");
                return false;
               }
             var alertmsg = $('#MainContent_MainContent_txtAlert1').val();
            if ($('#MainContent_MainContent_ApplyDateRange').prop('checked')) {
                var date1 = $('#MainContent_MainContent_dtFrom').val();
                var date2 = $('#MainContent_MainContent_dtTo').val();

                // Regular expression to validate yyyy-mm-dd format
                var dateRegex = /^\d{4}-\d{2}-\d{2}$/;

                if (!date1 || !date2 || !dateRegex.test(date1) || !dateRegex.test(date2) || !(date2 > date1)) {
                    alert("Please check the 'Date From' and 'Date To' in the Date Range.");
                    return false;
                }
            }
            if (($('#MainContent_MainContent_ApplyAutomatic').is(':visible') == false) && ($('#MainContent_MainContent_ApplyDateRange').is(':visible') == false) && ($('#MainContent_MainContent_enableExceptions').is(':visible') == false) && ($('#MainContent_MainContent_ApplyOneTime').is(':visible') == false)) {
                alert("Please enable one of the Auto Priority Settings first before trying to verify the rule.")
                return false
            }
            if($('#MainContent_MainContent_enableExceptions').attr('checked') == "checked")
            {
              var daysCount=$('#MainContent_MainContent_txtReviewAgeingDays').val();
               if(isNaN(daysCount)){
                                alert("Please enter numeric values in the Days coloumn.");
                                return false;
                            }
                            if(daysCount.indexOf(".") != -1){
                                alert("Please enter numeric values in the Days coloumn.");
                                return false;
                            }
                       }         
            //if (($('#MainContent_MainContent_ApplyAutomatic').attr('checked') != "checked") && ($('#MainContent_MainContent_ApplyDateRange').attr('checked') != "checked") && ($('#MainContent_MainContent_enableExceptions').attr('checked') != "checked") && ($('#MainContent_MainContent_ApplyOneTime').attr('checked') != "checked")) {
            //    alert("Please select atleast one check box to proceed!");
            //    return false;
            //}

            showMask();
        }

        var sortOldIndex;

function makeFieldsDraggable() {
            var fName = '';
            var STname = '';
            $('.mastefield-list').sortable().disableSelection();
            $('.cat-fields').droppable({
                drop: function (event, ui) {
                    var src = event.srcElement;
                    if ($(src).hasClass('mfield')) {
                        var fieldId = $(src).attr('fieldid1');
                        var tableId = $(src).attr('tableid');
                        var target = event.target;
                        fName = $(src).attr('fieldName');
                        STname = $(src).attr('STname');
                        var postdata = {
                            FieldId: fieldId
                        };
                        $("#" + '<%= txtAlert.ClientID%>').val($("#" + '<%= txtAlert.ClientID%>').val() + '{' + STname + '.' + fName + '}')

                    }

                }
            });

            $('.cat-fields').sortable({
                start: function (event, ui) {
                    var o = ui.item[0];
                    sortOldIndex = $(o).index();
                },
                stop: function (event, ui) {
                    var o = ui.item[0];
                    var fieldId = $(o).attr('fieldid1')
                    var index = $(o).index();
                    var newIndex = (index + 1) * 10;
                    if (sortOldIndex > index) {
                        newIndex -= 5;
                    } else {
                        newIndex += 5;
                    }

                }
            });
        }
        
        function finalizePage() {
            $('.fields-list1').height(145);
            $('.cat-fields').height(169);
            makeFieldsDraggable();
        }


        function setPageDimensions(cheight) {
            var dtl = cheight - 208 - 40;
            $('.fields-list1').height(145);
            $('.cat-fields').height(169);
        }
        function dateRangeCheck() {
            if (document.getElementById('MainContent_MainContent_ApplyDateRange').checked) {
                document.getElementById("MainContent_MainContent_divDateRangeSub").style.display = "block";
            }
            else {
                document.getElementById("MainContent_MainContent_divDateRangeSub").style.display = "none";
            }
        }        
        function validateInput(inputElement) {
            var pattern = /^[a-zA-Z0-9.,\-]*$/; // Allow letters, numbers, dot, comma, and minus
            var inputValue = inputElement.value;
            var sanitizedValue = inputValue.replace(/[^a-zA-Z0-9.,\-]/g, ''); // Remove disallowed characters

            if (inputValue !== sanitizedValue) {
                inputElement.value = sanitizedValue;
            }
        }
    </script>
    <style>
        .pl20 {
            padding-left: 20px;
        }

        .chckbox {
            vertical-align: middle;
            width: 20px;
            height: 20px;
        }

        category {
            border: 1px solid #bfbdad;
        }

        h3 {
            margin: 0px;
        }

        #pmargin {
            margin: 2px;
        }

        .fields-list1 {
            height: 145px;
            border-right: 1px solid #CFCFCF;
            overflow-y: auto;
            overflow-x: hidden;
            overflow-style: panner;
            font-size: 0.9em;
        }

        .NoteSize {
            font-size: 12px;
        }

        #btn_customMsg {
            left;
        }

        .closeCstMsg {
            color: #000;
            font-size: 28px;
            font-weight: bold;
            float: right;
            top: -8px;
            position: absolute;
            right: 7px;
            cursor: pointer;
        }

            .closeCstMsg a {
                text-decoration: none;
                color: black;
                font-size: 20px;
                cursor: pointer;
            }

        .finalbuttons a {
            margin-right: 0px;
        }

        .mask {
            height: 116vh;
        }

        .btnsave a {
            padding: 5px 30px;
        }

        .deschov {
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
            position: absolute;
        }

            .deschov:hover {
                word-break: break-word;
                overflow: visible;
                white-space: normal;
                background-color:;
                position: relative;
            }
          #webmessage{
              background-color: #f9eeb5;
              }
          .row{
	    		width:450px;
	    		word-break: break-word;
    		 }
    		 .break {
    		   max-width:150px;
    		   word-break: break-all;
    		 }
		 .gvHeaderStyle{
			white-space:nowrap; 
			/*text-align: center !important;*/
			border-right: 1px solid !important;
			border-right-style: inset !important;
		   }
			.gvNonCountItemStyle{       
				text-align: left;
				white-space:nowrap;
				/*min-width: 130px;*/
			    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField runat="server" ID="hdnRuleId" Value="0" />
    <asp:HiddenField runat="server" ID="hdnUpdateFlag" Value="0" />
    <asp:LinkButton ID="HideLinkBt" runat="server" visibility="false" OnClick="HideLinkBt_Click"></asp:LinkButton>
    <h1 style="margin-bottom: 10px;">Auto Priority Rules</h1>
      <asp:Panel runat="server" ID="pnl1" Visible="True">
        <asp:label  runat="server" id = "norulemsg"></asp:label>
        <div id="topgrid">
               <div class="link-panel" style="float:right;"> 
                   <asp:LinkButton style="margin-right: 22px;" runat="server" ID="addnewrule" Text="Add New" OnClick="addnew_Click"  align="right" ValidationGroup="AutoVal" />
                   <asp:LinkButton style="margin-right: 22px;" runat="server" ID="reOrder" Text="Re-order" CssClass="reOrderCls" OnClick="reOrder_Click" align="right" />
                   <asp:LinkButton style="margin-right: 22px; display: none;" runat="server" ID="reOrderSave" CssClass="scCls" Text="Save" OnClientClick="return saveReOrderRows();" align="right" />
                   <asp:LinkButton style="margin-right: 22px; display: none;" runat="server" ID="reOrderCancel" CssClass="scCls" Text="Cancel" OnClick="recancel_Click" align="right" />
               </div>       
        <asp:GridView runat="server" ID="gvRuleList"  AllowPaging="True" AllowSorting="True"  AutoEventWireup="false" PageSize="10" OnPageIndexChanging="gvRuleList_PageIndexChanging" Width="98%">
                <Columns>
					<asp:TemplateField HeaderText = "Sl No." ItemStyle-Width="100">
				        <ItemTemplate>
				            <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
				        </ItemTemplate>
				    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemStyle Width="10px" />
                        <ItemTemplate>
                            <input type="hidden" id="HiddenField1" class="Idcls" value='<%# Eval("RuleId") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
					<asp:BoundField DataField="RuleId" HeaderText="RuleId" ItemStyle-Width="300px" Visible="false" />
					<asp:BoundField DataField="Title" HeaderText="Title" ItemStyle-Width="300px" />
					<asp:BoundField DataField="Priority" HeaderText="Priority"  ItemStyle-Width="150px" />
					<asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate"  ItemStyle-Width="200px" />
					<asp:TemplateField>
		                <ItemStyle Width="100px" />
		                <ItemTemplate>
							<asp:LinkButton runat="server" Text='Show Parcel' CommandName='Parcel' CommandArgument='<%# Eval("RuleId") %>' tooltip="Show affected parcel based on the rule " />
						</ItemTemplate>
		            	</asp:TemplateField> 
                    <asp:TemplateField>
		                <ItemStyle Width="60px" />
		                <ItemTemplate>
							<asp:LinkButton runat="server" Text='<%# IIf(Eval("ShowApply"), "Apply", "")%>' CommandName='Apply' CommandArgument='<%# Eval("RuleId") %>' OnClientClick="return showMask();" tooltip="Apply will update the priority on all properties in the cloud matching the business rule's criteria." />
						</ItemTemplate>
		            	</asp:TemplateField>
					<asp:TemplateField ItemStyle-Width="50px">
					<ItemTemplate>
						<asp:LinkButton ID="lbnEdit" runat="server" Text="Edit" CommandName='Edit1'  CommandArgument='<%# Eval("RuleId") %>'/>
					</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField ItemStyle-Width="70px">
						<ItemTemplate>
							<asp:LinkButton runat="server" Text="Delete" CommandName='Delete1' CommandArgument='<%# Eval("RuleId") %>' OnClientClick="return GetConfirmation();" />
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
			</asp:GridView>

        </div>

        <div id="parcelgrid" runat="server" visibility="false" style="position: absolute; top: 13%; left: 15%; background-color: ghostwhite;
        z-index: 99999;overflow: auto; padding: 20px;  height: 58%;  width: 69%;  border: 1px solid;  border-radius: 5px;">
            <h3 style="margin-left: 40%;">Priority Applied Parcel List</h3>
             <asp:HiddenField runat="server" ID="hiddenrule" Value="0" />
            <span class="closeCstMsg" onclick="" title="Close">
            
            <asp:LinkButton ID="LinkButton4" CssClass="lablebold" runat="server" OnClick="closegirdmsg" AutoPostBack="true">x</asp:LinkButton>
            
           </span>
            Last <asp:DropDownList ID="ddlRows" runat="server" width="150px" ClientIDMode="Static"  AutoPostBack="true" SelectedIndexChanged="showAffectedParcels" >
                   <asp:ListItem Text="10" Value="1" />
				   <asp:ListItem Text="100" Value="2" />
				   <asp:ListItem Text="1000" Value="3" />
				    <asp:ListItem Text="5000" Value="4" />
                   </asp:DropDownList> Rows
            <asp:LinkButton style="padding-left: 15px;" runat="server" ID="btnExport"  Text="Export All" OnClick="export_All"  align="right"/>
            <asp:UpdatePanel ID="UpparcelList" runat="server">
    		<ContentTemplate>        
            <asp:GridView runat="server" ID="gvAuditTrail" AllowPaging ="True" PageSize="5"  Width="100%" OnPageIndexChanging="gvAuditTrail_PageIndexChanging">
				<Columns>
				<asp:TemplateField HeaderText = "Sl No." ItemStyle-Width="45px">
				        <ItemTemplate>
				            <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
				        </ItemTemplate>
				    </asp:TemplateField>
					<asp:BoundField DataField="ParcelId" HeaderText="Parcel Id" ItemStyle-CssClass = "break" ItemStyle-Width="180px" />
					<asp:BoundField DataField="KeyField" HeaderText="" ItemStyle-CssClass = "break" ItemStyle-Width="180px"/>
					<asp:BoundField DataField="EventTime" HeaderText="Time"  ItemStyle-Width="180px" />
					<asp:BoundField DataField="OldPriority" HeaderText="Old Priority"  ItemStyle-Width="100px" />
					<asp:BoundField DataField="Description" HeaderText="Description"  ItemStyle-Width="350px"/>
					
				</Columns>
			</asp:GridView>
			</ContentTemplate>
            </asp:UpdatePanel> 
        </div>
        </asp:Panel>
    <asp:Panel runat="server" ID="editpanel" Visible="False">                   

                    <div class="link-panel" style="float: right;  display: block;">
                  <asp:LinkButton style="font-weight: 600;" runat="server" ID="LinkButton5" Text="Return To List" OnClick="btnReturntolist_Click" align="right" ValidationGroup="AutoVal" />

                    </div>
                <asp:UpdatePanel ID="uplMain" runat="server">
    			<ContentTemplate>
                <table style="margin-bottom:5px;">
                    <tr>
                        <th width="100px"></th>
                        <th width="200px"></th>
                        <th width="100px"></th>
                        <th width="200px"></th>
                    </tr>
                    <h3>Edit - Rule Name</h3>
                    <tr>
                        <td style="padding-bottom: 7px;">Rule Name:</td>
                        <td>
                            <asp:TextBox ID="txtTitle" runat="server" onkeypress="return isAlphaNumericKey(event)" Width="300px" MaxLength="50"></asp:TextBox>
                        </td>
                        <td>
                            <asp:CheckBox ID="CheckEnable" class="chckbox" runat="server" Text="Enable"/>  
                        </td>
                    </tr>
                    <asp:UpdatePanel ID="uplRules" runat="server">
                    <ContentTemplate>      
                    <tr >
                        <td>Field Category: </td>
                        <td>
                            <asp:DropDownList ID="ddlFieldCategory" runat="server" Width="200px" ClientIDMode="Static" AutoPostBack="true" onchange="finalizePage();showMask();" SelectedIndexChanged="ddlFieldCategory_SelectedIndexChanged"></asp:DropDownList>                            
                        
                        </td>
                    </tr>
                </table>
                <table  style="width: 80.0%; border: 1px solid #BEBEBE; background-color: #F5F1F1;height: 80px;
    padding: 0px 0px 7px 9px;">
                    <h3 style="margin-bottom:5px;">Rule Condition</h3>
                     <tr>
                        <th width="200px"></th>
                        <th width="150px"></th>
                        <th width="600px"></th>
                        <th width="91px"></th>
                        <th width="200px"></th>
                    </tr>
                     <tr>
                        <td>Field
                        </td>
                        <td>Condition
                        </td>
                        <td>Values
                        </td>
                        <td>Operator
                        </td>
                        <td>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">
                            <asp:DropDownList ID="ddlField" runat="server" Width="190px" AutoPostBack="true"  onchange="showMask();" SelectedIndexChanged="ddlField_SelectedIndexChanged"></asp:DropDownList>
                        </td>
                        <td style="vertical-align: top;">
                            <asp:DropDownList ID="ddlCondition" runat="server" Width="140px" AutoPostBack="true" onchange="showMask();" SelectedIndexChanged="ddlCondition_SelectedIndexChanged">
                                <asp:ListItem Text="-- All --" Value="0" />
                                <asp:ListItem Text="Equal to" Value="1" />
                                <asp:ListItem Text="Not Equal to" Value="2" />
                                <asp:ListItem Text="Greater than" Value="3" />
                                <asp:ListItem Text="Less than" Value="4" />
                                <asp:ListItem Text="Greater than or Equal to" Value="5" />
                                <asp:ListItem Text="Less than or equal to" Value="6" />
                                <asp:ListItem Text="IN" Value="7" />
                                <asp:ListItem Text="Is NULL" Value="8" />
                                <asp:ListItem Text="Is Not NULL" Value="9" />
                                <asp:ListItem Text="BETWEEN" Value="10" />
                            </asp:DropDownList>
                        </td>
                        <td style="vertical-align: top;">
                            <asp:TextBox ID="txtValuesString" runat="server" Width="96%" TextMode="MultiLine" oninput="validateInput(this)" onkeypress="return isAlphaNumericKey(event)" Rows="2" ToolTip="Enter the values only,please avoid single quotes." MaxLength="100"></asp:TextBox>
                            <asp:TextBox ID="txtValuesNumber" runat="server" Width="96%" TextMode="Number" visibility="false"></asp:TextBox>
                            <asp:TextBox ID="txtValuesDate" runat="server" Width="96%" TextMode="Date" visibility="false" ToolTip="Enter the values only,please avoid single quotes."></asp:TextBox>
                            <asp:TextBox ID="txtValuesNumber2" runat="server" Width="96%" TextMode="Number" visibility="false"></asp:TextBox>
                            <asp:TextBox ID="txtValuesDate2" runat="server" Width="96%" TextMode="Date" visibility="false" ToolTip="Enter the values only,please avoid single quotes."></asp:TextBox>
                        </td>
                        <td style="vertical-align: top;">
                             <asp:DropDownList ID="DdlOperator" runat="server" Width="80px" AutoPostBack="true"  >
                                <asp:ListItem Text="-- Select --" Value="0" />
                                <asp:ListItem Text="AND" Value="1" />
                                <asp:ListItem Text="OR" Value="2" />
                            </asp:DropDownList>
                        </td>
                        <td style="vertical-align: top;">
                            <div class="link-panel">
                                <asp:LinkButton runat="server" Text="Add" ID="lblAdd" OnClick="LinkAdd_Click" OnClientClick="return confirmAlert();" Style="margin: 0px 0px 0px 0px; width: 49px; padding: 4px 14px;text-align: center;" />
                            </div>
                        </td>
                    </tr>
                </table>
                 <div class="WrapText">
                    <asp:Panel runat="server" ID="pnlExcel" Visible="true">
                        <asp:GridView runat="server" ID="gvCondition" AllowPaging="True" PageSize="5" Width="80%"  OnPageIndexChanging="gvCondition_PageIndexChanging">
                            <Columns>
                                <asp:BoundField DataField="FieldName" HeaderText="Field" ItemStyle-Width="300px" />
                                <asp:BoundField DataField="Condition" HeaderText="Condition" ItemStyle-Width="200px" />
                                <asp:BoundField DataField="ValueString" HeaderText="Values" ItemStyle-CssClass="row" />
                                <asp:BoundField DataField="Operatorstr" HeaderText="Operator" ItemStyle-Width="118px" />
                                <asp:TemplateField ItemStyle-Width="50px">
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" OnClientClick="showMask();" Text="Edit" CommandName='EditRule' CommandArgument='<%# Eval("Rowno") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-Width="80px">
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" OnClientClick="updateFlag();" Text="Remove" CommandName='remove' CommandArgument='<%# Eval("Rowno") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </asp:Panel>
                </div>
                <table>
                </ContentTemplate>
                </asp:UpdatePanel>
                    <h3>Rule Output</h3>
                     <tr>
                        <th width="95px"></th>
                        <th width="315px"></th>
                         <th></th>
                    </tr>
                    <tr>
                        <td  style="padding-bottom: 8px; ">Priority: </td>
                        <td>
                            <asp:DropDownList ID="ddlPriority" runat="server" Width="150px" ClientIDMode="Static" AutoPostBack="False" onchange="updateFlag();">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvddlPriority" runat="server" ForeColor="Red" Display="Dynamic"
                                ErrorMessage="*" ControlToValidate="ddlPriority" ValidationGroup="AutoVal"></asp:RequiredFieldValidator>
                        </td>
                        <td style="vertical-align: bottom;">
                            <p style="margin: 0px" >
                              <asp:LinkButton runat="server" Text="Custom Message" OnClientClick ="showCustMsg(); return false;" ID="btn_customMsg"  Style="margin: 0px;float: right" />
                         </p>
                        </td>
                       
                    </tr>
                    <tr>
                        <td>Alert: </td>
                        <td colspan="2" class="NoteSize" >
                            <asp:TextBox class="" ID="txtAlert1" runat="server" onkeypress="return isAlphaNumericKey(event)" TextMode="MultiLine" Rows="4" Width="100%"  
                            MaxLength="500" Height="50px"></asp:TextBox><br />
                             
                        </td>
                    </tr>
                </table>
                
                <table>
                   
                    <h3 style="margin-bottom:3px;">Conditions</h3>
                    <tr>
                        <td>
                            <div id="divDateRange" runat="server" visibility="true">
                                <p style="    margin: 4px 0px;">
                                    <!--OnCheckedChanged="chkDaterange_CheckedChanged"-->
                                    <asp:CheckBox runat="server" ID="ApplyDateRange" AutoPostBack="True" onchange="showMask();" OnCheckedChanged="chkDaterange_CheckedChanged" />
                                    <span style="font-weight: 900">Date Range </span>– configure a date range for the rule to be applicable.
                                </p>
                            </div>
                        </td>
                        <td>
                            <div id="divDateRangeSub" runat="server" visibility="false" >
                                <table>
                                    <tr>
                                        <td>&nbsp;Date From:
	                        <asp:TextBox runat="server" ID="dtFrom" Width="130px" TextMode="Date" Enabled="true" onclick="updateFlag();" />
                                            <asp:RequiredFieldValidator runat="server" ID="rfvDtF" ControlToValidate="dtFrom" ErrorMessage="*" Enabled="false" />&nbsp;and&nbsp;&nbsp;
                                        </td>
                                        <td>Date To: 
	                    <asp:TextBox runat="server" ID="dtTo" Width="130px" TextMode="Date" Enabled="true" onclick="updateFlag();" />
                                            <asp:RequiredFieldValidator runat="server" ID="rfvDtT" ControlToValidate="dtTo" ErrorMessage="*" Enabled="false" />
                                        </td>
                                    </tr>
                             </table>
                            </div> 

                        </td>
                    </tr>
                    <tr>
                    <div id="divAutomatic" runat="server" visibility="false">
                    <p style="margin:0px; padding-left: 3px; margin-top: 5px;">
                        <asp:CheckBox runat="server" ID="ApplyAutomatic" AutoPostBack="True" onclick="updateFlag();" />
                        <span style="font-weight: 900">Automatic </span>– when a rule is set to Automatic, the rules will be applied during the next sync(upsync).
                    </p>
                </div>
                    </tr> 

                </table>
                    <div>
                            <p>
                              <span style="font-weight: bold;">Note : If none of the above options are checked, apply the rule manually by using the 'Apply' button in the rules log.</span>
                              </p>
                    </div>
                              
        <div>
            <h3 >Exceptions</h3>
            <div id="divExceptions" runat="server" visibility="True">
                <p style="margin-top:5px;padding-left: 3px;margin-right: 25px;text-align: justify;">
                    <asp:CheckBox runat="server" ID="enableExceptions" AutoPostBack="True" onchange="showMask();" OnCheckedChanged="chkException_CheckedChanged" onclick="updateFlag();" />
                    <span>By default the Auto Priority Rules will not apply to parcels that have already been marked as complete. Checking this box will override that setting, meaning that if a property has already been visited (is currently marked as complete) and a condition is met for an Auto Priority Rule, the mark as complete information will be removed and the parcel will be sent back to the appraiser's iPad.</span>
                </p>
            </div>
            <div id="divExceptionsSub" runat="server" visibility="false">
                <p>
                    &nbsp; Do not include parcels reviewed within the last
                        <asp:TextBox ID="txtReviewAgeingDays" runat="server" onkeypress="return isAlphaNumericKey(event)"  Width="40px" MaxLength="3"></asp:TextBox>
                    days.
                </p>
            </div>
        </div>
        </ContentTemplate>
        </asp:UpdatePanel>
        <table class="finalbuttons">        
            <tr>
                <th width="65px"></th>
                <th width="122px"></th>
                <th width="200px"></th>
            </tr>
            <tr>
                <td>
                    <div class="link-panel">
                        <table>
                            <tr>
                                <td class="btnsave" >
                                    <asp:LinkButton style="font-weight: 800;" runat="server" ID="lblVerify" Text="Save" OnClick="btnVerify_Click"  OnClientClick="return confirmAlertOnSave();" align="right" ValidationGroup="AutoVal" ToolTip="Saves the rule and makes it active in CAMA CLoud." />
                                </td>
                            </tr>
                        </table> </div>
                </td>
                <td>
                    <div class="link-panel">
                        <table>
                            <tr>
                                <td style="padding-left: 2px;">
                                    <asp:LinkButton style="font-weight: 600;" runat="server" ID="returnToList" Text="Return To List" OnClick="btnReturntolist_Click" align="right" ValidationGroup="AutoVal" />
                                </td>
                            </tr>
                        </table> 

                    </div>
                </td>
                <td>
                    <div class="link-panel">
                        <table>
                            <tr>
                                <td>
                                    <asp:LinkButton style="font-weight: 600;" runat="server" ID="lbnrevoke" Text="Clear Priorities" OnClick="lbnRevoke_Click" OnClientClick="return confirmAlertOnSave();" align="right" ValidationGroup="AutoVal" ToolTip="Revoking Priorities will clear the Priority of all properties matching this criteria and set back to Normal Priority." />
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
         </table>
    </asp:Panel>
       <div id="CustMessBox" style="position: absolute;display: none;top: 30%; left: 30%;background-color: #f5f1f1; z-index: 99999;    padding: 20px;border:1px solid; border-radius:11px;"">
            <span class="closeCstMsg"  onclick="closeCustmsg(); return false ;" title="Close">&times;</span>
           <table>
               <tr>
                   <td class="NoteSize">
                       <asp:TextBox class="cat-fields" ID="txtAlert" runat="server" onkeypress="return isAlphaNumericKey(event)" TextMode="MultiLine" Rows="4" Width="300px"
                           MaxLength="500"></asp:TextBox><br />
                       <b>NOTE:</b> Only top one data will be displayed if multiple records exists.
                   </td>
                   <td class="td-field-master" rowspan="4">
                       <h3>Data Fields</h3>
                       <p id="pmargin">Drag and drop fields to the space on the Left side with label Alert.</p>
                       <asp:UpdatePanel runat="server" class="fields-list1 drop-box flist" ID="FieldList" ClientIDMode="Static">
                           <ContentTemplate>
                               <asp:Repeater runat="server" ID="rpFieldslist">
                                   <ItemTemplate>
                                       <div class="mastefield-list">
                                           <span fieldid1='<%# Eval("Id") %>' class='mfield' catid='<%# Eval("CategoryId") %>' fieldname='<%# Eval("Name") %>' stname='<%# Eval("SourceTable") %>' tableid='<%# Eval("FieldTableId") %>'>
                                               <%# Eval("Name")%>
                                           </span>
                                       </div>
                                   </ItemTemplate>
                               </asp:Repeater>
                           </ContentTemplate>
                       </asp:UpdatePanel>
                   </td>
               </tr>
           </table>
           <div>
               <div class="link-panel" style="float: right; margin-top: 5px">
                   <asp:LinkButton runat="server" ID="LinkButton2" OnClientClick="SubmitCstMsg(); return false;" Text="Add" align="right" ValidationGroup="AutoVal" />
                   <asp:LinkButton runat="server" ID="LinkButton3" Text="Clear" OnClientClick="clrCstMsg(); return false;" ValidationGroup="AutoVal" />
               </div>
           </div>
       </div>        
    <div id="FinalResult" runat="server" visibility="false" style="position:absolute; top: 30%; left: 32%; background-color: #f5f1f1; z-index: 99999; padding: 20px;    height: auto;
    width: 37%;border: 1px solid;
    border-radius: 11px;">
        <span class="closeCstMsg" onclick="" title="Close">
            
            <asp:LinkButton ID="LinkButton1" CssClass="lablebold" runat="server" OnClick="closefinalmsg" AutoPostBack="true">x</asp:LinkButton>
            
           </span>
        <div id="divRuleResult" runat="server" visibility="false" class="link-panel">
            <p>
                Count of resulted parcels :
                        <asp:Label ID="lblCount" Text="" runat="server"></asp:Label>
                &nbsp;<asp:LinkButton ID="lbDowload" CssClass="lablebold" runat="server" OnClick="ExportCSV" AutoPostBack="true">Click here to download</asp:LinkButton>
            </p>
        </div>
        <div id="divMpsResult" runat="server" visibility="false" class="link-panel">
            <p>
                Count of Manual Priority Locked parcels :
                        <asp:Label ID="lblMpsCount" Text="" runat="server"></asp:Label>
                &nbsp;<asp:LinkButton ID="lbMpsDowload" CssClass="lablebold" runat="server" OnClick="ExportMPSCSV" AutoPostBack="true">Click here to download</asp:LinkButton>
                &nbsp;<asp:LinkButton ID="lbMpsReset" CssClass="lablebold" runat="server" OnClick="UnlockMPS" AutoPostBack="true">Unlock MPS Parcels</asp:LinkButton>
            </p>
        </div>  
      <asp:Button ID="lbClose" CssClass="lablebold" runat="server" OnClick="closefinalmsg" AutoPostBack="true"  Text="OK" style="float:right;border-radius:10px;font-weight:bold;"  />           
    </div>
    <div class="windowMask" style="display: none;opacity:0.3; z-index: 9999; background: black; height: 100%; position:absolute;width:100%;left:0px;top:0px;">
    </div>
</asp:Content>


