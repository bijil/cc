﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/ParcelManager.master"
	AutoEventWireup="false" Inherits="CAMACloud.Console.parcels_assignments" Codebehind="assignments.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
	<style type="text/css">
		.nbhd
		{
			padding: 3px 10px;
			border-radius: 5px;
			margin-right: 5px;
		}
		
		.pending-nbhd
		{
			background: #d9d9fc;
			border: 1px solid #7777fc;
			cursor:move;
		}
		
		.completed-nbhd
		{
			background: #fcd9d9;
			border: 1px solid #fc7777;
		}
	</style>
	<script type="text/javascript">
		$(function () {
			$('.sortable').sortable({
				start: function (event, ui) {
					var o = ui.item[0];
					sortOldIndex = $(o).index();
				},
				stop: function (event, ui) {
					var o = ui.item[0];
					var nbhdid = $(o).attr('nbhdid')
					var index = $(o).index();
					var newIndex = (index + 1) * 10;

					if (sortOldIndex > index) {
						newIndex -= 5;
					} else {
						newIndex += 5;
					}

//					$ds('movecategory', {
//						NewIndex: newIndex,
//						NbhdId: nbhdid
//					}, function (data) {
//						if (data.status == "OK") {

//						}
//					});

				}
			});

		});
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<h1>
		Assigned Neighborhoods</h1>
	<p>
		Select User:
		<asp:DropDownList runat="server" ID="ddlUsers" Width="200px" AutoPostBack="true" />
	</p>
	<asp:Panel runat="server" ID="pnlUser" Visible="false" Width="800px">
		<h3>
			Pending Neighborhoods</h3>
		<p class="sortable">
			<asp:Repeater runat="server" ID="rpPending">
				<ItemTemplate>
					<span class="nbhd pending-nbhd" nbhdid='<%# Eval("Id") %>'>
						<%# Eval("Number")%></span>
				</ItemTemplate>
			</asp:Repeater>
			<asp:Label runat="server" ID="lblNoPending" Text="None" />
		</p>
		<h3>
			Completed Neighborhoods</h3>
		<p>
			<asp:Repeater runat="server" ID="rpCompleted">
				<ItemTemplate>
					<span class="nbhd completed-nbhd">
						<%# Eval("Number")%></span>
				</ItemTemplate>
			</asp:Repeater>
			<asp:Label runat="server" ID="lblNoCompleted" Text="None" />
		</p>
	</asp:Panel>
</asp:Content>
