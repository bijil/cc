﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/ParcelManager.master"
    AutoEventWireup="false" Inherits="CAMACloud.Console.protected_parcels_userAlerts" Codebehind="useralerts.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

<script type="text/javascript" >
    function ListBoxValid(sender, args) {
        var ctlDropDown = document.getElementById(sender.controltovalidate);
        args.IsValid = ctlDropDown.options.length > 0;
    }
</script>
    <h1>
        User Alerts</h1>
    <div>
    <asp:UpdatePanel ID="uplMain" runat="server"><ContentTemplate>
        <table>
            <tr>
                <td>
                    Subject
                </td>
                <td>
                    <asp:TextBox ID="txtSubject" runat="server" Width="400px" MaxLength="100"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvTxtSubject" runat="server" ForeColor="Red" Display="Dynamic"
                        ErrorMessage="*" ControlToValidate="txtSubject" ValidationGroup="UserProp"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Message
                </td>
                <td>
                    <asp:TextBox ID="txtMessage" style="max-width: 865px;" runat="server" TextMode="MultiLine" Rows="6" Width="400px"
                        MaxLength="500"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvTxtMessage" runat="server" ForeColor="Red" Display="Dynamic"
                        ErrorMessage="*" ControlToValidate="txtMessage" ValidationGroup="UserProp"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr><td colspan="2">Select User</td></tr>
            <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                <asp:ListBox ID="lbUserLeft" runat="server" Rows="10" Width="200px" DataTextField="Name" DataValueField="LoginId"></asp:ListBox>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                        <asp:Button ID="btnRight" runat="server" Text=">  " ToolTip="Move selected to Right" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                         <asp:Button ID="btnLeft" runat="server" Text="<  " ToolTip="Move selected to Left" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                         <asp:Button ID="btnRightRight" runat="server" Text=">>" ToolTip="Move All to Right" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                          <asp:Button ID="btnLeftLeft" runat="server" Text="<<" ToolTip="Move All to Left" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <asp:ListBox ID="lbUserRight" runat="server" Rows="10" Width="200px" ClientIDMode="Static"></asp:ListBox>
                                <asp:CustomValidator id="cvLbUserRight" runat="server"  ValidationGroup="UserProp" Display="Dynamic" ValidateEmptyText="True" style="font-weight: bold;" ControlToValidate="lbUserRight" ForeColor="Red"  ErrorMessage="*"  ClientValidationFunction="ListBoxValid"></asp:CustomValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:Button ID="btnSend" runat="server" Text="Send" ValidationGroup="UserProp" style="margin-left: 50px" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" style="margin-top:10px;" />
                </td>
            </tr>
        </table>
        </ContentTemplate>
        </asp:UpdatePanel> 
    </div>
</asp:Content>
