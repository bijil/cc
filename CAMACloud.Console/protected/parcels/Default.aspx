﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/ParcelManager.master"
    AutoEventWireup="false" Inherits="CAMACloud.Console.parcels_Default" CodeBehind="Default.aspx.vb" %>

<%@ Import Namespace="CAMACloud" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .data-progress
        {
            width: 95%;
            height: 18px !important;
            position:relative;
        }
        
        .data-progress span
        {
            position: absolute;
            left: 46%;
            top: 1px;
            font-weight: bold;
            text-shadow: 1px 1px 0 #fff;
        }
        
        .th-center
        {
            text-align: center !important;
        }

        .gnNbhdsFooter {
            background-color: #e9f5cf !important;
        }

        .gnNbhdsFooter td {
            color: #128ee3 !important;
        }
        
    </style>
    <script src="/App_Static/js/parcelmanager.js"></script>
      <script type="text/javascript">
       
        $(function () {
            loadPercentBar();
            $('.currAssignGroup').change(function () {
                var value = $(this).val().toString().split('||');
                console.log(value);
                $('#' + value[1]).attr('prgv', value[0]);
                loadPercentBar();
            })

        });
        function loadPercentBar() {
            $('.data-progress').each(function () {
                var prg = this;
                var pgv = $(prg).attr('prgv');
                $(prg).progressbar({
                    value: parseInt(pgv)
                });
                $('span', prg).text(pgv + '%');

            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <h1>Field Appraisals - Dashboard</h1>
    
        <table style="border-spacing: 0px; width: 100%;">
        <tr>
            <td>
                <h3 class="tip" style="width: 200px" abbr="Review the information below, dynamically updated<br/>with your fields agent's progress when they're online.">  Current Appraisals Status
                </h3>
                
                <asp:GridView runat="server" ID="gvNbhds" DataKeyNames="LoginId" AllowPaging="true" PageSize="10" ShowFooter="True">
                    <Columns>
                        <asp:TemplateField HeaderText="Appraiser">
                            <ItemStyle Width="130px" />
                            <ItemTemplate>
                                <%# Eval("FullName")%></ItemTemplate>
                        </asp:TemplateField>

                        <asp:BoundField HeaderText="Current Assgn Grp" AccessibleHeaderText="The last Assignment Group that the field<br/>agent downloaded to their field device."  DataField="CurrentNbhd"
                            HeaderStyle-CssClass="th-center tip" ItemStyle-HorizontalAlign="Center"  Visible="False" ItemStyle-Wrap="false">                        
<HeaderStyle CssClass="th-center tip"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Current Assgn Grp" Visible="False" >
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlCurrentAssgnGrp" AccessibleHeaderText="The last Assignment Group that the field<br/>agent downloaded to their field device." Width="120px" runat="server" CssClass="currAssignGroup"></asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:BoundField HeaderText="In List" DataField="AllNbhds" ItemStyle-Width="90px"
                            HeaderStyle-CssClass="th-center tip" AccessibleHeaderText="Total number of Assignment Groups assigned to the<br/>field agent that contain priorities" ItemStyle-HorizontalAlign ="Center" >
                            <HeaderStyle CssClass="th-center tip"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="90px"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField HeaderText="All Tasks" DataField="AllTasks" ItemStyle-Width="70px"
                            HeaderStyle-CssClass="th-center tip" AccessibleHeaderText="Total list of Priority properties + Proximity properties that<br/>the appraiser marked as complete in <u><b>all</b></u> Assignment Groups<br/>assigned to the appraiser." ItemStyle-HorizontalAlign="Center" >
<HeaderStyle CssClass="th-center tip"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="70px"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Pending" DataField="PendingTasks" ItemStyle-Width="70px"
                            HeaderStyle-CssClass="th-center tip" AccessibleHeaderText="All priority properties in Assignment Groups assigned<br/>to the appraiser that have not been marked as complete." ItemStyle-HorizontalAlign="Center" >
<HeaderStyle CssClass="th-center tip"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="70px"></ItemStyle>
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Current List Progress" AccessibleHeaderText="The percent complete of Priority properties + Proximity properties<br/>that have been marked as complete in the current Assignment Group<br/>the appraiser is working.">
                            <HeaderStyle CssClass="c tip"/>
                            <ItemStyle Width="150px" />
                            <ItemTemplate>
                                <div class="data-progress" id='<%# Eval("LoginId")%>' prgv='<%# Eval("CurrentPercentDone", "{0:0.#}") %>'>
                                    <span>0%</span>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="All Tasks Progress" AccessibleHeaderText="The percent complete of <u><b>all</b></u> Priority properties + <br/>Proximity properties that have been marked as<br/>complete in <u><b>all</b></u> Assignment Groups assigned<br/>to the appraiser.">
                            <HeaderStyle CssClass="c tip" />
                            <ItemStyle Width="150px" />
                            <ItemTemplate>
                                <div class="data-progress" prgv='<%# Eval("OverallPercentDone", "{0:0.#}") %>'>
                                    <span>0%</span>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField HeaderText="List Progress %" DataFormatString="{0:0.#}" DataField="OverallPercentDone"
                            ItemStyle-Width="70px" HeaderStyle-CssClass="th-center" ItemStyle-HorizontalAlign="Center" />--%>
                        
<%--                        <asp:BoundField HeaderText="Current Progress %" DataFormatString="{0:0.#}" DataField="CurrentPercentDone"
                            ItemStyle-Width="70px" HeaderStyle-CssClass="th-center" ItemStyle-HorizontalAlign="Center" />--%>
                    </Columns>
                    <FooterStyle BackColor="#D2DBAC" Font-Bold="True" ForeColor="#1889D7" HorizontalAlign="Center" CssClass="gnNbhdsFooter" />
                </asp:GridView>
                <p style="display: none;">
                    <asp:Button runat="server" ID="btnRefresh" Text=" Refresh " />
                </p>
                <table style="width: 100%; border-spacing: 0px;">
                    <tr>
                        <td style="vertical-align: top; width: 50%;">
                            <h3 class="tip" abbr="This will list any Assignment Groups that were accessed today <br/>(The appraiser logged in to the field device and selected the Assignment Group for download)">
                               <%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName() %>s accessed today:</h3>
                            <asp:Repeater runat="server" ID="rpToday">
                                <ItemTemplate>
                                    <span style="white-space: nowrap;"><strong>
                                        <%# Eval("Number")%></strong> (<%# Eval("AssignedTo")%>)</span>
                                </ItemTemplate>
                                <SeparatorTemplate>
                                    ,&nbsp;
                                </SeparatorTemplate>
                            </asp:Repeater>
                            <asp:Label runat="server" ID="lblNoneToday" Text="None" />
                            <h3  class="tip" abbr="This will list any Assignment Groups that were accessed within the last 7 days<br/> (The appraiser logged in to the field device and selected the Assignment Group for download)">
                                Groups with recent activity:</h3>
                            <asp:Repeater runat="server" ID="rpLast">
                                <ItemTemplate>
                                    <span style="white-space: nowrap;"><strong>
                                        <%# Eval("Number")%></strong> (<%# Eval("AssignedTo")%>)</span>
                                </ItemTemplate>
                                <SeparatorTemplate>
                                    ,&nbsp;
                                </SeparatorTemplate>
                            </asp:Repeater>
                            <asp:Label runat="server" ID="lblNoneLast" Text="None" />
                        </td>
                        <td style="vertical-align: top; width: 50%;">
                            <h3 class="tip" abbr="This will show the total number of properties marked as complete<br/>for all appraisers in the last 7 days.">
                                Last 7 Days Productivity</h3>
                            <asp:GridView runat="server" ID="gdP" ShowHeader="true" SkinID="PlainList">
                                <HeaderStyle HorizontalAlign="Left" />
                                <Columns>
                                    <asp:BoundField DataField="ReviewDate" HeaderText="Date" ItemStyle-Width="100px"
                                        DataFormatString="{0:MM/dd/yyyy}" />
                                    <asp:BoundField DataField="TotalParcels" HeaderText="No. of Parcels" ItemStyle-Width="100px" />
                                </Columns>
                                <EmptyDataTemplate>
                                    No field activity during the last 7 days.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
