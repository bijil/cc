﻿
Partial Class protected_parcels_userAlerts
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            _bindUsers()
        End If
    End Sub

    Private Sub _bindUsers()
        Dim dtUser As DataTable
        dtUser = Database.Tenant.GetDataTable("SELECT LoginId,FirstName+' '+coalesce(MiddleName,'')+' '+coalesce(LastName,'') as Name FROM UserSettings order by FirstName")
        'dtUser = Database.Tenant.GetDataTable("SELECT LoginId,FirstName as Name FROM UserSettings order by FirstName")
        lbUserLeft.DataSource = dtUser
        lbUserLeft.DataBind()
    End Sub

    Protected Sub btnLeft_Click(sender As Object, e As System.EventArgs) Handles btnLeft.Click
        Dim Selected As Boolean = False
        For Each item As ListItem In lbUserRight.Items
            If item.Selected Then
                Dim newItem As New ListItem(item.Text, item.Value)
                lbUserLeft.Items.Add(newItem)
                Selected = True
            End If
        Next
        If Selected = True Then
            lbUserRight.Items.RemoveAt(lbUserRight.SelectedIndex)
        End If
    End Sub
    Protected Sub btnRight_Click(sender As Object, e As System.EventArgs) Handles btnRight.Click
        Dim Selected As Boolean = False
        For Each item As ListItem In lbUserLeft.Items
            If item.Selected Then
                Dim newItem As New ListItem(item.Text, item.Value)
                lbUserRight.Items.Add(newItem)
                Selected = True
            End If
        Next
        If Selected = True Then
            lbUserLeft.Items.RemoveAt(lbUserLeft.SelectedIndex)
        End If
    End Sub
    Protected Sub btnRightRight_Click(sender As Object, e As System.EventArgs) Handles btnRightRight.Click
        For Each item As ListItem In lbUserLeft.Items
           ' If item.Selected And Not lbUserRight.Items.Contains(item) Then
            Dim newItem As New ListItem(item.Text, item.Value)
            lbUserRight.Items.Add(newItem)

        Next
        lbUserLeft.Items.Clear()
    End Sub
    Protected Sub btnLeftLeft_Click(sender As Object, e As System.EventArgs) Handles btnLeftLeft.Click
        For Each item As ListItem In lbUserRight.Items
            ' If item.Selected And Not lbUserRight.Items.Contains(item) Then
            Dim newItem As New ListItem(item.Text, item.Value)
            lbUserLeft.Items.Add(newItem)

        Next
        lbUserRight.Items.Clear()
    End Sub
    Protected Sub btnSend_Click(sender As Object, e As System.EventArgs) Handles btnSend.Click

        Try
            Dim subject As String
            Dim message As String
            subject = txtSubject.Text
            message = txtMessage.Text
            For Each item As ListItem In lbUserRight.Items
                NotificationMailer.SendNotification(item.Value, subject, message)
            Next
            Alert("Alert Send to " + lbUserRight.Items.Count.ToString + " users")
            _clearAll()
           
        Catch ex As Exception
            Alert(ex.Message)
            _clearAll()
        End Try
      

    End Sub
    
   Protected Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
   		_clearAll()
	End Sub 
    
    Private Sub _clearAll()
        txtSubject.Text = ""
        txtMessage.Text = ""
        lbUserRight.Items.Clear()
        lbUserLeft.Items.Clear()
        _bindUsers()

    End Sub

    'Sub ServerValidate(ByVal source As System.Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs)
    '    If Me.lbUserRight.Items.Count = 0 Then
    '        args.IsValid = False
    '        Alert("not valid")
    '    Else
    '        args.IsValid = True
    '        Alert("Valid")
    '    End If
    'End Sub
End Class
