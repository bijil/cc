﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="autoprioritylog.aspx.vb" Inherits="CAMACloud.Console.autoprioritylog" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript" ></script>
 <h1>
        Auto Priority Log </h1>
   <div>
     <asp:UpdatePanel ID="uplMain" runat="server" UpdateMode="Always">
       <ContentTemplate>
       <style type="text/css">  
		        .WrapText {  
		            width: 100%;  
		            word-break: break-all;  
		        }  
		 </style> 
        <div class="WrapText"> 
		<asp:Panel runat="server" ID="pnl1" Visible="true">
			<asp:GridView runat="server" ID="gvRuleList"  AllowPaging="True" AllowSorting="True"  AutoEventWireup="false" PageSize="5" OnPageIndexChanging="gvRuleList_PageIndexChanging" Width="90%">
				<Columns>
					<asp:TemplateField HeaderText = "Sl No." ItemStyle-Width="100">
				        <ItemTemplate>
				            <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
				        </ItemTemplate>
				    </asp:TemplateField>
					<asp:BoundField DataField="RuleId" HeaderText="RuleId" ItemStyle-Width="300px" Visible="false" />
					<asp:BoundField DataField="Title" HeaderText="Title" ItemStyle-Width="300px" />
					<asp:BoundField DataField="Priority" HeaderText="Priority"  ItemStyle-Width="150px" />
					<asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate"  ItemStyle-Width="200px" />
					 <asp:TemplateField>
		                <ItemStyle Width="80px" />
		                <ItemTemplate>
							<asp:LinkButton runat="server" Text='<%# IIf( Eval("IsActive"),"Disable", "Enable")%>' CommandName='Enable' CommandArgument='<%# Eval("RuleId") %>'  />
						</ItemTemplate>
		            	</asp:TemplateField>
					<asp:TemplateField ItemStyle-Width="80px">
					<ItemTemplate>
						<asp:LinkButton ID="lbnEdit" runat="server" Text="Edit"  PostBackUrl='<%# Eval("RuleId", "~/protected/parcels/autopriorityrules.aspx?Id={0}") %>'/>
					</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField ItemStyle-Width="80px">
						<ItemTemplate>
							<asp:LinkButton runat="server" Text="Delete" CommandName='Delete1' CommandArgument='<%# Eval("RuleId") %>'  />
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
			</asp:GridView>
		</asp:Panel>
		</div>
       <div> 
		<asp:Panel runat="server" ID="pnlExcel" Visible="true">
			Filter by: Rule <asp:DropDownList ID="ddlRules" runat="server" width="150px" ClientIDMode="Static"  AutoPostBack="true" SelectedIndexChanged="ddlfilter_SelectedIndexChanged"></asp:DropDownList>
			   Last <asp:DropDownList ID="ddlRows" runat="server" width="150px" ClientIDMode="Static"  AutoPostBack="true" SelectedIndexChanged="ddlfilter_SelectedIndexChanged" >
                   <asp:ListItem Text="10" Value="1" />
				   <asp:ListItem Text="100" Value="2" />
				   <asp:ListItem Text="1000" Value="3" />
				    <asp:ListItem Text="5000" Value="4" />
				    <asp:ListItem Text="10000" Value="5" />
				    <asp:ListItem Text="ALL" Value="6" />
                   </asp:DropDownList> Rows
			<asp:GridView runat="server" ID="gvAuditTrail" AllowPaging="True" PageSize="10"  OnPageIndexChanging="gvAuditTrail_PageIndexChanging" Width="90%">
				<Columns>
				<asp:TemplateField HeaderText = "Sl No." ItemStyle-Width="100">
				        <ItemTemplate>
				            <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
				        </ItemTemplate>
				    </asp:TemplateField>
					<asp:BoundField DataField="ParcelId" HeaderText="Parcel Id" ItemStyle-Width="100px" />
					<asp:BoundField DataField="KeyField" HeaderText="" ItemStyle-Width="200px" />
					<asp:BoundField DataField="EventTime" HeaderText="Time"  ItemStyle-Width="200px" />
					<asp:BoundField DataField="OldPriority" HeaderText="Old Priority"  ItemStyle-Width="100px" />
					<asp:BoundField DataField="Description" HeaderText="Description"  ItemStyle-Width="300px"/>
					<asp:TemplateField ItemStyle-Width="80px">
				</asp:TemplateField>
				</Columns>
			</asp:GridView>
		</asp:Panel>
		</div>
		</ContentTemplate>
		   </asp:UpdatePanel>
    </div>
</asp:Content>
