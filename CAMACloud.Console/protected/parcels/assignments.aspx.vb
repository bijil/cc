﻿
Partial Class parcels_assignments
	Inherits System.Web.UI.Page


	Sub LoadUsers()
		Dim uc = Membership.GetAllUsers
		Dim users(uc.Count - 1) As MembershipUser
		Membership.GetAllUsers().CopyTo(users, 0)
		ddlUsers.DataSource = users.Where(Function(x) Roles.IsUserInRole(x.UserName, "MobileAssessor"))
		ddlUsers.DataValueField = "UserName"
		ddlUsers.DataTextField = "UserName"
		ddlUsers.DataBind()
		ddlUsers.InsertItem("-- Select --", "", 0)

	End Sub

	Sub LoadNeighborhoods() Handles ddlUsers.SelectedIndexChanged
		If ddlUsers.SelectedValue = "" Then
			pnlUser.Visible = False
		Else
			pnlUser.Visible = True
			rpPending.DataSource = d_("SELECT * FROM Neighborhood WHERE Completed = 0 AND AssignedTo = " + ddlUsers.SelectedValue.ToSqlValue + " ORDER BY AssignOrdinal")
			rpPending.DataBind()
			rpCompleted.DataSource = d_("SELECT * FROM Neighborhood WHERE Completed = 1 AND AssignedTo = " + ddlUsers.SelectedValue.ToSqlValue + " ORDER BY LastVisitedDate DESC")
			rpCompleted.DataBind()

			If rpPending.Items.Count > 0 Then
				lblNoPending.Visible = False
			Else
				lblNoPending.Visible = True
			End If

			If rpCompleted.Items.Count > 0 Then
				lblNoCompleted.Visible = False
			Else
				lblNoCompleted.Visible = True
			End If
		End If

	End Sub

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
			LoadUsers()
		End If
	End Sub
End Class
