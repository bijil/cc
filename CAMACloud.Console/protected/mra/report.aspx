﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="report.aspx.vb" Inherits="CAMACloud.Console.report" %>

<!DOCTYPE html>

<html>

<head runat="server">
    <title>Scatter Chart</title>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js'></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
</head>

<body>
    <div style="width:50%">
        <div>
            <canvas id="canvas" height="450" width="600"></canvas>
        </div>
    </div>
    <button id="randomizeData">Randomize Data</button>
    <script>
  var graphData = [];
  var pageSize = 0;

  function getGraphicalRecords(callback) {
      console.log(pageSize)
      $.ajax({
          type: 'POST',
          url: "/mra/graphicalrecords.jrq",
          dataType: 'json',
          data: {
              page: pageSize
          },
          success: function(res) {
              var i = 0
              while (i < res.length) {
                  graphData.push(res[i]);
                  i++;
              }
              console.log(res.length)
              if (res.length > 999) {
                  pageSize++;
                  getGraphicalRecords();
              }
          },

          error: function(response) {
              console.error(response);
          }

      })

  }
 
 
  /* var scatterChartData = {
        datasets: [{
     		fillColor: "#fff",
            strokeColor: "#fff",
            pointColor: "#fff",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
			data: [{
				x: 10,
				y: 20,
				Id:120
			}, {
				x: 13,
				y: 30,
					Id:121
			}, {
				x: 15,
				y: 40,
					Id:122
			}],
        }]};
    window.onload = function() {
    getGraphicalRecords();
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myScatter =new Chart(ctx, {
    		type: 'scatter',
        	data: scatterChartData,
        	options: {
	            responsive: true,
	            hoverMode: 'single', // should always use single for a scatter chart
	            scales: {
	            	xAxes: [{
	            		gridLines: {
	            			zeroLineColor: "#fff"
	            		}
	            	}]
	            }
            }
        });
$('#canvas').click( function(evt) {
   var activePoint = window.myScatter.getElementAtEvent(evt)[0];
   if(!activePoint || activePoint===undefined) return;
   var data = activePoint._chart.data;
   var datasetIndex = activePoint._datasetIndex;
   var label = data.datasets[datasetIndex].label;
   var value = data.datasets[datasetIndex].data[activePoint._index];
   console.log(value.Id);
   });
    };*/
 
    </script>
</body>

</html>