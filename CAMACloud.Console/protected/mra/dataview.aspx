﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DesktopWeb.master" CodeBehind="dataview.aspx.vb" Inherits="CAMACloud.Console.dataview" %>
<%@ Register TagPrefix="cc" TagName="MRA_NavigationControl" Src="~/App_Controls/mra/MRA_NavigationControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .aw {
            padding-right:15px;
        }

        .resd {

        }
            #divContentArea {
    top:100px;
    }
    .divNavBtn {
        padding: 4px 0px 4px;
    }
    </style>
<script>
function changeSelectedField(){
    var tempId=$('#<%= hdntemplateId.ClientID %>').val();
    $('.ddlModel option[value="'+tempId+'"]').prop('selected', true);
    }
     function showHide(){
   		var tempId	= $('.ddlModel').find(":selected").val();
        if(tempId != "") $('.Cont_Content').show();
    	else{ $('.pnlOutput').hide(); $('.Rght_linkdiv').hide(); $('.lbExport').hide();}
    }
$(function(){
$("#spanMainHeading").html('Residuals - Data View');
$('.dataView').hide();
showHide();
})
</script>
 </asp:Content>
 <asp:Content ID="Content3" ContentPlaceHolderID="NavBarContent" runat="server">
 <div class="Overlay_Div">
    <cc:MRA_NavigationControl ID="mra_nav" runat="server" />
    </div>
 </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="Cont_Content">
   
    <%--vm-str <div Style="float: left;">
    <div Style="float: left;background-color: #eaeaea;padding: 4px;width: 868px;"> vm-end--%>
    <asp:Panel runat="server" ID="pnlSelect" Visible="true"  Style="margin-top: 3px;" >
    <asp:HiddenField ID="hdntemplateId" runat="server" />
    <asp:HiddenField runat="server" ID="hdnLoadOnce" Value="0"/>
       <%--vm-str   <table>
            <tr>
                <td style="width: 110px;"><label class="Labl">Select Model:</label></td>
                <td style="width: 210px;">

                    <asp:DropDownList runat="server" ID="ddlModel" cssClass="ddlModel" Width="210px" AutoPostBack="true" />
                </td>
            </tr>
        </table>
    </asp:Panel>vm-end--%>
   
    </div>
    <asp:Panel runat="server" ID="pnlOutput" class="pnlOutput" Style="clear: both;float: left;margin-top: 10px;">
        <asp:HiddenField runat="server" ID="tid" />
        <table style="float:left">
            <tr>
                <td style="padding-top: 4px;width: 65px;"><label class="Labl">Usability: </label>
                </td>
                <td style="width: 80px;">
                    <asp:Label runat="server" ID="lblUsability" style="font-weight:bold;font-size:17px;" />
                </td>
                <td style="padding-top: 4px;width: 182px;padding-left: 30px;"><label class="Labl">Prediction Approximation:</label>
                </td>
                <td style="">
                    <asp:Label runat="server" ID="lblStandardError" style="font-weight:bold;font-size:17px;" />
                </td>
            </tr>
        </table>
         <asp:Panel runat="server" ID="pnlLinks" Style="float: right;margin-right: 20px;">
    	<asp:LinkButton style="margin-top:4px;width: 135px;font-size: 14px;" runat="server" class='lbExport' Text="Export Residual Output" ID="lbExport" />
         <%--vm-str  <asp:HyperLink style="margin-top:4px;width: 65px;" runat="server" ID="hlVisualization" Text="Plot-View" />
        <asp:HyperLink style="margin-top:4px;width: 70px;" runat="server" ID="hlCalc" Text="Calculator" />
        <asp:HyperLink style="margin-top:4px;width: 90px;" runat="server" ID="hlResult" Text="Analysis Result" />vm-end--%>
    </asp:Panel>
        <asp:GridView runat="server" ID="gvResiduals" PageSize="50" style="float: left;margin-right: 20px;" AllowPaging="true" PagerSettings-Position="TopAndBottom">
        </asp:GridView>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlResidual" Visible="false">
            <h3>Residual Output</h3>
            <asp:GridView runat="server" ID="residuals">
                <Columns>
                    <asp:BoundField DataField="ObservationNo" HeaderText="Observation No" ItemStyle-Width="80px" />
                    <asp:BoundField DataField="AssignmentGroup" HeaderText="Assignment Group" ItemStyle-Width="200px" />
                    <asp:BoundField DataField="KeyValue" HeaderText="Parcel" ItemStyle-Width="180px" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-CssClass="th-left" />
                    <asp:BoundField DataField="PredictedValue" HeaderText="Predicted Value" ItemStyle-Width="180px" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-CssClass="th-left" />
                        <asp:BoundField DataField="Residual" HeaderText="Residual" ItemStyle-Width="180px" ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-CssClass="th-left" />
                </Columns>
            </asp:GridView>
        </asp:Panel>
        <p>
            &nbsp;
        </p>
    </asp:Panel>
    </div>

    </div>
</asp:Content>
