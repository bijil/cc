﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DesktopWeb.master" CodeBehind="calculator.aspx.vb" Inherits="CAMACloud.Console.calculator" %>
<%@ Register TagPrefix="cc" TagName="MRA_NavigationControl" Src="~/App_Controls/mra/MRA_NavigationControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
 #divContentArea {
    top:100px;
    }
    .divNavBtn {
        padding: 4px 0px 4px;
    }
.result {
padding: 5px;
font-size: 16pt;
font-weight: bold;
border: 1px solid #BBF;
}

.mra_calc_form {
margin: 21px;
}

.mra_calc_form .mra_calc_row {
}

.mra_calc_form .mra_calc_label {
min-width: 120px;
padding-right: 15px;
white-space: nowrap;
}

.mra_calc_form .mra_calc_value {
padding-bottom: 8px;
}

.mra_calc_form .mra_calc_value input {
width: 100px;
font-size: 1.2em;
padding: 4px;
}

.mra_calc_form .mra_calc_value select {
max-width: 300px;
min-width: 112px;
font-size: 1.2em;
}

.clsdivSlctIn{
float:left;
width: auto;
border: 1px solid #3e76a2;
padding: 4px;
}
.clsdivSlctOut{
width: 100%;
float: left;
margin-top: 1%;
}
.btnCalc {  
    font-weight: bold;
    margin-top: 20px;
    border:none;
    border-radius: 0px;
    background: linear-gradient(#88c2f1,#326b84);
    color: white;
    font-family: sans-serif;
    font-size: 12px;
    cursor: pointer;
    height: 22px;
    }
</style>
<script>
$(function () {
$('.calculator').hide();
$('#calcstate').attr('name', 'calcstate');
$("#spanMainHeading").html('Predictive Calculator');
widthTop=$("#MainContent_tbl tr td").width();
$("#tdBtm").width(widthTop);
changeSelectedField();
showHide();
$('.ddlModel').change(function(){$("#<%=hdnLoadOnce.ClientID %>").val(2);})
})

function changeSelectedField(){
    var tempId=$("#<%=hdnModel.ClientID %>").val();
    $('.ddlModel option[value="'+tempId+'"]').prop('selected', true);
    }
    function showHide(){
   		var tempId	= $('.ddlModel').find(":selected").val();
        if(tempId != ""){
        	if($('.result').find('span').html()=="-")
        		$('.mraf').attr('value','');
        		$('.clsdivSlctIn').show();
        }
    	else{ $('.clsdivSlctIn').hide();
    		$('.Rght_linkdiv').hide(); 
    	}
    }
</script> 
</asp:Content>
 <asp:Content ID="Content4" ContentPlaceHolderID="NavBarContent" runat="server">
 <div class="Overlay_Div">
    <cc:MRA_NavigationControl ID="mra_nav" runat="server" />
    </div>
 </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="Cont_Content" >
<asp:HiddenField runat="server" ID="hdnModel" />
<asp:HiddenField runat="server" ID="hdnTID" />
<asp:HiddenField runat="server" ID="hdnLoadOnce" Value="0"/>
<asp:HiddenField runat="server" ID="calcstate" Value="" ClientIDMode="Static" />
<div class="clsdivSlctOut" >
<div class="clsdivSlctIn" >
 <%--vm-str<asp:Panel runat="server" ID="pnlSelect" Visible="true">
<table style="border: 1px solid #cccbcb;">
<tr>
<td style="padding-right: 20px;">Select Model: </td>
<td>
<asp:DropDownList runat="server" ID="ddlModel" Width="240px" AutoPostBack="true" />
</td>
</tr>
</table>
</asp:Panel> vm-end--%>

<asp:Panel runat="server" ID="pnlCalculator" Visible="true">
<p style="font-weight: bold;">
<asp:Label runat="server" ID="lblRegressionEquation" />
</p>
<asp:Table runat="server" ID="tbl" CssClass="mra_calc_form" />


<table class="mra_calc_form" id="tblBtm" style="margin-top:0px;">
<tr class="mra_calc_row">
<td class="mra_calc_label" id="tdBtm">
<asp:Label runat="server" ID="lblOutputLabel" />:
</td>
<td class="mra_calc_value result" >
<asp:Label runat="server" ID="lblOutput" Text="-" />
</td>
</tr>
<tr>
<td></td>
<td class="c" >
<asp:Button class="btnCalc" runat="server" ID="btnCalculate" Text=" Calculate Predicted Value " ValidationGroup="InputForm" />
</td>
</tr>
</table>
<div style="width: 230px; text-align: center">
</div>
</asp:Panel>
</div>
</div>
</div>
</asp:Content>
