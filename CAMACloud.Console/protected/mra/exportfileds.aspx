﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="exportfileds.aspx.vb" MasterPageFile="~/App_MasterPages/DesktopWeb.master" Inherits="CAMACloud.Console.exportfileds" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type ="text/javascript">
var expanded = false;
$(document).ready(function(e){
if ( $( e.target ).parents( '.cbl_show' ).length == 0 )
$( '#checkboxes' ).hide();
var t = $( '#checkboxes input:checked' ).length;
if ( t == 0 )
$( ".selectBox select option[value='0']" ).text( '--Select--' )
else
$( ".selectBox select option[value='0']" ).text( t + ' selected' )

})
$(document).on('click',function(e){
if ( $( e.target ).parents( '.cbl_show' ).length == 0 )
$( '#checkboxes' ).hide();
})
function showCheckboxes()
{
var checkboxes = document.getElementById( "checkboxes" );
if ( !expanded)
{
checkboxes.style.display = "block";
expanded = true;
$( '.cbl_show input[type="checkbox"]' ).unbind( 'click' )
$('.cbl_show input[type="checkbox"]').bind('click', function (e) {
var t = $( '#checkboxes input:checked' ).length;
if ( t == 0 )
$( ".selectBox select option[value='0']" ).text( '--Select--' )
else
$( ".selectBox select option[value='0']" ).text( t + ' selected' )
} )
} else
{
checkboxes.style.display = "none";
expanded = false;
}
}

function OpenAddWindow()
{
$('#MainContent_divAddcontnt1').css("display", "block");
$('#MainContent_divAddcontnt2').css("display", "block");
return false;
}
$(function(){
$("#spanMainHeading").html('MRA Settings');
})
</script>

<style type="text/css">
.selectBox {
position: relative;
}
.overSelect {
position: absolute;
left: 0;
right: 0;
top: 0;
bottom: 0;
}
#checkboxes {
position: absolute;
display: none;
border: 1px #dadada solid;
height: 160px;
overflow: auto;
width: 239px;
background: white;
}

</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server"> 
<asp:LinkButton id = "linkFilterSettings" runat = "server" Text = "Filter Settings" Style = "float:right;font-size:15px;padding-right:15px;font-weight: bold;"></asp:LinkButton>
	<div class="Cont_Content" style="margin-top: 1%;padding-left: 30px;position:absolute;">
	<div id="divAddcontnt1" runat="server" style="float: right;width: 45%;padding-left: 5%;padding-top: 36px;">
		<span class="spn_Subhead" ><span  >Select Fields</span></span>
			<table>
				<tr class = "secExpFields">
					<td style="padding-left: 15px;padding-top: 15px;"> Select Fields: </td>
					<td class="cbl_show" style = "padding-left: 25px;padding-top: 15px;">
						<div class="selectBox tip" onclick="showCheckboxes()" abbr="Select the fields from the drop-down list">
							<asp:DropDownList runat="server" ID="ddlExpFields" Width="250px"  />
						<div class="overSelect"></div>
						</div>
						<div id="checkboxes" style = "overflow-y : scroll; width : 250px;">
							<asp:CheckBoxList ID="cbl_ExpFields" runat="server"></asp:CheckBoxList>
						</div>
					</td>
			</tr>
		</table>
		<div id="divBtn" style="float: left;margin-left: 34%;margin-top: 6%;">
			<asp:Button runat="server" class="btnSav" ID="btnSave" ValidationGroup="SaveFilter" Text=" Save " />
			<asp:Button runat="server" class="btnSav" ID="btnCancel" Text=" Cancel "/>
		</div>
	</div>
</div>
<div style="float: left;width: 60%;padding-left: 30px;">
<table>
<tr>
	<td style = "width:302px"><span class="spn_Subhead" style="width:60%"><span  >Additional Export Fields</span></span></td>
	<td><span style="float:right;Margin-top:25px;"><asp:LinkButton Text="Add New" runat="server" ID="btnAdd" style="font-weight: bold;"/></span></td>
</tr>
<tr>	
	<td colspan = "2">
		<div style="float: right;width: 100%;padding-top:10px;min-height:200px">
		<asp:GridView runat="server" ID="gvExportFields" AutoGenerateColumns="False" DataKeyNames="RowId">
			<Columns>
				<asp:BoundField DataField="Name" HeaderText="Name" ItemStyle-Width="150px" >
					<ItemStyle Width="150px" />
				</asp:BoundField>
				<asp:BoundField DataField="DisplayLabel" HeaderText="Display Label" ItemStyle-Width="150px" >
					<ItemStyle Width="150px" />
				</asp:BoundField>
				<asp:BoundField DataField="DisplayLabel" HeaderText="Source Table" ItemStyle-Width="150px" >
					<ItemStyle Width="150px" /> 
				</asp:BoundField>
					<asp:TemplateField>
						<ItemStyle Width="50px" />
							<ItemTemplate>
								<asp:LinkButton runat="server" ID="lkDelete" Text="Delete"  CommandName='DeleteAction' CommandArgument='<%# Eval("RowId") %>' />
							</ItemTemplate>
					</asp:TemplateField>

			</Columns>
		<EmptyDataTemplate>No Record Available</EmptyDataTemplate> 
	</asp:GridView>
</div> 
	</td>
</tr>
<table>
</div>
</asp:Content>

