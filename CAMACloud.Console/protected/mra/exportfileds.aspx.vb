﻿Imports CAMACloud.BusinessLogic.DataAnalyzer
Imports System.IO

Public Class exportfileds
    Inherits System.Web.UI.Page

     Dim fields As List(Of DataSourceField)

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        fields = DataSourceField.GetFields
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
        	LoadGrid()
        	divAddcontnt1.Visible =False
            ddlExpFields.InsertItem("--Select fields--", "0", 0)
        End If
    End Sub


    Sub LoadGrid()
        gvExportFields.DataSource = Database.Tenant.GetDataTable("SELECT m.RowId,m.FieldId,d.Name,d.DisplayLabel,d.SourceTable FROM MRA_Filters m INNER JOIN DataSourceField d on m.FieldId = d.Id ")
        gvExportFields.DataBind()

    End Sub

    Private Sub gvExportFields_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvExportFields.PageIndexChanging
        gvExportFields.PageIndex = e.NewPageIndex
        LoadGrid()
    End Sub

    Private Sub gvExportFields_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvExportFields.RowCommand
        Dim rowid As Integer = e.CommandArgument
        If e.CommandName = "DeleteAction" Then
        	Database.Tenant.Execute("DELETE FROM MRA_Filters where RowId = " & rowid)
        	divAddcontnt1.Visible =False
        	btnAdd.Visible = True
        End If
        LoadGrid()
    End Sub

Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
		btnAdd.Visible = False
		cbl_ExpFields.DataSource = Database.Tenant.GetDataTable("SELECT df.Id, df.AssignedName FROM DataSourceField df INNER JOIN DataSourceTable dt ON df.TableId = dt.Id WHERE dt.CC_TargetTable = {0} AND df.Id NOT IN (SELECT FieldId FROM MRA_Filters WHERE IsExportField = 1) ORDER BY df.Id".SqlFormatString("ParcelData"))
        cbl_ExpFields.DataTextField = "AssignedName"
        cbl_ExpFields.DataValueField = "Id"
        cbl_ExpFields.DataBind()
        For Each item As ListItem In cbl_ExpFields.Items
            item.Selected = False
        Next
    	divAddcontnt1.Visible =True 
     End Sub
    
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
    	Dim Flag = False
        For Each item As ListItem In cbl_ExpFields.Items
        	If item.Selected Then
        		Flag = True
                Database.Tenant.Execute("INSERT INTO MRA_Filters (FieldId ,IsExportField) values ({0},1) ".SqlFormatString(item.Value))
            End If
        Next
        For Each item As ListItem In cbl_ExpFields.Items
            item.Selected = False
        Next
        LoadGrid()
        If Flag = True
        	divAddcontnt1.Visible =False
        	Alert("Export Fields have been saved successfully.")
        Else
        	Alert("Please select fields to continue.")
        End If 
        btnAdd.Visible = True
    End Sub
    
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
    	divAddcontnt1.Visible =False
    	btnAdd.Visible = True
    End Sub
    
     Private Sub linkFilterSettings_Click(sender As Object, e As EventArgs) Handles linkFilterSettings.Click
    	Response.Redirect("/protected/mra/settings.aspx")
    End Sub
End Class