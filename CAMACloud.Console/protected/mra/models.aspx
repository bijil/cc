﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DesktopWeb.master" CodeBehind="models.aspx.vb" Inherits="CAMACloud.Console.models" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script>
$(function(){
$("#spanMainHeading").html('Cost Prediction / Regression Models');
})
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="Cont_Content">
<table>
    	<tr>
			<td align="right">
				<div style = "float:right;padding: 6px 0px 0px 0px;">
			        <div>
			        	<asp:LinkButton runat="server" ID="btnExp" Text="Export Templates" style="margin-left:10px;font-size: 14px;" />
			        	<asp:LinkButton runat="server" ID="btnImp" Text="Import Templates" style="margin-left:10px;font-size: 14px;" OnClientClick="document.getElementById('importFile').click();return false;"/>
			        	<asp:HyperLink runat="server" Text="Create New" CssClass="b" style = "float:left;font-size: 14px;" NavigateUrl="~/protected/mra/design.aspx" />
			        </div>
			        <div>
				        <div style="display:none;">
					        <asp:FileUpload runat="server" ID="importFile" ClientIDMode="Static" onchange="document.getElementById('btnImport').click()"  accept="application/xml" />
			                <asp:Button runat="server" ID="btnImport" ClientIDMode="Static" />
				        </div>
			        </div>
		        </div>			
			</td>
		<tr>
			<td colspan = "2">		
		        <div class="clear"></div>
		        <asp:GridView runat="server" ID="grid" >
		            <Columns>
		                <asp:BoundField DataField="Ordinal" DataFormatString="{0}. " ItemStyle-HorizontalAlign="Right"  ItemStyle-Width="25px" />
		                <asp:HyperLinkField DataTextField="Name" DataNavigateUrlFields="ID" HeaderText="Model Name" ItemStyle-Width="300px" itemstyle-font-size="15px" DataNavigateUrlFormatString="resultview.aspx?_tid={0}" />
		                <asp:BoundField DataField="OutputFieldName" HeaderText="Output Field" ItemStyle-Width="350px" />
		                <asp:BoundField DataField="PoolSize" HeaderText="Pool Size" ItemStyle-Width="90px" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="c" />
		                <asp:BoundField DataField="CreatedBy" HeaderText="Created by" ItemStyle-Width="180px" />
		                <asp:HyperLinkField Text="Edit" DataNavigateUrlFields="ID" DataNavigateUrlFormatString="design.aspx?_tid={0}" ItemStyle-Width="50px"  ItemStyle-Font-Bold="true" />
		                <asp:TemplateField>
		                    <ItemStyle Width="50px" />
		                    <ItemTemplate>
		                        <asp:LinkButton runat="server" Text="Delete" CommandName="DeleteItem" CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you sure you want to delete this regression model permanently?')" />
		                    </ItemTemplate>
		                </asp:TemplateField>
		            </Columns>
		            <EmptyDataTemplate>
		                No regression models have been created yet! Click <b>Create New</b> button to create a new regression model.
		            </EmptyDataTemplate>
		        </asp:GridView>
		    </td>
		</tr>		    
</div>
</asp:Content>
