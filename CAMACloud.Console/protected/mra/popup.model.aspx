﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/Popup.master" AutoEventWireup="false" Inherits="CAMACloud.Console.mra_popup_model" Codebehind="popup.model.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .result
        {
            padding:5px;
            font-size:16pt;
            font-weight:bold;
            border-top:1px solid #BBF;
            border-bottom:3px solid #99F;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
	<h1>Regression Model</h1>
	<p style="font-weight:bold;"><asp:Label runat="server" ID="lblRegressionEquation" /></p>
    <p style="width:450px;">Enter values in the form below and click the <b>Calculate</b> button to get the predicted value based on the regression model.</p>
	<asp:Table runat="server" ID="tbl" />
	<asp:Button runat="server" ID="btnCalculate" Text=" Calculate Predicted Value " ValidationGroup="InputForm" />
	<p class="result">
		<asp:Label runat="server" ID="lblOutput" />
	</p>
</asp:Content>

