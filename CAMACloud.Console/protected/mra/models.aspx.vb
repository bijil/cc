﻿Imports CAMACloud.BusinessLogic
Imports CAMACloud.BusinessLogic.Installation
Imports CAMACloud.BusinessLogic.Installation.EnvironmentConfiguration
Imports CAMACloud.BusinessLogic.RemoteIntegration
Imports System.IO
Imports System.Xml
Imports System.Data
Imports System.Linq

Public Class models
    Inherits System.Web.UI.Page
    Sub RefreshTemplates()
        Dim sqlTemplates = "SELECT ROW_NUMBER() OVER (ORDER BY t.Name) As Ordinal, t.*, CASE WHEN f.Name <> f.DisplayLabel THEN f.DisplayLabel + ' - ' ELSE '' END + f.SourceTable + '.' + f.Name As OutputFieldName FROM MRA_Templates t INNER JOIN DataSourceField f ON t.OutputField = f.Id ORDER BY t.Name"

        grid.DataSource = Database.Tenant.GetDataTable(sqlTemplates)
        grid.DataBind()
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            RefreshTemplates()
        End If

    End Sub

    Private Sub grid_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grid.RowCommand
        If e.CommandName = "DeleteItem" Then
            Dim tid As Integer = e.CommandArgument

            Database.Tenant.Execute("DELETE FROM MRA_Templates WHERE Id = " & tid)
            RefreshTemplates()
        End If
    End Sub
    
    Protected Sub btnExp_Click(sender As Object, e As System.EventArgs) Handles btnExp.Click
        Dim gen = New EnvironmentConfiguration
        gen.ExportSettings(ConfigFileType.MRATemplates, Response, "MRATemplates", Database.Tenant)
	End Sub
	
	
	Protected Sub btnImp_Click(sender As Object, e As System.EventArgs) Handles btnImport.Click
		If importFile.HasFile Then
'			Try
'	            Dim gen = New EnvironmentConfiguration
'	            gen.ImportSettings(importFile.FileContent, ConfigFileType.MRATemplates, Database.Tenant, Membership.GetUser().ToString)
'	            Alert("Imported successfully.")
'	        Catch ex As Exception
'	               Alert(ex.Message)
'	               Return
'	        End Try
			Dim stream As IO.Stream = importFile.FileContent
			Dim strXml As String
            Using Xmlreader = New StreamReader(stream, Encoding.UTF8)
            	strXml = Xmlreader.ReadToEnd()
            End Using
            Dim xmlDocument = XDocument.Parse(strXml)
            Dim dupNames As New ArrayList
            Dim currDate As Date = DateAndTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
            Dim lastExecutionTime As DateTime
            Dim noName = False
            For Each rowItem In xmlDocument.Descendants("MRATemplates")
            	Dim dupCount = 0
            	Dim name As String  = rowItem.@Name.ToString
            	lastExecutionTime = IIf(rowItem.@LastExecutionTime<>"",rowItem.@LastExecutionTime.ToString,Nothing)
        		dupCount = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM  MRA_Templates WHERE Name = {0}".SqlFormatString(name))
        		If dupCount > 0 Then
        			dupNames.Add("'" + name + "'")
        		Else If rowItem.@Name.ToString = ""
        			noName = True
        		Else
        			Dim sqlFCTemplate As String = "INSERT INTO MRA_Templates (Name,OutputField,InputFields,Filters,CreatedBy,CreatedDate,PoolSize,LastExecutionTime,CostFormula,CostFormulaSQL,Groups) VALUES ({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10})"     			
        			Database.Tenant.Execute(sqlFCTemplate.SqlFormat(True,rowItem.@Name,rowItem.@OutputField,rowItem.@InputFields,rowItem.@Filters,rowItem.@CreatedBy,currDate,rowItem.@PoolSize,lastExecutionTime,rowItem.@CostFormula,rowItem.@CostFormulaSQL,rowItem.@Groups))
        		End If
            Next
            Dim duplicateNames = ""
            If dupNames.Count > 0 Then
            	duplicateNames = Strings.Join(dupNames.ToArray, ",")
            End If
            If noName = True Then
            	Alert("Some templates do not have names and these templates were ignored during import.Please use template name and try again.")
            End If	
        	If duplicateNames <> "" Then
        		Dim alertMsg As String = "The template names " + duplicateNames  +" have already been used and these templates were ignored during import.Please use different names and try again."
        		Alert(alertMsg)
            End If	
        	If noName = False AndAlso duplicateNames = ""
        		Alert("Import completed successfully.")
        	End If
	        RefreshTemplates()
        End If     
    End Sub

End Class