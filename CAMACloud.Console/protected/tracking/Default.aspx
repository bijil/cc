﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/CAMACloud.master"
	AutoEventWireup="false" Inherits="CAMACloud.Console.tracking_Default" Codebehind="Default.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
	<style type="text/css">
		.panel-spacer
		{
			display: none;
		}
		#map
		{
			background: #8f9292;
		}
		
		fieldset
		{
			border: 1px solid #CFCFCF;
			border-radius: 6px;
			padding: 0px 20px 30px 10px;
		}
		
		legend
		{
			font-weight: bold;
		}
		
		.track-user span
		{
			float: right;
			font-size: smaller;
			color: #0bc0f8;
			margin-top: 5px;
		}
		
		.filters select
		{
			width: 206px;
			height: 20px;
			display: block;
			margin-top: 2px;
			margin-bottom: 2px;
		}
		
		.filters input
		{
			width: 200px;
			height: 20px;
			display: block;
			margin-top: 2px;
			margin-bottom: 2px;
			padding: 0px;
		}
		
		.filters label
		{
			margin-top: 10px;
			display: block;
		}


	</style>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyC7e5YrZlKqWtaY4ehsDwMgCis4IfNs7Xw"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OverlappingMarkerSpiderfier/1.0.3/oms.min.js"></script>
    <script type="text/javascript" src="/App_Static/js/qc/000-utillib.js"></script>
	<script type="text/javascript" src="/App_Static/js/tracking.js"></script>
    <script type="text/javascript" src="../../App_Static/js/qc/010-constants.js?1234"></script>
    <script type="text/javascript" src="../../App_Static/js/qc/000-objects.js"></script>
    <script type="text/javascript" src="../../App_Static/jslib/jsrepeater.js"></script>
    <script type="text/javascript" src="/App_Static/js/parcelmanager.js"></script>
  
	<script type="text/javascript">
        var trackPoints = {};
        var trackBounds = new google.maps.LatLngBounds();
        var timerTracking, omSpider;
        var centered = false;
        var fullScreenTrue = false;


        function hideSearchScreen() {
            var w = $(window).width()
            var h = $(window).height();
            $('.left-panel').hide();
            $('.hideSidePanel').hide();
            $('.left-panel2').show();
            $('.showSidePanel').show();
            var wid = $('body').width();
            $('.widget-scene-canvas').width(wid);
            setScreenDimensions();
        }


        $(function () {
            $(".datetime").datetimepicker("destroy");
            $('#startdate').datetimepicker({
                beforeShow: function (input, inst) {
                    setTimeout(function () {
                        inst.dpDiv.css({
                            top: $('#startdate').offset().top + 20,
                            left: $('#startdate').offset().left
                        });
                    }, 0);
                },
                onSelect: function (dateText) {
                    if ($('#startdate').val() == '') {
                        $('#startdate').val(new Date());
                    }
                },

                onClose: function (sddateText, inst) {
                    setOneDayRangeReq();
                }


            });


            $('#enddate').datetimepicker({
                beforeShow: function (input, inst) {
                    setTimeout(function () {
                        inst.dpDiv.css({
                            top: $('#enddate').offset().top + 20,
                            left: $('#enddate').offset().left
                        });
                    }, 0);
                },

                onSelect: function (dateText) {
                    if ($('#enddate').val() == '') {
                        $('#enddate').val(new Date());
                    }
                },

                onClose: function (dateText, inst) {
                    setOneDayRangeReq();
                },

            });


            hght = $(window).height() - 8;
            $('#map').height(hght);
            $(window).resize(function () {
                //Wnhgt=$(window).height()-20;
                //$('#map').height(Wnhgt);
                var visble = $('.left-content-area').is(':visible');
                if (visble)
                    var hght = $('.left-panel').height() + 2;
                else
                    var hght = $('.left-panel2').height() + 2;

                if (fullScreenTrue)
                    hght = $(window).height() - 20;
                if (window.innerWidth == screen.width && window.innerHeight == screen.height)
                    fullScreenTrue = true;
                else
                    fullScreenTrue = false;
                $('#map').height(hght);
            });


            $('.tracking-tabs').tabs({
                select: function (event, ui) {
                    switchView(ui.panel.id);
                }
            });
            updateTrackingLayer(function () {
                centerTracking();
            });
            var hght = $('.left-panel').height();
            $('#map').height(hght);
        });

        function setOneDayRangeReq() {
            var sd = $("#startdate").datepicker('getDate');
            var ed = $("#enddate").datepicker('getDate');

            if (sd == null || ed == null) return false;

            var edday = ed.getDate();
            var edmonth = ed.getMonth() + 1;
            var edyear = ed.getFullYear();
            var edfullDate = edmonth + "/" + edday + "/" + edyear;

            var sdday = sd.getDate();
            var sdmonth = sd.getMonth() + 1;
            var sdyear = sd.getFullYear();
            var sdfullDate = sdmonth + "/" + sdday + "/" + sdyear;

            var edHrs = ed.getHours();
            var edMin = ed.getMinutes();

            if (sdfullDate == edfullDate && edHrs == 0 && edMin == 0) {
                $("#enddate").datepicker("setDate", edfullDate + " 23:59");
            }
        }

        var activeTab;
        function switchView(tab) {
            activeTab = tab;
            switch (tab) {
                case "tracking":
                    cleanHistoryPoints();
                    refreshTracking();
                    break;
                case "history":
                    if (timerTracking != null)
                        window.clearTimeout(timerTracking);
                    clearTrackingPoints();
                    setTimeout(function () { var hght = $('.left-panel').height() + 2; $('#map').height(hght); }, 0);

                    var regex = /(0\d{1}|1[0-2])\/([0-2]\d{1}|3[0-1])\/(19|20)\d{2}/;
                    var validateStart = $('#startdate').val()
                    var validateEnd = $('#enddate').val()
                    var validateStartDate = (regex.test($('#startdate').val()));
                    var validateEndDate = (regex.test($('#enddate').val()));

                    if (($('.history-agent').val() != "") && (Date.parse($('#enddate').val()) > Date.parse($('#startdate').val())) && (validateStartDate && validateEndDate)) {
                        getHistoryLayer();
                    }

                    break;
            }
        }

        function updateTrackingLayer(callback) {
            if (timerTracking != null)
                window.clearTimeout(timerTracking);

            var updateTick = (new Date()).getTime();

            $('.track-user').each(function () {
                var selected = $(':checkbox', this)[0].checked;
                if (selected) {
                    var user = $(this).attr('loginid');
                    var lat = parseFloat($(this).attr('latitude'));
                    var lng = parseFloat($(this).attr('longitude'));
                    var acc = parseInt($(this).attr('accuracy'));
                    var name = $('label', this).text().trim();
                    var lut = $('span', this).text().trim();
                    var initials = '';
                    var ll = new google.maps.LatLng(lat, lng);
                    var names = name.split(' ');
                    for (x in names) {
                        initials += names[x].charAt(0);
                    }

                    var accs = acc + 'm';
                    if (acc > 1000) {
                        accs = Math.round(acc / 1000) + 'km';
                    }

                    if (trackPoints[user] == null) {
                        var mo = {
                            icon: {
                                url: createMarkerSvg(initials)
                            }
                        }
                        //icon: new google.maps.MarkerImage("https://chart.googleapis.com/chart?chst=d_map_spin&chld=0.8|0|FFFF42|10|b|" + initials)
                        var mk = new google.maps.Marker(mo);

                        var range = new google.maps.Circle({
                            map: map,
                            strokeColor: '#0000FF',
                            fillColor: '#0095fc',
                            fillOpacity: 0.2,
                            strokeWeight: 1
                        });

                        mk.setMap(map);
                        range.bindTo('center', mk, 'position');

                        trackPoints[user] = { marker: mk, range: range };
                    }

                    var marker = trackPoints[user].marker;
                    var range = trackPoints[user].range;

                    marker.setPosition(ll);
                    marker.setTitle(name + '\n\nLocated within ' + accs + "\nLast reported: " + lut);
                    range.setRadius(acc);

                    trackPoints[user] = {
                        user: user,
                        point: ll,
                        accuracy: acc,
                        marker: marker,
                        range: range,
                        updated: updateTick
                    };
                }
            });

            trackBounds = new google.maps.LatLngBounds();

            var pc = 0;
            for (x in trackPoints) {
                var pt = trackPoints[x];
                if (pt.updated < updateTick) {
                    pt.marker.setMap(null);
                    pt.range.setMap(null);
                    pt.marker = null;
                    pt.range = null;
                    delete trackPoints[x];
                } else {
                    pc++;
                }

            }

            for (x in trackPoints) {
                trackBounds.extend(trackPoints[x].point);
            }

            if (!centered)
                centerTracking();

            timerTracking = window.setTimeout('refreshTracking();', 1 * 60 * 1000);

            if (callback) callback();
        }

        function clearTrackingPoints() {
            for (x in trackPoints) {
                var pt = trackPoints[x];
                pt.marker.setMap(null);
                pt.range.setMap(null);
                pt.marker = null;
                pt.range = null;
                delete trackPoints[x];
            }
        }

        function centerTracking() {
            var pc = 0;
            for (x in trackPoints) {
                pc++;
            }
            if (pc == 0) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    map.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
                    centered = false;
                });
            } else {
                map.setCenter(trackBounds.getCenter());
                map.fitBounds(trackBounds);
                centered = true;
            }
        }

        function locateUser(label) {
            var ug = $(label).parents('.track-user');
            var selected = $(':checkbox', ug)[0].checked;
            var user = $(ug).attr('loginid');
            if ($(label).attr('type') != 'checkbox') {
                if (!selected) {
                    $(':checkbox', ug)[0].checked = true;
                }
                else
                    $(':checkbox', ug)[0].checked = false;
            }
            updateTrackingLayer(function () {
                if (trackPoints[user]) map.setCenter(trackPoints[user].point);
            });
        }
        function utl() {
            try {
                updateTrackingLayer();
            }
            catch (e) {
                console.error(e);
                console.error(e);
            }
            return false;
        }

        function refreshTracking() {
            $('#btnRefresh').click();
        }

        function createPinMarker(clr, text) {
            let pinColor = '#' + clr.split('|')[0], borderColor = '#' + clr.split('|')[1];
            let svgString = `<svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" viewBox="0 0 24 24"><path d="M12 2C8.14 2 5 5.13 5 9c0 4.41 5.4 9.6 6.3 10.49.39.4 1.01.4 1.4 0 .9-.89 6.3-6.08 6.3-10.49C19 5.13 15.86 2 12 2z" fill="${pinColor}" stroke="${borderColor}" stroke-width="1" /><text x="12" y="13" text-anchor="middle" font-family="Arial" font-size="6" fill="${borderColor}">${text}</text></svg>`;
            let url = `data:image/svg+xml;charset=utf-8,${encodeURIComponent(svgString)}`;
            return url;
        }

        function drawOutlinedText(text, fontSize, fontColor, outlineColor) {
            let canvas = document.createElement('canvas');
            let ctx = canvas.getContext('2d');
            ctx.font = fontSize + 'px Arial';
            let textWidth = ctx.measureText(text).width;
            canvas.width = textWidth + 3;
            canvas.height = fontSize + 3;
            ctx.fillStyle = '#' + fontColor;
            ctx.strokeStyle = '#' + outlineColor;
            ctx.lineWidth = 2;
            ctx.strokeText(text, 1, fontSize - 1);
            ctx.fillText(text, 1, fontSize - 1);
            return canvas.toDataURL();
        }

        function createMarkerSvg(initials) {
            let svgString = `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 64 64" width="64" height="64"><circle cx="24" cy="24" r="16" fill="#FFFF42" /><text x="25" y="28" font-family="Arial, sans-serif" font-size="10px" font-weight="bold" fill="black" text-anchor="middle">${initials}</text></svg>`;
            let url = `data:image/svg+xml;charset=utf-8,${encodeURIComponent(svgString)}`;
            return url;
        }

        /*
    	
        TRACKING

        */

        var historyScroller;
        var historyPoints = [];
        var visitedParcels = [];
        var infoWindow = new google.maps.InfoWindow({ content: 'parcel' });

        function getHistoryLayer() {
            omSpider = new OverlappingMarkerSpiderfier(map,
                {
                    markersWontMove: true,
                    markersWontHide: true,
                    keepSpiderfied: true,
                    circleSpiralSwitchover: 10,
                    basicFormatEvents: true,
                    circleFootSeparation: 50
                });
            var keys = $('.history-agent').val();
            if (keys == '') {
                alert('Please select an Agent to view the Field Tracking history.');
                return;
            }
            //var url = baseUrl + 'tracking/history.kml';
            var url = '/tracking/historypoints.jrq';
            url += '?keys=' + keys;

            /*if (keys == '') {
                alert('Please select an agent to view the field track history.');
                return;
            }*/

            if ($('#startdate').val() == '') {
                $('#startdate').val(new Date().toUSString());
            }

            if ($('#enddate').val() == '') {
                var ed = Date.from('#startdate').getTime() + (1000 * 60 * 60 * 24);
                var ed2 = (new Date())
                isNaN(ed) ? ed2.setTime(ed2) : ed2.setTime(ed);
                $('#enddate').val(ed2.toUSString());
            }

            var regex = /(0\d{1}|1[0-2])\/([0-2]\d{1}|3[0-1])\/(19|20)\d{2}/;
            var validateStart = $('#startdate').val() && $('#startdate').val().trimRight();
            var validateEnd = $('#enddate').val() && $('#enddate').val().trimRight();
            var dateRegex = /^\d{2}\/\d{2}\/\d{4}$/;
            var startdateOnly = validateStart.split(' ').length > 1 ? validateStart.split(' ')[0] : validateStart;
            var endDateOnly = validateEnd.split(' ').length > 1 ? validateEnd.split(' ')[0] : validateEnd;
            var vaild_Start = (dateRegex.test(startdateOnly));
            var vaild_End = (dateRegex.test(endDateOnly));
            var validateStartDate = (regex.test($('#startdate').val()));
            var validateEndDate = (regex.test($('#enddate').val()));

            if ((Date.parse($('#enddate').val())) - (Date.parse($('#startdate').val())) >= 5184000000) {
                var sd = (new Date()).addDay(60);

                $('#enddate').val(sd.toUSStringTime());
            }

            if (!(validateStartDate && validateEndDate) || !(vaild_Start && vaild_End)) {
                alert("Please enter a valid Date Format.");
                return;
            }
            else if (validateEnd.length > 16 || validateStart.length > 16) {
                alert("Please enter a valid Date Format.");
                return;
            }
            else if (validateEnd.length > 8 || validateStart.length > 8) {
                var hrRegx = /^([01]?[0-9]|2[0-3]):[0-5][0-9]$/;
                var vstime = true, vetime = true;
                if (validateStart.length > 8) {
                    var stime = validateStart.split(' ')[1];
                    vstime = ((stime && stime.trim() != "")) ? hrRegx.test(stime) : true;
                }
                if (validateEnd.length > 8) {
                    var stime = validateEnd.split(' ')[1];
                    vetime = ((stime && stime.trim() != "")) ? hrRegx.test(stime) : true;
                }
                if (!(vetime && vstime)) {
                    alert("Please enter a valid Date Format.");
                    return;
                }
            }

            if (Date.parse($('#enddate').val()) < Date.parse($('#startdate').val())) {
                alert('The End Date should be greater than or equal to the Start Date.');
                return;
            }

            var startDate = Date.from('#startdate');
            var endDate = Date.from('#enddate');

            //var endDate = Date.parse($('#enddate').val());
            ts1 = startDate.getTime();
            ts2 = endDate.getTime();

            tsnow = (new Date()).getTime();
            if (isNaN(ts2))
                ts2 = Math.min(ts1 + 86400, tsnow);

            var mac;
            if (mactoggle.checked == true) {
                mac = 1;
            }
            else {
                mac = 0;
            }

            url += '&ts1=' + ts1 + '&ts2=' + ts2 + '&mac=' + mac;

            $('.button-history').attr('disabled', 'disabled');

            cleanHistoryPoints();
            $('#points').html('');
            $('#pointsMac').html('');

            $$$(url, [], function (data) {
                if (data.failed) {
                    alert('Tracking history service is not currently available.');
                    $('.button-history').removeAttr('disabled');
                    return;
                }
                if (data.Parcels == undefined) {
                    data.Parcels = [];
                }
                if (data.Results == undefined) {
                    data.Results = [];
                }
                if (mactoggle.checked == false) {
                    if (data.Results) {
                        if (data.Results.length > 0) {
                            $('.points-display').show();
                            $('#points').html($('#points-template').html());
                            $('#points').fillTemplate(data.Results);

                            if (historyPoints.length == 0) {
                                var bounds = new google.maps.LatLngBounds();
                                var ind = 0;
                                var colors;

                                if (activeTab == "history")
                                    for (x in data.Results) {
                                        ind++;
                                        if (ind == 1) {
                                            colors = '33FF33|000000';
                                        } else if (ind == data.Results.length) {
                                            colors = 'FFFFFF|000000';
                                        } else {
                                            colors = 'FF8080|000000';
                                        }
                                        var tp = data.Results[x];
                                        var ll = new google.maps.LatLng(tp.lat, tp.lng);
                                        var mo = {
                                            position: ll,
                                            icon: {
                                                url: createPinMarker(colors, ind)
                                            }, //new google.maps.MarkerImage("https://chart.googleapis.com/chart?chst=d_map_pin_letter_withshadow&chld=" + ind + "|" + colors),
                                            title: tp.date + ' ' + tp.time
                                        }
                                        var mk = new google.maps.Marker(mo);
                                        mk.setMap(map);

                                        historyPoints.push(mk);
                                        bounds.extend(ll);
                                    }

                                if (activeTab == "history")
                                    map.fitBounds(bounds);

                            }
                        } else {
                            $('#points').html('No points found!');
                        }
                    }

                    if (data.Parcels) {
                        if (data.Parcels.length > 0) {
                            if (activeTab == "history")
                                if (visitedParcels.length == 0) {
                                    var ind = 0;
                                    for (x in data.Parcels) {
                                        ind += 1;
                                        var vp = data.Parcels[x];
                                        var ll = new google.maps.LatLng(vp.lat, vp.lng);
                                        var tc = "0000FF";
                                        var bc = "FFFFFF";
                                        if (vp.type == 1) {
                                            bc = "FFCC33";
                                        } else if (vp.type == 2) {
                                            bc = "FF0000";
                                        }
                                        var mo = {
                                            position: ll,
                                            icon: {
                                                url: drawOutlinedText(vp.assrno, 25, tc, bc)
                                            },//new google.maps.MarkerImage("https://chart.googleapis.com/chart?chst=d_text_outline&chld=" + tc + "|13|h|" + bc + "|b|" + vp.assrno),
                                            title: vp.address + ' at ' + vp.time
                                        }
                                        var mk = new google.maps.Marker(mo);
                                        mk.setMap(map);

                                        mk.Parcel = vp;

                                        google.maps.event.addListener(mk, 'click', function () {
                                            var vp = this.Parcel;
                                            var html = "<div><div style='border-bottom:1px solid'>" + vp.address + "</div>Type: " + (vp.type == 2 ? "Reference/Alert" : vp.type == 1 ? "Priority" : "Other") + "</div>"
                                            infoWindow.setContent(html);
                                            infoWindow.open(map, this);
                                            setTimeout(function () { $('.gm-style-iw-d').css({ overflow: 'hidden', fontSize: 'medium', padding: '0px 11px 5px 0px' }) }, 0.01);
                                        });

                                        visitedParcels.push(mk);
                                    }
                                }
                        }
                    }


                    $('.button-history').removeAttr('disabled');

                    if (activeTab != "history")
                        cleanHistoryPoints();

                }

                if (mactoggle.checked == true) {
                    if (data.Parcels) {
                        if (data.Parcels.length > 0) {
                            $('.points-display').hide();
                            $('#pointsMac').html($('#pointsMac-template').html());
                            $('#pointsMac').fillTemplate(data.Parcels);
                            if (activeTab == "history")
                                if (visitedParcels.length == 0) {
                                    var bounds = new google.maps.LatLngBounds();
                                    var ind = 0;
                                    var firstTime = true;
                                    data.Parcels = data.Parcels.filter((x) => { return x.lat && x.lng });
                                    for (x in data.Parcels) {
                                        ind += 1;
                                        var vp = data.Parcels[x];
                                        var ll = new google.maps.LatLng(vp.lat, vp.lng);
                                        var tc = "0000FF";
                                        var bc = "FFCC33";
                                        var mo = {
                                            position: ll,
                                            icon: {
                                                url: createPinMarker((bc + "|" + '000000'), ind)
                                            },//new google.maps.MarkerImage("https://chart.googleapis.com/chart?chst=d_map_pin_letter_withshadow&chld=" + ind + "|" + bc),
                                            title: vp.address + ' at ' + vp.time
                                        }
                                        var mk = new google.maps.Marker(mo);
                                        mk.setMap(map);
                                        mk.Parcel = vp;
                                        omSpider.addMarker(mk);

                                        google.maps.event.addListener(mk, 'click', function () {
                                            var vp = this.Parcel;
                                            var html = "<div><div style='border-bottom:1px solid'>" + vp.address + "</div>Type: " + (vp.type == 2 ? "Reference/Alert" : vp.type == 1 ? "Priority" : "Other") + "</div>"
                                            infoWindow.setContent(html);
                                            infoWindow.open(map, this);
                                            setTimeout(function () { $('.gm-style-iw-d').css({ overflow: 'hidden', fontSize: 'medium', padding: '0px 11px 5px 0px' }) }, 0.01);
                                        });

                                        visitedParcels.push(mk);
                                        if (ll && firstTime) {
                                            bounds.extend(ll);
                                            map.setCenter(ll);
                                            map.setZoom(11);
                                            firstTime = false;
                                        }
                                    }
                                }

                        }
                        else {
                            $('#points').html('No parcels found!');
                        }
                    }
                    $('.button-history').removeAttr('disabled');

                    if (activeTab != "history")
                        cleanHistoryPoints();
                }
            }, function () {
                $('.button-history').removeAttr('disabled');
            });
        }

        function zoomToPoint(lat, lng) {
            var ll = new google.maps.LatLng(lat, lng);
            map.setCenter(ll);
            if (map.getZoom() < 19) {
                map.setZoom(19);
            }
        }

        function cleanHistoryPoints() {
            while (historyPoints.length > 0) {
                //for (x in historyPoints) {
                var pt = historyPoints.pop();
                pt.setMap(null);
            }

            while (visitedParcels.length > 0) {
                var pt = visitedParcels.pop();
                pt.setMap(null);
            }

            if (historyPoints.length > 0) {
                alert('Previous map points are remaining, please refresh browser to avoid any unexpected views.');
            }
        }

        function uhl() {
            try {

                $('#points').html('');
                setTimeout(function () { getHistoryLayer(); }, 400);
                //getHistoryLayer();
            }
            catch (e) {
                console.error(e);
                console.error(e);
            }
            return false;
        }

        function historyAgentChange() {
            $('#points').html('');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftContent" runat="Server">
	<div class="tracking-tabs">
		<ul>
			<li class="tip" tip-margin-top="35" abbr="While the MobileAssessor application is open, and is the active <br/> application on the field device, GPS locations are recorded.<br/>Appraiser’s current location, and historical locations are stored."><a href="#tracking">Tracking</a></li>
			<li class="tip" tip-margin-top="35" abbr="Accessing the History tab will show historical locations stored at the <br/> appraiser’s location at the time a property was marked as complete.<br/> Each pin on the map corresponds to list of the left of the map view.<br/> Time between each mark as complete is calculated and displayed in gray.<br/> The From and To dates must be within a 60 day period."><a href="#history">History</a></li>
		</ul>
		<div id="tracking">
			<fieldset>
				<legend class="tip" div-width="300" abbr="This tab will show users that logged in to the system today. Select an individual, or multiple users to see their last recorded location. If a user has gone offline due to no connectivity this will display their last location in online mode with connectivity.">Current Field Activity</legend>
				<div class="user-list-a">
					<div style="margin-top: 10px;">
						<asp:UpdatePanel runat="server">
							<ContentTemplate>
								<asp:GridView runat="server" ID="gvUsers" Width="100%" SkinID="PlainList" ShowHeader="false">
									<Columns>
										<asp:TemplateField>
											<ItemTemplate>
												<div class='track-user' loginid='<%# Eval("LoginId") %>' latitude='<%# Eval("Latitude") %>'
													longitude='<%# Eval("Longitude") %>' accuracy='<%# Eval("Accuracy") %>'>
													<asp:HiddenField runat="server" ID="user" Value='<%# Eval("LoginID") %>' />
                                                    <asp:CheckBox runat="server" ID="chk" onclick= "locateUser(this)" />
													<label onclick='locateUser(this)'><%# Eval("FullName")%> </label>
                                                    <span><%# Eval("LastTime", "{0:h\:mm tt}")%></span>
												</div>
											</ItemTemplate>
										</asp:TemplateField>
									</Columns>
									<EmptyDataTemplate>
										<p>
											No field activity yet today.</p>
									</EmptyDataTemplate>
								</asp:GridView>
								<p style="text-align: center;">
									<asp:Button runat="server" ID="btnRefresh" Text="Refresh" />&nbsp;
									<button onclick="centerTracking();return false;">
										Reset View</button>
								</p>
							</ContentTemplate>
						</asp:UpdatePanel>
					</div>
				</div>
			</fieldset>
			<div class="track-update-time c" style="font-weight: bold; margin-bottom: 50px;">
			</div>
		</div>
		<div id="history">
			<fieldset class="filters" style = "padding-right: 10px;">
				<legend>Search Filters:</legend>
				<label>
					Agent:</label>
				<select class="history-agent" onchange="historyAgentChange()">
					<option value="">-- Select Agent --</option>
					<asp:Repeater runat="server" ID="rpUsers">
						<ItemTemplate>
							<option value='<%# Eval("LoginId") %>'>
								<%# Eval("FullName")%></option>
						</ItemTemplate>
					</asp:Repeater>
				</select>
				<label class = "tip" style="white-space:nowrap;" abbr = "The From and To dates must be within a 60 day period." >
					Start Date:</label>
				<input class="datetime" style="display: block;" id="startdate" />
				<label class = "tip" style="white-space:nowrap;" abbr = "The From and To dates must be within a 60 day period." >
					End Date:</label>
				<input class="datetime" style="display: block;" id="enddate" /> 
		    
			 <input type="checkbox" id="mactoggle" style="float: left;width: 15px; height: 15px; cursor: pointer; margin-left: 0px;margin-top: 7px;"/>
			   <label class = "tip" style="white-space:nowrap;" abbr = "By default, the Field Tracking location shows the pings that were tracked every<br/>few minutes as the appraiser used MobileAssessor. Select this option to only<br/>show the pings on the map that indicate where the appraiser was at the time<br/>the property was marked as complete." style="margin-left: 15px;">Show Location of Mark As Complete</label> 
			 
			</fieldset>
			<div style="text-align: left; padding: 5px;">
				<button style="padding: 4px 20px;" onclick="return uhl();" class="button-history">
					Get Route History</button>
			</div>
			<div id="history-results">
				<div>
					<div id="time-range">
					</div>
					<div class="points-display">
						<table style="width: 100%;" id="points">
						</table>
					</div>
					<table style="width: 100%; display: none;" id="points-template">
						<tr>
							<td style="width: 20px;">${i}.</td>
							<td style="width: 75px;">${firstdate}</td>
							<td style="width: auto;"><a onclick="zoomToPoint(${lat}, ${lng})">${time}</a> </td>
							<td style="width: 60px; color: Gray; font-style: italic;">${wait}</td>
						</tr>
					</table>
					</div>
				<div>
					<div class="pointsMac-display" style="height: 464px; overflow: auto; display: block;">
						<table style="width: 100%;" id="pointsMac">
						</table>
					</div>
					<table style="width: 100%; display: none;" id="pointsMac-template">
						<tr>
							<td style="width: 20px;">${id}.</td>
							<td style="width: 75px;">${assrno}</td>
							<td style="width: auto;"><a onclick="zoomToPoint(${lat}, ${lng})">${time}</a> </td>
							<td style="width: 60px; color: Gray; font-style: italic;">${date}</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="map" style ="height:window.height">
	</div>
</asp:Content>
