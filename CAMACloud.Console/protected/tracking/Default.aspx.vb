﻿
Partial Class tracking_Default
	Inherits System.Web.UI.Page

	Sub LoadUsers()
		'Dim uc = Membership.GetAllUsers
		'Dim users(uc.Count - 1) As MembershipUser
		'Membership.GetAllUsers().CopyTo(users, 0)
		'gvUsers.DataSource = users.Where(Function(x) Roles.IsUserInRole(x.UserName, "MobileAssessor"))
		'gvUsers.DataBind()
		Dim sql As String = "SELECT utc.LoginId, COALESCE(us.FirstName + COALESCE(' ' + us.LastName, ''), utc.LoginId) As FullName,Latitude, Longitude, Accuracy, dbo.GetLocalDate(LastUpdated) As LastTime FROM UserTrackingCurrent utc LEFT OUTER JOIN UserSettings us ON utc.LoginId = us.LoginId WHERE dbo.GetLocalDate(LastUpdated) > CAST(dbo.GetLocalDate(GETUTCDATE()) AS DATE) AND us.LoginId NOT IN ('admin', 'support', 'dcs-qa','dcs-support','dcs-ps') AND (us.DoNotTrack <> 1 OR us.DoNotTrack IS NULL )GROUP BY utc.LoginId, COALESCE(us.FirstName + COALESCE(' ' + us.LastName, ''), utc.LoginId),Latitude, Longitude, Accuracy,dbo.GetLocalDate(LastUpdated)ORDER BY FullName"
		gvUsers.DataSource = Database.Tenant.GetDataTable(sql)
		gvUsers.DataBind()
	End Sub

	Sub LoadHistoryUsers()
		Dim sql As String = "SELECT utc.LoginId, COALESCE(us.FirstName + COALESCE(' ' + us.LastName, ''), utc.LoginId) As FullName FROM UserTrackingCurrent utc LEFT OUTER JOIN UserSettings us ON utc.LoginId = us.LoginId  WHERE us.LoginId NOT IN ('admin', 'support', 'dcs-qa','dcs-support','dcs-ps') AND (us.DoNotTrack <> 1 OR us.DoNotTrack IS NULL )GROUP BY utc.LoginId, COALESCE(us.FirstName + COALESCE(' ' + us.LastName, ''), utc.LoginId) ORDER BY FullName"
		rpUsers.DataSource = Database.Tenant.GetDataTable(sql)
        rpUsers.DataBind()
    End Sub

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
			LoadUsers()
			LoadHistoryUsers()
		End If
	End Sub

	Protected Sub btnRefresh_Click(sender As Object, e As System.EventArgs) Handles btnRefresh.Click
		Dim selectedUsers As New List(Of String)
		For Each gr As GridViewRow In gvUsers.Rows
			If gr.GetChecked("chk") Then selectedUsers.Add(gr.GetHiddenValue("user"))
		Next
		LoadUsers()
		For Each gr As GridViewRow In gvUsers.Rows
			If selectedUsers.Contains(gr.GetHiddenValue("user")) Then gr.GetControl(Of CheckBox)("chk").Checked = True
		Next
		RunScript("utl();")
	End Sub

End Class
