﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Default.aspx.vb" Inherits="CAMACloud.Console._Default6" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CAMA Cloud - View Sketch</title>

    <link rel="stylesheet" type="text/css" href="/App_Static/css/view-sketch.css?12345678" />
    <script type="text/javascript" src="/App_Static/jslib/jquery.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/jsrepeater.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/underscore-min.js?123456"></script>
    <script type="text/javascript" src="/App_Static/js/viewSketch/01-viewSketch.js?1234567891"></script>
    <script type="text/javascript" src="/App_Static/js/viewSketch/14-ccsketcheditor.js?1234568"></script>
    <script type="text/javascript" src="/App_Static/js/qc/010-constants.js?12345"></script>
    <script type="text/javascript" src="/App_Static/js/qc/000-objects.js?123456"></script>
    <script type="text/javascript" src="/App_Static/js/qc/02-app-localdb.js?12345"></script>
    <script type="text/javascript" src="/App_Static/jslib/hammer.js"></script>
    <cc:JavaScript runat="server" IncludeFolder="~/App_Static/js/sketch/sketchlib/" />
    <script type="text/javascript">
        $(document).ready(function () {
            initApplication();
        });
       
    </script>
    <style>
		::-webkit-scrollbar
		{
		width:12px;
		height:8px;
		}
		
		::-webkit-scrollbar-button
		{
		background:#;
		height:8px;
		background:#ABABAB;
		}
		
		::-webkit-scrollbar-track-piece
		{
		background:#DDDDEE;
		}
		
		::-webkit-scrollbar-thumb
		{
		background:#999999;
		}
		@media only screen and (min-height:500px){
		.left-panel{
		height:100vh;
		}
		}
		
    
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table id="contentTable" class="min-width">
                <tr>
                    <td class="left-panel hideonprint" >
                        <div class="left-content-area">
                            <div class="sketch-left-content filter-panel">
                                <div class="sketch-filter-header">Search Parcels w/ Sketch Edits</div>
                                <div class="filter">
                                    <div class="filter-field">
                                        <span>Assignment Group:</span>
                                        <div class="assignmentGroup"></div>
                                    </div>
                                    <div class="filter-field">
                                        <span>DTR Status:</span>
                                        <div class="dtrStatusType"></div>
                                    </div>
                                    <div class="filter-field">
                                        <span>Reviewed By:</span>
                                        <div class="reviewedBy"></div>
                                    </div>
                                    <div class="filter-field">
                                        <span>Reviewed Between:</span>
                                        <div class="reviewedOn">
                                            <input type="date" id="reviewedFrom" class='fromDate' style='width: 46%; float: left' />
                                            <input type="date" id="reviewedTo" class="toDate" style='width: 46%; margin-left: 5px' />
                                        </div>
                                    </div>
                                    <div>
                                        <input type="checkbox" id="includeReviewed" />&nbsp;<span>Show Completed Sketch edits as well</span>
                                    </div>
                                    <div style="text-align: center; color: #808080; padding: 6px;">OR</div>
                                    <div class="filter-field">
                                        <span>Search using Parcel Number</span>
                                        <div class="parcelNo">
                                            <input id="parcelNo" type="text" placeholder="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="c">
                                    <button accesskey="F" type="button" class="searchParcel" onclick="return searchParcels()" id="btnSrch" style="">Search</button>
                                </div>

                            </div>
                            <div class="sketch-left-content parcel-list" style="display: none;"></div>
                        </div>
                    </td>
                    <td style="padding: 0px;">
                        <div class="control-panel" style="display: none; width: 1040px;">
                            <table style="width: 100%;" class="parcel-data">
                                <tr>
                                    <td style="padding-left: 8px;">
                                        <span class="parcel-key-value" field="KeyValue1"></span>
                                    </td>
                                    <td class="button-bar">
                                        <button class="control-action navigation prev" accesskey="P">Previous</button>
                                        <button class="control-action navigation next" accesskey="N">Next</button>
                                        <button class="control-action markAsReview" accesskey=" "></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table style="width: 100%;" class="data-fields">
                                            <tr>
                                                <td style="padding-left: 5px;"><span field="StreetAddress"></span></td>
                                                <td>Group: <span field="AssignmentGroup"></span></td>
                                                <td>Status: <span field="DTRStatus"></span></td>
                                                <td>Reviewed By: <span field="ReviewedUser"></span> on <span field="ReviewedDate"></span></td>
                                                <td>Approved By: <span field="QCUser"></span></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 8px;">
                                        <div class="ccse-other-tools">
                                            <span>Select Sketch</span>
                                            <select class="select-sketch-segments" onchange="bindSketchSegment()" style="width: 140px; margin-right: 30px;">
                                            </select>
                                            <span class="deletedmsg b" style="color: red; display: none;">1 or more vectors have been deleted</span>
                                        </div>

                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                            <div class="button-bar">
                            </div>
                            <div class="parcel-details">
                            </div>
                        </div>

                        <div class="sketch-panel" style="display: none;">
                        </div>
                        <div style="display: none;">
                            <canvas height="300" width="300" class="ccse-img"></canvas>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="masklayer" style="display: none; position: fixed; z-index: 20">
            <span style="margin-left: 47%; margin-top: 22%; float: left">
                <img src="/App_Static/css/images/comploader.gif" />
                <span style='color: wheat; float: right; margin-left: 12px; margin-top: 20px;' class="app-mask-layer-info">Please wait...</span>
            </span>
        </div>


    </form>
</body>
</html>
