﻿Imports System.IO
Imports System.Drawing
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Web.Services


Public Class settingsaudittrailreport
    Inherits System.Web.UI.Page

    Public name As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then

            name = "" + HttpContext.Current.GetCAMASession.OrganizationName + " - Setting Audit Trail"
            RunScript("OrgName('" + name + "');")
            Dim dummy As DataTable = New DataTable()
            dummy.Columns.Add("Id")
            dummy.Columns.Add("EventTime")
            dummy.Columns.Add("LoginId")
            dummy.Columns.Add("Description")

            dummy.Rows.Add()
            gvSettingsAuditTrail.DataSource = dummy
            gvSettingsAuditTrail.DataBind()


            'Required for jQuery DataTables to work.
            gvSettingsAuditTrail.UseAccessibleHeader = True
            gvSettingsAuditTrail.HeaderRow.TableSection = TableRowSection.TableHeader

        End If
    End Sub


    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub


    Public Class satReportRecord

        Public Property Id As Integer
        Public Property EventTime As DateTime
        Public Property LoginId As String
        Public Property Description As String


    End Class



    <WebMethod>
    Public Shared Function GetsatReport() As List(Of satReportRecord)
        Dim settingaudittrailrecord As List(Of satReportRecord) = New List(Of satReportRecord)()


        Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT Id,  Format(dbo.GetLocalDate(EventTime),'MM-dd-yyyy hh:mm:ss tt') AS EventTime, LoginId, Description FROM SettingsAuditTrail")

        For Each q In dt.Rows

            settingaudittrailrecord.Add(New satReportRecord With {
                            .Id = q("Id"),
                            .EventTime = q("EventTime"),
                            .LoginId = q("LoginId").ToString(),
                            .Description = q("Description").ToString()
                        })
        Next



        Return settingaudittrailrecord
    End Function


End Class