﻿Public Class querytester
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    	If Not IsPostBack Then
    		refersh()
    	End If
    End Sub
    
    Private Sub btnExecute_Click(sender As Object, e As System.EventArgs) Handles btnExecute.Click
    	grid.PageIndex = 0
    	BindGrid()
    	RunScript("maskQuery(false)")
    End Sub
    
    Private Sub btnClear_Click(sender As Object, e As System.EventArgs) Handles btnClear.Click
    	refersh()
    	txtQuery.Text = ""
    End Sub
       
    Private Sub refersh()
    	grid.DataSource = Nothing
    	grid.DataBind()
    	'txtQuery.Text = ""
    	pnlResult.Visible = False
    End Sub
    
    Private Sub BindGrid()
    	Dim query As String = txtQuery.Text
    	
    	  'Checks the query is EMPTY or NOT.
        If query = Nothing OrElse query.Trim() = "" Then
    		refersh()
    		Return
        End If
     
		'Vishnu VS -  (fd - 11904) **************************************************************** 
		
		Dim replacedquery As String = query
		Dim QuoteText As String  = ""
		Dim Sflag as Boolean = False,Eflag = False
		Dim tmpList As New List(Of String)
		
		For Each c As Char in replacedquery
			If c.ToString() = "'" And Not Sflag Then
				Sflag =True
			    QuoteText ="'"
			    Continue For
		    End If
			
			If c.ToString() = "'" And Sflag Then
				Eflag = True
				QuoteText +="'"
			End If
			
			If Sflag And Not Eflag Then
				QuoteText += c.ToString()
			End If
				
			If Eflag Then
					If QuoteText.ToUpper().Contains("FROM") Then
						tmpList.Add(QuoteText)	
					End If
					
					Sflag = False 
					Eflag=False 
			End If
		Next
		
		For Each value As String In tmpList.ToArray
        	replacedquery = replacedquery.Replace(value,"'test'")
		Next
		
		'Vishnu VS *********************************************************
		
        Dim tableSet As String = ""
      
        'Checks the query contains FROM clause
        Dim pattern = "\b" + Regex.Escape("from") + "\s+(\w+)"
 		For Each match As Match In Regex.Matches(replacedquery, pattern, RegexOptions.IgnoreCase)
    		For Each c As Capture In match.Captures
    			tableSet +=  c.Value + ","
    		Next
		Next
		'Checks the query contains JOIN clause
		pattern = "\b" + Regex.Escape("join") + "\s+(\w+)"
		For Each match As Match In Regex.Matches(query, pattern, RegexOptions.IgnoreCase)
    		For Each c As Capture In match.Captures
    			tableSet +=  c.Value + ","
    		Next
    	Next
    	'Checks the query contains any TABLE NAME
    	If tableSet.Length = 0 Then
    		Alert("Invalid Query")
    		refersh()
    		Return
    	End If
    	'Last , in tableSet and more than two spaces in tableSet are removed
    	tableSet = tableSet.Substring(0,tableSet.Length-1)
    	tableSet = Regex.Replace(tableSet, "\s{2,}", " ")
    	
    	Dim tables As String() = Regex.Split(tableSet, "\,")
		Dim targetTblName As String = ""
		Dim tblName As String()
		Dim subQuery As String=""
		Dim AliasTable As String()
		Dim AliasPattern As String=""
		Dim AliasName As String=""
		Dim AliasTblName As String()
		Dim flag As Integer=0
		Dim AliasPattern2 As String=""
		
		Try
			For Each table As String In tables
                tblName = table.Split(" ")
                targetTblName = Database.Tenant.GetStringValue("SELECT CC_TargetTable FROM DataSourceTable WHERE Name = '"+tblName(1)+"'")
                'for using AS in Alias
                targetTblName = " " + targetTblName
                AliasPattern=tblName(1)+ "\s+as\s+(\w+)"
                Dim ma As Match=Regex.Match(query,AliasPattern,RegexOptions.IgnoreCase)
                If ma.Success Then
                	For Each c As Capture In ma.Captures
                	    AliasName+= c.Value + ","
                	Next
                	AliasName = AliasName.Substring(0,AliasName.Length-1)
                	AliasTable=Regex.Split(AliasName, "\,")
                	     AliasTblName=AliasName.Split(" ")
                	     AliasName=AliasTblName(2)
                	     flag=1
                End If
                'for without using AS in Alias
                AliasPattern2="\b"+tblName(1)+"\s+(?!CONSTRAINT|FOR|FROM|GROUP BY|HAVING|ORDER BY|fetch first|USING|WHERE|WHERE CURRENT OF|AS|UNION|UNION ALL)\w+\b"
                Dim ma2 As Match =Regex.Match(query,AliasPattern2,RegexOptions.IgnoreCase)
                If ma2.Success Then
                	For Each c As Capture In ma2.Captures
                		AliasName+=c.Value + ","
                	Next
                	AliasName = AliasName.Substring(0,AliasName.Length-1)
                	AliasTable=Regex.Split(AliasName, "\,")
                	AliasTblName=AliasName.Split(" ")
                	AliasName=AliasTblName(1)
                	flag=1
                End If
                'End of  without using AS in Alias

                'Check whether query contain "SELECT * " in it and code change for results order change
                Dim m As Match = Regex.Match(query, "select\s*\*", RegexOptions.IgnoreCase)
                Dim t As Match = Regex.Match(query, "select\s*top\s*[0-9]*\s*[*]", RegexOptions.IgnoreCase)
                If (m.Success Or t.Success) And targetTblName <> Nothing Then
                    Dim datavalues As String = ""
                    Dim fields As New List(Of String)
                    Dim tempTable As DataTable = Database.Tenant.GetDataTable("select	df.Name,ds.IsPrimaryKey,ds.IsReferenceKey from	DataSourceField df left join	DataSourceKeys ds ON	ds.FieldId = df.Id and df.TableId = ds.TableId where	df.SourceTable ='" + tblName(1) + "'")
                    Dim importType As Integer = Database.Tenant.GetIntegerValue("select ImportType from DataSourceTable where Name='" + tblName(1) + "'")
                    Dim primaryField As DataRow() = tempTable.Select("IsPrimaryKey = 1")
                    For Each dr As DataRow In primaryField
                        If (flag = 1) Then
                            If Not fields.Contains(AliasName + ".[" + dr(0).ToString() + "]") Then
                                fields.Add(AliasName + ".[" + dr(0).ToString() + "]")
                            End If
                        End If
                        If (flag = 0) Then
                            If Not fields.Contains(targetTblName + ".[" + dr(0).ToString() + "]") Then
                                fields.Add(targetTblName + ".[" + dr(0).ToString() + "]")
                            End If
                        End If
                    Next
                    Dim referenceField As DataRow() = tempTable.Select("IsReferenceKey = 1")
                    For Each dr As DataRow In referenceField
                        If (flag = 1) Then
                            If Not fields.Contains(AliasName + ".[" + dr(0).ToString() + "]") Then
                                fields.Add(AliasName + ".[" + dr(0).ToString() + "]")
                            End If
                        End If
                        If (flag = 0) Then
                            If Not fields.Contains(targetTblName + ".[" + dr(0).ToString() + "]") Then
                                fields.Add(targetTblName + ".[" + dr(0).ToString() + "]")
                            End If
                        End If

                    Next
                    For Each dr As DataRow In tempTable.Select("IsReferenceKey  is null OR IsPrimaryKey is null")                    	
                        If (flag = 1) Then
                            datavalues += ", " + AliasName + ".[" + dr(0).ToString() + "]"
                        End If
                        If (flag = 0) Then
                            datavalues += ", " + targetTblName + ".[" + dr(0).ToString() + "]"
                        End If
                    Next
                    
                   
                    If subQuery <> "" Then
                        subQuery += ", "
                    End If
               
                    If (importType = 0 Or importType = 1) Then
                        If (flag = 1) Then
                            subQuery += AliasName + ".ROWUID, " + AliasName + ".ParentROWUID, " + AliasName + ".ClientROWUID, " + AliasName + ".ClientParentROWUID, " + AliasName + ".CC_ParcelId, " + AliasName + ".CC_LastUpdateTime, " + AliasName + ".CC_Deleted"
                        End If
                        If (flag = 0) Then
                            subQuery += targetTblName + ".ROWUID, " + targetTblName + ".ParentROWUID, " + targetTblName + ".ClientROWUID, " + targetTblName + ".ClientParentROWUID, " + targetTblName + ".CC_ParcelId, " + targetTblName + ".CC_LastUpdateTime, " + targetTblName + ".CC_Deleted"
                        End If
                    End If
                    If (importType = 3) Then
                        If (flag = 1) Then
                            subQuery += AliasName + ".ROWUID, " + AliasName + ".ParentROWUID, " + AliasName + ".ClientROWUID, " + AliasName + ".ClientParentROWUID, " + AliasName + ".CC_LastUpdateTime, " + AliasName + ".CC_Deleted"
                        End If
                        If (flag = 0) Then
                            subQuery += targetTblName + ".ROWUID, " + targetTblName + ".ParentROWUID, " + targetTblName + ".ClientROWUID, " + targetTblName + ".ClientParentROWUID, " + targetTblName + ".CC_LastUpdateTime, " + targetTblName + ".CC_Deleted"
                        End If
                    End If
                    If (importType = 5) Then
                        If (flag = 1) Then
                            subQuery += AliasName + ".ROWUID, " + AliasName + ".CC_LastUpdateTime, " + AliasName + ".CC_Deleted"
                        End If
                        If (flag = 0) Then
                            subQuery += targetTblName + ".ROWUID, " + targetTblName + ".CC_LastUpdateTime, " + targetTblName + ".CC_Deleted"
                        End If
                    End If
                    subQuery += IIf(fields.Count = 0, "", "," + String.Join(",", fields)) + datavalues
                End If

                If tblName(1).ToLower() = "mappoints" Then
                	targetTblName = "ParcelMapPoints"
                End If
                
                If targetTblName = Nothing Or targetTblName = "" Or targetTblName = " " Then
                	If tblName(1).ToLower() = "audittrail" Then
                		Alert("You're not allowed to run Audit Trail queries in Query Analyzer.") ''12698 disabled querying in Audit Trail
                		refersh()
						Return
                		targetTblName = "ParcelAuditTrail"
                	Else
                		Alert("Please check the table name:  "+ tblName(1))
                		refersh()
						Return
                	End If
                End If
                'Table name given in the query is replaced with TARGET TABLE NAME
               pattern= "\s" + tblName(1) + "\b"
            	For Each mstring As Match In Regex.Matches(query,pattern,RegexOptions.IgnoreCase)
            		query=Regex.Replace(query,pattern,targetTblName)
            	Next
                ' * in query is replaced with the formed subQuery
                Dim match As Match = Regex.Match(query, "select\s*\*\s*from", RegexOptions.IgnoreCase)
                Dim top_match As Match = Regex.Match(query, "select\s*top\s*[0-9]*\s*[*]", RegexOptions.IgnoreCase)
                If match.Success Or top_match.Success Then
                    query = Replace(query, "*", " " + subQuery,, 1)
                End If
                subQuery = ""
			Next
			
		Catch ex As Exception
			Alert("Please check the query:" + ex.Message)
            refersh()
			Return
		End Try
		
		'Chr(39)-singleQuote & Chr(34)-doubleQuote and \\ replaced by +
		query = Regex.Replace(query,"\|\|","+")
        'query = Regex.Replace(query,Chr(34),Chr(39))

        'checks TOP clause in query
        Dim topMatch As Match = Regex.Match(query,"\bTOP\b",RegexOptions.IgnoreCase)
		Dim distinctMatch As Match = Regex.Match(query,"\bDISTINCT\b",RegexOptions.IgnoreCase)
		If Not topMatch.Success AndAlso Not distinctMatch.Success Then
			query = Regex.Replace(query, "^(.*?)\bselect\b", "select TOP 75000 ",RegexOptions.IgnoreCase)
		End If
		
		Dim dt As DataTable = New DataTable()
        Try
        	dt = Database.Tenant.GetDataTable(query)
        	'Checks the record is EMPTY or NOT
        	If dt.Rows.Count=0 Then
        		RunScript("setTimeout(function(){alert('No Records found in this table');}, 1000);")
        		''Alert("No Records found in this table")
        		refersh()
        		Return
        	End If
        	
        Catch ex As Exception 
        	Dim newstring1 As String ()
        	Dim newstring As String = ex.Message.ToString
        	newstring = newstring.Replace(query,"")
            newstring1 = newstring.Split("#")
            If newstring1.Length > 1 Then
                Alert("Invalid Query: " + newstring1(1))
            Else
                Alert("Invalid Query : " + txtQuery.Text)
            End If
            refersh()
            Return

        End Try

        If dt.Rows.Count > 0 Then
            '---To set default value for boolean columns (otherwise it shows false for null value column)
            Dim drn = Nothing
            Dim dtCloned As DataTable = dt.Clone()
            For Each col As DataColumn In dt.Columns
                Dim Datatype As String = col.DataType.ToString
                dtCloned.Columns(col.ColumnName).DataType = GetType(String)
                If col.ColumnName = "CC_ParcelId" Then
                    drn = (From auxTable In dt.AsEnumerable
                           Where (auxTable.Field(Of Integer?)("CC_ParcelId") <> -99 Or IsDBNull(auxTable("CC_ParcelId")))
                           Select auxTable).ToList
                End If
            Next
            If drn Is Nothing Then
                drn = dt.Rows
            End If

            For Each row As DataRow In drn
                dtCloned.ImportRow(row)
            Next
            For Each cols As DataColumn In dt.Columns
                Dim Datatype As String = cols.DataType.ToString
                For Each row As DataRow In dtCloned.Rows
                    Dim val = row(cols.ColumnName)
                    If IsDBNull(val) Then
                        row(cols.ColumnName) = "NULL"
                    End If
                Next
            Next
            'End of ---
            If dtCloned.Rows.Count=0 Then
            	RunScript("setTimeout(function(){alert('No Records found in this table');}, 1000);")
                ''Alert("No Records found in this table")
        		refersh()
        		Return
        	End If
            ViewState("dtData") = query
            pnlResult.Visible = True
            grid.AutoGenerateColumns = True
            grid.AllowPaging = True
            grid.DataSource = dtCloned
            grid.DataBind()
            PageCountQueryAnalyzer.InnerHtml = "Displaying records {0} to {1} of {2}".FormatString(grid.PageIndex * grid.PageSize + 1, grid.PageIndex * grid.PageSize + grid.Rows.Count, dtCloned.Rows.Count)
            grid.CssClass = "gridView"
        Else
            pnlResult.Visible = False
        End If
        Try
        	'
	        For Each row As GridViewRow In grid.Rows
	            For i As Integer = 0 To row.Cells.Count - 1
	                If row.Cells(i).HasControls = True Then
	                    For n As Integer = 0 To row.Cells(i).Controls.Count - 1
	                        If row.Cells(i).Controls(n).GetType() Is GetType(CheckBox) Then
	                            Dim cb As CheckBox = row.Cells(i).Controls(n)
	                            row.Cells(i).Controls.RemoveAt(n)
	                            row.Cells(i).Text = cb.Checked.ToString()
	                        End If
	                    Next
	                End If
	            Next
	        Next
	    Catch ex As Exception
			If(ex.Message.Contains("TimeOut") Or ex.Message.Contains("Memory"))
				Alert(ex.Message)
				refersh()
				Return
			End If
		End Try
	End Sub
    Protected Sub OnPageIndexChanging(sender As Object, e As GridViewPageEventArgs)
    	grid.PageIndex = e.NewPageIndex
    	BindGrid()
	End Sub

    Protected Sub grid_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grid.RowDataBound
    	If e.Row.RowType = DataControlRowType.DataRow Then
    			For c As Integer =0 To e.Row.Cells.Count-1
    				e.Row.Cells(c).Text = e.Row.Cells(c).Text.Replace(" ", "&nbsp;")
    			Next
        End If

    End Sub

    Public Function ExportGridView(ByVal dt As DataTable, Optional ByVal formatColumn As String = Nothing, Optional ByVal format As String = Nothing) As GridView
        Dim GridView1 As New GridView()
        GridView1.AllowPaging = False
        GridView1.AutoGenerateColumns = False
        Dim bfieldName As String
        Dim bHeaderText As String
        GridView1.Columns.Clear()
        Dim dr = Nothing
        Dim dtnew As DataTable = dt.Clone()
        For Each column As DataColumn In dt.Columns
            bfieldName = column.ColumnName
            bHeaderText = column.ColumnName
            Dim bfield As New BoundField()
            bfield.HeaderText = bHeaderText
            bfield.DataField = bfieldName
            If (column.ColumnName = formatColumn) Then
                bfield.DataFormatString = format
            End If
            GridView1.Columns.Add(bfield)
            If column.ColumnName = "CC_ParcelId" Then
                dr = (From auxTable In dt.AsEnumerable
                      Where IsDBNull(auxTable("CC_ParcelId")) Or auxTable.Field(Of Integer?)("CC_ParcelId") <> -99
                      Select auxTable).ToList
            End If
        Next
        If dr Is Nothing Then
            dr = dt.Rows
        End If
        For Each row As DataRow In dr
            dtnew.ImportRow(row)
        Next
        GridView1.DataSource = Nothing
        GridView1.DataSource = dtnew
        GridView1.DataBind()
        Return GridView1
    End Function

    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
      Dim dt As DataTable = Database.Tenant.GetDataTable(ViewState("dtData"))


        Dim GridView1 As GridView = ExportGridView(dt)
        ExportToCSV(GridView1, "resultsquery.csv")
    End Sub

    Protected Sub ExportToCSV(ByVal GridView1 As GridView, ByVal filename As String)
        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("Content-Disposition", "attachment;filename=" + filename)
        Response.Charset = ""
        Response.ContentType = "application/text"
        Dim sBuilder As StringBuilder = New System.Text.StringBuilder()
        For index As Integer = 0 To GridView1.Columns.Count - 1
            sBuilder.Append(GridView1.Columns(index).HeaderText + ","c)
        'Response.Output.Write(GridView1.Columns(index).HeaderText +","c)
        Next
        sBuilder.Append(vbCr & vbLf)
      ' Response.Output.write(vbCr & vbLf)
        Dim QUOTE As String = Convert.ToChar(&H22)
        For i As Integer = 0 To GridView1.Rows.Count - 1
        	For k As Integer = 0 To GridView1.HeaderRow.Cells.Count - 1
                'If GridView1.Rows(i).Cells(k).Text.Contains(",") Then
                '    Dim CellText As String = GridView1.Rows(i).Cells(k).Text.Replace("&nbsp;", "").Replace(vbCr, "").Replace(vbLf, "").Replace("&quot;", Chr(34)).Replace("&#39;", Chr(39)).Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">")
                '    CellText = QUOTE + CellText + QUOTE
                '    sBuilder.Append(CellText + ",")
                'Else
                '    sBuilder.Append(GridView1.Rows(i).Cells(k).Text.Replace("&nbsp;", "").Replace(vbCr, "").Replace(vbLf, "").Replace("&quot;", Chr(34)).Replace("&#39;", Chr(39)).Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">") + ",")
                'End If

                '''''
                Dim CellText As String = GridView1.Rows(i).Cells(k).Text.Replace("&nbsp;", "").Replace(vbCr, "").Replace(vbLf, "").Replace("&quot;", Chr(34) + Chr(34)).Replace("&#39;", Chr(39)).Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">")
                CellText = QUOTE + CellText + QUOTE
                'Response.Output.Write(CellText + ",")
                sBuilder.Append(CellText + ",")
            Next
            sBuilder.Append(vbCr & vbLf)
              'Response.Output.Write(vbCr & vbLf)
        Next
        Response.Output.Write(sBuilder.ToString())
        Response.Flush()
        Response.[End]()
    End Sub
    
    Protected Sub ddlPageSize_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
    	grid.PageSize = ddlPageSize.SelectedValue
    	grid.PageIndex = 0
    	BindGrid()
    End Sub
End Class