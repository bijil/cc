﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="photodownload.aspx.vb" Inherits="CAMACloud.Console.photodownload" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .group-title {
            font-weight: bold;
            margin-top: 12px;
            margin-bottom: 8px;
        }

        .option-box {
            margin: 8px;
            margin-left: 30px;
        }
        .spaced input[type="radio"] {
            margin-left: 50px; /* Or any other value */
        }
        .info {
            width: 850px;
            border-left: 8px solid #CFCFCF;
            padding-left: 10px;
            line-height: 15pt;
            padding-top: 5px;
            padding-bottom: 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<h1>Download Photos from Cloud</h1>
	<table>
        <tr>
            <td style="width: 150px">Total photos in cloud: </td>
            <td style="font-weight: bold;">
                <asp:Label runat="server" ID="lblPhotoCount" /></td>
        </tr>
    </table>
 	<asp:Panel runat="server" ID="pnlInitDownload">
        <div class="group-title">Select criteria:</div>
         <asp:HiddenField runat="server" ID="hdnProcessID" />
        <asp:RadioButtonList runat="server" ID="rbPhotos" AutoPostBack="true" RepeatDirection="Horizontal" CssClass="spaced">
            <asp:ListItem Value="ALL" Text="All Photos" Selected="True" />
            <asp:ListItem Value="AMF" Text="All Main/First Photos" />
            <asp:ListItem Value="FIL" Text="Based on filter condition" />
        </asp:RadioButtonList>

        <asp:Panel runat="server" ID="pnlPhotoOps" Class="info" CssClass="option-box info" Visible="false">
            <table>
                <tr>
                    <td style="width: 220px">
                        <asp:CheckBox runat="server" ID="chkDateBetween" Text="Captured/uploaded between:" AutoPostBack="true" />
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="dtFrom" Width="130px" type="date" Enabled="false" />
                    </td>
                    <td>
                        <asp:RequiredFieldValidator runat="server" ID="rfvDtF" ControlToValidate="dtFrom" ErrorMessage="*" Enabled="false" />&nbsp;and&nbsp;&nbsp;
                    <asp:TextBox runat="server" ID="dtTo" Width="130px" type="date" Enabled="false" />
                        <asp:RequiredFieldValidator runat="server" ID="rfvDtT" ControlToValidate="dtTo" ErrorMessage="*" Enabled="false" />
                    </td>
                </tr>
                <asp:Repeater runat="server" ID="rpMetaFields">
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:CheckBox runat="server" ID="chkMetaOption" Text='<%# "MetaData: " + Eval("DisplayLabel")%>' AutoPostBack="true" OnCheckedChanged="chkMetaOption_CheckedChanged" />
                                <asp:HiddenField runat="server" ID="hdMetaName" Value='<%# Eval("AssignedName").Replace("PhotoMetaField", "MetaData") %>' />
                            </td>
                            <td>
                                <asp:DropDownList runat="server" ID="ddlMetaOption" AutoPostBack="true" Width="136px" Enabled="false" >
                                    <asp:ListItem Value="EQ" Text="Equal To" />
                                    <asp:ListItem Value="NE" Text="Not Equal To" />
                                    <asp:ListItem Value="SW" Text="Starting With" />
                                    <asp:ListItem Value="NSW" Text="Not Starting With" />
                                    <asp:ListItem Value="NULL" Text="Is Null Or Empty" />
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtOptionValue" Width="180px" Enabled="false" />
                                <asp:RequiredFieldValidator runat="server" ID="rfv" ControlToValidate="txtOptionValue" ErrorMessage="*" Enabled="false" />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </asp:Panel>

        <div class="group-title">Select parcels:</div>
        <asp:RadioButtonList runat="server" ID="rbParcels" AutoPostBack="true">
            <asp:ListItem Value="" Text="All Parcels" Selected="True" />
            <asp:ListItem Value="S" Text="Only parcels in list" />
        </asp:RadioButtonList>
        <asp:Panel runat="server" ID="pnlSelectParcels" CssClass="option-box" Visible="false">
            <p class="info">
               With the following header field, please upload parcel list in CSV format
                <br />
                <strong><%= KeyList() %></strong>
            </p>
            <asp:HiddenField runat="server" ID="hdHasFile" Value="0" />
            <asp:FileUpload runat="server" ID="fuParcelList" accept=".csv" Enabled="false" />
            <asp:RequiredFieldValidator runat="server" ID="rfvfu" ControlToValidate="fuParcelList" ErrorMessage="*" Enabled="false" />
            <br />
            <asp:Label runat="server" ID="lblSelectedParcels" />
        </asp:Panel>
        <div style="height: 20px"></div>

        <asp:Button runat="server" ID="btnDownload" Text="Download" Font-Bold="true" />
    </asp:Panel>
</asp:Content>
