﻿Public Class PhotoCommitBackup
    Inherits System.Web.UI.Page
	Shared dt As DataTable
    
   	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadGrid()
        End If
   	End Sub
   	
    Sub LoadGrid()
        dt = Database.Tenant.GetDataTable("Select id,dbo.GetLocalDate(UploadTime) as UploadTime,UserName,IpAddress,TotalCount,SyncStatus,InvalidRecords,Status From PhotoCommitBackup")
        gvPhotoCommitList.DataSource = dt
        gvPhotoCommitList.DataBind()

    End Sub
    
    Private Sub gvPhotoCommitList_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvPhotoCommitList.PageIndexChanging
        gvPhotoCommitList.PageIndex = e.NewPageIndex
        LoadGrid()
    End Sub

    Protected Sub gvPhotoCommitList_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvPhotoCommitList.RowCommand
        If e.CommandName = "Download" Then
            Dim Id As Integer = e.CommandArgument
            Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT InputData FROM PhotoCommitBackup  WHERE id = " & id)
            Dim FileName As String
            FileName = HttpContext.Current.GetCAMASession.OrganizationName.ToString() + "-" + "PhotoCommit.csv"
            Dim FileContent As String = dr.GetString("InputData")
            Response.Clear()
            Response.Buffer = True
            Response.AddHeader("content-disposition", "attachment;filename=" & FileName)
            Response.Charset = ""
            Response.ContentType = "application/text"
            Response.Output.Write(FileContent)
            Response.Flush()
            Response.End()
        End If
    End Sub

End Class