﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports CAMACloud.BusinessLogic
Imports System.Web.Services
Imports System.Drawing
Public Class schema_keys
    Inherits System.Web.UI.Page
    Dim dtReferenceKey As New DataTable
        Dim dtSchema As DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadSchemaTree()
            PanelDisplayContent.Visible = False
        End If
    End Sub
    Sub LoadSchemaTree()
        SchemaTables.Nodes.Clear()
        dtSchema = Database.Tenant.GetDataTable("SELECT * FROM vw_DataSourceTableTree ORDER BY TypeNo, ParentTableId, Relationship, TableName")
        LoadSchemaTableNode(SchemaTables.Nodes)
        SchemaTables.ExpandAll()
    End Sub
    Sub LoadSchemaTableNode(collection As TreeNodeCollection, Optional parentTableId As Integer = -1, Optional extraFilter As String = "")
        Dim filterString As String = "ParentTableId " + IIf(parentTableId = -1, "IS NULL", "= " & parentTableId)
        For Each cr As DataRow In dtSchema.Select(filterString + extraFilter)
            Dim cnode As New TreeNode
            cnode.Text = cr.GetString("TableName")
            cnode.Value = cr.GetString("TableId")
            Select Case cr.GetString("Type")
                Case "P"
                    cnode.ImageUrl = "~/App_Static/images/icon16/tree-p.jpg"
                    cnode.ImageToolTip = "Parcel Table"
                Case "N"
                    cnode.ImageUrl = "~/App_Static/images/icon16/tree-n.jpg"
                    cnode.ImageToolTip = "Neighborhood Table"
                Case "L"
                    cnode.ImageUrl = "~/App_Static/images/icon16/tree-l.jpg"
                    cnode.ImageToolTip = "Lookup Table"
                Case Else
                    Select Case cr.GetInteger("Relationship")
                        Case 0
                            cnode.ImageUrl = "~/App_Static/images/icon16/tree-121.jpg"
                            cnode.ImageToolTip = "One-to-one"
                        Case 2
                            cnode.ImageUrl = "~/App_Static/images/icon16/tree-m21.jpg"
                            cnode.ImageToolTip = "Many-to-one"
                    End Select
            End Select
            LoadSchemaTableNode(cnode.ChildNodes, cr.GetInteger("TableId"))
            collection.Add(cnode)
        Next
    End Sub

    Protected Sub TreeView1_SelectedNodeChanged(sender As Object, e As EventArgs) Handles SchemaTables.SelectedNodeChanged
        Dim nodeValue As String
        nodeValue = SchemaTables.SelectedNode.Value
        ViewState("TableId") = nodeValue
        SelectedTable.InnerHtml = "Table - " + SchemaTables.SelectedNode.Text
        SelectedTable.Visible = True
        'LoadFieldsWithPrimaryKeys(nodeValue)
        If Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM DataSourceTable WHERE ImportType=5 AND Id = {0}".SqlFormatString(nodeValue)) <> 0 Then
            tabrel.Visible = False
            gvReferenceRelationships.Visible = False
        Else
            LoadRelationships(nodeValue)
            tabrel.Visible = True
            gvReferenceRelationships.Visible = True
        End If
        TableSettings(nodeValue)
        PanelDisplayContent.Visible = True
    End Sub
    Private Sub LoadFieldsWithPrimaryKeys(tableId As String)
        Dim dtTable As New DataTable
        Dim sqlstr As String
        sqlstr = "SELECT DISTINCT df.TableId,df.SourceTable,df.Id,df.Name AS FieldName,dk.IsPrimaryKey,df.DisplayLabel,df.DataType,CASE WHEN ip.Name ='Text' THEN CONCAT(ip.Name,CASE WHEN df.SchemaDataType LIKE 'varchar(%' THEN CONCAT('(',SUBSTRING( SchemaDataType,  charindex('(',SchemaDataType,1) + 1, charindex(')',SchemaDataType,1) - charindex('(',SchemaDataType,1) - 1 ),')') ELSE '' END) ELSE ip.Name END As DataTypeName,df.MaxLength,CASE WHEN df.SchemaDataType like '%(%' THEN SUBSTRING(df.SchemaDataType,1,CHARINDEX('(',df.SchemaDataType)-1) ELSE df.SchemaDataType END As SchemaDataType FROM DataSourceField df LEFT JOIN DataSourceKeys dk ON df.TableId=dk.TableId  and Df.Id=dk.FieldId AND dk.IsPrimaryKey = 1 INNER JOIN vw_InputTypes ip ON df.DataType = ip.ID WHERE df.TableId=" + tableId + " ORDER BY df.Id "
        dtTable = Database.Tenant.GetDataTable(sqlstr)
        If dtTable IsNot Nothing Then
            gvDataSourceTable.DataSource = dtTable
            gvDataSourceTable.DataBind()
        End If
    End Sub

    Protected Sub GridOnDataBound(sender As Object, e As EventArgs)
        Dim row As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal)
        Dim cell As New TableHeaderCell()
        cell.Text = SchemaTables.SelectedNode.Text
        cell.ColumnSpan = 5
        row.Controls.Add(cell)
        row.BackColor = ColorTranslator.FromHtml("#3AC0F2")
        gvDataSourceTable.HeaderRow.Parent.Controls.AddAt(0, row)
    End Sub

    Protected Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
    	If IsPostBack Then
    		Dim node_value As String = SchemaTables.SelectedNode.Value
    		LoadFieldsWithPrimaryKeys(node_value)
            RunScript("setRelation();")
        End If
    End Sub
    Private Sub LoadRelationships(tableId As String)
        Dim sqlstr As String
        sqlstr = "select dd.TableId,dd.SourceTable,dd.Name AS Field,df.SourceTable As ReferenceTable,df.name AS ReferenceField from DataSourceKeys dk inner join DataSourceField df on df.id=dk.ReferenceFieldId inner join DataSourceField dd on dd.Id=dk.FieldId where IsReferenceKey = 1 And dk.TableId = " + tableId
        dtReferenceKey = Database.Tenant.GetDataTable(sqlstr)
        Try
            If dtReferenceKey.Rows.Count = 0 Then
                dtReferenceKey.Rows.Add(dtReferenceKey.NewRow())
                gvReferenceRelationships.DataSource = dtReferenceKey
                gvReferenceRelationships.DataBind()
            Else
                gvReferenceRelationships.DataSource = dtReferenceKey
                gvReferenceRelationships.DataBind()
            End If
            ViewState("dtReferenceKey") = dtReferenceKey
        Catch ex As Exception
        End Try
    End Sub


    Protected Sub gvReferenceRelationships_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvReferenceRelationships.RowDataBound
        Dim SourceTable As String = dtReferenceKey.Rows(0)("SourceTable").ToString()
        Dim ReferenceTable As String = dtReferenceKey.Rows(0)("ReferenceTable").ToString()
        Dim sql As String
        sql = "select  name from DataSourceField where SourceTable ={0}".SqlFormatString(SourceTable)
        Dim dtSourceField As DataTable = Database.Tenant.GetDataTable(sql)
        sql = ""
        sql = "select  name from DataSourceField where SourceTable ={0}".SqlFormatString(ReferenceTable)
        Dim dtReferenceField As DataTable = Database.Tenant.GetDataTable(sql)

        If e.Row.RowType = DataControlRowType.Header Then
            Dim lblSourceTable As Label = DirectCast(e.Row.FindControl("lblSourceTable"), Label)
            lblSourceTable.Text = SourceTable
            Dim lblReferenceTable As Label = DirectCast(e.Row.FindControl("lblReferenceTable"), Label)
            lblReferenceTable.Text = ReferenceTable
        End If

        If e.Row.RowType = DataControlRowType.Footer Then

            Dim ddlAddSourceField As DropDownList = DirectCast(e.Row.FindControl("ddlAddSourceField"), DropDownList)
            Dim ddlAddReferenceField As DropDownList = DirectCast(e.Row.FindControl("ddlAddReferenceField"), DropDownList)

            ddlAddSourceField.Items.Clear()
            ddlAddSourceField.FillFromTable(dtSourceField, True, )

            ddlAddReferenceField.Items.Clear()
            ddlAddReferenceField.FillFromTable(dtReferenceField, True, )
        End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            If (e.Row.RowState And DataControlRowState.Edit) > 0 Then
                Dim ddlEditSourceField As DropDownList = DirectCast(e.Row.FindControl("ddlEditSourceField"), DropDownList)
                Dim ddlEditReferenceField As DropDownList = DirectCast(e.Row.FindControl("ddlEditReferenceField"), DropDownList)

                ddlEditSourceField.Items.Clear()
                ddlEditSourceField.FillFromTable(dtSourceField, True, )

                ddlEditReferenceField.Items.Clear()
                ddlEditReferenceField.FillFromTable(dtReferenceField, True, )

                Dim dr As DataRowView = TryCast(e.Row.DataItem, DataRowView)
                ddlEditSourceField.SelectedItem.Text = dr(2).ToString()
                ddlEditReferenceField.SelectedItem.Text = dr(4).ToString()
            End If
        End If
    End Sub

    Protected Sub gvReferenceRelationships_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvReferenceRelationships.RowCommand
        If e.CommandName.Equals("AddRelationship") Then
            Dim dt As DataTable
            dt = ViewState("dtReferenceKey")
            If dt.Rows.Count > 0 Then
                Dim tableId As String = dt.Rows(0)("TableId").ToString()
                Dim SourceTable As String = dt.Rows(0)("SourceTable").ToString()
                Dim ReferenceTable As String = dt.Rows(0)("ReferenceTable").ToString()
                Dim SourceField As String = DirectCast(gvReferenceRelationships.FooterRow.FindControl("ddlAddSourceField"), DropDownList).SelectedItem.Text
                Dim ReferenceField As String = DirectCast(gvReferenceRelationships.FooterRow.FindControl("ddlAddReferenceField"), DropDownList).SelectedItem.Text
                If SourceField <> "-- Select --" And ReferenceField <> "-- Select --" Then
                    Dim sql As String
                    sql = "EXEC ccad_AddReferanceKey {0},{1},{2},{3}".SqlFormatString(SourceTable, SourceField, ReferenceTable, ReferenceField)
                    Dim Description As String = "New Relationship between "+SourceField+" of "+SourceTable+" and "+ReferenceField+" of "+ReferenceTable+" has been created."
        			SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
                    Dim Msg As String = Database.Tenant.GetStringValue(sql)
                    Alert(Msg)
                    LoadRelationships(tableId)
                Else
                    Alert("You should select both field values.")
                End If
            End If
        End If
    End Sub

    Protected Sub gvReferenceRelationships_RowEditing(sender As Object, e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvReferenceRelationships.RowEditing
        gvReferenceRelationships.EditIndex = e.NewEditIndex
        ViewState("oldSourceField") = DirectCast(gvReferenceRelationships.Rows(e.NewEditIndex).FindControl("lblFieldName"), Label).Text
        ViewState("oldReferenceField") = DirectCast(gvReferenceRelationships.Rows(e.NewEditIndex).FindControl("lblReferenceField"), Label).Text
        Dim dt As DataTable
        dt = ViewState("dtReferenceKey")
        If dt.Rows.Count > 0 Then
            Dim tableId As String = dt.Rows(0)("TableId").ToString()
            LoadRelationships(tableId)
        End If
    End Sub

    Protected Sub gvReferenceRelationships_RowCancelingEdit(sender As Object, e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles gvReferenceRelationships.RowCancelingEdit
        gvReferenceRelationships.EditIndex = -1
        Dim dt As DataTable
        dt = ViewState("dtReferenceKey")
        If dt.Rows.Count > 0 Then
            Dim tableId As String = dt.Rows(0)("TableId").ToString()
            LoadRelationships(tableId)
        End If
    End Sub

    Protected Sub gvReferenceRelationships_RowUpdating(sender As Object, e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles gvReferenceRelationships.RowUpdating
        Dim dt As DataTable
        dt = ViewState("dtReferenceKey")
        If dt.Rows.Count > 0 Then
            Dim tableId As String = dt.Rows(0)("TableId").ToString()
            Dim SourceTable As String = dt.Rows(0)("SourceTable").ToString()
            Dim ReferenceTable As String = dt.Rows(0)("ReferenceTable").ToString()
            Dim OldSourceField As String = ViewState("oldSourceField")
            Dim OldReferenceField As String = ViewState("oldReferenceField")
            Dim NewSourceField As String = DirectCast(gvReferenceRelationships.Rows(e.RowIndex).FindControl("ddlEditSourceField"), DropDownList).SelectedItem.Text
            Dim NewReferenceField As String = DirectCast(gvReferenceRelationships.Rows(e.RowIndex).FindControl("ddlEditReferenceField"), DropDownList).SelectedItem.Text
            Dim sql As String
            sql = "EXEC ccad_UpdateReferanceKey {0},{1},{2},{3},{4},{5}".SqlFormatString(SourceTable, OldSourceField, NewSourceField, ReferenceTable, OldReferenceField, NewReferenceField)
            If(OldSourceField <> NewSourceField Or OldReferenceField <> NewReferenceField) Then
            	Dim Description As String = "Reference Key relationship between "+OldSourceField+" of "+SourceTable+" table and "+OldReferenceField+" of "+ReferenceTable+" table has been Updated to "+NewSourceField+" of "+SourceTable+" table and "+NewReferenceField+" of "+ReferenceTable+" table"
        		SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
            End If
            Dim Msg As String = Database.Tenant.GetStringValue(sql)
            Alert(Msg)
            ViewState("oldSourceField") = ""
            ViewState("oldReferenceField") = ""
            gvReferenceRelationships.EditIndex = -1
            LoadRelationships(tableId)
        End If
    End Sub

    Protected Sub gvReferenceRelationships_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvReferenceRelationships.RowDeleting
        Dim dt As DataTable
        dt = ViewState("dtReferenceKey")
        If dt.Rows.Count > 1 Then
            Dim tableId As String = dt.Rows(0)("TableId").ToString()
            Dim SourceTable As String = dt.Rows(0)("SourceTable").ToString()
            Dim ReferenceTable As String = dt.Rows(0)("ReferenceTable").ToString()
            Dim SourceField As String = DirectCast(gvReferenceRelationships.Rows(e.RowIndex).FindControl("lblFieldName"), Label).Text
            Dim ReferenceField As String = DirectCast(gvReferenceRelationships.Rows(e.RowIndex).FindControl("lblReferenceField"), Label).Text
            Dim row As GridViewRow = DirectCast(gvReferenceRelationships.Rows(e.RowIndex), GridViewRow)
            Dim sql As String
            sql = "EXEC ccad_RemoveReferanceKey {0},{1},{2},{3}".SqlFormatString(SourceTable, SourceField, ReferenceTable, ReferenceField)
            Dim Description As String = "Reference Key relationship between "+SourceField+" of "+SourceTable+" table and "+ReferenceField+" of "+ReferenceTable+" table has been removed"
        	SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
            Dim Msg As String = Database.Tenant.GetStringValue(sql)
            Alert(Msg)
            LoadRelationships(tableId)
        Else
            Alert("You should keep at least one.")
        End If
    End Sub
    Private Sub TableSettings(tableId As String)
        Dim sqlstr As String
        sqlstr = "SELECT * FROM DataSourceTable WHERE Id=" + tableId
        Dim dr As DataRow = Database.Tenant.GetTopRow(sqlstr)

        If dr IsNot Nothing Then
            txtDestinationAlias.Text = dr("DestinationAlias").ToString()
            txtDestinationFilter.Text = dr("DestinationFilter").ToString()
            txtAbbreviatedName.Text = dr.GetString("AbbreviatedName")
            txtNote.Text = dr.GetString("CC_Note")
            txtSync.Text = dr.GetString("SyncProperties")
            txtDNKCondition.Text = dr.GetString("DoNotKeepCondition")
            cb_DisableDeletePCI.Checked = dr.GetBoolean("DisableDeletePCI")
            chkAllowInsert.Checked = dr.GetBoolean("AllowOrphanInsert")
        End If
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim DestinationAlias As String = txtDestinationAlias.Text
        Dim DestinationFilter As String = txtDestinationFilter.Text
        Dim TableId = ViewState("TableId")
        Dim tblname = SchemaTables.SelectedNode.Text
        Dim sqlstr As String
        sqlstr = "SELECT * FROM DataSourceTable WHERE Id=" + tableId
        Dim dr As DataRow = Database.Tenant.GetTopRow(sqlstr)
        Database.Tenant.Execute("UPDATE DataSourceTable set DestinationAlias={1},DestinationFilter={2}, AbbreviatedName = {3},CC_Note={4},SyncProperties={5}, DoNotKeepCondition = {6},DisableDeletePCI ={7}, AllowOrphanInsert = {8} where id={0}".SqlFormatStringWithNulls(TableId, DestinationAlias, DestinationFilter, txtAbbreviatedName.Text.Trim, txtNote.Text, txtSync.Text, txtDNKCondition.Text, If(cb_DisableDeletePCI.Checked, "1", "0"), chkAllowInsert.Checked))
        Alert("Table Settings Saved")
        If (txtDestinationAlias.Text <> dr("DestinationAlias").ToString()) Then 
        	Dim Description As String = "Destination Alias has been changed from "+dr("DestinationAlias").ToString()+" to "+txtDestinationAlias.Text+" for "+tblname+" table"
        	SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
        End If
        If (txtDestinationFilter.Text <> dr("DestinationFilter").ToString()) Then 
        	Dim Description As String = "Destination Filter has been changed from "+dr("DestinationFilter").ToString()+" to "+txtDestinationFilter.Text+" for "+tblname+" table"
        	SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
        End If
        If (txtAbbreviatedName.Text <> dr.GetString("AbbreviatedName")) Then 
        	Dim Description As String = "Abbreviated Name has been changed from "+dr.GetString("AbbreviatedName")+" to "+txtAbbreviatedName.Text+" for "+tblname+" table"
        	SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
        End If
        If (txtNote.Text <> dr.GetString("CC_Note")) Then 
        	Dim Description As String = "Note has been changed from "+dr.GetString("CC_Note")+" to "+txtNote.Text+" for "+tblname+" table"
        	SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
        End If
        If (txtSync.Text <> dr.GetString("SyncProperties")) Then 
        	Dim Description As String = "Sync Properties has been changed from "+dr.GetString("SyncProperties")+" to "+txtSync.Text+" for "+tblname+" table"
        	SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
        End If
        If (txtDNKCondition.Text <> dr.GetString("DoNotKeepCondition")) Then 
        	Dim Description As String = "'Do Not Keep' Condition has been changed from "+dr.GetString("DoNotKeepCondition")+" to "+txtDNKCondition.Text+" for "+tblname+" table"
        	SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
        End If
        If (cb_DisableDeletePCI.Checked <> dr.GetBoolean("DisableDeletePCI")) Then 
        	Dim Description As String = "Disable Delete PCI has been set to "+cb_DisableDeletePCI.Checked.ToString()+" for "+tblname+" table"
        	SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
        End If
        If (chkAllowInsert.Checked <> dr.GetBoolean("AllowOrphanInsert")) Then 
        	Dim Description As String = "Allow Insert without Parent has been set to "+chkAllowInsert.Checked.ToString()+" for "+tblname+" table"
        	SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
        End If
    End Sub
End Class
