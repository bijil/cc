﻿Imports CAMACloud.BusinessLogic.Installation
Public Class use_CC_Settings
    Inherits System.Web.UI.Page

    Shared dt As DataTable
    Shared ImportId As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadAndShowGrid()
        End If
    End Sub

    Sub LoadAndShowGrid()
        dt = Database.Tenant.GetDataTable("SELECT sb.ImportId, sb.BackupName, sb.BackupDate,dbo.GetLocalDate(sb.BackupTime) AS BackupTime,COALESCE(us.FirstName + COALESCE(' ' + us.LastName, ''), sb.LoginId)AS UserName,IPAddress,sb.ImportFrom AS ImportFrom FROM CC_SettingsBackup sb LEFT OUTER JOIN UserSettings us ON us.LoginId=sb.LoginId ORDER BY sb.BackupTime DESC")
        If dt.Rows.Count > 0 Then
            lbNotification.Text = ""
            gvCC_Settings.DataSource = dt
            gvCC_Settings.DataBind()
        Else
            lbNotification.Text = "No Backup Files Available"
        End If

    End Sub

    Private Sub gvCC_Settings_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvCC_Settings.PageIndexChanging
        gvCC_Settings.PageIndex = e.NewPageIndex
        LoadAndShowGrid()
    End Sub

    Protected Sub gvCC_Settings_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvCC_Settings.RowCommand
        Dim importId As Integer = e.CommandArgument
        Dim xmlDoc As String = Database.Tenant.GetStringValue("SELECT BackupDocument FROM CC_SettingsBackup WHERE ImportId = " & importId)
        If e.CommandName = "Restore" Then
        	Try
	            Dim UserName = Membership.GetUser().ToString
	            Dim gen = New EnvironmentConfiguration
	            gen.ImportSettings(xmlDoc, BusinessLogic.Installation.ConfigFileType.All, Database.Tenant, UserName, "restore")
	            NotificationMailer.SendNotificationToVendorCDS(Membership.GetUser().ToString, 1, "Application Setting Backup")
	            Response.Redirect("~/protected/support/use_CC_Settings.aspx")
            Catch ex As Exception
            	Alert(ex.Message)
                Return
            End Try
            'LoadAndShowGrid()
        ElseIf e.CommandName = "Download" Then
            Dim xDoc = XDocument.Parse(xmlDoc)
            Dim xml As String = xDoc.ToString(SaveOptions.None)
            Dim exportName = HttpContext.Current.GetCAMASession.OrganizationCodeName + "-" + "restore" + "-" + Now.ToString("yyMM-dd-HHmmss") + ".xml"
            Response.WriteAsAttachment(xml, exportName)
            Response.Redirect("~/protected/support/use_CC_Settings.aspx")
        End If
    End Sub

    Protected Sub gvCC_Settings_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        ImportId = Convert.ToInt32(gvCC_Settings.DataKeys(e.Row.RowIndex).Values(0))
    End Sub
End Class