﻿Imports System.IO
Imports System.Drawing
Imports System.Collections
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Web.Services

Public Class nbhdaudittrail
    Inherits System.Web.UI.Page
    Public orgName As String = ""
    Public ReadOnly Property EnableNewPriorities As Boolean
        Get
            Dim EnableNewPriority = False
            Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM ClientSettings WHERE Name = 'EnableNewPriorities' AND Value = 1")
            If dr IsNot Nothing Then
                EnableNewPriority = True
            End If
            Return EnableNewPriority
        End Get
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        orgName = HttpContext.Current.GetCAMASession.OrganizationName + "-NbhdAuditTrail"
    End Sub

    Protected Sub ExportButton_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim query As String = ""
        Dim resultFields As String = ""
        If EnableNewPriorities Then
            query = "SELECT Id, FORMAT(dbo.GetLocalDate(EventDate),'MM-dd-yyyy hh:mm:ss tt') as EventDate, NbhdList, LoginId, Urgent, High, Normal, Critical, Medium, Proximity, TotalCount FROM NeighborhoodDownloadStats WHERE Status = 1 ORDER BY Id desc"
            resultFields = "EventDate,EventDate,150|NbhdList,NbhdList,150|LoginId,LoginId,150|Critical,Critical,150|Urgent,Urgent,150|High,High,150|Medium,Medium,150|Normal,Normal,150|Proximity,Proximity,150|TotalCount,TotalCount,150"
        Else
            query = "SELECT Id, FORMAT(dbo.GetLocalDate(EventDate),'MM-dd-yyyy hh:mm:ss tt') as EventDate, NbhdList, LoginId, Urgent, High, Normal, TotalCount FROM NeighborhoodDownloadStats WHERE Status = 1 ORDER BY Id desc"
            resultFields = "EventDate,EventDate,150|NbhdList,NbhdList,150|LoginId,LoginId,150|Urgent,Urgent,150|High,High,150|Normal,Normal,150|TotalCount,TotalCount,150"
        End If

        Dim dt As DataTable = Database.Tenant.GetDataTable(query)
        Dim fileName As String = orgName & DateTime.Now.ToString("yyyy-MM-dd") & ".xlsx"
        Dim grd As New GridView()
        grd.AutoGenerateColumns = False

        Dim line As String = ""
        Dim fieldNames As New List(Of String)
        For Each field In resultFields.Split("|")
            If line <> "" Then line += ","
            Dim dataField As String = field.Split(",")(0).ToString()
            Dim columnName As String = field.Split(",")(1).ToString()
            Dim width As String = field.Split(",")(2).ToString()
            width = If(width = "" OrElse width = "0", "50", width)
            fieldNames.Add(dataField)
            line += columnName
            Dim gridField As New BoundField()
            gridField.DataField = dataField
            gridField.HeaderText = columnName
            gridField.ItemStyle.Width = CType(width, Integer)
            grd.Columns.Add(gridField)
        Next
        grd.DataSource = dt
        grd.DataBind()
        Response.Clear()
        Response.Buffer = True
        Response.ClearContent()
        Response.ClearHeaders()
        Response.AddHeader("Content-Disposition", "attachment;filename=" & fileName)
        Response.Charset = ""
        Response.ContentType = "application/vnd.openxml.formats-officedocument.spreadsheetml.sheet"

        Using excelStream = ExcelGenerator.ExportGrid(grd, dt, "UDAG - Assignment Group Audit Trail", "UDAG -Assignment Group Audit Trail")
            excelStream.CopyTo(Response.OutputStream)
        End Using

        Response.End()
    End Sub

    <WebMethod>
    Public Shared Function GetNeighborhoodDownloadStats(ByVal EnableNewPriorities1 As String) As JSONTable
        Dim dt As DataTable
        If EnableNewPriorities1 = "True" Then
            dt = Database.Tenant.GetDataTable("Select Id, Format(dbo.GetLocalDate(EventDate),'MM-dd-yyyy hh:mm:ss tt') as EventDate, NbhdList, LoginId, Urgent, High, Normal, Critical, Medium, Proximity, TotalCount FROM NeighborhoodDownloadStats WHERE Status = 1 ORDER BY EventDate desc")
        Else
            dt = Database.Tenant.GetDataTable("Select Id, Format(dbo.GetLocalDate(EventDate),'MM-dd-yyyy hh:mm:ss tt') as EventDate, NbhdList, LoginId, Urgent, High, Normal, TotalCount FROM NeighborhoodDownloadStats WHERE Status = 1 ORDER BY EventDate desc")
        End If
        Return dt.ConvertToList()
    End Function

    <WebMethod>
    Public Shared Function GetNbhdList(ByVal NbhdId As String, ByVal EnableNewPriorities1 As String) As JSONTable
        Dim alternateDataRow As DataRow = Database.Tenant.GetTopRow("SELECT Alternatekeyfieldvalue, KeyField1, ShowKeyValue1, ShowAlternateField FROM Application")
        Dim keyFields As String = ""
        If GetString(alternateDataRow, "ShowKeyValue1") = "True" And GetString(alternateDataRow, "KeyField1") <> "" Then
            keyFields = "pd." + GetString(alternateDataRow, "KeyField1")
        End If
        If GetString(alternateDataRow, "ShowAlternateField") = "True" And GetString(alternateDataRow, "Alternatekeyfieldvalue") <> "" Then
            keyFields += IIf(keyFields = "", "", ", ") + "pd." + GetString(alternateDataRow, "Alternatekeyfieldvalue")
        End If

        Dim dt As DataTable
        If EnableNewPriorities1 = "True" Then
            dt = Database.Tenant.GetDataTable("SELECT FORMAT(dbo.GetLocalDate(EventDate),'MM-dd-yyyy hh:mm:ss tt') as Date, n.LoginId LoginId, " + keyFields + ", CASE WHEN np.Priority = 5 THEN 'Critical' WHEN np.Priority = 4 THEN 'Urgent' WHEN np.Priority = 3 THEN 'High' WHEN np.Priority = 2 THEN 'Medium' WHEN np.Priority = 1 THEN 'Normal' ELSE 'Proximity' END Priority, np.Nbhd as [Assignment Group] FROM NeighborhoodDownloadparcelList np JOIN NeighborhoodDownloadStats n ON n.Id = np.NbhdStatsId JOIN ParcelData pd ON np.pId = pd.CC_ParcelId WHERE np.NbhdStatsId = " + NbhdId.ToString())
        Else
            dt = Database.Tenant.GetDataTable("SELECT FORMAT(dbo.GetLocalDate(EventDate),'MM-dd-yyyy hh:mm:ss tt') as Date, n.LoginId LoginId, " + keyFields + ", CASE WHEN np.Priority = 2 THEN 'Urgent' WHEN np.Priority = 1 THEN 'High' ELSE 'Normal' END Priority, np.Nbhd as [Assignment Group] FROM NeighborhoodDownloadparcelList np JOIN NeighborhoodDownloadStats n ON n.Id = np.NbhdStatsId JOIN ParcelData pd ON np.pId = pd.CC_ParcelId WHERE np.NbhdStatsId = " + NbhdId.ToString())
        End If
        Return dt.ConvertToList()
    End Function

End Class