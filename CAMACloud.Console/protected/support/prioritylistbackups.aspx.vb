﻿Public Class prioritylistbackups
    Inherits System.Web.UI.Page
    Shared dt As DataTable
    Shared ImportId As String

    Public ReadOnly Property EnableNewPriorities As Boolean
        Get
            Dim EnableNewPriority = False
            Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM ClientSettings WHERE Name = 'EnableNewPriorities' AND Value = 1")
            If dr IsNot Nothing Then
                EnableNewPriority = True
            End If
            Return EnableNewPriority
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadGrid()
        End If
    End Sub
    Sub LoadGrid()
        If EnableNewPriorities Then
            gvPriorityLists.Visible = False
            gvPriorityListsNew.Visible = True
            dt = Database.Tenant.GetDataTable("SELECT pb.id,pb.FileName,dbo.GetLocalDate(Pb.UploadTime) As UploadTime,CASE WHEN COALESCE(us.FirstName,'') = '' AND COALESCE(us.LastName,'') = '' THEN  pb.LoginId ELSE ISNULL(us.FirstName,'') + ' '+ ISNULL(us.LastName,'') END As UserName,pb.IPAddress,TotalParcels,Urgent,High,Normal, Critical, Medium, Proximity,Invalid,CASE WHEN IsResetStatus = 1 THEN 'Yes' ELSE 'No' END AS IsResetStatus,CASE WHEN IsAdhoc = 1 THEN 'Yes' ELSE 'No' END AS IsAdhoc ,GroupCode,CASE WHEN COALESCE(us1.FirstName,'') = '' AND COALESCE(us1.LastName,'') = '' THEN  pb.AssignTo ELSE ISNULL(us1.FirstName,'') + ' '+ ISNULL(us1.LastName,'') END As AssignTo,CASE pb.Status WHEN 1 THEN 'Success' WHEN 0 THEN 'Failure' END AS Status,pb.Error FROM PriorityListBackup pb LEFT OUTER JOIN UserSettings us ON us.LoginId=pb.LoginId  LEFT OUTER JOIN UserSettings us1 On us1.LoginId=pb.AssignTo WHERE pb.UploadTime> dateadd(ww,-2,GETUTCDATE()) ORDER BY ID DESC")
            gvPriorityListsNew.DataSource = dt
            gvPriorityListsNew.DataBind()
        Else
            gvPriorityLists.Visible = True
            gvPriorityListsNew.Visible = False
            dt = Database.Tenant.GetDataTable("SELECT pb.id,pb.FileName,dbo.GetLocalDate(Pb.UploadTime) As UploadTime,CASE WHEN COALESCE(us.FirstName,'') = '' AND COALESCE(us.LastName,'') = '' THEN  pb.LoginId ELSE ISNULL(us.FirstName,'') + ' '+ ISNULL(us.LastName,'') END As UserName,pb.IPAddress,TotalParcels,Urgent,High,Normal,Invalid,CASE WHEN IsResetStatus = 1 THEN 'Yes' ELSE 'No' END AS IsResetStatus,CASE WHEN IsAdhoc = 1 THEN 'Yes' ELSE 'No' END AS IsAdhoc ,GroupCode,CASE WHEN COALESCE(us1.FirstName,'') = '' AND COALESCE(us1.LastName,'') = '' THEN  pb.AssignTo ELSE ISNULL(us1.FirstName,'') + ' '+ ISNULL(us1.LastName,'') END As AssignTo,CASE pb.Status WHEN 1 THEN 'Success' WHEN 0 THEN 'Failure' END AS Status,pb.Error FROM PriorityListBackup pb LEFT OUTER JOIN UserSettings us ON us.LoginId=pb.LoginId  LEFT OUTER JOIN UserSettings us1 On us1.LoginId=pb.AssignTo WHERE pb.UploadTime> dateadd(ww,-2,GETUTCDATE()) ORDER BY ID DESC")
            gvPriorityLists.DataSource = dt
            gvPriorityLists.DataBind()
        End If
    End Sub

    Private Sub gvPriorityLists_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvPriorityLists.PageIndexChanging
        gvPriorityLists.PageIndex = e.NewPageIndex
        LoadGrid()
    End Sub

    Private Sub gvPriorityListsNew_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvPriorityListsNew.PageIndexChanging
        gvPriorityListsNew.PageIndex = e.NewPageIndex
        LoadGrid()
    End Sub

    Protected Sub gvPriorityLists_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvPriorityLists.RowCommand
        If e.CommandName = "Download" Then
            Dim Id As Integer = e.CommandArgument
            Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT FileName,FileContent FROM PriorityListBackup  WHERE Id = " & Id)
            Dim FileName As String = dr.GetString("FileName")
            FileName = HttpContext.Current.GetCAMASession.OrganizationName.ToString() + "-" + FileName
            Dim FileContent As String = dr.GetString("FileContent")
            Response.Clear()
            Response.Buffer = True
            Response.AddHeader("content-disposition", "attachment;filename=" & FileName)
            Response.Charset = ""
            Response.ContentType = "application/text"
            Response.Output.Write(FileContent)
            Response.Flush()
            Response.End()
        End If
    End Sub

    Protected Sub gvPriorityListsNew_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvPriorityListsNew.RowCommand
        If e.CommandName = "Download" Then
            Dim Id As Integer = e.CommandArgument
            Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT FileName,FileContent FROM PriorityListBackup  WHERE Id = " & Id)
            Dim FileName As String = dr.GetString("FileName")
            FileName = HttpContext.Current.GetCAMASession.OrganizationName.ToString() + "-" + FileName
            Dim FileContent As String = dr.GetString("FileContent")
            Response.Clear()
            Response.Buffer = True
            Response.AddHeader("content-disposition", "attachment;filename=" & FileName)
            Response.Charset = ""
            Response.ContentType = "application/text"
            Response.Output.Write(FileContent)
            Response.Flush()
            Response.End()
        End If
    End Sub

    Protected Sub gvPriorityLists_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        ImportId = Convert.ToInt32(gvPriorityLists.DataKeys(e.Row.RowIndex).Values(0))
    End Sub
    Protected Sub gvPriorityListsNew_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        ImportId = Convert.ToInt32(gvPriorityListsNew.DataKeys(e.Row.RowIndex).Values(0))
    End Sub


End Class