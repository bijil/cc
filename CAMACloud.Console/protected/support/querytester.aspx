﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="querytester.aspx.vb" Inherits="CAMACloud.Console.querytester" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<style>
		.txtQuery {
		    width: 95%;
    		margin-left: 12px;
    		overflow-y: hidden; 
    		resize: none;
            min-height	:84px;
			max-height :628px;
		}
		.btnExecute {
			float: right;
    		margin-right: 35px;
    		margin-top: 10px;
		}
		.gridViewDiv {
			width: 100%;
		    overflow-x: auto;

		}
		.gridView {
                table-layout: fixed;
                margin-top:10px;
                min-width: 100%;
		}
        .pgr td{
                border:none !important;
        }
        .gridView td,.gridView th{
                   white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    border: 1px solid rgb(226, 224, 224);
    padding: 1px 2px;
    background-color: none;
    font-size: 12px;
    font-weight: normal;
    color: #484646;
    text-align: left;
        }
        
         .maskquery
{
    background: black;
    opacity: 0.5;
    top: 0%;
    bottom:0%;
    position: absolute;
    display: none;
    right: 1px;
    z-index: 100;
}
        
	</style>
	<script type="text/javascript">
		 $(document).ready(function () {
        	$('.pgr a').click(function () { maskQuery(true); });
    	 });
    
		function maskQuery(flag){
			if(flag){
				$('.maskloader').css('display','block');
				$('.maskquery').css('display','block');	
                window.scrollTo(0, 0);
   			    $('body').css('overflow', 'hidden');				
			}
			else{
				$('.maskloader').css('display','none');
				$('.maskquery').css('display','none');
				$('body').css('overflow-Y', 'auto');
			}
		}
	
		function verifyQuery() {
			maskQuery(true);
			if ($('.txtQuery').val().trim().length == 0) {
				maskQuery(false);
				alert("Please enter a query");
			}  
			return true;
		}
        function setHeight(idname) {
			idname.addEventListener('change', autosize);  
			idname.addEventListener('keydown', autosize);
			idname.addEventListener('keypress', autosize);
		function autosize(){
			var el = this;
 			setTimeout(function(){
   				el.style.cssText = 'height:auto;';
   				el.style.cssText = 'height:' + el.scrollHeight + 'px';	   	
 			}, 0 );
          		var hidden  = $( '.txtQuery' ).height();
   				localStorage.setItem( "hidden", hidden);
   			}
       	}
       $( document ).ready( function (){
       if($( '.txtQuery' ).text()!=""){
           	var ht = localStorage.getItem( "hidden" );
            $( '.txtQuery' ).css({'height': ht +  'px' });
          }
         $('.pgr').css({"position": "absolute","margin-top": "10px"});
         $('.main-content-area').css('padding-bottom', '35px');
       });
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server" >

	<h1>Query Analyzer</h1>
	<div style="margin-bottom: 30px;">
		<asp:TextBox runat="server" ID="txtQuery" CssClass="txtQuery" Rows="3" TextMode="MultiLine"  onkeydown="setHeight(this);" onchange="setHeight(this);" onkeypress="setHeight(this);"/>
		<asp:Button runat="server" ID="btnExecute" CssClass="btnExecute" Text=" Execute " OnClientClick="return verifyQuery();" />
		<asp:Button runat="server" ID="btnClear" CssClass="btnExecute" Text=" Refresh " />
	</div>
	<div class = "maskquery" style = "display:none; width:100%; height: auto">
		<span style="margin-left: 47%; margin-top: 22%; float:left">
            <img src="/App_Static/css/images/comploader.gif">
            <span style="color: wheat;float: right;margin-left: 12px;margin-top: 20px;display: none;" class="maskloader">
            Please
                wait...</span>
    	</span>
	</div>
	<asp:Panel runat="server" ID="pnlResult" Visible="False" >
	<div style="font-weight: bold; width: 97%;float: left; padding-bottom: 9px ;margin-top:20px;">			
			 <span id="PageCountQueryAnalyzer" runat="server"></span>
			 
        	<div ID="pageSize_div"style = "float:right; ">
         	<asp:Button runat="server" ID="btnExport" Text=" Export " />
    		No. of items per page:
    			<asp:DropDownList runat="server" ID="ddlPageSize" AutoPostBack="true" width="50px" onchange="maskQuery(true)">
			        <asp:ListItem Value="15" />
			        <asp:ListItem Value="30" />
			        <asp:ListItem Value="50" />
		         </asp:DropDownList>
    	</div>
       
            </div>
		<div class="gridViewDiv">

			<asp:GridView runat="server" ID="grid" CssClass="gridView" AllowPaging="True"  border="0" OnPageIndexChanging="OnPageIndexChanging" PageSize="15" CellPadding="0" EnableViewState="true" >
		    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"  FirstPageText="<<" LastPageText=">>"/>
		    </asp:GridView>
	    </div>
	    
    </asp:Panel>
</asp:Content>
