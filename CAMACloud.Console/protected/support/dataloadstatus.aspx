﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="dataloadstatus.aspx.vb" Inherits="CAMACloud.Console.dataloadstatus" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <link rel="Stylesheet" href="/App_Static/css/tabcontent.css" />
    <script type="text/javascript" src="/App_Static/js/tabcontent.js"></script>
        <style type="text/css">
        .search-panel
        {
            padding: 7px;
            border: 1px solid #CFCFCF;
            width: 97.5%;
            background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#CFCFCF), to(#F8F8F8));
            margin-bottom:20px;
        }
        
        .search-panel label
        {
            margin-right: 5px;
        }
        .parcellabel{
                font-size: medium;
                font-weight: bold;
        }
    </style>
    <script>


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
   <h1>System Data Status</h1>
    <table style="width: 100%;">
        <tr>
            <td style="width: 300px;">
                <h2 style="margin:0px;">Tables</h2>
                <div style="width:280px;overflow-x:auto;border-right:2px solid #e2e2e2;min-height:600px">
                    <asp:TreeView runat="server" ID="SchemaTables" PopulateNodesFromClient="true" ShowLines="true" ShowExpandCollapse="true" AutoGenerateDataBindings="False" SelectedNodeStyle-Font-Bold="true" />
                </div>
                
            </td>
            <td>
                <asp:Panel ID="PanelDisplayContent" runat="server" style="padding-top: 4px; margin-right: 10px;">
                    <asp:Label ID="lblHead" runat="server" Style="margin-left: 10px; font-size: large; font-weight: bold; color: #065a9f;" Visible="true">Adding</asp:Label>
                    <div class="tabs-main" style="padding-top: 5px;">
                        <ul class="tabs" data-persist="true">
                            <li><a href="#tab-pk">Data Load Count</a></li>
                            <li><a href="#tab-rel">Parcel Data Count</a></li>
                        </ul>
                    </div>
                    <div class="tabcontent" style="border:1px solid #b7b7b7;border-top:0px none;padding:6px 10px 20px 10px;height:500px;overflow-y:auto;">
                        <div id="tab-pk">
                            <div id ="displayTotalRecord" runat="server" style="margin-top: 20px;">
                                <asp:Label ID="lblTotalRecord" runat="server" Text="Label" CssClass="parcellabel"> Total Records: </asp:Label>
                            <asp:Label ID="lblTotalRecordCount" runat="server"  Text="Label" CssClass="parcellabel"></asp:Label>
                            </div>
                            
                            <asp:GridView ID="gvDataCount" runat="server" width=" 70%" Style="margin-top: 20px;">
                                <Columns>
                                <asp:TemplateField HeaderText="Total Records" >
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1" runat="server" Text='<%# Eval("TotalRecord", "{0:#,0}")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Original Records">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lbl2" Text='<%# Eval("OriginalRecord", "{0:#,0}")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="New Records">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lbl3" Text='<%# Eval("NewRecord", "{0:#,0}")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>   
                            </Columns>
                        </asp:GridView>
                        </div>
                        <div id="tab-rel">
                            <div class="search-panel">
                                <table>
                                    <tr>
                                        <td>
                                            <label> Parcel Id:</label>
                                            <asp:TextBox runat="server" ID="txtKeyValue" Width="300px" MaxLength="30"  placeholder="Enter KeyValue"  CssClass="textboxAuto" />
                                        </td>
                                        <td style="width: 10px;">
                                        </td>
                                        <td>
                                            <asp:Button runat="server" ID="btnSearch" Text=" Search " />
                                        </td>
                                    </tr>
                                </table>
                        </div>
                            <div id="displayParcelCount" runat="server" CssClass="parcellabel">
                            <asp:Label ID="lblParcel" runat="server" Text="Label" CssClass="parcellabel"> Total Records in Parcel: </asp:Label>
                            <asp:Label ID="lblParcelCount" runat="server"  Text="Label" CssClass="parcellabel"></asp:Label>
                            </div>

                             <asp:GridView ID="gvParcelCount" runat="server" width=" 70%" Style="margin-top: 20px;">
                                <Columns>
                                <asp:TemplateField HeaderText="Total Records" >
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1" runat="server" Text='<%# Eval("TotalRecord", "{0:#,0}")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Original Records">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lbl2" Text='<%# Eval("OriginalRecord", "{0:#,0}")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="New Records">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lbl3" Text='<%# Eval("NewRecord", "{0:#,0}")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>   
                            </Columns>
                        </asp:GridView>
                        </div>
                    </div>
                </asp:Panel>
            </td>
        </tr>
    </table>

</asp:Content>
