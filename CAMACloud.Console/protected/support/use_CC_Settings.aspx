﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="use_CC_Settings.aspx.vb" Inherits="CAMACloud.Console.use_CC_Settings" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
        $(document).ready(function () {
            $('.pgr a').click(function () { showMask(); });
        });
 </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Application Setting Backup</h1><br />
    <asp:Label ID="lbNotification" runat="server" Text=""></asp:Label>
    <asp:Panel ID="Panel1" runat="server">
        <asp:GridView ID="gvCC_Settings" runat="server" AutoGenerateColumns="False" DataKeyNames="ImportId" PageSize="20" AllowPaging="true" >
            <Columns>
                <asp:BoundField DataField="BackupName" HeaderText="Name" ItemStyle-Width="450px"/>
                <asp:BoundField DataField="UserName" HeaderText="UserName" ItemStyle-Width="100px" />
                <asp:BoundField DataField="IPAddress" HeaderText="IP Address" ItemStyle-Width="100px" />
                <asp:BoundField DataField="BackupTime" HeaderText="Date" DataFormatString="{0: M\/d\/yyyy h:mm tt }" ItemStyle-Width="155px" />
                <asp:BoundField DataField="ImportFrom" HeaderText="Imported From" ItemStyle-Width="200px" />
                 <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton runat="server" ID="lbUse" Text="Restore" CommandName="Restore" CommandArgument='<%# Eval("ImportId")%>' Width ="85" OnClientClick = "return confirm('Are you sure?')" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton runat="server" ID="lbDownload" Text="Download" CommandName="Download" CommandArgument='<%# Eval("ImportId") %>' Width="85"/>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </asp:Panel>
</asp:Content>
