﻿<%@ Page Title="Cleanup Photos" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="photocleanup.aspx.vb" Inherits="CAMACloud.Console.photocleanup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .group-title {
            font-weight: bold;
            margin-top: 12px;
            margin-bottom: 8px;
        }

        .option-box {
            margin: 8px;
            margin-left: 30px;
        }
        .spaced input[type="radio"] {
            margin-left: 4px; /* Or any other value */
        }
        .info {
            width: 850px;
            border-left: 8px solid #CFCFCF;
            padding-left: 10px;
            line-height: 15pt;
            padding-top: 5px;
            padding-bottom: 5px;
        }
    </style>
    <script>
        function checkRequiredField() {

            var rbPhotosChecked = $('#<%= rbPhotos.ClientID %> input[type="radio"]:checked').length > 0;
            var rbnPendingPhotosChecked = $('#<%= rbnPendingPhotos.ClientID %> input[type="radio"]:checked').length > 0;
            var rbParcelsChecked = $('#<%= rbParcels.ClientID %> input[type="radio"]:checked').length > 0;

            var btnDelete = $('#<%= btnDelete.ClientID %>');

            if (rbPhotosChecked || rbnPendingPhotosChecked || rbParcelsChecked) {
                btnDelete.prop('disabled', false);
            } else {
                btnDelete.prop('disabled', true);
            }
        
    if($('#MainContent_MainContent_chkDateBetween').attr('checked') == "checked"){   	
    	var fromDate = $('#MainContent_MainContent_dtFrom').val();
    	var toDate = $('#MainContent_MainContent_dtTo').val();
    	if(fromDate>toDate){
    		alert("Please check the date.");
    		return false;
    	}
    	else if(fromDate=="" || toDate==""){
    		alert("Please fill the mandatory fields.");
    		return false;
    	}
    	else if(fromDate>"2078/12/12" || toDate>"2078/12/12"){
    		alert("Year above 2078 is not allowed. Please change the year.");
    		return false;
    	}
    	else{
    		return true;
    	}
    }
    if($('#MainContent_MainContent_rpMetaFields_chkMetaOption_0').attr('checked') == "checked"){
     	if($('#MainContent_MainContent_rpMetaFields_txtOptionValue_0').val()==""){
    		alert("Please fill the mandatory fields.");
    		return false;
    	}
    	else{
			return true;
	 	}
     }
     if($('#MainContent_MainContent_rpMetaFields_chkMetaOption_1').attr('checked') == "checked"){
     	if($('#MainContent_MainContent_rpMetaFields_txtOptionValue_1').val()==""){
    		alert("Please fill the mandatory fields.");
    		return false;
    	}
    	else{
			return true;
	 	}
     }   
 }

$( document ).ready(function() {
     if($('#MainContent_MainContent_chkDateBetween').attr('checked') == "checked"){
     	$('.chkDate').show();
     }
     if($('#MainContent_MainContent_rpMetaFields_chkMetaOption_0').attr('checked') == "checked"){
     	$('#PhotoMetaField1').show();
     }
     if($('#MainContent_MainContent_rpMetaFields_chkMetaOption_1').attr('checked') == "checked"){
     	$('#PhotoMetaField2').show();
    }
    enableNextButton();
    $('#<%= rbPhotos.ClientID %> input[type="radio"], #<%= rbnPendingPhotos.ClientID %> input[type="radio"], #<%= rbParcels.ClientID %> input[type="radio"]').change(function () {
        enableNextButton();
    });
});
function confirm() {
    if ($('#MainContent_MainContent_chkPendingConfirm').length > 0) {
        if ($('#MainContent_MainContent_chkPendingConfirm')[0].checked == false) {
            alert('Please check the confirmation checkbox to continue.');
            return false;
        }
    }
    if ($('#chkagree')[0].checked) {
        return true;
    }
    else {
        alert('Please check the confirmation checkbox to continue.');
        return false;
    }
 }
        function enableNextButton() {
            var rbPhotosChecked = $('#<%= rbPhotos.ClientID %> input[type="radio"]:checked').length > 0;
             var rbnPendingPhotosChecked = $('#<%= rbnPendingPhotos.ClientID %> input[type="radio"]:checked').length > 0;
             var rbParcelsChecked = $('#<%= rbParcels.ClientID %> input[type="radio"]:checked').length > 0;

             var btnDelete = $('#<%= btnDelete.ClientID %>');
             if (rbPhotosChecked || rbnPendingPhotosChecked || rbParcelsChecked) {
                 btnDelete.prop('disabled', false);
             } else {
                 btnDelete.prop('disabled', true);
             }
         }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Delete Photos from Cloud</h1>
    <table>
        <tr>
            <td style="width: 150px">Total photos in cloud: </td>
            <td style="font-weight: bold;">
                <asp:Label runat="server" ID="lblPhotoCount" /></td>
        </tr>
    </table>
    <asp:Panel runat="server" ID="pnlInitDelete">
        <p class="info"><strong>CAUTION! Major processes, like Photo Cleanup, should be done at the end of the business day or when the environment is not in use by others. <br />  
                   </strong></p>
       
        <div class="group-title">Select criteria:</div>
         <asp:HiddenField runat="server" ID="hdnProcessID" />
        <asp:RadioButtonList runat="server" ID="rbPhotos" AutoPostBack="true" RepeatDirection="Horizontal" CssClass="spaced">
            <%--<asp:ListItem Value="ALL" Text="All Photos" Selected="True" />--%>
            <asp:ListItem Value="ALL" Text="All Photos" />
            <asp:ListItem Value="EMF" Text="All Photos except Main/First" />
            <asp:ListItem Value="FIL" Text="Based on filter condition" />
            <asp:ListItem Value="LIM" Text="Based on photo limit" />
            <asp:ListItem Value="ECC" Text="All photos loaded from CAMA" />
        </asp:RadioButtonList>

        <asp:Panel runat="server" ID="pnlPhotoOps" Class="info" CssClass="option-box info" Visible="false">
            <table>
                <tr>
                    <td style="width: 220px">
                        <asp:CheckBox runat="server" ID="chkDateBetween" Text="Captured/uploaded between:" AutoPostBack="true" /><span class="chkDate" style="font-weight: bold; color:red; display:none;">*</span>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="dtFrom" Width="130px" type="date" Enabled="false" />
                    </td>
                    <td>
                        <asp:RequiredFieldValidator runat="server" ID="rfvDtF" ControlToValidate="dtFrom" ErrorMessage="*" Enabled="false" />&nbsp;and&nbsp;&nbsp;
                    <asp:TextBox runat="server" ID="dtTo" Width="130px" type="date" Enabled="false" />
                        <asp:RequiredFieldValidator runat="server" ID="rfvDtT" ControlToValidate="dtTo" ErrorMessage="*" Enabled="false" />
                    </td>
                </tr>
                <asp:Repeater runat="server" ID="rpMetaFields">
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:CheckBox runat="server" ID="chkMetaOption" Text='<%# "MetaData: " + Eval("DisplayLabel")%>' AutoPostBack="true" OnCheckedChanged="chkMetaOption_CheckedChanged" /><span ID='<%# Eval("AssignedName")%>' style="font-weight: bold; color:red; display:none;">*</span>
                                <asp:HiddenField runat="server" ID="hdMetaName" Value='<%#IIf(IsDBNull(Eval("AssignedName")), "", Eval("AssignedName")).Replace("PhotoMetaField", "MetaData") %>' />
                            </td>
                            <td>
                                <asp:DropDownList runat="server" ID="ddlMetaOption" AutoPostBack="true" Width="136px" Enabled="false" OnSelectedIndexChanged="ddlMetaOption_CheckedChanged">
                                    <asp:ListItem Value="EQ" Text="Equal To" />
                                    <asp:ListItem Value="NE" Text="Not Equal To" />
                                    <asp:ListItem Value="SW" Text="Starting With" />
                                    <asp:ListItem Value="NSW" Text="Not Starting With" />
                                    <asp:ListItem Value="NULL" Text="Is Null Or Empty" />
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtOptionValue" Width="180px" Enabled="false" />
                                <asp:RequiredFieldValidator runat="server" ID="rfv" ControlToValidate="txtOptionValue" ErrorMessage="*" Enabled="false" />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </asp:Panel>
           
        <asp:Panel runat="server" ID="pnlLimitOps" CssClass="option-box info" Visible="false">
            Delete 
            <asp:DropDownList runat="server" ID="ddlLimitTrimType">
                <asp:ListItem Text="Earliest" Value="0" />
                <asp:ListItem Text="Latest" Value="1" />
            </asp:DropDownList>
             photos
            when exceeding limit of
            <asp:DropDownList runat="server" ID="ddlLimit">
            </asp:DropDownList>
        </asp:Panel>
        <div class="group-title">Include pending photos:</div>

            <asp:RadioButtonList ID="rbnPendingPhotos" runat="server" CssClass="spaced">
                <asp:ListItem Value ="1" Text ="Yes"></asp:ListItem>
<%--                <asp:ListItem Value ="0" Text ="No" Selected ="True"></asp:ListItem>--%>
                                <asp:ListItem Value ="0" Text ="No" ></asp:ListItem>

            </asp:RadioButtonList>

        <div class="group-title">Select parcels:</div>
        <asp:RadioButtonList runat="server" ID="rbParcels" AutoPostBack="true">
<%--            <asp:ListItem Value="" Text="All Parcels" Selected="True" />--%>
                        <asp:ListItem Value="" Text="All Parcels" />

            <asp:ListItem Value="S" Text="Only parcels in list" />
        </asp:RadioButtonList>
        <asp:Panel runat="server" ID="pnlSelectParcels" CssClass="option-box" Visible="false">
            <p class="info">
               With the following header field, please upload parcel list in CSV format
                <br />
                <strong><%= KeyList() %></strong>
            </p>
            <asp:HiddenField runat="server" ID="hdHasFile" Value="0" />
            <asp:FileUpload runat="server" ID="fuParcelList" accept=".csv" Enabled="false" />
            <asp:RequiredFieldValidator runat="server" ID="rfvfu" ControlToValidate="fuParcelList" ErrorMessage="*" Enabled="false" />
            <br />
            <asp:Label runat="server" ID="lblSelectedParcels" />
        </asp:Panel>
        <div style="height: 20px"></div>

        <asp:Button runat="server" ID="btnDelete" Text="Next: Confirmation" Font-Bold="true" OnClientClick="return checkRequiredField();" Enabled="false" />
    </asp:Panel>

    <asp:Panel runat="server" ID="pnlConfirm" Visible="false">
        <h2 runat="server" id="confirmTitle">Delete
            <asp:Label runat="server" ID="lblDeletePercentTitle" /> photos</h2>
        <p class="info">
            <strong>CAUTION: You are about to permanently delete photos from the cloud as a bulk action. Please check the photo counts shown below before clicking the "Confirm Delete" button.</strong><br />
        </p>
        <div runat="server" id="pendingPhotoConformationDiv" visible ="false">
            <p class="info">
                There are 
                <asp:Label runat="server" ID="lblTotalPendingCount" Font-Bold="true" />
                <strong>Photos that have not synced back to your server</strong>,Please confirm you want these deleted.
            </p>
            <asp:CheckBox ID="chkPendingConfirm" runat="server" />
            <label for="chkPendingConfirm">I confirm I want to delete these photos.</label>
        </div>
        <p class="info">
            <asp:Label runat="server" ID="lblDeleteCount" Font-Bold="true" />
            out of 
            <asp:Label runat="server" ID="lblTotalCount" Font-Bold="true" />
            photos 
            <asp:Label runat="server" ID="lblDeletePercent" Font-Bold="true" />
            will be permanently deleted from the cloud. This action cannot be undone.
        </p>
        
        <p>
            <input type="checkbox" id="chkagree" />
            <label for="chkagree">I confirm the bulk delete action.</label>
        </p>
        <div>
            <asp:Button runat="server" ID="btnConfirm" Text="Confirm Delete" Font-Bold="true" OnClientClick="return confirm()" />
            <asp:Button runat="server" ID="btnCancel" Text="Cancel" />
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlFinalResult" Visible="false">
        <p>
            <asp:Label runat="server" ID="lblFinalDeleteCount" Font-Bold="true" /> photo(s) has been deleted from the cloud successfully.<br />
            <asp:Label runat="server" ID="lblIgnoreWarning" ForeColor="Gray"/>
        </p>
        <p>
            <asp:LinkButton runat="server" ID="lbReturn" Text="Click here to start over." />
        </p>
    </asp:Panel>
    <asp:HiddenField runat="server" ID="hdnTotalCount" />
    <asp:HiddenField runat="server" ID="hdnDeleteCount" />
</asp:Content>
