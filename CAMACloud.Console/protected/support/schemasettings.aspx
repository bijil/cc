﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="schemasettings.aspx.vb" Inherits="CAMACloud.Console.schemasettings" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .ddlList { margin-bottom: 6%; width: 88% !important; }
        .editImage { width: 20%; }
        .mGrid { width:95% !important; }
        .textarea { font: 13.3333px Arial; }
        .btnFieldSave { float: right; margin-right: 1%; }
        .imgEditTable { margin-bottom: -1%; }
        .btnTableSave { float: right; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Schema Settings</h1>
    <div>
        <table style="width: 100%;">
            <tr>
                <td style="width: 22%;">
                    <table style="width: 95%;">
                        <tr><td><label><b>Data Source Table</b></label></td></tr>
                        <tr><td>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlTableList" runat="server" AutoPostBack="true" CssClass="ddlList">
                                    </asp:DropDownList>
                                    <asp:ImageButton ID="imgTableEdit" CssClass="imgEditTable" ToolTip="Edit Table Property" ImageUrl="~/App_Static/css/icons16/edit_new.png" Width="20" runat="server" CommandArgument='<%# Eval("Id")%>' />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            </td></tr>
                        <tr><td><label><b>Data Source Field</b></label></td></tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView ID="gridDataList" class="fieldContainer" runat="server" AutoGenerateColumns="False" Width="100%">
                                            <Columns>
                                                <asp:BoundField DataField="Name" />
                                                <asp:TemplateField ControlStyle-Width="18" >
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="ImageButton1" CssClass="imgEditField" ToolTip="Edit Field Property" ImageUrl="~/App_Static/css/icons16/edit_new.png" runat="server" CommandArgument='<%# String.Format("{0},{1}", Eval("Id"), Eval("Name"))%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <SelectedRowStyle BackColor="#FF6666" CssClass="&lt;font size=&quot;6&quot;&gt;&lt;/font&gt;" />
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <div id="divField" runat="server">
                                <table style="margin-top: -2.5%;">
                                <tr>
                                    <td colspan="2"><h2 runat="server" id="formHeaderField"></h2></td>
                                </tr> 
                                <tr>
                                    <td style="width: 36%;">Field ID</td>
                                    <td><asp:TextBox runat="server"  ID="txtFieldID" Width="300px" Enabled="False"></asp:TextBox> </td>
                                </tr>
                                <tr>
                                    <td style="width: 36%;">Assigned Name</td>
                                    <td>
                                        <asp:TextBox runat="server" ValidationGroup="a" ID="txtAssignedName" Width="300px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="a" ErrorMessage="*" ControlToValidate="txtAssignedName" ForeColor="Red"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>   
                                <tr>
                                    <td>Destination Null Replacement</td>
                                    <td><asp:TextBox runat="server" ID="txtNullReplacement" CssClass="textarea" TextMode="MultiLine" Width="400px" Rows="6"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>Alias Source</td>
                                    <td><asp:TextBox runat="server" ID="txtAllias" Width="300px"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>Source Calculation Expression</td>
                                    <td><asp:TextBox runat="server" ID="txtSourceCal" CssClass="textarea" Width="400px" TextMode="MultiLine" Rows="3"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>Sync Properties</td>
                                    <td><asp:TextBox runat="server" ID="txtSync" CssClass="textarea" Width="400px" TextMode="MultiLine" Rows="8"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>DoNot Insert/Update At Destination</td>
                                    <td>
                                        <asp:RadioButtonList ID="rblRoles" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="False"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:HiddenField ID="fieldHidden" runat="server" />
                                        <asp:Button id="btnFieldSave" CssClass="btnFieldSave" runat="server" Text="Save" />
                                    </td>
                                </tr>
                            </table>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <div id="divTable" runat="server">
                                <table style="margin-top: -2.5%;">
                                <tr>
                                    <td colspan="2"><h2 runat="server" id="formHeaderTable"></h2></td>
                                </tr>
                                <tr>
                                    <td style="width: 37%;">Destination Alias</td>
                                    <td>
                                        <asp:TextBox runat="server" ValidationGroup="a" ID="txtDestAllias" Width="300px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Destination Filter</td>
                                    <td><asp:TextBox runat="server" ID="txtDestFilter" CssClass="textarea" TextMode="MultiLine" Width="400px" Rows="5"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>Abbreviated Name</td>
                                    <td>
                                        <asp:TextBox runat="server" ValidationGroup="a" ID="txtAbbrivateName" MaxLength="10" Width="300px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Sync Properties</td>
                                    <td><asp:TextBox runat="server" ID="txtSyncProp" CssClass="textarea" TextMode="MultiLine" Width="400px" Rows="8"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:HiddenField ID="tableHidden" runat="server" />
                                        <asp:Button id="btnTableSave" CssClass="btnTableSave" runat="server" Text="Save" />
                                    </td>
                                </tr>
                            </table>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
