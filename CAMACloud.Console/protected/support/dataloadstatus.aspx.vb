﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports CAMACloud.BusinessLogic
Imports System.Web.Services
Imports System.Drawing
Public Class dataloadstatus
    Inherits System.Web.UI.Page
    Dim mainTable As New DataTable()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        displayParcelCount.Visible = False
        If Not IsPostBack Then
            mainTable = DataSource.GetParentChildTableInSchema(Database.Tenant)
            BindTree()
            SchemaTables.ExpandAll()
            PanelDisplayContent.Visible = False
        End If
    End Sub
    Private Sub BindTree()
        Dim parcelTable As String = Database.Tenant.Application.ParcelTable
        Dim parcelTableId As Integer = DataSource.GetTableId(Database.Tenant, parcelTable)

        If parcelTableId = -1 Then
            'Schema is not yet available.
            Return
        End If

        Dim dt As New DataTable
        Dim tn As New TreeNode()
        tn.Text = parcelTable
        tn.Value = parcelTableId
        SchemaTables.Nodes.Add(tn)
        PopulateSubLevel(parcelTable, tn)
    End Sub
    Private Sub PopulateNodes(ByVal dt As DataTable, ByVal nodes As TreeNodeCollection)
        For Each dr As DataRow In dt.Rows
            Dim tn As New TreeNode()
            tn.Text = dr("ChildTable").ToString()
            tn.Value = dr("ChildTableId").ToString()
            nodes.Add(tn)
            Dim k As DataRow()
            k = mainTable.Select("ParentTable='" + dr("ChildTable").ToString() + "'")

            If (CInt(k.Length) > 0) Then
                PopulateSubLevel(dr("ChildTable"), tn)
            End If
        Next
    End Sub
    Private Sub PopulateSubLevel(ByVal TableName As String, ByVal parentNode As TreeNode)
        Dim dt As New DataTable()
        dt = mainTable.Select("ParentTable='" + TableName + "'").CopyToDataTable()
        PopulateNodes(dt, parentNode.ChildNodes)
    End Sub
    Protected Sub treeview1_DataBound(sender As Object, e As EventArgs)
        SchemaTables.CollapseAll()
        Dim tn As TreeNode = SchemaTables.SelectedNode
        tn.Expand()
        While tn.Parent IsNot Nothing
            tn = tn.Parent
            tn.Expand()
        End While
    End Sub

    Protected Sub TreeView1_SelectedNodeChanged(sender As Object, e As EventArgs) Handles SchemaTables.SelectedNodeChanged
        Dim TableId As String
        Dim TableName As String
        TableId = SchemaTables.SelectedNode.Value
        TableName = SchemaTables.SelectedNode.Text
        ViewState("TableId") = TableId
        ViewState("TableName") = TableName
        gvParcelCount.Visible = False
        txtKeyValue.Text = Nothing
        lblHead.Text = TableName
        LoadRecordCount(TableId, TableName)
        PanelDisplayContent.Visible = True
    End Sub

    Private Sub LoadRecordCount(tableId As String, tableName As String)
        Dim dt As New DataTable
        Dim sqlstr As String
        Dim ImportType As Integer
        Dim targetTable = DataSource.FindTargetTable(Database.Tenant, tableName)
        If Not targetTable.StartsWith("XT_") Then
            Dim count = Database.Tenant.GetIntegerValue("select  count(*) from " + targetTable)
            displayTotalRecord.Visible = True
            lblTotalRecordCount.Text = String.Format("{0:#,0}", count)
            gvDataCount.Visible = False
        Else
            displayTotalRecord.Visible = False
            gvDataCount.Visible = True
            ImportType = Database.Tenant.GetIntegerValue("select ImportType from DataSourceTable where id=" + tableId)
            If (ImportType = 1) Then
                sqlstr = "SELECT (SELECT count(*) FROM XT_" + tableName + ") AS [TotalRecord],(SELECT count(*) FROM XT_" + tableName + " WHERE CC_RecordStatus <> 'I')  AS [OriginalRecord],(SELECT count(*) FROM XT_" + tableName + " WHERE CC_RecordStatus = 'I')  AS [NewRecord]"
                dt = Database.Tenant.GetDataTable(sqlstr)
                If dt IsNot Nothing Then
                    gvDataCount.DataSource = dt
                    gvDataCount.DataBind()
                End If
            End If
        End If
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        If (txtKeyValue.Text <> "") Then
            gvParcelCount.Visible = True
            Dim TableId As String = ViewState("TableId")
            Dim TableName As String = ViewState("TableName")
            Dim targetTable = DataSource.FindTargetTable(Database.Tenant, TableName)
            Dim CC_ParcelId As String
            CC_ParcelId = Database.Tenant.GetStringValue("select Id from Parcel where KeyValue1={0}".SqlFormatString(txtKeyValue.Text))
            If (CC_ParcelId <> "") Then
                If Not targetTable.StartsWith("XT_") Then
                    Dim count = Database.Tenant.GetIntegerValue("select  count(*) from " + targetTable + " WHERE CC_ParcelId=" + CC_ParcelId)
                    displayParcelCount.Visible = True
                    lblParcelCount.Text = String.Format("{0:#,0}", count)
                    gvParcelCount.Visible = False
                    displayParcelCount.Visible = True
                Else
                    displayParcelCount.Visible = False
                    gvParcelCount.Visible = True
                    Dim dt As New DataTable
                    Dim sqlstr As String
                    Dim ImportType As Integer
                    ImportType = Database.Tenant.GetIntegerValue("select ImportType from DataSourceTable where id=" + TableId)
                    If (ImportType = 1) Then
                        sqlstr = "SELECT (SELECT count(*) FROM XT_" + TableName + "  WHERE CC_ParcelId=" + CC_ParcelId + ") AS [TotalRecord],(SELECT count(*) FROM XT_" + TableName + " WHERE CC_ParcelId=" + CC_ParcelId + " And ClientROWUID is null)  AS [OriginalRecord],(SELECT count(*) FROM XT_" + TableName + " WHERE CC_ParcelId=" + CC_ParcelId + " And CC_RecordStatus = 'I')  AS [NewRecord]"
                        dt = Database.Tenant.GetDataTable(sqlstr)
                        If dt IsNot Nothing Then
                            gvParcelCount.DataSource = dt
                            gvParcelCount.DataBind()
                        End If
                    End If
                End If
            Else
                Alert("Please enter a valid parcel id")
            End If
        Else
            Alert("Please enter parcel Id")
        End If
    End Sub
End Class