﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="PhotoCommitBackup.aspx.vb" Inherits="CAMACloud.Console.PhotoCommitBackup" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <style type="text/css">
	.popupbox {
	    padding: 10px;
	    width: 250px;
	    height: auto;
	    border: 2px solid rgba(204, 204, 204, 0.5);
	    background-color: white;
	    border-radius: 10px;
	    position: absolute;
	    box-shadow: 2px 2px 8px 2px gray;
	
	}
	.popupcontent {
	    overflow-y: auto;
	   /* max-height: 200px; */
	    height: auto;
	    float: right; 
	   /* margin-top: 12px; */
	   /* position:inherit; */
	
	}
	.close-btn {
	    float: right;
	    left: 18px;
	    position: relative;
	    top: -11px;
	    font-weight: bold;
	    font-size: larger;
	    cursor: pointer;
	    /* margin-bottom: 13px; */
	    margin-right: 15px;
	
	}
	.close-btn:hover{
	    font-size:medium;
	
	}
	.errorLink{
	    float:right; 
	    padding-top: 9px;
	
	}
	.popupbox:after { 
	    content: '';
	    display: block;
	    position: absolute;
	    left: 100%;
	    margin-top: -3px;
	    border-top: 10px solid transparent;
	    border-bottom: 10px solid transparent;
	    border-left: 10px solid white;
	
	}
	.changed:after {
	    content: '';
	    display: block;
	    position: absolute;
	    left: 100%;
	    margin-top: 130px;
	    border-top: 10px solid transparent;
	    border-bottom: 10px solid transparent;
	    border-left: 10px solid white;
	
	}
    </style>
    <script type="text/javascript">
    	$(document).ready(function () {
		    $('.close-btn').click(function () {
		        $('.popupbox').fadeOut('fast');
		    });
		});

		$(document).mouseup(function (e) {
		    var container = $(".popupbox");
		    if (!container.is(e.target) // if the target of the click isn't the container...
		        && container.has(e.target).length === 0) // ... nor a descendant of the container
		    {
		        $('.popupbox').fadeOut('slow');
		    }
		});
	    function loadError(source) {
		    $('.popupbox').css("display", "none");
		    var str = $(source).parent().find('.spError').text();
		    str = str.replace(/(?:\r\n|\r|\n)/g, '<br />');
		    if (str != undefined && str != '') {
		        var top;
		        var left;
		        var pos = [];
		        var sourceTop = $(source).offset().top;
		        var sourceLeft = $(source).offset().left;
		        var windowWidth = $(window).width();
		        var windowHght = $(window).height();
		        $(source).parent().find('.popupcontent').html(str);
		        $(source).parent().find('.popupbox').css("display", "inline-block");
		        left = sourceLeft - 283;
		        var _height = $(source).parent().find('.popupbox').height() + 12;
		        if ((windowHght - sourceTop) > _height) {
		            top = sourceTop;
		            $(source).parent().find('.popupbox').removeClass('changed');
		        } else {
		            top = sourceTop - _height - 2;
		            $(source).parent().find('.popupbox').addClass('changed');
		        }
		        pos[0] = top;
		        pos[1] = left;
		        $(source).parent().find('.popupbox').css({ 'top': pos[0] + 'px', 'left': pos[1] + 'px' });
		    }
		}
    </script>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	
	<h1>Photo Commit Backups</h1><br />
	<asp:Panel ID="Panel1" runat="server">
		<asp:GridView ID="gvPhotoCommitList" runat="server" AutoGenerateColumns="False"  PageSize="10" AllowPaging="true" ShowHeaderWhenEmpty="true" width="99%">
    		<Columns>
    		   	<asp:BoundField DataField="UploadTime" HeaderText="Time" DataFormatString="{0:MM-dd-yyyy hh:mm tt}" ItemStyle-Width="130px" />
    	       	<asp:BoundField DataField="UserName" HeaderText="CC User" ItemStyle-Width="90px" />
    	       	<asp:BoundField DataField="IpAddress" HeaderText="IP Address" ItemStyle-Width="45px" />
               	<asp:BoundField DataField="TotalCount" HeaderText="Total" ItemStyle-Width="35px" ></asp:BoundField>
               	<asp:BoundField DataField="InvalidRecords" HeaderText="Invalid" ItemStyle-Width="25px" ></asp:BoundField>
               	<asp:BoundField DataField="SyncStatus" HeaderText="Sync Status" ItemStyle-Width="65px"></asp:BoundField>
			   	<asp:BoundField DataField="Status" HeaderText="Status" ItemStyle-Width="65px"></asp:BoundField>

				<asp:TemplateField ItemStyle-Width="5px" HeaderText="" ItemStyle-HorizontalAlign="center">
                    <ItemTemplate>
                    	<asp:ImageButton ID="catSettings" class="a16 set16" CommandName="Download"  CommandArgument='<%# Eval("Id") %>'  runat="server" BorderStyle="None" ImageUrl="~/App_Static/css/icon16/download.png" ImageAlign="Bottom" ToolTip="Download" style="outline: none;" />
                    </ItemTemplate>
                </asp:TemplateField>
                


				<asp:TemplateField HeaderText="" ItemStyle-Width="20px" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" >
                	<ItemTemplate>
                	<div style= "width:100%">
                	<asp:ImageButton  ID="resultImage"  runat ="server" ImageUrl='<%# If(Eval("Status").ToString() = "Completed", "~/App_Static/css/icon16/done.png", "~/App_Static/css/icons16/del16.gif") %>' ToolTip='<%# Eval("Status") %>' OnClientClick="loadError(this); return false" style="outline: none;"/>
						
	                    <div id="dynamicPopUp" class="popupbox" style="display: none;text-align: left;">
	                        <span class="close-btn" title="Close">&times</span>
	                     <div class="popupcontent"></div>
                    </div>
                	</ItemTemplate>
                </asp:TemplateField>
    	    </Columns>
    	    <EmptyDataTemplate>
                    <label style="color:Red;font-weight:bold">No record found !</label>
             </EmptyDataTemplate>
             <EmptyDataRowStyle HorizontalAlign="Center" />
		</asp:GridView>
	 </asp:Panel>
</asp:Content>

