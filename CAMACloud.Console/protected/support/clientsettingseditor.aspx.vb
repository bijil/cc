﻿Imports System.IO
Public Class clientsettingseditor
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
        	LoadGrid()
        	CancelForm()
        End If
    End Sub

    Sub LoadGrid()
        Dim Sql As String
        Sql = "SELECT * FROM  ClientSettings  ORDER BY Name"
        grid.DataSource = Database.Tenant.GetDataTable(Sql)
        grid.DataBind()
    End Sub

    Private Sub grid_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grid.RowCommand
        Select Case e.CommandName
        	Case "EditItem"
        		Dim type As String = e.CommandArgument
			    type = type.Replace("'","''")
			    Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM  ClientSettings WHERE Name = '" & type & "'")
			    If dr Is Nothing Then
	            	Alert("Client Settings have been modified.Please reload and try again.")
	            Else
	                hname.Value = dr.GetString("Name")
	                txtName.Text = dr.GetString("Name")
					txtValue.Text = dr.GetString("Value")
					'txtName.Enabled = False
					'txtValue.Enabled = True
					btnSave.Text = "Save "
	                btnCancel.Visible = True
	                lblHeadText.Text = "Edit Settings"
                End If
        	Case "DeleteItem"
        	    Dim clnt As String = e.CommandArgument
			    clnt = clnt.Replace("'","''")
            	Database.Tenant.Execute("DELETE FROM  ClientSettings  WHERE Name = '" & clnt & "'")
            	Dim Description As String = "A Client Setting has been deleted."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description) 
                CancelForm()
                LoadGrid()
                
                NotificationMailer.SendNotificationToVendorCDS(Membership.GetUser().ToString, 2, "Client Settings")
        End Select
    End Sub

Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
		Dim isRowExist As Boolean = False
		Dim isSSExist As Boolean = False
		Dim isSSExist1 As Boolean = False
		Dim type As String = txtName.Text.Trim()
		Dim sValue As String = txtValue.Text.Trim()
		If ( type = "DoNotShowLabelDescription" And sValue = "1" ) Then
			isSSExist = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM SketchSettings WHERE Value = '1' AND Name = 'DoNotShowLabelCodeInDropDown' ")
		End If 
		If ( type = "DoNotShowLabelDescriptionSketch" And sValue = "1")
			isSSExist1 = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM SketchSettings WHERE Value = '1' AND Name = 'DoNotShowLabelCode' ")
		End If 
		
		If isSSExist Then
			Alert("Unable to save changes! " + "\n" + "Either the Sketch Code (DoNotShowLabelCodeInDropDown) or Sketch Description (DoNotShowLabelDescription) must be configured to display on the UI.")
		Else If isSSExist1 Then
			Alert("Unable to save changes! " + "\n" + "Either the Sketch Code (DoNotShowLabelCode) or Sketch Description (DoNotShowLabelDescriptionSketch) must be configured to display on the UI.")			
		Else
			If (hname.Value = "") Then
				type = type.Replace("'","''")
				isRowExist = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM ClientSettings WHERE Name='" & type & "'")
			Else
				Dim temphname As String = hname.Value
				temphname = temphname.Replace("'","''")
				Dim sssql As String = "SELECT COUNT(*) FROM ClientSettings WHERE Name='" & temphname & "'"
	    	    isRowExist = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM ClientSettings WHERE Name='" & temphname & "'")
	    	End If    
	    	Dim o As DataRow = Database.System.GetTopRow("SELECT * FROM OrganizationSettings WHERE OrganizationId = " & HttpContext.Current.GetCAMASession.OrganizationId)
	        Select Case txtName.Text.Trim()
	            Case "UseOSM"
	                If (o.GetBoolean("EnableAdvancedMap")) Then
	                    UpdateValue(isRowExist)
	                    If txtValue.Text.Trim() = "0" Then
	                        Database.Tenant.Execute("DELETE FROM  ClientSettings WHERE  Name = 'EnableEagleViewInMA' ")
	                        Database.System.Execute("UPDATE OrganizationSettings SET PictometryInMA=0  WHERE OrganizationId= " & HttpContext.Current.GetCAMASession.OrganizationId & ";")
	                    End If
	                Else
	                        Alert("OSM Map Is Not enabled For this Organization.")
	                End If
	            Case "EnableEagleViewInMA"
	                If (o.GetBoolean("PictometryInMA")) Then
	                    UpdateValue(isRowExist)
	                Else
	                    Alert("EagleView In MA Is Not enabled For this Organization.")
	                End If
	            Case "EnableDisto"
	                If (o.GetBoolean("DistoEnabled")) Then
	                    UpdateValue(isRowExist)
	                Else
	                    Alert("Disto Is Not enabled For this Organization.")
	                End If
	            Case "FutureYearEnabled"
	                If (o.GetBoolean("FYLEnabled")) Then
	                    UpdateValue(isRowExist)
	                Else
	                    Alert("FYL Is Not enabled For this Organization.")
	                End If
	            Case "CurrentYearValue"
	                If (o.GetBoolean("FYLEnabled")) Then
	                    UpdateValue(isRowExist)
	                Else
	                    Alert("FYL Is Not enabled For this Organization.")
	                End If    
	            Case "EnableFieldTracking"
	                If (o.GetBoolean("PCITrackingEnabled")) Then
	                    UpdateValue(isRowExist)
	                Else
	                    Alert("PCI Field Tracking Is Not enabled For this Organization.")
	                End If
	            Case Else
	                UpdateValue(isRowExist)
	                If txtName.Text = "TimeZone" And isRowExist Then
	                    ClientSettings.AlterDateFunctions("TimeZone", txtValue.Text, Database.Tenant)
	                End If
	                If txtName.Text = "DST" And isRowExist Then
	                    ClientSettings.AlterDateFunctions("DST", txtValue.Text, Database.Tenant)
	                End If
	        End Select
	
	        CancelForm()
	        LoadGrid()
	        NotificationMailer.SendNotificationToVendorCDS(Membership.GetUser().ToString, 2, "Client Settings")
		End If
    End Sub
    Private Sub UpdateValue(isRowExist As Boolean)
        Dim Sql As String = ""
        If isRowExist Then
            If (hname.Value = "") Then
                Sql = "UPDATE ClientSettings Set Name ={1},Value = {2}  WHERE Name = {0}"
                Database.Tenant.Execute(Sql.SqlFormat(False, txtName.Text.Trim(), txtName.Text.Trim(), txtValue.Text.Trim()))
            Else
            	Sql = "UPDATE ClientSettings Set Name ={1},Value = {2}  WHERE Name = {0}"
                Database.Tenant.Execute(Sql.SqlFormat(False, hname.Value, txtName.Text.Trim(), txtValue.Text.Trim()))
            End If
            Dim Description As String = "("+txtName.Text.Trim()+","+txtValue.Text.Trim()+") has been updated in Client Settings."
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description) 
        Else
        	Sql = "INSERT INTO ClientSettings (Name, Value) VALUES ( {0}, {1} )"
            Database.Tenant.Execute(Sql.SqlFormat(False, txtName.Text.Trim(), txtValue.Text.Trim()))
            Dim Description As String = "("+txtName.Text.Trim()+","+txtValue.Text.Trim()+") has been Inserted in Client Settings."
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description) 
        End If
        Alert("Setting saved successfully")
    End Sub

Private Sub CancelForm() Handles btnCancel.Click
		hname.Value = ""
        txtName.Text = ""
		txtValue.Text = ""
		'txtName.Enabled = True
		'txtValue.Enabled = True
		btnSave.Text = "Add "
        btnCancel.Visible = False
        lblHeadText.Text = "Add Settings"
    End Sub

    Protected Sub OnPageIndexChanging(sender As Object, e As GridViewPageEventArgs)
    	grid.PageIndex = e.NewPageIndex
        Me.LoadGrid()
    End Sub
    
    Protected Sub ddlPageSize_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
    	grid.PageSize = ddlPageSize.SelectedValue
    	grid.PageIndex = 0
    	LoadGrid()
    End Sub
End Class