﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="prioritylistbackups.aspx.vb" Inherits="CAMACloud.Console.prioritylistbackups" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <style type="text/css">
	.popupbox {
	    padding: 10px;
	    width: 250px;
	    height: auto;
	    border: 2px solid rgba(204, 204, 204, 0.5);
	    background-color: white;
	    border-radius: 10px;
	    position: absolute;
	    box-shadow: 2px 2px 8px 2px gray;
	
	}
	.popupcontent {
	    overflow-y: auto;
	   /* max-height: 200px; */
	    height: auto;
	    float: right; 
	   /* margin-top: 12px; */
	   /* position:inherit; */
	
	}
	.close-btn {
	    float: right;
	    left: 18px;
	    position: relative;
	    top: -11px;
	    font-weight: bold;
	    font-size: larger;
	    cursor: pointer;
	    /* margin-bottom: 13px; */
	    margin-right: 15px;
	
	}
	.close-btn:hover{
	    font-size:medium;
	
	}
	.errorLink{
	    float:right; 
	    padding-top: 9px;
	
	}
	.popupbox:after { 
	    content: '';
	    display: block;
	    position: absolute;
	    left: 100%;
	    margin-top: -3px;
	    border-top: 10px solid transparent;
	    border-bottom: 10px solid transparent;
	    border-left: 10px solid white;
	
	}
	.changed:after {
	    content: '';
	    display: block;
	    position: absolute;
	    left: 100%;
	    margin-top: 130px;
	    border-top: 10px solid transparent;
	    border-bottom: 10px solid transparent;
	    border-left: 10px solid white;
	
	}
    </style>
    <script type="text/javascript">
    	$(document).ready(function () {
		    $('.close-btn').click(function () {
		        $('.popupbox').fadeOut('fast');
		    });
		});

		$(document).mouseup(function (e) {
		    var container = $(".popupbox");
		    if (!container.is(e.target) // if the target of the click isn't the container...
		        && container.has(e.target).length === 0) // ... nor a descendant of the container
		    {
		        $('.popupbox').fadeOut('slow');
		    }
		});
	    function loadError(source) {
		    $('.popupbox').css("display", "none");
		    var str = $(source).parent().find('.spError').text();
		    str = str.replace(/(?:\r\n|\r|\n)/g, '<br />');
		    if (str != undefined && str != '') {
		        var top;
		        var left;
		        var pos = [];
		        var sourceTop = $(source).offset().top;
		        var sourceLeft = $(source).offset().left;
		        var windowWidth = $(window).width();
		        var windowHght = $(window).height();
		        $(source).parent().find('.popupcontent').html(str);
		        $(source).parent().find('.popupbox').css("display", "inline-block");
		        left = sourceLeft - 283;
		        var _height = $(source).parent().find('.popupbox').height() + 12;
		        if ((windowHght - sourceTop) > _height) {
		            top = sourceTop;
		            $(source).parent().find('.popupbox').removeClass('changed');
		        } else {
		            top = sourceTop - _height - 2;
		            $(source).parent().find('.popupbox').addClass('changed');
		        }
		        pos[0] = top;
		        pos[1] = left;
		        $(source).parent().find('.popupbox').css({ 'top': pos[0] + 'px', 'left': pos[1] + 'px' });
		    }
		}
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
      <h1>Priority List Backups</h1><br />
    <asp:Label ID="lbNotification" runat="server" Text=""></asp:Label>
    <asp:Panel ID="Panel1" runat="server">
        <%If EnableNewPriorities Then %>
			<asp:GridView ID="gvPriorityListsNew" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" PageSize="15" AllowPaging="true" ShowHeaderWhenEmpty="true" width="99%">
               <Columns>
                <asp:BoundField DataField="UserName" HeaderText="User" ItemStyle-Width="90px" />
                <asp:BoundField DataField="UploadTime" HeaderText="Date" DataFormatString="{0:MM-dd-yyyy hh:mm tt}" ItemStyle-Width="80px" />
                <asp:BoundField DataField="IPAddress" HeaderText="IP Address" ItemStyle-Width="45px" />
                <asp:BoundField DataField="TotalParcels" HeaderText="Total" ItemStyle-Width="35px" ></asp:BoundField>
				<asp:BoundField DataField="Critical" HeaderText="Critical" ItemStyle-Width="25px"></asp:BoundField>
                <asp:BoundField DataField="Urgent" HeaderText="Urgent" ItemStyle-Width="25px"></asp:BoundField>
                <asp:BoundField DataField="High" HeaderText="High" ItemStyle-Width="32px" ></asp:BoundField>
				<asp:BoundField DataField="Medium" HeaderText="Medium" ItemStyle-Width="25px" ></asp:BoundField>
                <asp:BoundField DataField="Normal" HeaderText="Normal" ItemStyle-Width="25px" ></asp:BoundField>
				<asp:BoundField DataField="Proximity" HeaderText="Proximity" ItemStyle-Width="25px" ></asp:BoundField>
                <asp:BoundField DataField="Invalid" HeaderText="Invalid" ItemStyle-Width="25px" ></asp:BoundField>
                <asp:BoundField DataField="IsResetStatus" HeaderText="Status Reset" ItemStyle-Width="25px" ></asp:BoundField>
                <asp:BoundField DataField="IsAdhoc" HeaderText="Adhoc" ItemStyle-Width="25px" ></asp:BoundField>
                <asp:TemplateField>
                    <HeaderTemplate>Group</HeaderTemplate>
                    <ItemStyle Width="65px" />
                    <ItemTemplate>
                    	<asp:Label runat="server" style="cursor:default" ID="lblComments" Text= <%# If(Eval("GroupCode").ToString().Length > 12, Left(Eval("GroupCode"), 10) + "...", Eval("GroupCode"))%> ToolTip='<%# If(Eval("GroupCode").ToString().Length > 12, Eval("GroupCode"), "")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="AssignTo" HeaderText="Assigned" ItemStyle-Width="90px" />
                <asp:TemplateField ItemStyle-Width="5px" HeaderText="" ItemStyle-HorizontalAlign="center">
                    <ItemTemplate>
                    	<asp:ImageButton ID="catSettings" class="a16 set16" CommandName="Download"  CommandArgument='<%# Eval("Id") %>'  runat="server" BorderStyle="None" ImageUrl="~/App_Static/css/icon16/download.png" ImageAlign="Bottom" ToolTip="Download" style="outline: none;" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Status" ItemStyle-Width="20px" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" >
                	<ItemTemplate>
                	<div style= "width:100%">
                	<asp:ImageButton  ID="resultImage"  runat ="server" ImageUrl='<%# If(Eval("Status").ToString() = "Success", "~/App_Static/css/icon16/done.png", "~/App_Static/css/icons16/del16.gif") %>' ToolTip='<%# Eval("Status") %>' OnClientClick="loadError(this); return false" style="outline: none;"/>
						<span style="display: none;" class="spError"><%# Eval("Error")%></span>
	                    <div id="dynamicPopUp" class="popupbox" style="display: none;text-align: left;">
	                        <span class="close-btn" title="Close">&times</span>
	                     <div class="popupcontent"></div>
                    </div>
                	</ItemTemplate>
                </asp:TemplateField>
              </Columns>
              <EmptyDataTemplate>
                    <label style="color:Red;font-weight:bold">No record found !</label>
              </EmptyDataTemplate>
              <EmptyDataRowStyle HorizontalAlign="Center" />
			</asp:GridView>
		<%Else %>  
			<asp:GridView ID="gvPriorityLists" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" PageSize="15" AllowPaging="true" ShowHeaderWhenEmpty="true" width="99%">
               <Columns>
                <asp:BoundField DataField="UserName" HeaderText="User" ItemStyle-Width="90px" />
                <asp:BoundField DataField="UploadTime" HeaderText="Date" DataFormatString="{0:MM-dd-yyyy hh:mm tt}" ItemStyle-Width="80px" />
                <asp:BoundField DataField="IPAddress" HeaderText="IP Address" ItemStyle-Width="45px" />
                <asp:BoundField DataField="TotalParcels" HeaderText="Total" ItemStyle-Width="35px" ></asp:BoundField>
                <asp:BoundField DataField="Urgent" HeaderText="Urgent" ItemStyle-Width="25px"></asp:BoundField>
                <asp:BoundField DataField="High" HeaderText="High" ItemStyle-Width="32px" ></asp:BoundField>
                <asp:BoundField DataField="Normal" HeaderText="Normal" ItemStyle-Width="25px" ></asp:BoundField>
                <asp:BoundField DataField="Invalid" HeaderText="Invalid" ItemStyle-Width="25px" ></asp:BoundField>
                <asp:BoundField DataField="IsResetStatus" HeaderText="Status Reset" ItemStyle-Width="25px" ></asp:BoundField>
                <asp:BoundField DataField="IsAdhoc" HeaderText="Adhoc" ItemStyle-Width="25px" ></asp:BoundField>
                <asp:TemplateField>
                    <HeaderTemplate>Group</HeaderTemplate>
                    <ItemStyle Width="65px" />
                    <ItemTemplate>
                    	<asp:Label runat="server" style="cursor:default" ID="lblComments" Text= <%# If(Eval("GroupCode").ToString().Length > 12, Left(Eval("GroupCode"), 10) + "...", Eval("GroupCode"))%> ToolTip='<%# If(Eval("GroupCode").ToString().Length > 12, Eval("GroupCode"), "")%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="AssignTo" HeaderText="Assigned" ItemStyle-Width="90px" />
                <asp:TemplateField ItemStyle-Width="5px" HeaderText="" ItemStyle-HorizontalAlign="center">
                    <ItemTemplate>
                    	<asp:ImageButton ID="catSettings" class="a16 set16" CommandName="Download"  CommandArgument='<%# Eval("Id") %>'  runat="server" BorderStyle="None" ImageUrl="~/App_Static/css/icon16/download.png" ImageAlign="Bottom" ToolTip="Download" style="outline: none;" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Status" ItemStyle-Width="20px" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" >
                	<ItemTemplate>
                	<div style= "width:100%">
                	<asp:ImageButton  ID="resultImage"  runat ="server" ImageUrl='<%# If(Eval("Status").ToString() = "Success", "~/App_Static/css/icon16/done.png", "~/App_Static/css/icons16/del16.gif") %>' ToolTip='<%# Eval("Status") %>' OnClientClick="loadError(this); return false" style="outline: none;"/>
						<span style="display: none;" class="spError"><%# Eval("Error")%></span>
	                    <div id="dynamicPopUp" class="popupbox" style="display: none;text-align: left;">
	                        <span class="close-btn" title="Close">&times</span>
	                     <div class="popupcontent"></div>
                    </div>
                	</ItemTemplate>
                </asp:TemplateField>
              </Columns>
              <EmptyDataTemplate>
                    <label style="color:Red;font-weight:bold">No record found !</label>
              </EmptyDataTemplate>
              <EmptyDataRowStyle HorizontalAlign="Center" />
			</asp:GridView>
		<%End If %>
    </asp:Panel>
</asp:Content>