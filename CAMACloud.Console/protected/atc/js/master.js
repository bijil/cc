﻿var map, layerControl;
var _localDb, searchIndex = 0, linkOpenedInQc = false, dbflag = false;
var subTables = [], syncProgressList = [], savedFilterLists = [], savedResultsLists = [], dashboardAlltasks = [], dashboardNbhdtasks = [], userList = [];
var tableList = {}, parcelSearchFields = {}, columnDir = {}, lookup = {};
const atcDataTypes = {
    1: { "cctype": 1, "typename": "Text", "htmltag": "input", "type": "text", "pattern": "", "maxlength": 50, "rows": null, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "string", "jsType": "String", "formatter": "s" },
    2: { "cctype": 2, "typename": "Real Number", "htmltag": "input", "type": "number", "pattern": "^[\-]?([0-9]*(.[0-9]{1,10})?)?$", "maxlength": 15, "rows": null, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "float", "jsType": "Number", "formatter": ",." },
    3: { "cctype": 3, "typename": "Yes/No", "htmltag": "select", "type": null, "pattern": "", "maxlength": null, "rows": null, "multiple": null, "dataSource": "ccma.YesNo", "changeEvent": "change", "dataType": "string", "jsType": "String", "formatter": "s" },
    4: { "cctype": 4, "typename": "Date", "htmltag": "input", "type": "date", "pattern": "", "maxlength": 10, "rows": null, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "date", "jsType": "Date", "formatter": "d" },
    5: { "cctype": 5, "typename": "Lookup", "htmltag": "select", "type": null, "pattern": "", "maxlength": null, "rows": null, "multiple": null, "dataSource": "field.LookupTable", "changeEvent": "change", "dataType": "string", "jsType": "String", "formatter": "s" },
    6: { "cctype": 6, "typename": "Long Text", "htmltag": "textarea", "type": null, "pattern": "", "maxlength": 200, "rows": 2, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "string", "jsType": "String", "formatter": "s" },
    7: { "cctype": 7, "typename": "Money", "htmltag": "input", "type": "number", "pattern": "^([0-9]*(.[0-9]{1,4})?)?$", "maxlength": 15, "rows": null, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "money", "jsType": "Number", "formatter": "$" },
    8: { "cctype": 8, "typename": "Whole Number", "htmltag": "input", "type": "number", "pattern": "^(?:[1-9][0-9]*|0)$", "maxlength": 15, "rows": null, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "int", "jsType": "Number", "formatter": "," },
    9: { "cctype": 9, "typename": "Cost Value", "htmltag": "input", "type": "number", "pattern": "^[0-9]*$", "maxlength": 15, "rows": null, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "value", "jsType": "Number", "formatter": "$$$" },
    10: { "cctype": 10, "typename": "Year", "htmltag": "input", "type": "number", "pattern": "^(0|9999|[12][0987][0-9][0-9])?$", "maxlength": 4, "rows": null, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "year", "jsType": "Number", "formatter": "i" },
    11: { "cctype": 11, "typename": "Lookup List", "htmltag": "select", "type": null, "pattern": "", "maxlength": null, "rows": null, "multiple": "multiple", "dataSource": "field.LookupTable", "changeEvent": "change", "dataType": "string", "jsType": "String", "formatter": "s" },
    12: { "cctype": 12, "typename": "Geo Location", "baseType": "text", "htmltag": "input", "type": "text", "pattern": "", "maxlength": 30, "rows": null, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "string", "jsType": "String", "formatter": "g" },
    13: { "cctype": 13, "typename": "Tri State Radio", "baseType": "text", "htmltag": "radiogroup", "type": null, "pattern": "", "maxlength": null, "rows": null, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "string", "jsType": "String", "formatter": "s" }
}

$(document).ready(() => {
    atcInitApplication();
    pageActions.globalListeners();

    $('.sb__item').on('click', function (e) {
        pageActions.openPage($(this).attr('sb-page'));
    });

    $('.sb__close').on('click', function (e) {
        pageActions.closePage();
        map.invalidateSize();
    });

});

const pageActions = {
    openPage: function (page, parms) {
        let sbItem = $(`.sb__item[sb-page=${page}]`), sbItemOthers = $(`.sb__item:not([sb-page=${page}])`);
        let sbPage = $(`.sb__page[sb-id=${page}]`), sbPageOthers = $(`.sb__page:not([sb-id=${page}])`);
        let prevPage = $('.sb__page--active').attr('sb-id');

        if (sbItem && sbPage) {
            sbItemOthers.removeClass('sb__item--active');
            sbPageOthers.removeClass('sb__page--active');
            sbItem.addClass('sb__item--active');
            sbPage.addClass('sb__page--active');
            $('.sb__close').show();
            pageActions.clearSearch(page);
            pageActions.onChange(page, parms, prevPage);
        } else console.error('Invalid page identifier - ' + page);
    },
    closePage: function () {
        $('.sb__close').hide();
        $(`.sb__item`).removeClass('sb__item--active');
        $(`.sb__page`).removeClass('sb__page--active');
    },
    globalListeners: function () {
        //Card detail expand on accordion button click
        $(document).on('click', '.card .card__accordion', function (e) {
            let cd = $('.card__detail', $(this).parents('.card')),
                ca = $('.card__accordion', $(this).parents('.card'))
            if (cd.hasClass('card__detail--expanded')) {
                cd.slideUp(() => { cd.removeClass('card__detail--expanded'); });
                ca.removeClass('card__accordion--expanded');
            } else {
                cd.slideDown(() => { cd.addClass('card__detail--expanded'); });
                ca.addClass('card__accordion--expanded');
            }
        });

        //Search page Tristate toggle switch
        $(document).on('change', '.stoggle input[type=radio]', function () {
            let grp = $(this).attr('name');
            let val = $(this).attr('r-val');
            initSearch.toggleTriSwitch(grp, val);
        });

        //Common search button at the top
        $('.sb__button-search .sb__search-icon').click(function () {
            let ic = $(this), pt = $(this).parents('.sb__page-buttons'), sd = $('.sb__search-details', pt);
            $('span.btn:not(.sb__button-search,.sb__search-cancel)', pt).hide();
            ic.hide();
            sd.css('display', 'flex');
        })

        //Dashboard Card Click Accordion
        $(document).on('click', '.dash__card .dash__card-title', function (e) {
            let cd = $('.card__detail', $(this).parents('.dash__card')),
                ca = $('.card__accordion', $(this).parents('.dash__card'))
            if ($('.dash__card-agroup', cd).length < 1) return false;
            if (cd.hasClass('card__detail--expanded')) {
                cd.slideUp(() => { cd.removeClass('card__detail--expanded'); });
                ca.removeClass('card__accordion--expanded');
            } else {
                cd.slideDown(() => { cd.addClass('card__detail--expanded'); });
                ca.addClass('card__accordion--expanded');
            }
        });

        //To close marker popups when click is not in the popup
        $(document).on('click', function (e) {
            if (!document.querySelector('#map').contains(e.target)) {
                map.closePopup();
            }
        });

    },
    onChange: function (page, parms, prevPage) {
        const fn = {
            'dashboard': dashboard.init,
            'search': initSearch.initView,
            'filter': saveFilters.init,
            'result': searchSavedResults.init,
            'group': assignmentGroupStatus.init,
            'priority': priority.init
        };
        pageActions.layerSettings(false);
        dashboard.userPopup(false);
        if (page == 'dashboard') {
            pageActions.toggleDashboard(true);
        }
        if (prevPage == 'dashboard' && page != 'dashboard') {
            pageActions.toggleDashboard(false);
        } else if (prevPage == 'priority') {
            atcMap.reset();
            pageActions.setTitle(false);
            atcMapLayers.addLayerButton(false);
            $('.sb__page[sb-id="priority"] input').val('');
        }
        let sbFun = fn[page];
        if (sbFun && typeof sbFun === 'function') sbFun(parms);
    },
    loading: function (flag, page) {
        let ctx = $(`.sb__page[sb-id="${page}"]`);
        if (flag) {
            $(`.sb__page-bar`, ctx).addClass('sb__page-loading');
            $(`.sb__page-content,.sb__page-footer`, ctx).hide();
        } else {
            $(`.sb__page-bar`, ctx).removeClass('sb__page-loading');
            $(`.sb__page-content,.sb__page-footer`, ctx).show();
        }
    },
    prompt: function (page, title, saveFn, cancelFn) {
        pageActions.loading(true, page);
        $('.sb__input-popup', page).remove();
        let ctx = $(`.sb__page[sb-id="${page}"]`);
        let ip = $('<input/>').attr({ 'type': 'text', 'maxlength': '50', class: 'sb__input-name', onchange: "$(this).siblings('.sb__validate').hide()" });
        let vd = $('<span/>').attr({ class: 'sb__validate' });
        let sv = $('<span/>').attr('class', 'btn btn--small btn--blue').text('Save');
        let cn = $('<span/>').attr('class', 'btn btn--small').text('Cancel');
        let hd = $('<p />').attr('class', '').text(title || '');
        let btn = $('<div />').attr('class', 'sb__input-btn').append(sv, cn)

        let div = $('<div/>').attr('class', 'sb__input-popup popup-dg').append(hd, ip, vd, btn);
        ctx.append(div);
        sv.click(function () {
            if (saveFn && typeof saveFn === 'function') {
                saveFn($('.sb__input-name').val(), div, (status) => {
                    if (status) {
                        pageActions.loading(false, page);
                        div.remove();
                    }
                    else {
                        return false;
                    }
                });
            }
            else {
                pageActions.loading(false, page);
                div.remove();
            }
        })
        cn.click(function () {
            if (cancelFn && typeof cancelFn === 'function') cancelFn();
            pageActions.loading(false, page);
            div.remove();
        })
    },
    clearSearch: function (page) {
        let ctx = $(`.sb__page[sb-id="${page}"]`);
        let pt = $('.sb__page-buttons', ctx);
        $('span.btn:not(.sb__button-search)', pt).show();
        $('.sb__search-icon', ctx).show();
        $('.sb__search-details', ctx).hide();
        $('.sb__search-input', ctx).val('');
        if (page == 'filter') saveFilters.clearfilterSearch();
        else if (page == 'result') searchSavedResults.clearResultSearch();
        else if (page == 'group') assignmentGroupStatus.clearResultSearch();
        else if (page == 'dashboard') dashboard.cleardashboardSearch();
    },
    setTitle: function (title, append) {
        let mt = $('.map__title');
        if (title) {
            if (append) title = $('p', mt).text() + " , " + title;
            $('p', mt).text(title);
            mt.show();
        } else {
            $('p', mt).text('');
            mt.hide();
        }
    },
    mapButton: function (group, result, list, image, lasso, lassoExport, lassoRemove) {
        let bGroup = $('.mfab .mfab__group'),
            bResult = $('.mfab .mfab__result'),
            bList = $('.mfab .mfab__list'),
            bImage = $('.mfab .mfab__image'),
            //bPriority = $('.mfab .mfab__priority'),
            bLasso = $('.mfab .mfab__lasso'),
            blassoExport = $('.mfab .mfab__lasso-list');
            bLassoRemove = $('.mfab .mfab__lasso-clear');

        //Give -1 if you don't want to change the visibility and keep it however they are
        (group != -1) && bGroup[group ? 'show' : 'hide']();
        (result != -1) && bResult[result ? 'show' : 'hide']();
        (list != -1) && bList[list ? 'show' : 'hide']();
        (image != -1) && bImage[image ? 'show' : 'hide']();
        //(priority != -1) && bPriority[priority ? 'show' : 'hide']();
        (lasso != -1) && bLasso[lasso ? 'show' : 'hide']();
        (lassoExport != -1) && blassoExport[lassoExport ? 'show' : 'hide']();
        (lassoRemove != -1) && bLassoRemove[lassoRemove ? 'show' : 'hide']();
    },
    popup: function (el) {
        let $el = $(el, '.mask__atc');
        $('.popup-atc', '.mask__atc').fadeOut();
        $('.mask__atc').css('display', (el != false ? 'flex' : 'none'));
        if ($el.hasClass('layer-pop')) {
            $('.layer-pop .sb__validate').text("").hide();
            $('input,select', $el).val('');
        }
        if ($el && $el.length) $el.fadeIn();
    },
    layerSettings: function (flag) {
        // flag value [ true : show, false : hide, -1 : toggle]
        let ls = $('.layer-select');

        flag == -1 ? ls.fadeToggle() : ls[flag ? 'fadeIn' : 'fadeOut']();
    },
    toggleDashboard: function (flag) {
        $('.mv__dash')[flag ? 'show' : 'hide']();
    }
}

const dashboard = {
    taskChart: null,
    pendingPie: null,
    secondChart: null,
    init: function (callback) {
        pageActions.toggleDashboard(true);
        PageMethods.GetAssignedTaskList((atcList) => {
            if (atcList.Success) {

                dbflag = true;

                dashboardAlltasks = atcList.Data.allTasks;
                dashboardNbhdtasks = atcList.Data.nbhdTasks;

                dashboard.bindData(() => {
                    pageActions.loading(false, 'dashboard');
                    pageTour.startTour();
                    if (callback) callback();
                });
            }
            else
                console.error(atcList.ErrorMessage);
        });

        dashboard.loadDashboardData();
    },
    plotPoints: function (current) {
        let nbhdId = $(current).attr('nbhdId'), nbhd = $(current).attr('nbhd');
        dashboard.userPopup(false);
        pageActions.setTitle(false);
        pageActions.toggleDashboard(false);
        loading.show(`Loading Group '${nbhd}'`);
        nbhdId = (((nbhdId || nbhdId === 0) && nbhdId != 'null') ? nbhdId : -1);
        if (nbhdId == -1 && nbhd == "") {
            setTimeout(function () { loading.hide(); }, 500);
            return true;
        }
        else {
            PageMethods.GetParcelPoints(nbhdId, nbhd, (parcelPoints) => {
                if (parcelPoints.Success) {
                    let parcels = parcelPoints.Data.parcelPoints;
                    initSearch.searchList = [];
                    atcMapLayers.plotMarkers(parcels, null, () => {
                        pageActions.setTitle(`Viewing Group '${nbhd}' (${parcels.length?.toLocaleString('en-US') && (parcels.length?.toLocaleString('en-US'))} parcels)`);
                        pageActions.mapButton(0, 0, 0, 1, 1);
                        loading.hide();
                    });
                    return true;
                }
                else {
                    console.error(parcelPoints.ErrorMessage);
                    pageActions.mapButton();
                    setTimeout(function () { loading.hide(); }, 500);
                    return true;
                }
            });
        }
    },
    hideEmptyArrow: function (s) {
        return s > 0 ? '' : 'card__accordion--hidden';
    },
    recentGroupCount: function (s) {
        return s > 0 ? 'dash__card--click' : '';
    },
    filterSearch: function (el) {
        let val = $(el).val();
        dashboard.bindData(null, val);
    },
    cleardashboardSearch: function () {
        dashboard.bindData();
    },
    bindData: function (callback, val) {

        if (!dbflag) return;

        let arr = [...dashboardAlltasks];

        if (val) {
            arr = dashboardAlltasks.filter((x) => {
                let nm = x?.CurrentNbhd;
                if (nm && nm.toLowerCase().includes(val.toLowerCase())) return x;
            })
        }
        if (arr.length > 0) {
            let b = [];

            arr.forEach((dfl) => {
                b.push(_.clone({ AllNbhds: dfl.AllNbhds, AllTasks: dfl.AllTasks.toLocaleString('en-US'), CompletedTasks: dfl.CompletedTasks.toLocaleString('en-US'), CurrentNbhd: dfl.CurrentNbhd, CurrentPercentDone: dfl.CurrentPercentDone, FullName: dfl.FullName, LoginId: dfl.LoginId, NbhdId: dfl.NbhdId, OverallPercentDone: dfl.OverallPercentDone, PendingTasks: dfl.PendingTasks.toLocaleString('en-US') }));
            });

            let alltasks = b;
            let nbhdtasks = dashboardNbhdtasks;
            let uniqueTasks = [], nbhdGroups = {};

            alltasks.forEach(function (task) {
                if (uniqueTasks.filter(function (ut) { return ut.FullName == task.FullName }).length == 0) {
                    uniqueTasks.push(_.clone(task));
                    nbhdGroups[task["FullName"]] = [task.NbhdId];
                }
                else
                    nbhdGroups[task.FullName].push(task.NbhdId);
            });

            uniqueTasks.forEach((d, i) => {
                let taskcount = d["AllNbhds"], nbhds = [];
                d["subNbhds"] = [];
                if (taskcount < 1) {
                    d["subNbhds"].push(_.clone({
                        Id: d["NbhdId"],
                        Nbhd: d["CurrentNbhd"] ? d["CurrentNbhd"] : '',
                        CompletedTasks: (d["AllTasks"] - d["PendingTasks"]).toLocaleString('en-US'),
                        pending: d["PendingTasks"].toLocaleString('en-US'),
                        TotalTasks: d["AllTasks"].toLocaleString('en-US'),
                        PercentDone: d["AllTasks"]
                    }));
                }
                else {
                    let t = nbhdtasks.filter((x) => { return (x.LoginId == d.LoginId) }).map((x) => { return x['LastVisitedDate'] });
                    let maxDate = new Date(Math.max.apply(null, t));
                    let ft = nbhdtasks.filter((x) => { return (x.LoginId == d.LoginId && x.LastVisitedDate && x.LastVisitedDate.toLocaleString() == maxDate.toLocaleString()) });
                    ft.forEach((n) => {
                        if (!val || val == '' || (val && n.Nbhd && (n.Nbhd.toLowerCase().includes(val.toLowerCase())))) {
                            n.CompletedTasks = n.CompletedTasks ? n.CompletedTasks.toLocaleString('en-US') : n.CompletedTasks;
                            n.pending = n.pending ? n.pending.toLocaleString('en-US') : n.pending;
                            n.TotalTasks = n.TotalTasks ? n.TotalTasks.toLocaleString('en-US') : n.TotalTasks;
                            d["subNbhds"].push(_.clone(n));

                        }
                    });
                }
                d["RecentGroups"] = d["subNbhds"]?.length;
            });

            if (val) {
                uniqueTasks.forEach((x, i) => {
                    if (x["subNbhds"].length == 0)
                        uniqueTasks.splice(i, 1);
                });
            }

            if (uniqueTasks.length > 0) {
                dashboard.bind(uniqueTasks);

                if (val)
                    dashboard.expandall();
            }
            else {
                $('.dashboard__empty').show();
                $("#dash__cardId").empty().hide();
            }
        }
        else {
            $('.dashboard__empty').show();
            $("#dash__cardId").empty().hide();
        }

        if (callback) callback();
    },
    bind: function (b) {
        $("#dash__cardId").bindData(b);
        $("#dash__cardId").show();
        $('.dashboard__empty').hide();
    },
    expandall: function () {
        $(".dash__card-title").each(function (e) {
            let cd = $('.card__detail', $(this).parents('.dash__card')),
                ca = $('.card__accordion', $(this).parents('.dash__card'))
            if ($('.dash__card-agroup', cd).length < 1) return false;
            if (cd.hasClass('card__detail--expanded')) {
                cd.slideUp(() => { cd.removeClass('card__detail--expanded'); });
                ca.removeClass('card__accordion--expanded');
            } else {
                cd.slideDown(() => { cd.addClass('card__detail--expanded'); });
                ca.addClass('card__accordion--expanded');
            }
        });
    },
    bindUser: function (el) {
        dashboard.userPopup(true);
        let lId = $(el).parent().attr('loginid');
        let un = $(el).attr('title');
        if (lId) {
            PageMethods.UserActionDates(lId, (res) => {
                if (res.Success) {
                    let b = { Username: un, MaLoginDate: res.MaLoginDate, DownloadDate: res.DownloadDate, DataSyncDate: res.DataSyncDate, QCAccessDate: res.QCAccessDate, subNbhds: [] };
                    let t = dashboardNbhdtasks.filter((x) => { return (x.LoginId == lId) });
                    t.forEach((n) => {
                        b["subNbhds"].push(_.clone(n));
                    });
                    let userTemplate = $('#user-pop');
                    $(userTemplate).bindData(b);
                    dashboard.userPopup(true, false);
                }
                else {
                    console.error(res.ErrorMessage);
                }
            });
        }
    },
    userPopup: function (flag, loading = true) {
        let uPop = $('#user-pop'),
            uLoad = $('.user-pop__loading', uPop),
            uData = $('.user-pop__data', uPop);
        uData.hide();
        if (flag) {
            if (loading) {
                uData.fadeOut((x) => {
                    uLoad.fadeIn();
                });
            }
            else {
                uLoad.fadeOut((x) => {
                    uData.fadeIn();
                });
            }
            $(uPop).fadeIn();
        } else {
            $(uPop, uData, uLoad).fadeOut();
        }
    },
    reloadDashboard: function () {
        pageActions.loading(true, 'dashboard');
        dashboard.init();
        atcMap.reset();
    },
    loadDashboardData: () => {
        PageMethods.GetDashboardData((data) => {
            if (data && data.Success) {
                let recents = data.Data?.RecentGroups;
                let recentToday = data.Data?.RecentGroupsToday;
                dashboard.loadDashRecent(recents, recentToday);

                // Task Count Pie Chart
                let taskCount = data.Data?.TaskCount?.[0]?.toLocaleString('en-US');
                dashboard.loadPendingPieChart(taskCount);

                let groupCount = data.Data?.GroupCount?.[0];
                let parcelCount = data.Data?.ParcelCount?.[0];
                dashboard.loadGroupCount(groupCount, parcelCount);

                let weeklyTask = data.Data?.WeeklyTask;
                dashboard.loadTaskTrendChart(weeklyTask);

                let totalCount = data.Data?.TotalCount?.[0];
                dashboard.loadTotalCount(totalCount);
            } else console.error('error');
        });
        dashboard.loadSecondTrendChart();
    },
    loadDashRecent: (groupsWeek, groupsToday) => {
        let ctx = $('.dt__recent');
        $('.dt__recent-switch').off('click').on('click', function () {
            $(this).toggleClass('dt__recent-switch--week dt__recent-switch--today');
            if ($(this).hasClass('dt__recent-switch--week')) {
                showData(groupsWeek);
            } else showData(groupsToday);
        });
        function showData(dt) {
            $('.dt__recent-links', ctx).empty();
            let n = dt?.length, names = '';
            if (n) {
                dt?.forEach(item => {
                    $('.dt__recent-links', ctx).append($('<span>').text(item.Number + ' (' + item.AssignedTo + ')').attr('class', 'dt__recent-name text-ellipsis'));
                });
                //names = names.substring(0, names.length - 2);
            } else $('.dt__recent-links', ctx).html('No groups worked in the period');
            $('span.count', ctx).text(n);
        }
        $('.dt__recent-switch').removeClass('dt__recent-switch--week').addClass('dt__recent-switch--today');
        showData(groupsToday);
    },
    loadGroupCount: (groupCount, parcelCount) => {
        let ctx = $('.dt__total-progress');

        function showData(div, count, text) {
            let completed = count?.Completed, total = count?.Total, percent = 0;
            if (completed > 0 && total > 0) percent = Math.round((completed / total) * 100);
            $('progress', div).val(percent).attr('title', `${completed.toLocaleString('en-US')} ${text} completed out of ${total.toLocaleString('en-US')}`);
            $('.count', div).text(`${percent}%`).attr('title', `${percent}% ${text} completed`);
            $('.sub-count', div).text(`(${completed.toLocaleString('en-US')}/${total.toLocaleString('en-US')})`).attr('title', `${completed.toLocaleString('en-US')} ${text} completed out of ${total.toLocaleString('en-US')}`);
        }

        showData($('.dt__groups', ctx), groupCount, 'groups');
        showData($('.dt__parcels', ctx), parcelCount, 'parcels');
    },
    loadPendingPieChart: (tasks) => {
        let ctx = document.getElementById('pendingPieChart'), pendingPie = dashboard.pendingPie;
        if (pendingPie) {
            pendingPie.destroy();
        }
        const config = {
            type: 'doughnut',
            data: {
                labels: ['Urgent Priorities', 'High Priorities', 'Normal Priorities'],
                datasets: [{
                    label: 'My First Dataset',
                    data: [tasks?.PendingAlerts,
                    tasks?.PendingPriorities,
                    tasks?.PendingParcels],
                    backgroundColor: [
                        'rgb(255, 99, 132)',
                        'rgb(255, 205, 86)',
                        'rgb(54, 162, 235)'
                    ],
                    hoverOffset: 4
                }]
            },
            options: {
                responsive: true,
                plugins: {
                    legend: {
                        display: false
                    },
                }
            }
        };
        dashboard.pendingPie = new Chart(ctx, config);
    },
    loadTaskTrendChart: (weeklyTask) => {
        //const ctx = document.getElementById('lastWeekTasks');
        //debugger;
        let dArray = [], tArray = weeklyTask;
        const dates = [...Array(7)].map((_, i) => {
            const d = new Date();
            d.setDate(d.getDate() - i);
            return d.toLocaleDateString();
        }).reverse();

        //if (tArray && tArray?.length > 0) {
        for (let dt of dates) {
            let tLen = tArray.length;
            if (tLen && dt == new Date(tArray[tLen - 1]?.Date).toLocaleDateString()) {
                dArray.push(tArray[tLen - 1]?.Completed);
                if (tLen) tArray.pop();
            } else dArray.push(0);
        };
        //}

        let ctx = document.getElementById('lastWeekTasks'), taskChart = dashboard.taskChart;
        if (taskChart) {
            taskChart.destroy();
        }
        let gradient = ctx.getContext("2d").createLinearGradient(0, 0, 0, 400);
        gradient.addColorStop(0, '#ff5559');
        gradient.addColorStop(1, '#fff');
        const config = {
            type: 'line',
            data: {
                labels: dates,
                datasets: [{
                    label: 'Tasks completed',
                    data: dArray,
                    fill: true,
                    backgroundColor: gradient,
                    borderColor: '#de2d2d',
                    tension: 0.3,
                    borderWidth: 2
                }]
            },
            options: {
                responsive: true,
                legend: {
                    display: false
                },
                scales: {
                    x: {
                        grid: {
                            display: false
                        }
                    },
                    y:
                    {
                        beginAtZero: true,
                        grid: {
                            display: false
                        }
                    }
                }
            }
        };
        dashboard.taskChart = new Chart(ctx, config);
    },
    loadSecondTrendChart: () => {
        let ctx = document.getElementById('lastWeekSecond'), secondChart = dashboard.secondChart;
        if (secondChart) {
            secondChart.destroy();
        }
        const dates = [...Array(7)].map((_, i) => {
            const d = new Date();
            d.setDate(d.getDate() - i);
            return d.toLocaleDateString();
        }).reverse();
        const config = {
            type: 'line',
            data: {
                labels: dates,
                datasets: [{
                    label: 'Urgent Parcels',
                    data: [14, 21, 16, 7, 10, 22, 8],
                    borderColor: '#de2d2d',
                    tension: 0.3,
                    borderWidth: 2
                }, {
                    label: 'High Parcels',
                    data: [25, 10, 12, 14, 20, 6, 0],
                    borderColor: '#ffcd56',
                    tension: 0.3,
                    borderWidth: 2
                }, {
                    label: 'Normal Parcels',
                    data: [12, 62, 45, 87, 34, 14, 5],
                    borderColor: '#36a2eb',
                    tension: 0.3,
                    borderWidth: 2
                },]
            },
            options: {
                responsive: true,
                legend: {
                    display: false
                },
                scales: {
                    x: {
                        grid: {
                            display: false
                        }
                    },
                    y:
                    {
                        beginAtZero: true,
                    }
                }
            }
        };
        dashboard.secondChart = new Chart(ctx, config);
    },
    loadTotalCount: (tc) => {
        $('.dt__group-count .count').text(tc?.GroupCount?.toLocaleString('en-US') || '--');
        $('.dt__parcel-count .count').text(tc?.ParcelCount?.toLocaleString('en-US') || '--');
        $('.dt__priority-count .count').text(tc?.PriorityCount?.toLocaleString('en-US') || '--');
        $('.dt__user-count .count').text(tc?.UserCount?.toLocaleString('en-US') || '--');
    }

}

const initDb = {
    init: function (callback) {
        if (typeof(openDatabase) == 'function') {
            _localDb = openDatabase('ATCMAPVIEW', '1.0', 'CAMACloud', 500 * 1024 * 1024);
        }
        else {
            _localDb = new SQL.Database();
            _localDb.transaction = function (callback) {
                var tx = {
                    executeSql: function (query, params, success, failure) {
                        var _tx = this;
                        var resp = {
                            rows: {
                                length: 0,
                                item: function (i) {
                                    return this[i];
                                }
                            }
                        };
                        try {
                            if (params?.length == 0) {
                                var raw = _localDb.exec(query)[0];
                                if (raw) {
                                    var values = raw.values;
                                    resp = {
                                        rows: {
                                            length: values.length,
                                            item: function (i) {
                                                return this[i];
                                            }
                                        }
                                    };

                                    for (var i = 0; i < values.length; i++) {
                                        resp.rows[i] = {};
                                        var k = 0;
                                        for (var c of raw.columns) {
                                            resp.rows[i][c] = values[i][k];
                                            k++;
                                        }
                                    }

                                }
                            }
                            else {
                                _localDb.run(query, params);
                            }
                        }
                        catch (ex) {
                            failure && failure(_tx, ex);
                            return;
                        }

                        success && success(_tx, resp);
                    }
                }

                callback && callback(tx);
            }
        }
        if (callback) callback();
    },
    cachelookupTable: function (callback) {
        $.ajax({
            url: '/sv/lookupdatatables.jrq',
            data: {},
            dataType: 'json',
            success: function (data) {
                var tableName = '*ld*';
                if ((data && data.length != undefined)) {
                    _syncSubTables(tableName, data, function () {
                        if (callback) callback();
                    });
                }
            },
            error: function (req, err, msg) {
                alert('Data synchronization failed!');
            }
        });
    },
    parcelDataLookup: function (callback) {
        PageMethods.GetParcelLookups((parcelLookups) => {
            if (parcelLookups.Success) {
                let lv = parcelLookups.Data.parcelLookups;
                tableList.Neighborhoods = parcelLookups.Data.Nbhdlist ? parcelLookups.Data.Nbhdlist : [];
                tableList.Users = parcelLookups.Data.Users ? parcelLookups.Data.Users : [];
                for (var x in lv) {
                    let f = lv[x].Source, v = lv[x].Value.trim();
                    let n = lv[x].Name, d = lv[x].Description;
                    let o = parseInt(lv[x].Ordinal), t = lv[x].SortType;
                    let color = lv[x].Color ? lv[x].Color : '', NumericValue = lv[x].NumericValue;
                    if (lookup[f] === undefined)
                        lookup[f] = {};
                    if (lookup[f][v] === undefined)
                        lookup[f][v] = { Name: n, Description: d, Ordinal: o, SortType: t, Id: v, color: color, NumericValue: NumericValue, AdditionalValue1: lv[x].AdditionalValue1, AdditionalValue2: lv[x].AdditionalValue2 };
                }
                if (callback) callback();
            }
            else {
                console.error(parcelLookups.ErrorMessage);
            }
        });
    }
}

const initSearch = {
    parcelList: [],
    searchList: [],
    lookupLoaded: false,
    init: function (callback) {
        PageMethods.GetSearchFields((searchFields) => {
            if (searchFields.Success) {
                let searchLists = searchFields.Data.searchFields;
                tableList.dtrStatus = searchFields.Data.dtrStatus;
                for (let x in searchLists) {
                    let n = searchLists[x];
                    for (let f in n) {
                        if (n[f] == 'true') n[f] = true;
                        if (n[f] == 'false') n[f] = false;
                    }
                    parcelSearchFields[n.Id.toString()] = n;
                }
                let html = "<option value=''>-- Select --</option><option value='0'>Parcel</option><option value='1'>Assignment Group</option><option value='2'>CC Tag</option><option value='3'>Street Address</option><option value='4'>Priority</option><option value='5'>Desk Review Status</option><option value='6' disabled='disabled'>─────────────────────────────────</option>";
                searchLists.forEach((sl) => {
                    html += "<option value=" + sl.value + ">" + sl.DisplayLabel + "</option>";
                    if (sl.LookupTable == "$QUERY$")
                        getLookupData(sl, {}, function () { });
                });

                let uhtml = "<option value=''>-- Select --</option>";
                tableList.Users.forEach((u) => {
                    uhtml += "<option value=" + u.Id + ">" + u.Name + "</option>";
                });

                let searchOperators = [{ Id: "", Name: "--Select--" }, { Id: "cn", Name: "Contains" }, { Id: "eq", Name: "Equal to" }, { Id: "ne", Name: "Not equal to" }, { Id: "sw", Name: "Starting with" }, { Id: "nsw", Name: "Not starting with" }, { Id: "ew", Name: "Ending with" }, { Id: "lt", Name: "Less Than" }, { Id: "gt", Name: "Greater Than" }, { Id: "le", Name: "Less than or equal" }, { Id: "ge", Name: "Greater than or equal" }, { Id: "nn", Name: "Is NOT NULL" }, { Id: "nl", Name: "Is NULL" }, , { Id: "bw", Name: "Between" }];
                let ophtml = "", dhtml = "";
                searchOperators.forEach((s) => {
                    let dis = (s.Id == '') ? "disabled='disabled'" : '';
                    ophtml += "<option " + dis + " value=" + s.Id + ">" + s.Name + "</option>";
                    if (['cn', 'sw', 'nsw', 'ew', 'ne'].indexOf(s.Id) == -1)
                        dhtml += "<option value=" + s.Id + ">" + s.Name + "</option>";
                });

                $('.search__by').append(html);
                $('.search__MAuser').append(uhtml);
                $('.search__DTRuser').append(uhtml);
                $('.search__QCuser').append(uhtml);
                $('.search__MAdate__opr').append(dhtml);
                $('.search__DTRdate__opr').append(dhtml);
                $('.search__QCdate__opr').append(dhtml);
                $('.search__op').append(ophtml);
                $('.search__op').attr('disabled', 'disabled');
                $('.srch__Id').attr('disabled', 'disabled');
                $('.search__op').val('');
                $('.srch__Id2').hide();
                initSearch.bindOnchange(() => {
                    initSearch.bindOnclick(() => {
                        if (callback) callback();
                    });
                });
            }
            else {
                console.error(searchFields.ErrorMessage);
            }
        });
    },
    initView: function (parms) {
        pageTour.startTour();
        initSearch.clearAll();
        initSearch.toggleAddFilter(false);
        currentSearchTemplate.clear();
        searchSavedResults.result = {};
        if (!(parms && parms.search))
            saveFilters.init(true);
        return;
    },
    bindOnchange: function (callback) {
        $('.search__by').off('change');
        $('.search__op').off('change');
        $('.search__MAdate__opr, .search__DTRdate__opr, .search__QCdate__opr').off('change');
        $('.search__by').on('change', function () {
            initSearch.hideDisableAll();
            if ($(this).val() != "") {
                let splitValue = $(this).val().split('/');
                $('.search__op').removeAttr('disabled');

                if (splitValue[0] == '0' || splitValue[0] == '2' || splitValue[0] == '3') {
                    $('.srch__Id').show(); $('.srch__Id').removeAttr('disabled');
                    initSearch.disableOperators(['gt', 'ge', 'le', 'lt', 'bw']);
                    $('.search__op').val('cn');
                }
                else if (splitValue[0] == '4') {
                    $('.srch__Id2').show(); $('.srch__Id2').removeAttr('disabled');
                    let l = [{ Id: '0', Name: 'Normal' }, { Id: '1', Name: 'High' }, { Id: '2', Name: 'Urgent' }]
                    if (EnableNewPriorities == 'True') l = [{ Id: '0', Name: 'Proximity' }, { Id: '1', Name: 'Normal' }, { Id: '2', Name: 'Medium' }, { Id: '3', Name: 'High' }, { Id: '4', Name: 'Urgent' }, { Id: '5', Name: 'Critical' }]
                    initSearch.fillDDL('.srch__Id2', l);
                    initSearch.disableOperators(['cn', 'sw', 'nsw', 'ew', 'ne', 'nn', 'nl', 'gt', 'ge', 'le', 'lt', 'bw']);
                    $('.search__op').val('eq');
                }
                else if (splitValue[0] == '1') {
                    $('.srch__Id2').show(); $('.srch__Id2').removeAttr('disabled');
                    initSearch.fillDDL('.srch__Id2', tableList.Neighborhoods);
                    initSearch.disableOperators(['gt', 'ge', 'le', 'lt', 'bw']);
                    $('.search__op').val('eq');
                }
                else if (splitValue[0] == '5') {
                	$('.srch__Id2').show(); $('.srch__Id2').removeAttr('disabled');
                	initSearch.fillDDL('.srch__Id2', tableList.dtrStatus);
                	initSearch.disableOperators(['cn', 'sw', 'nsw', 'ew', 'ne', 'gt', 'ge', 'le', 'lt', 'bw']);
                    $('.search__op').val('eq');
                }
                else if (splitValue[3] && parcelSearchFields[splitValue[3]]) {
                    let pfield = parcelSearchFields[splitValue[3]];
                    let dataType = parseInt(pfield.DataType);
                    if (dataType == 4) {
                        $('.srch__Id').show(); $('.srch__Id').removeAttr('disabled');
                        $('.srch__Id').prop('type', 'date');
                        initSearch.disableOperators(['cn', 'sw', 'nsw', 'ew', 'ne']);
                        $('.search__op').val('eq');
                    }
                    else if (dataType == 5 && pfield.LookupTable) {
                        $('.srch__Id2').show(); $('.srch__Id2').removeAttr('disabled');
                        initSearch.copyLookupToFilter('.srch__Id2', pfield.Name, pfield.LookupTable, pfield.Id, pfield);
                        $('.search__op').val('eq');
                        initSearch.disableOperators();
                    }
                    else if ([2, 7, 8, 9, 10].indexOf(dataType) > -1) {
                        $('.srch__Id').show(); $('.srch__Id').removeAttr('disabled');
                        initSearch.disableOperators(['cn', 'sw', 'nsw', 'ew', 'bw']);
                        $('.search__op').val('eq');
                    }
                    else if (dataType != 5) {
                        $('.srch__Id').show(); $('.srch__Id').removeAttr('disabled');
                        initSearch.disableOperators(['gt', 'ge', 'le', 'lt', 'bw']);
                        $('.search__op').val('cn');
                    }
                }
            }
            else {
                $('.srch__Id').show();
            }
        });

        $('.search__op').on('change', function () {
            let opval = $(this).val();
            let splitValue = $('.search__by').val().split('/');

            if (opval == 'nn' || opval == 'nl') {
                initSearch.hideShowElement('.srch__Id');
                initSearch.hideShowElement('.srch__Id2', true);
                initSearch.hideShowElement('.srch__Id3', true);
                $('.srch__Id').attr('disabled', 'disabled');
            }
            else {
                initSearch.hideShowElement('.srch__Id');
                if (splitValue[0] == '1' || splitValue[0] == '5' || splitValue[1] == '5') {
                    if (opval == 'eq') {
                        initSearch.hideShowElement('.srch__Id2');
                        initSearch.hideShowElement('.srch__Id', true);
                    }
                    else {
                        initSearch.hideShowElement('.srch__Id2', true);
                        initSearch.hideShowElement('.srch__Id');
                    }
                }
                else if (splitValue[1] == '4') {
                    if (opval == 'bw') {
                        initSearch.hideShowElement('.srch__Id');
                        initSearch.hideShowElement('.srch__Id3');
                    }
                    else {
                        initSearch.hideShowElement('.srch__Id');
                        initSearch.hideShowElement('.srch__Id3', true);
                    }
                }
            }

        });

        $('.search__MAdate__opr, .search__DTRdate__opr, .search__QCdate__opr').on('change', function () {
            let opval = $(this).val();
            let el1 = $(this).siblings('.search__date1')[0];
            let el2 = $(this).siblings('.search__date2')[0];
            $(el1, el2).removeClass('search__field-missing');
            $(this).removeClass('search__field-missing');

            if (opval == 'nn' || opval == 'nl') {
                initSearch.hideShowElement(el1);
                initSearch.hideShowElement(el2, true);
                $(el1).attr('disabled', 'disabled');
            }
            else {
                if (opval == 'bw') {
                    initSearch.hideShowElement(el1);
                    initSearch.hideShowElement(el2);
                }
                else {
                    initSearch.hideShowElement(el1);
                    initSearch.hideShowElement(el2, true);
                }
            }
        });

        $('.search__MAdate, .search__DTRdate, .search__QCdate').off('change').on('change', function () {
            if ($(this).val()) $(this).removeClass('search__field-missing');
        });

        if (callback) callback();
    },
    bindOnclick: function (callback) {
        $('.search__add').on('click', function () {
            let splitValue = $('.search__by').val().split('/');
            let opval = $('.search__op').val();
            let idValue = $('.srch__Id').val();
            let idValue2 = $('.srch__Id2').val();
            let idValue3 = $('.srch__Id3').val();
            let label = $('.search__by option:selected').text();
            let textIdValue2 = idValue2 ? $('.srch__Id2 option:selected').text() : '';
            let isValid = initSearch.validateSearch(splitValue, opval, idValue, idValue2, idValue3);
            if (isValid) {
                currentSearchTemplate.addFilter(label, $('.search__by').val(), opval, idValue, idValue2, idValue3, null, textIdValue2);
                currentSearchTemplate.render('.search__filters__container');
                initSearch.toggleAddFilter();
                if (currentSearchTemplate.Filters.length > 0) $('.search__clear-filter').removeAttr('disabled');
                if (currentSearchTemplate.Filters.length > 4)
                    $('.search__add-filter').hide();
            }
        });

        $('.search__editSave').on('click', function () {
            let val = $(this).attr('filter-id');
            let splitValue = $('.search__by').val().split('/');
            let opval = $('.search__op').val();
            let idValue = $('.srch__Id').val();
            let idValue2 = $('.srch__Id2').val();
            let idValue3 = $('.srch__Id3').val();
            let label = $('.search__by option:selected').text();
            let textIdValue2 = idValue2 ? $('.srch__Id2 option:selected').text() : '';
            let isValid = initSearch.validateSearch(splitValue, opval, idValue, idValue2, idValue3);
            if (isValid) {
                currentSearchTemplate.addFilter(label, $('.search__by').val(), opval, idValue, idValue2, idValue3, val, textIdValue2);
                currentSearchTemplate.render('.search__filters__container');
                initSearch.toggleAddFilter();
            }
        });

        $('.search__actions__search').on('click', function () {
            if (initSearch.defaultDataValidation()) return false;
            loading.show('Searching Parcels...');
            searchSavedResults.result = {};
            initSearch.doSearch();
            return false;
        });

        $('.search__actions__save').on('click', function () {
            if (initSearch.defaultDataValidation()) return false;
            let valid = ((currentSearchTemplate.Filters.length > 0) || ($('.stoggle__radio[name="radioMA"]:checked').attr('r-val') != '0') || ($('.stoggle__radio[name="radioDTR"]:checked').attr('r-val') != '0') || ($('.stoggle__radio[name="radioQC"]:checked').attr('r-val') != '0')) ? true : false;
            if (valid)
                pageActions.prompt('search', 'Filter Name', saveFilters.save);
            else
                say('Please choose atleast one condition for saving the Filter.');
            return false;
        });

        $('.search__actions__edit').on('click', function () {
            let valid = ((currentSearchTemplate.Filters.length > 0) || ($('.stoggle__radio[name="radioMA"]:checked').attr('r-val') != '0') || ($('.stoggle__radio[name="radioDTR"]:checked').attr('r-val') != '0') || ($('.stoggle__radio[name="radioQC"]:checked').attr('r-val') != '0')) ? true : false;
            if (valid) {
                let fId = $('.search__actions__edit').attr('filter-id');
                saveFilters.save(null, null, null, fId);
            }
            else
                say('Please choose atleast one condition for saving the Filter.');
            return false;
        });

        if (callback) callback();
    },
    dateValidation: function (val) {
        let regex = /(\b[12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;
        let valid = regex.test(val);
        return valid;
    },
    defaultDataValidation: function () {
        let rad = $('.stoggle__radio[r-val="1"]:checked');
        let valid = true, validAll = true;
        rad.each((i, x) => {
            let ctx = $(x).parents('.search__card--default'),
                sel = $('.search__card-date select', ctx),
                dt1 = $('.search__card-date .search__date1', ctx),
                dt2 = $('.search__card-date .search__date2', ctx),
                v1 = true, v2 = true;
            if (sel.val()) {
                if (!dt1.is(':disabled')) {
                    valid = !!dt1.val();
                    if (valid) valid = v1 = initSearch.dateValidation(dt1.val());
                }
                if (valid && !dt2.is(':disabled')) {
                    valid = !!dt2.val();
                    if (valid) valid = v2 = initSearch.dateValidation(dt2.val());
                }
                dt1[dt1.val() && v1 ? 'removeClass' : 'addClass']('search__field-missing');
                dt2[(dt2.is(':disabled') || (dt2.val() && v2)) ? 'removeClass' : 'addClass']('search__field-missing');
            } else {
                //If dt1 val is selected then sel is missing otherwise valid = true
                //If sel is not selected dt2 will be hidden, no need to check that
                valid = !dt1.val();
                sel[dt1.val() ? 'addClass' : 'removeClass']('search__field-missing');
            }
            if (!valid) validAll = false;
        });

        return (!(valid && validAll));
    },
    validateSearch: function (splitValue, opr, idValue, idValue2, idValue3) {
        if (splitValue == '' || opr == '') {
            say('Please select a valid condition to add the filter.');
            return false;
        }

        let dataType = 1, value = '';
        if (splitValue[1]) {
            dataType = parseInt(splitValue[1]);
        }

        if (((splitValue[0] == '1' || dataType == '5') && opr == 'eq') || (splitValue[0] == '4' || splitValue[0] == '5'))
            value = idValue2;
        else
            value = idValue;

        if (((opr != 'nn' && opr != 'nl') && value == '') || (opr == 'bw' && idValue3 == '')) {
            say('Please provide an ID or Value for saving the condition.');
            return false;
        }

        if (dataType && opr != 'nn' && opr != 'nl') {
            let pattern = new RegExp(atcDataTypes[dataType].pattern);
            let valid = pattern.test(value);
            if (!valid) {
                say("Your search didn't produced any results.");
                return false;
            }
        }

        if (dataType == 4 && opr != 'nn' && opr != 'nl') {
            let regex = /(\b[12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;
            let valid = regex.test(value);
            let btValid = opr == 'bw' ? regex.test(idValue3) : true;
            if (!(valid && btValid)) {
                say("Please choose a valid Date Format.");
                return false;
            }
        }
        return true;
    },
    disableOperators: function (ops) {
        if (!ops) {
            $('.search__op').attr('disabled', 'disabled');
        }
        else if (ops.length) {
            $('.search__op').removeAttr('disabled');
            $('.search__op option').show();
            for (x in ops) {
                $('.search__op option[value="' + ops[x] + '"]').hide();
            }
        }
    },
    hideDisableAll: function () {
        $('.search__op').val('');
        $('.srch__Id').prop('type', 'text');
        $('.srch__Id, .srch__Id3').val('');
        $('.srch__Id2').html('');
        $('.srch__Id, .srch__Id2, .srch__Id3').hide();
        $('.search__op, .srch__Id, .srch__Id2, .srch__Id3').attr('disabled', 'disabled');
    },
    addToDDL: function (ddl, text, value, fieldId) {
        var opt = document.createElement('option');
        $(opt).attr('value', value);
        opt.innerHTML = text;
        $(opt).attr('fieldId', fieldId);
        $(ddl).append(opt);
    },
    fillDDL: function (ddl, table, fieldId) {
        $(ddl).html('');
        for (x in table) {
            var d = table[x];
            initSearch.addToDDL(ddl, d.Name, d.Id, d.fieldId);
        }
    },
    hideShowElement: function (el, hide) {
        $(el).val('');
        if (hide) {
            $(el).hide(); $(el).attr('disabled', 'disabled');
        }
        else {
            $(el).show(); $(el).removeAttr('disabled');
        }
    },
    clearSearch: function () {
        $('.search__by, .search__op, .srch__Id, .srch__Id3').val('');
        $('.srch__Id2').html('');
        $('.srch__Id').show();
        $('.search__op, .srch__Id, .srch__Id2, .srch__Id3').attr('disabled', 'disabled');
        $('.srch__Id2, .srch__Id3').hide();
    },
    editFilter: function (filterElement) {
        let val = $(filterElement).attr('filter-id');
        initSearch.toggleAddFilter(true);
        $('.search__add').hide(); $('.search__editSave').show();
        let selectedFilter = currentSearchTemplate.Filters.filter((x) => { return (x.ID == val) })[0];
        if (selectedFilter) {
            $('.search__by').val(selectedFilter.Field);
            $('.search__op').removeAttr('disabled');
            $('.search__op').val(selectedFilter.Operator);
            $('.srch__Id').prop('type', 'text');
            let field = selectedFilter.Field.split('/');
            let type = field[1] ? parseInt(field[1]) : 1;
            let isNullOpr = (selectedFilter.Operator == 'nn' || selectedFilter.Operator == 'nl')? true:  false;
            if (type == 4) {
                initSearch.disableOperators(['cn', 'sw', 'nsw', 'ew', 'ne']);   
				$('.srch__Id').prop('type', 'date');                
                if (!isNullOpr) {
                	initSearch.hideShowElement('.srch__Id');
	                $('.srch__Id').val(selectedFilter.Value1);
	                if (selectedFilter.Operator == 'bw') {
	                    initSearch.hideShowElement('.srch__Id3');
	                    $('.srch__Id3').val(selectedFilter.Value3);
	                }
                }
            }
            else if (type == 5 || field[0] == '1' || field[0] == '5') {
                if (type == 5) initSearch.disableOperators();
                else initSearch.disableOperators(['gt', 'ge', 'le', 'lt', 'bw']);
                
				if (field[0] == '1')
                        initSearch.fillDDL('.srch__Id2', tableList.Neighborhoods);
                else if (field[0] == '5') {
                	initSearch.fillDDL('.srch__Id2', tableList.dtrStatus);
                	initSearch.disableOperators(['cn', 'sw', 'nsw', 'ew', 'ne', 'gt', 'ge', 'le', 'lt', 'bw']);
                }
                else {
                    let pfield = parcelSearchFields[field[3]];
                    initSearch.copyLookupToFilter('.srch__Id2', pfield.Name, pfield.LookupTable, pfield.Id, pfield);
                }				
				
                if (selectedFilter.Operator == 'eq' && !isNullOpr) {
                    initSearch.hideShowElement('.srch__Id', true);
                    initSearch.hideShowElement('.srch__Id2');                    
                    $('.srch__Id2').val(selectedFilter.Value2);
                }
                else if (type != 5 && !isNullOpr) {
                    initSearch.hideShowElement('.srch__Id');
                    $('.srch__Id1').val(selectedFilter.Value1);
                }
            }
            else if (field[0] == '4') {
                initSearch.hideShowElement('.srch__Id', true);
                initSearch.hideShowElement('.srch__Id2');
                let l = [{ Id: '0', Name: 'Normal' }, { Id: '1', Name: 'High' }, { Id: '2', Name: 'Urgent' }];
                if (EnableNewPriorities == 'True') l = [{ Id: '0', Name: 'Proximity' }, { Id: '1', Name: 'Normal' }, { Id: '2', Name: 'Medium' }, { Id: '3', Name: 'High' }, { Id: '4', Name: 'Urgent' }, { Id: '5', Name: 'Critical' }];
                initSearch.fillDDL('.srch__Id2', l);
                initSearch.disableOperators(['cn', 'sw', 'nsw', 'ew', 'ne', 'nn', 'nl', 'gt', 'ge', 'le', 'lt', 'bw']);
                $('.srch__Id2').val(selectedFilter.Value2);
            }
            else {
                if ([2, 7, 8, 9, 10].indexOf(type) > -1) initSearch.disableOperators(['cn', 'sw', 'nsw', 'ew', 'bw']);
                else initSearch.disableOperators(['gt', 'ge', 'le', 'lt', 'bw']);
                if(!isNullOpr) {
	                initSearch.hideShowElement('.srch__Id');
	                $('.srch__Id').val(selectedFilter.Value1);
                }
            }
        }
        $('.search__editSave').attr('filter-id', val);
        return false;
    },
    removeFilter: function (filterElement) {
        let val = $(filterElement).attr('filter-id');
        currentSearchTemplate.remove(val);
        currentSearchTemplate.render('.search__filters__container');
        if (currentSearchTemplate.Filters.length > 0) $('.search__clear-filter').removeAttr('disabled');
        else $('.search__clear-filter').attr('disabled', true);
        if (currentSearchTemplate.Filters.length <= 5)
            $('.search__add-filter').show();
        return false;
    },
    copyLookupToFilter: function (ddl, fieldName, lookupName, fieldId, pfield) {
        $(ddl).html('');
        if (lookupName.toString() == "$QUERY$") {
            let t = fieldName;
            let opt = fieldId ? lookup['#FIELD-' + fieldId] : lookup[t];
            opt = opt ? opt : {};
            let sortedopt = Object.keys(opt).sort(function (a, b) { return (opt[a].Ordinal - opt[b].Ordinal) });
            for (o in sortedopt) {
                let text = opt[sortedopt[o]].Name;
                let value = opt[sortedopt[o]].Id;
                let fieldId = opt[sortedopt[o]].fieldId;
                initSearch.addToDDL(ddl, text, value, fieldId);
            }
        }
        else {
            if (pfield && pfield.DataType == 5 && lookup && lookup[lookupName]) {
                for (var keys in lookup[lookupName]) {
                    var lkdata = lookup[lookupName][keys];
                    if (lkdata) {
                        var id = lkdata['Id'], name = lkdata['Name'];
                        if (id == "True" || id == "TRUE" || id == "False" || id == "FALSE")
                            id = id.toLowerCase();
                        $(ddl).append($('<option/>', { value: id, text: name }));
                    }
                }
            }
            $('option[value=""]', ddl).remove();
            $(ddl).prepend($('<option/>', { value: -1, text: 'Is NULL' }));
            $(ddl).prepend($('<option/>', { value: -2, text: 'Is not NULL' }));
        }
    },
    toggleAddFilter: function (flag) {
        if (flag) {
            $('.search__card .card__accordion.card__accordion--expanded').trigger('click');
            $('.search__filters').slideUp();
            $('.search__af').slideDown();
            $('.search__filter-btn').hide();
            $('.search__editSave').hide();
            $('.search__add').show();
            initSearch.clearSearch();
        } else {
            $('.search__af').slideUp();
            $('.search__filters').slideDown();
            $('.search__filter-btn').show();
        }

    },
    toggleTriSwitch: function (grp, flag) {
        let row = $(`.stoggle input[name=${grp}]`).parents('.search__card--default');
        let cardDetail = $('.card__detail', row);
        let cardAcc = $('.card__accordion', row);
        $('.search__date1,.search__date2', row).removeClass('search__field-missing');
        $('.search__card-date select', row).removeClass('search__field-missing');
        if (flag == 1) {
            // Toggle Active - YES
            //cardAcc.show();
        } else {
            // Toggle Active - NA & NO

            //Removing accordion on YES selection
            if (cardAcc.hasClass('card__accordion--expanded')) {
                cardDetail.slideUp(() => { cardDetail.removeClass('card__detail--expanded'); });
                cardAcc.removeClass('card__accordion--expanded');
            }
            $('input, select', cardDetail).val('');
            //cardAcc.hide();
        }
    },
    removeAllFilters: function () {
        $('.search__clear-filter').attr('disabled', true);
        currentSearchTemplate.clear();
        currentSearchTemplate.render('.search__filters__container');
        $('.search__add-filter').show();
        return false;
    },
    clearAll: function () {
        atcMap.reset();
        pageActions.setTitle(false);
        currentSearchTemplate.clear();
        currentSearchTemplate.render('.search__filters__container');
        $('.search__clear-filter').attr('disabled', true);
        $('[sb-id="search"] .sb__page-title').text('Search');
        $('.search__actions__save, .search__add-filter').show();
        $('.stoggle__radio[r-val="0"]').prop('checked', 'checked');
        $('.stoggle__radio[r-val="0"]').change();
        $('.search__MAdate__opr, .search__MAdate, .search__MAuser').val('');
        $('.search__DTRdate__opr, .search__DTRdate, .search__DTRuser').val('');
        $('.search__QCdate__opr, .search__QCdate, .search__QCuser').val('');
        $('.search__card-date select').removeClass('search__field-missing');
        $('.search__MAdate,.search__DTRdate,.search__QCdate').removeClass('search__field-missing');
        $('.search__actions__edit, .search__MAdate.search__date2, .search__DTRdate.search__date2, .search__QCdate.search__date2').hide();
        return false;
    },
    getSearchParam: function () {
        let searchParam = '';

        ['MA', 'DTR', 'QC'].forEach((type) => {
            let v = $('.stoggle__radio[name="radio' + type + '"]:checked').attr('r-val');
            if (v == '1') {
                let u = $('.search__' + type + 'user').val();
                let op = $('.search__' + type + 'date__opr').val();
                let d1 = (op != '') ? $('.search__' + type + 'date.search__date1').val() : '';
                let d2 = (op == 'bw') ? $('.search__' + type + 'date.search__date2').val() : '';
                let parms = u + '~' + op + '~' + d1 + '~' + d2;
                searchParam += "^^^" + 'default' + "|" + 1 + "|" + -1 + "|" + type + "|" + '' + "|" + v + "|" + parms + "___@@@\n";
            }
            else if (v == '2') {
                searchParam += "^^^" + 'default' + "|" + 1 + "|" + -1 + "|" + type + "|" + '' + "|" + v + "|" + '' + "___@@@\n";
            }
        });

        currentSearchTemplate.Filters.forEach((fltr) => {
            let sf = fltr.Field.split("/");
            let dt = sf[1] ? sf[1] : '1';
            let l = sf[0];
            let i = sf[3] ? sf[3] : '-1';
            let op = fltr.Operator;
            let v = (dt == '5' || ((l == '1' || l == '5') && op == 'eq') || (l == '4')) ? fltr.Value2 : fltr.Value1;
            let v2 = fltr.Value3;
            searchParam += "^^^" + 'custom' + "|" + dt + "|" + i + "|" + l + "|" + op + "|" + (v + '¤' + v2) + "|" + '' + "___@@@\n";
        });

        return searchParam;
    },
    doSearch: function () {
        pageActions.setTitle(false);
        let valid = ((currentSearchTemplate.Filters.length > 0) || ($('.stoggle__radio[name="radioMA"]:checked').attr('r-val') != '0') || ($('.stoggle__radio[name="radioDTR"]:checked').attr('r-val') != '0') || ($('.stoggle__radio[name="radioQC"]:checked').attr('r-val') != '0')) ? true : false;
        if (valid) {
            let startTime = performance.now();
            let searchParms = initSearch.getSearchParam();
            searchSavedResults.results = { SearchParms: searchParms };
            PageMethods.GetSearchResults('search', searchParms, (searchResults) => {
                if (searchResults.Success) {
                    let sl = searchResults.Data.SearchLists.length;
                    initSearch.parcelList = searchResults.Data.DownloadLists;
                    initSearch.searchList = searchResults.Data.SearchLists;
                    let tc = searchResults.Data.CountLists[0].TotalCount;
                    if (sl > 0) {
                        let m = tc > 5000 ? 'Viewing top ' + sl.toLocaleString('en-US') + ' parcels out of ' + tc.toLocaleString('en-US') + ' parcels' : 'Viewing ' + sl.toLocaleString('en-US') + ' parcels';
                        searchSavedResults.results.ParcelCount = sl;
                        adhocCreate.priorityListItems = [];
                        initSearch.searchList.forEach((p) => {
                            adhocCreate.priorityListItems.push(p.Id);
                        });
                        atcMapLayers.plotMarkers(initSearch.searchList, null, () => {
                            pageActions.setTitle(m);
                            pageActions.mapButton(1, 1, 1, 1, 1);
                        });
                    }
                    else {
                        atcMap.reset();
                        SnackBar('No parcels found', { position: 'bottom-right', autoHide: true, autoHideAfter: 3000 });
                        searchSavedResults.results = {};
                    }

                    loading.hide();
                }
                else {
                    atcMap.reset();
                    searchSavedResults.results = {};
                    loading.hide();
                    SnackBar('Searching failed', { position: 'bottom-right', autoHide: true, autoHideAfter: 3000 });
                }
                let searchTime = performance.now() - startTime;
                console.warn(`Took ${parseInt(searchTime / 1000)}s to finish searching.`)

            });
        }
        else
            say('Please add atleast one condition to start searching.');
    }
}

const saveFilters = {
    init: function (nbd, callback) {
        pageActions.loading(true, 'filter');
        PageMethods.SaveSearchFilter('', '', '', (filterLists) => {
            if (filterLists.Success) {
                savedFilterLists = filterLists.Data.SaveFilter;
                if (!nbd) {
                    saveFilters.bindData(() => {
                        pageActions.loading(false, 'filter');
                        pageTour.startTour();
                        if (callback) callback();
                    });
                }
                else if (callback) {
                    pageActions.loading(false, 'filter');
                    callback();
                }
            }
            else {
                pageActions.loading(false, 'filter');
                if (callback) callback();
            }
        });
    },
    bind: function (b) {
        let filterTemplate = $('#filter__card--template-Id').clone();
        $(filterTemplate).removeClass('filter__card--template');
        $('.filter__list').empty().html(filterTemplate).bindData(b);
        $('.filters').scrollTop(0);
        $('.filter__empty').hide();
    },
    bindData: function (callback, val) {
        let arr = [...savedFilterLists];
        if (val) {
            arr = savedFilterLists.filter((x) => {
                let nm = x?.FilterName;
                if (nm && nm.toLowerCase().includes(val.toLowerCase())) return x;
            })
        }
        if (arr.length > 0) {
            let b = [];
            arr.forEach((sfl) => {
                let fd = JSON.parse(sfl.FilterData);
                let subFilters = [], fdt = fd.slice(0, 3);
                fdt.forEach((f) => {
                    let fClass = f.Value == 2 ? 'filter__card--red' : (f.Value == 1 ? 'filter__card--green' : 'filter__card--blue');
                    subFilters.push(_.clone({ FilterText: f.DisplayText, fClass }));
                });
                b.push(_.clone({ Id: sfl.Id, FilterName: sfl.FilterName, subFilters: subFilters, Count: fd.length }));
            });
            b = b.sort((a, b) => parseInt(b.Id) - parseInt(a.Id));
            saveFilters.bind(b);
        }
        else {
            $('.filter__list').empty();
            $('.filter__empty').show();
        }

        if (callback) callback();
    },
    save: function (name, div, callback, fId) {
        fId = fId ? fId : '';
        if (!fId) {
            if (savedFilterLists.length >= 50) {
                pageActions.loading(false, 'search');
                div.remove();
                say("You have added 50 filters already. Please delete some to add more.");
                return false;
            }

            if (name == '') {
                $('.sb__validate', div).html('Please provide a name for the filter').show();
                return false;
            }

            let existFilter = savedFilterLists.filter((x) => { return (x.FilterName == name) })[0];
            if (existFilter) {
                $('.sb__validate', div).html('A Filter with the same name already exists. Specify another one.');
                $('.sb__validate', div).show();
                return false;
            }

            pageActions.loading(false, 'search');
            div.remove();
        }
        else
            name = '';

        loading.show();
        let fd = [], fds = '';

        ['MA', 'DTR', 'QC'].forEach((type) => {
            if ($('.stoggle__radio[name="radio' + type + '"]:checked').attr('r-val') != '0') {
                let v = $('.stoggle__radio[name="radio' + type + '"]:checked').attr('r-val');
                let l = type + ((v == '1') ? ' Reviewed' : ' not Reviewed');
                let u = (v == '1') ? $('.search__' + type + 'user').val() : '';
                let uh = u != '' ? $('.search__' + type + 'user').find(":selected").text() : '';
                let op = (v == '1') ? $('.search__' + type + 'date__opr').val() : '';
                let d1 = (op != '') ? $('.search__' + type + 'date.search__date1').val() : '';
                let d2 = (op == 'bw') ? $('.search__' + type + 'date.search__date2').val() : '';
                let ls = op ? suffix(op) : '';
                l += (u ? ' by ' + uh : '') + (op ? suffix(op) : '') + (d1 ? (' ' + d1) : '') + (op == 'bw' ? (' and ' + d2) : '');
                fd.push(_.clone({ Type: type, DisplayText: l, Value: v, Custom: 0, User: u, Operator: op, Date1: d1, Date2: d2 }));
            }
        });

        currentSearchTemplate.Filters.forEach((fls) => {
            let d = {};
            for (let x in fls) {
                d[x] = fls[x];
            }
            fd.push(_.clone(d));
        });

        fds = JSON.stringify(fd);

        PageMethods.SaveSearchFilter(name, fds, fId, (sfs) => {
            if (sfs.Success) {
                savedFilterLists = sfs.Data.SaveFilter;
                loading.hide();
                let msg = '';
                if (fId && fId != '') {
                    let sf = savedFilterLists.filter((x) => { return x.Id == fId })[0];
                    if (sf && sf.FilterName) msg = 'Filter "' + sf.FilterName + '" has been updated successfully.';
                    else msg = 'Filter has been updated successfully.';
                }
                else msg = 'Filter "' + name + '" has been created successfully.';
                say(msg);
                return false;
            }
            else {
                loading.hide();
                say('An error occured while saving the filter.');
                console.error(sfs.ErrorMessage);
                return false;
            }
        });
    },
    remove: function (el) {
        let delId = el ? ($(el).hasClass('filter__reset') ? '-1' : $(el).parents('.filter__card').attr('filter-id')) : '';
        let fl = delId && delId != '-1' ? savedFilterLists.filter((x) => { return x.Id == delId })[0] : null;
        let m = fl ? `Are you sure you want to delete the filter '${fl.FilterName}'?` : `Are you sure you want to delete all the filters ?`;
        ask(m, function () {
            PageMethods.DeleteSearchFilter(delId, (df) => {
                if (df.Success) {
                    savedFilterLists = df.Data.DeleteFilter;
                    if (delId && delId != '-1') {
                        $('.filter__card[filter-id="' + delId + '"]').remove();
                        if ($('.filter__list .filter__card').length == 0) {
                            $('.filter__list').html('');
                            $('.filter__empty').show();
                        }
                    }
                    else {
                        $('.filter__list').html('');
                        $('.filter__empty').show();
                    }

                    let sb = SnackBar('Filter Deleted', { position: 'bottom-right', autoHide: true, autoHideAfter: 2000 });
                    return false;
                }
                else {
                    say('An error occured while deleting the filter.');
                    console.error(df.ErrorMessage);
                    return false;
                }
            });
        });
    },
    clone: function (el) {
        let eId = el ? $(el).parents('.filter__card').attr('filter-id') : '';
        if (savedFilterLists.length >= 50) {
            say("You have added 25 results already. Please delete some to add more.");
            return false;
        }

        let fd = savedFilterLists.filter((x) => { return (x.Id == eId) })[0];
        if (fd) {
            let fn = fd.FilterName
            if (fn.length > 49) fn = fn.substring(1, fn.length - 10);
            fn += '_' + randomNumber();
            let fdt = fd.FilterData;
            PageMethods.SaveSearchFilter(fn, fdt, '', (sfs) => {
                if (sfs.Success) {
                    savedFilterLists = sfs.Data.SaveFilter;
                    saveFilters.bindData();
                    return false;
                }
                else {
                    say('An error occured while duplicating the Filter.');
                    console.error(sfs.ErrorMessage);
                    return false;
                }
            });
        }
    },
    rename: function (el) {
        let fc = $(el).parents('.filter__card');
        let cname = $('.filter__card-name', fc).hide().text();
        $('.filter__card-actions', fc).hide();
        $('.filter__card-rename-input', fc).val(cname).removeClass('invalid');
        $('.filter__card-rename-valid', fc).hide();
        $('.filter__card-rename-box', fc).show();

    },
    renameSave: function (el) {
        let fc = $(el).parents('.filter__card');
        let nn = $('.filter__card-rename-input', fc).val();
        let fId = fc.attr('filter-id');
        let cn = savedFilterLists.filter((x) => { return (x.Id == fId) })[0]?.FilterName;
        if (nn == '') {
            $('.filter__card-rename-input', fc).addClass('invalid');
            return false;
        }
        else if (cn == nn) {
            saveFilters.renameCancel(el);
            return false;
        }
        else {
            let existFilter = savedFilterLists.filter((x) => { return (x.FilterName == nn && x.Id != fId) })[0];
            if (existFilter) {
                say('A Filter with the same name already exists. Specify another one.');
                return false;
            }
        }

        PageMethods.UpdateSearchFilter(fId, nn, '', (uf) => {
            if (uf.Success) {
                savedFilterLists = uf.Data.UpdateFilter;
                $('.filter__card-name', fc).attr('title', nn).html(nn).show();
                $('.filter__card-rename-box', fc).hide();
                $('.filter__card-actions', fc).show();
                return false;
            }
            else {
                say('An error occured while renaming the filter.');
                console.error(uf.ErrorMessage);
                return false;
            }
        });
    },
    renameCancel: function (el) {
        let fc = $(el).parents('.filter__card');
        $('.filter__card-rename-box', fc).hide();
        $('.filter__card-actions', fc).show();
        $('.filter__card-name', fc).show();
        return false;
    },
    renameKeyup: function (el) {
        $(el).removeClass('invalid');
        return true;
    },
    search: function (el) {
        if ($(el).attr('disabled')) return false;
        loading.show('Searching Parcels...');
        pageActions.openPage('search', { search: true });
        let fId = $(el).attr('filter-id');
        let ft = savedFilterLists.filter((x) => { return (x.Id == fId) })[0];
        let fdata = JSON.parse(ft.FilterData);

        $('[sb-id="search"] .sb__page-title').text(ft.FilterName);

        let fillTriRadio = function (type, value, user, opr, d1, d2) {
            $('.search__add-filter').show();
            $('.stoggle__radio[name="radio' + type + '"][r-val="' + value + '"]').prop('checked', 'checked');
            $('.stoggle__radio[name="radio' + type + '"][r-val="' + value + '"]').change();
            $('.search__' + type + 'user').val(user);
            $('.search__' + type + 'date__opr').val(opr);
            $('.search__' + type + 'date.search__date1').val(d1);
            if (opr == 'bw') {
                $('.search__' + type + 'date.search__date2').show();
                $('.search__' + type + 'date.search__date2').val(d2);
            }
            else {
                $('.search__' + type + 'date.search__date2').val('');
                $('.search__' + type + 'date.search__date2').hide();
            }
        }

        fdata.forEach((fd) => {
            if (fd.Custom == '0')
                fillTriRadio(fd.Type, fd.Value, fd.User, fd.Operator, fd.Date1, fd.Date2);
            else
                currentSearchTemplate.addFilter(fd.Label, fd.Field, fd.Operator, fd.Value1, fd.Value2, fd.Value3, null, fd.TextValue2);
        });

        currentSearchTemplate.render('.search__filters__container');
        $('.search__actions__edit').show().attr('filter-id', fId);
        $('.search__actions__save').hide();
        if (currentSearchTemplate.Filters.length > 0) $('.search__clear-filter').removeAttr('disabled');
        else $('.search__clear-filter').attr('disabled', true);
        if (currentSearchTemplate.Filters.length > 4)
            $('.search__add-filter').hide();
        initSearch.doSearch();
    },
    filterSearch: function (el) {
        let val = $(el).val();
        saveFilters.bindData(null, val);
    },
    clearfilterSearch: function () {
        saveFilters.bindData();
    }
}

const searchSavedResults = {
    result: {},
    init: function (nb) {
        pageActions.loading(true, 'result');
        searchSavedResults.loadResults('', '', 0, 0, '', '', (res) => {
            pageActions.loading(false, 'result');
            pageTour.startTour();

            if (!nb)
                searchSavedResults.bind();
        });
    },
    loadResults: function (name, u, pc, fc, rId, searchQuery, callback) {
        PageMethods.SaveSearchResults(name, u, pc, fc, rId, searchQuery, (res) => {
            if (res.Success)
                savedResultsLists = res.Data.SearchLists;
            else
                savedResultsLists = [];
            if (callback) callback(res.Success);
        });
    },
    bind: function (val) {
        let arr = [...savedResultsLists];
        if (val) {
            arr = savedResultsLists.filter((x) => {
                let nm = x?.ResultName;
                if (nm && nm.toLowerCase().includes(val.toLowerCase())) return x;
            })
        }
        if (arr.length > 0) {
            arr = arr.sort((a, b) => parseInt(b.Id) - parseInt(a.Id));
            arr.forEach((x) => { x.ParcelCount = x.ParcelCount ? x.ParcelCount.toLocaleString('en-US') : x.ParcelCount; });
            let resultTemplate = $('.result__card--template').clone();
            $(resultTemplate).removeClass('result__card--template');
            $('#resultList').empty().html(resultTemplate).bindData(arr);
            $('.sb__page-content.results').scrollTop(0);
            $('.results__empty').hide();
        }
        else {
            $('#resultList').empty();
            $('.results__empty').show();
        }
    },
    save: function () {
        let div = $('.result-pop'), name = $('.result-pop__name', div).val();

        if (savedResultsLists.length >= 25) {
            searchSavedResults.showSavePopup(false);
            say("You have added 25 results already. Please delete some to add more.");
            return false;
        }

        if (name == '') {
            $('.sb__validate', div).html('Please specify a name for the Result').show();
            return false;
        }

        let existFilter = savedResultsLists.filter((x) => { return (x.ResultName == name) })[0];
        if (existFilter) {
            $('.sb__validate', div).html('A Result with the same name already exists. Specify another one.');
            $('.sb__validate', div).show();
            return false;
        }

        searchSavedResults.showSavePopup(false);
        loading.show('Saving...');

        let u = $('.atc__login-user').html();
        let pc = searchSavedResults.results.ParcelCount;
        let sp = searchSavedResults.results.SearchParms;
        if (sp && pc) {
            let fc = sp.split('___@@@').length - 1;

            searchSavedResults.loadResults(name, u, pc, fc, '', sp, (res) => {
                if (res) {
                    loading.hide();
                    SnackBar('Saved Successfully', { position: 'bottom-right', autoHide: true, autoHideAfter: 3000 });
                    return false;
                }
                else {
                    loading.hide();
                    SnackBar('Failed!', { position: 'bottom-right', autoHide: true, autoHideAfter: 3000 });
                    console.error(res.ErrorMessage);
                    return false;
                }
            });
        } else {
            loading.hide();
            SnackBar('Failed! Search Parameters are empty', { position: 'bottom-right', autoHide: true, autoHideAfter: 3000 });
        }
    },
    rename: function (el) {
        let rc = $(el).parents('.result__card');
        let cname = $('.result__card-name', rc).text();
        $('.result__card-actions', rc).hide();
        $('.result__card-title>div:first-child', rc).hide();
        $('.result__card-rename-input', rc).val(cname).removeClass('invalid');
        $('.result__card-rename-valid', rc).hide();
        $('.result__card-rename-box', rc).show();
    },
    renameSave: function (el) {
        let rc = $(el).parents('.result__card');
        let nn = $('.result__card-rename-input', rc).val();
        let rId = rc.attr('result-id');
        let cn = savedResultsLists.filter((x) => { return (x.Id == rId) })[0]?.ResultName;
        if (nn == '') {
            $('.result__card-rename-input', rc).addClass('invalid');
            return false;
        }
        else if (cn == nn) {
            searchSavedResults.renameCancel(el);
            return false;
        }
        else {
            let existFilter = savedResultsLists.filter((x) => { return (x.ResultName == nn && x.Id != rId) })[0];
            if (existFilter) {
                say('A Result with the same name already exists. Specify another one.');
                return false;
            }
        }
        searchSavedResults.loadResults(nn, '', 0, 0, rId, '', (res) => {
            if (res) {
                $('.result__card-name', $(el).parents('.result__card')).attr('title', nn).html(nn);
                searchSavedResults.renameCancel(el);
            }
        });
    },
    renameCancel: function (el) {
        let rc = $(el).parents('.result__card');
        $('.result__card-rename-box', rc).hide();
        $('.result__card-actions', rc).show();
        $('.result__card-title>div:first-child', rc).show();
        return false;
    },
    renameKeyup: function (el) {
        $(el).removeClass('invalid');
        return true;
    },
    remove: function (el, all) {
        let dId = all ? '-1' : $(el).parents('.result__card').attr('result-id');
        let fl = dId && dId != '-1' ? savedResultsLists.filter((x) => { return x.Id == dId })[0] : null;
        let m = fl ? `Are you sure you want to delete the result '${fl.ResultName}'?` : `Are you sure you want to delete the filters ?`;

        ask(m, function () {
            searchSavedResults.loadResults('', '', 0, 0, dId, '', (res) => {
                if (res) {
                    searchSavedResults.bind();
                }
            });
        });
    },
    deleteAll: function () {
        searchSavedResults.remove(null, true);
    },
    resultSearch: function (el) {
        let val = $(el).val();
        searchSavedResults.bind(val);
    },
    clearResultSearch: function () {
        searchSavedResults.bind();
    },
    showSavePopup: function (flag) {
        let pop = $('.result-pop');
        if (flag) {
            pageActions.popup(pop);
        } else {
            pageActions.popup(false);
            $('.sb__validate', pop).html('');
            $('.result-pop__name', pop).val('');
        }
    },
    viewOnMap: function (el) {
        loading.show();
        pageActions.setTitle(false);
        let rId = $(el).attr('result-id');
        let rName = savedResultsLists.filter((x) => { return (x.Id == rId) })[0]?.ResultName;
        PageMethods.ReturnSavedResults(rId, (res) => {
            if (res.Success) {
                let sl = res?.Data?.SearchLists?.length
                if (sl > 0) {
                    atcMapLayers.plotMarkers(res.Data.SearchLists, null, () => {
                        pageActions.mapButton(0, 0, 0, 1, 0, 0, 0);
                        sl = sl? sl.toLocaleString('en-US'): sl;
                        pageActions.setTitle(`Viewing '${rName}'(${sl} parcels)`);
                    });
                } else {
                    atcMap.reset();
                    SnackBar('No parcels found', { position: 'bottom-right', autoHide: true, autoHideAfter: 3000 });
                }
            }
            else {
                atcMap.reset();
                SnackBar(`Error opening result '${rName}'`, { position: 'bottom-right', autoHide: true, autoHideAfter: 3000 });
                console.log(res.ErrorMessage);
            }
            loading.hide();
        });
    }
}

const assignmentGroupStatus = {
    AssignmentGroupList: [],
    AssignedUserList: [],
    assignmentGroupFilters: [{ Id: "1", Name: "Currently Progressing", filter: "x.Completed == 0 && x.AssignedTo && x.LastVisitedDate" }, { Id: "2", Name: "Assigned but Not Completed", filter: "x.Completed == 0 && x.AssignedTo" }, { Id: "3", Name: "Completed Groups", filter: "x.Completed == 1 && x.VisitedParcels > 0 && x.TotalPriorities > 0" }, { Id: "4", Name: "Unassigned Groups with Pending Priorities", filter: "!x.AssignedTo && (x.TotalAlerts + x.TotalPriorities > 0) && x.PendingPriorities > 0" }, { Id: "5", Name: "Unassigned Groups", filter: "!x.AssignedTo" }, { Id: "6", Name: "No Priorities", filter: "x.TotalPriorities == 0 && x.TotalAlerts == 0" }, { Id: "7", Name: "Assignment Groups By User", filter: "" }, { Id: "0", Name: "Show All", filter: "" }],
    init: function () {
        pageActions.loading(true, 'group');

        $('.group__filter-flag').val('1');
        $('.group__filter-user').val('');
        assignmentGroupStatus.assgnCancel();
        assignmentGroupStatus.bindChange();
        assignmentGroupStatus.filterList('1');
        if (EnableNewPriorities == 'True') {
            assignmentGroupStatus.assignmentGroupFilters[3].filter = "!x.AssignedTo && x.TotalPriorityparcels > 0 && x.PendingPriorities > 0";
            assignmentGroupStatus.assignmentGroupFilters[5].filter = "x.TotalPriorityparcels = 0";
        }
        pageTour.startTour();
    },
    loadAssignUser: function (callback) {
        PageMethods.AssignmentGroupUserStatus((res) => {
            if (res.Success) {
                let ul = res.Data;
                assignmentGroupStatus.AssignedUserList = ul;
                let uhtml = "<option value=''>-- Select --</option>";
                ul.forEach((u) => {
                    uhtml += "<option value=" + u.LoginId + ">" + u.UserName + "</option>";
                });
                $('.group__assign-select').html(uhtml);
            }

            if (callback) callback();
        });
    },
    loadNbhds: function (callback) {
        PageMethods.AssignmentGroupStatus((res) => {
            if (res.Success) {
                let ul = res.Data.FilterUser, html = "";
                assignmentGroupStatus.AssignmentGroupList = res.Data.AssignmentGroupList;
                assignmentGroupStatus.assignedUserBind(ul);
                assignmentGroupStatus.assignmentGroupFilters.forEach((f) => {
                    html += "<option value=" + f.Id + ">" + f.Name + "</option>";
                });

                $('.group__filter-flag').html(html);

            }
            if (callback) callback();
        });
    },
    bind: function (arr) {
        if (arr.length > 0) {
            arr.forEach((x) => {
                x.PendingPriorities = x.PendingPriorities ? x.PendingPriorities.toLocaleString('en-US') : x.PendingPriorities;
                x.TotalParcels = x.TotalParcels ? x.TotalParcels.toLocaleString('en-US') : x.TotalParcels;
                x.TotalPriorities = x.TotalPriorities ? x.TotalPriorities.toLocaleString('en-US') : x.TotalPriorities;
                x.TotalAlerts = x.TotalAlerts ? x.TotalAlerts.toLocaleString('en-US') : x.TotalAlerts;
                x.VisitedParcels = x.VisitedParcels ? x.VisitedParcels.toLocaleString('en-US') : x.VisitedParcels;
                x.VisitedPriorities = x.VisitedPriorities ? x.VisitedPriorities.toLocaleString('en-US') : x.VisitedPriorities;
                x.VisitedAlerts = x.VisitedAlerts ? x.VisitedAlerts.toLocaleString('en-US') : x.VisitedAlerts;
                if (EnableNewPriorities == 'True') {
                    x.TotalPriorityparcels = x.TotalPriorityparcels ? x.TotalPriorityparcels.toLocaleString('en-US') : x.TotalPriorityparcels;
                    x.TotalVisitedPriorityParcels = x.TotalVisitedPriorityParcels ? x.TotalVisitedPriorityParcels.toLocaleString('en-US') : x.TotalVisitedPriorityParcels;
                }
            });
            let assgnTemplate = $('.group__card--template').clone();
            $(assgnTemplate).removeClass('group__card--template');
            $('#assgnCard').empty().html(assgnTemplate).bindData(arr);
            $('.assgn__empty').hide();
            pageActions.loading(false, 'group');
        } else {
            $('#assgnCard').empty();
            $('.assgn__empty').show();
            pageActions.loading(false, 'group');
        }
    },
    bindPages: function (arr) {
        let ctx = $(`.sb__page[sb-id="group"]`), pageDiv = $('.group__page-div', ctx);
        let l = arr.length, pageLimit = 10, sArr = [];
        if (l > 0) {
            if (l <= pageLimit) {
                pageDiv.empty().hide();
                assignmentGroupStatus.bind(arr);
            } else {
                let numPages = Math.ceil(l / pageLimit), activePage = 1;
                assignmentGroupStatus.setPages(numPages, activePage, (p) => {
                    $('#assgnCard').scrollTop(0);
                    let s = ((p - 1) * pageLimit) + 1;
                    sArr = arr.slice(s, s + pageLimit);
                    assignmentGroupStatus.bind(sArr);
                });
            }
        } else {
            pageDiv.empty().hide();
            assignmentGroupStatus.bind(sArr);
        }
    },
    setPages: function (num, current, pageClick) {
        let ctx = $(`.sb__page[sb-id="group"]`), pageDiv = $('.group__page-div', ctx);
        pageDiv.empty().show();
        let pages = [];
        if (num > 12) pages = ['<<', '<', current, '>', '>>'];
        else pages = [...Array.from({ length: num }, (_, i) => i + 1)];
        pages.forEach((i) => {
            let el = $('<span/>'), pid;
            if (i == current) el.addClass('group__page-text').text(i);
            else {
                if (num > 12) pid = (i == '<<' ? 1 : i == '<' ? current - 1 : i == '>' ? current + 1 : i == '>>' ? num : current);
                else pid = i;
                el.attr({ class: 'btn btn--small group__page-link', 'page-id': pid, }).text(i);
                if ((current == num && (i == '>' || i == '>>')) || (current == 1 && (i == '<' || i == '<<'))) el.attr('page-dis', true);
            }
            pageDiv.append(el);
        });
        $('.group__page-link').off('click').on('click', (x) => {
            let el = $(x.target), pg = parseInt(el.attr('page-id'));
            if (el.attr('page-dis') != 'true') {
                assignmentGroupStatus.setPages(num, pg, pageClick);
                $('.group__assign-div').hide();
                $('.group__filter-div').show();
                if (pageClick && typeof pageClick === 'function') pageClick(pg);
            }
        })
        if (pageClick && typeof pageClick === 'function') pageClick(1);
    },
    bindChange: function () {
        $('.group__filter-flag').off('change');
        $('.group__filter-flag').on('change', function () {
            $('.group__filter-user').val('');
            pageActions.clearSearch('group');
            assignmentGroupStatus.filterList(this.value);
        });

        $('.group__filter-user').off('change');
        $('.group__filter-user').on('change', function () {
            assignmentGroupStatus.filterList($('.group__filter-flag').val());
        });
    },
    filterList: function (fId, skip, val) {
        let f = "x.Number";
        let sf = assignmentGroupStatus.assignmentGroupFilters.filter((x) => { return x.Id == fId })[0];
        let u = $('.group__filter-user').val();

        if (fId == '7' && u == "" && !skip) {
            return;
        }

        f += (sf?.filter ? " && " + sf.filter : '') + (u ? " && x.AssignedTo == '" + u + "'" : '');
        let fa = assignmentGroupStatus.AssignmentGroupList.filter((x) => { return eval(f) });

        if (skip && fId == '7' && u == "")
            fa = [];

        if (val) {
            fa = fa.filter((x) => {
                let nm = x?.Number;
                if (nm && nm.toLowerCase().includes(val.toLowerCase())) return x;
            })
        }
        assignmentGroupStatus.bindPages(fa);
    },
    checkBoxChange: function () {
        let c = $('.group__card-selected:checked').length;
        if (c > 0) {
            if (!$('.group__assign-div').is(":visible")) {
                $('.group__filter-div').hide();
                $('.group__assign-div').show();
                $('.group__assign-select').val('');
                $('.group__assignuser').hide();
            }
            $('.group__assign-count').text(c);
        }
        else {
            $('.group__assign-div').hide();
            $('.group__filter-div').show();
        }
    },
    assgnChange: function (el) {
        if ($(el).val() == '')
            $('.group__assignuser').hide();
        else
            $('.group__assignuser').show();
    },
    assignedUserBind: function (ul) {
        let uhtml = "<option value=''>-- Select --</option>";
        ul.forEach((u) => {
            uhtml += "<option value=" + u.AssignedTo + ">" + u.FullName + "</option>";
        });
        $('.group__filter-user').html('');
        $('.group__filter-user').append(uhtml);
    },
    unAssignGroup: function () {
        let assgnIdStr = '', c = $('.group__card-selected:checked').length, gu = $('.group__filter-user').val();
        let nm = $('.group__card-selected:checked ~ .group__card-name')?.eq(0)?.text();

        let sbText = `Unassigning ${c} Groups`;
        if (c == 1) sbText = `Unassigning Group ${nm || ''}`;

        let sb = SnackBar(sbText, { loading: true, position: 'bottom-right', autoHide: false });
        $('.group__card-selected:checked').each((i, group) => {
            assgnIdStr += $(group).attr('assgn-id') + ',';
        });
        assgnIdStr = assgnIdStr.slice(0, -1);

        PageMethods.UnAssignGroups(assgnIdStr, (res) => {
            sb.hide();
            if (res.Success) {
                let ul = res.Data.FilterUser, html = "";
                assignmentGroupStatus.AssignmentGroupList = res.Data.AssignmentGroupList;
                assignmentGroupStatus.assignedUserBind(ul);
                $('.group__filter-user').val(gu);
                assignmentGroupStatus.assgnCancel();
                assignmentGroupStatus.filterList($('.group__filter-flag').val(), true);
                SnackBar(c + ' group(s) has been unassigned from corresponding user', { position: 'bottom-right', autoHide: true, autoHideAfter: 3000 });
            }
            else {
                assignmentGroupStatus.assgnCancel();
                SnackBar('Failed!', { position: 'bottom-right', autoHide: true, autoHideAfter: 3000 });
            }
        });
    },
    assignGroups: function () {
        let assgnIdStr = '', c = $('.group__card-selected:checked').length, gu = $('.group__filter-user').val();
        let nm = $('.group__card-selected:checked ~ .group__card-name')?.eq(0)?.text();

        let sbText = 'Assigning ' + c + ' Groups';
        if (c == 1) sbText = `Assigning Group '${nm || ''}'`;

        let sb = SnackBar(sbText, { loading: true, position: 'bottom-right', autoHide: false });
        let u = $('.group__assign-select').val(), un = $('.group__assign-select').find(":selected").text();
        $('.group__card-selected:checked').each((i, group) => {
            assgnIdStr += $(group).attr('assgn-id') + ',';
        });
        assgnIdStr = assgnIdStr.slice(0, -1);

        PageMethods.AssignGroups(assgnIdStr, u, (res) => {
            sb.hide();
            if (res.Success) {
                let ul = res.Data.FilterUser, html = "";
                assignmentGroupStatus.AssignmentGroupList = res.Data.AssignmentGroupList;
                assignmentGroupStatus.assignedUserBind(ul);
                $('.group__filter-user').val(gu);
                assignmentGroupStatus.assgnCancel();
                assignmentGroupStatus.filterList($('.group__filter-flag').val(), true);
                SnackBar(c + ' groups have been assigned to user ' + un, { position: 'bottom-right', autoHide: true, autoHideAfter: 3000 });
            }
            else {
                assignmentGroupStatus.assgnCancel();
                SnackBar('Failed!', { position: 'bottom-right', autoHide: true, autoHideAfter: 3000 });
            }
        });
    },
    assgnCancel: function () {
        $('.group__assign-div').hide();
        $('.group__filter-div').show();
        $('.group__assign-select').val('');
        $('.group__assignuser').hide();
        $('.group__card-selected:checked').removeAttr('checked');
    },
    groupSearch: function (el) {
        let val = $(el).val();
        assignmentGroupStatus.filterList($('.group__filter-flag').val(), null, val);
    },
    clearResultSearch: function () {
        assignmentGroupStatus.filterList($('.group__filter-flag').val());
    },
    viewOnMap: function (el) {
        loading.show();
        pageActions.setTitle(false);
        let nbhdId = $(el).siblings('.group__card-selected').attr('assgn-id');
        let nbhd = $(el).siblings('.group__card-name').attr('title');
        PageMethods.GetParcelPoints(nbhdId, '', (parcelPoints) => {
            if (parcelPoints.Success) {
                let parcels = parcelPoints.Data.parcelPoints;
                atcMapLayers.plotMarkers(parcels, null, () => {
                    pageActions.setTitle(`Viewing Group '${nbhd}' (${parcels.length?.toLocaleString('en-US') && (parcels.length?.toLocaleString('en-US'))} parcels)`);
                    pageActions.mapButton(0, 0, 0, 1, 0, 0, 0);
                    loading.hide();
                });
                return true;
            }
            else {
                SnackBar(`Error opening Group '${nbhd}'`, { position: 'bottom-right', autoHide: true, autoHideAfter: 3000 });
                console.error(parcelPoints.ErrorMessage);
                setTimeout(function () { loading.hide(); }, 500);
                return true;
            }
        });
    },
    assignedUser: function (x) {
        return x ? x : 'Unassigned';
    },
    reloadGroups: function () {
        loading.show('Syncing Assignment Groups. Please wait...')
        assignmentGroupStatus.loadAssignUser(function () {
            assignmentGroupStatus.loadNbhds(function () {
                loading.hide();
                assignmentGroupStatus.init();
                adhocCreate.loadAdhocUser(function () { });
            });
        });

    }
}

//const downloadList = {
//    exportData: function () {
//        let filename = 'ParcelList_' + new Date().toLocaleString() + '.xlsx';
//        var ws = XLSX.utils.json_to_sheet(initSearch.parcelList);
//        var wb = XLSX.utils.book_new();
//        XLSX.utils.book_append_sheet(wb, ws, "ParcelList");
//        XLSX.writeFile(wb, filename);
//    }
//}

const downloadList = {
    exportData: function () {
        let filename = 'ParcelList_' + new Date().toLocaleString() + '.xlsx';
        var excludedFields1 = ['Id'];
        var data = initSearch.parcelList.map(function (parcel) {
            var filteredParcel = {};
            for (var field in parcel) {
                if (!excludedFields1.includes(field)) {
                    filteredParcel[field] = parcel[field];
                }
            }
            return filteredParcel;
        });
        var ws = XLSX.utils.json_to_sheet(data);
        var wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, "ParcelList");
        XLSX.writeFile(wb, filename);
    }
}

const adhocCreate = {
    fromLasso: false,
    properties: {},
    priorityListItems: [],
    appendYesTick: [],
    loadAdhocUser: function () {
        PageMethods.AdhocUser((res) => {
            let uhtml = "<option value='0'>None, will assign later</option>";
            if (res) {
                res.forEach((u) => {
                    uhtml += "<option value=" + u.LoginId + ">" + u.UserName + "</option>";
                });
            }
            $('.ddlAssignTo').html('');
            $('.ddlAssignTo').append(uhtml);

        });
    },
    showAdhocPopup: function (flag) {
        pageActions.layerSettings(false);
        let pop = $('.adhoc-pop');
        if (flag) {
            if (hasTaskManagerRole == 'False') {
                say('You do not have the Appraisal Task Control user role, which is needed in order to create Assignment Groups.')
                return false;
            }

            if (EnableNewPriorities == 'True') {
                if (initSearch.searchList.filter((x) => { return (x['Priority'] && ['1', '2', '3', '4', '5'].includes(x['Priority'].toString())) }).length == 0) {
                    $('.adhoc__priority', pop).show();
                    $('.adhoc__priority-div').val('');
                }
                else
                    $('.adhoc__priority', pop).hide();
            }
            else {
                if (initSearch.searchList.filter((x) => { return (x['Priority'] == 1 || x['Priority'] == 2) }).length == 0) {
                    $('.adhoc__priority', pop).show();
                    $('.adhoc__priority-div').val('');
                }
                else
                    $('.adhoc__priority', pop).hide();
            }

            $('.txt-adhoc-group-name').val('').removeClass('invalid');
            $('.adhoc__error-msg').hide();
            $('.ddlAssignTo').val('0');
            $('.adhocCheck').attr('checked', false);
            pageActions.popup(pop);
        }
        else {
            pageActions.popup(false);
        }
    },
    setProperties: function (prId) {
        adhocCreate.properties = { isCreateAdhoc: 'true', CreateNbhd: 'true', priorityFileName: 'C:\ fakepath \ SamplePriorityList.csv', isDoNotReset: ($('.adhocCheck').attr('checked') == 'checked' ? 'true' : 'false'), groupCode: $('.txt-adhoc-group-name').val(), assignTo: $('.ddlAssignTo').val(), PrioTableId: prId };
    },
    resetProperties: function () {
        let prop = ['priorityFileName', 'isDoNotReset', 'isMultiAdhoc', 'groupCode', 'assignTo', 'isAppend', 'PrioTableId', 'backUpId', 'Duplicated', 'High', 'Invalid', 'Normal', 'Urgent', 'TotalParcel', 'isNotApproved', 'isAssigned', 'isNotSyncedBack', 'isWorkedInFieldLast30days', 'AdhcAssgn', 'CAMASystem', 'isDonotReset', 'fielName', 'noPriority', 'isAdho'];
        if (EnableNewPriorities == 'True') prop = ['priorityFileName', 'isDoNotReset', 'isMultiAdhoc', 'groupCode', 'assignTo', 'isAppend', 'PrioTableId', 'backUpId', 'Duplicated', 'High', 'Invalid', 'Normal', 'Urgent', 'TotalParcel', 'isNotApproved', 'isAssigned', 'isNotSyncedBack', 'isWorkedInFieldLast30days', 'AdhcAssgn', 'CAMASystem', 'isDonotReset', 'fielName', 'noPriority', 'isAdho', 'Critical', 'Proximity', 'Medium'];
        prop.forEach((p) => {
            delete adhocCreate.properties[p];
        });
    },
    createBackup: function (prId) {
        let isAdhoc = adhocCreate.properties.isCreateAdhoc;
        let AdhcAssgn = adhocCreate.properties.AdhcAssgn;
        if (AdhcAssgn == undefined) {
            let grpCode = adhocCreate.properties.groupCode;
            let assign = adhocCreate.properties.assignTo;
            AdhcAssgn = grpCode + "@!*!*$#@" + assign;
        }

        adhocCreate.takePriorityBackup(prId, isAdhoc, currentLoginId, function () {
            $(".masklayer .mask-new .mask-msg").text('Creating new Ad-Hoc. Please wait...');
            let startTime = performance.now(), searchTime;
            $atc('priority', { adhoc: isAdhoc, grpwithAss: AdhcAssgn, prioId: prId, reload: "true", selectedNbhd: "" }, function (res) {
                let t = JSON.parse(JSON.stringify(res));
                searchTime = performance.now() - startTime;
                console.warn(`Took ${parseInt(searchTime / 1000)}s for adhoc backup.`);
                adhocCreate.verifyPriorityList(t);
            }, function (res) {
                alert(JSON.parse(JSON.stringify(res)));
                loading.hide();
            });
        });
    },
    verifyPriorityList: function (t) {
        $(".group-pop input[type='checkbox']").prop('checked', false);
        let cama = t.CAMASystem, duplicated = t.Duplicated, high = t.High == null ? 0 : t.High, invalid = t.Invalid,
            normal = t.Normal == null ? 0 : t.Normal, urgent = t.Urgent == null ? 0 : t.Urgent, valid = t.Valid,
            isAssigned = t.isAssigned, isNotApproved = t.isNotApproved, isNotSyncedBack = t.isNotSyncedBack,
            isWorkedInFieldLast30days = t.isWorkedInFieldLast30days, totalParcel = t.TotalParcel, adhcAssgn = t.AdhcAssgn,
            count = 0, prioId = t.prioId, assignedGroupNames = t.AssignedGroupNames, isAdho = t.isAdho,
            isDonotReset = adhocCreate.properties.isDoNotReset == 'true' ? "1" : "0",
            critical = t.Critical == null ? 0 : t.Critical, proximity = t.Proximity == null ? 0 : t.Proximity, medium = t.Medium == null ? 0 : t.Medium, bppCount = t.BppCount;

        if (parseInt(isAssigned) == 0 && parseInt(isNotApproved) == 0 && parseInt(isNotSyncedBack) == 0 && isWorkedInFieldLast30days == 0)
            $(".group-pop__warning").hide();
        else
            $(".group-pop__warning").show();

        if (parseInt(duplicated) > 0) {
            $(".group__pop-duplicateParcel").show();
            $("#lblDuplicatedParcel").text(duplicated);
        }
        else
            $(".group__pop-duplicateParcel").hide();

        if (parseInt(invalid) > 0) {
            $("#lblInvalid").text(invalid);
            $(".group__pop-invalidParcel").show();
        }
        else
            $(".group__pop-invalidParcel").hide();

        if (parseInt(isNotApproved) > 0) {
            count += 1;
            $(".group-pop__warn--approved").show();
            $("#lblNotApprovedParcel").html("<b>" + isNotApproved + "</b>" + (isNotApproved == "1") ? " Property In your list Is currently Marked as Complete In the cloud, but<font color='red'> <b> has not been QC Approved</b></font>." : " properties in your list are currently Marked As Complete in the cloud, but<font color='red'> <b> have not been QC Approved</b></font>.");
            $("#lblRemoveNotApprovedparcel").html((isNotApproved == "1") ? "Click here to remove the property from the Priority List upload. This property <strong><u>will not</u></strong> have its priority reset" : " Click here to remove the <b>" + isNotApproved.toLocaleString('en-US') + "</b> properties from the Priority List upload. These properties <strong><u>will not</u></strong> have their priorities reset.");
        }
        else
            $(".group-pop__warn--approved").hide();


        if (parseInt(isAssigned) > 0) {
            count += 1;
            $(".group-pop__warn--assigned").show();
            $("#lblAssignedParcel").html((isAssigned == "1") ? "Your Priority List includes a property in the following Assignment Group that Is currently being worked in the field:" : "Your Priority List includes properties in the following Assignment Groups that are currently being worked in the field:");
            $("#gpAssignedGroups").html("<strong>".concat(assignedGroupNames, "</strong>"));
            $("#lblRemoveAssignedParcel").html((isAssigned == "1") ? "Click here to remove the property from the Priority List upload.This property <strong><u>will not</u></strong> have its priority reset" : "Click here to remove the <b>" + isAssigned.toLocaleString('en-US') + "</b> properties from the Priority List upload. These properties <strong><u>will not</u></strong> have their priorities reset.");
        }
        else
            $(".group-pop__warn--assigned").hide();

        if (parseInt(isNotSyncedBack) > 0) {
            count += 1;
            let select = "";

            if (cama != "") select = cama;
            else select = "CAMASystem";

            $(".group-pop__warn--synced").show();
            $("#lblNotSyncedParcel").html("<b>" + isNotSyncedBack + "</b> " + (isNotSyncedBack == "1") ? "Property in your list is currently QC Approved, but <font color='red'> <b> has not synced back to " + select + "</b></font>. " : " properties in your list are currently QC Approved, but <font color='red'><b>have not synced back to " + select + "</b></font>.");
            $("#lblRemoveNotSyncedParcel").html((isNotSyncedBack == "1") ? "Click here to remove the property from the Priority List upload. This property <strong><u>will not</u></strong> have its priority reset" : "Click here to remove the <b>" + isNotSyncedBack + "</b> properties from the Priority List upload. These properties <strong><u>will not</u></strong> have their priorities reset.");
        }
        else
            $(".group-pop__warn--synced").hide();


        if (parseInt(isWorkedInFieldLast30days) > 0) {
            count += 1;
            $(".group-pop__warn--workedRecently").html("Your Priority List includes&nbsp;<b>" + isWorkedInFieldLast30days + "</b>&nbsp;properties that were worked in the field in the last 30 days.").show();
            $("#lblRemoveWorkedInFieldLast30days").html((isWorkedInFieldLast30days == "1") ? "Click here to remove the property from the Priority List upload. This property <strong><u>will not</u></strong> have its priority reset" : "Click here to remove the <b>" + isWorkedInFieldLast30days + "</b> properties from the Priority List upload. These properties <strong><u>will not</u></strong> have their priorities reset.");
        }
        else
            $(".group-pop__warn--workedRecently").hide();

        if (count > 0) {
            $(".group-pop__alert").addClass('alert-danger');
            $(".group-pop__alert>img").attr("src", "/App_Static/css/icon16/exclamation.png");
        }
        else {
            $(".group-pop__alert").addClass('alert-success');
            $(".group-pop__alert>img").attr("src", "/App_Static/css/icon16/done.png");
        }

        let existGrp = "", grCode = "<table class='tableVerify' id ='tableVerify'>", html = "<div id ='appendableDiv'>", select = "";

        if (adhcAssgn != "") {
            let grpexists = false;
            $('.group-pop__group-exists').empty().hide();
            for (let t = 0; t < adhcAssgn.length; t++) {
                var grp = adhcAssgn[t].split("@!*!*$#@");
                if (grp[2] == "false") {
                    if (existGrp != "") {
                        existGrp = existGrp + ",";
                    }

                    grpexists = true;
                    let grpExistsWarning = "<div class='alert' id ='adhgroupExists" + t + "'>"
                        + "<div class='flex-row-center' > "
                        + "<span class ='lblAdhocNameExistsWarnings' id ='adcgroupnameExists" + t + "'>A group named  <strong>" + grp[0] + "</strong> already exists with  parcels. Do you want to append the parcels to the existing list?</span>"
                        + "<div class='flex-row-center' > "
                        + "<input type = 'radio' name = 'yes' value = '1' id = 'rdbtnYes" + t + "' />"
                        + "<label for='rdbtnYes" + t + "'> Yes, append to existing</label>"
                        + "<input type='radio' name='yes' value='0' id='rdbtnNo" + t + "' />"
                        + "<label for='rdbtnNo" + t + "'>No, I want to Rename.</label>"
                        + "</div >"
                        + "</div >"
                        + "<span style='margin-top:4px;display:block;'><b>Note:</b> If you do not rename the Group Code/Number,it will append the parcels to the existing list.</span>"
                        + "</div>";
                    $('.group-pop__group-exists').html(grpExistsWarning).show();
                    select = "<img src='/App_Static/css/icon16/exclamation.png'  id='imgdone" + t + "' style='height: 20px; position: absolute; margin-left: 5px;' class='imgGroupNumberVerify' />"
                    existGrp = existGrp + grp[0];
                }
                else {
                    select = "<img src='/App_Static/css/icon16/done.png'  id='imgdone" + t + "' style='height: 20px; position: absolute; margin-left: 5px;' class='imgGroupNumberVerify' />"
                }

                let gn = grp[1] == '0' ? 'None, will assign later' : grp[1];
                html = html + "<div class ='a tableVerify' Id ='tableVerify" + t + "' >"
                    + "<div class ='a'>"
                    + "<span>Group Code/Number: </span><input type='text' id='txtgrcode" + t + "' MaxLength='50' autocomplete='off' class='newGroupNumber leftSide' Enabled='false' disabled value='" + grp[0] + "' orginalValue='" + grp[0] + "' />"
                    + select
                    + "</div>";
                html = html + "<div class='a'>"
                    + "<span>Assign Group To: </span><input type='text' id='txtasgn" + t + "' class='txtAssignTo rightSide' Enabled='false' disabled value='" + gn + "' />"
                    + "</span>"
                    + "</div>";
                html = html + "</div>";
            }
        }

        html = html + "</div>";
        $("#adhgrpExists").html(html);

        if (existGrp != "")
            $("#adhgroupExists").show();

        $("#lblTotalParcel").text(totalParcel?.toLocaleString('en-US'));
        $("#lblUrgent").text(urgent?.toLocaleString('en-US'));
        $("#lblHigh").text(high?.toLocaleString('en-US'));
        $("#lblNormal").text(normal?.toLocaleString('en-US'));
        if (EnableNewPriorities == 'True') {
            $("#lblCritical").text(critical?.toLocaleString('en-US'));
            $("#lblMedium").text(medium?.toLocaleString('en-US'));
            $("#lblProximity").text(proximity?.toLocaleString('en-US'));
        }
        $(".group-pop-second").show();

        $('.bppUpdate').hide();
        if (parseInt(bppCount) > 0) {
            $('.bppUpdate').show();
        }

        adhocCreate.editGrpCode();
        adhocCreate.resetProperties();

        adhocCreate.properties["PrioTableId"] = prioId;
        adhocCreate.properties["Duplicated"] = duplicated;
        adhocCreate.properties["High"] = high;
        adhocCreate.properties["Invalid"] = invalid;
        adhocCreate.properties["Normal"] = normal;
        adhocCreate.properties["Urgent"] = urgent;
        if (EnableNewPriorities == 'True') {
            adhocCreate.properties["Medium"] = medium;
            adhocCreate.properties["Critical"] = critical;
            adhocCreate.properties["Proximity"] = proximity;
        }
        adhocCreate.properties["TotalParcel"] = totalParcel;
        adhocCreate.properties["isNotApproved"] = isNotApproved;
        adhocCreate.properties["isAssigned"] = isAssigned;
        adhocCreate.properties["isNotSyncedBack"] = isNotSyncedBack;
        adhocCreate.properties["isWorkedInFieldLast30days"] = isWorkedInFieldLast30days;
        adhocCreate.properties["AdhcAssgn"] = adhcAssgn;
        adhocCreate.properties["CAMASystem"] = cama;
        adhocCreate.properties["isDonotReset"] = isDonotReset;
        $('.group-pop .group-third').hide();
        pageActions.popup($('.group-pop'));
        loading.hide();
    },
    closeSecondStep: function () {
        adhocCreate.properties = {};
        adhocCreate.appendYesTick = [];
        pageActions.popup(false);
    },
    uploadListConfirmation: function () {
        if ($('.group-pop__group-exists').is(':visible') && $('input:radio[name="yes"]:checked')[0] && $('input:radio[name="yes"]:checked')[0].value == '0' && $('.leftSide').val() == '') {
            $('.newGroupNumber').css('border', '1px solid red');
            return false;
        }

        let validObj = adhocCreate.validateGroupName($('.leftSide').val());
        let returnFlag = !!validObj?.isInvalid;
        if (returnFlag) {
            $('.newGroupNumber').css('border', '1px solid red');
            return false;
        }

        ask('Are you sure you want to proceed?', function () {
            loading.show();
            if ($('.sb__item--active').attr('sb-page') == 'priority')
                adhocCreate.properties["isCreateAdhoc"] = "false";
            adhocCreate.uploadPriorityList();
            adhocCreate.progress();
            return false;
        });
    },
    uploadPriorityList: function () {
        let removeApprovedParcels, removeNotSyncedBackParcels, removeAssignedParcels, removeWorkedInFieldLast30daysParcels;
        let createNbhd = adhocCreate.properties["CreateNbhd"], donotReset, grpString = "";
        if (adhocCreate.properties['isCreateAdhoc'] == null) {
            if (adhocCreate.properties["fileName"] != null)
                adhocCreate.properties['isCreateAdhoc'] = 'false';
            else
                adhocCreate.properties['isCreateAdhoc'] = 'true';
        }

        let createAdh = adhocCreate.properties['isCreateAdhoc'], priorityType = "0";
        removeApprovedParcels = ($("#chkNotApprovedparcel").attr('checked') == 'checked') ? true : false;
        removeNotSyncedBackParcels = ($("#chkNotSyncedparcel").attr('checked') == 'checked') ? true : false;
        removeAssignedParcels = ($("#chkAssignedParcel").attr('checked') == 'checked') ? true : false;
        removeWorkedInFieldLast30daysParcels = ($("#chkWorkedInFieldLast30days").attr('checked') == 'checked') ? true : false;

        if (createAdh == "true" || createNbhd == "true") {
            createNbhd = true; grpString = "";
            let divCollection = $("#appendableDiv").children('div .tableVerify');
            divCollection.each(function (index, item) {
                let l = $(this).$('.leftSide').val(), r = $(this).$('.rightSide').val();
                r = (r == 'None, will assign later') ? '0' : r;
                if (grpString != "")
                    grpString = grpString + ","
                grpString = grpString + l + "@!*!*$#@" + r;
            });
        }
        else
            createNbhd = false;

        donotReset = adhocCreate.properties["isDonotReset"] == 'true' || adhocCreate.properties["isDonotReset"] == '1'? "1": "0";
        let prioId = adhocCreate.properties["PrioTableId"], backupId = adhocCreate.properties["backupId"],
            duplicated = adhocCreate.properties["Duplicated"], high = adhocCreate.properties["High"],
            invalid = adhocCreate.properties["Invalid"], normal = adhocCreate.properties["Normal"],
            urgent = adhocCreate.properties["Urgent"], totalParcel = adhocCreate.properties["TotalParcel"];
        critical = EnableNewPriorities == 'True' ? adhocCreate.properties["Critical"] : 0, medium = EnableNewPriorities == 'True' ? adhocCreate.properties["Medium"] : 0, proximity = EnableNewPriorities == 'True'? adhocCreate.properties["Proximity"]: 0;
        var updateBpp = $('.bppUpdate').is(':visible') ? $('input[name="bppGroup"]:checked').val() : '0';
        $atc('priorityupload', { updateBpp: updateBpp, prioId: prioId, CreateNbhd: createNbhd, DonotReset: donotReset, appendGrp: "", backupId: backupId, grpString: grpString, createAdh: createAdh, priorityType: priorityType, removeApprovedParcels: removeApprovedParcels, removeNotSyncedBackParcels: removeNotSyncedBackParcels, removeAssignedParcels: removeAssignedParcels, removeWorkedInFieldLast30daysParcels: removeWorkedInFieldLast30daysParcels, Duplicated: duplicated, High: high, Invalid: invalid, Normal: normal, Urgent: urgent, TotalParcel: totalParcel, Critical: critical, Medium: medium, Proximity: proximity }, function (res) {
        //var updateBpp = $('input[name="bppGroup"]:checked').val();
        //$atc('priorityupload', { updateBpp: updateBpp, prioId: prioId, CreateNbhd: createNbhd, DonotReset: donotReset, appendGrp: "", backupId: backupId, grpString: grpString, createAdh: createAdh, priorityType: priorityType, removeApprovedParcels: removeApprovedParcels, removeNotSyncedBackParcels: removeNotSyncedBackParcels, removeAssignedParcels: removeAssignedParcels, removeWorkedInFieldLast30daysParcels: removeWorkedInFieldLast30daysParcels, Duplicated: duplicated, High: high, Invalid: invalid, Normal: normal, Urgent: urgent, TotalParcel: totalParcel }, function (res) {
            let t = JSON.parse(JSON.stringify(res));
            let multiple = t.multiGrp, result = t.Result;

            $("#lblTotalAffectedparcelCount").html(t.AffectedParcelCount);
            $("#lblTotalAffectedUrgentCount").html(t.AffectedUrgentCount);
            $("#lblTotalAffectedHighCount").html(t.AffectedHighCount);
            if (EnableNewPriorities == 'True') {
                $("#lblTotalAffectedCriticalCount").html(t.AffectedCriticalCount);
                $("#lblTotalAffectedMediumCount").html(t.AffectedMediumCount);
                $("#lblTotalAffectedProximityCount").html(t.AffectedProximityCount);
            }
            $("#lblTotalAffectedNormalCount").html(t.AffectedNormalCount);
            $("#lblSuccess").html(t.resultString.replace(' and assigned to 0', ''));
            $(".group-pop-second").hide();
            $(".group-third").show();

            adhocCreate.resetProperties();
            loading.hide();
        }, function (res) {
            let data = JSON.parse(JSON.stringify(res));
            alert(data);
            loading.hide();
        });
    },
    closeThirdStep: function () {
        $atc("updateclient", null, function (res) {
            let pId = $('.sb__item--active').attr('sb-page') == 'priority' ? priority.Id : '';
            adhocCreate.properties = {};
            adhocCreate.appendYesTick = [];
            $('#group-progressBar').hide();
            var finalstep = function () {
                pageActions.popup(false);
            }

            let pl = adhocCreate.priorityListItems.toString();

            PageMethods.UpdateAtcPriority(pl, pId, (res) => {
                if (res.Success) {
                    let sl = res.Data.SearchLists;
                    if (initSearch.searchList.length == sl.length)
                        initSearch.searchList = sl;
                    else {
                        sl.forEach((pl) => {
                            if (initSearch.searchList.length > 0) {
                                let l = initSearch.searchList.filter((x) => { return x.Id == pl.Id })[0];
                                l.Priority = pl.Priority; l.Reviewed = pl.Reviewed; l.QC = pl.QC;
                                l.NbhdName = pl.NbhdName; l.AssignedTo = pl.AssignedTo;
                            }
                        });
                    }
                    atcMapLayers.plotMarkers(initSearch.searchList, true, () => {
                        pageActions.mapButton(1, 0, 0, 1, 1);
                    });
                }
                finalstep();
            });
        }, function (res) {
            let data = JSON.parse(JSON.stringify(res));
            alert(data);
        });
    },
    progress: function () {
        $('#group-progressBar').show();
        var frame = function () {
            if (progress >= 95) {
                clearInterval(intId);
                $('#group-currentProgress').width(100 + '%')
                intId = 0;
            }
            else {
                progress++;
                $('#group-currentProgress').width(progress + '%');
            }
        }
        var progress = 1, intId = setInterval(frame, 7);
    },
    takePriorityBackup: function (prioId, isAdhoc, user, callback) {
        $(".masklayer .mask-new .mask-msg").text('Creating priority list backup. Please wait...');
        $atc("priobackup", { adhoc: isAdhoc, prioId: prioId, fileName: isAdhoc ? "AdhocFile.csv" : adhocCreate.properties.fileName, user: user }, function (res) {
            let pb = JSON.parse(JSON.stringify(res));
            adhocCreate.properties.backupId = pb.backupId;
            if (callback) callback();
        }, function (res) {
            alert(JSON.parse(JSON.stringify(res)));
            loading.hide();
        });
    },
    validateGroupName: (name) => {

        let charCheck = ["{", "}", "(", ")", "'", '"', ',', "/"];
        let isInvalid = false, message = '';

        if (name == '' || name?.length == name?.match(/\./g)?.length) {
            message = "Please provide a valid name for the Group Name";
            isInvalid = true;
        }

        if (name.indexOf('@!*!*$#@') > -1) {
            message = "Given Group name contains an reserved character '@!*!*$#@'. Please specify another.";
            isInvalid = true;
        }

        if (name.indexOf('>') > -1 || name.indexOf('<') > -1) {
            message = "Special characters '>' and '<' can not be used as a Group Name.";
            isInvalid = true;
        }

        if (charCheck.some(function (x) { return name.indexOf(x) > -1 })) {
            message = 'Please avoid invalid characters from Ad-Hoc Assignment Group name.';
            isInvalid = true;
        }

        if (!(/\d/.test(name) || /[a-zA-Z]/.test(name))) {
            message = 'Please add atleast a letter or number in the Assignment Group name.';
            isInvalid = true;
        }

        return { isInvalid, message }
    },
    createPriority: function () {
        adhocCreate.properties = {};
        let adhocgroupname = $('.txt-adhoc-group-name').val();
        let validObj = adhocCreate.validateGroupName(adhocgroupname);
        let returnFlag = !!validObj?.isInvalid;

        if (returnFlag) {
            $('.adhoc__error-msg').text(validObj?.message).show();
            $('.txt-adhoc-group-name').addClass('invalid');
            return false;
        }

        if ($('.adhoc__priority').is(":visible") && !$('.adhoc__priority-div').val()) {
            say('Please choose a priority.');
            return false;
        }
        //if ($('.adhoc__priority').is(":visible") && !$('.adhoc__priority-radio:checked').val()) {
        //    say('Please choose a priority.');
        //    return false;
        //}

        let u = $('.ddlAssignTo').val();
        let p = $('.adhoc__priority').is(":visible") ? $('.adhoc__priority-div').val() : '';
        //let p = $('.adhoc__priority').is(":visible") ? $('.adhoc__priority-radio:checked').val() : '';
        let prioityId = $('.sb__item--active').attr('sb-page') == 'priority' ? priority.Id : '';

        ask('This will create an Ad-Hoc Assignment Group containing the parcels in the search results below. Do you want to proceed?', function () {
            let pl = adhocCreate.priorityListItems.toString();
            adhocCreate.showAdhocPopup();
            loading.show('Creating list. Please wait...');
            PageMethods.CreatePriority(pl, p, prioityId, (res) => {
                if (res.indexOf('Success~') > -1) {
                    let prId = res.split('Success~')[1];
                    adhocCreate.setProperties(prId);
                    adhocCreate.appendYesTick = [];
                    adhocCreate.properties["NewPriority"] = p;
                    adhocCreate.createBackup(prId);
                    return true;
                }
                else {
                    loading.hide();
                    SnackBar('Adhoc creation failed', { position: 'bottom-right', autoHide: true, autoHideAfter: 3000 });
                }
            });
        });
    },
    editGrpCode: function () {
        $('input:radio[name="yes"]').click(function () {
            if ($(this).is(':checked')) {
                let id = $(this).attr("id"), identifier;
                if ($(this).val() == "0") {
                    identifier = id.substring(7);
                    $("#txtgrcode" + identifier).prop('disabled', false);
                    $('#btnContinue').attr('disabled', 'disabled');
                    $('#imgdone' + identifier).attr('src', '/App_Static/css/icon16/exclamation.png');
                    adhocCreate.groupValidation();
                }
                else {
                    identifier = id.substring(8);
                    adhocCreate.appendYesTick.push(identifier);
                    $("#txtgrcode" + identifier).prop('disabled', 'disabled');
                    $('#adhgroupExists' + identifier).hide();
                    $('#imgdone' + identifier).attr('src', '/App_Static/css/icon16/done.png');
                }
            }
        });
    },
    groupValidation: function () {
        let oldName = '';

        $(".newGroupNumber").click(function () {
            oldName = $(this).attr("orginalvalue");
        });

        $(".newGroupNumber").keyup(function () {
            $('.newGroupNumber').css('border', '')
        });

        $(".newGroupNumber").focusout(function () {
            let id = $(this).attr("id"), identifier = id.substring(9), adhocgroupname = $(this).val();
            if (adhocgroupname == '') {
                $('.newGroupNumber').css('border', '1px solid red');
                return false;
            }

            let validObj = adhocCreate.validateGroupName($('.leftSide').val());
            let returnFlag = !!validObj?.isInvalid;
            if (returnFlag) {
                $('.newGroupNumber').css('border', '1px solid red');
                return false;
            }

            $atc('verifyahg', { name: adhocgroupname, prioId: adhocCreate.properties.PrioTableId, oldName: oldName }, function (resp) {
                if (resp.status == "OK") {
                    if (!resp.exists) {
                        $('#imgdone' + identifier).attr('src', '/App_Static/css/icon16/done.png');
                        $('#adhgroupExists' + identifier).hide();
                        $('#' + id).prop('disabled', 'disabled');

                    }
                    else {
                        let msg = resp.message.replace(/&lt;/g, '<').replace(/&gt;/g, '>');
                        $('.lblAdhocNameExistsWarnings').html(msg + '. Do you want to append the parcels to the existing list?');
                        $('#imgdone' + identifier).attr('src', '/App_Static/css/icon16/exclamation.png');
                        $('#adhgroupExists' + identifier).show();
                    }
                }
                else {
                    alert(resp.message);
                }
            });
        });
    }
}

const pageTour = {
    sideTour: new Tour({
        steps: [
            {
                element: ".sb__item[sb-page='dashboard']",
                title: "Dashboard",
                content: "Shows recently worked groups and appraiser progress. Also shows 3 of the most recently saved filters and results when loaded first time.",
                placement: "right"
            },
            {
                element: ".sb__item[sb-page='search']",
                title: "Search",
                content: "Search for Parcels and see the results in Map. Also the search filters can be saved here.",
                placement: "right"
            },
            {
                element: ".sb__item[sb-page='filter']",
                title: "Saved Filters",
                content: "Shows all the saved filters for searching. ",
                placement: "right"
            },
            {
                element: ".sb__item[sb-page='result']",
                title: "Saved Results",
                content: "Shows all the previously saved results view.",
                placement: "right"
            },
            {
                element: ".sb__item[sb-page='group']",
                title: "Assignment Group Status",
                content: "This page will list all the assignment groups and their statuses.",
                placement: "right"
            },
            {
                element: ".sb__item[sb-page='priority']",
                title: "Upload Priority List",
                content: "Can upload priority list in this view and can view it on the map.",
                placement: "right"
            }
        ],
        backdrop: true,
        storage: false,
        name: 'Sidebar',
        onEnd: (x) => {
            pageTour?.tourEnd(x);
        }
    }),
    dashTour: new Tour({
        steps: [
            {
                element: ".dash__card:first-child",
                title: "Dashboard Card",
                content: "Shows the details of recently worked groups of an appraiser.",
                placement: "right"
            },
            {
                element: ".dash__card:first-child .dash__card-grp",
                title: "Recent Group Count",
                content: "Shows the count of recently worked groups of an appraiser.",
                placement: "right"
            },
            {
                element: ".dash__card-agroup:first-child",
                title: "Appraiser with Recently Worked Group",
                content: "If there are any recently worked groups, clicking on the card will show the recent groups and their progress",
                placement: "right"
            },
            {
                element: ".dash__card-agroup:first-child",
                title: "Appraiser with Recently Worked Group",
                content: "If there are any recently worked groups, clicking on the card will show the recent groups and their progress",
                placement: "right"
            },
            {
                element: ".dash__card-agroup:first-child",
                title: "Recently Worked Group",
                content: "Should show the recently worked group and it's progress. Clicking the group name will show the Group Parcels on the map",
                placement: "right"
            },
        ],
        backdrop: true,
        storage: false, name: 'Dashboard',
        onStart: (x) => {
            //Finding the first card with assignment groups inside.
            let fCard = $('.dash__card-agroup:first-child').eq(0).parents('.dash__card');

            x._options.steps[2].element = fCard;
            x._options.steps[3].element = fCard;
            x._options.steps[4].element = $('.dash__card-expand', fCard);
        },
        onNext: (x) => {
            if (x?._current == 2) $('.dash__card-title', x._options.steps[2].element).trigger('click');
        },
        onEnd: (x) => {
            let fCard = $('.dash__card-expand.card__detail--expanded').parents('.dash__card');
            $('.dash__card-title', fCard).trigger('click');
            pageTour?.tourEnd(x);
        },
    }),
    searchTour: new Tour({
        steps: [
            {
                element: ".search__card--default:nth-child(1)",
                title: "MA Reviewed Status",
                content: "Used to filter Parcels based on their MA Reviewed Status. Choose <b>NO</b> for 'Not Reviewed', <b>YES</b> for 'Reviewed' and <b>NA</b> for showing both.",
                placement: "right"
            },
            {
                element: ".search__card--default:nth-child(2)",
                title: "DTR Reviewed Status",
                content: "Used to filter Parcels based on their DTR Reviewed Status. Choose <b>NO</b> for 'Not Reviewed', <b>YES</b> for 'Reviewed' and <b>NA</b> for showing both.",
                placement: "right"
            },
            {
                element: ".search__card--default:nth-child(3)",
                title: "QC Approved Status",
                content: "Used to filter Parcels based on their QC Approved Status. Choose <b>NO</b> for 'Not Approved', <b>YES</b> for 'Approved' and <b>NA</b> for showing both.",
                placement: "right"
            },
            {
                element: ".search__add-filter",
                title: "Add New Filter",
                content: "Used to add more conditions to the Search Query.",
                placement: "right"
            },
            {
                element: ".search__af",
                title: "Add Filter",
                content: "Add filter conditions here. Choose the field using the first dropdown, condition in the 2nd and give Value in the 3rd input if needed.",
                placement: "right"
            },
            {
                element: ".search__actions__save",
                title: "Save as Filter",
                content: "Save the added filter conditions for future uses. Can be found in Saved Filters page.",
                placement: "right"
            },
        ],
        backdrop: true,
        storage: false,
        name: 'Search',
        onStart: (x) => {
        },
        onNext: (x) => {
            if (x?._current == 3) $('.search__add-filter').trigger('click');
        },
        onEnd: (x) => {
            $('.search__af-buttons #closefilter').trigger('click');
            pageTour?.tourEnd(x);
        },
    }),
    filtersTour: new Tour({
        steps: [
            {
                element: ".filter__list div:first",
                title: "Saved Filter",
                content: "Shows the details of recently Saved Filters.",
                placement: "right"
            },
            {
                element: ".filter__list div:first",
                title: "Recently Saved Filter",
                content: "Shows the details of conditions applied.Clicking the Search button will show the Saved Filter's Parcels on the map.",
                placement: "right"
            }
        ],
        backdrop: true,
        storage: false, name: 'Filters',
        onStart: (x) => { },
        onNext: (x) => { },
        onEnd: (x) => {
            pageTour?.tourEnd(x);
        },
    }),
    resultsTour: new Tour({
        steps: [
            {
                element: ".result__list div:first",
                title: "Saved Result",
                content: "Shows the details of recently Saved Result.",
                placement: "right"
            },
            {
                element: ".result__list div:first",
                title: "Appraiser with Recently Saved Result",
                content: "Should show the Appraiser with Pracel count.Clicking the View button will show the Saved Result's Parcels on the map.",
                placement: "right"
            }
        ],
        backdrop: true,
        storage: false, name: 'Results',
        onStart: (x) => { },
        onNext: (x) => { },
        onEnd: (x) => {
            pageTour?.tourEnd(x);
        },
    }),
    groupTour: new Tour({
        steps: [
            {
                element: ".group__filter-div",
                title: "Assignment Group Filter",
                content: "Shows the Assignment Group Flags and the Assigned User List for filtering.",
                placement: "right"
            },
            {
                element: ".group__card:first-child",
                title: "Assignment Group Status",
                content: "Shows the details of Assignment Group Status with Appraiser.It includes the Total and Visited count of Pracels and their Priority.",
                placement: "right"
            },
            {
                element: ".group__page-div",
                title: "Assignment Group Status Page Selection",
                content: "Shows the Pages of Assignment Group for selection.",
                placement: "right"
            }
        ],
        backdrop: true,
        storage: false, name: 'Group',
        onStart: (x) => { },
        onNext: (x) => { },
        onEnd: (x) => {
            pageTour?.tourEnd(x);
        },
    }),
    priorityTour: new Tour({
        steps: [
            {
                element: ".priority__info",
                title: "Priority List Upload",
                content: "Shows the details of Priority List Upload.",
                placement: "right"
            },
            {
                element: ".priority__sample",
                title: "Download Sample",
                content: "Download the sample file for the Priority List Upload.",
                placement: "right"
            },
            {
                element: ".priority__file-upload",
                title: "Select File",
                content: "Select the Priority List Upload file.",
                placement: "right"
            },
            {
                element: ".priority__btn-upload",
                title: "Upload File",
                content: "Upload the Priority List file.",
                placement: "right"
            }
        ],
        backdrop: true,
        storage: false, name: 'Priority',
        onStart: (x) => { },
        onNext: (x) => { },
        onEnd: (x) => {
            pageTour?.tourEnd(x);
        },
    }),
    startTour: function () {
        let obj = JSON.parse(localStorage.getItem("ATC_Tour"));
        if (!obj) {
            obj = {
                Sidebar: false,
                Dashboard: false,
                Search: false,
                Filters: false,
                Results: false,
                Group: false,
                Priority: false,
            }
            localStorage.setItem("ATC_Tour", JSON.stringify(obj));
        }
        if (!(obj?.Sidebar)) pageTour.sideTour.start();
        else if (!(obj?.Search) && $('.sb__item.sb__item--active').attr('sb-page') == 'search') pageTour.showTour('search')
        else if (!(obj?.Filters) && $('.sb__item.sb__item--active').attr('sb-page') == 'filter') pageTour.showTour('filter')
        else if (!(obj?.Results) && $('.sb__item.sb__item--active').attr('sb-page') == 'result') pageTour.showTour('result')
        else if (!(obj?.Group) && $('.sb__item.sb__item--active').attr('sb-page') == 'group') pageTour.showTour('group')
        else if (!(obj?.Priority) && $('.sb__item.sb__item--active').attr('sb-page') == 'priority') pageTour.showTour('priority')
    },
    init: function () {
    },
    showTour: function (page) {
        if (!page) page = $('.sb__item.sb__item--active').attr('sb-page');
        if (page == 'dashboard') pageTour.dashTour.start();
        else if (page == 'search') pageTour.searchTour.start();
        else if (page == 'filter') pageTour.filtersTour.start();
        else if (page == 'result') pageTour.resultsTour.start();
        else if (page == 'group') pageTour.groupTour.start();
        else if (page == 'priority') pageTour.priorityTour.start();
    },
    tourEnd: function (tour) {
        let name = tour?._options?.name || '';
        let obj = JSON.parse(localStorage.getItem("ATC_Tour"));
        if (name == 'Sidebar') {
            obj.Sidebar = true;
            pageTour.showTour();
        }
        else if (name == 'Dashboard') obj.Dashboard = true;
        else if (name == 'Search') obj.Search = true;
        else if (name == 'Filters') obj.Filters = true;
        else if (name == 'Results') obj.Results = true;
        else if (name == 'Group') obj.Group = true;
        else if (name == 'Priority') obj.Priority = true;
        localStorage.setItem("ATC_Tour", JSON.stringify(obj));
    },
    restartTour: () => {
        localStorage.setItem("ATC_Tour", null);
        pageTour.startTour();
    }
}

var atcMap = {
    OSMStreet: L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        minZoom: 0
    }),
    googleStreets: L.tileLayer('https://mt0.google.com/vt/lyrs=m&hl=en&x={x}&y={y}&z={z}&s=Ga', {
        maxZoom: 19,
        minZoom: 10,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    }),
    googleHybrid: L.tileLayer('https://mt0.google.com/vt/lyrs=y&hl=en&x={x}&y={y}&z={z}&s=Ga', {
        maxZoom: 19,
        minZoom: 0,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    }),
    toggleButton: L.easyButton({
        position: 'bottomleft',
        states: [{
            stateName: 'enabled-cluster',
            icon: 'cluster-deactive',
            title: 'Disable clustering',

            onClick: function (control) {
                atcMapLayers.groupMarkerLayer.disableClustering();
                control.state('disabled-cluster');
            }
        },
        {
            icon: 'cluster-active',
            stateName: 'disabled-cluster',
            onClick: function (control) {
                atcMapLayers.groupMarkerLayer.enableClustering()
                control.state('enabled-cluster');
            },
            title: 'Enable Clustering'
        }]
    }),
    imageDownload: L.easyPrint({
        title: 'Export View as Image',
        position: 'topleft',
        exportOnly: true,
        hidden: true,
        spinnerBgColor: 'yellow',
        hideControlContainer: false,
        sizeModes: ['Current']
    }),
    lasso: '',
    geocoded: false,
    center: '',
    init: function () {
        map = L.map('map', {
            center: atcMap.center || [47.872144, -87.758075],
            zoom: 10,
            minZoom: 6,
            maxZoom: 19,
            layers: [this.googleHybrid],
            zoomControl: false,
        });
        if (this.geocoded == false && (countyName || countyLatLong)) {
            try {
                if (atcMap.center == '' || countyName) {
                    atcMap.geoCode({ 'address': countyName });
                }
                if (atcMap.center == '' && countyLatLong?.lat && countyLatLong?.lng) {
                    var latlng = new google.maps.LatLng(countyLatLong.lat, countyLatLong.lng);
                    atcMap.geoCode({ 'location': latlng });
                }
            }
            catch (e) {
                console.log(e)
            }
        }
    },
    reset: function () {
        pageActions.mapButton();
        pageActions.setTitle(false);
        atcMapLayers.addLayerButton(false);
        lasso.lassoList = [];
        $('#lassoCount').text('');
       	$('#lassoCount').hide();
        map.remove();
        atcMap.init();
        atcMapLayers.init();
        atcMapLayers.groupMarkerLayer.addTo(map);
        for (const layer in atcMapLayers.layers) {
            atcMapLayers.layerControl.addOverlay(atcMapLayers.layers[layer], atcMapLayers.layers[layer].options.id)
        }
    },
    geoCode: function (request) {
        geocoder = new google.maps.Geocoder();
        geocoder
            .geocode(request)
            .then((result) => {
                const { results } = result;
                atcMap.center = [results[0].geometry.location.lat(), results[0].geometry.location.lng()];
                map.flyTo(new L.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng()), 8, {
                    animate: true,
                    duration: 5
                });
                atcMap.geocoded = true;

            }, (noResult) => { atcMap.geocoded = false })
            .catch((e) => {
                atcMap.geocoded = false;
            });
    },
    saveAsImage: () => {
        loading.show('Downloading MapView Image');
        atcMap.imageDownload.printMap('CurrentSize', 'Map');
        setTimeout(function () {
            loading.hide();
        }, 2500);
    }
}

var atcMapLayers = {
    addLayerButton: function (flag) {
        $('.add-overlay')[flag ? 'show' : 'hide']();
    },
    cancelLayerPopup: function () {
        $('.add-overlay').show();
        pageActions.popup(false);
    },
    baseMaps: {
        "Google Map": atcMap.googleHybrid,
        "OpenStreet Map": atcMap.OSMStreet
    },
    overlayMaps: {

    },
    groupMarkerLayer: L.markerClusterGroup({
        maxClusterRadius: 80,
        disableClusteringAtZoom: 16
    }),
    tempMarkerLayer: L.markerClusterGroup({
        maxClusterRadius: 80,
        disableClusteringAtZoom: 16,
    }),
    layerControl,
    layers: [],
    arrayOfMarkers: [],
    init: function () {
        this.layerControl = L.control.layers(this.baseMaps, this.overlayMaps, { position: 'bottomright' });
        atcMapLayers.initLayerControl(true);
        atcMap.lasso = L.lasso(map);
        lasso.lassoListeners();
        atcMap.imageDownload.addTo(map);
        atcMapLayers.addLegend();
        $('.legend-atc-div').hide();
    },
    initLayerControl: function (FreshLoad) {

        if (FreshLoad) {
            $(".layer-select__radio").html('');
            var noBasemap = true;
            for (const layer in atcMapLayers.baseMaps) {
                $(".layer-select__radio").append(`
	        				<div class="flex-row-center">
	                            <input type="radio" id="baseLayer-${layer.replace(/\s+/g, '')}" name="mapRadioGroup" value="${layer}">
	                            <label for="baseLayer-${layer.replace(/\s+/g, '')}">${layer}</label>
	                        </div>`);
                noBasemap ? $(`#baseLayer-${layer.replace(/\s+/g, '')}`).prop("checked", true) : ''
                noBasemap = false;
            }
            atcMapLayers.groupMarkerLayer.clearLayers();
            if (atcMapLayers.layers.length != 0) {
                $(".layer-list").html('');
                $(".layer-list").append(`<p class="layer-list__title">Added Layers</p>`);
                for (const layer in atcMapLayers.layers) {
                    layerName = atcMapLayers.layers[layer].options.id
                    $(".layer-list").append(`
		        		<div class="flex-row-center layer-select__item">
		                    <input id="${layerName}" type="checkbox" value="${layerName}">
		                    <label for="${layerName}" class="text-ellipsis">${layerName}</label>
		                    <span class="btn btn--icon" onclick="atcMapLayers.deleteLayer('${layerName}')"><i class="ic ic__delete"></i></span>
		                </div>
		            `);
                }
            } else {
                $(".layer-list").html('');
            }
        } else {
            if (atcMapLayers.layers.length != 0) {
                $(".layer-list").html('');
                $(".layer-list").append(`<p class="layer-list__title">Added Layers</p>`);
                for (const layer in atcMapLayers.layers) {
                    layerName = atcMapLayers.layers[layer].options.id
                    $(".layer-list").append(`
		        		<div class="flex-row-center layer-select__item">
		                    <input id="${layerName}" type="checkbox" value="${layerName}">
		                    <label for="${layerName}" class="text-ellipsis">${layerName}</label>
		                    <span class="btn btn--icon" onclick="atcMapLayers.deleteLayer('${layerName}')"><i class="ic ic__delete"></i></span>
		                </div>
		            `);
                }
            } else {
                $(".layer-list").html('');
            }
        }
        atcMapLayers.layerControlEvents();

    },
    layerControlEvents: function () {
        $('input[type=radio][name="mapRadioGroup"]').off("change");
        $('.layer-list input:checkbox').off("change");
        $('input[type=radio][name="mapRadioGroup"]').change(function () {
            for (const layer in atcMapLayers.baseMaps) {
                map.removeLayer(atcMapLayers.baseMaps[layer])
            }
            map.addLayer(atcMapLayers.baseMaps[this.value]);
        });
        $('.layer-list input:checkbox').change(function () {
            var index;            
            var doTempLayer = map.hasLayer(atcMapLayers.tempMarkerLayer);// indicating an active search result is on the screen
            if (doTempLayer) {
                atcMapLayers.layers.forEach((layer) => layer.removeFrom(atcMapLayers.groupMarkerLayer))
            }
            for (const layer in atcMapLayers.layers) {
                if (atcMapLayers.layers[layer].options.id == this.value) {
                    index = layer;
                }
            }
            if(lasso.lassoList.length != 0){
            
            	lasso.lassoList.forEach(parcel =>{
            		parcel.options.icon.options.className = "";
            		if(adhocCreate.priorityListItems.includes(parcel.options.parcelInfo.Id)){
	            			const index = adhocCreate.priorityListItems.indexOf(parcel.options.parcelInfo.Id);
							if (index > -1) {
							  adhocCreate.priorityListItems.splice(index, 1);
							}
            		}
            	});
            	lasso.lassoList = [];
            }
            $('#lassoCount').hide();
        	$('#lassoCount').text('');
        	pageActions.mapButton(0, 0, 0,-1,-1, 0, 0);
            if ($(this).is(':checked')) {
                if (doTempLayer) {
                    map.removeLayer(atcMapLayers.tempMarkerLayer);
                    loading.show('Clearing search result'); setTimeout(loading.hide, 1000)
                    pageActions.setTitle(false);
                }

                atcMapLayers.layers[index].addTo(atcMapLayers.groupMarkerLayer);
                atcMapLayers.groupMarkerLayer.addTo(map);
                try {
                    if (!(atcMapLayers.layers[index].lBounds && atcMapLayers.layers[index].lBounds != '')) {
                        map.fitBounds(atcMapLayers.layers[index].lBounds);
                    } else throw 'error';
                }
                catch (e) {
                    atcMapLayers.layers[index].lBounds = [];
                    Object.values(atcMapLayers.layers[index]._layers).forEach((t) => { atcMapLayers.layers[index].lBounds.push(t.getLatLng()) })
                    map.fitBounds(atcMapLayers.layers[index].lBounds);
                }

            }
            else {

                //atcMapLayers.groupMarkerLayer.removeLayer(atcMapLayers.layers[index]);
                atcMapLayers.layers[index].removeFrom(atcMapLayers.groupMarkerLayer);
                var del = Object.values(atcMapLayers.layers[index]._layers);
                lasso.lassoList = lasso.lassoList.filter(val => !del.includes(val));
                if (lasso.lassoList.length == 0) {
                    pageActions.mapButton(-1, -1, -1, -1, 1, 0, 0)
                } else {
                    pageActions.mapButton(1, -1, -1, -1, 1, 1, 1)
                }
            }
        });
    },
    addLayerToLayerGroup: function () {
        let layerName = $('.layer-pop__name').val(), color = $('.layer-pop__color').val();
        let format = /[ `!@$%^&()+\=\[\]{};':"\\|,.<>\/?~]/;
        if (layerName == '' || format.test(layerName)) {
            $('.layer-pop .sb__validate').text("* Please provide a valid Layer Name .( Allowed special characters are -_ and no spaces allowed.)").show();
            return;
        } else {
            $('.layer-pop .sb__validate').text("").hide();
            for (const layer in atcMapLayers.layers) {
                if (atcMapLayers.layers[layer].options.id == layerName) {
                    say("Layer Name already exists");
                    return;
                }
            }
        }

        if (color == '') {
            $('.layer-pop .sb__validate').text("* Please select a valid Color").show();
            return;
        }

        $('.add-overlay').hide();
        pageActions.popup(false);
        if (map.hasLayer(atcMapLayers.tempMarkerLayer)) {
            atcMapLayers.tempMarkerLayer.remove();
            atcMapLayers.groupMarkerLayer.clearLayers();
        }
        var newLayer = L.featureGroup.subGroup(atcMapLayers.groupMarkerLayer);
        newLayer.options.id = layerName;
        for (i = 0; i < atcMapLayers.arrayOfMarkers.length; i++) {
            parcelStatus = atcMapLayers.arrayOfMarkers[i].options.parcelStatus;
            parcelStatusStyle = atcMapLayers.arrayOfMarkers[i].options.parcelStatusStyle;
            const markerHtmlStyles = `
				background-color: ${atcMapLayers.arrayOfMarkers[i].options.priorityColor} ;
				width: 1.50rem;
				height: 1.50rem;
				display: block;
				left: -0.75rem;
				top: -0.50rem;
				position: relative;
				border-radius: 3rem 3rem 0;
				transform: rotate(45deg);
				border: .40rem solid ${color}`
            const icon = L.divIcon({
                className: "",
                iconAnchor: [0, 24],
                labelAnchor: [-6, 0],
                popupAnchor: [0, -36],
                html: `<span style="${markerHtmlStyles}" /><span class="${parcelStatusStyle}"> ${parcelStatus} </span>`
            })
            atcMapLayers.arrayOfMarkers[i].setIcon(icon);
            atcMapLayers.arrayOfMarkers[i].addTo(newLayer);
        }
        atcMapLayers.layers.push(newLayer);
        newLayer.addTo(atcMapLayers.groupMarkerLayer)
        atcMapLayers.groupMarkerLayer.addTo(map);
        pageActions.setTitle(false);
        pageActions.mapButton(0, 0, 0, 1, 1, 0, 0);
        atcMapLayers.initLayerControl(false);
        $(`#${layerName}`).prop("checked", true);
        atcMapLayers.layerControlEvents();
        $('#lassoCount').text('');  //24803
        $('#lassoCount').hide();  //24803
        pageActions.mapButton(0, 1, -1, -1, -1, -1, -1);
        adhocCreate.priorityListItems = [];
        lasso.lassoList = [];  //24803
    },
    deleteLayer: function (layerName) {
        atcMapLayers.layers = atcMapLayers.layers.filter(function (elem) {
            if (elem.options.id == layerName) {
                atcMapLayers.groupMarkerLayer.removeLayer(elem);
                var del = Object.values(elem._layers);
                lasso.lassoList = lasso.lassoList.filter(val => !del.includes(val));
				del.forEach(parcel =>{
            		if(adhocCreate.priorityListItems.includes(parcel.options.parcelInfo.Id)){
	            			const index = adhocCreate.priorityListItems.indexOf(parcel.options.parcelInfo.Id);
							if (index > -1) {
							  adhocCreate.priorityListItems.splice(index, 1);
							}
            		}
            	});
            } else {
                return elem;
            }
        });
        if (lasso.lassoList.length == 0) {
            pageActions.setTitle(false);
            $('#lassoCount').text('');
            $('#lassoCount').hide();
            if (atcMapLayers.layers.length == 0) {
                pageActions.mapButton(0, 0, 0, 0, 0, 0, 0);
            } else {
                pageActions.mapButton(0, -1, -1, -1, 1, 0, 0);
            }
        } else {
            $('#lassoCount').text(lasso.lassoList.length?.toLocaleString('en-US'));
                $('#lassoCount').show();
        }
        adhocCreate.priorityListItems = [];
        lasso.lassoList.forEach((p) => {
            //initSearch.searchList.push(p.options.parcelInfo);
            adhocCreate.priorityListItems.push(p.options.parcelInfo.Id);
        });
        $(`.layer-list #${layerName}`).closest('div').remove();
        if ($('.layer-list :checkbox').length == 0)
            $('.layer-list').html('');
        //loading.show('Loading Parcels...');   //24803
        //searchSavedResults.result = {};
        //initSearch.doSearch();
        //return false;
    },
    addLegend: function () {
        var legend = L.control({ position: 'bottomright' });
        legend.onAdd = function (map) {
            var div = L.DomUtil.create('div', 'info legend legend-atc-div');
            if (EnableNewPriorities == 'True') {
                div.innerHTML +=
                    '<div class="legend-item"><span class="marker_legend_priority marker__priority-critical"/></span> <span class="legend-atc-text"> - Critical Priority</span></div>' +
                    '<div class="legend-item"><span class="marker_legend_priority marker__priority-urgent"/></span> <span class="legend-atc-text"> - Urgent Priority</span></div>' +
                    '<div class="legend-item"><span class="marker_legend_priority marker__priority-high"/></span> <span class="legend-atc-text"> - High Priority</span></div>' +
                    '<div class="legend-item"><span class="marker_legend_priority marker__priority-medium"/></span> <span class="legend-atc-text"> - Medium Priority</span></div>' +
                    '<div class="legend-item"><span class="marker_legend_priority marker__priority-normal"/></span> <span class="legend-atc-text"> - Normal Priority</span></div>' +
                    '<div class="legend-item"><span class="marker_legend_priority marker__priority-proximity"/></span> <span class="legend-atc-text"> - Proximity Priority</span></div>' ;
            }
            else {
                div.innerHTML += 
                    '<div class="legend-item"><span class="marker_legend_priority marker__priority-old-urgent"/></span> <span class="legend-atc-text"> - Urgent Priority</span></div>' +
                    '<div class="legend-item"><span class="marker_legend_priority marker__priority-old-high"/></span> <span class="legend-atc-text"> - High Priority</span></div>' +
                    '<div class="legend-item"><span class="marker_legend_priority marker__priority-old-normal"/></span> <span class="legend-atc-text"> - Normal Priority</span></div>';
            }

            div.innerHTML += '<div class="legend-item"><span class="marker_legend_priority"/><span class="marker__info">A</span></span> <span class="legend-atc-text"> - Approved Parcel</span></div>' +
                '<div class="legend-item"><span class="marker_legend_priority"/> <span class="marker__info">R</span></span> <span class="legend-atc-text"> - Reviewed Parcel</span></div>';

            return div;
        };

        legend.addTo(map);

    },
    plotMarkers: function (parcels, hideResults, callback) {
        atcMap.reset();
        if (parcels.length > 0) $('.legend-atc-div').show();
        else $('.legend-atc-div').hide();
        atcMapLayers.tempMarkerLayer = L.markerClusterGroup({
            maxClusterRadius: 80,
            disableClusteringAtZoom: 15
        });
        if (map.hasLayer(atcMapLayers.groupMarkerLayer))
            map.removeLayer(atcMapLayers.groupMarkerLayer);
        var arrayOfMarkers = this.arrayOfMarkers;
        arrayOfMarkers.splice(0, arrayOfMarkers.length);
        var bounds = L.latLngBounds();
        let noGISAlert = '';
        let noGISParcels = 0;
        $(parcels).each(function (index) {
            if ($(this)[0].latitude != null && $(this)[0].longitude != null) {
                let lat_lng = [$(this)[0].latitude, $(this)[0].longitude];
                let markerStyle = 'marker__priority';
                let priorityColor;
                let parcelStatus = '';
                let parcelStatusStyle = 'marker__info';
                if ($(this)[0].Reviewed == true && $(this)[0].QC == true) {
                    parcelStatus = 'A';
                    parcelStatusStyle += ' marker__info-qc';
                } else if ($(this)[0].Reviewed == true) {
                    parcelStatus = 'R';
                    parcelStatusStyle += ' marker__info-ma'
                }
                if (EnableNewPriorities == 'True') {
                    switch ($(this)[0].Priority) {
                        case 5:
                        case 'Critical':
                            markerStyle += ' marker__priority-critical';
                            priorityColor = 'red';
                            break;
                        case 4:
                        case 'Urgent':
                            markerStyle += ' marker__priority-urgent';
                            priorityColor = 'orange';
                            break;
                        case 3:
                        case 'High':
                            markerStyle += ' marker__priority-high';
                            priorityColor = 'yellow';
                            break;
                        case 2:
                        case 'Medium':
                            markerStyle += ' marker__priority-medium';
                            priorityColor = 'blue';
                            break;
                        case 1:
                        case 'Normal':
                            markerStyle += ' marker__priority-normal';
                            priorityColor = 'green';
                            break;
                        default:
                            markerStyle += ' marker__priority-proximity';
                            priorityColor = 'gray';

                    }
                }
                else {
                    switch ($(this)[0].Priority) {
                        case 2:
                            markerStyle += ' marker__priority-old-urgent';
                            priorityColor = 'red';
                            break;
                        case 1:
                            markerStyle += ' marker__priority-old-high';
                            priorityColor = 'orange';
                            break;
                        case 0:
                            markerStyle += ' marker__priority-old-normal';
                            priorityColor = 'green';
                            break;
                        default:
                            markerStyle += ' marker__priority-old-normal';
                            priorityColor = 'green';

                    }
                }
                const icon = L.divIcon({
                    className: '',
                    iconAnchor: [0, 24],
                    labelAnchor: [-6, 0],
                    popupAnchor: [0, -36],
                    html: `<span class="${markerStyle}"/> <span class="${parcelStatusStyle}"> ${parcelStatus} </span>`
                });
                var gMarker = L.marker(lat_lng);
                gMarker.setIcon(icon);
                gMarker.options.parcelInfo = $(this)[0];
                gMarker.options.priority = $(this)[0].Priority;
                gMarker.options.priorityColor = priorityColor;
                gMarker.options.parcelStatus = parcelStatus;
                gMarker.options.parcelStatusStyle = parcelStatusStyle;
                arrayOfMarkers.push(gMarker);
                let markerId = L.stamp(gMarker);
                var Pri = ['Urgent', 'High', 'Normal'];
                let maClass = 'alert-danger', maText = 'Not Reviewed', qcClass = 'alert-danger', qcText = 'Not Approved';
                if ($(this)[0].Reviewed == true) {
                    maClass = 'alert-success';
                    maText = 'Reviewed';
                    if ($(this)[0].QC == true) {
                        qcClass = 'alert-success';
                        qcText = 'Approved';
                    }
                }
                let grp = ($(this)[0].NbhdName ? $(this)[0].NbhdName : ''), gUser = $(this)[0].AssignedTo ? $(this)[0].AssignedTo : '',
                    gu = gUser ? userList.filter((x) => { return (x['LoginId'] == gUser) })[0] : '',
                    grpUser = (gUser && gUser != '' && gu && gu.UserName) ? gu.UserName : gUser;
                var parcelPopup = `
                    <div class="marker__popup">
                        <h4 class="marker__popup-title text-ellipsis">${$(this)[0].keyFieldValue}</h4>
                        <div class="marker__popup-address">
                            <i>${$(this)[0].StreetAddress}</i>
                        </div>
                        <div class="marker__popup-alerts flex justify-between">
                            <span class="alert ${maClass}">${maText}</span>
                            <span class="alert ${qcClass}">${qcText}</span>
                        </div>
	                    <div class="marker__popup-group">
	                        <span class="marker__popup-group-assign text-ellipsis">Assgn Group : <b>${grp}</b></span>
	                        <span class="text-ellipsis">Assigned User : <b>${grpUser}</b></span>
	                    </div>
	                    <div class="marker__reassign-div" style="display: none;">
	                        <div class="flex-row-center justify-between">
	                            <label> Group </label>
	                            <input type="text" class="marker__reassign-input" list="marker__reassign-datalist" onchange="$('.marker__reassign-invalid').hide()">
	 							<datalist id="marker__reassign-datalist" class="marker__reassign-datalist"></datalist> 
	                        </div>
	 						<span class="marker__reassign-invalid" style="display: none;">Please choose a valid group name</span>
	                        <div class="flex-row-center marker__reassign-buttons">
	                            <button class="btn btn--small marker__assign-cancel" onclick="atcMapLayers.assgnCancel(); return false;">Cancel</button>
	                            <button class="btn btn--small marker__assign-save" onclick="atcMapLayers.assgnConfirm(this); return false;" pid="${$(this)[0].Id}">Confirm</button>
	                        </div>
	                    </div>
                        <div class="flex-row-center justify-between">
	                        <button class="btn btn--small marker__popup-assign" data-markerid="${markerId}" onclick="atcMapLayers.bindAssgnGroups(this); return false;">Re-Assign</button>
                    	    <button class="btn btn--small marker__qc" pid="${$(this)[0].Id}" onclick="openInQc(this); return false;">Open in QC</button>
                        </div>
                    </div>`
                var popupOptions = { 'maxWidth': '500', 'autoPan': true, 'keepInView': true };
                gMarker.bindPopup(parcelPopup, popupOptions).addTo(atcMapLayers.tempMarkerLayer);
                bounds.extend(lat_lng);
            }
            else noGISParcels++;
        });
        if (bounds.isValid()) {
            map.fitBounds(bounds);
            atcMapLayers.tempMarkerLayer.addTo(map);
            $('.add-overlay').show();
            (noGISParcels != 0 && !hideResults) ? loading.show(noGISParcels + 'Parcels with no GIS Points') : '';
        } else if (!hideResults)
            noGISAlert = 'No Parcels with GIS Points found';
        if (!hideResults) {
            if (noGISAlert != '')
                SnackBar(noGISAlert, { position: 'bottom-right', autoHide: true, autoHideAfter: 3000 });//say(noGISAlert);
            else if (noGISParcels != 0) {
                SnackBar(`${noGISParcels} Parcels found with no GIS Points`, { position: 'bottom-right', autoHide: true, autoHideAfter: 3000 });
            }
        }

        if (callback && typeof callback === 'function') callback();
    },
    bindAssgnGroups: function (el) {
        $(el).addClass('btn--loading');
        let mId = $(el).data("markerid");
        setTimeout(() => {
            $('.marker__reassign-datalist').html('');
            dashboardNbhdtasks.forEach((n) => {
                if (n.Nbhd && n.Id)
                    $('.marker__reassign-datalist').append($('<option>').val(n.Nbhd).attr('data-value', n.Id));
            });
            $('.marker__popup-assign').hide();
            $('.marker__reassign-invalid').hide();
            $('.marker__reassign-div').show();

            let popup = atcMapLayers.arrayOfMarkers.filter((x) => { return x._leaflet_id == mId })[0];
            if (popup) {
                let oy = 0, popel = popup.getElement(), ph = popel.offsetHeight,
                    mc = map.getContainer(), mh = mc.offsetHeight;
                oy = (ph + 5) - (mh / 2);
                if ($('#map').offset().top > $('.leaflet-popup').offset().top) map.panBy([0, 40]);
            }
        }, 200);
    },
    assgnCancel: function () {
        $('.marker__popup-assign').removeClass('btn--loading').show();
        $('.marker__reassign-div').hide();
        document.querySelector('.marker__reassign-input').value = "";
    },
    assgnConfirm: function (el) {
        let v = document.querySelector('.marker__reassign-input').value;
        let o = $('.marker__reassign-datalist option[value="' + v + '"]')[0];
        if (!v || v == '' || !o || !o.dataset || !o.dataset.value) {
            $('.marker__reassign-invalid').show();
            return false;
        }

        let sb = SnackBar('Reassigning Parcel...', { position: 'bottom-right', autoHide: false });
        let oId = o.dataset.value, pId = $(el).attr('pid');
        PageMethods.AssignParcelToGroup(pId, oId, currentLoginId, v, (res) => {
            if (res == 'Success') {
                $('.marker__popup-group-assign').html('Assgn Group : <b>' + v + '</b>');
                atcMapLayers.assgnCancel();
                sb.hide();
                SnackBar('Parcel moved to group ' + v, { position: 'bottom-right', autoHide: true, autoHideAfter: 3000 });
            }
            else {
                SnackBar('Failed!', { position: 'bottom-right', autoHide: true, autoHideAfter: 3000 });
            }
        });
    },
}

const lasso = {
    lassoList: [],
    resetSelectedState: function () {
        lasso.lassoList.forEach(parcel => {
            parcel.options.icon.options.className = "";
        });
        if (map.hasLayer(atcMapLayers.tempMarkerLayer)) {
            atcMapLayers.tempMarkerLayer.remove();
            atcMapLayers.tempMarkerLayer.addTo(map);
            
        }
        if (map.hasLayer(atcMapLayers.groupMarkerLayer)) {
            atcMapLayers.groupMarkerLayer.remove();
            atcMapLayers.groupMarkerLayer.addTo(map);
            pageActions.setTitle(false);
        }
        lasso.lassoList = [];
        $('#lassoCount').hide();
        $('#lassoCount').text('');
        pageActions.mapButton(0, -1, -1, -1, 1, 0, 0);
        //initSearch.searchList = [];
        adhocCreate.priorityListItems = [];
    },
    //SelectedexportData: function () {
    //    let filename = 'Selected_ParcelList_' + new Date().toLocaleString() + '.xlsx';
    //    var excludedFields2 = ['latitude', 'longitude', 'Id', 'NbhdName', 'AssignedTo', 'keyFieldValue', 'MESSAGE'];
    //    var filteredList = lasso.lassoList.map((lassoItem) => {
    //        var filteredInfo = {};
    //        for (var key in lassoItem.options.parcelInfo) {
    //            if (!excludedFields2.includes(key)) {
    //                filteredInfo[key] = lassoItem.options.parcelInfo[key];
    //            }
    //        }
    //        return filteredInfo;
    //    });
    //    var ws = XLSX.utils.json_to_sheet(filteredList);
    //    var wb = XLSX.utils.book_new();
    //    XLSX.utils.book_append_sheet(wb, ws, "Selected Parcel List");
    //    XLSX.writeFile(wb, filename);
    //},
    SelectedexportData: function () {
        let filename = 'Selected_ParcelList_' + new Date().toLocaleString() + '.xlsx';
        var excludedFields2 = ['latitude', 'longitude', 'Id', 'NbhdName', 'AssignedTo', 'keyFieldValue', 'MESSAGE'];
        var filteredList = lasso.lassoList.map((lassoItem) => {
            var filteredInfo = {};
            for (var key in lassoItem.options.parcelInfo) {
                if (!excludedFields2.includes(key)) {
                    // Map number values to corresponding labels
                    if (key === 'Priority') {
                        var priorityValue = lassoItem.options.parcelInfo[key];
                        var priorityLabel = '';
                        if (priorityValue === 'Normal' || priorityValue === 'High' || priorityValue === 'Urgent' || priorityValue === 'Critical' || priorityValue === 'Medium' || priorityValue === 'Proximity') {
                            priorityLabel = priorityValue; // If already set to 'Normal', 'High', or 'Urgent', keep the value as it is
                        } else {
                            if (EnableNewPriorities == 'True') {
                                switch (priorityValue) {
                                    case 0:
                                        priorityLabel = 'Proximity';
                                        break;
                                    case 1:
                                        priorityLabel = 'Normal';
                                        break;
                                    case 2:
                                        priorityLabel = 'Medium';
                                        break;
                                    case 3:
                                        priorityLabel = 'High';
                                        break;
                                    case 4:
                                        priorityLabel = 'Urgent';
                                        break;
                                    case 5:
                                        priorityLabel = 'Critical';
                                        break;
                                    default:
                                        priorityLabel = 'Unknown';
                                }
                            }
                            else {
                                switch (priorityValue) {
                                    case 0:
                                        priorityLabel = 'Normal';
                                        break;
                                    case 1:
                                        priorityLabel = 'High';
                                        break;
                                    case 2:
                                        priorityLabel = 'Urgent';
                                        break;
                                    default:
                                        priorityLabel = 'Unknown';
                                }
                            }
                        }
                        filteredInfo[key] = priorityLabel;
                    } else {
                        filteredInfo[key] = lassoItem.options.parcelInfo[key];
                    }
                }
            }
            return filteredInfo;
        });
        var ws = XLSX.utils.json_to_sheet(filteredList);
        var wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, "Selected Parcel List");
        XLSX.writeFile(wb, filename);
    },
    setSelectedLayers: function (layers) {
        layers.forEach(layer => {
            if (layer.getChildCount) {
                layer.getAllChildMarkers().forEach(marker => {
                    lasso.lassoList.push(marker);
                });
            } else if (layer instanceof L.Marker) {
                lasso.lassoList.push(layer);
            }
        });
        lasso.lassoList = lasso.remove_Duplicate(lasso.lassoList);
        //console.log(layers.length ? `Selected ${layers.length} Parcels` : '');
        lasso.lassoList.forEach(parcel => {
            parcel.options.icon.options.className = "marker__lasso-selected";
        });
        if (map.hasLayer(atcMapLayers.tempMarkerLayer)) {
            atcMapLayers.tempMarkerLayer.remove();
            atcMapLayers.tempMarkerLayer.addTo(map);
        }
        if (map.hasLayer(atcMapLayers.groupMarkerLayer)) {
            atcMapLayers.groupMarkerLayer.remove();
            atcMapLayers.groupMarkerLayer.addTo(map);
        }
    },
    lassoListeners: function () {
        map.on('mousedown', () => {
        });
        map.on('lasso.finished', event => {
            var hasTemp = map.hasLayer(atcMapLayers.tempMarkerLayer);
            lasso.setSelectedLayers(event.layers);
            adhocCreate.priorityListItems = [];
            loading.show()
            lasso.lassoList.forEach((p) => {
                //initSearch.searchList.push(p.options.parcelInfo);
                adhocCreate.priorityListItems.push(p.options.parcelInfo.Id);
            });
            if (lasso.lassoList.length == 0) {
                pageActions.mapButton(0, -1, -1, -1, 1, 0, 0);
                loading.hide();
            	$('#lassoCount').hide();
            	$('#lassoCount').text('');
                
            } else {
                $('#lassoCount').text(lasso.lassoList.length?.toLocaleString('en-US'));
            	$('#lassoCount').show();
                pageActions.mapButton(1, -1, -1, -1, 1, 1, 1);
                loading.hide();
            }
            return true;
        });
        map.on('lasso.enabled', () => {
        });
        map.on('lasso.disabled', () => {
        });

    },
    remove_Duplicate: function (list) {
        var newArray = [];
        $.each(list, function (key, value) {
            var exists = false;
            $.each(newArray, function (k, val2) {
                if (value._leaflet_id == val2._leaflet_id) { exists = true };
            });
            if (exists == false && value._leaflet_id != "") { newArray.push(value); }
        });
        return newArray;
    }

}

var atcInitApplication = function () {
    loading.show('Initializing');
    pageActions.loading(true, 'dashboard');
    pageActions.loading(true, 'search');
    pageActions.loading(true, 'group');
    pageActions.mapButton();

    pageTour.init();

    atcMap.init();
    atcMapLayers.init();

    //When Coming back from Adhoc creation page added query string to open search tab
    const urlSearchParams = new URLSearchParams(window.location.search);
    const params = Object.fromEntries(urlSearchParams.entries());
    if (params && params['Name'] == 'Search') pageActions.openPage('search');
    else pageActions.openPage('dashboard');

    dashboard.init(function () {
        PageMethods.GetUserDetails((res) => {
            if (res.Success && res.Data.UserList)
                userList = res.Data.UserList;

            loading.hide();
            pageActions.loading(false, 'dashboard');
            if ($('.dashboard__empty').is(":visible"))
                $("#dash__cardId").hide();
            let sb = SnackBar('Loading Lookup Tables', { position: 'bottom-right', autoHide: false, loading: true });
            initDb.init(function () {
                initDb.cachelookupTable(function () {
                    initDb.parcelDataLookup(function () {
                        initSearch.init(function () {
                            sb.hide();
                            initSearch.lookupLoaded = true;
                            $('#dashPopFilterRecent .filter__card-button .btn').removeAttr('disabled'); // Making dashboard filter card search button active once the lookups are loaded
                            pageActions.loading(false, 'search');
                        });
                    });
                });
            });
            assignmentGroupStatus.loadAssignUser(function () {
                pageActions.loading(true, 'group');
                assignmentGroupStatus.loadNbhds(function () {
                    pageActions.loading(true, 'group');
                    if ($('.sb__page[sb-id="group"]').hasClass('sb__page--active')) assignmentGroupStatus.init();
                    adhocCreate.loadAdhocUser(function () {
                    });
                });
            });
        });
    });
}

function Filter(label, field, op, value1, value2, value3, textValue2) {
    this.ID = searchIndex++;
    this.Label = label || "";
    this.Field = field || "";
    this.Operator = op || "";
    this.Value1 = value1 || "";
    this.Value2 = value2 || "";
    this.Value3 = value3 || "";
    this.TextValue2 = textValue2 || "";
    this.DataType = "";
    this.DisplayText = "";

    this.form = function () {
        let field = this.Field.split('/');
        let label = this.Label;
        let type = field[1] ? parseInt(field[1]) : 1;
        let labelSuffix = "", value = '';

        switch (this.Operator) {
            case "bw":
                labelSuffix = " between";
                break;
            case "gt":
                labelSuffix = " greater than";
                break;
            case "lt":
                labelSuffix = " lesser than";
                break;
            case "cn":
                labelSuffix = " contains";
                break;
            case "sw":
                labelSuffix = " starting with";
                break;
            case "nsw":
                labelSuffix = " not starting with";
                break;
            case "ew":
                labelSuffix = " ending with";
                break;
            case "ge":
                labelSuffix = " greater than or equals";
                break;
            case "le":
                labelSuffix = " lesser than or equals";
                break;
            case "nl":
                labelSuffix = " is null";
                break;
            case "nn":
                labelSuffix = " is not null";
                break;
            case "eq":
                labelSuffix = " equal to";
                break;
            case "ne":
                labelSuffix = " not equal to";
                break;
        }

        if (type == 4) {
            labelSuffix = suffix(this.Operator) ? suffix(this.Operator) : labelSuffix;
            label = label.replace(' On', '');
        }

        if (this.Operator != "nl" && this.Operator != "nn") {
            value += this.Value1;
            if (((type == 5 || field[0] == '1' || field[0] == '5') && this.Operator == 'eq') || field[0] == '4')
                value = this.TextValue2;
            else if (type == 4 && this.Operator == "bw")
                value += ' and ' + this.Value3;
        }

        let search__template = $('#searchFilterTemplate').clone();
        $(search__template).removeClass('search__filter--template');
        let search__obj = { Id: this.ID, label: label, operator: labelSuffix, value: value };
        $(search__template).bindData(search__obj);
        this.DisplayText = label + ' ' + labelSuffix + ' ' + value;

        return search__template;
    }
}

function SearchTemplate() {
    var filters = [];

    this.addFilter = function (label, field, op, value1, value2, value3, filter__Id, textValue2) {
        if (filter__Id) {
            let selectedFilter = filters.filter((x) => { return (x.ID == filter__Id) })[0];
            selectedFilter.ID = filter__Id;
            selectedFilter.Label = label || "";
            selectedFilter.Field = field || "";
            selectedFilter.Operator = op || "";
            selectedFilter.Value1 = value1 || "";
            selectedFilter.Value2 = value2 || "";
            selectedFilter.Value3 = value3 || "";
            selectedFilter.TextValue2 = textValue2 || "";
            selectedFilter.DataType = "";
            selectedFilter.DisplayText = "";
        }
        else {
            let f = new Filter(label, field, op, value1, value2, value3, textValue2);
            if (f) filters.push(f);
        }
    }

    this.__defineGetter__("Filters", function () { return filters; });

    this.clear = function () {
        filters = [];
    }

    this.remove = function (val) {
        let dx = -1;
        for (x in filters) {
            var fx = filters[x];
            if (fx.ID == val) {
                dx = x;
            }
        }
        filters.splice(dx, 1);
    }

    this.render = function (container) {
        $(container).html('');
        $.each(filters, function (i, filter) {
            $(container).append(filter.form());
        });
    }
}

var suffix = function (opr) {
    let labelSuffix = '';
    switch (opr) {
        case "bw":
            labelSuffix = " between";
            break;
        case "gt":
            labelSuffix = " after";
            break;
        case "ge":
            labelSuffix = " on or after";
            break;
        case "lt":
            labelSuffix = " before";
            break;
        case "le":
            labelSuffix = " on or before";
            break;
    }
    return labelSuffix;
}

var currentSearchTemplate = new SearchTemplate();

var randomNumber = function () {
    let rn = Number(Math.floor(Math.random() * (100000 - 0 + 1)) + 0);
    return rn;
}

var getData = function (query, params, callback, errorCallback, otherData) {
    if (params == null) params = [];
    var data = [];
    if (query) {
        _localDb.transaction(function (x) {
            x.executeSql(query, params, function (x, results) {
                var len = results.rows.length, i;
                for (i = 0; i < len; i++) {
                    data.push(results.rows.item(i));
                }
                if (callback) callback(data, results, otherData);
            }, function (x, e) {
                if (errorCallback) errorCallback();
                console.warn(e.message);
                console.warn(query);

            });
        });
    }
}

var _syncSubTables = function (tableName, data, callback) {
    if (data.length == 0) {
        if (callback) callback();
        return;
    }
    var filtered = {};
    for (x in data) {
        var item = data[x];
        var dsName = item.DataSourceName;
        if (filtered[dsName] == null) {
            filtered[dsName] = [];
        }
        filtered[dsName].push(item);
    }
    _syncSubTableFromList(filtered, callback);
}

var _syncSubTableFromList = function (filtered, callback) {
    if (Object.keys(filtered).length == 0) {
        if (callback) callback();
        return;
    }
    for (var key in filtered) {
        _syncTable(key, filtered[key], function () {
            delete filtered[key];
            _syncSubTableFromList(filtered, callback);
        });
        break;
    }
}

var _syncTable = function (tableName, data, callback) {
    if (data.length == 0) {
        syncProgressList.push(tableName);
        if (callback) callback();
        return;
    }
    if (syncProgressList.indexOf(tableName) > -1) {
        syncProgressList.push(tableName);
        _syncData(tableName, data, columnDir[tableName], callback);
        return;
    }

    if (data.length > 0) {
        _localDb.transaction(function (x) {
            var dropAction = "DROP TABLE IF EXISTS " + tableName;
            var dropConfirm = tableName + " dropped.";
            executeSql(x, dropAction, [], function (x) {
                if (data.length > 0) {
                    var firstRow = data[0];
                    _getColumnInformation(firstRow, function (cInfo) {
                        columnDir[tableName] = cInfo;
                        syncProgressList.push(tableName);
                        _syncData(tableName, data, cInfo, callback);
                    }, tableName);
                }
            });
        });

    }
}

var _syncData = function (tableName, data, cinfo, callback) {
    var createSql = "CREATE TABLE IF NOT EXISTS " + tableName + " (" + cinfo.CreateColumns + ");";
    _localDb.transaction(function (x) {
        executeSql(x, createSql, [], function (tx) {
            var firstRow = data[0];
            var progress = 0;
            for (i in data) {
                var parcel = data[i];
                var total = data.length;
                var pa = _parseDataForInsert(parcel, firstRow, cinfo);
                var columns = "";
                var phs = "";
                for (key in parcel) {
                    if (cinfo.ColumnKeys.indexOf(key) > -1) {
                        if (key == "Index") key = "SortIndex";
                        if (columns == "") { columns = "[" + key + "]"; phs = "?"; }
                        else { columns += ", " + "[" + key + "]"; phs += ", ?"; }
                    }
                }
                var insertSql = "INSERT INTO " + tableName + " (" + columns + ") VALUES (" + phs + ")";
                executeSql(tx, insertSql, pa, function (tx, res) {
                    progress += 1;
                    if (progress == total) {
                        if (callback) callback();
                    }
                }, function (e, m) {
                    log(m.message, true);
                });
            }
        });
    });
}

var _getColumnInformation = function (firstRow, callBack, tableName) {
    var cKeys = [];
    var columns = "";
    var createColumns = "";
    var phs = "";
    for (key in firstRow) {
        cKeys.push(key);
        var type = "TEXT";
        if (key == "Id" || /^.*?Id$/.test(key) || /^.*?_id$/.test(key) || /^.*?UID$/.test(key)) {
            type = "INTEGER"
        }
        if (key == "Synced") {
            type = "INTEGER"
        }
        if (firstRow[key] != null) {
            if ((firstRow[key].toString() == 'true') || (firstRow[key].toString() == 'false')) {
                type = "BOOL";
            }
        }

        if (columns == "") {
            columns = key;
            createColumns = "[" + key + "]" + " " + type;
            phs = "?";
        }
        else {
            columns += ", " + key;
            createColumns += ", " + "[" + key + "]" + " " + type;
            phs += ", ?"
        }
    }

    var r = new Object();
    r.Columns = columns;
    r.CreateColumns = createColumns;
    r.PlaceHolders = phs;
    r.ColumnKeys = cKeys;

    if (callBack) callBack(r);
}

var executeSql = function (tx, query, params, success, failure) {
    tx.executeSql(query, params, function (x, results) { if (success) success(x, results) }, function (x, e) { if (failure) failure(x, e); else console.error(query, e.message) });
}

var _parseDataForInsert = function (parcel, firstRow, cinfo) {
    var pa = [];
    for (key in parcel) {
        if (cinfo.ColumnKeys.indexOf(key) > -1) {
            var dat;
            dat = parcel[key];
            if (TrueFalseInPCI == "False") {
                if (dat == "True" || dat == "true") dat = true;
                if (dat == "False" || dat == 'false') dat = false;
            }
            if (key == "Index") {
                dat = parseInt(dat);
            }
            dat = dat === null ? dat : dat.toString();
            pa.push(dat);
        }
    }
    return pa;
}

var getLookupData = function (field, options, callback) {
    var data = [];
    var cat = options.category;
    if (cat == null)
        cat = field.Category;
    if (field.IsLargeLookup == 'true' || field.IsLargeLookup == true) {
        if (callback) callback([]);
        return;
    }
    if (field.SourceTable == "_photo_meta_data") {
        if (callback) callback([]);
        return;
    }
    var specialType = false;
    if (options.dataSource == 'ccma.YesNo') {
        if (callback) callback(ccma.YesNo);
    } else {
        if (field.LookupTable == '$QUERY$') {
            var query = field.LookupQuery || '';
            if (lookup['#FIELD-' + field.Id] == undefined) {
                var fullQuery = query || '';
                if (query.search(/\{[a-zA-Z0-9_.]*\}/i) > 0) {
                    if (query.search(/where/i) > 0) {
                        if (query.search(/union/i) > 0) {
                            var unionSplit = query.split(/union/i)
                            unionSplit.forEach(function (each, index) { if (unionSplit[index].search(/where/i) > 0) unionSplit[index] = unionSplit[index].substr(0, unionSplit[index].search(/where/i)) });
                            fullQuery = unionSplit.join('union');
                        }
                        else
                            fullQuery = query.substr(0, query.search(/where/i));
                    }
                }
                if (fullQuery && fullQuery.search(/\{.*?\}/g) == -1 || (field.LookupQueryForCache && field.LookupQueryForCache != '')) {
                    if (field.LookupQueryFilter) extendedLookup['#FIELD-' + field.Id] = []
                    if (field.LookupQueryForCache && field.LookupQueryForCache != '') fullQuery = field.LookupQueryForCache;
                    getData(fullQuery, [], function (ld) {
                        if (ld.length > 0) {
                            var f = field.Name;
                            lookup[f] = {};
                            var props = Object.keys(ld[0]);
                            var IdField = props[0]; var NameField = props[1]; var DescField = props[2]
                            if (!NameField) NameField = IdField;
                            if (!DescField) DescField = NameField;
                            for (var x in ld) {
                                var d = ld[x];
                                var v = d[IdField];
                                if (v) v = v.toString().trim();
                                if (lookup[f][v] === undefined)
                                    lookup[f][v] = { Name: d[NameField], Description: d[DescField], Ordinal: x, SortType: null, Id: v, Object: d };
                                if (field.LookupQueryFilter) {
                                    d.Ordinal = x;
                                    d.Name = d[NameField];
                                    d.Description = d[DescField];
                                    d.Id = v;
                                    extendedLookup['#FIELD-' + field.Id].push(d)
                                }
                            }
                            lookup['#FIELD-' + field.Id] = lookup[f];
                        }
                    });
                } else specialType = true;

            }
            if (query) {
                if (query.search(/\{.*?\}/g) == -1) {
                    getData(query, [], function (ld) {
                        if (ld.length > 0) {
                            var f = field.Name;
                            var props = Object.keys(ld[0]);
                            var IdField = props[0]; var NameField = props[1]; var DescField = props[2]
                            if (!NameField) NameField = IdField;
                            for (var x in ld) {
                                if (specialType) lookup[f] = {};
                                var d = ld[x];
                                var v = d[IdField];
                                if (v === 'null') { v = ''; d[IdField] = ''; }
                                var name = d[NameField];
                                if (name) name = name.toString().trim();
                                if (!name) name = v;
                                data.push({ Name: name, Description: d[DescField], Ordinal: x, SortType: null, Id: v });
                                if (specialType) {
                                    if (lookup[f][v] === undefined)
                                        lookup[f][v] = { Name: d[NameField], Description: d[DescField], Ordinal: x, SortType: null, Id: v, Object: d };
                                }
                            }
                        }
                        if (options.addNoValue) {
                            data.unshift({ Name: '-- No Value --', Id: '' });
                        } else if (isTrue(field.LookupShowNullValue)) {
                            data.unshift({ Id: '', Name: '-- No Value --' });
                        } else {
                            data.unshift({ Name: '', Id: '' });
                        }
                        if (callback) callback(data, options.source, options);
                    }, function () {
                        if (callback) callback([]);
                    });
                } else {
                    if (callback) callback([]);
                }
            }
            else {
                if (callback) callback([]);
            }
        } else {
            var l = lookup[field.LookupTable]
            for (x in l) {
                var Idfield = x;
                data.push({
                    Id: x,
                    Name: l[x].Name,
                    Description: l[x].Description,
                    Ordinal: l[x].Ordinal,
                    SortType: (l[x].SortType && !isNaN(l[x].SortType)) ? parseInt(l[x].SortType) : null
                })
            }
            if (options.addNoValue) {
                data.unshift({ Name: 'NO VALUE', Id: '' });
            }
            if (isTrue(field.LookupShowNullValue)) {
                data.unshift({ Name: '', Value: '<blank>' });
            }
            if (callback) callback(data.sort(function (x, y) { return x.Ordinal ? x.Ordinal - y.Ordinal : x.Id > y.Id ? 1 : -1 }), options.source, options);
        }
    }
}

var isTrue = function (v) {
    return v == "true" || v == true;
}

var priority = {
    init: function () {
        $('.mfab .mfab__image').hide();
        atcMap.reset();
        pageTour.startTour();
    },
    clearFile: function () {
        document.getElementById("priListFile").value = null;
    },
    verifyPriorityList: function () {
        var fileUpload = $('#priListFile');
        var filePath = ''
        var filevalid;

        if ($('#priListFile').val() != '') {
            filePath = $('#priListFile').val();
        }
        if (filePath == '') {
            SnackBar('You need to choose a .csv file to continue', { position: 'bottom-right', autoHide: true, autoHideAfter: 2000 });
            return false;
        }
        if (filePath.split('.').pop().toLowerCase() != 'csv') {
            SnackBar("You have uploaded an invalid file. Only '.csv' formats are allowed.", { position: 'bottom-right', autoHide: true, autoHideAfter: 2000 });
            return false;
        }
        var fileSize = ''
        if (fileUpload[0].files.length > 0) {
            fileSize = fileUpload[0].files[0].size;
        }

        if (fileUpload[0].files.length == 0 && $('#priListFile').val() != '')
            fileSize = $('#priListFile')[0].files[0].size

        if (filePath && filePath != '' && fileSize && fileSize != '' && parseInt(fileSize) > 4194304) {
            SnackBar("File too big ! The maximum size of uploaded file should be 4MB.", { position: 'bottom-right', autoHide: true, autoHideAfter: 2000 });
            return false;
        }
        fileValid = 'valid';
        this.ExportCSV();
        return true;
    },
    priParcels: [],
    parcelList: '',
    activeParcels: [],
    Id: null,
    ExportCSV: function () {
        if (typeof (FileReader) != "undefined") {
            var reader = new FileReader();
            reader.onload = function (e) {
                loading.show('Procesing file...');
                priority.activeParcels = [];
                priority.parcelList = '';
                let csvrows = e.target.result.split("\n");
                let text = e.target.result.replace(/\r/g, "\n").replaceAll('<', '').replaceAll('>', '');
                let re = new RegExp("\n(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)"), row = text.split(re),
                    keyarray = keyList.concat(['PRIORITY', 'MESSAGE']),
                    keys = keyarray.length, emptyHeader = false, missingColumns = [],
                    headerCell = row[0].split(new RegExp(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)"));

                headerCell = headerCell.map(function (x) { return x.trim().toUpperCase() });
                headerCell.forEach((value) => {
                    if (value == "")
                        emptyHeader = true;
                });

                if (headerCell.includes('GROUPNAME')) {
                    say("Warning: Given priorities will not be added to the specified groups. You can add them later by using 'Select Parcel' tool. \n\nPlease revise your list and upload again.");
                    return false;
                }

                if (headerCell.length != keys) {
                    say("You have uploaded an invalid list.\n\n Please revise your list and upload again.");
                    return false;
                }

                for (let i = 0; i < keys; i++) {
                    if (!headerCell.includes(keyarray[i].trim().toUpperCase()))
                        missingColumns.push(keyarray[i]);
                }

                if (missingColumns.length > 1) {
                    say("Your list could not be uploaded because it does not contain '" + missingColumns.toString().trim() + "' as columns.\n\n Please revise your list and upload again.");
                    return false;
                }
                else if (missingColumns.length == 1) {
                    say("Your list could not be uploaded because it does not contain '" + missingColumns.toString().trim() + "' as a column.\n\n Please revise your list and upload again.");
                    return false;
                }

                if (emptyHeader) {
                    say("You have uploaded an invalid list.\n\n Please revise your list and upload again.");
                    return false;
                }

                for (let i = 0; i < keys; i++) {
                    if (keyarray[i].trim().toUpperCase() != headerCell[i].toUpperCase()) {
                        say("Your list could not be uploaded because its fields are not in a correct order.\n\n Please revise your list and upload again.");
                        return false;
                    }
                }

                for (var i = 1; i < csvrows.length; i++) {
                    if (csvrows[i] != "") {
                        var csvcols = csvrows[i].split(",");
                        if (csvcols.length < keys) {
                            say('You have uploaded an invalid list.\n\n Please revise your list and upload again.');
                            return false;
                        }
                        priority.parcelList += "'" + csvcols[0] + "',";
                        var obj = { parcelId: csvcols[0], priority: csvcols[1], message: csvcols[2] }
                        priority.priParcels.push(obj);
                    }
                }
                priority.parcelList = priority.parcelList.replace(/,\s*$/, "");
                priority.Id = (new Date().getTime());
                $(".masklayer .mask-new .mask-msg").text('Uploading...');
                PageMethods.GetParcelPointsFromParcelList(priority.parcelList, text, priority.Id, (parcelPoints) => {
                    if (parcelPoints.Success) {
                        priority.activeParcels = parcelPoints;
                        let parcels = parcelPoints.Data.parcelPoints;
                        initSearch.searchList = parcelPoints.Data.parcelPoints;
                        adhocCreate.priorityListItems = [];
                        initSearch.searchList.forEach((p) => {
                            adhocCreate.priorityListItems.push(p.Id);
                        });
                        atcMapLayers.plotMarkers(parcels, null, () => {
                            pageActions.mapButton(1, 0, 0, 1, 1);
                            pageActions.setTitle(`Viewing ${parcels?.length ? parcels.length : 0} parcels`);
                            loading.hide();
                            if (parcelPoints.Data.IgnoredParcelCount > 0) SnackBar('Ignored parcel count : ' + parcelPoints.Data.IgnoredParcelCount, { position: 'bottom-right', autoHide: true, autoHideAfter: 5000 });
                        });
                        return true;
                    }
                    else {
                        console.error(parcelPoints.ErrorMessage);
                        SnackBar('Priority List upload Failed!', { position: 'bottom-right', autoHide: true, autoHideAfter: 3000 });
                        setTimeout(function () { loading.hide(); }, 500);
                        return true;
                    }
                });
            }
            reader.readAsText($("#priListFile")[0].files[0]);

        } else
            say("Sorry! Your browser does not support HTML5!");
    }

}

var openInQc = function (el) {
    let parcelId = $(el).attr("pid");
    function openInNewTab(href) {
        Object.assign(document.createElement('a'), {
            target: '_blank',
            href: href,
        }).click();
    }

    if (!linkOpenedInQc) {
        linkOpenedInQc = !linkOpenedInQc;
        openInNewTab("/protected/quality/#/parcel/" + parcelId);
        setTimeout(function () { linkOpenedInQc = false; }, 60000);
    }
    else
        say("Opening in QC is disabled for multiple clicks in short time. Try again after completely loading the opened one.");
}

var $atc = function (keyword, data, callback, errorCallback) {
    $$$('/parcels/' + keyword + '.jrq', data, callback, errorCallback);
}
