﻿<%@ Page Title="ATC Map View" Language="VB" MasterPageFile="~/App_MasterPages/DesktopWeb.Master"
    AutoEventWireup="false" Inherits="CAMACloud.Console.ATCMapView" CodeBehind="default.aspx.vb" %>

<%@ Import Namespace="System.Web.Script.Services" %>
<%@ Import Namespace="System.Web.Services" %> 
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="CAMACloud" %>
<%@ Import Namespace="CAMACloud.Console" %>
<%@ Import Namespace="CAMACloud.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyC7e5YrZlKqWtaY4ehsDwMgCis4IfNs7Xw"></script>
    <script type="text/javascript" src="/App_Static/jslib/sqllite.js"></script>
    <script type="text/javascript" src="./js/leaflet.js?t=9"></script>
    <script type="text/javascript" src="./js/leaflet.markercluster.js?t=10"></script>
    <script type="text/javascript" src="./js/leaflet.easyprint.js?t=9"></script>
    <script type="text/javascript" src="./js/bootstrap-tour.min.js?t=1"></script>
    <script type="text/javascript" src="./js/bindData.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="./js/master.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="/App_Static/jslib/xlsx.full.min.js"></script>

    <link rel="stylesheet" type="text/css" href="./css/leaflet.css?t=10" />
    <link rel="stylesheet" type="text/css" href="./css/MarkerCluster.Default.css" />
    <link rel="stylesheet" type="text/css" href="./css/MarkerCluster.css" />
    <link rel="stylesheet" type="text/css" href="./css/bootstrap-tour.min.css" />
    <link rel="stylesheet" type="text/css" href="./css/master.css?<%= Now.Ticks %>" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.6.0/chart.min.js" integrity="sha512-GMGzUEevhWh8Tc/njS0bDpwgxdCJLQBWG3Z2Ct+JGOpVnEmjvNx6ts4v6A2XJf1HOrtOsfhv3hBKpK9kE5z8AQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <style type="text/css"></style>
    <script type="text/javascript">
        var hasTaskManagerRole = '<%=  hasTaskManagerRole %>'
        var currentLoginId = '<%= Page.User.Identity.Name %>'
        var countyName = '<%= countyName %>'
        var countyLatLong = { lat:'<%= countyLatLong(0) %>', lng:'<%= countyLatLong(1) %>' }
        var keyList = '<%= KeyList() %>'.split(',');
        var EnableNewPriorities = '<%= EnableNewPriorities %>'
        var TrueFalseInPCI = '<%= DoNotChangeTrueFalseInPCI %>'

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="content-area-wrapper">
        <%-- Page contents go here --%>
        <div class="sb">
            <div class="sb__bar">
                <div class="sb__item" sb-page="dashboard" aria-label="Dashboard">
                    <span class="sb__item-icon"></span>
                    <p class="sb__item-text">Dashboard</p>
                </div>
                <div class="sb__item" sb-page="search" aria-label="Search">
                    <span class="sb__item-icon"></span>
                    <p class="sb__item-text">Search</p>
                </div>
                <div class="sb__item" sb-page="filter" aria-label="Saved Filters">
                    <span class="sb__item-icon"></span>
                    <p class="sb__item-text">Saved Filters</p>
                </div>
                <div class="sb__item" sb-page="result" aria-label="Saved Results">
                    <span class="sb__item-icon"></span>
                    <p class="sb__item-text">Saved Results</p>
                </div>
                <div class="sb__item" sb-page="group" aria-label="Assignment Group Status">
                    <span class="sb__item-icon"></span>
                    <p class="sb__item-text">Assignment Group Status</p>
                </div>
                <div class="sb__item" sb-page="priority" aria-label="Priority List Upload">
                    <span class="sb__item-icon"></span>
                    <p class="sb__item-text">Priority List Upload</p>
                </div>
            </div>
            <div class="sb__content">
                <span class="sb__close"></span>
                <div class="sb__page" sb-id="dashboard">
                    <div class="sb__page-head">
                        <p class="sb__page-title">Current Appraisals Status</p>
                        <div class="sb__page-buttons">
                            <span class="btn btn--icon dash__card-reload" style="display: none;" title="Reload Dashboard Cards" onclick="dashboard.reloadDashboard();">
                                <i class="ic ic__home"></i>
                            </span>
                            <span title="Search Assignment Group by name" class="sb__button-search">
                                <span class="btn btn--icon sb__search-icon"><i class="ic ic__search"></i></span>
                                <span class="sb__search-details">
                                    <input type="text" class="sb__search-input" onkeyup="if(!(event.keyCode>36&&event.keyCode<41)){dashboard.filterSearch(this);}" maxlength="50" placeholder="Search Assignment Group by name" />
                                    <span class="ic ic__search sb__search-find" onclick=""></span>
                                    <span class="btn btn--small sb__search-cancel" onclick="pageActions.clearSearch('dashboard');"><i class="ic ic__close"></i></span>
                                </span>
                            </span>
                        </div>
                    </div>
                    <div class="sb__page-bar"></div>
                    <span class="dashboard__empty sb__page-empty" style="display: none;">No recent appraisals</span>
                    <div id="dash__cardId" class="sb__page-content dash">
                        <div class="dash__card card ${RecentGroups:dashboard.recentGroupCount}">
                            <div class="dash__card-title dash__card-row">
                                <div class="dash__title-left">
                                    <div class="flex align-center justify-between" loginid="${LoginId}">
                                        <p class="text-ellipsis dash__card-user" title="${FullName}" onclick="event.stopPropagation();dashboard.bindUser(this);">${FullName}</p>
                                        <progress max="100" value="${OverallPercentDone}" title="${OverallPercentDone}% completed">${OverallPercentDone}</progress>
                                    </div>
                                    <div class="dash__title-tasks flex align-center">
                                        <span class="text-ellipsis dash__card-nbhd dash__card-tasks--nbhd" title="${AllNbhds} groups assigned to ${FullName}">Total Groups : ${AllNbhds}</span>
                                        <span class="text-ellipsis dash__card-tasks dash__card-tasks--total format_num_task" title="${AllTasks} tasks assigned in ${AllNbhds} groups">Tasks : ${AllTasks}</span>
                                        <span class="text-ellipsis dash__card-tasks" title="${PendingTasks} tasks pending">Pending : ${PendingTasks}</span>
                                    </div>
                                </div>
                                <div class="dash__title-right">
                                    <span class="text-ellipsis dash__card-grp" title="${RecentGroups} recently active Groups">${RecentGroups}</span>
                                    <i class="card__accordion ${RecentGroups:dashboard.hideEmptyArrow}"></i>
                                </div>
                            </div>
                            <div class="dash__card-expand card__detail">
                                <div context="subNbhds" class="dash__card-agroup dash__card-row">
                                    <div class="flex align-center text-ellipsis" nbhdid="${Id}" nbhd="${Nbhd}" onclick="dashboard.plotPoints(this);return false;">
                                        <i class="ic ic__map" title="View Parcels on Map"></i>
                                        <p class="text-ellipsis" title="${Nbhd}">${Nbhd}</p>
                                    </div>
                                    <span class="text-ellipsis dash__card-tasks" title="${pending} Pending out of ${TotalTasks} Priority Tasks">Priorities: ${CompletedTasks}/${TotalTasks}</span>
                                    <progress max="100" value="${PercentDone}" title="${PercentDone}% completed">${PercentDone}</progress>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sb__page" sb-id="search">
                    <div class="sb__page-head">
                        <p class="sb__page-title text-ellipsis">Search</p>
                        <span class="btn btn--small search__reset" title="Resets all filters & conditions" onclick="initSearch.clearAll();">Clear All</span>
                    </div>
                    <div class="sb__page-bar"></div>
                    <div class="sb__page-content search">
                        <div class="search__container">
                            <div class="card search__card search__card--default">
                                <div class="search__card-title flex-row-center">
                                    <p>MA Reviewed</p>
                                    <div class="stoggle">
                                        <div class="stoggle__state">
                                            <input class="stoggle__radio" type="radio" r-val="0" name="radioMA" id="maRadioOne" autocomplete="off" checked />
                                            <label for="maRadioOne" class="stoggle__label">NA</label>
                                        </div>
                                        <div class="stoggle__state">
                                            <input class="stoggle__radio" type="radio" r-val="2" name="radioMA" id="maRadioThree" autocomplete="off" />
                                            <label for="maRadioThree" class="stoggle__label">NO</label>
                                        </div>
                                        <div class="stoggle__state">
                                            <input class="stoggle__radio" type="radio" r-val="1" name="radioMA" id="maRadioTwo" autocomplete="off" />
                                            <label for="maRadioTwo" class="stoggle__label">
                                                YES
                                            <i class="card__accordion"></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="card__detail">
                                    <div class="search__card-date flex-row-center">
                                        <label>Date</label>
                                        <select class="search__MAdate__opr"></select>
                                        <input class="search__MAdate search__date1" type="date" />
                                        <input class="search__MAdate search__date2" style="display: none;" type="date" />
                                    </div>
                                    <div class="search__card-user flex-row-center">
                                        <label>Done by</label>
                                        <select class="search__MAuser"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="card search__card search__card--default">
                                <div class="search__card-title flex-row-center">
                                    <p>DTR Reviewed</p>
                                    <div class="stoggle">
                                        <div class="stoggle__state">
                                            <input class="stoggle__radio" type="radio" r-val="0" name="radioDTR" id="dtrRadioOne" autocomplete="off" checked />
                                            <label for="dtrRadioOne" class="stoggle__label">NA</label>
                                        </div>
                                        <div class="stoggle__state">
                                            <input class="stoggle__radio" type="radio" r-val="2" name="radioDTR" id="dtrRadioThree" autocomplete="off" />
                                            <label for="dtrRadioThree" class="stoggle__label">NO</label>
                                        </div>
                                        <div class="stoggle__state">
                                            <input class="stoggle__radio" type="radio" r-val="1" name="radioDTR" id="dtrRadioTwo" autocomplete="off" />
                                            <label for="dtrRadioTwo" class="stoggle__label">
                                                YES
                                            <i class="card__accordion"></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="card__detail">
                                    <div class="search__card-date flex-row-center">
                                        <label>Date</label>
                                        <select class="search__DTRdate__opr"></select>
                                        <input class="search__DTRdate search__date1" type="date" />
                                        <input class="search__DTRdate search__date2" style="display: none;" type="date" />
                                    </div>
                                    <div class="search__card-user flex-row-center">
                                        <label>Done by</label>
                                        <select class="search__DTRuser"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="card search__card search__card--default">
                                <div class="search__card-title flex-row-center">
                                    <p>Approved</p>
                                    <div class="stoggle">
                                        <div class="stoggle__state">
                                            <input class="stoggle__radio" r-val="0" type="radio" name="radioQC" id="qcRadioOne" autocomplete="off" checked />
                                            <label for="qcRadioOne" class="stoggle__label">NA</label>
                                        </div>
                                        <div class="stoggle__state">
                                            <input class="stoggle__radio" r-val="2" type="radio" name="radioQC" id="qcRadioThree" autocomplete="off" />
                                            <label for="qcRadioThree" class="stoggle__label">NO</label>
                                        </div>
                                        <div class="stoggle__state">
                                            <input class="stoggle__radio" r-val="1" type="radio" name="radioQC" id="qcRadioTwo" autocomplete="off" />
                                            <label for="qcRadioTwo" class="stoggle__label">
                                                YES
                                            <i class="card__accordion"></i>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="card__detail">
                                    <div class="search__card-date flex-row-center">
                                        <label>Date</label>
                                        <select class="search__QCdate__opr"></select>
                                        <input class="search__QCdate search__date1" type="date" />
                                        <input class="search__QCdate search__date2" style="display: none;" type="date" />
                                    </div>
                                    <div class="search__card-user flex-row-center">
                                        <label>Done by</label>
                                        <select class="search__QCuser"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="search__filter-div">
                                <div class="search__af">
                                    <p class="search__af-title">Add New Filter</p>
                                    <div class="search__af-row">
                                        <label>Search By</label>
                                        <select class="search__by"></select>
                                    </div>
                                    <div class="search__af-row">
                                        <label>Filter condition</label>
                                        <select class="search__op"></select>
                                    </div>
                                    <div class="search__af-row">
                                        <label>ID/Value</label>
                                        <input id="SrchId" type="text" autocorrect="off" autocapitalize="off" maxlength="50" class="srch__Id srchtxt" />
                                        <select id="SrchId2" type="text" autocorrect="off" autocapitalize="off" maxlength="50" class="srch__Id2 srchtxt" style="display: none;"></select>
                                        <input id="SrchId3" type="date" autocorrect="off" autocapitalize="off" style="display: none;" class="srch__Id3 srchtxt" />
                                    </div>

                                    <div class="search__af-buttons flex-row-center">
                                        <span class="btn" id="closefilter" onclick="initSearch.toggleAddFilter(false);">Close </span>
                                        <span class="btn search__add" id="addcstmfltr">Add Filter</span>
                                        <span class="btn search__editSave" id="editcstmfltr" style="display: none;">Save</span>
                                    </div>
                                </div>
                                <div class="search__filters">
                                    <div id="searchFilterTemplate" class="card search__filter--template flex align-center justify-between">
                                        <p class="search__filter-title">
                                            <span class="search__filter-field">${label}</span>
                                            <span class="search__filter-condition">${operator}</span>
                                            <span class="search__filter-value">${value}</span>
                                        </p>
                                        <div class="flex-row-center">
                                            <span class="btn btn--icon search__filter-edit" onclick="initSearch.editFilter(this);" filter-id="${Id}"><i class="ic ic__edit"></i></span>
                                            <span class="btn btn--icon search__filter-remove" onclick="initSearch.removeFilter(this);" filter-id="${Id}"><i class="ic ic__close"></i></span>
                                        </div>
                                    </div>
                                    <div class="search__filters__container">
                                    </div>
                                </div>
                                <div class="search__filter-btn flex align-center">
                                    <span class="btn btn--small search__clear-filter" onclick="initSearch.removeAllFilters();" disabled="true"><i class="ic ic__close"></i>Clear</span>
                                    <span class="btn btn--small search__add-filter" onclick="initSearch.toggleAddFilter(true);"><i class="ic ic__filter"></i>Add</span>
                                </div>
                            </div>
                        </div>
                        <div class="sb__page-footer search__actions flex-row-center">
                            <span class="btn btn--primary search__actions__save"><i class="ic ic__download"></i>Save Filters</span>
                            <span class="btn btn--primary search__actions__edit" style="display: none;"><i class="ic ic__download"></i>Save</span>
                            <span class="btn btn--primary search__actions__search"><i class="ic ic__search"></i>Search</span>
                        </div>
                    </div>
                </div>
                <div class="sb__page" sb-id="filter">
                    <div class="sb__page-head">
                        <p class="sb__page-title">Saved Filters</p>
                        <div class="sb__page-buttons">
                            <span title="Search Filter names" class="sb__button-search">
                                <span class="btn btn--icon sb__search-icon"><i class="ic ic__search"></i></span>
                                <span class="sb__search-details">
                                    <input type="text" class="sb__search-input" onkeyup="if(!(event.keyCode>36&&event.keyCode<41)){saveFilters.filterSearch(this);}" maxlength="50" placeholder="Type filter name to search" />
                                    <span class="ic ic__search sb__search-find" onclick=""></span>
                                    <span class="btn btn--small sb__search-cancel" onclick="pageActions.clearSearch('filter');"><i class="ic ic__close"></i></span>
                                </span>
                            </span>
                            <span class="btn btn--icon filter__reset" title="Delete all filters" onclick="if (savedFilterLists.length == 0) return false; saveFilters.remove(this);">
                                <i class="ic ic__delete"></i>
                            </span>
                        </div>
                    </div>
                    <div class="sb__page-bar"></div>
                    <div class="sb__page-content filters">
                        <span class="filter__empty sb__page-empty" style="display: none;">No filters found</span>
                        <div id="filter__card--template-Id" class="card filter__card filter__card--template" filter-id="${Id}">
                            <div class="filter__card-title card__title-lightblue flex align-center justify-between">
                                <label class="filter__card-name text-ellipsis" title="${FilterName}">${FilterName}</label>
                                <span class="filter__card-rename-box card__rename" filter-id="${Id}" style="display: none">
                                    <input type="text" class="filter__card-rename-input" onkeyup="saveFilters.renameKeyup(this);" maxlength="50" />
                                    <span class="filter__card-rename-save card__rename-save" onclick="saveFilters.renameSave(this);">Save</span>
                                    <span class="filter__card-rename-cancel card__rename-cancel" onclick="saveFilters.renameCancel(this);">Cancel</span>
                                </span>
                                <div class="filter__card-actions flex align-center">
                                    <span class="card__action-btn filter__card-rename btn btn--icon" title="Rename" onclick="saveFilters.rename(this);">
                                        <i class="ic ic__rename"></i>
                                    </span>
                                    <span class="card__action-btn filter__card-clone btn btn--icon" title="Duplicate" onclick="saveFilters.clone(this);">
                                        <i class="ic ic__copy"></i>
                                    </span>
                                    <span class="card__action-btn filter__card-delete btn btn--icon" title="Delete" onclick="saveFilters.remove(this);">
                                        <i class="ic ic__delete"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="filter__card-filters flex justify-between">
                                <span context="subFilters" class="${fClass} filter__card-default" title="${FilterText}">${FilterText}</span>
                            </div>
                            <div class="filter__card-footer flex align-center justify-between">
                                <span class="">${Count} conditions applied</span>
                                <div class="filter__card-button flex">
                                    <span class="btn" filter-id="${Id}" onclick="saveFilters.search(this);">Search</span>
                                </div>
                            </div>
                        </div>
                        <div class="filter__list">
                        </div>
                    </div>
                </div>
                <div class="sb__page" sb-id="result">
                    <div class="sb__page-head">
                        <p class="sb__page-title">Saved Results</p>
                        <div class="sb__page-buttons">
                            <span title="Search Filter names" class="sb__button-search">
                                <span class="btn btn--icon sb__search-icon"><i class="ic ic__search"></i></span>
                                <span class="sb__search-details">
                                    <input type="text" class="sb__search-input" onkeyup="if(!(event.keyCode>36&&event.keyCode<41)){searchSavedResults.resultSearch(this);}" maxlength="50" placeholder="Type result name to search" />
                                    <span class="ic ic__search sb__search-find" onclick=""></span>
                                    <span class="btn btn--small sb__search-cancel" onclick="pageActions.clearSearch('result');"><i class="ic ic__close"></i></span>
                                </span>
                            </span>
                            <span class="btn btn--icon filter__reset" title="Delete all saved results" onclick="if (savedResultsLists.length == 0) return false; ask('Are you sure you want to delete all the saved results ?', searchSavedResults.deleteAll, null);">
                                <i class="ic ic__delete "></i>
                            </span>
                        </div>
                    </div>
                    <div class="sb__page-bar"></div>
                    <div class="sb__page-content results">
                        <span class="results__empty sb__page-empty" style="display: none;">No results found</span>
                        <div class="card result__card result__card--template" result-id="${Id}">
                            <div class="result__card-title card__title-lightblue flex align-center justify-between">
                                <div class="flex align-center text-ellipsis">
                                    <label class="result__card-name text-ellipsis" title="${ResultName}">${ResultName}</label>
                                    <span class="result__card-count flex-row-center" title="${ParcelCount} parcels">Parcels <span class="text-ellipsis">${ParcelCount}</span>
                                    </span>
                                </div>
                                <span class="result__card-rename-box card__rename" result-id="${Id}" style="display: none">
                                    <input type="text" class="result__card-rename-input" onkeyup="searchSavedResults.renameKeyup(this);" maxlength="50" />
                                    <span class="result__card-rename-save card__rename-save" onclick="searchSavedResults.renameSave(this)">Save</span>
                                    <span class="result__card-rename-cancel card__rename-cancel" onclick="searchSavedResults.renameCancel(this)">Cancel</span>
                                </span>
                                <div class="result__card-actions flex align-center">
                                    <span class="card__action-btn result__card-rename btn btn--icon" title="Rename" onclick="searchSavedResults.rename(this);">
                                        <i class="ic ic__rename"></i>
                                    </span>
                                    <span class="card__action-btn result__card-delete btn btn--icon" title="Delete" onclick="searchSavedResults.remove(this);">
                                        <i class="ic ic__delete"></i>
                                    </span>
                                </div>
                            </div>
                            <div class="result__card-details flex">
                                <div class="flex justify-between">
                                    <div class="result__card-info flex align-center">
                                        <i class="ic ic__filter-list"></i>
                                        <span class="result__card-filters text-ellipsis">${FilterCount} conditions</span>
                                    </div>
                                    <div class="result__card-info flex align-center">
                                        <i class="ic ic__user"></i>
                                        <span class="result__card-user text-ellipsis">${UserName}</span>
                                    </div>
                                    <div class="result__card-info flex align-center">
                                        <i class="ic ic__save"></i>
                                        <span class="result__card-date text-ellipsis">${ResultDate}</span>
                                    </div>
                                </div>
                                <span result-id="${Id}" class="result__card-btn btn" onclick="searchSavedResults.viewOnMap(this);">View</span>
                            </div>
                        </div>
                        <div id="resultList" class="result__list">
                        </div>
                    </div>
                </div>
                <div class="sb__page" sb-id="group">
                    <div class="sb__page-head">
                        <p class="sb__page-title text-ellipsis">Assignment Group Status</p>
                        <div class="sb__page-buttons">
                            <span class="btn btn--icon group__reload" title="Reload Assignment Group Data" onclick="assignmentGroupStatus.reloadGroups(this);">
                                <i class="ic ic__reload"></i>
                            </span>
                            <span title="Search Filter names" class="sb__button-search">
                                <span class="btn btn--icon sb__search-icon"><i class="ic ic__search"></i></span>
                                <span class="sb__search-details">
                                    <input type="text" class="sb__search-input" onkeyup="if(!(event.keyCode>36&&event.keyCode<41)){assignmentGroupStatus.groupSearch(this);}" maxlength="50" placeholder="Search Assignment Group by name" />
                                    <span class="ic ic__search sb__search-find" onclick=""></span>
                                    <span class="btn btn--small sb__search-cancel" onclick="pageActions.clearSearch('group');"><i class="ic ic__close"></i></span>
                                </span>
                            </span>
                        </div>
                    </div>
                    <div class="sb__page-bar"></div>
                    <div class="sb__page-content">
                        <div class="group__page-content">
                            <div class="group__filter-div flex align-center justify-between">
                                <div class="group__filter group__filter--flag">
                                    <label>Assignment Group Flag</label>
                                    <select class="group__filter-flag"></select>
                                </div>
                                <div class="group__filter group__filter--user">
                                    <label>Assigned User</label>
                                    <select class="group__filter-user"></select>
                                </div>
                            </div>
                            <div class="group__assign-div flex justify-between" style="display: none;">
                                <div class="group__assign group__assign--user">
                                    <label>Assign <b class="group__assign-count"></b>&nbsp;groups to </label>
                                    <select class="group__assign-select" onchange="assignmentGroupStatus.assgnChange(this);"></select>
                                </div>
                                <div class="group__assign group__assign--buttons flex-row-center">
                                    <span class="btn btn--small group__assignuser" onclick="assignmentGroupStatus.assignGroups(this);">Assign</span>
                                    <span class="btn btn--small group__unassign" onclick="assignmentGroupStatus.unAssignGroup(this);">Unassign</span>
                                    <span class="btn btn--small group__assign-cancel" onclick="assignmentGroupStatus.assgnCancel();">Cancel</span>
                                </div>
                            </div>
                            <span class="assgn__empty sb__page-empty" style="display: none;">Sorry, it appears there is no data matching your criteria. Please adjust your search parameters and try again.</span>
                            <div class="card group__card group__card--template" assgn-id="${Id}">
                                <div class="group__card-title card__title-lightblue">
                                    <div class="flex-row-center">
                                        <input type="checkbox" class="group__card-selected" onchange="assignmentGroupStatus.checkBoxChange();" assgn-id="${Id}" />
                                        <label class="group__card-name text-ellipsis" title="${Number}">${Number}</label>
                                        <span class="btn btn--icon group__card-map" title="View Group on Map" onclick="assignmentGroupStatus.viewOnMap(this);"><i class="ic ic__map"></i></span>
                                    </div>
                                </div>

                                <div class="group__card-content ">
                                    <div class="group__card-details flex-row-center justify-between">
                                        <div class="group__card-user flex align-center text-ellipsis">
                                            <i class="ic ic__user"></i>
                                            <span class="text-ellipsis" title="Assigned to ${FullName}">${FullName:assignmentGroupStatus.assignedUser}</span>
                                        </div>
                                        <div class="group__card-date flex align-center text-ellipsis" title="Last visited : ${LastVisitedTime}">
                                            <span class="text-ellipsis">Last visited :<i>&nbsp;<b>${LastVisitedTime}</b></i></span>
                                        </div>
                                        <div class="flex align-center text-ellipsis" title="Pending Priorities : ${PendingPriorities}">
                                            <span class="text-ellipsis"><b>${PendingPriorities}&nbsp;</b>Priorities Pending</span>
                                        </div>
                                    </div>
                                    <%If EnableNewPriorities Then%>
                                        <table class="group__card-table">
                                            <tr>
                                                <td class="">Total</td>
                                                <td class=""><span class="group__table-count text-ellipsis">${TotalParcels}</span></td>
                                                <td class="" title="Total priority Parcels : ${TotalPriorityparcels}">
                                                    <div class="flex-row-center">
                                                        <span class="group__table-count text-ellipsis">${TotalPriorityparcels}</span>
                                                        <!--<label class="group__table-label">!</label>-->
                                                    </div>
                                                </td>
                                                <!--<td class="" title="Total Urgent Parcels : ${TotalAlerts}">
                                                    <div class="flex-row-center">
                                                        <span class="group__table-count text-ellipsis">${TotalAlerts}</span>
                                                        <label class="group__table-label">!!</label>
                                                    </div>
                                                </td>-->
                                            </tr>
                                            <tr>
                                                <td class="">Visited</td>
                                                <td class=""><span class="group__table-count text-ellipsis">${VisitedParcels}</span></td>
                                                <td class="" title="Visited priority Parcels : ${TotalVisitedPriorityParcels}">
                                                    <div class="flex-row-center">
                                                        <span class="group__table-count text-ellipsis">${TotalVisitedPriorityParcels}</span>
                                                        <!--<label class="group__table-label">!</label>-->
                                                    </div>
                                                </td>
                                                <!--<td class="" title="Visited Urgent Parcels : ${VisitedAlerts}">
                                                    <div class="flex-row-center">
                                                        <span class="group__table-count text-ellipsis">${VisitedAlerts}</span>
                                                        <label class="group__table-label">!!</label>
                                                    </div>
                                                </td>-->
                                            </tr>
                                        </table>
                                    <% Else %>
                                        <table class="group__card-table">
                                            <tr>
                                                <td class="">Total</td>
                                                <td class=""><span class="group__table-count text-ellipsis">${TotalParcels}</span></td>
                                                <td class="" title="Total High Parcels : ${TotalPriorities}">
                                                    <div class="flex-row-center">
                                                        <span class="group__table-count text-ellipsis">${TotalPriorities}</span>
                                                        <label class="group__table-label">!</label>
                                                    </div>
                                                </td>
                                                <td class="" title="Total Urgent Parcels : ${TotalAlerts}">
                                                    <div class="flex-row-center">
                                                        <span class="group__table-count text-ellipsis">${TotalAlerts}</span>
                                                        <label class="group__table-label">!!</label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="">Visited</td>
                                                <td class=""><span class="group__table-count text-ellipsis">${VisitedParcels}</span></td>
                                                <td class="" title="Visited High Parcels : ${VisitedPriorities}">
                                                    <div class="flex-row-center">
                                                        <span class="group__table-count text-ellipsis">${VisitedPriorities}</span>
                                                        <label class="group__table-label">!</label>
                                                    </div>
                                                </td>
                                                <td class="" title="Visited Urgent Parcels : ${VisitedAlerts}">
                                                    <div class="flex-row-center">
                                                        <span class="group__table-count text-ellipsis">${VisitedAlerts}</span>
                                                        <label class="group__table-label">!!</label>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    <%End If%>
                                    
                                </div>
                            </div>
                            <div id="assgnCard" class="group__list">
                            </div>
                        </div>
                    </div>
                    <div class="sb__page-footer ">
                        <div class="group__page-div flex-row-center">
                        </div>
                    </div>
                </div>
                <div class="sb__page" sb-id="priority">
                    <div class="sb__page-head">
                        <p class="sb__page-title">Priority List Upload</p>
                    </div>
                    <div class="sb__page-bar"></div>
                    <div class="sb__page-content">
                        <div class="priority__info badge badge--grey card">
                            <div class="flex align-center">
                                <h4>Please read the requirements below prior to uploading any files!</h4>
                                <i class="card__accordion card__accordion--expanded">&nbsp;</i>
                            </div>
                            <div class="card__detail card__detail--expanded">
                                <ul class="priority__points">
                                    <li>Uploaded file should be an Excel <b>.csv</b> file with a maximum size of <b>4MB</b>.</li>
                                    <li>The Column headers in the CSV file should be <b runat="server"><%=KeyList()%>,PRIORITY, MESSAGE</b> in same order.</li>
                                    <%If EnableNewPriorities Then%>
                                        <li><b>PRIORITY</b> - The accepted values are: 0 (Proximity), 1 <mark style="background-color: #8944FA;">(Normal)</mark>, 2 <mark style="background-color: #00B0F0;">(Medium)</mark>, 3 <mark style="background-color: #FFFF00;">(High)</mark>, 4 <mark style="background-color: #FFC000;">(Urgent)</mark>, 5 <mark style="background-color: #FF0000;">(Critical)</mark>.</li>
                                        <li><b>MESSAGE</b> - This field will set the alert message for non proximity parcels.
                                            For proximity parcels, this field will be ignored even if provided and will be updated as blank.</li>
                                    <% Else %>  
                                            <li><b>PRIORITY</b> - The accepted values are: 0 (Normal), 1 (High), 2 (Urgent).</li>
                                            <li><b>MESSAGE</b> - This field will set the alert message for the High and Urgent parcels.
                                            For Normal parcels, this field will be ignored even if provided and will be updated as blank.</li>
                                    <%End If %>   
                                    <li>Be sure to check the column headers for typos or spaces: Example: <span id='prioritykeyValueMsg'>
                                        <script>let pos = Math.floor(Math.random() * (keyList[0].length - 1)) + 1; $('#prioritykeyValueMsg').html('<b>' + keyList[0] + '</b> is Correct. <b>' + [keyList[0].slice(0, pos), ' ', keyList[0].slice(pos)].join('') + '</b> is not correct.')</script>
                                    </span></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card">
                            <div class="flex align-center">
                                <span class="priority__sample">You can download the sample Priority List file
                                    <asp:LinkButton ID="lbDowload" OnClick="ExportCSV" runat="server"> here</asp:LinkButton>
                                </span>
                            </div>
                            <input type="file" id='priListFile' class="priority__file-upload" onclick="priority.clearFile();" accept=".csv" />
                            <div class="flex">
                                <span class="priority__btn-upload btn" onclick="priority.verifyPriorityList()"><i class="ic ic__upload"></i>Upload</span>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mv">
            <div class="map__div">
                <div class="mfab">
                    <div class="mfab__button mfab__group hover__popup" onclick="adhocCreate.showAdhocPopup(true);" aria-label="Create Assignment Group">
                        <label class="mfab__label">Create Assignment Group</label>
                        <span class="mfab__span"><i class="ic ic__create"></i></span>
                        <%--<span class="mfab__span btn btn--icon"><i class="ic ic__create"></i></span>--%>
                    </div>
                    <div class="mfab__button mfab__result hover__popup" onclick="searchSavedResults.showSavePopup(true);" aria-label="Save Result">
                        <label class="mfab__label">Save Result</label>
                        <span class="mfab__span"><i class="ic ic__save"></i></span>
                    </div>
                    <div class="mfab__button mfab__list hover__popup" onclick="downloadList.exportData();" aria-label="Download Parcel List">
                        <label class="mfab__label">Download Parcel List</label>
                        <span class="mfab__span"><i class="ic ic__download"></i></span>
                    </div>
                    <%--<div class="mfab__button mfab__priority hover__popup" aria-label="Priority List Upload">
                    <label class="mfab__label">Priority List Upload</label>
                    <span class="mfab__span" onclick="pageActions.openPage('priority');"><i class="ic ic__upload"></i></span>
                </div>--%>
                    <div class="mfab__button mfab__image hover__popup" aria-label="Save as Image">
                        <label class="mfab__label">Save as Image</label>
                        <span class="mfab__span" onclick="atcMap.saveAsImage();"><i class="ic ic__image"></i></span>
                    </div>
                    <div class="mfab__button mfab__lasso hover__popup" aria-label="Select Parcels">
                        <label class="mfab__label">Select Parcels</label>
                        <span class="mfab__span" id="toggleLasso" onclick="atcMap.lasso.enabled()?atcMap.lasso.disable():atcMap.lasso.enable()"><i class="ic ic__lasso"></i></span>
                    	<span class= "mfab__info" style="display:none" id= "lassoCount"> </span>
                    </div>
                    <div class="mfab__button mfab__lasso-list hover__popup" aria-label="Download Selected Parcel List">
                        <label class="mfab__label">Download Selected Parcel List</label>
                        <span class="mfab__span" onclick="lasso.SelectedexportData();"><i class="ic ic__download-lasso"></i></span>
                    </div>
                    <div class="mfab__button mfab__lasso-clear hover__popup" aria-label="Clear All Selection">
                        <label class="mfab__label">Clear Selection</label>
                        <span class="mfab__span" onclick="lasso.resetSelectedState();"><i class="ic ic__remove-lasso"></i></span>
                    </div>
                </div>
                <div class="map__title popup-dg">
                    <p class=""></p>
                </div>
                <div id="map" style="width: 100%; height: 100%"></div>
                <div class="map-zoom">
                    <span class="map-zoom__in" onclick="map.zoomIn();"><i class="ic ic__add"></i></span>
                    <span class="map-zoom__in" onclick="map.zoomOut();"><i class="ic ic__remove"></i></span>
                </div>
                <div class="layer-settings">
                    <div class='add-overlay hover__popup' onclick='pageActions.popup($("#layer-pop"));' aria-label="Add Layer">
                        <i class="ic ic__add-layer">&nbsp;</i>
                    </div>
                    <span class="layer__button hover__popup" onclick="pageActions.layerSettings(-1);" aria-label="Layers"><i class="ic ic__layer"></i></span>
                    <div class="layer-select">
                        <div class="layer-select__radio">
                        </div>
                        <div class="layer-list">
                        </div>
                    </div>
                </div>
            </div>
            <div class="mv__dash">
                <i style="color: red; font-weight: 600">Dashboard under developement. Some of the datas are dummy data</i>
                <div id="user-pop" class="card user-pop">
                    <a onclick="return dashboard.userPopup(false);" class="user-pop__close"><i class="ic__close ic"></i></a>
                    <div class="user-pop__loading">
                        <label class="user-pop__name">User Statistics - <span class="placeholder" style="width: 200px">&nbsp;</span></label>
                        <div class="user-pop__time">
                            <div class="flex-row-center">
                                <label>Last MA Login</label>
                                <span class="placeholder" style="margin: 0 4px; width: 85%">&nbsp;</span>
                            </div>
                            <div class="flex-row-center">
                                <label>Last MA Download</label>
                                <span class="placeholder" style="margin: 0 4px; width: 85%">&nbsp;</span>
                            </div>
                            <div class="flex-row-center">
                                <label>Last MA Data Sync</label>
                                <span class="placeholder" style="margin: 0 4px; width: 85%">&nbsp;</span>
                            </div>
                            <div class="flex-row-center">
                                <label>Last QC Opened</label>
                                <span class="placeholder" style="margin: 0 4px; width: 85%">&nbsp;</span>
                            </div>
                        </div>
                    </div>
                    <div class="user-pop__data">
                        <label class="user-pop__name">User Statistics - ${Username}</label>
                        <div class="user-pop__time">
                            <div class="flex-row-center">
                                <label>Last MA Login</label>
                                <span class="text-ellipsis">${MaLoginDate}</span>
                            </div>
                            <div class="flex-row-center">
                                <label>Last MA Download</label>
                                <span class="text-ellipsis">${DownloadDate}</span>
                            </div>
                            <div class="flex-row-center">
                                <label>Last MA Data Sync</label>
                                <span class="text-ellipsis">${DataSyncDate}</span>
                            </div>
                            <div class="flex-row-center">
                                <label>Last QC Opened</label>
                                <span class="text-ellipsis">${QCAccessDate}</span>
                            </div>
                        </div>
                        <h4 style="font-weight: 500">Assigned Groups</h4>
                        <table class="user-pop__table">
                            <tr>
                                <th>Group Name</th>
                                <th>Total Tasks</th>
                                <%If EnableNewPriorities Then %>
                                    <th>Priority | Visited</th>
                                <%Else %>
                                    <th>Urgent | Visited</th>
                                    <th>High | Visited</th>
                                <%End If%>
                                <%--<th>Visited Urgent</th>
                            <th>Visited High</th>--%>
                            </tr>
                            <%If EnableNewPriorities Then %>
                                <tbody>
                                    <tr context="subNbhds">
                                        <td><a class="text-ellipsis" nbhdid="${Id}" nbhd="${Nbhd}" onclick="dashboard.plotPoints(this);return false;">${Nbhd}</a></td>
                                        <td>
                                            <div class="user-pop__td--plain flex">
                                                <span class="text-ellipsis">${TotalTasks}</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="user-pop__td flex">
                                                <span class="text-ellipsis">${TotalPriorityparcels}</span>
                                                <span class="text-ellipsis">${TotalVisitedPriorityParcels}</span>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            <%Else %>
                                <tbody>
                                    <tr context="subNbhds">
                                        <td><a class="text-ellipsis" nbhdid="${Id}" nbhd="${Nbhd}" onclick="dashboard.plotPoints(this);return false;">${Nbhd}</a></td>
                                        <td>
                                            <div class="user-pop__td--plain flex">
                                                <span class="text-ellipsis">${TotalTasks}</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="user-pop__td flex">
                                                <span class="text-ellipsis">${TotalAlerts}</span>
                                                <span class="text-ellipsis">${VisitedAlerts}</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="user-pop__td flex">
                                                <span class="text-ellipsis">${TotalPriorities}</span>
                                                <span class="text-ellipsis">${VisitedPriorities}</span>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            <%End If%>
                        </table>
                    </div>
                </div>
                <div Class="flex dt">
                    <div Class="card dt__count dt__pending">
                        <Label Class="dt__title">Pending Parcels</label>
                        <canvas id = "pendingPieChart" ></canvas>
                                                                                                                    </div>
                    <div Class="mv__dash-center">
                        <div Class="card dt__count dt__today">
                            <div Class="dt__sub-card dt__group-count">
                                <Label Class="dt__title">Total Groups</label>
                                <span Class="count">--</span>
                            </div>
                            <div Class="dt__sub-card dt__parcel-count">
                                <Label Class="dt__title">Total Parcels</label>
                                <span Class="count">--</span>
                            </div>
                            <div Class="dt__sub-card dt__priority-count">
                                <Label Class="dt__title">Priority Parcels</label>
                                <span Class="count">--</span>
                            </div>
                            <div Class="dt__sub-card dt__user-count">
                                <Label Class="dt__title">Total Appraisers</label>
                                <span Class="count">--</span>
                            </div>
                        </div>
                        <div Class="card dt__count dt__total-progress">
                            <div Class="dt__groups">
                                <Label Class="dt__title">Groups Completed</label>
                                <div Class="flex">
                                    <span Class="count">0</span>
                                    <span Class="sub-count"></span>
                                </div>
                                <Progress max = "100" value="0" title="0% completed">0</progress>
                            </div>
                            <div Class="dt__parcels">
                                <Label Class="dt__title">Parcels Completed</label>
                                <div Class="flex">
                                    <span Class="count">0</span>
                                    <span Class="sub-count"></span>
                                </div>
                                <Progress max = "100" value="0" title="0% completed">0</progress>
                            </div>
                        </div>
                    </div>
                    <div Class="card dt__count dt__recent">
                        <div Class="flex">
                            <Label Class="dt__title">Groups with recent activity</label>
                            <a Class="dt__recent-switch dt__recent-switch--today" onclick=""></a>
                        </div>
                        <div Class="flex">
                            <span Class="count"></span>
                            <div Class="dt__recent-links">
                            </div>
                        </div>
                    </div>
                </div>
                <div Class="flex mv__line-chart">
                    <div Class="card line__weekly-task">
                        <p Class="line__title">Tasks completed during last 7 days</p>
                        <canvas id = "lastWeekTasks" ></canvas>
                                                                                                                                                                                                                                                                                                                                                                                                            </div>
                    <div Class="card line__weekly-second">
                        <p Class="line__title">Priority parcels worked during last 7 days</p>
                        <canvas id = "lastWeekSecond" ></canvas>
                                                                                                                                                                                                                                                                                                                                                                                                                            </div>
                </div>
                <div Class="flex mv__recent">
                    <div Class="mv__recent-filters"></div>
                    <div Class="mv__recent-results"></div>
                </div>
            </div>
        </div>
        <span class="atc__login-user" style="display: none;"><%=Page.UserDisplayName %></span>
    </div>
</asp:Content>
<asp:Content ID="PopupContent" ContentPlaceHolderID="ATCPopupPlaceholder" runat="server">
    <div class="mask__atc">
        <div class="result-pop popup-atc popup-dg">
            <h3 class="">Save Result</h3>
            <input type="text" maxlength="50" class="result-pop__name" placeholder="Enter the result name" />
            <span class="sb__validate"></span>
            <p class="alert alert-info">
                The result will be stored as is for viewing later. Even if the parcel details 
                    are changed later the result will be based on the filter conditions at the time the Filter was saved.
            </p>
            <div class="sb__input-btn">
                <span class="btn" onclick="searchSavedResults.showSavePopup(false);">Cancel</span>
                <span class="btn btn--blue" onclick="searchSavedResults.save()">Save</span>
            </div>
        </div>
        <div class="layer-pop popup-atc popup-dg" id="layer-pop">
            <h3 class="">Add new Layer</h3>
            <div class="flex-row-center">
                <label>Layer Name :</label>
                <input type="text" class="layer-pop__name" maxlength="25" id="layer-pop__name" onkeyup="$('.layer-pop .sb__validate').hide(); return false;" />
            </div>
            <div class="flex-row-center layer-pop__color-div">
                <label>Marker Color :</label>
                <span style="width: 250px">
                    <input type="color" class="layer-pop__color" id="layer-pop__color" />
                </span>
            </div>
            <span class="sb__validate"></span>
            <div class="sb__input-btn">
                <span class="btn" onclick="atcMapLayers.cancelLayerPopup();">Cancel</span>
                <span class="btn btn--blue" onclick="atcMapLayers.addLayerToLayerGroup();">Save</span>
            </div>
        </div>
        <div class="adhoc-pop popup-atc popup-dg">
            <h3 class="">Create Assignment Group</h3>
            <div>
                <span class="adhoc__div adhoc__name-div flex-row-center">
                    <label>Group Code/Number :</label>
                    <input type="text" maxlength="50" class="txt-adhoc-group-name" onkeyup="$('.adhoc__error-msg').hide();$('.txt-adhoc-group-name').removeClass('invalid');" />
                </span>
                <span class="adhoc__div adhoc__user-div flex-row-center">
                    <label>Assign group to : </label>
                    <select class="ddlAssignTo"></select>
                </span>
            </div>
            <div class="adhoc__priority" style="display: none;">
                <span class="adhoc__div flex-row-center">
                    <label>Set all Parcel priorities as :</label>
                    <% If EnableNewPriorities Then %>
                        <select class="adhoc__priority-div">
                            <option value="">--- Select ---</option>
                            <option value="0">Proximity</option>
                            <option value="1">Normal</option>
                            <option value="2">Medium</option>
                            <option value="3">High</option>
                            <option value="4">Urgent</option>
                            <option value="5">Critical</option>
                        </select>
                    <%Else %>
                        <select class="adhoc__priority-div">
                            <option value="">--- Select ---</option>
                            <option value="0">Normal</option>
                            <option value="1">High</option>
                            <option value="2">Urgent</option>
                        </select>
                    <%End If %>
                </span>
                <%--<div class="flex-row-center">
                    <p>Set all Parcel priorities as :</p>
                    <div class="flex adhoc__priority-div">
                        <input id="adhocPriorityUrgent" type="radio" class="adhoc__priority-radio" name="adhoc__priority" value="2" />
                        <label for="adhocPriorityUrgent">Urgent</label>
                    </div>
                    <div class="flex adhoc__priority-div">
                        <input id="adhocPriorityHigh" type="radio" class="adhoc__priority-radio" name="adhoc__priority" value="1" />
                        <label for="adhocPriorityHigh">High</label>
                    </div>
                    <div class="flex adhoc__priority-div">
                        <input id="adhocPriorityNormal" type="radio" class="adhoc__priority-radio" name="adhoc__priority" value="0" />
                        <label for="adhocPriorityNormal">Normal</label>
                    </div>
                </div>--%>
            </div>
            <div>
                <p class="alert alert-info">
                    Important: The Field Review and QC approval status of all parcels in the list will be reset to NULL.
                </p>
                <span class="adhoc__check flex">
                    <input type="checkbox" class="adhocCheck" id="popAdhocCheck" />
                    <label for="popAdhocCheck">Do not reset review and QC approval status for the parcels in the uploaded list.</label>
                </span>
            </div>
            <p class="alert alert-danger adhoc__error-msg"></p>
            <div class="sb__adhoc-btn flex-row-center">
                <span class="btn" onclick="adhocCreate.showAdhocPopup(false);">Cancel</span>
                <span class="btn btn--blue" onclick="adhocCreate.createPriority()">Continue</span>
            </div>
        </div>
        <div class="group-pop popup-atc popup-dg">
            <div class="group-pop-second" style="display: none;">
                <h3>Verify Priority List</h3>
                <div class="group-pop__scroll">
                    <div class="group-pop__group-exists"></div>
                    <div class="flex group-pop__info">
                        <div class="group-pop__count">
                            <p class="group-pop__info-title">Priority List Analytic Results</p>
                            <table class="group-pop__count-table">
                                <tr>
                                    <%If EnableNewPriorities Then %>
                                        <td>Total</td>
                                        <td>Critical</td>
                                        <td>Urgent</td>
                                        <td>High</td>
                                        <td>Medium</td>
                                        <td>Normal</td>
                                        <td>Proximity</td>
                                    <%Else %>
                                        <td>Total</td>
                                        <td>Urgent</td>
                                        <td>High</td>
                                        <td>Normal</td>
                                    <%End If %>
                                    
                                    <td class="group__pop-invalidParcel">Invalid</td>
                                    <td class="group__pop-duplicateParcel">Duplicated</td>
                                </tr>
                                <tr>
                                    <td>
                                        <label class="lablebold count" id="lblTotalParcel"></label>
                                    </td>
                                    <%If EnableNewPriorities Then %>
                                        <td>
                                            <label class="lablebold count" id="lblCritical"></label>
                                        </td>
                                        <td>
                                            <label class="lablebold count" id="lblUrgent"></label>
                                        </td>
                                        <td>
                                            <label class="lablebold count" id="lblHigh"></label>
                                        </td>
                                        <td>
                                            <label class="lablebold count" id="lblMedium"></label>
                                        </td>
                                        <td>
                                            <label class="lablebold count" id="lblNormal"></label>
                                        </td>
                                        <td>
                                            <label class="lablebold count" id="lblProximity"></label>
                                        </td>
                                    <%Else %>
                                        <td>
                                            <label class="lablebold count" id="lblUrgent"></label>
                                        </td>
                                        <td>
                                            <label class="lablebold count" id="lblHigh"></label>
                                        </td>
                                        <td>
                                            <label class="lablebold count" id="lblNormal"></label>
                                        </td>
                                    <%End If %>
                                    <td class="group__pop-invalidParcel">
                                        <label class="lablebold count" id="lblInvalid"></label>
                                    </td>
                                    <td class="group__pop-duplicateParcel">
                                        <label class="lablebold count" id="lblDuplicatedParcel"></label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="group-pop__adhoc">
                            <p class="group-pop__info-title">Group Information</p>
                            <div id="adhgrpExists"></div>
                        </div>
                    </div>
                    <div class="group-pop__alert alert flex-row-center">
                        <img src="/App_Static/css/icon16/done.png" id="imgcompAnalyticWarning" style="margin-right: 5px;" />
                        <label>Compiling Analytics and Warnings</label>
                    </div>
                    <div class="group-pop__warning" style="overflow-y:auto; max-height:150px">
                        <label id="lblWarning"></label>
                        <div class="group-pop__warn group-pop__warn--approved flex">
                            <img src="/App_Static/images/warning-sign.png" width="30" height="30" />
                            <div>
                                <label id="lblNotApprovedParcel"></label>
                                <div class="group-pop__warn-check flex">
                                    <input type="checkbox" name="adhc" value="High" id="chkNotApprovedparcel" />
                                    <label id="lblRemoveNotApprovedparcel"></label>
                                </div>
                            </div>
                        </div>
                        <div class="group-pop__warn group-pop__warn--synced flex">
                            <img src="/App_Static/images/warning-sign.png" width="30" height="30" />
                            <div class="">
                                <label id="lblNotSyncedParcel"></label>
                                <div class="group-pop__warn-check flex">
                                    <input type="checkbox" name="adhc" value="High" id="chkNotSyncedparcel" />
                                    <label id="lblRemoveNotSyncedParcel"></label>
                                </div>
                            </div>
                        </div>
                        <div class="group-pop__warn group-pop__warn--assigned flex">
                            <img src="/App_Static/images/warning-sign.png" width="30" height="30" />
                            <div class="">
                                <label id="lblAssignedParcel"></label>
                                <div class="group-pop__warn-check flex">
                                    <input type="checkbox" name="adhc" value="High" id="chkAssignedParcel" />
                                    <label id="lblRemoveAssignedParcel"></label>
                                </div>
                                <div id="gpAssignedGroups"></div>
                            </div>
                        </div>
                        <div class="group-pop__warn group-pop__warn--workedRecently flex">
                            <img src="/App_Static/images/warning-sign.png" width="30" height="30" />
                            <div class="">
                                <label id="lblWorkedInFieldLast30days"></label>
                                <div class="group-pop__warn-check flex">
                                    <input type="checkbox" name="adhc" value="High" id="chkWorkedInFieldLast30days" />
                                    <label id="lblRemoveWorkedInFieldLast30days"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="bppUpdate" style="display: none;">
                        <hr style="border-top: 1px solid #ddd;"/>
                        <p style="font-weight: bold;">There are Real Properties related to the Personal Property accounts in this list. Do you want these Real Properties to be visited along with the Personal Property?</p>
                        <div style="display: block;">
                          <%If EnableNewPriorities Then %>
                              <input type="radio" id="pr6" name="bppGroup" value="6" style="margin-bottom: 10px;" checked="checked">
                              <label for="pr6"><b>Yes</b> -  Set the Real Property's Priority the same as the Personal Property's Priority </label>
                              <br>
                              <input type="radio" id="pr5" name="bppGroup" value="5" style="margin-bottom: 10px;">
                              <label for="pr5"><b>Yes</b> - Set as Critical priority </label>
                              <br>
                              <input type="radio" id="pr4" name="bppGroup" value="4" style="margin-bottom: 10px;">
                              <label for="pr4"><b>Yes</b> - Set as Urgent priority </label>
                              <br>
                              <input type="radio" id="pr3"  value="3" style="margin-bottom: 10px;" name="bppGroup">
                              <label for="pr3"><b>Yes</b> - Set as High priority </label>
                              <br>
                              <input type="radio" id="pr2" value="2" style="margin-bottom: 10px;" name="bppGroup">
                              <label for="pr2"><b>Yes</b> - Set as Medium priority </label>
                              <br>
                              <input type="radio" id="pr1" value="1" style="margin-bottom: 10px;" name="bppGroup">
                              <label for="pr1"><b>Yes</b> - Set as Normal priority </label>
                              <br>
                              <input type="radio" id="pr0" value="0" style="margin-bottom: 10px;" name="bppGroup">
                              <label for="pr0"><b>No</b> - Do not flag Real Properties to be visited</label>
                          <%Else %>
                              <input type="radio" id="pr3" name="bppGroup" value="3" style="margin-bottom: 10px;" checked="checked"/>
                              <label for="pr3"><b>Yes</b> -  Set the Real Property's Priority the same as the Personal Property's Priority </label>
                              <br/>
                              <input type="radio" id="pr2" name="bppGroup" value="2" style="margin-bottom: 10px;"/>
                              <label for="pr2"><b>Yes</b> - Set as Urgent priority </label>
                              <br/>
                              <input type="radio" id="pr1" value="1" style="margin-bottom: 10px;" name="bppGroup"/>
                              <label for="pr1"><b>Yes</b> - Set as High priority </label>
                              <br/>
                              <input type="radio" id="pr0" value="0" style="margin-bottom: 10px;" name="bppGroup"/>
                              <label for="pr0"><b>No</b> - Do not flag Real Properties to be visited</label>
                          <%End If %>
                                                 
                       </div>
                   </div>
                </div>
                <div class="group-pop-btn flex-row-center">
                    <span class="btn" onclick="adhocCreate.closeSecondStep()">Close</span>
                    <span class="btn btn--blue" onclick="adhocCreate.uploadListConfirmation();">Submit</span>
                </div>
            </div>
            <div class="group-third" style="display: none;">
                <h3>Priority List Upload Results</h3>
                <div class="group-third__success">
                    <label class="alert alert-success" id="lblSuccess"></label>
                    <div class="group-third__parcels">
                        <span class="group-third__parcels-title">Parcel Affected</span>
                        <table>
                            <tr>
                                <%If EnableNewPriorities Then %>
                                    <td>Total Parcels</td>
                                    <td>Critical Priority(s)</td>
                                    <td>Urgent Priority(s)</td>
                                    <td>High Priority(s)</td>
                                    <td>Medium Priority(s)</td>
                                    <td>Normal Priority(s)</td>
                                    <td>Proximity Priority(s)</td>
                                <%Else %>
                                    <td>Total Parcels</td>
                                    <td>Urgent Priority(s)</td>
                                    <td>High Priority(s)</td>
                                    <td>Normal Priority(s)</td>
                                <%End If %>
                            </tr>
                            <tr>
                                <%If EnableNewPriorities Then %>
                                    <td>
                                        <label id="lblTotalAffectedparcelCount" class="lablebold count"></label>
                                    </td>
                                    <td>
                                        <label id="lblTotalAffectedCriticalCount" class="lablebold count"></label>
                                    </td>
                                    <td>
                                        <label id="lblTotalAffectedUrgentCount" class="lablebold count"></label>
                                    </td>
                                    <td>
                                        <label id="lblTotalAffectedHighCount" class="lablebold count"></label>
                                    </td>
                                    <td>
                                        <label id="lblTotalAffectedMediumCount" class="lablebold count"></label>
                                    </td>
                                    <td>
                                        <label id="lblTotalAffectedNormalCount" class="lablebold count"></label>
                                    </td>
                                    <td>
                                        <label id="lblTotalAffectedProximityCount" class="lablebold count"></label>
                                    </td>
                                <%Else %>
                                    <td>
                                        <label id="lblTotalAffectedparcelCount" class="lablebold count"></label>
                                    </td>
                                    <td>
                                        <label id="lblTotalAffectedUrgentCount" class="lablebold count"></label>
                                    </td>
                                    <td>
                                        <label id="lblTotalAffectedHighCount" class="lablebold count"></label>
                                    </td>
                                    <td>
                                        <label id="lblTotalAffectedNormalCount" class="lablebold count"></label>
                                    </td>
                                <%End If %>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="group-third__btn flex">
                    <span class="btn btn--blue" onclick="adhocCreate.closeThirdStep()">Close</span>
                </div>
            </div>
            <div id="group-progressBar" style="display: none; position: relative; width: 100%; background: #dcdcdc; height: 15px; margin-top: 10px;">
                <div id="group-currentProgress" style="position: relative; width: 0%; height: 100%; background: #2164df;"></div>
            </div>
        </div>
    </div>

      
</asp:Content>

<script runat="server">
    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetAssignedTaskList() As ActionResult(Of AssignTasks)
        Dim l As New ActionResult(Of AssignTasks)
        Try
            Dim li As New AssignTasks
            li.allTasks = d_("SELECT ats.*, COALESCE(us.FirstName + COALESCE(' ' + us.LastName, ''), ats.LoginId) As FullName  FROM cc_AppraiserTaskStatus ats LEFT OUTER JOIN UserSettings us ON ats.LoginId = us.LoginId  ORDER BY us.FirstName, ats.NbhdId").ConvertToList
            li.nbhdTasks = d_("SELECT nts.*, COALESCE(us.FirstName + COALESCE(' ' + us.LastName, ''), nts.LoginId) As FullName, (nts.TotalTasks - nts.CompletedTasks) As pending  FROM cc_NbhdTaskStatus nts LEFT OUTER JOIN UserSettings us ON nts.LoginId = us.LoginId  ORDER BY us.FirstName, nts.Id").ConvertToList
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetParcelPoints(nbhdId As String, nbhdName As String) As ActionResult(Of ParcelPoints)
        Dim l As New ActionResult(Of ParcelPoints)
        Try
            Dim li As New ParcelPoints
            Dim condition As String = If(nbhdId <> -1, "n.Id = " + nbhdId, "n.Number = " + nbhdName)
            Dim alternateDataRow As DataRow = Database.Tenant.GetTopRow("SELECT Alternatekeyfieldvalue, KeyField1, ShowKeyValue1, ShowAlternateField FROM Application")
            Dim KeyValue1Name As String = GetString(alternateDataRow, "KeyField1")
            Dim alternateField As String = GetString(alternateDataRow, "Alternatekeyfieldvalue")
            Dim alternateCondition As String = If((GetString(alternateDataRow, "ShowAlternateField") = "True" AndAlso alternateField <> ""), "pd." + alternateField, "pt.KeyValue1")
            li.parcelPoints = d_("SELECT p.Id INTO #temp_atc_getPointsPId FROM Parcel p JOIN Neighborhood n ON p.NeighborhoodId = n.Id WHERE " + condition + "; SELECT p.Id as parcelId, AVG(pm.Latitude) As MidLatitude, AVG(pm.Longitude) As MidLongitude INTO #temp_atc_getMapPoints FROM #temp_atc_getPointsPId p LEFT OUTER JOIN ParcelMapPoints pm on p.Id = pm.ParcelId GROUP BY p.Id; SELECT pt.Id, " + alternateCondition + " as " + KeyValue1Name + ", pt.StreetAddress, ISNULL(pt.Priority, 0) As Priority, ISNULL(FieldAlert,  '''') AS FieldAlert, ISNULL(pt.Reviewed, 0) As Reviewed, ISNULL(pt.QC, 0) As QC, xt.MidLatitude as latitude, xt.MidLongitude as longitude, ISNULL(nb.Name, '') As NbhdName, ISNULL(nb.AssignedTo, '') As AssignedTo, " + alternateCondition + " as keyFieldValue FROM Parcel pt JOIN #temp_atc_getMapPoints xt On pt.Id = xt.parcelId JOIN ParcelData pd On pt.Id = pd.CC_ParcelId Join Neighborhood nb On pt.NeighborhoodId = nb.Id;").ConvertToList
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetParcelLookups() As ActionResult(Of ParcelLookups)
        Dim l As New ActionResult(Of ParcelLookups)
        Try
            Dim li As New ParcelLookups
            li.parcelLookups = d_("Select  Id, LookupName As Source, IdValue As Value, NameValue As Name, DescValue As Description, Ordinal, ColorCode As Color, value As NumericValue, AdditionalValue1, AdditionalValue2 FROM dbo.ParcelDataLookup").ConvertToList
            li.Nbhdlist = d_("Select Number Id, Number Name FROM Neighborhood ORDER BY Number").ConvertToList
            li.Users = d_("Select DISTINCT us.LoginId Id, Coalesce(us.FirstName + Coalesce(' ' + us.LastName, ''),  us.LoginId) As Name FROM USERSETTINGS us WHERE us.Id IS NOT NULL ORDER BY COALESCE(us.FirstName + COALESCE(' ' + us.LastName, ''),  us.LoginId)").ConvertToList
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetSearchFields() As ActionResult(Of SearchFields)
        Dim l As New ActionResult(Of SearchFields)
        Try
            Dim li As New SearchFields
            Dim keyField As String = Database.Tenant.GetStringValue("SELECT KeyField1 FROM Application")
            li.searchFields = d_("select concat (f.Name, '/', f.datatype, '/', f.SchemaDataType, '/', f.id) as value, f.* FROM DataSourceField f INNER JOIN DataSourceTable t ON f.TableId = t.Id INNER JOIN FieldCategory fc ON fc.Id = f.CategoryId WHERE t.ImportType = 0 AND f.CategoryId IS NOT NULL AND f.IsCalculated = 0 AND f.Name <> '" + keyField + "' ORDER BY ordinal").ConvertToList
            li.dtrStatus = d_("SELECT code as Id, description as Name FROM DTR_StatusType").ConvertToList
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        Return l
    End Function

    <System.Web.Services.WebMethod()>
    Function KeyList() As String
        Dim keys As New List(Of String)
        For i = 1 To 10
            If Database.Tenant.Application.KeyField(i).IsNotEmpty Then
                keys.Add(Database.Tenant.Application.KeyField(i))
            End If
        Next
        If Database.Tenant.Application.ShowKeyValue1 = False Then
            keys.Remove(Database.Tenant.Application.KeyField(1))
        End If
        If Database.Tenant.Application.ShowAlternateField = True Then
            keys.Add(Database.Tenant.Application.AlternateKeyfield)
        End If
        ''If keys.Count > 0 Then
        ''pNoCK.Visible = True
        ''Else
        ''pNoCK.Visible = False
        ''End If

        ''pHaveCK.Visible = keys.Count > 0
        ''hdnKeys.Value = keys.Count
        Return String.Join(", ", keys.ToArray.Select(Function(x) x))
    End Function

    <System.Web.Services.WebMethod()>
    Public Sub ExportCSV(sender As Object, e As EventArgs)

        Dim keys As New List(Of String)
        For i = 1 To 10
            If Database.Tenant.Application.KeyField(i).IsNotEmpty Then
                keys.Add(Database.Tenant.Application.KeyField(i))
            End If
        Next
        If Database.Tenant.Application.ShowKeyValue1 = False Then
            keys.Remove(Database.Tenant.Application.KeyField(1))
        End If
        If Database.Tenant.Application.ShowAlternateField = True Then
            keys.Add(Database.Tenant.Application.AlternateKeyfield)
        End If
        Dim dt As DataTable
        Try
            Dim keysFields As String = String.Join(",", keys.ToArray())
            Dim ca As New ClientApplication(Database.Tenant)
            Dim neighborhoodNumberField As String = ca.NeighborhoodField
            Dim dType = Database.Tenant.GetStringValue("Select Data_Type FROM information_schema.columns WHERE table_name = 'NeighborhoodData' AND column_name='" + neighborhoodNumberField + "'")
            'Dim adhocName As String =iif (dType = "int",0001,"TestAdhoc")
            Dim sqlquery As String
            If EnableNewPriorities Then
                sqlquery = "SELECT " & keysFields & ", PRIORITY = CASE WHEN RowNumber = 1 THEN 0 WHEN RowNumber = 2 THEN 1 WHEN RowNumber = 3 THEN 2 WHEN RowNumber = 4 THEN 3 WHEN RowNumber = 5 THEN 4 WHEN RowNumber = 6 THEN 5 END ,'sample field alert message' AS MESSAGE FROM (SELECT TOP 6 " & keysFields & ",ROW_NUMBER() OVER(ORDER BY RowUID ASC) AS RowNumber FROM parceldata pd inner join parcel p on p.id=pd.CC_ParcelId) a ORDER BY PRIORITY"
            Else
                sqlquery = "SELECT " & keysFields & ", PRIORITY = CASE WHEN RowNumber in (1,4) THEN 0 WHEN RowNumber in (2,5) THEN 1 WHEN RowNumber in (3,6) THEN 2 END ,'sample field alert message' AS MESSAGE FROM (SELECT TOP 6 " & keysFields & ",ROW_NUMBER() OVER(ORDER BY RowUID ASC) AS RowNumber FROM parceldata pd inner join parcel p on p.id=pd.CC_ParcelId) a ORDER BY PRIORITY"
            End If

            dt = Database.Tenant.GetDataTable(sqlquery)
        Catch ex As Exception
            Alert("An unexpected error occurred, couldn't complete the download process.")
            Return
        End Try

        Dim csv As String = String.Empty
        For Each column As DataColumn In dt.Columns
            csv += column.ColumnName + ","c
        Next
        csv = csv.Trim().Remove(csv.LastIndexOf(","))
        csv += vbCr & vbLf

        For Each row As DataRow In dt.Rows
            For Each column As DataColumn In dt.Columns
                csv += row(column.ColumnName).ToString().Replace(",", ";") + ","c
            Next
            csv = csv.Trim().Remove(csv.LastIndexOf(","))
            csv += vbCr & vbLf
        Next

        Response.Clear()
        Response.Buffer = True
        Response.AddHeader("content-disposition", "attachment;filename=SamplePriorityList.csv")
        Response.Charset = ""
        Response.ContentType = "application/text"
        Response.Output.Write(csv)
        Response.Flush()
        Response.End()
    End Sub

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function SaveSearchFilter(Name As String, FilterData As String, FId As String) As ActionResult(Of SaveFilter)
        Dim l As New ActionResult(Of SaveFilter)
        Try
            If Name <> "" AndAlso FilterData <> "" AndAlso FId = "" Then
                d_("INSERT INTO ATC_SavedFilters (FilterName, FilterData) VALUES ({0}, {1});".SqlFormat(False, Name, FilterData))
            ElseIf Name = "" AndAlso FilterData <> "" AndAlso FId <> "" Then
                d_("UPDATE ATC_SavedFilters SET FilterData = {0} WHERE Id = {1};".SqlFormat(False, FilterData, FId))
            End If

            Dim li As New SaveFilter
            li.SaveFilter = d_("SELECT * FROM ATC_SavedFilters ORDER BY Id").ConvertToList
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function DeleteSearchFilter(DeleteId As String) As ActionResult(Of DeleteFilter)
        Dim l As New ActionResult(Of DeleteFilter)
        Try
            Dim delCondition = " WHERE Id = " + DeleteId
            If DeleteId <> "" AndAlso DeleteId <> "-1" Then
                d_("DELETE FROM ATC_SavedFilters" + delCondition)
            ElseIf DeleteId = "-1" Then
                d_("DELETE FROM ATC_SavedFilters")
            End If

            Dim li As New DeleteFilter
            li.DeleteFilter = d_("SELECT * FROM ATC_SavedFilters ORDER BY Id").ConvertToList
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function UpdateSearchFilter(FilterId As String, Name As String, FilterData As String) As ActionResult(Of UpdateFilter)
        Dim l As New ActionResult(Of UpdateFilter)
        Try
            If FilterData <> "" Then
                d_("UPDATE ATC_SavedFilters SET FilterName = {0}, FilterData = {1} WHERE Id = {2};".SqlFormat(False, Name, FilterData, FilterId))
            Else
                d_("UPDATE ATC_SavedFilters SET FilterName = {0} WHERE Id = {1};".SqlFormat(False, Name, FilterId))
            End If

            Dim li As New UpdateFilter
            li.UpdateFilter = d_("SELECT * FROM ATC_SavedFilters ORDER BY Id").ConvertToList
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetParcelPointsFromParcelList(parcels As String, Lines As String, newKey As String) As ActionResult(Of PriorityParcelPoints)
        Dim l As New ActionResult(Of PriorityParcelPoints)
        Try
            Dim li As New PriorityParcelPoints
            Dim pattern As String = "^\s*""?|""?\s*$"
            Dim rgx As New Regex(pattern)
            Dim rgx1 As New Regex(",(?=(?:[^""]*""[^""]*"")*[^""]*$)")
            Dim rgx2 As New Regex("\n(?=(?:[^""]*""[^""]*"")*[^""]*$)")
            Dim Fields As String()
            Dim header As String()
            Dim Les As String() = rgx2.Split(Lines).Where(Function(x) Not String.IsNullOrWhiteSpace(x)).ToArray()
            Dim ignoredPcount As Integer = 0
            header = rgx1.Split(Les(0)).Select(Function(x) x.ToUpper()).ToArray()

            d_("DELETE FROM ATC_PriorityBackup WHERE BackUpDate < DATEADD(day, -2, GETDATE())")

            For i As Integer = 1 To Les.GetLength(0) - 1
                If Les(i).Trim.Length = 0 Then
                    Continue For
                End If
                Fields = rgx1.Split(Les(i))

                If Fields.Length < header.Length Then
                    Throw New Exception("Your list could not be uploaded because " + header(Fields.Length).ToString() + " column is missing in the " + IIf(i > 3, i.ToString() + "th", IIf(i > 1, IIf(i = 2, i.ToString() + "nd", i.ToString() + "rd"), i.ToString() + "st")) + " row in the list Or an invalid line break is present in the list.")
                End If
                If Fields.Length > header.Length Then ' field and header length should same
                    Throw New Exception("Your list could not be uploaded because data exists without header part.")
                End If
                Dim priority As Integer = 0
                Dim message As String = ""
                Dim keyFieldName As String = ""
                Dim keyfieldValue As String = ""
                For j As Integer = 0 To Fields.Length - 1
                    If header(j) = "PRIORITY" Then
                        priority = rgx.Replace(Fields(j), "")
                    ElseIf header(j) = "MESSAGE" Then
                        message = rgx.Replace(Fields(j), "")
                    ElseIf keyFieldName = "" Then
                        keyFieldName = header(j)
                        keyfieldValue = rgx.Replace(Fields(j), "")
                        If (keyfieldValue IsNot Nothing AndAlso keyfieldValue <> "" AndAlso keyfieldValue.ToString().StartsWith("'")) Then
                            Dim keyfieldcopy = keyfieldValue.ToString().Remove(0, 1)
                            If (IsNumeric(keyfieldcopy)) AndAlso (((keyfieldcopy.ToString().Substring(0, 1) = "0") AndAlso (keyfieldcopy.ToString().Length > 1)) Or (keyfieldcopy.ToString().Length > 15)) Then
                                keyfieldValue = keyfieldcopy
                            End If
                        End If
                    End If
                Next
                Dim pId As Integer = Database.Tenant.GetIntegerValue("SELECT CC_ParcelId FROM ParcelData WHERE " + keyFieldName + " = '" + keyfieldValue + "'")
                If pId <> 0 Then
                    d_("INSERT INTO ATC_PriorityBackup (ParcelId, PriorityId, Priority, PriorityMessage) VALUES ({0}, {1}, {2}, {3});".SqlFormat(False, pId, newKey, priority, message))
                Else
                    ignoredPcount = ignoredPcount + 1
                End If
            Next

            Dim alternateDataRow As DataRow = Database.Tenant.GetTopRow("SELECT Alternatekeyfieldvalue, KeyField1, ShowKeyValue1, ShowAlternateField FROM Application")
            Dim KeyValue1Name As String = GetString(alternateDataRow, "KeyField1")
            Dim alternateField As String = GetString(alternateDataRow, "Alternatekeyfieldvalue")
            Dim alternateCondition As String = If((GetString(alternateDataRow, "ShowAlternateField") = "True" AndAlso alternateField <> ""), "pd." + alternateField, "pt.KeyValue1")
            Dim query As String = "SELECT p.*, a.Priority as NewPriority, a.PriorityMessage INTO #temp_atc_priority_parcel FROM Parcel p JOIN (SELECT ParcelId, PriorityId, Priority, PriorityMessage FROM ATC_PriorityBackup WHERE PriorityId = " + newKey + ") a ON p.Id = a.ParcelId; SELECT p.Id as parcelId, AVG(pm.Latitude) As MidLatitude, AVG(pm.Longitude) As MidLongitude INTO #temp_atc_priority_parcel_mps FROM #temp_atc_priority_parcel p LEFT OUTER JOIN ParcelMapPoints pm on p.Id = pm.ParcelId GROUP BY p.Id; SELECT pt.Id, " + alternateCondition + " as " + KeyValue1Name + ", pt.StreetAddress, ISNULL(pt.NewPriority, 0) As Priority, ISNULL(FieldAlert,  '''') AS FieldAlert, ISNULL(pt.Reviewed, 0) As Reviewed, ISNULL(pt.QC, 0) As QC, xt.MidLatitude as latitude, pt.StreetAddress, xt.MidLongitude as longitude, pt.PriorityMessage As MESSAGE, ISNULL(n.Name, '') As NbhdName, ISNULL(n.AssignedTo, '') As AssignedTo, " + alternateCondition + " as keyFieldValue FROM #temp_atc_priority_parcel pt JOIN #temp_atc_priority_parcel_mps xt on pt.Id = xt.parcelId JOIN ParcelData pd on pt.Id = pd.CC_ParcelId LEFT OUTER JOIN Neighborhood n on pt.NeighborhoodId = n.Id;"
            li.parcelPoints = d_(query).ConvertToList
            li.IgnoredParcelCount = ignoredPcount
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetSearchResults(Type As String, SearchQuery As String) As ActionResult(Of SearchLists)
        Dim l As New ActionResult(Of SearchLists)
        Try
            Dim li As New SearchLists
            Dim ds As DataSet = Database.Tenant.GetDataSet(String.Format("EXEC atc_SearchParcels @Type = {0}, @Query = {1} ", Type.ToSqlValue(True), SearchQuery.ToSqlValue(True)))
            li.SearchLists = ds.Tables(0).ConvertToList
            li.DownloadLists = ds.Tables(1).ConvertToList
            li.CountLists = ds.Tables(2).ConvertToList
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function SaveSearchResults(Name As String, UserName As String, ParcelCount As Integer, FilterCount As Integer, ResultId As String, SearchQuery As String) As ActionResult(Of SearchLists)
        Dim l As New ActionResult(Of SearchLists)
        Try
            Dim li As New SearchLists
            If Name <> "" AndAlso ResultId = "" Then
                Dim newId As Integer = Database.Tenant.GetIntegerValue("INSERT INTO ATC_SavedResults (ResultName, UserName, ParcelCount, FilterCount) VALUES ({0}, {1}, {2}, {3}); SELECT CAST(@@IDENTITY  AS INT);".SqlFormat(False, Name, UserName, ParcelCount, FilterCount))
                If newId > 0 Then
                    Dim Type As String = "Saved Result"
                    d_(String.Format("EXEC atc_SearchParcels @Type = {0}, @Query = {1}, @ResultId = {2} ", Type.ToSqlValue(True), SearchQuery.ToSqlValue(True), newId))
                End If
            ElseIf Name <> "" AndAlso ResultId <> "" Then
                d_("UPDATE ATC_SavedResults SET ResultName = {0} WHERE Id = {1};".SqlFormat(False, Name, ResultId))
            ElseIf Name = "" AndAlso ResultId <> "" Then
                If ResultId = "-1" Then
                    d_("DELETE FROM ATC_SavedResults; DELETE FROM ATC_SavedParcelResults;")
                Else
                    Dim delCondition = " WHERE Id = " + ResultId
                    d_("DELETE FROM ATC_SavedResults" + delCondition + "; DELETE FROM ATC_SavedParcelResults WHERE SavedResultId = " + ResultId + ";")
                End If
            End If

            li.SearchLists = d_("select Id, ResultName, CAST(CAST(dbo.GetLocalDate(ResultDate) AS date) AS varchar(100)) as ResultDate, UserName, ParcelCount, FilterCount FROM ATC_SavedResults ORDER BY Id").ConvertToList
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function ReturnSavedResults(ResultId As String) As ActionResult(Of SearchLists)
        Dim l As New ActionResult(Of SearchLists)
        Try
            Dim li As New SearchLists
            Dim condition As String = "a.SavedResultId = " + ResultId
            Dim alternateDataRow As DataRow = Database.Tenant.GetTopRow("SELECT Alternatekeyfieldvalue, KeyField1, ShowKeyValue1, ShowAlternateField FROM Application")
            Dim KeyValue1Name As String = GetString(alternateDataRow, "KeyField1")
            Dim alternateField As String = GetString(alternateDataRow, "Alternatekeyfieldvalue")
            Dim alternateCondition As String = If((GetString(alternateDataRow, "ShowAlternateField") = "True" AndAlso alternateField <> ""), "pd." + alternateField, "pt.KeyValue1")
            li.SearchLists = d_("SELECT p.Id INTO #temp_atc_results_parcels_pid FROM Parcel p Join ATC_SavedParcelResults a on p.Id = a.ParcelId WHERE " + condition + "; SELECT p.Id as parcelId, AVG(pm.Latitude) As MidLatitude, AVG(pm.Longitude) As MidLongitude INTO #temp_atc_results_parcels FROM #temp_atc_results_parcels_pid p LEFT OUTER JOIN ParcelMapPoints pm on p.Id = pm.ParcelId GROUP BY p.Id; SELECT pt.Id, " + alternateCondition + " as " + KeyValue1Name + ", pt.StreetAddress, ISNULL(pt.Priority, 0) As Priority, ISNULL(FieldAlert,  '''') AS FieldAlert, ISNULL(pt.Reviewed, 0) As Reviewed, ISNULL(pt.QC, 0) As QC, xt.MidLatitude as latitude, xt.MidLongitude as longitude, ISNULL(n.Name, '') As NbhdName, ISNULL(n.AssignedTo, '') As AssignedTo, " + alternateCondition + " as keyFieldValue FROM Parcel pt JOIN #temp_atc_results_parcels xt on pt.Id = xt.parcelId JOIN ParcelData pd on pt.Id = pd.CC_ParcelId LEFT OUTER JOIN Neighborhood n on pt.NeighborhoodId = n.Id;").ConvertToList
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function AssignmentGroupUserStatus() As ActionResult(Of List(Of AssignmentGroupUser))
        Dim l As New ActionResult(Of List(Of AssignmentGroupUser))
        Try
            Dim ApplicationName As String = System.Web.Security.Membership.Provider.ApplicationName
            Dim query As String = "EXEC sp_GetAllUsersWithSpecificRole @ApplicationName = {0}, @OrganizationID = {1}, @Role = {2}".SqlFormatString(ApplicationName, HttpContext.Current.GetCAMASession.OrganizationId.ToString, "MobileAssessor")
            Dim dt As DataTable = Database.System.GetDataTable(query)
            l.Data = dt.ConvertToList(Of AssignmentGroupUser)
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function AssignmentGroupStatus() As ActionResult(Of AssignmentGroupList)
        Dim l As New ActionResult(Of AssignmentGroupList)
        Try
            Dim li As New AssignmentGroupList
            d_("EXEC [cc_UpdateNeighborhoodStatistics] NULL, 0")
            Dim OrderBy As String = "CASE WHEN IsNumeric(Name) <> 1 THEN Name ELSE NULL END ASC , CASE WHEN IsNumeric(Name) = 1 THEN CAST(Name as FLOAT) ELSE NULL END ASC"
            li.AssignmentGroupList = d_("SELECT FullName = ISNULL((CASE COALESCE(us.FirstName, '') + COALESCE(' ' + us.LastName, '') WHEN '' THEN nbh.AssignedTo ELSE COALESCE(us.FirstName, '') + COALESCE(' ' + us.LastName, '') END), '') , ISNULL(CONVERT(varchar, nbh.LastVisitedDate, 10), '') AS LastVisitedTime, nbh.* FROM Neighborhood nbh LEFT JOIN UserSettings us ON nbh.AssignedTo = us.LoginId ORDER BY " + OrderBy).ConvertToList
            li.FilterUser = d_("SELECT AssignedTo, CASE WHEN FirstName IS NULL AND lastname IS NULL THEN AssignedTo ELSE COALESCE(FirstName, '') + COALESCE(' ' + LastName, '') END AS FullName FROM (SELECT DISTINCT Loginid, firstname, lastname, nbh.AssignedTo FROM UserSettings u RIGHT OUTER JOIN Neighborhood nbh ON nbh.AssignedTo = u.LoginId) rslt WHERE rslt.AssignedTo IS NOT NULL ORDER BY FullName").ConvertToList
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function UnAssignGroups(GroupIds As String) As ActionResult(Of AssignmentGroupList)
        Dim l As New ActionResult(Of AssignmentGroupList)
        Try
            Dim li As New AssignmentGroupList
            Dim neighborhoodsArray As String() = GroupIds.Split(New Char() {","c})
            Dim txtNeighborhoodName As String = CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName()
            Dim neighborhoodNameList As String = ""
            For Each nbh As String In neighborhoodsArray
                Dim NeighborhoodId As String = nbh
                Dim neighborhoodName = Database.Tenant.GetStringValue("SELECT name FROM Neighborhood WHERE id= " & NeighborhoodId)
                If (NeighborhoodId <> Nothing And neighborhoodName <> "") Then
                    Database.Tenant.Execute("EXEC cc_UnassignNbhd " + NeighborhoodId)
                    neighborhoodNameList += neighborhoodName + ","
                End If
            Next

            neighborhoodNameList = neighborhoodNameList.TrimEnd(","c)
            NotificationMailer.SendNotification(Membership.GetUser().ToString, txtNeighborhoodName + " Unassigned", txtNeighborhoodName + " " + neighborhoodNameList + " have been unassigned.")

            d_("EXEC [cc_UpdateNeighborhoodStatistics] NULL, 0")
            Dim OrderBy As String = "CASE WHEN IsNumeric(Name) <> 1 THEN Name ELSE NULL END ASC , CASE WHEN IsNumeric(Name) = 1 THEN CAST(Name as FLOAT) ELSE NULL END ASC"
            li.AssignmentGroupList = d_("SELECT FullName = ISNULL((CASE COALESCE(us.FirstName, '') + COALESCE(' ' + us.LastName, '') WHEN '' THEN nbh.AssignedTo ELSE COALESCE(us.FirstName, '') + COALESCE(' ' + us.LastName, '') END), '') , ISNULL(CONVERT(varchar, nbh.LastVisitedDate, 10), '') AS LastVisitedTime, nbh.* FROM Neighborhood nbh LEFT JOIN UserSettings us ON nbh.AssignedTo = us.LoginId ORDER BY " + OrderBy).ConvertToList
            li.FilterUser = d_("SELECT AssignedTo, CASE WHEN FirstName IS NULL AND lastname IS NULL THEN AssignedTo ELSE COALESCE(FirstName, '') + COALESCE(' ' + LastName, '') END AS FullName FROM (SELECT DISTINCT Loginid, firstname, lastname, nbh.AssignedTo FROM UserSettings u RIGHT OUTER JOIN Neighborhood nbh ON nbh.AssignedTo = u.LoginId) rslt WHERE rslt.AssignedTo IS NOT NULL ORDER BY FullName").ConvertToList
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function AssignGroups(GroupIds As String, User As String) As ActionResult(Of AssignmentGroupList)
        Dim l As New ActionResult(Of AssignmentGroupList)
        Try
            Dim li As New AssignmentGroupList
            Dim SelectedUser As String = User
            Dim neighborhoodsArray As String() = GroupIds.Split(New Char() {","c})
            Dim txtNeighborhoodName As String = CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName()
            Dim neighborhoodNameList As String = ""
            For Each nbh As String In neighborhoodsArray
                Dim NeighborhoodId As String = nbh.Split("$")(0)
                Dim neighborhoodName = Database.Tenant.GetStringValue("SELECT COALESCE(NULLIF(Name,''),Number) FROM Neighborhood WHERE id= " & NeighborhoodId)
                If (NeighborhoodId <> Nothing And neighborhoodName <> "" And SelectedUser <> "") Then
                    Database.Tenant.Execute("EXEC cc_AssignNbhdToUser " + SelectedUser.ToSqlValue + ", " + NeighborhoodId)
                    neighborhoodNameList += neighborhoodName + ","
                End If
            Next

            neighborhoodNameList = neighborhoodNameList.TrimEnd(","c)
            NotificationMailer.SendNotification(SelectedUser, txtNeighborhoodName + " assigned", txtNeighborhoodName + " " + neighborhoodNameList + " assigned to you.")

            d_("EXEC [cc_UpdateNeighborhoodStatistics] NULL, 0")
            Dim OrderBy As String = "CASE WHEN IsNumeric(Name) <> 1 THEN Name ELSE NULL END ASC , CASE WHEN IsNumeric(Name) = 1 THEN CAST(Name as FLOAT) ELSE NULL END ASC"
            li.AssignmentGroupList = d_("SELECT FullName = ISNULL((CASE COALESCE(us.FirstName, '') + COALESCE(' ' + us.LastName, '') WHEN '' THEN nbh.AssignedTo ELSE COALESCE(us.FirstName, '') + COALESCE(' ' + us.LastName, '') END), '') , ISNULL(CONVERT(varchar, nbh.LastVisitedDate, 10), '') AS LastVisitedTime, nbh.* FROM Neighborhood nbh LEFT JOIN UserSettings us ON nbh.AssignedTo = us.LoginId ORDER BY " + OrderBy).ConvertToList
            li.FilterUser = d_("SELECT AssignedTo, CASE WHEN FirstName IS NULL AND lastname IS NULL THEN AssignedTo ELSE COALESCE(FirstName, '') + COALESCE(' ' + LastName, '') END AS FullName FROM (SELECT DISTINCT Loginid, firstname, lastname, nbh.AssignedTo FROM UserSettings u RIGHT OUTER JOIN Neighborhood nbh ON nbh.AssignedTo = u.LoginId) rslt WHERE rslt.AssignedTo IS NOT NULL ORDER BY FullName").ConvertToList
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function AdhocUser() As Object
        Dim uc = Membership.GetAllUsers
        Dim users(uc.Count - 1) As MembershipUser
        Membership.GetAllUsers().CopyTo(users, 0)
        Dim fielduser = users.Where(Function(x) Roles.IsUserInRole(x.UserName, "MobileAssessor"))
        Dim dataUsers As New DataTable()
        dataUsers.Columns.Add("UserName")

        For Each u As MembershipUser In fielduser
            Dim dr As DataRow = dataUsers.NewRow
            dr("UserName") = u.UserName
            dataUsers.Rows.Add(dr)
        Next

        Dim s As Enumerable
        Dim userFromSettings As DataTable = Database.Tenant.GetDataTable("SELECT FirstName,LastName,LoginId FROM UserSettings")
        Dim query = From A In userFromSettings.AsEnumerable
                    Join B In dataUsers.AsEnumerable On
              A.Field(Of String)("LoginId") Equals B.Field(Of String)("UserName")
                    Order By If((A.Field(Of String)("FirstName") + " " + A.Field(Of String)("LastName")), B.Field(Of String)("UserName"))
                    Select New With {
              .LoginId = B.Field(Of String)("UserName"),
              .UserName = If((A.Field(Of String)("FirstName") + " " + A.Field(Of String)("LastName")), B.Field(Of String)("UserName"))
        }

        Return query
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function CreatePriority(Parcels As String, Priority As String, PriorityId As String) As String
        Try
            Dim ds As DataSet
            Dim alternateDataRow As DataRow = Database.Tenant.GetTopRow("SELECT Alternatekeyfieldvalue, KeyField1, ShowKeyValue1, ShowAlternateField FROM Application")
            Dim alternateField As String = GetString(alternateDataRow, "Alternatekeyfieldvalue")
            Dim alternateCondition As String = If((GetString(alternateDataRow, "ShowAlternateField") = "True" AndAlso alternateField <> ""), "pd." + alternateField, "")
            If PriorityId = "" Then
                If alternateCondition <> "" Then
                    ds = Database.Tenant.GetDataSet("SELECT p.Id, p.KeyValue1, p.KeyValue2, p.KeyValue3, p.KeyValue4, p.KeyValue5, p.KeyValue6, p.KeyValue7, p.KeyValue8, p.KeyValue9, p.KeyValue10, p.Priority, p.ParcelAlert, " + alternateCondition + " AS Alternatekeyfieldvalue FROM Parcel p JOIN ParcelData pd ON p.Id = pd.CC_ParcelId WHERE p.Id IN (" + Parcels + ") ORDER BY p.Id")
                Else
                    ds = Database.Tenant.GetDataSet("SELECT Id, KeyValue1, KeyValue2, KeyValue3, KeyValue4, KeyValue5, KeyValue6, KeyValue7, KeyValue8, KeyValue9, KeyValue10, Priority, ParcelAlert FROM Parcel WHERE Id IN (" + Parcels + ") ORDER BY Id")
                End If
            Else
                If alternateCondition <> "" Then
                    ds = Database.Tenant.GetDataSet("SELECT * INTO #temp_atc_priority_parcelList FROM Parcel WHERE Id IN (" + Parcels + "); SELECT * INTO #temp_atc_Priority_Backup FROM ATC_PriorityBackup WHERE PriorityId = " + PriorityId + " ; SELECT p.Id, p.KeyValue1, p.KeyValue2, p.KeyValue3, p.KeyValue4, p.KeyValue5, p.KeyValue6, p.KeyValue7, p.KeyValue8, p.KeyValue9, p.KeyValue10, t.Priority, t.PriorityMessage As ParcelAlert, " + alternateCondition + " AS Alternatekeyfieldvalue FROM #temp_atc_priority_parcelList p JOIN ParcelData pd ON p.Id = pd.CC_ParcelId JOIN #temp_atc_Priority_Backup t ON p.Id = t.ParcelId ORDER BY p.Id;")
                Else
                    ds = Database.Tenant.GetDataSet("SELECT * INTO #temp_atc_Priority_Backup FROM ATC_PriorityBackup WHERE PriorityId = " + PriorityId + " ; SELECT p.Id, p.KeyValue1, p.KeyValue2, p.KeyValue3, p.KeyValue4, p.KeyValue5, p.KeyValue6, p.KeyValue7, p.KeyValue8, p.KeyValue9, p.KeyValue10, t.Priority, t.PriorityMessage As ParcelAlert FROM Parcel p JOIN #temp_atc_Priority_Backup t ON p.Id = t.ParcelId WHERE p.Id IN (" + Parcels + ") ORDER BY p.Id;")
                End If
            End If

            Dim keyValues As New List(Of String)
            Dim keyfields As New List(Of String)

            For i = 1 To 10
                If Database.Tenant.Application.KeyField(i).IsNotEmpty Then
                    keyValues.Add("KEYVALUE" + i.ToString())
                End If
            Next

            If Database.Tenant.Application.ShowKeyValue1 = False Then
                keyValues.Remove("KEYVALUE1")
            End If
            If Database.Tenant.Application.ShowAlternateField = True Then
                keyValues.Add("Alternatekeyfieldvalue")
            End If

            keyValues.Add("PRIORITY")
            keyValues.Add("ParcelAlert")

            Dim dt As DataTable = New System.Data.DataView(ds.Tables(0)).ToTable(False, keyValues.ToArray())

            For Each col As DataColumn In dt.Columns
                If col.ColumnName.ToString() <> "Alternatekeyfieldvalue" Then
                    col.ColumnName = col.ColumnName.ToString().ToUpper()
                End If
            Next

            dt.Columns("ParcelAlert").ColumnName = "MESSAGE"
            Database.Tenant.LoadTableFromDataTable("PriorityListItems", dt, 1)
            If Priority <> "" Then
                Database.Tenant.Execute("UPDATE [dbo].[PriorityListItems] SET Priority = {0} WHERE PriorityListId = {1}".SqlFormatString(Priority, HttpContext.Current.Session("ptid")))
            End If
            Return "Success~" + HttpContext.Current.Session("ptid").ToString()
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function



    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function UpdateAtcPriority(Parcels As String, PriorityId As String) As ActionResult(Of SearchLists)
        Dim l As New ActionResult(Of SearchLists)
        Try
            If PriorityId <> "" Then
                d_("UPDATE a SET a.Priority = p.Priority FROM ATC_PriorityBackup a JOIN Parcel p ON a.ParcelId = p.Id WHERE a.ParcelId IN (" + Parcels + ") AND a.PriorityId = {0} ".SqlFormatString(PriorityId))
            End If
            Dim li As New SearchLists
            Dim alternateDataRow As DataRow = Database.Tenant.GetTopRow("SELECT Alternatekeyfieldvalue, KeyField1, ShowKeyValue1, ShowAlternateField FROM Application")
            Dim KeyValue1Name As String = GetString(alternateDataRow, "KeyField1")
            Dim alternateField As String = GetString(alternateDataRow, "Alternatekeyfieldvalue")
            Dim alternateCondition As String = If((GetString(alternateDataRow, "ShowAlternateField") = "True" AndAlso alternateField <> ""), "pd." + alternateField, "pt.KeyValue1")
            Dim query As String = "SELECT * INTO #temp_atc_upriority_parcel FROM Parcel WHERE Id IN (" + Parcels + "); SELECT p.Id as parcelId, AVG(pm.Latitude) As MidLatitude, AVG(pm.Longitude) As MidLongitude INTO #temp_atc_upriority_parcel_mps FROM #temp_atc_upriority_parcel p LEFT OUTER JOIN ParcelMapPoints pm on p.Id = pm.ParcelId GROUP BY p.Id; SELECT pt.Id, " + alternateCondition + " as " + KeyValue1Name + ", pt.StreetAddress, ISNULL(pt.Priority, 0) As Priority, ISNULL(FieldAlert,  '''') AS FieldAlert, ISNULL(pt.Reviewed, 0) As Reviewed, ISNULL(pt.QC, 0) As QC, xt.MidLatitude as latitude, xt.MidLongitude as longitude, ISNULL(n.Name, '') As NbhdName, ISNULL(n.AssignedTo, '') As AssignedTo, " + alternateCondition + " as keyFieldValue FROM #temp_atc_upriority_parcel pt JOIN #temp_atc_upriority_parcel_mps xt on pt.Id = xt.parcelId JOIN ParcelData pd on pt.Id = pd.CC_ParcelId LEFT OUTER JOIN Neighborhood n on pt.NeighborhoodId = n.Id;"
            li.SearchLists = d_(query).ConvertToList
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function AssignParcelToGroup(parcelId As String, GroupId As String, User As String, GroupName As String) As String
        Try
            d_("UPDATE Parcel  SET Reviewed = 0, ReviewedBy = NULL,  QCDate = NULL, QC = NULL, QCBy = NULL, ReviewDate = NULL, Mps = 1, Committed = 0, FailedOnDownSync = 0, FailedOnDownSyncStatus = 0, DownSyncRejectReason = NULL, NeighborhoodId = " + GroupId + " WHERE Id = " + parcelId)
            d_("INSERT INTO ParcelAuditTrail (ParcelId, LoginID, EventType, Description,ApplicationType) VALUES ({0}, {1}, {2}, {3},'Desktop');".SqlFormat(False, parcelId, User, 0, "Parcel moved to group " + GroupName + " via ATC."))
            d_("EXEC [cc_UpdateNeighborhoodStatistics] NULL, 0")
            Return "Success"
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetUserDetails() As ActionResult(Of UserList)
        Dim l As New ActionResult(Of UserList)
        Try
            Dim li As New UserList
            li.UserList = d_("SELECT LoginId, CASE COALESCE(FirstName, '') + COALESCE(' ' + MiddleName, '')  + COALESCE(' ' + LastName, '') WHEN '' THEN LoginId ELSE COALESCE(FirstName, '') + COALESCE(' ' +MiddleName, '') + COALESCE(' ' + LastName, '') End UserName FROM UserSettings;").ConvertToList
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function UserActionDates(LoginId As String) As Object
        Try
            Dim MaLoginDate As String = Database.Tenant.GetStringValue("SELECT CAST(MAX(EventDate) AS varchar(100)) FROM UserAuditTrail WHERE LoginId = '" + LoginId + "' AND Description = 'User logged in from mobile.';")
            Dim QCAccessDate As String = Database.Tenant.GetStringValue("SELECT CAST(MAX(EventDate) AS varchar(100)) FROM UserAuditTrail WHERE LoginId = '" + LoginId + "' AND Description = 'User opened QC Module.';")
            Dim DownloadDate As String = Database.Tenant.GetStringValue("SELECT CAST(MAX(EventDate) AS varchar(100)) FROM UserAuditTrail WHERE LoginId = '" + LoginId + "' AND (Description like '%User downloaded all data -%' OR Description like '%User downloaded CAMA data for neighborhood -%' OR Description like '%User downloaded system data.%');")
            Dim DataSyncDate As String = Database.Tenant.GetStringValue("SELECT CAST(MAX(EventDate) AS varchar(100)) FROM UserAuditTrail WHERE LoginId = '" + LoginId + "' AND Description like '%Uploaded%mobile changes.%';")
            Dim resp As Object = New With {.MaLoginDate = MaLoginDate, .QCAccessDate = QCAccessDate, .DownloadDate = DownloadDate, .DataSyncDate = DataSyncDate, .Success = True}
            Return resp
        Catch ex As Exception
            Dim resp As Object = New With {.ErroMessage = ex.Message, .Success = False}
            Return resp
        End Try
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetDashboardData() As ActionResult(Of DashboardData)
        Dim l As New ActionResult(Of DashboardData)
        Try
            Dim li As New DashboardData
            li.RecentGroups = d_("SELECT Number, CONCAT(us.FirstName,' ',us.LastName) AS AssignedTo FROM Neighborhood nd JOIN UserSettings us ON us.LoginId = nd.AssignedTo WHERE CAST(dbo.GetLocalDate(LastVisitedDate) AS DATE) >= DATEADD(DAY,-6,CAST(dbo.GetLocalDate(GETUTCDATE()) AS DATE))").ConvertToList
            li.RecentGroupsToday = d_("SELECT Number, CONCAT(us.FirstName,' ',us.LastName) AS AssignedTo FROM Neighborhood nd JOIN UserSettings us ON us.LoginId = nd.AssignedTo WHERE CAST(dbo.GetLocalDate(LastVisitedDate) AS DATE) >= CAST(dbo.GetLocalDate(GETUTCDATE()) AS DATE)").ConvertToList
            li.TaskCount = d_("SELECT SUM(TotalParcels) AS TotalParcels,SUM(TotalAlerts) AS TotalAlerts,SUM(TotalPriorities) AS TotalPriorities,SUM(TotalParcels)-SUM(VisitedParcels) AS PendingParcels, SUM(TotalAlerts)-SUM(VisitedAlerts) AS PendingAlerts, SUM(TotalPriorities)-SUM(VisitedPriorities) AS PendingPriorities FROM Neighborhood WHERE Completed = 0").ConvertToList
            li.GroupCount = d_("SELECT COUNT(CASE WHEN Completed = 1 THEN 1 END) AS Completed, COUNT(*) AS Total FROM Neighborhood").ConvertToList
            li.ParcelCount = d_("SELECT COUNT(CASE WHEN Reviewed = 1 THEN 1 END) AS Completed, COUNT(*) AS Total FROM Parcel").ConvertToList
            li.WeeklyTask = d_("SELECT CAST(ReviewDate AS DATE) AS Date,COUNT(*) AS Completed FROM Parcel WHERE Reviewed = 1 AND ReviewDate > DATEADD(DAY,-6,CAST(dbo.GetLocalDate(GETUTCDATE()) AS DATE)) GROUP BY CAST(ReviewDate AS DATE) ORDER BY CAST(ReviewDate AS DATE) DESC").ConvertToList
            Dim dTable As DataTable = Database.System.GetDataTable("EXEC sp_GetAllUsersWithSpecificRole @ApplicationName = {0}, @OrganizationID = {1}, @Role = {2}".SqlFormatString(System.Web.Security.Membership.Provider.ApplicationName, HttpContext.Current.GetCAMASession.OrganizationId.ToString, "MobileAssessor"))
            li.TotalCount = d_("SELECT COUNT(*) AS ParcelCount,COUNT(CASE WHEN Priority <> 0 THEN 1 END) AS PriorityCount, (SELECT COUNT(*) FROM Neighborhood) AS GroupCount, " + dTable.Rows.Count.ToString() + " AS UserCount FROM Parcel WHERE Id <> -99").ConvertToList
            l.Data = li
            l.Success = True
        Catch ex As Exception
            l.ErrorMessage = ex.Message
            l.Success = False
        End Try
        Return l
    End Function

    Public Class AssignTasks
        Public Property allTasks As JSONTable
        Public Property nbhdTasks As JSONTable
    End Class

    Public Class ParcelPoints
        Public Property parcelPoints As JSONTable
    End Class

    Public Class PriorityParcelPoints
        Public Property parcelPoints As JSONTable
        Public Property IgnoredParcelCount As Integer
    End Class

    Public Class ParcelLookups
        Public Property parcelLookups As JSONTable
        Public Property Nbhdlist As JSONTable
        Public Property Users As JSONTable
    End Class

    Public Class SearchFields
        Public Property searchFields As JSONTable
        Public Property dtrStatus As JSONTable
    End Class

    Public Class SaveFilter
        Public Property SaveFilter As JSONTable
    End Class

    Public Class DeleteFilter
        Public Property DeleteFilter As JSONTable
    End Class

    Public Class UpdateFilter
        Public Property UpdateFilter As JSONTable
    End Class

    Public Class SearchLists
        Public Property SearchLists As JSONTable
        Public Property DownloadLists As JSONTable
        Public Property CountLists As JSONTable
    End Class

    Public Class AssignmentGroupUser
        Public Property LoginId As String
        Public Property UserName As String
    End Class

    Public Class AssignmentGroupList
        Public Property AssignmentGroupList As JSONTable
        Public Property FilterUser As JSONTable
    End Class

    Public Class UserList
        Public Property UserList As JSONTable
    End Class

    Public Class DashboardData
        Public Property RecentGroups As JSONTable
        Public Property RecentGroupsToday As JSONTable
        Public Property TaskCount As JSONTable
        Public Property GroupCount As JSONTable
        Public Property ParcelCount As JSONTable
        Public Property WeeklyTask As JSONTable
        Public Property TotalCount As JSONTable
        Public Property UserCount As JSONTable
    End Class
</script>