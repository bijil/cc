﻿
Partial Class protected_tools_upload_photo
    Inherits System.Web.UI.Page

Protected Sub btnUpload_Click(sender As Object, e As System.EventArgs) Handles btnUpload.Click
        Dim validFileTypes As String() = {"jpeg", "jpg", "png"}
        Dim ext As String = System.IO.Path.GetExtension(file.PostedFile.FileName)
        Dim isValidFile As Boolean = False
        Dim uName As String =Database.System.GetStringValue("SELECT au.UserName FROM  aspnet_Users au INNER JOIN aspnet_Applications a On a.ApplicationId=au.ApplicationId WHERE  a.ApplicationName='" & Membership.ApplicationName & "' And au.UserName='" &  UserName & "'")
		For i As Integer = 0 To validFileTypes.Length - 1
            If ext.ToLower() = "." & validFileTypes(i) Then
                isValidFile = True
                Exit For
            End If
        Next
        If Not isValidFile Then
            Alert("Invalid File. Please upload a file with extension: " & _
            String.Join(" , ", validFileTypes))
            
        Else
			Dim application = 0
			Dim AppType = hIsDTR.Value
			Dim pid As String = ParcelId.Value.ToString()
            Try
	            Dim keyValue As String = Database.Tenant.GetStringValue("SELECT KeyValue1 FROM Parcel WHERE Id = " & pid)
	            Dim s3Path As String = HttpContext.Current.GetCAMASession.TenantKey + "/pc/"
	
	            Dim uploadedFileName As String = file.FileName
	
	            Dim fileName As String = pid & Now.ToString("-yyyyMMdd-HHmmss") & "." & IO.Path.GetExtension(uploadedFileName).TrimStart(".")
	            For Each count In New Integer() {2, 3, 3, 3}
	                If keyValue.Length > 0 Then
	                    Dim part As String = keyValue.Substring(0, Math.Min(count, keyValue.Length))
	                    keyValue = keyValue.Substring(part.Length)
						If part(part.Length - 1) = "." Or part(part.Length - 1) = " " Or part(part.Length - 1) = "-" Then
							part = part.Remove(part.Length - 1, 1)
						End If
	                    s3Path += part + "/"
	                End If
	            Next
	            s3Path += fileName
	
	            Dim s3 As New S3FileManager()
	            s3.UploadFile(file.FileContent, s3Path)
				'Dim sql = "EXEC cc_Parcel_AddPhoto {0}, {1}, 0, {2}, {3}".SqlFormat(True, UserName, pid, s3Path, application)
				Dim sql = "EXEC cc_Parcel_AddPhoto {0}, {1}, 0, {2}, {3},@AppType = {4}".SqlFormat(True, uName, pid, s3Path, application, AppType)
				'Dim imageId As Integer = Database.Tenant.GetIntegerValue("INSERT INTO ParcelImages (ParcelId, IsSketch, Storage, Path) VALUES ({0}, 0, 1, {1});SELECT CAST(@@IDENTITY AS INT) As NewId".SqlFormat(True, pid, s3Path))
				Dim imageId As Integer = Database.Tenant.GetIntegerValue(sql)
	
	            hImageId.Value = imageId
	            hImagePath.Value = "/imgsvr/" + s3Path
	            RunScript("fileUploaded();")
			Catch ex As Exception
				RunScript("fileUploadedFailed();")
			End Try
        End If
    End Sub
End Class
