﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="CAMACloud.Console.protected_tools_upload_photo" Codebehind="upload-photo.aspx.vb" %>

<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <style type="text/css">
        body
        {
            font-family: Segoe UI, Arial, Helvetica, sans-serif;
            font-size: 10pt;
            color: #333;
       
        }
       #progressBar
    {
        background: blue;
        height: 20px;
     
     } 
     #btnCncl, #btnUpload{
     	border: solid black 0.1px;
    	border-radius: 3px;
    	height: 23px;
     }
     #file{
     	border: solid black 0.1px;
    	border-radius: 3px;
     }
      @media only screen and (max-width:675px){
            div#newline{
                    padding-left: 12.3rem;
                    padding-top: 0.75%;}
       }
    @media only screen and (max-width:548px){
            div#newline{
                    padding-left: 0px !important;
                    padding-top: 0.75%;}
       }
       
    
     
     
       
    </style>
    <script type="text/javascript" src="/App_Static/jslib/jquery.js"></script>
    <script type="text/javascript">
        var hash = window.location.hash.substr(1);
        if (hash != "dtrcall") {
            window.onhashchange = function () {
                var hash = window.location.hash.substr(1);
                document.getElementById('ParcelId').value = hash;
            }
        }

        

        window.onprogress = function (a, b, c) {
            console.log(a, b, c);
        }

        window.onwaiting = function (a, b, c) {
            console.log(a, b, c);
        }

        $(function () {
            if ($('#hIsDTR')[0]) {
                $('#hIsDTR')[0].value = window?.top?.__DTR ? 'DTR' : 'QC';
            }
        });

        function showProgress() {
            $('#uploadForm').hide();
            $('#progressBar').show();

        }


        function validateForm() {
         
            var file = document.getElementById("file").value;
            if (!file) {
                alert('No file has been selected. Please choose a file.')
                    return false;
                }
                 
            var activeParcel = window.top.activeParcel;
            if(activeParcel.CC_Deleted == true){
				alert('Photos cannot be added to a property that is deleted. Recovering this parcel will allow you to add photos.');
				hide();
				return false;
			}
            var images = activeParcel.Photos.filter(function (i) { return i.IsPrimary != true });
            var photosLimit = window.top.getClientSettingValue("MaxPhotosPerParcel") != undefined ? window.top.getClientSettingValue("MaxPhotosPerParcel") : localStorage.getItem('MaxPhotosPerParcel');
            var currentPhotos = (photosLimit == activeParcel.Photos.length) ? (activeParcel.Photos.length) + 1 : activeParcel.Photos.length;
            if (photosLimit == 1 && activeParcel.Photos.length == 1) {
                alert("Reached maximum photos limit for the parcel");
                return false;
            }
            var oldestImage = images[0];
            var photosToBeDeleted = currentPhotos - photosLimit;
            var imageIDs = "";
            if (photosToBeDeleted > 0) {
                var r = confirm('This property has exceeded the limit of photos.The oldest photo will be deleted.Do you want to continue ?');
                if (r == true) {
                    for (var j = 0 ; j < photosToBeDeleted ; j++) {
                        imageIDs = (imageIDs && imageIDs.length != 0) ? imageIDs + '$' + images[j].Id.toString() : images[j].Id.toString();
                        for (var k = 0; k < activeParcel.Photos.length; k++) {
                            if (activeParcel.Photos[k].Id == images[j].Id)
                                activeParcel.Photos.splice(k, 1);
                        }
                    }
                    window.top.$qc('deleteimage', { ImageId: imageIDs }, function (data) { });
                }
                else
                    return false;
            }
            if(hash=="dtrcall")
            {
                var parentUrl = window.parent.location.href;
                parentUrl = parentUrl.substring(parentUrl.indexOf('=') + 1);
                if (parentUrl.indexOf("#") > -1) {
                    parentUrl = parentUrl.substring(0, parentUrl.indexOf('#'));
                }
            $('#ParcelId').val(parentUrl);
            }
            if ($('#file').val().trim() == '')
                return false;
            showProgress();
          }
        function fileUploaded() {
            window.top.acceptNewPhoto($('#hImageId').val(), $('#hImagePath').val());
             if (localStorage.getItem('PhotosPopupSatus') == "true") {
                  window.top.reloadParentWindow();   
             }
             alert('Photo uploaded successfully.');
             window.top.deSelectAll();
        }
        function fileUploadedFailed(){
        	 $('.add-photo-frame', window.parent.document).hide();
             alert('Photo upload failed.');
             window.top.deSelectAll();
        }
        function hide() {
        if($('.photo-all-parcels', window.parent.document).length > 0)
        	{
            $('.add-photo-frame', window.parent.document).hide();
            $('.photo-Manager', window.parent.document).show();
            }
        else
            {
            $('.add-photo-frame', window.parent.document).show();
            $('.photo-Manager', window.parent.document).hide();
            }
            var input = $('#file');
            input.replaceWith(input.val('').clone(true));
            return false;
        }
        function DeleteBtnProp() {
            $('#DeletePh', window.parent.document).attr('disabled','disabled');
        	setTimeout(function(){$('#DeletePh', window.parent.document).removeAttr('disabled')}, 3000);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField runat="server" ID="ParcelId" />
    <asp:HiddenField runat="server" ID="hImagePath" />
    <asp:HiddenField runat="server" ID="hImageId" />
    <asp:HiddenField runat="server" ID="hIsDTR" Value="QC"/>
    <div id="uploadForm">
    	<div style="display:inline-block;">
        <span style="font-weight: bold; margin-right: 10px;">Add new photo to the parcel:
        </span>
        <asp:FileUpload runat="server" ID="file" Width="300px" />
        </div>
	<div id="newline" style="display:inline-block;">
        <asp:Button runat="server" ID="btnUpload" Text=" Upload " value="upload" Font-Bold="true" OnClientClick="if (validateForm() == false) return(false); DeleteBtnProp();" />
        <button class="btnCncl" onclick="return hide()" ><b>Cancel</b></button>
    </div>
    </div>
    <div id="progressBar" style="display: none;">
    </div>
    </form>
</body>
</html>
