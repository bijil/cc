﻿Imports System.IO
Public Class parcelworkflowmgmt
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Roles.IsUserInRole("DTR-Manager") Then
            If Not IsPostBack Then
                LoadGrid()
                RunScript("UpdateChecked();")
            End If
        Else
            Alert("You must have the DTR Manager role to access this screen.")
            Page.RunScript("hidedtrStatus();")
            Return
        End If
    End Sub

    Sub LoadGrid()
        grid.DataSource = Database.Tenant.GetDataTable("SELECT * FROM ParcelWorkFlowStatusType ORDER BY Code")
        grid.DataBind()
    End Sub

    Private Sub grid_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grid.RowCommand
        Select Case e.CommandName
            Case "EditItem"
                Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM ParcelWorkFlowStatusType WHERE Id = " & e.CommandArgument)
                txtName.Text = dr.GetString("Code")
                hdname.Value = dr.GetString("Code")
                txtDescription.Text = dr.GetString("Description")
                hdnId.Value = dr.GetInteger("Id")
                Dim checkValue As String = txtDescription.Text
                btnSave.Text = "Save Status Type"
                btnCancel.Visible = True
            Case "DeleteItem"
                Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM ParcelWorkFlowStatusType WHERE Id = " & e.CommandArgument)
                Dim Code As String = dr.GetString("Code")
                Dim DSql As String = "INSERT INTO ParcelAuditTrail (ParcelId, LoginID, EventType, Description, ApplicationType) SELECT p.ParcelId, '" + Membership.GetUser().ToString() + "', 24, 'Parcel Workflow status has been removed due to the deletion of the status type('+ p.statusCode + ' - ' + t.Description +'). ', 'Desktop' from ParcelWorkFlowStatus p JOIN ParcelWorkFlowStatusType t ON p.statusCode = t.Code WHERE p.StatusCode = '" + Code + "';"
                DSql += "DELETE FROM ParcelWorkFlowStatus WHERE StatusCode = '" + Code + "';"
                DSql += "DELETE FROM ParcelWorkFlowStatusType WHERE Id = " & e.CommandArgument
                Database.Tenant.Execute(DSql)
                Dim Description As String = "Parcel WorkFlow status type deleted."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
                CancelForm()
                LoadGrid()
        End Select
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Dim unique As String = txtName.Text
        Dim txtDescriptions As String = txtDescription.Text
        unique = Trim(unique)

        If Not Regex.IsMatch(unique(0), "[a-z A-Z 0-9]") Then
            Alert("The status code should be started with alphanumerics.")
            Return
        End If

        Dim uniqueData As DataTable = Database.Tenant.GetDataTable("SELECT * from ParcelWorkFlowStatusType where Code = {0}".SqlFormatString(unique))
        If hdnId.Value = "" Then
            If uniqueData.Rows.Count > 0 Then
                Alert("Status Code already exists. Please use a unique status code.")
                Return
            End If
            Dim sql As String = "INSERT INTO ParcelWorkFlowStatusType (Code, Description) VALUES ({0}, {1})"
            Database.Tenant.Execute(sql.SqlFormat(False, unique, txtDescriptions))
            Dim Description As String = "New Parcel WorkFlow status type added."
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            Alert("New Parcel WorkFlow status type added")
        Else
            If uniqueData.Rows.Count > 0 AndAlso Not hdname.Value = unique Then
                Alert("Status Code already exists. Please use a unique status code.")
            Else
                Dim dold As DataTable = Database.Tenant.GetDataTable(" SELECT * FROM  ParcelWorkFlowStatusType WHERE Id = {0} and Code = {1} and Description= {2}".SqlFormat(False, hdnId.Value, unique, txtDescriptions))
                Dim sold As DataRow = Database.Tenant.GetTopRow(" SELECT * FROM  ParcelWorkFlowStatusType WHERE Id = {0}".SqlFormat(False, hdnId.Value))
                If (dold.Rows.Count > 0) Then
                    hdnId.Value = ""
                    txtName.Text = ""
                    txtDescription.Text = ""
                    btnSave.Text = "Add Status Type"
                    btnCancel.Visible = False
                Else
                    Dim sql As String = ""
                    Dim cd As String = sold.GetString("Code")
                    Dim de As String = sold.GetString("Description")
                    If cd <> unique Then
                        sql += "UPDATE ParcelWorkFlowStatus SET StatusCode = '" + unique + "' WHERE StatusCode = '" + cd + "';"
                    End If
                    sql += "INSERT INTO ParcelAuditTrail (ParcelId, LoginID, EventType, Description, ApplicationType) SELECT ParcelId,'" + Membership.GetUser().ToString() + "', 24, 'Parcel Workflow Status (" + cd + " - " + de + ") updated to (" + unique + " - " + txtDescriptions + ") through Manage Parcel Workflow edit', 'Desktop' from ParcelWorkFlowStatus WHERE StatusCode = '" + unique + "';"
                    sql += "UPDATE ParcelWorkFlowStatusType SET Code = {1}, Description = {2} WHERE Id = {0}"
                    Database.Tenant.Execute(sql.SqlFormat(False, hdnId, unique, txtDescriptions))
                    Dim Description As String = "Parcel WorkFlow status type updated."
                    SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
                    Alert("Parcel WorkFlow status type modified")
                End If
            End If
        End If
        CancelForm()
        LoadGrid()
        RunScript("UpdateChecked();")
    End Sub

    Private Sub CancelForm() Handles btnCancel.Click
        hdnId.Value = ""
        txtName.Text = ""
        txtDescription.Text = ""
        btnSave.Text = "Add Status Type"
        btnCancel.Visible = False
    End Sub
End Class