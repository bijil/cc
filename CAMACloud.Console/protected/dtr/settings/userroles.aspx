﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master"
    CodeBehind="userroles.aspx.vb" Inherits="CAMACloud.Console.userroles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
        Manage Desktop Review Role Assignments</h1>
    <asp:GridView runat="server" ID="gvUsers" AllowPaging="true" PageSize="30">
        <Columns>
            <asp:TemplateField HeaderText="Full Name">
                <ItemStyle Width="200px" />
                <ItemTemplate>
                    <%# Eval("FirstName") + " " + Eval("LastName")%>
                    <asp:HiddenField runat="server" ID="hdnLoginId" Value='<%#Eval("LoginId") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="LoginId" HeaderText="LoginId" ItemStyle-Width="120px" />
            <asp:TemplateField>
                <HeaderStyle CssClass="c" Width="100px" />
                <ItemStyle CssClass="c" />
                <HeaderTemplate>
                    Data Entry<br />
                    (Read-Only)
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:RadioButton runat="server" GroupName='<%#Eval("LoginId") %>' Checked='<%# Roles.IsUserInRole( Eval("LoginId"), "DTR-ReadOnly") %>'
                        OnCheckedChanged="ToggleReadOnlyUser" AutoPostBack="true" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderStyle CssClass="c" Width="100px" />
                <ItemStyle CssClass="c" />
                <HeaderTemplate>
                    Remote<br />
                    Verification
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:RadioButton runat="server" GroupName='<%#Eval("LoginId") %>' Checked='<%# Roles.IsUserInRole( Eval("LoginId"), "DTR-Editor") %>'
                        OnCheckedChanged="ToggleEditorUser" AutoPostBack="true" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderStyle CssClass="c" Width="100px" />
                <ItemStyle CssClass="c" />
                <HeaderTemplate>
                    Manager
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:RadioButton runat="server" GroupName='<%#Eval("LoginId") %>' Checked='<%# Roles.IsUserInRole( Eval("LoginId"), "DTR-Manager") %>'
                        OnCheckedChanged="ToggleManagerUser" AutoPostBack="true" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
