﻿Imports System.IO
Public Class parcelstatuslist
    Inherits System.Web.UI.Page

    Dim dtTemp As New DataTable
    Dim keys As New List(Of String)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    	If Roles.IsUserInRole("DTR-Manager") Then
    		
    	Else      	
      	Alert("You must have the DTR Manager role to access this screen.")      	
      	Page.RunScript("hidedtrStatusList();")
      	Return	
		 End If
    End Sub
    Function KeyList() As String
        Dim keys As New List(Of String)

        For i = 1 To 10
            If Database.Tenant.Application.KeyField(i).IsNotEmpty Then
                keys.Add(Database.Tenant.Application.KeyField(i))
                Exit For
            End If
        Next

        If keys.Count > 0 Then
            pNoCK.Visible = True
        Else
            pNoCK.Visible = False
        End If

        pHaveCK.Visible = keys.Count > 0

        Return String.Join(", ", keys.ToArray.Select(Function(x) "<strong>" + x + "</strong>"))
    End Function
    Private Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
        Dim dtTemp As DataTable = New DataTable()
        Dim sr As New StreamReader(ControlFile.FileContent)
        Dim colName As String = "", joinCondition As String = "", query As String = "", i As Integer, code As String, count As Integer = 0
        
        If ControlFile.FileBytes.Length = 0 Then
        	Alert("Please select the file to be uploaded.")
            Return
        End If
        
        Dim Description As String = "New parcel status list uploaded."


        If csv_to_datatableConvertion(sr, dtTemp) Then
            For i = 1 To 10
                If Database.Tenant.Application.KeyField(i).IsNotEmpty Then
                    colName = Database.Tenant.Application.KeyField(i)
                    Exit For
                End If
            Next
            query = "DECLARE @count INT = 0; SET NOCOUNT ON; "
            For Each dr As DataRow In dtTemp.Rows
                If dr(0).ToString() <> "" Then
                    code = dr(1).ToString()
                    If code.Trim() <> "" Then
                        If Regex.IsMatch(code, "^[0-9A-Za-z ]+$") Then
                        Else
                            Alert("Your list could not be uploaded because '" + colName + "' has an invalid value.\n Please revise your list and upload again.")
                            Return
                        End If
                        query += "UPDATE Parcel SET DTRStatus = " & "'" + code + "' WHERE KeyValue" & i.ToString() & " = " & "'" + dr(colName).ToString() + "'; if(@@ROWCOUNT = 1) SET @count =  @count + 1; "
                        count += 1
                    Else

                    End If
                End If
            Next
            query += "select @count; " 
            Try
            	Dim ExecutedQuerycount As Integer = Database.Tenant.GetIntegerValue(query)
            	If count = ExecutedQuerycount Then
                    SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)

                    Alert(ExecutedQuerycount.ToString() & " parcels updated")
            	Else 
            		Alert(ExecutedQuerycount.ToString() & " parcels updated but failed on " & count-ExecutedQuerycount &" invalid parcels.")
            	End If
            Catch ex As Exception
                Alert(ex.Message)
            End Try
        End If

    End Sub

    Public Function csv_to_datatableConvertion(stream As StreamReader, ByRef dt As DataTable) As Boolean

        Try

            Dim pattern As String = "^\s*""?|""?\s*$"
            Dim rgx As New Regex(pattern)
            Dim Lines As String() = stream.ReadToEnd().Split(Environment.NewLine)
			Dim AppcolName As String = ""
            Dim Fields As String() = Lines(0).Split(New Char() {","c})
              
            Dim Cols As Integer = Fields.GetLength(0)
			For i = 1 To 10
                If Database.Tenant.Application.KeyField(i).IsNotEmpty Then
                    AppcolName = Database.Tenant.Application.KeyField(i)
                    Exit For
                End If
            Next


            If (Lines(0).Split(",").Length <> 2) Then
                Alert("There is should be 2 Fields in uploading CSV File")


                Return False


            End If



            If (Fields(0) <> AppcolName Or Fields(1) <> "DTR Status Code") Then
                Alert("Invalid data format please revise your entry")
                Return False
            End If
            '1st row must be column names. 
            For i As Integer = 0 To Cols - 1
                dt.Columns.Add(rgx.Replace(Fields(i).ToUpper(), ""), GetType(String))
            Next
            Dim rgx1 As New Regex("(?!\B""[^""]*),(?![^""]*""\B)")    'Regex to split comma not in quotes
            Dim Row As DataRow
            Dim PSLCount As Integer = Lines.GetLength(0) - 1
            If(PSLCount=0)
            	Alert("No parcels found")
            	Return False
        	End If
            For i As Integer = 1 To Lines.GetLength(0) - 1
                If Lines(i).Trim.Length = 0 Then
                    Continue For
                End If
                Fields = rgx1.Split(Lines(i))

                If Fields.Length < Cols Then
                    Continue For
                End If
                Row = dt.NewRow()
                If Fields(0).Trim() <> "" Then
                    For j As Integer = 0 To Cols - 1
                        If Fields.Length <> Cols Then
                            Exit For
                        End If
                        Row(j) = rgx.Replace(Fields(j), "")
                    Next
                    dt.Rows.Add(Row)
                End If
            Next
            'Database.Tenant.BulkLoadTableFromDataTable("temp_ParcelPriority", dt)
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function

End Class