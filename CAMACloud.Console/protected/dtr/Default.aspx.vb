﻿Public Class _Default1
    Inherits System.Web.UI.Page
	Public organizationName As String
    Public stateName As String
    Public alterNateField As String
    Public woolpertApikey As String
	Public woolpert6inchId As String
    Public woolpert3inchId As String
    Public ReadOnly Property LocalDatabaseName As String
        Get
            Return HttpContext.Current.GetCAMASession.TenantKey.ToUpper + "__DTR" + Right(CAMACloud.Data.Database.Tenant.Application.SchemaVersion.ToString.PadLeft(5, "0"), 5) + "_" + User.Identity.Name
        End Get
    End Property

    Public ReadOnly Property EnableNewPriorities As Boolean
        Get
            Dim EnableNewPriority = False
            Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM ClientSettings WHERE Name = 'EnableNewPriorities' AND Value = 1")
            If dr IsNot Nothing Then
                EnableNewPriority = True
            End If
            Return EnableNewPriority
        End Get
    End Property

    Public ReadOnly Property DoNotChangeTrueFalseInPCI As Boolean
        Get
            Dim ChangeTrueFalse = False
            Dim dr As String = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name = 'DoNotChangeTrueFalseInPCI'")
            If dr = "1" Then
                ChangeTrueFalse = True
            Else
                ChangeTrueFalse = False
            End If
            Return ChangeTrueFalse
        End Get
    End Property

    Public Property DTRSearchEnabled As Boolean = True

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            RefreshDTRTaskStatus()
            ddlDTRTask.FillFromSql("SELECT Id, Name FROM DTR_Tasks WHERE TotalParcels > 0 AND AssignedTo = " + Page.User.Identity.Name.ToSqlValue)
            If ddlDTRTask.Items.Count = 0 Then
                DTRSearchEnabled = False
            End If
            Dim query As String
            If Roles.IsUserInRole("DTR-ReadOnly") Then
                query = "SELECT Name, Description FROM DTR_StatusType where DTRReadOnly = 1 ORDER BY Code"
            ElseIf Roles.IsUserInRole("DTR-Editor") Then
                query = "SELECT Name, Description FROM DTR_StatusType where DTREditor = 1 ORDER BY Code"
            ElseIf Roles.IsUserInRole("DTR-Manager") Then
                query = "SELECT Name, Description FROM DTR_StatusType where DTRManager = 1 ORDER BY Code"
            Else
                query = "SELECT Name, Description FROM DTR_StatusType ORDER BY Code"
            End If
            ddlSelectedStatus.FillFromSql(query, True, "-- Not Selected --")
            ddlSelectedEstimate.FillFromSql("SELECT Id, Description FROM AppraisalType", True, "-- Not Selected --")
            ddlBulkDTRStatus.FillFromSql(query, True, "-- Not Set --")
            ' rpFieldCategory.DataSource = d_("SELECT * FROM FieldCategory WHERE ShowCategory = 1 ORDER BY Ordinal")
            Dim dtFieldCategory As DataTable = d_("SELECT * FROM FieldCategory WHERE ShowCategory = 1 and ParentCategoryId is null  ORDER BY Ordinal")
            rpFieldCategory.DataSource = dtFieldCategory
            rpFieldCategory.DataBind()
            If dtFieldCategory.Rows.Count > 0 Then
                ltParceltab.Text = "<li class='tip' div-width='520'  tip-margin-top='10' abbr='The panels on the Parcel Data tab will display in the same order that the appraiser sees on the field device. Any panel where changes were made by the appraiser will display in <span style=""color:red;font-weight:bold"">red</span> letters to indicate a change. Each field changed will have a red exclamation mark next to it. A pop-up window will show the before / after values where each value is a link. To revert a change to the prior value, click on that value.' content=""parceldata"" ><a href=""#parceldata"" onclick=""openCategory('" + dtFieldCategory.Rows(0)("Id").ToString() + "', '" + dtFieldCategory.Rows(0)("Name").ToString() + "','parceldata');$('.category-menu').show();return false;"" category=""" + dtFieldCategory.Rows(0)("Id").ToString() + """>Parcel Data</a></li>"
            Else
                ltParceltab.Text = ""
            End If
            'rpCategoryForFields
            rpCategoryForFields.DataSource = BusinessLogic.FieldCategory.GetFieldCategories(True)
            rpCategoryForFields.DataBind()
            getClassCalculatorAttributes()

            'FillSubmenus

            'rpFieldAllCategories.DataSource = d_("SELECT * FROM FieldCategory WHERE ShowCategory = 1  ORDER BY Ordinal")
            ''rpFieldAllCategories.DataSource = d_("exec  qc_getAllCategories")
            'rpFieldAllCategories.DataBind()

            getDataForCategories()
            LoadColumnsList()
            getPhotoProperties()
            organizationName = ClientOrganization.PropertyValue("County", HttpContext.Current.GetCAMASession().OrganizationId)
            stateName = ClientOrganization.PropertyValue("State", HttpContext.Current.GetCAMASession().OrganizationId)
        End If
        Dim alternate As String = Database.Tenant.Application.AlternateKeyfield
        Dim parcelTable As String = Database.Tenant.Application.ParcelTable
        If alternate IsNot Nothing Then
            alterNateField = Database.Tenant.GetStringValue("select DisplayLabel from DataSourceField where Name ={0} AND SourceTable = {1}".SqlFormatString(alternate, parcelTable))
        End If
        	woolpertApikey = ClientSettings.PropertyValue("woolpertApikey")
        	woolpert6inchId = ClientSettings.PropertyValue("woolpert6inchId")
        	woolpert3inchId = ClientSettings.PropertyValue("woolpert3inchId")

    End Sub
    Protected Sub rpCategoryForFields_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpCategoryForFields.ItemDataBound
        Dim rpFields As Repeater = e.Item.FindControl("rpFields")
        Dim hCID As HiddenField = e.Item.FindControl("hCID")
        'Dim dt As DataTable = d_("SELECT * FROM DataSourceField WHERE (DoNotShowOnDTR = 0 AND DoNotShowOnDE = 0) AND CategoryId = " & hCID.Value & " ORDER BY CategoryOrdinal")
        Dim dt As DataTable = d_("SELECT * FROM DataSourceField WHERE (DoNotShowOnDTR = 0) AND (ExclusiveForSS IS NULL OR ExclusiveForSS=0) AND CategoryId = " & hCID.Value & " ORDER BY CategoryOrdinal")

        If Roles.IsUserInRole("DTR-ReadOnly") Then
            For Each dr As DataRow In dt.Rows
                dr("IsReadOnly") = True
            Next
        End If
        rpFields.DataSource = dt
        rpFields.DataBind()
    End Sub

    Public Function ShowFieldForDataType(dataType As Integer, input As String) As Boolean
        Dim stdinput As String = "txt"
        Select Case dataType
            Case 1
                stdinput = "txt"
            Case 2
                stdinput = "num"
            Case 3
                stdinput = "yesno"
            Case 4
                stdinput = "date"
            Case 5
                stdinput = "lookup"
            Case 6
                stdinput = "ltxt"
            Case 7
                stdinput = "num"
            Case 8
                stdinput = "num"
            Case 9
                stdinput = "num"
            Case 10
                stdinput = "num"
            Case 11
            	stdinput = "ddl"
            Case 13
            	stdinput ="TriStateRadio"
            Case Else
                stdinput = "txt"
        End Select

        Return (input = stdinput)
    End Function

    Public Function SelectTypeWithReadOnlyStatus(isReadOnly As Boolean, isCalculated As Boolean, isReadOnlyDTR As Boolean, categoryId As String)
    	Dim catReadOnly As Boolean = False	
		If  categoryId <> ""
		   catReadOnly  = Database.Tenant.GetIntegerValue("SELECT  IsNull(IsReadOnly, '') FROM FieldCategory WHERE Id = " &categoryId)
		End If
    	If isReadOnly Or isCalculated Or isReadOnlyDTR Or catReadOnly = True Then
            Return "text"
        Else
            Return "number"
        End If
    End Function

    Public Function IsFieldReadOnly() As Boolean
        Return Eval("IsCalculated") Or Eval("IsReadOnly") Or Eval("IsReadOnlyDTR")
    End Function

    Public Function IsFieldEnabled() As Boolean
        Return Not IsFieldReadOnly()
    End Function

    Public Function haveMoreField(fieldId As String, dt As String) As String
        Dim returnValues As String = "hide"
        If dt = "5" Then
            If Database.Tenant.GetDataTable("select p.IdValue as total from DataSourceField d left outer join  ParcelDataLookup p on d.LookupTable=p.LookupName where d.Id={0} and IsReadOnly=0".SqlFormatString(fieldId)).Rows.Count > 25 Then
                returnValues = "show"
            End If
        End If

        Return returnValues
    End Function

    Protected Sub LoadYesOrNoLookup(sender As Object, e As System.EventArgs)
        Dim ddl As DropDownList = sender
        Dim RadioFieldValues As String = ddl.Attributes("RadioFieldValues")
        If RadioFieldValues <> Nothing And RadioFieldValues <> "" Then
            Dim arr As String() = RadioFieldValues.Split(",")
            ddl.InsertItem("Yes", arr(0), 0)
            ddl.InsertItem("No", arr(1), 1)
            ddl.Enabled = False
        Else
            ddl.InsertItem("No", "false", 0)
            ddl.InsertItem("Yes", "true", 1)
        End If
    End Sub

    Protected Sub LookupDropdown_Load(sender As Object, e As System.EventArgs)
        Dim ddl As DropDownList = sender
        Dim lookupTable As String = ddl.Attributes("LookupTable")
        Dim lookupShowNullValue As String = ddl.Attributes("LookupShowNullValue")

        If lookupTable <> "" Then

            Dim ldt = d_("SELECT IdValue, NameValue FROM ParcelDataLookup WHERE LookupName = '" + lookupTable + "' ORDER BY Ordinal")
            For Each r As DataRow In ldt.Rows
                Dim IdVal As String = r.GetString("idvalue").Trim
                If IdVal = "True" Or IdVal = "TRUE" Or IdVal = "False" Or IdVal = "FALSE"
                	IdVal = IdVal .ToLower 
                End If
                ddl.AddItem(r.GetString("namevalue").Trim,IdVal )
            Next
            If lookupShowNullValue.ToLower = "true" Then
                ddl.InsertItem("-- No Value --", "", 0)
            End If
            '
        End If
        Dim isLookup As Boolean = Boolean.Parse(ddl.Attributes("IsListBox"))
        If isLookup Then
            ddl.Attributes.Add("multiple", "multiple")
            ddl.Style("height") = "120px"
        End If
    End Sub

    Private Sub getDataForCategories()
        Dim query1 As String = "select f2.Id,f2.Name,f2.ParentCategoryId,f2.SourceTableName,f3.id as ChildCategoryId,f2.FilterFields as NodeFilterField from FieldCategory f2 left outer join FieldCategory f3 on f3.ParentCategoryId=f2.Id"
        Dim dt1 As DataTable
        dt1 = d_(query1)
        If (dt1.Rows.Count > 0) Then
            Dim iter_val As Integer = 0
            Dim filters As String = ""
            While (iter_val < dt1.Rows.Count)
                Dim dt2 As DataTable
                Dim sourceTable As String = dt1.Rows(iter_val)("SourceTableName").ToString()
                ' Dim query2 As String = "SELECT f.Name As FieldName FROM DataSourceRelationships r INNER JOIN DataSourceKeys k ON k.RelationshipId = r.Id INNER JOIN DataSourceField f ON k.FieldId = f.Id inner join DataSourceTable df on df.Id=r.TableId WHERE r.Relationship is null and df.Name='" + sourceTable + "'"

                Dim query2 As String = "SELECT  tf.Name As TableField, pf.Name As ReferenceField FROM DataSourceRelationships r LEFT OUTER JOIN DataSourceKeys k ON k.RelationshipId = r.Id LEFT OUTER JOIN DataSourceField tf ON k.FieldId = tf.Id LEFT OUTER JOIN DataSourceField pf ON k.ReferenceFieldId = pf.Id 	LEFT OUTER JOIN DataSourceTable tt ON r.TableId = tt.Id LEFT OUTER JOIN DataSourceTable pt ON r.ParentTableId = pt.Id WHERE tt.Name ='" + sourceTable + "' AND Relationship IS NOT NULL AND Relationship <> 0 AND k.IsReferenceKey = 1"
                dt2 = d_(query2)
                If (dt2.Rows.Count > 0) Then
                    Dim iter_val2 As Integer = 0
                    While (iter_val2 < dt2.Rows.Count)
                        Dim f = dt2(iter_val2)("TableField").ToString()
                        If dt2(iter_val2)("TableField").ToString() <> dt2(iter_val2)("ReferenceField").ToString() Then
                            f = dt2(iter_val2)("ReferenceField").ToString() + "/" + dt2(iter_val2)("TableField").ToString()
                        End If
                        filters += f
                        iter_val2 += 1
                        If (iter_val2 < dt2.Rows.Count) Then
                            filters += ","
                        End If
                    End While

                End If
                dt1.Rows(iter_val)("NodeFilterField") = filters + IIf(dt1.Rows(iter_val)("NodeFilterField").ToString().IndexOf("ROWUID") > -1, ",ROWUID", "")
                dt1.AcceptChanges()
                filters = ""
                iter_val += 1
            End While
            rpFieldAllCategories.DataSource = dt1
            rpFieldAllCategories.DataBind()
        End If


    End Sub

    Private Sub LoadColumnsList()
        'Dim dtGridColumns As DataTable = Database.Tenant.GetDataTable("SELECT DISTINCT Heading as Name FROM UI_GridViewFieldSettings WHERE IsDTR = 1 And Heading IS NOT NULL UNION SELECT DisplayLabel as Name FROM DataSourceField f INNER JOIN DataSourceTable t ON f.TableId = t.Id INNER JOIN FieldCategory fc ON fc.Id = f.CategoryId WHERE t.ImportType = 0 And f.CategoryId Is Not NULL And f.IsCalculated = 0 And DisplayLabel IS NOT NULL order by Name")
        Dim dtGridColumns As DataTable = Database.Tenant.GetDataTable("EXEC Sp_ColumnChooser '1'")
        lbColumnsLeft.DataSource = dtGridColumns
        lbColumnsLeft.DataTextField = "Name"
        lbColumnsLeft.DataValueField = "Name"
        lbColumnsLeft.DataBind()
    End Sub
    Public Sub getPhotoProperties()
        Dim dtPhotoProperties As DataTable = d_("SELECT * FROM DataSourceField WHERE SourceTable= '_photo_meta_data' ORDER BY AssignedName ") ''DoNotShowOnDE = 0  AND
        rpPhotoProperties.DataSource = dtPhotoProperties
        rpPhotoProperties.DataBind()
    End Sub

    Public Sub getClassCalculatorAttributes()
        Dim dtClassCalculatorAttributes As DataTable = d_("SELECT * FROM DataSourceField d INNER JOIN FieldCategory f ON d.CategoryId = f.Id where  d.IsClassCalculatorattribute = 1 and f.ClassCalculatorParameters IS NOT NULL ORDER BY SourceOrdinal")
        'DoNotShowOnDE = 0 AND
        rpClassCalcFields.DataSource = dtClassCalculatorAttributes
        rpClassCalcFields.DataBind()
    End Sub

    Private Sub RefreshDTRTaskStatus()
        Dim sql As String = "SELECT * FROM DTR_Tasks where AssignedTo = " + Page.User.Identity.Name.ToSqlValue
        For Each dr As DataRow In Database.Tenant.GetDataTable(sql).Rows
            Dim compiledSql As String = dr.GetString("CompiledSqlFilter")
            If compiledSql <> "" Then
            	Dim fdata As String = dr.GetString("FilterData")
            	Dim sfQuery As String = ""
                Dim tableClause As String = "FROM Parcel p INNER JOIN ParcelData pdata ON pdata.CC_ParcelId = p.Id"
                If fdata.Contains("$ParcelWorkflow") Then
                    tableClause = tableClause + " LEFT OUTER JOIN ParcelWorkFlowStatus pwf ON p.Id = pwf.ParcelId"
                End If
                If fdata.contains("$SketchFlag") Then
                	sfQuery = "SELECT * INTO #temp_sketchSRFlags FROM (SELECT psf.ParcelId As PId, CAST(psf.FlagId As varchar ) As FlagId, srf.Name As Name FROM ParcelSketchFlag psf JOIN SketchReviewFlags srf on psf.FlagId = srf.Id UNION SELECT NULL As PId, CONCAT('S_', Id) As FlagId, Name As Name FROM SketchStatusFlags) SSRF; "           	
        			tableClause = tableClause + " LEFT OUTER JOIN #temp_sketchSRFlags ssf ON (p.Id = ssf.PId OR CONCAT('S_', ISNULL(p.SketchFlag, '')) = ssf.FlagId)"
                End If
                Dim testSql As String = sfQuery + "SELECT COUNT(DISTINCT(p.Id)) " + tableClause + " WHERE " + compiledSql
                Dim parcelCount As Integer = Database.Tenant.GetIntegerValue(testSql)
                Dim completedParcelsSql As String = sfQuery + "SELECT COUNT(DISTINCT(p.Id)) " + tableClause + " WHERE p.Reviewed = 1 AND p.QC IS NULL AND (" + compiledSql + ")"
                Dim completedCount As Integer = Database.Tenant.GetIntegerValue(completedParcelsSql)
                Dim sqlUpdate As String = "UPDATE DTR_Tasks SET TotalParcels = {1}, CompletedParcels = {2} WHERE ID = {0}".SqlFormat(False, dr.GetInteger("Id"), parcelCount, completedCount)
                Database.Tenant.Execute(sqlUpdate)
            End If
        Next
    End Sub
End Class