﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/Quality.master"
    CodeBehind="Default.aspx.vb" Inherits="CAMACloud.Console._Default1" EnableEventValidation="false" ValidateRequest="false" %>

<%@ Import Namespace="CAMACloud" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var __DTR = true;
        var CAMACloud = {};
        var localDBName = '<%=  LocalDatabaseName %>'
        var currentLoginId = '<%= Page.User.Identity.Name %>'
        var countyName = '<%= organizationName%>'
        var stateName = '<%= stateName%>'
        var TrueFalseInPCI = '<%= DoNotChangeTrueFalseInPCI %>'
        <%If Roles.IsUserInRole("DTR-Manager") Then%>
        var DTRRoles = { Manager: true };
        <%ElseIf Roles.IsUserInRole("DTR-Editor") Then%>
        var DTRRoles = { Editor: true };
        <%ElseIf Roles.IsUserInRole("DTR-ReadOnly") Then%>
        var DTRRoles = { ReadOnly: true };
        <%End If%>
        <%If Roles.IsUserInRole("TaskManager") Then%>
        var AtcWorkflowrole = true;
        <%End If%>
    </script>
    <link rel="stylesheet" href="/App_Static/css/quality.css?<%= Now.Ticks %>" />
    <link rel="stylesheet" href="/App_Static/css/flexigrid.css?<%= Now.Ticks %>" />
    <link rel="Stylesheet" href="/App_Static/css/tabcontent.css?<%= Now.Ticks %>" />
	<link rel="stylesheet" href="/App_Static/css/leaflet.css?<%= Now.Ticks %>" />
    <link rel="Stylesheet" href="/App_Static/css/01-lookup.css?<%= Now.Ticks %>" />
    <script src="https://maps.googleapis.com/maps/api/js?libraries=geometry&key=AIzaSyC7e5YrZlKqWtaY4ehsDwMgCis4IfNs7Xw"></script>
    <script type="text/javascript" src="/App_Static/jslib/sqllite.js"></script>
    <script src="/App_Static/js/qc/dtr.js" type="text/javascript"></script>
    <script type="text/javascript" src="/App_Static/jslib/infobubble.js"></script>
    <script type="text/javascript" src="/App_Static/js/qc/010-constants.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="/App_Static/js/qc/000-objects.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="/App_Static/js/qc/000-utillib.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="/App_Static/js/qc/000-window.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="/App_Static/js/tabcontent.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/base64.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/canvas2image.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/leaflet.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/esri-leaflet.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/flexigrid.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="/App_Static/js/qc/100-MiscellaneousImprovements.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="/App_Static/js/qc/010-quality.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="/App_Static/js/dtr/010-dtr.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="/App_Static/js/qc/02-app-localdb.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="/App_Static/js/qc/025-sync.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="/App_Static/js/qc/02-local-cache.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="/App_Static/js/qc/015-datatypes.js?<%= Now.Ticks %>"></script>

    <script type="text/javascript" src="/App_Static/js/qc/02-lookup.js?<%= Now.Ticks %>"></script>

    <script type="text/javascript" src="/App_Static/js/qc/020-parcel.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="/App_Static/js/qc/030-parcel-search.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="/App_Static/js/qc/050-photos.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="/App_Static/js/qc/060-bulkeditor.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="/App_Static/js/qc/080-validations.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="/App_Static/js/qc/010-app-ui-comp.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="/App_Static/js/qc/010-conversions.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="/App_Static/js/qc/040-custom-ddl.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="/App_Static/js/qc/055-sketch.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="/App_Static/js/sketch/14-ccsketcheditor.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="/App_Static/js/parcelmanager.js?<%= Now.Ticks %>"></script>
    <script type="text/javascript" src="/App_Static/jslib/hammer.js"></script>
    <cc:JavaScript ID="JavaScript1" runat="server" IncludeFolder="~/App_Static/js/sketch/sketchlib/" />

 

    <%-- <cc:JavascriptCompressor runat="server" JSPath="~/App_Static/js/qc/" OutPutFileName="newjsnameminjs.js" EnableCompression="true" IncludeSubFolders="true"  />--%>
    <style type="text/css">
        #progressBar {
            background: #dcdcdc;
            height: 11px;
        }
        .photocss{
		    float: right;
    		border-radius: 5px;
    		padding: 2px 18px;
    		background-color: white;
    		color: #424242;
    		margin-top: 1px;
    		border: 1px solid silver;
    		font-size: 13px;
    		cursor: pointer;
		}
		.photocss:hover {
    		color: Black;
		}
		
		 .filter-container::-webkit-scrollbar 
        {
        width: 7px;
        }

        .filter-container::-webkit-scrollbar-thumb 
        {
        background: #ABABAB;
        border-radius: 30px;
        }
        
        .filter-container::-webkit-scrollbar-track 
        {
        background: #DDDDEE;
        border-radius: 30px;
        }
        .filter-container::-webkit-scrollbar-button 
        {
        display:none;
        }
        
		.closeCstMsg {
            color: #000;
            font-size: 28px;
            font-weight: bold;
            float: right;
            top: -8px;
            position: absolute;
            right: 7px;
            cursor: pointer;
            margin-top: 2px;
        }
        .btn-filter{
        	font-weight: bold;
		    text-decoration: none;
		    padding: 9px 26px;
		    margin: 1px;
		    margin-left: 0px;
		    color: #ffff;
		    margin-right: 15px;
		    background: #008cba;
		    border-radius: 5px;
		    display: inline-block;
		    cursor: pointer;
		    margin-top: 6px;
		    border: solid 1px #ccc
		}

	    button.btn-filter:hover {
		    background: #ffffff;
		    color: #008cba;
		    border: solid 1px #008cba;
        }
        
        /*.lastReviewedMain {
            float: right;
            margin-top: 0px !important;
            margin-right: -250px !important;
        }
        
        .tab_selected_estimate {
            float: right;
            margin-top: -25px;
            margin-right: 53px;
        }*/

        .dslw-tabs {
            display: flex;
            flex-direction: column;
            float: right;
            margin-top: -70px;
            margin-right: 15px;
        }

        .Desk_Review_Status, .tab_selected_estimate, .lastReviewedMain {
            margin: 0px !important;
        }
        
        .work-flow-div {
            width: 150px;
            max-height: 95px;
            overflow-y: auto;
            margin-right: 300px;
            margin-top: -73px;
            float: right;
        }

        @media screen and (max-width: 1425px) {
            .work-flow-div {
                max-height: 68px;
            }
        }

        .work-flow-div::-webkit-scrollbar {
            width: 5px; 
        }

        .work-flow-div .wf-span {
            display: inline-block;
            margin: 0 5px 2px 0; 
            padding: 3px;
        }
    </style>
    <script type="text/javascript">
        $(window).bind('hashchange', hashProcessor);
        //   var pageTimeout = setTimeout('window.location.reload();', 12000);
        var serviceUnavailabilityWarned = false;
    </script>
    <script type="text/javascript">
    	var priorityListFlag;
        function showPopup() {
            $('.columns').dialog({
                modal: true,
                width: 538,
                height: 360,
                position: ['center'],
                resizable: false,
                title: 'Column Chooser',
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
            var left = document.getElementById("lbColumnsLeft");
            var right = document.getElementById("lbColumnsRight");
            for (var i = 0; i < right.options.length; i++) {
            	var val = right.options[i].value;
	            if ($('.hDivBox tr th').text().indexOf(val) == -1) {
                    left.innerHTML += "<option value='" + val + "'>" + val + "</option>"
                    right.options[i].remove();
                    i--;
                }
            }
            var listLength = left.options.length;
            $('#lbColumnsLeft option').filter(function () {
                return !this.value || $.trim(this.value).length == 0 || $.trim(this.text).length == 0;
            }).remove();
            for (var i = 0; i < listLength; i++) {
                var val = left.options[i].value;
                if ($('.hDivBox tr th').text().indexOf(val) >= 0) {
                    right.innerHTML += "<option value='" + val + "'>" + val + "</option>"
                    left.options[i].remove();
                    listLength = listLength - 1;
                    i--;
                }
            }
            $("#lbColumnsRight option").each(function () {
                var $option = $(this);
                $option.siblings()
                      .filter(function () { return $(this).val() == $option.val() })
                      .remove()
            })

            $('.comparable-edit').on('click', function (e) {
                e.preventDefault();
                return false;
            });
        }

        function PrintElem(elem) {
            Popup($(elem).html());
        }
        function searchFieldsChangedForDTR() {
            $('#mySelect option[value="p.ReviewDate"]').text('Field / Desk Reviewed On'); $('#mySelect option[value="p.ReviewedBy"]').text('Field / Desk Reviewed By');
        }
        $(function () {
            //prevent unwanted submits on script errors
            document.body.onsubmit = function () { return false; }
        });

        function setHeight(idname) {
            idname.addEventListener("keyup", autosize);    //keydown changed to keyUp.If copy paste, then scroll height not calculated based on paste string length.
            function autosize() {
                var el = this;
                //setTimeout(function(){
                el.style.cssText = 'height:auto;';
                el.style.cssText = 'height:' + el.scrollHeight + 'px';
                //}, 0 );
            }
        }

		function showexportPopup() {
            $('.modal').dialog({
                modal: true,
                width: 350,
                height: 150,
                draggable: false,
                resizable: false,
                position: ['center', '45%'],
                title: 'Choose the format of the Export:',
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                    $("input[value='0']",this).prop("checked",true);
                }
            });
          }
          
       function export_click() {
            var exprtdiv = $("#<%= exportlist.ClientID%>");
		    var selectedValue = exprtdiv.find("input:checked").val();
		     switch (selectedValue) {
		     	case '0':
		     		exporttoformats('csv');
		     		break;
		     	case '1':
		     		exporttoformats('xls');
					break;
				case '2':
					exporttoformats('xlsx');
					break;				
		     }
		     $('.modal').dialog('close');
        }
        
    	function calcHeightOfcontentWindow() {
    		var lheight = $('.left-panel').height();
    		$('.flexigrid .bDiv').height(lheight - 87);
    	}
    </script>
    <%If Not DTRSearchEnabled Then%>
        <script>
            function doSearch() {
                alert('You have not been assigned with any tasks on DTR. Search functionality is currently disabled. ')
            }

            doSearch();
        </script>
    <%End If%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftContent" runat="Server">
    <div style="margin: 10px 10px 10px 0px;font-size:14pt;" class="c b">
        <%If Roles.IsUserInRole("DTR-Manager") Then%>
            DTR Manager
        <%ElseIf Roles.IsUserInRole("DTR-ReadOnly") Then%>
            Data Entry
        <%ElseIf Roles.IsUserInRole("DTR-Editor") Then%>
            Remote Verification
        <%End If%>
    </div>
    <div style="margin: 10px 5px 10px 0px;">
        <div style="font-weight: bold;">
            Select DTR Task:</div>
        <div>
            <asp:DropDownList runat="server" ID="ddlDTRTask" Width="100%"  CssClass="app-dtr-selected-task" />
        </div>
    </div>
    <div class="qc-tabs hidden">
        <ul>
            <li><a href="#qc-search" onclick="setTimeout(function(){ calcHeightOfcontentWindow(); }, 20);">Search Parcels</a></li>
            <li class="tip" tip-margin-top="35" abbr="To access any saved queries or templates select the  Templates tab then <br/> select the template to be used and the ‘Use Template’ button.<br/> (Templates are saved by user, not system-wide)."><a href="#qc-saved" onclick="setTimeout(function(){ calcHeightOfcontentWindow(); }, 20);">Templates</a></li>
        </ul>
        <div id="qc-search">
            <div style="height: 10px;">
            </div>
            <div class="create-filter" style="display: none;">
                <div class="b">
                    Add new filter:</div>
                <div>
                    <select class="select field" id="mySelect" onclick="searchFieldsChangedForDTR();">
                    </select>
                </div>
                <div>
                    <select class="select operator" style="margin-top: 6px">
                        <option value="eq">Equal to</option>
                        <option value="ne">Not equal to</option>
                        <option value="gt">Greater than</option>
                        <option value="ge">Greater than / Equals</option>
                        <option value="lt">Lesser than</option>
                        <option value="le">Lesser than / Equals</option>
                        <option value="bw">Between</option>
                        <option value="sw">Starting with</option>
                        <option value="ew">Ending with</option>
                        <option value="nl">Is NULL</option>
                        <option value="nn">Is not NULL</option>
                        <option value="al">All</option>
                    </select>
                </div>
                <div class="values1" style="margin-top: 6px">
                    <select class="value lookup-value">
                    </select>
                    <input type="text" class="value" />
                    <%--<input type="number" class="value" />
                    <input type="date" class="value" />--%>
                    <input type="number" class="value" onkeydown="preventExponentAndPlus(event)" oninput="checkyear(this);"/>
                    <input type="date" class="value" min="1000-01-01" max="9999-12-31"/> 
                </div>
                <div class="value2" style="margin-top: 6px">
                    <select class="value lookup-value">
                    </select>
                    <input type="text" class="value" />
                    <%--<input type="number" class="value" />
                    <input type="date" class="value" />--%>
                    <input type="number" class="value" onkeydown="preventExponentAndPlus(event)" oninput="checkyear(this);"/>
                    <input type="date" class="value" min="1000-01-01" max="9999-12-31"/> 
                </div>
                <div>
                    <button class="filter-add" style="margin-top: 6px">
                        Add Filter</button>
                    <button class="filter-cancel">
                        Cancel</button>
                </div>
            </div>
            <div class="current-filter">
             <div class="andOr-filter" style="display:none;" >
                <span style=" font-size: 9px; margin-left: 145px;"> Choose Method : </span>	<div class="andorswitch">
    					<input type="checkbox" name="andorswitch" class="andorswitch-checkbox" id="myandorswitch" checked>
    					<label class="andorswitch-label" for="myandorswitch">
        					<span class="andorswitch-inner"></span>
        					<span class="andorswitch-switch"></span>
    					</label>
					</div>
                </div>
            <div class="templateName" style="display: none; margin-bottom: 12px;"></div>
                <div class="filter-container" style ="overflow-y:auto; max-height:135px;">
                </div>
                
                <div class="filter-customfilter" style="display:none;">
                	<input type="text" value="" id="custom-filtername" style="width: 264px; margin-bottom: 8px;" disabled>
                	<input type="hidden" id="joinstring"  value="">
                	<input type="hidden" id="Conditionstring"  value="">
                </div>   
                   
                <table class="filter-control-links">
                    <tr>
                        <td>
                            <a class="filters-add-filter">Add filter</a>
                        </td>
                        <td>
                            <a class="filters-clear-filters">Clear filters</a>
                        </td>
                        <td>
                            <a class="filters-adv-filters" style="display: none;">Advance Search</a>
                        </td>
                    </tr>
                </table>
                <div>
                    <div class="b" style="margin-top: 20px;">
                        Random Selection : <span class="randomizer-value">100</span>%</div>
                    <input type="range" min="5" max="100" step="5" style="width: 100%" value="100" class="randomizer" />
                </div>
                <div class="buttons" >
                    <button class="control">
                        Search Parcels</button>
                </div>
                <table class="query-save-links" style="margin-top: 20px;">
                    <tr>
                        <td>
                            <a class="qc-save-template icon-link icon-save tip" div-width="520" tip-margin-top="5" abbr="Searches made through DTR can be saved so the user does not need to build the same search each time the DTR module is accessed. (Searches are saved by user, not system-wide). Saving a template saves the <b>parameters</b> of your query only. For a search by user, and then date range, using the Save Template method is recommended. ">Save Template</a>
                        </td>
                        <td>
                            <a class="qc-save-query icon-link icon-save tip" div-width="590" tip-margin-top="5" abbr="Searches made through DTR can be saved so the user does not need to build the same search each time the DTR module is accessed. (Searches are saved by user, not system-wide). Saving a query saves your parameters as well as the criteria for your search. For a repetitive search with specific criteria that won’t change, using Save Query is suggested.">Save Query</a>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="height: 10px;">
            </div>
        </div>
        <div id="qc-saved">
            <div style="height: 5px;">
            </div>
            <div>
                <div class="qc-saved-edit-name">
                    <input type="text" class="qc-saved-name qc-save-control" style="width: 180px" maxlength="25"
                        disabled="disabled" />
                    <button class="qc-saved-save-name qc-save-control" style="width: 80px; display: none;">
                        Save</button>
                </div>
                <select size="20" style="width: 265px; height:148px;margin-top:3px" class="qc-selected-template standard-input">
                    <optgroup label="Standard Templates">
                        <option value="PROPSEARCH" standard="standard">Property Search</option>
                        <option value="FIELDVISIT" standard="standard">Field Visit</option>
                        <option value="NBHD" standard="standard"><%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName()%></option>
                    </optgroup>
                    <optgroup label="My Templates">
                    </optgroup>
                </select>
                <table class="template-control" style="display: none;">
                    <tr>
                        <td class="l">
                            <button class="qc-delete-template" style="display:none">Delete</button>
                        </td>
                        <td class="r">
                            <button class="qc-use-template b">
                                Use Template</button>
                        </td>
                    </tr>
                </table>
                <a class='qc-cancel-save qc-save-control' style="font-size: 9pt; display: none;">Cancel</a>
            </div>
            <div style="height: 10px;">
            </div>
        </div>
    </div>

    <div style="margin:10px 0px;text-align:center;">
        <a style="font-size:smaller;" class="app-action-reset-cache tip" abbr="For faster search performance, codefiles/lookup tables are cached in each DTR session. Clicking Update System Data will re-cache the lookup tables. 
If a user is logged in to<br/>the DTR for a long duration and changes to codefiles in the CAMA system were made the user would click Update System Data and Search Parcels to update search results."title="Clear cached data and download fresh content from server. Shortcut key: Ctrl + Alt + 0">Update System Data</a>
    </div>
    <asp:Panel runat="server" style="text-align:center;margin:10px 0px;">
        <a runat="server" class="dtr_sketchviewer" id="hlSketchViewer" style="color:white;" onclick="openSketchViewer(); return false;">Open Sketch Viewer</a>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="search-window">
        <div class="grid-container">
            <table class="parcel-results" style="display: none" onselectstart="return false;"
                ondragstart="return false;" selectable="false">
            </table>
        </div>
        <div class="bulk-editor" style="height: 0px; display: none; overflow-y: auto;">
            <div class="content-block-header">
                <div class="ftitle">
                    Bulk Edit Parcels</div>
            </div>
            <div class="bulk-edit-form">
                <table>
                    <tr>
                        <td style="width: 120px;" class="tip" abbr='Update all parcels in the search results list to High or Urgent priority <br/> by selecting the priority and “Update all parcels." Clear the priority from all <br/> parcels in the search results list by setting back to Normal and "Update all parcels."'>
                            Field Visit Priority:
                        </td>
                        <td style="width: 380px">
                            <select class="bulk-priority">
                                <%If EnableNewPriorities Then %>
                                    <option value="0">Proximity</option>
                                    <option value="1">Normal</option>
                                    <option value="2">Medium</option>
                                    <option value="3">High</option>
                                    <option value="4">Urgent</option>
                                    <option value="5">Critical</option>
                                <%Else %>
                                    <option value="0">Normal</option>
                                    <option value="1">High</option>
                                    <option value="2">Urgent</option>
                                <%End If %>
                            </select>
                        </td>
                        <td style="width: 120px;">
                            Bulk QC:
                        </td>
                        <td class="bulk-op-links">
                            <a style="color: Green;" class="tip" abbr="All properties in the search parcel results list will be downsynced to the local CAMA database." onclick="return bulkQC(1);">Set All Approved</a> <a style="color: Red;" class="tip"
                              abbr="All properties in the parcel search results list will have <u><b>all</b></u> changes made on the field device deleted. This should be accompanied with a priority change to Urgent and an alert message with instructions on action needed for the properties in the list."  onclick="return bulkQC(0);">Set All Rejected</a> <a style="color: Gray;display:inline-block;" class="tip" abbr="Clear any prior DTR action of  Approval or Rejection through the ‘Recheck All (Not Set)’ action. This is the same as<br/>the property-level ‘Not Set’ button." onclick="return bulkQC(-1);">
                                    Recheck All (Not Set)</a>
                        </td>
                    </tr>
                    <tr class="bulk-alert-message-row">
                        <td class="tip" abbr="Set the priority for properties and add an alert message indicating the purpose<br/>of the property visit by typing the message in this field."rowspan="3">
                            Alert Message:
                        </td>
                        <td rowspan="3">
                            <textarea class="bulk-alert-message" style="" maxlength="500"></textarea>
                        </td>
                        <td>
                            Field Review Status:
                        </td>
                        <td class="bulk-op-links">
                            <a class="tip" abbr="This is a mock mark as complete. Selecting ‘Set All Reviewed’ will update all properties with a Review Date with the current date and update Reviewed By with the DTR user logged in."onclick="return bulkReview(1);">Set All Reviewed</a> <a onclick="return bulkReview(0);"
                                class="tip" abbr="This will clear the mark as complete/Review Date flag from the properties. Clearing the Review Date flag is the equivalent of a Soft Reject. Clear the Review Date, update the priority and alert message for the properties to communicate additional action needed by the appraiser." style="color: Gray;">Set None Reviewed</a>
                        </td>
                    </tr>
                    <tr>
                        <td>Desk Review Status: </td>
                        <td class="bulk-op-links">
                            <asp:DropDownList runat="server" ID="ddlBulkDTRStatus" Width="150px" ClientIDMode="Static" CssClass="bulk-dtr-status" />&nbsp;
                            <a onclick="return bulkDTRStatus();">Set Status for All</a> 
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp; </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="tip" abbr="Selecting the Edit Notes button will make a box available to type and update<br/>field appraiser notes (Data Collection Card > Quick Review > Alert From Office message box)." colspan="2">
                            <input type="checkbox" id="editNotes"  onclick="return toggleEditNotes();" />&nbsp;Edit
                           <%-- Notes / Alert from Office--%>  Notes / Alert from Field
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td class="bulk-op-links">
                            <a class="tip" abbr='Clear all notes from the appraiser from prior<br/>property visits by selecting “Clear All Notes."' onclick="return bulkNotes(0);">Clear All Notes</a>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr class="bulk-Notes-row" style="display: none;">
                        <td>
                            Notes:
                        </td>
                        <td>
                            <textarea class="bulk-Notes" style=""></textarea>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td class="bulk-op-links">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <button class="updateBulk" style="width: 160px; height: 28px; font-weight: bold;"
                                onclick="return bulkUpdateAll();">
                                Update all parcels</button>
                            <button style="width: 70px; height: 28px; font-weight: normal;" onclick="return closeBulkEditor();">
                                Close</button>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </div>
            <div id="progressBar" style="display:none; position: relative; width: 100%; "><div id="currentProgress" style="position:relative; width:0%; height:100%; background:#2164df;"></div></div>
        </div>
    </div>
    <div class="parcel-details hidden">
        <div class="parcel-toolbar">
            <table>
                <tr>
                    <td>
                        <button onclick="return saveParcelChanges();" class="btn-save-changes">
                            Save Changes</button>
                        <button onclick="return closeParcelView();">
                            Close</button>
                    </td>
                    <td class="r">
                        <button class='ReviewSet tip'  tip-margin-top="10" abbr="This button toggles between Set as Reviewed and Set as Not Reviewed. If the button shows Set as Reviewed it means the property has not yet been marked as complete. When the button shows Set as Not Reviewed it means the property <b>has</b> been marked as complete." onclick="return doreview();" style='cursor: pointer;'>
                            Reviewed
                        </button>
                        <%If Roles.IsUserInRole("DTR-Manager") Then%>
                        <button  class="tip"  tip-margin-top="10" abbr="<span style='color:red'>Delete all data changes</span> made by the appraiser and make property available on the field device." onclick="return doQCAction(this);" qc="0">
                            Reject</button>
                        <%End If%>
                        <button class="nav-parcel nav-parcel-prev" onclick="return openPreviousParcel();">
                            Previous Parcel</button>
                         <strong><span class="parcel-result-count"></span></strong>
                        <button class="nav-parcel nav-parcel-next" onclick="return openNextParcel();">
                            Next Parcel</button>
                    </td>
                </tr>
            </table>
        </div>
        <div class="parcel-head release-category-menu">
            <div class="parcel-photo-thumb">
                <div class="photo-thumb">
                </div>
            </div>
            <table class="parcel-head-values">
                <tr>
                    <td colspan="4" class="value bigtext bottom-border" fieldname="StreetAddress" style="height:22px;">
                        Street Address comes here
                    </td>
                </tr>
                <tr id ="label">
                    <td class="label">
                        <%=If(CAMACloud.Data.Database.Tenant.Application.ShowKeyValue1, "Property ID", alterNateField)%>
                    </td>
                    <td class="label"></td>
                    <td class="label"></td>
                    <td class="alternate"><%=If(CAMACloud.Data.Database.Tenant.Application.ShowAlternateField, alterNateField, "")%></td>
                    
                </tr>
                <tr id ="fld">
                    <td class="value medtext rightpad" style="/* white-space: pre */" fieldname="KeyValue1"  id ="kyf">
                        00000000
                    </td>
                    <td class="value medtext rightpad" fieldname="KeyValue2">
                        000-0000-000-000
                    </td>
                    <td class="value medtext rightpad" fieldname="KeyValue3">
                        00000000
                    </td>
                    <td class="value medtext rightpad" fieldname="Alternatekeyfieldvalue"  id="altf">
                        00000000
                    </td>
                </tr>
            </table>
            <div class="dslw-tabs">
                <table class="Desk_Review_Status">
                    <tr>
                        <td style="width: 120px;">
                            Desk Review Status :
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlSelectedStatus" CssClass="dtr-selected-status" runat="server"
                                onchange="markDashboardChanged();">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <table class="tab_selected_estimate">
                    <tr>
                        <td style="width: 120px;">Selected Estimate :
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlSelectedEstimate" CssClass="selected-estimate" runat="server"
                                onchange="markDashboardChanged();">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <table class="lastReviewedMain" width='250px;'>
                    <tr>
                        <td style="width: 118px;">
                            Last QC Reveiwed :
                        </td>
                        <td>
                            <div class="lastReviewedValue">
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div>
                <div class="work-flow-div" style="display: none;"></div>
            </div>
            <div class="parcel-qc-pad">
            <%If Roles.IsUserInRole("DTR-Manager") Then%>
                <button class="qc-notset tip" tip-margin-top="10" abbr="Clear previous DTR actions." qc="-1">
                    NOT SET</button>
                <button class="qc-passed tip" tip-margin-top="10" abbr="Approve the property for downsync to the local CAMA database.<br/>Once Approved, this button will be green." qc="1">
                    APPROVE</button>
                <button class="tip" tip-margin-top="10" abbr="Remove the mark as complete flag and make property available<br/>on the field device for further modifications by the appraiser."onclick="return doSoftReject();">
                    Soft Reject</button>
            <%End If%>
            </div>
        </div>
        <div class="tabs-main">
            <ul class="tabs" data-persist="true">
                <li><a href="#parceldata" onclick="openCategory('dashboard', this.innerHTML);$('.category-menu').hide();return false;"
                    category="dashboard">Dashboard</a></li>
                <%-- <li><a href="#parceldata" onclick="openCategory('1', this.innerHTML);$('.category-menu').show();return false;" category="1">Parcel Data</a></li>--%>
                <asp:Literal ID="ltParceltab" runat="server"></asp:Literal>
                <li style="display: none !important"><a href="#parceldata" onclick="openCategory('Neighborhood', this.innerHTML);$('.category-menu').hide();return false;"
                    category="neighborhood">
                    <%--<%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName()%--%>></a></li>
                <li category="digital-prc" style='position:relative'>
                    <a href="#parceldata" onclick="openCategory('digital-prc', this.innerHTML);$('.category-menu').hide();return false;" category="digital-prc" style="padding-right: 21px;">Digital PRC</a>
                    <span class="audit-lock audit-lock-unlock" style="display:none;" onclick="auditLock(); return false;"></span>
                    <span class="btnPopUp"; onclick="loadParcelPRC();return false;">&#10063;</span>
                </li>
                <li sketch class="tip"  tip-margin-top="10" abbr="The sketch tab will display the property sketch, with any segments that had changes highlighted in orange.<br/> Selecting the Open Sketch Editor button will open the sketch editor and allow the DTR user to make modifications to the sketch."sketch-frame><a href="#parceldata" onclick="openCategory('sketch-manager', this.innerHTML);$('.category-menu').hide();return true;"
                    category="sketch-manager">Sketch</a></li>
                <li class="tip" tip-margin-left="270"  tip-margin-top="10" abbr="GIS Map View will show the subject property’s location on the map."><a href="#parceldata" onclick="openCategory('mapview', this.innerHTML);$('.category-menu').hide();return false;"
                    category="mapview">GIS Map View</a></li>
                <li class="tip"  tip-margin-top="10" abbr='The Audit Trail will display the 75 most recent changes made on the property. The audit trail shows field level changes and the after values for each change. A line stating: "Parcel changes committed via SyncService/API (12 changes) will indicate that the changes made in the field have been synced back to the local CAMA database. A line stating "4 photo(s) transferred via SyncService/API" will indicate that the parcel photos have been synced back to the local CAMA database.'><a href="#parceldata" onclick="openCategory('audit-trail', this.innerHTML);$('.category-menu').hide();return false;"
                    category="audit-trail">Audit Trail</a></li>
                <li category="photo-viewer" style='position:relative'><a href="#parceldata" onclick="openCategory('photo-viewer', this.innerHTML);showNoTabs(); deSelectAll();showAllPhotos(); $('.category-menu').hide();return false;"
                    category="photo-viewer" style="padding-right: 21px;">Parcel Photos</a><span class="btnPopUp"; onclick="loadParcelPhotos();return false;">&#10063;</span></li>
                <li category="nbhd-profile"><a href="#parceldata" onclick="openCategory('nbhd-profile', this.innerHTML);$('.category-menu').hide();return false;">
                    Assignment Group Profile </a></li>
            </ul>
        </div>
        <div class="parcel-data-head">
            <!-- This section commented by MSP on 30-08-2013
            <a class="menu-trigger" status="off"></a>
            -->
            <div class="parcel-data-title release-category-menu">
                Parcel Dashboard
            </div>
             <span class="smart-screen screen-up" onclick="return expandScreenUp()"></span>
            <span class="smart-screen screen-down" onclick="return shortenScreen()"></span>
            <input id="printPRC" class='btn btn-2 btn-2g' type="button" onmousedown="prtPRC('.digital-prc', 'DigitalPRC')"
                value="Print" style='float: right; margin: 8px 45px 0px 13px; padding: 1px 15px;
                display: inline' />
                <div class="sketch-details hidden" style='float: right;visibility:visible;margin: 2px 45px 0px 13px; padding: 1px 15px;display: inline'>
              <table>
                    <%--<tr style='width:60vw'>--%><tr>
                        <td>
                            Select Sketch/Segments :
                        </td>
                        <td >
                            <select  class="select-sketch-segments" onchange="showSketchesInQC(false,previousSketch)" style="width:140px;margin-right:30px;">
                            </select>
                        </td>
                        <td><div style='width:120px;'><a class="before-sketch" mode="1" style="padding:0px 32px 0px 1px;cursor: default;" onclick="showPreviousSketch($('.before-sketch').attr('mode'))"> Sketch Before</a></div></td>   
                        <td>
                            <input id="printSketch" class='btn btn-2 btn-2g' type="button" onmousedown="prtSketch()" value="Print" /> 
                        </td>
                    </tr>
                </table>

          </div>  
            <label ID="nostreet" class="nostreetview">No Street view available in this range</label>
            <input id="customwms" class='customwms btn btn-2 btn-2g' type="button" onclick="loadCustomWMS();"
                value="Show Imagery" style='float: right;margin-top: 6px;' />
             <input id="woolpert" class='woolpert btn btn-2 btn-2g' type="button" onclick="loadWoolpert();"
                value="Show Woolpert Imagery" style='float: right; margin: 6px 0px 0px 11px' />
            <input id="pictometry" class='pictometry btn btn-2 btn-2g' type="button" onclick="loadPictometry();"
                value="Show in EagleView" style='float: right; margin: 6px 0px 0px 11px;' />
            <input id="eagleView" class='eagleView btn btn-2 btn-2g' type="button" onclick="loadEagleView();"
                value="Show in EagleViewNew" style='float: right; margin: 6px 0px 0px 11px;' />
            <input id="nearmap" class='nearmap btn btn-2 btn-2g' type="button" onclick="loadNearmap();"
                value="Show in Nearmap" style='float: right; margin: 6px 0px 0px 11px;' />
            <input id="nearmapWMS" class='nearmapwms btn btn-2 btn-2g' type="button" onclick="loadNearmapWMS();"
                value="Show in WMS Nearmap" style='float: right; margin: 6px 0px 0px 11px;' />
            <input id="cyclomedia" class='cyclomedia btn btn-2 btn-2g' type="button" onclick="loadCyclomedia();"
                value="Show in Cyclomedia" style='float: right;margin-top: 6px; display:none;' />
            <input id="sanborn" class='sanborn btn btn-2 btn-2g' type="button" onclick="loadSanborn();"
                value="Show in Sanborn" style='float: right;margin-top: 6px; display:none;' />
            <input id="qPublic" class='qPublic btn btn-2 btn-2g' type="button" onclick="loadQPublic();"
                value="Show in qPublic" style='float: right;margin-top: 6px; display:none;' />
            <div class="photo-Manager hidden">
                <table>
                    <tr>
                    	<td><button type="button" id="DownloadSelectedPh" class="photocss" onclick="downloadSelectedPhotos()">Download Selected</button></td>
                    	<td><button type="button" id="SelectAllPh" class="photocss" onclick="checkAll(this)">Select All</button></td>
                    	<td><button type="button" id="AddPh" class="photocss" onclick="photUploader()">Add Photo</button></td>
                    	<td><button type="button" id="DeletePh" class="photocss" onclick="deleteAllSelectedPhoto()">Delete</button></td>
                   <!--   <td class="btn btn-2 btn-2g-hdn">
                            <a href="#parceldata" id="DownloadSelectedPhhid" class="photo-button photo-download-button"></a>
                            </td>-->
                        <!-- <td class="btn btn-2 btn-2g" onclick="downloadSelectedPhotos()" style="display:none;">
                            <a href="#parceldata" id="DownloadSelectedPh">Download Selected</a>
                        </td>
                        <td class="btn btn-2 btn-2g" onclick="checkAll(this)">
                            <a href="#parceldata" id="SelectAllPh">Select All</a>
                        </td>
                        <td class="btn btn-2 btn-2g" onclick="photUploader()">
                            <a href="#parceldata" id="AddPh"></a>Add Photo
                        </td>
                        <td class="btn btn-2 btn-2g" onclick="deleteAllSelectedPhoto();">
                            <a href="#parceldata" id="DeletePh">Delete</a>
                        </td> -->
                        <!--  <td class="btn btn-2 btn-2g" onclick=""><a href="#parceldata" id="DwloadPh" >Download</a></td> 
                  <td class="btn btn-2 btn-2g"><a href="#parceldata" id="ResyncPh" onclick="makeDownSynced(this);">Resync</a></td> -->
                    </tr>
                </table>
            </div>
            <div class="photo-Manager-Back hidden">
                <table>
                    <tr>
                    	<td><button type="button" id="A1" class="photocss" onclick="backPhotoMang();">Back</button></td>
                        <!--<td class="btn btn-2 btn-2g" onclick="backPhotoMang();">
                            <a href="#parceldata" id="A1">Back</a>
                        </td>-->
                    </tr>
                </table>
            </div>
            <div id="AllCategories" style="display: none;">
                <ul>
                    <asp:Repeater runat="server" ID="rpFieldAllCategories">
                        <ItemTemplate>
                            <%--<a href="#" onclick='openCategory(<%# Eval("Id") %>, this.innerHTML);return false;'
                            category='<%# Eval("Id") %>' parentid='<%# Eval("ParentCategoryId") %>' style="text-decoration: none;
                            padding-left: 10px; display: none;" class="submenu-child field-category-link">
                            <%# Eval("Name")%></a>--%>
                            <li category='<%# Eval("Id")%>' <%# IIf(Eval("ParentCategoryId").Equals(DBNull.value), "", "parentId="&Eval("ParentCategoryId")) %>
                                <%# IIf(Eval("ChildCategoryId").Equals(DBNull.value), "", "childId="&Eval("ChildCategoryId")) %>
                                <%# IIf(Eval("NodeFilterField").Equals(DBNull.value), "", "NodeFilterField="&Eval("NodeFilterField")) %>>
                                <%# Eval("Name")%>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </div>
            <%-- <div class="clear">
            </div>--%>
        </div>
        <div class="parcel-data-panel">
            <!-- This Region commented by MSP on 30-08-2013
            <div class="category-menu hidden">
          -->
            <div class="tabcontents">
                <div id="parceldata">
                    <div class="category-menu" style="overflow-x: auto; overflow-y: auto;">
                        <%--    <a href="#" onclick="openCategory('dashboard', this.innerHTML);return false;" category="dashboard">
                    Parcel Dashboard</a>--%>
                        <asp:Repeater runat="server" ID="rpFieldCategory">
                            <ItemTemplate>
                                <a href="#" onclick='openCategory(<%# Eval("Id") %>, this.innerHTML);return false;'
                                    category='<%# Eval("Id") %>' class="field-category-link" isreadonly='<%# Eval("IsReadOnly")%>'
                                    allowadddelete='<%# Eval("AllowAddDelete") %>'>
                                    <%# Eval("Name")%>
                                </a>
                            </ItemTemplate>
                        </asp:Repeater>
                        <%--                <a href="#" onclick="openCategory('photo-viewer', this.innerHTML);showPhoto(0);return false;"
                    category="photo-viewer">Parcel Photos</a> --%>
                        <%-- <a href="#" onclick="openCategory('sketch-manager', this.innerHTML);return false;"
                        category="sketch-manager" style="display: none;">Sketch Manager</a>--%>
                        <%--This region commented by MSP on 30-08-2013 5.40 PM 
                        <a href="#" onclick="openCategory('mapview', this.innerHTML);return false;"
                            category="mapview">GIS Map View</a>--%>
                    </div>
                    <div class="parcel-data-category-container release-category-menu parcel-data-view"
                        style="position: relative; overflow-x: hidden; overflow-y: auto;">
                        <cc:NeighborhoodProfile ID="nbhdprofiletemp" runat="server" />
                        <div class="category-page digital-prc hidden" categoryid="digital-prc">
                            <div class="digital-prc-card">
                            </div>
                        </div>
                        <div class="nbhd-profile-area " style="background-color: #fff !important; height: 0px; padding: 0px;">
                        </div>
                        <div class="pdc-margin">
                            <div class="category-page hidden" categoryid="dashboard">
                                <table class="dashboard-layout">
                                    <tr>
                                        <td style="padding-left: 4%; width: 42%;">
                                            <div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 25px; max-width: 42%;">
                                            <div class="dashboard-title">
                                                <table width='100%'>
                                                    <tr>
                                                        <td>
                                                            Parcel Class Control
                                                        </td>
                                                        <td style='font-size: 12px; font-weight: 200; width: 35%'>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <table>
                                                <tr>
                                                    <td style="width: 120px;">
                                                        Field Visit Priority:
                                                    </td>
                                                    <td>
                                                        <select class="parcel-priority" onchange="markDashboardChanged(this);">
                                                            <%If EnableNewPriorities Then %>
                                                                <option value="0">Proximity</option>
                                                                <option value="1">Normal</option>
                                                                <option value="2">Medium</option>
                                                                <option value="3">High</option>
                                                                <option value="4">Urgent</option>
                                                                <option value="5">Critical</option>
                                                            <%Else %>
                                                                <option value="0">Normal</option>
                                                                <option value="1">High</option>
                                                                <option value="2">Urgent</option>
                                                            <%End If %>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr class="parcel-alert-message-row">
                                                    <td>
                                                        Alert from Office:
                                                    </td>
                                                    <td>
                                                        <textarea class="parcel-alert-message" style="" onchange="markDashboardChanged(this);" maxlength="500"></textarea>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="dashboard-title">
                                                Appraiser's Feedback</div>
                                            <table class="field-review-results">
                                                <%--<tr>
                                                    <td style="width: 120px;">
                                                        Selected Estimate:
                                                    </td>
                                                    <td>
                                                      
                                                      <asp:DropDownList ID="ddlSelectedEstimate" CssClass="selected-estimate" runat="server"  onchange="markDashboardChanged(this);"></asp:DropDownList>
                                                    </td>
                                                </tr>--%>
                                                <tr>
                                                    <td style="width: 120px;">
                                                        Alert from Field:
                                                    </td>
                                                    <td>
                                                        <span class="field-alert-type"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Notes:
                                                    </td>
                                                    <td>
                                                        <span class="field-alert-notes" style="display: block;width: 400px;word-wrap: break-word;"></span>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div class="cautionMessageMain" style="width: 100%;">
                                                <div style="float: left;font-weight: bold;">Caution Alert:&nbsp&nbsp;  
                                                </div>
                                                <span class="caution-Message" style="word-wrap: break-word;width:510px;display: block;">
                                                </span>
                                            </div>
                                            <div class="parcelRejectMain" style="width: 100%;">
                                                <div style="float: left; color: Red; font-weight: bold;">
                                                    Parcel rejected on CAMA DownSync, reason is :
                                                </div>
                                                <div class="parcelRejectReason" style="color: Red; overflow: hidden; float: left; max-width: 500px;">
                                                </div>
                                            </div>
                                              <div class="CC_error_div" style="width: 100%;">
                                                <div style="float: left; color: Red; font-weight: bold;">
                                                    CC Errors:
                                                </div>
                                                <div class="CC_error_message" style="color: Red; overflow: hidden; float: left">
                                                </div>
                                            </div>
                                        </td>
                                        <td class="recent-audit-trail" style="width: 41%; margin-left: 4%;">
                                            <div class="recent-at-title">
                                                Recent Changes</div>
                                            <table class="recent-audit-trail-table">
                                                <tbody>
                                                </tbody>
                                                <tfoot class="hidden">
                                                    <tr>
                                                        <td>
                                                            <div class="audit-info">
                                                                ${AuditTime} - <b>${AuditUser}</b></div>
                                                            <div class="audit-detail">
                                                                <a style="cursor: pointer; text-decoration: undeline; color: #999999;" onclick="focusOnField(${CorrespondingChangeId});"
                                                                    correspondingchangeid="${CorrespondingChangeId}">${Description} </a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="category-page photo-viewer hidden" categoryid="photo-viewer">
                                <div class="photo-frame-allPhotos" style="padding-top: 10px; overflow: auto; overflow-x: hidden;
                                    height: 100%;">
                                </div>
                                <div class="photo-frame hidden">
                                    <a class="photo-nav-button nav-left" onclick="prevPhoto();"></a>
                                    <!--<a class="photo-button photo-delete-button"
                                        onclick="deleteCurrentPhoto();"></a>-->
                                    <a class="photo-button mainImage-button hidden"></a>
                                    <a class="photo-button photo-download-button"></a><a class="photo-nav-button nav-right"
                                        onclick="nextPhoto();"></a>
                                    <a class="photo-button photo-properties-button" onclick="showPhotoProperties();"></a>
                                    <a class="photo-button photo-recover-button hidden" onclick="recoverDeletedPhoto();"></a>
                                    <a class="photo-button setAsMain-button hidden" onclick="setAsMainImage();"></a>
                                </div>
                                <!--<div style='width: 20%; float: right;'>
                                    <span class='ccimageId' style='position: absolute; margin-top: 10px; display: none'></span>
                                </div>-->
                                <!--<div id="comploader">
                                    <div id="bgcomploader">
                                    </div>
                                </div>-->

                                <div id="photo-properties-frame" style="display: none" class="parcel-field-values">
                                    <a class="photo-nav-button nav-left" onclick="prevPhotoProperties();"></a>
                                    <a class="photo-nav-button nav-right" onclick="nextPhotoProperties();"></a>
                                    <table style="margin-left: auto; margin-right: auto;">
                                        <tr>
                                            <td>
                                                <div class="photo-preview" style="display: none;">
                                                    <div id="comploader" style="width: 374px; height: 204px; position: relative">
                                                        <div id="bgcomploader"></div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="parcel-field-values" style="margin-left: auto; margin-right: auto;">
                                        <asp:Repeater runat="server" ID="rpPhotoProperties">
                                            <ItemTemplate>
                                                <tr hidedonotshowph='<%# Eval("DoNotShowOnDE") %>'>
                                                    <td class="label" style="padding-left: 50px;">
                                                        <span><%# Eval("DisplayLabel")%></span> <span class="ColoumnSep">:</span>
                                                        <span class="required">*</span>
                                                        <span class="infoButton" onclick="showInfoContent(this);"></span>
                                                    </td>
                                                    <td class="value" fieldid='<%# Eval("Id") %>' fieldname='<%# Eval("Name") %>' datatype='<%# Eval("DataType") %>' assignedname='<%# Eval("AssignedName")%>'>
                                                        <asp:TextBox runat="server" ID="txt" CssClass="input trackchanges" Width="250px"
                                                            Visible='<%# ShowFieldForDataType(Eval("DataType"),"txt") %>' ReadOnly='<%#IsFieldReadOnly() %>' />
                                                        <asp:TextBox runat="server" ID="ltxt" CssClass="input trackchanges" Width="400px"
                                                            Rows="2" TextMode="MultiLine" Visible='<%# ShowFieldForDataType(Eval("DataType"),"ltxt") %>'
                                                            ReadOnly='<%#IsFieldReadOnly() %>' onkeydown="setHeight(this);" />
                                                        <asp:TextBox runat="server" ID="number" CssClass="input trackchanges" Width="250px"
                                                            type='<%# SelectTypeWithReadOnlyStatus(Eval("IsReadOnly"), Eval("IsCalculated"), Eval("IsReadOnlyDTR"), IIf(Eval("CategoryId").Equals(DBNull.Value), "", Eval("CategoryId"))) %>'
                                                            Visible='<%# ShowFieldForDataType(Eval("DataType"),"num") %>' ReadOnly='<%#IsFieldReadOnly() %>' />
                                                        <asp:TextBox runat="server" ID="datevalue" CssClass="input trackchanges" Width="250px"
                                                            type="date" Visible='<%# ShowFieldForDataType(Eval("DataType"),"date") %>' Enabled='<%# IsFieldEnabled() %>' />
                                                        <asp:DropDownList runat="server" ID="ddl" CssClass="input trackchanges" Width="256px"
                                                            Visible='<%# ShowFieldForDataType(Eval("DataType"),"ddl") %>'
                                                            IsListBox='<%# IIf(Eval("DataType") = 11, True, False) %>'
                                                            Enabled='<%# IsFieldEnabled() %>' />
                                                        <asp:TextBox runat="server" ID="ddlSpan" CssClass="input trackchanges cc-drop cusDpdownSpan photoCusDown" Visible='<%# ShowFieldForDataType(Eval("DataType"), "lookup") %>'
                                                            IsListBox='<%# IIf(Eval("DataType") = 11, True, False) %>'
                                                            Enabled='<%# IsFieldEnabled() %>'>
                                                        </asp:TextBox>
                                                        <asp:DropDownList runat="server" ID="yesno" CssClass="input trackchanges" Width="60px"
                                                            Visible='<%# ShowFieldForDataType(Eval("DataType"), "yesno") %>' Enabled='<%# IsFieldEnabled() %>' EnableViewState="false">
                                                            <asp:ListItem Text="No" Value="false"></asp:ListItem>
                                                            <asp:ListItem Text="Yes" Value="true"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RadioButtonList ID="TriStateRadio" runat="Server" CssClass="input trackchanges" Width="140px" Visible='<%# ShowFieldForDataType(Eval("DataType"), "TriStateRadio") %>'
                                                            Enabled='<%# IsFieldEnabled() %>' RepeatDirection="Horizontal" RepeatLayout="Flow">
                                                            <asp:ListItem Text="Yes" Value="true"></asp:ListItem>
                                                            <asp:ListItem Text="No" Value="false"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                        <i class="cusDpdown-arrow" onclick="return openCustomlkd(this, true)" <%# haveMoreField(Eval("Id"), Eval("DataType"))%> style="display: none"></i><span class="qc" fieldid='<%# Eval("Id") %>'><a class="check">!</a> <a class="passed">a</a> </span>
                                                        <a class="LoadGeolocationIcon" onclick="return GeolocationLoad(this);" style="display: none;"></a>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </table>

                                    <div class="photo-details" style="postion: absolute;">
                                        <span class='downsycset' style='display: none; margin-left: 100px;'>
                                            <input id="Checkbox1" class="photo-DownSynced-button" onclick="makeDownSynced(this);"
                                                runat="server" type="checkbox" name="vdvsdsvd" />Flag for Resync</span>
                                        <span class='referenceId' style='display: none;'></span>
                                        <span class='ccimageId' style='display: none; margin-left: 40px; float: right; margin-right: 8%; margin-bottom: 20px'></span>
                                    </div>
                                </div>

                                <%If Not Roles.IsUserInRole("DTR-ReadOnly") Then%>
                                    <div class="add-photo-frame hidden" style='width: 100%; float: left;'>
                                        <iframe id="photouploader" frameborder="0" style='width: 70%; float: left' src="/protected/tools/upload-photo.aspx"
                                            scrolling="no"></iframe>
                                    </div>
                                <%End If %>
                            </div>
                        </div>
                        <div class="category-page sketch-manager hidden" categoryid="sketch-manager">
                        <input class="hidden" value="<%=CAMACloud.Data.Database.Tenant.Application.KeyField(1)%>" id='keyvalue1'/>
                        <input class="hidden" value="<%=CAMACloud.Data.Database.Tenant.Application.NeighborhoodField%>" id='nbhdname'/>
                        <div class="sketch-details-view">
                          <div class="sketch-frame">
                          </div>
                        </div>
                            <div class="add-sketch-frame" style="text-align: center;">
                                <button onclick="return openSketchEditor(previousSketch);" style=" margin-top:  10px;">
                                    Open Sketch Editor</button>
                            </div>
                            <div class="all-sketch-frame hidden">
                             </div>
                        </div>
                        <div class="category-page mapview hidden" categoryid="mapview">
                            <div class="googlemap" id="googlemap">
                                Google Map</div>
                                <div class="custommap" id="custommap">
                                </div>
                        </div>
                        <asp:Repeater runat="server" ID="rpCategoryForFields">
                            <ItemTemplate>
                                <asp:HiddenField runat="server" ID="hCID" Value='<%# Eval("Id") %>' />
                                <div class="category-page hidden" categoryid='<%# Eval("Id") %>' <%# IIf(Eval("TabularData"), "auxdata='" + Eval("SourceTable") + "'", "parceldata") %>>
                                    <div class="grid-view" categoryid='<%# Eval("Id") %>'></div>
                                    <div class="data-navigator" style="height: 26px; position: absolute; width: 100%;
                                        top: 0; z-index: 20; padding-top: 7px; padding-left: 10px;">
                                        <button class="grid-view-qc" categoryid='<%# Eval("Id") %>'>&#x0229E;</button>
                                        <button class="move-first" ddir="first">
                                            &#9668;&#9668;</button>
                                        <button class="move-prev" ddir="prev">
                                            &#9668;</button>
                                        <span class="records-indicator">Showing record <span class="aux-index">0</span> of <span
                                            class="aux-records">0</span></span>
                                        <button class="move-next" ddir="next">
                                            &#9658;</button>
                                        <button class="move-last" ddir="last">
                                            &#9658;&#9658;</button>
                                        <button class="new-item" action="new" allowdeleteonly = '<%# Eval("AllowDeleteOnly") %>' allowadddelete='<%# Eval("AllowAddDelete") %>'
                                            isreadonly='<%# Eval("IsReadOnly") %>'>
                                            &#9679;</button>
                                        <button class="del-item" action="del" allowdeleteonly = '<%# Eval("AllowDeleteOnly") %>' allowadddelete='<%# Eval("AllowAddDelete") %>'
                                            isreadonly='<%# Eval("IsReadOnly") %>' DoNotAllowDeleteFirstRecord='<%# Eval("DoNotAllowDeleteFirstRecord")%>'>
                                            &#935;</button>
                                        <button class="classCalc" style="width: 22px; height: 22px; padding: 4px; display: inline-block; background-image: url(/App_Static/css/images/calculator.png); margin-right: 19px; background-size: 100%; background-repeat: no-repeat;"></button>
                                    </div>
                                    <div class='heightdiv' style='height: 40px;'>
                                    </div>
                                    <div class="sublevelDiv" style="width: 100%; height: 17px;">
                                        <div style="float: left; margin: 0px 10px; display: none;" class="parentCategoryDiv">
                                            <a class="parentCategoryTitle" nodetype="parent" onclick="openSublevel(this);" style="cursor: pointer;">
                                                parent </a><span>&lt;</span>
                                        </div>
                                        <div style="float: left; margin: 0px 10px">
                                            <a class="currentCategoryTitle" style="cursor: no-drop; color: #d3d3d3;">Current
                                            </a>
                                        </div>
                                        <div class="childDiv" style="float:left" >
                                            <span>&gt;</span>
                                            <div class="childCategoryDiv" style="display: none;">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="auxrecover">
                                        This record has been deleted by the field agent.&nbsp;<span rec onclick="return recoverAuxRecord(this);">Click
                                            here</span>&nbsp; to recover or undo this delete. <span class="ui-icon ui-icon-closethick"
                                                close onclick="return closeAuxRecover();">close</span>
                                    </div>
                                    <table class="parcel-field-values">
                                        <asp:Repeater runat="server" ID="rpFields">
                                            <ItemTemplate>
                                                <tr>
                                                    <td class="label">
                                                        <span>
                                                            <%# Eval("DisplayLabel")%></span> <span class="ColoumnSep">:</span> <span class="required">
                                                                *</span>
                                                        <span class="infoButton" onclick="showInfoContent(this);"></span>
                                                    </td>
                                                    <td class="value" fieldid='<%# Eval("Id") %>' fieldname='<%# Eval("Name") %>' datatype='<%# Eval("DataType") %>'>
                                                        <asp:TextBox runat="server" ID="txt" CssClass="input trackchanges" Width="250px"
                                                            Visible='<%# ShowFieldForDataType(Eval("DataType"), "txt") %>' ReadOnly='<%#IsFieldReadOnly() %>' />
                                                        <asp:TextBox runat="server" ID="ltxt" CssClass="input trackchanges" Width="400px"
                                                            Rows="4" TextMode="MultiLine" Visible='<%# ShowFieldForDataType(Eval("DataType"), "ltxt") %>'
                                                            ReadOnly='<%#IsFieldReadOnly() %>' />
                                                        <asp:TextBox runat="server" ID="number" CssClass="input trackchanges" Width="250px"
                                                            type='<%# SelectTypeWithReadOnlyStatus(Eval("IsReadOnly"), Eval("IsCalculated"), Eval("IsReadOnlyDTR"), IIf(Eval("CategoryId").Equals(DBNull.Value), "", Eval("CategoryId"))) %>'
                                                            Visible='<%# ShowFieldForDataType(Eval("DataType"),"num") %>' ReadOnly='<%#IsFieldReadOnly() %>' />
                                                        <asp:TextBox runat="server" ID="datevalue" CssClass="input trackchanges" Width="250px"
                                                            type="date" Visible='<%# ShowFieldForDataType(Eval("DataType"), "date") %>' Enabled='<%# IsFieldEnabled() %>' ReadOnly='<%#IsFieldReadOnly() %>' />
                                                        <asp:DropDownList runat="server" ID="ddl" CssClass="input trackchanges" Width="256px"
                                                            Visible='<%# ShowFieldForDataType(Eval("DataType"), "ddl") %>' LookupTable='<%# Eval("LookupTable") %>'
                                                            LookupShowNullValue='<%# Eval("LookupShowNullValue") %>' IsListBox='<%# IIf(Eval("DataType") = 11, True, False) %>'
                                                            OnLoad="LookupDropdown_Load" Enabled='<%# IsFieldEnabled() %>' ReadOnly='<%#IsFieldReadOnly() %>' />
                                                        <asp:TextBox runat="server" ID="ddlSpan" CssClass="input trackchanges cc-drop cusDpdownSpan"
		                                                    Visible='<%# ShowFieldForDataType(Eval("DataType"), "lookup") %>' LookupTable='<%# Eval("LookupTable") %>'
		                                                    LookupShowNullValue='<%# Eval("LookupShowNullValue") %>' IsListBox='<%# IIf(Eval("DataType") = 11, True, False) %>'
		                                                    Enabled='<%# IsFieldEnabled() %>' ReadOnly='<%#IsFieldReadOnly() %>'/>
		                                                </asp:TextBox>    
                                                        <asp:DropDownList runat="server" ID="yesno" CssClass="input trackchanges" Width="60px"
                                                            Visible='<%# ShowFieldForDataType(Eval("DataType"), "yesno") %>' Enabled='<%# IsFieldEnabled() %>'
                                                            EnableViewState="false">
                                                            <asp:ListItem Text="No" Value="false"></asp:ListItem>
                                                            <asp:ListItem Text="Yes" Value="true"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RadioButtonList ID="TriStateRadio" runat="Server" CssClass="input trackchanges" Width="140px" Visible='<%# ShowFieldForDataType(Eval("DataType"), "TriStateRadio") %>' 
                                                        	Enabled='<%# IsFieldEnabled() %>' RepeatDirection="Horizontal" RepeatLayout="Flow">  
															<asp:ListItem Text="Yes" Value="true"></asp:ListItem> 
															<asp:ListItem Text="No" Value="false"></asp:ListItem>                                                
                        								</asp:RadioButtonList>
                        								<i class="cusDpdown-arrow" onclick="return openCustomlkd(this, true);" <%# haveMoreField(Eval("Id"), Eval("DataType"))%> style="display:none"></i>
                                                        <a class="LoadGeolocationIcon" onclick="return GeolocationLoad(this);" style="display: none;"></a>
                                                        <span class="qc" fieldid='<%# Eval("Id") %>'><a class="check">
                                                                !</a> <a class="passed">&#10004;</a> </span>
                                                    </td>
                                                    <%--<td class="qc" fieldid='<%# Eval("Id") %>'>
<a class="check">!</a> <a class="passed">a</a>
</td>--%>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </table>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>

                        <div id="classCalculatorFrame" class="parcel-field-values" style="display: none;">
                            <table class="parcel-field-values">
                                <div style="margin-left: 10px;">
                                    <label><b>Current : </b></label>
                                    <span class='currClass' style="color: green;"></span>&nbsp;&nbsp;&nbsp;&nbsp;<span class='classRec'></span>&nbsp;&nbsp;&nbsp;
                                   <button class='btnCalculate'>Calculate Class </button>
                                    &nbsp;&nbsp;
                                     <button class='updateClass' style="display: none">Update</button>
                                    &nbsp;&nbsp;
                                    <button class='btnCancel'>Cancel </button>
                                </div>
                                <asp:Repeater runat="server" ID="rpClassCalcFields">
                                    <ItemTemplate>
                                        <tr>
                                            <td class="label">
                                                <span><%# Eval("DisplayLabel")%></span> <span class="ColoumnSep">:</span> <span class="required">*</span>
                                                <span class="infoButton" onclick="showInfoContent(this);"></span>
                                            </td>
                                            <td class="value" fieldid='<%# Eval("Id") %>' fieldname='<%# Eval("Name") %>' datatype='<%# Eval("DataType") %>'>
                                                <asp:TextBox runat="server" ID="txt" CssClass="input trackchanges classCalcAttributes" Width="250px"
                                                    Visible='<%# ShowFieldForDataType(Eval("DataType"),"txt") %>' ReadOnly='<%#IsFieldReadOnly() %>' />
                                                <asp:TextBox runat="server" ID="ltxt" CssClass="input trackchanges classCalcAttributes" Width="400px"
                                                    Rows="2" TextMode="MultiLine" Visible='<%# ShowFieldForDataType(Eval("DataType"),"ltxt") %>'
                                                    ReadOnly='<%#IsFieldReadOnly() %>' onkeydown="setHeight(this);" />
                                                <asp:TextBox runat="server" ID="number" CssClass="input trackchanges classCalcAttributes" Width="250px"
                                                    type='<%# SelectTypeWithReadOnlyStatus(Eval("IsReadOnly"), Eval("IsCalculated"), Eval("IsReadOnlyDTR"), IIf(Eval("CategoryId").Equals(DBNull.Value), "", Eval("CategoryId"))) %>'
                                                    Visible='<%# ShowFieldForDataType(Eval("DataType"),"num") %>' ReadOnly='<%#IsFieldReadOnly() %>' />
                                                <asp:TextBox runat="server" ID="datevalue" CssClass="input trackchanges classCalcAttributes" Width="250px"
                                                    type="date" Visible='<%# ShowFieldForDataType(Eval("DataType"),"date") %>' Enabled='<%# IsFieldEnabled() %>' /> 
                                                <asp:DropDownList runat="server" ID="ddl" CssClass="input trackchanges classCalcAttributes" Width="256px"
                                                    Visible='<%# ShowFieldForDataType(Eval("DataType"),"ddl") %>' LookupTable='<%# Eval("LookupTable") %>'
                                                    IsClassCalculatorAttribute='<%# Eval("IsClassCalculatorAttribute")%>'
                                                    LookupShowNullValue='<%# Eval("LookupShowNullValue") %>' IsListBox='<%# IIf(Eval("DataType") = 11, True, False) %>'
                                                    OnLoad="LookupDropdown_Load" Enabled='<%# IsFieldEnabled() %>' />
                                                <asp:TextBox runat="server" ID="ddlSpan" CssClass="input trackchanges cc-drop cusDpdownSpan classCalcAttributes"
                                                    Visible='<%# ShowFieldForDataType(Eval("DataType"),"lookup") %>' LookupTable='<%# Eval("LookupTable") %>'
                                                    IsClassCalculatorAttribute='<%# Eval("IsClassCalculatorAttribute")%>'
                                                    LookupShowNullValue='<%# Eval("LookupShowNullValue") %>' IsListBox='<%# IIf(Eval("DataType") = 11, True, False) %>'
                                                    Enabled='<%# IsFieldEnabled() %>'  />
                                                </asp:TextBox>   
                                                <asp:DropDownList runat="server" ID="yesno" CssClass="input trackchanges classCalcAttributes" Width="60px"
                                                    Visible='<%# ShowFieldForDataType(Eval("DataType"),"yesno") %>' Enabled='<%# IsFieldEnabled() %>'
                                                    EnableViewState="false" OnLoad="LoadYesOrNoLookup" RadioFieldValues='<%# Eval("RadioFieldValues")%>'>
                                                </asp:DropDownList>
                                                <asp:RadioButtonList ID="TriStateRadio" runat="Server" CssClass="input trackchanges" Width="140px" Visible='<%# ShowFieldForDataType(Eval("DataType"),"TriStateRadio") %>' 
                                                        	Enabled='<%# IsFieldEnabled() %>' RepeatDirection="Horizontal" RepeatLayout="Flow">  
															<asp:ListItem Text="Yes" Value="true"></asp:ListItem> 
															<asp:ListItem Text="No" Value="false"></asp:ListItem>                                                
                        						</asp:RadioButtonList>
                        						<i class="cusDpdown-arrow" onclick="return openCustomlkd(this, true);" <%# haveMoreField(Eval("Id"),Eval("DataType"))%> style="display:none"></i>
                                                <span class="qc" fieldid='<%# Eval("Id") %>'><a class="check">!</a> <a class="passed">&#10004;</a> </span>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </div>


                        <div class="field-audit-qc hidden">
                            <div>
                                <div class="info">
                                    Click on the appropriate value to select the approved one. You may also select/enter
                                    a new value if required.</div>
                                <div class="audit-head">
                                    Initial Value: <a class="initial-value"></a>
                                </div>
                                <table class="parcel-changes">
                                    <thead>
                                        <tr>
                                            <th>
                                                Date
                                            </th>
                                            <th>
                                                Changed by
                                            </th>
                                            <th>
                                                New Value
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot class="hidden">
                                        <tr>
                                            <td class="date">
                                                ${ChangedOn}
                                            </td>
                                            <td class="user">
                                                ${ReviewedBy}
                                            </td>
                                            <td class="value">
                                                <a title="${FullDisplayText}" onclick="selectChange(${Id}, ${FieldId}, ${AuxROWUID}, ${Index});">${DisplayText}</a>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="field-audit-qc-template hidden">
                        </div>
                        <div class="category-page neighborhood hidden" categoryid="neighborhood">
                        </div>
                        <div class="category-page audit-trail hidden" categoryid="audit-trail">
                            <div class="recent-audit-trail-main" style="width: 100%;">
                                <table class="recent-audit-trail-table" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <td>
                                                Date Time
                                            </td>
                                            <td>
                                                User
                                            </td>
                                            <td>
                                                Description
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot class="hidden">
                                        <tr>
                                            <td style="width: 15%">
                                                ${AuditTime}
                                            </td>
                                            <td style="width: 15%">
                                                ${AuditUser}
                                            </td>
                                            <td style="width: 62%">
                                                ${Description}
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="parcel-status-bar hidden">
        <table class="status-bar">
        </table>
    </div>
    <div class="hidden">
        <canvas class="sketch-renderer" width="1000" height="1000" style="background: White;
            height: 1000px; width: 1000px;"></canvas>
    </div>
    <div class='hidden'>
        <cc:PropertyRecordCard ID="PropertyRecordCard" runat="server" PRCTemplateName="dtr-property-record-card" />
    </div>
    <div class="divCodeFile">
        <div class="divin">
            <span legend style="margin-left: 20px; font-weight: bold;"></span><span selectedcustomddl>
            </span>
        </div>
        <div class="searchtools">
            <input type="text" style="float: left;" id="searchtxt" onkeyup="return keyUp(this)">
            <button type="button" onclick="return cancelClick()" style="display: inline-block;">
                Cancel</button>
            <button type="button" onclick="return okClick()" style="display: inline-block; margin-right: -12px;">
                OK</button>
        </div>
        <%-- <div class="searchitem">
    <span  style="margin-left: 20px;">Search items:</span>
    </div>--%>
    	<div class="lkShowAll">
        	<span class="lkShowAllStatus" style="float: left;"> Showing 0 records out of 0 </span>
        	<span class="lkShowAllRec" style="display: none; float: Right; margin-right: 20px"> Showing All Records </span>
        	<span class="lkShowAlllink" onclick="return fillAllLookupList();" style="text-decoration: underline; float: Right; margin-right: 20px">Show All Records</span>
        </div>
        <div class="match">
            <div class="noitem">
            </div>
        </div>
    </div>
    <div class="columns" style="display: none; height: 310px; border: 1px solid #CFCFCF;
        width: 800px;">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlUserEdit" CssClass="user-edit-panel" Style="width: 250px;
                    height: 200px;">
                    <table class="comparable-edit " style="margin-left: 20px;">
                        <tr>
                            <td class="v-split" style="width: 7px;">
                            </td>
                            <td style="width: 200px; vertical-align: top;">
                                <asp:ListBox ID="lbColumnsLeft" runat="server" Rows="15" Width="200px" ClientIDMode="static"></asp:ListBox>
                            </td>
                            <td>
                                <table class="moveButtons">
                                    <tr>
                                        <td>
                                        <Button ID="btnRight" onclick="return ToMoveRight();">></Button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                         <Button ID="btnLeft" onclick="return ToMoveLeft();"><</Button>
                                        </td>
                                    </tr>
                                   <%-- <tr>
                                        <td>
                                         <Button ID="btnRightRight" onclick="return ToMoveAllRight();">>></Button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                          <Button ID="btnLeftLeft" onclick="return ToMoveAllLeft();" ><<</Button>
                                        </td>
                                    </tr>--%>
                                </table>
                            </td>
                             <td>
                                <asp:ListBox ID="lbColumnsRight" runat="server" Rows="15" Width="200px" ClientIDMode="static"></asp:ListBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <div style="margin-top: 10px;margin-bottom: 15px">
                                    <button onclick="return setgridcolumns();">
                                        Ok</button>
                                    <button onclick="return hidePopup();">
                                        Cancel</button>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="infoContentContainer">
		<div class="infoContentHeader">
			Information - <span class="info_fieldName"></span><span style="float: right; margin: 2px; cursor: pointer;" onclick="hideInfoContent()">X</span>
		</div>
		<div class="infoContent"></div>
		<div class="infoContentFooter"> 
			<span type="button" value="Close" style="float: right; margin-right: 10px; cursor: pointer;" onclick="hideInfoContent()">Close</span>
		</div>
	</div>
	
	<div id="CustomQueryBuilder" style="width: 90%;height: auto;position: absolute;top: 5%;left: 4%;background-color: #ffffff;z-index: 99999;padding: 10px;box-shadow: 0px 0px 10px #000000db;border-radius: 5px;display:none;">
            <span class="closeCstMsg"  onclick="closeCustmsg(); return false ;" title="Close">&times;</span>
            <div>
    		<table>
        		<tbody><tr>
            		<td>
                 		<h2 style="margin-top: 1px;margin-bottom: 10px;color: #008cba;">Custom Filter Selection</h2>
            		</td>
            		<td>
            			<input type="text" id="filter-name" style="margin-top: 6px;margin-bottom: 10px;width: 500px;padding: 5px;border-radius: 5px;border: solid 1px #ccc;" maxlength="50">
            		</td>
        		</tr>
   			 </tbody></table>
		</div>
            
           <div id="CustomFilter" style="width:100%; background-color: #ffffff; border:1px solid; min-height: 400px;">	
           </div>
            <button class="btn-filter apply">Apply Filter</button>
            <button class="btn-filter savecustomfilter">Save Filter</button>
            <input type="hidden" id="Issave"  value="">
            <input type="hidden" id="filterID"  value="">
    </div>
        
     <div class="modal" style="display: none;">
        <asp:RadioButtonList ID="exportlist" runat="server" RepeatDirection="Vertical">
            <asp:ListItem Value="0" Selected="True">.csv</asp:ListItem>
            <%--<asp:ListItem Value="1">.xls</asp:ListItem>--%>
            <asp:ListItem Value="2">.xlsx</asp:ListItem>
        </asp:RadioButtonList>
        <div style="text-align: center;margin-top:20px">
             <asp:Button Text="Continue" runat="server" ID="exportto" OnClientClick="return export_click();" />
        </div>
    </div>
	<div class="cc-drop-pop" style="width: 200px;display:none;">
        <input type="text" class="cc-drop-customtext" />
        <input type="text" class="cc-drop-search" />
        <div class="cc-drop-items"></div>
    </div>
    
    <div class="GeoLocationMap" style='display: none; background-color: white; position: absolute;top: 20%;z-index: 5002;'>
	    <div style='padding: 4px 3px; font-size: 18px; font-weight: bold; background: #BBB'>Select Geo Location</div>
	    <div id='GeoMAP' style='width: 100%; height: 100%;'></div>
	    <div style='padding: 5px; background: #EFEFEF; text-align: right; padding-right: 48px;'>
	        <button onclick='CloseGeoLocation()' style=' margin-right:10px;'>Cancel</button>
	        <button onclick='SetGeoLocationInField()'>Save</button>
	    </div>
	</div>
    
	<script type="text/javascript">
    
    var woolpertApikey = '<%= woolpertApikey%>';
	var woolpert6inchId = '<%= woolpert6inchId%>';
	var woolpert3inchId = '<%= woolpert3inchId%>';
    
</script>
	
</asp:Content>
