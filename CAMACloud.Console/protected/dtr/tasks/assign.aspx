﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ParcelManager.master" CodeBehind="assign.aspx.vb" Inherits="CAMACloud.Console.assign" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<script>
function hidedtrAssign(){
			$('.info').hide();        	
	}


	$('document').ready(function () {
		$(".assignButton").click(function () {
			console.log("Assign button clicked");
			return confirm("Are you sure you want to assign this task to user?");
		});
	});
	

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<h1>Assign Tasks</h1>
	<asp:Panel runat="server" ID="pnlUsers">
		<p  class="info">
			Below is the list of users who are assigned with <strong>Data Entry, Manager and Remote Verification</strong> role. Select a user to assign tasks.
		</p>
		<asp:GridView runat="server" ID="gvUsers" AllowPaging="true" PageSize="15">
			<Columns>
				<asp:TemplateField HeaderText="Full Name">
						<ItemStyle Width="180px" />
						<ItemTemplate>
							<%# Eval("UserName")%>
						</ItemTemplate>
					</asp:TemplateField>
                <asp:TemplateField HeaderText="Role">
						<ItemStyle Width="150px" />
						<ItemTemplate>
							<%# If(Roles.IsUserInRole(Eval("LoginId"), "DTR-ReadOnly"), "Data Entry", If(Roles.IsUserInRole(Eval("LoginId"), "DTR-Editor"), "Remote Verification", "Manager"))%>
						</ItemTemplate>
					</asp:TemplateField>
				<asp:TemplateField HeaderText="Click to assign">
					<ItemStyle Width="180px" />
					<ItemTemplate>
						<asp:LinkButton ID="LinkButton1" runat="server" Text='<%#Eval("LoginId") %>' CommandName="Assign" CommandArgument='<%# Eval("LoginId") %>' />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</asp:GridView>
	</asp:Panel>
	<asp:Panel runat="server" ID="pnlAssign" Visible="false">
		<asp:HiddenField runat="server" ID="hdnUserName" />
        <asp:HiddenField runat="server" ID="hdnFullUserName" />
		<asp:HiddenField runat="server" ID="hdnTaskId" />
		<h3>Assign tasks to <asp:Label runat="server" ID="lblName" /></h3>
		<asp:LinkButton runat="server" ID="lbBack" Text="Return to List" />
		<p>
			Select Task: 
			<asp:DropDownList runat="server" ID="ddlTask" Width="180px" AutoPostBack="true" />
			<asp:Button runat="server" ID="btnAssign" Text=" Assign to User "  class="assignButton"/>
		</p>
		<p runat="server" id="nbhdDetails">
			Task: <asp:Label runat="server" ID="lblNbhdName" Font-Bold="true" /><br />
			Currently assigned to: <asp:Label runat="server" ID="lblAssignedTo" Font-Bold="true" />
		</p>
		<asp:GridView runat="server" ID="gvNbhds">
			<Columns>
				<asp:BoundField DataField="Name" HeaderText="DTR Task" ItemStyle-Width="220px" />
				<asp:TemplateField ItemStyle-Width="80px">
					<ItemTemplate>
						<asp:LinkButton ID="LinkButton2" runat="server" Text="Remove" CommandName='Unassign' CommandArgument='<%# Eval("Id") %>' OnClientClick="return confirm('Are you sure you want to remove the selected task from the current user\'s assignments?')" />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
			<EmptyDataTemplate>
				No tasks assigned currently.
			</EmptyDataTemplate>
		</asp:GridView>
		<p>
			<asp:LinkButton runat="server" ID="lbRemoveAll" Text="Remove all tasks" OnClientClick="return confirm('All assigned tasks will be removed from the current user. Are you sure you want to continue?')" />
		</p>
	</asp:Panel>
</asp:Content>
