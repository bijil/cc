﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ParcelManager.master"
    CodeBehind="default.aspx.vb" Inherits="CAMACloud.Console._default4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $("#hrfCreate").click(function () {
                var url = 'edittask.aspx?action=new&pI=' + $('.mGrid .pgr span').text() + '&pS=' + $('#<%=ddlPageSize.ClientID %> option:selected').val() + '&pO=' + $('#<%=ddlOrderBy.ClientID %> option:selected').val();
                $(this).attr("href", url);
            });
        })
        function hidedtrtable(){
        	$('.dtrtable').hide();        	
        }
    </script>
    <style type="text/css">
        .spacer {
            display: inline-block;
            width: 30px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Manage Desktop Review Tasks</h1>
    <table class = "dtrtable" style="width: 90%">
        <tr>
            <td>Page Size:
                <asp:DropDownList runat="server" ID="ddlPageSize" AutoPostBack="true">
                    <asp:ListItem Value="1" Text="25" />
                    <asp:ListItem Value="2" Text="50" />
                    <asp:ListItem Value="3" Text="100" />
                    <asp:ListItem Value="4" Text="250" />
                </asp:DropDownList>
                <span class="spacer"></span>Sort by:
                <asp:DropDownList runat="server" ID="ddlOrderBy" AutoPostBack="true">
                    <asp:ListItem Text="Name" Value="1" />
                    <asp:ListItem Text="Appraiser" Value="2" />
                    <asp:ListItem Text="Total Parcels (Max First)" Value="3" />
                </asp:DropDownList>
            </td>
            <td class="r">
                <a href="#" id="hrfCreate" style="font-weight: bold;">Create New Task</a>
            </td>
        </tr>
        <tr>
            <td id="tdrefresh" colspan="2">
                  <asp:LinkButton runat="server" ID="btnRefresh" Text="Refresh" Style="font-weight: normal !important; float: right !important; text-align: right !important;" />
<%--                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                      
                    </ContentTemplate>
                </asp:UpdatePanel>--%>
            </td>
        </tr>
    </table>
    <div>
    </div>

    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <table style="border-spacing: 0px; width: 90%; font-weight: bold;">
                <tr>
                    <td>
                        <asp:Label runat="server" ID="lblPage" />
                    </td>
                </tr>
            </table>
            <asp:GridView runat="server" ID="gvTasks" AllowPaging="true" PageSize="25" Width="900px">
                <Columns>
                    <asp:BoundField DataField="Name" HeaderText="Name" />
                    <asp:BoundField DataField="AssignedTo" HeaderText="Assigned To" ItemStyle-Width="200px" />
                    <asp:BoundField DataField="TotalParcels" HeaderText="Parcels" ItemStyle-Width="100px" />
                    <asp:BoundField DataField="CompletedParcels" HeaderText="Reviewed" ItemStyle-Width="100px" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="hyp" runat="server" Text="Edit"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemStyle Width="50px" />
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="btnDelete" Text="Delete" CommandArgument='<%# Eval("Id") %>' CommandName="DeleteItem" OnClientClick='return confirm("Are you sure you want to delete this task item?")' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
