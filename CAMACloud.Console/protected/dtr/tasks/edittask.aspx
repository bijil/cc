﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/ParcelManager.master"
    CodeBehind="edittask.aspx.vb" Inherits="CAMACloud.Console.edittask" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .col1
        {
            width: 140px;
        }
        textarea {
			resize: none;
		}

        .hrfCopy {
            background-color: transparent;
            border: none;
            color: #167ccf;
            text-decoration: underline;
            cursor: pointer;
            font-size: 14px;
            font-weight: bold;
            margin-right: 50px;
            float: right;
            display: none;
        }

        .popup-box {
            background-color: #fff;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.3);
            text-align: center;
            display: none;
            top: 30%;
            left: 30%;
            z-index: 25;
            height: 150px;
            position: absolute;
        }

        .popup-box button {
          margin: 5px;
          padding: 8px 15px;
          cursor: pointer;
          width: 100px;    
          background-color: #68b6f7;
          color: white;
          border: #4aa1eb 1px solid;
        }
    </style>   
    <script type="text/javascript">
    //function preventSingleQuotes(event) {
    //    var key = event.which || event.keyCode;
    //    if (key === 39) { // 39 is the keycode for single quote (')
    //        event.preventDefault();
    //        return false;
    //    }
    //    return true;
    //    }      
        <%--function validateInput(inputElement) {
            var charCode = inputElement.keyCode;
            if (charCode !== 39) { // Check if the key is not a single quote (')
                return true;
            }
            return false;
        } 
        CC_3997--%>

        $(function () {
            let url = new URL(window.location.href), params = new URLSearchParams(url.search);
            if (params.get("taskid") && parseInt(params.get("taskid")) > -1)  $('.hrfCopy').show();
            else $('.hrfCopy').hide();
        });

        function showPopup() {
            $('.copycb').each(function (i, cb) { $(this)[0].checked = false; });
            $('.copycb').unbind("change");
            $('.copycb').bind("change", () => {
                $('.cv').hide();
            });
            $('.popup-box').show();
            $('.cv').hide();
            showMask();
            return false;
        }

        function closePopup() {
            $('.popup-box').hide();
            hideMask();
            return false;
        }

        function copyTask() {
            let currentURL = window.location.href, newurl = new URL(currentURL), params = new URLSearchParams(newurl.search), cc = '';
            $('.copycb').each(function (i, cb) {
                if ($(this)[0].checked) cc += $(this).val();
            });

            if (cc != '') {
                let newParameters = 'action=copy&tId=' + params.get('taskid') + '&pI=' + params.get('pI') + '&pS=' + params.get('pS') + '&pO=' + params.get('pS') + '&cC=' + cc;
                currentURL = currentURL.split('?')[0];
                let newURL = currentURL.includes('?') ? currentURL + '&' + newParameters : currentURL + '?' + newParameters;
                window.location.href = newURL;
            }
            else $('.cv').show();
            return false;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
        Edit Data Review Task</h1>
    <p>
        <asp:HiddenField runat="server" ID="hdnTaskID" Value="" />
        <asp:Button runat="server" ID="btnSave" Text=" Save " Width="100px" Height="26px" Font-Bold="true" ValidationGroup="EditTask"/>
        <asp:Button runat="server" ID="btnCancel" Text=" Back " Width="100px" Height="26px" />
        <button class="hrfCopy" onclick="return showPopup()">Copy to New Task</button>
    </p>
    <table>
        <tr>
            <td class="col1">
                Task Name: <asp:RequiredFieldValidator runat="server" ControlToValidate="txtName" ErrorMessage="*" ValidationGroup="EditTask" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtName" Width="200px" MaxLength="50" />
            </td>
        </tr>
        <tr>
            <td class="col1">
                Filter Description:
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtFilterData" ErrorMessage="*" ValidationGroup="EditTask" />

            </td>
            <td>
                <div>
                    <%--<asp:TextBox runat="server" ID="txtFilterData" TextMode="MultiLine" Columns="90"
                        Rows="25" onkeypress="return preventSingleQuotes(event);"/> CC_3997 --%>
                    <asp:TextBox runat="server" ID="txtFilterData" TextMode="MultiLine" Columns="90"
                        Rows="25"/>
                </div>
                 <div>
                     <asp:Label ID="lblcount" runat="server" Text="Label"></asp:Label>
                </div>
                <p class="info">
                    You may use T-SQL Compatible syntax to build the filter. For example,
                </p>
                <blockquote>
                    <pre>Field1 BETWEEN 0 AND 100 AND (Field2 > 100 OR Field3 = 1 OR Field4 IS NULL)</pre>
                </blockquote>
                <p class="info spFlagClass">
                    Special flag fields can also be used: $DTRStatus, $Reviewed (bit),  $ReviewedBy, $ReviewDate, $QC (bit), $QCBy, $QCDate, $Priority, $StreetAddress, $CautionAlert, $CautionMessage, $CC_Tag, $ParcelAlert, $FieldAlert, $FieldAlertType, $SketchReviewedDate, $SketchReviewedBy, $SketchReviewNote <asp:Label Visible="false" ID="labelSketchFlag" Text=", $SketchFlag" runat="server"></asp:Label><asp:Label Visible="false" ID="labelWorkFlow" Text=", $ParcelWorkflow" runat="server"></asp:Label>
                </p>
                <p class="info">
                    All fields in <b><%= CAMACloud.Data.Database.Tenant.Application.ParcelTable%></b> table can be used in the filter expression.:
                </p>
                <p class="info">
                    For Appraisal Method You may use SelectedAppraisalType = 'Method Name'.
                </p>
            </td>
        </tr>
    </table>
    <div class="popup-box">
        <p style="font-size: 15px; font-weight: bold; margin:10px;">Select whether to create a copy with the same Task Filter Description and/or the same Task Title.</p>
        <div style="height: 60px; font-size: 16px;">
            <label style="margin: 10px;">
              <input type="checkbox" style="height: 15px; width: 15px;" class="copycb" value="d"> Copy Task Filter Description
            </label>
            <label style="margin: 10px;">
              <input type="checkbox"  style="height: 15px; width: 15px;" class="copycb"  value="t"> Copy Task Title
            </label>
            <p class="cv" style="color: red; text-align: center; display: none;">Please check at least one option</p>
        </div>
        <div>
            <button onclick="return copyTask()"> Ok </button>
            <button onclick="return closePopup()"> Cancel </button>
        </div>
    </div>

</asp:Content>
