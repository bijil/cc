﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="digitalPRC.aspx.vb" Inherits="CAMACloud.Console.digitalPRC" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CAMA Cloud&#174; Digital PRC</title>
    <link rel="stylesheet" href="/App_Static/css/quality.css?4568" />
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyC7e5YrZlKqWtaY4ehsDwMgCis4IfNs7Xw"></script>
    <script type="text/javascript" src="/App_Static/jslib/infobubble.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/jquery.js"></script>
    <script type="text/javascript" src="/App_Static/js/dtr/010-dtr.js?"></script>
    <script type="text/javascript" src="/App_Static/jslib/underscore-min.js"></script>
    <script type="text/javascript" src="/App_Static/js/qc/010-constants.js"></script>
    <script type="text/javascript" src="../../App_Static/js/qc/020-parcel.js"></script>
    <script type="text/javascript" src="../../App_Static/js/qc/050-photos.js?1238"></script>
    <script type="text/javascript" src="/App_Static/js/system.js"></script>
    <script type="text/javascript" src="/App_Static/js/qc/010-conversions.js"></script>
    <script type="text/javascript" src="/App_Static/js/qc/015-datatypes.js"></script>
    <script type="text/javascript" src="/App_Static/js/qc/080-validations.js"></script>
    <script type="text/javascript" src="/App_Static/js/qc/010-quality.js?123"></script>
    <script type="text/javascript" src="/App_Static/js/qc/000-window.js"></script>
    <script type="text/javascript" src="/App_Static/js/qc/030-parcel-search.js"></script>
    <script type="text/javascript" src="/App_Static/js/qc/055-sketch.js"></script>
    <script type="text/javascript" src="/App_Static/jslib/flexigrid.js"></script>
    <script type="text/javascript"  src="../../App_Static/jslib/tipr.js?01123478"></script>
    <script>
        var __DTR = true;
        window.onunload = function (e) {
            opener.onunloadPoupUp1();
        };

        $(window).load(function () {
            opener.hidePrc();
            showNoTabs();
            deSelectAll();
            // showAllPhotos();
            //Thumnailphotos();
        });

        function showPhotos() {
            return false;
        }
        $(function () {
            let prccontent = window.opener.document.getElementsByClassName('digital-prc')[0].innerHTML;
            $('.digital-prc-card').html(prccontent);
        });
        window.onerror = function (message, source, lineno, colno, error) {
            console.error('Unhandled error:', message, 'at', source, 'Line:', lineno, 'Column:', colno, 'Error:', error);
            return true; // Prevent the default browser error handling
        };
    </script>
</head>
<body>
    <div class="category-page digital-prc" categoryid="digital-prc">
        <div class="digital-prc-card">
        </div>
    </div>
</body>
</html>
