﻿Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Web.Script.Serialization

Partial Class quality_Default
    Inherits System.Web.UI.Page
    Public RedirectedPriorityListFlag As String
	Public organizationName As String
    Public stateName As String
    Public alterNateField As String
    Public ReadOnly Property EnableNewPriorities As Boolean
        Get
            Dim EnableNewPriority = False
            Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM ClientSettings WHERE Name = 'EnableNewPriorities' AND Value = 1")
            If dr IsNot Nothing Then
                EnableNewPriority = True
            End If
            Return EnableNewPriority
        End Get
    End Property

    Public ReadOnly Property DoNotChangeTrueFalseInPCI As Boolean
        Get
            Dim ChangeTrueFalse = False
            Dim dr As String = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name = 'DoNotChangeTrueFalseInPCI'")
            If dr = "1" Then
                ChangeTrueFalse = True
            Else
                ChangeTrueFalse = False
            End If
            Return ChangeTrueFalse
        End Get
    End Property


    Public Property RedirectedPriorityListFlags() As String
        Get
            Return RedirectedPriorityListFlag
        End Get
        Private Set(ByVal value As String)
            RedirectedPriorityListFlag = value
        End Set
    End Property

    Public ReadOnly Property LocalDatabaseName As String
        Get
            Return HttpContext.Current.GetCAMASession.TenantKey.ToUpper + "_CON" + Right(CAMACloud.Data.Database.Tenant.Application.SchemaVersion.ToString.PadLeft(5, "0"), 5) + "_" + User.Identity.Name
        End Get
    End Property
    
    Public ReadOnly Property hasTaskManagerRole As Boolean
		Get
			Dim hasroles = False
			Dim currentUserRole = Roles.GetRolesForUser()
	        If currentUserRole.Contains("TaskManager") Then
	        	hasroles = True
	        End If
	        Return hasroles
	    End Get
    End Property
    
    Public Sub OpenParcel(parcelId As Integer)

    End Sub
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim dType As String = ""
            Dim internalSupportUsers As String() = {"admin", "dcs-support", "vendor-cds", "dcs-qa", "dcs-rd", "dcs-integration", "dcs-ps"}
            Dim currentLogUser As String = Membership.GetUser().ToString
            Dim dtFieldCategory As DataTable
            ddlSelectedEstimate.FillFromSql("SELECT Id, Description FROM AppraisalType", True, "-- Not Selected --")
            ' rpFieldCategory.DataSource = d_("SELECT * FROM FieldCategory WHERE ShowCategory = 1 ORDER BY Ordinal")

            If internalSupportUsers.Contains(currentLogUser) Then
                dtFieldCategory = d_("SELECT * FROM FieldCategory WHERE ParentCategoryId is null  ORDER BY Ordinal")
            Else
                dtFieldCategory = d_("SELECT * FROM FieldCategory WHERE ShowCategory = 1 and ParentCategoryId is null  ORDER BY Ordinal")
            End If

            rpFieldCategory.DataSource = dtFieldCategory
            rpFieldCategory.DataBind()
            If dtFieldCategory.Rows.Count > 0 Then
                Dim dtidString As String = dtFieldCategory.Rows(0)("Id").ToString()
                Dim dtnameString As String = dtFieldCategory.Rows(0)("Name").ToString()
                Dim newdtidString As String = dtFieldCategory.Rows(0)("Id").ToString()
                If internalSupportUsers.Contains(currentLogUser) Then
                    For Each dtfdRow As DataRow In dtFieldCategory.Rows
                        If dtfdRow("ShowCategory").ToString() = "1" Then
                            dtidString = dtfdRow("Id").ToString()
                            dtnameString = dtfdRow("Name").ToString()
                            Exit For
                        End If
                    Next
                End If
                ltParceltab.Text = "<li content=""parceldata"" ><a href=""#parceldata"" div-width='520'  tip-margin-top='10' class='tip' abbr='The panels on the Parcel Data tab will display in the same order that the appraiser sees on the field device. Any panel where changes were made by the appraiser will display in <span style=""color:red;font-weight:bold"">red</span> letters to indicate a change. Each field changed will have a red exclamation mark next to it. A pop-up window will show the before / after values where each value is a link. To revert a change to the prior value, click on that value.' onclick=""openCategory('" + dtidString + "', '" + dtnameString + "','parceldata');$('.category-menu').show();return false;"" category=""" + dtidString + """ supportCategory=""" + newdtidString + """ mainCategory=""" + dtidString + """>Parcel Data</a></li>"
            Else
                ltParceltab.Text = ""
            End If
            'rpCategoryForFields
            rpCategoryForFields.DataSource = BusinessLogic.FieldCategory.GetFieldCategories(True)
            rpCategoryForFields.DataBind()
            getClassCalculatorAttributes()
            'FillSubmenus
            'rpFieldAllCategories.DataSource = d_("SELECT * FROM FieldCategory WHERE ShowCategory = 1  ORDER BY Ordinal")
            ''rpFieldAllCategories.DataSource = d_("exec  qc_getAllCategories")
            'rpFieldAllCategories.DataBind()
            getDataForCategories()
            LoadColumnsList()
            getPhotoProperties()
            ' Dim ca As New ClientApplication(Database.Tenant)
            ' Dim neighborhoodNumberField As String = ca.NeighborhoodField
            ' txtGroupNumber.Text=""
            ' ddlAssignTo.SelectedIndex=0
            'dType = Database.Tenant.GetStringValue ("SELECT Data_Type FROM information_schema.columns WHERE table_name = 'NeighborhoodData' AND column_name='" + neighborhoodNumberField + "'")
            ' Dim nbhdNumberFieldLength As Integer = IIF (dType = "int" , 4 , Database.Tenant.GetIntegerValue("SELECT character_maximum_length FROM information_schema.columns WHERE table_name = 'NeighborhoodData' AND column_name='" + neighborhoodNumberField + "'"))

            ' If dType = "varchar" And nbhdNumberFieldLength < 50 Then
            '     Database.Tenant.Execute("ALTER TABLE  NeighborhoodData ALTER COLUMN " + neighborhoodNumberField + " VARCHAR(50)")
            '     'Database.Tenant.Execute("UPDATE datasourcefield SET MaxLength = 50 WHERE Id IN (SELECT df.Id FROM datasourcefield df INNER JOIN DataSourceTable dt ON dt.Id = df.TableId WHERE dt.CC_TargetTable ='NeighborhoodData')")
            ' End If
            ' If dType = "int" Then
            '     txtGroupNumber.Attributes.Add("dType", "int")
            ' ElseIf dType = "varchar"
            '     txtGroupNumber.Attributes.Add("MaxLength", 50)
            ' Else
            '     txtGroupNumber.Attributes.Add("MaxLength", nbhdNumberFieldLength)
            ' End If
            Dim s = Database.Tenant.Application.AlternateKeyfield

            LoadUserList()
            UserAuditTrail.CreateEvent(UserName, UserAuditTrail.ApplicationType.Console, UserAuditTrail.EventType.OpenQC, "User opened QC Module.")
            RedirectedPriorityListFlag = ""
            If Not Request.QueryString() Is Nothing And Request.QueryString("PrioPage") IsNot Nothing Then
                Dim sql As String = ""
                If Request.QueryString("PrioPage") = "PriorityNotApproved" Then
                    RedirectedPriorityListFlags = "PriorityNotApproved"
                End If
                If Request.QueryString("PrioPage") = "AssignedParcels" Then
                    RedirectedPriorityListFlags = "AssignedParcels"
                End If
            End If
        End If
        organizationName = ClientOrganization.PropertyValue("County", HttpContext.Current.GetCAMASession().OrganizationId)
        stateName = ClientOrganization.PropertyValue("State", HttpContext.Current.GetCAMASession().OrganizationId)
        Dim alternate As String = Database.Tenant.Application.AlternateKeyfield
        Dim parcelTable As String = Database.Tenant.Application.ParcelTable
        If alternate IsNot Nothing Then
            alterNateField = Database.Tenant.GetStringValue("select DisplayLabel from DataSourceField where Name ={0} AND SourceTable = {1}".SqlFormatString(alternate, parcelTable))
        End If
    End Sub

    Protected Sub rpCategoryForFields_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpCategoryForFields.ItemDataBound
        Dim rpFields As Repeater = e.Item.FindControl("rpFields")
        Dim hCID As HiddenField = e.Item.FindControl("hCID")
        rpFields.DataSource = d_("SELECT * FROM DataSourceField WHERE CategoryId = " & hCID.Value & " AND (IsClassCalculatorAttribute=0 OR IsClassCalculatorAttribute IS NULL) AND (ExclusiveForSS IS NULL OR ExclusiveForSS=0) ORDER BY CategoryOrdinal")
        rpFields.DataBind()
    End Sub

    Public Function ShowFieldForDataType(dataType As Integer, input As String) As Boolean
        Dim stdinput As String = "txt"
        Select Case dataType
            Case 1
                stdinput = "txt"
            Case 2
                stdinput = "num"
            Case 3
                stdinput = "yesno"
            Case 4
                stdinput = "date"
            Case 5
                stdinput = "lookup"
            Case 6
                stdinput = "ltxt"
            Case 7
                stdinput = "num"
            Case 8
                stdinput = "num"
            Case 9
                stdinput = "num"
            Case 10
                stdinput = "num"
            Case 11
            	stdinput = "ddl"
            Case 13
            	stdinput ="TriStateRadio"
            Case Else
                stdinput = "txt"
        End Select

        Return (input = stdinput)
    End Function

	Public Function SelectTypeWithReadOnlyStatus(isReadOnly As Boolean, isCalculated As Boolean ,categoryId As String)
		Dim catReadOnly As Boolean = False	
		If  categoryId <> ""
		   catReadOnly  = Database.Tenant.GetIntegerValue("SELECT  IsNull(IsReadOnly, '') FROM FieldCategory WHERE Id = " &categoryId)
		End If
	    If isReadOnly Or isCalculated Or catReadOnly = True Then
	        Return "text"
	    Else
	        Return "number"
	    End If
    End Function

    Public Function IsFieldReadOnly() As Boolean
        Return Eval("IsCalculated") Or Eval("IsReadOnly")
    End Function

    Public Function IsFieldEnabled() As Boolean
        Return Not IsFieldReadOnly()
    End Function

    Public Function haveMoreField(fieldId As String, dt As String) As String
        Dim returnValues As String = "hide"
        If dt = "5" Then
            If Database.Tenant.GetDataTable("select p.IdValue as total from DataSourceField d left outer join  ParcelDataLookup p on d.LookupTable=p.LookupName where d.Id={0} and IsReadOnly=0".SqlFormatString(fieldId)).Rows.Count > 25 Then
                returnValues = "show"
            End If
        End If

        Return returnValues
    End Function
    
    Protected Sub LoadYesOrNoLookup(sender As Object, e As System.EventArgs)
        Dim ddl As DropDownList = sender
        Dim RadioFieldValues As String = ddl.Attributes("RadioFieldValues")
        If RadioFieldValues <> Nothing And RadioFieldValues <> "" Then
            Dim arr As String() = RadioFieldValues.Split(",")
            ddl.InsertItem("Yes", arr(0), 0)
            ddl.InsertItem("No", arr(1), 1)
            ddl.Enabled = False
        Else
            ddl.InsertItem("No", "false", 0)
            ddl.InsertItem("Yes", "true", 1)   
        End If
    End Sub

    Protected Sub LookupDropdown_Load(sender As Object, e As System.EventArgs)
        Dim ddl As DropDownList = sender
        Dim lookupTable As String = ddl.Attributes("LookupTable")
        Dim lookupShowNullValue As String = ddl.Attributes("LookupShowNullValue")
        Dim IsClassCalculatorAttribute As String = ddl.Attributes("IsClassCalculatorAttribute")
        If (String.IsNullOrWhiteSpace(IsClassCalculatorAttribute)) Then
            IsClassCalculatorAttribute = "false"
        End If
        If lookupTable <> "" Then

            Dim ldt = d_("SELECT IdValue, NameValue,isNull(Value,0) as Value FROM ParcelDataLookup WHERE LookupName = '" + lookupTable + "' ORDER BY Ordinal")
            For Each r As DataRow In ldt.Rows
                'ddl.AddItem(r.GetString("namevalue").Trim, If(IsClassCalculatorAttribute.ToLower = "true", r.GetString("Value"), r.GetString("idvalue").Trim))
                Dim IdVal As String = r.GetString("idvalue").Trim
                Dim dr As String = "0"
                dr = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name = 'DoNotChangeTrueFalseInPCI'")
                If dr <> "1" Then
                    If IdVal = "True" Or IdVal = "TRUE" Or IdVal = "False" Or IdVal = "FALSE" Then
                        IdVal = IdVal.ToLower
                    End If
                End If
                ddl.AddItem(r.GetString("namevalue").Trim,IdVal )
            Next
            If lookupShowNullValue.ToLower = "true" Then
                ddl.InsertItem("-- No Value --", "", 0)
            End If
            '
        End If
        Dim isLookup As Boolean = Boolean.Parse(ddl.Attributes("IsListBox"))
        If isLookup Then
            ddl.Attributes.Add("multiple", "multiple")
            ddl.Style("height") = "120px"
        End If
    End Sub

    Private Sub getDataForCategories()
        Dim query1 As String = "select f2.Id,f2.Name,f2.ParentCategoryId,f2.SourceTableName,f3.id as ChildCategoryId,f2.FilterFields as NodeFilterField from FieldCategory f2 left outer join FieldCategory f3 on f3.ParentCategoryId=f2.Id"
        Dim dt1 As DataTable
        dt1 = d_(query1)
        If (dt1.Rows.Count > 0) Then
            Dim iter_val As Integer = 0
            Dim filters As String = ""
            While (iter_val < dt1.Rows.Count)
                Dim dt2 As DataTable
                Dim sourceTable As String = dt1.Rows(iter_val)("SourceTableName").ToString()
                ' Dim query2 As String = "SELECT f.Name As FieldName FROM DataSourceRelationships r INNER JOIN DataSourceKeys k ON k.RelationshipId = r.Id INNER JOIN DataSourceField f ON k.FieldId = f.Id inner join DataSourceTable df on df.Id=r.TableId WHERE r.Relationship is null and df.Name='" + sourceTable + "'"
                Dim query2 As String = "SELECT  tf.Name As TableField, pf.Name As ReferenceField FROM DataSourceRelationships r LEFT OUTER JOIN DataSourceKeys k ON k.RelationshipId = r.Id LEFT OUTER JOIN DataSourceField tf ON k.FieldId = tf.Id LEFT OUTER JOIN DataSourceField pf ON k.ReferenceFieldId = pf.Id 	LEFT OUTER JOIN DataSourceTable tt ON r.TableId = tt.Id LEFT OUTER JOIN DataSourceTable pt ON r.ParentTableId = pt.Id WHERE tt.Name ='" + sourceTable + "' AND Relationship IS NOT NULL AND Relationship <> 0 AND k.IsReferenceKey = 1"
                dt2 = d_(query2)
                If (dt2.Rows.Count > 0) Then
                    Dim iter_val2 As Integer = 0
                    While (iter_val2 < dt2.Rows.Count)
                        Dim f = dt2(iter_val2)("TableField").ToString()
                        If dt2(iter_val2)("TableField").ToString() <> dt2(iter_val2)("ReferenceField").ToString() Then
                            f = dt2(iter_val2)("ReferenceField").ToString() + "/" + dt2(iter_val2)("TableField").ToString()
                        End If
                        filters += f
                        iter_val2 += 1
                        If (iter_val2 < dt2.Rows.Count) Then
                            filters += ","
                        End If
                    End While

                End If
                dt1.Rows(iter_val)("NodeFilterField") = filters + IIf(dt1.Rows(iter_val)("NodeFilterField").ToString().IndexOf("ROWUID") > -1, ",ROWUID", "")
                dt1.AcceptChanges()
                filters = ""
                iter_val += 1
            End While
            rpFieldAllCategories.DataSource = dt1
            rpFieldAllCategories.DataBind()
        End If


    End Sub

    Private Sub LoadColumnsList()
        'Dim dtGridColumns As DataTable = Database.Tenant.GetDataTable("SELECT DISTINCT Heading as Name FROM UI_GridViewFieldSettings WHERE (IsDTR IS NULL OR IsDTR = 0) And Heading IS NOT NULL  UNION SELECT DisplayLabel as Name FROM DataSourceField f INNER JOIN DataSourceTable t ON f.TableId = t.Id INNER JOIN FieldCategory fc ON fc.Id = f.CategoryId WHERE t.ImportType = 0 And f.CategoryId Is Not NULL And f.IsCalculated = 0 And f.DisplayLabel IS NOT NULL order by Name")
        'Dim dtGridColumns As DataTable = Database.Tenant.GetDataTable("SELECT DISTINCT CASE WHEN Heading != (SELECT keyfield1 FROM Application) AND FieldExpression = 'KeyValue1' THEN (SELECT keyfield1 FROM Application)  ELSE Heading END as Name FROM UI_GridViewFieldSettings WHERE (IsDTR IS NULL OR IsDTR = 0) And Heading IS NOT NULL  UNION  SELECT CASE WHEN DisplayLabel != (SELECT keyfield1 FROM Application) AND f.Name in(SELECT keyfield1 FROM Application) THEN (SELECT keyfield1 FROM Application)  ELSE DisplayLabel END AS  Name FROM DataSourceField f INNER JOIN DataSourceTable t ON f.TableId = t.Id INNER JOIN FieldCategory fc ON fc.Id = f.CategoryId WHERE t.ImportType = 0 And f.CategoryId Is Not NULL And f.IsCalculated = 0 And f.DisplayLabel IS NOT NULL order by Name")
        Dim dtGridColumns As DataTable = Database.Tenant.GetDataTable("EXEC Sp_ColumnChooser")
        lbColumnsLeft.DataSource = dtGridColumns
        lbColumnsLeft.DataTextField = "Name"
        lbColumnsLeft.DataValueField = "Name"
        lbColumnsLeft.DataBind()
    End Sub
    
    Public Sub getPhotoProperties()
        Dim dtPhotoProperties As DataTable = d_("SELECT * FROM DataSourceField WHERE SourceTable= '_photo_meta_data' ORDER BY AssignedName ") ''DoNotShowOnDE = 0  AND
        rpPhotoProperties.DataSource = dtPhotoProperties
        rpPhotoProperties.DataBind()
    End Sub

    Public Sub getClassCalculatorAttributes()
        Dim dtClassCalculatorAttributes As DataTable = d_("SELECT * FROM DataSourceField d INNER JOIN FieldCategory f ON d.CategoryId = f.Id where  d.IsClassCalculatorattribute = 1 and f.ClassCalculatorParameters IS NOT NULL ORDER BY SourceOrdinal")
        'DoNotShowOnDE = 0 AND
        rpClassCalcFields.DataSource = dtClassCalculatorAttributes
        rpClassCalcFields.DataBind()
    End Sub

    Protected Sub ExportReport_Click(sender As Object, e As EventArgs)
        Dim KeyValue1 As String = hdnKeyValue1.Value
        Dim rpviewer As New ReportViewer()

        'Dim gridview = New GridView()
        'Dim dt As DataTable = Database.Tenant.GetDataTable("EXEC reports_ParcelAuditTrail @KeyValue1={0}".SqlFormatString(KeyValue1))
        'If dt.Rows.Count > 0 Then
        '    gridview.DataSource = dt
        '    gridview.DataBind()
        '    Response.Clear()
        '    Response.Buffer = True
        '    Response.ClearContent()
        '    Response.ClearHeaders()
        '    Dim FileName As String = "Audit-Trail - " + KeyValue1.ToString() + ".xls"
        '    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName)
        '    Response.Charset = ""
        '    Response.ContentType = "application/vnd.ms-excel"
        '    Dim sw As New StringWriter()
        '    Dim hw As New HtmlTextWriter(sw)
        '    gridview.RenderControl(hw)
        '    Response.Output.Write(sw.ToString())
        '    Response.Flush()
        '    Response.End()
        'Else
        '    Alert("Parcel doesn't have any audit trail entry.")
        'End If


        Dim reportData As DataTable = Database.Tenant.GetDataTable("EXEC reports_ParcelAuditTrail @KeyValue1={0}, @LatestFirst=0".SqlFormatString(KeyValue1))
        rpviewer.ProcessingMode = ProcessingMode.Local
        rpviewer.LocalReport.ReportPath = "reports/subreports/UserAuditTrail.rdlc"
        rpviewer.LocalReport.Refresh()

        Dim dataSetName As String = ""
        If dataSetName = "" Then
            For Each ds In rpviewer.LocalReport.GetDataSourceNames
                dataSetName = ds
            Next
        End If
        rpviewer.LocalReport.DataSources.Clear()
        Dim rs As New ReportDataSource(dataSetName, reportData)
        rpviewer.LocalReport.DataSources.Add(rs)
        rpviewer.LocalReport.DisplayName = "Audit-Trail - " + KeyValue1.ToString()
        rpviewer.Visible = True

        Dim bytes() As Byte
        Dim strDeviceInfo As String = ""
        Dim strMimeType As String = ""
        Dim strEncoding As String = ""
        Dim strExtension As String = ""
        Dim strStreams As String() = Nothing
        Dim warnings As Microsoft.Reporting.WebForms.Warning() = Nothing
        Dim _stream = New List(Of Stream)
        Try
            bytes = rpviewer.LocalReport.Render("Excel", strDeviceInfo, strMimeType, strEncoding, strExtension, strStreams, warnings)
            Response.Buffer = True
            Response.Clear()
            Response.ContentType = strMimeType
            Response.AddHeader("content-disposition", "attachment; filename=" + rpviewer.LocalReport.DisplayName.ToString() + ".xls")
            Response.BinaryWrite(bytes)
            Response.Flush()
            ClientScript.RegisterStartupScript(Me.GetType(), "alert", "alert('Audit trail report has been successfully downloaded');", True)
        Finally
            '//
        End Try
    End Sub

    Sub LoadUserList()
        Dim uc = Membership.GetAllUsers
        Dim users(uc.Count - 1) As MembershipUser
        Membership.GetAllUsers().CopyTo(users, 0)
        Dim fielduser = users.Where(Function(x) Roles.IsUserInRole(x.UserName, "MobileAssessor"))
        Dim dataUsers As New DataTable()
        dataUsers.Columns.Add("UserName")
        For Each u As MembershipUser In fielduser
            Dim dr As DataRow = dataUsers.NewRow
            dr("UserName") = u.UserName
            dataUsers.Rows.Add(dr)
        Next

        Dim userFromSettings As DataTable = Database.Tenant.GetDataTable("SELECT FirstName,LastName,LoginId FROM UserSettings")
        Dim query = From A In userFromSettings.AsEnumerable
                    Join B In dataUsers.AsEnumerable On
              A.Field(Of String)("LoginId") Equals B.Field(Of String)("UserName")
                    Order By If((A.Field(Of String)("FirstName") + " " + A.Field(Of String)("LastName")), B.Field(Of String)("UserName"))
                    Select New With {
              .LoginId = B.Field(Of String)("UserName"),
              .UserName = If((A.Field(Of String)("FirstName") + " " + A.Field(Of String)("LastName")), B.Field(Of String)("UserName"))
        }
        ddlAssignTo.DataSource = query
        ddlAssignTo.DataTextField = "UserName"
        ddlAssignTo.DataValueField = "LoginId"
        ddlAssignTo.DataBind()
        ddlAssignTo.InsertItem("None, will assign later", "", 0)
    End Sub
    
    Protected Sub customizeSearchFilters_click(sender As Object, e As EventArgs)
    	Response.Redirect("~/protected/datasetup/data/searchFilter.aspx")
    End Sub  
    
    Public Function ShowInternalViewProp(propertyValue As String, fiedlProperty As String, Optional propertyValue1 As String = "") As String
        Dim propertyValueString As String = If((Eval(propertyValue) Is DBNull.Value), "", Eval(propertyValue))
        Dim propertyValue1String As String = If((propertyValue1 = ""), "", (If((Eval(propertyValue1) Is DBNull.Value), "", Eval(propertyValue1))))
        Dim iconEnabled As String = "Disabled"
        If fiedlProperty = "reSumExp" Then
        	propertyValueString = Database.Tenant.GetStringValue ("SELECT Value FROM DataSourceFieldProperties WHERE PropertyName= 'RequiredSum' AND FieldId =" & propertyValueString)
        Else If  fiedlProperty = "calExp" Then
        	If propertyValueString = "" Then
        		propertyValueString = propertyValue1String
        	End If
        Else If (fiedlProperty = "deAuditExp" Or fiedlProperty = "DoNotShowOnDE") And propertyValueString = "False" Then	
        	propertyValueString = Nothing
        End If
        If propertyValueString IsNot Nothing And propertyValueString <> "" Then
        	iconEnabled = "IconEnabled"
        End If
        Return iconEnabled
    End Function
    
    Public Function ShortenedSourceTableText(fieldName As String, Optional maxLength As Integer = 8)
        If Eval(fieldName) Is DBNull.Value Then
            Return ""
        End If
        Dim text As String = Eval(fieldName)
        Dim l As Integer = text.Length
        If l < maxLength + 3 Then
            Return text
        Else
            Dim p2 As Integer = 3
            Dim p1 As Integer = maxLength - p2
            Return text.Substring(0, p1) + "..." + text.Substring(l - p2, p2)
        End If
    End Function
    
    Public Function showKeyPropIcons(keyFieldId As String, keyFieldTableId As String) As String
    	keyFieldTableId = If((Eval(keyFieldTableId) Is DBNull.Value), "", Eval(keyFieldTableId))
    	If keyFieldTableId = "" Then
    		Return "Disabled"
    	End If
        Dim forKeyTbl As DataTable = d_("SELECT * FROM DataSourceKeys WHERE FieldId = {0} AND TableId = {1} And IsReferenceKey = 1 ".SqlFormatString(keyFieldId, keyFieldTableId))
        If forKeyTbl.Rows.Count > 0 Then
        	Return "FK"
        End If
        Dim primaryKeyTbl As DataTable = d_("SELECT * FROM DataSourceKeys WHERE FieldId = {0} AND TableId = {1} And IsPrimaryKey = 1 ".SqlFormatString(keyFieldId, keyFieldTableId))
        If primaryKeyTbl.Rows.Count > 0 Then
        	Return "PK"
        End If
        Return "Disabled"
    End Function
    
End Class