﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="customactions.aspx.vb"
    Inherits="CAMACloud.Console.customactions1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('.enableToggler').click(function () {
                var id = $(this).attr('validatorId');
                var that = this;
                $ds('toggledisable', { Id: id, TableName: 'ClientCustomAction' }, function (res) {
                    var text = $(that).text();
                    text = (text == 'Enable') ? 'Disable' : 'Enable';
                    $(that).text(text);
                });
            });
        });
    </script>
    <style>
    	.row{
    		width:150px;
    		word-break: break-word;
    		}
        .rowaction{
    		width:215px;
    		word-break: break-word;
    		}
    	.rowNobreak{
    		width:150px;
    		}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
        Custom Field Actions for Flag Changes</h1>
    <p class="info">
        You can add custom field actions, applicable to parcel and associated table attributes only, for Flag Change actions in CAMA Cloud. These will
        be registered as field changes for the parcel.</p>
    <table>
        <tr>
            <td style="padding-right:80px;">Action: </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlAction" Width="240px" AutoPostBack="true">
                    <asp:ListItem Text="-- Select --" Value="" />
                    <asp:ListItem Text="Mark as Complete" Value="MarkAsComplete" />
                    <asp:ListItem Text="Mark as Complete (Auxiliary Table)" Value="MarkAsCompleteM21" />
                    <asp:ListItem Text="Appraisal Method Selection" Value="AppraisalMethod" />
                    <asp:ListItem Text="Field Alert Selection" Value="FieldAlertType" />
                    <asp:ListItem Text="Field Alert Text" Value="FieldAlert" />
                    <asp:ListItem Text="QC Save Changes" Value="saveparcelchanges" />
                    <asp:ListItem Text="QC Approve Parcel" Value="ApproveParcel" />
                </asp:DropDownList>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="ddlAction" ErrorMessage="*" ValidationGroup="CustomAction" />
            </td>
        </tr>
        <tr>
            <td style="padding-right:80px;">Target Table: </td>
            <td>
                <asp:DropDownList ID="ddlTable" runat="server" AutoPostBack="true" Width="240px"/>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlTable" ErrorMessage="*" ValidationGroup="CustomAction" />
            </td>
        </tr>
        <tr>
            <td style="padding-right:80px;">Target Field: </td>
            <td>
                <asp:DropDownList ID="ddlField" runat="server" Width="240px"/>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlField" ErrorMessage="*" ValidationGroup="CustomAction" />
            </td>
        </tr>
        <tr>
            <td style="padding-right:80px;">Update with Value: </td>
            <td>
                <asp:DropDownList ID="ddlValue" runat="server" Width="240px" AutoPostBack="true">
                    <asp:ListItem Text="Field Value" Value="" />
                    <asp:ListItem Text="Login ID" Value="LoginId" />
                    <asp:ListItem Text="Event Date/Time" Value="EventDate" />
                    <%--<asp:ListItem Text="Event Date/Time (Local)" Value="EventDateLocal" />--%>
                    <asp:ListItem Text="Current Year" Value="CurrentYear" />
                    <asp:ListItem Text="Current Month" Value="CurrentMonth" />
                    <asp:ListItem Text="Current Day" Value="CurrentDay" />
                    <asp:ListItem Text="Use from other table" Value="Other" />
                    <asp:ListItem Text="Custom Value" Value="CustomValue" />
                </asp:DropDownList>
            </td>
        </tr>
         <tr runat="server"  id="CustText">
             <td style="padding-right: 80px;">Custom Value: </td>
            <td><asp:TextBox ID="txtValue" runat="server" Width="240px" MaxLength="50" > </asp:TextBox></td>
        </tr>
        <tr runat="server" id="trOther1">
            <td style="padding-right:80px;">Other Table: </td>
            <td>
                <asp:DropDownList ID="ddlTable2" runat="server" AutoPostBack="true" Width="240px"/>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlTable2" ErrorMessage="*" ValidationGroup="CustomAction" />
            </td>
        </tr>
        <tr runat="server" id="trOther2">
            <td style="padding-right:80px;">Other Table Field: </td>
            <td>
                <asp:DropDownList ID="ddlField2" runat="server" Width="240px"/>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlField2" ErrorMessage="*" ValidationGroup="CustomAction" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button runat="server" ID="btnAddAction" Text="Add Custom Action" Width="180px" Height="28px" ValidationGroup="CustomAction" />
            </td>
        </tr>
    </table>
    <asp:GridView runat="server" ID="gvCustomActions"  AllowPaging="true" PageSize="10"  Width="95%">
        <Columns>
            <asp:BoundField DataField="Action" HeaderText="Action" ItemStyle-CssClass="rowaction" />
            <asp:BoundField DataField="TableName" HeaderText="Target Table" ItemStyle-CssClass="rowNobreak" />
            <asp:BoundField DataField="FieldName" HeaderText="Field" ItemStyle-CssClass="row" />
            <asp:BoundField DataField="ValueTag" HeaderText="Value Tag" ItemStyle-CssClass="row" />
             <asp:BoundField DataField="CustomText" HeaderText="Custom Text" ItemStyle-CssClass="row" />
            <asp:TemplateField>
                <ItemStyle Width="50px" />
                <ItemTemplate>
                     <a class="enableToggler" validatorId=<%# Eval("Id")%> style="cursor:pointer" ><%# IIf( Eval("Enabled"),"Disable", "Enable")%></a>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField>
                <ItemStyle Width="50px" />
                <ItemTemplate>
                           <asp:LinkButton runat="server" Text="Delete" CommandName='DeleteAction' CommandArgument='<%# Eval("Id") %>' OnClientClick='return confirm("Are you sure you want to delete this action?")' />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>