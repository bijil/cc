﻿<%@ Page Title="" Language="vb"  MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="sketchsetup.aspx.vb" Inherits="CAMACloud.Console.sketchsetup" EnableEventValidation="false" ValidateRequest="false"%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--<h1>Sketch Configuration</h1>
    <p class="info">
        Enter/Edit the sketch configuration JSON, or select a pre-existing configuration from the list.
    </p>
    <div>
        <div>
            Use Configuration: 
            <asp:DropDownList runat="server" ID="ddlConfig">
                <asp:ListItem Text="-- None --" Value=""></asp:ListItem>
                <asp:ListItem Text="PACS80"></asp:ListItem>
                <asp:ListItem Text="RapidSketch"></asp:ListItem>
                <asp:ListItem Text="[User Defined]" Value="UserDefined"></asp:ListItem>
            </asp:DropDownList>
        </div>
        <div>
            User-defined Configuration JSON:<br />
            <asp:TextBox runat="server" ID="txtJSON" TextMode="MultiLine" Rows="20" Columns="80" />
        </div>
        
    </div>--%>
   <link href="/App_Static/css/evol-colorpicker.min.css" rel="stylesheet" type="text/css" />
   <script type="text/javascript" src="/App_Static/jslib/evol-colorpicker.js"></script>
   <link href="/App_Static/css/wcolpick.css" rel="stylesheet" type="text/css" />
   <script type="text/javascript" src="/App_Static/jslib/wcolpick.js"></script>
   
   <style type="text/css">
   		input.searchBox {
		    height: 18px;
		    padding-left: 25px;
		}
		
       .searchBoxDiv::before {
		    z-index: 10000;
		    content: '';
		    position: absolute;
		    left: 5px;
		    top: 5px;
		    width: 9px;
		    height: 9px;
		    border: 1px solid;
		    display: block;
		    border-radius: 100px;
		}
		
		.searchBoxDiv::after {
		    z-index: 10000;
		    content: '';
		    position: absolute;
		    left: 16px;
		    top: 13px;
		    width: 1px;
		    height: 7px;
		    background: #000;
		    display: block;
		    transform: rotate(
			-45deg
			);
		}
		.picker-popup {
			display:none;
			position:fixed;
			height:100vh;
			width:100vw;
			top:0;
			left:0;
			background: #000000aa;
		}
		.picker {
		  border: 1px solid #ccc;
		  width: 450px;
		  margin:0 auto;
		  display: flex;
		  flex-wrap: wrap;
		  background:#e4eaec;
		  position:absolute;
		  top:50%;
		  left:0;
		  right:0;
		  transform:translateY(-50%);
	      border-radius: 3px;
		}
		.picker__title {
		    flex: 0 100%;
		    flex-grow: 1;
		    display: flex;
		    justify-content: space-between;
		    background: #fff;
		}
		.picker__title p {
		    margin: 10px 8px;
		    font-weight: 700;
		    font-size: 1.2em;
		}
		.picker__title span.close {
		    display: block;
		    padding-right: 8px;
		    transform: rotate(45deg);
		    font-size: 2em;
		    cursor: pointer;
		}
		.picker__title span.close:hover {
			color: #7b0000;
		}
		
		.picker__left {
		  flex: 1;
		  border-right: 1px solid #fff;
		  padding: 8px;
		  min-height:265px;
		}
		
		.picker__left-head {
		  display: flex;
		}
		
		.picker__left-type {
		  margin: 0;
		  padding: 6px 10px;
		  border: none;
		  background:#f9fafb;
		  cursor:pointer;
    	  border-bottom: 2px solid #eee;
		}
		
		.clrPickerSelected {
			border-color: #008C4D;
		}
		
		.picker__item {
		  padding: 8px;
		  display: flex;
		  background: #f9fafb;
	      border-bottom: 1px solid #eee;
		  border-top: 1px solid #eee;
		  border-left: 8px solid transparent;
		  cursor: pointer;
		}
		.pickerSelected {
		    border-left: 8px solid #0d7906;
		}		
		.picker__item:not(.pickerSelected):hover {
		  background: #f1f1f1;
		}
		
		
		.picker__item span {
		  display: block;
		  width: 18px;
		  height: 18px;
		  margin-right: 6px;
		  border:1px solid #eee;
		}
		
		.picker__item p {
		  margin: 0;
		}
		.picker__standard .evo-pop {
			border-radius:0;
			border:none;
		}
		
		.picker__right {
		  min-width: 140px;
		  padding: 8px;
	      display: flex;
    	  flex-direction: column;
		}
		
		.picker__button {
			display: flex;
			justify-content: space-between;
			margin-top:auto;
		}
		.picker__button button {
		    border: 1px solid #eee;
		    padding: 8px;
		    border-radius: 3px;
		    border:1px solid #00582e;
		    cursor: pointer;
		    background: #00582e;
		    color: #fff;
		    min-width: 65px;
		    text-align: center;
		}
		.picker__button button:hover {
			background:#05a85c;
		}
		.picker__button button.clrWindowCancel {
			border-color: #3e3e3e;
			color:#fff;
			background: #3e3e3e;
		}
		.picker__button button.clrWindowCancel:hover {
			border-color: #585858;
			background: #585858;
		}
		.picker__custom {
			width:210px;
		}
		.wcolpick_colors {
			display:none;
		}
		.evo-pop {
    		width: 270px;
		}
		.evo-palette2 td {
		    padding: 8px 9px;
		}
		.picker__standard {
			height: 356px;
		}	
		
    </style>
    
    <script>
    	var bulkEditData = [];
    	$(function () {
             $('.picker__standard').standardcolorpicker({
            	defaultPalette:'web',
            	history: false,
            }); 
			$('.picker__standard').on("change.color", function(event, color) {
				$('.pickerSelected').children('span').css('background-color', color);
				$('.pickerSelected').children('span').val(color);
			});
			
			$('.picker__custom').loads({
				layout:'rgbhex',
				compactLayout:true,
				enableSubmit:false,


				onChange: function(ev) {
					$('.pickerSelected').children('span').css('backgroundColor', '#' + ev.hex);
					$('.pickerSelected').children('span').val('#' + ev.hex);
  				}
			});
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        var avgCount = 0;
        //prm.add_endRequest(function () {
        //    $('.pickcolor').colorpicker();
       // });
        
        function setColorInGrid(){
        
        }
        
        function colorband1() {
            var clrcode = $('.ddlcolor1').val();
            var satPercentage = 20, satAdj = 1, valAdj = 0;
            var darkShade = 0, i = 0;
            var rowscount = $('#lblkpLeft option:selected').length - 1; 
            //var rowscount = $('.popupcolorpick').length - 1;
            avgCount = Math.round(rowscount / 2);
            var incrVal = (1 / (avgCount + 1));
            satAdj = ((1 / avgCount - 0.01)) * 100;
            if (clrcode != " ") {
                if (clrcode == "1") {
                    $(".lookupload .pickcolor").each(function () {

                        var randColor = GetRandomColor();
                        var defRandVal = $(this).colorpicker('getValue', '#ffffff');
                        if (defRandVal == '#ffffff') {
                            $('.pickcolor input').val(randColor);
                            $(this).colorpicker('setValue', randColor);
                        }
                        else {
                            $(this).colorpicker('setValue', randColor);
                        }
                    });
                }
                else {

                    $(".lookupload .pickcolor").each(function () {
                        i = i + 1;
                        if (i < avgCount)
                            darkShade = 0;
                        else
                            darkShade = 1;

                        if (darkShade == "0") {
                            var colColor = hexColorWithSaturation(clrcode, satPercentage);
                            var defVal = $(this).colorpicker('getValue', '#ffffff');
                            if (defVal == '#ffffff') {
                                $('.pickcolor input').val(colColor);
                                $(this).colorpicker('setValue', colColor);
                                satPercentage = satPercentage + satAdj;
                            }
                            else {
                                $(this).colorpicker('setValue', colColor)
                                satPercentage = satPercentage + satAdj;

                            }
                        }
                        else {
                            var valColor = colorLuminance(clrcode, -valAdj);
                            var defdarkVal = $(this).colorpicker('getValue', '#ffffff');
                            if (defdarkVal == '#ffffff') {
                                $('.pickcolor input').val(valColor);
                                $(this).colorpicker('setValue', valColor);
                            }
                            else {
                                $(this).colorpicker('setValue', valColor);
                            }
                            valAdj = valAdj + incrVal;
                        }

                    });
                }
            }
        }
        
        
        function colorband() {
            var clrcode = $('.ddlcolor').val();
            var satPercentage = 20, satAdj = 1, valAdj = 0;
            var darkShade = 0, i = 0;
            var rowscount = $('.popupcolorpick .mGrid tr').length - 1;
            avgCount = Math.round(rowscount / 2);
            var incrVal = (1 / (avgCount + 1));
            satAdj = ((1 / avgCount - 0.01)) * 100;
            if (clrcode != " ") {
                if (clrcode == "1") {
                    $(".popupcolorpick .mGrid .pickcolor").each(function () {

                        var randColor = GetRandomColor();
                        var defRandVal = $(this).colorpicker('getValue', '#ffffff');
                        if (defRandVal == '#ffffff') {
                            $('.pickcolor input').val(randColor);
                            $(this).colorpicker('setValue', randColor);
                        }
                        else {
                            $(this).colorpicker('setValue', randColor);
                        }
                    });
                }
                else {

                    $(".popupcolorpick .mGrid .pickcolor").each(function () {
                        i = i + 1;
                        if (i < avgCount)
                            darkShade = 0;
                        else
                            darkShade = 1;

                        if (darkShade == "0") {
                            var colColor = hexColorWithSaturation(clrcode, satPercentage);
                            var defVal = $(this).colorpicker('getValue', '#ffffff');
                            if (defVal == '#ffffff') {
                                $('.pickcolor input').val(colColor);
                                $(this).colorpicker('setValue', colColor);
                                satPercentage = satPercentage + satAdj;
                            }
                            else {
                                $(this).colorpicker('setValue', colColor)
                                satPercentage = satPercentage + satAdj;

                            }
                        }
                        else {
                            var valColor = colorLuminance(clrcode, -valAdj);
                            var defdarkVal = $(this).colorpicker('getValue', '#ffffff');
                            if (defdarkVal == '#ffffff') {
                                $('.pickcolor input').val(valColor);
                                $(this).colorpicker('setValue', valColor);
                            }
                            else {
                                $(this).colorpicker('setValue', valColor);
                            }
                            valAdj = valAdj + incrVal;
                        }

                    });
                }
            }
        }
        
        function hexColorWithSaturation(hex, percent) {
            if (!/^#([0-9a-f]{6})$/i.test(hex)) {
                throw ('Unexpected color format');
            }
            var r = parseInt(hex.substr(1, 2), 16),
	        g = parseInt(hex.substr(3, 2), 16),
	        b = parseInt(hex.substr(5, 2), 16),
	        sorted = [r, g, b].sort(function (a, b) { return a - b; }),
	        min = sorted[0],
	        med = sorted[1],
	        max = sorted[2];
	
	            if (min == max) {
	                return hex;
	            }
	
	            var max2 = max,
	        rel = (max - med) / (med - min),
	        min2 = max / 100 * (100 - percent),
	        med2 = isFinite(rel) ? (rel * min2 + max2) / (rel + 1) : min2,
	        rgb2hex = function (rgb) { return '#' + rgb[0].toString(16) + rgb[1].toString(16) + rgb[2].toString(16); },
	        hex2;
            min2 = Math.round(min2);
            med2 = Math.round(med2);

            if (r == min) {
                hex2 = rgb2hex(g == med ? [min2, med2, max2] : [min2, max2, med2]);
            }
            else if (r == med) {
                hex2 = rgb2hex(g == max ? [med2, max2, min2] : [med2, min2, max2]);
            }
            else {
                hex2 = rgb2hex(g == min ? [max2, min2, med2] : [max2, med2, min2]);
            }

            return hex2;
        }

        function loadPopup(popupWindow, title) {
            $('.' + popupWindow).dialog({
                modal: true,
                width: 450,
                height: 225,
                resizable: false,
                title: title,
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                    $(this).parent().css('z-index','5004')
                    $('body').css('overflow','auto')
                }
            });
             $('.ui-widget-overlay').css('z-index','5003');
        }
        
        function loadPopup1(popupWindow, title) {
            $('.' + popupWindow).dialog({
                modal: true,
                width: 500,
                height: 475,
                resizable: false,
                title: title,
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                    $(this).parent().css('z-index','5004')
                }
            });
            $('.ui-widget-overlay').css('z-index','5003');
            if (title == 'Choose Lookups') {
            	bulkEditData = [];
        		var options = $('.lkpData')[0].options;
			    for (x in options) {
			        if (options[x].tagName) {
			            bulkEditData.push({ 'label': options[x].label, 'value': options[x].value });
			        }
			    }
            }
        }
        
        function hidePopup(poptoclose) {
            $('.' + poptoclose).dialog('close');
        }
        
        function ListBoxValid(sender, args) {
            var ctlDropDown = document.getElementById(sender.controltovalidate);
            args.IsValid = ctlDropDown.options.length > 0;
        }
        
        function Rand(min, max) {
            return parseInt(Math.random() * (max - min + 1), 10) + min;
        }

        function GetRandomColor() {
            var h = Rand(1, 360); // color hue between 1 and 360
            var s = Rand(30, 100); // saturation 30-100%
            var l = Rand(30, 70); // lightness 30-70%
            return 'hsl(' + h + ',' + s + '%,' + l + '%)';
        }
        
        function colorLuminance(hex, lum) {
            hex = String(hex).replace(/[^0-9a-f]/gi, "");
            if (hex.length < 6) {
                hex = hex.replace(/(.)/g, '$1$1');
            }
            lum = lum || 0;
            var rgb = "#",
                c;
            for (var i = 0; i < 3; ++i) {
                c = parseInt(hex.substr(i * 2, 2), 16);
                c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
                rgb += ("00" + c).substr(c.length);
            }
            return rgb;
        }
        
        function OnSearchBoxChanging(searchField) {
        	var searchText = $(searchField).val(), resultData = [];
        	if (searchText == '') 
        		resultData = bulkEditData;
        	else {
			    if(searchText && typeof(searchText) == "string") 
			    	searchText = searchText.replace('>','&gt;').replace('<','&lt;');
			    resultData = bulkEditData.filter(function (el) { return (el.label.toLowerCase().indexOf(searchText.toLowerCase()) != -1) || (el.value.toLowerCase().indexOf(searchText.toLowerCase()) != -1); });
        	}
        	$('.lkpData').html('');
        	for (i in resultData) {
            	$('.lkpData').append('<option value="'+ resultData[i].value +'">'+ resultData[i].label +'</option>');
        	}
        	if(resultData.length == 0){
    				$('.clrchoose').prop('disabled', true);
    				$('#MainContent_MainContent_lkpSave').prop('disabled',true);
    			}
				else{
    				$('.clrchoose').prop('disabled', false);
    				$('#MainContent_MainContent_lkpSave').prop('disabled', false);
    				
    			}
        	$('.lkpData').scrollTop(0)
        }
        
        function fillBackColor() {
        	$('.colorGridBox[value != ""]').each( (index, clrGridBox) => { 
				var bclrcode = clrGridBox.value;
				bclrcode = bclrcode.split('~')[0];
				$(clrGridBox).siblings('.colorbox').css({ "background-color": bclrcode, "display": "block" });
			});	
        }
        
        function loadColorPopup(popupWindow, colorCode, rowuid, isBulEditClick ) {
            /*$('.' + popupWindow).dialog({
                modal: true,
                width: 400,
                height: 375,
                resizable: false,
                title: 'Set Color',
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                    $(this).parent().css('z-index','5004');
                }
            });
            $('.ui-widget-overlay').css('z-index','5003');*/
            $('.picker-popup').show();
            $('.evo-more').css('display', 'none');
            $('.picker__item').removeClass('pickerSelected'); 
            $('.picker__left-type').removeClass('clrPickerSelected');
            $('.picker__left-type:first-child').addClass('clrPickerSelected');
            $('.picker__custom').css('display', 'none');
            $('.picker__standard').css('display', 'block');
            $('.skclr').parent('.picker__item').addClass('pickerSelected');
            splitColorcodes = colorCode.split('~');
            rowuid = rowuid? rowuid: '';  isBulEditClick = isBulEditClick? 'bulk': 'nbulk';
            $('.clrwindowSave').attr({ 'rowuid': rowuid, 'bulk': isBulEditClick });
            var skColor = splitColorcodes[0]? splitColorcodes[0]: '';
            var lineColor = splitColorcodes[1]? splitColorcodes[1]: '';
            var labelColor = splitColorcodes[2]? splitColorcodes[2]: '';
            $('.skclr').css('background-color', skColor);  $('.skclr').val(skColor);
            $('.lineclr').css('background-color', lineColor); $('.lineclr').val(lineColor);
            $('.labelclr').css('background-color', labelColor);  $('.labelclr').val(labelColor);
        }
        
        function gridclrBoxFun(clrboxelmt, isEditClick, isBulEditClick) {
        	var clrcode = "", rowuid = "";
        	if (isBulEditClick) {
        		clrcode = $(clrboxelmt).siblings('.bulkcolortextbox').val();
        	}
        	else {
        		rowuid = $(clrboxelmt).siblings('.hiddenRowuidClass').val();
	        	if (isEditClick)
	        		clrcode = $(clrboxelmt).parent().siblings().find('.colorGridBox')[0]? $(clrboxelmt).parent().siblings().find('.colorGridBox').val(): '';
	        	else
	        		clrcode = $(clrboxelmt).siblings('.colorGridBox').val();
        	}
        	clrcode = clrcode? clrcode: '';		
        	loadColorPopup('picker', clrcode, rowuid, isBulEditClick);
        	return false;
        }
        
        function clrWindowSave(clrSaveButton) {
        	var rowuid = $('.clrwindowSave').attr('rowuid');
        	var isBulk = $('.clrwindowSave').attr('bulk');
        	var skclr = $('.skclr').val(), lineclr = $('.lineclr').val(), labelclr = $('.labelclr').val();
        	var clrcode = skclr + '~' + lineclr + '~' + labelclr;
        	if (isBulk == 'bulk') {
        		$('.picker-popup').hide();
        		$('.bulkcolortextbox').val(clrcode);
        		$('.bulkcolortextbox').siblings('.bulkcolorbox').css('background-color', skclr);
        		return false;
        	}
        	else {
        		$.ajax({
	                url: "sketchsetup.aspx/saveColorData",
	                data: "{ 'colorCode':'" + clrcode + "','rowuid':'" + rowuid + "' }",
	                dataType: "json",
	                type: "POST",
	                contentType: "application/json; charset=utf-8",
	                success: function (data) {
	                	$('.picker-popup').hide();
	                	$($('.hiddenRowuidClass[value="'+ rowuid +'"')[0]).siblings('.colorGridBox').val(clrcode);
	                	$($('.hiddenRowuidClass[value="'+ rowuid +'"')[0]).siblings('.colorbox').css('background-color', skclr);
	                	setTimeout(function() { alert('Saved successfully'); }, 500);
	                	return false;
	                },
	                error: function (response) {
	                	$('.picker-popup').hide();
	                    alert('Save failed');
	                    return false;
	                }
	            });
        	}
        }
        
        function pickerItemSelection(pickerItemDiv) {
        	$('.picker__item').removeClass('pickerSelected');
        	$(pickerItemDiv).addClass('pickerSelected');
        	return false;
        }
        
        function switchClrPickers(clrPicker, pickVal) {
        	$('.picker__left-type').removeClass('clrPickerSelected');
        	$(clrPicker).addClass('clrPickerSelected');
        	if (pickVal == 'standard') {
        		$('.picker__custom').css('display', 'none');
        		$('.picker__standard').css('display', 'block');
        	}
        	else {
        		$('.picker__standard').css('display', 'none');
        		$('.picker__custom').css('display', 'block');
        	}
        }
        function closeColorPopup(){
        	$('.picker-popup').hide();
        }
        
        function showMask(info) {		
		    $('.masklayer').height($(document).height())
		    $('.masklayer').show();		    
		        info = 'Please wait ...'
		}
		
		function hideMask() {
   		$('.masklayer').hide();
		}
    </script>
    
    <h1>Sketch Lookup Color Setup</h1>
    <p runat ="server" id="head">Add/Edit color for sketch lookups</p>
    
    <div> 
	    <asp:UpdatePanel runat="server" class="sketchlookupclr">
			<ContentTemplate> 
				 <asp:LinkButton ID="lkBulkEdit" runat="server" style = "font-size: 17px;" OnClick="lkBulkEdit_Click" Enabled="true">Bulk Edit</asp:LinkButton><br><br>
			     <div ID="pageSize_div" style="Width: 725px;">
				 	No. of items per page:
					<asp:DropDownList runat="server" ID="ddlPageSize" AutoPostBack="true" width="50px">
				        <asp:ListItem Value="25" />
				        <asp:ListItem Value="50" />
				        <asp:ListItem Value="100" />
			         </asp:DropDownList>
			         <asp:LinkButton ID="lkBulkRemove" runat="server" style = "font-size: 14px; float: right; margin-left: 10px;" OnClick="lkBulkRemove_Click" Enabled="true">Remove All</asp:LinkButton>
			         <asp:LinkButton ID="lkExport" runat="server" style = "font-size: 14px; float: right;" OnClick="lkexport_Click" Enabled="true">Export </asp:LinkButton>
			     </div>
			     <asp:GridView runat="server" ID="gvSketchLookupColor" AutoGenerateColumns="False" AllowPaging="true" PageSize = "25" OnPageIndexChanging="OnPageIndexChanging" Width="725px" SelectedIndex="0">
			        <Columns>
			            <asp:BoundField DataField="IdValue" HeaderText="Code" ItemStyle-Width="200px" >
			            <ItemStyle Width="200px" />
			            </asp:BoundField>
			            <asp:BoundField DataField="DescValue" HeaderText="Name" ItemStyle-Width="200px" >
			              <ItemStyle Width="200px" />
			            </asp:BoundField>
			            
			            <asp:TemplateField HeaderText="Color">
				           <ItemStyle Width="200px" />
				           <ItemTemplate>
					            <div class="input-group" style="width:203px;">
					            	<asp:TextBox runat="server" ID="hiddenRowuid" CssClass="hiddenRowuidClass" style="display: none;" value='<%# Eval("Id") %>' /> 
						            <asp:TextBox runat="server" ID="colorcode" CssClass="colorGridBox" HeaderText="Color Code" style="display: none;" ItemStyle-Width="120px" value='<%# Eval("ColorCode") %>' />
						            <span class="colorbox" onclick="return gridclrBoxFun(this)" style="width:20px; height:20px; display:block"></span>
				            	</div>
				           </ItemTemplate>
			            </asp:TemplateField>
			            <asp:TemplateField>
				            <ItemStyle Width="90px" />
				            <ItemTemplate> <%-- CommandName='EditAction' CommandArgument='<%# Eval("Id") %>' --%>
				            	<asp:TextBox runat="server" ID="hiddenRowuid1" CssClass="hiddenRowuidClass" style="display: none;" value='<%# Eval("Id") %>' /> 
				            	<asp:LinkButton runat="server" ID="lkEditColor" Text="Edit"  OnClientClick="return gridclrBoxFun(this, true);"/>
				            	<asp:LinkButton runat="server" ID="lkRemoveColor" Text="Remove"  CommandName='RemoveAction' CommandArgument='<%# Eval("Id") %>' />
				            </ItemTemplate>
			            </asp:TemplateField>
			        </Columns>
			    </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
	
	
   
	<div class="popupcolorpick" style="display: none">
	 <asp:UpdatePanel runat="server" ID="Updatepanelcolor">
    <ContentTemplate>
                <table style="padding: 15px; border-collapse: separate; border-spacing: 0 8px;">
                    <%-- <asp:Panel runat="server" ID="Panel1" CssClass="color-edit-panel" <%--Style="width: 500px; height: 300px; margin-top: 14px;"--%>
                    <asp:Panel runat="server" ID="pnlcolorEdit" CssClass="color-edit-panel">
                        <tr>
                            <div>
                                <td>
                                    <label id="Label1">Pick a color</label></td>
                                <td>
                                    <select class="ddlcolor" width="240px" onchange="colorband()">
                                        <option value=" ">-- Select --</option>
                                        <option value="#ff0000">Red</option>
                                        <option value="#0000ff">Blue</option>
                                        <option value="#00FF00">Green</option>
                                        <option value="#FFFF00">Yellow</option>
                                        <option value="#ee82ee">Violet</option>
                                        <option value="#00FFFF">Cyan</option>
                                        <option value="1">Random</option>
                                    </select></td>
                            </div>
                        </tr>
                        <tr>
                        <div>
                            <td colspan="4">
                                <asp:GridView runat="server" ID="gridfield" AutoGenerateColumns="False" DataKeyNames="Id">
                                    <Columns>
                                        <asp:BoundField DataField="DescValue" HeaderText="Description" ItemStyle-Width="180px" />
                                        <asp:TemplateField HeaderText="Color Code">
                                            <ItemStyle Width="220px" />
                                            <ItemTemplate>
                                                <div class="input-group">
                                                    <asp:TextBox runat="server" ID="colorcode" HeaderText="Color Code" ItemStyle-Width="120px"
                                                        value='<%# Eval("ColorCode") %>' />
                                                    <span class="input-group-addon"><i style= "vertical-align : top;" ></i></span>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </div>    
                        </tr>
                            <div class="savecolor">
                                <td colspan="2" style="padding-left: 130px;">
                                    <asp:Button ID="btnsaveclr" runat="server" Text="Save" />
                                    <asp:Button ID="btncancel" runat="server" Text="Cancel" OnClientClick="hidePopup('popupcolorpick'); " /></td>
                            </div>
                        </tr>

                    </asp:Panel>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    
    
     <div class="lookupload" style="display: none;margin-left:110px">
        <asp:UpdatePanel ID="lookupUpdate" runat="server">
            <ContentTemplate>
                <table>
                	<asp:Panel runat="server" ID="pnlBulkcolorEdit" CssClass="color-edit-panel">
	                    <tr>
	                        <td>
	                        	<div class="searchBoxDiv" style="position: relative;">
	                        		<input class="searchBox" style="width: 220px;" onkeyup="OnSearchBoxChanging(this);"/>
	                        	</div>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td>
	                            <asp:ListBox ID="lblkpLeft" SelectionMode = "multiple" CssClass="lkpData" runat="server" Rows="18" Width="250px" DataTextField="DescValue" DataValueField="Id"></asp:ListBox>
	                        </td>
	                    </tr>
	                    <tr>
	                    	<td>
	                            <%-- <select class="ddlcolor1" width="240px" onchange="fillcolorband()">
	                                <option value=" ">-- Select --</option>
	                                <option value="#ff0000">Red</option>
	                                <option value="#0000ff">Blue</option>
	                                <option value="#00FF00">Green</option>
	                                <option value="#FFFF00">Yellow</option>
	                                <option value="#ee82ee">Violet</option>
	                                <option value="#00FFFF">Cyan</option>
	                                <option value="Custom">Custom</option>
	                            </select> --%>
			                    <div class="input-group" style="display: flex; align-items: center;">
		                            <asp:TextBox runat="server" ID="colorcode" class="bulkcolortextbox"  style="Width=120px;margin-top:5px; display: none;" />
		                            <button class="clrchoose" onclick="gridclrBoxFun(this, null, true); return false;">Choose color</button>
		                            <span class="bulkcolorbox" onclick="return gridclrBoxFun(this, null, true)" style="width:21px; height:21px; display:inline-block; margin-left: 4px;"></span>
			                    </div>
	                    	</td>
	                    </tr>
						<tr>                    
	                        <td>
	                            <asp:Button ID="lkpSave" runat="server" Text="Save" ValidationGroup="UserProp" OnClientClick="showMask();hidePopup('lookupload');" />
	                            <asp:Button ID="lkpCancel" runat="server" Text="Cancel" OnClientClick="hidePopup('lookupload');" />
	                        </td> 
	                    </tr>
                    </asp:Panel>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    
    <div class="picker-popup " style="z-index: 5020;">
    	<div class="picker-mask"></div>
	    <div class="picker">
	    	<div class="picker__title">
	    		<p>Colors</p>
	    		<span class="close" onclick="closeColorPopup();">+</span>
    		</div>
			<div class="picker__left">
				<div class="picker__left-head">
				  <p class="picker__left-type clrPickerSelected" onclick="switchClrPickers(this, 'standard')">Standard </p>
				  <p class="picker__left-type" onclick="switchClrPickers(this, 'custom')">Custom </p>
				</div>
				<div class="picker__standard" >
				</div>
				<div class="picker__custom" style="display: none;">
				</div>
			</div>
			<div class="picker__right">
				<div class="picker__item" onclick="pickerItemSelection(this)">
				  <span class="skclr"></span>
				  <p>Sketch</p>
				</div>
				<div class="picker__item" onclick="pickerItemSelection(this)">
				  <span class="lineclr"></span>
				  <p>Line</p>
				</div>
				<div class="picker__item" onclick="pickerItemSelection(this)">
				  <span class="labelclr"></span>
				  <p>Label</p>
				</div>
				<div class="picker__button">
				  <button class="clrwindowSave" onclick="clrWindowSave(this); return false;">Save</button>
				  <button class="clrWindowCancel" onclick="closeColorPopup(); return false;">Cancel</button>
				</div>
	  		</div>
		</div>
	<div>
    
</asp:Content>
