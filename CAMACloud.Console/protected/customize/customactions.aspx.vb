﻿Public Class customactions1
    Inherits System.Web.UI.Page

    Sub LoadFieldList() Handles ddlTable.SelectedIndexChanged
        If ddlTable.SelectedValue = "" Then
            ddlField.FillFromSql("SELECT Id, Name FROM DataSourceField WHERE TableId = 0 ORDER BY Name", True, "-- Select Table --")
        Else
            ddlField.FillFromSql("SELECT Id, Name FROM DataSourceField WHERE TableId = " & ddlTable.SelectedValue & " ORDER BY Name", True)
        End If
    End Sub


    Sub LoadOtherFieldList() Handles ddlTable2.SelectedIndexChanged
        If ddlTable2.SelectedValue = "" Then
            ddlField2.FillFromSql("SELECT Id, Name FROM DataSourceField WHERE TableId = 0 ORDER BY Name", True, "-- Select Table --")
        Else
            ddlField2.FillFromSql("SELECT Id, Name FROM DataSourceField WHERE TableId = " & ddlTable2.SelectedValue & " ORDER BY Name", True)
        End If
    End Sub

Sub LoadActions()
	    gvCustomActions.DataSource = Database.Tenant.GetDataTable("SELECT cca.Id, CASE WHEN cca.Action='MarkAsCompleteM21' THEN 'MarkAsComplete (Auxiliary Table)' ELSE cca.Action END AS Action, cca.TableName, cca.FieldName, CASE WHEN f.Id IS NULL THEN cca.ValueTag ELSE f.SourceTable + '.' + f.Name END As ValueTag,cca.CustomText,cca.Enabled FROM ClientCustomAction cca LEFT OUTER JOIN DataSourceField f ON cca.ValueTag = CAST(f.Id AS VARCHAR) ORDER BY cca.Action, cca.Ordinal, cca.Id")
        gvCustomActions.DataBind()
    End Sub
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
        	ddlTable.FillFromTable(BusinessLogic.DataSource.GetParcelTables, True)
        	ddlTable2.FillFromTable(BusinessLogic.DataSource.GetParcelTables, True)
            LoadFieldList()
            LoadActions()
            ClearForm()
            txtValue.Text = ""
            CustText.Visible = False
            trOther1.Visible = False
            trOther2.Visible = False
        Else
        	LoadActions()
        End If
    End Sub

    Private Sub gvCustomActions_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvCustomActions.RowCommand
        Select Case e.CommandName.ToString
            Case "DeleteAction"
                Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT Action FROM ClientCustomAction cca WHERE Id = " & e.CommandArgument.ToString) 
                Dim Action As String = dr("Action").ToString
            	 
            	Database.Tenant.Execute("DELETE FROM ClientCustomAction WHERE Id = " & e.CommandArgument.ToString)
            	SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),"Custom Action "+Action.ToString+" has been Deleted.")
                  
                LoadActions()
            Case "EnableOrDisable"
            	'Database.Tenant.Execute("UPDATE ClientCustomAction SET Enabled=(SELECT ~Enabled FROM ClientCustomAction WHERE Id ={0} ) WHERE Id = {0}".SqlFormatString(e.CommandArgument.ToString))
                LoadActions()
        End Select
        'Response.Redirect(Request.Url.AbsoluteUri)
    End Sub
    Sub ClearForm()
    	ddlAction.SelectedIndex = 0
    	ddlValue.SelectedIndex = 0
    	ddlField2.FillFromSql("SELECT Id, Name FROM DataSourceField WHERE TableId = 0 ORDER BY Name", True, "-- Select Table --")
    	ddlTable.FillFromSql("SELECT Id, Name FROM DataSourceTable", True)     	
        ddlField.FillFromSql("SELECT Id, Name FROM DataSourceField WHERE Id = 0", True, "-- Select Table --")
        txtValue.Text = ""
        CustText.Visible = False
    End Sub
    Private Sub btnAddAction_Click(sender As Object, e As System.EventArgs) Handles btnAddAction.Click
    	Page.Validate()
    	 Dim value As String = ddlValue.SelectedValue
    	 Dim action_Value As String = ddlAction.SelectedValue
         Dim customValue As String
         Dim MaxValue As Integer = Database.Tenant.GetIntegerValue("Select maxLength From DatasourceField where SourceTable = '"+ddlTable.SelectedItem.text+"' and Name = '"+ddlField.SelectedItem.text+"' ")
         Dim table_count As Integer = Database.Tenant.GetIntegerValue("SELECT count(*) FROM DataSourceTable WHERE ImportType = 1 AND Name = '"+ddlTable.SelectedItem.text+"'")
         If action_Value <> "MarkAsCompleteM21" And table_count > 0 Then
         	Alert("The Target Table you have selected is an Auxiliary Table that is many-to-one with the Parcel Table. Please select the Mark as Complete (Auxiliary Table) option or consult with staff on how to accomplish what you are trying to configure.")
         	Return
         End If
         If value = "Other" Then
            value = ddlField2.SelectedValue
        End If
        If value = "CustomValue" Then
            customValue = txtValue.Text
            Dim intValue As Integer
            If Integer.TryParse(customValue, intValue) Then
                If intValue > MaxValue Then
                    Alert("The Custom Value entered is greater than " & MaxValue.ToString() & ", which is the max length for " & ddlTable.SelectedItem.Text & "." & ddlField.SelectedItem.Text)
                    Return
                End If
            End If
        End If
        Dim sql As String = "INSERT INTO ClientCustomAction (Action, TableName, FieldName, ValueTag,CustomText) VALUES ( {0}, {1}, {2}, {3}, {4} );"
        Database.Tenant.Execute(sql.SqlFormat(False, ddlAction.SelectedValue, ddlTable.SelectedItem.Text, ddlField.SelectedItem.Text, value, customValue))
        Dim Description As String = "New Custom Action "+ddlAction.SelectedValue+" Added."
        SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
        LoadActions()
        ClearForm()
        Alert("New custom action has been added successfully.")
    End Sub
    Private Sub ddlValue_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlValue.SelectedIndexChanged
        If ddlValue.SelectedValue = "Other" Then
            trOther1.Visible = True
            trOther2.Visible = True
        Else
            trOther1.Visible = False
            trOther2.Visible = False
        End If
        If ddlValue.SelectedValue = "CustomValue" Then
            txtValue.Text = ""
            CustText.Visible = True
        Else
            txtValue.Text = ""
            CustText.Visible = False
        End If
    End Sub



    Private Sub gvCustomActions_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvCustomActions.PageIndexChanging

        gvCustomActions.PageIndex = e.NewPageIndex
        LoadActions()
    End Sub
    Protected Sub ddlAction_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAction.SelectedIndexChanged
        If ddlAction.SelectedValue = "saveparcelchanges" Then       
            ddlValue.Items(0).Enabled = False
        Else
            ddlValue.Items(0).Enabled = True
        End If
    End Sub
End Class