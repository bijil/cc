﻿Public Class searchOptions
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
	If Not IsPostBack Then
    		loadSearchFilterFields()
    		RunScript("UpdateChecked();")
	End If
    End Sub
    
    Sub loadSearchFilterFields
        Dim keyField As String = DataBase.Tenant.GetStringValue("SELECT KeyField1 FROM Application")
        Dim Sql As String = "SELECT 
    f.Id,
    f.DisplayLabel,
    f.IncludeInSearchFilter,
    f.FunctionName,
    f.FieldName,
    f.TableName,
    CASE 
        WHEN f.FunctionName IS NOT NULL THEN 
            CONCAT('_agg_', REPLACE(f.FunctionName, '_', ''), REPLACE(f.FieldName, '_', ''), REPLACE(f.TableName, '_', ''))
        ELSE NULL 
    END AS IfAggFuncShowThen
FROM 
    (
        SELECT 
            f.Id,
            f.DisplayLabel,
            f.IncludeInSearchFilter,
            NULL AS FunctionName,
            NULL AS FieldName,
            NULL AS TableName
        FROM 
            DataSourceField f 
        INNER JOIN 
            DataSourceTable t ON f.TableId = t.Id 
        INNER JOIN 
            FieldCategory fc ON fc.Id = f.CategoryId 
        WHERE 
            t.ImportType = 0 
            AND f.CategoryId IS NOT NULL 
            AND f.IsCalculated = 0 
            AND f.Name <> 'PARID' 

        UNION ALL

        SELECT  
            agf.ROWUID AS Id,
            CONCAT(agf.FunctionName, '_', agf.TableName, '_', agf.FieldName) AS DisplayLabel,
            agf.showInSearchFilter AS IncludeInSearchFilter,
            agf.FunctionName,
            agf.FieldName,
            agf.TableName
        FROM  
            AggregateFieldSettings agf
    ) AS f;
".SqlFormatString(keyField)
        searchFilterGrid.DataSource = Database.Tenant.GetDataTable(Sql)
    	searchFilterGrid.DataBind()
    End Sub
    
    Protected Sub searchFilterGrid_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles searchFilterGrid.PageIndexChanging
    	searchFilterGrid.PageIndex = e.NewPageIndex
    	loadSearchFilterFields()
    	RunScript("UpdateChecked();")
    End Sub
    
    Protected Sub ddlPageSize_SelectedIndexChanged (sender As Object, e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
    	searchFilterGrid.PageSize = ddlPageSize.SelectedValue
    	searchFilterGrid.PageIndex = 0
    	loadSearchFilterFields()
    	RunScript("UpdateChecked();")
    End Sub
    
End Class