﻿Public Class apprmethods
    Inherits System.Web.UI.Page

    Dim entityName As String = "Method"
    Dim tableName As String = "AppraisalType"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadGrid()
        End If
    End Sub

    Sub LoadGrid()
        grid.DataSource = Database.Tenant.GetDataTable("SELECT * FROM " + tableName + " ORDER BY Name")
        grid.DataBind()
    End Sub

    Private Sub grid_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grid.RowCommand
        Select Case e.CommandName
            Case "EditItem"
                Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM " + tableName + " WHERE Id = " & e.CommandArgument)
                txtName.Text = dr.GetString("Name")
                ViewState("n")=dr.GetString("Name")
                txtDescription.Text = dr.GetString("Description")
                hdnId.Value = dr.GetInteger("Id")

                btnSave.Text = "Save " + entityName
                btnCancel.Visible = True
            Case "DeleteItem"
            	Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM " + tableName + " WHERE Id = " & e.CommandArgument)
                txtName.Text = dr.GetString("Name")
                Database.Tenant.Execute("DELETE FROM " + tableName + " WHERE Id = " & e.CommandArgument)
                'CC_3995
                'Database.Tenant.Execute("DELETE FROM DTR_Tasks WHERE CompiledSQLFilter LIKE 'SelectedAppraisalType =" + e.CommandArgument + "'")
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), "Apprasial Method (" + txtName.Text + ") has been Deleted.")
                'CancelForm()
                'LoadGrid()
                Response.Redirect("apprmethods.aspx")
        End Select
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Dim name As String = Trim(txtName.Text)
        Dim desc As String = Trim(txtDescription.Text)
        Dim AlertMsg As String = ""
        Dim reccount As Integer
        Dim con As String = " AND 1 = 1"
        If hdnId.Value <> "" Then
            con = " AND Id <> " + hdnId.Value
        End If
        Dim csql As String = "SELECT * FROM " + tableName + " WHERE (Name = {0} OR Description = {1}) " + con
        Dim dt As DataRow = Database.Tenant.GetTopRow(csql.SqlFormat(False, Trim(txtName.Text), Trim(txtDescription.Text)))
        If dt IsNot Nothing Then
            If dt.GetString("Name") = Trim(txtName.Text) Then
                AlertMsg = "Appraisal method already exists."
            ElseIf dt.GetString("Description") = Trim(txtDescription.Text) Then
                AlertMsg = "Appraisal Description already exists."
            End If
            reccount = 1
        End If

        If reccount = 0 Then
            If hdnId.Value = "" Then
                Dim sql As String = "INSERT INTO " + tableName + " (Name, Description, Code) VALUES ( {0}, {1}, {0})"
                Database.Tenant.Execute(sql.SqlFormat(False, Trim(txtName.Text), Trim(txtDescription.Text)))
                Dim type As String = txtName.Text
                type = type.Replace("'", "''")
                Dim Description As String = "New Apprasial Method (" + type + ") Added."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
                AlertMsg = "New appraisal method added Successfully."
            Else
                Dim sql As String = "UPDATE " + tableName + " SET Name = {1}, Description = {2}, Code = {1} WHERE Id = {0}"
                Database.Tenant.Execute(sql.SqlFormat(False, hdnId, txtName, txtDescription))
                If (ViewState("n") = txtName.Text) Then
                    SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), "Apprasial Method (" + ViewState("n") + ") has been modified. ")
                Else
                    SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), "Apprasial Method changed from " + ViewState("n") + " to " + txtName.Text + ".")
                End If
                AlertMsg = "The appraisal method has been modified."
            End If
        End If

        'CancelForm()
        'LoadGrid()

        Dim Script As String = "$(document).ready(function () {"

        If AlertMsg.Length > 0 Then
            Script += "alert('" + AlertMsg + "');"
        End If

        Script += "window.location = 'apprmethods.aspx';});"
        RunScript(Script)

    End Sub

    Private Sub CancelForm() Handles btnCancel.Click
        hdnId.Value = ""
        txtName.Text = ""
        txtDescription.Text = ""
        btnSave.Text = "Add " + entityName
        btnCancel.Visible = False
    End Sub

    Protected Sub OnPageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        grid.PageIndex = e.NewPageIndex
        Me.LoadGrid()
    End Sub

End Class