﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master"
    CodeBehind="groupprofile.aspx.vb" Inherits="CAMACloud.Console.groupprofile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function warnDeletion() {
        	if($('#MainContent_MainContent_grid tr').length > 1){
        		var alertMax = false;
        		for (var i = 0; i < $('.filterExp').length; i++ ) {
        			var val= $('.filterExp')[i].value;
        			if (val && val.length > 500)
        				alertMax = 'FilterExpression';	
        		}
        		if(!alertMax) { 
        			if($('#MainContent_MainContent_txtFilter').val() && $('#MainContent_MainContent_txtFilter').val().length > 500)
        				alertMax = 'filter condition';
        		} 
        		if (alertMax) {
        			alert("Sorry, you cannot write " + alertMax + " exceeding 500 characters.");
        			return false;
        		}
            	var msg = "Note: Updating profile setup will delete any rows in the grid which don't have atleast one aggregate selected. \n\nAre you sure you want to continue?"
            	return confirm(msg);
            }
            else {
 				return false;
            }      
        }
        function ischecked(){
        	
      		var checker =document.querySelectorAll("#MainContent_MainContent_grid input[type='checkbox']:checked");
      		checked=checker.length;
        	if (checked ==0 || checked==null ) {
            alert(" You have not selectd any fields. Please select at least one field to continue.");
            return false;
            }
        	else return warnDeletion();
        }	
        
    </script>
    <style>
        .avail-fields
        {
            display:block;
            margin-top:10px;
            margin-bottom:30px;
        }
        
        .avail-fields span
        {
            display:inline-block;
            background:LightSteelBlue;
            border:1px solid SteelBlue;
            border-radius:3px;
            padding:2px 8px;
            margin:2px 2px 3px 15px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
        <%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName()%>
        Profile Setup</h1>
        <p> <asp:CheckBox runat="server" ID="enableprofile" AutoPostBack="True" OnCheckedChanged ="enableprofile_CheckedChanged" /> 
             <span>Enable Group Profile View in MobileAssessor and Quality Control console</span></p>
      <div id="groupprofmain" runat="server" visibility="false">
      <p class="info">
        Select fields to be used in the
        <%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName().ToLower()%> profile.
    </p>
    <table>
        <tr>
        	<td style="padding-right: 20px;">
                Select Table:
            </td>
            <td >
                <asp:DropDownList ID="ddlTable" runat="server" AutoPostBack="true" style="Width:200px;" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlTable"
                    ErrorMessage="*" ValidationGroup="CustomAction" />
            </td>
            <td style="padding-right: 20px;">
                Select field:
            </td>
            <td >
                <asp:DropDownList ID="ddlField" runat="server" AutoPostBack="true" style="Width:200px;" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlField"
                    ErrorMessage="*" ValidationGroup="CustomAction" />
                <asp:Button runat="server" ID="btnAddAction" Text="Add Field" Width="120px" ValidationGroup="CustomAction" />
            </td>&nbsp;&nbsp;&nbsp;
    	</tr>
    </table>
    <asp:GridView runat="server" ID="grid" >
        <Columns>
            <asp:BoundField DataField="Field" HeaderText="Field" ItemStyle-Width="200px" />
            <asp:TemplateField HeaderText="Mean">
                <ItemStyle CssClass="c" Width="60px" />
                <HeaderStyle CssClass="c" />
                <ItemTemplate>
                    <asp:HiddenField runat="server" ID="ROWID" Value='<%# Eval("ROWID") %>' />
                    <asp:HiddenField runat="server" ID="FieldId" Value='<%# Eval("FieldId") %>' />
                    <asp:HiddenField runat="server" ID="Field" Value='<%# Eval("Field") %>' />
                    <asp:HiddenField runat="server" ID="InputType" Value='<%# Eval("DataType") %>' />
                    <asp:CheckBox runat="server" ID="Mean" Checked='<%# Eval("UseMean") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Range">
                <ItemStyle CssClass="c" Width="60px" />
                <HeaderStyle CssClass="c" />
                <ItemTemplate>
                    <asp:CheckBox runat="server" ID="Range" Checked='<%# Eval("UseRange") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Median">
                <ItemStyle CssClass="c" Width="60px" />
                <HeaderStyle CssClass="c" />
                <ItemTemplate>
                    <asp:CheckBox runat="server" ID="Median" Checked='<%# Eval("UseMedian") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Mode">
                <ItemStyle CssClass="c" Width="60px" />
                <HeaderStyle CssClass="c" />
                <ItemTemplate>
                    <asp:CheckBox runat="server" ID="Mode" Checked='<%# Eval("UseMode") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="IQR">
                <ItemStyle CssClass="c" Width="60px" />
                <HeaderStyle CssClass="c" />
                <ItemTemplate>
                    <asp:CheckBox runat="server" ID="IQR" Checked='<%# Eval("UseIQR") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Sum">
                <ItemStyle CssClass="c" Width="60px" />
                <HeaderStyle CssClass="c" />
                <ItemTemplate>
                    <asp:CheckBox runat="server" ID="Sum" Checked='<%# Eval("UseSUM") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Avg">
                <ItemStyle CssClass="c" Width="60px" />
                <HeaderStyle CssClass="c" />
                <ItemTemplate>
                    <asp:CheckBox runat="server" ID="Avg" Checked='<%# Eval("UseAVG") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Min">
                <ItemStyle CssClass="c" Width="60px" />
                <HeaderStyle CssClass="c" />
                <ItemTemplate>
                    <asp:CheckBox runat="server" ID="Min" Checked='<%# Eval("UseMIN") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Max">
                <ItemStyle CssClass="c" Width="60px" />
                <HeaderStyle CssClass="c" />
                <ItemTemplate>
                    <asp:CheckBox runat="server" ID="Max" Checked='<%# Eval("UseMAX") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="FilterExpression">
                <ItemStyle Width="350px" />
                <ItemTemplate>
                    <asp:TextBox style="max-width: 600px;" runat="server" ID="FilterExpression" CssClass="filterExp" Text='<%# Eval("FilterExpression") %>'
                        TextMode="MultiLine" Width="345px" Rows="2" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <table>
        <tr>
            <td style="padding-right: 20px;">
                Common Parcel Data Filter:
                <br />
                <small>(Optional, applies to the all above parcel data fields)</small>
            </td>
            <td>
                <asp:TextBox style="max-width: 720px;" runat="server" ID="txtFilter" TextMode="MultiLine" Rows="4" Columns="60" />
            </td>
        </tr>
        
    </table>
    <div>
        <asp:Button runat="server" ID="btnSave" Text="Update Profile Setup" style="Font-weight:bold; margin-left: 194px; margin-top: 10px;"
            OnClientClick="return ischecked(); " />
    </div>
    <div style="width: 100%; margin-top: 30px;">
        <a name="available-fields"></a>
        <b>Available profile fields:</b>
        <br />
        <asp:Label runat="server" ID="lblFields" CssClass="avail-fields" />
    </div>
    </div>
</asp:Content>
