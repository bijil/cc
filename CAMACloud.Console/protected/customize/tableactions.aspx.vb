﻿Public Class tableactions
    Inherits System.Web.UI.Page

    Sub ClearForm()

        ddlTable.FillFromSql("SELECT Id, Name FROM DataSourceTable WHERE ImportType IN (0, 1)", True)
        ddlField.FillFromSql("SELECT Id, Name FROM DataSourceField WHERE Id = 0", True, "-- Select Table --")
        txtValue.Text = ""
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
        	ClearForm()
        	ViewState("IsAscendingSort") = True
            RefreshGrid()
            txtValue.Text = ""
            CustText.Visible = False
        End If
    End Sub

    Private Sub ddlTable_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTable.SelectedIndexChanged
        If ddlTable.SelectedIndex = 0 Then
            ddlField.FillFromSql("SELECT Id, Name FROM DataSourceField WHERE Id = 0", True, "-- Select Table --")
        Else
            ddlField.FillFromSql("SELECT Id, Name FROM DataSourceField WHERE TableId = " & ddlTable.SelectedValue, True, "-- Select --")
        End If
    End Sub

    Private Sub btnAddAction_Click(sender As Object, e As EventArgs) Handles btnAddAction.Click
    	Dim valTag As String = ""
    	Dim MaximumValue As Integer = Database.Tenant.GetIntegerValue("Select maxLength From  Datasourcefield where SourceTable = '"+ddlTable.SelectedItem.Text+"' and Name = '"+ddlField.SelectedItem.text+"' ")
        If ddlValue.SelectedValue = "CustomText" Then
            valTag = txtValue.Text
            Dim vLen As Integer = IIf((valTag = "{MM/DD/YYYY}" Or valTag = "{MM-DD-YYYY}"), 10, valTag.Length)
            Dim intValue As Integer
            If Integer.TryParse(valTag, intValue) Then
                If valTag > MaximumValue Then
                    Alert("The Custom Value entered is greater than " + MaximumValue.ToString() + ", which is the max length for " + ddlTable.SelectedItem.Text + "." + ddlField.SelectedItem.Text + " ")
                    Return
                End If
            End If
        Else
                valTag = ddlValue.SelectedValue
        End If
        Dim ds As DataTable = Database.Tenant.GetDataTable("Select * From  DataSourceTableAction where Action = '" + ddlAction.SelectedValue + "' and TableId = '" + ddlTable.SelectedValue + "' and FieldName = '" + ddlField.SelectedItem.Text + "' and ValueTag = '" + valTag + "' ")

        If (ds.Rows.Count < 1) Then
            Dim Description As String = "New custom field " + ddlField.SelectedItem.Text + "  for table  " + ddlTable.SelectedItem.Text + " data action added."
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
        End If
        Dim sql As String = "DELETE FROM DataSourceTableAction WHERE Action = {0} AND TableId = {1} AND FieldName = {2} ; INSERT INTO DataSourceTableAction (Action, TableId, FieldName, ValueTag) VALUES ({0}, {1}, {2}, {3})".SqlFormat(False, ddlAction.SelectedValue, ddlTable.SelectedValue, ddlField.SelectedItem.Text, valTag)
        Database.Tenant.Execute(sql)
        ClearForm()
        RefreshGrid()
    End Sub
    Private Sub ddlValue_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlValue.SelectedIndexChanged
        If ddlValue.SelectedValue = "CustomText" Then
            txtValue.Text = ""
            CustText.Visible = True
        Else
            txtValue.Text = ""
            CustText.Visible = False
        End If

    End Sub
    Sub RefreshGrid()
    	Dim dt As New DataTable()
        Dim sql As String = "SELECT dta.*, t.Name As TableName, CASE dta.Action WHEN 'D' THEN 'Delete' WHEN 'E' THEN 'Edit' WHEN 'I' THEN 'Insert' WHEN 'R' THEN 'Edit or Descendent Records Edit' END As ActionName FROM DataSourceTableAction dta INNER JOIN DataSourceTable t ON dta.TableId = t.Id"
        dt = Database.Tenant.GetDataTable(sql)
    	dt.DefaultView.Sort = SortExpression + " " + If(IsAscendingSort, "ASC", "DESC")
    	gvCustomActions.DataSource = dt.DefaultView
        gvCustomActions.DataBind()
    End Sub

    Private Sub gvCustomActions_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvCustomActions.RowCommand
    	If e.CommandName = "DeleteAction" Then
    		
    	    Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM  DataSourceTableAction WHERE Id = " & e.CommandArgument)
    	    Database.Tenant.Execute("DELETE FROM DataSourceTableAction WHERE Id = " & e.CommandArgument)
    	    
    	    Dim TableId As String = dr("TableId").ToString
    	    Dim FldName As String = dr("FieldName").ToString    	    
    	    
    	    Dim dr2 As DataRow = Database.Tenant.GetTopRow("SELECT * FROM  DataSourceTable WHERE Id = " & TableId)
       	    Dim TblName As String = dr2("Name").ToString    	
       	    
        	Dim Description As String = "Deleted custom field "+FldName+" for table "+TblName+" data action ."
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
            RefreshGrid()
            ClearForm()
        End If
    End Sub
 
    Protected Property SortExpression As String
    Get
        Dim value as Object = ViewState("SortExpression")
        Return If(Not IsNothing(value), CStr(value), "Action")
    End Get
    Set(value As String)
        ViewState("SortExpression") = value
    End Set
   End Property
    Protected Property IsAscendingSort As Boolean
        Get
            Dim value As Object = ViewState("IsAscendingSort")
            Return If(Not IsNothing(value), CBool(value), False)
        End Get
        Set(value As Boolean)
            ViewState("IsAscendingSort") = value
        End Set
    End Property

    Private Sub gvCustomActions_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvCustomActions.PageIndexChanging
        gvCustomActions.PageIndex = e.NewPageIndex
        RefreshGrid()

    End Sub


    Protected Sub GridView1_Sorting(sender As Object, e As GridViewSortEventArgs)
        If e.SortExpression = SortExpression Then
            IsAscendingSort = Not IsAscendingSort
    Else
          SortExpression = e.SortExpression
    End If
    RefreshGrid()
End Sub
End Class