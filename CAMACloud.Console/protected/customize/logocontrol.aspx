﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/DataSetup.master" AutoEventWireup="false" Inherits="CAMACloud.Console.customize_logocontrol" Codebehind="logocontrol.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        $(function () {
            $('.upload').change(function () {
                $('.btn', $(this).parent()).click();
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:HiddenField runat="server" ID="tickValue" />
    <h1>Client Logo Control</h1>
    <p class="info">
        Upload custom logos for your CAMA Cloud<sup>&#174;</sup> Desktop &amp; MobileAssessor<sup>&#174;</sup>. The artworks, preferably in PNG format with transparent background, should be matching the exact specifications given against each item. 
    </p>
    <table class="logo-control-table">
        <tr>
            <td class="content">
                <h3>MobileAssessor Logo #1 <span>(Top Left)</span></h3>
                <div class="mobile-assessor-app-logo-container"><div class="mobile-assessor-app-logo mobile-assessor-app-top-left-logo" runat="server" id="logoMA1"></div>
                </div>
                <asp:FileUpload runat="server" ID="FileMA1" Width="350px" CssClass="upload" />
                <div class="hidden"><asp:Button runat="server" ID="btnMA1" CssClass="btn" CommandName="MA1" OnClick="btnUpload_Click"/></div>
                <div style="margin-top:10px;font-weight:bold;">
                    <asp:LinkButton runat="server" Text="Download" CommandName="Download" CommandArgument="MA1" OnClick="btnDownload_Click"  />
                    &nbsp;&nbsp;
                    <asp:LinkButton runat="server" Text="Reset" CommandName="Reset" CommandArgument="MA1" OnClick="btnReset_Click" OnClientClick="return confirm('Are you sure you want to reset the logo to the default MobileAssessor logo?')"  />
                </div>
            </td>
            <td class="info">
            <b>Specifications:</b><br />
                Format: PNG<br />
                Height: 85px<br />
                Minimum Width: 120px<br />
                Maximum Width: 1600px<br />
                Background: Transparent
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="content">
                <h3>MobileAssessor Logo #2 <span>(Top Right)</span></h3>
                <div class="mobile-assessor-app-logo-container"><div class="mobile-assessor-app-logo mobile-assessor-app-top-right-logo" runat="server" id="logoMA2"></div>
                </div>
                <asp:FileUpload runat="server" ID="FileMA2" Width="350px" CssClass="upload" />
                <div class="hidden"><asp:Button runat="server" ID="btnMA2" CssClass="btn" CommandName="MA2" OnClick="btnUpload_Click"/></div>
                <div style="margin-top:10px;font-weight:bold;">
                    <asp:LinkButton runat="server" Text="Download" CommandName="Download" CommandArgument="MA2" OnClick="btnDownload_Click"  />
                    &nbsp;&nbsp;
                    <asp:LinkButton runat="server" Text="Reset" CommandName="Reset" CommandArgument="MA2" OnClick="btnReset_Click" OnClientClick="return confirm('Are you sure you want to reset the logo to the default MobileAssessor logo?')"  />
                </div>
            </td>
            <td class="info">
            <b>Specifications:</b><br />
                Format: PNG<br />
                Height: 85px<br />
                Minimum Width: 120px<br />
                Maximum Width: 800px<br />
                Background: Transparent
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
         <tr>
            <td class="content">
                <h3>MobileAssessor Splash <span></span></h3>
                <div class="mobile-assessor-app-splash-logo mobile-assessor-app-splash-logo" runat="server" id="logoMA3">
                </div>
                <asp:FileUpload runat="server" ID="FileMA3" Width="350px" CssClass="upload" />
                <div class="hidden"><asp:Button runat="server" ID="btnMA3" CssClass="btn" CommandName="MA3" OnClick="btnUpload_Click"/></div>
                <div style="margin-top:10px;font-weight:bold;">
                    <asp:LinkButton runat="server" Text="Download" CommandName="Download" CommandArgument="MA3" OnClick="btnDownload_Click"  />
                    &nbsp;&nbsp;
                    <asp:LinkButton runat="server" Text="Reset" CommandName="Reset" CommandArgument="MA3" OnClick="btnReset_Click" OnClientClick="return confirm('Are you sure you want to reset the logo to the default MobileAssessor logo?')"  />
                </div>
            </td>
            <td class="info">
            <b>Specifications:</b><br />
                Format: PNG<br />
                Minimum Height: 350px<br />
                Maximum Height: 1000px<br />
                Minimum Width: 500px<br />
                Maximum Width: 1000px<br />
                Background: Transparent
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
         <tr>
            <td class="content">
                <h3>Console Main Logo <span></span></h3>
                <div class="console-main console-main-logo" runat="server" id="logoMA4">
                </div>
                <asp:FileUpload runat="server" ID="FileMA4" Width="350px" CssClass="upload" />
                <div class="hidden"><asp:Button runat="server" ID="btnMA4" CssClass="btn" CommandName="MA4" OnClick="btnUpload_Click"/></div>
                <div style="margin-top:10px;font-weight:bold;">
                    <asp:LinkButton runat="server" Text="Download" CommandName="Download" CommandArgument="MA4" OnClick="btnDownload_Click"  />
                    &nbsp;&nbsp;
                    <asp:LinkButton runat="server" Text="Reset" CommandName="Reset" CommandArgument="MA4" OnClick="btnReset_Click" OnClientClick="return confirm('Are you sure you want to reset the logo to the default MobileAssessor logo?')"  />
                </div>
            </td>
            <td class="info">
            <b>Specifications:</b><br />
                Format: PNG Preferred<br />
                Height:100px <br />
                Width:318px <br />
                Background: Transparent
            </td>
        </tr>
    </table>
    <p>
        <asp:Button runat="server" ID="btnUpdateVersion" Text="Update Client Customization" OnClientClick="showMask();"/>
    </p>
</asp:Content>

