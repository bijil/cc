﻿<%@ Page Title="Table Change Actions" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="tableactions.aspx.vb" Inherits="CAMACloud.Console.tableactions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Custom Field Actions for Table Data Actions</h1>
    <table>
        <tr>
            <td style="padding-right: 80px;">Action: </td> 
            <td>
                <asp:DropDownList runat="server" ID="ddlAction" Width="240px" AutoPostBack="true">
                    <asp:ListItem Text="Delete" Value="D" />
                    <asp:ListItem Text="Edit" Value="E"/>
                    <asp:ListItem Text="Edit or Descendent Records Edit" Value="R"/>
                </asp:DropDownList>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="ddlAction" ErrorMessage="*" ValidationGroup="CustomAction" /><br />
            </td>
        </tr>
        <tr>
            <td style="padding-right: 80px;">Schema Table: </td>
            <td>
                <asp:DropDownList ID="ddlTable" runat="server" AutoPostBack="true" Width="240px" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlTable" ErrorMessage="*" ValidationGroup="CustomAction" />
            </td>
        </tr>
        <tr>
            <td style="padding-right: 80px;">Schema Field: </td>
            <td>
                <asp:DropDownList ID="ddlField" runat="server" Width="240px" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlField" ErrorMessage="*" ValidationGroup="CustomAction" />
            </td>
        </tr>
        <tr>
            <td style="padding-right: 80px;">Value: </td>
         <td>
                <asp:DropDownList ID="ddlValue" runat="server" Width="240px" AutoPostBack ="true"  >
                    <asp:ListItem Text="Current User" Value="LoginId" />
                    <asp:ListItem Text="Event Date/Time" Value="EventDate" />
                     <asp:ListItem Text="Current Year" Value="CurrentYear" />
                    <asp:ListItem Text="Current Month" Value="CurrentMonth" />
                    <asp:ListItem Text="Current Day" Value="CurrentDay" />
                      <asp:ListItem Text="Custom Value" Value="CustomText" />
                </asp:DropDownList>
            </td>
        </tr>
        <tr runat="server"  id="CustText">
             <td style="padding-right: 80px;">Custom Value: </td>
            <td><asp:TextBox ID="txtValue" runat="server" Width="240px" MaxLength="50"> </asp:TextBox></td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button runat="server" ID="btnAddAction" Text="Add Table Action" style="height:25px;width:150px;margin-top: 10px;"  ValidationGroup="CustomAction" />
            </td>
        </tr>
    </table>
    <asp:GridView runat="server" ID="gvCustomActions" class="mGrid" AllowPaging="True" PageSize="15" AutoGenerateColumns="false"  AllowSorting="true"   EnableViewState="true" Onsorting = "GridView1_Sorting" >
        <Columns>
            <asp:BoundField DataField="ActionName" HeaderText="Action" ItemStyle-Width="220px" SortExpression="Action"> <HeaderStyle Font-Underline="True" /></asp:BoundField>
            <asp:BoundField DataField="TableName" HeaderText="Schema Table" ItemStyle-Width="200px" ItemStyle-wrap="false" SortExpression="TableName"> <HeaderStyle Font-Underline="True" /></asp:BoundField>
            <asp:BoundField DataField="FieldName" HeaderText="Field" ItemStyle-Width="180px" ItemStyle-wrap="false" SortExpression="FieldName"> <HeaderStyle Font-Underline="True" /></asp:BoundField>
            <asp:BoundField DataField="ValueTag" HeaderText="Value" ItemStyle-Width="180px" SortExpression="ValueTag"> <HeaderStyle Font-Underline="True" /></asp:BoundField>
            <asp:TemplateField>
                <ItemStyle Width="50px" />
                <ItemTemplate>
                    <asp:LinkButton runat="server" Text="Delete" CommandName='DeleteAction' CommandArgument='<%# Eval("Id") %>' OnClientClick='return confirm("Are you sure you want to delete this action?")' />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
