﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/DataSetup.master" AutoEventWireup="false" Inherits="CAMACloud.Console.protected_customize_clienttemplates"
    CodeBehind="clienttemplates.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h1>
        Client Templates for MobileAssessor</h1>
    <p class="info">
        Upload custom view templates for MobileAssessor Data Views.
    </p>
    <p>
        You could download the existing template, make changes and upload the same. You can compose the formatted templates and upload in TXT or HTM
        format.
        <br />
        When you upload HTM, it should not contain HTML, HEAD and BODY tags. Embedded scripts are not allowed.
    </p>
    <p>
        Note: You can upload only one template at a time. Select corresponding template file, and click Upload before proceeding to next.
    </p>
    <asp:GridView runat="server" ID="gvTemplate">
        <Columns>
            <asp:BoundField DataField="Name" HeaderText="Template Name" ItemStyle-Width="280px" ItemStyle-VerticalAlign="Middle" />
            <asp:TemplateField>
                <ItemStyle Width="140px" VerticalAlign="Middle" />
                <ItemTemplate>
                    <asp:LinkButton runat="server" Text="Download" CommandArgument='<%# Eval("Code") %>' CommandName="Download" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Upload Template File">
                <ItemStyle Width="480px" />
                <ItemTemplate>
                    <asp:HiddenField runat="server" ID="TName" Value='<%# Eval("Name") %>' />
                    <asp:FileUpload runat="server" ID="File" Width="350px" CssClass="upload" />
                    <asp:Button runat="server" ID="btnUpload" Text="Upload File" CommandArgument='<%# Eval("Code") %>' CommandName="Upload" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
