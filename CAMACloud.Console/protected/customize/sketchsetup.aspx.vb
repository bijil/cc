﻿Imports System.Web.Script.Services
Imports System.Web.Services
Public Class sketchsetup
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    	LoadGrid()
    	Dim scriptManager As ScriptManager = scriptManager.GetCurrent(Me.Page)
		scriptManager.RegisterPostBackControl(Me.lkExport)
    End Sub
    
    Sub LoadGrid()
        Dim lookupName As String = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name ={0}".SqlFormatString("SketchLookupForColor"))
        If lookupName = "" Then
        	lkBulkEdit.Visible = False
        	lkBulkRemove.Visible = False
        	lkExport.Visible = False
        	RunScript("$('#pageSize_div').hide();")
            head.InnerText = "No lookup is configured for sketch coloring."
        Else
            Dim dt1 As DataTable = Database.Tenant.GetDataTable("SELECT Id, IdValue, NameValue As DescValue, ColorCode FROM ParcelDataLookup where LookupName ={0}".SqlFormatString(lookupName))
            gvSketchLookupColor.DataSource = dt1
            gvSketchLookupColor.DataBind()
            RunScript("fillBackColor();")
        End If
    End Sub
    
     Private Sub gvValidations_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSketchLookupColor.RowCommand
     	Dim rowid As Integer
     	If e.CommandName = "EditAction" Then
     	 	rowid = e.CommandArgument
    	 	'gridfield.DataSource = Database.Tenant.GetDataTable("SELECT Id, DescValue, ColorCode FROM parcelDataLookup where Id = {0}".SqlFormatString(rowid))
    	 	'gridfield.DataBind()
    	 	Dim lookupNames As String = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name ={0}".SqlFormatString("SketchLookupForColor"))
     		Dim clrCodes As String = Database.Tenant.GetStringValue("SELECT ColorCode FROM ParcelDataLookup where Id = {0} and LookupName = {1}".SqlFormatString(rowid, lookupNames))
         	ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "loadColorPopup('picker', clrCodes);", True)
     	ElseIf e.CommandName = "RemoveAction" Then
     		rowid = e.CommandArgument
     		Dim lookupName As String = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name ={0}".SqlFormatString("SketchLookupForColor"))
     		Dim clrCode As String = Database.Tenant.GetStringValue("SELECT ColorCode FROM ParcelDataLookup where Id = {0} and LookupName = {1}".SqlFormatString(rowid, lookupName))
     		If clrCode Is Nothing Or clrCode = "" Then
     			Alert("Selected field has no colorcode! Please choose a color code and retry.")
     		Else
     			Database.Tenant.Execute("UPDATE ParcelDataLookup set ColorCode = {0} where Id = {1} and LookupName = {2}".SqlFormatString("", rowid, lookupName))
    	 		Alert("Removed Successfully")
    	 		LoadGrid()
     		End If
    	End If
    End Sub
    
    Protected Sub btnsaveclr_Click(sender As Object, e As EventArgs) Handles btnsaveclr.Click
    	Dim rowid As String = ""
    	Dim lookupName As String = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name ={0}".SqlFormatString("SketchLookupForColor"))
    	For Each gvr As GridViewRow In gridfield.Rows()
    		If Not IsDBNull(gridfield.DataKeys(gvr.RowIndex)("Id")) Then
                rowid = gridfield.DataKeys(gvr.RowIndex)("Id")
    		End If
    		Dim tb As TextBox = DirectCast(gvr.FindControl("colorcode"), TextBox)
    		Dim txt As String = tb.Text
    		Database.Tenant.Execute("UPDATE ParcelDataLookup set ColorCode = {0} where Id = {1} and LookupName = {2}".SqlFormatString(txt, rowid, lookupName))
    	Next
    	LoadGrid()
    	ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "hidePopup('popupcolorpick');", True)
    End Sub
    
     Private Sub gvSketchLookupColor_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSketchLookupColor.PageIndexChanging
        gvSketchLookupColor.PageIndex = e.NewPageIndex
        LoadGrid()
     End Sub
     
     Protected Sub lkBulkEdit_Click(sender As Object, e As EventArgs) 
     	lblkpLeft.DataSource = Nothing
     	lblkpLeft.Items.Clear()
     	colorcode.Text = ""
        Dim lookupName As String = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name ={0}".SqlFormatString("SketchLookupForColor"))
        lblkpLeft.DataSource = Database.Tenant.GetDataTable("SELECT Id, concat(IdValue,' - ', NameValue) As DescValue FROM ParcelDataLookup  WHERE LookupName = {0}".SqlFormatString(lookupName))
        lblkpLeft.DataBind()
     	ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "loadPopup1('lookupload', 'Choose Lookups')", True)
     End Sub
     
     Protected Sub lkBulkRemove_Click(sender As Object, e As EventArgs) 
     	Dim lookupName As String = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name = {0}".SqlFormatString("SketchLookupForColor"))
     	Dim clrtble As DataTable = Database.Tenant.GetDataTable("SELECT ColorCode FROM ParcelDataLookup where ColorCode IS NOT NULL and ColorCode <> ''")
     	If clrtble.Rows.Count = 0 Then
     		Alert("No colorcode")
 		Else
 			Database.Tenant.Execute("UPDATE ParcelDataLookup set ColorCode = {0} where LookupName = {1}".SqlFormatString("", lookupName))
	 		Alert("Removed Successfully")
	 		LoadGrid()
 		End If
     End Sub
     
     Protected Sub lkexport_Click(sender As Object, e As EventArgs) 
     	Dim lookupName As String = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name = {0}".SqlFormatString("SketchLookupForColor"))
     	Dim exportfilename As String = lookupName
     	Response.Clear()
        Dim o As DataRow = Database.System.GetTopRow("SELECT * FROM Organization WHERE Id = " & HttpContext.Current.GetCAMASession().OrganizationId)
        Dim orgName As String = o.GetString("Name")
        Dim exportName As String = orgName.ToLower() + "-lookup-" + lookupName + "-" + Now.ToString("yyMM-dd-HHmmss") + ".xml"
        Response.ContentType = "application/xml"
        Response.WriteAsAttachment(CAMACloud.BusinessLogic.ParcelDataLookup.ExportLookup(lookupName), "" & exportName & "")
     End Sub
     
     Protected Sub lkpSave_Click(sender As Object, e As EventArgs) Handles lkpSave.Click
     	If (lblkpLeft.Items.Count < 1 ) Then
        Return
    	End If
    	Dim Selected = False
    	Dim lookupName As String = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name ={0}".SqlFormatString("SketchLookupForColor"))
    	For Each item As ListItem In lblkpLeft.Items
	        If item.Selected Then
	        	Dim color As String = colorcode.Text
	        	Database.Tenant.Execute("UPDATE ParcelDataLookup Set ColorCode = {0} where Id = {1} and LookupName = {2}".SqlFormatString(color, item.Value, lookupName))
	        	item.Selected = False
	        	Selected = True
	        End If
    	Next
    	If Selected = True Then
    		RunScript("hideMask();")
    		Alert("Saved Successfully")
    	Else
    	    RunScript("hideMask();")
    	End If
    	LoadGrid()
     End Sub
    
     Protected Sub OnPageIndexChanging(sender As Object, e As GridViewPageEventArgs)
    	gvSketchLookupColor.PageIndex = e.NewPageIndex
        Me.LoadGrid()
     End Sub
    
     Protected Sub ddlPageSize_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlPageSize.SelectedIndexChanged
    	gvSketchLookupColor.PageSize = ddlPageSize.SelectedValue
    	gvSketchLookupColor.PageIndex = 0
    	LoadGrid()
     End Sub
    
     <WebMethod()> <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
     Public Shared Function saveColorData(colorCode As String, rowuid As String) As List(Of String)
    	Dim lookupName As String = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name ={0}".SqlFormatString("SketchLookupForColor"))
    	Database.Tenant.Execute("UPDATE ParcelDataLookup set ColorCode = {0} where Id = {1} and LookupName = {2}".SqlFormatString(colorCode, rowuid, lookupName))
    	Return Nothing
     End Function
    
End Class