﻿Public Class fieldalerttypes
    Inherits System.Web.UI.Page


    Dim entityName As String = "Type"
    Dim tableName As String = "FieldAlertTypes"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadGrid()
        End If
    End Sub

    Sub LoadGrid()
        grid.DataSource = Database.Tenant.GetDataTable("SELECT * FROM " + tableName + " ORDER BY Name")
        grid.DataBind()
    End Sub

    Private Sub grid_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grid.RowCommand
        Select Case e.CommandName
            Case "EditItem"
                Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM " + tableName + " WHERE Id = " & e.CommandArgument)
                txtName.Text = dr.GetString("Name")
                ViewState("n")=dr.GetString("Name")
                hdnId.Value = dr.GetInteger("Id")
				
                btnSave.Text = "Save " + entityName
                btnCancel.Visible = True
            Case "DeleteItem"
                Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM " + tableName + " WHERE Id = " & e.CommandArgument)
                txtName.Text = dr.GetString("Name")
                Database.Tenant.Execute("DELETE FROM " + tableName + " WHERE Id = " & e.CommandArgument)
                Dim Description As String = "Field Alert Type (" + txtName.Text + ") has been Deleted."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
                CancelForm()
                LoadGrid()
        End Select
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
    	Dim Alertmsg As String
    	If hdnId.Value = "" Then
    		Dim dr = Database.Tenant.GetTopRow("SELECT * FROM FieldAlertTypes WHERE Name = " + txtName.Text.Trim.ToSqlValue)
    		If  dr IsNot Nothing Then
    			Alert("The alert type name already exists")
    		    Return 
    		End If
    		
            Dim sql As String = "INSERT INTO " + tableName + " (Name) VALUES ( {0})"
            Database.Tenant.Execute(sql.SqlFormat(False, txtName.Text.Trim))
            Dim type As String = txtName.Text.Trim
            type = type.Replace("'", "''")
            Dim Description As String = "New Field Alert Type (" + type + ") Added."
        	SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
        	Alertmsg = "Field Alert Type Added Successfully."
    	Else
    		Dim dr = Database.Tenant.GetTopRow("SELECT * FROM FieldAlertTypes WHERE Name = " + txtName.Text.Trim.ToSqlValue)
    		If  dr IsNot Nothing Then
    			Alert("The Field alert type name already exists")
    		    Return 
    		End If
            Dim sql As String = "UPDATE " + tableName + " SET Name = {1} WHERE Id = {0}"
            Database.Tenant.Execute(sql.SqlFormat(False, hdnId, txtName.Text.Trim))
            Dim Description As String = "Field Alert Type Changed from ("+ ViewState("n") +") to (" + txtName.Text.Trim + ")."
        	SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
    	End If
    	
    	Dim RedirScript As String = "window.onload = function(){ "
        If Alertmsg <> "" Then
        	RedirScript += "alert('" + Alertmsg + "');"
    	End If
	    RedirScript += "window.location = 'fieldalerttypes.aspx';}"
	    
	    RunScript(RedirScript)
    	
'        CancelForm()
'        LoadGrid()
    End Sub

    Private Sub CancelForm() Handles btnCancel.Click
        hdnId.Value = ""
        txtName.Text = ""
        btnSave.Text = "Add " + entityName
        btnCancel.Visible = False
    End Sub

    Protected Sub OnPageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        grid.PageIndex = e.NewPageIndex
        Me.LoadGrid()
    End Sub

End Class