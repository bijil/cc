﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master"
    CodeBehind="timezone.aspx.vb" Inherits="CAMACloud.Console.timezone" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
        Timezones
    </h1>
    <style type="text/css">
        select
        {
            width: 200px;
        }
        input[type="text"]
        {
            width: 196px;
        }
        input[type="submit"]
        {
            margin-top: 35px;
        }
        td span[sep]
        {
            margin-left: 15px;
            float: right;
        }
    </style>
    <script>
        $('document').ready(function () {

            $('.chkChange').on('change', function () {
                $('.btSave').removeAttr("disabled");
            });
        });
    </script>
    <table>
        <tbody>
            <tr>
                <td>
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    Time Zone<span sep>:</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlTimeZone" runat="server" TabIndex="1" class="chkChange"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvddlTimeZone" runat="server" ControlToValidate="ddlTimeZone"
                                        ValidationGroup="save" InitialValue="0" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Daylight Saving Time<span sep>:</span>
                                </td>
                                <td>
                                    <asp:CheckBox ID="cbDST" runat="server" TabIndex="2" class="chkChange"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Button ID="btnSave" runat="server" TabIndex="3" Text="Save Changes" ValidationGroup="save"   disabled  class="btSave"/>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td>
                    <asp:UpdatePanel runat="server" ID="dd" >
                        <ContentTemplate>
                            <asp:Label ID="lblCurrentTime" runat="server" Style="font-weight: bold; font-size: 60px;
                                margin-left: 178px; color: #34AEFA; width:100%"></asp:Label>
                            <asp:Timer ID="Timer1" runat="server">
                            </asp:Timer>
                            <asp:Button runat="server" ID="refreshBtn" Visible="False" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>
