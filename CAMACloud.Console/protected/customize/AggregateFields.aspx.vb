﻿
Public Class AggregateFields
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not Database.Tenant.DoesTableExists("AggregateFieldSettings") Then
                Database.Tenant.Execute(<sql>CREATE TABLE AggregateFieldSettings(ROWUID int IDENTITY PRIMARY KEY,TableName varchar(50),FieldName varchar(50),FunctionName varchar(20) )
                                        </sql>)

            End If
            If txtUnique.Visible = False Then
            	RequiredFieldValidator4.Enabled = False
            End If
            LoadTables()
            LoadFields()
            LoadGrid()
        End If
    End Sub
    Sub LoadTables()
        ddlTable.FillFromSql("SELECT Name from DataSourceTable where ImportType = 1 ORDER BY Name", True)
    End Sub

    Sub LoadFields()
        If ddlTable.SelectedValue <> "" Then
            ddlField.FillFromSql("SELECT Name from DataSourceField where SourceTable = '" & ddlTable.SelectedValue & "' ORDER BY Name")
        Else
        	ddlField.Items.Clear()
        	txtCndtn.Text = ""
        End If
        ddlField.Items.Insert(0, "-- Select --")
        txtCndtn.Text = ""
        txtUnique.Text = ""
        labelUnique.Visible = False
    	txtUnique.Visible = False
    End Sub
    Private Sub btnAddAction_Click(sender As Object, e As System.EventArgs) Handles btnAddAction.Click
    	Try
    		Dim sourceTable As String = ddlTable.SelectedValue
    		Dim editCondition As String = IIf((editId.Value = ""), " AND (1 = 1)", " AND ROWUID <> " + editId.Value + "" )
    		Dim targetTable = Database.Tenant.GetStringValue("SELECT CC_TargetTable FROM DatasourceTable WHERE Name = '"+ ddlTable.SelectedValue + "' and ImportType=1 ")
    		If targetTable IsNot Nothing AndAlso targetTable <>  "XT_" + sourceTable Then
    			Dim alertText As String ="Selected field's target table is '"+ targetTable.Substring(3) +"'. '"+ targetTable.Substring(3) +"' will be shown as its table here."
    			Alert(alertText)
    			sourceTable = targetTable.Substring(3)
    		End If
    		Dim cond = txtCndtn.Text
    		Dim UniqName
    		cond = cond.Replace("'", ControlChars.Quote)
    		Dim ExistCond = Database.Tenant.GetIntegerValue("select count(*) from AggregateFieldSettings where TableName ='" + sourceTable + "'and FieldName='" + ddlField.SelectedValue + "' and FunctionName = '" + ddlFunction.SelectedValue + "' and Condition IS NOT NULL" + editCondition)
    		Dim ExistNotCond = Database.Tenant.GetIntegerValue("select count(*) from AggregateFieldSettings where TableName ='" + sourceTable + "'and FieldName='" + ddlField.SelectedValue + "' and FunctionName = '" + ddlFunction.SelectedValue + "' and (Condition = '' OR Condition IS NULL)" + editCondition)
    		Dim sqlQuery As String = "select count(*) from AggregateFieldSettings where TableName = '" + sourceTable + "' and FieldName = '" + ddlField.SelectedValue + "' and FunctionName = '" + ddlFunction.SelectedValue + "' and Condition = '" + cond + "'" + editCondition
    		If cond = "" Then
    			sqlQuery = "select count(*) from AggregateFieldSettings where TableName = '" + sourceTable + "' and FieldName = '" + ddlField.SelectedValue + "' and FunctionName = '" + ddlFunction.SelectedValue + "' and (Condition = '' OR Condition IS NULL)" + editCondition
    		End If
    		Dim preExistRow = Database.Tenant.GetIntegerValue(sqlQuery)
    		If preExistRow = 0 Then
    			If ExistCond = 0 And ExistNotCond = 0 Then
    				UniqName = "new"
    			ElseIf ExistCond >= 0 And ExistNotCond >= 0 Then
    				UniqName = "Value"
    			End If
    		Else
    			Throw New System.Exception("")
    		End If
    		If txtUnique.Visible = True And txtUnique.Text = "" Then
    			RequiredFieldValidator4.Enabled = True
    			Return
    		End If
    		
    		If UniqName = "Value" And txtUnique.Text = "" Then
    			labelUnique.Visible = True
    			txtUnique.Visible = True
    			Alert("Aggregate Field already exists. Please provide Unique Name to Add this Aggregate Field")
    			Return
    		ElseIf UniqName = "Value" And txtUnique.Text <> "" Then
    			Dim count = Database.Tenant.GetIntegerValue("select count(*) from AggregateFieldSettings where UniqueName='" + txtUnique.Text + "'" + editCondition)
    			If count > 0 Then
    				txtUnique.Text = ""
    				Alert ("Unique Name already exists")
    				Return
    			End If
    		End If
            Dim dataType As Int32 = Database.Tenant.GetIntegerValue("select DataType from DataSourceField where	Name = '" + ddlField.SelectedValue + "'and SourceTable ='" + ddlTable.SelectedValue + "'")
            If ((UniqName = "Value" Or UniqName = "new") And (dataType = 8 OrElse dataType = 2 OrElse dataType = 7 OrElse dataType = 9 OrElse dataType = 10 OrElse ddlFunction.SelectedValue = "COUNT" OrElse ddlFunction.SelectedValue = "FIRST" OrElse ddlFunction.SelectedValue = "LAST")) Then
                Dim countMessage As String = ""
                Dim condn = txtCndtn.Text
                If condn <> "" Then
                    Try
                        Dim TableName As String = Database.Tenant.GetStringValue("SELECT CC_TargetTable FROM DataSourceTable WHERE Name = {0}".SqlFormatString(sourceTable))
                        Dim WhereClause As String = "WHERE " + condn
                        Dim TestSql As String = "SELECT CC_ParcelID FROM {0} {1} Group By CC_Parcelid".FormatString(TableName, WhereClause)
                        Dim TestResult As DataTable = Database.Tenant.GetDataTable(TestSql)
                        countMessage = TestResult.Rows.Count.ToString + " records matches the filter condition given."
                    Catch ex As Exception
                        Alert("Please check the condition added. " + ex.Message)
                        Return
                    End Try
                End If
                Dim Description As String = ""
                If editId.Value = "" Then
                    Database.Tenant.Execute("INSERT INTO AggregateFieldSettings (TableName, FieldName, FunctionName, Condition, UniqueName ) VALUES ({0}, {1}, {2}, {3}, {4})".SqlFormatString(sourceTable, ddlField.SelectedValue, ddlFunction.SelectedValue, condn, txtUnique.Text))
                    Description = "New (" + sourceTable + ", " + ddlField.SelectedValue + ", " + ddlFunction.SelectedValue + ") added to Aggregate Fields."
                    Alert("Aggregate Field Added. " + countMessage)
                Else
                    Dim count = Database.Tenant.GetIntegerValue("select count(*) from AggregateFieldSettings where TableName='" + sourceTable + "' and FieldName='" + ddlField.SelectedValue + "' and FunctionName='" + ddlFunction.SelectedValue + "' and Condition='" + cond + "' and UniqueName ='" + txtUnique.Text + "'  ")

                    If count <> 0 Then
                        Alert("You have not made any modifications in Aggregate Fields.")
                        Return
                    Else
                        Database.Tenant.Execute("UPDATE AggregateFieldSettings SET TableName = {0}, FieldName = {1}, FunctionName = {2}, Condition = {3}, UniqueName = {4} WHERE ROWUID = {5}".SqlFormatString(sourceTable, ddlField.SelectedValue, ddlFunction.SelectedValue, condn, txtUnique.Text, editId.Value))
                        Description = "Aggregate field settings modified"
                        Alert("Aggregate Field Modified. " + countMessage)
                    End If

                    ClearFields()
                End If

                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
                LoadTables()
                LoadFields()
                LoadGrid()
            Else
                Alert("Selected Field is not a number type to calculate aggregate function")
            End If
        Catch ex As Exception
            Alert("Another field with same name exists in the profile. You cannot add two fields with same names from the one-to-one tables of parcel.")
        End Try
        ddlFunction.SelectedIndex = 0
        txtUnique.Text = ""
        RequiredFieldValidator4.Enabled = False
        labelUnique.Visible = False
    	txtUnique.Visible = False
    End Sub

    Sub LoadGrid()
	     Dim dt As New DataTable()
         dt = Database.Tenant.GetDataTable("SELECT ROWUID,TableName, FieldName, FunctionName, Condition, UniqueName from AggregateFieldSettings")
         dt.DefaultView.Sort = SortExpression + " " + If(IsAscendingSort, "ASC", "DESC")
         grid.DataSource = dt.DefaultView
         grid.DataBind()
	
'        grid.DataSource = Database.Tenant.GetDataTable("SELECT ROWUID,TableName, FieldName, FunctionName, Condition, UniqueName from AggregateFieldSettings")
'        grid.DataBind()
    End Sub

    Protected Sub grid_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grid.RowCommand
        If (e.CommandName = "Delete") Then
            Dim Description As String = ""
            Dim ROWUID As Integer = e.CommandArgument



            Dim aggDataTable As DataRow = Database.Tenant.GetTopRow("select TableName, FieldName, FunctionName, Condition, UniqueName from  AggregateFieldSettings where ROWUID = {0}".SqlFormatString(ROWUID))
            Dim tablename As String = ""
            Dim FieldName As String = ""
            Dim FunctionName As String = ""
            Try
                tablename = aggDataTable.GetString("TableName")
                FieldName = aggDataTable.GetString("FieldName")
                FunctionName = aggDataTable.GetString("FunctionName")

            Catch ex As Exception

                ddlTable.SelectedIndex = 0
                ddlField.SelectedIndex = 0
                ddlFunction.SelectedIndex = 0
            End Try
            Database.Tenant.Execute("Delete from AggregateFieldSettings where ROWUID = " & ROWUID)
            Description = "The Aggrgate Field  (" + tablename + "," + FieldName + ", " + FunctionName + ") deleted."
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            ClearFields()
            LoadGrid()
        ElseIf (e.CommandName = "EditItem") Then
            Dim editRowid As Integer = e.CommandArgument
        	editId.Value = editRowid
        	Dim aggDataTable As DataRow = Database.Tenant.GetTopRow("select TableName, FieldName, FunctionName, Condition, UniqueName from  AggregateFieldSettings where ROWUID = {0}".SqlFormatString(editRowid))
        	Try
        		ddlTable.SelectedValue = GetString(aggDataTable, "TableName")
        		LoadFields()
        		ddlField.SelectedValue = GetString(aggDataTable, "FieldName")
        		ddlFunction.SelectedValue = GetString(aggDataTable, "FunctionName")
            Catch ex As Exception
                ddlTable.SelectedIndex = 0
        		ddlField.SelectedIndex = 0
        		ddlFunction.SelectedIndex = 0
            End Try
            txtCndtn.Text = GetString(aggDataTable, "Condition")
            Dim uniqueNm As String = GetString(aggDataTable, "UniqueName")
            If uniqueNm <> "" Then
            	labelUnique.Visible = True
            	txtUnique.Visible = True
            	txtUnique.Text = uniqueNm
            End If
            btnAddAction.Text = "Update "
            btnAddCancel.Visible = True
        End If
    End Sub
    
    Private Sub btnAddCancel_Click(sender As Object, e As System.EventArgs) Handles btnAddCancel.Click
        ClearFields()
    End Sub
    
    Private Sub ClearFields()
        editId.Value = ""
        btnAddCancel.Visible = False
        labelUnique.Visible = False
    	txtUnique.Visible = False
    	txtUnique.Text = ""
        btnAddAction.Text = "Add Field"
        txtCndtn.Text = ""
        ddlFunction.SelectedIndex = 0
        ddlTable.SelectedIndex = 0
        LoadFields()
        ddlField.SelectedIndex = 0
    End Sub
    
    Private Sub ddlTable_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTable.SelectedIndexChanged
        LoadFields()
    End Sub
    
    Private Sub ddl_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlField.SelectedIndexChanged, ddlFunction.selectedIndexChanged
        txtCndtn.Text = ""
        txtUnique.Text = ""
        labelUnique.Visible = False
    	txtUnique.Visible = False
    End Sub
    
    Protected Sub grid_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles grid.RowDeleting

    End Sub

    Protected Sub grid_SelectedIndexChanging(sender As Object, e As GridViewSelectEventArgs) Handles grid.SelectedIndexChanging
    End Sub

    Protected Sub GridView1_Sorting(sender As Object, e As GridViewSortEventArgs)
        If e.SortExpression = SortExpression Then
            IsAscendingSort = Not IsAscendingSort
        Else
            SortExpression = e.SortExpression
        End If
        LoadGrid()
    End Sub

    Protected Sub OnPageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        grid.PageIndex = e.NewPageIndex
        Me.LoadGrid()
    End Sub

    Protected Property SortExpression As String
	    Get
	        Dim value as Object = ViewState("SortExpression")
	        Return If(Not IsNothing(value), CStr(value), "ROWUID")
	    End Get
	    Set(value As String)
	        ViewState("SortExpression") = value
	    End Set
     End Property
     
     Protected Property IsAscendingSort As Boolean
	    Get
	        Dim value as Object = ViewState("IsAscendingSort")
	        Return If(Not IsNothing(value), CBool(value), False)
	    End Get
	    Set(value As Boolean)
	        ViewState("IsAscendingSort") = value
	    End Set
     End Property
   
End Class