﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="annualprocesses.aspx.vb" Inherits="CAMACloud.Console.AnnualProcesses" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style>
.lnkButton{
font-weight: bold;
text-decoration: none; 
padding: 5px 15px;
margin: 5px;
margin-left: 0px;
margin-right: 15px;
background: #CFCFCF;
border-radius: 8px;
display: inline-block;
cursor: pointer;
}
</style>
<script type="text/javascript">
	 function confirmResetAllParcels() {
            var warning_msg = 'WARNING! This process will reset ALL parcels\' QC status, Field Reviewed Flags, Alerts, Priority, Messages and clear / Reject any Pending Changes not yet downsynced.'
            if (confirm(warning_msg)) {
                return true;
            }
            else {
                return false;
            }
        }
        
        function warnResetAllParcels(pCount) {
        	var confirm_msg = 'Are you sure? You will be required to set new Priorities for field visits, etc. Please CANCEL and export any Messages and Flags to CSV if they are needed for future visits.\n\nThis action cannot be undone.'
        	if(pCount > 0) {
				if (confirm("There are " + pCount + " properties that are QC Approved but have not synced to CAMA. Are you sure you want to proceed?")) {
					if(confirm(confirm_msg))
						$('.btn-Hidden').click();
				}
				else return false;				
			}
			else {
				if(confirm(confirm_msg)) $('.btn-Hidden').click();
				else return false;
			}
        }

		confirmDateValidation = () => {

			var datetime = new Date();       
            var selectedDate = $(".nylDateClass").val();
            if (datetime > new Date(selectedDate))
			{
				alert("You have selected an old date, Please confirm it once again.")
				//$('#MainContent_MainContent_nylDate').val("");
				//return false;
			}

		}
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <h1>
             Annual Processes
        </h1> 
		<h2>
			 Rollover Reminder
		</h2>         
   			 <table>
	        	 <tr>
		             <td style="padding-right: 20px;">
		                Enter the anticipated date of your rollover in CAMA. You will receive a reminder in the Admin Console to complete the necessary preparation steps starting one month prior to this date :
		             </td>
	             </tr>
	              <tr>
		             <td style="padding-right: 20px; padding-top:10px">
		              <asp:TextBox ID="nylDate" runat="server" class ="nylDateClass" TextMode="Date" style="margin-right: 15px;"></asp:TextBox>
		             <asp:LinkButton runat="server" ID="btnClear" class= "lnkButton" Text="Clear Date" />
		             <asp:LinkButton runat="server" ID="btnSave" class= "lnkButton" Text="Save" OnClientClick="return confirmDateValidation();"/>
		             </td>
	             </tr>
             </table>
		<h2>End of Year Process :</h2>
		<table> 
			<p>The End of Year Process can be used to <i><b><U>completely</U></b></i> clear your cloud environment of pending changes.</p>
			<tr><td>This process will:</td></tr>
			<tr><td>- Reset all parcels' QC status</td></tr>
			<tr><td>- Reset all parcels' Field Reviewed flags</td></tr>
			<tr><td>- Clear all Field Alert flags </td></tr>
			<tr><td>- Clear all Alert from Field and Alert from Office comments </td></tr>
			<tr><td>- Clear all Priorities, setting the properties back to Normal priority </td></tr>
			<tr><td>- <font color="red"><b>Reject (delete)</b> </font> any Pending Changes (data and photos) that have not downsynced to CAMA </td></tr>
		</table>
			<p>This is a method to clear all of the above information from your recent field cycle before starting a new one. You can export any field alert flags and messages in Quality Control to CSV for historical purposes, or if they are needed for future visits.</p> 

			<p><b>If your office chooses to retain field alert flags and comments,</b> or any piece of the above listed information from one appraisal cycle to the next, you can clear this information individually through Quality Control > Bulk Edit.</p>
		<asp:LinkButton runat="server" ID="lbResetAllQCParcels" Text="Reset All Parcels" class= "lnkButton" OnClientClick="return confirmResetAllParcels();" />
		<asp:Button runat="server" ID="btnHidden" Text="" Style="display: none;"  CssClass="btn-Hidden"  />
</asp:Content>

