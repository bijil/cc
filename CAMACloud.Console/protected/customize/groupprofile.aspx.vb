﻿Imports CAMACloud.BusinessLogic

Public Class groupprofile
    Inherits System.Web.UI.Page

    Public Enum Aggregate
        Mean
        Range
        RangeMin
        RangeMax
        Median
        Mode
        IQR
        IQRMin
        IQRMax
        Sum
        Avg
        Min
        Max
    End Enum

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ''Dim sqlQuery = "select Value from clientsettings where Name='EnableGroupProfileView' "
            'If Database.Tenant.Execute(sqlQuery) = " " Or Database.Tenant.GetStringValue(sqlQuery) = "0" Then

            If Database.Tenant.GetStringValue("SELECT COALESCE( (select Value from clientsettings where Name='EnableGroupProfileView'), '0')") = "0" Then
                enableprofile.Checked = False
                groupprofmain.Visible = False
            Else
                enableprofile.Checked = True
            End If
            If Not Database.Tenant.DoesTableExists("NeighborhoodProfileSettings") Then
                Database.Tenant.Execute(<sql>CREATE TABLE NeighborhoodProfileSettings
                                            (
                                             ROWID INT IDENTITY PRIMARY KEY,
                                             Field VARCHAR(50) UNIQUE,
                                                FieldId INT,
                                                FilterExpression varchar(500),
                                             UseMean BIT DEFAULT 0,
                                             UseRange BIT DEFAULT 0,
                                             UseMedian BIT DEFAULT 0,
                                             UseMode BIT DEFAULT 0,
                                             UseIQR BIT Default 0,
                                             UseSUM BIT Default 0,
                                             UseAVG BIT Default 0,
                                             UseMIN BIT Default 0,
                                             UseMAX BIT Default 0,
                                            )
                                        </sql>)
            End If
            LoadTables()
            LoadFields()
            LoadGrid()
            LoadAvailableFields()
            txtFilter.Text = Database.Tenant.GetStringValue("SELECT TemplateContent FROM ClientTemplates WHERE Name = '__neighborhoodprofile_filter'")

        End If


    End Sub
	Sub LoadTables()
		ddlTable.FillFromSql("SELECT Name from DataSourceTable where ImportType in (0, 1) ORDER BY Name", True)
		ddlTable.SelectedIndex = 0
    End Sub
    Sub LoadFields()
    	If ddlTable.SelectedValue <> "" Then
            ddlField.FillFromSql("SELECT Id, Name from DataSourceField where SourceTable = '" & ddlTable.SelectedValue & "' AND Id NOT IN (SELECT FieldId FROM NeighborhoodProfileSettings) AND DataType IN (2, 7, 8, 9, 10) ORDER BY Name")
        Else
        	ddlField.Items.Clear()
        End If
        ddlField.Items.Insert(0, "-- Select --")
        ddlField.SelectedIndex = 0
        ''ddlField.FillFromSql("SELECT Id, SourceTable + '.' + Name As Name FROM DataSourceField WHERE TableId IN ( SELECT Id FROM DataSourceTable WHERE ImportType = 0) AND Name NOT IN (SELECT Field FROM NeighborhoodProfileSettings) AND DataType IN (2, 7, 8, 9, 10) ORDER BY SourceTable, Name", True)
    End Sub
    
    Private Sub ddlTable_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTable.SelectedIndexChanged
        LoadFields()
    End Sub

    Private Sub btnAddAction_Click(sender As Object, e As System.EventArgs) Handles btnAddAction.Click
        Dim fieldName As String = ddlField.SelectedItem.Text
        Try
        	If (Database.Tenant.GetIntegerValue("SELECT ImportType FROM DataSourceTable WHERE Id = (SELECT TableId FROM DataSourceField where Id = " & ddlField.SelectedValue & ")") <> 0) Then
        		fieldName = ddlTable.SelectedItem.Text + "_" + fieldName
        	End If
            Database.Tenant.Execute("INSERT INTO NeighborhoodProfileSettings (FieldId, Field) VALUES ({0}, {1})".SqlFormatString(ddlField.SelectedValue, fieldName))
            LoadTables()
            LoadFields()
            LoadGrid()
        Catch ex As Exception
            Alert("Another field with same name exists in the profile. You cannot add two fields with same names from the one-to-one tables of parcel.")
        End Try

    End Sub

    Sub LoadGrid()
        grid.DataSource = Database.Tenant.GetDataTable("SELECT ns.*, f.DataType FROM NeighborhoodProfileSettings ns INNER JOIN DataSourceField f ON ns.FieldId = f.Id ORDER BY Field")
        grid.DataBind()
    End Sub

    Sub LoadAvailableFields()
        Dim sql As String = "SELECT Name FROM DataSourceField WHERE TableId IN (SELECT Id FROM DataSourceTable WHERE Name = '_neighborhood_profile') AND Name <> " + Database.Tenant.Application.NeighborhoodField.ToSqlValue(True)
        lblFields.Text = ""
        For Each dr As DataRow In d_(sql).Rows
            If lblFields.Text <> "" Then
                lblFields.Text += " "
            End If
            lblFields.Text += "<span>" + dr.GetString("Name") + "</span>"
        Next

    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click

        'If txtFilter.Text.Length > 500 Then
           ' Alert("Sorry, you cannot write filter condition exceeding 500 characters.")
        'End If
        'If enableprofile.Checked Then
        '    Database.Tenant.Execute("update ClientSettings set value='0' where Name='EnableGroupProfileView'")
        'Else
        '    Database.Tenant.Execute("update ClientSettings set value='1' where Name='EnableGroupProfileView'")
        'End If
        If Database.Tenant.GetTopRow("EXEC sp_configure 'clr enabled';")("run_value") = 0 Then
            Database.Tenant.Execute("EXEC sp_configure 'clr enabled' , '1'; RECONFIGURE;")
        End If

        Dim ca As New ClientApplication(Database.Tenant)
        Dim neighborhoodTable As String = ca.NeighborhoodTable
        Dim neighborhoodNumberField As String = ca.NeighborhoodField

        Database.Tenant.Execute("INSERT INTO NeighborhoodData (NbhdId, [" + neighborhoodNumberField + "]) SELECT Id, Number FROM Neighborhood WHERE Number NOT IN (SELECT [" + neighborhoodNumberField + "] FROM NeighborhoodData)")

        Database.Tenant.Execute("IF NOT EXISTS (SELECT * FROM ClientTemplates WHERE Name = '__neighborhoodprofile_filter') INSERT INTO ClientTemplates (Name) VALUES ('__neighborhoodprofile_filter');")
        Database.Tenant.Execute("UPDATE ClientTemplates SET TemplateContent = {0} WHERE Name = '__neighborhoodprofile_filter'", txtFilter.Text.ToSqlValue)

        Dim nptid As Integer = createProfileTableIfNotExists(neighborhoodTable, neighborhoodNumberField)

        Dim groupAssignmentStatement As String = ""
        Dim groupAggregateStatement As String = ""
        Dim groupAuxilaryStatement As String = ""
        
        Dim AuxilaryTableList As New List(Of String)

        Dim procedureBody As String = ""

        For Each gr As GridViewRow In grid.Rows

            Dim assignmentStatement As String = ""
            Dim aggregateStatement As String = ""

			Dim allUsed As Boolean = False
			Dim tableName As String = ""

            Dim rowId = gr.GetHiddenValue("ROWID")
            Dim fieldName = gr.GetHiddenValue("Field")
            Dim fieldId = CInt(gr.GetHiddenValue("FieldId"))
            Dim inputType = CInt(gr.GetHiddenValue("InputType"))
            Dim useMean = gr.GetChecked("Mean")
            Dim useRange = gr.GetChecked("Range")
            Dim useMedian = gr.GetChecked("Median")
            Dim useMode = gr.GetChecked("Mode")
            Dim useIQR = gr.GetChecked("IQR")
            Dim useSUM = gr.GetChecked("SUM")
            Dim useAVG = gr.GetChecked("AVG")
            Dim useMIN = gr.GetChecked("MIN")
            Dim useMAX = gr.GetChecked("MAX")
            Dim auxilaryFieldName As String = ""
            Dim filterExpression As String = gr.GetTextValue("FilterExpression")
            
            Dim tableRow As DataRow = Database.Tenant.GetTopRow("SELECT ImportType, Name, CC_TargetTable FROM DataSourceTable WHERE Id = (SELECT TableId FROM DataSourceField where Id = " & fieldId & ")")
            
            If (GetInteger(tableRow, "ImportType") <> 0 AndAlso GetInteger(tableRow, "ImportType") <> Nothing) Then
            	tableName = GetString(tableRow, "CC_TargetTable")
            	auxilaryFieldName = Database.Tenant.GetStringValue("SELECT Name FROM DataSourceField where Id = " & fieldId)
            	If AuxilaryTableList.IndexOf(tableName) = -1 Then
            		AuxilaryTableList.Add(tableName)
            	End If
        	End If

            e_("UPDATE NeighborhoodProfileSettings SET UseMean = {1}, UseRange = {2}, UseMedian = {3}, UseMode = {4}, UseIQR = {5}, FilterExpression = {6}, UseMIN = {7}, UseMAX = {8}, UseSUM = {9} ,UseAVG = {10}  WHERE ROWID = {0}", False, rowId, useMean.GetHashCode, useRange.GetHashCode, useMedian.GetHashCode, useMode.GetHashCode, useIQR.GetHashCode, filterExpression, useMIN.GetHashCode, useMAX.GetHashCode, useSUM.GetHashCode, useAVG.GetHashCode)

            Dim createFieldFunction = Function(ag As Aggregate)
                                          createFieldIfNotExists(nptid, fieldId, fieldName, ag, inputType, aggregateStatement, assignmentStatement, auxilaryFieldName)
                                          Return 0
                                      End Function

            Dim deleteFieldRoutine = Sub(ag As Aggregate)
                                         deleteFieldIfExists(nptid, fieldName, ag)
                                     End Sub

            If useMean Then
                createFieldFunction(Aggregate.Mean)
                allUsed = allUsed Or True
            Else
                deleteFieldRoutine(Aggregate.Mean)
            End If
            If useRange Then
                createFieldFunction(Aggregate.Range)
                createFieldFunction(Aggregate.RangeMin)
                createFieldFunction(Aggregate.RangeMax)
                allUsed = allUsed Or True
            Else
                deleteFieldRoutine(Aggregate.Range)
                deleteFieldRoutine(Aggregate.RangeMin)
                deleteFieldRoutine(Aggregate.RangeMax)
            End If
            If useMedian Then
                createFieldFunction(Aggregate.Median)
                allUsed = allUsed Or True
            Else
                deleteFieldRoutine(Aggregate.Median)
            End If
            If useMode Then
                createFieldFunction(Aggregate.Mode)
                allUsed = allUsed Or True
            Else
                deleteFieldRoutine(Aggregate.Mode)
            End If
            If useIQR Then
                createFieldFunction(Aggregate.IQR)
                createFieldFunction(Aggregate.IQRMin)
                createFieldFunction(Aggregate.IQRMax)
                allUsed = allUsed Or True
            Else
                deleteFieldRoutine(Aggregate.IQR)
                deleteFieldRoutine(Aggregate.IQRMin)
                deleteFieldRoutine(Aggregate.IQRMax)
            End If
            If useSUM Then
                createFieldFunction(Aggregate.Sum)
                allUsed = allUsed Or True
            Else
                deleteFieldRoutine(Aggregate.Sum)
            End If
            If useAVG Then
                createFieldFunction(Aggregate.Avg)
                allUsed = allUsed Or True
            Else
                deleteFieldRoutine(Aggregate.Avg)
            End If
            If useMIN Then
                createFieldFunction(Aggregate.Min)
                allUsed = allUsed Or True
            Else
                deleteFieldRoutine(Aggregate.Min)
            End If
            If useMAX Then
                createFieldFunction(Aggregate.Max)
                allUsed = allUsed Or True
            Else
                deleteFieldRoutine(Aggregate.Max)
            End If

            If Not allUsed Then
                e_("DELETE FROM NeighborhoodProfileSettings  WHERE ROWID = {0}", False, rowId)
            Else
            	If tableName <> "" Then
            		Dim filterExp As String = " AND (1 = 1)"
            		If filterExpression.Trim.IsNotEmpty Then
            			filterExp = " AND (" + filterExpression + ")"
            		End If
            		'Dim updateTempAuxilaryStatement As String = "SELECT p.NeighborhoodId, " + aggregateStatement + " INTO #temp_aggregate_" + tableName + " FROM #temp_aux_aggregate_" + tableName + " ax INNER JOIN Parcel p ON ax.CC_ParcelId = p.Id" + vbNewLine + vbTab + filterExp + vbNewLine + vbTab + "GROUP BY p.NeighborhoodId;"
            		'Dim updateAuxilaryStatement As String = "UPDATE NeighborhoodData SET " + vbNewLine + vbTab + assignmentStatement + vbNewLine + " FROM #temp_aggregate_" + tableName + " prof WHERE NeighborhoodData.NbhdId = prof.NeighborhoodId;"
            		Dim updateAuxilaryStatement As String = "UPDATE NeighborhoodData SET " + vbNewLine + vbTab + assignmentStatement + vbNewLine + " FROM (SELECT p.NeighborhoodId, " + aggregateStatement + " FROM " + tableName + " ax INNER JOIN Parcel p ON ax.CC_ParcelId = p.Id WHERE ax.CC_Deleted <> 1 AND ax.CC_RecordStatus <> 'I' " + vbNewLine + vbTab + filterExp + vbNewLine + vbTab + "GROUP BY p.NeighborhoodId" + vbNewLine + ") prof WHERE NeighborhoodData.NbhdId = prof.NeighborhoodId;"
            		groupAuxilaryStatement.AppendLine("-- " + fieldName)
            		'groupAuxilaryStatement.AppendLine(updateTempAuxilaryStatement)
            		groupAuxilaryStatement.AppendLine(updateAuxilaryStatement)
            		groupAuxilaryStatement.Append(vbNewLine)
            		
            	Else
            		If filterExpression.Trim.IsNotEmpty Then
	                    Dim filter As String = filterExpression
	                    If txtFilter.Text.Trim <> "" Then
	                        If filter.Trim.IsNotEmpty Then
	                            filter += " AND "
	                        End If
	                        filter += txtFilter.Text.ToString
	                    End If
	                    If filter.Trim.IsNotEmpty Then
	                        filter = "WHERE " + filter
	                    End If
	
	                    'Dim updateStatement As String = "UPDATE NeighborhoodData SET " + vbNewLine + vbTab + assignmentStatement + vbNewLine + "FROM (" + vbNewLine + vbTab + "SELECT " + neighborhoodNumberField + ", " + aggregateStatement + " FROM ParcelData" + vbNewLine + vbTab + filter + vbNewLine + vbTab + "GROUP BY " + neighborhoodNumberField + vbNewLine + ") prof WHERE NeighborhoodData." + neighborhoodNumberField + " = prof." + neighborhoodNumberField + ";"
	                    Dim updateStatement As String = "UPDATE NeighborhoodData SET " + vbNewLine + vbTab + assignmentStatement + vbNewLine + "FROM (" + vbNewLine + vbTab + "SELECT p.NeighborhoodId, " + aggregateStatement + " FROM ParcelData pd INNER JOIN Parcel p ON pd.CC_ParcelId = p.Id" + vbNewLine + vbTab + filter + vbNewLine + vbTab + "GROUP BY p.NeighborhoodId" + vbNewLine + ") prof WHERE NeighborhoodData.NbhdId = prof.NeighborhoodId;"
	
	                    procedureBody.AppendLine("-- " + fieldName)
	                    procedureBody.AppendLine(updateStatement)
	                    procedureBody.AppendLine("")
	                Else
	                    groupAggregateStatement = groupAggregateStatement.Append(vbTab + vbTab + aggregateStatement, ", " + vbNewLine)
	                    groupAssignmentStatement = groupAssignmentStatement.Append(vbTab + assignmentStatement, ", " + vbNewLine)
	                End If
            	End If

            End If
        Next

        If groupAggregateStatement.IsNotEmpty AndAlso groupAssignmentStatement.IsNotEmpty Then
        	'Dim groupUpdateStatement As String = "UPDATE NeighborhoodData SET " + vbNewLine + groupAssignmentStatement + vbNewLine + "FROM (" + vbNewLine + vbTab + "SELECT " + vbNewLine + vbTab + vbTab + neighborhoodNumberField + ", " + vbNewLine + groupAggregateStatement + " FROM ParcelData " + vbNewLine + vbTab + IIf(txtFilter.Text.Trim <> "", "WHERE " + txtFilter.Text, "") + vbNewLine + vbTab + "GROUP BY " + neighborhoodNumberField + vbNewLine + ") prof WHERE NeighborhoodData." + neighborhoodNumberField + " = prof." + neighborhoodNumberField + ";"
        	 Dim groupUpdateStatement As String = "UPDATE NeighborhoodData SET " + vbNewLine + groupAssignmentStatement + vbNewLine + "FROM (" + vbNewLine + vbTab + "SELECT p.NeighborhoodId" + vbNewLine + vbTab + vbTab  + ", " + vbNewLine + groupAggregateStatement + " FROM ParcelData pd INNER JOIN Parcel p ON pd.CC_ParcelId = p.Id" + vbNewLine + vbTab + IIf(txtFilter.Text.Trim <> "", "WHERE " + txtFilter.Text, "") + vbNewLine + vbTab + "GROUP BY p.NeighborhoodId" + vbNewLine + ") prof WHERE NeighborhoodData.NbhdId = prof.NeighborhoodId;"

            procedureBody.AppendLine("-- Running aggregate on other fields with common filter")
            procedureBody.Append(groupUpdateStatement, vbNewLine)
            'Dim rowsUpdated As Integer = Database.Tenant.Execute(groupUpdateStatement)
        End If
        
        If groupAuxilaryStatement.IsNotEmpty Then
        	procedureBody.Append(vbNewLine)
'        	procedureBody.Append("-- Creating tempAuxliary tables", vbNewLine)
'        	For Each AuxilaryTable As String In AuxilaryTableList
'        		Dim AuxTabInsert As String = "SELECT * INTO #temp_aux_aggregate_" + AuxilaryTable + " FROM " + AuxilaryTable + " ;" 
'		        procedureBody.Append(AuxTabInsert, vbNewLine)
'        	Next
'        	Dim AuxilaryTableString As String = String.Join(",", AuxilaryTableList.ToArray())
'        	procedureBody.Append("-- Executing Sp to update PCI changes to temp table", vbNewLine)
'        	Dim sp As String = "EXEC [SP_GetEditedRecordsForExport] '" + AuxilaryTableString + "';"
'        	procedureBody.Append(sp, vbNewLine)
        	procedureBody.Append("-- Running auxilary aggregate on fields", vbNewLine)
            procedureBody.Append(groupAuxilaryStatement, vbNewLine)
        End If
        
        Dim sqlCreateProcedure = "CREATE PROCEDURE job_UpdateNeighborhoodProfile AS"

		'If Database.Tenant.GetStringValue("IF OBJECT_ID('job_UpdateNeighborhoodProfile', 'P') IS NOT NULL  RETURN 1") = "1" Then
		If Database.Tenant.GetStringValue("IF OBJECT_ID('job_UpdateNeighborhoodProfile', 'P') IS NOT NULL BEGIN SELECT 'true' END ELSE BEGIN SELECT 'false' END") = "true" Then
        	sqlCreateProcedure = "ALTER PROCEDURE job_UpdateNeighborhoodProfile AS"
        End If  

        sqlCreateProcedure.AppendLine("BEGIN")
        sqlCreateProcedure.AppendLine(procedureBody, 1)
        sqlCreateProcedure.AppendLine("END")

        Try
            Database.Tenant.Execute(sqlCreateProcedure)
            Database.Tenant.Execute("EXEC [job_UpdateNeighborhoodProfile]")
            Alert("Automated job updated for computing profile.")
        Catch ex As Exception
            ErrorMailer.ReportException("Job-Configuration", ex, HttpContext.Current.Request)
            Alert("An error occurred while creating the job. The automated procedure will not execute on this county database.")
            'Database.Tenant.Execute("IF OBJECT_ID('job_UpdateNeighborhoodProfile', 'P') IS NOT NULL DROP PROCEDURE job_UpdateNeighborhoodProfile")
        End Try
        Dim Description As String = "New Assignment Group Profile Setup added."
        SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
        LoadTables()
        LoadFields()
        LoadGrid()
        LoadAvailableFields()
    End Sub

    Private Function createProfileTableIfNotExists(neighborhoodTable As String, neighborhoodNumberField As String) As Integer
        If neighborhoodTable Is Nothing OrElse neighborhoodTable = "" Then
            Throw New Exception("Neighborhood Table setting is not defined.")
        End If
        If neighborhoodNumberField Is Nothing OrElse neighborhoodNumberField = "" Then
            Throw New Exception("Neighborhood Number Field setting is not defined.")
        End If

        If Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM DataSourceTable WHERE Name = {0}".SqlFormatString(neighborhoodTable)) = 0 Then
            Database.Tenant.Execute("INSERT INTO DataSourceTable (Name, ImportType) VALUES ({0}, 3)".SqlFormatString(neighborhoodTable))
        End If
        Dim ntid As Integer = Database.Tenant.GetIntegerValue("SELECT Id FROM DataSourceTable WHERE Name = {0}".SqlFormatString(neighborhoodTable))
        If Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM DataSourceField WHERE TableId = " & ntid) = 0 Then
            Database.Tenant.Execute("INSERT INTO DataSourceField (Name, AssignedName, DisplayLabel, DataType, MaxLength, SourceTable, SourceOrdinal, TableId) VALUES ({0}, {0}, {0}, 1, 50, {1}, 0, {2})".SqlFormatString(neighborhoodNumberField, neighborhoodTable, ntid))
        End If


        If Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM DataSourceTable WHERE Name = '_neighborhood_profile'") = 0 Then
            Database.Tenant.Execute("INSERT INTO DataSourceTable (Name, ImportType) VALUES ('_neighborhood_profile', 3)")
        End If
        Dim dtid As Integer = Database.Tenant.GetIntegerValue("SELECT Id FROM DataSourceTable WHERE Name = '_neighborhood_profile'")

        If Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM DataSourceField WHERE TableId = " & dtid) = 0 Then
            Database.Tenant.Execute("INSERT INTO DataSourceField (Name, AssignedName, DisplayLabel, DataType, MaxLength, SourceTable, SourceOrdinal, TableId) VALUES ({0}, {0}, {0}, 1, 50, {1}, 0, {2})".SqlFormatString(neighborhoodNumberField, neighborhoodTable, dtid))
        End If

        If Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM DataSourceRelationships WHERE TableId = " & dtid) = 0 Then
            Dim relId As Integer = Database.Tenant.GetIntegerValue("INSERT INTO DataSourceRelationships (Name, DataGroup, TableId, ParentTableId, Relationship) VALUES ('REL_neighborhood_profile_" + neighborhoodTable + "', 'N', {0}, {1}, 0); SELECT CAST(@@IDENTITY AS INT);".SqlFormatString(dtid, ntid))
            Dim thisFieldId As Integer = Database.Tenant.GetIntegerValue("SELECT Id FROM DataSourceField WHERE TableId = {0} AND Name = {1}".SqlFormatString(dtid, neighborhoodNumberField))
            Dim refFieldId As Integer = Database.Tenant.GetIntegerValue("SELECT Id FROM DataSourceField WHERE TableId = {0} AND Name = {1}".SqlFormatString(ntid, neighborhoodNumberField))
            Database.Tenant.Execute("INSERT INTO DataSourceKeys (RelationshipId, TableId, FieldId, IsprimaryKey, IsReferenceKey, ReferenceTableId, ReferenceFieldId) VALUES ({0}, {1}, {2}, 0, 1, {3}, {4})".SqlFormatString(relId, dtid, thisFieldId, ntid, refFieldId))
        End If

        Return dtid
    End Function

    Private Sub createFieldIfNotExists(profileTableId As Integer, fieldId As Integer, fieldName As String, ag As Aggregate, inputType As Integer, ByRef aggregrateStatement As String, ByRef assignmentStatement As String, Optional auxilaryFieldName As String = "")
        Dim targetFieldName As String = fieldName + "_" + ag.ToString
        Dim sqlType As String = DataSource.TranslateInputTypeToSqlType(inputType)
        If Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM DataSourceField WHERE TableId = {0} AND Name = {1}".SqlFormatString(profileTableId, targetFieldName)) = 0 Then
            Database.Tenant.Execute("INSERT INTO DataSourceField (Name, AssignedName, DisplayLabel, DataType, MaxLength, SourceTable, SourceOrdinal, TableId) VALUES ({0}, {0}, {0}, 1, 50, {1}, 0, {2})".SqlFormatString(targetFieldName, "_neighborhood_profile", profileTableId))
            Database.Tenant.Execute("ALTER TABLE NeighborhoodData ADD [" + targetFieldName + "] " + sqlType)
        End If
        
        If auxilaryFieldName <> "" Then fieldName = auxilaryFieldName
        
		If aggregrateStatement <> "" Then aggregrateStatement += ", "
		If ag.ToString = "Sum" OrElse ag.ToString = "Avg" OrElse ag.ToString = "Min" OrElse ag.ToString = "Max" Then
			aggregrateStatement += ag.ToString + "(" + fieldName + ") AS " + targetFieldName
		Else
			aggregrateStatement += "dbo." + ag.ToString + "(" + fieldName + ") AS " + targetFieldName
		End If

        If assignmentStatement <> "" Then assignmentStatement += ","
        assignmentStatement += targetFieldName.Wrap("[]") + " = prof." + targetFieldName.Wrap("[]")
    End Sub

    Private Sub deleteFieldIfExists(profileTableId As Integer, fieldName As String, ag As Aggregate)
        Dim targetFieldName As String = fieldName + "_" + ag.ToString
        If Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM DataSourceField WHERE TableId = {0} AND Name = {1}".SqlFormatString(profileTableId, targetFieldName)) <> 0 Then
            Database.Tenant.Execute("DELETE FROM DataSourceField WHERE TableId = {0} AND Name = {1}".SqlFormatString(profileTableId, targetFieldName))
            Database.Tenant.Execute("ALTER TABLE NeighborhoodData DROP COLUMN [" + targetFieldName + "]")
        End If
    End Sub
    Protected Sub enableprofile_CheckedChanged(sender As Object, e As EventArgs) Handles enableprofile.CheckedChanged
        If Not enableprofile.Checked Then
            Database.Tenant.Execute("update ClientSettings set value='0' where Name='EnableGroupProfileView'")
            Dim Description As String = "Group Profile View in MA and console is " + "<i>Disabled</i>"
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            groupprofmain.Visible = False
        Else
            Database.Tenant.Execute("update ClientSettings set value='1' where Name='EnableGroupProfileView' IF @@ROWCOUNT=0  INSERT INTO clientsettings VALUES ('EnableGroupProfileView','1')")
            Dim Description As String = "Group Profile View in MA and console is " + "<i>Enabled</i>"
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            groupprofmain.Visible = True
        End If
    End Sub

End Class