﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DesktopWeb.Master" %>

<%@ Import Namespace="System.Web.Script.Services" %>
<%@ Import Namespace="System.Web.Services" %>
<%@ Import Namespace="CAMACloud.Console" %>
<%@ Import Namespace="CAMACloud.BusinessLogic.DataAnalyzer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        a {
            cursor: pointer;
        }

        .ol-control {
            display: none;
        }

        .main-content-area, .main-content-area * {
            box-sizing: border-box;
        }

        .lookup-picker {
            float: none;
            position: fixed;
            z-index: 100;
        }

        .v-spacer {
            width: 5px;
        }

        .dataset-layout {
            display: flex;
        }

            .dataset-layout > div:first-child {
                width: 250px;
                padding: 4px;
                background-color: #6b94b7;
            }

                .dataset-layout > div:first-child > .head-links a {
                }

                    .dataset-layout > div:first-child > .head-links a:hover {
                        color: #EEE;
                    }

            .dataset-layout > .dataset-view {
                flex-grow: 1;
            }

                .dataset-layout > .dataset-view > div:first-child {
                    background-color: #ebeff2;
                    height: 60px;
                    padding: 4px;
                    border-bottom: 1px solid #d5dde4;
                }

                    .dataset-layout > .dataset-view > div:first-child + div {
                        display: flex;
                        min-height:400px;
                    }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="dataset-layout">
        <div>
            <div class="head-links white">
                <a class="b2" onclick="createDataset();">New Dataset</a>
                <asp:LinkButton runat="server" ID="lbCreateDataSet" Text="New Dataset" CssClass="b2" Visible="false" />
            </div>
            <table class="mGrid app-content" cellspacing="0" style="width: 100%; border-collapse: collapse;">
                <thead>
                    <tr class="thr">
                        <th scope="col">Dataset Name</th>
                    </tr>
                </thead>
                <tbody class="dataset-list"></tbody>
            </table>
            <asp:GridView runat="server" ID="grid" Width="100%" Visible="false">
                <Columns>
                    <asp:BoundField DataField="Name" HeaderText="Dataset Name" />
                </Columns>
            </asp:GridView>
        </div>
        <div class="v-spacer" style="display: none;"></div>
        <div class="dataset-view">
            <div class="dsv-header">
                <div>
                    
                </div>
            </div>
            <div>
                
            </div>
        </div>
    </div>
</asp:Content>
