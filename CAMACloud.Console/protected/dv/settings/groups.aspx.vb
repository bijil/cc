﻿Imports System.Web.Script.Services
Imports System.Web.Services
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Public Class groups
    Inherits System.Web.UI.Page

    Sub ShowGridView()
        grid.DataSource = Database.Tenant.GetDataTable("EXEC MRA_GetDataGroups")
        grid.DataBind()

        ListView.Visible = True
        FormView.Visible = False
    End Sub

    Sub ReturnToMainView()
        Response.Redirect(Request.Url.AbsolutePath)
    End Sub

    Private Sub CreateNewGroup()
        ListView.Visible = False
        FormView.Visible = True

        groupid.Value = -1
        fieldid.Value = ""
        txtFieldName.Text = ""
        txtLabel.Text = ""
        txtLookupTable.Text = ""
        groupexport.Checked = False

        lblFieldSpec.Text = "NEW"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim groupId As Integer = 0
        If Not IsPostBack Then
            If Request("option") = "add" Then
                CreateNewGroup()
            ElseIf Request("group") IsNot Nothing AndAlso Integer.TryParse(Request("group"), groupId) Then
                If groupId > 0 Then
                    OpenDataGroup(groupId)
                End If
            Else
                ShowGridView()
            End If

        End If
        If Request.Form.Keys.Count > 0 Then
            If Request.Files.Count > 0 Then
                UploadFile()
            End If
        End If
        If Request("action") = "downloadlookup" Then
            Dim lookupName As String = Request("lookup")
            If Not String.IsNullOrEmpty(lookupName) Then
                CAMACloud.BusinessLogic.ParcelDataLookup.DownloadLookup(Response, lookupName)
            End If
        End If

    End Sub

    Public Sub OpenDataGroup(id As Integer)
        Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT g.*, f.Name As FieldName, f.SourceTable FROM MRA_DataGroups g INNER JOIN DataSourceField f ON g.FieldId = f.Id WHERE g.ID = " & id)
        If dr Is Nothing Then
            ShowGridView()
            Exit Sub
        End If
        groupid.Value = dr.GetInteger("ID")
        fieldid.Value = dr.GetInteger("FieldId")
        txtFieldName.Text = dr.GetString("SourceTable") + "." + dr.GetString("FieldName")
        txtLabel.Text = dr.GetString("Label")
        txtLookupTable.Text = dr.GetString("LookupName")
        groupexport.Checked = dr.GetBoolean("IncludeInExport")

        lblFieldSpec.Text = txtLabel.Text

        ListView.Visible = False
        FormView.Visible = True
    End Sub

    Public Sub UploadFile()
        Dim file = Request.Files(0)
        Dim headline = Request.Form("head_line") = "1"
        Dim fileName = file.FileName

        Response.Clear()
        Response.ContentType = "application/json"

        Try
            Dim fileExtension = IO.Path.GetExtension(fileName).TrimStart(".").ToLower
            Dim lookupName As String = IO.Path.GetFileNameWithoutExtension(fileName)
            Dim count As Integer = 0, lookupsImported As Integer = 0
            Select Case fileExtension
                Case "xml"
                    count = CAMACloud.BusinessLogic.ParcelDataLookup.ImportLookupFromXml(file.InputStream, True, lookupName, lookupsImported)
                Case "csv"
                    count = CAMACloud.BusinessLogic.ParcelDataLookup.ImportLookupFromCSV(file.InputStream, lookupName, headline, True)
                    lookupsImported = 1
            End Select

            Dim containsValues As Boolean = False
            If count > 0 AndAlso lookupsImported = 1 Then
                containsValues = Database.Tenant.GetTopRow("SELECT LookupName, COUNT(*) As LookupValues, CAST(CASE WHEN COUNT(NULLIF(Value, 0)) > 0 THEN 1 ELSE 0 END AS BIT) As Valid FROM ParcelDataLookup WHERE LookupName = " + lookupName.ToSqlValue + "  GROUP BY LookupName").GetBoolean("Valid")
            End If

            Response.Write(JSONResponse.Serialize(New With {
                                                  .success = True,
                                                  .lookupName = lookupName,
                                                  .count = lookupsImported,
                                                  .records = count,
                                                  .valid = containsValues
                                                  }))
        Catch ex As Exception
            Response.Write(JSONResponse.Serialize(New With {.success = False, .errorMessage = ex.Message}))
        End Try

        Response.End()
    End Sub
    
    Public Function DeleteGroupScript()
        Return "deleteDataGroup(" & Eval("ID") & ");return false;"
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function FindField(q As String) As FieldSearchResult
        Dim l As New FieldSearchResult
        l.Query = q
        Dim dt As DataTable = Database.Tenant.GetDataTable("EXEC MRA_FindFields " + q.ToSqlValue + ", 1")
        l.Fields = dt.Rows.Cast(Of DataRow).Select(Function(x) New DataField With {.ID = x("Id"), .DataTypeName = x("DataTypeName"), .DataType = x("DataType"), .Name = x("Name"), .LookupTable = x.Get("LookupTable"), .SourceTable = x.GetString("SourceTable"), .DisplayLabel = x.GetString("DisplayLabel")}).ToList

        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=True)> Public Shared Function FindLookup(q As String) As SearchResult(Of LookupTable)
        Dim l As New SearchResult(Of LookupTable)
        l.Query = q
        Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT * FROM (SELECT LookupName, COUNT(*) As LookupValues, CAST(CASE WHEN COUNT(*) = COUNT(Value) THEN 1 ELSE 0 END AS BIT) As Valid FROM ParcelDataLookup GROUP BY LookupName) x WHERE x.LookupName LIKE " + ("%" + q + "%").ToSqlValue)
        l.Values = dt.Rows.Cast(Of DataRow).Select(Function(x) New LookupTable With {.Name = x.GetString("LookupName"), .Valid = x.GetBoolean("Valid"), .ValueCount = x.GetInteger("LookupValues")}).ToList
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function SaveGroup(Id As Integer, fieldId As Integer, label As String, lookupName As String, includeInExport As Boolean) As ActionResult(Of Integer)
        Dim res As New ActionResult(Of Integer)
        Try
        	Dim status as Integer = Database.Tenant.GetIntegerValue("SELECT COUNT(FieldID) FROM MRA_DataGroups WHERE FieldID = {0} ".SqlFormat(True, fieldId))
        	If Id <= 0 Then
                If status <> 0
                    Throw New Exception("Field Already Exists")
                End iF
                Id = Database.Tenant.GetIntegerValue("INSERT INTO MRA_DataGroups (FieldId) VALUES ({0}); SELECT CAST(@@IDENTITY AS INT) As NewID;".SqlFormat(False, fieldId))
            Else
                Dim ExtField as Integer = Database.Tenant.GetIntegerValue("SELECT FieldID FROM MRA_DataGroups WHERE ID = {0} ".SqlFormat(True, Id))
                If fieldId <> ExtField AND status <> 0
                    Throw New Exception("Field Already Exists")
                End If
            End If

            Dim sql As String = "UPDATE MRA_DataGroups SET FieldId = {1}, Label = {2}, LookupName = {3}, IncludeInExport = {4} WHERE ID = {0}"
            Database.Tenant.Execute(sql.SqlFormat(False, Id, fieldId, label, lookupName, includeInExport))

            Database.Tenant.Execute("UPDATE MRA_DataGroups SET PoolSize = LookupSize FROM (SELECT g.ID As DataGroupID, COUNT(l.Id) As LookupSize FROM MRA_DataGroups g INNER JOIN ParcelDataLookup l ON g.LookupName = l.LookupName WHERE g.ID = " & Id & " GROUP BY g.ID) x WHERE ID = DataGroupID")
            res.Success = True
            res.Data = Id
        Catch ex As Exception
            res.ErrorMessage = ex.Message
        End Try

        Return res
    End Function
    
    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function DeleteGroup(Id As Integer) As ActionResult(Of Integer)
    	Dim res As New ActionResult(Of Integer)
    	Try
    	    Dim deleteStatements = "DELETE FROM MRA_DataSetGroupValues WHERE DataSetGroupID IN (SELECT ID FROM MRA_DataSetGroups WHERE GroupID = {0}); UPDATE MRA_DataSet SET Error = 1 WHERE ID IN (SELECT DataSetID FROM MRA_DataSetGroups WHERE GroupID = {0}); DELETE FROM MRA_DataSetGroups WHERE GroupID = {0}; DELETE FROM MRA_DataGroups WHERE ID = {0}"
            Database.Tenant.Execute(deleteStatements.FormatString(Id))
	    	res.Success = True
	     Catch ex As Exception
            res.ErrorMessage = ex.Message
	     End Try
	     Return res
    End Function
    
    

    Private Sub grid_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grid.RowCommand
        Select Case e.CommandName
            Case "EditItem"
                 OpenDataGroup(e.CommandArgument)
'            Case "DeleteItem"
'                Dim deleteStatements = "DELETE FROM MRA_DataSetGroupValues WHERE DataSetGroupID IN (SELECT ID FROM MRA_DataSetGroups WHERE GroupID = {0}); UPDATE MRA_DataSet SET Error = 1 WHERE ID IN (SELECT DataSetID FROM MRA_DataSetGroups WHERE GroupID = {0}); DELETE FROM MRA_DataSetGroups WHERE GroupID = {0};  DELETE FROM MRA_DataGroups WHERE ID = {0}"
'                Database.Tenant.Execute(deleteStatements.FormatString(e.CommandArgument))
'                ShowGridView()
        End Select
    End Sub

    Private Sub lbViewList_Click(sender As Object, e As EventArgs) Handles lbViewList.Click
        ReturnToMainView()
    End Sub

    Private Sub lbCreateGroup_Click(sender As Object, e As EventArgs) Handles lbCreateGroup.Click
        CreateNewGroup()
    End Sub


End Class

Public Class FieldSearchResult
    Public Property Query As String
    Public Property Fields As List(Of DataField)
End Class

Public Class DataField
    Public Property ID As Integer
    Public Property Name As String
    Public Property DisplayLabel As String
    Public Property SourceTable As String
    Public Property LookupTable As String
    Public Property DataType As Integer
    Public Property DataTypeName As String
    Public Property IsParcelField As Boolean
    Public Property Expression As String
    Public Property FilterOperator As String
    Public Property FilterValue As String
    Public Property FilterValue2 As String
    Public Property FieldType As String
    Public Property Aggregate As String
    Public Property AggregateOverExpression As Boolean
End Class

Public Class SearchResult(Of T)
    Public Property Query As String
    Public Property Values As List(Of T)
End Class

Public Class LookupTable
    Public Property Name As String
    Public Property Valid As Boolean
    Public Property ValueCount As Integer
End Class

Public Class ActionResult(Of T)
    Public Property Success As Boolean = False
    Public Property DebugMessage As String
    Public Property ErrorMessage As String
    Public Property Data As T
    Public Property Query As String
    Public Property Count As Integer
    Public Property FullCount As Integer
End Class

Public Class ActionResultList(Of T)
    Public Property Success As Boolean = False
    Public Property DebugMessage As String
    Public Property ErrorMessage As String
    Public Property Data As List(Of T)
    Public Property Query As String
    Public Property Count As Integer
    Public Property FullCount As Integer
End Class

Public Class ActionResultData
    Inherits ActionResult(Of JSONTable)

End Class