﻿<%@ Page Title="Regression Model - Result" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DesktopWeb.Master" CodeBehind="mra-result.aspx.vb" Inherits="CAMACloud.Console.mra_result" %>

<%@ Import Namespace="System.Web.Script.Services" %>
<%@ Import Namespace="System.Web.Services" %>
<%@ Import Namespace="CAMACloud" %>
<%@ Import Namespace="CAMACloud.Console" %>
<%@ Import Namespace="CAMACloud.Data" %>
<%@ Import Namespace="CAMACloud.BusinessLogic.DataAnalyzer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="/App_Static/jslib/Chart.min.js"></script>
    <script src="/App_Static/jslib/domtoimage.js"></script>
    <style>
        .templates-list-div > .mGrid {
            margin-top:0;
            margin-bottom:1px;
        }
        .reg-results, .reg-anova {
            border: 1px solid #ccc;
            background-color: #F5F5F5;
        }
        .mra-links a.button-action {
		    color: yellow;
		    margin-bottom: 3px;
		    display: inline-block;
		    cursor: pointer;
		    margin: 3px 15px;
    		margin-top: 14px;
    		font-weight:500;
		    float:left;
		    display: none;
		}

            .reg-results > tbody > tr > td {
                width: 50%;
                padding: 5px;
            }

                .reg-results > tbody > tr > td > div {
                    font-size: 0.8em;
                }

                .reg-results > tbody > tr > td > span {
                    font-size: 1.2em;
                    font-weight: 500;
                }

            .reg-anova > tbody > tr > td {
                padding: 3px;
            }

        .info-button {
            float: right;
            display: inline-block;
            background: #ceecf7;
            width: 36px;
            height: 32px;
            color: navy;
            font-size: 1.8em;
            font-style: italic;
            font-family: Times New Roman;
            font-weight: bold;
            text-align: center;
            padding-top: 1px;
            box-sizing: border-box;
            border-top: 4px solid navy;
            cursor: pointer;
        }

        .mra-item > a > span {
            display: inline-block;
            margin-left: 0px;
            text-decoration: none;
            color: red;
            font-size: 0.8em;
        }

        .result-scroller {
            overflow-y: scroll;
        }

        .field-list-bar {
            background-color: #F5F5F5;
        }

            .field-list-bar > span {
                display: inline-block;
                font-size: 1.0em;
                font-weight: 500;
                padding: 4px 12px;
                margin: 4px;
                float: left;
            }

        .field-list {
            display: inline;
        }

            .field-list a {
                display: inline-block;
                float: left;
                font-size: 1.0em;
                border: 1px solid #CCC;
                padding: 4px 12px;
                border-radius: 20px;
                margin: 4px;
                font-weight: 500;
                cursor: pointer;
            }

                .field-list a.selected, .field-list a.selected:hover {
                    background-color: #663399;
                    color: white;
                }

        .template-name > span {
            font-size: 0.6em;
        }

        .residuals-loading {
            padding: 40px;
            text-align: center;
            font-style: italic;
        }

        .page-button {
        }

        

        .report-links a {
            padding: 3px 5px;
            cursor: pointer;
            font-weight: 500;
            text-decoration: underline;
        }

        .result-filter {
            height: auto;
        }

            .result-filter .result-filter-body {
                padding: 10px;
            }



            .result-filter .range-fields > span {
                font-size: 1.1em;
                font-weight: 500;
                display: inline-block;
                padding-left: 10px;
                padding-right: 5px;
                vertical-align: text-bottom;
            }

        .slider-filter > span:first-child {
            padding-right: 20px;
            padding-left: 0px;
        }

        .slider-filter > input[type="number"] {
            width: 70px;
        }

        .slider-filter > .rf-slider {
            display: inline-block;
            width: 300px;
            margin-right: 20px;
        }

        .result-filter .div_chk {
            margin-right: 20px;
        }

            .result-filter .div_chk input[type=checkbox] {
                min-width: 18px;
                min-height: 18px;
                width: 18px;
            }

            .result-filter .div_chk label {
                font-size: 1.1em;
                display: inline-block;
                vertical-align: top;
                padding-top: 2px;
            }

        button {
            font-family: Segoe UI, Arial, Helvetica, sans-serif;
            border: 0px none;
            font-weight: 600;
            cursor: pointer;
            box-sizing: border-box;
        }
        
        .close-button:hover {
        	background-color: #115293;
        }
        
        .close-button {
        	margin:5px;
        	padding: 6px 12px;
		    border-radius: 4px;
		    border: none;
		    background-color: #1976d2;
		    color: #fff;
		    font-weight: 600;
		    line-height: 1.4;
		    font-size: 1.1em;
		    cursor: pointer;
		    text-align: center;
		    min-width: 70px;
        }
        
        .color-gradient {
        	width:100%;
        	height:12.5%;
        }
        
        .corr-table-head {
        	min-width: 80px;
        	max-width: 80px;
		    overflow: hidden;
		    text-overflow: ellipsis;
		    white-space: nowrap;
		    cursor : pointer;
        }
        
        .corr-table-color {
        	border: 3px solid #fff;
        }
        .corr-result {
        	max-width: 90%;
        	max-height: 90%;
        }

        .range-filter-panel {
            margin-top: 15px;
        }

            .range-filter-panel button {
                padding: 7px 20px;
                background-color: #ce7600;
            }

        
    </style>
    <script src="/App_Static/js-v2/ChartManager.js"></script>
    <script src="/App_Static/js-v2/RangeFilter.js"></script>
    <script src="/App_Static/js-v2/mra/mra-common.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
    <%--<script src="/App_Static/js-v2/mra/mra-result.js"></script>--%>
    <script>
        $.getScript('/App_Static/js-v2/mra/mra-result.js?zt=' + (new Date).getTime());

        $(function () {
		 	$('.button-action-clone').on('click', function () {         
				var d = { templateId , clone: true };
				var url = 'mra.aspx?' + URLPack(d) + '&t=' + (new Date()).getTime();
				window.location.href = url;
				return false;
	        })
        });

        function applyFilter() {
            filter.set();
            loadResidualData(0);
            loadGraph(selectedFieldId);
            $('[viewselector=".result-filter"]').trigger('click');
        }
        
        function numberWithCommas(x) {
    		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		}

        function showCPInfo(pd) {
            var cpi = $('.chart-info');
            $('.iv-parcel', cpi).html(pd.pk);
            if(outputType != 8) {
            	$('.iv-actual', cpi).html(numberWithCommas(Math.round($out(pd.a) * 100) / 100));
            	$('.iv-predicted', cpi).html(numberWithCommas(Math.round($out(pd.p) * 100) / 100));
            } else {
            	$('.iv-actual', cpi).html($out(pd.a));
            	$('.iv-predicted', cpi).html($out(pd.p));
            }
            //$(".iv-actual", cpi).html($out(pd.a));
    		//$(".iv-predicted", cpi).html($out(pd.p));

            $('.iv-parcel', cpi).html(pd.pk);
            var cb = $(regchart.chart.canvas).box();
            var pb = cpi.box();
            var t = -cb.height + (cb.height - pb.height) / 2, l = (cb.width - pb.width) / 2;
            cpi.css({ 'margin-top': t, 'margin-left': l });
            cpi.show();

            var qcurl = '/protected/quality/#/parcel/' + pd.pid;
            $('.iv-qc-link', cpi).attr('href', qcurl);
            $('.iv-qc-link', cpi).attr('target', '_parcel_' + pd.pid);
            $('.iv-qc-link', cpi).attr('rel', 'noopener');

            $(regchart.chart.canvas).attr('disabled', 'disabled');

            $('.close', cpi).off('click').on('click', () => {
                $(regchart.chart.canvas).removeAttr('disabled');
                cpi.hide();
            })
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="wui-layout d1001">
        <div class="ca3-bg" style="width: 300px; padding: 5px;">
            <div class="data-col-head">
                <span>Regression Models</span>
                <a href="mra.aspx" class="new-btn"> New </a>
            </div>
            <div class="templates-list-div vscroll thin-scrollbar">
                <table class="mGrid app-content common-list unselectable" cellspacing="0" style="width: 100%; border-collapse: collapse;">
                    <tbody class="templates-list"></tbody>
                </table>
            </div>
<%-- 
            <div class="c">
                <button style="padding: 4px 15px; margin: 10px;" action="location.href = 'mra.aspx'">Create new model</button>
            </div> --%>
        </div>
        <div class="wui-stretch">
            <div class="ca2-bg ca unselectable" style="padding: 5px 10px;">
                <h1 class="nomar template-name fl"></h1>
                <div class="info-button fr" status="1" onclick="toggleRegressionInfo(this);">i</div>
                <div class="mra-links">
                	<a class="button-action button-action-clone">Clone Model</a>
                </div>
                <div class="mra-links fr">
                	<a href="mra-calc.aspx" class="button-action">Goto Calculation</a>
                </div>
            </div>

            <div class="ca4-bg output-data template-result" style="padding: 5px; display: none;">
                <div class="wui-sub" style="">
                    <div class="" style="width: 380px; padding: 3px;">
                        <table style="width: 100%; max-width: 370px;" class="reg-results">
                            <tbody>
                                <tr style="display: none;">
                                    <td>
                                        <div>Observations</div>
                                        <span field="Observations">0</span>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div>Standard Error</div>
                                        <span field="StandardError" format="f">0</span>
                                    </td>
                                    <td>
                                        <div>Multiple R</div>
                                        <span field="MultipleR" format="f">0</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div>R Square</div>
                                        <span field="RSquare" format="f">0</span>
                                    </td>
                                    <td>
                                        <div>Adjusted R Square</div>
                                        <span field="AdjustedRSquare" format="f">0</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="wui-stretch" style="padding: 3px; padding-left: 0px;">
                        <table style="width: 100%;" class="reg-anova">
                            <tbody>
                                <tr>
                                    <th scope="col" class="ca2-bg b c">ANOVA</th>
                                    <th class="c" scope="col">dF</th>
                                    <th class="r" scope="col">SS</th>
                                    <th class="r" scope="col">MS</th>
                                    <th class="r" scope="col">Regression-F</th>
                                    <th class="r" scope="col">Significance-F</th>
                                </tr>
                                <tr>
                                    <td style="width: 90px;">Regression</td>
                                    <td class="c" style="width: 60px;" field="RegressionDegreeOfFreedom">0</td>
                                    <td class="r" style="min-width: 120px;" field="RegressionSumOfSquares" format="f">0</td>
                                    <td class="r" style="min-width: 120px;" field="RegressionMeanSquare" format="f">0</td>
                                    <td class="r" style="min-width: 120px;" field="RegressionF" format="f">0</td>
                                    <td class="r" style="min-width: 120px;" field="SignificantF" format="f">0</td>
                                </tr>
                                <tr>
                                    <td>Residual</td>
                                    <td class="c" field="ResidualDegreeOfFreedom">0</td>
                                    <td class="r" field="ResidualSumOfSquares" format="f">0</td>
                                    <td class="r" field="ResidualMeanSquare" format="f">0</td>
                                    <td class="r">&nbsp;</td>
                                    <td class="r">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Total</td>
                                    <td class="c" field="Observations">0</td>
                                    <td class="r" format="f">&nbsp;</td>
                                    <td class="r">&nbsp;</td>
                                    <td class="r">&nbsp;</td>
                                    <td class="r">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="result-scroller output-data">
                <table class="mGrid app-content" style="width: 100%; max-width: 1200px; margin-top: 0px;">
                    <thead>
                        <tr class="thr">
                            <th scope="col">Input Field (Factor)
                            </th>
                            <th scope="col">Coefficient
                            </th>
                            <th scope="col">Standard Error
                            </th>
                            <th scope="col">t-Stat
                            </th>
                            <th scope="col">p-Value
                            </th>
                        </tr>
                    </thead>
                    <tbody class="coefficients-list"></tbody>
                </table>
                <div class="field-list-bar unselectable">
                    <span>Select Field: </span>
                    <div class="field-list ca"></div>
                </div>
                <div class="wui-sub" style="">
                    <div class="wui-stretch chart1-container">
                        <canvas height="300" id="chart1"></canvas>
                        <div class="chart-info" style="display: none;">
                            <a class="close" nosel="1">X</a>
                            <h3 class="nomar" style="margin-bottom: 6px;">Parcel: <span class="iv-parcel b2"></span></h3>
                            <table style="border-spacing: 0px;">
                                <tr>
                                    <td style="width: 80px;">Actual: </td>
                                    <td class="iv-actual iv-field r"></td>
                                </tr>
                                <tr>
                                    <td>Predicted: </td>
                                    <td class="iv-predicted iv-field r"></td>
                                </tr>
                            </table>
                            <div class="r" nosel="1">
                                <a class="b2 iv-qc-link">Open in QC</a>
                            </div>

                        </div>
                    </div>
                    <div style="width: 400px; min-height: 300px;">
                        <canvas height="300" width="400" id="chart2"></canvas>
                    </div>
                </div>
                <div class="section-report">
                    <div class="ca4-bg ca unselectable" style="padding: 5px 10px;">
                        <h3 class="nomar fl">Residual Output</h3>
                        <span class="residuals-count fl" style="margin-left: 10px; display: inline-block; margin-top: 5px; font-weight: 500;">0 parcels</span>
                        <div class="fr report-links">
                            <a class="corr-matrix" onclick="getCorrelationMatrix();event.stopPropagation();">Correlation Matrix</a>  
                            <a class="b-t-v" viewselector=".result-filter" status="0">Filter</a>
                            <a style="display:none;">Open in QC</a>
                            <a class="data-download" onclick="downloadReport();">Download Report</a>
                        </div>
                    </div>
                    <div class="result-filter unselectable">
                        <div class="ca4-bg ca" style="padding: 5px 10px; display: none;">
                            <h3 class="nomar">Data Range Filter</h3>
                        </div>
                        <div class="result-filter-body">
                            <div class="range-fields slider-filter"></div>
                            <div style="margin-top: 8px;" class="ca">
                                <div class="div_chk fl">
                                    <input type="checkbox" id="chk_range_mirror" />
                                    <label for="chk_range_mirror">Mirrored range</label>
                                </div>
                                <div class="div_chk fl">
                                    <input type="checkbox" id="chk_range_inverse" />
                                    <label for="chk_range_inverse">Inverse selection (select parcels out of selected range)</label>
	                                </div>
	                                <div class="fr range-filter-panel">
	                                    <button class="ca6-bg" action="applyFilter();">Apply Filter</button>
	                                    <button class="ca6-bg" cancel="true">Cancel</button>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <div style="padding: 8px 20px;">
	
	                        <div class="data-page-links unselectable">
	                        </div>
	                        <div class="rt-container">
	                            <table class="mGrid app-content residual-table" style="width: 100%; max-width: 1200px;">
	                                <thead>
                                    <tr class="thr">
                                        <th scope="col" title="Observation No." style="width: 70px;text-decoration: underline;cursor: pointer;" onclick = "sortResidualDatabyId()">Obs.#</th>
                                        <th scope="col">Parcel</th>
                                        <th scope="col" style="width: 170px;">Actual Value</th>
                                        <th scope="col" style="width: 170px;">Predicted Value</th>
                                        <th scope="col" style="width: 140px;">Residual</th>
                                        <th scope="col" style="width: 90px;text-decoration: underline;cursor: pointer;" onclick = "sortResidualDatabyRatio()">Ratio</th>
                                    </tr>
                                </thead>
                                <tbody class="residual-list"></tbody>
                            </table>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="shade dock-full" style="display: none; z-index:3"></div>
    <div class="popup-dg corr-result" style=" display:none; z-index:4;">
		<div style="display: flex; justify-content: space-between; margin-left: 10px;">
            <h1 class="nomar filter-title">Correlation Matrix</h1>
        </div>
        <div class="corr-head" style="overflow: auto;">
            <div class="corr-conatiner" style="display: flex;">
	            <div class="vscroll thin-scrollbar" style="margin: 10px 10px 16px;max-height:3500px; overflow: inherit;">
	        		<table class="corr-result-table table-new">	
	        		</table>
	        	</div>
	        	<div class="vscroll thin-scrollbar" style="margin: 10px 10px 16px 10px;max-height:3500px; overflow: inherit;">
	        		<table class="corr-result-matrix table-new">	
	        		</table>
	        	</div>
	        	<!--<div class="corr-gradient" style="width:20px; height: fit-content;"><b>
	        		1<div class="blue-white" style="width: 100%; height: 50%; background-image: linear-gradient(#2b2bca, #ffffff);"></div>
	        		0<div class="white-red" style="width: 100%; height: 50%; background-image: linear-gradient(#ffffff, #ca2b2b);"></div>
	        		-1</b>
	         	</div>//gradient bar -->
	         	<div class="corr-gradient" style="width:20px; height: fit-content;">
	         		1<div class="color-gradient" style="background-color:#2b2bca;"></div>
	         		<div class="color-gradient" style="background-color:#5162ce;"></div>
	         		<div class="color-gradient" style="background-color:#8192be;"></div>
	         		<div class="color-gradient" style="background-color:#eeeeff;"></div>
	         		0<div class="color-gradient" style="background-color:#ffeeee;"></div>
	         		<div class="color-gradient" style="background-color:#be8192;"></div>
	         		<div class="color-gradient" style="background-color:#ce5162;"></div>
	         		<div class="color-gradient" style="background-color:#ca2b2b;"></div>
	         		-1
	         	</div>
	        </div>
         </div>
         <div class="matrix-button-panel" style="justify-content: flex-end; display: flex;">
         	<button class="close-button matrix-save-image" style="margin-right: 5px;" onclick="downloadReport(1); return false;">
                Export As Excel
            </button>
         	<button class="close-button matrix-save-image" style="margin-right: 5px;" onclick="saveAsJPEG(); return false;">
                Save As Image
            </button>
            <button class="close-button matrix-close" onclick="$('.corr-result').hide();enableLayout(); return false;">
                Close
            </button>
		</div>
    </div>
</asp:Content>
<script runat="server">
    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetLoadInfo() As ActionResult(Of MRADesignerPageLoadInfo)
        Dim l As New ActionResult(Of MRADesignerPageLoadInfo)
        Try
            Dim li As New MRADesignerPageLoadInfo
            li.GIS = Database.Tenant.GetDataTable("EXEC GIS_GetConfiguration").ConvertToList(Of GISConfig).FirstOrDefault
            li.Datasets = Database.Tenant.GetDataTable("SELECT * FROM MRA_DataSet WHERE ISNULL(PoolSize,0) > 0 AND Error = 0 ORDER BY Name").ConvertToList(Of Dataset)
            li.Templates = Database.Tenant.GetDataTable("SELECT t.*, CAST(CASE WHEN r.TemplateID IS NULL THEN 0 ELSE 1 END AS BIT) As Compiled, f.Name OutputFieldName, f.SourceTable As OutputFieldTable, ISNULL(t.OutputFieldLabel, f.DisplayLabel) As DisplayLabel, f.DataType FROM MRA_Templates t LEFT OUTER JOIN MRA_Result r ON t.ID = r.TemplateID LEFT OUTER JOIN DataSourceField f ON t.OutputField = f.ID ORDER BY Compiled DESC, Name").ConvertToList(Of MRATemplate)
            l.Data = li ' 
            l.Success = l.Data IsNot Nothing
            l.Count = li.Templates.Count
            l.FullCount = l.Count
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        'l.Query = q
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetTemplate(templateId As Integer) As ActionResult(Of MRAResultInfo)
        Dim l As New ActionResult(Of MRAResultInfo)
        Try
            Dim li As New MRAResultInfo
            li.Result = Database.Tenant.GetTopRow("SELECT * FROM MRA_Result WHERE TemplateID = " & templateId).ToKeyValueList
            li.Coefficients = Database.Tenant.GetDataTable("SELECT * FROM MRA_Coefficients WHERE TemplateID = " & templateId).ConvertToList(Of MRACoefficients)
            li.RatioDistribution = Database.Tenant.GetDataTable("EXEC MRA_GetGraphData 'ratio-distribution', @TemplateID = " & templateId).ConvertToList
            li.InputFields = Database.Tenant.GetDataTable("SELECT FieldID, Label, Aggregate, Expression, FilterOperator, CAST(FilterValue AS VARCHAR(50)) As FilterValue, CAST(FilterValue2 As VARCHAR(50)) As FilterValue2, LookupTable FROM MRA_TemplateFields WHERE TemplateID = " & templateId).ConvertToList(Of MRAInputFields)
            l.Data = li ' 
            l.Success = l.Data IsNot Nothing
            l.Count = 1
            l.FullCount = li.Coefficients.Count
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        'l.Query = q
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetChartData(templateId As Integer, fieldId As Integer, rangeApplied As Boolean, minVal As Single, maxVal As Single, inversed As Boolean) As ActionResult(Of MRAOutputsChartData)
        Dim l As New ActionResult(Of MRAOutputsChartData)
        Try
            l.Data = New MRAOutputsChartData

            Dim ds = Database.Tenant.GetDataSet("EXEC MRA_GetGraphData 'regression-graph', {0}, {1}, @RangeApplied = {2}, @MinVal = {3}, @MaxVal = {4}, @RangeInversed = {5}".SqlFormat(True, templateId, fieldId, rangeApplied, minVal, maxVal, inversed))

            'l.Data.Properties = ds.Tables(0).ToRows.FirstOrDefault.ToKeyValueList
            'l.Data.RatioData = ds.Tables(1).ToRows.Select(Function(x) New XYDecimal With {.x = x.Get("InputValue"), .y = x.Get("PredictedValue")}).ToList
            'l.Data.RatioDistribution = ds.Tables(2).ToRows.Select(Function(x) New XYDecimal With {.x = x.Get("RoundedRatio"), .y = x.Get("Percentage")}).ToList
            l.Data.GraphData = ds.Tables(0).ConvertToList
            l.Success = True
            l.Count = l.Data.GraphData.Count
            l.FullCount = l.Count
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        'l.Query = q
        Return l
    End Function

    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function GetResidualsData(templateId As Integer, pageIndex As Integer, pageSize As Integer, rangeApplied As Boolean, rangeMin As Single, rangeMax As Single, rangeInverse As Boolean,RangeSort As Integer) As ActionResultData
        Dim l As New ActionResultData
        Try
            Dim ds = Database.Tenant.GetDataSet("EXEC MRA_GetDataForExport {0}, 'residuals-view', {1}, {2}, {3}, {4}, {5}, {6}, {7}".SqlFormat(False, templateId, pageIndex, pageSize, rangeApplied, rangeMin, rangeMax, rangeInverse, RangeSort))

            l.Data = ds.Tables(0).ConvertToList
            l.Success = True
            l.Count = l.Data.Count
            l.FullCount = ds.Tables(1).Rows(0).Get(0)
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        'l.Query = q
        Return l
    End Function
	
	<WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function LoadCorrelation(templateId As Integer) As ActionResultData
        Dim l As New ActionResultData
        Try
            Dim ds = Database.Tenant.GetDataSet("EXEC MRA_GetCorrelationReport {0}".SqlFormat(False, templateId))

            l.Data = ds.Tables(0).ConvertToList
            l.Success = True
            l.Count = l.Data.Count
            l.FullCount = l.Data.Count
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        'l.Query = q
        Return l
    End Function
	
    <WebMethod, ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)> Public Shared Function DeleteTemplate(templateId As Integer) As ActionResult(Of Boolean)
        Dim l As New ActionResult(Of Boolean)
        Try

            l.Count = CAMACloud.BusinessLogic.DataAnalyzer.RegressionAnalyzer.DeleteTemplate(templateId)
            l.Success = True
            l.FullCount = l.Count
        Catch ex As Exception
            l.ErrorMessage = ex.Message
        End Try
        'l.Query = q
        Return l
    End Function

    Public Sub Page_PageCommand(commandName As String) Handles Me.RunCommand
        Dim templateId As Integer = 0
        If Integer.TryParse(Request("templateId"), templateId) Then
            Dim template As System.Data.DataRow = Database.Tenant.GetTopRow("EXEC [MRA_GetTemplate] " & templateId)
            Dim reportTitle As String = "Data Export", fileName As String = "report", templateName As String = template.GetString("Name"), outputLabel As String = template.GetString("DisplayLabel")

            Dim outputField As String = template.GetString("OutputFieldName")
            Dim timeSignature = Now.ToString("yyyyMMdd") + "_" + Now.ToString("HHmm")
            Dim exportGrid As New GridView
            Page.Controls.Add(exportGrid)

            Dim rangeMin As Single = 0, rangeMax As Single, rangeInverse As Boolean, rangeApplied As Boolean = False
            If Request.QueryString.AllKeys.Contains("rmin") Then
                rangeApplied = True
                Single.TryParse(Request("rmin"), rangeMin)
                Single.TryParse(Request("rmax"), rangeMax)
                Boolean.TryParse(Request("rinv"), rangeInverse)
            End If

             If commandName = "exportdata" Then
                reportTitle = templateName + " - Residuals Report - " + outputLabel
                fileName = {templateName, outputField, "residuals", timeSignature}.Aggregate(Function(x, y) x + "_" + y)
                Dim data = RegressionAnalyzer.GetReportsData(templateId, "residuals-export", exportGrid, rangeApplied, rangeMin, rangeMax, rangeInverse)
                exportGrid.ExportAsExcel(data, reportTitle, fileName, "residuals")
            ElseIf commandName = "exportinput" Then
                reportTitle = templateName + " - Sample Data - " + outputLabel
                fileName = {templateName, outputField, "samples", timeSignature}.Aggregate(Function(x, y) x + "_" + y)
                Dim data = RegressionAnalyzer.GetReportsData(templateId, "input-data-export", exportGrid, rangeApplied, rangeMin, rangeMax, rangeInverse)
                exportGrid.ExportAsExcel(data, reportTitle, fileName, "sample-data")
            ElseIf commandName = "exportCorrelation" Then
            	Dim Tstring = RegressionAnalyzer.BuildCorrTables(templateId)
            	Dim Response = Page.Response
	        	Response.Clear()
		        Response.Buffer = True
		        Response.ClearContent()
		        Response.ClearHeaders()
		        Response.AddHeader("Content-Disposition", "attachment;filename=" + templateName + "-Correlation-Report.xlsx")
		        Response.Charset = ""
		        Response.ContentType = "application/vnd.openxml.formats-officedocument.spreadsheetml.sheet"
		        Dim temp As TableExportExcel = New TableExportExcel()
		        Dim excelStream = temp.Generate(Tstring)
		        excelStream.CopyTo(Response.OutputStream)
		        Response.End()
            End If

            Page.Controls.Remove(exportGrid)
        End If
    End Sub

    Public Class MRAResultInfo
        Public Property Result As Dictionary(Of String, Object)
        Public Property Coefficients As List(Of MRACoefficients)
        Public Property RatioDistribution As JSONTable
        Public Property InputFields As List(Of MRAInputFields)
    End Class

    Public Class MRACoefficients
        Public Property FieldId As Integer
        Public Property Head As String
        Public Property FieldSpecification As String
        Public Property Coefficient As Double
        Public Property StandardError As Double
        Public Property tStat As Double
        Public Property pValue As Double
    End Class

    Public Class MRAOutputsChartData
        Public Property RatioData As List(Of XYDecimal)
        Public Property Properties As Dictionary(Of String, Object)
        Public Property RatioDistribution As List(Of XYDecimal)
        Public Property GraphData As JSONTable
    End Class
    
    Public Class MRAInputFields
        Public Property FieldId As Integer
        Public Property Label As String
        Public Property Aggregate As String
        Public Property Expression As String
        Public Property FilterOperator As String
        Public Property FilterValue As String
        Public Property FilterValue2 As String
        Public Property LookupTable As String
    End Class
</script>
