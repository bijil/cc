﻿Imports CAMACloud.BusinessLogic.DataAnalyzer
Public Class mra
    Inherits System.Web.UI.Page


    Public Event RunCommand(pageCommand As String)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then

        End If

        If Page.Request.CurrentExecutionFilePath <> Page.Request.Url.AbsolutePath Then
            Dim commandName = Page.Request.Url.AbsolutePath.Replace(Page.Request.CurrentExecutionFilePath, "").TrimStart("/")
            RaiseEvent RunCommand(commandName)
        End If

    End Sub

End Class

Public Class MRADesignerPageLoadInfo
    Public Property GIS As GISConfig
    Public Property Datasets As List(Of Dataset)
    Public Property Templates As List(Of MRATemplate)
    Public Property LookupNames As JSONTable
    Public Property SourceTables As JSONTable	
End Class

Public Class MRATemplate
    Public Property ID As Integer
    Public Property DataSetID As Integer
    Public Property Name As String
    Public Property DisplayLabel As String
    Public Property DataType As Integer
    Public Property OutputField As Integer
    Public Property OutputAggregate As String
    Public Property OutputFilterOperator As String
    Public Property OutputFilterValue As String
    Public Property OutputFilterValue2 As String
    Public Property PoolSize As Integer
    Public Property OutputFieldName As String
    Public Property OutputFieldTable As String
    Public Property OutputFieldExpression As String
    Public Property Compiled As Boolean
    Public Property NoIntercept As Boolean
    Public Property AggregateOverExpression As Boolean
End Class

Public Class XY
    Public Property x As Double

    Public Property y As Double

    Public Property ParcelId As String

    Public Property KeyValue1 As String
End Class

Public Class XYDecimal
    Public Property x As Decimal
    Public Property y As Decimal
End Class