﻿Imports CAMACloud.BusinessLogic
Imports CAMACloud.BusinessLogic.Installation
Imports CAMACloud.BusinessLogic.Installation.EnvironmentConfiguration
Public Class miscellaneous
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        RefreshPurgeLockStatus()
    End Sub

    Protected Sub lnkExport_Click(sender As Object, e As EventArgs) Handles lnkExport.Click
        Dim gen = New EnvironmentConfiguration
        gen.ExportSettings(ConfigFileType.All, Response, "configuration", Database.Tenant)
    End Sub

    Protected Sub lbImport_Click(sender As Object, e As System.EventArgs) Handles btnUploadImportFile.Click
        If fuImportFile.HasFile Then
        	Try
        		Dim Name As String = fuImportFile.FileName
        		Name = Name.Replace(".xml", "")
                Dim gen = New EnvironmentConfiguration
                gen.ImportSettings(fuImportFile.FileContent, ConfigFileType.All, Database.Tenant, Membership.GetUser().ToString, "configuration", Name)
                NotificationMailer.SendNotificationToVendorCDS(Membership.GetUser().ToString, 1, "Miscellaneous Tasks")
                UpdateCCSettings()
                Alert("Imported successfully")
            Catch ex As Exception
                Alert(ex.Message)
                Return
            End Try
        End If
    End Sub

    Sub RefreshPurgeLockStatus()
        If Not Session("roleLink") Is Nothing Then
            Dim roleSession = Session("roleLink").ToString()
            If roleSession = "BasicSettings" Then
                For Each con As Control In dvMiscellaneous.Controls
                    If con.ID = "dvBasicSettings" Then
                    	con.Visible = True
                    	dvEYPDescription.Visible = True
                    Else
                    	con.Visible = False
                    End If
                Next
            Else
            	dvEYPDescription.Visible = False
            End If
        End If
        Dim Username = Membership.GetUser().ToString
        Dim role = Roles.GetRolesForUser(Username)
        If role.Length >= 1 Then
        	If role.Contains("BasicSettings") Then
         		dvBasicSettings.Visible = True
         	End If
        End If	
        If Database.System.GetTopRow("SELECT PurgeLocked FROM Organization WHERE ID = " & HttpContext.Current.GetCAMASession.OrganizationId)(0) = True Then
            lblPurgeLockStatus.Text = "ON"
            lbTogglePurgeLock.Text = "Click here to turn OFF"
            hdnPurgeStatus.Value = "1"
        Else
            lblPurgeLockStatus.Text = "OFF"
            lbTogglePurgeLock.Text = "Click here to turn ON"
            hdnPurgeStatus.Value = "0"
        End If
    End Sub

    Private Sub lbTogglePurgeLock_Click(sender As Object, e As System.EventArgs) Handles lbTogglePurgeLock.Click
        If hdnPurgeStatus.Value = "1" Then
            Database.System.Execute("UPDATE Organization SET PurgeLocked = 0 WHERE ID = " & HttpContext.Current.GetCAMASession.OrganizationId)
        Else
            Database.System.Execute("UPDATE Organization SET PurgeLocked = 1 WHERE ID = " & HttpContext.Current.GetCAMASession.OrganizationId)
        End If
        RefreshPurgeLockStatus()
    End Sub

    Sub LoadPendingJobs()
        Dim sql As String = "SELECT ID, TransferType, CASE TransferType WHEN 101 THEN 'Data Transfer' WHEN 102 THEN 'Photo Transfer' ELSE 'Other' END As Type FROM TransferJob WHERE Cancelled = 0 AND Completed = 0"
        gridPendingJobs.DataSource = Database.Tenant.GetDataTable(sql)
        gridPendingJobs.DataBind()
    End Sub
    Sub UpdateCCSettings()
    	Database.Tenant.Execute("if exists(SELECT * FROM ClientSettings WHERE Name = 'LastSchemaUpdatedTime') UPDATE ClientSettings SET Value = CONVERT(NVARCHAR(24),GETUTCDATE(),121) WHERE Name = 'LastSchemaUpdatedTime' else INSERT INTO ClientSettings(name,Value) Values('LastSchemaUpdatedTime',CONVERT(NVARCHAR(24),GETUTCDATE(),121))")
    	Database.Tenant.Execute("if exists(SELECT * FROM ClientSettings WHERE Name = 'LastSchemaUpdatedCounter') UPDATE ClientSettings SET Value = Value + 1 WHERE Name = 'LastSchemaUpdatedCounter' else INSERT INTO ClientSettings(name,Value) Values('LastSchemaUpdatedCounter','0')")
    End Sub

    Private Sub lbShowJobs_Click(sender As Object, e As System.EventArgs) Handles lbShowJobs.Click
        LoadPendingJobs()
    End Sub

    Private Sub gridPendingJobs_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gridPendingJobs.RowCommand
        If e.CommandName = "CancelJob" Then
            Database.Tenant.Execute("UPDATE TransferJob SET Cancelled = 1, Reason = 'Cancelled from Console' WHERE ID = " & e.CommandArgument)
            LoadPendingJobs()
        End If
    End Sub

    Private Sub lbUncommitPhoto_Click(sender As Object, e As System.EventArgs) Handles lbUncommitPhoto.Click
        Dim jobId As Integer = -1
        If txtJobId.Text = "" Then
            txtJobId.Text = Database.Tenant.GetIntegerValue("SELECT MAX(ID) FROM TransferJob WHERE TransferType = 102 AND Completed = 1 AND Cancelled = 0")
        End If
        If Integer.TryParse(txtJobId.Text, jobId) Then
            If Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM TransferJob WHERE ID = " & jobId & " AND TransferType = 102 AND Completed = 1 AND Cancelled = 0") > 0 Then
                Database.Tenant.Execute("CREATE TABLE temp_Page_ParcelImages (ParcelImageId INT);")
                Database.Tenant.Execute("UPDATE TransferJob SET Cancelled = 0, Completed = 0 WHERE ID = " & jobId)
                Alert("Job uncommitted successfully.")
                txtJobId.Text = ""
                Return
            End If
        End If
        Alert("Invalid Photo Job ID")
    End Sub
    Private Sub lbResetAllQCParcels_Click(sender As Object, e As System.EventArgs) Handles lbResetAllQCParcels.Click
    	Try
    		Dim pCount = Database.Tenant.GetIntegerValue("SELECT DISTINCT ParcelId INTO #changes FROM ParcelChanges;SELECT DISTINCT ParcelId INTO #images FROM ParcelImages WHERE DownSynced = 0;SELECT COUNT(*) FROM Parcel p LEFT JOIN #changes pci ON p.Id = pci.ParcelId LEFT JOIN #images img ON p.Id = img.ParcelId WHERE p.QC = 1 AND (pci.ParcelId IS NOT NULL OR img.ParcelId IS NOT NULL);Drop table #changes;Drop table #images;")
    		RunScript("warnResetAllParcels(" + pCount.ToString() + ")")
        Catch ex As Exception
            Alert(ex.Message)
            Return
        End Try
    End Sub

    Protected Sub lbResetDbUpdatedTime_Click(sender As Object, e As EventArgs) Handles lbResetDbUpdatedTime.Click
        Dim query As String = "IF ((SELECT COUNT(*) FROM ClientSettings WHERE Name = 'DbUpdatedTime') = 0) INSERT INTO ClientSettings VALUES('DbUpdatedTime',(SELECT cast(Datediff(s, '1970-01-01', GETUTCDATE()) AS BIGINT)*1000)) ELSE UPDATE ClientSettings SET Value = (SELECT cast(Datediff(s, '1970-01-01', GETUTCDATE()) AS BIGINT)*1000) WHERE Name = 'DbUpdatedTime'"
        Database.Tenant.Execute(query)
        Alert("QC Cache Cleared.")
    End Sub

    Protected Sub lbResetAllSearchResultFields_Click(sender As Object, e As EventArgs)
        Dim dtr As String = 0
        Dim reset As String = 1
        Dim _camaSystemName = Database.System.GetStringValue("Select c.Name FROM Organization o INNER JOIN CAMASystem c On o.CAMASystem= c.Id WHERE o.id = " & HttpContext.Current.GetCAMASession().OrganizationId)
		If _camaSystemName Is Nothing Or _camaSystemName = "" Then
			_camaSystemName = "0"
		End If
        e_("EXEC qc_GetSearchFieldSettings @LoginId={0}, @Reset={1},@DTR={2},@camaSystem={3}".SqlFormatString(UserName, reset, dtr, _camaSystemName))
        If Roles.IsUserInRole("DTR") Then
            dtr = 1
            e_("EXEC qc_GetSearchFieldSettings @LoginId={0}, @Reset={1},@DTR={2},@camaSystem={3}".SqlFormatString(UserName, 0, dtr, _camaSystemName))
        End If
    End Sub
    
    Protected Sub btnHidden_Click(sender As Object, e As EventArgs) Handles btnHidden.Click
        proceedToResetAllParcels()
    End Sub
    
    Private Sub proceedToResetAllParcels()
    	Try
    		e_("EXEC cc_ResetAllParcels {0}", True, UserName)
    	Catch ex As Exception    		
            Alert(ex.Message)
            Return
    	End Try
        Alert("All parcels have been reset successfully.")
    End Sub
End Class