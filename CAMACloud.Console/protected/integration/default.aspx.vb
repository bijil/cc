﻿Imports CAMACloud.BusinessLogic
Imports CAMACloud.BusinessLogic.Installation
Imports CAMACloud.BusinessLogic.Installation.EnvironmentConfiguration



Partial Class protected_integration_default
    Inherits System.Web.UI.Page


    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            FillChanges()
        End If
    End Sub


    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSketch.Click, btnStreet.Click, btnNeighborhood.Click, btnParcel.Click, btnBulkApp.Click, btnMACMessage.Click



        ' Settings for Parcel and Neighborhood
        If Page.IsValid = False Then
            Return
        End If
        Dim btn As Button = DirectCast(sender, Button)
        Dim result As Boolean

        Select Case btn.ID
            Case "btnParcel"
                result = CreateOrUpdateApplicationSettings()
                'result = True
            Case "btnSketch"
                If (drpDimension.SelectedItem.Text = "Quarter Foot") Then
                    If (drpDecimal.SelectedItem.Text = "0" Or drpDecimal.SelectedItem.Text = "1") Then
                        Alert("To Enable Quarter Foot settings , Select sketch decimal greater than 1 ")
                        Return
                    End If
                End If
                If (drpDimension.SelectedItem.Text = "Half Foot") Then
                    If (drpDecimal.SelectedItem.Text = "0") Then
                        Alert("To Enable Half Foot settings , Select sketch decimal greater than 0 ")
                        Return
                    End If
                End If

                Dim configValue = ""
                Dim decimalsValue = ""
                Dim displaySqftValue = ""
                Dim roundToDecimalValue = ""
                If Not String.IsNullOrEmpty(GetSketchSSValue("SketchConfig")) Then
                    configValue = GetSketchSSValue("SketchConfig")
                End If
                If Not String.IsNullOrEmpty(GetSketchSSValue("SketchDecimals")) Then
                    decimalsValue = GetSketchSSValue("SketchDecimals")
                End If
                If Not String.IsNullOrEmpty(GetSketchSSValue("DisplaySQFTOnUI")) Then
                    displaySqftValue = GetSketchSSValue("DisplaySQFTOnUI")
                End If
                If Not String.IsNullOrEmpty(GetSketchSSValue("RoundToDecimalPlace")) Then
                    roundToDecimalValue = GetSketchSSValue("RoundToDecimalPlace")
                End If

                If configValue IsNot Nothing AndAlso decimalsValue IsNot Nothing AndAlso displaySqftValue IsNot Nothing AndAlso roundToDecimalValue IsNot Nothing AndAlso configValue = txtSketchConfig.Text AndAlso decimalsValue = drpDecimal.SelectedItem.Text AndAlso displaySqftValue = drpAreaDecimal.SelectedItem.Text AndAlso roundToDecimalValue = drpDimension.SelectedItem.Text Then
                    Alert("No changes found!")
                    result = False
                    Return
                End If
                CreateUpdateSketchSettings()
                result = True
            Case "btnNeighborhood"
                Dim DtApp As DataTable = Database.Tenant.GetDataTable("SELECT NeighborhoodTable,NeighborhoodField, NeighborhoodNameField FROM Application")
                Dim preNeighborhoodTable = ""
                Dim preNeighborhoodField = ""
                Dim preNeighborhoodNameField = ""
                For Each row As DataRow In DtApp.Rows
                    preNeighborhoodTable = row.Item("NeighborhoodTable").ToString()
                    preNeighborhoodField = row.Item("NeighborhoodField").ToString()
                    preNeighborhoodNameField = row.Item("NeighborhoodNameField").ToString()
                Next
                If preNeighborhoodTable = txtNeighborhoodTable.Text AndAlso preNeighborhoodField = txtNeighborhoodField.Text AndAlso preNeighborhoodNameField = txtNeighborhoodNameField.Text Then
                    Alert("No changes found!")
                    result = False
                    Return
                End If
                CreateOrUpdateNeighborhoodSettings()
                result = True
            Case "btnStreet"
                Dim streetAddress1 = txtStreetAddressTable.Text
                Dim targetStreetAddressTable = DataSource.FindTargetTable(Database.Tenant, streetAddress1)
                'Dim FieldstoCheck As String = txtFilterColoumnName.Text + "," + txtStreetNumberField.Text + "," + txtStreetNameField.Text + "," + txtStreetNameSuffixField.Text
                Dim FieldstoCheck As String = txtFilterColoumnName.Text + "," + txtStreetNumberField.Text + "," + txtStreetNameField.Text + "," + txtStreetNameSuffixField.Text + "," + txtStreetUnitField.Text
                Dim AlertMsg As String = Database.Tenant.GetStringValue("EXEC dbo.FieldFinder @tablename={0},@String={1},@tablegiven={2},@delimiter={3}".SqlFormatString(targetStreetAddressTable, FieldstoCheck, streetAddress1, ","))
                If AlertMsg <> "" Then
                    Alert(AlertMsg)
                    result = False
                    Return
                End If
                Dim filter = txtFilterColoumnName.Text + " " + ddlFilterCondition.SelectedValue.ToString + " " + txtFilterValue.Text
                If filter.ToString.Length > 500 Then
                    Alert("Combination of Filter Column Name, Filter Condition and Filter Value characters must be 500 or less characters")
                    result = False
                    Return
                End If

                result = CreateUpdateStreetAddressSettings()
            Case "btnBulkApp"

                If Not String.IsNullOrEmpty(txtBatchSize.Text) Then
                    If Not IsNumeric(txtBatchSize.Text) OrElse CDec(txtBatchSize.Text) <= 0 Then
                        Alert("Batch Size & Time Interval should be in a valid number format ")
                        result = False
                        Return
                    End If
                End If
                If Not String.IsNullOrEmpty(txtTimeInterval.Text) Then
                    If Not IsNumeric(txtTimeInterval.Text) OrElse CDec(txtTimeInterval.Text) <= 0 Then
                        Alert("Time Interval & Batch Size  should be in a valid number format ")
                        result = False
                        Return
                    End If
                End If
                If txtBatchSize.Text.Length > 500 Then
                    Alert("Please short your Batch Size characters to 500 or less")
                    result = False
                    Return
                ElseIf txtTimeInterval.Text.Length > 500 Then
                    Alert("Please short your Time Interval characters to 500 or less")
                    result = False
                    Return
                End If
                If GetClientSSValue("BatchSize") = txtBatchSize.Text AndAlso GetClientSSValue("TimeInterval") = txtTimeInterval.Text Then
                    Alert("No changes found!")
                    result = False
                    Return
                End If
                result = True
                saveBulkApprovalSettings()
            Case "btnMACMessage"
                If txtStart.Text.Length > 500 Then
                    Alert("Please short your Start Message characters to 500 or less")
                    result = False
                    Return
                ElseIf txtEnd.Text.Length > 500 Then
                    Alert("Please short your End Message characters to 500 or less")
                    result = False
                    Return

                    ' modified

                    'ElseIf txtStart.Text.Length = 0 And txtEnd.Text.Length = 0 Then
                    '    Alert("Please Enter Your Messages")
                    '    result = False
                    '    Return
                    'ElseIf txtStart.Text.Length = 0 Then
                    '    Alert("Please Enter Your Start Message")
                    '    result = False
                    '    Return
                    'ElseIf txtEnd.Text.Length = 0 Then
                    '    Alert("Please Enter Your End Message")
                    '    result = False
                    '    Return

                    ' end modified

                End If
                result = True
                MACMessageSetup()
        End Select
        If result Then
            Alert("Saved Successfully .")
        End If
        NotificationMailer.SendNotificationToVendorCDS(Membership.GetUser().ToString, 2, "Application Settings")
        Dim RedirScript As String = "window.onload = function(){ "
        RedirScript += "window.location = 'default.aspx';}"
        RunScript(RedirScript)

    End Sub

    Public Function GetClientSSValue(nm As String) As String
        Dim val As String = Database.Tenant.GetStringValue("SELECT VALUE FROM ClientSettings WHERE NAME = '" + nm + "'")
        Return val
    End Function
    Public Function GetSketchSSValue(nm As String) As String
        Dim val As String = Database.Tenant.GetStringValue("SELECT VALUE FROM SketchSettings WHERE NAME = '" + nm + "'")
        If val IsNot Nothing AndAlso Not IsDBNull(val) Then
            Return val.ToString()
        Else
            Return String.Empty
        End If
    End Function

    Public Function CreateOrUpdateApplicationSettings() As Boolean
        Dim showKeyfield As Boolean
        Dim showAlternateField As Boolean
        If chkShowKeyfield.Checked Then
            showKeyfield = True
        End If
        If chkShowAlternate.Checked Then
            showAlternateField = True
        End If
        Dim KeyFieldstoCheck As String = txtAlternateField.Text + "," + txtKeyField1.Text + "," + txtKeyField2.Text + "," + txtKeyField3.Text + "," + txtKeyField4.Text + "," + txtKeyField5.Text + "," + txtKeyField6.Text + "," + txtKeyField7.Text + "," + txtKeyField8.Text + "," + txtKeyField9.Text + "," + txtKeyField10.Text
        Dim AlertMsg As String = Database.Tenant.GetStringValue("EXEC dbo.FieldFinder @tablename={0},@String={1},@tablegiven={2},@delimiter={3}".SqlFormatString("ParcelData", KeyFieldstoCheck, "", ","))
        If AlertMsg <> "" Then
            Alert(AlertMsg)
            Return False
        End If
        Dim DtParcl As DataTable = Database.Tenant.GetDataTable("SELECT ParcelTable, Alternatekeyfieldvalue, KeyField1, KeyField2,KeyField3, KeyField4, KeyField5, KeyField6, KeyField7,KeyField8,
        KeyField9, KeyField10,  ShowKeyValue1,ShowAlternateField  FROM Application")
        Dim prePrclTbl = ""
        Dim preAltrKyFldVal = ""
        Dim parDesc = ""
        Dim preKyFld1 = ""
        Dim preKyFld2 = ""
        Dim preKyFld3 = ""
        Dim preKyFld4 = ""
        Dim preKyFld5 = ""
        Dim preKyFld6 = ""
        Dim preKyFld7 = ""
        Dim preKyFld8 = ""
        Dim preKyFld9 = ""
        Dim preKyFld10 = ""
        Dim prevShwKyVal = ""
        Dim prevShwAltrntfld = ""
        Dim keyvalueTrue As Boolean = False
        For Each row As DataRow In DtParcl.Rows

            If Not IsDBNull(row.Item("ParcelTable")) AndAlso Not String.IsNullOrEmpty(row.Item("ParcelTable").ToString()) Then
                prePrclTbl = row.Item("ParcelTable").ToString()
            End If
            ' prePrclTbl = row.Item("ParcelTable").ToString()
            If Not IsDBNull(row.Item("Alternatekeyfieldvalue")) AndAlso Not String.IsNullOrEmpty(row.Item("Alternatekeyfieldvalue").ToString()) Then
                preAltrKyFldVal = row.Item("Alternatekeyfieldvalue").ToString()
            End If
            'preAltrKyFldVal = row.Item("Alternatekeyfieldvalue").ToString()
            If Not IsDBNull(row.Item("KeyField1")) AndAlso Not String.IsNullOrEmpty(row.Item("KeyField1").ToString()) Then
                preKyFld1 = row.Item("KeyField1").ToString()
            End If
            'preKyFld1 = row.Item("KeyField1").ToString()
            If Not IsDBNull(row.Item("KeyField2")) AndAlso Not String.IsNullOrEmpty(row.Item("KeyField2").ToString()) Then
                preKyFld2 = row.Item("KeyField2").ToString()
            End If
            'preKyFld2 = row.Item("KeyField2").ToString()
            If Not IsDBNull(row.Item("KeyField3")) AndAlso Not String.IsNullOrEmpty(row.Item("KeyField3").ToString()) Then
                preKyFld3 = row.Item("KeyField3").ToString()
            End If
            ' preKyFld3 = row.Item("KeyField3").ToString()
            If Not IsDBNull(row.Item("KeyField4")) AndAlso Not String.IsNullOrEmpty(row.Item("KeyField4").ToString()) Then
                preKyFld4 = row.Item("KeyField4").ToString()
            End If
            'preKyFld4 = row.Item("KeyField4").ToString()
            If Not IsDBNull(row.Item("KeyField5")) AndAlso Not String.IsNullOrEmpty(row.Item("KeyField5").ToString()) Then
                preKyFld5 = row.Item("KeyField5").ToString()
            End If
            'preKyFld5 = row.Item("KeyField5").ToString()
            If Not IsDBNull(row.Item("KeyField6")) AndAlso Not String.IsNullOrEmpty(row.Item("KeyField6").ToString()) Then
                preKyFld6 = row.Item("KeyField6").ToString()
            End If
            'preKyFld6 = row.Item("KeyField6").ToString()
            If Not IsDBNull(row.Item("KeyField7")) AndAlso Not String.IsNullOrEmpty(row.Item("KeyField7").ToString()) Then
                preKyFld7 = row.Item("KeyField7").ToString()
            End If
            'preKyFld7 = row.Item("KeyField7").ToString()
            If Not IsDBNull(row.Item("KeyField8")) AndAlso Not String.IsNullOrEmpty(row.Item("KeyField8").ToString()) Then
                preKyFld8 = row.Item("KeyField8").ToString()
            End If
            'preKyFld8 = row.Item("KeyField8").ToString()
            If Not IsDBNull(row.Item("KeyField9")) AndAlso Not String.IsNullOrEmpty(row.Item("KeyField9").ToString()) Then
                preKyFld9 = row.Item("KeyField9").ToString()
            End If
            'preKyFld9 = row.Item("KeyField9").ToString()
            If Not IsDBNull(row.Item("KeyField10")) AndAlso Not String.IsNullOrEmpty(row.Item("KeyField10").ToString()) Then
                preKyFld10 = row.Item("KeyField10").ToString()
            End If
            'preKyFld10 = row.Item("KeyField10").ToString()
            If Not IsDBNull(row.Item("ShowKeyValue1")) AndAlso Not String.IsNullOrEmpty(row.Item("ShowKeyValue1").ToString()) Then
                prevShwKyVal = row.Item("ShowKeyValue1").ToString()
            End If
            'prevShwKyVal = row.Item("ShowKeyValue1").ToString()
            If Not IsDBNull(row.Item("ShowAlternateField")) AndAlso Not String.IsNullOrEmpty(row.Item("ShowAlternateField").ToString()) Then
                prevShwAltrntfld = row.Item("ShowAlternateField").ToString()
            End If
            'prevShwAltrntfld = row.Item("ShowAlternateField").ToString()
        Next

        If preKyFld2 = txtKeyField2.Text AndAlso preKyFld3 = txtKeyField3.Text AndAlso preKyFld4 = txtKeyField4.Text AndAlso preKyFld5 = txtKeyField5.Text AndAlso preKyFld6 = txtKeyField6.Text AndAlso preKyFld7 = txtKeyField7.Text AndAlso preKyFld8 = txtKeyField8.Text AndAlso preKyFld9 = txtKeyField9.Text AndAlso preKyFld10 = txtKeyField10.Text Then
            keyvalueTrue = True
        End If

        If txtParcelTable.Text = prePrclTbl AndAlso txtAlternateField.Text = preAltrKyFldVal AndAlso preKyFld1 = txtKeyField1.Text AndAlso prevShwKyVal = showKeyfield.ToString() AndAlso prevShwAltrntfld = showAlternateField.ToString() AndAlso keyvalueTrue Then
            Alert("No Changes Found !")
            Return False
        End If
        If (prevShwKyVal <> showKeyfield) Then
            If (prevShwKyVal = False) Then
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), "Enabled Key Field 1 in Application settings of Parcel ")
            Else
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), "Disabled Key Field 1 in Application settings of Parcel ")
            End If
        End If
        If (showAlternateField <> prevShwAltrntfld) Then
            If (prevShwAltrntfld = False) Then
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), "Enabled  Alternate Key Field in Application settings of Parcel ")
            Else
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), "Disabled Alternate Key Field in Application settings of Parcel ")
            End If
        End If
        If (txtParcelTable.Text <> prePrclTbl) Then
            parDesc = "Parcel Table changed from ' " + prePrclTbl + " ' to ' " + txtParcelTable.Text + " ' in Application Settings for Parcel"
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), parDesc)
        End If
        If (txtAlternateField.Text <> preAltrKyFldVal) Then
            parDesc = "Alternate Key Field changed from ' " + preAltrKyFldVal + " ' to ' " + txtAlternateField.Text + " ' in Application Settings for Parcel"
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), parDesc)
        End If
        If (preKyFld1 <> txtKeyField1.Text) Then
            parDesc = "Key Field1 changed from ' " + preKyFld1 + " ' to ' " + txtKeyField1.Text + " ' in Application Settings for Parcel"
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), parDesc)
        End If
        If (preKyFld2 <> txtKeyField2.Text) Then
            parDesc = "Key Field2 changed from ' " + preKyFld2 + " ' to ' " + txtKeyField2.Text + " ' in Application Settings for Parcel"
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), parDesc)
        End If
        If (preKyFld3 <> txtKeyField3.Text) Then
            parDesc = "Key Field3 changed from ' " + preKyFld3 + " ' to ' " + txtKeyField3.Text + " ' in Application Settings for Parcel"
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), parDesc)
        End If
        If (preKyFld4 <> txtKeyField4.Text) Then
            parDesc = "Key Field4 changed from ' " + preKyFld4 + " ' to ' " + txtKeyField4.Text + " ' in Application Settings for Parcel"
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), parDesc)
        End If
        If (preKyFld5 <> txtKeyField5.Text) Then
            parDesc = "Key Field5 changed from ' " + preKyFld5 + " ' to ' " + txtKeyField5.Text + " ' in Application Settings for Parcel"
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), parDesc)
        End If
        If (preKyFld6 <> txtKeyField6.Text) Then
            parDesc = "Key Field6 changed from ' " + preKyFld6 + "' to ' " + txtKeyField6.Text + " ' in Application Settings for Parcel"
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), parDesc)
        End If
        If (preKyFld7 <> txtKeyField7.Text) Then
            parDesc = "Key Field7 changed from ' " + preKyFld7 + " ' to ' " + txtKeyField7.Text + " ' in Application Settings for Parcel"
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), parDesc)
        End If
        If (preKyFld8 <> txtKeyField8.Text) Then
            parDesc = "Key Field8 changed from ' " + preKyFld8 + " ' to ' " + txtKeyField8.Text + " ' in Application Settings for Parcel"
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), parDesc)
        End If
        If (preKyFld9 <> txtKeyField9.Text) Then
            parDesc = "Key Field9 changed from ' " + preKyFld9 + " ' to ' " + txtKeyField9.Text + " ' in Application Settings for Parcel"
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), parDesc)
        End If
        If (preKyFld10 <> txtKeyField10.Text) Then
            parDesc = "Key Field10 changed from ' " + preKyFld10 + " ' to ' " + txtKeyField10.Text + " ' in Application Settings for Parcel"
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), parDesc)
        End If

        Database.Tenant.Execute("EXEC cc_UpdateApplication @ParcelTable = {0},@AlternateField ={1},@ShowKeyfield ={2},@ShowAlternate ={3}".SqlFormat(True, txtParcelTable.Text, txtAlternateField.Text, showKeyfield, showAlternateField))
        'Dim DtApp As DataTable = Database.Tenant.GetDataTable("SELECT * FROM Application")
        'If DtApp.Rows.Count() > 0 Then
        '    Database.Tenant.Execute("UPDATE  Application SET ParcelTable={0},alternatekeyfieldvalue ={1} ".SqlFormat(True, txtParcelTable.Text, txtAlternateField.Text))
        'Else
        '    Database.Tenant.Execute("INSERT INTO Application (ParcelTable,alternatekeyfieldvalue) VALUES ({0},{1})".SqlFormat(True, txtParcelTable.Text, txtAlternateField.Text))
        'End If
        'SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), "Application Settings for Parcel has been updated.")
        'Setting For StreetAddress
        UpdateKeyFields(KeyfieldNumber.Value)

        Return True
    End Function

    Private Sub refreshApplicationStreetSettings(streetFilter As String)
        Dim DtApp As DataTable = Database.Tenant.GetDataTable("SELECT * FROM Application")
        If DtApp.Rows.Count() > 0 Then
            Database.Tenant.Execute("UPDATE Application SET StreetNumberField={0},StreetNameField={1},StreetNameSuffixField={2},StreetAddressTable={3},StreetAddressField={4},StreetAddressFilter={5}, StreetUnitField={6} ".SqlFormatString(txtStreetNumberField.Text, txtStreetNameField.Text, txtStreetNameSuffixField.Text, txtStreetAddressTable.Text, txtStreetAddressField.Text, If(streetFilter, ""), txtStreetUnitField.Text))
        Else
            Database.Tenant.Execute("INSERT INTO Application (StreetNumberField,StreetNameField,StreetNameSuffixField,StreetAddressTable,StreetAddressField,StreetAddressFilter, StreetUnitField) VALUES ({0},{1},{2},{3},{4},{5}, {6})".SqlFormatString(txtStreetNumberField.Text, txtStreetNameField.Text, txtStreetNameSuffixField.Text, txtStreetAddressTable.Text, txtStreetAddressField.Text, If(streetFilter, ""), txtStreetUnitField.Text))
        End If
        'If DtApp.Rows.Count() > 0 Then
        'Database.Tenant.Execute("UPDATE Application SET StreetNumberField={0},StreetNameField={1},StreetNameSuffixField={2},StreetAddressTable={3},StreetAddressField={4},StreetAddressFilter={5}".SqlFormatString(txtStreetNumberField.Text, txtStreetNameField.Text, txtStreetNameSuffixField.Text, txtStreetAddressTable.Text, txtStreetAddressField.Text, If(streetFilter, "")))
        'Else
        'Database.Tenant.Execute("INSERT INTO Application (StreetNumberField,StreetNameField,StreetNameSuffixField,StreetAddressTable,StreetAddressField,StreetAddressFilter) VALUES ({0},{1},{2},{3},{4},{5})".SqlFormatString(txtStreetNumberField.Text, txtStreetNameField.Text, txtStreetNameSuffixField.Text, txtStreetAddressTable.Text, txtStreetAddressField.Text, If(streetFilter, "")))
        'End If
    End Sub

    Public Sub CreateOrUpdateNeighborhoodSettings()

        Dim nbrHoodTble = txtNeighborhoodTable.Text
        Dim nbrHoodFld = txtNeighborhoodField.Text
        Dim nbrHoodNameFld = txtNeighborhoodNameField.Text
        Dim DtApp As DataTable = Database.Tenant.GetDataTable("SELECT NeighborhoodTable,NeighborhoodField, NeighborhoodNameField, NeighborhoodNumberField FROM Application")
        Dim audtDes = ""
        Dim preNeighborhoodTable = ""
        Dim preNeighborhoodField = ""
        Dim preNeighborhoodNameField = ""
        For Each row As DataRow In DtApp.Rows
            preNeighborhoodTable = row.Item("NeighborhoodTable").ToString()
            preNeighborhoodField = row.Item("NeighborhoodField").ToString()
            preNeighborhoodNameField = row.Item("NeighborhoodNameField").ToString()
            ' Dim neighborhoodNumberField As String = row.Item("NeighborhoodNumberField").ToString()
        Next

        If DtApp.Rows.Count() > 0 Then
            Database.Tenant.Execute("UPDATE  Application SET NeighborhoodTable={0},NeighborhoodField={1},NeighborhoodNameField={2},NeighborhoodNumberField={2}".SqlFormatString(txtNeighborhoodTable.Text, txtNeighborhoodField.Text, txtNeighborhoodNameField.Text))
        Else
            Database.Tenant.Execute("INSERT INTO Application (NeighborhoodTable,NeighborhoodField,NeighborhoodNameField,NeighborhoodNumberField) VALUES ({0},{1},{2},{2})".SqlFormatString(txtNeighborhoodTable.Text, txtNeighborhoodField.Text, txtNeighborhoodNameField.Text))
        End If
        If (nbrHoodTble <> preNeighborhoodTable) Then
            audtDes = "Assignment Group Table changed from '" + preNeighborhoodTable + "' to '" + nbrHoodTble + "' in Assignment Group"
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), audtDes)
        End If
        If (nbrHoodFld <> preNeighborhoodField) Then
            audtDes = "Assignment Group Field changed from '" + preNeighborhoodField + "' to '" + nbrHoodFld + "' in Assignment Group"
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), audtDes)
        End If
        If (nbrHoodNameFld <> preNeighborhoodNameField) Then
            audtDes = "Assignment Group Name Field changed from '" + preNeighborhoodNameField + "' to '" + nbrHoodNameFld + "' in Assignment Group"
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), audtDes)
        End If
        'SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), "Application Settings for Neighbourhood has been updated.")

    End Sub

    Public Sub CreateUpdateSketchSettings()
        Dim skConfig = txtSketchConfig.Text
        Dim skDecml = drpDecimal.SelectedItem.Text
        Dim disSqft = drpAreaDecimal.SelectedItem.Text
        Dim roundToDecml = drpDimension.SelectedItem.Text
        Dim preskConfig = ""
        Dim preSkDecml = ""
        Dim preDisSqft = ""
        Dim preRoundToDecml = ""
        Dim prevValskConfig As DataRow = Database.Tenant.GetTopRow("SELECT Value FROM SketchSettings WHERE Name ='SketchConfig'")
        If prevValskConfig IsNot Nothing Then
            preskConfig = prevValskConfig.GetString("Value")
        End If
        Dim prevDrDecimal As DataRow = Database.Tenant.GetTopRow("SELECT Value FROM SketchSettings WHERE Name ='SketchDecimals'")
        If prevDrDecimal IsNot Nothing Then
            preSkDecml = prevDrDecimal.GetString("Value")
        End If
        Dim prevAreaDec As DataRow = Database.Tenant.GetTopRow("SELECT Value FROM SketchSettings WHERE Name ='DisplaySQFTOnUI'")
        If prevAreaDec IsNot Nothing Then
            preDisSqft = prevAreaDec.GetString("Value")
        End If
        Dim prevdrDimension As DataRow = Database.Tenant.GetTopRow("SELECT Value FROM SketchSettings WHERE Name ='RoundToDecimalPlace'")
        If prevdrDimension IsNot Nothing Then
            preRoundToDecml = prevdrDimension.GetString("Value")
        End If

        Dim adtDescr = ""

        If (skConfig <> preskConfig) Then
            adtDescr = "Sketch Configuration changed from ' " + preskConfig + " ' to ' " + skConfig + " '"
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), adtDescr)
        End If
        If (skDecml <> preSkDecml) Then
            adtDescr = "Sketch Decimals changed from ' " + preSkDecml + " ' to  ' " + skDecml + " '"
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), adtDescr)
        End If
        If (disSqft <> preDisSqft) Then
            adtDescr = "Sketch Decimals changed from ' " + preDisSqft + " ' to ' " + disSqft + " '"
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), adtDescr)
        End If
        If (roundToDecml <> preRoundToDecml) Then
            adtDescr = "Sketch Decimals changed from ' " + preRoundToDecml + " ' to ' " + roundToDecml + " '"
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), adtDescr)
        End If
        ClientSettUpdate("SketchConfig", txtSketchConfig.Text)
        SketchSettUpdate("SketchConfig", txtSketchConfig.Text)

        '''Sketch Label
        'ClientSettUpdate("SketchLabel", txtSketchLabel.Text)

        '''Perimeter
        'ClientSettUpdate("SketchPerimeter", txtSketchPerimeter.Text)

        '''Sketch Area Field
        'ClientSettUpdate("SketchAreaField", txtAreaField.Text)

        '''Sketch Area Field
        'ClientSettUpdate("SketchKeyField", txtScketchKeyField.Text)

        '''scketch command Field
        'ClientSettUpdate("SketchCommandField", txtsketchcommand.Text)

        '''Sketch Table
        'ClientSettUpdate("SketchTable", txtSketchTable.Text)

        '''Sketch Vector Table
        'ClientSettUpdate("SketchVectorTable", txtVectorTable.Text)

        '''Sketch Vector Lable Field
        'ClientSettUpdate("SketchVectorLabelField", txtVectorLabelField.Text)

        '''Sketch Vector Command Field
        'ClientSettUpdate("SketchVectorCommandField", txtVectorCommand.Text)

        '''Sketch Vector Connecting Field
        'ClientSettUpdate("SketchVectorConnectingField", txtVectorConnect.Text)

        ''Sketch Decimal Field
        ClientSettUpdate("SketchDecimals", drpDecimal.SelectedItem.Text)
        SketchSettUpdate("SketchDecimals", drpDecimal.SelectedItem.Text)
        SketchSettUpdate("DisplaySQFTOnUI", drpAreaDecimal.SelectedItem.Text)
        SketchSettUpdate("RoundToDecimalPlace", drpDimension.SelectedItem.Text)
        'Dim tempold As DataRow = Database.System.GetTopRow("SELECT Name FROM GlobalFieldInputTypes WHERE ID = " & oldValue)

        '  SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), adtDescr)




    End Sub

    Public Function CreateUpdateStreetAddressSettings(Optional recordUpdate As Boolean = False) As Boolean
        Dim prevStrtAddrsTbleStrng = ""
        Dim prevStrtAddrsFldStrng = ""
        Dim prevStrtAddrsFrmlaStrng = ""
        Dim prevStrtNmbrFldStrng = ""
        Dim prevStrtNameFldStrng = ""
        Dim prevStrtNameSfxFldStrng = ""
        Dim prevStrtUnitFldStrng = ""

        Dim prevStrtAddrsTble As DataRow = Database.Tenant.GetTopRow("SELECT Value FROM ClientSettings WHERE Name ='StreetAddressTable'")
        If prevStrtAddrsTble IsNot Nothing Then
            prevStrtAddrsTbleStrng = prevStrtAddrsTble.GetString("Value")
        End If
        Dim prevStrtAddrsFld As DataRow = Database.Tenant.GetTopRow("SELECT Value FROM ClientSettings WHERE Name ='StreetAddressField'")
        If prevStrtAddrsFld IsNot Nothing Then
            prevStrtAddrsFldStrng = prevStrtAddrsFld.GetString("Value")
        End If
        Dim prevStrtAddrsFrmla As DataRow = Database.Tenant.GetTopRow("SELECT Value FROM ClientSettings WHERE Name ='StreetAddressFormula'")
        If prevStrtAddrsFrmla IsNot Nothing Then
            prevStrtAddrsFrmlaStrng = prevStrtAddrsFrmla.GetString("Value")
        End If

        Dim prevStrtNmbrFld As DataRow = Database.Tenant.GetTopRow("SELECT Value FROM ClientSettings WHERE Name ='StreetNumberField'")
        If prevStrtNmbrFld IsNot Nothing Then
            prevStrtNmbrFldStrng = prevStrtNmbrFld.GetString("Value")
        End If

        Dim prevStrtNameFld As DataRow = Database.Tenant.GetTopRow("SELECT Value FROM ClientSettings WHERE Name ='StreetNameField'")
        If prevStrtNameFld IsNot Nothing Then
            prevStrtNameFldStrng = prevStrtNameFld.GetString("Value")
        End If

        Dim prevStrtNameSfxFld As DataRow = Database.Tenant.GetTopRow("SELECT Value FROM ClientSettings WHERE Name ='StreetNameSuffixField'")
        If prevStrtNameSfxFld IsNot Nothing Then
            prevStrtNameSfxFldStrng = prevStrtNameSfxFld.GetString("Value")
        End If

        Dim prevStrtUnitfxFld As DataRow = Database.Tenant.GetTopRow("SELECT Value FROM ClientSettings WHERE Name ='StreetUnitField'")
        If prevStrtUnitfxFld IsNot Nothing Then
            prevStrtUnitFldStrng = prevStrtUnitfxFld.GetString("Value")
        End If

        Dim prevStrtAddressFilter = Database.Tenant.GetStringValue("SELECT StreetAddressFilter FROM Application")
        Dim strtDescr = ""
        Dim streetFilter = ""
        Dim prevColName = ""
        Dim prevFltrCondtn = ""
        Dim prevfltrval = ""
        Dim prevFltrCondtnWrd = ""
        If Not String.IsNullOrEmpty(prevStrtAddressFilter) Then

            Dim fltrParts() As String = prevStrtAddressFilter.Split(" ")
            prevColName = fltrParts(0)
            prevFltrCondtn = fltrParts(1)
            If (prevFltrCondtn = ">") Then
                prevFltrCondtnWrd = "Greater Than"
            End If
            If (prevFltrCondtn = "<") Then
                prevFltrCondtnWrd = "Less Than"
            End If
            If (prevFltrCondtn = "=") Then
                prevFltrCondtnWrd = "Equal To"
            End If
            prevfltrval = fltrParts(2)
        End If

        If Not recordUpdate AndAlso prevStrtAddrsTbleStrng = txtStreetAddressTable.Text AndAlso prevStrtAddrsFldStrng = txtStreetAddressField.Text AndAlso prevStrtNmbrFldStrng = txtStreetNumberField.Text AndAlso prevStrtNameFldStrng = txtStreetNameField.Text AndAlso prevStrtNameSfxFldStrng = txtStreetNameSuffixField.Text AndAlso prevStrtUnitFldStrng = txtStreetUnitField.Text AndAlso prevColName = txtFilterColoumnName.Text AndAlso prevfltrval = txtFilterValue.Text AndAlso ((prevFltrCondtn <> "" AndAlso prevFltrCondtn = ddlFilterCondition.SelectedValue.ToString) Or (prevFltrCondtn = "" AndAlso txtFilterColoumnName.Text = "")) Then
            '    Alert("No changes found!")
            '    Return False
            'End If

            'If Not recordUpdate AndAlso prevStrtAddrsTbleStrng = txtStreetAddressTable.Text AndAlso prevStrtAddrsFldStrng = txtStreetAddressField.Text AndAlso prevStrtNmbrFldStrng = txtStreetNumberField.Text AndAlso prevStrtNameFldStrng = txtStreetNameField.Text AndAlso prevStrtNameSfxFldStrng = txtStreetNameSuffixField.Text AndAlso prevColName = txtFilterColoumnName.Text AndAlso prevfltrval = txtFilterValue.Text AndAlso ((prevFltrCondtn <> "" AndAlso prevFltrCondtn = ddlFilterCondition.SelectedValue.ToString) Or (prevFltrCondtn = "" AndAlso txtFilterColoumnName.Text = "")) Then
            Alert("No changes found!")
            Return False
        End If

        If (prevColName <> txtFilterColoumnName.Text) Then
            strtDescr = "Filter Column Name Changed from ' " + prevColName + " ' to ' " + txtFilterColoumnName.Text + " ' in Application Settings for Street Address."
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), strtDescr)
            If (prevfltrval <> txtFilterValue.Text) Then
                strtDescr = "filter value changed from ' " + prevfltrval + " ' to ' " + txtFilterValue.Text + " ' in application settings for street address."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), strtDescr)
            End If

            If ((prevFltrCondtnWrd <> "" AndAlso prevFltrCondtnWrd <> ddlFilterCondition.SelectedItem.Text) Or (prevFltrCondtnWrd = "")) Then
                strtDescr = "filter condition  changed from ' " + prevFltrCondtnWrd + " ' to ' " + ddlFilterCondition.SelectedItem.Text + " ' in application settings for street address."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), strtDescr)
            End If
        End If

        If (prevStrtAddrsTbleStrng <> txtStreetAddressTable.Text) Then
            strtDescr = "Street Adress Table value Changed from ' " + prevStrtAddrsTbleStrng + " ' to ' " + txtStreetAddressTable.Text + " ' in Application Settings for Street Address."
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), strtDescr)
        End If
        If (prevStrtAddrsFldStrng <> txtStreetAddressField.Text) Then
            strtDescr = "Street Adress Field value Changed from ' " + prevStrtAddrsFldStrng + " ' to ' " + txtStreetAddressField.Text + " ' in Application Settings for Street Address."
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), strtDescr)
        End If
        If (prevStrtNmbrFldStrng <> txtStreetNumberField.Text) Then
            strtDescr = "Street number field Changed from ' " + prevStrtNmbrFldStrng + " ' to ' " + txtStreetNumberField.Text + " ' in Application Settings for Street Address."
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), strtDescr)
        End If
        If (prevStrtNameFldStrng <> txtStreetNameField.Text) Then
            strtDescr = "Street name field value  changed from  ' " + prevStrtNameFldStrng + " ' to ' " + txtStreetNameField.Text + " 'in Application Settings for Street Address."
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), strtDescr)
        End If
        If (prevStrtNameSfxFldStrng <> txtStreetNameSuffixField.Text) Then
            strtDescr = "Street name suffix field value  changed from  ' " + prevStrtNameSfxFldStrng + " ' to ' " + txtStreetNameSuffixField.Text + " ' in Application Settings for Street Address."
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), strtDescr)
        End If

        If (prevStrtUnitFldStrng <> txtStreetUnitField.Text) Then
            strtDescr = "Street unit field value  changed from  ' " + prevStrtUnitFldStrng + " ' to ' " + txtStreetUnitField.Text + " ' in Application Settings for Street Address."
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), strtDescr)
        End If

        'Street Address Table
        ClientSettUpdate("StreetAddressTable", txtStreetAddressTable.Text)

        'Street Address Table
        ClientSettUpdate("StreetAddressField", txtStreetAddressField.Text)

        'Street Address Filter

        If txtFilterColoumnName.Text <> "" Then
            streetFilter = txtFilterColoumnName.Text + " " + ddlFilterCondition.SelectedValue.ToString + " " + txtFilterValue.Text
        End If

        ClientSettUpdate("StreetAddressFormula", streetFilter)

        ''Street Number Field
        ClientSettUpdate("StreetNumberField", txtStreetNumberField.Text)

        ''Street Name Field
        ClientSettUpdate("StreetNameField", txtStreetNameField.Text)

        ''Street Name Suffix Field
        ClientSettUpdate("StreetNameSuffixField", txtStreetNameSuffixField.Text)

        ClientSettUpdate("StreetUnitField", txtStreetUnitField.Text)

        refreshApplicationStreetSettings(streetFilter)
        'SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), "Application Settings for Street Address has been updated.")

        Return True
    End Function


    Private Function GetKeyFields() As String()
        Dim KeyNo As Integer = Convert.ToInt32(KeyfieldNumber.Value)
        Dim KeyValue(KeyNo) As String
        Dim iter As Integer = 0
        While (iter < KeyNo)
            iter += 1
            Dim parent As Control = txtKeyField1.NamingContainer
            Dim textName As String = "txtKeyField" + iter.ToString
            KeyValue(iter) = DirectCast(parent.FindControl(textName), TextBox).Text
        End While
        Return KeyValue
    End Function

    Private Sub SetKeyFields(dr As DataRow)
        Dim keycount As Integer = 0
        For i = 1 To 10
            If ((dr.GetString("KeyField" + i.ToString()) <> "") AndAlso (dr.GetString("KeyField" + i.ToString()) IsNot Nothing)) Then
                keycount += 1
                Dim parent As Control = txtKeyField2.NamingContainer
                Dim textName As String = "txtKeyField" + i.ToString
                DirectCast(parent.FindControl(textName), TextBox).Text = dr.GetString("KeyField" + i.ToString())
            End If
        Next
        KeyfieldNumber.SelectedIndex = KeyfieldNumber.Items.IndexOf(KeyfieldNumber.Items.FindByValue("0" + keycount.ToString()))
    End Sub

    Public Function visibleFieldTracking()
        Dim ft = Database.Tenant.GetStringValue("select value from clientsettings where name='EnableFieldTracking'")
        If (ft = "1" Or ft = "true") Then
            Return "'padding:1px'"
        Else
            Return "'display:none'"
        End If
    End Function

    Public Function visibleBpp()
        Dim ft As DataRow = Database.System.GetTopRow("SELECT os.BPPEnabled FROM Organization o INNER JOIN OrganizationSettings os ON o.id=os.OrganizationId WHERE o.Id = " & HttpContext.Current.GetCAMASession.OrganizationId)
        If (ft.GetString("BPPEnabled") = "1" Or ft.GetString("BPPEnabled") = "true" Or ft.GetString("BPPEnabled") = "True") Then
            Return "'padding:1px'"
        Else
            Return "'display:none'"
        End If
    End Function

    Private Sub FillChanges()
        LoadBulkApprovalSettings()
        Dim DrParcel As DataRow = Database.Tenant.GetTopRow("SELECT * FROM Application")
        If DrParcel Is Nothing Then
            txtParcelTable.Text = ""
            txtKeyField1.Text = ""
            txtNeighborhoodTable.Text = ""
            txtNeighborhoodField.Text = ""
            txtNeighborhoodNameField.Text = ""
            txtAlternateField.Text = ""
            'txt_bppCondition.Text = ""
            'txt_BppPPF.Text = ""
        Else
            txtParcelTable.Text = GetString(DrParcel, "ParcelTable")
            SetKeyFields(DrParcel)
            txtNeighborhoodTable.Text = GetString(DrParcel, "NeighborhoodTable")
            txtNeighborhoodField.Text = GetString(DrParcel, "NeighborhoodField")
            txtNeighborhoodNameField.Text = GetString(DrParcel, "NeighborhoodNameField")
            txt_bppCondition.Text = GetString(DrParcel, "BPPCondition")
            txt_BppPPF.Text = GetString(DrParcel, "BPPParentPropertyField")
            txt_rpCondition.Text = GetString(DrParcel, "RPCondition")
            txtAlternateField.Text = GetString(DrParcel, "Alternatekeyfieldvalue")
            chkShowKeyfield.Checked = GetBoolean(DrParcel, "ShowKeyValue1")
            chkShowAlternate.Checked = GetBoolean(DrParcel, "ShowAlternateField")
        End If

        'Dim bppVal As DataTable = Database.Tenant.GetDataTable("Select * from ClientSettings Where Name in ('BPPRealDataReadOnly','BPPDownloadType')")
        'Dim bppValcount As Integer = bppVal.Rows.Count
        'If bppValcount > 0 Then
        'For i = 0 To bppValcount - 1
        'Dim name As String = bppVal.Rows(i)("Name").ToString()
        'Dim value As String = bppVal.Rows(i)("Value").ToString()
        'If (name = "BPPRealDataReadOnly") Then
        'chk_bppRDR.Checked = Convert.ToInt32(value)
        'ElseIf (name = "BPPDownloadType") Then
        'ddl_BppPD.SelectedValue = value                
        'End If
        'Next
        'End If

        'Setting For StreetAddress
        ddl_table.FillFromSql("SELECT Id, Name FROM DataSourceTable where importType=0", True)

        Dim ft = Database.Tenant.GetStringValue("select value from clientsettings where name='EnableFieldTracking'")

        If ft IsNot Nothing AndAlso (ft = "1" Or ft.ToLower = "true") Then
            Dim tb As DataRow = Database.Tenant.GetTopRow("select top 1 Id,TableID,TrackingEnabledValue from datasourcefield where TrackingEnabled = 1")
            If Not (tb Is Nothing) Then
                ddl_table.SelectedValue = GetString(tb, "TableID")
                ddl_field.FillFromSql("SELECT Id, Name FROM DataSourceField WHERE TableId = " & ddl_table.SelectedValue & " ORDER BY Name", True)
                ddl_field.SelectedValue = GetString(tb, "Id")
                '  txt_tracking.Text = GetString(tb, "TrackingEnabledValue")
            End If
        End If
        If (DrParcel.GetString("BPPEnabled") = "1" Or DrParcel.GetString("BPPEnabled") = "True" Or DrParcel.GetString("BPPEnabled") = "true") Then
            chk_bpp.Checked = True
        Else
            chk_bpp.Checked = False
        End If
        RunScript("BPPChecked();")
        ''Street Address Table
        UpdateClientSettingFields("StreetAddressTable", txtStreetAddressTable)

        ''Street Address Table
        UpdateClientSettingFields("StreetAddressField", txtStreetAddressField)

        ''Street Address Filter
        setStreetAddressFilter()

        '''Sketch Label
        'UpdateClientSettingFields("SketchLabel", txtSketchLabel)

        '''Perimeter
        'UpdateClientSettingFields("SketchPerimeter", txtSketchPerimeter)

        '''Sketch Area Field
        'UpdateClientSettingFields("SketchAreaField", txtAreaField)

        '''Sketch Key Field
        'UpdateClientSettingFields("SketchKeyField", txtScketchKeyField)

        '''scketch command Field
        'UpdateClientSettingFields("SketchCommandField", txtsketchcommand)

        '''Sketch Table
        'UpdateClientSettingFields("SketchTable", txtSketchTable)

        '''Sketch Vector Table
        'UpdateClientSettingFields("SketchVectorTable", txtVectorTable)

        '''Sketch Vector Lable Field
        'UpdateClientSettingFields("SketchVectorLabelField", txtVectorLabelField)

        '''Sketch Vector Command Field
        'UpdateClientSettingFields("SketchVectorCommandField", txtVectorCommand)

        '''Sketch Vector Connecting Field
        'UpdateClientSettingFields("SketchVectorConnectingField", txtVectorConnect)

        UpdateClientSettingFields("SketchConfig", txtSketchConfig)
        UpdateClientSettingFieldDropDown("SketchDecimals", drpDecimal)
        UpdateClientSettingFieldDropDown("DisplaySQFTOnUI", drpAreaDecimal)
        UpdateClientSettingFieldDropDown("RoundToDecimalPlace", drpDimension)

        ''Sketch Field Id
        'UpdateClientSettingFields("SketchFieldId", ddlFieldId)

        ' Street Number Field
        txtStreetNumberField.Text = StreetAddress.GetStreetNumberField(Database.Tenant)

        'StreetNameField
        txtStreetNameField.Text = StreetAddress.GetStreetNameField(Database.Tenant)

        'StreetNameSuffixField
        txtStreetNameSuffixField.Text = StreetAddress.GetStreetNameSuffixField(Database.Tenant)

        txtStreetUnitField.Text = StreetAddress.GetStreetUnitField(Database.Tenant)

        Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT * FROM ClientSettings WHERE Name IN ('MACStartMessage', 'MACEndMessage', 'MACStartMessageColor', 'MACEndMessageColor')")
        Dim count As Integer = dt.Rows.Count
        If count > 0 Then
            For i = 0 To count - 1
                Dim name As String = dt.Rows(i)("Name").ToString()
                Dim value As String = dt.Rows(i)("Value").ToString()
                If (name = "MACStartMessage") Then
                    txtStart.Text = value
                ElseIf (name = "MACEndMessage") Then
                    txtEnd.Text = value
                ElseIf (name = "MACStartMessageColor") Then
                    ddlStartColor.SelectedValue = value
                ElseIf (name = "MACEndMessageColor") Then
                    ddlEndColor.SelectedValue = value
                End If
            Next
        ElseIf count = 0 Then
            Database.Tenant.Execute("INSERT INTO ClientSettings VALUES ('MACStartMessage',NULL); INSERT INTO ClientSettings VALUES ('MACStartMessageColor',NULL); INSERT INTO ClientSettings VALUES ('MACEndMessage',NULL); INSERT INTO ClientSettings VALUES ('MACEndMessageColor',NULL);")
        End If

    End Sub

    Private Sub ClientSettUpdate(Name As String, Value As String)
        Dim DtStreetAdressTab As DataTable = Database.Tenant.GetDataTable("SELECT Value FROM ClientSettings WHERE Name={0}".SqlFormatString(Name))
        If DtStreetAdressTab.Rows.Count > 0 Then
            Database.Tenant.Execute("UPDATE ClientSettings SET Value={0} WHERE Name={1} ".SqlFormatString(Value, Name))
        Else
            Database.Tenant.Execute("INSERT INTO ClientSettings (Value,Name) VALUES({0},{1}) ".SqlFormatString(Value, Name))
        End If
    End Sub

    Private Sub ClientSettDelete(Field As String)
        Database.Tenant.Execute("DELETE ClientSettings WHERE Name={0} ".SqlFormatString(Field))
    End Sub

    Private Sub SketchSettUpdate(Name As String, Value As String)
        Dim DtStreetAdressTab As DataTable = Database.Tenant.GetDataTable("SELECT Value FROM SketchSettings WHERE Name={0}".SqlFormatString(Name))
        If DtStreetAdressTab.Rows.Count > 0 Then
            Database.Tenant.Execute("UPDATE SketchSettings SET Value={0} WHERE Name={1} ".SqlFormatString(Value, Name))
        Else
            Database.Tenant.Execute("INSERT INTO SketchSettings (Value,Name) VALUES({0},{1}) ".SqlFormatString(Value, Name))
        End If
    End Sub

    Private Sub UpdateClientSettingFields(Field As String, Txt As TextBox)
        Dim DrClientSettings As DataRow = Database.Tenant.GetTopRow("SELECT Value FROM ClientSettings WHERE Name={0}".SqlFormatString(Field))
        If Field = "SketchConfig" AndAlso DrClientSettings Is Nothing Then
            DrClientSettings = Database.Tenant.GetTopRow("SELECT Value FROM SketchSettings WHERE Name={0}".SqlFormatString(Field))
        End If
        If DrClientSettings Is Nothing Then
            Txt.Text = ""
        Else
            Txt.Text = GetString(DrClientSettings, "Value")

        End If
    End Sub
    Private Sub UpdateClientSettingFieldDropDown(Field As String, Drp As DropDownList)
        Dim DrClientSettings As DataRow = Database.Tenant.GetTopRow("SELECT Value FROM ClientSettings WHERE Name={0}".SqlFormatString(Field))
        If (Field = "DisplaySQFTOnUI" Or Field = "RoundToDecimalPlace" Or Field = "SketchDecimals") AndAlso DrClientSettings Is Nothing Then
            DrClientSettings = Database.Tenant.GetTopRow("SELECT Value FROM SketchSettings WHERE Name={0}".SqlFormatString(Field))
        End If
        If DrClientSettings Is Nothing Then
            Drp.SelectedIndex = 0
        Else
            Drp.SelectedValue = GetString(DrClientSettings, "Value")

        End If
    End Sub
    Private Sub UpdateClientSettingFields(Field As String, ddl As DropDownList)
        Dim DrClientSettings As DataRow = Database.Tenant.GetTopRow("SELECT Value FROM ClientSettings WHERE Name={0}".SqlFormatString(Field))
        If DrClientSettings Is Nothing Then
            'Set top Row
        Else
            ddl.SelectedValue = GetString(DrClientSettings, "Value")

        End If
    End Sub

    Private Sub setStreetAddressFilter()
        Dim Value As String = "", LeftValue As String = "", RightValue As String = "", Condition As String = ""
        Dim DrClientSettings As DataRow = Database.Tenant.GetTopRow("SELECT Value FROM ClientSettings WHERE Name={0}".SqlFormatString("StreetAddressFormula"))
        If DrClientSettings Is Nothing Then
            txtFilterColoumnName.Text = ""
            ddlFilterCondition.SelectedValue = "="
            txtFilterValue.Text = ""
        Else
            Value = GetString(DrClientSettings, "Value")
        End If

        Dim ConditionChars As Char() = New Char() {"=", ">", "<"}
        If Value <> "" Then
            For Each Chr As Char In ConditionChars
                If (Value.Contains(Chr.ToString())) Then
                    Dim filter = Value.Split(New Char() {Chr}, 2)
                    ddlFilterCondition.SelectedValue = Chr.ToString()
                    txtFilterColoumnName.Text = filter(0).Trim()
                    txtFilterValue.Text = If(filter(1).Trim(), "")
                    Exit For
                End If
            Next
        End If
    End Sub

    Protected Sub lbExport_Click(sender As Object, e As EventArgs) Handles lbExport.Click
        Dim exportName = HttpContext.Current.GetCAMASession.OrganizationCodeName + "-allsettings-" + Now.ToString("yyMM-dd-HHmmss") + ".xml"
        Response.WriteAsAttachment(ExportAsXML, exportName)
    End Sub
    Private Function ExportAsXML() As String
        Dim gen = New EnvironmentConfiguration
        Dim doc = gen.ExportSettings(ConfigFileType.ApplicationSettings, Database.Tenant)
        Return doc.ToString(SaveOptions.None)
    End Function
    Protected Sub lbImport_Click(sender As Object, e As System.EventArgs) Handles btnUploadImportFile.Click
        If fuImportFile.HasFile Then
            Try
                Dim gen = New EnvironmentConfiguration
                gen.ImportSettings(fuImportFile.FileContent, ConfigFileType.ApplicationSettings, Database.Tenant, Membership.GetUser().ToString, "CAMAIntegration")
                NotificationMailer.SendNotificationToVendorCDS(Membership.GetUser().ToString, 1, "Application Settings")
                FillChanges()
                Dim Description As String = "Application Settings has been updated."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
                Alert("Imported successfully")
            Catch ex As Exception
                Alert(ex.Message)
                Return
            End Try
        End If
    End Sub

    Private Sub saveBulkApprovalSettings()
        Dim btchSize = txtBatchSize.Text
        Dim timeIntrvl = txtTimeInterval.Text
        Dim audtDesc = ""
        Dim prevbtchSize = ""
        Dim prevtimeIntrvl = ""
        'Dim DtApp As DataTable = Database.Tenant.GetDataTable("SELECT TimeInterval,BatchSize FROM ClientSettings")
        Dim preBatchSz As DataRow = Database.Tenant.GetTopRow("SELECT Value FROM ClientSettings WHERE Name ='BatchSize'")
        If preBatchSz IsNot Nothing Then
            prevbtchSize = preBatchSz.GetString("Value")
        End If

        Dim preTimeIntrvl As DataRow = Database.Tenant.GetTopRow("SELECT Value FROM ClientSettings WHERE Name ='TimeInterval'")
        If preTimeIntrvl IsNot Nothing Then
            prevtimeIntrvl = preTimeIntrvl.GetString("Value")
        End If

        If (btchSize <> prevbtchSize) Then
            audtDesc = "Batch size changed from ' " + prevbtchSize + " ' to ' " + btchSize + " ' in Application Settings for Bulk Approval"
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), audtDesc)
        End If
        If (timeIntrvl <> prevtimeIntrvl) Then
            audtDesc = "Time Interval changed from ' " + prevtimeIntrvl + " ' to ' " + timeIntrvl + " ' in Application Settings for Bulk Approval"
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), audtDesc)
        End If
        ClientSettUpdate("BatchSize", txtBatchSize.Text)
        ClientSettUpdate("TimeInterval", txtTimeInterval.Text)
        'SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), "Application Settings for Bulk Approval has been updated.")
    End Sub

    Private Sub LoadBulkApprovalSettings()
        UpdateClientSettingFields("BatchSize", txtBatchSize)
        UpdateClientSettingFields("TimeInterval", txtTimeInterval)
    End Sub

    Private Sub UpdateKeyFields(totalKeyFields As Integer)
        Try
            Select Case totalKeyFields
                Case 1
                    Database.Tenant.Execute("UPDATE  Application SET KeyField1={0}, KeyField2={1}, KeyField3={2}, KeyField4={3}, KeyField5={4}, KeyField6={5}, KeyField7={6}, KeyField8={7}, KeyField9={8}, KeyField10={9}".SqlFormat(True, txtKeyField1, "", "", "", "", "", "", "", "", ""))
                Case 2
                    Database.Tenant.Execute("UPDATE  Application SET KeyField1={0}, KeyField2={1}, KeyField3={2}, KeyField4={3}, KeyField5={4}, KeyField6={5}, KeyField7={6}, KeyField8={7}, KeyField9={8}, KeyField10={9}".SqlFormat(True, txtKeyField1, txtKeyField2, "", "", "", "", "", "", "", ""))
                Case 3
                    Database.Tenant.Execute("UPDATE  Application SET KeyField1={0}, KeyField2={1}, KeyField3={2}, KeyField4={3}, KeyField5={4}, KeyField6={5}, KeyField7={6}, KeyField8={7}, KeyField9={8}, KeyField10={9}".SqlFormat(True, txtKeyField1, txtKeyField2, txtKeyField3, "", "", "", "", "", "", ""))
                Case 4
                    Database.Tenant.Execute("UPDATE  Application SET KeyField1={0}, KeyField2={1}, KeyField3={2}, KeyField4={3}, KeyField5={4}, KeyField6={5}, KeyField7={6}, KeyField8={7}, KeyField9={8}, KeyField10={9}".SqlFormat(True, txtKeyField1, txtKeyField2, txtKeyField3, txtKeyField4, "", "", "", "", "", ""))
                Case 5
                    Database.Tenant.Execute("UPDATE  Application SET KeyField1={0}, KeyField2={1}, KeyField3={2}, KeyField4={3}, KeyField5={4}, KeyField6={5}, KeyField7={6}, KeyField8={7}, KeyField9={8}, KeyField10={9}".SqlFormat(True, txtKeyField1, txtKeyField2, txtKeyField3, txtKeyField4, txtKeyField5, "", "", "", "", ""))
                Case 6
                    Database.Tenant.Execute("UPDATE  Application SET KeyField1={0}, KeyField2={1}, KeyField3={2}, KeyField4={3}, KeyField5={4}, KeyField6={5}, KeyField7={6}, KeyField8={7}, KeyField9={8}, KeyField10={9}".SqlFormat(True, txtKeyField1, txtKeyField2, txtKeyField3, txtKeyField4, txtKeyField5, txtKeyField6, "", "", "", ""))
                Case 7
                    Database.Tenant.Execute("UPDATE  Application SET KeyField1={0}, KeyField2={1}, KeyField3={2}, KeyField4={3}, KeyField5={4}, KeyField6={5}, KeyField7={6}, KeyField8={7}, KeyField9={8}, KeyField10={9}".SqlFormat(True, txtKeyField1, txtKeyField2, txtKeyField3, txtKeyField4, txtKeyField5, txtKeyField6, txtKeyField7, "", "", ""))
                Case 8
                    Database.Tenant.Execute("UPDATE  Application SET KeyField1={0}, KeyField2={1}, KeyField3={2}, KeyField4={3}, KeyField5={4}, KeyField6={5}, KeyField7={6}, KeyField8={7}, KeyField9={8}, KeyField10={9}".SqlFormat(True, txtKeyField1, txtKeyField2, txtKeyField3, txtKeyField4, txtKeyField5, txtKeyField6, txtKeyField7, txtKeyField8, "", ""))
                Case 9
                    Database.Tenant.Execute("UPDATE  Application SET KeyField1={0}, KeyField2={1}, KeyField3={2}, KeyField4={3}, KeyField5={4}, KeyField6={5}, KeyField7={6}, KeyField8={7}, KeyField9={8}, KeyField10={9}".SqlFormat(True, txtKeyField1, txtKeyField2, txtKeyField3, txtKeyField4, txtKeyField5, txtKeyField6, txtKeyField7, txtKeyField8, txtKeyField9, ""))
                Case 10
                    Database.Tenant.Execute("UPDATE  Application SET KeyField1={0}, KeyField2={1}, KeyField3={2}, KeyField4={3}, KeyField5={4}, KeyField6={5}, KeyField7={6}, KeyField8={7}, KeyField9={8}, KeyField10={9}".SqlFormatString(txtKeyField1.Text, txtKeyField2.Text, txtKeyField3.Text, txtKeyField4.Text, txtKeyField5.Text, txtKeyField6.Text, txtKeyField7.Text, txtKeyField8.Text, txtKeyField9.Text, txtKeyField10.Text))
            End Select
        Catch ex As Exception
        End Try
    End Sub



    Public Sub MACMessageSetup()

        Dim sql As String = ""
        Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT Name,Value FROM ClientSettings WHERE Name in ('MACStartMessage','MACEndMessage','MACStartMessageColor','MACEndMessageColor')")
        Dim count As Integer = dt.Rows.Count
        If count > 0 Then
            Dim drstart As DataRow() = dt.Select("Name= 'MACStartMessage'")
            Dim drend As DataRow() = dt.Select("Name= 'MACEndMessage'")
            Dim drstartcolor As DataRow() = dt.Select("Name= 'MACStartMessageColor'")
            Dim drendcolor As DataRow() = dt.Select("Name= 'MACEndMessageColor'")

            If drstart.Length <> 0 Then
                Dim MacStartClientSettings As String
                If drstart(0)("Value") Is Nothing Or IsDBNull(drstart(0)("Value")) Then
                    MacStartClientSettings = String.Empty
                Else
                    MacStartClientSettings = drstart(0)("Value")
                End If

                If MacStartClientSettings <> txtStart.Text Then
                    If txtStart.Text <> "" Then
                        sql += "UPDATE ClientSettings SET Value = '" & txtStart.Text & "' WHERE Name = 'MACStartMessage';"
                        SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), "MACStartMessage has been updated  from ' " & drstart(0)("Value") & " '  to ' " & txtStart.Text & " ' in Application Settings.")
                    Else
                        sql += "UPDATE ClientSettings SET Value = NULL WHERE Name = 'MACStartMessage'; "
                        SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), "MACStartMessage has been updated to blank value in Application Settings.")
                    End If
                End If
            End If

            If drend.Length <> 0 Then
                Dim MacEndClientSettings As String
                If drend(0)("Value") Is Nothing Or IsDBNull(drend(0)("Value")) Then
                    MacEndClientSettings = String.Empty
                Else
                    MacEndClientSettings = drend(0)("Value")
                End If
                If MacEndClientSettings <> txtEnd.Text Then
                    If txtEnd.Text <> "" Then
                        sql += "UPDATE ClientSettings SET Value = '" & txtEnd.Text & "' WHERE Name = 'MACEndMessage';"
                        SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), "MACEndMessage has been updated from ' " & drend(0)("Value") & " ' to ' " & txtEnd.Text & " ' in Application Settings.")
                    Else
                        sql += "UPDATE ClientSettings SET Value = NULL WHERE Name = 'MACEndMessage'; "
                        SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), "MACEndMessage has been updated to blank value in Application Settings.")
                    End If
                End If
            End If

            If drstartcolor.Length <> 0 Then
                Dim MacStartColorClientSettings As String
                If drstartcolor(0)("Value") Is Nothing Or IsDBNull(drstartcolor(0)("Value")) Then
                    MacStartColorClientSettings = String.Empty
                Else
                    MacStartColorClientSettings = drstartcolor(0)("Value")
                End If
                If MacStartColorClientSettings <> ddlStartColor.SelectedValue.ToString() Then
                    sql += "UPDATE ClientSettings SET Value = '" & ddlStartColor.SelectedValue.ToString() & "' WHERE Name = 'MACStartMessageColor';"
                    If (drstartcolor(0)("Value") = "Black") Then
                        Dim srtClr = "Default Color"
                        SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), "MACStartMessageColor has been updated from ' " & srtClr & " ' to ' " & ddlStartColor.SelectedItem.Text & " ' in Application Settings.")
                    Else
                        SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), "MACStartMessageColor has been updated from ' " & drstartcolor(0)("Value") & " ' to ' " & ddlStartColor.SelectedItem.Text & " ' in Application Settings.")
                    End If

                End If
            End If

            If drendcolor.Length <> 0 Then
                Dim MacEndColorClientSettings As String
                If drendcolor(0)("Value") Is Nothing Or IsDBNull(drendcolor(0)("Value")) Then
                    MacEndColorClientSettings = String.Empty
                Else
                    MacEndColorClientSettings = drendcolor(0)("Value")
                End If
                If MacEndColorClientSettings <> ddlEndColor.SelectedValue.ToString() Then
                    sql += "UPDATE ClientSettings SET Value = '" & ddlEndColor.SelectedValue.ToString() & "' WHERE Name = 'MACEndMessageColor';"
                    If (drendcolor(0)("Value") = "Black") Then
                        Dim clr = "Default Color"
                        SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), "MACEndMessageColor has been updated from ' " & clr & "' to ' " & ddlEndColor.SelectedItem.Text & " ' in Application Settings.")
                    Else
                        SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), "MACEndMessageColor has been updated from ' " & drendcolor(0)("Value") & "' to ' " & ddlEndColor.SelectedItem.Text & " ' in Application Settings.")
                    End If

                End If
            End If

        End If

        If sql <> "" Then
            Database.Tenant.Execute(sql)
        End If
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim streetAddress1 = txtStreetAddressTable.Text
        Dim targetStreetAddressTable = DataSource.FindTargetTable(Database.Tenant, streetAddress1)
        Dim FieldstoCheck As String = txtFilterColoumnName.Text + "," + txtStreetNumberField.Text + "," + txtStreetNameField.Text + "," + txtStreetNameSuffixField.Text + "," + txtStreetUnitField.Text
        'Dim FieldstoCheck As String = txtFilterColoumnName.Text + "," + txtStreetNumberField.Text + "," + txtStreetNameField.Text + "," + txtStreetNameSuffixField.Text
        Dim AlertMsg As String = Database.Tenant.GetStringValue("EXEC dbo.FieldFinder @tablename={0},@String={1},@tablegiven={2},@delimiter={3}".SqlFormatString(targetStreetAddressTable, FieldstoCheck, streetAddress1, ","))
        If AlertMsg <> "" Then
            Alert(AlertMsg)
            Return
        End If
        Dim res = CreateUpdateStreetAddressSettings(True)
        StreetAddress.UpdateStreetAddress(Database.Tenant)
        Alert("Updated Successfully")
    End Sub

    Protected Sub ddl_table_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_table.SelectedIndexChanged
        If ddl_table.SelectedValue = "" Then
            ddl_field.FillFromSql("SELECT Id, Name FROM DataSourceField WHERE TableId = 0 ORDER BY Name", True, "-- Select Table --")
        Else
            ddl_field.FillFromSql("SELECT Id, Name FROM DataSourceField WHERE TableId = " & ddl_table.SelectedValue & " ORDER BY Name", True)
        End If

    End Sub

    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If ddl_table.SelectedValue <> "" Then
            If ddl_field.SelectedValue <> "" Then
                Database.Tenant.Execute("update datasourceField set TrackingEnabled = NULL ")
                Database.Tenant.Execute("update datasourceField set TrackingEnabled = 1 WHERE id = " & ddl_field.SelectedValue)
                'Database.Tenant.Execute(" update datasourceField set TrackingEnabledValue='" + txt_tracking.Text + "' WHERE id = " & ddl_field.SelectedValue)
                Alert("Updated Successfully.")
            Else
                Alert("Please select one of the fields.")
                Return
            End If
        Else
            Database.Tenant.Execute("update datasourceField set TrackingEnabled = NULL ")
            Alert("Please select one of the fields.")
        End If
    End Sub
    Dim flag As Integer = 0
    Protected Sub Btn_Bppsave_Click(sender As Object, e As EventArgs) Handles Btn_Bppsave.Click
        Dim Sql As String
        Dim AppnDetails As DataRow = Database.Tenant.GetTopRow("SELECT BPPEnabled,BPPCondition,BPPParentPropertyField,RPCondition  FROM Application")
        Dim BPPEnabledDts = AppnDetails.GetBoolean("BPPEnabled")
        Dim BPPConditionDts = AppnDetails.GetString("BPPCondition")
        Dim BPPParentPropertyFieldDts = AppnDetails.GetString("BPPParentPropertyField")
        Dim RPConditionDts = AppnDetails.GetString("RPCondition")
        If chk_bpp.Checked = BPPEnabledDts And BPPConditionDts = txt_bppCondition.Text And BPPParentPropertyFieldDts = txt_BppPPF.Text And RPConditionDts = txt_rpCondition.Text Then
            Alert("You haven't updated any fields. Please update one of the fields.")
            Return
        End If
        If chk_bpp.Checked Then
            If txt_bppCondition.Text.Length <= 100 And txt_BppPPF.Text.Length <= 100 And txt_rpCondition.Text.Length <= 200 Then
                If txt_bppCondition.Text.Trim() <> "" And txt_BppPPF.Text.Trim() <> "" And txt_rpCondition.Text.Trim() <> "" Then
                    Database.Tenant.Execute("update Application Set BPPEnabled=1, BPPCondition={0}, BPPParentPropertyField={1}, RPCondition={2}".SqlFormatString(txt_bppCondition.Text, txt_BppPPF.Text, txt_rpCondition.Text))

                Else
                    Alert("Please fill the mandatory fields")
                    Return
                End If
            Else
                If txt_rpCondition.Text.Length > 200 Then
                    Alert("Exceeding maximum number of characters. Please shorten your RP Condition to 200 characters or less.")
                    Return
                Else
                    Alert("Exceeding maximum number of characters. Please shorten your BPP Condition or BPP Parent Property Field to 100 characters or less.")
                    Return
                End If
            End If
        Else
            Database.Tenant.Execute("update Application Set BPPEnabled={0}".SqlFormatString(0))

        End If

        If flag = 1 Then
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), "BPP Enabled")
        Else
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), "BPP Disabled")
        End If

        'Dim BPPReadOnly = IIf(chk_bppRDR.Checked, 1, 0)
        'Sql = " IF NOT EXISTS(SELECT * FROM ClientSettings WHERE Name='BPPDownloadType') INSERT INTO ClientSettings(Name, Value) VALUES('BPPDownloadType','" + ddl_BppPD.SelectedValue + "') ELSE UPDATE ClientSettings Set Value = '" & ddl_BppPD.SelectedValue & "' WHERE Name = 'BPPDownloadType'; "
        'Sql += " IF NOT EXISTS(SELECT * FROM ClientSettings WHERE Name='BPPRealDataReadOnly') INSERT INTO ClientSettings(Name, Value) VALUES('BPPRealDataReadOnly','BPPReadOnly') ELSE UPDATE ClientSettings Set Value = '" & BPPReadOnly & "' WHERE Name = 'BPPRealDataReadOnly'; "
        Sql = "IF EXISTS(SELECT * FROM ClientSettings WHERE Name = 'LastSchemaUpdatedTime') UPDATE ClientSettings SET Value = CONVERT(NVARCHAR(24),GETUTCDATE(),121) WHERE Name = 'LastSchemaUpdatedTime' ELSE INSERT INTO ClientSettings(name,Value) Values('LastSchemaUpdatedTime',CONVERT(NVARCHAR(24),GETUTCDATE(),121));"
        Sql += "IF EXISTS(SELECT * FROM ClientSettings WHERE Name = 'LastSchemaUpdatedCounter') UPDATE ClientSettings SET Value = Value + 1 WHERE Name = 'LastSchemaUpdatedCounter' ELSE INSERT INTO ClientSettings(name,Value) Values('LastSchemaUpdatedCounter','1');"
        Database.Tenant.Execute(Sql)
        Alert("Saved Successfully .")
    End Sub

End Class



