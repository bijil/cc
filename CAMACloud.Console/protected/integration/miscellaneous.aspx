﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master"
    CodeBehind="miscellaneous.aspx.vb" Inherits="CAMACloud.Console.miscellaneous" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function confirmResetAllParcels() {
            var warning_msg = 'WARNING! This process will reset ALL parcels\' QC status, Field Reviewed Flags, Alerts, Priority, Messages and clear / Reject any Pending Changes not yet downsynced.'
            if (confirm(warning_msg)) {
                return true;
            }
            else {
                return false;
            }
        }
        
        function warnResetAllParcels(pCount) {
        	var confirm_msg = 'Are you sure? You will be required to set new Priorities for field visits, etc. Please CANCEL and export any Messages and Flags to CSV if they are needed for future visits.\n\nThis action cannot be undone.'
        	if(pCount > 0) {
				if (confirm("There are " + pCount + " properties that are QC Approved but have not synced to CAMA. Are you sure you want to proceed?")) {
					if(confirm(confirm_msg))
						$('.btn-Hidden').click();
				}
				else return false;				
			}
			else {
				if(confirm(confirm_msg)) $('.btn-Hidden').click();
				else return false;
			}
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
        Miscellaneous Tasks</h1>
    <div runat="server" id="dvMiscellaneous">
        <h2>
        Import/Export All CC Settings</h2>
    <p class="info" style="width: auto;">
        The import and export button below can help you to import data to CC and export
        from CC respectively.
    </p>
    <div class="link-panel">
        <asp:LinkButton runat="server" ID="lnkExport" Text="Export All CC Settings" />
        <asp:LinkButton runat="server" ID="lnkImport" OnClientClick="document.getElementById('fuImportFile').click();return false;"
            Text="Import All CC Settings" />
    </div>
    <div class="link-panel">
    </div>
    <div class="clear">
        <div style="display: none;">
            <asp:FileUpload runat="server" ID="fuImportFile" ClientIDMode="Static" onchange="document.getElementById('btnUploadImportFile').click()"
                accept="application/xml" />
            <asp:Button runat="server" ID="btnUploadImportFile" ClientIDMode="Static" />
        </div>
    </div>
    <h2>
        Account Purge Lock</h2>
    <p class="info">
        Purge Lock protects an account from an accidental purges.
    </p>
    <p>
        <asp:HiddenField runat="server" ID="hdnPurgeStatus" />
        <div class="link-panel">
            Purge Lock:
            <asp:Label runat="server" ID="lblPurgeLockStatus" Text="??" Font-Bold="true" Font-Size="Larger" />&nbsp;&nbsp;&nbsp;<asp:LinkButton
                runat="server" ID="lbTogglePurgeLock" Text="Click here to toggle" Font-Bold="true" />
        </div>
    </p>
    <h2>
        Job Management</h2>
    <div class="link-panel">
        <asp:LinkButton runat="server" Text="Show pending jobs" ID="lbShowJobs" OnClientClick="showMask();"/>
    </div>
    <asp:GridView runat="server" ID="gridPendingJobs">
        <Columns>
            <asp:BoundField DataField="ID" HeaderText="Job ID" ItemStyle-Width="70px" />
            <asp:BoundField DataField="Type" HeaderText="Type" ItemStyle-Width="100px" />
            <asp:TemplateField>
                <ItemStyle Width="80px" />
                <ItemTemplate>
                    <asp:LinkButton runat="server" Text="Cancel Job" CommandName="CancelJob" CommandArgument='<%# Eval("ID") %>'
                        OnClientClick="return confirm('Cancelling the job will abort the transfer process if it is active. Are you sure you want to cancel this job?')" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            No pending jobs
        </EmptyDataTemplate>
    </asp:GridView>
    <p class="b">
        Uncommit Photo Job</p>
    <div class="link-panel">
        Photo Job ID (blank to use last job):
        <asp:TextBox runat="server" type="number" ID="txtJobId" Width="80px" />&nbsp;&nbsp;
        <asp:LinkButton runat="server" Text="Uncommit Job" ID="lbUncommitPhoto" />
    </div>
    <div class="link-panel" runat="server" id="dvBasicSettings" Visible="False" >
        <h2> End of Year Process :</h2>
        <div runat="server" ID="dvEYPDescription" Visible="False">
	        The End of Year Process can be used to <b>completely</b> clear your cloud environment of pending changes. <br><br>
			This process will:<br>
			- Reset all parcels' QC status<br>
			- Reset all parcels' Field Reviewed flags<br>
			- Clear all Field Alert flags<br>
			- Clear all Alert from Field and Alert from Office comments<br>
			- Clear all Priorities, setting the properties back to Normal priority<br>
			- <span style="color: red">Reject</span> any Pending Changes (data and photos) that have not downsynced to CAMA<br><br>
			
			This is a method to clear all of the above information from your recent field cycle before starting a new one. You can export any field alert flags and messages in Quality Control to CSV for historical purposes, or if they are needed for future visits.<br><br> 
			
			If your office chooses to retain field alert flags and comments, or any piece of the above listed information from one appraisal cycle to the next, you can clear this information individually through Quality Control > Bulk Edit.
		</div>           
       	<asp:LinkButton runat="server" ID="lbResetAllQCParcels" Text="Reset All Parcels" OnClientClick="return confirmResetAllParcels();"  />
        <asp:Button runat="server" ID="btnHidden" Text="" Style="display: none;"  CssClass="btn-Hidden"  />
    </div>
    <div class="link-panel">
        <h2> Clear QC Cache :</h2>
            <asp:LinkButton runat="server" ID="lbResetDbUpdatedTime" Text="Clear QC Cache"  OnClientClick="showMask();"/>
    </div>
     <div class="link-panel">
        <h2> Reset Search Field Settings :</h2>
            <asp:LinkButton runat="server" ID="lbResetAllSearchResultFields" Text="Reset All UI Gridview Result Fields" OnClick="lbResetAllSearchResultFields_Click"/>
    </div>
    </div>
</asp:Content>
