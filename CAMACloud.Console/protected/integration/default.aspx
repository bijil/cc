﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/DataSetup.master" AutoEventWireup="false" Inherits="CAMACloud.Console.protected_integration_default" Codebehind="default.aspx.vb" %>

<%@ Import Namespace="CAMACloud" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link rel="Stylesheet" href="/App_Static/css/tabcontent.css" />
   <script type="text/javascript" src="/App_Static/js/tabcontent.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <h1>CAMA Cloud - Application Settings</h1>
   
   <style type="text/css">
       select
       {
           width: 200px;
       }
       input[type="text"]
       {
           width: 196px;
       }
        input[type="submit"]
        {
            margin-top:35px;
        }
        td span[sep]
        {
            margin-left:15px;
            float:right;
        }
        tr[hideOnLoad]
        {
            display:none;
        }
        span[req]
        {
            color:Red;
            font-size: 16px;
        }
       .auto-style1 {
           width: 217px;
       }
   </style>
   <script type="text/javascript">
       $(window).load(function () {
           //KeyfieldsDropdown
           showKeyFields($('.KeyfieldsDropdown').val());
           $('.txtField').blur(function () {
               if($(this).val()=='')
                   $(this).next('span').show();
               else
                   $(this).next('span').hide();
           
           });
           	setTimeout(function(){
           	if(!($(".ui-tabs > #fieldBppLI").is(":visible")) && $("#fieldBppLI").hasClass("selected")){	
           	
				$("#fieldBppLI").removeClass('selected');
				$('.ui-tabs > :first-child').addClass('selected');
				$("#Parcel").css("display","block");
			}
			}, 100)
 			if ($('#MainContent_MainContent_chk_bpp').attr("checked")) {
                $('#MainContent_MainContent_txt_bppCondition').removeAttr('readonly');
                $('#MainContent_MainContent_txt_BppPPF').removeAttr('readonly');
                $('#MainContent_MainContent_txt_rpCondition').removeAttr('readonly');
                $('.req').show();
                return;
            }
            else {
            	$('#MainContent_MainContent_txt_bppCondition').attr('readonly', 'readonly');
                $('#MainContent_MainContent_txt_BppPPF').attr('readonly', 'readonly');  
                $('#MainContent_MainContent_txt_rpCondition').attr('readonly', 'readonly');  
				$('.req').hide();                
                return;
           }
           
						
       });


       
     
       function showKeyFields(KeyFields) {
           try {

               var valueIter = 0;
               $(' tr[hideOnLoad]').hide();
               while (valueIter < KeyFields) {
                   valueIter += 1;
                   $('tr[' + valueIter + ']').show();

               }
       }
       catch (e) {
                console.log(e.message);
           }
       }
       function generateKeyFields(source) {
           try {
               console.log('inside generateKeyFields');
               var KeyFieldQuantity = parseInt(source.value);
               showKeyFields(KeyFieldQuantity);
           }
           catch (e) {
               console.log(e.message);
           }
       }

       function validateKeyFields() {
           try {
               var chk = $('#<%=chkShowAlternate.ClientID%>:checked')
               var alkVal = $('#MainContent_MainContent_txtAlternateField').val();
               if (alkVal.length == 0) {
                   if (chk.length != 0) {
                       alert('Please Select the Alternate Field.');
                       return false;
                   }
               }
               var div = $('#Parcel');
               var chk = $('input[type="checkbox"]:checked', div).length;
               if (chk == 0) {
                   alert('Please select atleast one field.')
                   return false;
               }
               var total = $('.KeyfieldsDropdown').val();
               var flag = 0, keyFieldArray = [], sameKeyField = false;
               
               //Alternate key field and keyfield should be different
               var Akschk = $('#<%=chkShowAlternate.ClientID%>').is(':checked')
               var Kfschk = $('#<%=chkShowKeyfield.ClientID%>').is(':checked')
               
               if (Akschk && Kfschk && alkVal != '') {
                 	for (i = 1; i <= total;i++) {
	                   if ($('tr[' + i + '] input').val().toUpperCase() == alkVal.toUpperCase()) {
	                     alert("Alternate Key Field and Key Fields should be different.");
	                     return false;
	                   }
               		}
                }
               //
               
               for (i = 1; i <= total;i++) {
                   if ($('tr[' + i + '] input').val() == '') {
                       flag = 1;
                       $('tr[' + i + '] input').next('span').show();
                   }
                   else {
                   		 keyFieldArray.push($('tr[' + i + '] input').val());
                         $('tr[' + i + '] input').next('span').hide();
                   }
               }
               
               if (!flag) {
                    if (!(keyFieldArray.length === new Set(keyFieldArray).size))
                        sameKeyField = true;    
               }
               
               var totCounter = parseInt(total)
               while(totCounter < $('input[type="text"].txtField ').length){
                 totCounter += 1;
                 $('tr[' + totCounter + '] input').val('')
               }
               if (flag == 1) {
                   alert('Fill selected Key Fields');
                   return false;
                  
               } 
               else if (sameKeyField) {
                   alert('Same column is given as another keyfield value. Please provide another.');
                   return false;
               }
               else
                   return true;
               }
           catch (e) {
               console.log(e.message);
           }
           
       }

       function disabeEnter(){
        	var focused = $(':focus');
       		if(focused.context.activeElement.id!="MainContent_MainContent_txtStart" && focused.context.activeElement.id!="MainContent_MainContent_txtEnd"){
    			$(document).keypress(
                function(event){
 					if (event.which == '13') {
                        event.preventDefault();
  						}
				});
			}
		} 
 		
 		function BPPChecked(){
            var flag
 			if ($('#MainContent_MainContent_chk_bpp').attr("checked")) {
            flag = 1;
                $('#MainContent_MainContent_txt_bppCondition').removeAttr('readonly');
                $('#MainContent_MainContent_txt_BppPPF').removeAttr('readonly');
                $('#MainContent_MainContent_txt_rpCondition').removeAttr('readonly');
                $('.req').show();
                return;
            }
            else {
            flag = 0;
            	$('#MainContent_MainContent_txt_bppCondition').attr('readonly', 'readonly').val("");
                $('#MainContent_MainContent_txt_BppPPF').attr('readonly', 'readonly').val("");  
                $('#MainContent_MainContent_txt_rpCondition').attr('readonly', 'readonly').val("");  
				$('.req').hide();                
                return;
            }           
       }
       function filtrColmnValdtn() {
           let fltrValue = $('#MainContent_MainContent_txtFilterValue').val(), fltrClmn = $('#MainContent_MainContent_txtFilterColoumnName').val(), v = true;

           $('.validateReqStreet').each((i, x) => {
               if ($(x).val().trim() == '') v = false;
           });
           if (!v) return false;
           if ((fltrValue != "" && fltrClmn == "") || (fltrValue == "" && fltrClmn != "")) {
               alert("Please enter both Filter Column Name and Filter Value or Leave both these field as empty !");
               return false;
           }
       }


   </script>
   <asp:HiddenField ID="tabIndex" runat="server"  />
   <div id="content-Panel">

     <div class="link-panel">
                              <div  style="float:right">  <asp:LinkButton runat="server" ID="lbExport" Text="Export Settings" />&nbsp;&nbsp;<span style="font-weight:normal;color:Black;">|</span>&nbsp&nbsp;
                                <asp:LinkButton runat="server" ID="lbImport" Text="Import" OnClientClick="document.getElementById('fuImportFile').click();return false;"/>
     </div>
     </div>
       <div class="clear">
                            <div style="display:none;">
                                <asp:FileUpload runat="server" ID="fuImportFile" ClientIDMode="Static" onchange="document.getElementById('btnUploadImportFile').click()"  accept="application/xml" />
        <asp:Button runat="server" ID="btnUploadImportFile" ClientIDMode="Static" />
                            </div>
       </div>
           
       <div class="tabs-main">
       
            <ul class="tabs" data-persist="true">
                <li><a href="#Parcel">Parcel</a></li>
                <li><a href="#Neighborhood" ><%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName()%></a></li>
                <li><a href="#Street"> Street Address</a></li>
                <li><a href="#Sketch" >Sketch</a></li>
                 <li><a href="#bulkapp"  > Bulk Approval</a></li>
                 <li><a href="#macMessage"  > MAC Message</a></li>
                 <li  id="fieldTrackingLI" style=<%=visibleFieldTracking()%> ><a href="#FieldTracking"  > Field Tracking</a></li>
                 <li  id="fieldBppLI" style=<%=visibleBpp()%> ><a href="#FieldBpp"  > BPP</a></li>
            </ul> 
          
        </div>
  <div class="tabcontents" onkeypress="disabeEnter();">
   <table>
        <tbody>
        <tr>
        <td>
        <div id="Parcel" style="display:none;" class="">
      <table>
      <tbody>
        <tr>
            <td style="padding-right:80px;">Parcel Table<span sep>:</span> </td>
            <td>
               <asp:TextBox ID="txtParcelTable" runat="server"></asp:TextBox>
               <asp:RequiredFieldValidator ID="rfvtxtParcelTable" runat="server" Enabled="false" ControlToValidate="txtParcelTable" ValidationGroup="save" />
            </td>
        </tr>
       <tr>
           <td style="padding-right:80px;">Alternate Key Field<span sep>:</span> </td>
           <td>
               <asp:TextBox ID="txtAlternateField" runat="server"></asp:TextBox>
               <asp:CheckBox ID ="chkShowAlternate" runat="server"></asp:CheckBox>
           </td>
           
       </tr>
         <tr>
            <td style="padding-right:80px;">Number Of Key Fields<span sep>:</span> </td>
            <td>
                <select Id="KeyfieldNumber" class="KeyfieldsDropdown" runat="server" onchange="generateKeyFields(this);"   >
                   <option value="01">01</option>
                   <option value="02">02</option>
                   <option value="03">03</option>
                   <option value="04">04</option>
                   <option value="05">05</option>
                   <option value="06">06</option>
                   <option value="07">07</option>
                   <option value="08">08</option>
                   <option value="09">09</option>
                   <option value="10">10</option>
               </select>
            </td>
        </tr>
          <tr 1>
            <td style="padding-right:80px;">Key Field 1<span sep>:</span> </td>
            <td>
                  <asp:TextBox ID="txtKeyField1" runat="server" CssClass="txtField" ></asp:TextBox>
                  <span  style="color:red; display:none">*</span>
                  <asp:CheckBox ID ="chkShowKeyfield" runat="server"></asp:CheckBox>
               
                  <%--<asp:RequiredFieldValidator ID="rfvtxtKeyField1" runat="server"  Enabled="false" ControlToValidate="txtKeyField1" ValidationGroup="ParcelGrp" />--%>
            </td>
        </tr>
          <tr 2 hideOnLoad>
            <td style="padding-right:80px;">Key Field 2<span sep>:</span> </td>
            <td>
                  <asp:TextBox ID="txtKeyField2" runat="server" CssClass="txtField"></asp:TextBox>
                   <span style="color:red; display:none;">*</span>
            </td>
        </tr>
          <tr 3 hideOnLoad>
            <td style="padding-right:80px;">Key Field 3<span sep>:</span> </td>
            <td>
                  <asp:TextBox ID="txtKeyField3" runat="server" CssClass="txtField"></asp:TextBox>
                  <span style="color:red; display:none;">*</span>
            </td>
        </tr>
          <tr 4 hideOnLoad>
            <td style="padding-right:80px;">Key Field 4<span sep>:</span> </td>
            <td>
                  <asp:TextBox ID="txtKeyField4" runat="server" CssClass="txtField"></asp:TextBox>
                  <span style="color:red; display:none">*</span>
            </td>
        </tr>
          <tr 5 hideOnLoad>
            <td style="padding-right:80px;">Key Field 5<span sep>:</span> </td>
            <td>
                  <asp:TextBox ID="txtKeyField5" runat="server" CssClass="txtField"></asp:TextBox>
                 <span style="color:red; display:none;">*</span>
            </td>
        </tr>
          <tr 6 hideOnLoad> 
            <td style="padding-right:80px;">Key Field 6<span sep>:</span> </td>
            <td>
                  <asp:TextBox ID="txtKeyField6" runat="server" CssClass="txtField"></asp:TextBox>
                   <span style="color:red; display:none;">*</span>
            </td>
        </tr>
          <tr 7 hideOnLoad>
            <td style="padding-right:80px;">Key Field 7<span sep>:</span> </td>
            <td>
                  <asp:TextBox ID="txtKeyField7" runat="server" CssClass="txtField"></asp:TextBox>
                   <span style="color:red; display:none;">*</span>
            </td>
        </tr>
          <tr 8 hideOnLoad >
            <td style="padding-right:80px;">Key Field 8<span sep>:</span> </td>
            <td>
                  <asp:TextBox ID="txtKeyField8" runat="server" CssClass="txtField"></asp:TextBox>
                  <span style="color:red; display:none;">*</span>
            </td>
        </tr>
          <tr 9 hideOnLoad>
            <td style="padding-right:80px;">Key Field 9<span sep>:</span> </td>
            <td>
                  <asp:TextBox ID="txtKeyField9" runat="server" CssClass="txtField"></asp:TextBox>
                   <span style="color:red; display:none;">*</span>
            </td>
        </tr>
          <tr 10 hideOnLoad>
            <td style="padding-right:80px;">Key Field 10<span sep>:</span> </td>
            <td>
                  <asp:TextBox ID="txtKeyField10" runat="server" CssClass="txtField"></asp:TextBox>
                  <span style="color:red; display:none;">*</span>
            </td>
        </tr>
         <tr>
         <td></td>
         <td>
             <asp:Button ID="btnParcel" runat="server" Text="Submit"  ValidationGroup="ParcelGrp" OnClientClick="return validateKeyFields();"/></td>
        </tr>
      </tbody>
      </table>
      </div>
      </td>
      </tr>
       <tr>
       <td>
       <div id="Neighborhood">
       <table>
       <tbody>
         <tr>
            <td style="padding-right:80px;"><%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName()%> Table<span sep>:</span> </td>
            <td>
                <asp:TextBox ID="txtNeighborhoodTable" runat="server"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="rfvtxtNeighborhoodTable" runat="server" ControlToValidate="txtNeighborhoodTable" ValidationGroup="NbhdGrp" />
            </td>
        </tr>
        <tr>
            <td style="padding-right:80px;"><%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName()%> Field<span sep>:</span> </td>
            <td>
                   <asp:TextBox ID="txtNeighborhoodField" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvtxtNeighborhoodField" runat="server" ControlToValidate="txtNeighborhoodField" ValidationGroup="NbhdGrp" />
            </td>
        </tr>
        <tr>
            <td style="padding-right:80px;"><%=CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName()%> Name Field<span sep>:</span> </td>
            <td>
                <asp:TextBox ID="txtNeighborhoodNameField" runat="server"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="rfvtxtNeighborhoodNameField" runat="server" ControlToValidate="txtNeighborhoodNameField" ValidationGroup="NbhdGrp" />
            </td>
        </tr>
        <tr>
         <td></td>
         <td>
             <asp:Button ID="btnNeighborhood" runat="server" Text="Submit"  ValidationGroup="NbhdGrp"/></td>
        </tr>
       </tbody>
       </table>
       </div>
       </td>
       </tr>
      <tr>
      <td>
      <div id="Street">
      <table>
      <tbody>
       <tr>
            <td style="padding-right:80px;">Street Address Table<span sep>:</span> </td>
            <td>
                  <asp:TextBox ID="txtStreetAddressTable" CssClass="validateReqStreet" runat="server"></asp:TextBox>
                   <asp:RequiredFieldValidator ID="rfvtxtStreetAddressTable" runat="server" ControlToValidate="txtStreetAddressTable" ValidationGroup="StreetGrp" />
            </td>
        </tr>
         <tr>
            <td style="padding-right:80px;">Street Address Field<span sep>:</span> </td>
            <td>
                 <asp:TextBox ID="txtStreetAddressField" CssClass="validateReqStreet" runat="server" MaxLength="200" ></asp:TextBox>
                  <asp:RequiredFieldValidator ID="rfvtxtStreetAddressField" runat="server" ControlToValidate="txtStreetAddressField" ValidationGroup="StreetGrp"  />
           </td>
        </tr>
          <tr>
            <td style="padding-right:80px;">Street Number Field<span sep>:</span> </td>
            <td>
                 <asp:TextBox ID="txtStreetNumberField" runat="server" MaxLength="50"></asp:TextBox>
           </td>
        </tr>
          <tr>
            <td style="padding-right:80px;">Street Name Field<span sep>:</span> </td>
            <td>
                 <asp:TextBox ID="txtStreetNameField" runat="server"  MaxLength="50"></asp:TextBox>
                  </td>
        </tr>
          <tr>
            <td style="padding-right:80px;">Street Name Suffix Field<span sep>:</span> </td>
            <td>
                 <asp:TextBox ID="txtStreetNameSuffixField" runat="server"></asp:TextBox>
                  </td>
        </tr>
        <tr>
            <td style="padding-right:80px;">Street Unit Field<span sep>:</span></td>
            <td><asp:TextBox ID="txtStreetUnitField" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="padding-right:80px;">Filter Column Name<span sep>:</span> </td>
            <td>
            <asp:TextBox ID="txtFilterColoumnName" runat="server"></asp:TextBox>
                
            </td>
        </tr>
        <tr>
            <td style="padding-right:80px;">Filter Condition<span sep>:</span> </td>
            <td>
                  <asp:DropDownList ID="ddlFilterCondition" runat="server">
                  <asp:ListItem Text="Equal To" Value="="></asp:ListItem>
                   <asp:ListItem Text="Less Than" Value="<"></asp:ListItem>
                   <asp:ListItem Text="Greater Than" Value=">"></asp:ListItem>

                  </asp:DropDownList>
                  
            </td>
        </tr>
        <tr>
            <td style="padding-right:80px;">Filter Value<span sep>:</span> </td>
            <td>
               <asp:TextBox ID="txtFilterValue" runat="server"></asp:TextBox>
               
            </td>
        </tr>
        
       <tr>
         <td></td>
         <td>
             <asp:Button ID="btnStreet" runat="server" Text="Submit" ValidationGroup="StreetGrp" OnClientClick="return filtrColmnValdtn();" />
             <asp:Button ID="Button1" runat="server" Text="Update All Records" ValidationGroup="StreetGrp" />
           </td>
        </tr>
         
      </tbody></table>
      </div>
      </td>
      </tr>
      <tr>
     <td>
     <div id="Sketch">
        <table><tbody>
      <%--<tr>
            <td style="padding-right:80px;">Sketch Table<span sep>:</span> </td>
            <td>
            <asp:TextBox ID="txtSketchTable" runat="server" ></asp:TextBox>
             <asp:RequiredFieldValidator ID="rfvtxtSketchTable" runat="server" ControlToValidate="txtSketchTable" ValidationGroup="SketchGrp" />
            </td>
        </tr>
         <tr>
            <td style="padding-right:80px;">Sketch Label<span sep>:</span> </td>
            <td>
                <asp:TextBox ID="txtSketchLabel" runat="server"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="rfvtxtSketchLabel" runat="server" ControlToValidate="txtSketchLabel" ValidationGroup="SketchGrp" />
            </td>
        </tr>
        <tr>
            <td style="padding-right:80px;">Sketch Command Field<span sep>:</span> </td>
            <td>
                <asp:TextBox ID="txtsketchcommand" runat="server"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="rgdsketchcommand" runat="server" ControlToValidate="txtsketchcommand" ValidationGroup="SketchGrp" />
            </td>
        </tr>
        <tr>
            <td style="padding-right:80px;">Sketch Key Field<span sep>:</span> </td>
            <td>
                <asp:TextBox ID="txtScketchKeyField" runat="server"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="rfScketchKeyField" runat="server" ControlToValidate="txtScketchKeyField" ValidationGroup="SketchGrp" />
            </td>
        </tr>
       <tr>
            <td style="padding-right:80px;">Sketch Vector Table<span sep>:</span> </td>
            <td>
                <asp:TextBox ID="txtVectorTable" runat="server"></asp:TextBox></td>
        </tr>
     
      
       <tr>
            <td style="padding-right:80px;">Perimeter<span sep>:</span> </td>
            <td>
                  <asp:TextBox ID="txtSketchPerimeter" runat="server"></asp:TextBox>
                   <asp:RequiredFieldValidator ID="rfvtxtSketchPerimeter" runat="server" ControlToValidate="txtSketchPerimeter" ValidationGroup="SketchGrp" />
            </td>
        </tr>
        <tr>
            <td style="padding-right:80px;">Area Field<span sep>:</span> </td>
            <td>
                  <asp:TextBox ID="txtAreaField" runat="server"></asp:TextBox>
                   <asp:RequiredFieldValidator ID="rqdAreaField" runat="server" ControlToValidate="txtAreaField" ValidationGroup="SketchGrp" />
            </td>
        </tr>
         <tr>
            <td style="padding-right:80px;">Vector Label Field<span sep>:</span> </td>
            <td>
                 <asp:TextBox ID="txtVectorLabelField" runat="server"></asp:TextBox>
                  <asp:RequiredFieldValidator ID="rfVectorLabelField" runat="server" ControlToValidate="txtVectorLabelField" ValidationGroup="SketchGrp" />
           </td>
        </tr>

        <tr>
            <td style="padding-right:80px;">Vector Command Field<span sep>:</span> </td>
            <td>
                <asp:TextBox ID="txtVectorCommand" runat="server"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="rfVectorCommand" runat="server" ControlToValidate="txtVectorCommand" ValidationGroup="SketchGrp" />
            </td>
        </tr>
         <tr>
            <td style="padding-right:80px;">Vector Connecting Field<span sep>:</span> </td>
            <td>
                  <asp:TextBox ID="txtVectorConnect" runat="server"></asp:TextBox >
                   <asp:RequiredFieldValidator ID="rqdVectorConnect" runat="server" ControlToValidate="txtVectorConnect"  ValidationGroup="SketchGrp" />
            </td>
        </tr>--%>
            <tr>
                <td style="padding-right:80px;">Sketch Config<span sep>:</span> </td>
                <td>
                    <asp:TextBox ID="txtSketchConfig" runat="server" MaxLength="500" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvtxtSketchConfig" runat="server" ControlToValidate="txtSketchConfig" ValidationGroup="SketchGrp" />
                </td>
            </tr>
            <tr>
                <td style="padding-right:80px;">Decimal Points<span sep>:</span> </td>
                <td>
                    <asp:DropDownList ID="drpDecimal" runat="server">
                        <asp:ListItem>0</asp:ListItem>
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                    </asp:DropDownList>
                 </td>
            </tr>
            <tr>
                <td style="padding-right:80px;">Display SQFT on UI<span sep>:</span> </td>
                <td>
                    <asp:DropDownList ID="drpAreaDecimal" runat="server">
                        <asp:ListItem>0</asp:ListItem>
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                    </asp:DropDownList>
                 </td>
            </tr>
            <tr>
                <td style="padding-right:80px;">Round to Decimal Place<span sep>:</span> </td>
                <td>
                    <asp:DropDownList ID="drpDimension" runat="server">
                        <asp:ListItem>None</asp:ListItem>
                        <asp:ListItem>Half Foot</asp:ListItem>
                        <asp:ListItem>Quarter Foot</asp:ListItem>
                    </asp:DropDownList>
                 </td>
            </tr>
            <tr>
                 <td></td>
                 <td><asp:Button ID="btnSketch" runat="server" Text="Submit" ValidationGroup="SketchGrp" /></td>
            </tr>
       
        <%-- <tr>
            <td style="padding-right:80px;">FieldID<span sep>:</span> </td>
            <td>
                  <asp:DropDownList ID="ddlFieldId" runat="server"></asp:DropDownList>
                   <asp:RequiredFieldValidator ID="rfvddlFieldId" runat="server" ControlToValidate="ddlFieldId"  ValidationGroup="save" />
            </td>
        </tr>--%>
        </tbody></table>
     </div>
     </td>
     </tr>
     <tr>
     <td>
     <div id="bulkapp">
        <table>
            <tr>
                <td style="padding-right:80px;">Batch Size<span sep>:</span></td>
                <td>  <asp:TextBox ID="txtBatchSize" runat="server" MaxLength="5"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="padding-right:80px;">Time Interval<span sep>:</span></td>
                <td>  <asp:TextBox ID="txtTimeInterval" runat="server" MaxLength="5"></asp:TextBox></td>
            </tr>
            <tr>
                <td></td>
                <td>  <asp:Button ID="btnBulkApp" runat="server" ValidationGroup="btnBulkApp" Text="Submit" /></td>
          </tr>
        </table>     
     </div>
     </td>
     </tr>
     <tr>
         <td>
             <div id="macMessage">
                <table>
                    <tr>
                        <td style="padding-right:80px;">Start of the message<span sep>:</span></td>
                        <td class="auto-style1">  <asp:TextBox ID="txtStart" runat="server" Height="50px" TextMode="MultiLine" Width="200px"></asp:TextBox></td>
                        <td>
                            <asp:DropDownList ID="ddlStartColor" runat="server" Height="20px" Width="109px">
                                <asp:ListItem Value="Black">Default Color</asp:ListItem>
                                <asp:ListItem Value="Blue">Blue</asp:ListItem>
                                <asp:ListItem Value="Red">Red</asp:ListItem>
                                <asp:ListItem Value="Green">Green</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-right:80px;">End of the message<span sep>:</span></td>
                        <td class="auto-style1">  <asp:TextBox ID="txtEnd" runat="server" Height="50px" TextMode="MultiLine" Width="200px"></asp:TextBox></td>
                        <td>
                            <asp:DropDownList ID="ddlEndColor" runat="server" Height="20px" Width="109px">
                                <asp:ListItem Value="Black">Default Color</asp:ListItem>
                                <asp:ListItem Value="Blue">Blue</asp:ListItem>
                                <asp:ListItem Value="Red">Red</asp:ListItem>
                                <asp:ListItem Value="Green">Green</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="auto-style1">  <asp:Button ID="btnMACMessage" runat="server" ValidationGroup="btnMACMessage" Text="Submit" /></td>
                  </tr>
                </table>     
     </div>
         </td>
     </tr>
              <tr>
         <td>
             <div id="FieldTracking"  >
                <table>
                    <tr>
                        <td style="padding-right:80px;">Table<span sep>:</span></td>
                        
                        <td>
                            <asp:DropDownList ID="ddl_table" runat="server" AutoPostBack="True" ></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-right:80px;">Field:<span sep>:</span></td>
                     
                        <td>
                             <asp:DropDownList ID="ddl_field" runat="server"></asp:DropDownList>
                        </td>
                    </tr>
                    <%--   <tr>
                        <td style="padding-right:80px;">Value to Enable Tracking:<span sep>:</span></td>
                     
                        <td>
                            <asp:TextBox ID="txt_tracking" runat="server"></asp:TextBox>
                        </td>
                    </tr>--%>
                    <tr>
                        <td></td>
                        <td class="auto-style1">  <asp:Button ID="Button2" runat="server"  Text="Submit" /></td>
                  </tr>
                </table>     
     </div>
         </td>
     </tr>
     
     <tr style=<%=visibleBpp()%> >
         <td>
             <div id="FieldBpp"  >
                <table>
                    <tr>
                        <td style="padding-right:80px; height:25px; display:flex; align-items:center;"  >BPP Enabled<span sep>:</span></td>
                        
                        <td>
                            <asp:CheckBox ID="chk_bpp" runat="server" onclick="BPPChecked();" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-right:80px; width: 200px; height:25px; display:flex; align-items:center;">BPP Condition<span sep>:</span><span class="req" style="color:red; font-size: 16px;">*</span></td>
                     
                        <td>
                             <asp:TextBox ID="txt_bppCondition" runat="server" Width ="200" ></asp:TextBox>
                        </td>
                    </tr>
                     <tr>
                        <td style="padding-right:80px; width: 200px; height:25px; display:flex; align-items:center;">BPP Parent Property Field<span sep>:</span><span class="req" style="color:red; font-size: 16px;">*</span></td>
                     
                        <td>
                            <asp:TextBox ID="txt_BppPPF" runat="server" Width ="200" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-right:80px; width: 200px; height:25px; display:flex; align-items:center;">RP Condition<span sep>:</span><span class="req" style="color:red; font-size: 16px;">*</span></td>
                     
                        <td>
                             <asp:TextBox ID="txt_rpCondition" runat="server" Width ="200" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                    <%--<tr>
                        <td style="padding-right:80px;">BPP Property Download<span sep>:</span></td>
                        
                        <td>
                            <asp:DropDownList ID="ddl_BppPD" runat="server" Height="20px" Width="109px">
                                <asp:ListItem Value="Bpp">BPP Only</asp:ListItem>
                                <asp:ListItem Value="Both">Both</asp:ListItem>                               
                            </asp:DropDownList>
                        </td>                       
                    </tr>
                    <tr>
                        <td style="padding-right:80px;">BPP Real Data Read Only<span sep>:</span></td>
                        
                        <td>
                            <asp:CheckBox ID="chk_bppRDR" runat="server" />
                        </td>
                    </tr>--%>
                        <td></td>
                        <td class="auto-style1">  <asp:Button ID="Btn_Bppsave" ValidationGroup="btnBPPMessage" runat="server"  Text="Submit" /></td>
                  </tr>
                </table>     
     </div>
         </td>
     </tr>
       <%-- <tr>
        
            <td>
            <asp:Button ID="btnSave" Text="Save Changes" runat="server" ValidationGroup="save"  OnClientClick="return validateKeyFields();" />
          
            <%-- <asp:Button ID="btnCancel" Text="Cancel"  runat="server" />--%>
         <%--   </td>
          
        </tr>--%>
    </tbody></table>
    </div>
    </div>

</asp:Content>

