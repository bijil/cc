﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/DesktopWeb.master"
    AutoEventWireup="false" Inherits="CAMACloud.Console.history" Codebehind="history.aspx.vb" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
<link rel="Stylesheet" type="text/css" href="/App_Static/css/Svo/SketchValidation.css" />

 <script type="text/javascript">
	 function setPageLayout(w, h) {
         $('.screen').height(h);
         $('.contentpage').height(h - $('.page-title').height()-10);
         $('.contentpage').css({ 'overflow-y': 'auto' });
     }
	 function historyloading() {
		 showMask();
		 return true;
	 }
	 function loadinghide() {
		 setTimeout(hideMask, 4000);
     }
$(function () {
$("#spanMainHeading").html('Audit Trail');
})
 </script>
    <section class="screen" id="history">
			
			<div style="float:left;width:94%;margin-left:3%;margin-top: 1%;">
            <div class="contentpage">
            		<div class="historypage">
			<asp:UpdatePanel runat="server" ID="upHistory">
				<ContentTemplate>
					<p>
						<asp:Button runat="server" class="btnNrml" ID="btnRefreshHistory" ClientIDMode="Static" OnClientClick = "return historyloading()" Text=" Refresh History " Style="width:130px;height: 26px;" />
					</p>
					<p>
						Showing latest
						<asp:Label runat="server" ID="lblAuditCount" Text="0" />
						items from history</p>
					<asp:Repeater runat="server" ID="rpAudit">
						<ItemTemplate>
							<span>
								<%# ShowDate()%></span>
							<div class="h-item">
								<table><tr><td><span class="h-time">
									<%# CDate(Eval("ReviewTime")).ToString("h:mm tt")%></span></td> <td><span class="h-user" style="width:100px;word-break:break-all;">
										<%# Eval("LoginID")%></span></td> <td> <span class="h-kv1"><a
											href='sv.aspx#/parcel/<%# Eval("ParcelId") %>' >
											<%# Eval("KeyValue1")%></a></span></td> <td><span class="h-parcel" style="width:250px;">
												<%# Eval("StreetAddress")%></span></td> <td><span id="data" class="h-desc">
													<%# Eval("Description")%></span></td></tr></table>
							</div>
						</ItemTemplate>
					</asp:Repeater>
				</ContentTemplate>
			</asp:UpdatePanel>
		</div>
        </div>
        </div>
	</section>
</asp:Content>
