﻿
Partial Class svadmin_flags
	Inherits System.Web.UI.Page

    Sub LoadFlags()

        Dim x = Session("Hello")

        Try
            If Database.Tenant Is Nothing Then
                Throw New Exception("Database.Tenant is Nothing")
            End If
        Catch ex As Exception
            Throw New Exception("Error accessing database - " + ex.Message)
        End Try

        Try
            grid.DataSource = d_("SELECT * FROM SketchStatusFlags")
            grid.DataBind()
        Catch ex As Exception
            Throw New Exception("Error on loading grid")
        End Try
    End Sub

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Dim UserName = Membership.GetUser().ToString
			Dim dcsUser As Boolean = True
			If UserName.ToLower <> "dcs-qa" And UserName.ToLower <> "dcs-rd" And UserName.ToLower <> "admin" And UserName.ToLower <> "dcs-support" And UserName.ToLower <> "dcs-ps" Then
				dcsUser = False
			End If
			If Roles.IsUserInRole("SVAdmin") Then
			Else
				If dcsUser = False Then
                	Response.Redirect("~/web/403.aspx")
				End If
			End If
			LoadFlags()
		End If
	End Sub

	Protected Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
		LoadFlags()
	End Sub

	Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
		For Each gr As GridViewRow In grid.Rows
			Dim id As String = gr.GetHiddenValue("FID")
			Dim colorCode As String = gr.GetTextValue("ColorCode")
            e_("UPDATE SketchStatusFlags SET ColorCode = '" + colorCode + "' WHERE Id = " & id)
		Next
		Alert("Changes has been Updated Successfully ")
		LoadFlags()		
	End Sub
End Class
