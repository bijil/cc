﻿Imports System.Security
Imports System.Linq
Imports System.Data.Linq

Partial Class svadmin_users
    Inherits System.Web.UI.Page

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Dim UserName = Membership.GetUser().ToString
			Dim dcsUser As Boolean = True
			If UserName.ToLower <> "dcs-qa" And UserName.ToLower <> "dcs-rd" And UserName.ToLower <> "admin" And UserName.ToLower <> "dcs-support" And UserName.ToLower <> "dcs-ps" Then
				dcsUser = False
			End If
			If Roles.IsUserInRole("SVAdmin") Then
			Else
				If dcsUser = False Then
                	Response.Redirect("~/web/403.aspx")
				End If
			End If
			LoadRolesList()
			BindGridView()
			'FullNameValidator.ErrorMessage = " *"
			'RequiredFieldValidator1.ErrorMessage = " *"
		    'UserNameRequiredFieldValidator.ErrorMessage = " *"
		    'rfvPassword.ErrorMessage = " *"
		    'rfvPasswordConfirm.ErrorMessage = " *"

		End If
	End Sub

	Protected Sub BindGridView()
		Dim cUsers = Membership.GetAllUsers
		Dim removeList As New List(Of String)
		'For Each u As MembershipUser In cUsers
		'	'If Roles.GetRolesForUser(u.UserName).Count > 0 Then
         '   If Not Roles.IsUserInRole(u.UserName, "SVAdmin") AndAlso Not Roles.IsUserInRole(u.UserName, "SVReviewer") AndAlso Not Roles.IsUserInRole(u.UserName, "SVReport") AndAlso Not Roles.IsUserInRole(u.UserName, "SVQC") Then
         '       removeList.Add(u.UserName)
         '   End If

			'End If
		'Next

		'For Each un In removeList
		'	cUsers.Remove(un)
		'Next

		If cUsers.Count = 0 Then
			Dim res As MembershipCreateStatus
			Membership.CreateUser("svadmin", "Reset123#", "svadmin@sketchvalidation.com", "@", "#", True, res)

            'Dim pr = Profile.GetProfile("svadmin")
            'pr.SetPropertyValue("FullName", "Administrator")
            'pr.Save()

			Roles.AddUserToRole("svadmin", "SVAdmin")

			If Not Database.Tenant.DoRowsExist("UserSettings", "LoginId = 'svadmin'") Then
				Database.Tenant.Execute("INSERT INTO UserSettings (LoginId, FirstName, Email) VALUES ('svadmin', 'SV Admin', 'svadmin@sketchvalidation.com')")
			End If

			BindGridView()
			Return
		End If
		Dim ut As DataTable = Database.Tenant.GetDataTable("SELECT * FROM UserSettings ORDER BY LoginID")
		For Each ur As DataRow In ut.Rows
			Dim uname As String = ur.GetString("LoginId")
			For Each u As MembershipUser In cUsers
				If u.UserName = uname Then
					u.Comment = ur.GetString("FirstName")
				End If
			Next
		Next


		gdvUserDetails.DataSource = cUsers
		gdvUserDetails.DataBind()
	End Sub


#Region "GridView LinkButtons Events"

#End Region

	 Private Sub gdvUserDetails_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gdvUserDetails.PageIndexChanging
        gdvUserDetails.PageIndex = e.NewPageIndex
        BindGridView()
  	 End Sub
	
	Protected Sub gdvUserDetails_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gdvUserDetails.RowCommand
		hdnUserId.Value = e.CommandArgument
		Select Case e.CommandName
			Case "DeleteUser"
				If e.CommandArgument = Membership.GetUser().UserName Then
					Alert("You cannot delete your own user account.")
					Return
				End If
				If e.CommandArgument = "svadmin" Then
					Alert("You cannot delete the default 'svadmin' user.")
					Return
				End If
				Membership.DeleteUser(e.CommandArgument)
				Database.Tenant.Execute("DELETE FROM UserTrackingCurrent WHERE LoginId = " + e.CommandArgument.ToString.ToSqlValue)
				Alert("User has been deleted.")
				BindGridView()
			Case "EditUser"
				LoadUserForEdit(e.CommandArgument)
			Case Else

		End Select
		'BindGridView()
	End Sub

	Protected Sub lbNewUser_Click(sender As Object, e As System.EventArgs) Handles lbNewUser.Click
		ClearForm()
	End Sub

	Sub ClearForm()
		hdnUserId.Value = ""
		txtFullName.Text = ""
		txtUserMail.Text = ""
		txtLoginId.Text = ""
		txtPassword.Text = ""
		txtPassword2.Text = ""
		txtLoginId.Enabled = True
		'rfvPassword.Enabled = True
		'rfvPasswordConfirm.Enabled = True
		cblRoles.Enabled = True
		For Each li As ListItem In cblRoles.Items
			li.Selected = False
		Next
		btnCancel.Text = "Cancel"
	End Sub

	Protected Sub btnSaveUser_Click(sender As Object, e As System.EventArgs) Handles btnSaveUser.Click
		Dim res As MembershipCreateStatus
		Dim userName As String = txtLoginId.Text
		Dim newUser As Boolean = False
		Try
			'If txtFullName.Text = "" Or txtUserMail.Text = "" Or txtLoginId.Text = "" Or txtPassword.Text = "" Or txtPassword2.Text = "" Then 
			'Alert("Please fill all the mandatory fields.")
			'Return
			'End If


			If hdnUserId.Value = "" Then
				Membership.CreateUser(userName, txtPassword.Text, txtUserMail.Text, "@", "#", True, res)
				If res = MembershipCreateStatus.Success Then
					SetProfileName(userName, txtFullName.Text)
					Database.Tenant.Execute("EXEC sv_UpdateUserSetting @LoginID = {0}, @FullName = {1}, @Email = {2}".SqlFormat(False, txtLoginId, txtFullName, txtUserMail))
					AssignRolesFromList(userName)
					ClearForm()
					BindGridView()
					RunScript("showMsg('New user has been created.', '1');")
					newUser = True
				Else
					If res = MembershipCreateStatus.DuplicateEmail Then
						RunScript("showMsg('This email address has been used for another account. Please try with a different email address.');")
						Return
					End If
					If res = MembershipCreateStatus.DuplicateUserName Then
						RunScript("showMsg('Login ID already exists. Please choose another one.');")
						Return
					End If
					If res = MembershipCreateStatus.InvalidUserName Then
						RunScript("showMsg('Login ID is invalid. Please choose another one.');")
						Return
					End If
					If res = MembershipCreateStatus.InvalidPassword Then
						RunScript("showMsg('Password must be minimum 6 characters in length.');")
						Return
					End If
					RunScript("showMsg('Error: " + res.ToString + "');")
					Return
				End If

			Else
				Dim u As MembershipUser = Membership.GetUser(userName)

				If txtPassword.Text.IsNotEmpty AndAlso txtPassword.Text = txtPassword2.Text Then
					If txtPassword.Text.Length < 6 Then
						RunScript("showMsg('Password must be minimum 6 characters in length.');")
						Return
					End If
					u.ChangePassword(u.GetPassword(), txtPassword.Text)
				End If

				u.Email = txtUserMail.Text
				Membership.UpdateUser(u)

				SetProfileName(userName, txtFullName.Text)
				AssignRolesFromList(userName)

				Database.Tenant.Execute("EXEC sv_UpdateUserSetting @LoginID = {0}, @FullName = {1}, @Email = {2}".SqlFormat(False, txtLoginId, txtFullName, txtUserMail))
				BindGridView()
				RunScript("showMsg('Changes updated successfully.\n\nClick Close to go back to the list.');")
			End If

		Catch ex As Exception
			RunScript("showMsg('Error: " + ex.Message + "');")
		End Try

	End Sub

	Sub SetProfileName(userName As String, fullName As String)
        'Dim pr = Profile.GetProfile(userName)
        'pr.SetPropertyValue("FullName", fullName)
        'pr.Save()
	End Sub

	Sub AssignRolesFromList(userName As String)
		For Each li As ListItem In cblRoles.Items
			If li.Selected Then
				If Not Roles.IsUserInRole(userName, li.Value) Then
					Roles.AddUserToRole(userName, li.Value)
				End If
			Else
				If Roles.IsUserInRole(userName, li.Value) Then
					Roles.RemoveUserFromRole(userName, li.Value)
				End If
			End If
		Next
	End Sub

	Sub LoadRolesList()
		Dim dtOrgRoles As DataTable = Database.System.GetDataTable("EXEC GetRoles 0, " + HttpContext.Current.GetCAMASession.OrganizationId.ToString)
		Dim roleNames As String() = dtOrgRoles.AsEnumerable.Select(Function(x) x.GetString("ASPRoleName")).ToArray
		For Each role In roleNames
			If Not Roles.RoleExists(role) Then
				Roles.CreateRole(role)
			End If
		Next
		For Each role In Roles.GetAllRoles()
			If Not roleNames.Contains(role) Then
				If Roles.GetUsersInRole(role).Count > 0 Then
					Roles.RemoveUsersFromRole(Roles.GetUsersInRole(role), role)
				End If
				Roles.DeleteRole(role)
			End If
		Next
        cblRoles.DataSource = Database.System.GetDataTable("EXEC GetRoles 2")
		cblRoles.DataTextField = "Name"
		cblRoles.DataValueField = "ASPRoleName"
		cblRoles.DataBind()
	End Sub

	Sub LoadUserForEdit(userName As String)
		hdnUserId.Value = userName
		Dim u As MembershipUser = Membership.GetUser(userName)
		Dim ur As DataRow = Database.Tenant.GetTopRow("SELECT * FROM UserSettings WHERE LoginId = " + userName.ToSqlValue)
		'txtFullName.Text = Profile.GetProfile(userName).FullName

		If ur IsNot Nothing Then
			txtUserMail.Text = ur.GetString("Email")
			txtLoginId.Text = ur.GetString("LoginId")
		Else
			txtFullName.Text = ""
		End If
		txtLoginId.Enabled = False
		'txtUserMail.Text = u.Email
		'txtLoginId.Text = u.UserName
		'rfvPassword.Enabled = False
		'rfvPasswordConfirm.Enabled = False

		If ur IsNot Nothing Then
			txtFullName.Text = ur.GetString("FirstName")
		End If

		For Each li As ListItem In cblRoles.Items
			If Roles.IsUserInRole(userName, li.Value) Then
				li.Selected = True
			Else
				li.Selected = False
			End If
		Next

		If userName = "svadmin" Then
			cblRoles.Enabled = False
		Else
			cblRoles.Enabled = True
		End If
		btnCancel.Text = "Close"
	End Sub

	Protected Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
		RunScript("hidePopup();")
	End Sub

End Class
