﻿
<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/DesktopWeb.Master" AutoEventWireup="false" Inherits="CAMACloud.Console.SearchParcelsSV" CodeBehind="SearchParcelsSv.aspx.vb" %>
<asp:Content runat="server" ContentPlaceHolderID="head">
    <link rel="Stylesheet" href="/App_Static/slider/winclassic.css" type="text/css" />
    <asp:Literal runat="server" Text="<style type='text/css'>" />
    <asp:Repeater runat="server" ID="rpFlagStyles">
        <itemtemplate>
            label[for="rblStatus_<%# Container.ItemIndex%>"]:before {
                    background: <%# Eval("ColorCode")%>;
                }
        </itemtemplate>
    </asp:Repeater>
    <asp:Literal runat="server" Text="</style>" />
<link rel="Stylesheet" type="text/css" href="/App_Static/css/Svo/SketchValidation.css" />
<script type="text/javascript" src="/App_Static/jslib/bootstrap-colorpicker.min.js"></script>
<link href="/App_Static/css/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css" />	
<%--<script type="text/javascript" src="/App_Static/js/qc/010-constants.js"></script>
<script type="text/javascript" src="/App_Static/slider/range.js"></script>
<script type="text/javascript" src="/App_Static/slider/timer.js"></script>
<script type="text/javascript" src="/App_Static/slider/slider.js"></script>--%>
<%--<script type="text/javascript" src="/App_Static/js/Svo/map.js?4"></script>--%>
<%--<script type="text/javascript" src="/App_Static/js/Svo/search.js?<%= Now.Ticks %>"></script>
<script type="text/javascript" src="/App_Static/js/qc/000-objects.js?636960322665895356"></script>
<script type="text/javascript" src="/App_Static/js/Svo/parcel.js?<%= Now.Ticks %>"></script>
<script type="text/javascript" src="/App_Static/js/Svo/sketch.js?148"></script>
<script type="text/javascript" src="/App_Static/js/Svo/lib/jsrepeater.js?12"></script>
<script type="text/javascript" src="/App_Static/js/qc/000-utillib.js"></script>
<script type="text/javascript" src="/App_Static/js/qc/010-conversions.js"></script>
<script type="text/javascript" src="/App_Static/js/system.js"></script>
<script type="text/javascript" src="/App_Static/js/sketch/sketchlib/base64.js"></script>--%>
<script type="text/javascript" src="/App_Static/jslib/sqllite.js"></script>
<link rel="Stylesheet" href="/App_Static/css/Svo/iphonestyle.css?1233" type="text/css" />
<script type="text/javascript" src="/App_Static/jslib/iphonestyle.js?1232"></script>
	<link rel="Stylesheet" href="/App_Static/slider/winclassic.css" type="text/css" />

<script type="text/javascript">
    $(document).ready(function () {
        loadEssentialTables()
    });

        

    var SvbulkeditSuccess = false
	var bulkedit = 0;
    var google = false;
    var smap, map;
    var theForm;
    var currentInput;
    var activeParcel;
    var Hammer = false;
    var __DTR = false;
    var appType = "sv";
    var activeTab;
    var isBeforeUnload = false;
    var ascParcel = true;
    var ascAddress = true;;
    var countyName = '<%= organizationName %>'
    var _organizationId = '<%=  _organizationId %>'
	var stateName = '<%= stateName%>'
    var TrueFalseInPCI = '<%= DoNotChangeTrueFalseInPCI %>'
    function clearVectorlabelPosition() {
        vectorLabelPositions = [];
    }
</script>

	<style>
	
::-webkit-scrollbar
{
width:15px;
height:12px;
}
.mGrid{
margin-top: 0px !important;
}
 .hiddencol
  {
    display: none;
        
  }
  .style3{
  display: none;
  }
  @media (min-width: 1339px) and (max-width: 1399.98px) {
}
  @media screen and (max-width: 1400px) {
  .divSearchBtmtableContainer{
	  width:74%!important;
  }

}
	</style>
	<script type="text/javascript">




        //function adminPageFunctions() {
        //    $('.colorpicker').colorPicker();
        //}

        $(function () {
            $("#spanMainHeading").html('Search Parcels');
        })

        $(function () {
            $('.pickcolor').colorpicker();
        });
        function CheckFields() {
            $('.cbPrimary input').each(function () {
                $(this).change(function () {
                    checboxselected();
                });
            });
        }
        function checboxselected() {
            var total = 0
            total = $('input:checked').length;
            if (total > 4) {
                $('.cbPrimary input').attr('disabled', 'disabled');
                $('.cbPrimary input:checked').removeAttr('disabled');
                return false;
            }
            else {
                $('.cbPrimary input').removeAttr('disabled');
            }
        }
    </script>
<%--<script type="text/javascript" src="/App_Static/js/qc/010-quality.js?<%= Now.Ticks %>"></script>--%>

</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="SwitchMode">
	<div id = "lightWeight" runat="server" class="switchLNMode tip" style="display:none;" abbr="The Light mode is used to reduce the load in SV. This will enable only limited functionalities.</br>
We will be able to access only the MAP and its sketch editor , once we enable the SV LM.</br> Light Mode shows shapefile view of sketch after save" data-mode="left" >
		<label class="switch">
			<input class="switch-input" type="checkbox" onchange="return enableLightMode();" />
			<span class="switch-label" data-on="LM" data-off="NM"></span> 
			<span class="switch-handle"></span> 
		</label>
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<input type="hidden" id="sketchid" name="sketchid" value="">
	<span class = "showSidePanel"></span>
	<div id="divSearchCntnr">
		<div id="divSearchTop">
			<span class= "hideSidePanel"></span>		
		</div>
		<div id="divSearchTop1" Style="display:none">
		</div>
		<div class="addclrfltr" style="margin-right: 0px;">
			<button class="addfilter" style="border-left-width: 2px;  color: #167ccf; border: none; background: none;">Add Filter</button>
			<button class="clearfilter" style="border-left-width: 2px;  color: #167ccf; border: none; background: none;">Clear Filter</button>
			<%--<a class="addfilter">Add Filter</a>
			<a class="clearfilter"> Clear Filter</a>--%>
		</div>
		<div id="section1" style="display: none;">
    		<h2 style="font-size: 20px;text-decoration: underline;">Add New Filter</h2>
			<div style="display:flex;margin-top: 13px;">
					<div style="flex:50%;padding-top: 0px;">
							<h2 >Search By</h2>
					</div>
					<div style="flex:55%;margin-right: 20px;">
						 <div class="eachDivL1" style="width: 112%;">
						 <asp:DropDownList runat="server" ID="ddlUsersSV" CssClass="ddlUsersSV srchSlct">
						 </asp:DropDownList>
						</div>
					</div>
			</div>
				
				<div style="display:flex;padding-top: 4px;">
					<div style="flex:50%">
					 	<h2 >Filter condition</h2>
					</div>				
					<div style="flex: 64%;margin-right: -5px;">
						<div style="width: 97%;">
							<asp:DropDownList runat="server" ID="Operator" CssClass="Operator srchSlct" AppendDataBoundItems="true">
								<asp:ListItem Text="--Select--" Value="select"></asp:ListItem>
								<asp:ListItem Text="Contains" Value=" LIKE "></asp:ListItem>
								<asp:ListItem Text="Equal to" Value=" = "></asp:ListItem>
								<asp:ListItem Text="Not equal to" Value=" <> "></asp:ListItem>
								<asp:ListItem Text="Starting with" Value="startwith"></asp:ListItem>
								<asp:ListItem Text="Not starting with" Value="notstrtwith"></asp:ListItem>
								<asp:ListItem Text="Ending with" Value="endwith"></asp:ListItem>
								<asp:ListItem Text="Less Than" Value=" < "></asp:ListItem>
								<asp:ListItem Text="Greater Than" Value=" > "></asp:ListItem>
								<asp:ListItem Text="Less than or equal" Value=" <= "></asp:ListItem>
								<asp:ListItem Text="Greater than or equal" Value=" >= "></asp:ListItem>
								<asp:ListItem Text="Is NOT NULL" Value=" IS NOT NULL "></asp:ListItem>
								<asp:ListItem Text="Is NULL" Value=" IS NULL "></asp:ListItem>
								
							</asp:DropDownList>
						</div>
					</div>
				</div>
				<div style="display:flex;padding-top: 4px;padding-bottom: 7px;">
					<div style="flex:50%;">
						<h2 >ID/Value</h2>
					</div>
					<div style="flex:50%;margin-right: -5px;">
						<div class="eachDivR"style="width:112%">
							<input id="SrchId" type="text" autocorrect="off" autocapitalize="off" maxlength="50" class="srchtxt" />
							<select id="SrchId2" type="text" autocorrect="off" autocapitalize="off" maxlength="50" class="srchtxt" style="display: none;"></select>
						</div>
					</div>
				</div>
				<div class="divbtn">
					<input type="button" class="btnrch" value="Close" id="closefilter" style="float: right;margin-right: 22px;"></button>
					<input type="button" value="Add Filter" class="btnrch" id="addcstmfltr" style="float: right;margin-right: 10px;width: 81px;"></input>
			</div>
		</div>
		<div id="section2">
			<div class="eachDivR">
				<h2 >Reviewed By</h2>
				<asp:DropDownList runat="server" ID="ddlUsers" CssClass="ddlUsers srchSlct" AppendDataBoundItems="true">
					<asp:ListItem Text="All" Value="all"></asp:ListItem>
					<asp:ListItem Text="Is NULL" Value="None"></asp:ListItem>					
				</asp:DropDownList>
			</div>			
			<div class="eachDivL2">
				<h2>Flag Status</h2>
				<asp:DropDownList runat="server" ID="SrchFlag" CssClass="SrchFlag srchSlct" AppendDataBoundItems="true">
					<asp:ListItem Text="All" Value="all"></asp:ListItem>
					<asp:ListItem Text="Field Inspection" Value="1"></asp:ListItem>
					<asp:ListItem Text="Data Entry" Value="2"></asp:ListItem>
					<asp:ListItem Text="Further Review" Value="3"></asp:ListItem>
					<asp:ListItem Text="Complete" Value="4"></asp:ListItem>
					<asp:ListItem Text="Quality Control" Value="5"></asp:ListItem>
					<asp:ListItem Text="Internal Question" Value="6"></asp:ListItem>
					<asp:ListItem Text="Is NULL" Value="7"></asp:ListItem>
					<asp:ListItem Text="Is Not NULL" Value="8"></asp:ListItem>				
				</asp:DropDownList>
			</div>
			<div class="divbtn">
				<button class="btnrch" id="btnSearchSvBlk" >Search</button>
				<button class="btnrch" id="btnClear" >Clear</button>
				<button class="btnrch" id="btnRefresh">Refresh</button>
			</div>
		</div>
	</div>
	<div id="divContents" style="float: left;border-left: 1px solid #ddf2ff;border-right:4px solid #ddf2ff; height: 100%; width: 65%">
		<div id="divSearchBtm" style="height: 100%;" class="divSearchBtmtableContainer">
			<table class="tablesrch" id="TabSearchContentsHd">
				<thead>
					<tr>
						<td width="20%"></td>
						<td  width="40%"><span class="hdspn">Parcel Id</span></td>
						<td  width="40%"><span class="hdspn">Address</span></td>
						<td width="20%"></td>
					</tr>
				</thead>
			</table>
			<div id="divtab">
			<table class="tablesrch" id="TabSearchContents" >
				<tbody>
					<tr>
						<td colspan="3" class="TdNodata" >No Data Available</td>
					</tr>
				</tbody>
			</table>
		</div>
<div id="divprclcnt" class="parcel-info">
    <b>Total Number of Parcels:
    <label id="lblCntParcl"></label></b>


<div class="search-container" style="display:none">
    <div class="pagination-controls">
        <span id="btnPreviousPage" class="pagination-button">Previous</span>
        <select id="pageSelector" class="page-selector"></select>
        <span id="btnNextPage" class="pagination-button">Next</span>

        <label for="recordsPerPageSelector" class="page-size-label">Page</label>
        <select id="recordsPerPageSelector" class="records-per-page-selector">
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="75">75</option>
            <option value="100">100</option>
			<option value="500">500</option>
			<option value="1000">1000</option>
        </select>
    </div>
</div>
			</div>		</div>
	</div>
	<div id="divRghtCntnr" >
	<div id="divSketchReview" style="height:72%; overflow-y:auto;">
	<img src="/App_Static/css/images/close.png" class="TrnsbackClose" onclick="return closeParcel(true)" Search By*="close parcel">
	<div id="divRhtHead"><label class="LblRghthd">Select the Sketch Validation Status that the parcels will be updated to. </label> </div>
	<div class="aux-data"></div>
	<div id="divRighCntnts"style>
	<asp:RadioButtonList runat="server" ID="rblStatus" style="width:100%; align-items:center; font-weight: 500;" RepeatColumns="1"
	RepeatDirection="Horizontal" ClientIDMode="Static">
	<asp:ListItem Text="Field Inspection" Value="1" />
	<asp:ListItem Text="Data Entry" Value="2" />
	<asp:ListItem Text="Further Review" Value="3" />
	<asp:ListItem Text="Completed" Value="4" />
	<asp:ListItem Text="Internal Question" Value="6" />
	<asp:ListItem Text="Reset SV Status" Value="7" />
	</asp:RadioButtonList>

	<asp:RadioButtonList runat="server" ID="rblQc" RepeatDirection="Horizontal" ClientIDMode="Static">
    <asp:ListItem Text="QC Passed" Value="5" />
    </asp:RadioButtonList>	
	<%--<div class="otherflags" id="divOtherFlags" style="min-height:20px;" runat="server">
	<span style="float:left;width:100%"><h2>Other flags :</h2></span>
	<table class="flag-fields">
	<asp:Repeater runat="server" ID='rptOtherFlags'>
	<ItemTemplate>
	<tr>
	<td >
	<%#Eval("Name") %>
	</td>
	<td style="width: 50px;">
	<input type="checkbox" flag='<%# Eval("Id") %>' style="width: 40px;" />
	</td>
	</tr>
	</ItemTemplate>
	</asp:Repeater>
	</table>
	
	</div>--%>
	<div id="divRightButtons">
		<a class="svBulksavebtn" id="bulkSaveBtnSV" >Bulk Edit</a>
	</div>
	</div>
	</div>
	</div>
	</asp:Content>
	<asp:Content ID="Content4" ContentPlaceHolderID="FootContents" runat="server">
	<script type="text/javascript" src="/App_Static/js/Svo/svbulkeditor.js?<%= Now.Ticks %>"></script>
</asp:Content>