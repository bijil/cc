﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/DesktopWeb.master"
	AutoEventWireup="false" Inherits="CAMACloud.Console.svadmin_users" Codebehind="users.aspx.vb" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="Server">
	
	<style type="text/css">
		.ui-dialog-titlebar{
			width:572px;
        }

        .required{
   	    	color:red;
		}

		.masklayer {
            z-index: 1003 !important;
        }
    </style>
	
	<script type="text/javascript">

		function showPopup(title) {
			$('.edit-popup').dialog({
				modal: true,
				width: 600,
				resizable: false,
				title: title,
				open: function (type, data) {
					$(this).parent().appendTo("form");
				},
                close: function () {
                    $('.txtContrl').val('');
					$('#MainContent_cblRoles_0').prop('checked', false);
					$('#MainContent_cblRoles_1').prop('checked', false);
					$('#MainContent_cblRoles_2').prop('checked', false);
                    $('#MainContent_cblRoles_3').prop('checked', false);
                },
			});
			$('.ui-dialog').css({'left':'30%','top':'30%'});
		}

		function hidePopup() {
			$('.edit-popup').dialog('close');

		}

		function setPageLayout(w, h) {
		    $('.screen').height(h); 
		    $('.admin-container-cell').height(h - $('.page-title').height() - 10);
		    $('.screen').css({ 'overflow-y': 'auto' });
		    $('.admin-container-cell').css({ 'overflow-y': 'auto' });
		}
		function confirmAlert(){
			 var password = $("#" + '<%= txtPassword.ClientID%>').val();
			 var confPass = $("#" + '<%= txtPassword2.ClientID%>').val();
			 var mail = $( "#" + '<%= txtUserMail.ClientID%>' ).val();            
	         var remail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	         var fname = $( "#" + '<%= txtFullName.ClientID%>' ).val(); 
	         var logId = $( "#" + '<%= txtLoginId.ClientID%>' ).val(); 
			 var hdnVal = $("#" + '<%= hdnUserId.ClientID%>').val();
			 if(!fname){
            	alert("Name is required, please enter your name!");
            	return false;
             }
             if(!mail){
            	alert("Email Address is required, please enter a valid email address!")
            	return false;
             }
             else if(!remail.test(String(mail).toLowerCase())) {
	            alert("Invalid Email!");
	            return false;
           
             } 
			 if(!logId) {
			 	alert("Login Id is required, please enter Login Id!");
            	return false;
			 }           
			 
			 if(hdnVal != ""){
				
			   	if(password!="" || confPass!= ""){	
                  
				
	            	if(password && password.trim()==""){
	            		alert("Blank space is not allowed, please enter a valid password!.");
	                	return false;
	            	}
	            	if(password && password.length<6){ 
	            		alert("Password must be minimum 6 characters in length.");
	                	return false;
	            	}
	            	if ( !confPass || (password != confPass) ) {
	                	alert("Password does not match the confirm password.");
	               		return false;
	            	}
	          	}
	         }
	         else {
	            if (password != confPass) {
	              	alert("Password does not match the confirm password.");
	               	return false;
				 }
			}
            
			showMask();
		}

		function showMsg(msg, hp) {
			hideMask();
			if (hp == '1') hidePopup();
			alert(msg);
        }

		$(function () {
			$("#spanMainHeading").html('Manage Users');
		})
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainContent" runat="Server">
	<div style="float:left;width:94%;margin-left:3%;margin-top: 1%;">
	<asp:UpdatePanel runat="server" ID="uplGrid">
		<ContentTemplate>
			<div class="link-panel">
				<asp:LinkButton runat="server" ID="lbNewUser" Text="Add New User" OnClientClick="showPopup('Create new CAMA user');" style="padding:4px 10px"/>
			</div>
			<asp:GridView ID="gdvUserDetails" runat="server" AutoGenerateColumns="false" AllowPaging="true"  PageSize="50">
				<Columns>
					<asp:TemplateField>
						<ItemStyle Width="25px" />
						<ItemTemplate>
							<%# Container.DataItemIndex + 1 %>.
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Full Name">
						<ItemStyle Width="250px" />
						<ItemTemplate>
							<%# Eval("Comment")%>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField HeaderText="Login Id" DataField="UserName" ItemStyle-Width="120px">
						<ItemStyle Width="120px"></ItemStyle>
					</asp:BoundField>
					<asp:TemplateField HeaderText="Last Login">
						<ItemStyle Width="100px" />
						<ItemTemplate>
							<%# Eval("LastLoginDate", "{0:MMM d, yyyy}")%>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="Last Login IP" ItemStyle-Width="120px" Visible="false">
						<ItemTemplate>
							
						</ItemTemplate>
						<ItemStyle Width="120px"></ItemStyle>
					</asp:TemplateField>
					<asp:TemplateField>
						<ItemStyle Width="90px" />
						<ItemTemplate>
							<asp:LinkButton ID="LinkButton1" runat="server" CommandName="EditUser" Text="Edit"
								CommandArgument='<%# Eval("UserName") %>' OnClientClick="showPopup('Edit User Properties')" />
							<asp:LinkButton ID="lbtnDeleteUser" Text="Delete" runat="server" CommandName="DeleteUser" OnClientClick="return confirm('Are you sure that you want to delete this user?');"
								CommandArgument='<%# Eval("UserName")%>' CausesValidation="false"></asp:LinkButton>
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
			</asp:GridView>
		</ContentTemplate>
	</asp:UpdatePanel>
	<div class="edit-popup" style="display: none;">
		<asp:UpdatePanel ID="uplEdit" runat="server">
			<ContentTemplate>
				<asp:Panel runat="server" ID="pnlUserEdit">
					<asp:HiddenField ID="hdnUserId" runat="server" />
					<table class="user-edit-table">
						<tr>
							<td style="width: 8px;"></td>
							<td>
								<h3>
									User Properties</h3>
								<table class="user-edit-form">
									<tr>
										<td>Full Name:<span class="required">*</span></td>
										<td>
											<asp:TextBox ID="txtFullName" runat="server" autocomplete="off" class="txtContrl"></asp:TextBox> 
											 <%-- <asp:RequiredFieldValidator
												ID="FullNameValidator" runat="server" ForeColor="Red" ErrorMessage="*" ControlToValidate="txtFullName"
												ValidationGroup="UserProp"></asp:RequiredFieldValidator>--%>
										</td>
									</tr>
									<tr>
										<td>Email:<span class="required">*</span></td>
										<td>
											<asp:TextBox ID="txtUserMail" runat="server" autocomplete="off" class="txtContrl"></asp:TextBox>
											 <%--<asp:RequiredFieldValidator
												ID="RequiredFieldValidator1" runat="server" ForeColor="Red" ErrorMessage="*"
												ControlToValidate="txtUserMail" Display="Dynamic" ValidationGroup="UserProp"></asp:RequiredFieldValidator>--%>
											<%--<asp:RegularExpressionValidator ID="rgeEmailValidator" runat="server" ControlToValidate="txtUserMail"
												ErrorMessage="Invalid Email!" ForeColor="#D20B0E" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
												Display="Dynamic" ValidationGroup="UserProp"></asp:RegularExpressionValidator>--%>
										</td>
									</tr>
									<tr>
										<td>Login ID:<span class="required">*</span></td>
										<td>
											<asp:TextBox ID="txtLoginId" runat="server" autocomplete="off" class="txtContrl"></asp:TextBox>
											 <%--<asp:RequiredFieldValidator ID="UserNameRequiredFieldValidator" runat="server" ForeColor="Red" ErrorMessage="*"
												ControlToValidate="txtLoginId" Display="Dynamic" ValidationGroup="UserProp"></asp:RequiredFieldValidator>--%>
										</td>
									</tr>
									<tr>
										<td>Password:<span class="required">*</span></td>
										<td>
											<asp:TextBox ID="txtPassword" TextMode="Password" runat="server" autocomplete="off" class="txtContrl"></asp:TextBox>
											 <%-- <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ForeColor="Red" ErrorMessage="*" ControlToValidate="txtPassword"
												Display="Dynamic" ValidationGroup="UserProp"></asp:RequiredFieldValidator>--%>
										</td>
									</tr>
									<tr>
										<td>Confirm Password:<span class="required">*</span></td>
										<td>
											<asp:TextBox ID="txtPassword2" TextMode="Password" runat="server" autocomplete="off" class="txtContrl"></asp:TextBox>
											 <%-- <asp:RequiredFieldValidator ID="rfvPasswordConfirm" runat="server" ForeColor="Red" ErrorMessage="*" ControlToValidate="txtPassword2"
												Display="Dynamic" ValidationGroup="UserProp"></asp:RequiredFieldValidator>--%>
											<%--<asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassword"
												ControlToValidate="txtPassword2" ErrorMessage="Mismatch!" ForeColor="#D20B0E"
												ValidationGroup="UserProp"></asp:CompareValidator>--%>
										</td>
									</tr>
								</table>
								<div style="margin-top: 10px; margin-bottom: 15px;">
									<asp:Button runat="server" ID="btnSaveUser" Text="Save User" OnClientClick="return confirmAlert();" ValidationGroup="UserProp" />
									<asp:Button runat="server" ID="btnCancel" Text="Cancel" />
								</div>
							</td>
							<td class="v-split" style="width: 12px;"></td>
							<td style="width: 143px;vertical-align:top;">
								<h3>
									Select Roles</h3>
								<asp:CheckBoxList ID="cblRoles" runat="server" >
								</asp:CheckBoxList>
							</td>
						</tr>
					</table>
				</asp:Panel>
			</ContentTemplate>
		</asp:UpdatePanel>
	</div>
	</div>
</asp:Content>
