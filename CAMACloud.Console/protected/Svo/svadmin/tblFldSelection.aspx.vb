﻿Public Class tblFldSelection
    Inherits System.Web.UI.Page
    Public Shared ValCheckFlag As Integer = 0 
   
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim UserName = Membership.GetUser().ToString
			Dim dcsUser As Boolean = True
            If UserName.ToLower <> "dcs-qa" And UserName.ToLower <> "dcs-rd" And UserName.ToLower <> "admin" And UserName.ToLower <> "dcs-support" And UserName.ToLower <> "dcs-ps" Then
                dcsUser = False
            End If
            If Roles.IsUserInRole("SVAdmin") Then
            Else
				If dcsUser = False Then
                	Response.Redirect("~/web/403.aspx")
				End If
            End If
            Dim table As DataTable = Database.Tenant.GetDataTable("SELECT Id,Name FROM DataSourceTable WHERE ImportType = 1")
            ddlTable.FillFromTable(table, True)
        End If
    End Sub
    
     Protected Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
     	Dim _tableId As String = ""
     	
     	If Not IsPostBack Then
     		Dim _value As String  = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name = 'SVAuxData'")
     		If _value <> "" Then
     			Dim _array As String()
     			Dim _tableName As String
    			_array = _value.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
    			_tableName = _array(0)
    			If _tableName <> "" Then
    				_tableId = Database.Tenant.GetStringValue("SELECT TableId from DataSourceField WHERE SourceTable='" + _tableName + "'")
    			End If
			End If
     	End If
     	
     	If IsPostBack Or _tableId <> "" Then
    		LoadFields(_tableId)
    		RunScript("CheckFields();")
    	Else
    		ClearForm()
    	End If
    	    RunScript("checboxselected();")
     End Sub
     
     Private Sub LoadFields( Optional ByVal _tableName As String = "")
     	Dim tableid As String
     	
     	If( _tableName = "" )
     		tableid  = ddlTable.SelectedValue.ToString()
     	Else
     		tableid = _tableName
     	End If
    	
    	If (tableId <> "") Then
        	Dim dtTable As New DataTable
        	Dim sqlstr As String
        	Dim array As String()
        	Dim tableName
        	Dim fields As Object = ","
        	ValCheckFlag = 0
			Dim value As String  = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name = 'SVAuxData'")
			If value <> "" Then
        		array = value.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
        		tableName = array(0)
        		For ar As Integer = 1 To array.Count - 1
        			fields += array(ar) + ","
        		Next
			Else
				ValCheckFlag = 1
        	End If
        	sqlstr = "SELECT Id,TableId, Name as FieldName, DisplayLabel, SourceTable, Null As Status from DataSourceField WHERE TableId=" + tableId + " ORDER BY Id"
        	dtTable = Database.Tenant.GetDataTable(sqlstr)        	
        	If dtTable IsNot Nothing Then
        		For i As Integer = 0 To dtTable.Rows.Count - 1
        			Dim dt = dtTable.Rows(i)
        			Dim field As String = dt.GetString("Fieldname")
        			Dim display As  String = dt.GetString("DisplayLabel")
        			Dim table As String = dt.GetString("SourceTable")
        			If fields IsNot Nothing Then
        				If (fields.Contains("," & field & ",") And tableName=table) Or (fields.Contains("," & display & ",") And tableName=table) Then
        					dtTable.Rows(i)("status") = True
        				End If
    				End If
        		Next
            	gvDataSourceTable.DataSource = dtTable
            	gvDataSourceTable.DataBind()
            	btnSave.Visible = True
            	btnCancel.Visible = True
        	End If
        	If  _tableName <> "" Then
        		ddlTable.SelectedValue = _tableName
        	End If
		Else
			ClearForm()
        End If
    End Sub
    
    Private Sub ClearForm()
			gvDataSourceTable.DataSource = Nothing
			gvDataSourceTable.DataBind()
			btnSave.Visible = False
			btnCancel.Visible = False
    End Sub
    
    Protected Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
		ClearForm()
	End Sub

	Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
		Dim val As String = ddlTable.SelectedItem.Text
		Dim tableid As String = ddlTable.SelectedValue.ToString()
		Dim selectTable As String = ddlTable.SelectedItem.Text
		Dim values As [String] = ""
		Dim dvalues As [String] = ""
		Dim dtTable As New DataTable
		Dim array As String()
        Dim tableName
		Dim value As String  = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name = 'SVAuxData'")
		If value <> "" Then
        		array = value.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
        		tableName = array(0)
        End If
		Dim i As Integer = 0
		For Each row As GridViewRow In gvDataSourceTable.Rows
			Dim chkRow As CheckBox = TryCast(row.Cells(0).FindControl("CheckBox1"), CheckBox)
			If chkRow.Checked Then
            	values += row.Cells(2).Text + ","
            	dvalues += row.Cells(1).Text + ","
            	i = i+1
            End If
            If i > 5 Then
            	Return
            End If
		Next
		If (i = 0 AND ValCheckFlag=1) Then
			Alert("Please select any field")
		ElseIf (i = 0 AND ValCheckFlag=0 AND selectTable <> tableName) Then
			Alert("Please select any field") 
		ElseIf i = 0 Then
			Database.Tenant.Execute("Delete ClientSettings WHERE Name = 'SVAuxData'")
			Database.Tenant.Execute("Delete ClientSettings WHERE Name = 'SVAuxDisplayData'")
			Alert("Configuration removed succesfully")
		Else
			values = val + "," + values
			values = values.TrimEnd(","c)
			dvalues = dvalues.TrimEnd(","c)
        	Database.Tenant.Execute("if exists(SELECT * FROM ClientSettings WHERE Name = 'SVAuxData') UPDATE ClientSettings SET Value = '" & values & "' WHERE Name = 'SVAuxData' else INSERT INTO ClientSettings(name,Value) Values('SVAuxData','" + values + "')")
        	Database.Tenant.Execute("if exists(SELECT * FROM ClientSettings WHERE Name = 'SVAuxDisplayData') UPDATE ClientSettings SET Value = '" & dvalues & "' WHERE Name = 'SVAuxDisplayData' else INSERT INTO ClientSettings(name,Value) Values('SVAuxDisplayData','" + dvalues + "')")
        	LoadFields()
			Alert("Configuration added succesfully")
		End If
	End Sub
End Class