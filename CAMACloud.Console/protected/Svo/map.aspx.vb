﻿
Partial Class sketchv_map
    Inherits System.Web.UI.Page
    Public NearmapAPIKey As String
	Public NearmapAccess As String
	Public NearmapWMSAPIKey As String
	Public NearmapWMSAccess As String
	Public woolpertApikey As String
	Public woolpert6inchId As String
	Public woolpert3inchId As String
	Public woolpertAccess As String
	Public customWMS As String
	Public customMapIsImage As String
	Public customWMSAccess As String
	Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
		Response.Cache.SetNoStore()
		NearmapAPIKey = ClientSettings.PropertyValue("Nearmap.APIKey")
		NearmapAccess = ClientSettings.PropertyValue("EnableNearmap")
		NearmapWMSAPIKey = ClientSettings.PropertyValue("NearmapWMS.APIKey")
		NearmapWMSAccess = ClientSettings.PropertyValue("EnableNearmapWMS")
		woolpertApikey = ClientSettings.PropertyValue("woolpertApikey")
        woolpert6inchId = ClientSettings.PropertyValue("woolpert6inchId")
        woolpert3inchId = ClientSettings.PropertyValue("woolpert3inchId")
        woolpertAccess = ClientSettings.PropertyValue("EnableWoolpert")
        customWMS = ClientSettings.PropertyValue("customWMS")
        customMapIsImage = ClientSettings.PropertyValue("customMapIsImage")
        customWMSAccess = ClientSettings.PropertyValue("EnableCustomWMS")
	End Sub
End Class
