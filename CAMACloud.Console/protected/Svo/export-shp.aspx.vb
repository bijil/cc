﻿Imports CAMACloud.WebService.ShapeFileFactory

Public Class export_shp
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            RefreshGrid()
        End If
    End Sub

    Private Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        If Database.System.GetIntegerValue("SELECT COUNT(*) FROM ExportJobs WHERE OrganizationID = {0} AND ISNULL(Status,0) IN (0, 1)".SqlFormat(False, Database.Tenant.OrganizationId)) > 0 Then
            Me.Alert("Another export job is pending/processing. Please wait till the job is completed or cancelled.")
            Exit Sub
        End If
        Database.System.Execute("INSERT INTO ExportJobs (OrganizationID, JobType) VALUES ({0}, 'shp')".SqlFormat(False, Database.Tenant.OrganizationId))
        Response.Redirect(Request.Url.PathAndQuery)
        'Dim fileName = "temp"

        'Dim parcels As DataTable = Database.Tenant.GetDataTable("SELECT Id, KeyValue1, StreetAddress FROM Parcel WHERE CC_GeoReferencedBy IS NOT NULL")
        'Dim shapeData = Database.Tenant.GetDataTable("SELECT CC_ParcelId, VectorNumber, Label, Points FROM SV_GIS_Vectors ORDER BY CC_ParcelId, VectorNumber")

        'Dim vectors = shapeData.AsEnumerable.Select(Of GIS_Sketch_Vector)(Function(x) New GIS_Sketch_Vector() With {.Points = x.GetString("Points"), .Label = x.GetString("Label"), .ParcelId = x.GetInteger("CC_ParcelId"), .VectorNumber = x.GetInteger("VectorNumber")})
        'Dim shp As New GISShapeFile()
        'shp.KeyValue = fileName

        'For Each pr As DataRow In parcels.Rows
        '    Dim parcelId As Integer = pr.GetInteger("Id")
        '    'Dim keyValue As String = pr.GetString("KeyValue1")
        '    'Dim streetAddress As String = pr.GetString("StreetAddress")


        '    Dim vdata = vectors.Where(Function(x) x.ParcelId = parcelId).OrderBy(Function(x) x.VectorNumber).ToList
        '    Dim parcelSketches As New GISShapeGroup
        '    For Each v In vdata
        '        Dim vector As New GISShape
        '        For Each p In v.Points.Split(";")
        '            Dim coord = p.Split(",").ToArray
        '            vector.AddVertex(coord(1), coord(0))
        '        Next
        '        parcelSketches.AddShape(vector)
        '    Next
        '    shp.AddSketch(parcelSketches)
        'Next

        'Dim ms As New IO.MemoryStream
        'shp.Save(ms, parcels)
        'Dim shpBase64 = Convert.ToBase64String(ms.ToArray)

        'Response.Clear()
        'Response.ContentType = "application/octet-stream"
        'Response.AppendHeader("Content-Disposition", "attachment; filename=""" & fileName & ".zip""")
        'shp.Save(Response.OutputStream)
        'Response.Flush()
        'Response.End()

    End Sub


    Public Sub RefreshGrid()
        Dim sql As String = "SELECT *, dbo.GetLocalDate(CreatedDate) As RequestDate, CASE Status WHEN 9 THEN 'Error!' WHEN 3 THEN 'Cancelled' WHEN 2 THEN 'Completed' WHEN 1 THEN 'Processing' ELSE 'Pending' END AS StatusText  FROM ExportJobs WHERE JobType = 'shp' AND OrganizationId = " + Database.Tenant.OrganizationId.ToString + " AND CreatedDate > DATEADD(day, -7, GETUTCDATE()) ORDER BY CreatedDate DESC"
        grid.DataSource = Database.System.GetDataTable(sql)
        grid.DataBind()
    End Sub

End Class

Public Class GIS_Sketch_Vector
    Public Property ParcelId As Integer
    Public Property VectorNumber As Integer
    Public Property Label As String
    Public Property Points As String
End Class