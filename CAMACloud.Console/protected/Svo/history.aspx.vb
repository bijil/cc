﻿
Partial Class history
	Inherits System.Web.UI.Page

Sub LoadHistory()
		Dim dt As DataTable = d_("SELECT * FROM Application WHERE ShowKeyValue1 = 0 AND ShowAlternateField = 1")
		Dim condition As String = "p.KeyValue1"
		If dt.Rows.Count = 1 Then
			Dim alternateField = dt.Rows(0)("Alternatekeyfieldvalue").ToString()
			condition = "pd."+alternateField
		End If
		rpAudit.DataSource = d_("SELECT TOP 50 pa.*, dbo.GetLocalDate(EventTime) As ReviewTime, p.StreetAddress,CASE WHEN p.IsPersonalProperty = 1 AND p.KeyValue1 = '0' THEN cast(pd.ClientROWUID as varchar(50)) ELSE cast(" + condition + " as varchar(50)) END AS  KeyValue1 FROM ParcelAuditTrail pa LEFT OUTER JOIN Parcel p ON pa.ParcelId = p.Id JOIN ParcelData pd ON pd.CC_ParcelId=p.Id  WHERE EventType = 10 ORDER BY EventTime DESC")
		rpAudit.DataBind()
		lblAuditCount.Text = rpAudit.Items.Count
	End Sub

	Protected Sub btnRefreshHistory_Click(sender As Object, e As System.EventArgs) Handles btnRefreshHistory.Click
		LoadHistory()
		RunScript("loadinghide()")
	End Sub

	Dim lastDate As String = ""
	Public Function ShowDate() As String
		Dim dt As String = CDate(Eval("ReviewTime")).ToString("MMM d, yyyy")
		If dt <> lastDate Then
			lastDate = dt
			Return "<div class='date-header'>" + dt + "</div>"
		End If
		lastDate = dt
		Return ""
	End Function

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Dim UserName = Membership.GetUser().ToString
			Dim dcsUser As Boolean = True
			If UserName.ToLower <> "dcs-qa" And UserName.ToLower <> "dcs-rd" And UserName.ToLower <> "admin" And UserName.ToLower <> "dcs-support" And UserName.ToLower <> "dcs-ps" Then
				dcsUser = False
			End If
			If Roles.IsUserInRole("SVReviewer") Then
			Else
				If dcsUser = False Then
                	Response.Redirect("~/web/403.aspx")
				End If
			End If
			LoadHistory()
		End If
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class
