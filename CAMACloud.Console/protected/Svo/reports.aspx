﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/DesktopWeb.master"
	AutoEventWireup="false" Inherits="CAMACloud.Console.reports" Codebehind="reports.aspx.vb" %>
<asp:Content runat="server" ContentPlaceHolderID="head">
<link rel="Stylesheet" type="text/css" href="/App_Static/css/Svo/SketchValidation.css" />
<link rel="Stylesheet" href="/App_Static/css/Svo/reports.css?<%=Now.Ticks%>" type="text/css" />

	<script type="text/javascript">
		var CAMACloud = {};
		CAMACloud.Sketching = {};
		CAMACloud.Sketching.Formatters = {};
		CAMACloud.Sketching.Encoders ={};
		var sketchconfig = '<%=  sketchconfig %>'
		
	</script>
	<cc:JavaScript ID="JavaScript1" runat="server" IncludeFolder="/App_Static/js/sketch/sketchlib/" />
    <style>
        .report-filter-container {
           padding-bottom:10px;
        }
        .b{
            padding-bottom:5px;
        }
        #ctl00_MainContent_viewer_AsyncWait_Wait
        {
        	width : 130px;
        }
    </style>
	<script type="text/javascript">

		var rlayout;
		var sht;
		var validDate = true;
		$( document ).ready(function() {
   				 setSketchtable();
			});
		function setSketchtable() //setting strings
		{
			var len = CAMACloud.Sketching.Configs[sketchconfig].sources.length
			
			var sourcetable = '';
			var commfield = '' ;
			var velen = 0;
			for (var i=0; i<len;i++){
         			velen = CAMACloud.Sketching.Configs[sketchconfig].sources[i].VectorSource.length;
         			for(var j =0;j < velen;j++){
         			
         				if (i == 0 && j == 0)
         				{
         					sourcetable = CAMACloud.Sketching.Configs[sketchconfig].sources[i].VectorSource[j].Table;
         					commfield = CAMACloud.Sketching.Configs[sketchconfig].sources[i].VectorSource[j].CommandField;
         					sourcetable = sourcetable + ';' + ((!commfield) ? '' : commfield);
         					
         				
         				}
         				else
         				{
         				sourcetable = sourcetable +','+CAMACloud.Sketching.Configs[sketchconfig].sources[i].VectorSource[j].Table;
         				commfield = CAMACloud.Sketching.Configs[sketchconfig].sources[i].VectorSource[j].CommandField;
         				sourcetable = sourcetable + ';' + ((!commfield) ? '' : commfield);
						
         				}
         			}
				}
					console.log('Table :'+ sourcetable);
					document.getElementById('<%= HdnfTableName.ClientID %>').value = sourcetable;
		    
		}
		function dateValidate(dateString) {
            var label = document.getElementById('<%=Label1.ClientID%>');
            var datevallabel = dateString.nextElementSibling;
            var datevalVis = false;
            var dateOf = "#" + dateString.id;
            var dateVal = $(dateOf).val();
            var validat = true;
            if(datevallabel){
              if(datevallabel.style.visibility == "visible") datevalVis = true;
              }
            //var pattern = '^((18)|(19)|(20))[0-9]{2}([-/.])((0|1))([1-9])([-/.])([0-3][0-9])$';
            var pattern=/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/;
            //var pattern=/^((18)|(19)|(20))[0-9]{2}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/
            var dtRegex = new RegExp(pattern);
            if ((dateString.validity.valid==true || dtRegex.test(dateVal))){
              $("input:[type='date']").each(function(){
              if ($(this).val() == ""){
                  label.innerText = "";
                  validDate = true;
              	  return true ;
              	}
               if ((!this.validity.valid==true || (!dtRegex.test($(this).val())==true)))
                  {
                   validat = false;
                   }
                if (validat == false && !datevalVis)
                   {
                     label.innerText = "Invalid date format";
                     label.style["margin-left"] = "24%";
                     label.style["margin-bottom"] = "5px";
                     validDate = false;
                     //document.getElementById('<%= hdnFld.ClientID %>').value = "1";
                       return false;
                    }
                else
                   {
                     label.innerText = "";
                     validDate = true;
                     //document.getElementById('<%= hdnFld.ClientID %>').value = "0";
                     return true;
                    
                }
            });
            }
            else if(dateString.validity.valid!=true && !dtRegex.test(dateVal)){
                label.innerText = "Invalid date format";
                label.style["margin-left"] = "24%";
                label.style["margin-bottom"] = "5px";
                validDate = false;
                //document.getElementById('<%= hdnFld.ClientID %>').value = "1";
                return false;
              
            }
        }
        
       
         function GenerateMask() {
             if (!$('#ddlReport').val()) {
                 return false;
             }
                   
            var dtRegex = new RegExp(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/);
            if (validDate == "false" || validDate == false) {
                return false;
            }

            $('.div_reportSelect input[type="date"]').each(function() { 
                if (dtRegex.test($(this).val()) || $(this).val() == '') {  
                    validDate = true;
                }
                else {
                    validDate = false; 
                    return false;
                }
            });

            if (Page_ClientValidate('reportquery')) {
                showMask();
                return true;
            }
            Page_BlockSubmit = false;
            return false;
        }     
	
		function setPageLayout(w, h) {
			sht = h;
			setTimeout('setReportHeight()', 500);
			$('.report-container-cell').height(h - $('.page-title').height());
		}
		function openReport(rid) {
		    $('#ddlReport').val(rid);
		    $('#btnRefresh').click();
		}
		function setReportHeight() {
			rlayout = $('#ctl00_MainContent_viewer_fixedTable > tbody > tr')[4];
			$(rlayout).attr('class', 'report-view-row');
			var h = sht; 
			var rh = h - $('.page-title').height() - 29 - 11;
			$('.report-view-row > td').height(rh);
		}

		function setPageFunctions() {
		}
		$(function () {
            $("#spanMainHeading").html('Sketch Validation Reports');
            $( window ).resize(function() {
		  $('.report-container').height($('#divContentArea').height() - (Math.ceil($('#divContentArea').height() * 0.05)));
		});
        })
    </script>
	
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<section class="screen" id="reports">
	<div style="float:left;width:98%;margin-left:1%;margin-top: 1%;">
	
		<table class="report-container">
			<tr>
				<td class="report-container-cell report-selection-frame">
					<div class="report-selection">
                        <asp:HiddenField ID="HdnfTableName" runat="server" />
                        <div class="report-control-head" style="padding: 5px;"> Select Report:</div> 
						<div>
                             <asp:Button runat="server" class="btnNrml" ID="btnRefresh" ClientIDMode="Static" Text="Refresh" style='display:none;'/></div>
						<div>
							<asp:DropDownList runat="server" ID="ddlReport" AutoPostBack="true" ClientIDMode="Static" style="width: 232px;">
							</asp:DropDownList>
                             <asp:RequiredFieldValidator runat="server" ID="rfv_ddlReport" ControlToValidate="ddlReport" ErrorMessage="*" ForeColor="Red" SetFocusOnError="True" ValidationGroup="reportquery" />
						</div >
                         <div class="report-control-head" runat="server" id="lblReportParameters" style="padding: 5px; margin-top:25px;"> Report Parameters:</div> 
                         <asp:Panel runat="server" ID="reportParams" class="div_reportSelect">
                          <asp:HiddenField runat="server" ID="HasFilters" />
                          <asp:HiddenField id="hdnFld" runat="server" value = "0" />
                           &nbsp;
                          <asp:PlaceHolder runat="server" ID="ph"></asp:PlaceHolder> <br />
                         <asp:Label ID="Label1" runat="server" Text="" ForeColor="Red"></asp:Label>
                         </asp:Panel>
						<div style="margin-left: 20%;">
							<asp:Button runat="server" class="btnNrml" Style="width:130px;height: 26px;margin-top: 15px;margin-bottom: 15px;" ID="btnGenerate" Text="Generate Report" OnClientClick="javascript:return GenerateMask();" OnClick="btnGenerate_Click"  ValidationGroup="reportquery" />
						</div>
					</div>
				</td>
				<td class="report-container-cell report-frame">
                    <asp:Panel runat="server" ID="reportsHome">
                        <div style="padding:15px;padding-top:0px;">
                             <asp:Repeater runat="server" ID="rptGroups">
                                <ItemTemplate>
                                <asp:HiddenField runat="server" ID="RID" Value='<%# Eval("Id") %>' />
                                <div class="clear"></div>
                                <asp:Repeater runat="server" ID="rptReports">
                                       <ItemTemplate>
                                           <a class="report-item tip"  tip-margin-left ="-100" max-width="300" abbr="<%# Eval("Description") %>" OnClick='openReport(<%# Eval("Id") %>)'>
                                             <div class="icon"></div>
                                             <span class="text"><%# Eval("Name")%></span>
                                           </a>                                         
                                       </ItemTemplate>
                               </asp:Repeater>
                               <div class="clear"></div> 
                              </ItemTemplate>
                            </asp:Repeater>
                        </div>     
                    </asp:Panel>
					<reports:ReportViewer ID="viewer" runat="server" Width="100%" Height="90%" ShowPrintButton="true" ShowZoomControl="true"
						  KeepSessionAlive="true" CssClass="report-viewer" ExportContentDisposition="AlwaysAttachment"  
						ClientIDMode="Static">
					</reports:ReportViewer>
                     <asp:Panel runat="server" ID="pnl_nofile" Visible="false">                 
                              <div style="padding:15px;padding-top:15px;">
                                  <div class="report-viewer">Sorry, it appears there is no data matching your criteria. Please adjust your search parameters and try again.</div>
                              </div>     
                     </asp:Panel>
				</td>
			</tr>
		</table>
		</div>
	</section>
</asp:Content>
