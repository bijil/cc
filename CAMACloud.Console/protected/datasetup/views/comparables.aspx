﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/DataSetup.master"
    AutoEventWireup="false" Inherits="CAMACloud.Console.datasetup_views_comparables" Codebehind="comparables.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function setPageDimensions(cheight) {
            var dtl = cheight - 100 - 60;
           // $('.sortable-table').height(dtl);
        }

        function resetPageFunctions() {
            $('.sortable-table').sortable({
                start: function (event, ui) {
                    var o = ui.item[0];
                    sortOldIndex = $(o).index();
                },
                stop: function (event, ui) {
                    var o = ui.item[0];
                    var cfid = $(o).attr('cid')
                    var index = $(o).index();
                    var newIndex = (index + 1) * 10;
                    if (sortOldIndex > index) {
                        newIndex -= 5;
                    } else {
                        newIndex += 5;
                    }

                    $ds('movecomparable', {
                        NewIndex: newIndex,
                        CFID: cfid
                    }, function (data) {
                        if (data.status == "OK") {
                            $('#btnRefresh').click();
                        }
                    });

                }
            });
        }
        $(function () {
            resetPageFunctions();
        });
		
    </script>
    <script type="text/javascript">
        function showPopup(title) {
            $('.edit-popup').dialog({
                modal: true,
                width: 600,
                resizable: false,
                title: title ? title : 'Edit Field',
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                    $(this).parent().css('z-index','5004')
                }
            });
            $('.ui-widget-overlay').css('z-index','5003');
        }

        function hidePopup() {
            $('.edit-popup').dialog('close');
        }
	
		var submit = 0;
	    function PreventDoubleClick() {
		     if (++submit > 1) {
			     return false;
		     }
	    }
    </script>
    
    <style type="text/css">
        .col0
        {
            width: 10px;
        }
        .col1
        {
            width: 480px;
            text-align: left;
        }
        
        .col2
        {
            width: 420px;
            text-align: left;
        }
        
        .col3
        {
            width: 30px;
            text-align: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h1>
        Setup Comparable Report</h1>
    <div class="div-cat-create ui-state-default ui-corner-all">
        <label>
            Add Field:</label>
        <asp:DropDownList runat="server" ID="ddlField" Style="width: 220px;" AutoPostBack="true" />
        <asp:TextBox runat="server" ID="txtLabel" Style="width: 220px;" MaxLength ="50" />
        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtLabel" ErrorMessage="*"
            ValidationGroup="Create" />
        <asp:Button runat="server" ID="btnAddField" Text="Create" ValidationGroup="Create" OnClientClick="return PreventDoubleClick();"/>
    </div>
    <div class="edit-popup" style="display: none;">
        <asp:UpdatePanel runat="server" UpdateMode="Always">
            <ContentTemplate>
                <div>
                    <asp:HiddenField runat="server" ID="hdnCFID" />

                    <table class="comparable-edit">
						<tr>
							<td class="label">Display Label: </td>
							<td class="input">
								<asp:TextBox runat="server" ID="txtDisplayLabel" MaxLength="50" />
<asp:RequiredFieldValidator runat="server" ControlToValidate="txtDisplayLabel" ErrorMessage="*"
            ValidationGroup="edit" />
							</td>
						
                        </tr>
						<tr>
							<td class="label">Subject/Default Field: </td>
							<td class="input">
								<asp:TextBox runat="server" ID="txtDefaultFieldName" MaxLength="50" /></td>
						</tr>
						<tr>
							<td colspan="2">
								<p>
									Provide field names below if the comparable columns needs to show value from the Subject Data.
								</p>
							</td>
						</tr>
						<tr>
							<td class="label">Comparable Field #1: </td>
							<td class="input">
								<asp:TextBox runat="server" ID="txtComp1" MaxLength="50" /></td>
						</tr>
						<tr>
							<td class="label">Comparable Field #2: </td>
							<td class="input">
								<asp:TextBox runat="server" ID="txtComp2" MaxLength="50"/></td>
						</tr>
						<tr>
							<td class="label">Comparable Field #3: </td>
							<td class="input">
								<asp:TextBox runat="server" ID="txtComp3" MaxLength="50" /></td>
						</tr>
						<tr>
							<td class="label">Comparable Field #4: </td>
							<td class="input">
								<asp:TextBox runat="server" ID="txtComp4" MaxLength="50" /></td>
						</tr>
						<tr>
							<td class="label">Comparable Field #5: </td>
							<td class="input">
								<asp:TextBox runat="server" ID="txtComp5" MaxLength="50" /></td>
						</tr>
					</table>
                    <asp:Button ValidationGroup="edit" runat="server" ID="btnSave" Text=" Save Changes " />
                    <asp:Button runat="server" ID="btnCancel" Text=" Cancel " />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <section class="comparable comparable-header">
        <table style="border-spacing: 0px; font-weight: bold;">
            <tr>
                <td class="col0">
                </td>
                <td class="col0">
                </td>
                <td class="col1" style ="font-size: 15px">
                    Display Label
                </td>
                <td class="col2" style ="font-size: 15px">
                    Field to Compare (Name/Formula)
                </td>
                <td class="col3">
                </td>
            </tr>
        </table>
    </section>
    <asp:UpdatePanel runat="server" ID="RightPanel" UpdateMode="Always">
        <ContentTemplate>
            <asp:Button runat="server" ID="btnRefresh" Style="display: none;" ClientIDMode="Static" />
            <div class="sortable-table" style = "height : auto !important;">
                <asp:Repeater runat="server" ID="rpComp">
                    <ItemTemplate>
                        <div class="comparable" cid='<%# Eval("Id") %>'>
                            <table style="border-spacing: 0px;">
                                <tr>
                                    <td class="col0">
                                    </td>
                                    <td class="col0">
                                        <asp:LinkButton runat="server" CssClass="a16 del16" CommandArgument='<%# Eval("Id") %>'
                                            CommandName='DeleteComparable' />
                                    </td>
                                    <td class="col1" style = "font-size: 13px">
                                        <%# Eval("Label")%>
                                    </td>
                                    <td class="col2" style = "font-size: 13px">
                                        <%# Eval("DefaultFieldName")%>
                                    </td>
                                    <td class="col3" style = "font-size: 13px">
                                        <asp:LinkButton runat="server" ID="lbEdit" Text="Edit" CommandName="EditCF" CommandArgument='<%# Eval("Id") %>'
                                            OnClientClick='showPopup();' />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
