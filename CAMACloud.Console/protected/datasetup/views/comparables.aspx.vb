﻿
Partial Class datasetup_views_comparables
	Inherits System.Web.UI.Page

	Protected Sub LoadComparables()
		rpComp.DataSource = Database.Tenant.GetDataTable("SELECT * FROM cc_ComparableFields ORDER BY Ordinal")
		rpComp.DataBind()
	End Sub

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then

			ddlField.FillFromSql("Select id, AssignedName FROM cc_DataFields UNION Select a.ROWUID, CONCAT(a.FunctionName, '_', a.TableName, '_', a.FieldName) AS AssignedName  FROM DataSourceField f JOIN AggregateFieldSettings a ON a.TableName = f.SourceTable AND a.FieldName = f.Name ORDER BY 2", True, "** Non-Default Entry")
			LoadComparables()
		End If
	End Sub

	Protected Sub ddlField_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlField.SelectedIndexChanged
		Dim defaultLabel As String = ""
		Dim selectedText As String = ddlField.SelectedItem.Text
		Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT id,AssignedName FROM cc_DataFields UNION SELECT f.id, CONCAT(a.FunctionName,'_',a.TableName,'_',a.FieldName)AS AssignedName  FROM DataSourceField f JOIN AggregateFieldSettings a ON a.TableName = f.SourceTable AND a.FieldName = f.Name")

		' Find the corresponding id for the selected text
		Dim correspondingId As Integer = -1 ' Initialize with a default value
		For Each row As DataRow In dt.Rows
			Dim assignedName As String = row("AssignedName").ToString()

			If assignedName = selectedText Then
				correspondingId = Convert.ToInt32(row("id"))
				Exit For ' Exit loop if match found
			End If
		Next

		' Use the correspondingId in your SQL query
		If correspondingId > 0 Then
			defaultLabel = s_("SELECT DisplayLabel FROM DataSourceField WHERE Id = " & correspondingId)
		End If

		txtLabel.Text = defaultLabel

		'Dim defaultLabel As String = ""
		'Dim selectedText As String = ddlField.SelectedItem.Text

		'If ddlField.SelectedIndex > 0 Then
		'	defaultLabel = s_("SELECT DisplayLabel FROM DataSourceField WHERE Id = " & ddlField.SelectedValue & " AND DisplayLabel like '" & ddlField.SelectedItem.Text & "'")
		'End If
	End Sub

	Protected Sub btnAddField_Click(sender As Object, e As System.EventArgs) Handles btnAddField.Click
		If txtLabel.Text.Trim.ToString = "" Then Return

		Dim ordinal As Integer = i_("SELECT COALESCE(MAX(Ordinal), 0) + 10 FROM ComparableFields")
		e_("INSERT INTO ComparableFields (Label, DefaultFieldName, Ordinal) VALUES ({0}, {1}, {2})", True, txtLabel, IIf(ddlField.SelectedValue = "", "", ddlField.SelectedItem.Text), ordinal)
		ddlField.SelectedIndex = 0
		txtLabel.Text = ""
		LoadComparables()
		Dim Description As String = "New Comparable Report Setup created."
		SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)

		'Response.Redirect("comparables.aspx")

		Dim Script As String = "$(document).ready(function () {alert('New field created'); window.location = 'comparables.aspx';});"
		'Script += "alert('New field created');}"
		'Script += "window.location.href = 'comparables.aspx';}"
		RunScript(Script)

	End Sub

	Protected Sub rpComp_ItemCommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpComp.ItemCommand
		If e.CommandName = "DeleteComparable" Then
			e_("DELETE FROM ComparableFields WHERE ID = " & e.CommandArgument)
			Alert("Field deleted")
			LoadComparables()
			Dim Description As String = " Comparable Report Setup deleted."
			SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
		End If
		If e.CommandName = "EditCF" Then
			OpenCFforEdit(e.CommandArgument)
		End If
	End Sub

	Protected Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
		If IsPostBack Then
			RunScript("resetPageFunctions();")
		End If
	End Sub

	Protected Sub UseSubjectFields_Clicked(sender As Object, e As System.EventArgs)
		Dim chk As CheckBox = sender
		Dim rpi As RepeaterItem = chk.Parent
		Dim divNonDefault As HtmlControl = rpi.FindControl("divNonDefault")
		divNonDefault.Visible = chk.Checked
	End Sub


	Sub OpenCFforEdit(cfid As Integer)
		Dim dr As DataRow = t_("SELECT * FROM ComparableFields WHERE Id = " & cfid)
		hdnCFID.Value = dr.GetString("Id")
		txtDisplayLabel.Text = dr.GetString("Label")
		txtDefaultFieldName.Text = dr.GetString("DefaultFieldName")
		txtComp1.Text = dr.GetString("NonDefaultCompField1")
		txtComp2.Text = dr.GetString("NonDefaultCompField2")
		txtComp3.Text = dr.GetString("NonDefaultCompField3")
		txtComp4.Text = dr.GetString("NonDefaultCompField4")
		txtComp5.Text = dr.GetString("NonDefaultCompField5")
	End Sub

	Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
		If txtDisplayLabel.Text.Trim.ToString = "" Then Return
		e_("UPDATE ComparableFields SET [Label] = {1},[DefaultFieldName] = {2},[NonDefaultCompField1] = {3}, [NonDefaultCompField2] = {4}, [NonDefaultCompField3] = {5}, [NonDefaultCompField4] = {6}, [NonDefaultCompField5] = {7} WHERE Id = {0}", True,
		   CInt(hdnCFID.Value), txtDisplayLabel, txtDefaultFieldName, txtComp1, txtComp2, txtComp3, txtComp4, txtComp5)

		LoadComparables()

		RunScript("hidePopup();")
	End Sub

	Protected Sub btnRefresh_Click(sender As Object, e As System.EventArgs) Handles btnRefresh.Click
		LoadComparables()
	End Sub

	Protected Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
		RunScript("hidePopup();")
	End Sub
End Class
