﻿Public Class ParcelComparables
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ddlTable.FillFromSql("SELECT Name from DataSourceTable WHERE CC_TargetTable = 'ParcelData' ORDER BY Id", True)
            LoadParcelComparables()
        End If
    End Sub

    Private Sub ddlTable_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTable.SelectedIndexChanged
        LoadParcelComparablesFields(ddlTable.SelectedIndex, ddlTable.SelectedValue)
    End Sub

    Public Sub LoadParcelComparables()
        Dim dtRow As DataRow = Database.Tenant.GetTopRow("SELECT top 1 * FROM MAParcelComparableFields")
        If dtRow IsNot Nothing AndAlso dtRow.GetString("TableName") <> "" Then
            ddlTable.SelectedValue = dtRow.GetString("TableName")
            LoadParcelComparablesFields(ddlTable.SelectedIndex, ddlTable.SelectedValue)
            If dtRow.GetString("Comparable1") <> "" Then
                DropDownList1.SelectedValue = dtRow.GetString("Comparable1")
            End If
            If dtRow.GetString("Comparable2") <> "" Then
                DropDownList2.SelectedValue = dtRow.GetString("Comparable2")
            End If
            If dtRow.GetString("Comparable3") <> "" Then
                DropDownList3.SelectedValue = dtRow.GetString("Comparable3")
            End If
            If dtRow.GetString("Comparable4") <> "" Then
                DropDownList4.SelectedValue = dtRow.GetString("Comparable4")
            End If
            If dtRow.GetString("Comparable5") <> "" Then
                DropDownList5.SelectedValue = dtRow.GetString("Comparable5")
            End If
            If dtRow.GetString("KeyFieldValue") = "A" Then
                radioList.Items(1).Selected = True
            Else
                radioList.Items(0).Selected = True
            End If
        Else
            LoadParcelComparablesFields(0, "")
            radioList.Items(0).Selected = True
        End If
    End Sub
    Public Sub LoadParcelComparablesFields(selectedIndex, selectedValue)
        Dim qry As String = IIf(selectedIndex = 0, "SELECT Name FROM DataSourceField WHERE Id = 0", "SELECT Name FROM DataSourceField WHERE SourceTable = '" & selectedValue & "'")
        Dim dt As DataTable = Database.Tenant.GetDataTable(qry)
        DropDownList1.FillFromTable(dt, True, "---  Select Field  ---")
        DropDownList2.FillFromTable(dt, True, "---  Select Field  ---")
        DropDownList3.FillFromTable(dt, True, "---  Select Field  ---")
        DropDownList4.FillFromTable(dt, True, "---  Select Field  ---")
        DropDownList5.FillFromTable(dt, True, "---  Select Field  ---")
    End Sub
    Protected Sub btnUpdate_Click(sender As Object, e As System.EventArgs) Handles btnUpdate.Click
        Try
            Dim tbl As String = ddlTable.SelectedValue
            Dim Comparable1 As String = DropDownList1.SelectedValue
            Dim Comparable2 As String = DropDownList2.SelectedValue
            Dim Comparable3 As String = DropDownList3.SelectedValue
            Dim Comparable4 As String = DropDownList4.SelectedValue
            Dim Comparable5 As String = DropDownList5.SelectedValue
            Dim keyField As String = radioList.SelectedValue
            Dim keyFd As String = ""
            Dim drow As DataRow = Database.Tenant.GetTopRow("SELECT * FROM Application")
            Dim keyFieldOption As String = "K"
            If keyField = "K" Then
                keyFd = drow.GetString("KeyField1")
                If keyFd = "" Then
                    Alert("Keyfield1 is not enabled!")
                    Return
                End If
                keyFd = "pd." + keyFd
                keyFieldOption = "K"
            Else
                keyFd = drow.GetString("Alternatekeyfieldvalue")
                If keyFd = "" Then
                    Alert("AlternateField is not enabled!")
                    Return
                End If
                keyFd = "pd." + keyFd
                keyFieldOption = "A"
            End If

            If tbl = "" Then
                Alert("Please choose table to Update.")
                Return
            ElseIf Comparable1 = "" And Comparable2 = "" And Comparable3 = "" And Comparable4 = "" And Comparable5 = "" Then
                Alert("Please choose at least one field to Update.")
                Return
            End If
            Dim sql As String = ""
            Dim iqtemp As String = "ParcelId, "
            Dim vq As String = "CC_ParcelId, "
            Dim up As String = ""
            Dim iq As String = ""
            Dim iqtWhere = ""
            Dim vl = "'" + tbl + "', "
            Dim inpc = "DELETE FROM MAParcelComparableFields; INSERT INTO MAParcelComparableFields(TableName, "
            Dim delWhere = ""

            If Comparable1 <> "" Then
                iqtemp += "C1, "
                vq += Comparable1 + ", "
                up += "UPDATE t SET t." + Comparable1 + " = pd.CC_ParcelId FROM #temp_parcel_comparable t JOIN ParcelData pd ON t." + Comparable1 + " = " + keyFd
                iqtWhere += Comparable1 + " IS NOT NULL OR "
                inpc += "Comparable1, "
                vl += "'" + Comparable1 + "', "
                delWhere += " (" + Comparable1 + " Is Not NULL And ISNUMERIC(" + Comparable1 + ") = 0) OR"
            End If
            If Comparable2 <> "" Then
                iqtemp += "C2, "
                vq += Comparable2 + ", "
                up += "; UPDATE t SET t." + Comparable2 + " = pd.CC_ParcelId FROM #temp_parcel_comparable t JOIN ParcelData pd ON t." + Comparable2 + " = " + keyFd
                iqtWhere += Comparable2 + " IS NOT NULL OR "
                inpc += "Comparable2, "
                vl += "'" + Comparable2 + "', "
                delWhere += " (" + Comparable2 + " Is Not NULL And ISNUMERIC(" + Comparable2 + ") = 0) OR"
            End If
            If Comparable3 <> "" Then
                iqtemp += "C3, "
                vq += Comparable3 + ", "
                up += "; UPDATE t SET t." + Comparable3 + " = pd.CC_ParcelId FROM #temp_parcel_comparable t JOIN ParcelData pd ON t." + Comparable3 + " = " + keyFd
                iqtWhere += Comparable3 + " IS NOT NULL OR "
                inpc += "Comparable3, "
                vl += "'" + Comparable3 + "', "
                delWhere += " (" + Comparable3 + " Is Not NULL And ISNUMERIC(" + Comparable3 + ") = 0) OR"
            End If
            If Comparable4 <> "" Then
                iqtemp += "C4, "
                vq += Comparable4 + ", "
                up += "; UPDATE t SET t." + Comparable4 + " = pd.CC_ParcelId FROM #temp_parcel_comparable t JOIN ParcelData pd ON t." + Comparable4 + " = " + keyFd
                iqtWhere += Comparable4 + " IS NOT NULL OR "
                inpc += "Comparable4, "
                vl += "'" + Comparable4 + "', "
                delWhere += " (" + Comparable4 + " Is Not NULL And ISNUMERIC(" + Comparable4 + ") = 0) OR"
            End If
            If Comparable5 <> "" Then
                iqtemp += "C5, "
                vq += Comparable5 + ", "
                up += "; UPDATE t SET t." + Comparable5 + " = pd.CC_ParcelId FROM #temp_parcel_comparable t JOIN ParcelData pd ON t." + Comparable5 + " = " + keyFd
                iqtWhere += Comparable5 + " IS NOT NULL OR "
                inpc += "Comparable5, "
                vl += "'" + Comparable5 + "', "
                delWhere += " (" + Comparable5 + " Is Not NULL And ISNUMERIC(" + Comparable5 + ") = 0) OR"
            End If

            inpc += "KeyFieldValue"
            vl += "'" + keyFieldOption + "'"

            iqtWhere = iqtWhere.Substring(0, iqtWhere.Length - 4)
            vq = vq.Substring(0, vq.Length - 2)
            iqtemp = iqtemp.Substring(0, iqtemp.Length - 2)
            ''inpc = inpc.Substring(0, inpc.Length - 2)
            ''vl = vl.Substring(0, vl.Length - 2)
            delWhere = delWhere.Substring(0, delWhere.Length - 3)
            inpc += ") VALUES (" + vl + ");"

            sql += inpc + "SELECT " + vq + " INTO #temp_parcel_comparable FROM ParcelData WHERE " + iqtWhere
            sql += "; " + up
            sql += "; TRUNCATE TABLE ParcelComparables"
            sql += "; DELETE FROM #temp_parcel_comparable WHERE " + delWhere
            sql += "; INSERT INTO ParcelComparables (" + iqtemp + ") SELECT " + vq + " FROM #temp_parcel_comparable;"
            Database.Tenant.Execute(sql)
            RunScript("maskHide(1);")
            'CC_3994 start
            'Dim Description As String = "Parcel comparables For Table " + tbl + " created successfully."
            'SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            'CC_3994 end
        Catch ex As Exception
            Alert("An error occured: " + ex.Message)
            RunScript("maskHide();")
        End Try
    End Sub

End Class