﻿Imports System.Security
Imports System.Linq
Imports System.Data.Linq
Imports System.Web.Script.Services
Imports System.Web.Services
Partial Class datasetup_users
    Inherits System.Web.UI.Page
    Dim FullName As String
    Dim SqlNew = ""
    Dim maxSeat As New Hashtable()

    Public Function HasDTRModule() As Boolean
        Return CAMACloud.ClientOrganization.HasModule(HttpContext.Current.GetCAMASession.OrganizationId, "DTR")
    End Function

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Membership.ApplicationName <> HttpContext.Current.GetCAMASession.TenantKey Then
            CAMASession.Logout(HttpContext.Current)
            Exit Sub
        End If
        If Not IsPostBack Then
            BindGridView()
            LoadRolesList()
            ddlMobileCarrierLoad()
        End If
        Dim scriptManager1 As ScriptManager = ScriptManager.GetCurrent(Me.Page)
        scriptManager1.RegisterPostBackControl(Me.lbExport)
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If IsPostBack Then
            RunScript("getChecked();")
        End If

        If (hdnUserId.Value <> "") Then
'            btnSaveUser.ValidationGroup = "UserProp2"
            'rfvtxtFirstName.ValidationGroup = "UserProp2"
'            rfvtxtUserMail.ValidationGroup = "UserProp2"
'            revtxtUserMail.ValidationGroup = "UserProp2"
'            revtxtMobileNumber.ValidationGroup = "UserProp2"
            ' rfvtxtMobileNumber.ValidationGroup = "UserProp2"
'            rfvddlMobileCarrier.ValidationGroup = "UserProp2"
            '  rfvtxtMobileNumber.ValidationGroup = "UserProp2"
        Else
'            btnSaveUser.ValidationGroup = "UserProp"
            'rfvtxtFirstName.ValidationGroup = "UserProp"
'            rfvtxtUserMail.ValidationGroup = "UserProp"
'            revtxtUserMail.ValidationGroup = "UserProp"
'            revtxtMobileNumber.ValidationGroup = "UserProp"
            ' rfvtxtMobileNumber.ValidationGroup = "UserProp"
'            rfvddlMobileCarrier.ValidationGroup = "UserProp"
            'rfvtxtMobileNumber.ValidationGroup = "UserProp"
        End If
    End Sub
    Protected Sub BindGridView()
        For Each u As MembershipUser In Membership.GetAllUsers
            If Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM UserSettings WHERE LoginId = {0}".SqlFormatString(u.UserName)) = 0 Then
                Database.Tenant.Execute("INSERT INTO UserSettings (LoginId, FirstName) VALUES ({0}, {0})".SqlFormatString(u.UserName))
            End If
        Next
        hfCurrentUser.Value = UserName()
        If UserName() = "dcs-support" Then
            gdvUserDetails.DataSource = Database.Tenant.GetDataTable("SELECT * FROM UserSettings ORDER BY FirstName")
        Else
            gdvUserDetails.DataSource = Database.Tenant.GetDataTable("SELECT * FROM UserSettings WHERE LoginId NOT IN ('dcs-support') ORDER BY FirstName")
        End If

        gdvUserDetails.DataBind()
    End Sub

    Private Sub gdvUserDetails_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gdvUserDetails.PageIndexChanging
        gdvUserDetails.PageIndex = e.NewPageIndex
        BindGridView()
    End Sub

    Protected Sub gdvUserDetails_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gdvUserDetails.RowCommand
        hdnUserId.Value = e.CommandArgument.ToString().ToLower
        Select Case e.CommandName
        	Case "DeleteUser"
                If e.CommandArgument.ToString().ToLower = Membership.GetUser().UserName.ToString().ToLower Then
                    Alert("You cannot delete your own user account.")
                    Return
                End If
                If e.CommandArgument = "admin" Or e.CommandArgument = "dcs-qa" Or e.CommandArgument = "dcs-support" Or e.CommandArgument = "vendor-cds" Or e.CommandArgument = "dcs-rd" Or e.CommandArgument = "malite_admin" Or e.CommandArgument = "dcs-integration" Or e.CommandArgument = "dcs-ps" Then
                    Alert("You cannot delete the default '" + e.CommandArgument.ToString + "' user.")
                    Return
                End If
                If e.CommandArgument = "dcs-support" Then
                    Alert("You cannot delete the default 'dcs-support' user.")
                    Return
                End If
                Dim userid = Database.Tenant.GetIntegerValue("select id from UserSettings where LoginId ='" + e.CommandArgument.ToString() + "'")
                Database.Tenant.Execute("INSERT INTO UserLog(LoginId,Action,UserId,Description) VALUES({0},{1},{2},{3})".SqlFormatString(Membership.GetUser().ToString(), "Delete", userid,
                                              Membership.GetUser().ToString() + " deleted the user " + e.CommandArgument.ToString()))
                If Not (IsNothing(Membership.GetUser(e.CommandArgument.ToString()))) Then
                    Database.System.Execute("IF EXISTS(SELECT * FROM LoginDetails WHERE UserId ='" + Membership.GetUser(e.CommandArgument.ToString()).ProviderUserKey.ToString() + "') DELETE FROM LoginDetails WHERE UserId ='" + Membership.GetUser(e.CommandArgument.ToString()).ProviderUserKey.ToString() + "'")
                End If
                Dim applicationName As String = System.Web.Security.Membership.Provider.ApplicationName
                Dim orgid = HttpContext.Current.GetCAMASession.OrganizationId
                Database.System.Execute("EXEC SP_RemoveSeatWhileDeletingUser @ApplicationName = {0},@UserName = {1},@OrgId = {2}".SqlFormatString(applicationName, e.CommandArgument, orgid))
                Membership.DeleteUser(e.CommandArgument)
                Database.Tenant.Execute("DELETE FROM UserTrackingCurrent WHERE LoginId = " + e.CommandArgument.ToString.ToSqlValue)
                Database.Tenant.Execute("DELETE FROM UserSettings WHERE LoginId = " + e.CommandArgument.ToString.ToSqlValue)
                Dim Description As String = "User " + e.CommandArgument + " has been Deleted."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
                txtSearch.Text = ""
                BindGridView()
                'Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
            Case "EditUser"
                Try
                    checkBtnClick.Value = 0 'Variable to find "Edit User" button click
                    ClearForm()
                    LoadUserForEdit(e.CommandArgument)
                Catch ex As Exception
                    Alert(ex.Message)
                End Try

            Case "UnlockUser"
                Dim u As MembershipUser = Membership.GetUser(e.CommandArgument)
                If u IsNot Nothing Then
                    u.UnlockUser()
                End If
                BindGridView()
            Case Else

        End Select
        'BindGridView()
    End Sub

    Protected Sub lbNewUser_Click(sender As Object, e As System.EventArgs) Handles lbNewUser.Click
        txtLoginId.Text = ""
        txtLoginId.Enabled = True
        txtUserMail.Enabled = True
        txtUserMail.Text = ""
        For Each li As ListItem In cblRoles.Items
            li.Selected = False
            li.Enabled = True
            Dim role = Roles.GetRolesForUser(UserName)
            If (Membership.GetUser().ToString() = "admin" Or Membership.GetUser().ToString() = "dcs-qa" Or Membership.GetUser().ToString() = "dcs-rd" Or Membership.GetUser().ToString() = "dcs-support" Or Membership.GetUser().ToString() = "dcs-integration" Or Membership.GetUser().ToString() = "dcs-ps" Or role.Contains("DataSetup") Or role.Contains("BasicSettings")) Then
                If (li.Value = "SketchValidation" Or li.Value = "DTR" Or li.Value = "CompSales" Or li.Value = "MRA") Then
                    li.Enabled = True
                End If
            Else
                If (li.Value = "SketchValidation" Or li.Value = "DTR" Or li.Value = "CompSales" Or li.Value = "MRA") Then
                    li.Enabled = False
                End If
            End If
        Next
        checkBtnClick.Value = 1 'Variable to find "Add User" button click
    End Sub

    Sub ClearForm()
        hdnUserId.Value = ""
        txtFirstName.Text = ""
        txtMiddleName.Text = ""
        txtLastName.Text = ""
        txtMobileNumber.Text = ""
        ddlMobileCarrier.SelectedIndex = 0
        txtUserMail.Text = ""
        txtLoginId.Text = ""
        txtPassword.Text = ""
        txtPassword2.Text = ""
        txtLoginId.Enabled = True
        'rfvPassword.Enabled = True
        For Each li As ListItem In cblRoles.Items
            li.Selected = False
        Next
        btnCancel.Text = "Cancel"
    End Sub

    Protected Sub btnSaveUser_Click(sender As Object, e As System.EventArgs) Handles btnSaveUser.Click
        'If checkBtnClick.Value = 0 Then
        '    RunScript("confirmAlert();")
        'End If
        Dim orgid = HttpContext.Current.GetCAMASession.OrganizationId
        Dim res As MembershipCreateStatus
        Dim userName As String = txtLoginId.Text.Trim
        FullName = txtFirstName.Text + " " + txtMiddleName.Text + " " + txtLastName.Text
        Dim prevNotifyByEmail As Boolean = False
        Dim prevNotifyBySMS As Boolean = False
        Dim prevAutoQC As Boolean = False

        ' Retrieve the previous values from the database
        Dim prevSettingsRow As DataRow = Database.Tenant.GetTopRow("SELECT * FROM UserSettings WHERE LoginId= '" + userName + "'")

        If prevSettingsRow IsNot Nothing Then
            prevNotifyByEmail = Convert.ToBoolean(prevSettingsRow("NotifyByEmail"))
            prevNotifyBySMS = Convert.ToBoolean(prevSettingsRow("NotifyBySMS"))
            prevAutoQC = Convert.ToBoolean(prevSettingsRow("AutoQC"))
        End If
        Dim Noptions = ""
        If prevNotifyByEmail <> cbNotifybyEmail.Checked Then
            If cbNotifybyEmail.Checked Then
                Noptions += cbNotifybyEmail.Text + ","
                Dim Description As String = "Send Notification Via Email has been enabled for user " & userName & "."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            Else
                Dim Description As String = "Send Notification Via Email has been disabled for user " & userName & "."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If
        End If
        If prevNotifyBySMS <> cbNotifybySMS.Checked Then
            If cbNotifybySMS.Checked Then
                Noptions += cbNotifybySMS.Text + ","
                Dim Description As String = "Send Notification Via SMS has been enabled for user " & userName & "."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            Else
                Dim Description As String = "Send Notification Via SMS has been disabled for user " & userName & "."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If
        End If
        If prevAutoQC <> cbAutoQC.Checked Then
            If cbAutoQC.Checked Then
                Noptions += cbAutoQC.Text + ","

                Noptions += cbAutoQC.Text + ","
                Dim Description As String = "Compliance For Auto-QC has been enabled for user " & userName & "."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            Else
                Dim Description As String = "Compliance For Auto-QC has been disabled for user " & userName & "."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If
        End If
        Try
            If hdnUserId.Value = "" Then

                Membership.CreateUser(userName, txtPassword.Text, txtUserMail.Text, "@", "#", True, res)
                If res = MembershipCreateStatus.Success Then
                    Database.Tenant.Execute("INSERT INTO UserSettings(LoginId,FirstName,MiddleName,LastName,Email,Mobile,MobileCarrierId,NotifyByEmail,NotifyBySMS,AutoQC,IsBPPAppraiser,BPPDownloadType,BPPRealDataReadOnly) VALUES({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12})".SqlFormat(True, userName, txtFirstName.Text,
                                            IIf(txtMiddleName.Text = "", "", txtMiddleName.Text), IIf(txtLastName.Text = "", "", txtLastName.Text), txtUserMail.Text, txtMobileNumber.Text, IIf(ddlMobileCarrier.SelectedValue = "", "", ddlMobileCarrier.SelectedValue), IIf(cbNotifybyEmail.Checked, 1, 0), IIf(cbNotifybySMS.Checked, 1, 0), IIf(cbAutoQC.Checked, 1, 0), IIf(chkbpp.Checked, 1, 0), IIf(chkbppAction1.Checked, 1, 0), IIf(chkbppAction2.Checked, 1, 0)))
                    AssignRolesFromList(userName)
                    Dim userid = Database.Tenant.GetIntegerValue("select id from UserSettings where LoginId ='" + userName + "'")
                    Database.Tenant.Execute("INSERT INTO UserLog(LoginId,Action,UserId,Name,Email,MobileNumber,MobileCarrier,Roles,OtherOptions,Description) VALUES({0},{1},{2},{3},{4},{5},{6},{7},{8},{9})".SqlFormat(True, Membership.GetUser().ToString(), "Create", userid, FullName,
                                              txtUserMail.Text, txtMobileNumber.Text, ddlMobileCarrier.SelectedValue, SqlNew, Noptions, Membership.GetUser().ToString() + " created a new user " + txtLoginId.Text.ToString()))
                    Dim Description As String = "New User " + userName + " has been Created."
                    SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
                    ClearForm()
                    BindGridView()
                    If maxSeat.Count > 0 Then
                        Dim role As String = ""
                        For Each de As DictionaryEntry In maxSeat
                            role += IIf(role = "", de.Key, " , " + de.Key)
                        Next

                        Dim msg As String = "The user was successfully created.\n"
                        If maxSeat.Count > 1 Then
                            msg = "The roles " + role + " cannot be assigned. Maximum number of Seats purchases have already been assigned.\n\n"
                        Else
                            msg = "The role " + role + " cannot be assigned. Maximum number of Seats purchases have already been assigned.\n\n"
                        End If

                        For Each d As DictionaryEntry In maxSeat
                            msg += d.Key + "  - " + d.Value + "\n"
                        Next
                        Alert(msg)
                    Else
                        Alert("The user was successfully created.")
                    End If
                    RunScript("hidePopup();")
                Else
                    If res = MembershipCreateStatus.DuplicateEmail Then
                        Alert("This email address has been used for another account. Please try with a different email address.")
                        Return
                    End If
                    If res = MembershipCreateStatus.DuplicateUserName Then
                        Alert("Login ID already exists. Please choose another one.")
                        Return
                    End If
                    If res = MembershipCreateStatus.InvalidUserName Then
                        Alert("Login ID is required. Please enter a Login ID.")
                        Return
                    End If
                    If res = MembershipCreateStatus.InvalidPassword Then
                        Alert("Please enter a password that is a minimum of 6 characters in length.")
                        Return
                    End If
                    'Alert("Error: " + res.ToString)
                    If (txtUserMail.Text = "") Then
                        Alert("Please provide an email address.")
                    End If
                    Return
                End If

            Else
                Dim u As MembershipUser = Membership.GetUser(userName)
                Dim des As String = ""
                If txtPassword.Text.IsNotEmpty AndAlso txtPassword.Text = txtPassword2.Text Then
                    If txtPassword.Text.Length < 6 Then
                        Alert("Password must be minimum 6 characters in length.")
                        Return
                    End If
                    u.ChangePassword(u.ResetPassword(), txtPassword.Text)
                    des += "Password has been updated."
                End If
                Dim Orgemail = u.Email

                u.Email = txtUserMail.Text
                Try
					Membership.UpdateUser(u)
				Catch ex As Exception
					If ex.Message.Equals("The E-mail supplied is invalid.") Then
						Alert("This email address has been used for another account. Please try with a different email address.")
						Return
					Else
						Alert(ex.Message)
						Return
					End If
			    End Try
			    Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM UserSettings WHERE LoginId='" + u.UserName + "'")
			    
                Dim lookUpId As Integer
                lookUpId = Database.Tenant.Execute("UPDATE UserSettings SET FirstName={1},MiddleName={2},LastName={3},Email={4},Mobile={5},MobileCarrierId={6},NotifyByEmail={7},NotifyBySMS={8},AutoQC={9},DoNotTrack={10},IsBPPAppraiser={11},BPPDownloadType={12},BPPRealDataReadOnly={13} WHERE LoginId={0};SELECT CAST(@@ROWCOUNT AS INT) As NeewId ".SqlFormat(True, userName, txtFirstName.Text,
                                            IIf(txtMiddleName.Text = "", "", txtMiddleName.Text), IIf(txtLastName.Text = "", "", txtLastName.Text), txtUserMail.Text, txtMobileNumber.Text, IIf(ddlMobileCarrier.SelectedValue = "", "", ddlMobileCarrier.SelectedValue), IIf(cbNotifybyEmail.Checked, 1, 0), IIf(cbNotifybySMS.Checked, 1, 0), IIf(cbAutoQC.Checked, 1, 0), IIf(chkDoNotTrack.Checked, 1, 0), IIf(chkbpp.Checked, 1, 0), IIf(chkbppAction1.Checked, 1, 0), IIf(chkbppAction2.Checked, 1, 0)))

                If (lookUpId = 0) Then
                    Database.Tenant.Execute("INSERT INTO UserSettings(LoginId,FirstName,MiddleName,LastName,Email,Mobile,MobileCarrierId,NotifyByEmail,NotifyBySMS,AutoQC,DoNotTrack,IsBPPAppraiser,BPPDownloadType,BPPRealDataReadOnly) VALUES({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13})".SqlFormat(True, userName, txtFirstName.Text,
                                          IIf(txtMiddleName.Text = "", "", txtMiddleName.Text), IIf(txtLastName.Text = "", "", txtLastName.Text), txtUserMail.Text, txtMobileNumber.Text, IIf(ddlMobileCarrier.SelectedValue = "", "", ddlMobileCarrier.SelectedValue), IIf(cbNotifybyEmail.Checked, 1, 0), IIf(cbNotifybySMS.Checked, 1, 0), IIf(cbAutoQC.Checked, 1, 0), IIf(chkDoNotTrack.Checked, 1, 0), IIf(chkbpp.Checked, 1, 0), IIf(chkbppAction1.Checked, 1, 0), IIf(chkbppAction2.Checked, 1, 0)))
                End If
                Dim PRoles = ""
                For Each li As ListItem In cblRoles.Items
                    If Roles.IsUserInRole(u.UserName, li.Value) Then
                        PRoles += li.Value + ","
                    End If
                Next
                Dim options = ""
                If dr.GetBoolean("NotifyByEmail") Then
                    options += cbNotifybyEmail.Text + ","
                End If
                If dr.GetBoolean("NotifyBySMS") Then
                    options += cbNotifybySMS.Text + ","
                End If
                If dr.GetBoolean("AutoQC") Then
                    options += cbAutoQC.Text + ","
                End If
                AssignRolesFromList(userName)
                Dim NRoles = ""
                For Each li As ListItem In cblRoles.Items
                    If li.Selected Then
                        NRoles += li.Value + ","
                    End If
                    If (userName = "admin" Or userName = "dcs-support" Or userName = "dcs-ps") Then
                        If li.Value = "BasicSettings" Then
                            li.Attributes.Add("style", "display:none")
                        End If
                    End If
                Next
                Dim user_id = Database.Tenant.GetIntegerValue("select id from UserSettings where LoginId ='" + userName + "'")
                Dim editQuery = "INSERT INTO UserLog(LoginId,Action,UserId"
                Dim Value = "('" + Membership.GetUser().ToString() + "','Edit','" + user_id.ToString() + "'"
                Dim OrgName = dr.GetString("FirstName") + " " + dr.GetString("MiddleName") + " " + dr.GetString("LastName")
                If FullName <> OrgName Then
                    editQuery += ",Name"
                    Value += ",'Changed from " + OrgName + " To " + FullName + "'"
                    des += " FullName Changed from " + OrgName + " To " + FullName + "."
                End If
                If txtUserMail.Text <> Orgemail Then
                    editQuery += ",Email"
                    Value += ",'Changed from " + Orgemail + " To " + txtUserMail.Text + "'"
                    des += " Email Changed from " + Orgemail + " To " + txtUserMail.Text + "."
                End If
                If txtMobileNumber.Text <> dr.GetString("Mobile") Then
                    editQuery += ",MobileNumber"
                    Value += ",'Changed from " + dr.GetString("Mobile") + " To " + txtMobileNumber.Text + "'"
                    des += " Mobile number Changed from " + dr.GetString("Mobile") + " To " + txtMobileNumber.Text + "."
                End If
                If ddlMobileCarrier.SelectedValue <> dr.GetString("MobileCarrierId") Then
                    editQuery += ",MobileCarrier"
                    Value += ",'Changed from " + dr.GetString("MobileCarrierId") + " To " + ddlMobileCarrier.SelectedValue + "'"
                    des += " Mobile Carrier Changed from " + dr.GetString("MobileCarrierId") + " To " + ddlMobileCarrier.SelectedValue + "."
                End If
                If PRoles <> NRoles Then
                    editQuery += ",Roles,NewRoles"
                    Value += ",'" + PRoles + "','" + NRoles + "'"
                End If
                If options <> Noptions Then
                    editQuery += ",OtherOptions,NewOtherOptions"
                    Value += ",'" + options + "','" + Noptions + "'"
                End If
                Dim NeditQuery = ""
                NeditQuery += editQuery + ",Description)VALUES" + Value + ",'" + Membership.GetUser().ToString() + " changed fields of the user " + u.ToString() + "')"
                Database.Tenant.Execute(NeditQuery)
                
                If des <> ""
                	SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), des)
                End If	
                
                Dim BppDes As String = ""
                
                If (chkbpp.Checked And Not dr.GetBoolean("IsBPPAppraiser"))   
                	BppDes = "Personal Property has been Enabled to User " + userName
                Else If (Not chkbpp.Checked And dr.GetBoolean("IsBPPAppraiser"))
                	BppDes = "Personal Property has been Disabled to User " + userName
                End If
                
                If BppDes <> "" Then
                    SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), BppDes)
                End If
                
                BppDes = ""

	            If (chkbppAction1.Checked And Not dr.GetBoolean("BPPDownloadType")) Then
                	BppDes = "Personal Property + Real Property Data Editable has been Enabled to User " + userName
                Else If (Not chkbppAction1.Checked And dr.GetBoolean("BPPDownloadType"))
                	BppDes = "Personal Property + Real Property Data Editable has been Disabled to User " + userName
                End If
                
                If BppDes <> "" Then
                    SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), BppDes)
                End If
                
                BppDes = ""
                
	            If (chkbppAction2.Checked And Not dr.GetBoolean("BPPRealDataReadOnly")) Then
                	BppDes = "Real Property Data Read-Only has been Enabled to User " + userName
                Else If (Not chkbppAction2.Checked And dr.GetBoolean("BPPRealDataReadOnly"))
                	BppDes = "Real Property Data Read-Only has been Disabled to User " + userName
                End If
                
                If BppDes <> "" Then
                    SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), BppDes)
                End If
                 
                BindGridView()
                ClearForm()
                LoadUserForEdit(userName)
                If maxSeat.Count > 0 Then
                    Dim msg As String = ""
                    Dim role As String = ""

                    For Each de As DictionaryEntry In maxSeat
                        role += IIf(role = "", de.Key, ", " + de.Key)
                    Next
                    If maxSeat.Count > 1 Then
                        msg = "The roles " + role + " cannot be assigned. Maximum number of Seats purchased have already been assigned.\n\n"
                    Else
                        msg = "The role " + role + " cannot be assigned. Maximum number of Seats purchased have already been assigned.\n\n"
                    End If

                    For Each d As DictionaryEntry In maxSeat
                        msg += d.Key + "  - " + d.Value + "\n"
                    Next
                    Alert(msg)
                Else
                    Alert("Changes updated successfully.\n\nClick Close to go back to the list.")
                End If
                RunScript("maskHeight();")
                RunScript("initTooltip();")
                If (chkbpp.Checked) Then
                    chkbppdiv.Attributes.Add("style", "display:block")
                Else
                    chkbppdiv.Attributes.Add("style", "display:none")
                End If

                If chkLocked.Visible AndAlso chkLocked.Checked = False Then
                    u.UnlockUser()
                End If

            End If

        Catch ex As Exception
            Alert(ex.Message)
        End Try
    End Sub

    Sub AssignRolesFromList(userName As String)
        Dim applicationName As String = System.Web.Security.Membership.Provider.ApplicationName
        Dim sql As String = ""
        Dim orgid = HttpContext.Current.GetCAMASession.OrganizationId
        Dim autoCreatedUsers As String() = {"admin", "dcs-support", "vendor-cds", "dcs-qa", "System", "dcs-rd", "malite_admin", "dcs-integration", "dcs-ps"}
        Dim Description As String = ""
        
        For Each li As ListItem In cblRoles.Items
        	If li.Selected Then
        	    Dim dr As DataRow = Database.System.GetTopRow("EXEC  SP_RoleCount @ApplicationName = {0},@RoleName = {1},@OrgId = {2}".SqlFormatString(applicationName, li.Value, orgid))
                If Not Roles.IsUserInRole(userName, li.Value) Then
                    If dr IsNot Nothing AndAlso dr("Seat") IsNot DBNull.Value AndAlso dr.GetInteger("InUseCount") >= CType(dr("Seat"), Integer) And Not autoCreatedUsers.Contains(userName) Then
                        maxSeat.Add(dr.GetString("RoleName"), dr.GetString("Seat"))
                    Else
                    	Roles.AddUserToRole(userName, li.Value)
                    	Description = "Role "+li.Text.ToString+" has been Enabled to User "+userName+"." 
    	                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
                    	               
                        If li.Value = "SketchValidation" Then
                        	If Not Roles.IsUserInRole(userName,"SVReviewer") Then
                        		Roles.AddUserToRole(userName, "SVReviewer")
                        	End If	
                        End If
                        SqlNew += li.Value + ","
                    End If
                Else
                    If dr("Seat") IsNot DBNull.Value AndAlso dr.GetInteger("InUseCount") > CType(dr("Seat"), Integer) And Not autoCreatedUsers.Contains(userName) Then
                        maxSeat.Add(dr.GetString("RoleName"), dr.GetString("Seat"))
                        Roles.RemoveUserFromRole(userName, li.Value)
                        Description = "Role "+li.Text.ToString+" has been Disabled from User "+userName+"." 
                        SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
                        
                        If li.Value = "SketchValidation" And Roles.IsUserInRole(userName, "SVReviewer") Then
                        	Roles.RemoveUserFromRole(userName, "SVReviewer")
                        End If
                    End If
                End If
            Else
                If Roles.IsUserInRole(userName, li.Value) Then
                	 Roles.RemoveUserFromRole(userName, li.Value)
                     Description = "Role "+li.Text.ToString+" has been Disabled from User "+userName+"."  
                	 SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
                	 
                    If li.Value = "SketchValidation" And Roles.IsUserInRole(userName, "SVReviewer") Then
                    	Roles.RemoveUserFromRole(userName, "SVReviewer")
                    End If
                End If
            End If
        Next
    End Sub

    Sub LoadRolesList()
        Dim dtOrgRoles As DataTable = Database.System.GetDataTable("EXEC GetRoles 0, " + HttpContext.Current.GetCAMASession.OrganizationId.ToString)
        Dim roleNames As List(Of String) = dtOrgRoles.AsEnumerable.Select(Function(x) x.GetString("ASPRoleName")).ToList
        Dim chkBPPEnabled As DataRow = Database.Tenant.GetTopRow("SELECT BPPEnabled FROM Application")
        If HasDTRModule() Then roleNames.AddRange(New String() {"DTR-ReadOnly", "DTR-Editor", "DTR-Manager"})

        For Each role In roleNames
            If Not Roles.RoleExists(role) Then
                Roles.CreateRole(role)
            End If
        Next

        For Each role In Roles.GetAllRoles()
            If Not roleNames.Contains(role) Then
                If Roles.GetUsersInRole(role).Count > 0 Then
                    Roles.RemoveUsersFromRole(Roles.GetUsersInRole(role), role)
                End If
                Roles.DeleteRole(role)
            End If
        Next


        Dim dt As DataTable = Database.System.GetDataTable("EXEC GetRoles 1, " + HttpContext.Current.GetCAMASession.OrganizationId.ToString)
        Dim dtCopy As New DataTable
        dtCopy.Columns.Add("Name", GetType(String))
        dtCopy.Columns.Add("ASPRoleName", GetType(String))
        If Not dt Is Nothing Then
            For Each dr As DataRow In dt.Rows
                If dr(1) <> "DataSetup" Then
                    dtCopy.ImportRow(dr)
                End If
            Next
        End If

        Dim currentUserRole = Roles.GetRolesForUser()
        If currentUserRole.Contains("BasicSettings") Then
            cblRoles.DataSource = dtCopy
        Else
            cblRoles.DataSource = dt
        End If
        cblRoles.DataTextField = "Name"
        cblRoles.DataValueField = "ASPRoleName"
        cblRoles.DataBind()
        If (Membership.GetUser().ToString = "admin" Or Membership.GetUser().ToString() = "dcs-ps") Then
            chkDoNotTrack.Visible = True
        Else
            chkDoNotTrack.Visible = False
        End If
        If (chkBPPEnabled.GetString("BPPEnabled") = "1" Or chkBPPEnabled.GetString("BPPEnabled") = "True" Or chkBPPEnabled.GetString("BPPEnabled") = "true") Then
            chkbpp.Visible = True
            chkbppAction1.Visible = True
            chkbppAction2.Visible = True
        Else
            chkbpp.Visible = False
            chkbppAction1.Visible = False
            chkbppAction2.Visible = False
        End If
    End Sub

    Sub ddlMobileCarrierLoad()
        FillFromSqlWithDatabase(ddlMobileCarrier, Database.System, "SELECT ID, CarrierName, Gateway FROM MobileCarrier ORDER BY CarrierName", True)
    End Sub

    Sub LoadUserForEdit(user_Name As String)
        Dim currentUser As String = UserName()
        Dim auto_created_account() As String = {"admin", "dcs-support", "vendor-cds", "dcs-qa", "System", "dcs-rd", "malite_admin", "dcs-integration", "dcs-ps"}
        If auto_created_account.Contains(user_Name) Then
            If Roles.IsUserInRole(currentUser, "BasicSettings") Then
                RunScript("setBasicSettings();")
            End If
        End If
        txtUserMail.Enabled = True
        hdnUserId.Value = user_Name
        
        Dim u As MembershipUser = Membership.GetUser(user_Name)
        If u Is Nothing Then
            Dim tr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM UserSettings WHERE LoginId='" + user_Name + "'")
            Membership.CreateUser(user_Name, "123456", tr.Get("Email"))
          	u = Membership.GetUser(user_Name)
        End If
            

'        txtUserMail.Text = u.Email
'        txtLoginId.Text = u.UserName
         txtLoginId.Enabled = False
        '        rfvPassword.Enabled = False

        Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM UserSettings WHERE LoginId='" + u.UserName + "'")
        If dr IsNot Nothing Then
        	
        	txtUserMail.Text = dr.GetString("Email")
            txtLoginId.Text = dr.GetString("LoginId")
           
            txtFirstName.Text = dr.GetString("FirstName")
            txtMiddleName.Text = dr.GetString("MiddleName")
            txtLastName.Text = dr.GetString("LastName")
            txtMobileNumber.Text = dr.GetString("Mobile")

            ' ddlMobileCarrier.SelectedValue = IIf(dt.Rows(0)("MobileCarrierId") IsNot Nothing, ddlMobileCarrier.Items.IndexOf(ddlMobileCarrier.Items.FindByValue(dt.Rows(0)("MobileCarrierId").ToString())), ddlMobileCarrier.Items.IndexOf(ddlMobileCarrier.Items.FindByValue("15")))
            ddlMobileCarrier.SelectedValue = dr.GetString("MobileCarrierId")

            cbNotifybyEmail.Checked = dr.GetBoolean("NotifyByEmail")
            cbNotifybySMS.Checked = dr.GetBoolean("NotifyBySMS")
            cbAutoQC.Checked = dr.GetBoolean("AutoQC")
            chkbpp.Checked = dr.GetBoolean("IsBPPAppraiser")
            chkbppAction1.Checked = dr.GetBoolean("BPPDownloadType")
            chkbppAction2.Checked = dr.GetBoolean("BPPRealDataReadOnly")
            chkDoNotTrack.Checked = dr.GetBoolean("DoNotTrack")
        Else
            txtFirstName.Text = ""
        End If

        For Each li As ListItem In cblRoles.Items
            li.Enabled = True
            If Roles.IsUserInRole(user_Name, li.Value) Then
                li.Selected = True
                'If li.Value = "BasicSettings" And li.Selected = True Then
                'For Each list As ListItem In cblRoles.Items
                ' If list.Value = "DataSetup" Then list.Enabled = False
                'Next
                'End If
            Else
                li.Selected = False
            End If
            If (user_Name = "admin" Or user_Name = "dcs-support" Or user_Name = "dcs-ps") Then
                If li.Value = "BasicSettings" Then
                    li.Attributes.Add("style", "display:none")
                End If
                If li.Value = "DataSetup" Then
                    li.Enabled = False
                End If
            End If
            Dim role = Roles.GetRolesForUser(UserName)
            If (Membership.GetUser().ToString() = "admin" Or Membership.GetUser().ToString() = "dcs-qa" Or Membership.GetUser().ToString() = "dcs-rd" Or Membership.GetUser().ToString() = "dcs-support" Or Membership.GetUser().ToString() = "dcs-integration" Or Membership.GetUser().ToString() = "dcs-ps" Or role.Contains("DataSetup") Or role.Contains("BasicSettings")) Then
                If (li.Value = "SketchValidation" Or li.Value = "DTR" Or li.Value = "CompSales" Or li.Value = "MRA") Then
                    li.Enabled = True
                End If
            Else
                If (li.Value = "SketchValidation" Or li.Value = "DTR" Or li.Value = "CompSales" Or li.Value = "MRA") Then
                    li.Enabled = False
                End If
            End If

        Next
        If (chkbpp.Checked) Then
            chkbppdiv.Attributes.Add("style", "display:block")
        Else
            chkbppdiv.Attributes.Add("style", "display:none")
        End If
        btnCancel.Text = "Close"

        If u.IsLockedOut Then
            chkLocked.Checked = True
        Else
            chkLocked.Visible = False
        End If

        If user_Name = "vendor-cds" Then
            Dim vendorCDSRoles = {"DataSetup", "TaskManager", "MobileAssessor", "Reports", "Tracking", "QC"}
            For Each list As ListItem In cblRoles.Items
                If Not vendorCDSRoles.Contains(list.Value) Then
                    list.Enabled = False
                End If
            Next
            txtUserMail.Enabled = False
        End If

        If user_Name = " malite_admin" Then
            Dim vendorCDSRoles = {"DataSetup", "TaskManager", "MobileAssessor", "Reports", "Tracking", "QC"}
            For Each list As ListItem In cblRoles.Items
                If Not vendorCDSRoles.Contains(list.Value) Then
                    list.Enabled = False
                End If
            Next
            txtUserMail.Enabled = False
        End If


        If user_Name = "dcs-rd" And Not currentUser = "admin" Then
            RunScript("setDcsRdUser();")
        End If
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        RunScript("hidePopup();")
    End Sub

    Public ReadOnly Property UserLastLoginDate As String
        Get
            If Membership.GetUser(Eval("LoginId").ToString) Is Nothing Then
                Return String.Empty
            Else
                If Membership.GetUser(Eval("LoginId").ToString).LastLoginDate <> Membership.GetUser(Eval("LoginId").ToString).CreationDate Then
                    Dim userId = Membership.GetUser(Eval("LoginId").ToString).ProviderUserKey.ToString()
                    Dim createDate = Membership.GetUser(Eval("LoginId").ToString).CreationDate.ToString()
                    Dim CurrentUser = Membership.GetUser().ProviderUserKey.ToString()
                    If userId = CurrentUser Then
                        Dim LastLoginTime As String = Database.System.GetStringValue("IF EXISTS(SELECT * FROM LoginDetails WHERE UserId='" + userId + "' and ApplicationId = 2 ) SELECT LastLoginTime from LoginDetails where UserId ='" + userId + "' and ApplicationId = 2")
                        If LastLoginTime <> createDate Then
                            If LastLoginTime <> "" Then
                                Dim LastLoginTime_dateTime As DateTime = Convert.ToDateTime(LastLoginTime)

                                Return Database.Tenant.GetStringValue("SELECT dbo.GetLocalDate(" + LastLoginTime_dateTime.ToSqlValue + ")")

                            Else
                                Return Database.Tenant.GetStringValue("SELECT dbo.GetLocalDate('" + Membership.GetUser(Eval("LoginId").ToString).LastActivityDate.ToString("yyyy-MM-dd HH:mm:ss") + "')")
                            End If
                        Else

                            Return "Never"
                        End If
                    Else
                        Dim LoginTime As String = Database.System.GetStringValue("IF EXISTS(SELECT * FROM LoginDetails WHERE UserId='" + userId + "' and ApplicationId = 2 ) SELECT LoginTime from LoginDetails where UserId ='" + userId + "' and ApplicationId = 2")

                        If LoginTime <> "" Then
                            Dim LoginTime_dateTime As DateTime = Convert.ToDateTime(LoginTime)

                            Return Database.Tenant.GetStringValue("SELECT dbo.GetLocalDate(" + LoginTime_dateTime.ToSqlValue + ")")

                        Else
                            Return Database.Tenant.GetStringValue("SELECT dbo.GetLocalDate('" + Membership.GetUser(Eval("LoginId").ToString).LastActivityDate.ToString("yyyy-MM-dd HH:mm:ss") + "')")
                        End If
                        '          				
                    End If
                Else
                    Return "Never"
                End If
            End If
        End Get

    End Property

    Public Function IsUserLocked() As Boolean
        Dim loginId As String = Eval("LoginId")
        Dim u As MembershipUser = Membership.GetUser(loginId)
        If u Is Nothing Then
            Return False
        End If
        If u.IsLockedOut Then
            Return True
        End If
        Return False
    End Function

    Public Function UserStatus() As String
        Dim loginId As String = Eval("LoginId")
        Dim u As MembershipUser = Membership.GetUser(loginId)
        If u Is Nothing Then
            Return "invalid"
        End If
        If u.IsLockedOut Then
            Return "locked"
        End If
        If Not u.IsApproved Then
            Return "pending"
        End If
        Return "ok"
    End Function

    Sub CreateRolesIfNotExists(ParamArray roleNames() As String)
        For Each roleName As String In roleNames
            If Not Roles.RoleExists(roleName) Then
                Roles.CreateRole(roleName)
            End If
        Next
    End Sub

    Sub DeleteRolesIfExists(ParamArray roleNames() As String)
        For Each roleName As String In roleNames
            If Roles.RoleExists(roleName) Then
                Roles.RemoveUsersFromRole(Roles.GetUsersInRole(roleName), roleName)
                Roles.DeleteRole(roleName)
            End If
        Next
    End Sub


    <WebMethod()>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Shared Function GetUser(pre As String, UserName As String, SearchBy As String) As List(Of String)
        Dim Users As New List(Of String)()
        Dim dt As DataTable
        Dim sql As String
        Dim search As String
        pre = pre.Replace("%","[%]")
        pre = pre.Replace("_","[_]")
        
        If SearchBy = "1" Then
            search = "LoginId"
        ElseIf SearchBy = "2" Then
            search = "Firstname + ' ' + ISNULL(LastName,'')"
        ElseIf SearchBy = "3" Then
            search = "Email"
        End If

        If UserName = "dcs-support" Then
            sql = "SELECT Id," & search & " FROM UserSettings WHERE " & search & " like '" & pre & "%'  ORDER BY " & search & " "
        Else
            sql = "SELECT Id," & search & " FROM UserSettings WHERE " & search & " like '" & pre & "%' AND LoginId NOT IN ('dcs-support') ORDER BY " & search & " "
        End If

        dt = Database.Tenant.GetDataTable(sql)

        If Not dt Is Nothing Then
            For Each dr As DataRow In dt.Rows
                Users.Add(String.Format("{0}/{1}", dr.Item(1), dr.Item(0)))
            Next
            Return Users
        Else
            Return Nothing
        End If

    End Function
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        LoadGrid()
    End Sub
    Sub LoadGrid(Optional ByVal pageIndex As Integer = 0)
        Dim customerId As String = Request.Form(hfCustomerId.UniqueID)
        If customerId <> "" Then
            gdvUserDetails.PageIndex = pageIndex
            gdvUserDetails.DataSource = Database.Tenant.GetDataTable("SELECT * FROM UserSettings WHERE Id = {0}".SqlFormatString(customerId))
        Else
            If txtSearch.Text.Trim <> "" Then
                Dim search As String
                Dim sql As String
                Dim textSearch As String = txtSearch.Text
                textSearch = textSearch.Replace("%","[%]") 
                Select Case ddlFilter.SelectedValue
                    Case "1"
                        search = "LoginId"
                    Case "2"
                        search = "Firstname + ' ' + ISNULL(LastName,'')"
                    Case "3"
                        search = "Email"
                End Select
                If UserName() = "dcs-support" Then
                    sql = "SELECT * FROM UserSettings WHERE " & search & " like '%" & textSearch.Trim().Replace("'", "''")& "%'  ORDER BY " & search & " "
                Else
                    sql = "SELECT * FROM UserSettings WHERE " & search & " like '%" & textSearch.Trim().Replace("'", "''") & "%' AND LoginId NOT IN ('dcs-support') ORDER BY " & search & " "
                End If
                gdvUserDetails.PageIndex = pageIndex
                gdvUserDetails.DataSource = Database.Tenant.GetDataTable(sql)
            Else
                BindGridView()
            End If
        End If
        gdvUserDetails.DataBind()
        hfCustomerId.Value = Nothing
    End Sub

    Private Sub ddlFilter_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFilter.SelectedIndexChanged
        txtSearch.Text = ""
    End Sub
    Public Enum ConfigFileType
        UserRoles
    End Enum
    Protected Sub lbExport_Click(sender As Object, e As System.EventArgs) Handles lbExport.Click
    	ExportUserSettings(ConfigFileType.UserRoles, Response, "UserSettings")
    	Response.Redirect("~/protected/datasetup/users.aspx")
    End Sub
    Public Sub ExportUserSettings(configType As ConfigFileType, response As HttpResponse, tag As String)
        Dim doc = ExportUserSettings(configType)
        Dim xml As String = doc.ToString(SaveOptions.None)
        Dim exportName = HttpContext.Current.GetCAMASession.OrganizationCodeName + "-" + tag + "-" + Now.ToString("yyMM-dd-HHmmss") + ".xml"
        response.WriteAsAttachment(xml, exportName)
    End Sub
    Public Function ExportUserSettings(configType As ConfigFileType) As XDocument
        Dim doc = New XDocument(New XDeclaration("1.0", "utf-8", True))
        Dim parentRoot = <CAMACloudUsers></CAMACloudUsers>
        doc.Add(parentRoot)
        Dim xdoc As XDocument = doc
        If configType = ConfigFileType.UserRoles Then
            _exportUserRoles(xdoc)
        End If
        Return xdoc
    End Function
    Public Sub _exportUserRoles(ByRef xdoc As XDocument)
        Dim OrgId As String = HttpContext.Current.GetCAMASession().OrganizationId
        Dim ApplicationName As String = System.Web.Security.Membership.Provider.ApplicationName
        Dim AutoCreatedAccount As String = "'admin','dcs-support','dcs-qa','vendor-cds','dcs-rd','malite_admin','dcs-integration','dcs-ps'"
        Dim query As String
        query = "EXEC sp_ExportUserRoles @ApplicationName={0},@orgId={1}, @AutoCreatedAccount= {2}".SqlFormatString(ApplicationName, OrgId, AutoCreatedAccount)
        Dim dtUsersRoles As DataTable = Database.System.GetDataTable(query)
        If dtUsersRoles.Rows.Count > 0 Then
            For Each drow As DataRow In dtUsersRoles.Rows
                Dim user As New XElement("User")
                user.Attribute("UserName")
                Dim uname As String = drow.Item(0).ToString()
                user.SetAttributeValue("UserName", uname)
                Dim roles As New XElement("Roles")
                user.Add(roles)
                For Each column As DataColumn In dtUsersRoles.Columns
                    If column.ColumnName.ToString() <> "LoginId" Then
                        Dim val As String = drow.GetString(column.ColumnName.ToString())
                        If (val <> "False") Then
                            Dim role As New XElement("Role")
                            roles.Add(role)
                            role.Attribute("Name")
                            role.SetAttributeValue("Name", column.ColumnName.ToString())
                        End If
                    End If
                Next
                xdoc.Root.Add(user)
            Next
        End If
    End Sub

    Protected Sub lbImport_Click(sender As Object, e As System.EventArgs) Handles btnUploadImportFile.Click
        If fuImportFile.HasFile Then
            Try
                ImportUserSettings(fuImportFile.FileContent)
                If UserName() = "dcs-support" Then
                    gdvUserDetails.DataSource = Database.Tenant.GetDataTable("SELECT * FROM UserSettings ORDER BY FirstName")
                Else
                    gdvUserDetails.DataSource = Database.Tenant.GetDataTable("SELECT * FROM UserSettings WHERE LoginId NOT IN ('dcs-support') ORDER BY FirstName")
                End If
                gdvUserDetails.DataBind()
                txtSearch.Text = ""
                Dim Description As String = "User Roles Settings has been Updated."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
                Alert("User Roles Settings imported successfully")
            Catch ex As Exception
                Alert(ex.Message)
                Return
            End Try
        End If
    End Sub
    Public Sub ImportUserSettings(stream As IO.Stream)
        Dim xDoc As XDocument = XDocument.Load(stream)
        _importUserRoles(xDoc)
    End Sub
    Private Sub _importUserRoles(doc As XDocument)
    	
        ValidateXml(doc) 
    		
        Dim applicationName As String = System.Web.Security.Membership.Provider.ApplicationName
        Dim OrgId As String = HttpContext.Current.GetCAMASession().OrganizationId
        Dim AutoCreatedAccount As String = "'admin','dcs-support','dcs-qa','vendor-cds','dcs-rd', 'malite_admin','dcs-integration','dcs-ps'"
        Dim currentUser As String = UserName()
        Dim NotAccessibleUser As String = "''''"
        If Roles.IsUserInRole(currentUser, "BasicSettings") Then
            NotAccessibleUser = "'System'"
        End If
        Dim query As String
        query = "EXEC [sp_ImportUserRoles] @ApplicationName={0},@orgId={1},@XMLData={2}, @AutoCreatedAccount= {3},@NotAccessibleUser={4},@currentUser={5},@SeatLogic = {6}".SqlFormatString(applicationName, OrgId, doc.ToString(), AutoCreatedAccount, NotAccessibleUser, currentUser, 1)
        Dim dt As DataTable = Database.System.GetDataTable(query)
        Dim seatNotavailableRoles As List(Of String) = New List(Of String)
        Dim message As String = ""
        Dim msg As String = ""
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                message += "The role " + dr("RoleName").ToString() + " can't be assigned to " + dr("UserName").ToString() + " \n\"
                msg += dr("RoleName").ToString() + "  -  " + dr("Seat").ToString() + "\n"
            Next
            message += "Maximum number of Seats purchased have already been assigned.\n" + msg
            Alert(message)
        End If
    End Sub
    
    Sub ValidateXml(xdoc As XDocument)
    	Dim flag As Boolean = True
    	
    	Dim node = xdoc.Elements.Where(Function(x) x.Name = "CAMACloudUsers").FirstOrDefault()
    	
    	If node Is Nothing Then
    		flag = False
    	Else
    		Dim rnode = node.Elements.Where(Function(x) x.Name = "User").FirstOrDefault()
    	
	        If rnode Is Nothing Then
	        	flag = False
	        Else
	        	If Not rnode.FirstAttribute.Name.ToString = "UserName" Then
	        		flag = False
	        	Else
	        		Dim Childnode = rnode.Elements.Where(Function(x) x.Name = "Roles").FirstOrDefault()
	        		If Childnode Is Nothing Then
	        			flag = False
	        		End If
	    	    End If
	        End If
    	End If
    	
        If Not flag Then
        	Throw New Exception("You have uploaded an invalid User file")
        End If
    End Sub
End Class


