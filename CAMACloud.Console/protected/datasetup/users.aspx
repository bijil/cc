﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/DataSetup.master" AutoEventWireup="false" EnableEventValidation="false" Inherits="CAMACloud.Console.datasetup_users"
    CodeBehind="users.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
    	var _currentScroll = 0;
        function getChecked() {
            $('.cblRoleCheck input').each(function () {
                $(this).change(function () {
                    var value = $(this).val();

                    if (value == 'BasicSettings') {
                        if ($(this).is(':checked')) {
                            $('.cblRoleCheck input[value="DataSetup"]').prop('disabled', true).prop('checked', false);
                            return;
                        }
                        else {
                            $('.cblRoleCheck input[value="DataSetup"]').prop('disabled', false);
                            return;
                        }
                    }
                                       
                  	if (value == 'DataSetup') {
                        if ($(this).is(':checked')) {
                            $('.cblRoleCheck input[value="BasicSettings"]').prop('disabled', true).prop('checked', false);
                            return;
                        }
                        else {
                        	if ($('#MainContent_MainContent_txtLoginId').val() == "vendor-cds")
                        		$('.cblRoleCheck input[value="BasicSettings"]').prop('disabled', true).prop('checked', false);
                        	else
                            	$('.cblRoleCheck input[value="BasicSettings"]').prop('disabled', false);
                            return;
                        }
                    }
                });
                var fvalue = $(this).val();
                if (fvalue == 'BasicSettings') {
                    if ($(this).is(':checked')) {
                        $('.cblRoleCheck input[value="DataSetup"]').prop('disabled', true).prop('checked', false);
                        return;
                    }
                    else {
                        $('.cblRoleCheck input[value="DataSetup"]').prop('disabled', false);
                         if($('.cblRoleCheck input[value="DataSetup"]').is(':checked'))
                           $('.cblRoleCheck input[value="BasicSettings"]').prop('disabled', true);
                        return;
                    }
                }
            });
             var ddlCarrier = $( "#" + '<%=  ddlMobileCarrier.ClientID%>' ).val();
             	if(ddlCarrier!=""){
             	$('#spanMobid').show();
         	}
        }
        
        function maskHeight() {
        var dheight=document.documentElement.scrollHeight;
         $('.ui-widget-overlay').css({"height":dheight,"width":"100%"});
        }
        
        function showPopup(title) {
        	var dheight = document.documentElement.scrollHeight;
            $('.user-edit-panel').html('');
            $('.edit-popup').dialog({
                modal     : true,
                width     : 825,
                height    : 455,
                resizable : false,
                title     : title,
                open      : function (type, data) {
                    $(this).parent().appendTo("form");                   
                    setTimeout(function (){$('.tip').tipr();
					$('.tip', $('.ui-dialog').contents()).tipr($(".ui-dialog").contents());},2000);               
                },
                close     : function(event, ui) {
                	//$(window).scrollTop(_currentScroll);
                	EnableScrollbar(true);
                }
            });
            _currentScroll = $(window).scrollTop();
            $(window).scrollTop(0);
         	$('.ui-widget-overlay').css({"height":dheight,"width":"100%"});			
            $('.ui-dialog').css({'left':'24%','top':'18%'});
            
            EnableScrollbar(false);
            
            initTooltip();
        }

		function EnableScrollbar(flag)
		{
			 if(flag)
			 {
			 	 $("body").css("overflow" , "");
				 $(window).scrollTop(0);
			 }				 
			 else  $("body").css({"overflow":"hidden"}); 
		}
		
        function confirmAlert() {
            var pword = $("#" + '<%= txtPassword.ClientID%>').val();
            var cpword = $("#" + '<%= txtPassword2.ClientID%>').val();
            var mail = $( "#" + '<%= txtUserMail.ClientID%>' ).val();            
            var remail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            var fname = $( "#" + '<%= txtFirstName.ClientID%>' ).val();
            var mobil = $( "#" + '<%= txtMobileNumber.ClientID%>' ).val();
    		var remobil =/\d{10,11}/;
            var ddlCarrier = $( "#" + '<%=  ddlMobileCarrier.ClientID%>').val();
            if(pword!=""){
            if(pword.trim()==""){
            	alert("Blank space is not allowed, please enter a valid password!.");
                return false;
            }
            if(pword.length<6){ 
            	alert("Password must be minimum 6 characters in length.");
                return false;
            }
            }
            if (pword != cpword) {
                alert("Password does not match the confirm password.");
                return false;
            }
            if(!fname){
            	alert("First name is required, please enter your first name!");
            	return false;
            }
            if(!mail){
            	alert("Email Address is required, please enter a valid email address!")
            	return false;
            }
            else if(!remail.test(String(mail).toLowerCase())) {
	            alert("Invalid Email!");
	            return false;
           
            }
            if (mobil) {
                if (!remobil.test(String(mobil))) {
                    alert("Invalid Mobile Number!");
                    return false;

                }
            }
           
			if(ddlCarrier!=""){
				if(!mobil){
	            	alert("Mobile number is required, please enter a valid mobile number!");
	            	return false;
	            }
	            else if (!remobil.test(String(mobil))){
	                alert( "Invalid Mobile Number!" );
	                return false;
	
	            }
			}
            var hdnValue = $("#" + '<%= checkBtnClick.ClientID%>').val();
            if (hdnValue == "0") {
                return confirm("Are you sure you want to modify these settings? Please note, if a Sync Service manages user information these settings will be replaced with your system's user information upon the next Upsync.");
            }
        }

        function hidePopup() {
            $('.edit-popup').dialog('close');  
        }

        function UserDeleteConfirmation(that) {
            var logonId = $(that).attr('username');
            var autoCreateUsers = ["admin", "dcs-support", "vendor-cds", "dcs-qa", "dcs-rd", " malite_admin", "dcs-ps"];
            if (autoCreateUsers.indexOf(logonId) > -1) {
                return true
            } else {
                return confirm("Are you sure that you want to delete this user? Please note, if a Sync Service manages user information these settings will be replaced with your system's user information upon the next Upsync.");
            }
        }

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(InitializeRequest);
            prm.add_endRequest(EndRequest);
            // Place here the first init of the autocomplete
            InitAutoCompl();            
        });

        function InitializeRequest(sender, args) {
        }

        function EndRequest(sender, args) {
            // after update occur on UpdatePanel re-init the Autocomplete
            InitAutoCompl();
        }
        function InitAutoCompl() {
            $('#<%=txtSearch.ClientID%>').autocomplete({
                source: function (request, response) {
                
                    if(request.term.indexOf('"') >= 0)  { return false; }
                    
                    var req = JSON.stringify(request.term).replace(/'/g, '').replace(/^\s+|\s+|$/g, '');
                    var term = req.replace(/"/g, '');
                    if (!term) { return false; }
                    $.ajax({
                        url: "users.aspx/GetUser",
                        data: "{ 'pre':'" + term + "','UserName':'" + $('#<%= hfCurrentUser.ClientID%>').val() + "','SearchBy':'" + $('#<%= ddlFilter.ClientID%>').val() + "'}",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.split('/')[0],
                                    val: item.split('/')[1]
                                }
                            }))
                        },
                        error: function (response) {
                            alert(response.responseText);
                        },
                        failure: function (response) {
                            alert(response.responseText);
                        }
                    });
                },
                select: function (e, i) {
                    $("#<%=hfCustomerId.ClientID %>").val(i.item.val);
                },
                minLength: 1
            });
        }

        function ValidateSearch() {            
            var txt = $('#<%=txtSearch.ClientID%>').val();
			if (txt == null || txt.trim() === "" && ($('.textboxAuto').is(':focus') || $('.btnSearch').is(':focus'))) {
                alert("Please enter a search term.");
                return true;
            }
            else {
    $('.textboxAuto').val(txt).blur();
                return true;
            }
        }
$(document).on('click', function(event) {
    if (!$(event.target).closest('.textboxAuto').length) {
        $('.textboxAuto').autocomplete('close');
    }
});

        function setBasicSettings()
        {
            $('.setRole').find('*').each(function () { $(this).attr("disabled", true); })
            $("#" + '<%= btnSaveUser.ClientID%>').attr("disabled", true);
            $('.user-edit-form').find('*').each(function () { $(this).attr("disabled", true); })
        }

        function setDcsRdUser() {
            $('.user-edit-form').find('*').each(function () { $(this).attr("disabled", true); })
        }

        function Logintext(event) {
            
            var x = event.charCode || event.keyCode;
            if  (x == 222)
            {
                
                event.preventDefault();
                return false;
            }
        }

        function textFieldValidation(that) {
            var len = $( that ).val().length;
            var limit = parseInt( $( that ).attr( 'maxlength' ) );
			if (len >= limit)
                alert( 'You are exceeding the length allowed {' + limit + ' characters}.\nPlease shorten it to ' + limit + '  characters or less.');
        }
        function SelectedMobileCarrier(id)
        {
        	var option = id.options[id.selectedIndex].text;
        	if(option!='-- Select --'){
        		$('#spanMobid').show();
        		return false;
        	}
        	else{
        		$('#spanMobid').hide();
        		return false;
    		}
        }
        function isNumber(evt) {
		    evt = (evt) ? evt : window.event;
		    var charCode = (evt.which) ? evt.which : evt.keyCode;
		    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		        return false;
		    }
		    return true;
		}
		function emailvalidation(){
			$('.emailvalidation').hide();
			var mail = $( "#" + '<%= txtUserMail.ClientID%>' ).val();            
            var remail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if(mail!=""){
            if(!remail.test(String(mail).toLowerCase())) {	            
				$('.emailvalidation').show();
			}
		}
	}
	    (function($) {
       chkfunction();
})(jQuery);
	
	function chkfunction(){
			if($('#MainContent_MainContent_chkbpp').length)
				{
					if(document.getElementById('<%=chkbpp.ClientID%>').checked)
						{
						document.getElementById("<%=chkbppdiv.ClientID%>").style.display="block";
						}
					else
						{
						document.getElementById("<%=chkbppdiv.ClientID%>").style.display="none";
						}
			}
	}
	function mutChkbox(chk) {
	        var chkList = chk.parentNode.parentNode;
	        var chks = chkList.getElementsByTagName("input");
	        for (var i = 0; i < chks.length; i++) {
	            if (chks[i] != chk && chk.checked) {
	                chks[i].checked = false;
	            }
	        }
	    }
    </script>
    	<style type="text/css">	
		.spacer
		{
			display: inline-block; 
			width: 30px;
		}
        .user-spacer
		{
			display: inline-block;
			width: 39px;
		}
        .search-panel 
        {
            padding    : 7px;
            border     : 1px solid #CFCFCF;
            width      : 787px ;
            background : -webkit-gradient(linear, 0% 0%, 0% 100%, from(#CFCFCF), to(#F8F8F8));
        }
        .search-panel label 
        {
             margin-right : 5px;

        }

        .ui-autocomplete { 
            cursor:pointer; 
            max-height: 500px;
            overflow-y:auto;
            overflow-x:hidden;
        }  
        .mGrid .empty-row-template TD {
            width: 775px;
            text-align: center;
        }
       .WordWrap {
            width: 100%;
            word-break: break-all;
        }
       
       .required{
   	    	color:red;
		}
		.chkdiv{
			margin-left:15px
			}
		.tipr_container_bottom{
		margin-top:-100px !important
		}
	</style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <h1>Manage Users</h1>
    <asp:UpdatePanel runat="server" ID="uplGrid">
        <ContentTemplate>
            <div class="link-panel" ID="NewUser" runat="server">
                <asp:LinkButton runat="server" ID="lbNewUser" Text="Add New User" OnClientClick="showPopup('Create new CAMA user');" />
            </div>
              <asp:HiddenField ID="hfCurrentUser" runat="server" />
             <asp:HiddenField ID="hfCustomerId" runat="server" />
            <div class="search-panel">
                <table>
                    <tr>
                        <td>
                            <label>
                                Search By:</label>
                            <asp:DropDownList runat="server" ID="ddlFilter" AutoPostBack="true">
                                <asp:ListItem Text="Login Id" Value="1" />
                                <asp:ListItem Text="Name" Value="2" />
                                <asp:ListItem Text="Email" Value="3" />
                            </asp:DropDownList>

                        </td>
                        <td style="width: 10px;"></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtSearch" Width="300px" MaxLength="30" CssClass="textboxAuto"/>
                        </td>
                        <td style="width: 10px;">
                        </td>
                        <td>
                            <asp:Button runat="server" ID="btnSearch" Text=" Search " cssClass="btnSearch" OnClientClick="return ValidateSearch()" />
                        </td>
                        <td style="width: 30px;">
                        <td>
                            <asp:LinkButton runat="server" ID="lbExport" Text="Export"/>&nbsp;&nbsp;<span style="font-weight: normal; color: Black;">|</span>&nbsp&nbsp;
                        </td>
                        <td>
                            <asp:LinkButton runat="server" ID="lbImport" Text="Import" OnClientClick="document.getElementById('fuImportFile').click();return false;" />
                        </td>
                    </tr>
                </table>
            </div>
            <div class="clear">
                <div style="display: none;">
                    <asp:FileUpload runat="server" ID="fuImportFile"  ClientIDMode="Static" onchange="document.getElementById('btnUploadImportFile').click()" accept="application/xml" />
                    <asp:Button runat="server" ID="btnUploadImportFile" ClientIDMode="Static" />
                </div>
            </div>
            <div class="WordWrap">
            <asp:GridView ID="gdvUserDetails" runat="server" AutoGenerateColumns="false"  ShowHeaderWhenEmpty="True" AllowPaging="true"  PageSize="50" Width="804px">
                <Columns>
                    <asp:TemplateField>
                        <ItemStyle Width="50px" />
                        <ItemTemplate>
                            <%# Container.DataItemIndex + 1 %>.
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Full Name">
                        <ItemStyle Width="200px" />
                        <ItemTemplate>
                             <%# Eval("FirstName") + " "+Eval("MiddleName") + " " + Eval("LastName")%>
                        </ItemTemplate>
                    </asp:TemplateField>                    
                    <asp:BoundField HeaderText="Login Id" DataField="LoginId">
                        <ItemStyle Width="160px"></ItemStyle>
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Status">
                        <ItemStyle Width="80px" />
                        <ItemTemplate>
                            <%#UserStatus() %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Last Login">
                        <ItemStyle Width="160px" />
                        <ItemTemplate>
                            <%#UserLastLoginDate %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Last Login IP" ItemStyle-Width="120px" Visible="false">
                        <ItemTemplate>
                            <%# Eval("LoginId")%>
                        </ItemTemplate>
                        <ItemStyle Width="120px"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Email">
                        <ItemStyle Width="250px" />
                        <ItemTemplate>
                            <%--<%# Eval("Email")%>--%>
                            <%# Eval("Email").ToString().ToLower()%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemStyle Width="140px" />
                        <ItemTemplate>
                            <asp:LinkButton runat="server" CommandName="EditUser" Text="Edit" CommandArgument='<%# Eval("LoginId") %>' OnClientClick="showPopup('Edit User Properties')" />
                            <asp:LinkButton ID="lbtnDeleteUser" Text="Delete" runat="server" CommandName="DeleteUser" CommandArgument='<%# Eval("LoginId")%>' UserName='<%# Eval("LoginId") %>' CausesValidation="false" OnClientClick="if ( ! UserDeleteConfirmation(this)) return false;" ></asp:LinkButton>
                            <asp:LinkButton ID="lbtnUnlock" Text="Unlock" runat="server" CommandName="UnlockUser" CommandArgument='<%# Eval("LoginId")%>' CausesValidation="false" Visible='<%#IsUserLocked() %>'></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    <label style="color:Red;font-weight:bold">No user found !</label>
                </EmptyDataTemplate>
            </asp:GridView>
            </div>   
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUploadImportFile"/>
        </Triggers>
    </asp:UpdatePanel>
    <div class="edit-popup" style="display: none;" >
        <asp:UpdatePanel ID="uplEdit" runat="server">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlUserEdit" CssClass="user-edit-panel" Style="width: 800px; height: 410px;">
                    <asp:HiddenField ID="hdnUserId" runat="server" />
                    <table class="user-edit-table">
                        <tr>
                            <td style="width: 8px;"></td>
                            <td>
                                <h3>User Properties</h3>
                                <table class="user-edit-form">
                                    <tr>
                                        <td>First Name:<span class="required">*</span></td>
                                        <td>
                                            <asp:TextBox ID="txtFirstName" runat="server" MaxLength="50" onkeypress="textFieldValidation(this);" autocomplete="off"></asp:TextBox>
                                           <%--  <asp:RequiredFieldValidator ID="rfvtxtFirstName" runat="server" ForeColor="Red" Display="Dynamic" ControlToValidate="txtFirstName"
                                                ValidationGroup="UserProp"></asp:RequiredFieldValidator>--%>
                                            <%-- <asp:RequiredFieldValidator
                                                ID="rfvtxtFirstNameForEdit" runat="server" ForeColor="Red" Display="Dynamic" ControlToValidate="txtFirstName"
                                                ValidationGroup="UserProp2"></asp:RequiredFieldValidator>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Middle Name </td>
                                        <td>
                                            <asp:TextBox ID="txtMiddleName" MaxLength="50" onkeypress="textFieldValidation(this);" runat="server"></asp:TextBox>
                                            <%--<asp:RequiredFieldValidator
												ID="MiddleNameValidator" runat="server" ForeColor="Red" ControlToValidate="txtMiddleName"
												ValidationGroup="UserProp"></asp:RequiredFieldValidator>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Last Name </td>
                                        <td>
                                            <asp:TextBox ID="txtLastName" MaxLength="50" onkeypress="textFieldValidation(this);" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Email:<span class="required">*</span></td>
                                        <td>
                                            <asp:TextBox ID="txtUserMail" MaxLength="50" onkeypress="textFieldValidation(this);" onchange="emailvalidation();" runat="server" autocomplete="off" style="text-transform:lowercase"></asp:TextBox>
                                            <span class="emailvalidation" style="display:none; color:red;">Invalid Email!</span>
                                           <%-- <asp:RequiredFieldValidator ID="rfvtxtUserMail" runat="server" ForeColor="Red"  ControlToValidate="txtUserMail" Display="Dynamic"
                                                ValidationGroup="UserProp"></asp:RequiredFieldValidator>--%>
                                            <%--  <asp:RequiredFieldValidator
                                                ID="RequiredFieldValidator3" runat="server" ForeColor="Red" 
                                                ControlToValidate="txtUserMail" Display="Dynamic" ValidationGroup="UserProp2"></asp:RequiredFieldValidator>--%>
                                        <%--  <asp:RegularExpressionValidator ID="revtxtUserMail" runat="server" ControlToValidate="txtUserMail" ErrorMessage="Invalid Email!" ForeColor="#D20B0E"
                                                ValidationExpression="^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]+$" Display="Dynamic" ValidationGroup="UserProp"></asp:RegularExpressionValidator>--%>
                                            <%--  <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtUserMail"
                                                ErrorMessage="Invalid Email!" ForeColor="#D20B0E" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                Display="Dynamic" ValidationGroup="UserProp2"></asp:RegularExpressionValidator>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Mobile Number<span id="spanMobid" class="required" style="display:none">*</span> </td>
                                        <td>
                                            <asp:TextBox ID="txtMobileNumber" runat="server" onkeypress="return isNumber(event)" MaxLength="10" ></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="revtxtMobileNumber" runat="server" ForeColor="Red" ControlToValidate="txtMobileNumber" ErrorMessage="Invalid MobileNo"
                                                ValidationExpression="\d{10,11}" ValidationGroup="UserProp" Display="Dynamic"></asp:RegularExpressionValidator>
                                            <%-- <asp:RequiredFieldValidator ID="rfvtxtMobileNumber" runat="server" ForeColor="Red" ControlToValidate="txtMobileNumber" Display="Dynamic"
                                                ValidationGroup="UserProp"></asp:RequiredFieldValidator>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Mobile Carrier </td>
                                        <td>
                                            <asp:DropDownList ID="ddlMobileCarrier" onchange="SelectedMobileCarrier(this);" runat="server" Width="188px" Height="22px">
                                            </asp:DropDownList>
                                          <%--   <asp:RequiredFieldValidator ID="rfvddlMobileCarrier" runat="server" ForeColor="Red"  ControlToValidate="ddlMobileCarrier" Display="Dynamic"
                                                InitialValue="--Select--" ValidationGroup="UserProp"></asp:RequiredFieldValidator>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Login ID:<span class="required">*</span></td>
                                        <td>
                                            <asp:TextBox ID="txtLoginId" MaxLength="50" onkeypress="textFieldValidation(this);" runat="server" autocomplete="off" onkeydown='Logintext(event);'></asp:TextBox>
                                           <%--  <asp:RequiredFieldValidator ID="UserNameRequiredFieldValidator" runat="server" ForeColor="Red" ErrorMessage="Login"  ControlToValidate="txtLoginId"
                                                Display="Dynamic" ValidationGroup="UserProp"></asp:RequiredFieldValidator>--%>
                                            
                                        
                                       </td>


                                    </tr>
                                    <tr>
                                        <td>Password:<span class="required">*</span></td>
                                        <td>
                                            <asp:TextBox ID="txtPassword" MaxLength="50" TextMode="Password" runat="server" onkeypress="textFieldValidation(this);" autocomplete="off" />
                                          <%--  </asp:TextBox><asp:RequiredFieldValidator ID="rfvPassword"
                                                runat="server" ForeColor="Red"  ControlToValidate="txtPassword" Display="Dynamic" ValidationGroup="UserProp"></asp:RequiredFieldValidator> --%>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Confirm Password: </td>
                                        <td>
                                            <asp:TextBox ID="txtPassword2" TextMode="Password" MaxLength="50" runat="server" autocomplete="off"></asp:TextBox>
                                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassword" ControlToValidate="txtPassword2" ErrorMessage="Mismatch!"
                                                Display="Dynamic" ForeColor="#D20B0E" ValidationGroup="UserProp"></asp:CompareValidator>
                                        </td>
                                    </tr>
                                </table>
                                <div id="btnContainer" style="margin-top: 10px; margin-bottom: 15px;">
                                    <asp:HiddenField ID="checkBtnClick" runat="server" />
                                    <asp:Button runat="server" ID="btnSaveUser" Text="Save User" ValidationGroup="UserProp2" OnClientClick="return confirmAlert();" />
                                    <asp:Button runat="server" ID="btnCancel" Text="Cancel" />
                                </div>
                            </td>
                            <td class="v-split" style="width: 12px;"></td>
                            <td style="width: 300px;" class="setRole">
                                <table>
                                    <tr>
                                        <td>
                                            <h3>Select Roles</h3>
                                            <asp:CheckBoxList ID="cblRoles" class="cblRoleCheck" runat="server" Style="margin-left: -3px">
                                            </asp:CheckBoxList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="margin-top: 20px;">
                                            <h3>Other Options</h3>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="cbNotifybyEmail" Text="Send Notification Via Email" Checked="true" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="cbNotifybySMS" Text="Send Notification Via SMS" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="cbAutoQC" Text="Compliance For Auto-QC" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chkLocked" Text="User access is locked" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chkDoNotTrack" Text="Do Not Track (GIS)" runat="server" Visible="False" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class ="tip" abbr = "Check this box is the user is a personal property field agent. Only personal property data will be downloaded."> 
                                            	<asp:CheckBox ID="chkbpp" Text="Personal Property" onclick="chkfunction()" runat="server" Visible="False"/>
                                            </div>
                                        	<div id="chkbppdiv" style="display:none;" runat="server" class="chkdiv">
                                       	  		<div class = "tip" abbr = "This setting can be enabled per user, if there are field agents that work both the real property + personal property." style="width:308px">
                                       				<asp:CheckBox ID="chkbppAction1" Text="Personal Property + Real Property Data Editable" onclick="mutChkbox(this);" runat="server" />
										  		</div>
										  		<div class = "tip" abbr = "This setting will allow for the real property data to be downloaded for the personal property field agent, but will be read-only.">
													<asp:CheckBox ID="chkbppAction2" Text="Real Property Data Read-Only" onclick="mutChkbox(this);" runat="server" />                                            	
                                          		</div>
                                        	</div>
                                        		
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
