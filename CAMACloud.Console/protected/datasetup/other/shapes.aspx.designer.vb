﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class datasetup_other_shapes

    '''<summary>
    '''UpdatePanel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''lbRecover control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbRecover As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''metaXmlPath control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents metaXmlPath As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''ddlImportType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlImportType As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''chkParceldDiv control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkParceldDiv As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''ddlAreaNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlAreaNo As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlBlockNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlBlockNo As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlLotNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlLotNo As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlAssrNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlAssrNo As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''chkStreetdDiv control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkStreetdDiv As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''ddlStreetName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlStreetName As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlStreetType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlStreetType As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''chkNeighborhoodDiv control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkNeighborhoodDiv As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''ddlGroupNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlGroupNo As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''ddlNbhdNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlNbhdNo As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''btnImport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnImport As Global.System.Web.UI.WebControls.LinkButton
End Class
