﻿
Imports System.Runtime.CompilerServices
Imports System.Web.UI
Imports System.Xml
Imports System.IO

Partial Class datasetup_other_shapes
    Inherits System.Web.UI.Page


    Private Sub bindDDL(ByVal ParamArray ddlArray() As DropDownList)
        Dim ddlDataSource As New DataSet
        ddlDataSource.ReadXml(metaXmlPath.Value)
        For Each ddl As DropDownList In ddlArray
            ddl.DataSource = ddlDataSource.Tables(0)
            ddl.DataTextField = ddlDataSource.Tables(0).Columns(0).ColumnName
            ddl.DataValueField = ddlDataSource.Tables(0).Columns(0).ColumnName
            ddl.DataBind()
        Next

    End Sub




    Protected Sub ddlImportType_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlImportType.SelectedIndexChanged, ddlImportType.TextChanged

        Dim selection As String = ddlImportType.SelectedItem.Text

        Select Case selection
            Case "Parcel"
                bindDDL(ddlAreaNo, ddlBlockNo, ddlLotNo, ddlAssrNo)
                showDiv(chkParceldDiv)
            Case "Street"

                bindDDL(ddlStreetName, ddlStreetType)
                showDiv(chkStreetdDiv)

            Case "Neighborhood"

                bindDDL(ddlGroupNo, ddlNbhdNo)
                showDiv(chkNeighborhoodDiv)

            Case Else

        End Select


    End Sub

    Sub hideAll()
        chkParceldDiv.Visible = False
        chkNeighborhoodDiv.Visible = False
        chkStreetdDiv.Visible = False
    End Sub

    Sub showDiv(ByVal id As HtmlGenericControl)
        id.Visible = True
    End Sub


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        hideAll()
    End Sub

    Protected Sub btnImport_Click(sender As Object, e As System.EventArgs) Handles btnImport.Click
        Dim selection As String = ddlImportType.SelectedItem.Text
        Dim attribute As New Hashtable
        Dim xmlAttributes As New XmlDocument
        Dim importType As Integer = 0

        Select Case selection
            Case "Parcel"
                attribute.Add("AreaNo", ddlAreaNo.SelectedItem.Text)
                attribute.Add("BlockNo", ddlBlockNo.SelectedItem.Text)
                attribute.Add("LotNo", ddlLotNo.SelectedItem.Text)
                attribute.Add("AssrNo", ddlAssrNo.SelectedItem.Text)
                xmlAttributes = createXml(attribute)
                importType = 0
            Case "Street"

                attribute.Add("StreetName", ddlStreetName.SelectedItem.Text)
                attribute.Add("StreetType", ddlStreetType.SelectedItem.Text)
                xmlAttributes = createXml(attribute)
                importType = 1
            Case "Neighborhood"

                attribute.Add("GroupNo", ddlGroupNo.SelectedItem.Text)
                attribute.Add("NbhdNo", ddlNbhdNo.SelectedItem.Text)
                xmlAttributes = createXml(attribute)
                importType = 2
            Case Else

        End Select
        Dim sql As String = String.Format("INSERT INTO GISImportJob (OrganizationId,ImportType,ImportConfiguration,JobStatus) values({0},{1},@xml,{2})", ClientSettings.OrganizationId, importType, 0)
		Database.Tenant.Execute(sql, xmlAttributes.ToString())

        File.Delete(metaXmlPath.Value)
    End Sub

#Region " Database.Tenant.Execute(sql, xmlAttributes)"

    'Public Sub Execute(ByVal sql As String, ByVal xml As XmlDocument)
    '    If _conn.State = ConnectionState.Closed Then
    '        _conn.Open()
    '    End If
    '    Dim cmd As New SqlCommand
    '    cmd.CommandText = sql
    '    cmd.Connection = _conn

    '    Dim xmlParam As New SqlParameter
    '    xmlParam.ParameterName = "xml"
    '    xmlParam.SqlDbType = SqlDbType.Xml
    '    xmlParam.Value = xml.DocumentElement.InnerXml
    '    cmd.Parameters.Add(xmlParam)
    '    Try
    '        cmd.ExecuteNonQuery()
    '    Catch ex As Exception
    '        Dim msg As String = "Error on executing command - [" & sql & "]. Error Message is - " & ex.Message
    '        Throw New Exception(msg)
    '    End Try
    'End Sub
#End Region


    Public Function createXml(ByVal attributes As Hashtable) As XmlDocument

        Dim settings As XmlWriterSettings = New XmlWriterSettings()
        settings.Indent = True
        Dim tempPath As String = (ConfigurationManager.AppSettings("UploadTempRoot") + HttpContext.Current.GetCAMASession().TenantKey + "\GIS\")
        If Not Directory.Exists(tempPath) Then
            Directory.CreateDirectory(tempPath)
        End If
        Using writer As XmlWriter = XmlWriter.Create(tempPath + "\\attribute.xml", settings)
            writer.WriteStartDocument()
            writer.WriteStartElement("configuration") '
            writer.WriteStartElement("attributes") '
            For Each attribute As DictionaryEntry In attributes
                writer.WriteStartElement(attribute.Key)
                writer.WriteString(attribute.Value)
                writer.WriteEndElement()
            Next
            writer.WriteEndElement()
            writer.WriteEndElement()
            writer.WriteEndDocument()
        End Using
        Dim xmlDoc As New XmlDocument
        xmlDoc.Load(tempPath + "\\attribute.xml")
        File.Delete(tempPath + "\\attribute.xml")
        Return xmlDoc

    End Function


End Class
