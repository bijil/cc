﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/DataSetup.master"
    AutoEventWireup="false" Inherits="CAMACloud.Console.datasetup_other_shapes" Codebehind="shapes.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .configurationPanel
        {
            font-family: Arial, Helvetica, sans-serif;
            margin-left: 30px;
            margin-top: 30px;
        }
        .slidingDiv
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 10pt;
            color: #777;
            margin-top: 40px;
        }
        .attribute
        {
            display: block;
            font-weight: bold;
            float: left;
            margin-top: 5px;
            width: 150px;
        }
        .ddl
        {
            width: 200px;
            margin-left: 5px;
            color: Red;
        }
        .link-button
        {
            font-weight: bold;
            text-decoration: none;
            padding: 5px 15px;
            margin: 5px;
            margin-left: 0px;
            margin-right: 15px;
            background: #CFCFCF;
            border-radius: 8px;
            display: inline-block;
            cursor: pointer;
        }
        .link-button:hover
        {
            color: White;
            background: #065a9f;
        }
        #setting-panel
        {
            margin-bottom: 10px;
        }
    </style>
    <script type="text/javascript">

        $(document).ready(function () {

            $("#setting-panel").hide();
            $(".link-button").hide();

        });

 
 
    </script>
    <script type="text/javascript">
        function hookup() {

        }

        function newid() {
            var S4 = function () {
                return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
            };
            return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
        }

        var kb = 1024;
        var mb = kb * kb;
        var gb = kb * kb * kb;
        var blobslicer = File.prototype.mozSlice || File.prototype.webkitSlice;
        var blobbuilder = window.MozBlobBuilder || window.WebKitBlobBuilder || window.MSBlobBuilder || window.BlobBuilder;

        function selectFile() {
            document.getElementById('upload').click();
        }

        function fileSize(file) {
            var bytes = file.size;
            if (bytes >= gb) {
                return ((Math.round(bytes / gb * 100) / 100) + ' GB');
            } else if (bytes > 10 * mb) {
                return (Math.round(bytes / mb) + ' MB');
            } else if (bytes >= 1 * mb) {
                return (Math.round(bytes / mb * 10) / 10 + ' MB');
            } else if (bytes > 10 * kb) {
                return (Math.round(bytes / kb) + ' kB');
            } else if (bytes >= kb) {
                return (Math.round(bytes / kb * 10) / 10 + ' kB');
            } else {
                return (bytes + ' bytes');
            }
        }

        var uploadReader;

        function handleUpload(uploader) {
            var files = uploader.files;
            var file = files[0];
            var filename = file.name.toLowerCase();
            var ext = filename.substr(filename.lastIndexOf(".")).substr(1);
            if ((ext == "zip") || (ext == "rar")) {
                // Okay
            } else {
                alert('You have selected an invalid type of file.');
                return;
            }
            var reader = new FileReader();


            var chunkSize = 2 * mb;
            var chunks = Math.ceil(file.size / chunkSize);
            var start = 0;
            var ccount = 0;
            //start = 1535115264 + chunkSize;
            if (localStorage.getItem('data-file-upload-started') == 1) {
                if ((localStorage.getItem('data-file-upload-name') == file.name) && (localStorage.getItem('data-file-upload-size') == file.size)) {
                    start = parseInt(localStorage.getItem('data-file-upload-start'));
                }
            }

            if (start == 0) {
                localStorage.setItem('data-file-upload-uuid', newid());
            }

            function startUpload() {
                console.log('Starting');
                $('.upload-link').hide();
                $('.upload-progress').show();
                $('.upload-title').html('Uploading <b>' + file.name + '</b>, ' + fileSize(file));
                $('.upload-meter div').css({ 'width': '0%' });
                $('.upload-meter meter').mousedown();

                localStorage.setItem('data-file-upload-started', 1);
                localStorage.setItem('data-file-upload-name', file.name);
                localStorage.setItem('data-file-upload-size', file.size);
            }

            function clearFlags() {
                $('.upload-link').show();
                $('.upload-progress').hide();
                $('#upload').val('');
                localStorage.removeItem('data-file-upload-started');
                localStorage.removeItem('data-file-upload-start');
                localStorage.removeItem('data-file-upload-name');
                localStorage.removeItem('data-file-upload-size');
                localStorage.removeItem('data-file-upload-uuid');
            }

            function finishUpload(msg) {
                $("#setting-panel").show();
                  $(".link-button").show();
                clearFlags();
               // window.location.reload();
                $("#metaXmlPath").val(msg)

  

            }

            function abortUpload() {
                alert('Uploaded is aborted due to an error at server.');
                clearFlags();
            }

            function displayError() {

            }

            function readNext() {

                ccount++;
                var stop = Math.min(start + chunkSize - 1, file.size);

                reader.onloadend = function (e) {
                    if (e.target.readyState == FileReader.DONE) {
                        try {
                            var progress = Math.floor(stop / file.size * 100);
                            var form = new FormData();

                            var bb = new blobbuilder();
                            bb.append(e.target.result);

                            form.append(file.name, bb.getBlob("application/octet-stream"), file.name);
                            form.append("name", localStorage.getItem('data-file-upload-name'));
                            form.append("uuid", localStorage.getItem('data-file-upload-uuid'));
                            form.append("start", start);
                            form.append("stop", stop);
                            form.append("size", file.size);
                            form.append("progress", progress)

                            var xhr = new XMLHttpRequest();
                            xhr.open('POST', '/uploads/catch-shape.aspx', true);

                            xhr.onload = function (e) {

                                if (this.status == 200) {
                                    $('.upload-meter div').css({ 'width': progress + '%' });
                                    start += chunkSize;
                                    localStorage.setItem('data-file-upload-start', start);
                                    if (start < file.size) {
                                        uploadReader = readNext;
                                        var wait = 0;
                                        window.setTimeout('uploadReader()', wait);
                                    } else {

                                        finishUpload(this.response);
                                    }
                                } else if (this.status = 500) {
                                    abortUpload();
                                }
                            }
                            xhr.send(form);
                        }
                        catch (ex) {
                            console.log(ex);
                        }
                    }
                };

                try {
                    var blob = blobslicer.call(file, start, stop + 1);
                    reader.readAsArrayBuffer(blob);
                }
                catch (ex2) {
                    alert(ex2);
                }
            }

            startUpload();
            readNext();

        }

        function wl(message) {
            $('.output').append($('<div>' + message + '</div>'));
        }

        $(hookup);
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h1>
       Upload GIS Files</h1>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="configurationPanel">
                <div id="upload-div">
                    <p style="display: none;" class="html-uploader">
                        <input type="file" id="upload" onchange="handleUpload(this);" />
                        <%--<input type="file" id="File1" onchange="handleUpload(this);" accept="application/msaccess" />--%>
                    </p>
                    <div class="link-panel upload-link">
                        <a onclick="selectFile();">Upload File</a>
                        <asp:LinkButton runat="server" ID="lbRecover" Text="Recover last upload" Visible="false" />
                    </div>
                    <div class="upload-progress">
                        <div class="upload-title">
                            Uploading</div>
                        <div class="upload-meter">
                            <div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="setting-panel">
                    <asp:HiddenField ID="metaXmlPath" ClientIDMode="Static" runat="server" />
                    <h3>
                        Import Type</h3>
                    <asp:DropDownList ID="ddlImportType" runat="server" AutoPostBack="true">
                        <asp:ListItem Value="">please select</asp:ListItem>
                        <asp:ListItem Value="chkParceldDiv">Parcel</asp:ListItem>
                        <asp:ListItem Value="chkStreetdDiv">Street</asp:ListItem>
                        <asp:ListItem Value="chkNeighborhoodDiv">Neighborhood</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div id="chkParceldDiv" class="slidingDiv" runat="server">
                    <span class="attribute">AreaNo </span>:
                    <asp:DropDownList CssClass="ddl" ID="ddlAreaNo" runat="server">
                    </asp:DropDownList>
                    <br />
                    <span class="attribute">BlockNo </span>:<span class="ddl"><asp:DropDownList CssClass="ddl"
                        ID="ddlBlockNo" runat="server">
                    </asp:DropDownList>
                    </span>
                    <br />
                    <span class="attribute">LotNo </span>:<span class="ddl"><asp:DropDownList CssClass="ddl"
                        ID="ddlLotNo" runat="server">
                    </asp:DropDownList>
                    </span>
                    <br />
                    <span class="attribute">AssrNo </span>:<span class="ddl"><asp:DropDownList CssClass="ddl"
                        ID="ddlAssrNo" runat="server">
                    </asp:DropDownList>
                    </span>
                    <br />
                </div>
                <div id="chkStreetdDiv" class="slidingDiv" runat="server">
                    <span class="attribute">Street Name </span>:<span class="ddl"><asp:DropDownList CssClass="ddl"
                        ID="ddlStreetName" runat="server">
                    </asp:DropDownList>
                    </span>
                    <br />
                    <span class="attribute">Street Type </span>:<span class="ddl"><asp:DropDownList CssClass="ddl"
                        ID="ddlStreetType" runat="server">
                    </asp:DropDownList>
                    </span>
                    <br />
                </div>
                <div id="chkNeighborhoodDiv" class="slidingDiv" runat="server">
                    <span class="attribute">GroupNo </span>:<span class="ddl"><asp:DropDownList CssClass="ddl"
                        ID="ddlGroupNo" runat="server">
                    </asp:DropDownList>
                    </span>
                    <br />
                    <span class="attribute">Nbhd No </span>:<span class="ddl"><asp:DropDownList CssClass="ddl"
                        ID="ddlNbhdNo" runat="server">
                    </asp:DropDownList>
                    </span>
                    <br />
                </div>
                <asp:LinkButton ID="btnImport" runat="server" class="link-button" Text="Import" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
