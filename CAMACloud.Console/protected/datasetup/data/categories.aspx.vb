﻿Imports CAMACloud.BusinessLogic
Imports CAMACloud.BusinessLogic.Installation
Imports CAMACloud.BusinessLogic.Installation.EnvironmentConfiguration

Partial Class datasetup_data_categories
    Inherits System.Web.UI.Page

    Dim fields As DataTable

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ddlDataSource.FillFromTable(BusinessLogic.FieldCategory.GetTableListForCategories, False)
            LoadFields()
            LoadCategories()
        End If
    End Sub

    Protected Sub ddlDataSource_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlDataSource.SelectedIndexChanged
    	LoadFields()
    	LoadCategories()
    	runscript("hideMask(); bindHighlightEvent();")
    End Sub

    Protected Sub btnCreateCategory_Click(sender As Object, e As System.EventArgs) Handles btnCreateCategory.Click
        Try
        	BusinessLogic.FieldCategory.Create(txtNewCategory.Text)
        	Alert("New category created")
            LoadFields()
            Dim cname As String=txtNewCategory.Text
            txtNewCategory.Text = ""
            LoadCategories()
            Dim Description As String = "New Category "+cname+" added in Field Categories."
            SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(),Description)
            
        Catch ex As Exceptions.DuplicateEntryExpections
            Alert("The category name already exists. Duplicate name is not allowed.")
        End Try
    End Sub

    Sub LoadCategories()
    	
    	Dim tablecount As Integer = Database.Tenant.GetIntegerValue("Select count(*) FROM DataSourceTable where CC_TargetTable = 'ParcelData'")
    	If tablecount > 1 Then
    		fields = Database.Tenant.GetDataTable("SELECT df.*, df.Name As dispName FROM DataSourceField df INNER JOIN DatasourceTable ds ON df.TableId = ds.Id WHERE df.CategoryId IS NOT NULL AND ISNULL(df.ExclusiveForSS,0)<>1  AND ds.CC_TargetTable <> 'ParcelData' ORDER BY df.CategoryOrdinal")
    		Dim ptable As DataTable = Database.Tenant.GetDataTable("SELECT df.*, df.Name+'.'+df.SourceTable As dispName FROM DataSourceField df INNER JOIN DatasourceTable ds ON df.TableId = ds.Id WHERE df.CategoryId IS NOT NULL AND ISNULL(df.ExclusiveForSS,0)<>1  AND ds.CC_TargetTable = 'ParcelData' ORDER BY df.CategoryOrdinal")
    		fields.Merge(ptable)	
    	Else
    		fields = Database.Tenant.GetDataTable("SELECT *, Name As dispName FROM DataSourceField WHERE CategoryId IS NOT NULL AND ISNULL(ExclusiveForSS,0)<>1 ORDER BY CategoryOrdinal")
    	End If
        rpCategories.DataSource = Database.Tenant.GetDataTable("SELECT * FROM FieldCategory ORDER BY Ordinal")
        rpCategories.DataBind()
    End Sub
    Sub UpdateFieldCategory()
    	Database.Tenant.Execute("if exists(SELECT * FROM ClientSettings WHERE Name = 'LastSchemaUpdatedTime') UPDATE ClientSettings SET Value = CONVERT(NVARCHAR(24),GETUTCDATE(),121) WHERE Name = 'LastSchemaUpdatedTime' else INSERT INTO ClientSettings(name,Value) Values('LastSchemaUpdatedTime',CONVERT(NVARCHAR(24),GETUTCDATE(),121))")
    	Database.Tenant.Execute("if exists(SELECT * FROM ClientSettings WHERE Name = 'LastSchemaUpdatedCounter') UPDATE ClientSettings SET Value = Value + 1 WHERE Name = 'LastSchemaUpdatedCounter' else INSERT INTO ClientSettings(name,Value) Values('LastSchemaUpdatedCounter','1')")
    End Sub

    Sub LoadFields()
        If ddlDataSource.SelectedValue = "" Then
        Else
        	Dim dtField As DataTable
            Try
            	dtField = Database.Tenant.GetDataTable("EXEC cc_GetAllDataFieldsFromTableAndAssociated " & ddlDataSource.SelectedValue)
            	
            Catch ex As Exception
                dtField = Database.Tenant.GetDataTable("SELECT *, TableId As FieldTableId FROM DataSourceField WHERE TableId = " & ddlDataSource.SelectedValue & "  AND ISNULL(ExclusiveForSS,0)<>1 ORDER BY Name")
            End Try
            
            rpFields.DataSource = dtField
            rpFields.DataBind()          
        End If
    End Sub

    Public Function MasterFieldClass() As String
        Dim catId = Eval("CategoryId")
        If catId IsNot DBNull.Value AndAlso catId.ToString <> "" Then
            Return "cat-added"
        Else
            Return ""
        End If
    End Function

    Protected Sub rpCategories_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rpCategories.ItemDataBound
        Dim cid As HiddenField = e.Item.FindControl("CatId")
        Dim rpFields As Repeater = e.Item.FindControl("rpFields")

        Dim catFields = fields.AsDataView
        catFields.RowFilter = "CategoryId = " & cid.Value
        catFields.Sort = "CategoryOrdinal"

        rpFields.DataSource = catFields 'Database.Tenant.GetDataTable("SELECT * FROM DataSourceField WHERE CategoryId = " + cid.Value + " ORDER BY CategoryOrdinal")
        rpFields.DataBind()
    End Sub
    Protected Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If IsPostBack Then
            RunScript("finalizePage();")
        End If
    End Sub

    Protected Sub lbExport_Click(sender As Object, e As System.EventArgs) Handles lbExport.Click
        Dim gen = New EnvironmentConfiguration
        gen.ExportSettings(ConfigFileType.FieldCategory, Response, "categories", Database.Tenant)
    End Sub

    Protected Sub lbImport_Click(sender As Object, e As System.EventArgs) Handles btnUploadImportFile.Click
        If fuImportFile.HasFile Then
    	    Try
        		Dim Name As String = fuImportFile.FileName
        	    Name = Name.Replace(".xml", "")
                Dim gen = New EnvironmentConfiguration
                Dim rtnval = gen.ImportSettings(fuImportFile.FileContent, ConfigFileType.FieldCategory, Database.Tenant, Membership.GetUser().ToString, "categories", Name)
                If rtnval = 1 Then
                	Alert("Invalid Field Category, Previous Backup Restored")
                Else
                NotificationMailer.SendNotificationToVendorCDS(Membership.GetUser().ToString, 1, "Setup Field Categories")
                LoadCategories()
                UpdateFieldCategory()
                Alert("Imported successfully.")
                End If
            Catch ex As Exception
                Alert(ex.Message)
                Return
            End Try
        End If
    End Sub
    Public Function EditCatPropertiesScript()
    	
    	Dim fieldName As String = Eval("Name")
    	fieldName = fieldName.Replace("'","\'") 
        Return "editCatProperties(" & Eval("Id") & ", '" + fieldName + "');"
    End Function
    Private Sub rpFieldCategories_Itemcommand(source As Object, e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpCategories.ItemCommand
        Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT * FROM FieldCategory WHERE Id = " & e.CommandArgument)
        hcId.Value = e.CommandArgument.ToString()
        txtHideExp.Text = dr.GetString("HideExpression")
        txtHideRecExp.Text = dr.GetString("HideRecordExpression")
        txtHeaderInfo.Text = dr.GetString("HeaderInfo")
        txtSrtExp.Text = dr.GetString("SortExpression")
        txtUniqueSiblingFields.Text = dr.GetString("UniqueFieldsInSiblings")
        txtFilterField.Text = dr.GetString("FilterFields")
        txtUniqueKeys.Text = dr.GetString("UniqueKeys")
        txtMinRec.Text = dr.GetString("MinRecords")
        txtMaxRec.Text = dr.GetString("MaxRecords")
        txtNote.Text = dr.GetString("CC_Note")
        txtDisablecpyRecrdExp.Text = dr.GetString("DisableCopyRecordExpression")
        chkShwcatgry.Checked = dr.GetBoolean("ShowCategory")
        chkTabdata.Checked = dr.GetBoolean("TabularData")
        chkEditexst.Checked = dr.GetBoolean("DoNotAllowEditExistingRecords")
        chkReadOnly.Checked = dr.GetBoolean("isReadonly")
        chkAllowAddDelete.Checked = dr.GetBoolean("AllowAddDelete")
        chkAllowDeleteOnly.Checked = dr.GetBoolean("AllowDeleteOnly")
        chkMultiGridStub.Checked = dr.GetBoolean("MultiGridStub")
        chkDoNotShowInMultiGrid.Checked = dr.GetBoolean("DoNotShowInMultiGrid")
        Dim chkrdDPlist As String = dr.GetString("DisplayType")
        If chkrdDPlist <> "" Then
        	rdDPlist.SelectedValue = chkrdDPlist
        End If

        Dim enableClassCalculator As String = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name ='EnableClassCalculator'")
        If (enableClassCalculator = "1" OrElse enableClassCalculator = "True" OrElse enableClassCalculator = "true") Then
            trClsCalcParam.Visible = True
            txtClsCalcParam.Text = dr.GetString("ClassCalculatorParameters")
        Else
            trClsCalcParam.Visible = False
        End If
        
        Dim enableDisplayfor As String = Database.Tenant.GetStringValue("SELECT BPPEnabled FROM Application")
        If (enableDisplayfor = "1" OrElse enableDisplayfor = "True" OrElse enableDisplayfor = "true") Then
            rdDP.Visible = True
            
        Else
            rdDP.Visible = False
        End If
        LoadFields()
        LoadCategories()
        
        RunScript("EnableCloseBtn(true);")
    End Sub

Private Sub btnSaveChanges_click(source As Object, e As System.EventArgs) Handles btnSaveChanges.Click	

    	If txtSrtExp.Text IsNot Nothing And txtSrtExp.Text <> "" Then 
    		Dim sTable = Database.Tenant.GetStringValue("SELECT SourceTableName FROM FieldCategory WHERE Id ={0}".SqlFormat(True, hcId.Value))
    		Dim sortExpression = txtSrtExp.Text
        	If Not ( sortExpression.Contains("CASE") And sortExpression.Contains("WHEN") ) Then
        		Dim sortingfields As String() = sortExpression.Split(New Char() {","c})
        		For Each fds In sortingfields
        			Dim sFields As String() = fds.Trim().Split(New Char() {" "c})
        			Dim sExpression As Boolean = True
        			
        			If sFields.length > 2 Then
        				sExpression = False
        			Else If sFields.length > 1 Then
        				If Not( sFields(1) = "" Or sFields(1).ToLower() = "asc" Or sFields(1).ToLower() = "desc" ) Then
        					sExpression = False
        				End If
        			End If
        			
        			If sExpression And ( sFields(0) Is Nothing Or sFields(0) = "" ) Then
        				sExpression = False
        			End If
        			
        			If sExpression And ( Not( sFields(0).ToLower() = "rowuid" Or sFields(0).ToLower() = "clientrowuid" Or sFields(0).ToLower() = "parentrowuid" ) ) Then
        				Dim dataRows As DataTable = Database.Tenant.GetDataTable("SELECT Name FROM DataSourceField WHERE Name = '" + sFields(0) + "' AND SourceTable " + If(sTable = "", "Is Null", "= '"+ sTable + "'") )
                        If dataRows.Rows.Count() < 1 Then
                        	sExpression = False
                        End If
        			End If 
        			
        			If Not sExpression Then
        				Alert("The given sort expression is invalid. Please review the expression.")
				        RunScript("EnableCloseBtn(true);")
        				RunScript("CloseDialog()")
        				Return
        			End If
        		Next
        	End If
    	End If
    	
    	txtHeaderInfo.Text = txtHeaderInfo.Text.Replace("##", "<").Replace("@@", ">")
    	Dim categoryName As String = Database.Tenant.GetStringValue("SELECT Name FROM FieldCategory WHERE Id = {0}".SqlFormat(True, hcId.Value))
        For Each dr As DataRow In Database.Tenant.GetDataTable("SELECT HeaderInfo,HideRecordExpression,HideExpression,SortExpression,UniqueFieldsInSiblings,CC_Note,DisableCopyRecordExpression,FilterFields,UniqueKeys,MinRecords,MaxRecords,ShowCategory,TabularData,DoNotAllowEditExistingRecords,IsReadOnly,AllowAddDelete,MultiGridStub,DoNotShowInMultiGrid,AllowDeleteOnly FROM FieldCategory where Id = {0}".SqlFormat(True, hcId.Value)).Rows
            If (txtHeaderInfo.Text <> dr.Get("HeaderInfo")) Then
                Dim Description As String = "Header Info has been changed to " + txtHeaderInfo.Text + " for " + categoryName + " Category."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If
            If (txtHideExp.Text <> dr.Get("HideExpression")) Then
                Dim Description As String = "Hide Expression has been changed to " + txtHideExp.Text + " for " + categoryName + " Category."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If
            If (txtHideRecExp.Text <> dr.Get("HideRecordExpression")) Then
                Dim Description As String = "Hide Record Expression has been changed to " + txtHideRecExp.Text + " for " + categoryName + " Category."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If
            If (txtSrtExp.Text <> dr.Get("SortExpression")) Then
                Dim Description As String = "Sort Expression Info has been changed to " + txtHeaderInfo.Text + " for " + categoryName + " Category."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If
            If (txtUniqueSiblingFields.Text <> dr.Get("UniqueFieldsInSiblings")) Then
                Dim Description As String = "Unique Fields In Siblings has been changed to " + txtUniqueSiblingFields.Text + " for " + categoryName + " Category."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If
            If (txtNote.Text <> dr.Get("CC_Note")) Then
                Dim Description As String = "Note has been changed to " + txtNote.Text + " for " + categoryName + " Category."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If
            If (txtDisablecpyRecrdExp.Text <> dr.Get("DisableCopyRecordExpression")) Then
                Dim Description As String = "Disable Copy Record Expression has been changed to " + txtDisablecpyRecrdExp.Text + " for " + categoryName + " Category."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If
            If (txtFilterField.Text <> dr.Get("FilterFields")) Then
                Dim Description As String = "Filter Fields has been changed to " + txtFilterField.Text + " for " + categoryName + " Category."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If
            If (txtUniqueKeys.Text <> dr.Get("UniqueKeys")) Then
                Dim Description As String = "Unique keys has been changed to " + txtUniqueKeys.Text + " for " + categoryName + " Category."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If
            If (txtMinRec.Text <> dr.Item("MinRecords").ToString()) Then
                Dim MinRecordsBackup = dr.Item("MinRecords").ToString()
                Dim Description As String = "Minimum Records has been changed from " + MinRecordsBackup + " to " + txtMinRec.Text.ToString() + " for " + categoryName + " Category."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If
            If (txtMaxRec.Text <> dr.Item("MaxRecords").ToString()) Then
                Dim MaxRecordsBackup = dr.Item("MaxRecords").ToString()
                Dim Description As String = "Maximum Records has been changed from " + MaxRecordsBackup + " to " + txtMaxRec.Text + " for " + categoryName + " Category."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If
            If (chkShwcatgry.Checked <> dr.GetBoolean("ShowCategory")) Then
                Dim Description As String = "Show Category has been set to " + chkShwcatgry.Checked.ToString() + " for " + categoryName + " Category."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If
            If (chkTabdata.Checked <> dr.Get("TabularData")) Then
                Dim Description As String = "Tabular Data has been set to " + chkTabdata.Checked.ToString() + " for " + categoryName + " Category."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If
            If (chkEditexst.Checked <> dr.Get("DoNotAllowEditExistingRecords")) Then
                Dim Description As String = "Do Not Allow Edit Exist has been set to " + chkEditexst.Checked.ToString() + " for " + categoryName + " Category."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If
            If (chkReadOnly.Checked <> dr.Get("IsReadOnly")) Then
                Dim Description As String = "Read Only has been set to " + chkReadOnly.Checked.ToString() + " for " + categoryName + " Category."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If
            If (chkAllowAddDelete.Checked <> dr.Get("AllowAddDelete")) Then
                Dim Description As String = "Allow Add Delete has been set to " + chkAllowAddDelete.Checked.ToString() + " for " + categoryName + " Category."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If
            If (chkMultiGridStub.Checked <> dr.Get("MultiGridStub")) Then
                Dim Description As String = "Multi Grid Sub has been set to " + chkMultiGridStub.Checked.ToString() + " for " + categoryName + " Category."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If
            If (chkDoNotShowInMultiGrid.Checked <> dr.Get("DoNotShowInMultiGrid")) Then
                Dim Description As String = "Do Not Show In Multigrid has been set to " + chkDoNotShowInMultiGrid.Checked.ToString() + " for " + categoryName + " Category."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If
            If (chkAllowDeleteOnly.Checked <> dr.Get("AllowDeleteOnly")) Then
                Dim Description As String = "Allow Delete Only has been set to " + chkAllowDeleteOnly.Checked.ToString() + " for " + categoryName + " Category."
                SettingsAuditTrail.insertSettingsAuditTrail(Membership.GetUser().ToString(), Description)
            End If

        Next
        If (Database.Tenant.Execute("UPDATE FieldCategory SET HideExpression= {1},HeaderInfo= {2}, SortExpression = {3},FilterFields = {4}, UniqueKeys ={5}, MinRecords = {6}, MaxRecords = {7}, ShowCategory = {8}, TabularData ={9}, DoNotAllowEditExistingRecords ={10},ClassCalculatorParameters ={11} ,UniqueFieldsInSiblings = {12},isReadonly = {13},AllowAddDelete = {14},MultiGridStub = {15},DoNotShowInMultiGrid = {16},DisplayType = {17},AllowDeleteOnly = {18},CC_Note={19},DisableCopyRecordExpression={20},HideRecordExpression = {21} where Id ={0}".SqlFormat(True, hcId.Value, txtHideExp.Text, txtHeaderInfo.Text, txtSrtExp.Text, txtFilterField.Text, txtUniqueKeys.Text, txtMinRec.Text, txtMaxRec.Text, If(chkShwcatgry.Checked, 1, 0), If(chkTabdata.Checked, 1, 0), If(chkEditexst.Checked, 1, 0) > 0, txtClsCalcParam.Text, txtUniqueSiblingFields.Text, If(chkReadOnly.Checked, 1, 0), If(chkAllowAddDelete.Checked, 1, 0), If(chkMultiGridStub.Checked, 1, 0), If(chkDoNotShowInMultiGrid.Checked, 1, 0), rdDPlist.SelectedValue, If(chkAllowDeleteOnly.Checked, 1, 0), txtNote.Text, txtDisablecpyRecrdExp.Text, txtHideRecExp.Text))) Then
            RunScript("hideCatPopup();minMaxChange();alert('Field Settings Modified!');")
            NotificationMailer.SendNotificationToVendorCDS(Membership.GetUser.UserName, 2, "Setup Field Categories")
        End If


        RunScript("EnableCloseBtn(true);")
        RunScript("CloseDialog()")
    End Sub

End Class
