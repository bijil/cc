﻿
Partial Class datasetup_data_lookup
    Inherits System.Web.UI.Page

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
			Dim data As DataTable = Database.Tenant.GetDataTable("SELECT * FROM DataSourceTable WHERE ImportType = 2 AND DoNotImport = 0 ORDER BY Name")
			gvAux.DataSource = data
			gvAux.DataBind()

			For Each gr As GridViewRow In gvAux.Rows
				Dim tid As Integer = gr.GetHiddenValue("TID")
				Dim row As DataRow = data.Rows(gr.RowIndex)
				Dim dt As DataTable = Database.Tenant.GetDataTable("SELECT Name FROM DataSourceField WHERE TableId = {0} ORDER BY Name".FormatString(tid))

				gr.GetControl(Of DropDownList)("ddlCF1").FillFromTable(dt, True, "None", row.GetString("LookupIDField"))
				gr.GetControl(Of DropDownList)("ddlCF2").FillFromTable(dt, True, "None", row.GetString("LookupNameField"))
				gr.GetControl(Of DropDownList)("ddlCF3").FillFromTable(dt, True, "None", row.GetString("LookupDescField"))
				gr.GetControl(Of DropDownList)("ddlLV").FillFromTable(dt, True, "None", row.GetString("LookupValueField"))
				gr.GetControl(Of DropDownList)("ddlRef").FillFromTable(dt, True, "None", row.GetString("LookupReferredTableField"))
				gr.GetControl(Of DropDownList)("ddlGroup").FillFromTable(dt, True, "None", row.GetString("LookupGroupField"))
				gr.GetControl(Of DropDownList)("ddlSort").FillFromTable(dt, True, "None", row.GetString("LookupOrdinalField"))
			Next
		End If
	End Sub

End Class
