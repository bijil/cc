﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class datastatus
    
    '''<summary>
    '''txtKeyValue1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtKeyValue1 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''rfv control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rfv As Global.System.Web.UI.WebControls.RequiredFieldValidator
    
    '''<summary>
    '''btnSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSearch As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''btnClear control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnClear As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''btnParcelList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnParcelList As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''pnlParcel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlParcel As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblParcel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblParcel As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''tvTables control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tvTables As Global.System.Web.UI.WebControls.TreeView
    
    '''<summary>
    '''tdOtherTables control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdOtherTables As Global.System.Web.UI.HtmlControls.HtmlTableCell
    
    '''<summary>
    '''tvOtherTables control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tvOtherTables As Global.System.Web.UI.WebControls.TreeView
    
    '''<summary>
    '''ibtnExportToCSV control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ibtnExportToCSV As Global.System.Web.UI.WebControls.ImageButton
   '''<summary>
    '''hfKeyValue1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfKeyValue1 As Global.System.Web.UI.WebControls.HiddenField

End Class
