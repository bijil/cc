﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/DataSetup.master"
	AutoEventWireup="false" Inherits="CAMACloud.Console.datasetup_data_importsettings" Codebehind="importsettings.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<h1>
		CAMA Import Settings</h1>
	<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
		<ContentTemplate>
			<div style="padding-top: 10px;" class="asp-form f200 l150">
				<label>
					Select Primary Table:</label>
				<asp:DropDownList runat="server" ID="ddlPrimaryTable" ClientIDMode="Static" CssClass="input-element"
					AutoPostBack="true" />
				<label>
					Common Key Field #1:</label>
				<asp:DropDownList runat="server" ID="ddlCommonKeyField1" ClientIDMode="Static" CssClass="input-element" />
				<label>
					Common Key Field #2:</label>
				<asp:DropDownList runat="server" ID="ddlCommonKeyField2" ClientIDMode="Static" CssClass="input-element" />
				<label>
					Common Key Field #3:</label>
				<asp:DropDownList runat="server" ID="ddlCommonKeyField3" ClientIDMode="Static" CssClass="input-element" />
			</div>
			<div class="clear">
			</div>
			<h2>
				Special Fields Mapping</h2>
			<table class="special-fields-table">
				<tr>
					<td>
						<label>
							Neighborhood Source Table:
						</label>
					</td>
					<td>
						<asp:DropDownList runat="server" ID="ddlNbhdTable" ClientIDMode="Static" CssClass="input-element"
							AutoPostBack="true" />
					</td>
					<td>
						<label>
							Nbhd Field:
						</label>
					</td>
					<td>
						<asp:DropDownList runat="server" ID="ddlNbhdField" ClientIDMode="Static" CssClass="input-element" />
					</td>
				</tr>
				<tr>
					<td>
						&nbsp;
					</td>
					<td>
						&nbsp;
					</td>
					<td>
						<label>
							Group Field:
						</label>
					</td>
					<td>
						<asp:DropDownList runat="server" ID="ddlNbhdGroup" ClientIDMode="Static" CssClass="input-element" />
					</td>
				</tr>
				<tr>
					<td>
						<label>
							Street Address Source:
						</label>
					</td>
					<td>
						<asp:DropDownList runat="server" ID="ddlStreetSource" ClientIDMode="Static" CssClass="input-element"
							AutoPostBack="true" />
					</td>
					<td>
						<label>
							Fields:</label></td>
					<td>
						<asp:DropDownList runat="server" ID="ddlStreetFields" ClientIDMode="Static" CssClass="input-element"
							AutoPostBack="true" />
					</td>
				</tr>
				<tr>
					<td>
						<label>
							Address Formula:
						</label>
					</td>
					<td colspan="3">
						<asp:TextBox runat="server" ID="txtAddressFormula" ClientIDMode="Static" CssClass="input-element"
							TextMode="MultiLine" Rows="3" Width="420px" />
					</td>
				</tr>
				<tr>
					<td>
						<label>
							Comparables Source:
						</label>
					</td>
					<td>
						<asp:DropDownList runat="server" ID="ddlComparable" ClientIDMode="Static" CssClass="input-element"
							AutoPostBack="true" />
					</td>
					<td>
						<label>
							Subject Field:</label></td>
					<td>
						<asp:DropDownList runat="server" ID="ddlSubjectField" ClientIDMode="Static" CssClass="input-element"
							AutoPostBack="true" />
					</td>
				</tr>
				<tr>
					<td></td>
					<td colspan="2">
						<label>
							Select field to add as comparable:</label></td>
					<td>
						<asp:DropDownList runat="server" ID="ddlAddComparable" ClientIDMode="Static" CssClass="input-element"
							AutoPostBack="true" />
					</td>
				</tr>
				<tr>
					<td>
						<label>
							Comparable Key Fields:
						</label>
					</td>
					<td colspan="3">
						<asp:TextBox runat="server" ID="txtComparableKeyFields" ClientIDMode="Static" CssClass="input-element"
							TextMode="MultiLine" Rows="3" Width="420px" />
					</td>
				</tr>
			</table>
			<div class="link-panel clear" style="margin-top: 20px;">
				<asp:LinkButton runat="server" ID="lbSaveSettings" Text="Update Settings" />
			</div>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>
