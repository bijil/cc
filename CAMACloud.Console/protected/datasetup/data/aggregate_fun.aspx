﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master"
    CodeBehind="aggregate_fun.aspx.vb" Inherits="CAMACloud.Console.aggregate_fun" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    .categeroy
    {
        border:1px; solid #bfbdad;
    }
</style>
    <script type="text/javascript">
        function setPageDimensions(cheight) {
            var dtl = cheight - 208 - 40;
            $('.fields-list').height(dtl + 60);
            $('.cat-fields').height(dtl + 75);
        }
    </script>
    <script type="text/javascript">
        var sortOldIndex;

        $(finalizePage);
        function disableDrag() {
            //        $('#FieldLis').draggable( "disable" );
            ////        ('.mfield').draggable( "disable" );
            console.log("Maximum NPF is  Six");
        }

        function makeFieldsDraggable() {
            $('.master-field').sortable().disableSelection();
            $('.cat-fields').droppable({
                drop: function (event, ui) {
                    var src = event.srcElement;
                    if ($(src).hasClass('mfield') && (!$(src).hasClass('cat-added'))) {
                        var fieldId = $(src).attr('fieldid');
                        var tableId = $(src).attr('tableid');
                        var target = event.target;
                        var postdata = {
                            FieldId: fieldId
                        };
                        console.log(postdata);
                        if ($(".cat-field:visible").length < 6) {
                            $ds('addtonpf', postdata, function (resp) {
                                if (resp.status == "OK") {
                                    var fName = $(src).html();
                                    var newField = '<div class="cat-field" fieldid="' + fieldId + '"><a class="a16 del16" onclick="removeField(' + fieldId + ', $(this).parent())"></a><span>' + fName + '</span></div>';
                                    $(target).html($(target).html() + newField);
                                    $(src).addClass('cat-added');
                                } else {
                                    alert(resp.message);
                                }

                            });
                        }
                        else {
                            alert('Maximum NPF is  Six');
                            return false;
                        }

                    }

                }
            });

            $('.cat-fields').sortable({
                start: function (event, ui) {
                    var o = ui.item[0];
                    sortOldIndex = $(o).index();
                },
                stop: function (event, ui) {
                    var o = ui.item[0];
                    var fieldId = $(o).attr('fieldid')
                    var index = $(o).index();
                    var newIndex = (index + 1) * 10;
                    if (sortOldIndex > index) {
                        newIndex -= 5;
                    } else {
                        newIndex += 5;
                    }

                    $ds('moveqrfield', {
                        NewIndex: newIndex,
                        FieldId: fieldId
                    }, function (data) {
                        if (data.status == "OK") {

                        }
                    });

                }
            });
        }

        function removeField(fieldId, cf) {
            $ds('deletefromnpf', {
                FieldId: fieldId
            }, function (data) {
                if (data.status == "OK") {
                    $(cf).fadeOut();
                    $('span[fieldid=' + fieldId + ']').removeClass('cat-added');
                }
            });

        }

        function getAfnOnLoad() {
            $ds('fillafn', {}, function (data) {
                if (data.length) {
                    data.forEach(function (d) {
                        $('input[afn="' + d.Value + '"]')[0].checked = true;
                    });
                }
            });

        }

        function calafn(source) {
            console.log($(source).attr('afn'));
            console.log('calafn function called');
            if ($(source)[0].checked) {
                $ds('addafn', {
                    afn: $(source).attr('afn')
                }, function (data) {
                    console.log('inserted');
                });
            }
            else {
                $ds('deleteafn', {
                    afn: $(source).attr('afn')
                }, function (data) {
                    console.log('deleted');
                });
            }
        }

        function finalizePage() {
            makeFieldsDraggable();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
        Group Profile Settings</h1>
    <table class="app-page-table">
        <tr>
            <td class="td-field-master">
                <div class="field-master">
                    <h3>
                        Parcel Data Fields</h3>
                    <p>
                        Drag and drop fields from the list below to the space on the right side.</p>
                    <asp:UpdatePanel runat="server" class="fields-list drop-box flist" ID="FieldLis"
                        ClientIDMode="Static">
                        <ContentTemplate>
                            <asp:Repeater runat="server" ID="rpFields">
                                <ItemTemplate>
                                    <div class="master-field">
                                        <span fieldid='<%# Eval("Name") %>' class='mfield <%# checkForExisting(Eval("Name")) %>'>
                                            <%# Eval("Name")%></span></div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </td>
            <td>
                <div class="cat-editor">
                    <h2>
                        Profile Fields</h2>
                    <div class="category" catid='0'>
                        <div class="props ca">
                            <div class="fr view">
                                <div class="controls">
                                    <a class="a16 ed16" onclick="return editCategory(this);"></a><a class="a16 ed16"
                                        onclick="deleteCategory(this);return false;"></a>
                                </div>
                            </div>
                            <div style="float: left;">
                                <asp:Label ID="Label1" runat="server" Text="Select Aggregate "></asp:Label></div>
                            <div style="float: center;" >
                               
                                <input type="checkbox" afn="mean" onchange="return calafn(this);"/>
                                <label>Mean</label>
                                 <input type="checkbox" afn="median"  onchange="return calafn(this);"/>
                                <label>Median</label>
                                 <input type="checkbox" afn="mode"  onchange="return calafn(this);"/>
                                <label>Mode</label>
                                 <input type="checkbox" afn="range" onchange="return calafn(this);"/>
                                <label>Range</label>
                                 <input type="checkbox" afn="iqrmin" onchange="return calafn(this);"/>
                                <label>IQRMin</label>
                                <input type="checkbox" afn="iqrmax" onchange="return calafn(this);"/>
                                <label>IQRMax</label>
                            </div>
                        </div>
                        <div class="cat-fields drop-box ca" catid='0'>
                            <asp:Repeater runat="server" ID="rpnpfFields">
                                <ItemTemplate>
                                    <div class="cat-field" fieldid='<%# Eval("Name") %>'>
                                        <a class="a16 del16" onclick="removeField('<%# Eval("Name") %>', $(this).parent())">
                                        </a><span>
                                            <%# Eval("Name")%></span></div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="td-field-master">
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </td>
        </tr>
    </table>
</asp:Content>
