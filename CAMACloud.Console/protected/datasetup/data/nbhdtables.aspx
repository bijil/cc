﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/DataSetup.master"
	AutoEventWireup="false" Inherits="CAMACloud.Console.datasetup_data_nbhdtables" Codebehind="nbhdtables.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
	<script type="text/javascript">
		$(setSelectColors);
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<h1>
		Neighborhood Data Tables</h1>
	<p class="info">
		The following tables contain neighborhood related data. Please select the field for each which correspond to the neighborhood number.</p>
	<asp:GridView runat="server" ID="gvAux">
		<RowStyle CssClass="dst-row" />
		<Columns>
			<asp:BoundField HeaderText="Table Name" DataField="Name" ItemStyle-Width="200px" />
			<asp:TemplateField HeaderText="Nbhd No. Field">
				<ItemStyle Width="180px" />
				<ItemTemplate>
					<asp:HiddenField runat="server" ID="TID" Value='<%# Eval("Id") %>' ClientIDMode="Static" />
					<asp:DropDownList runat="server" ID="ddlCF1" onchange="setTableField(this, 'KeyField1')" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
		<EmptyDataTemplate>
			<div>
				No tables available</div>
		</EmptyDataTemplate>
	</asp:GridView>
</asp:Content>
