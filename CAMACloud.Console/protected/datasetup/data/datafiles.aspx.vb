﻿Imports CAMACloud.BusinessLogic
Imports System.Net.Sockets
Imports System.Net

Partial Class datasetup_data_datafiles
	Inherits System.Web.UI.Page

	Sub LoadFilesInGrid()
		gvFiles.DataSource = DataSource.GetDataSourceFiles
		gvFiles.DataBind()
	End Sub

	Protected Sub gvFiles_Load(sender As Object, e As System.EventArgs) Handles gvFiles.Load
		If Not IsPostBack Then
			LoadFilesInGrid()
		End If
	End Sub

	Protected Sub lbRecover_Click(sender As Object, e As System.EventArgs) Handles lbRecover.Click
		'Dim s3 As New S3FileManager()
		's3.TransferFile("D:\CAMACloud Temp\data\org001\f534596b-bcc9-f81d-f3e0-133d6d724cce\MydBase_CAMA_5-30-2012.mdb", "org001/data/MydBase_CAMA_5-30-2012.mdb")
		Dim filePath As String = "D:\CAMACloud Temp\data\org001\f534596b-bcc9-f81d-f3e0-133d6d724cce\MydBase_CAMA_5-30-2012.mdb"
		Dim filename As String = IO.Path.GetFileName(filePath)
		Dim ext As String = IO.Path.GetExtension(filename).TrimStart(".").ToLower
        Dim s3Path As String = HttpContext.Current.GetCAMASession().TenantKey + "/datasource/" + filename

        Dim dsfid As Integer = DataSource.RegisterDataFile(filePath, s3Path, Membership.GetUser.UserName, Request.ClientIPAddress)
		If ext = "mdb" Or ext = "accdb" Then
			DataSource.BuildDataStructureFromAccessDatabase(dsfid, filePath)
		Else

		End If

		'Dim netCommand As String = "UPLOADTOS3" +
		'  vbCr + "ORGANIZATIONID=" & HttpContext.Current.GetCAMASession().OrganizationId &
		'  vbCr + "LOCALPATH=" + filePath +
		'  vbCr + "S3PATH=" + s3Path +
		'  vbCr + "DELETELOCAL=false" +
		'  vbCr

		'Dim buffer = System.Text.Encoding.UTF8.GetBytes(netCommand)
		'Dim client As New TcpClient
		'client.Connect(IPAddress.Loopback, 9000)
		'client.GetStream().Write(buffer, 0, buffer.Length)

		'If IO.File.Exists(Server.MapPath("~/$$server$$.info")) Then


		'End If
	End Sub

	Protected Sub gvFiles_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvFiles.RowCommand
		If e.CommandName = "DeleteSourceFile" Then
			If DataSource.DeleteSourceFileAndDependencies(e.CommandArgument) Then
				Alert("Data source file and dependancies deleted successfully.")
				LoadFilesInGrid()
			Else
				Alert("Delete failed.")
			End If
		End If
	End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
End Class
