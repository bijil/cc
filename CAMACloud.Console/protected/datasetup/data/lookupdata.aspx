﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="lookupdata.aspx.vb" Inherits="CAMACloud.Console.lookupdata" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<script type="text/javascript">
 		var CAMACloud = {};
 		CAMACloud.Sketching = {};
 		CAMACloud.Sketching.Formatters = {};
 		CAMACloud.Sketching.Encoders ={};
 		
   		function UploadFile(fileUpload) {
	        if (fileUpload.value != '') { 
	              var fileName= fileUpload.files[0].name;
	              var index = fileName.indexOf('.');
	              fileName = fileName.substring(0, index != -1 ? index : fileName.length);
	              $("#" + '<%= txtLookupName.ClientID%>').val(fileName);
				  $('.validation-error').css("visibility","hidden");              
           }
        }
        
        function updateApexLookupName() { 
        	var _skConfig = '<%=  _sketchConfigName %>'
        	var _sktConfig = CAMACloud.Sketching.Configs[_skConfig];
            var _lkName = "Apex_Area_Codes";
        	
        	if(_sktConfig) {
        		if( _sktConfig.sources && _sktConfig.sources[0] && _sktConfig.sources[0].VectorSource && _sktConfig.sources[0].VectorSource[0] && _sktConfig.sources[0].VectorSource[0].LabelLookup && _sktConfig.sources[0].VectorSource[0].LabelLookup != "" )
        			_lkName = _sktConfig.sources[0].VectorSource[0].LabelLookup;
        	}
          
        	$('.apexAreaOptionsClass [value=noAddVal]').html(_lkName + ' - No Additional Value');
        	$('.apexAreaOptionsClass [value=addVal]').html(_lkName + ' - Additional Value included');
        	$('.txtLookupsktXMLClass')[0].readOnly = false;
        	document.getElementById('<%=txtLookupsktXML.ClientID %>').value = _lkName;
        	//$('.txtLookupsktXMLClass')[0].readOnly = true;
        }

        
    </script>
 	<cc:JavaScript ID="JavaScript1" runat="server" IncludeFolder="/App_Static/js/sketch/sketchlib/" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<style type="text/css">
		#outer
			{
   			 width:100%;
    		text-align: left;
			}
		.inner
			{
    		display: inline-block;
			}
		.inner2
			{
    		display: inline-block;
    		padding-left: 15px;
			}
	</style>
	
    <h1>
        Manage Lookup Table Data</h1>
    <asp:Panel runat="server" ID="pnlListView" Visible="false">
        <p class="info">
            The lookup tables imported to CAMA Cloud are listed below:
        </p>
        <p>
            <asp:LinkButton runat="server" ID="lbAddLookup" Text="Click to add new Lookup or import Lookup Data" Font-Bold="true" />
        </p>
        <asp:GridView runat="server" ID="gvLookups" AllowPaging="true" pageSize="20">
            <Columns>
                <asp:BoundField DataField="LookupName" HeaderText="Lookup Table" ItemStyle-Width="300px" />
                <asp:BoundField DataField="ValueCount" HeaderText="Records" ItemStyle-Width="60px" />
                <asp:TemplateField>
                    <ItemStyle Width="90px" />
                    <ItemTemplate>
                        <asp:LinkButton runat="server" ID="lbDownload" Text="Export as XML" CommandName="Exportxml" CommandArgument='<%# Eval("LookupName") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemStyle Width="90px" />
                    <ItemTemplate>
                        <asp:LinkButton runat="server" ID="lbDownloadcsv" Text="Export as CSV" CommandName="Exportcsv" CommandArgument='<%# Eval("LookupName") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemStyle Width="60px" />
                    <ItemTemplate>
                        <asp:LinkButton runat="server" ID="lbDelete" Text="Delete" CommandName="DeleteItem" CommandArgument='<%# Eval("LookupName") %>' OnClientClick="return confirm('Are you sure you want to delete this lookup table from the cloud?')" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <h3>Other tasks</h3>
		<div id ="outer"> 
        	<p class ="inner">
       	     <asp:LinkButton runat="server" ID="lbExportAll" CommandName="ExportallXML" Text="Export all tables as XML" />
       		 </p>
       	    <p class ="inner2">
       	     <asp:LinkButton runat="server" style = "display: none" ID="lbExportAllcsv" CommandName="ExportallCSV" Text="Export all tables as CSV" />
       		 </p>
      	</div>  
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlFormView">
        <p class="info">
            To create a new lookup table, upload the data in the illustrated formats. To make changes to an existing lookup table, export the data, make
            changes and upload here to replace existing Lookups.
        </p>
        <p>
            <asp:LinkButton runat="server" ID="lbReturnToList" Text="Return to list" Font-Bold="true" />
        </p>
        <p>
            <asp:RadioButtonList runat="server" ID="rblImportType" RepeatDirection="Horizontal" Width="300px" AutoPostBack="true">
                <asp:ListItem Text="XML" Value="xml" Selected="True" />
                <asp:ListItem Text="CSV" Value="csv" />
                <asp:ListItem Text="Sketch Codes XML" Value="skt_xml" />
            </asp:RadioButtonList>
        </p>
        <p>
        	<asp:DropDownList ID="apexAreaOptions" class="apexAreaOptionsClass" runat="server" >
                <asp:ListItem Value="noAddVal">Apex Area Codes - No Additional Value</asp:ListItem>
                <asp:ListItem Value="addVal">Apex Area Codes - Additional Value included</asp:ListItem>                               
            </asp:DropDownList>       	
        </p>
        <p>
            <asp:FileUpload runat="server" onchange="UploadFile(this)" ID="fuImportFile" />
         </p>
        <asp:Panel runat="server" ID="pnlImportXML">
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlImportCSV">
              <table>
                <tr>
                    <td>Lookup Name: </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtLookupName" MaxLength="50" />
                        <asp:RequiredFieldValidator runat="server" ID="rfv1" ControlToValidate="txtLookupName" ValidationGroup="Import" />
                    </td>
                </tr>
              <tr>
                    <td colspan="2">
                        <asp:CheckBox runat="server" ID="chkFirstLineHeaders" Text="The first line contains headers." />
                    </td>
                </tr>
              </table>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlImportSktXML">
              <table>
                <tr>
                    <td>Lookup Name: </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtLookupsktXML" class="txtLookupsktXMLClass" MaxLength="50"  />                      
                    </td>
                </tr>
              </table>
        </asp:Panel>
        <p>
            <asp:CheckBox runat="server" ID="chkTrim" Text="Trim trailing spaces from Id, Name, Description fields." />
        </p>
        <p>
            <asp:Button runat="server" ID="btnUpload" Text="Upload Lookup File"  ValidationGroup="Import" />
        </p>
    </asp:Panel>
</asp:Content>