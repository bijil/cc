﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master" CodeBehind="quickreview.aspx.vb"
    Inherits="CAMACloud.Console.quickreview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .category
        {
            border: 1px solid #bfbdad;
        }
    </style>
    <script type="text/javascript">
        function setPageDimensions(cheight) {
            var dtl = cheight - 208 - 40;
            $('.fields-list').height(dtl + 60);
            $('.cat-fields').height(dtl + 75);
        }
    </script>
    <script type="text/javascript">
        // CC_3944
       /* window.onload = function () {            
         {
            $('.mfield.cat-added').mouseover(function () {
                var fieldids = $(this).attr('fieldid');
                $('.cat-field[fieldid="' + fieldids + '"]').css("background-color", "#56b0e3");
            });

            $('.mfield.cat-added').mouseout(function () {
                var fieldids = $(this).attr('fieldid');
                $('.cat-field[fieldid="' + fieldids + '"]').css("background-color", "#F0F0F0");
            });
        }
        };*/
        var sortOldIndex;

        $(finalizePage);

        function makeFieldsDraggable() {
            $('.master-field').sortable().disableSelection();
            $('.cat-fields').droppable({
                drop: function (event, ui) {
                    var src = event.srcElement;
                    if ($(src).hasClass('mfield') && (!$(src).hasClass('cat-added'))) {
                        var fieldId = $(src).attr('fieldid');
                        var tableId = $(src).attr('tableid');
                        var target = event.target;

                        var postdata = {
                            FieldId: fieldId
                        };
                        console.log(postdata);
                        $ds('addfieldtoqrtab', postdata, function (resp) {
                            if (resp.status == "OK") {
                                var fName = $(src).html();
                                var newField = '<div class="cat-field" fieldid="' + fieldId + '"><a class="a16 del16" onclick="removeField(' + fieldId + ', $(this).parent())"></a><span>' + fName + '</span></div>';
                                $(target).html($(target).html() + newField);
                                $(src).addClass('cat-added');
                            } else {
                                alert(resp.message);
                            }

                        });
                    }

                }
            });

            $('.cat-fields').sortable({
                containment: ".app-page-table",
                start: function (event, ui) {
                    var o = ui.item[0];
                    sortOldIndex = $(o).index();
                },
                stop: function (event, ui) {
                    var o = ui.item[0];
                    var fieldId = $(o).attr('fieldid')
                    var index = $(o).index();
                    var newIndex = (index + 1) * 10;
                    if (sortOldIndex > index) {
                        newIndex -= 5;
                    } else {
                        newIndex += 5;
                    }

                    $ds('moveqrfield', {
                        NewIndex: newIndex,
                        FieldId: fieldId
                    }, function (data) {
                        if (data.status == "OK") {

                        }
                    });

                }
            });
        }

        function removeField(fieldId, cf) {
            $ds('removefieldfromqrtab', {
                FieldId: fieldId,
            }, function (data) {
                if (data.status == "OK") {
                    $(cf).fadeOut();
                    $('span[fieldid=' + fieldId + ']').removeClass('cat-added');
                }
            });

        }

        function finalizePage() {
            makeFieldsDraggable();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h1>
        MobileAssessor Quick Review Tab</h1>
    <table class="app-page-table">
        <tr>
            <td class="td-field-master">
                <div class="field-master">
                    <h3>
                        Parcel Data Fields</h3>
                    <p>
                        Drag and drop fields from the list below to the space on the right side.</p>
                    <asp:UpdatePanel runat="server" class="fields-list drop-box flist" ID="FieldList" ClientIDMode="Static">
                        <ContentTemplate>
                            <asp:Repeater runat="server" ID="rpFields">
                                <ItemTemplate>
                                    <div class="master-field">
                                        <span fieldid='<%# Eval("Id") %>' class='mfield <%# MasterFieldClass() %>' catid='<%# Eval("CategoryId") %>' tableid='<%# Eval("FieldTableId") %>'>
                                            <%# Eval("Name")%></span></div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </td>
            <td>
                <div class="cat-editor">
                    <h2>
                        Quick Review Fields</h2>
                    <div class="category" catid='0'>
                        <div class="props ca">
                            <div class="fr view ">
                                <div class="controls">
                                    <a class="a16 ed16" onclick="return editCategory(this);"></a>
                                    <a class="a16 del16" onclick="deleteCategory(this);return false;"></a>
                                </div>
                            </div>
                        </div>
                        <div class="cat-fields drop-box ca" catid='0'>
                            <asp:Repeater runat="server" ID="rpQuickReview">
                                <ItemTemplate>
                                    <div class="cat-field" title='<%# Eval("SourceTable") + "." + Eval("Name") %>' fieldid='<%# Eval("Id") %>'>
                                        <a class="a16 del16" onclick='removeField(<%# Eval("Id") %>, $(this).parent())'></a>
                                        <span>
                                            <%# Eval("Name")%></span></div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
