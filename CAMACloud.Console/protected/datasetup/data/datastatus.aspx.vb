﻿Public Class datastatus
    Inherits System.Web.UI.Page

    Dim dtSchema As DataTable

    Sub LoadSchemaTree(Optional parcelId As Integer = -1)
        tvTables.Nodes.Clear()
        tvOtherTables.Nodes.Clear()

        dtSchema = Database.Tenant.GetDataTable("SELECT * FROM vw_DataSourceTableTree ORDER BY TypeNo, ParentTableId, Relationship, TableName")

        Dim parcelFilter As String = "1 = 1"
        Dim templatefilter As String = " CC_Parcelid > -1 "

        For Each dr As DataRow In dtSchema.Rows
            If parcelId <> -1 Then
                Select Case dr.GetString("Type")
                    Case "P"
                        parcelFilter = "CC_ParcelId = " & parcelId
                    Case "A"
                        parcelFilter = "CC_ParcelId = " & parcelId
                    Case Else
                        parcelFilter = "1 = 1"
                End Select
            End If
            Dim Type As String = dr.GetString("Type")
            Dim targetTable As String = dr.GetString("TargetTable")
            Dim recordCount As Integer = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM " + targetTable + " WHERE " + parcelFilter + IIf((Type = "P" Or Type = "A"), " AND " + templatefilter, ""))
            dr("Records") = recordCount
            If dr.GetString("Type") = "A" Then
                dr("NewRecords") = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM " + targetTable + " WHERE " + IIf(targetTable = "ParcelData", " 1=2 AND ", "CC_RecordStatus = 'I' AND ") + parcelFilter + IIf((Type = "P" Or Type = "A"), " AND " + templatefilter, ""))
            End If
        Next

        LoadSchemaTableNode(tvTables.Nodes, , " AND Type IN ('P', 'A')")
        If parcelId = -1 Then
            tdOtherTables.Visible = True
            LoadSchemaTableNode(tvOtherTables.Nodes, , " AND Type IN ('N', 'NA', 'L')")
        Else
            tdOtherTables.Visible = False
        End If

        tvTables.ExpandAll()
        tvOtherTables.ExpandAll()
    End Sub

    Sub LoadSchemaTableNode(collection As TreeNodeCollection, Optional parentTableId As Integer = -1, Optional extraFilter As String = "")
        Dim filterString As String = "ParentTableId " + IIf(parentTableId = -1, "IS NULL", "= " & parentTableId)
        For Each cr As DataRow In dtSchema.Select(filterString + extraFilter)
            Dim cnode As New TreeNode
            cnode.Text = "<b>" + cr.GetString("TableName") + "</b>"
            cnode.ToolTip = cr.GetString("TableName") + vbNewLine + "Target Table:" + cr.GetString("TargetTable") + vbNewLine + "Number of records: " + cr("Records").ToString
            If cr.GetString("Type") = "A" Then
                cnode.ToolTip += vbNewLine + "New records created: " + cr("NewRecords").ToString
                cnode.Text += " (<span class='trc' num='" + cr("Records").ToString + "'>" + cr("Records").ToString + "</span>/<span class='nrc' num='" + cr("NewRecords").ToString + "'>" + cr("NewRecords").ToString + "</span>)"
            Else
                cnode.Text += " (<span class='trc' num='" + cr("Records").ToString + "'>" + cr("Records").ToString + "</span>)"
            End If

            Select Case cr.GetString("Type")
                Case "P"
                    cnode.ImageUrl = "~/App_Static/images/icon16/tree-p.jpg"
                    cnode.ImageToolTip = "Parcel Table"
                Case "N"
                    cnode.ImageUrl = "~/App_Static/images/icon16/tree-n.jpg"
                    cnode.ImageToolTip = "Neighborhood Table"
                Case "L"
                    cnode.ImageUrl = "~/App_Static/images/icon16/tree-l.jpg"
                    cnode.ImageToolTip = "Lookup Table"
                Case Else
                    Select Case cr.GetInteger("Relationship")
                        Case 0
                            cnode.ImageUrl = "~/App_Static/images/icon16/tree-121.jpg"
                            cnode.ImageToolTip = "One-to-one"
                        Case 2
                            cnode.ImageUrl = "~/App_Static/images/icon16/tree-m21.jpg"
                            cnode.ImageToolTip = "Many-to-one"
                    End Select
            End Select
            'cnode.SelectAction = TreeNodeSelectAction.None
            LoadSchemaTableNode(cnode.ChildNodes, cr.GetInteger("TableId"))
            collection.Add(cnode)
        Next
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadSchemaTree()
            hfKeyValue1.Value = Database.Tenant.GetStringValue("SELECT keyField1 FROM application")
        End If
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Dim parcelId As Integer = Database.Tenant.GetIntegerValue("EXEC SP_SearchParcelFromSchemaTable @ParcelKey ={0}".SqlFormatString(txtKeyValue1.Text))
        If parcelId = 0 Then
            Alert("Parcel not found - '" + txtKeyValue1.Text + "'")
        Else
            LoadSchemaTree(parcelId)
        End If
    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        LoadSchemaTree()
        txtKeyValue1.Text = ""
    End Sub
End Class