﻿
Partial Class datasetup_data_importsettings
    Inherits System.Web.UI.Page



	Sub LoadImportSettingsForm()
		ddlPrimaryTable.FillFromSql("SELECT Name FROM DataSourceTable WHERE ImportType = 0 AND DoNotImport = 0", ClientSettings.PrimaryTable = "", , ClientSettings.PrimaryTable)
		ddlNbhdTable.FillFromSql("SELECT Name FROM DataSourceTable WHERE ImportType = 0 AND DoNotImport = 0", ClientSettings.NeighborhoodTable = "", , ClientSettings.NeighborhoodTable)
		ddlStreetSource.FillFromSql("SELECT Name FROM DataSourceTable WHERE ImportType = 0 AND DoNotImport = 0", ClientSettings.StreetAddressTable = "", , ClientSettings.StreetAddressTable)
		ddlComparable.FillFromSql("SELECT Name FROM DataSourceTable WHERE ImportType = 0 AND DoNotImport = 0", ClientSettings.ComparablesSource = "", , ClientSettings.ComparablesSource)
		LoadCommonKeyFields(ClientSettings.PrimaryTable)
		LoadNeighborhoodFields()
		LoadStreetFields()
		LoadComparableFields()

		txtAddressFormula.Text = ClientSettings.StreetAddressFormula
		txtComparableKeyFields.Text = ClientSettings.ComparablesKeyFields
	End Sub

	Sub LoadCommonKeyFields(tableName As String)
		Dim cfs = {ddlCommonKeyField1, ddlCommonKeyField2, ddlCommonKeyField3}
		For Each c In cfs
			c.Enabled = tableName.IsNotEmpty
		Next
		If tableName.IsNotEmpty Then
			ddlCommonKeyField1.FillFromSql("SELECT DSF.Name FROM DataSourceField DSF INNER JOIN DataSourceTable DST ON (DSF.TableId = DST.Id) WHERE DST.Name = " + tableName.ToSqlValue, True, "None", ClientSettings.CommonKeyField1)
			ddlCommonKeyField2.FillFromSql("SELECT DSF.Name FROM DataSourceField DSF INNER JOIN DataSourceTable DST ON (DSF.TableId = DST.Id) WHERE DST.Name = " + tableName.ToSqlValue, True, "None", ClientSettings.CommonKeyField2)
			ddlCommonKeyField3.FillFromSql("SELECT DSF.Name FROM DataSourceField DSF INNER JOIN DataSourceTable DST ON (DSF.TableId = DST.Id) WHERE DST.Name = " + tableName.ToSqlValue, True, "None", ClientSettings.CommonKeyField3)
		End If
	End Sub

	Protected Sub btnSaveSettings_Click(sender As Object, e As System.EventArgs) Handles lbSaveSettings.Click
		If ddlNbhdTable.SelectedValue <> "" And ddlNbhdField.SelectedValue = "" Then
			Alert("Please select valid Neighborhood Field and click Update Settings to save changes.")
			Return
		End If
		If txtAddressFormula.Text.Trim <> "" And ddlStreetSource.SelectedValue = "" Then
			Alert("Please select an address source table.")
			Return
		End If

		If Not ValidateAddressFormula() Then
			Return
		End If
		If ClientSettings.PrimaryTable <> ddlPrimaryTable.SelectedValue Then
			ClientSettings.PrimaryTable = ddlPrimaryTable.SelectedValue
			LoadImportSettingsForm()
		Else
			ClientSettings.CommonKeyField1 = ddlCommonKeyField1.SelectedValue
			ClientSettings.CommonKeyField2 = ddlCommonKeyField2.SelectedValue
			ClientSettings.CommonKeyField3 = ddlCommonKeyField3.SelectedValue
		End If
		ClientSettings.NeighborhoodTable = ddlNbhdTable.SelectedValue
		ClientSettings.NeighborhoodField = ddlNbhdField.SelectedValue
		ClientSettings.NeighborhoodGroup = ddlNbhdGroup.SelectedValue

		ClientSettings.StreetAddressTable = ddlStreetSource.SelectedValue
		ClientSettings.StreetAddressFormula = txtAddressFormula.Text.Trim.Replace(vbCr, "").Replace(vbLf, "")

		ClientSettings.ComparablesSource = ddlComparable.SelectedValue
		ClientSettings.ComparablesSubjectField = ddlSubjectField.SelectedValue
		ClientSettings.ComparablesKeyFields = txtComparableKeyFields.Text.Trim.Replace(vbCr, "").Replace(vbLf, "")

		Database.Tenant.Execute("EXEC cc_AssignKeyFields")
	End Sub

	Protected Sub ddlPrimaryTable_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlPrimaryTable.SelectedIndexChanged
		LoadCommonKeyFields(ddlPrimaryTable.SelectedValue)
	End Sub

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
			LoadImportSettingsForm()
		End If
	End Sub

	Sub LoadNeighborhoodFields()
		If ddlNbhdTable.SelectedValue.IsNotEmpty Then
			ddlNbhdField.FillFromSql("SELECT DSF.Name FROM DataSourceField DSF INNER JOIN DataSourceTable DST ON (DSF.TableId = DST.Id) WHERE DST.Name = " + ddlNbhdTable.SelectedValue.ToSqlValue, True, "None", ClientSettings.NeighborhoodField)
			ddlNbhdGroup.FillFromSql("SELECT DSF.Name FROM DataSourceField DSF INNER JOIN DataSourceTable DST ON (DSF.TableId = DST.Id) WHERE DST.Name = " + ddlNbhdTable.SelectedValue.ToSqlValue, True, "None", ClientSettings.NeighborhoodGroup)
		Else
			ddlNbhdField.Items.Clear()
			ddlNbhdGroup.Items.Clear()
		End If
	End Sub

	Sub LoadStreetFields()
		If ddlStreetSource.SelectedValue.IsNotEmpty Then
			ddlStreetFields.FillFromSql("SELECT DSF.Name FROM DataSourceField DSF INNER JOIN DataSourceTable DST ON (DSF.TableId = DST.Id) WHERE DST.Name = " + ddlStreetSource.SelectedValue.ToSqlValue, True, "-- Select to Add --")
		Else
			ddlStreetFields.Items.Clear()
		End If
	End Sub

	Protected Sub ddlNbhdTable_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlNbhdTable.SelectedIndexChanged
		LoadNeighborhoodFields()
	End Sub

	Function ValidateAddressFormula() As Boolean
		If ddlStreetSource.SelectedValue = "" Then
			Return True
		End If

		Dim tableName As String = ddlStreetSource.SelectedValue
		Dim fields = Database.Tenant.GetDataTable("SELECT Name FROM DataSourceField WHERE TableId IN (SELECT Id FROM DataSourceTable WHERE Name = '" + tableName + "')").AsEnumerable.Select(Function(x) x.GetString("Name").ToLower).ToArray
		Dim testFields = txtAddressFormula.Text.Trim.Replace(" ", "").Split("+")
		For Each f In testFields
			If Not fields.Contains(f.ToLower) Then
				Alert("Field '" + f + "' does not exist in table '" + tableName + "'")
				Return False
			End If
		Next

		Return True
	End Function

	Protected Sub ddlStreetSource_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlStreetSource.SelectedIndexChanged
		LoadStreetFields()
	End Sub

	Protected Sub ddlStreetFields_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlStreetFields.SelectedIndexChanged
		Dim field As String = ddlStreetFields.SelectedValue
		If field.IsNotEmpty Then
			If Not txtAddressFormula.Text.ToLower.Contains(field.ToLower) Then
				If txtAddressFormula.Text.Trim = "" Then
					txtAddressFormula.Text = field
				ElseIf txtAddressFormula.Text.Trim.EndsWith("+") Then
					txtAddressFormula.Text += " " + field
				Else
					txtAddressFormula.Text += " + " + field
				End If
			End If
		End If
		ddlStreetFields.SelectedIndex = 0
	End Sub

	Private Sub LoadComparableFields()
		If ddlComparable.SelectedValue <> "" Then
			ddlSubjectField.FillFromSql("SELECT DSF.Name FROM DataSourceField DSF INNER JOIN DataSourceTable DST ON (DSF.TableId = DST.Id) WHERE DST.Name = " + ddlComparable.SelectedValue.ToSqlValue, ClientSettings.ComparablesSubjectField.IsEmpty, "None", ClientSettings.ComparablesSubjectField)
			ddlAddComparable.FillFromSql("SELECT DSF.Name FROM DataSourceField DSF INNER JOIN DataSourceTable DST ON (DSF.TableId = DST.Id) WHERE DST.Name = " + ddlComparable.SelectedValue.ToSqlValue, True, "-- Select to Add --")
		Else
			ddlSubjectField.Items.Clear()
			ddlAddComparable.Items.Clear()
		End If
	End Sub

	Protected Sub ddlComparable_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlComparable.SelectedIndexChanged
		LoadComparableFields()
	End Sub

	Protected Sub ddlAddComparable_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlAddComparable.SelectedIndexChanged
		Dim field As String = ddlAddComparable.SelectedValue
		If field.IsNotEmpty Then
			If Not txtComparableKeyFields.Text.ToLower.Contains(field.ToLower) Then
				If txtComparableKeyFields.Text.Trim = "" Then
					txtComparableKeyFields.Text = field
				ElseIf txtComparableKeyFields.Text.Trim.EndsWith(",") Then
					txtComparableKeyFields.Text += field
				Else
					txtComparableKeyFields.Text += "," + field
				End If
			End If
		End If
		ddlAddComparable.SelectedIndex = 0
	End Sub
End Class
