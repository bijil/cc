﻿Imports CAMACloud.BusinessLogic
Public Class aggregate_fun
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
           LoadFields()
            rpnpfFields.DataSource = Database.Tenant.GetDataTable("select Value as Name from ClientSettings where Name like 'NPF%'")
            rpnpfFields.DataBind()
            RunScript("getAfnOnLoad();")
        End If
    End Sub

  
    Sub LoadFields()
        Dim dtField As DataTable
        dtField = Database.Tenant.GetDataTable("SELECT COLUMN_NAME as Name,DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = N'ParcelData' and COLUMN_NAME not in('ROWUID','ParentROWUID','ClientROWUID','ClientParentROWUID','CC_ParcelId','CC_LastUpdateTime','CC_Deleted') and DATA_TYPE in('float','int','bigint','decimal')")
        rpFields.DataSource = dtField
        rpFields.DataBind()
    End Sub

    Protected Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If IsPostBack Then
            RunScript("finalizePage();")
        End If
    End Sub

    Public Function checkForExisting(name As String)
        If Database.Tenant.GetIntegerValue("select count(*) from ClientSettings where Name like 'NPF%' and value={0}".SqlFormatString(name)) > 0 Then
            Return "cat-added"
        End If
        Return ""
    End Function
End Class

