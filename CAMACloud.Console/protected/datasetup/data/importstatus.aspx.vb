﻿Imports System.Linq
Imports System.Net
Imports CAMACloud.RemoteServices

Partial Class datasetup_data_importstatus
    Inherits System.Web.UI.Page

    Public ImportRunning As Boolean = False

    Sub LoadStatusReport()
        If Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM ImportJob WHERE Status IN (0,1)") = 0 Then
            Response.Redirect("import.aspx")
            Return
        End If
        hdnImportJobId.Value = Database.Tenant.GetIntegerValue("SELECT COALESCE(MAX(Id),0) FROM ImportJob")
        Dim ds As DataSet = Database.Tenant.GetDataSet("EXEC cc_GetImportStatus " + hdnImportJobId.Value)
        Dim dtJob As DataTable = ds.Tables(0)
        Dim job As DataRow = ds.Tables(0).Rows(0)
        Dim dtTables As DataTable = ds.Tables(1)

        If job.GetInteger("Status") = 0 Then
            btnStart.Visible = True
            btnCancel.Visible = True
        ElseIf job.GetInteger("Status") > 1 Then
            btnStart.Visible = True
            btnCancel.Visible = True
            ImportRunning = False
        Else
            btnStart.Visible = False
            btnCancel.Visible = False
            ImportRunning = True
            RunScript("		window.setTimeout('refreshPage();', 2000);")
        End If

        'Dim data = (From dr In dtTables Where dr.GetInteger("ImportType") = 0 Select dr Order By dr.GetString("Name")).AsDataView
        'Dim aux = (From dr In dtTables Where dr.GetInteger("ImportType") = 1 Select dr Order By dr.GetString("Name")).AsDataView
        'Dim lookup = (From dr In dtTables Where dr.GetInteger("ImportType") = 2 Select dr Order By dr.GetString("Name")).AsDataView


        fvJob.DataSource = dtJob
        fvJob.DataBind()

        If job.GetBoolean("ImportPrimary") Then
            fvPrimary.DataSource = dtJob
            fvPrimary.DataBind()
        End If

        gvTables.DataSource = dtTables
        gvTables.DataBind()

    End Sub

    Sub CancelJob()
        Database.Tenant.Execute("DELETE FROM ImportJob WHERE Id = " & hdnImportJobId.Value)
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadStatusReport()
        End If
    End Sub

    Protected Sub btnStart_Click(sender As Object, e As System.EventArgs) Handles btnStart.Click
        Dim srq As New ServiceRequest("StartDataImport")
        With srq
            .Add("OrganizationId", HttpContext.Current.GetCAMASession().OrganizationId)
            .Add("DatabaseId", 0)
        End With
        Dim sc As New ServiceConnector(IPAddress.Loopback, 9010)
        Dim resp As ServiceResponse = sc.SendRequest(srq)
        If resp.Success Then
            btnStart.Visible = False
            btnCancel.Visible = False
        Else
            Alert(resp.ErrorMessage)
            'CancelJob()
        End If
    End Sub

    Protected Sub btnRefresh_Click(sender As Object, e As System.EventArgs) Handles btnRefresh.Click
        LoadStatusReport()
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        Database.Tenant.Execute("DELETE FROM ImportJob WHERE Id = " & hdnImportJobId.Value)
        LoadStatusReport()
    End Sub
End Class
