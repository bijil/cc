﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/DataSetup.master"
	AutoEventWireup="false" Inherits="CAMACloud.Console.datasetup_data_fields" CodeBehind="fields.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        input[type="submit"]
		{
			padding:2px 2px !important;
		}
		.verifyLookup-popup {
			text-align: -webkit-center;
			max-height: 400px;
			max-width: 800px;
		}
		#gvResult td {
			border-bottom: 1px solid silver;
			padding: 2px 7px;
		}
		.edit-panel::-webkit-scrollbar {
            width: 4px;
            height:7px;
        }
        .expressionfields::-webkit-scrollbar {
            width: 4px;
            height:7px;
        }
		.txtCondition
		{
			min-width: 450px;
			min-height: 0px;
		}
		.btnVerify {
			background-image:url(/protected/datasetup/data/img/validate-icon.png); 
			width: 512px; 
			height: 512px; 
			zoom: 5%;
		    position: absolute;
	    	right: 350px;
	    	top: 1200px;
	    	cursor: pointer;
		}
		.jqte {
			height: 268px !important;
		}
		.jqte_editor {
		    height: 215px !important;
		}
		.txtLookupQuery{
			width: 450px;
			max-height:200px;
		}
		.txtNote{
			width:450px;
			max-height:auto;
		}
		.txtArea{
			width:450px;
			height:49px;
		}
		.masklayers {
		    background: black;
		    opacity: 0.5;
		    top: 0%;
		    width: 100%;
		    height: 100%;
		    bottom: 0%;
		    position: absolute;
		    display: none;
		}
		.maskinglayer {
		   
		    top: 35%;
		    width: 97%;
		    height: auto;
		    bottom: 0%;
		    position: absolute;
		    display: none;
		}
		
		textarea {
			resize: none;
		}
		/*.ui-dialog{
             max-height: 600px !important;
             overflow-y: auto !important;
             
		}*/
		.ddl-category {
			min-width: 400px;
			max-width:600px;
		}
    </style>
    <link rel="stylesheet" type="text/css" href="../../../App_Static/jslib/jquery-te/jquery-te-1.4.0.css" />
    <script type="text/javascript" src="../../../App_Static/jslib/jquery-te/jquery-te-1.4.0.min.js"></script>
	
	<script type="text/javascript">
		var selectedFieldId;
	    function showLoader() {
	        $('.fields-table').hide();
	        $('.wait-indicator').show();	       
        }
        
        function fieldPropMaskHandler(hmask) {
        	if (hmask) {
        		hideMask();
        		$('.masklayer').css('z-index', '20');
        	}
        	else {
        		$('.masklayer').css('z-index', '10000');
        		showMask();
        	}
        }
        
        $(document).keypress(
  			function(event){
    			if (event.which == '13' && document.activeElement.tagName.toString() != "TEXTAREA") {
     				 event.preventDefault();
    			}
		});
        $(document).ready(function() {
        	$('#txtInfoContent').jqte();
        	var removedTools = [7,8,9,10,11,12,17,18];
        	removedTools.forEach(function(toolId) {
        		$('.jqte_tool_'+toolId).hide();
        	});
        });

	    function disableField(type) {
	        var readOnlyField = $('#<%=txtReadOnlyExp.ClientID %>'), visibiltyField = $('#<%=txtVisibilityExp.ClientID %>');
		    (type == 'r') ? ($(visibiltyField).val() == '') ? $(readOnlyField).removeAttr('disabled') : $(readOnlyField).attr('disabled', 'disabled') : ((type == 'v') ? ($(readOnlyField).val() == '') ? $(visibiltyField).removeAttr('disabled') : $(visibiltyField).attr('disabled', 'disabled') : '');
        }

        function hookFields() {

            if ($('.ddl-category').val() == "-1") {
                $('.noqr').hide();
            } else {
                $('.noqr').show();
            }

            $('*[field]').each(function () {
                $(this).change(function () {
                    var c = this;
                    if (c.tagName.toLowerCase() == "span") {
                        c = $('input', this)[0];
                    }
                    var col = $(this).attr('field');
                    var item = $(this).parents('.field-item');
                    var fieldid = $(item).attr('fieldid');
                    var type = c.tagName.toLowerCase();
                    if (type == "input") {
                        type = $(c).attr('type');
                    }
                    if (type == "") {
                        type = "text";
                    }
                    var data = {
                        FieldId: fieldid,
                        ColumnName: col,
                        Type: type,
                        Value: $(c).val(),
                        LoginId: $('#MainContent_MainContent_loginId').val()
                    };

                    if (type == "checkbox") {
                        data.Value = c.checked ? 1 : 0;
                    }

                    __(data);
                    $ds('updatefield', data, function (resp) {
                        if (resp.status == "OK") {
                            if (col == "DataType") {
                                if ((data.Value == "5") || (data.Value == "11")) {
                                    $('select[field="LookupTable"]', item).removeClass('lookup-hide');
                                }
                                else {
                                    $('select[field="LookupTable"]', item).addClass('lookup-hide');
                                }
                            }
                            if (((col == "DoNotShowOnDE") || (col == "IsReadOnly") || (col == "IsReadOnlyDTR") || (col == "DoNotShowOnDTR") || (col == "IsReadOnlyMA") || (col == "DoNotShowOnMA"))) {
                                if ((data.Value == 1)) {
                                    //Hidden or ReadOnly or HideDTR Checked -->uncheck and disabled  IsRequiredDTROnly 
                                    if ((col == "DoNotShowOnDE") || (col == "IsReadOnly")) {
                                        //Hidden or ReadOnly Checked -->uncheck and disabled IsRequired 
                                        $('[field="IsRequired"] input', item)[0].checked = false;
                                        $('[field="IsRequired"] input', item).attr('disabled', true);
                                        data.ColumnName = 'IsRequired';
                                        data.Value = 0;
                                        $ds('updatefield', data, function (resp) {

                                        });
                                    }
                                    if (col == "IsReadOnly") {
                                        $('[field="IsReadOnlyDTR"] input', item).attr('disabled', true);
                                        if ($('[field="IsReadOnlyDTR"] input', item)[0].checked) {
                                            $('[field="IsReadOnlyDTR"] input', item)[0].checked = false;
                                            data.ColumnName = 'IsReadOnlyDTR';
                                            data.Value = 0;
                                            $ds('updatefield', data, function (resp) {

                                            });
                                        }

                                        $('[field="IsReadOnlyMA"] input', item).attr('disabled', true);
                                        if ($('[field="IsReadOnlyMA"] input', item)[0].checked) {
                                            $('[field="IsReadOnlyMA"] input', item)[0].checked = false;
                                            data.ColumnName = 'IsReadOnlyMA';
                                            data.Value = 0;
                                            $ds('updatefield', data, function (resp) {

                                            });
                                        }
                                    }
                                    if (col == "DoNotShowOnDE") {
                                        $('[field="DoNotShowOnDTR"] input', item).attr('disabled', true);
                                        if ($('[field="DoNotShowOnDTR"] input', item)[0].checked) {
                                            $('[field="DoNotShowOnDTR"] input', item)[0].checked = false;
                                            data.ColumnName = 'DoNotShowOnDTR';
                                            data.Value = 0;
                                            $ds('updatefield', data, function (resp) {

                                            });
                                        }

                                        $('[field="DoNotShowOnMA"] input', item).attr('disabled', true);
                                        if ($('[field="DoNotShowOnMA"] input', item)[0].checked) {
                                            $('[field="DoNotShowOnMA"] input', item)[0].checked = false;
                                            data.ColumnName = 'DoNotShowOnMA';
                                            data.Value = 0;
                                            $ds('updatefield', data, function (resp) {

                                            });
                                        }
                                    }
                                    if ($('[field="IsRequiredDTROnly"] input', item)[0].checked)
                                        $('[field="IsRequired"] input', item).attr('disabled', false);
                                    if (col.indexOf('MA') == -1) {
                                        $('[field="IsRequiredDTROnly"] input', item)[0].checked = false;
                                        $('[field="IsRequiredDTROnly"] input', item).attr('disabled', true);
                                        data.ColumnName = 'IsRequiredDTROnly';
                                        data.Value = 0;
                                        $ds('updatefield', data, function (resp) {

                                        });
                                    }
                                    if ($('[field="IsRequiredMAOnly"] input', item)[0].checked)
                                        $('[field="IsRequired"] input', item).attr('disabled', false);
                                    if (col.indexOf('DTR') == -1) {
                                        $('[field="IsRequiredMAOnly"] input', item)[0].checked = false;
                                        $('[field="IsRequiredMAOnly"] input', item).attr('disabled', true);
                                        data.ColumnName = 'IsRequiredMAOnly';
                                        data.Value = 0;
                                        $ds('updatefield', data, function (resp) {

                                        });
                                    }

                                    if (col == "DoNotShowOnDTR" || col == "IsReadOnlyDTR") {
                                        if (!$('[field="IsReadOnlyMA"] input', item)[0].checked && !$('[field="DoNotShowOnMA"] input', item)[0].checked)
                                            $('[field="IsRequiredMAOnly"] input', item).attr('disabled', false);
                                    }
                                    if (col == "DoNotShowOnMA" || col == "IsReadOnlyMA") {
                                        if (!$('[field="IsReadOnlyDTR"] input', item)[0].checked && !$('[field="DoNotShowOnDTR"] input', item)[0].checked)
                                            $('[field="IsRequiredDTROnly"] input', item).attr('disabled', false);
                                    }
                                }
                                else {
                                    if (!$('[field="IsReadOnly"] input', item)[0].checked && !$('[field="DoNotShowOnDE"] input', item)[0].checked) {
                                        //Hidden and ReadOnly not Checked -->enable IsRequired 
                                        $('[field="IsRequired"] input', item).attr('disabled', false);
                                        $('[field="IsReadOnlyDTR"] input', item).attr('disabled', false);
                                        $('[field="IsReadOnlyMA"] input', item).attr('disabled', false);
                                        $('[field="DoNotShowOnMA"] input', item).attr('disabled', false);
                                        $('[field="DoNotShowOnDTR"] input', item).attr('disabled', false);
                                        if (!$('[field="DoNotShowOnDTR"] input', item)[0].checked) {
                                            //Hidden and ReadOnly and  HideDTR not Checked -->enable IsRequiredDTROnly 
                                            $('[field="IsRequiredDTROnly"] input', item).attr('disabled', false);
                                        }
                                        if (!$('[field="DoNotShowOnMA"] input', item)[0].checked) {
                                            $('[field="IsRequiredMAOnly"] input', item).attr('disabled', false);
                                        }
                                    }
                                    else if (!$('[field="DoNotShowOnDE"] input', item)[0].checked) {
                                    	$('[field="DoNotShowOnMA"] input', item).attr('disabled', false);
                                        $('[field="DoNotShowOnDTR"] input', item).attr('disabled', false);
                                    }
                                }
                            }

                            if ((col == "IsRequiredDTROnly") || (col == "IsRequiredMAOnly")) {
                                //IsRequiredDTROnly Checked  -->uncheck and disabled IsRequired
                                if (data.Value == 1) {
                                    $('[field="IsRequired"] input', item)[0].checked = false;
                                    $('[field="IsRequired"] input', item).attr('disabled', true);
                                    data.ColumnName = 'IsRequired';
                                    data.Value = 0;
                                    $ds('updatefield', data, function (resp) {

                                    });
                                }
                                else {
                                    // IsRequiredDTROnly  not Checked -->enable IsRequired
                                    if (!$('[field="IsRequiredDTROnly"] input', item)[0].checked && !$('[field="IsRequiredMAOnly"] input', item)[0].checked) {
                                        $('[field="IsRequired"] input', item).attr('disabled', false);
                                    }
                                }
                            }
                        }
                        else {
                            alert("Error on updating field property.");
                        }
                    });
                });
            });
        }

       
       function showPopup(title) {    
       		var wheight=document.documentElement.scrollHeight;
            $('.edit-panel').html('');;
            $('.edit-panel1').hide();
            $('.edit-popup').dialog({
                modal: true,
                width: 705,
                height: 'auto',
                minHeight: "auto",
                position: ['center', 70],
                resizable: false,
                title: title,
                open: function (type, data) {
                    $(this).parent().appendTo("form");                    
                },
 				dragStop: function(event, ui) {
		          if(document.documentElement.scrollHeight > $('body').height()) {
		           setDialogueTop();
		          }
				}                
            });
		    $('.showSidePanel').css('z-index','1000');          
			$('.ui-widget-overlay').css('height',wheight); 
			setDialogueTop()
        }

        function hidePopup() {
            $('.edit-popup').dialog('close');
        }

        function editProperties(fieldId, name, displayname) {
            try {
            	fieldPropMaskHandler();
                showPopup('Edit Properties - ' + displayname + ' (' + name + ')')
				selectedFieldId = fieldId;
            } catch (e) {

            }
            return false;
        }

        $(hookFields);

        function hideRoleProperties(role) {
            $('.' + role).hide();
        }        
        
        function verifyLookup() {
        	var lookupQuery = $('.txtLookupQuery').val();
        	if (lookupQuery.trim().length == 0) {
				alert("Please enter a valid query");
				return;
			}
			
			if (lookupQuery.search(/\{[a-zA-Z0-9_.]*\}/i) > 0) {
				alert("Query with the custom fields will not be executed.")
				return;
			}
			
			var fromSplits, tables = [], joinSplits, i;
			try {
	        	fromSplits = lookupQuery.split(/from/i);
	        	for (i = 1; i < fromSplits.length; i++) 
                    tables.push("FROM " + fromSplits[i].trim().split((/\s+/))[0])
	        	joinSplits = lookupQuery.split(/join/i);
	        	for (i = 1; i < joinSplits.length; i++) 
                    tables.push("JOIN " + joinSplits[i].trim().split((/\s+/))[0])
	        	lookupQuery = lookupQuery.replace(/\|\|/g,'+')
        	}
        	catch (ex) {
        		console.error(ex);
        		alert("Please check the query.")
        		return;
        	}
        	var data = {query: lookupQuery, tables: tables.join(',')}
        	$.ajax({
        		url: '/datasetup/executequery.jrq',
				type: "POST",
		        dataType: 'json',
		        data: data,
		        success: function (data, status) {
	        		console.log(data, status)
		        	if (!data.failed && data.length > 0)
		        		fillTable(data)
	        		else alert('No results found on executing this query');
		        },
		        error: function (msg) {
		        	console.error(msg);
		        	alert('Failed to execute the query. Please check the query or contact the administrator.')
		        }
			});
        }
        
        function fillTable(data) {
        	var columns = Object.keys(data[0])
        	var header = '', trow = '';
        	columns.forEach(function (c) {
        		header += '<td>' + c + '</td>'
        	});
        	header = '<thead style="background-color: rgba(193, 187, 187, 0.31);"><tr>' + header + '</tr></thead>';
        	
        	data.forEach(function (d) {
        		trow += '<tr>'
        		columns.forEach(function (c) {
        			trow += '<td>' + d[c] + '</td>'
        		});
        		trow += '</tr>'
        	});
        	
        	showVerifyLookupPopup(header + trow);
                 
        }
        
        function showVerifyLookupPopup(tbody) {        	
        	$('#gvResult').html('');
            $('.verifyLookup-popup').dialog('destroy');
            $('.verifyLookup-popup').dialog({
                modal: true,
                width: 'auto',
                minHeight: "auto",
                height: "auto",
                position: ['center', 200],
                resizable: false,
                title: 'Lookup Query Result',
                open: function (type, data) {
                    $(this).parent().appendTo("form");
            		$('#gvResult').append(tbody);
            		$(this).parent().css({"top":"40%","left":"40%"})
                }
            });
            
            $('.verifyLookup-popup').dialog('open');
        }
		
		function editInfoContent() {
			
			$.ajax({
        		url: '/datasetup/getinfocontent.jrq',
				type: "POST",
		        dataType: 'json',
		        data: {id: selectedFieldId},
		        success: function (data, status) {
		        	if (data.status == "OK")
		        		showEditInfoContentPopup(data.message);
		        },
		        error: function (msg) {
		        	console.error(msg);
		        	alert('Failed to execute the query. Please check the query or contact the administrator.')
		        }
			});
		}
		
		function setDialogueTop(){
		    var wtop = window.scrollY;
            var lftHgt = $('.left-panel2').height();
            var visble = $('.left-panel').is(':visible');
            if(wtop < 70){ if(lftHgt <600) if(!visble) {wtop = 0;} else {wtop = 70;}}
            $('.ui-dialog').css('top',wtop);
		}
		
		function showEditInfoContentPopup(infoContent) {
			$('#txtInfoContent').html('');
			infoContent = infoContent.replace(/&lt;/g,'<').replace(/&gt;/g,'>');
			console.log('infoContent(get): ',infoContent)
            $('.editInfoContent-popup').dialog('destroy');
            $('.editInfoContent-popup').dialog({
                modal: true,
                width: 640,
                height: 400,
                position: [360, 70],
                resizable: false,
                title: 'Edit Info Content',
                open: function (type, data) {
                    $(this).parent().appendTo("form");
            		$('.jqte_editor').html(infoContent);            		
                }
            });
            
            $('.editInfoContent-popup').dialog('open');
		}
		
		function saveInfoContent() {
			var infoContent = $('.jqte_editor').html();
			if ($('.jqte_editor').text().trim() == '') infoContent = '';
				infoContent = infoContent.replace(/</g,'&lt;').replace(/>/g,'&gt;')
			console.log('infoContent(set): ',infoContent)
			$.ajax({
        		url: '/datasetup/saveinfocontent.jrq',
				type: "POST",
		        dataType: 'json',
		        data: {id: selectedFieldId, infoContent: infoContent},
		        success: function (data, status) {
	        		console.log(data, status)
		        	if (data.status == "OK")
		        		$('.editInfoContent-popup').dialog('close');
		        },
		        error: function (msg) {
		        	console.error(msg);
		        	alert('Failed to execute the query. Please check the query or contact the administrator.')
		        }
			});
		}
		
		window.onkeydown = function (e) { if (e.ctrlKey && (e.which == 65 || e.which == 97)) { var v =document.activeElement.scrollHeight; $(document.activeElement).height(v);}}
		
        function setHeight(idname) {
 	    	idname.addEventListener('keydown', autosize);
	    function autosize(){
	    	var visble = $('.left-panel').is(':visible');
	    	if($('.edit-panel').height() > 530)
	    		{
	    			$('.edit-panel').height('552px');
	    		}
	    	else{ 
	    			$('.edit-panel').height('450px');
	    		}
		 	var el = this;
		 	var lftHgt = $('.left-panel2').height();
		  	setTimeout(function(){
		    	el.style.cssText = 'height:auto;';
		    	el.style.cssText = 'height:' + el.scrollHeight + 'px';	    	
		    },0);
		    $('.ui-dialog').position({
		  	  of: window,
		      using: function (pos) {
		      	  if(pos && pos.top < 70) if(lftHgt <600) if(!visble) {pos.top = 0;} else { pos.top = 70;}
		    	  $(this).css("top", pos.top);
		          }
		      });
		    }
		 }
		 function setH(idname){
		 	idname.style.cssText = 'height:49px';
		 	$('.edit-panel').height('auto');
		 }
		 function CheckBoxCheck(ab){
		 if ($(ab).prop('checked')){
		
    		$('#<%=spnPrecision.ClientID%>').css('display','inherit');
    		$('#<%=txtPrecision1.ClientID%>').removeAttr("readonly")
    		$('#<%=txtPrecision1.ClientID%>').removeAttr("disabled")
    		$('#<%=txtPrecision.ClientID%>').removeAttr("readonly")
    		$('#<%=txtPrecision.ClientID%>').removeAttr("disabled")
    	}
    	else{
    	$('#<%=txtPrecision1.ClientID%>').attr("disabled","disabled")
    	$('#<%=txtPrecision1.ClientID%>').attr("readonly","readonly")
    	$('#<%=txtPrecision.ClientID%>').attr("readonly","readonly")
    	$('#<%=txtPrecision.ClientID%>').attr("disabled","disabled")
    	var x = $('#<%=spnPrecision.ClientID%>').attr('datatype')
    	if (!x){
    	$('#<%=spnPrecision.ClientID%>').css('display','none');
    	}
    	
    	}
	}
		 	
		function mask(info, error, errorActionName, callback) {
			$('.edit-popup').addClass('masklayers');
			$('.ui-dialog').prepend('<div class="temp" style="width:100%;height:100%;position:absolute;z-index:1003;"></div>');
			//$('.masklayers').height($(window).height());
			 
			 $('.masklayers').show();
			 $('.maskinglayer').show();
		}
		function CloseDialog(){
			$('.maskinglayer').hide();
			$('.temp').remove();
			$('.edit-popup').removeClass('masklayers');
		}
    </script>
    <%If Not HasModule("DTR") Then%>
        <style type="text/css">
            .dtr {display:none !important;}
        </style>
    <%End If %>
    <%If Not HasModule("MobileAssessor") Then%>
        <style type="text/css">
            .ma {display:none !important;}
        </style>
    <%End If %>
    <%If HasModule("MobileAssessor") And Not HasModule("DTR") Then%>
        <style type="text/css">
            .ma {display:none !important;}
        </style>
    <%End If %>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<h1>
		Field Settings</h1>
	<asp:UpdatePanel runat="server" class="up-field-settings">
		<ContentTemplate>
			<div class="field-select-category ui-state-active ui-corner-all ca">
				<div class="fl">
					<span>Select Category:</span>
					<asp:DropDownList runat="server" ID="ddlCategory"  AutoPostBack="true" 
						onchange="showMask()" CssClass="ddl-category" />
				</div>
				<div class="fr" style="padding: 4px;">
					<asp:HiddenField runat="server" ID="hdnView" Value="0" />
					<asp:LinkButton runat="server" ID="lblSwitchView" Text="Switch to detailed view"
						OnClientClick="showLoader()" Visible="false" />
				</div>
			</div>
			<div style="height: 500px; display: none;" class="wait-indicator">
			</div>
			<table style="width: 100%;" class="mGrid fields-table">
                
				<tr class="ui-state-default">
					<th style="width: 18px;">
					</th>
					<th>
						Source/Name
					</th>
					<%--<th>Assigned Name</th>--%>
					<th style="min-width: 100px;">
						Display Label<br />(Max 30 chars)
					</th>
					<th style="width: 110px;">
						Input Type
					</th>
					<th style="width: 110px;">
						Lookup Source
					</th>
					<th style="width: 40px;" class="noqr">
						Read-Only
					</th>
					<th style="width: 40px;">
						Hidden
					</th>
					<th style="width: 35px;" class="noqr">
						Reqd.
					</th>
					<th style="width: 28px;" class="noqr auxonly">
						Grid.
					</th>
                    <th style="width: 30px;text-align: initial;" class="noqr dtr">
						Read-Only<br />DTR.
					</th>
                    <th style="width: 30px;text-align: initial;" class="noqr dtr">
						Hide<br />DTR.
					</th>
                    <th  style="width: 30px;text-align: initial;" class="noqr dtr">
						Reqd.<br />DTR Only
					</th>                    
                    <th  style="width: 30px;text-align: initial;" class="noqr ma">
						Read-Only<br />MA.
					</th>
                    <th style="width: 27px;text-align: initial;" class="noqr ma">
						Hide<br />MA.
					</th>
                    <th  style="width: 30px;text-align: initial;" class="noqr ma">
						Reqd.<br />MA.
					</th>
					<th style="width: 20px;">
					</th>
				</tr>
                <asp:HiddenField ID="loginId"  runat="server" />
				<asp:Repeater runat="server" ID="rpFields">
					<ItemTemplate>
						<tr class="field-row field-item" fieldid='<%# Eval("Id") %>'>
                            
							
                            <td class='<%# IIf((Eval("IsCustomField") = True And Not (Eval("CalculationExpression") Is DBNull.Value OrElse Eval("CalculationExpression").Trim = "")), "icon", "") %>' >
                              <asp:Image runat="server" ImageUrl="~/App_Themes/Cloud/images/custom_field_icon.png" CssClass="custome" Visible='<%# IIf(Eval("IsCustomField") = True, True, False)%>' ></asp:Image>
				              <asp:Image runat="server" ImageUrl="../../../App_Static/images/icon16/calculator.png" Visible='<%# IIf(Eval("CalculationExpression") Is DBNull.Value OrElse Eval("CalculationExpression").Trim = "", False, True) %>' />
							</td>
							<td><span title="<%# Eval("SourceTable") %>">
								<%# ShortenedText("SourceTable")%></span>.<b><%# Eval("Name")%></b></td>
							<%--							<td>
								<input class="text-input" type="text" field='AssignedName' value='<%# Eval("AssignedName")%>' /></td>--%>
							<td>
								<input class="text-input" type="text" field='DisplayLabel' value='<%# Eval("DisplayLabel")%>'
									maxlength="30" />
							</td>
							<td>
								<asp:DropDownList runat="server" ID="ddlInputTypes" field='DataType'>
								</asp:DropDownList>
							</td>
							<td>
								<asp:DropDownList runat="server" ID="ddlLookupSource" field='LookupTable'>
								</asp:DropDownList>
							</td>
							<td style="text-align: center;" class="noqr">
								<asp:CheckBox runat="server" Checked='<%# Eval("IsReadOnly") %>' field='IsReadOnly' />
							</td>
							<td style="text-align: center;">
								<asp:CheckBox runat="server" Checked='<%# Eval("DoNotShowOnDE") %>' field='DoNotShowOnDE' />
							</td>
							<td style="text-align: center;" class="noqr">
								<asp:CheckBox runat="server" Checked='<%# Eval("IsRequired") %>' field='IsRequired'
									Enabled='<%# Not Eval("IsReadOnly") And Not Eval("DoNotShowOnDE")  %>' />
							</td>
							<td style="text-align: center;" class="noqr auxonly">
								<asp:CheckBox runat="server" Checked='<%# Eval("ShowOnGrid") %>' field='ShowOnGrid' />
							</td>
                            <td style="text-align: center;" class="noqr dtr">
								<asp:CheckBox runat="server" Checked='<%# Eval("IsReadOnlyDTR")%>' field='IsReadOnlyDTR' 
                                    Enabled='<%# Not Eval("IsReadOnly")%>' />
							</td>
                            <td style="text-align: center;" class="noqr dtr">
								<asp:CheckBox runat="server" Checked='<%# Eval("DoNotShowOnDTR") %>' field='DoNotShowOnDTR' />
							</td>
                            <td style="text-align: center;" class="noqr dtr">
								<asp:CheckBox runat="server" Checked='<%# Eval("IsRequiredDTROnly")%>' field='IsRequiredDTROnly' 
                                    Enabled='<%# Not Eval("IsReadOnly") And Not Eval("DoNotShowOnDE")  %>' />
							</td>
                            <td style="text-align: center;" class="noqr ma">
								<asp:CheckBox runat="server" Checked='<%# Eval("IsReadOnlyMA")%>' field='IsReadOnlyMA' 
                                    Enabled='<%# Not Eval("IsReadOnly") %>' />
							</td>
                            <td style="text-align: center;" class="noqr ma">
								<asp:CheckBox runat="server" Checked='<%# Eval("DoNotShowOnMA")%>' field='DoNotShowOnMA' />
							</td>
                            <td style="text-align: center;" class="noqr ma">
								<asp:CheckBox runat="server" Checked='<%# Eval("IsRequiredMAOnly")%>' field='IsRequiredMAOnly' 
                                    Enabled='<%# Not Eval("IsReadOnly") And Not Eval("DoNotShowOnDE") And Not Eval("IsReadOnlyMA") And Not Eval("DoNotShowOnMA")  %>' />
							</td>
							<td>
								<asp:Button runat="server" Text="..." CommandName='EditProperties' CommandArgument='<%# Eval("Id") %>'
									OnClientClick='<%# EditPropertiesScript() %>' />
							</td>
						</tr>
						<tr class="field-settings-row field-item" fieldid='<%# Eval("Id") %>' visible='<%# hdnView.Value = "1" %>'
							runat="server">
							<td colspan="5" class="field-settings">Calculation Expression:<br />
								<asp:TextBox runat="server" TextMode="MultiLine" Text='<%# Eval("CalculationExpression")%>'
									field='CalculationExpression' />
							</td>
						</tr>
					</ItemTemplate>
				</asp:Repeater>
			</table>
			<div id="custom_panel" runat="server">
				<h3>
					Add custom field:</h3>
				<table>
					<tr>
						<td ><asp:Image runat="server"   CssClass="custome" ImageUrl="~/App_Themes/Cloud/images/custom_field_icon.png"></asp:Image> Field Name: </td>
						<td>
							<asp:TextBox runat="server" ID="txtFieldName" MaxLength="30" Width="250px" />
							<asp:RequiredFieldValidator runat="server" ControlToValidate="txtFieldName" ValidationGroup="ACF" />
						</td>
					</tr>
					<tr>
						<td style="padding-right: 30px;">Calculation Expression: </td>
						<td>
							<asp:TextBox runat="server" ID="txtCalcExp" MaxLength="30" Width="450px" TextMode="MultiLine"
								Rows="2" Style="overflow-y: auto; resize: none;" />
							<asp:RequiredFieldValidator runat="server" Enabled="false" ControlToValidate="txtCalcExp"
								ValidationGroup="ACF" />
						</td>
					</tr>
				</table>
				<asp:Button runat="server" ID="btnAddCustomField" Text="Add Field" ValidationGroup="ACF" style="margin-bottom: 10px;" />
			</div>
		</ContentTemplate>
	</asp:UpdatePanel>

	<div class="edit-popup" style="display: none;">    
	<div class="maskinglayer" style="display: none; z-index: 20;text-align:center">
			<span style="margin-top: 40%; align-items: center" class="app-mask-layer-info">
            <img src="/App_Static/images/loadingimg.gif"/><br>
            <span style='color: black; margin-left: 5px; margin-top: 20px;' >Please
                wait...</span></span>
    	</div>
		
		<asp:UpdatePanel ID="uplEdit" runat="server">
			<ContentTemplate>
				<asp:Panel runat="server" ID="pnlEdit" CssClass="edit-panel" Style="overflow-y:auto;">
                    <table>
                        <tr>
                            <td  style="padding-right: 10px;">Max Length: </td>
                            <td style="padding-right: 10px;">
                                <asp:TextBox runat="server" ID="txtMaxLength" MaxLength="4" Width="50px" ReadOnly="true"  onkeypress="return (event.keyCode!=13);" disabled/>
                                <asp:HiddenField ID="hcId" runat="server" Value="" />
                                <asp:HiddenField ID="hcType" runat="server" Value="" />
                                <asp:HiddenField ID="hReadOnly" runat="server" />
                                <asp:HiddenField ID="hIsCustomLookup" runat="server" />
                            </td>
                            <td>
                                <asp:CheckBox runat="server" ID="cbl_autonumber" Text="Auto Number" Style="float: right;" onkeypress="return (event.keyCode!=13);"/>
                                <asp:CheckBox runat="server" ID="cbl_autoselectfirst" Text="Auto select First Item from Dropdown" onkeypress="return (event.keyCode!=13);"/>
                                <asp:CheckBox runat="server" ID="cbLargeLookup" Text="Large Lookup" Visible="false" onkeypress="return (event.keyCode!=13);"/>                                          
                               <span id="spnPrecision" runat="server">
                               		<label for="txtPrecision1" style="margin-left: 24px;">Precision:</label><asp:TextBox ID="txtPrecision1" runat="server" onkeypress="return (event.keyCode!=13 && event.keyCode!=46);"  MaxLength="4" Width="50px" Style="margin-left: 10px;" type ="number" oninput="javascript: this.style.height = '15px'; if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"/>
                                    <label for="txtPrecision" style="margin-left: 24px;">Scale:</label><asp:TextBox ID="txtPrecision" runat="server" onkeypress="return (event.keyCode!=13 && event.keyCode!=46);"  MaxLength="4" Width="50px" Style="margin-left: 10px;"  type ="number" oninput="javascript: this.style.height = '15px'; if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"/>
                                    </span>                                 
                            </td>
                        </tr>
                        </table>
                    <div class = "expressionfields" style="overflow-y : auto;max-height : 360px;">
                       <table>
                        <tr runat="server" id="trCalcExp">
                            <td style="padding-right: 10px;">Calculation Expression: </td>
                            <td colspan="3">
                                <asp:TextBox runat="server" ID="txtFieldCalcExp" MaxLength="30" Width="450px" Height="50px" TextMode="MultiLine"
                                    Rows="3" Style="overflow-y: auto; resize: none;" onblur="setH(this);" CssClass="txtArea"/>
                            </td>
                        </tr>
                        <tr runat="server" id="trCalcoverrideExp">
                            <td style="padding-right: 10px;">Calculation Override Expression: </td>
                            <td colspan="3">
                                <asp:TextBox runat="server" ID="txtfieldoverridecalc" MaxLength="30" Width="450px" TextMode="MultiLine"
                                    Rows="3" Style="overflow-y: auto; resize: none;" onblur="setH(this);" CssClass="txtArea"/>
                            </td>
                        </tr>
                        <tr runat="server" id="trLookupQuery">
                            <td style="padding-right: 10px;">Lookup Query: </td>
                            <td colspan="3">
                                <asp:TextBox runat="server" ID="txtLookupQuery" CssClass="txtLookupQuery" TextMode="MultiLine"
                                    Rows="3" Style="overflow-y: auto; resize: none;" onblur="setH(this);"/>
                                <div class="btnVerify" onclick="verifyLookup();" title="Verify Lookup Query" ></div>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-right: 30px;">Hide-On Expression: </td>
                            <td colspan="3">
                                <asp:TextBox runat="server" ID="txtVisibilityExp" MaxLength="30" Width="450px" TextMode="MultiLine"
                                    Rows="3" Style="overflow-y: auto; resize: none;" onblur="setH(this);" CssClass="txtArea"/>
                                <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Enabled="false"  ControlToValidate="txtCalcExp" ValidationGroup="ACF" />--%>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-right: 30px;">ReadOnly Expression: </td>
                            <td colspan="3">
                                <asp:TextBox runat="server" ID="txtReadOnlyExp"
                                    MaxLength="30" Width="450px" TextMode="MultiLine" Rows="3" Style="overflow-y: auto; resize: none;" onblur="setH(this);" CssClass="txtArea"/>
                                <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Enabled="false"  ControlToValidate="txtCalcExp" ValidationGroup="ACF" />--%>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-right: 30px;">Required Expression: </td>
                            <td colspan="3">
                                <asp:TextBox runat="server" ID="txtrequiredExp"
                                    MaxLength="30" Width="450px" TextMode="MultiLine" Rows="3" Style="overflow-y: auto; resize: none;" onblur="setH(this);" CssClass="txtArea"/>
                                <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Enabled="false"  ControlToValidate="txtCalcExp" ValidationGroup="ACF" />--%>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-right: 30px;">RequiredSum Expression: </td>
                           <td colspan="3">
                               <asp:TextBox runat="server" ID="txtrequiredSum"  
                               Width="450px" TextMode="MultiLine" MaxLength="100" Rows="3" Style="overflow-y: auto; resize: none;" onkeypress="return (event.keyCode!=13);" onblur="setH(this);" CssClass="txtArea"/>
                           </td>
                        </tr>
                        <tr>
                            <td style="padding-right: 10px;">Default Value: </td>
                            <td colspan="3">
                                <asp:TextBox runat="server" ID="txtDefaultValue" MaxLength="100" Width="450px" onkeypress="return (event.keyCode!=13);"/>
                                <asp:DropDownList runat="server" ID="ddlDefaultValue" Width="456px" Visible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-right: 30px;">Note: </td>
                            <td colspan="3">
                                <asp:TextBox runat="server" ID="txtNote" CssClass="txtNote" TextMode="MultiLine"  Rows="3" Style="overflow-y: auto; resize: none;" onblur="setH(this);" />
                            </td>
                        </tr>
                       </table>
                      </div>
                     <table>                      
                        <tr>
                            <td style="padding-right: 10px;" colspan="4">
                                <asp:CheckBox runat="server" ID="chkMassUpdate" Text="Include this field in Mass Update facility (Aux Data Only)" onkeypress="return (event.keyCode!=13);"/><br />
                                <span>Note: The category should be marked to allow Mass Update.</span> </td>
                        </tr>
                        <tr>
                            <td style="padding-right: 10px;" colspan="4">
                                <asp:CheckBox runat="server" ID="chkUnique" Text="Hold unique value in sibling records (Aux Data Only)" onkeypress="return (event.keyCode!=13);"/>
                                    <asp:CheckBox runat="server" ID="isClsCalcAttr" Text="Is class calculator attribute" onkeypress="return (event.keyCode!=13);"/> 
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox runat="server" ID="isRequiredIfEdited" Text="Required if record edited" onkeypress="return (event.keyCode!=13);"/>
                            </td>
                        </tr>
                         <tr>
                            <td>
                                <asp:CheckBox runat="server" ID="isCustomInputType" Text="Custom Input Type" onkeypress="return (event.keyCode!=13);" onclick = "CheckBoxCheck(this);"/>
                            </td>
                        </tr>
                          <tr>
                            <td>
                                <asp:CheckBox runat="server" ID="DoNotAllowSpecialCharacters" Text="Do Not Allow Special Characters" onkeypress="return (event.keyCode!=13);" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>			
				<asp:Panel runat="server" ID="pnlEdit1" CssClass="edit-panel1" Style="">
					<asp:Button runat="server" ID="btnSaveChanges" Text=" Save Changes " OnClientClick="mask()"  /><input type="button" onclick="editInfoContent();" style="float: right;" value=" Edit Info Content " >
				</asp:Panel>
			</ContentTemplate>
		</asp:UpdatePanel>
	
	</div>

	<div class="verifyLookup-popup" style="display: none;">
		<div class="verifyLookup-panel">
			<table id="gvResult" style="border: 1px solid silver; margin: 20px; border-collapse: collapse;">
				<thead><tr><td>temp</td></tr></thead>
			</table>
		</div>
	</div>
	<div class="editInfoContent-popup" style="display: none;">
		<div class="editInfoContent-panel" style="overflow-y: auto; overflow-x: hidden; height: 330px;">
			<textarea id="txtInfoContent" ></textarea>
		</div>
		<input type="button" value=" Save " style="float: right;" onclick="saveInfoContent();" />
	</div>
	
</asp:Content>
