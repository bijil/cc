﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/DataSetup.master"
    AutoEventWireup="false" Inherits="CAMACloud.Console.datasetup_data_tables" Codebehind="tables.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">

        $(function () {
            $('.drop-box').sortable({
                connectWith: '.drop-box'
            }).disableSelection();

            $('.drop-box').droppable({
                drop: function (event, ui) {
                    var tableName = $.trim($(ui.draggable).text());
                    var importType = $(event.target).attr('boxtype');

                    if (importType == "-1") {
                        $ds('donotimport', {
                            TableName: tableName,
                            StatusFlag: true
                        }, function (data, status) {

                        });
                    }
                    else {
                        $ds('setimporttype', {
                            TableName: tableName,
                            ImportType: importType
                        }, function (data, status) {

                        });
                    }
                }
            });
        });

        function setPageDimensions() {
            var dtl = $('#contentTable').height() - 88 - 20 - 170 - 60 - 50;
            $('.ds-table-list').height(dtl);
            $('.ds-table-list').css({ 'overflow': 'auto' });
        }

    </script>
    <style type="text/css">
        .main-content-area
        {
            padding: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h1>
        Data Tables Layout & Distribution</h1>
    <p class="info">
        Drag and arrange the tables in required functional groups.</p>
    <div class="ds-table">
        <h3>
            Free Tables (Not Imported to CAMA Cloud)</h3>
        <div class="trash drop-box" boxtype="-1">
            <asp:Repeater runat="server" ID="rpDNI">
                <ItemTemplate>
                    <div class="table-name">
                        <span>
                            <%# Eval("Name")%></span></div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
    <table class="ds-table">
        <tr>
            <td>
                <h3>
                    Parcel Data</h3>
                <div class="ds-table-list drop-box" boxtype="0">
                    <asp:Repeater runat="server" ID="rpParcelData">
                        <ItemTemplate>
                            <div class="table-name">
                                <span>
                                    <%# Eval("Name")%></span></div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </td>
            <td>
                <h3>
                    Auxiliary Tables</h3>
                <div class="ds-table-list drop-box" boxtype="1">
                    <asp:Repeater runat="server" ID="rpSubTables">
                        <ItemTemplate>
                            <div class="table-name">
                                <span>
                                    <%# Eval("Name")%></span></div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </td>
            <td>
                <h3>
                    Lookup Tables</h3>
                <div class="ds-table-list drop-box" boxtype="2">
                    <asp:Repeater runat="server" ID="rpLookup">
                        <ItemTemplate>
                            <div class="table-name">
                                <span>
                                    <%# Eval("Name")%></span></div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </td>
            <td>
                <h3>
                    Neighborhood</h3>
                <div class="ds-table-list drop-box" boxtype="3">
                    <asp:Repeater runat="server" ID="rpNbhd">
                        <ItemTemplate>
                            <div class="table-name">
                                <span>
                                    <%# Eval("Name")%></span></div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
