﻿Imports System.IO
Imports System.Xml
Partial Class datasetup_apps_adeconfig
    Inherits System.Web.UI.Page

    Dim _path As String
    Sub SaveConfig()
        Dim sql As String
        If hdnConfigId.Value = "" Then
            sql = "INSERT INTO ADE_Config (Name, Description,ConfigType) VALUES ({1}, {2}, {3}); SELECT CAST(@@IDENTITY AS INT) As NeewId;"
        Else
            sql = "UPDATE ADE_Config SET Name = {1}, Description = {2},ConfigType={3} WHERE Id = {0};SELECT {0} As Id;"
        End If
        hdnConfigId.Value = Database.Tenant.GetIntegerValue(sql.SqlFormat(False, hdnConfigId, txtName, txtDescription, ddlConfigType.SelectedItem.Value))
        SetFormTitle()
    End Sub

    Sub SetFormTitle()
        If hdnConfigId.Value = "" Then
            lblTitle.Text = "Create new configuration"
        Else
            lblTitle.Text = "Edit configuration #" + hdnConfigId.Value
        End If
    End Sub

    Sub ClearForm()
        hdnConfigId.Value = ""
        txtName.Text = ""
        txtDescription.Text = ""
        SetFormTitle()
    End Sub

    Sub OpenConfigForEdit(configId As Integer)
        Dim dr As DataRow = t_("SELECT * FROM ADE_Config WHERE Id = " & configId)
        If dr IsNot Nothing Then
            txtName.Text = dr.GetString("Name")
            txtDescription.Text = dr.GetString("Description")
            hdnConfigId.Value = configId
        Else
            hdnConfigId.Value = ""
        End If
        SetFormTitle()
    End Sub

    Sub LoadGrid()
        grid.DataSource = d_("SELECT * FROM ADE_Config ORDER BY Name")
        grid.DataBind()
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        SaveConfig()
        LoadGrid()
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadGrid()
            ClearForm()
        End If
    End Sub

    Protected Sub grid_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grid.RowCommand
        Select Case e.CommandName
            Case "EditItem"
                OpenConfigForEdit(e.CommandArgument)
            Case "DeleteItem"
                e_("DELETE FROM ADE_Config WHERE Id = " & e.CommandArgument)
                e_("DELETE FROM ADE_Step WHERE ConfigId = " & e.CommandArgument)
                LoadGrid()
                If hdnConfigId.Value = e.CommandArgument Then
                    ClearForm()
                End If
            Case "SetAsDefault"
                e_("UPDATE ADE_Config SET IsDefault = 0")
                e_("UPDATE ADE_Config SET IsDefault = 1 WHERE Id = " & e.CommandArgument)
                LoadGrid()
            Case "ExportAsXML"
                Dim filename = GetConfigName(e.CommandArgument.ToString())
                Response.ContentType = "application/xml"
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filename + ".ccade")
                CreateXML(e.CommandArgument, Response.OutputStream)
                Response.End()
        End Select
    End Sub

    Protected Sub lblSwitchImport_Click(sender As Object, e As System.EventArgs) Handles lblSwitchImport.Click
        pnlEditor.Visible = False
        pnlImport.Visible = True
    End Sub

    Protected Sub lblSwitchEditor_Click(sender As Object, e As System.EventArgs) Handles lblSwitchEditor.Click
        pnlEditor.Visible = True
        pnlImport.Visible = False
    End Sub

    Protected Sub btnImport_Click(sender As Object, e As System.EventArgs) Handles btnImport.Click
        importXML(rdbImportType.SelectedItem.Value)
    End Sub

    Function GetConfigName(ByVal configid As String) As String
        Dim configname As String = "Export"
        Dim dr As DataRow = t_("SELECT * FROM ADE_Config WHERE Id = " & configid)
        If dr IsNot Nothing Then
            configname = dr.GetString("Name")
        End If
        Return configname
    End Function
    Sub CreateXML(ByVal configid As String, stream As Stream)
        Dim xmlDoc As New XmlDocument()
        'xmlDoc.AppendChild(xmlDoc.CreateElement("Configuration"))
        Dim dtconfig As DataTable = d_("SELECT * FROM ADE_config WHERE Id = " & configid)
        Dim xmljobstart As XmlElement = xmlDoc.CreateElement("Job")
        For Each dtjob In dtconfig.Rows

            Dim xmljobdata As XmlElement = xmlDoc.CreateElement("Jobdata")

            Dim xmlJobName As XmlElement = xmlDoc.CreateElement("Name")
            Dim xmlTextJobName As XmlText = xmlDoc.CreateTextNode(dtjob("Name"))
            xmlJobName.AppendChild(xmlTextJobName)
            xmljobdata.AppendChild(xmlJobName)

            Dim xmlJobdesc As XmlElement = xmlDoc.CreateElement("Description")
            Dim xmlTextjobdesc As XmlCDataSection = xmlDoc.CreateCDataSection(dtjob("Description"))
            xmlJobdesc.AppendChild(xmlTextjobdesc)
            xmljobdata.AppendChild(xmlJobdesc)

            Dim xmlIsdefault As XmlElement = xmlDoc.CreateElement("IsDefault")
            Dim xmlTextisdefault As XmlText = xmlDoc.CreateTextNode(dtjob("IsDefault"))
            xmlIsdefault.AppendChild(xmlTextisdefault)
            xmljobdata.AppendChild(xmlIsdefault)
            Dim xmlConfigType As XmlElement = xmlDoc.CreateElement("ConfigType")
            Dim xmlTextConfigType As XmlCDataSection = xmlDoc.CreateCDataSection(dtjob("ConfigType"))
            xmlJobdesc.AppendChild(xmlConfigType)
            xmljobdata.AppendChild(xmlTextConfigType)

            xmljobstart.AppendChild(xmljobdata)
        Next

        Dim dtsteps As DataTable = d_("SELECT * FROM ADE_Step WHERE ConfigId=" & configid)
        Dim xmlstepconfigroot As XmlElement = xmlDoc.CreateElement("StepConfig")

        For Each dr As DataRow In dtsteps.Rows
            Dim xmlsteps As XmlElement = xmlDoc.CreateElement("Steps")
            Dim xmlStage As XmlElement = xmlDoc.CreateElement("StageId")
            Dim xmlTextStage As XmlText = xmlDoc.CreateTextNode(dr("StageId"))
            xmlStage.AppendChild(xmlTextStage)
            xmlsteps.AppendChild(xmlStage)


            Dim xmlDescr As XmlElement = xmlDoc.CreateElement("Description")
            Dim xmlTextDescr As XmlCDataSection = xmlDoc.CreateCDataSection(dr("Description"))
            xmlDescr.AppendChild(xmlTextDescr)
            xmlsteps.AppendChild(xmlDescr)

            Dim xmlFieldId As XmlElement = xmlDoc.CreateElement("FieldId")
            Dim xmlTextFieldId As XmlText = xmlDoc.CreateTextNode(dr("FieldId"))
            xmlFieldId.AppendChild(xmlTextFieldId)
            xmlsteps.AppendChild(xmlFieldId)

            Dim xmlOrdinal As XmlElement = xmlDoc.CreateElement("Ordinal")
            Dim xmlTextOrdinal As XmlText = xmlDoc.CreateTextNode(dr("Ordinal"))
            xmlOrdinal.AppendChild(xmlTextOrdinal)
            xmlsteps.AppendChild(xmlOrdinal)

            Dim xmlFormFieldIdentifier As XmlElement = xmlDoc.CreateElement("FormFieldIdentifier")
            Dim xmlTextFormFieldIdentifier As XmlText = xmlDoc.CreateTextNode(dr.GetString("FormFieldIdentifier"))
            xmlFormFieldIdentifier.AppendChild(xmlTextFormFieldIdentifier)
            xmlsteps.AppendChild(xmlFormFieldIdentifier)

            Dim xmlFormInputTypeId As XmlElement = xmlDoc.CreateElement("FormInputTypeId")
            Dim xmlTextFormInputTypeId As XmlText = xmlDoc.CreateTextNode(dr.GetInteger("FormInputTypeId"))
            xmlFormInputTypeId.AppendChild(xmlTextFormFieldIdentifier)
            xmlsteps.AppendChild(xmlFormInputTypeId)

            Dim xmlFormFieldMaxLength As XmlElement = xmlDoc.CreateElement("FormFieldMaxLength")
            Dim xmlTextFormFieldMaxLength As XmlText = xmlDoc.CreateTextNode(dr.GetInteger("FormFieldMaxLength"))
            xmlFormFieldMaxLength.AppendChild(xmlTextFormFieldMaxLength)
            xmlsteps.AppendChild(xmlFormFieldMaxLength)

            Dim xmlTabIndex As XmlElement = xmlDoc.CreateElement("TabIndex")
            Dim xmlTextTabIndex As XmlText = xmlDoc.CreateTextNode(dr.GetInteger("TabIndex"))
            xmlTabIndex.AppendChild(xmlTextTabIndex)
            xmlsteps.AppendChild(xmlTabIndex)

            Dim xmlOffsetX As XmlElement = xmlDoc.CreateElement("OffsetX")
            Dim xmlTextOffsetX As XmlText = xmlDoc.CreateTextNode(dr.GetString("OffsetX"))
            xmlOffsetX.AppendChild(xmlTextOffsetX)
            xmlsteps.AppendChild(xmlOffsetX)

            Dim xmlOffsetY As XmlElement = xmlDoc.CreateElement("OffsetY")
            Dim xmlTextOffsetY As XmlText = xmlDoc.CreateTextNode(dr.GetString("OffsetY"))
            xmlOffsetY.AppendChild(xmlTextOffsetY)
            xmlsteps.AppendChild(xmlOffsetY)


            Dim xmlPreInputKeyCommands As XmlElement = xmlDoc.CreateElement("PreInputKeyCommands")
            Dim xmlTextPreInputKeyCommands As XmlCDataSection = xmlDoc.CreateCDataSection(dr.GetString("PreInputKeyCommands"))
            xmlPreInputKeyCommands.AppendChild(xmlTextPreInputKeyCommands)
            xmlsteps.AppendChild(xmlPreInputKeyCommands)

            Dim xmlPostInputCommands As XmlElement = xmlDoc.CreateElement("PostInputCommands")
            Dim xmlTextPostInputCommands As XmlCDataSection = xmlDoc.CreateCDataSection(dr.GetString("PostInputCommands"))
            xmlPostInputCommands.AppendChild(xmlTextPostInputCommands)
            xmlsteps.AppendChild(xmlPostInputCommands)

            Dim xmlConversionFormatter As XmlElement = xmlDoc.CreateElement("ConversionFormatter")
            Dim xmlTextConversionFormatter As XmlText = xmlDoc.CreateTextNode(dr.GetString("ConversionFormatter"))
            xmlConversionFormatter.AppendChild(xmlTextConversionFormatter)
            xmlsteps.AppendChild(xmlConversionFormatter)

            Dim xmlLookupValueField As XmlElement = xmlDoc.CreateElement("LookupValueField")
            Dim xmlTextLookupValueField As XmlText = xmlDoc.CreateTextNode(dr.GetString("LookupValueField"))
            xmlLookupValueField.AppendChild(xmlTextLookupValueField)
            xmlsteps.AppendChild(xmlLookupValueField)

            Dim xmlEnabled As XmlElement = xmlDoc.CreateElement("Enabled")
            Dim xmlTextEnabled As XmlText = xmlDoc.CreateTextNode(dr.GetBoolean("Enabled"))
            xmlEnabled.AppendChild(xmlTextEnabled)
            xmlsteps.AppendChild(xmlEnabled)
            xmlstepconfigroot.AppendChild(xmlsteps)

        Next
        xmljobstart.AppendChild(xmlstepconfigroot)
        xmlDoc.AppendChild(xmljobstart)
        xmlDoc.Save(stream)
    End Sub

    Private Sub importXML(ByVal importtype As String)
        Dim configxml As New XmlDocument()
        Dim sql As String

        'Try

        If CCADE.HasFile Then
            _path = CCADE.FileName

            If (System.IO.Path.GetExtension(_path).ToLower().Equals(".ccade")) Then

                Dim tempPath As String = (ConfigurationManager.AppSettings("UploadTempRoot") + HttpContext.Current.GetCAMASession().TenantKey + "\XML\")

                If Not Directory.Exists(tempPath) Then
                    Directory.CreateDirectory(tempPath)
                End If

                CCADE.PostedFile.SaveAs(tempPath + _path)
                configxml.Load(tempPath + _path)

                Dim xnjoblist As XmlNodeList = configxml.SelectNodes("//Jobdata")
                Dim xnsteplist As XmlNodeList = configxml.SelectNodes("//StepConfig//Steps")

                Dim dr As DataRow = t_("SELECT * FROM ADE_Config WHERE Name ='" + xnjoblist(0)("Name").InnerText + "'")
                If dr Is Nothing Or importtype = 1 Then
                    sql = "INSERT INTO ADE_Config (Name, Description,ConfigType) VALUES ({1}, {2}, {3}); SELECT CAST(@@IDENTITY AS INT) As NeewId;"
                    hdnConfigId.Value = Database.Tenant.GetIntegerValue(sql.SqlFormat(False, hdnConfigId, xnjoblist(0)("Name").InnerText, xnjoblist(0)("Description").InnerText, xnjoblist(0)("ConfigType").InnerText))
                Else
                    hdnConfigId.Value = dr.GetString("Id")
                    e_("UPDATE ADE_Config SET Name ={1}, Description = {2},ConfigType = {3} WHERE Id = {0}", True, hdnConfigId, xnjoblist(0)("Name").InnerText, xnjoblist(0)("Description").InnerText, xnjoblist(0)("ConfigType").InnerText)
                End If

                e_("DELETE FROM ADE_Step WHERE ConfigId = " & hdnConfigId.Value)

                For Each xnsteps As XmlNode In xnsteplist
                    e_("INSERT INTO ADE_Step (Description, FieldId, Ordinal, StageId, ConfigId,FormInputTypeId,FormFieldIdentifier,FormFieldMaxLength,TabIndex,OffsetX,OffsetY,PreInputKeyCommands,PostInputCommands) VALUES ({0}, {1}, {2}, {3}, {4},{5},{6},{7},{8},{9},{10},{11},{12})", True,
                       xnsteps("Description").InnerText, xnsteps("FieldId").InnerText, xnsteps("Ordinal").InnerText, xnsteps("StageId").InnerText, hdnConfigId.Value,
                       xnsteps("FormInputTypeId").InnerText, xnsteps("FormFieldIdentifier").InnerText, xnsteps("FormFieldMaxLength").InnerText, xnsteps("TabIndex").InnerText,
                       xnsteps("OffsetX").InnerText, xnsteps("OffsetY").InnerText, xnsteps("PreInputKeyCommands").InnerText, xnsteps("PostInputCommands").InnerText)
                Next

                LoadGrid()
            Else
                Alert("Invalid configuration file")
            End If
        End If
        'Catch ex As Exception
        '    Alert(_path.ToString())
        'End Try
    End Sub

    


End Class
