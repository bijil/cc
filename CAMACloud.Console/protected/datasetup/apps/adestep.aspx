﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/DataSetup.master"
    AutoEventWireup="false" Inherits="CAMACloud.Console.protected_datasetup_apps_adestep" Codebehind="adestep.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function setPageDimensions(cheight) {
            var dtl = cheight - 100 - 60;
            $('.sortable-table').height(dtl);
        }

        function resetPageFunctions() {
            $('.sortable-table').sortable({
                start: function (event, ui) {
                    var o = ui.item[0];
                    sortOldIndex = $(o).index();
                },
                stop: function (event, ui) {
                    var o = ui.item[0];
                    var cid = document.getElementById("CID").value;
                    var sid = $(o).attr('sid');
                    var sgid = $(o).attr('sgid');
                    var index = $(o).index();
                    var newIndex = (index + 1) * 10;
                    if (sortOldIndex > index) {
                        newIndex -= 5;
                    } else {
                        newIndex += 5;
                    }

                    $ds('moveadefield', {
                        NewIndex: newIndex,
                        ConfigId: cid,
                        StageId: sgid,
                        SID: sid
                    }, function (data) {
                        console.log(data);
                        if (data.status == "OK") {
                            $('#btnRefresh').click();
                        }
                    });

                }
            });
        }
        $(function () {
            resetPageFunctions();
        });
		
    </script>
    <script type="text/javascript">
        function showPopup(hideFields) {
            $('.edit-popup').dialog({
                modal: true,
                width: 600,
                resizable: false,
                title: 'Edit Data Entry Step',
                open: function (type, data) {
                    $(this).parent().appendTo("form");

                    $('#tabbed-editor').tabs({ disabled: [0], active: 1 });
                    if (hideFields) {
                        $('[href="\#tab-1"]').parent().hide()
                        $('#tab-2').removeClass('ui-tabs-hide');
                        $('#tab-1').addClass('ui-tabs-hide'); ;
                    }
                }
            });
        }

        function hidePopup() {
            $('.edit-popup').dialog('close');
        }

    </script>
    <style type="text/css">
        .col0
        {
            width: 25px;
        }
        .col1
        {
            width: 200px;
        }
        
        .col2
        {
            width: 300px;
        }
        
        .col3
        {
            width: 100px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField runat="server" ID="CID" ClientIDMode="Static" />
    <h1 runat="server" id="pageTitle">
        Automatic Data Entry - Edit Steps</h1>
    <div class="div-config-level">
        <label>
            Process Stage:
        </label>
        <asp:DropDownList runat="server" ID="ddlStage" AutoPostBack="true" />
    </div>
    <div class="div-cat-create ui-state-default ui-corner-all">
        <label>
            Add Field/Action:
        </label>
        <asp:DropDownList runat="server" ID="ddlField" Style="width: 260px;" AutoPostBack="true" />
        <label style="margin: 0px 8px 0px 15px;">
            Description:
        </label>
        <asp:TextBox runat="server" ID="txtLabel" Style="width: 220px;" />
        <asp:Button runat="server" ID="btnAddField" Text="Create" ValidationGroup="Create" />
        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtLabel" ValidationGroup="Create"
            Font-Size="Smaller">
               Description required
        </asp:RequiredFieldValidator>
    </div>
    <div class="edit-popup"  style="display: none;">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <div>
                    <asp:HiddenField runat="server" ID="hdnCFID" />
                    <table class="comparable-edit" style="margin-bottom: 10px;">
                        <tr>
                            <td class="label">
                                Task Description:
                            </td>
                            <td class="input" colspan="3">
                                <asp:TextBox runat="server" ID="txtDescription" />
                            </td>
                        </tr>
                    </table>
                    <div id="tabbed-editor">
                        <ul>
                            <li><a href="#tab-1">Field Settings</a></li>
                            <li><a href="#tab-2">Pre-Input Script</a></li>
                            <li><a href="#tab-3">Post-Input Script</a></li>
                        </ul>
                        <div id="tab-1">
                            <table class="comparable-edit">
                                <tr>
                                    <td class="label">
                                        Input Control Type:
                                    </td>
                                    <td class="hinput">
                                        <asp:DropDownList runat="server" ID="ddlInputType" />
                                    </td>
                                    <td class="label">
                                        &nbsp;
                                    </td>
                                    <td class="hinput">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <p>
                                            You can enter the mouse positions offset from the top left of the form.
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        Mouse Offset X:
                                    </td>
                                    <td class="hinput">
                                        <asp:TextBox runat="server" ID="txtOffsetX" />
                                    </td>
                                    <td class="label">
                                        Mouse Offset Y:
                                    </td>
                                    <td class="hinput">
                                        <asp:TextBox runat="server" ID="txtOffsetY" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <p>
                                            Other information could help correct identification of the controls.
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        Field Identifer (Internal):
                                    </td>
                                    <td class="input" colspan="3">
                                        <asp:TextBox runat="server" ID="txtFieldIdentifier" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">
                                        Tab Index:
                                    </td>
                                    <td class="hinput">
                                        <asp:TextBox runat="server" ID="txtTabIndex" />
                                    </td>
                                    <td class="label">
                                        Max Length:
                                    </td>
                                    <td class="hinput">
                                        <asp:TextBox runat="server" ID="txtMaxLength" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="tab-2">
                            <table class="comparable-edit">
                                <tr>
                                    <td class="input" colspan="4">
                                        <b>Pre-Input Commands</b> - Executed <b>before</b> Value Input in case of fields.<br />
                                        <asp:TextBox runat="server" ID="txtPreInputSequence" Rows="20" TextMode="MultiLine" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="tab-3">
                            <table class="comparable-edit">
                                <tr>
                                    <td class="input" colspan="4">
                                        <b>Post-Input Commands</b> - Executed <b>after</b> Value Input in case of fields.<br />
                                        <asp:TextBox runat="server" ID="txtPostInputCommands" Rows="20" TextMode="MultiLine" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <asp:Button runat="server" ID="btnSave" Text=" Save Changes " />
                    <asp:Button runat="server" ID="btnCancel" Text=" Cancel " />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <section class="comparable comparable-header">
        <table style="border-spacing: 0px; font-weight: bold;">
            <tr>
                <td class="col0">
                </td>
                <td class="col0">
                </td>
                <td class="col1">
                    Field Name
                </td>
                <td class="col2">
                    Description
                </td>
                <td class="col3">
                </td>
            </tr>
        </table>
    </section>
    <asp:UpdatePanel runat="server" ID="RightPanel" UpdateMode="Always">
        <ContentTemplate>
            <asp:Button runat="server" ID="btnRefresh" Style="display: none;" ClientIDMode="Static" />
            <div class="sortable-table">
                <asp:Repeater runat="server" ID="rpComp">
                    <ItemTemplate>
                        <div class="comparable" sid='<%# Eval("Id") %>' sgid='<%# Eval("StageId") %>'>
                            <table style="border-spacing: 0px;">
                                <tr>
                                    <td class="col0">
                                    </td>
                                    <td class="col0">
                                        <asp:LinkButton ID="LinkButton1" runat="server" CssClass="a16 del16" CommandArgument='<%# Eval("Id") %>'
                                            CommandName='DeleteItem' />
                                    </td>
                                    <td class="col1">
                                        <%# Eval("AssignedName")%>
                                    </td>
                                    <td class="col2">
                                        <%# Eval("Description")%>
                                    </td>
                                    <td class="col3">
                                        <asp:LinkButton runat="server" ID="lbEdit" Text="Edit" CommandName="EditItem" CommandArgument='<%# Eval("Id") %>' />
                                    </td>
                               </tr>
                            </table>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
