﻿
Partial Class protected_datasetup_apps_adestep
    Inherits System.Web.UI.Page
    Dim drc As DataRow
    Function PageIntegrityTest() As Boolean
        If ConfigId = -1 Then
            Response.Redirect("adeconfig.aspx")
            Return False
        End If
        drc = t_("SELECT * FROM ADE_Config WHERE Id = " & ConfigId)
        If drc Is Nothing Then
            Response.Redirect("adeconfig.aspx")
            Return False
        End If
        CID.Value = ConfigId
        pageTitle.InnerHtml = "Automatic Data Entry - Edit Steps - " & drc.GetString("Name")
        Return True
    End Function

    Public ReadOnly Property ConfigId As Integer
        Get
            Dim cid As Integer = -1
            Integer.TryParse(Request("cid"), cid)
            Return cid
        End Get
    End Property

    Sub FillADE_StageTable()
        Database.Tenant.Execute("INSERT INTO ADE_Stage VALUES (1, 'Job Start');")
        Database.Tenant.Execute("INSERT INTO ADE_Stage VALUES (2, 'Batch Start');")
        Database.Tenant.Execute("INSERT INTO ADE_Stage VALUES (3, 'Parcel Start');")
        Database.Tenant.Execute("INSERT INTO ADE_Stage VALUES (4, 'Field Configurations');")
        Database.Tenant.Execute("INSERT INTO ADE_Stage VALUES (5, 'Parcel Completion');")
        Database.Tenant.Execute("INSERT INTO ADE_Stage VALUES (6, 'Batch Completion');")
        Database.Tenant.Execute("INSERT INTO ADE_Stage VALUES (7, 'Job Completion');")
    End Sub

    Sub FillFormFieldInputTypes()
        Database.Tenant.Execute("TRUNCATE TABLE ADE_FormInputType")
        Database.Tenant.Execute("INSERT INTO ADE_FormInputType (Name) VALUES ('TextBox')")

    End Sub

    Protected Sub LoadGrid()
        rpComp.DataSource = Database.Tenant.GetDataTable("SELECT fc.*, df.AssignedName FROM ADE_Step fc LEFT OUTER JOIN cc_DataFields df ON fc.FieldId = df.Id WHERE fc.ConfigId = {0} AND fc.StageId = {1} ORDER BY Ordinal".FormatString(ConfigId, ddlStage.SelectedValue))
        rpComp.DataBind()
        If ddlStage.SelectedValue = "3" Or ddlStage.SelectedValue = "4" Or ddlStage.SelectedValue = "5" Then
            ddlField.Enabled = True
        Else
            ddlField.SelectedValue = ""
            ddlField.Enabled = False
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If PageIntegrityTest() Then

            End If
            If Not Database.Tenant.DoesTableExists("ADE_Stage") Then
                Database.Tenant.Execute("CREATE TABLE ADE_Stage (Id INT PRIMARY KEY, Name VARCHAR(50))")
                FillADE_StageTable()
            End If
            If Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM ADE_Stage") = 0 Then
                FillADE_StageTable()
            End If

            If Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM ADE_FormInputType") < 1 Then
                FillFormFieldInputTypes()
            End If

            ddlStage.FillFromSql("SELECT Id, Name FROM ADE_Stage", False)
            ddlField.FillFromSql("SELECT * FROM cc_DataFields ORDER BY 2", True, "** Non-Field Command/Operation")
            ddlInputType.FillFromSql("SELECT * FROM ADE_FormInputType", True, "Any Control", "")
            LoadGrid()
        End If
    End Sub

    Protected Sub ddlField_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlField.SelectedIndexChanged
        Dim defaultLabel As String = ""
        If ddlField.SelectedIndex > 0 Then
            defaultLabel = s_("SELECT DisplayLabel FROM DataSourceField WHERE Id = " & ddlField.SelectedValue)
        End If
        txtLabel.Text = "Field: " + defaultLabel
    End Sub

    Protected Sub btnAddField_Click(sender As Object, e As System.EventArgs) Handles btnAddField.Click
        Dim ordinal As Integer = i_("SELECT COALESCE(MAX(Ordinal), 0) + 10 FROM ADE_Step WHERE StageId = " & ddlStage.SelectedValue)
        e_("INSERT INTO ADE_Step (Description, FieldId, Ordinal, StageId, ConfigId) VALUES ({0}, {1}, {2}, {3}, {4})", False, txtLabel, IIf(ddlField.SelectedValue = "", "", ddlField.SelectedValue), ordinal, ddlStage.SelectedValue, ConfigId)
        LoadGrid()
        ddlField.SelectedIndex = 0
        txtLabel.Text = ""
    End Sub

    Protected Sub rpComp_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rpComp.ItemCommand
        If e.CommandName = "DeleteItem" Then
            e_("DELETE FROM ADE_Step WHERE ID = " & e.CommandArgument)
            LoadGrid()
        End If
        If e.CommandName = "EditItem" Then
            OpenCFforEdit(e.CommandArgument)
        End If
    End Sub

    Protected Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If IsPostBack Then
            RunScript("resetPageFunctions();")
        End If
    End Sub

    Protected Sub UseSubjectFields_Clicked(sender As Object, e As System.EventArgs)
        Dim chk As CheckBox = sender
        Dim rpi As RepeaterItem = chk.Parent
        Dim divNonDefault As HtmlControl = rpi.FindControl("divNonDefault")
        divNonDefault.Visible = chk.Checked
    End Sub


    Sub OpenCFforEdit(cfid As Integer)
        Dim dr As DataRow = t_("SELECT * FROM ADE_Step WHERE Id = " & cfid)
        hdnCFID.Value = dr.GetString("Id")
        ddlInputType.SelectedValue = dr.GetString("FormInputTypeId")
        txtDescription.Text = dr.GetString("Description")
        txtFieldIdentifier.Text = dr.GetString("FormFieldIdentifier")
        txtMaxLength.Text = dr.GetString("FormFieldMaxLength")
        txtOffsetX.Text = dr.GetString("OffsetX")
        txtOffsetY.Text = dr.GetString("OffsetY")
        txtTabIndex.Text = dr.GetString("TabIndex")
        txtPreInputSequence.Text = dr.GetString("PreInputKeyCommands")
        txtPostInputCommands.Text = dr.GetString("PostInputCommands")
        Select Case ddlStage.SelectedValue
            Case "3", "4", "5"
                RunScript("showPopup();")
            Case Else
                RunScript("showPopup(true);")
        End Select

    End Sub

    Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        e_("UPDATE ADE_Step SET [Description] = {1},[FormFieldIdentifier] = {2},[FormFieldMaxLength] = {3}, [TabIndex] = {4}, [OffsetX] = {5}, [OffsetY] = {6}, [PreInputKeyCommands] = {7}, [PostInputCommands] = {8}, FormInputTypeId = {9} WHERE Id = {0}", True,
           CInt(hdnCFID.Value), txtDescription, txtFieldIdentifier, txtMaxLength, txtTabIndex, txtOffsetX, txtOffsetY, txtPreInputSequence, txtPostInputCommands, ddlInputType)

        LoadGrid()

        RunScript("hidePopup();")
    End Sub

    Protected Sub btnRefresh_Click(sender As Object, e As System.EventArgs) Handles btnRefresh.Click
        LoadGrid()
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        RunScript("hidePopup();")
    End Sub

    Protected Sub ddlStage_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlStage.SelectedIndexChanged
        LoadGrid()
    End Sub

End Class
