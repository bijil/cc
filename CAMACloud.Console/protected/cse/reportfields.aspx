﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/DataSetup.master"
    CodeBehind="reportfields.aspx.vb" Inherits="CAMACloud.Console._new" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .validation{
            border-color : red;
        }
    </style>
    <script type="text/javascript">
        var nbsetting = '';
        function setPageDimensions(cheight) {
            var dtl = cheight - 208 - 40;
            $('.fields-list').height(dtl + 110);
            $('.category-list').height(dtl + 75);
        }
    </script>

    <script type="text/javascript">
        
        $(document).ready(function () {
            highLighter()
        })

        var sortOldIndex;
        function editCategory(b) {
            var co = $(b).parents('.category');
            $(co).addClass('editmode');
            $(".cat-num", co).hide();
            $('input[type="text"]', co).val($.trim($('.cat-name', co).text()));
            return false;
        }

        function deleteCategory(b) {
            var co = $(b).parents('.category');
            var catName = $.trim($('.cat-name', co).text());
            var catId = $(co).attr('catid');
             if (!confirm('Are you sure you want to delete the '+catName+' category and release the contained fields?')) { return false; }
            $ds('removeCSEcategory', {
                CatId: catId
            }, function (data) {
                if (data.status == "OK") {
                    $(co).fadeOut();
                    $('.cat-added[catid="' + catId + '"]').removeClass('cat-added');
                highLighter()
                }
                window.location.href = "/protected/cse/reportfields.aspx";
            });
        }

        function saveCatName(b) {
            var dupe= false
        	var c = $('.category').find('.cat-name').text();
            let h = []
            $('.category').find('.cat-name').each((i, el) => { h.push($(el)[0].innerText) })
            console.log(h)
        	var res = c.trim();
        	res = c.split(/\n/);
            var co = $(b).parents('.category');
            var oldName = $.trim($('.cat-name', co).text());
            var newName = $.trim( $('input[type="text"]', co).val());
            if (newName == '') {
                alert(" Category name cannot be blank");
                return cancelEdit(b);
            }
            if ((oldName == newName)||(newName.length > 50)) {
                return cancelEdit(b);
            }
            res.forEach(function(x){
            	if (x.indexOf(newName) != -1) {
            		dupe= true;}
            });
            dupe = h.includes(newName);
            	if (dupe == true ){
           alert(" The category name already exists");
           return cancelEdit(b);
           }
            var catId = $(co).attr('catid');
            $ds('renameCSEcategory', {
                CatId: catId,
                NewName: newName
            }, function (data) {
                if (data.status == "OK") {
                    $('.cat-name', co).html(newName);
                    $(".cat-num", co).show();
                    return cancelEdit(b);
                }
            });
        }

        function cancelEdit(b) {
            $(b).parents('.category').removeClass('editmode');
            $(".cat-num", $(b).parents('.category')).show();
            return false;
        }

        function toggleMinMax(b) {
            if ($(b).hasClass('max16')) {
                $(b).parents('.category').removeClass('minimized');
                $(b).switchClass('max16', 'min16');
            } else {
                $(b).parents('.category').addClass('minimized');
                $(b).switchClass('min16', 'max16');
            }
            return false;
        }

        $(finalizePage);

        function makeFieldsDraggable() {
            $('.master-field').sortable().disableSelection();
            $('.cat-fields').droppable({
                drop: function (event, ui) {
                    var src = event.srcElement;
                    if ($(src).hasClass('mfield') && (!$(src).hasClass('cat-added'))) {
                        var fieldId = $(src).attr('fieldid');
                        //    var tableId = $(src).attr('tableid');
                        var target = event.target;
                        var catId = $(target).attr('catid');
                        var displayname = $(src).attr('displaylabel');
                        var datatype = $(src).attr('datatype');
                        //  console.log(displayname, $(target), $(target).attr('fieldid'));
                        var postdata = {
                            FieldId: fieldId,
                            CatId: catId

                        };

                        $ds('addCSEfieldtocategory', postdata, function (resp) {
                            if (!isNaN(resp)) {
                                var fName = $(src).html();
                                var newField = '<div class="cat-field" fieldid="' + fieldId + '"><a class="a16 del16" onclick="removeField(' + fieldId + ',' + catId + ','+resp+', $(this).parent())"></a><span><a class="displaylabel" Dataformat="' + datatype + '"  onclick="editName(' + resp + ',$(this),event)">' + displayname + '</a>' + fName + '</span></div>';
                                $(target).html($(target).html() + newField);
                                $(src).attr('catid', catId);
                                $(src).addClass('cat-added');
                                highLighter()
                            } else {
                                alert(resp);
                            }

                        });
                    }

                }
            });

            $('.category-list').sortable({
                start: function (event, ui) {
                    var o = ui.item[0];
                    sortOldIndex = $(o).index();
                },
                stop: function (event, ui) {
                    var o = ui.item[0];
                    var catId = $(o).attr('catid')
                    var index = $(o).index();
                    var newIndex = (index + 1) * 10
                    console.log(sortOldIndex, index);

                    if (sortOldIndex > index) {
                        newIndex -= 5;
                    } else {
                        newIndex += 5;
                    }

                    $ds('moveCSEcategory', {
                        NewIndex: newIndex,
                        CatId: catId
                    }, function (data) {
                        if (data.status == "OK") {

                        }
                    });

                }
            });

            $('.cat-fields').sortable({
                start: function (event, ui) {
                    var o = ui.item[0];
                    sortOldIndex = $(o).index();
                },
                stop: function (event, ui) {
                    var o = ui.item[0];
                    var fieldId = $(o).attr('fieldid')
                    var index = $(o).index();
                    var newIndex = (index + 1) * 10;
                    if (sortOldIndex > index) {
                        newIndex -= 5;
                    } else {
                        newIndex += 5;
                    }

                    $ds('movecsefield', {
                        NewIndex: newIndex,
                        FieldId: fieldId
                    }, function (data) {
                        if (data.status == "OK") {

                        }
                    });

                }
            });
        }
       

        function removeField(fieldId, catId, forgid, cf) {
            if (!confirm('Are you sure you want to delete the category field ?')) { return false; }
            $ds('removeCSEfield', {
                FieldId: fieldId,
                CatId: catId
            }, function (data) {
                if (data.status == "OK") {
                    $(cf).fadeOut();
                    $('span[fieldid=' + forgid + ']').removeClass('cat-added');
                }
                highLighter()
                window.location.href = "/protected/cse/reportfields.aspx";
            });
           
        }
        var fID,fieldelement;
        function editName(csFID, field,e) {
            e.stopPropagation();
            var dataType = $(field).attr('dataformat')
            $('.txt_displayname').val($(field).html().trim());
            $('.txt_displayname').removeClass("validation");
            $('.ddl_datatype').val(dataType);
            if(dataType == "5"){
            	$('.ddl_lookuptable').val($(field).attr('lookuptable'));
            	$('.lookupTableRow').show();
            } else {
            	$('.lookupTableRow').hide();
            }
            fID = $(field).parents('.cat-field').first().attr('fieldid');
            fieldelement = field;
            $('.EditPopup').show();
            $('.EditPopup').css({ "top": $(fieldelement).position().top - (dataType == "5" ? 130 : 105), "left": $(fieldelement).position().left - 105 })
            
           $('.ddl_datatype').off('change').on('change',()=>{
	           var dType  = $('.ddl_datatype').val()
	           if(dType == "5"){
	            	$('.ddl_lookuptable').val($(field).attr('lookuptable'));
	            	$('.lookupTableRow').show();
	            } else {
	            	$('.lookupTableRow').hide();
	            }
	            $('.EditPopup').css({ "top": $(fieldelement).position().top - (dType == "5" ? 130 : 105), "left": $(fieldelement).position().left - 105 })
           });
        }
        function saveeditfield() {
            var label = $('.txt_displayname').val();
            $('.txt_displayname').removeClass("validation");
            var datatype=$('.ddl_datatype').val();
            var lookupTable = datatype == '5' ? $('.ddl_lookuptable').val() : '' ;
            if(!label.trim()){
                $('.txt_displayname').addClass("validation")
                return false;
            }
            if(label.length >50) label = label.slice(0,50);
            $ds('saveeditfield', {
                fieldid: fID,
                label: label,
                datatype: datatype,
                lookup : lookupTable
            }, function (data) {
                if (data.status == "OK") {
                  
                    $(fieldelement).html(label);
                    $(fieldelement).attr('dataformat', datatype);
                    $(fieldelement).attr('lookuptable', lookupTable);
                    $('.EditPopup').hide();
                }
            });
        }
        //        function setTableListStyles() {
        //            var aux = '[AUX]';
        //            var nbhd = '[NBHD]';
        //            var ddl = $('#ddlDataSource');
        //            $(ddl).append('<optgroup data label="Parcel Data"></optgroup>');
        //            $(ddl).append('<optgroup aux label="Auxiliary Data"></optgroup>');
        //            $(ddl).append('<optgroup nbhd label="' + nbsetting + ' Data"></optgroup>');
        //            var gd = $('optgroup[data]', ddl);
        //            var ga = $('optgroup[aux]', ddl);
        //            var gn = $('optgroup[nbhd]', ddl);
        //            $('option', ddl).each(function () {
        //                var li = this;
        //                var text = $(li).text();
        //                if (text.startsWith(aux)) {
        //                    text = text.replace(aux, '');
        //                    $(li).text(text);
        //                    $(li).attr('aux', 'true');
        //                    $(ga).append(li);
        //                } else if (text.startsWith(nbhd)) {
        //                    text = text.replace(nbhd, '');
        //                    $(li).text(text);
        //                    $(li).attr('nbhd', 'true');
        //                    $(gn).append(li);
        //                }
        //                else {
        //                    $(gd).append(li);
        //                }
        //            });
        //        }
        function closepopup() {
            $('.EditPopup').hide();
        }
        function finalizePage() {
            makeFieldsDraggable();
            $(document).click(function () {
                $('.EditPopup').hide();
            });
            $('.EditPopup').click(function (e) {
                e.stopPropagation();
            });
        }


        function highLighter() {
            $('.mfield.cat-added').mouseover(function () {
                var fieldids = $(this).attr('fieldid');
                $('.cat-field[fieldid="' + fieldids + '"]').css("background-color", "#56b0e3");
            });

            $('.mfield.cat-added').mouseout(function () {
                var fieldids = $(this).attr('fieldid');
                $('.cat-field[fieldid="' + fieldids + '"]').css("background-color", "#F0F0F0");
            });
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>
        Setup Comparable Field Categories</h1>
    <table class="app-page-table">
        <tr>
        	<div class="cat-editor">
        		<h2>
                        Manage Categories</h2>
        		<div class="div-cat-create ui-state-default ui-corner-all">
        			<div style="height: 28px;">
                        <div class="fl">
                            <label>
                                Add new category:</label>
                            <asp:TextBox runat="server" ID="txtNewCategory" Maxlength="50" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNewCategory"
                                ErrorMessage="*" ValidationGroup="Create" CssClass="validation-error" />
                            <asp:Button runat="server" ID="btnCreateCategory" Text="Create" ValidationGroup="Create" />
                        </div>
                        <div class="fr">
                            <asp:LinkButton runat="server" ID="lbExport" Text="Export Settings" />&nbsp;&nbsp;<span style="font-weight:normal;color:Black;">|</span>&nbsp&nbsp;
                            <asp:LinkButton runat="server" ID="lbImport" Text="Import" OnClientClick="document.getElementById('fuImportFile').click();return false;"/>
                        </div>
                    </div>
        			<div class="clear">
                            <div style="display: none;">
                                <asp:FileUpload runat="server" ID="fuImportFile" ClientIDMode="Static" onchange="document.getElementById('btnUploadImportFile').click()"
                                    accept="application/xml" />
                                <asp:Button runat="server" ID="btnUploadImportFile" ClientIDMode="Static" />
                            </div>
                        </div>
                </div>
            </div>
        </tr>
        <tr>
            <td class="td-field-master" style="width:350px">
                <div class="">
                    <h3>
                        Available Fields</h3>
                    <asp:UpdatePanel runat="server" class="fields-list drop-box flist" ID="FieldList"
                        ClientIDMode="Static">
                        <ContentTemplate>
                            <asp:Repeater runat="server" ID="rpFields">
                                <ItemTemplate>
                                    <div class="master-field">
                                        <span fieldid='<%# Eval("Id") %>' class='mfield <%# MasterFieldClass() %>' catid='<%# Eval("reportcategoryid") %>'
                                            tableid='<%# Eval("FieldTableId") %>' datatype='<%# Eval("datatype") %>' displaylabel='<%#Eval("DisplayLabel") %>'>
                                            <%# Eval("TableName")%>.<%# Eval("Name")%></span></div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </td>
            <td>
                <div class="cat-editor">
					<br> 
					<br>
					<br>					
                    <div class="category-list">
                        <asp:Repeater runat="server" ID="rpCategories">
                            <ItemTemplate>
                                <div class="category" catid='<%# Eval("Id") %>'>
                                    <div class="props ca">
                                        <div class="fl">
                                            <a class="a16 minmax min16 view" onclick="return toggleMinMax(this);"></a><span class="cat-name view">
                                                <%# Eval("Name")%>
                                            </span><span class="cat-num">(<%# Eval("Id") %>)</span><span class="cat-edit edit">
                                                <input type="text" MaxLength="50" />
                                                <div style="display: inline-block;" >
                                                <button onclick="saveCatName(this);return false;">
                                                    Save</button>
                                                <button onclick="return cancelEdit(this);">
                                                    Cancel</button>
                                                </div>
                                            </span>
                                        </div>
                                        <div class="fr view ">
                                            <div class="controls">
                                                <a class="a16 ed16" onclick="return editCategory(this);"></a><a class="a16 del16"
                                                    onclick="deleteCategory(this);return false;"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:HiddenField runat="server" ID="CatId" Value='<%# Eval("Id") %>' />
                                    <div class="cat-fields drop-box ca" catid='<%# Eval("Id") %>'>
                                        <asp:Repeater runat="server" ID="rpFields">
                                            <ItemTemplate>
                                                <div class="cat-field" title='<%# Eval("SourceTable") + "." + Eval("Name") %>' fieldid='<%# Eval("fieldorgid") %>'>
                                                    <a class="a16 del16" onclick='removeField(<%# Eval("cseID") %>, <%# Eval("reportcategory") %>, <%# Eval("fieldorgid") %>, $(this).parent())'>
                                                    </a><span><a class='displaylabel' dataformat='<%# Eval("OutputType") %>' lookupTable ='<%# Eval("LookupTable") %>' onclick='editName(<%# Eval("cseID") %>,$(this),event)'>
                                                        <%# Eval("Label")%></a>(<%# Eval("Name")%>)</span></div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <div class='EditPopup' style='display:none' >
        <table style="width: 100%;">
            <tr>
                <td>
                    Display Label :
                </td>
                <td>
                    <input id="txt_displayname" class='txt_displayname' type="text" />
                </td>
            </tr>
            <tr>
                <td>
                    Data Type :
                </td>
                <td>
                    <asp:DropDownList ID="ddl_datatype" class='ddl_datatype' runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class= 'lookupTableRow' style = 'display:none'>
                <td>
                    Lookup Table :
                </td>
                <td>
                    <asp:DropDownList ID="ddl_lookuptable" class='ddl_lookuptable' Width ="180px" runat="server" >
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td style=' padding-top:5px'>
                    <input id="btn_edit_save" type="button" value="Save" onclick='saveeditfield()' style=' margin-right:5px;' /><input
                        id="btn_edit_cancel" type="button" value="Cancel" onclick='closepopup()' />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
