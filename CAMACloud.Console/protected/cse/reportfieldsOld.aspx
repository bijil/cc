﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/DataSetup.master"
	AutoEventWireup="false" Inherits="CAMACloud.Console.cse_reportfields" Codebehind="reportfieldsOld.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
	<style type="text/css">
		.div-cat-create
		{
			padding:5px;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<h1>
		Setup Report Fields</h1>
	<div class="div-cat-create ui-state-default ui-corner-all">
		<label>
			Add Field:</label>
		<asp:DropDownList runat="server" ID="ddlCategory" Style="width: 160px;" />
		<asp:RequiredFieldValidator runat="server" ControlToValidate="ddlCategory" ErrorMessage="*" ValidationGroup="Create" />
		<asp:DropDownList runat="server" ID="ddlField" Style="width: 160px;" />
		<asp:RequiredFieldValidator runat="server" ControlToValidate="ddlField" ErrorMessage="*" ValidationGroup="Create" />
		<asp:TextBox runat="server" ID="txtLabel" Style="width: 160px;" />
		<asp:RequiredFieldValidator runat="server" ControlToValidate="txtLabel" ErrorMessage="*" ValidationGroup="Create" />
		<asp:Button runat="server" ID="btnAddField" Text="Add to Report" ValidationGroup="Create" />
	</div>
	<asp:GridView runat="server" ID="grid">
		<Columns>
			<asp:BoundField DataField="CategoryName" HeaderText="Category" ItemStyle-Width="180px" />
			<asp:BoundField DataField="FieldName" HeaderText="Field" ItemStyle-Width="180px" />
			<asp:BoundField DataField="Label" HeaderText="Label" ItemStyle-Width="180px" />
			<asp:TemplateField>
				<ItemTemplate>
					<asp:LinkButton runat="server" ID="lbDelete" Text="Delete" CommandName='DeleteField' CommandArgument='<%# Eval("Id") %>' OnClientClick="return confirm('Are you sure you want to delete the field from the report?');" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</asp:GridView>
</asp:Content>
