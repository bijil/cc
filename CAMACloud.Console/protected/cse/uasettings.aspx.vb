﻿
Partial Class cse_uasettings
    Inherits System.Web.UI.Page

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
            Dim dr As DataRow = t_("SELECT * FROM cse.Settings WHERE IsActive = 1 ORDER BY Id DESC")
            If dr Is Nothing Then
                e_("INSERT INTO cse.Settings (IsActive, UniformityIndicationLowPercent, MaxCompCount, HideUniformityIndicationValue) VALUES (1, 0.8, 5, 0);")
                dr = t_("SELECT * FROM cse.Settings WHERE IsActive = 1 ORDER BY Id DESC")
            End If

            hdnSID.Value = dr.GetInteger("Id")
            ddlUILowPerc.SelectedValue = dr.Get("UniformityIndicationLowPercent")
            ddlMaxComp.SelectedValue = dr.Get("MaxCompCount")
            chkHideUIF.Checked = dr.GetBoolean("HideUniformityIndicationValue")
        End If
	End Sub

	Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
		Dim sql As String = "UPDATE cse.Settings SET UniformityIndicationLowPercent = {0}, MaxCompCount = {1}, HideUniformityIndicationValue = {2} WHERE Id = " + hdnSID.Value
		Dim sqlF = sql.FormatString(ddlUILowPerc.SelectedValue, ddlMaxComp.SelectedValue, chkHideUIF.Checked.GetHashCode)
		e_(sqlF)
		Alert("Settings saved successfully")
	End Sub
End Class
