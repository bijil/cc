﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/CompSales.master" CodeBehind="testrules.aspx.vb" Inherits="CAMACloud.Console.testrules" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function validateInput() {
            var txtSubject = document.getElementById('<%= txtSubject.ClientID %>').value;
            var txtComp = document.getElementById('<%= txtComp.ClientID %>').value;
            if (txtSubject.includes("'") || txtComp.includes("'")) {
                alert('Invalid input! Single quotes not allowed.');
                return false; 
            }
            return true; 
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Test Rule Calculations</h1>
    <table>
        <tr>
            <td style="padding-right:30px;">Subject Parcel: </td>
            <td>
                <asp:TextBox runat="server" MaxLength="30" ID="txtSubject" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtSubject" Display="Dynamic" ErrorMessage="*" />
            </td>
        </tr>
        <tr>
            <td style="padding-right:30px;">Compare with: </td>
            <td>
                <asp:TextBox runat="server" MaxLength="30" ID="txtComp" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtComp" Display="Dynamic" ErrorMessage="*" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button runat="server" ID="btnVerify" Text="  Verify " OnClientClick="return validateInput();"  CausesValidation="true" />
            </td>
        </tr>
    </table>
    <asp:GridView runat="server" ID="grid">
        <Columns>
            <asp:BoundField DataField="Name" HeaderText="Name" ItemStyle-Width="300px" />
            <asp:BoundField DataField="Subj" HeaderText="@SUBJ" ItemStyle-Width="120px" DataFormatString="{0:0.0000}" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"  />
            <asp:BoundField DataField="Comp" HeaderText="@COMP" ItemStyle-Width="120px" DataFormatString="{0:0.0000}" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"  />
            <asp:BoundField DataField="ItemRank" HeaderText="@RANK" ItemStyle-Width="120px" DataFormatString="{0:0.0000}" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"  />
            <asp:BoundField DataField="ItemRule" HeaderText="@RULE" ItemStyle-Width="120px" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"  />
            <asp:BoundField DataField="ItemPercent" HeaderText="@PERCENT" ItemStyle-Width="120px" DataFormatString="{0:0.0000}" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"  />
            <asp:TemplateField>
                <ItemTemplate>
                    <%# If(Eval("ErrorOccured"), "<span style='color:red;font-weight:bold;'>" + Eval("ErrorMessage") + "</span>", "") %>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
