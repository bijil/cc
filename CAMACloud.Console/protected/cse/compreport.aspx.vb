﻿
Imports System.IO

Partial Class cse_compreport
    Inherits System.Web.UI.Page

    Dim NoOfComparables As Integer
    Dim SubjectId As Integer
    Dim ComparableIDs As New List(Of Integer)

    Public Uniformity As String
    Public UniformityIndication As String

    Public UniformityId As Integer
    Dim EstSalePrice
    Public organizationName As String
    Public stateName As String
    Dim hasroles = False
    Dim qPublicEnabled As String
    Dim qKeyField As String
    Public qFieldFormatter As String
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim currentUserRole = Roles.GetRolesForUser()
        If currentUserRole.Contains("QC") Then
            hasroles = True
        End If
        qPublicEnabled = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name='EnableqPublic'")
        qKeyField = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name='qPublicKeyField'")
        qFieldFormatter = Database.Tenant.GetStringValue("SELECT Value FROM ClientSettings WHERE Name='qPublicFormatter'")
        Dim requestedSubjectId As String = Request("s")
        Dim subject As String = ""
        If IsNumeric(requestedSubjectId) Then
            subject = requestedSubjectId
            SubjectId = Integer.Parse(requestedSubjectId)
        End If
        Dim comps As String = Request("c")
        comparableList.Value = requestedSubjectId + "," + comps
        Dim comparables As String = ""
        For Each c As String In comps.Split(",")
            If comparables <> "" Then
                comparables += ", "
            End If
            If IsNumeric(c) Then
                NoOfComparables += 1
                comparables += c
                ComparableIDs.Add(Int32.Parse(c))
            End If
        Next
        hid_search_params.Value = Request("searchparams")
        Dim k = hid_search_params.Value.Split(",").Length
        EstSalePrice = hid_search_params.Value.Split(",")(k - 1)
        If subject.IsNotEmpty AndAlso comparables.IsNotEmpty Then
            LoadReport(requestedSubjectId, comparables)
        End If
        organizationName = ClientOrganization.PropertyValue("County", HttpContext.Current.GetCAMASession().OrganizationId)
        stateName = ClientOrganization.PropertyValue("State", HttpContext.Current.GetCAMASession().OrganizationId)
    End Sub

    Sub ShowError(message As String)
        pnlError.Visible = True
        pnlResult.Visible = False
        errorMessage.InnerText = message
    End Sub

    Sub LoadReport(subject As String, comparables As String)
        Dim ds As DataSet
        Try
            ds = Database.Tenant.GetDataSet("EXEC cse.GetComparablesView " + subject + ", " + comparables + "," + "@EstSalePrice=" + EstSalePrice)
        Catch ex As Exception
            Dim message As String
            If TypeOf ex.InnerException Is SqlClient.SqlException Then
                Dim ix As SqlClient.SqlException = ex.InnerException
                message = ix.Message
            Else
                message = ex.Message
            End If

            ShowError(message)

            Return
        End Try

        report.DataSource = ds.Tables(0)
        report.DataBind()
        Dim dtIndex As Integer = 4
        footer.DataSource = ds.Tables(1)
        footer.DataBind()

        If ds.Tables.Count < 5 Then
            ShowError("Invalid output from the procedure. No sufficient data tables. 5 tables are expected.")
            Return
        End If

        Dim dt As New DataTable
        Dim u As DataRow = ds.Tables(2).Rows(0)
        Uniformity = u.GetString("Uniformity")
        UniformityIndication = u.GetString("UniformityIndication")
        hdnUniformityValue.Value = u.GetString("UniformityIndication")
        hideUniformity.Value = u.GetBoolean("HideUIF")
        UniformityId = u.GetInteger("UniformityId")
        uifooter.Visible = Not u.GetBoolean("HideUIF")
        If (EstSalePrice = 1) Then

            If ds.Tables.Count < 6 Then
                ShowError("Invalid output from the procedure. No sufficient data tables. 6 tables are expected.")
                Return
            End If

            dtIndex = 5
            div_Sale_price.Visible = True
            lbl_salePrice.Text = ds.Tables(4).Rows(0)(0)
            hdnsalePrice.Value = ds.Tables(4).Rows(0)(0)
            lbl_sl_label.Text = ds.Tables(4).Rows(1)(0)
        End If
        dt = ds.Tables(dtIndex)
        infoData.Value = GetJsonFromTAble(dt)

        lblUniInd.Text = Uniformity
        lblUniInd.Visible = Not u.GetBoolean("HideUIF")
    End Sub

    Public Function EvalCategoryColumn() As String
        If Eval("IsFirstItem") Then
            Return "<td class='catego' rowspan='" & Eval("CatCount") & "'>" + Eval("Category") + "</td>"
        End If
        Return ""
    End Function

    Public Function EvalCompColumns() As String
        Dim columns As String = ""
        For i As Integer = 1 To NoOfComparables
            Dim comp As Object = Eval("Comp" & i)
            Dim adj As Object = Eval("Adj" & i)
            If adj Is DBNull.Value Then
                columns += "<td colspan='2' class='val-col val-comp val-no-adj' style='text-align:center;'>" + comp.ToString + "</td>"
            Else
                columns += "<td class='val-col val-comp' style='text-align:center;'>" + comp.ToString + "</td><td class='val-col val-adj' style='text-align:center;'>" + adj.ToString + "</td>"
            End If
        Next
        Return columns
    End Function


    Public Function EvalCompHeaders(Optional excludeCloseButton As Boolean = False) As String
        Dim columns As String = ""
        For i As Integer = 1 To NoOfComparables
        	If excludeCloseButton = True Then
        		columns += "<th colspan='2'>Comparable #" & i & "</th>"
        	Else
        		columns += "<th colspan='2'>Comparable #" & i & "<span class='close-property no-print' style='color:red;float:right;cursor:pointer' onclick= 'RemoveCompfromReport(" & ComparableIDs(i - 1).ToString & ")'>X</span></th>"
        	End If	
        Next
        Return columns
    End Function
  
    Public Function IstipEnabled( Label As String ) As String
    	Label = Trim(Label)
    	Dim outt As String = " "
    	If  Label = "Median Sale for Comps" Then
    		outt="tip specialCase' abbr='Median Sale is the median sale price for the applicable parcel. i.e. - if it had more than 1 sale in the last few years. e.g., if Comp 2 had 2 sales of $50,000 and another of $61,000, then what would show in its column would be $55,500. If there was a 3rd of $70,000... then $61,000 would be displayed for the Median Sale value.'"
		'Select Case  attr
	    		'Case 1
	    			'outt = "tip"
	    		'Case 2 
	    			'outt = "Median Sale is the median sale price for the applicable parcel. i.e. - if it had more than 1 sale in the last few years. e.g., if Comp 2 had 2 sales of $50,000 and another of $61,000, then what would show in its column would be $55,500. If there was a 3rd of $70,000... then $61,000 would be displayed for the Median Sale value."
	    	'End Select
    
    	End if
    	Return outt	
    End Function

    Public Function EvalFooterColumns() As String
        Dim columns As String = ""
        For i As Integer = 1 To NoOfComparables
            columns += "<td colspan='2'  class='val-col foot-comp' style='text-align:center;'>" & Eval("Comp" & i) & "</td>"
        Next
        Return columns
    End Function

    Public Function EvalBlankColumns() As String
        Dim columns As String = ""
        For i As Integer = 1 To NoOfComparables
            columns += "<td colspan='2'>&nbsp;</td>"
        Next
        Return columns
    End Function

    Public Function EvalSubjectPhoto() As String
        Dim columns As String = "", img As String, img2 As String = ""
        Dim parcel As DataRow = t_("SELECT '/imgsvr/' + Path as Imagepath FROM ParcelImages WHERE parcelId = " & SubjectId.ToString + " order by isprimary desc")
        Dim click = EvalParcellink(SubjectId.ToString)
        If Not parcel Is Nothing Then
            img = "<img style='height:100px;min-width:135px;' src='" + parcel.GetString("Imagepath") + "' />"
            img2 = "<img style='height:200px;' class='img2' src='" + parcel.GetString("Imagepath") + "' />"
        Else
            img = "<img style='height:100px;min-width:135px;' src='/App_Static/images/residential.png' />"
            img2 = "<img style='height:200px;' class='img2' src='/App_Static/images/residential.png' />"
        End If
        columns += "<td class='val-col' " + click + "><a class='imgover' href='#'>" + img + img2 + "</a></td>"
        Return columns
    End Function
    Function EvalParcellink(parcelID) As String
        Return "onclick = ""openlink('" + parcelID + "')"""
    End Function
    Function EnabledqPublic() As String
        If (qPublicEnabled = "1" Or qPublicEnabled = "true") And (qKeyField <> "") Then
        Else
            Return "'display:none'"
        End If
    End Function
    Function QpublicLink(keyvalue, colspan) As String
        Dim column As String = ""
        Dim lnk = "onclick = ""openQpubliclink('" + keyvalue.ToString() + "')"""
        column = "<td colspan= " + colspan + " class='val-col' " + lnk + " ><a href='#'>Open In qPublic</a></td>"
        Return column
    End Function
    Function EvalQPublicLinkForSubject() As String
        If (qPublicEnabled <> "" And qPublicEnabled = "1") And (qKeyField <> "") Then
            Dim columns As String = ""
            Dim p As DataRow = t_("Select * From ParcelData WHERE cc_parcelId = " & SubjectId.ToString())
            If Not p Is Nothing Then
                columns = QpublicLink(p(qKeyField), "1")
            End If
            Return columns
        End If

    End Function
    Function EvalQPublicLinkForComps() As String
        If (qPublicEnabled <> "" And qPublicEnabled = "1") And (qKeyField <> "") Then
            Dim columns As String = ""
            For i As Integer = 1 To NoOfComparables
                Dim p As DataRow = t_("Select * From ParcelData WHERE cc_parcelId = " & ComparableIDs(i - 1).ToString)
                If Not p Is Nothing Then
                    columns += QpublicLink(p(qKeyField), "2")
                End If
            Next
            Return columns
        End If
    End Function

    Public Function EvalCompPhotos() As String
        Dim columns As String = "", img As String, img2 As String = ""
        For i As Integer = 1 To NoOfComparables
            Dim parcel As DataRow = t_("Select '/imgsvr/' + Path as Imagepath FROM ParcelImages WHERE parcelId = " & ComparableIDs(i - 1).ToString + " order by isprimary desc")
            Dim click = EvalParcellink(ComparableIDs(i - 1).ToString)
            If Not parcel Is Nothing Then
                img = "<img style='height:100px;min-width:135px;' src='" + parcel.GetString("Imagepath") + "' />"
                img2 = "<img style='height:200px;' class='img2' src='" + parcel.GetString("Imagepath") + "' />"
            Else
                img = "<img style='height:100px;min-width:135px;' src='/App_Static/images/residential.png' />"
                img2 = "<img style='height:200px;' class='img2' src='/App_Static/images/residential.png' />"
            End If
            columns += "<td colspan='2' class='val-col' " + click + " ><a class='imgover' href='#'>" + img + img2 + "</a></td>"
        Next
        Return columns
    End Function

    Dim EvenCateg As Boolean = False

    Public Function EvalCategoryRow() As String
        Dim k = "categ-first-row "
        If Eval("IsFirstItem") Then
            EvenCateg = Not EvenCateg
        End If
        If Eval("FieldType") = 2 Then
            k += "SalesField "
        End If
        k += IIf(EvenCateg, "categ-even", "categ-odd")
        Return k
    End Function

    Public Function UniformityStyle() As String
        If (EstSalePrice = 1) Then
            Return ""
        End If
        Select Case UniformityId
            Case 1
                Return "ind-uni"
            Case 2
                Return "ind-nonuni"
            Case 3
                Return "ind-insuff"
        End Select
        Return ""
    End Function
    Public Function GetJsonFromTAble(ByVal dt As DataTable) As String
        Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer()
        Dim rows As New List(Of Dictionary(Of String, Object))()
        Dim row As Dictionary(Of String, Object) = Nothing
        For Each dr As DataRow In dt.Rows
            row = New Dictionary(Of String, Object)()
            For Each dc As DataColumn In dt.Columns
                ''If dc.ColumnName.Trim() = "TAGNAME" Then
                row.Add(dc.ColumnName.Trim(), dr(dc))
                '' End If
            Next
            rows.Add(row)
        Next
        Return serializer.Serialize(rows)
    End Function

    Protected Sub Lb_go_back_Click(sender As Object, e As EventArgs) Handles Lb_go_back.Click
        Response.Redirect("Default.aspx?searchparams=" + hid_search_params.Value)
    End Sub
    Protected Sub lblExport_Click(sender As Object, e As System.EventArgs) Handles lblExport.Click
        Dim s As New StringBuilder()
        s.Append("<table style='border-spacing: 0px; border-collapse: collapse; font-size: 0.9em;' class='reportTable' id ='test_table'>")
        s.Append("<tr class='header-row'>")
        s.Append("<th colspan='2' class='" + UniformityStyle() + "'>")
        s.Append("<span>" + lblUniInd.Text + "</span></th>")
        s.Append("<th class='" + UniformityStyle() + "'>Subject</th>")
        s.Append(EvalCompHeaders(True))
        s.Append("<th class='no-border'></th></tr>")
        s.Append("<tr class='photo-row'><td colspan='2' class='label'></td>" + EvalSubjectPhoto() + EvalCompPhotos() + "</tr>")
        s.Append("<tr class='qPublic' style='" + EnabledqPublic() + "'><td colspan='2' class='label'></td>" + EvalQPublicLinkForSubject() + ">" + EvalQPublicLinkForComps() + "></tr>")
        Dim tw As New StringWriter(s)
        Dim hw As New HtmlTextWriter(tw)
        report.RenderControl(hw)
        footer.RenderControl(hw)
        uifooter.RenderControl(hw)
        s.Append(footer)
        s.Append(uifooter)
        s.Append("</table>")
        Response.Clear()
        Response.Buffer = True
        Response.ClearContent()
        Response.ClearHeaders()
        Response.AddHeader("Content-Disposition", "attachment;filename=" + organizationName + "-Comparability-Report.xlsx")
        Response.Charset = ""
        Response.ContentType = "application/vnd.openxml.formats-officedocument.spreadsheetml.sheet"
        Dim temp As TableToExcel = New TableToExcel()
        Dim excelStream = temp.Process(s.ToString())
        excelStream.CopyTo(Response.OutputStream)
        Response.End()

    End Sub
    Public Sub ExportToExcel(grdv As GridView, Name As String)
        Response.Clear()
        Response.ClearHeaders()
        Response.Buffer = True
        Response.ClearContent()
        Response.AddHeader("Content-Disposition", "Attachment;filename=" + Name)
        Response.Charset = ""
        Response.ContentType = "application/vnd.openxml.formats-officedocument.spreadsheetml.sheet"
        Dim excelstream = ExcelGenerator.ExportGrid(grdv, ViewState("exportdata"), "Comparability Report", "Comparability Report")
        excelstream.CopyTo(Response.OutputStream)
        Response.End()
    End Sub
End Class
