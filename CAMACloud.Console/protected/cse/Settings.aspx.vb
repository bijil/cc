﻿Public Class cse_Settings
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Database.Tenant.Execute("EXEC cse.VerifyEnvironment")
            BindData()
            ShowWarnings()
        End If
    End Sub

    Sub BindData()

        Dim sqlTables As String = "SELECT Name FROM DataSourceTable WHERE ImportType IN (0, 1) ORDER BY Name;"
        Dim sqlFields As String = "Select f.Name from DatasourceField f join DataSourceTable t on t.Id = f.TableId where f.DataType in (1,5) and t.ImportType in (0) ORDER BY f.Name"
        Dim SELECT_TABLE As String = "-- Select Table --", SELECT_FIELD As String = "-- Select Field --"

        Dim dr As DataRow = Database.Tenant.GetTopRow("SELECT TOP 1 * FROM cse.Settings WHERE IsActive = 1 ORDER BY Id DESC")
        hdnId.Value = dr.GetInteger("Id")
        ddlSales.FillFromSql(sqlTables, True, SELECT_TABLE, dr.GetString("SalesTable"))
        ddlValuation.FillFromSql(sqlTables, True, SELECT_TABLE, dr.GetString("ValuationTable"))
        ddlGroup.FillFromSql(sqlFields, True, SELECT_FIELD, dr.GetString("GroupField"))
        ddlNbhd.FillFromSql(sqlFields, True, SELECT_FIELD, dr.GetString("NBHDField"))
        Dim SaleTaleId = Database.Tenant.GetIntegerValue("Select ID from DataSourcetable where Name = {0}".SqlFormat(False, dr.GetString("SalesTable").ToString()))
        Dim sqlSalesField As String = "Select f.Name from DatasourceField f join DataSourceTable t on t.Id = f.TableId where f.DataType = 4 AND f.TableId = {0} ORDER BY f.Name".FormatString(SaleTaleId)
        ddlSalesfield.FillFromSql(sqlSalesField, True, SELECT_FIELD, dr.GetString("SalesDateField"))
        txtTtlfldVal.Text = dr.GetString("TotalValueField")
        txtSalesFilter.Text = dr.GetString("SalesTableFilter")
        txtAprlbl.Text = dr.GetString("AppraisedValueLabel")
    End Sub

    Sub ShowWarnings()
        warnings.Visible = False
        warnings.Controls.Clear()
        Dim parcelCount As Integer = Database.Tenant.GetIntegerValue("SELECT COUNT(*) FROM ParcelData")
        If parcelCount > 0 Then
            ShowIfNullCountHigher("Neighborhood Field", ddlNbhd.SelectedValue, parcelCount)
            ShowIfLargerDistribution("Neighborhood Field", ddlNbhd.SelectedValue, parcelCount)
            ShowIfNullCountHigher("Group Field", ddlGroup.SelectedValue, parcelCount)
            ShowIfLargerDistribution("Group Field", ddlGroup.SelectedValue, parcelCount)
        End If
    End Sub

    Sub ShowIfNullCountHigher(label As String, fieldName As String, parcelCount As Integer)
        If fieldName IsNot Nothing AndAlso fieldName.Trim <> "" Then
        	Dim sqlCheck As String = ""
        	Dim typeCheck As String = Database.Tenant.GetIntegerValue("SELECT df.DataType FROM DataSourceField df INNER JOIN DataSourceTable dt ON dt.Id = df.TableId WHERE dt.CC_targetTable = 'ParcelData' AND df.Name ='" + fieldName + "'")
        	If typeCheck = 1 Then
        		sqlCheck = "SELECT COUNT(*) FROM ParcelData WHERE NULLIF(" + fieldName.Wrap("[]") + ", '') IS NULL"
        	Else
        		sqlCheck = "SELECT COUNT(*) FROM ParcelData WHERE " + fieldName.Wrap("[]") + " IS NULL"
        	End If
            Try
            	Dim nullCount As Integer = Database.Tenant.GetIntegerValue(sqlCheck)
	            Dim percent As Integer = (nullCount / parcelCount) * 100
	            If percent > 10 Or nullCount > 2000 Then
	                Dim li As New HtmlGenericControl("li")
	                li.InnerHtml = "<b>{0}</b>: {1} of {2} ({3}%) parcels have empty or null value for <b>{4}</b>. This might result in bad computation.".FormatString(label, nullCount, parcelCount, percent, fieldName)
	                warnings.Controls.Add(li)
	                warnings.Visible = True
	            End If
            Catch ex As Exception
            	If label = "Neighborhood Field" Then
            		ddlNbhd.SelectedIndex = 0	
            	End If
            	If label = "Group Field" Then
            		ddlGroup.SelectedIndex = 0
            	End If
	          	Exit Sub
	          End Try
        End If
    End Sub

    Sub ShowIfLargerDistribution(label As String, fieldName As String, parcelCount As Integer)
        If fieldName IsNot Nothing AndAlso fieldName.Trim <> "" Then
            Dim sqlCheck As String = "SELECT TOP 1 PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY COUNT(*)) OVER (PARTITION BY null) FROM ParcelData GROUP BY " + fieldName.Wrap("[]")
            Try
            Dim median As Integer = Database.Tenant.GetIntegerValue(sqlCheck)
            Dim percent As Integer = (median / parcelCount) * 100
            If percent > 10 Or median > 1500 Then
                Dim li As New HtmlGenericControl("li")
                li.InnerHtml = "<b>{0}</b>: {1} appears to have broader distributed values. This might result in show or incorrect computation. The field is not a recommended grouping field.".FormatString(label, fieldName)
                warnings.Controls.Add(li)
                warnings.Visible = True
            End If
            Catch ex As Exception
            	If label = "Neighborhood Field" Then
	            		ddlNbhd.SelectedIndex = 0	
	            End If
	            If label = "Group Field" Then
	            		ddlGroup.SelectedIndex = 0
	            End If
            	Exit Sub
	          End Try
        End If
    End Sub

Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
		If txtTtlfldVal.Text.Length > 50 Then
			Alert("Maximum number of characters exceeded. Please short your TotalValueField characters to 50 or less.")
			Return
		End If
        If txtAprlbl.Text.Length > 50 Then
            Alert("Maximum number of characters exceeded. Please short your Appraised Value Label characters to 50 or less.")
            Return
        End If
        Dim DtApp As DataTable = Database.Tenant.GetDataTable("SELECT SalesTable,ValuationTable, GroupField, NBHDField,TotalValueField,SalesTableFilter, AppraisedValueLabel, SalesDateField FROM cse.Settings")
        Dim prevSalesTbl = ""
        Dim prevValuatnTbl = ""
        Dim prevGroupFld = ""
        Dim prevNbhdFld = ""
        Dim prevtotlValueFld = ""
        Dim prevSalesTbleFltr = ""
        Dim prevAprsdValLbl = ""
        Dim prevSalesDateField = ""
        For Each row As DataRow In DtApp.Rows
            prevSalesTbl = row.Item("SalesTable").ToString()
            prevValuatnTbl = row.Item("ValuationTable").ToString()
            prevGroupFld = row.Item("GroupField").ToString()
            prevNbhdFld = row.Item("NBHDField").ToString()
            prevtotlValueFld = row.Item("TotalValueField").ToString()
            prevSalesTbleFltr = row.Item("SalesTableFilter").ToString()
            prevAprsdValLbl = row.Item("AppraisedValueLabel").ToString()
            prevSalesDateField = row.Item("SalesDateField").ToString()
        Next
        If ((prevNbhdFld <> ddlNbhd.SelectedValue) Or (ddlGroup.SelectedValue <> prevGroupFld) Or (prevtotlValueFld <> txtTtlfldVal.Text) Or (ddlValuation.SelectedValue <> prevValuatnTbl) Or (prevAprsdValLbl <> txtAprlbl.Text) Or
            (prevSalesTbl <> ddlSales.SelectedValue) Or (prevSalesDateField <> ddlSalesfield.SelectedValue) Or (prevSalesTbleFltr <> txtSalesFilter.Text)) Then
            Alert("You have successfully updated Comparable Properties Settings ")
        Else
            Alert("You have not made any changes in Comparable Properties Settings")
        End If
        Dim sqlUpdate As String = "UPDATE cse.Settings SET SalesTable = {1}, ValuationTable = {2}, GroupField = {3}, NBHDField = {4}, TotalValueField = {5}, SalesTableFilter = {6}, AppraisedValueLabel = {7}, SalesDateField = {8} WHERE Id = {0}"
        Database.Tenant.Execute(sqlUpdate.SqlFormat(True, hdnId, ddlSales, ddlValuation, ddlGroup, ddlNbhd, txtTtlfldVal, txtSalesFilter, txtAprlbl, ddlSalesfield))
        ShowWarnings()
        'Alert("Settings Updated successfully")

    End Sub
End Class