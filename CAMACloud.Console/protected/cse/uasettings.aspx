﻿<%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/CompSales.master" AutoEventWireup="false" Inherits="CAMACloud.Console.cse_uasettings" Codebehind="uasettings.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
	<script>
        $(document).ready(function () {
            var ddlUILowPerc = $("#<%= ddlUILowPerc.ClientID %>");
            var ddlMaxComp = $("#<%= ddlMaxComp.ClientID %>");
            var chkHideUIF = $("#<%= chkHideUIF.ClientID %>");
            var btnSave = $("#<%= btnSave.ClientID %>");
            var originalValues = {
                ddlUILowPerc: ddlUILowPerc.val(),
                ddlMaxComp: ddlMaxComp.val(),
                chkHideUIF: chkHideUIF.prop("checked")
            };
            ddlUILowPerc.change(function () {
                checkValues();
            });
            ddlMaxComp.change(function () {
                checkValues();
            });
            chkHideUIF.change(function () {
                checkValues();
            });
            function checkValues() {
                if (ddlUILowPerc.val() != originalValues.ddlUILowPerc
                    || ddlMaxComp.val() != originalValues.ddlMaxComp
                    || chkHideUIF.prop("checked") != originalValues.chkHideUIF) {
                    btnSave.removeAttr("disabled");
                }
                else {
                    btnSave.attr("disabled", "disabled");
                }
            }
        });
    </script>
	<h1>User Accessibility Settings</h1>
	<asp:HiddenField runat="server" ID="hdnSID" />
	<table style="border-spacing:0px;">
		<tr>
			<td style="padding-right:25px;">
				Calculate Uniformity Indication based upon Comparability Indexes:
			</td>
			<td>
				<asp:DropDownList runat="server" ID="ddlUILowPerc" CssClass="chkSlct">
					<asp:ListItem Text="&gt;=80%" Value="0.80" />
					<asp:ListItem Text="&gt;=85%" Value="0.85" />
					<asp:ListItem Text="&gt;=90%" Value="0.90" />
				</asp:DropDownList>
			</td>
		</tr>
		<tr>
			<td>
				 Maximum number of Selections for Comp Report allowed:
			</td>
			<td>
				<asp:DropDownList runat="server" ID="ddlMaxComp" CssClass="chkSlct">
					<asp:ListItem Text="5" Value="5" />
					<asp:ListItem Text="10" Value="10" />
					<asp:ListItem Text="15" Value="15" />
				</asp:DropDownList>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<asp:CheckBox runat="server" ID="chkHideUIF" Text="Hide Uniformity Indicated Value" CssClass="chkSlct"  />
			</td>
		</tr>
	</table>
	<p>
		<asp:Button runat="server" ID='btnSave' Text="  Save Settings "  class="btnSave" disabled/>
	</p>
</asp:Content>

