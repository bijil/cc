﻿<%@ Page Title="Comparability Report View" Language="VB" MasterPageFile="~/App_MasterPages/CompSales.master"
    AutoEventWireup="false" Inherits="CAMACloud.Console.cse_compreport" CodeBehind="compreport.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyC7e5YrZlKqWtaY4ehsDwMgCis4IfNs7Xw"></script>
    <style type="text/css">
        .vt {
            -moz-transform: rotate(-270deg);
            -moz-transform-origin: bottom left;
            -webkit-transform: rotate(-270deg);
            -webkit-transform-origin: top right;
            -o-transform: rotate(-270deg);
            -o-transform-origin: bottom left;
            width: 50px;
            filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=1);
        }

        .report-container {
            overflow: auto;
            overflow-style: panner;
        }

        div.actions {
            text-align: right;
            padding-right: 20px;
            margin-top: -45px;
            margin-bottom: 15px;
        }

            div.actions a {
                margin-left: 20px;
                font-weight: bold;
            }

        .hideSearchPanel {
            background: url(/App_Static/css/images/close.png);
            background-size: 86%;
            background-repeat: no-repeat;
            cursor: pointer;
            float: right;
            width: 24px;
            height: 23px;
            margin: 2px -2px;
        }

        #legend {
            font-family: Arial, sans-serif;
            background: #fbff8a;
            padding: 13px 51px 13px 9px;
            margin: 4px;
            /* min-width: 120px; */
            /* min-height: 29px; */
            position: absolute;
            bottom: 127px;
            /* right: 0px; */
            font-weight: bold;
        }

        .showLeftPanel {
            cursor: pointer;
            float: left;
            z-index: 5000;
            top: 50%;
            font-size: 29px;
            position: absolute;
            padding: 2px 5px;
            border: 1px solid #dddddd;
            background-color: #dfdfdf;
        }

            .showLeftPanel:hover {
                background-color: #dcf2ff;
            }

        .gm-style-iw-d {
            overflow: hidden !important;
            white-space: nowrap !important;
            padding-right: 9px !important;
            padding-bottom: 17px !important;
        }
        .infoCheck{
            display: inline-block;
            position: absolute;
            background: none padding-box rgb(255, 255, 255);
            border: 0px;
            margin: 5px;
            padding: 10px 15px;
            text-transform: none;
            appearance: none;
            position: relative;
            cursor: pointer;
            user-select: none;
            direction: ltr;
            overflow: hidden;
            text-align: center;
            height: 25px;
            vertical-align: middle;
            color: rgb(0, 0, 0);
            font-family: Roboto, Arial, sans-serif;
            font-size: 18px;
            border-bottom-right-radius: 2px;
            border-top-right-radius: 2px;
            box-shadow: rgb(0 0 0 / 30%) 0px 1px 4px -1px;
            min-width: 66px;
            font-weight: 500;
        }
        .showInfo{
            height: 15px;
            width: 15px;
        }
        @media print
		{
			.no-print, .no-print *
			{
			display: none !important;
			}
		}
    </style>
    <script type="text/javascript">
        $(finalizePage);

        function initGoogleMap(callback) {
            mapZoom = 5;
            mapPosition = new google.maps.LatLng(42.345573, -71.098326);
            mapCenter = new google.maps.LatLng(35.56, -96.84);
            var mapOptions = {
                zoom: mapZoom,
                minZoom: 7,
                mapTypeId: google.maps.MapTypeId.HYBRID,
                center: mapCenter,
                gestureHandling: 'greedy'
            };
            map = new google.maps.Map(document.getElementById('gmap'), mapOptions);
            //console.log('map initialized');
        }

        function viewReport() {
            OpenInMap = false;
            $('.mapview').hide();
            $('.lnkMap').show();
            $('.lnkGrid').hide();
            $('.report-container').show();
            setPageSize();
        }
        function setPageSize() {
            var w = $(window).width()
            var h = $(window).height();
            formatPage(w, h);
        }
        var parcelsInMap = [];
        var parcelInfo = [];
        var OpenInMap = false;
        var infoWindows = [];
        var firstLoad = true;
        var bounds = new google.maps.LatLngBounds();

        function viewReportInMap() {
            var subjectId = $('#<%= comparableList.ClientID%>').val();
            var estimateSalePrice = $('#<%=hdnsalePrice.ClientID%>').val();
            var UniformityIndication = $('#<%= hdnUniformityValue.ClientID%>').val();
            var hideUniformity = $('#<%= hideUniformity.ClientID%>').val();

     OpenInMap = true;
     $('.mapview').show();
     $('.report-container').hide();
     if (!firstLoad) {
         $('.lnkGrid').show();
         $('.lnkMap').hide();
         if (map && bounds) {
             map.setCenter(bounds.getCenter());
             map.fitBounds(bounds);
             //map.setZoom(15);
             setPageSize();
             return;
         }
     }
     initGoogleMap();
     $('.lnkGrid').show();
     $('.lnkMap').hide();
     parcelInfo = JSON.parse($('#<%= infoData.ClientID%>').val());
            $.ajax({
                type: "POST",
                url: "/parcels/getcomparablesmappoints.jrq",
                dataType: 'json',
                data: {
                    pids: subjectId
                },
                success: function (res) {
                    if (res.length < 1)
                        return;
                    var sub = subjectId.split(',')[0];
                    parcelsIn = res;
                    var parcels = [];
                    var parcel;
                    // var infowindow = new google.maps.InfoWindow();
                    var icon = ["/App_Themes/Cloud/images/parcelUncheck.png",
                        "/App_Themes/Cloud/images/compParcel.png",
                        "/App_Themes/Cloud/images/subParcel.png"
                    ]
                    for (x in res) {
                        var point = res[x];
                        point.ParcelId.toString() == sub ? point.isSubject = '1' : point.isSubject = '0'
                        var pid = point.ParcelId.toString();
                        if (parcelsInMap[pid] == null)
                            parcelsInMap[pid] = {
                                ParcelId: point.ParcelId,
                                Keyvalue1: point.Keyvalue1,
                                addToCompare: false,
                                isSubject: point.isSubject,
                                Distance: 0.0,
                                Location: null,
                                infoSub: null,
                                Points: [],
                            };
                        parcelsInMap[pid].Points.push(point.Points);
                    }
                    var totalParcels = Object.keys(parcelsInMap).length;
                    var pinfo = [];
                    for (x in parcelsInMap) {
                        parcel = parcelsInMap[x];
                        var b = new google.maps.LatLngBounds();
                        pinfo = parcelInfo.filter(function (p) {
                            return p.Parcelid == parcelsInMap[x].ParcelId
                        });
                        parcel.infoSub = pinfo.length > 0 ? '</br><b>' + 'parcel_id : ' + '</b>' +  pinfo[0].keyvalue1 + '</br><b>' + 'Address : ' + '</b>' + (pinfo[0].AddressLine ? pinfo[0].AddressLine : '') + '</br><b>' + 'Sale Amount : ' + '</b>' + (pinfo[0].SaleAmount ? pinfo[0].SaleAmount : '') + '</br><b>' + 'Appraised Value : ' + '</b>' + (pinfo[0].AppraisedValue ? pinfo[0].AppraisedValue : '') : 'No Data Found';
                        for (i in parcel.Points) {
                            pstr = parcel.Points[i].split(',');
                            loc = new google.maps.LatLng(parseFloat(pstr[0].replace('[', '')), parseFloat(pstr[1].replace('[', '')));
                            if (parcel.isSubject != "1")
                                bounds.extend(loc);
                            b.extend(loc);
                        }

                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(b.getCenter().lat(), b.getCenter().lng()),
                            map: map,
                            icon: parcel.isSubject == '0' ? icon[1] : icon[2],
                            animation: google.maps.Animation.DROP
                        });
                        var info = new google.maps.InfoWindow();
                        info.setContent(parcel.infoSub.toString());
                        info.open(map, marker);
                        infoWindows.push(
                            {
                                infos : info,
                                marks : marker
                            }
                        )
                        google.maps.event.addListener(marker, 'click', (function (marker, parcel) {
                            return function () {
                                let activeInfo = false;
                                for (x in infoWindows) {
                                    if (infoWindows[x].infos !== info) {
                                        infoWindows[x].infos.close();
                                        if (!activeInfo) {
                                            $('.showInfo').prop("checked", false);
                                        }
                                    }
                                    else {
                                        info.close();
                                        info.setContent(parcel.infoSub.toString());
                                        info.open(map, marker);
                                    }
                                }
                                
                            }
                        })(marker, parcel));

                        google.maps.event.addListener(info,'closeclick',function(){
                            let activeInfo = false;
                            for(x in infoWindows){
                                with(infoWindows[x]){
                                    if (infos.getMap() != null)
                                        activeInfo = false;
                                }
                            }
                            if(!activeInfo){
                                $('.showInfo').prop("checked",false);
                            }
                        });

                    }
                    var legend = document.getElementById('legend');
                    //$('#mapView').append(legend);
                    var div = document.createElement('div');
                    legend.appendChild(div);
                    var innerValue = 'Estimate Sale Price:' + estimateSalePrice + ' <br />' + (hideUniformity == "False" ? 'Uniformity Value:' + UniformityIndication : '');
                    div.innerHTML = innerValue;
                    let infoButton = $('<div>').attr({
                        class : "infoCheck",
                        id : "info-check"
                    }).append(
                        '<span><input type="checkbox" class="showInfo" onClick = "toggleInfo(this);" id="check-labels" checked>Show All Labels</span>'
                    )
                    map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(legend);
                    map.controls[google.maps.ControlPosition.RIGHT_TOP].push($(infoButton)[0]);
                    map.setCenter(bounds.getCenter());
                    map.fitBounds(bounds);
                    //map.setZoom(15);
                    firstLoad = false;
                    setPageSize();
                },
                error: function (response) {
                    var test = response;
                }
            });

        }

        function toggleInfo(elem){
            let infoChecked = $(elem).prop("checked");
            for(x in infoWindows){
                with(infoWindows[x]){
                    if(infoChecked){
                        infos.open(map,marks);
                    }
                    else{
                        infos.close();
                    } 
                }
            }
        }

        function finalizePage() {
            var timer;
            $(".imgover").mouseenter(function () {
                var that = this;
                timer = setTimeout(function () {
                    $('.img2', that).addClass('hoverimg')

                }, 500);
            }).mouseleave(function () {
                $('.img2', this).removeClass('hoverimg')
                clearTimeout(timer);
            });
            $('.hideSearchPanel').show();
            $('.showLeftPanel').hide();
            $('.showLeftPanel').click(function () {
                getLeftScreen();
            });
            $('.hideSearchPanel').click(function () {
                hideLeftPanel();
            });
        }

        function setPageDimensions(cHeight) {
            $('.report-container').height(cHeight - 55);
            $('.report-container').width($('.main-content-area').width());

            //$('#gmap').width($(window).width() - $('.left-panel')[0].offsetWidth);
            //$('#gmap').height(cHeight - 65);
        }

        function openlink(PID, hasRole) {
            if (hasRole == 'False')
                alert("You do not have the user role to access the Quality Control module. Please speak with your CAMA Cloud administrator on access to the Quality Control module.")
            else
                window.open(window.location.origin + '/protected/quality/#/parcel/' + PID, '_blank', 'noopener,noreferrer');
        }
        function formatString(formatter, value) {
            if (!formatter || !value) return value;
            var s = value.toString(), r = '';
            for (var i = 0, j = 0; j < s.length; i++) {
                r += (formatter.charAt(i) == '#' || formatter.charAt(i) == '') ? s.charAt(j++) : formatter.charAt(i);
            }
            return r;
        }
        function openQpubliclink(keyvalue) {
            var organizationName ="<%=organizationName%>";
    var stateName ="<%=stateName%>";
    var qFieldFormatter = "<%=qFieldFormatter%>";
            var keyField = (qFieldFormatter && qFieldFormatter.trim() != "") ? formatField(qFieldFormatter, keyvalue) : keyvalue;
            var url = "https://qpublic.schneidercorp.com/application.aspx?app=" + organizationName + 'County' + stateName + "&layer=Parcels&PageType=Report&KeyValue=" + encodeURIComponent(keyField);
            //var url ="https://qpublic.schneidercorp.com/application.aspx?app=GilmerCountyGA&layer=Parcels&PageType=Report&KeyValue=1081%20%20%20080"
            window.open(url, '_blank');
        }

        function exportToExcel() {
            //			var exptable = document.createElement('table');
            //			$(exptable).html($('#report-view').html());
            //			$(exptable).prepend('<caption><div style="font-size:18pt">' + $('.report-title').html() + '</div></caption>');
            var orgName ="<%=organizationName%>";
            var wrapper = document.createElement('div');
            $(wrapper).html($('.report-container').html());
            $('.photo-row', wrapper).html('');
            $('.photo-row', wrapper).replaceWith('');
            var lnk = document.createElement('a');
            lnk.href = "data:application/vnd.ms-excel;charset=utf-8," + escape($(wrapper).html());
            lnk.download = orgName + '-Comparability-Report.xls';
            lnk.click();
        }
        
        function RemoveCompfromReport(pId){
        	showMask();
        	var queryParams = new URLSearchParams(window.location.search)
        	var comps  = queryParams.get("c")
        	var arr = comps.split(",")
        	var index = arr.indexOf(pId.toString())
        	if(index != -1)
        		arr.splice(index,1)
        	comps = arr.join(",");	
        	queryParams.set("c",comps);
        	history.replaceState(null,null,"?"+queryParams.toString());
        	location.reload();
        }
        function printpage() {
            $('.reportTable').css('width', '100%');
            if (OpenInMap) {
                var body = $('body'),
                    mapContainer = $('#gmap'),
                    mapContainerParent = mapContainer.parent(),
                    printContainer = $('<div>');
                body.prepend(printContainer);
                $("#gmap").css("width", '2000px')
                $("#gmap").css("height", '100%')
                printContainer.addClass('print-container').css('position', 'relative').height(mapContainer.height() + 200).append(mapContainer);
                var content = body.children().not('script').not(printContainer).detach();
                window.print();
                body.prepend(content);
                mapContainerParent.prepend(mapContainer);
                printContainer.remove();
                if (maximized) {
                    var w = $(window).width()
                    var h = $(window).height();
                    formatPage(w, h);
                }
                else {
                    $("#gmap").css("width", '75%');
                    $("#gmap").css("height", '80%');
                }
            } else {
                window.print();
                $('.reportTable').css('width', 'auto');
            }
        }
        var maximized = false;
        function hideSearchScreen() {
            var w = $(window).width()
            var h = $(window).height();
            $('.left-panel').hide();
            $('.hideSidePanel').hide();
            $('.showSidePanel').show();
            maximized = true;
            formatPage(w, h);
        }

        function formatPage(w, h) {
            var ch = h - $('#topNavBar').height() - $('#footer').height() - 12;
            var cw = w - $('.left-panel')[0].offsetWidth;
            //var cntHeight=$('#contentTable').height()
            $('.main-content-area').height(ch - 5);
            $('.main-content-area').width(cw - 14);
            $('.report-container').height(ch - 65);
            $('.report-container').width($('.main-content-area').width());
            if (maximized) {
                $('#gmap').width(cw - 26);
                $('#gmap').height(ch - 67);
            }
            else {
                $("#gmap").css("width", '75%');
                $("#gmap").css("height", '80%');
            }
        }

        function getSearchScreen() {
            var w = $(window).width()
            var h = $(window).height();
            $('.hideSidePanel').show();
            $('.left-panel').show();
            $('.showSidePanel').hide();
            maximized = false;
            formatPage(w, h);
            $("#gmap").css("width", '75%');
            $("#gmap").css("height", '80%');
            setScreenDimensions();
        }
        $( document ).ready(function() {
	        var w = $(window).width()
	        var h = $(window).height();
			formatPage(w, h);
			var queryParams = new URLSearchParams(window.location.search)
        	var comps  = queryParams.get("c")
        	var arr = comps.split(",")
        	if(arr.length > 5)
        		$('.close-property').show();
        	else
				$('.close-property').hide()        	
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h1>Comparability Report View</h1>
    <asp:Panel runat="server" ID="pnlResult">
<div class="actions">
        <asp:LinkButton ID="Lb_go_back" runat="server">Go Back</asp:LinkButton><asp:LinkButton ID="lblExport" runat="server">Export to Excel</asp:LinkButton><a onclick="printpage();return false;"
            href="#">Print</a> <a class="lnkMap" onclick="viewReportInMap()" href="#">Report In Map</a><a class="lnkGrid" style="display: none" onclick="viewReport()" href="#"> Report In Grid</a>
    </div>
    <div class="report-container">
        <style type="text/css">
            .SalesField .val-col, .SalesField .label {
                background-color: #d0d06e !important;
            }

            .catego {
                width: 60px;
                padding-right: 3px;
                padding-left: 3px;
                text-align: center;
                border: 1px solid #CFCFCF;
                border-right: 1px solid #333;
                border-left: 1px solid #333;
                border-top: 0px none;
                font-weight: bold;
                color: Black;
            }

            .categ-even {
                background: #EFEFEF;
            }

            .label {
                width: 135px;
                text-align: right;
                padding-right: 15px;
                border: 1px solid #CFCFCF;
                border-right: 1px solid #333;
                border-top: 0px none;
            }

            .qPublic {
                font-weight: bold;
                color: #167ccf;
            }

            .val-col {
                width: 75px;
                text-align: center;
                border: 1px solid #CFCFCF;
                border-top: 0px none;
                padding: 2px;
            }

            .val-subject {
                border-right: 1px solid #333;
                width: 140px;
            }

            .val-comp {
            }

            .val-adj {
                border-right: 1px solid #333;
                width: 60px;
                color: #ABABAB;
            }

            .val-no-adj {
                border-right: 1px solid #333;
            }

            .categ-first-row td {
                border-top: 2px solid #9999;
            }

            .no-border {
                border: 0px none !important;
                background: none !important;
                visibility: hidden;
            }

            .header-row th {
                padding: 3px;
                font-size: 1.2em;
                text-align: center;
                border: 1px solid #777;
                background: #DFDFDF;
            }

            .footer-row td {
                font-size: 1.2em;
                background: Yellow;
                font-weight: bold;
                border: 1px solid #333;
            }

            .ind-uni {
                background: Green !important;
                color: White;
            }

            .ind-nonuni {
                background: Red !important;
                color: White;
            }

            .ind-insuff {
                background: Yellow !important;
                color: Black;
            }

            .high {
                color: Green;
                font-weight: bold;
            }

            .low {
                color: Red;
                font-weight: bold;
            }

            .imgover {
                float: left;
                position: relative;
            }

                .imgover .img2 {
                    display: none;
                }

            .hoverimg {
                display: block !important;
                position: absolute;
                top: -33px;
                left: -75px;
                z-index: 100;
            }
        </style>
        <style type="text/css" media="print">
            .actions {
                display: none;
            }

            td {
                color: Black;
            }

            .report-container {
                height: auto !important;
                width: auto;
                overflow: visible !important;
            }
        </style>
        <asp:HiddenField runat="server" ID="infoData" Value="" />
        <asp:HiddenField runat="server" ID="comparableList" Value="" />
        <table style="border-spacing: 0px; border-collapse: collapse; font-size: 0.9em;" class='reportTable'>
            <tr class='header-row'>
                <th colspan="2" class='<%=UniformityStyle %>'>
                    <asp:Label runat="server" ID="lblUniInd" />
                </th>
                <th class='<%=UniformityStyle %>'>Subject
                </th>
                <%= EvalCompHeaders %>
                <th class="no-border"></th>
            </tr>
            <tr class="photo-row">
                <td colspan="2" class="label"></td>
                <%=EvalSubjectPhoto() %>
                <%=EvalCompPhotos() %>
            </tr>
            <tr class="qPublic" style="<%=EnabledqPublic()%>">
                <td colspan="2" class="label"></td>
                <%= EvalQPublicLinkForSubject()%>
                <%= EvalQPublicLinkForComps()%>
            </tr>
            <asp:Repeater runat="server" ID="report">
                <ItemTemplate>
                    <tr class='<%# EvalCategoryRow() %>'>
                        <%# EvalCategoryColumn()%>
                        <td class="label">
                            <%# Eval("Label")%>
                        </td>
                        <td class="val-col val-subject" style="text-align: center;">
                            <%# Eval("Subject")%>
                        </td>
                        <%# EvalCompColumns %>
                        <td class="no-border">&nbsp;</td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            <asp:Repeater runat="server" ID="footer">
                <ItemTemplate>
                    <tr class="footer-row">
                        <td colspan="2" class='label <%# IstipEnabled(Eval("Label")) %>'>
                            <%# Eval("Label")%>
                        </td>
                        <td class="val-col foot-col foot-subject">
                            <%# Eval("Subject")%>
                        </td>
                        <%# EvalFooterColumns %>
                        <td class="no-border">&nbsp;</td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            <asp:PlaceHolder runat="server" runat="server" ID="uifooter">
                <tr class="footer-row">

                    <td colspan="2" class='label <%=UniformityStyle() %>'>Uniformity Indication </td>
                    <td class="val-col foot-col foot-subject <%=UniformityStyle()%> " style="text-align: center;">
                        <% = UniformityIndication%>
                    </td>
                </tr>
            </asp:PlaceHolder>
        </table>
        <div id="div_Sale_price" runat="server" visible="false" style="font-size: 17px; padding-top: 10px">
            <asp:Label ID="lbl_sl_label" runat="server" Text=" Subject Property Estimated Sale Price"></asp:Label>
            <asp:Label ID="lbl_salePrice" runat="server" Text="Label" Font-Bold="true"></asp:Label>
        </div>
        <p>
            &nbsp;
        </p>
        <asp:HiddenField ID="hid_search_params" runat="server" />
        <asp:HiddenField ID="hdnUniformityValue" runat="server" />
        <asp:HiddenField ID="hdnsalePrice" runat="server" />
        <asp:HiddenField ID="hideUniformity" runat="server" />
    </div>
    <div class="mapview" id="mapView" runat="server" style="margin-top: 20px; display: none;">
        <div id="legend" class="legend"></div>
        <div class="gmap" id="gmap" style="height: 80%; width: 75%; position: absolute; overflow: hidden;">
        </div>
    </div>
    <div class="showLeftPanel" style="display: none">&#10095; </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlError">
        <pre runat="server" id="errorMessage" style="white-space:pre-wrap"></pre>
    </asp:Panel>
</asp:Content>
