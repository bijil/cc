﻿Imports CAMACloud.BusinessLogic

Partial Class cse_rules
	Inherits System.Web.UI.Page

    'Dim keyWords As String() = New String() {"CASE", "WHEN", "THEN", "ELSE", "END", "COALESCE", "ROUND", "ABS", "SGN"}

	Enum ViewMode
		List
		Edit
	End Enum

	Sub SwitchMode(mode As ViewMode)
		listPanel.Visible = mode = ViewMode.List
		editPanel.Visible = mode = ViewMode.Edit
	End Sub


#Region "List Panel Actions"
	Sub LoadRulesList()
		gvRules.DataSource = Database.Tenant.GetDataTable("SELECT * FROM cse.Rules WHERE IsGroupRule = " + ddlGroup.SelectedValue + " ORDER BY CreatedDate")
		gvRules.DataBind()
	End Sub

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
		If Not IsPostBack Then
			LoadRulesList()
			SwitchMode(ViewMode.List)

            ddlSourceTable.FillFromSql("SELECT Name FROM DataSourceTable WHERE ImportType IN (0,1) ORDER BY Name", True, "[Default Parcel Table]")
			ddlLookupTable.FillFromSql("SELECT * FROM cc_LookupSources", True)
		End If
	End Sub

	Protected Sub ddlGroup_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
		If ddlGroup.SelectedValue = 1 Then
			lbCopyToGroup.Visible = False
		Else 
			lbCopyToGroup.Visible = True
		End If
		LoadRulesList()
	End Sub

	Protected Sub gvRules_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvRules.RowCommand
		Select Case e.CommandName
			Case "EditRule"
				EditRule(e.CommandArgument)
			Case "DeleteRule"
				Database.Tenant.Execute("DELETE FROM cse.Rules WHERE Id = " + e.CommandArgument.ToString)
				LoadRulesList()
		End Select
	End Sub

	Protected Sub lbCreateRule_Click(sender As Object, e As System.EventArgs) Handles lbCreateRule.Click
		ClearForm()
		SwitchMode(ViewMode.Edit)
	End Sub
	
	Protected Sub lbCopyToGroup_Click(sender As Object, e As System.EventArgs) Handles lbCopyToGroup.Click
		Dim InsertQuery As String = "INSERT INTO cse.Rules"
		Dim SelectQuery As String = ""
		Dim SelectFields As String = ""
		Dim InsertFields As String = ""
		
		Database.Tenant.Execute("DELETE FROM cse.Rules WHERE IsGroupRule = 1")
		For Each gr As DataRow In Database.Tenant.GetDataTable("select sc.name from sys.Columns sc inner join sys.Tables st on st.object_id = sc.object_id where st. name like  '%Rules%'").Rows
			Dim Column = gr.GetString("name")
			If Column <> "Id" Then
				If Column = "IsGroupRule" Then
					SelectFields = SelectFields + "1," 
				Else
					SelectFields = SelectFields +  Column + ","
				End If
				InsertFields =InsertFields + Column + ","
			End If
		Next
	    SelectFields = SelectFields.TrimEnd(CChar(","))
	    InsertFields = InsertFields.TrimEnd(CChar(","))
	    Dim Query = InsertQuery + "(" + InsertFields +") SELECT " + SelectFields + " FROM cse.Rules"
	    Database.Tenant.Execute(Query)

	End Sub
	
	Public Function EvalRuleDependencies() As String
		If Eval("UseLookup") Then
			Return "Lookup, " + Eval("LookupName")
		End If
		If Eval("UseRangeLookup") Then
			Return "Range, " + Eval("LookupName")
		End If
		Return ""
	End Function

    Public Function EvalAggregate() As String
        Dim o = Eval("AggregateFunction")
        If o Is DBNull.Value Then
            Return ""
        End If
        Select Case o
            Case "AVG"
                Return "Average"
            Case "SUM"
                Return "Sum"
            Case "MIN"
                Return "Minimum"
            Case "MAX"
                Return "Maximum"
            Case Else
                Return o
        End Select
    End Function

    Public Function EvalFieldValue(field As String) As String
        Try
            Return Eval(field)
        Catch ex As Exception
            Return ""
            'Throw New Exception("Error on evaluating " + field + " - " + ex.Message)
        End Try
    End Function

    Public Function EvalFields() As String
		Dim fields As New List(Of String)
        Dim formulas() As String = New String() {EvalFieldValue("FieldSpec"), EvalFieldValue("RankFormula"), EvalFieldValue("RuleFormula"), EvalFieldValue("PercentFormulaIfPass"), EvalFieldValue("PercentFormulaIfFail")}
        For Each formula In formulas
            Dim matches = Regex.Matches(formula, "([A-Za-z][A-Za-z0-9_]+(?![\(\w]))([\s\+\),\-\*\/])?", RegexOptions.IgnoreCase)
            ''[A-Za-z][A-Za-z0-9_]+(?![\(\w])([\s\+\),\-\*\/])
            ''[A-Za-z][A-Za-z0-9_]+(?![\(\w])([\s\+\),\-\*\/])
            For Each m As Match In matches
				For Each g As Group In m.Groups
					For Each c As Capture In g.Captures

					Next
				Next
				Dim field As String = m.Groups(1).Value
                If Not fields.Contains(field) AndAlso Not SalesComparisonRule.ExpressionKeywords.Contains(field.ToUpper) Then fields.Add(field)
			Next

            Return IIf(EvalFieldValue("SourceTable") IsNot Nothing, "[" + EvalFieldValue("SourceTable") + "] ", "") + String.Join(", ", fields.ToArray)
        Next
		Return ""
	End Function

#End Region


#Region "Edit Panel Actions"

	Sub ClearForm()
		lblRuleType.Text = ddlGroup.SelectedItem.Text
		editTitle.InnerHtml = "Rule Specifications - New Rule"
		hdnRuleId.Value = ""
		txtName.Text = ""
        txtField.Text = ""
        ddlSourceTable.SelectedIndex = 0
        ddlAggregate.SelectedIndex = 0
		txtRuleFormula.Text = ""
		txtRankFormula.Text = ""
		txtPercentPass.Text = ""
		txtPercentFail.Text = ""
		hdnUseLookup.Value = "0"
		hdnUseRangeLookup.Value = "0"
		rblLookup.SelectedValue = LookupType
		ddlLookupTable.SelectedValue = ""
        hdnLookupName.Value = ""
        chkUseZeroIfNull.Checked = False
	End Sub

	Sub EditRule(id As Integer)
		ClearForm()
		Dim rule As New SalesComparisonRule(id)
		If rule.ID <> -1 Then
			editTitle.InnerHtml = "Rule Specifications - <span class='rulename'>" + rule.Name + "</span>"
			hdnRuleId.Value = id
            txtName.Text = rule.Name
            ddlSourceTable.SelectedValue = rule.SourceTable
            txtField.Text = rule.FieldSpecification
            ddlAggregate.SelectedValue = rule.AggregateFunction
			txtRuleFormula.Text = rule.RuleFormula
			txtRankFormula.Text = rule.RankFormula
			txtPercentPass.Text = rule.PercentFormulaIfPass
			txtPercentFail.Text = rule.PercentFormulaIfFail
			hdnUseLookup.Value = rule.UseLookup.GetHashCode
			hdnUseRangeLookup.Value = rule.UseRangeLookup.GetHashCode
			hdnLookupName.Value = rule.LookupName
            chkUseZeroIfNull.Checked = rule.UseZeroIfNull

			rblLookup.SelectedValue = LookupType
			LoadLookupSource()
		End If
		SwitchMode(ViewMode.Edit)
	End Sub


	Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click, btnSave2.Click
		Dim rule As SalesComparisonRule
		Dim ErrorOnFirstSave As Integer = 0
		If hdnRuleId.Value = "" Then
			rule = New SalesComparisonRule(txtName.Text, ddlGroup.SelectedValue = "1")
			hdnRuleId.Value = rule.CreateRule(Page.UserName)
			ErrorOnFirstSave =1
		Else
			rule = New SalesComparisonRule(hdnRuleId.Value)
		End If

        rule.Name = txtName.Text
        rule.SourceTable = ddlSourceTable.SelectedValue
        rule.FieldSpecification = txtField.Text
        rule.AggregateFunction = ddlAggregate.SelectedValue
		rule.RankFormula = txtRankFormula.Text
		rule.RuleFormula = txtRuleFormula.Text
		rule.PercentFormulaIfPass = txtPercentPass.Text
		rule.PercentFormulaIfFail = txtPercentFail.Text
		rule.UseLookup = hdnUseLookup.Value = "1"
		rule.UseRangeLookup = hdnUseRangeLookup.Value = "1"
		rule.LookupName = ddlLookupTable.SelectedValue
        rule.UseZeroIfNull = chkUseZeroIfNull.Checked
        Try
            rule.SaveRule(UserName)

            e_("DELETE FROM cse.ComparableCache")
        Catch ex As Exception
        	Alert(ex.Message)
        	If ErrorOnFirstSave =  1 Then
        		Database.Tenant.Execute("DELETE FROM cse.Rules WHERE Id = " + hdnRuleId.Value.ToString)
        	End If
        End Try
    End Sub

	Protected Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click, btnCancel2.Click
		ClearForm()
		SwitchMode(ViewMode.List)
		LoadRulesList()
	End Sub


#End Region

	Protected Sub rblLookup_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rblLookup.SelectedIndexChanged
		LookupType = rblLookup.SelectedValue
	End Sub

	Public Property LookupType As Integer
		Get
			trLookupTable.Visible = False
			If hdnUseLookup.Value = "0" And hdnUseRangeLookup.Value = "0" Then
				Return 0
			ElseIf hdnUseLookup.Value = "1" Then
				trLookupTable.Visible = True
				Return 1
			Else
				trLookupTable.Visible = True
				Return 2
			End If
		End Get
		Set(value As Integer)
			trLookupTable.Visible = False
			If value = 1 Then
				trLookupTable.Visible = True
				hdnUseLookup.Value = "1"
				hdnUseRangeLookup.Value = "0"
			ElseIf value = 2 Then
				trLookupTable.Visible = True
				hdnUseLookup.Value = "0"
				hdnUseRangeLookup.Value = "1"
			Else
				hdnUseLookup.Value = "0"
				hdnUseRangeLookup.Value = "0"
			End If
			LoadLookupSource()
		End Set
	End Property

	Sub LoadLookupSource()
		
		If hdnUseLookup.Value = "1" Then
			Try
				ddlLookupTable.FillFromSql("SELECT Name FROM cc_LookupSources", True, , hdnLookupName.Value)
			Catch ex As Exception
				Throw New Exception(hdnLookupName.Value, ex)
			End Try


		End If
		If hdnUseRangeLookup.Value = "1" Then
			ddlLookupTable.FillFromSql("SELECT DISTINCT GroupName FROM RangeLookup", True, , hdnLookupName.Value)
		End If


	End Sub

	Protected Sub lbShowHide_Click(sender As Object, e As System.EventArgs) Handles lbShowHide.Click
		lrHider.Visible = Not lrHider.Visible
		lbShowHide.Text = IIf(lrHider.Visible, "Show info messages", "Hide info messages")
	End Sub

    Protected Sub lbRecompile_Click(sender As Object, e As System.EventArgs) Handles lbRecompile.Click
        Try
            ComparableSalesEngine.ReCompileAllRules()
            e_("DELETE FROM cse.ComparableCache")
            Alert("All rules compiled successfully.")
        Catch ex As Exception
            Alert(ex.Message)
        End Try

    End Sub
End Class
