﻿Imports System.Text.RegularExpressions
Imports System.Web.Script.Services
Imports System.Web.Services
Public Class qgtest
	Inherits System.Web.UI.Page
	Public Event RunCommand(pageCommand As String)
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then

        End If

        If Page.Request.CurrentExecutionFilePath <> Page.Request.Url.AbsolutePath Then
            Dim commandName = Page.Request.Url.AbsolutePath.Replace(Page.Request.CurrentExecutionFilePath, "").TrimStart("/")
            RaiseEvent RunCommand(commandName)
        End If

    End Sub	

    Public Sub Page_PageCommand(commandName As String) Handles Me.RunCommand
        If commandName = "exportParcels" Then
             exportParcels()
        End If
    End Sub

	Public Function exportParcels()
		Dim exportGrid As New GridView
        Page.Controls.Add(exportGrid)
        Dim resultId = Request("s")
        Dim query As String = Database.Tenant.GetStringValue("select CONCAT(SelectQuery,JoinQuery,WhereQuery) As FullQuery FROM QueryBuilderFilters where ID = " + resultId)
        Dim data = Database.Tenant.GetDataTable(query)
        exportGrid.AllowPaging = False
        exportGrid.AutoGenerateColumns = False
        Dim bfieldName As String
        Dim bHeaderText As String
        exportGrid.Columns.Clear()
        For Each column As DataColumn In data.Columns
            bfieldName = column.ColumnName
            bHeaderText = column.ColumnName
            Dim bfield As New BoundField()
            bfield.HeaderText = bHeaderText
            bfield.DataField = bfieldName
            exportGrid.Columns.Add(bfield)
        Next
        exportGrid.DataSource = Nothing
        exportGrid.DataSource = data
        exportGrid.DataBind()
        Dim row as DataRow= Database.Tenant.GetTopRow("SELECT * FROM QueryBuilderFilters WHERE Id = "  & resultId)
        Dim FileName As String = row.GetString("Name").ToLower() + " filter result parcels.xlsx"
        Response.Clear()
        Response.Buffer = True
        Response.ClearContent()
        Response.ClearHeaders()
        Response.ContentType = "application/vnd.openxml.formats-officedocument.spreadsheetml.sheet"
        Response.AddHeader("Content-Disposition", "attachment;filename=""" & FileName & "")
        exportGrid.ControlStyle.Width = 130
        Dim excelStream = ExcelGenerator.ExportGrid(exportGrid, data, FileName ,FileName)
        excelStream.CopyTo(Response.OutputStream)
        Response.End()

	End Function
	
End Class