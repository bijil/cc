﻿ <%@ Page Title="" Language="VB" MasterPageFile="~/App_MasterPages/CAMACloud.master" AutoEventWireup="false" Inherits="CAMACloud.Console.reporting_Default" Codebehind="Default.aspx.vb"  ValidateRequest="false" %>
<%@ PreviousPageType VirtualPath="~/protected/quality/Default.aspx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
   <script type="text/javascript">
       var rlayout;
       var sht;
       var opt = 0;
       var validDate = true;
       $(window).resize(function() {
    		horizontalScroll();
		});
	    function horizontalScroll(){
	    if($('#ctl00_MainContent_viewer_ctl09').length){
	   	if(($('#ctl00_MainContent_viewer_ctl09')[0].scrollWidth > $('#ctl00_MainContent_viewer_ctl09')[0].clientWidth))
   	 	$('#ctl00_MainContent_viewer_ctl09').css({'overflow':'scroll'});
 		else 
		$('#ctl00_MainContent_viewer_ctl09').css({'overflow':'auto'});
		}
		$('#ctl00_MainContent_viewer_ctl09').off().scroll(function (e){
		let top = $(this).scrollTop();
		if(top>5) $('#ctl00_MainContent_viewer_ctl09 table~div').show()
		else $('#ctl00_MainContent_viewer_ctl09 table~div').hide()
		$('#ctl00_MainContent_viewer_ctl09 table~div').css('top',top)
		})
		}
       function hideSearchScreen() {
		    var w = $(window).width()
		    var h = $(window).height();
		    var vsble = $('.divBack').is(':visible');
		    $('.left-panel').hide();
		    $('.hideSidePanel').hide();
		    $('.left-panel2').show();
		    $('.showSidePanel').show();		    
			if (vsble){
                $('.divBack').css("margin-left","23%"); 
            }
            var scroll = $('#ctl00_MainContent_viewer_ctl10').scrollTop();
            $('#ctl00_MainContent_viewer_ctl10').scrollTop(scroll + 1);     
			setScreenDimensions();  
		}
		function getSearchScreen() {;
		    var w = $(window).width()
		    var h = $(window).height();
		    var vsble = $('.divBack').is(':visible');
		    $('.showSidePanel').hide();
		    $('.left-panel2').hide();
		    $('.hideSidePanel').show();
		    $('.left-panel').show();
		    if (vsble){
                $('.divBack').css("margin-left",""); 
            }
            var scroll = $('#ctl00_MainContent_viewer_ctl10').scrollTop();
            $('#ctl00_MainContent_viewer_ctl10').scrollTop(scroll + 1);
            setScreenDimensions();
		}
       function setPageLayout(w, h) {
           sht = h;
           setTimeout('setReportHeight()', 500);
           $('.report-container-cell').height(h - $('.page-title').height());
           if (opt == 0) {
               $("[id*='Menu']").each(function (i, el) {
                   var el_id = '#' + el.id;
                  // $('#MainContent_sendmail').append('<button type="button" onclick="return sendReportToProcessor();" >Send Report To Mail</button>');
               });
               opt = 1;
           }
           else {
               return false
           }
       }
       function sendReportToProcessor() {
           document.getElementById('<%= btnsendRptProcessor.ClientID%>').click();
       }

        function setReportHeight() {
            rlayout = $('#ctl00_MainContent_viewer_fixedTable > tbody > tr')[4];
            $(rlayout).attr('class', 'report-view-row');
            var h = sht;
            var rh = h - $('.page-title').height() - 29 - 11 - 30;
            $('.report-view-row > td').height(rh);
            horizontalScroll();
        }

        function openReport(rid) {
            $('#ddlReport').val(rid);
            localStorage.setItem($('#accordion').attr('name') + '-accordion', 0);
            $('#btnRefresh').click();
        }

        function DisableKeyPress() {
            return false;
        }
        function dateValidate(dateString) {
            var label = document.getElementById('<%=Label1.ClientID%>');
            var dateOf = "#" + dateString.id;
            var dateVal = $(dateOf).val();
            var validat = true;
            //var pattern = '^((18)|(19)|(20))[0-9]{2}([-/.])((0|1))([1-9])([-/.])([0-3][0-9])$';
            //var pattern=/^((18)|(19)|(20))[0-9]{2}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/;
            var pattern=/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/;
            var dtRegex = new RegExp(pattern);

            if (dateString.validity.valid==true || dtRegex.test(dateVal)){
            $("input:[type='date']").each(function(){
               
                if (!this.validity.valid || (!dtRegex.test($(this).val()) && $(this).val()!= ""))
                  {
                   validat = false;
                   }
               });
                if (validat == false)
                   {                   
                     label.innerText = "Invalid date format";
                     label.style["margin-left"] = "0%";
                     label.style["margin-bottom"] = "5px";
                     $('#email_val').remove();
                     validDate = false;
                      return false;
                    }
                else
                   {
                     label.innerText = "";
                     validDate = true;                  
                     return true;
                    
                   }
           
            }
            
            else if (dateString.validity.valid!=true && !dtRegex.test(dateVal)){
                label.innerText = "Invalid date format";
                validDate = false;
                
                return false;
            }
        }
        
        function _emailValidate(_eVal) {
        	$('#email_val').remove();
        }
        
        function GenerateMask() {
			var dtRegex = new RegExp(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/);
			if(validDate == "false" || validDate == false){
				return false;
			}
			
			$('.div_reportSelect input[type="date"]').each(function(){ 
				if (dtRegex.test($(this).val()) || $(this).val() == '') {  
					validDate = true
					}
				else {
					validDate = false; 
					return false;
				}
			});
			
			if( $('#LeftContent_report_filter_Email')[0] ) {
				var _emailValue = $('#LeftContent_report_filter_Email').val();
				if (_emailValue && _emailValue != '') {
					var _emailRegx = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
					var _emailValid = _emailRegx.test(_emailValue);
					if(!_emailValid) {
						$('#email_val').remove();
						$('.div_reportSelect').append('<span id="email_val" style="color: red; margin-left: 0%;">Invalid Email format</span>')
						$(".div_reportSelect").animate({ scrollTop: $(".div_reportSelect")[0].scrollHeight }, 10);
                     	return false;
					}
				}
			}
			
			if (Page_ClientValidate('reportquery')) {
			    var currentSelectedReport = $("#ddlReport option:selected").val();
			    if (currentSelectedReport == 9 || currentSelectedReport == 1 || currentSelectedReport == 10) {
			        $('.masklayer').height($(document).height());
			        $('.masklayer').show();
			    }
			    else {
			        $('.masklayer').hide();
			    }
			    return true;
			}
			
			Page_BlockSubmit = false;
			return false;   
        }

<%--        function exportReport(exportType) {
            if (exportType == 'Excel') 
                document.getElementById('<%= exportWithXLSX.ClientID%>').click();
            else if (exportType == 'Word') 
                document.getElementById('<%= exportWithXLSX.ClientID%>').click();
            else if (exportType == 'PDF') 
                document.getElementById('<%= exportWithXLSX.ClientID%>').click();
        }--%>

       $(document).ready(function () 
       {
        	
           //To xlsx           
           var totalrecords = $('#totalRecords').val();
                var el = $("a:contains('Excel')").get(0);
                var e2 = $("a:contains('PDF')").get(0);
                var e3 = $("a:contains('Word')").get(0);
            //if (totalrecords != undefined && totalrecords > 0 ) {
           //    el.removeAttribute("onclick");
          //    el.setAttribute("onclick", "exportReport('Excel');");
         //}
           if (totalrecords > 100000 )
            {

                if (el != undefined || el != null) 
                { 
                        $("a:contains('Excel')").get(0).removeAttribute("onclick");
                        $("a:contains('Excel')").click(function () {
                        alert('Please change the query filter or use another export format for this condition.\nNumber of rows exceeds the maximum possible rows(100000) per sheet in "Excel" format, Rows Requested : ' + $('#totalRecords').val());
                   });
                }
                if (e2 != undefined || e2 != null) 
                { 
	               	 $("a:contains('PDF')").get(0).removeAttribute("onclick");
	               	 $("a:contains('PDF')").click(function () {
	                    alert('Please change the query filter or use another export format for this condition.\nNumber of rows exceeds the maximum possible rows(100000) per sheet in "PDF" format, Rows Requested : ' + $('#totalRecords').val());
	               });
                }
                
           }
           if (totalrecords > 65536 && (e3 != undefined || e3 != null)) {
               $("a:contains('Word')").get(0).removeAttribute("onclick");
               $("a:contains('Word')").click(function () {
                   alert('Please change the query filter or use another export format for this condition.\nNumber of rows exceeds the maximum possible rows(65536) per sheet in "Word" format, Rows Requested : ' + $('#totalRecords').val());
               });
           }
            
            var url=window.location.href;
            var url_string=new URL(url);
            var querystring=String(url_string.searchParams.get("id"));
            if(querystring!="null")
            {
            $('.divBack').show();
            $('#btnBack').attr('onclick', 'window.location.href="/protected/quality/#/parcel/'+querystring+'"');
            }
            else
            {
            $('.divBack').hide();
            }
            
           //DTR Selection
           var rId = $('#ddlReport').val();
           $('#gv_DTRStatus' + rId+' input[type = checkbox]').change(function () {
                var text = $('#gv_DTRStatus' + rId + ' tbody .rowstyle').eq(1).html();
                var AllCheck = text;
                if (AllCheck == 'all' || AllCheck === 'All' || AllCheck == 'ALL') {    	// check if the checkbox is all
            	    if($(this).prop('checked') == false){
                        $("#LeftContent_gv_DTRStatus" + rId+"_ctl00_0").prop("checked", false);
				    }
                    var totaldtr = $('#gv_DTRStatus' + rId +' input[type = checkbox]').length;
                    var checkeddtr = $('#gv_DTRStatus' + rId +' input[type = checkbox]:checked').length;
                    if((totaldtr-1)==checkeddtr)
                    {
                       $("#LeftContent_gv_DTRStatus" + rId +"_ctl00_0").prop("checked", true);
                    }
                 }	  
             });

            $('#LeftContent_gv_DTRStatus' + rId + '_ctl00_0').click(function() {
                var text = $('#gv_DTRStatus' + rId + ' tbody .rowstyle').eq(1).html();
                var AllCheck = text;
                if (AllCheck == 'all' || AllCheck === 'All' || AllCheck == 'ALL') {
                    if ($('#LeftContent_gv_DTRStatus' + rId + '_ctl00_0').prop('checked') == true) {
                        $('#gv_DTRStatus' + rId + ' input[type="checkbox"]').prop('checked', true);
                        $('#gv_DTRStatus' + rId + ' input[type="checkbox"]').attr('readonly', true);
                        $('#LeftContent_gv_DTRStatus' + rId + '_ctl00_0').attr('readonly', false);
                    } else {
                        $('#gv_DTRStatus' + rId + ' input[type="checkbox"]').prop('checked', false);
                        $('#gv_DTRStatus' + rId + ' input[type="checkbox"]').attr('readonly', false);
                    }
                }
            });

            //Page Number
            $("#ctl00_MainContent_viewer_ctl05_ctl00_CurrentPage").keydown(function(e) {
            	var key = e.charCode || e.keyCode || 0;
            	return ( key == 8 || key == 9 || key == 13 || key == 46 || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105) );
            });
            $("#ctl00_MainContent_viewer_ctl05_ctl00_CurrentPage").on('input', function() {
                var inputValue = $(this).val();
                var numericValue = inputValue.replace(/\D/g, '');
                $(this).val(numericValue);
            });
           if ($('#LeftContent_gv_DTRStatus' + rId+'_ctl00_0').prop('checked') == true){
                var text = $('#gv_DTRStatus' + rId + ' tbody .rowstyle').eq(1).html();
                var AllCheck = text;
                if (AllCheck == 'all' || AllCheck === 'All' || AllCheck == 'ALL') {
                    $('#gv_DTRStatus' + rId +' input[type = checkbox]').prop('checked', true);
                    $('#gv_DTRStatus' + rId +' input[type = checkbox]').attr('readonly',true);
                    $('#LeftContent_gv_DTRStatus' + rId +'_ctl00_0').attr('readonly',false);
                }
           }  
        //LUC Selection         
           
           $('#gv_mClassificationId input[type = checkbox]').change(function () {
            	if($(this).prop('checked') == false){
    				$("#LeftContent_gv_mClassificationId_ctl00_0").prop("checked", false);
				}
				var total = $('#gv_mClassificationId input[type = checkbox]').length;
                var checked = $('#gv_mClassificationId input[type = checkbox]:checked').length;
                if((total-1)==checked)
                {  
                    $("#LeftContent_gv_mClassificationId_ctl00_0").prop("checked", true);
                }  
           });
             
           $('#LeftContent_gv_mClassificationId_ctl00_0').click(function () {
                if ($('#LeftContent_gv_mClassificationId_ctl00_0').prop('checked') == true ){
                    $('#gv_mClassificationId input[type = checkbox]').prop('checked', true);
                    $('#gv_mClassificationId input[type = checkbox]').attr('readonly',true);
                    $('#LeftContent_gv_mClassificationId_ctl00_0').attr('readonly',false);
                }
                else{
                    $('#gv_mClassificationId input[type = checkbox]').prop('checked', false);
                    $('#gv_mClassificationId input[type = checkbox]').attr('readonly',false);
                	}
            		});
           if ($('#LeftContent_gv_mClassificationId_ctl00_0').prop('checked') == true){
                    $('#gv_mClassificationId input[type = checkbox]').prop('checked', true);
                    $('#gv_mClassificationId input[type = checkbox]').attr('readonly',false);
                    $('#LeftContent_gv_mClassificationId_ctl00_0').attr('readonly',false);
                }  
       	 });

   </script>
    <link rel="stylesheet" href="/App_Static/css/reports.css?<%= Now.Ticks %>" />
    <style>
        .panel-spacer
        {
            display: none;
        }
        
        .report-filter-container
        {
            margin: 4px 0px;
        }
        .report-filter-container span
        {
            float: left;
    		margin-right: 103px;
    		margin-top: 1px;
        }
        .report-control-head
        {
            font-size: 11pt;
            font-weight: bold;
            border-bottom: 1px solid #999;
            margin-top: 15px;
            margin-bottom: 10px;
        }
        
        .report-button
        {
            padding: 8px 25px !important;
            font-weight: bold;
            margin-top: 10px;
        }
        
        .validation-error
        {
            font-size: 1.3em;
            vertical-align: middle;
        }
        
        .report-viewer td
        {
            vertical-align:middle;
        }
        .hidden
        {
            display:none;
        }
        .rowstyle {
            border-color: #c1c1c1 !important;
        }
        .masklayer{
                height: 100%;
                position: absolute;
                top: 0px;
                left: 0px;
                width: 100%;
                z-index: 100;
                opacity: 0.5;
                background-color: black;
        }
        .report-view-row{
                height: 470px !important;
        }
        .div_reportSelect{
               height: 430px !important;
               overflow-y: auto !important;
        }
         .div_reportSelect::-webkit-scrollbar {
            width: 5px;
        }
         .panelControls::-webkit-scrollbar{
            width: 5px;
         }
         .ui-accordion-content-active{
             padding-right: 0 !important;
         }
		 #btnBack{		 
 		   display: inline-block;
           float: right;
           cursor: pointer;
           font-size: 10pt;
           margin-left:80px;
           color:#1f1a1a;
		 }
        .mGrid {
            width : 100%;
        }
        .mGrid tbody tr td:first-child {
           
        }	 
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftContent" runat="Server">
    <div id="accordion" name="parcelmgr">
    <asp:PlaceHolder runat="server" ID="phBasicSettings">
    
        <h3>
            <a>Reports</a></h3> 
        <div>
            <div style="margin: 10px 0px; margin-top: 0px;">
                <div class="report-control-head">
                    Select Report:</div>
                <div>
                    <asp:Button runat="server" ID="btnRefresh" ClientIDMode="Static" Text="Refresh" style='display:none;'/>
                    <asp:DropDownList ClientIDMode="Static" runat="server" ID="ddlReport" Width="220px" AutoPostBack="true" name="ddlReport">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator runat="server" ID="rfv_ddlReport" ControlToValidate="ddlReport" ErrorMessage="*" SetFocusOnError="True" ValidationGroup="reportquery" />
                </div>
                 <div class="report-control-head" runat="server" id="lblReportParameters">
                        Report Parameters:</div>
                <asp:Panel runat="server" ID="reportParams" class="div_reportSelect">
                    <asp:HiddenField runat="server" ID="HasFilters" />
                    <asp:PlaceHolder runat="server" ID="ph"></asp:PlaceHolder> 
                   <br />                    
                </asp:Panel>
                <asp:Label ID="Label1" runat="server" Text="" ForeColor="Red"></asp:Label>
                <div>
                    <asp:Button runat="server" ID="btnGenerate" Text="Generate Report" CssClass="report-button" OnClientClick="javascript: return  dateValidate(this); GenerateMask();" OnClick="btnGenerate_Click" ValidationGroup="reportquery"  />
                </div>
            </div>
        </div>
        
    
    </asp:PlaceHolder>
    <%--<asp:PlaceHolder runat="server" ID="phBasic">
        
        <h3>
            <a>Offline Reports</a></h3> 
            <div>
                <ul class="group-menu">
                    <li>
                        <asp:HyperLink runat="server" ID="HplnkOfflineReport" Text="Report Status" NavigateUrl="~/protected/reports/OfflineReports.aspx" /></li>
                </ul>
        </div>
        
    </asp:PlaceHolder>--%>
   </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:Panel runat="server" ID="reportsHome">
        <h1 style="padding-left:2%;width:98%">Reports</h1>
        <div style="padding:15px;padding-top:0px;">
            <asp:Repeater runat="server" ID="rptGroups">
                <ItemTemplate>
                    <asp:HiddenField runat="server" ID="RID" Value='<%# Eval("Id") %>' />
                    <h2><%# Eval("GroupTitle") %></h2>
                    <div class="clear"></div>
                    <asp:Repeater runat="server" ID="rptReports">
                        <ItemTemplate>
                            <a class="fl report-item tip"  tip-margin-left="-100" max-width="300" abbr="<%# Eval("Description")%>" OnClick='openReport(<%# Eval("Id") %>)'>
                                 <div class="icon"></div>
                                          <span class="text"><%# Eval("Name")%></span>
                            </a>  
                                       
                        </ItemTemplate>
                    </asp:Repeater>
                    <div class="clear"></div> 
                </ItemTemplate>
            </asp:Repeater>
        </div>
       
    </asp:Panel>
     <div Id="Div1" style="display:none" class="export" runat="server">
         <%--<asp:Button ID="exportWithXLSX" runat="server" Text="" OnClick="exportWithXLSX_Click" />--%>
        <asp:Button ID="btnsendRptProcessor"  runat="server" onclick= "btnsendRptProcessor_Click" ClientIDMode="Static" />
     </div>
    
    <div class="report-viewer" id="reportViewerPanel" runat="server" visible="true">
        <div Id="sendmail" style="display:inline-block;padding-left:850px;" class="send" runat="server">
       
        </div>
        <asp:HiddenField ID="totalRecords" runat="server" ClientIDMode="Static"/>
        <div class="divBack" style="display:none; display:inline-block; position:absolute; margin-top:24px;">
			<a id="btnBack" title="Back">
                <asp:Button class="mybtn" Text="Back to parcel" runat="server" OnClientClick="JavaScript:window.history.back(1); return false;"></asp:Button>
            </a>
		</div>
        <reports:ReportViewer ID="viewer" runat="server" Width="100%" 
            ShowPrintButton="true" ShowZoomControl="true"
            CssClass="report-viewer" ExportContentDisposition="AlwaysAttachment" 
            KeepSessionAlive="True" 
            ClientIDMode="Static">
        </reports:ReportViewer>
    </div>
    <div id="customReportPanel" runat="server" style="width:100%; position:relative;" >
                 <cc:CustomReportViewer id="uc_CustomReport" runat="server"  />
     </div>  
     <asp:Panel runat="server" ID="pnl_nofile" Visible="false">
        <h1 style="padding-left:2%;width:98%">Reports</h1>
        <div style="padding:15px;padding-top:0px;">
             <div class="report-viewer">
                 <asp:Label runat="server" ID="lblnoFile"  Text="Sorry, it appears there is no data matching your criteria. Please adjust your search parameters and try again."></asp:Label>
             </div>
        </div>     
    </asp:Panel>
</asp:Content>


