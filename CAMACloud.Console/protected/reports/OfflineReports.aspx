﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/App_MasterPages/CAMACloud.master" CodeBehind="OfflineReports.aspx.vb" Inherits="CAMACloud.Console.OfflineReports" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
	.popupbox {
	    padding: 10px;
	    width: 250px;
	    height: 150px;
	    border: 2px solid rgba(204, 204, 204, 0.5);
	    background-color: white;
	    border-radius: 10px;
	    position: absolute;
	    box-shadow: 2px 2px 8px 2px gray;
	
	}
	.popupcontent {
	    overflow-y: auto;
	    max-height: 200px;
	    height: 80%;
	    float: right; 
	    margin-top: 12px;
	    position:inherit;
	
	}
	.close-btn {
	    float: right;
	    left: 18px;
	    position: relative;
	    top: -11px;
	    font-weight: bold;
	    font-size: larger;
	    cursor: pointer;
	    margin-bottom: 13px;
	    margin-right: 15px;
	
	}
	.close-btn:hover{
	    font-size:large;
	
	}
	.errorLink{
	    float:right; 
	    padding-top: 9px;
	
	}
	.break{
	   max-width:250px;
	   word-break : break-all;
	}
	.popupbox:after { 
	    content: '';
	    display: block;
	    position: absolute;
	    left: 100%;
	    margin-top: -3px;
	    border-top: 10px solid transparent;
	    border-bottom: 10px solid transparent;
	    border-left: 10px solid white;
	
	}
	.changed:after {
	    content: '';
	    display: block;
	    position: absolute;
	    left: 100%;
	    margin-top: 130px;
	    border-top: 10px solid transparent;
	    border-bottom: 10px solid transparent;
	    border-left: 10px solid white;
	
	}
    .label {
    display: inline;
    padding: .2em .6em .3em;
    font-size: 75%;
    font-weight: bold;
    line-height: 1;
    color: #ffffff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: .25em;
  }
    .label-success {
    background: #47a447;
    color: white;
 }
    .label-danger {
    background: #d2322d;
    color: white;
}
    .label-pending{
    background: #87cefa;
    color: white;
    } 
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            setInterval(function () {
                window.location.reload(true);
            }, 180000);
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftContent" runat="server">
    <div id="accordion" name="parcelmgr">
    <asp:PlaceHolder runat="server" ID="phBasicSettings">
    
        <h3>
            <a>Reports</a></h3> 
        <div>
            <ul class="group-menu">
                <li><asp:HyperLink runat="server" ID="HplnkOfflineReport" Text="Reports" NavigateUrl="~/protected/reports/Default.aspx" /></li>
            </ul>
        </div>
                        
        </asp:PlaceHolder>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Offline Reports</h1><br />
    <asp:Panel ID="pnlReports" runat="server">
        <asp:GridView ID="GrvReports" runat="server" AutoGenerateColumns="False" PageSize="15" AllowPaging="true" ShowHeaderWhenEmpty="false" Width="98%">
            <Columns>
                <asp:BoundField DataField = "RequestedBy"  HeaderText = "User" ItemStyle-Width="90px" />
                <asp:BoundField DataField = "CreatedDate" HeaderText = "Date" DataFormatString="{0:MM-dd-yyyy hh:mm tt}" ItemStyle-Width="190px"/>
                <asp:BoundField DataField = "Email" HeaderText = "Email Id" ItemStyle-CssClass = "break"/>
                <asp:BoundField DataField = "ReportName" HeaderText = "Report" ItemStyle-Width="250px" />
                <asp:TemplateField HeaderText = "Status" ItemStyle-Width="60px">
                    <ItemTemplate>
                        <%# If(Eval("Status") > 0, If(Eval("Status") = 1, "<span class='label label-success'> Success", "<span class='label label-danger'> Failed") + "</span>", "<span class='label label-pending'> Pending" + "</span>") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-Width="30px">
                    <ItemTemplate>
                    <asp:ImageButton ID="btnDownload" class="a16 set16" CommandName="Download" CommandArgument='<%# Eval("Id") %>'  runat="server" BorderStyle="None"  ImageUrl=  '<%# If(Eval("Status") > 0, If(Eval("Status") = 1, "~/App_Static/css/icon16/download.png", "~/App_Static/css/icons16/del16.gif"), "~/App_Static/css/icon16/process.gif")%>' ImageAlign="Bottom" ToolTip='<%# If(Eval("Status") > 0, If(Eval("Status") = 1, "Download", Eval("ErrorMessage")), "Processing Report")%>' style="outline: none;" OnClientClick ='<%# If(Eval("Status") = 1, "return true", "return false")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                    <p>Currently there are no offline reports available.</p>
            </EmptyDataTemplate>
        </asp:GridView>
    </asp:Panel>
</asp:Content>
