﻿Imports System.IO

Public Class DownloadReport
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim orgId As Integer = HttpContext.Current.GetCAMASession().OrganizationId
            Dim loginId As String = UserName()
            Dim dt As DataTable = Database.System.GetDataTable("SELECT Path FROM ReportJobs WHERE OrganizationID = " + orgId.ToString() + " AND RequestedBy ='" + loginId.ToString() + "'")
            Dim s3mgr As New S3FileManager
            For Each dr As DataRow In dt.Rows
                Try
                    If Not IsNothing(dr) Then
                        Dim path As String = dr("Path").ToString()
                        Dim fn As Array = path.Split("/")
                        Dim filename As String = fn(2).ToString()
                        Dim memoryStream As New MemoryStream
                        memoryStream = s3mgr.GetFile(path)
                        Dim success As Boolean = ExportToExcel(memoryStream, filename)
                        If success = True Then
                            's3mgr.DeleteFile(path)
                        End If
                    End If
                Catch ex As Exception
                    Throw ex
                End Try
            Next
        End If
    End Sub
    Private Function ExportToExcel(memoryStream As MemoryStream, filename As String)
        Try
            Response.Clear()
            Response.Buffer = True
            Response.ClearContent()
            Response.ClearHeaders()
            Response.AddHeader("content-disposition", "attachment;filename=" + filename + ".xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"
            memoryStream.WriteTo(Response.OutputStream)
            Response.Flush()
            Response.End()
            Return True
        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function
End Class