﻿Imports System.IO
Imports System.Text
Imports CAMACloud.BusinessLogic
Imports CAMACloud.RemoteServices
Imports System.Net
Imports System.Net.Sockets

Partial Class uploads_catch
	Inherits System.Web.UI.Page

	Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
		Response.Clear()
		_processRequest(HttpContext.Current)
	End Sub

	Public Sub _processRequest(context As HttpContext)
		Dim request As HttpRequest = context.Request
		Dim file As HttpPostedFile = request.Files(0)

		Dim filename As String = request("name")
		Dim uuid As String = request("uuid")
		Dim bstart As Integer = request("start")
		Dim bstop As Integer = request("stop")
		Dim progress As Integer = request("progress")

        Dim tempPath As String = ConfigurationManager.AppSettings("UploadTempRoot") + HttpContext.Current.GetCAMASession().TenantKey + "/" + uuid
		If tempPath.StartsWith("~/") Then
			tempPath = Server.MapPath(tempPath)
		End If
		If Not Directory.Exists(tempPath) Then
			Directory.CreateDirectory(tempPath)
		End If
		Dim filePath As String = Path.Combine(tempPath, filename)
		Using fs = New FileStream(filePath, IIf(bstart = 0, FileMode.Create, FileMode.Append))
			Dim buffer = New Byte(file.InputStream.Length - 1) {}
			file.InputStream.Read(buffer, 0, buffer.Length)
			fs.Write(buffer, 0, buffer.Length)
			fs.Close()
		End Using


		If progress = 100 Then
			Dim ext As String = IO.Path.GetExtension(filename).TrimStart(".").ToLower
            Dim s3Path As String = HttpContext.Current.GetCAMASession().TenantKey + "/datasource/" + filename

            Dim dsfid As Integer = DataSource.RegisterDataFile(filePath, s3Path, Membership.GetUser.UserName, request.ClientIPAddress)
			If ext = "mdb" Or ext = "accdb" Then
				DataSource.BuildDataStructureFromAccessDatabase(dsfid, filePath)
			Else

			End If

			If IO.File.Exists(Server.MapPath("~/$$server$$.info")) Then
				Dim srq As New ServiceRequest("UploadToS3")
				With srq
					.Add("OrganizationId", HttpContext.Current.GetCAMASession().OrganizationId)
					.Add("LocalPath", filePath)
					.Add("S3Path", s3Path)
					.Add("DeleteLocal", "true")
				End With
				Dim sc As New ServiceConnector(IPAddress.Loopback, 9000)
				Dim resp As ServiceResponse = sc.SendRequest(srq)
				If Not resp.Success Then

				End If
			End If
		End If
		context.Response.ContentType = "text/plain"
		context.Response.Write("OK")
	End Sub
End Class
