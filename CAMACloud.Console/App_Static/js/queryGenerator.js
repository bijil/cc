const queryGenerator = function (options, data){
        const plugin = {};
        plugin.results = {};
        var defaults = {
                onSaveCallback: null,
                onCancelCallBack : null
        };
        plugin.settings = {};
        plugin.settings = $.extend({}, defaults, options);
        var groupNo = 0;
        var ruleNo = 0;
        var fieldCategories = [], dataSourceTables = [], dataSourceFields= [];
        var specialFields = data && data.flags || '';
        var queryObject = data && data.queryObj || {};
        if(specialFields != '' && specialFields.split(',').length > 0){
                for(let x in specialFields.split(',')){
                        dataSourceFields.push({Id: (parseInt(x)+1)*-1, TableId: -1, Name: specialFields.split(',')[x], DataType: 1})
                }
                fieldCategories.push({Id: -1, Name: "Flag", SourceTableId: -1});
        }        
        var query = '';
        var selectQuery = ''
        var joinQuery = ''
        var queryObj = {};
        var tableObj = {};
        var returnQuery = '';
        var limitNumberField = "javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
        queryObj['g0'] = { condition: "AND" };
        let createElement = function(elem){
                let parent = $('<div>').attr({class : "qg"});
                let header = $('<div>').attr({class : "qg__header"}).append($('<span>').attr({class : "qg__header_heading"}).html("Custom Filter Selection"));
                let groups = $('<div>').attr({class : "qg__groups"});
                let resultQuery = $('<div>').attr({class : "qg__result"}).append($('<p>').attr({class: 'qg__footer-title'}).html('Generated Query :'), $('<p>').attr({class : 'qg__result__p'}));
                let buttonFooter = $('<div>').attr({class: "qg__footbut"}).append($('<button>').attr({class : "qg__save-button"}).html("Confirm"), $('<button>').attr({class: "qg__reset-button"}).html("Reset"), $('<button>').attr({class: "qg__close-button"}).html("Close"));
                let errorPanel = $('<div>').attr({class : "qg_error"}).append($('<p>').attr({class : 'qg__error__p'}));
                let footerPanel = $('<div>').attr({class  : "qg_footPanel"}).append(errorPanel,buttonFooter)
                let footer = $('<div>').attr({class : "qg__foot"}).append(resultQuery, footerPanel);
                parent.append(header, groups,footer);
                $(elem).append(parent);
                $(elem).show();
                footerButtons();
        }
        let addRule = function(elem,dataObj) {
                let rule = $('<div>').attr({class : "qg__rule", id: "rule-" + ruleNo});
                let inputs = $('<div>').attr({class: "qg__rule-left"});
                let category = $('<input>').attr({ type: "text",class: "qg__sel qg__sel--cat", id : `rule${ruleNo}-category`, rule : ruleNo, placeholder:'Select Category'});
                let field = $('<input>').attr({ type: "text", class: "qg__sel qg__sel--fie", id: `rule${ruleNo}-field`, rule: ruleNo, placeholder: 'Select Field', disabled : true});
                let aggregate = $('<input>').attr({ type: "text", class: "qg__sel qg__sel--aggr", id: `rule${ruleNo}-aggregate`, rule: ruleNo, placeholder: 'Select Aggregate', disabled : true});
                let condition = $('<input>').attr({ type: "text", class: "qg__sel qg__sel--con", id: `rule${ruleNo}-condition`, rule: ruleNo, placeholder: 'Select Condition', disabled : true});
                let value1 = $('<input>').attr({ type: "text", class: "qg__sel qg__sel--val1", id: `rule${ruleNo}-value1`, rule: ruleNo, placeholder: 'Select Value', disabled : true, maxlength: 30, oninput:limitNumberField});
                let value2 = $('<input>').attr({ type: "text", class: "qg__sel qg__sel--val2", id: `rule${ruleNo}-value2`, style : "display:none", rule: ruleNo, placeholder: 'Select Value', disabled : true, maxlength: 30, oninput:limitNumberField});
                
                category.lookup({ width : 160, searchActive: true, searchActiveAbove: 8, searchAutoFocus: false, onSave: onCategoryChange }, fieldCategories);
                field.lookup({ width : 160, searchActive: true, searchActiveAbove: 8, searchAutoFocus: false, onSave: onFieldChange }, []);
                condition.lookup({ width : 160, searchActive: true, searchActiveAbove: 30, searchAutoFocus: false, onSave: onCondChange }, []);
                aggregate.lookup({ width : 100, searchActive: true, searchActiveAbove: 30, searchAutoFocus: false, onSave: onAggrChange }, []);


                let remove = $('<div>').append($('<span>').attr({ class: "qg__but qg__but--red qg__rule--rem", ruleNo: `rule-${ruleNo}` }).html(""));
                inputs.append(category, field, aggregate, condition, value1, value2);
                rule.append(inputs, remove);
                elem.append(rule);
                dataObj = dataObj || {};
                if(Object.entries(dataObj).length && dataObj.constructor == Object){
                        var specialFlag = dataObj.fieldName.indexOf('$') > -1 && dataObj.category == 'Flag' ? true : false;
                        var catId = fieldCategories?.filter((x) => {return x.Name == dataObj.category})[0];
                        category.val(!specialFlag ? catId && catId.Id : -1);
                        var tableId = !specialFlag ? dataSourceTables.filter((x) => {return x.Name == dataObj.table})[0].Id : -1;
                        var fields = dataSourceFields.filter((x) => {return x.TableId ==  tableId && x.CategoryId == catId?.Id})
                        field[0].getLookup.setData(fields);
                        var fieldId = fields.filter((x) => {return x.Name == dataObj.fieldName && x.TableId == tableId})[0]?.Id;
                        field.val(fieldId);
                        field.prop('disabled',false);
                        if(catId && fieldId){
                                var fieldType = dataSourceFields.filter((x) => {return x.Id == fieldId})[0]?.DataType;
                                var condId;
                                aggregateConfigure(fieldType,ruleNo);
                                conditionConfigure(fieldType,ruleNo,dataObj.aggregate);
                                if([2,4,7,8,9,10].includes(fieldType)){
                                       condId = conditions[2].filter((x) => {return x.Name == dataObj.conditionName})[0]
                                       var aggegateId = dataObj.aggregate ? aggregates[1].filter((x) => { return x.Name == dataObj.aggregate})[0]?.Id : '';
                                       
                                } else {
                                        let aggegateDetails = dataObj.aggregate ? aggregates[2].filter((x) => { return x.Name == dataObj.aggregate})[0] : '';
                                        if(aggegateDetails?.Name == 'Count' || aggegateDetails?.Name == 'Distinct Count') 
                                                condId = conditions[2].filter((x) => {return x.Name == dataObj.conditionName})[0]
                                        else
                                                condId = conditions[1].filter((x) => {return x.Name == dataObj.conditionName})[0]
                                        var aggegateId = aggegateDetails?.Id
                                }
                                aggegateId ? aggregate.val(aggegateId).show().prop('disabled',false) : aggregate.hide();
                                condition.val(condId && condId.Id);
                                onCondChange(condition);
                                value1.val(dataObj.value1);
                                value2.val(dataObj.value2);
                                registerRuleEdit(ruleNo);
                        }        
                }
                $('.qg__rule--rem').off('click').on('click', function (){
                        let rNo = $(this)?.attr('ruleNo');
                        deleteRuleFromObject(rNo.replace('rule-',''));
                        $(`#${rNo}`).remove();
                        getQueryFromObject(queryObj);
                        clearValidationMsg();
                })
                $(value1).focusout(function(e){
                        e.stopPropagation();
                        if($(this).val()) showValidation($(this),true)
                        let ruleId = $(this).attr('rule');
                        var validDataType = registerRuleEdit(ruleId);
                        if(!validDataType.valid) {
                                showValidationMsg(validDataType.msg)
                        }
                        getQueryFromObject(queryObj);
                        clearValidationMsg();

                })
                $(value2).focusout(function(e){
                        e.stopPropagation();
                        if($(this).val()) showValidation($(this),true)
                        let ruleId = $(this).attr('rule');
                        var validDataType = registerRuleEdit(ruleId);
                        if(!validDataType.valid) {
                        	showValidationMsg(validDataType.msg)
                        }
                        getQueryFromObject(queryObj);
                        clearValidationMsg();
                });
                $('.qg__sel--val1').off('keypress').on('keypress', function() {
                        blockKeyPress($(this));
                });
                $('.qg__sel--val2').off('keypress').on('keypress', function() {
                        blockKeyPress($(this));
                });

                ruleNo++;
        }
        let addGroup = function(elem,addDefaultRule = true) {
                let group = $('<div>').attr({class : "qg__group", id: "group" + groupNo});
        	let head = $('<div>').attr({class: "qg__group_head"});
                let body = $('<div>').attr({class : "qg__group-body", id : `qg__group${groupNo}-rules`})
                head.append($('<div>').attr({class : "qg__wrapper qg__wrapper--style"}).append(
                        $('<input>').attr({class : "cbox" , type : "checkbox", id : `cbox${groupNo}` }),
                        $('<label>').attr({class : "label label2", for : `cbox${groupNo}`}).append(
                                $('<div>').attr({class : "text text2"}).html("AND"),
                                $('<div>').attr({class : "text text2"}).html("OR"))
                        ));
                               
                let grpControls = $('<div>').attr({class : "qg__head-controls"})
                let newRule = $('<span>').attr({class: "qg__but qg__but--green qg__but--rule", grpNo : `group${groupNo}`}).html("+ Rule");
                let newGroup = $('<span>').attr({ class: "qg__but qg__but--blue qg__but--group", grpNo: `group${groupNo}` }).html("+ Group");
                let remove = groupNo != 0 ? $('<span>').attr({ class: "qg__but qg__but--red qg__group--rem", grp: `group${groupNo}` }).html("Delete") : '';
                grpControls.append(newRule, newGroup, remove);
                head.append(grpControls);
                group.append(head,body);
                elem.append(group);
                if(addDefaultRule) {
                        addRuleToObject(queryObj,groupNo,ruleNo)
                        addRule($(`#qg__group${groupNo}-rules`));
                }
                groupNo++;
                $(`.cbox`).off('click').on('click',function(){
                        let groupId = $(this).attr('id');
                        groupId = groupId.replace('cbox','')
                        registerGroupChange(groupId);
                        getQueryFromObject(queryObj);
                })
                $('.qg__but--rule').off('click').on('click', function (){
                	    var isValid = validateRules({changeColor : true, add : true});
                        if(!isValid.validRule) {
                                showValidationMsg(isValid.errorMsg? isValid.errorMsg: 'Please fill the fields before adding a new Rule');
                        } else {
                                let grpNo = $(this).attr('grpNo')
                                let elem = $(`#${grpNo} #qg__${grpNo}-rules`);
                                addRuleToObject(queryObj,grpNo.replace('group',''),ruleNo)
                                addRule(elem);  
                                getQueryFromObject(queryObj);
                        }
                        clearValidationMsg();
                })
                $('.qg__but--group').off('click').on('click', function (){
                        var isValid = validateRules({changeColor : true, add : true});
                        if(!isValid.validRule) {
                                showValidationMsg(isValid.errorMsg? isValid.errorMsg: 'Please fill the fields before adding a new Rule');
                        } else {
		                        let grpNo = $(this).attr('grpNo')
		                        elem  = $(`#${grpNo} #qg__${grpNo}-rules`); 
		                        addGroupToObject(queryObj,grpNo.replace('group',''),groupNo);
		                        addGroup(elem,true,grpNo);
		                        getQueryFromObject(queryObj);							
                        }
                        clearValidationMsg();
                })
                $('.qg__group--rem').off('click').on('click', function (){
                        let gNo = $(this).attr('grp');
                        deleteGroupFromObject(gNo.replace('group',''));
                        $(`#${gNo}`).remove();
                        getQueryFromObject(queryObj);
                        clearValidationMsg();
                })
        }
        
        function onCategoryChange(category) {
                showValidation($(category),true)
                var categoryId = $($(category)[0]).val();
                let ruleId = $($(category)[0]).attr("rule");
                clearFields(ruleId,['0', '1', '2', '3', '4']);
                var fields = dataSourceFields?.filter((x) => {return x.CategoryId == categoryId});
                $(`#rule${ruleId}-field`)[0].getLookup.setData(fields);
                $(`#rule${ruleId}-field`).prop('disabled',false);
                $(`#rule${ruleId}-value1`).attr('placeholder', 'Select Value');
                registerRuleEdit(ruleId)
                getQueryFromObject(queryObj);
                clearValidationMsg();
        }
        
        function onFieldChange(field) {
                showValidation($(field),true)
                var fieldId = $($(field)[0]).val();
                let ruleId = $($(field)[0]).attr("rule");
                clearFields(ruleId,['1','2','3','4']);
                var field = dataSourceFields.filter((x) => {return x.Id == fieldId})[0];
                var fieldType = field.DataType
                var tableId = field.TableId
                var isParcelField = dataSourceTables.filter((x) => {return x.Id == tableId})[0].IsParcelTable;
                //$(`#rule${ruleId}-aggregate`)[isParcelField ? 'hide': ([1,3,4,5,6,11,12,13].includes(field.DataType)? 'hide': 'show')]();
                //$(`#rule${ruleId}-aggregate`).prop('disabled',false);
                aggregateConfigure(fieldType,ruleId);
                conditionConfigure(fieldType,ruleId);
                registerRuleEdit(ruleId)	
                getQueryFromObject(queryObj);
                clearValidationMsg();
        }

        function onAggrChange(aggregate) {
                showValidation($(aggregate),true)
                let ruleId = $($(aggregate)[0]).attr("rule");
                let aggrVal = $($(aggregate)[0]).attr("sel");
                let fieldId = $(`#rule${ruleId}-field`).attr("sel");
                let dataType = dataSourceFields.filter((x) => {return x.Id == fieldId})[0]?.DataType;
                let IsVarCharField = false;
                let aggrOperator;
                if([2,4,7,8,9,10].includes(dataType)){
                        aggrOperator =  aggrVal ? aggregates[1].filter((x) => { return x.Id == aggrVal})[0]?.Name : '';
                }
                else {
                        aggrOperator =  aggrVal ? aggregates[2].filter((x) => { return x.Id == aggrVal})[0]?.Name : '';
                        IsVarCharField = true;
                }
                if((aggrOperator == 'Count' || aggrOperator == 'Distinct Count') && IsVarCharField){
                        $(`#rule${ruleId}-condition`)[0].getLookup.setData(conditions[2]);
                }
                else {
                        $(`#rule${ruleId}-condition`)[0].getLookup.setData(IsVarCharField ? conditions[1] : conditions[2]); 
                }
                registerRuleEdit(ruleId)
                getQueryFromObject(queryObj);
                clearValidationMsg();
        }

        function onCondChange(condition) {
                showValidation($(condition),true)
                let ruleId = $($(condition)[0]).attr("rule");
                let fieldId = $(`#rule${ruleId}-field`).attr("sel");
                let dataType = dataSourceFields.filter((x) => {return x.Id == fieldId})[0]?.DataType;
                $(`#rule${ruleId}-value1,#rule${ruleId}-value2`).prop('disabled',false).show();
                if( $($(condition)[0]).html() == "Between") {
                        $(`#rule${ruleId}-value2`).show();
                        $(`#rule${ruleId}-value2, #rule${ruleId}-value1`).prop('disabled',false);
                } else if( $($(condition)[0]).html() == "Is NULL" || $($(condition)[0]).html() == "Is Not NULL"){
                        $(`#rule${ruleId}-value1, #rule${ruleId}-value2`).prop('disabled',true).hide();
                        $(`#rule${ruleId}-value1`).val('');
                        $(`#rule${ruleId}-value2`).val('');
                        showValidation($(`#rule${ruleId}-value1`), true)
                        showValidation($(`#rule${ruleId}-value2`), true)
                } else {
                        $(`#rule${ruleId}-value1`).prop('disabled',false);
                        $(`#rule${ruleId}-value2`).prop('disabled',true).hide();
                        $(`#rule${ruleId}-value2`).val('');
                        showValidation($(`#rule${ruleId}-value1`), true)
                        showValidation($(`#rule${ruleId}-value2`), true)
                }
                if($($(condition)[0]).html() == "IN"){
                        $(`#rule${ruleId}-value1, #rule${ruleId}-value2`).prop({type : "text"}) 
                } else {
                        valueFieldConfigure(dataType,ruleId);
                }
                registerRuleEdit(ruleId)
                getQueryFromObject(queryObj);
                clearValidationMsg();
        }

        var conditionConfigure = (datatype,ruleId, aggr)=>{
                if([2,4,7,8,9,10].includes(datatype)){
                        $(`#rule${ruleId}-condition`)[0].getLookup.setData(conditions[2]);  
                } else {
                        if(aggr && (aggr == 'Count' || aggr == 'Distinct Count'))
                                $(`#rule${ruleId}-condition`)[0].getLookup.setData(conditions[2]); 
                        else
                                $(`#rule${ruleId}-condition`)[0].getLookup.setData(conditions[1]);           
                }
                $(`#rule${ruleId}-condition`).prop('disabled',false);
                datatype == 3? $(`#rule${ruleId}-value1`).attr('placeholder', 'Yes/No (1/0)'): $(`#rule${ruleId}-value1`).attr('placeholder', 'Select Value');
                valueFieldConfigure(datatype,ruleId )
        }

        var aggregateConfigure = (datatype, ruleId) =>{
                if([2,4,7,8,9,10].includes(datatype)){
                        $(`#rule${ruleId}-aggregate`)[0].getLookup.setData(aggregates[1]);  
                } else {
                        $(`#rule${ruleId}-aggregate`)[0].getLookup.setData(aggregates[2]);  
                }
                $(`#rule${ruleId}-aggregate`).prop('disabled',false);
        }

        var valueFieldConfigure = (dataype,ruleId)=>{
                if([2,3,7,8,9,10].includes(dataype)){
                        $(`#rule${ruleId}-value1, #rule${ruleId}-value2`).prop({type : "number"})
                } else if(dataype == "4"){
                        $(`#rule${ruleId}-value1, #rule${ruleId}-value2`).prop({type : "date"}) 
                } else {
                        $(`#rule${ruleId}-value1, #rule${ruleId}-value2`).prop({type : "text"}) 
                }
        }

        function addGroupToObject(obj, parentGroup, GroupNumber){
                var parent = 'g'+ parentGroup
                for(let x in obj){
                        if(x.includes(parent)){
                                obj['g'+parentGroup]['g'+GroupNumber] = { condition: "AND" };
                        }  
                        else if(x.includes('g')){
                                addGroupToObject(obj[x],parentGroup, GroupNumber);
                        }        
                }
        } 
        
        function addRuleToObject(obj,GroupNumber,ruleNumber){
                var parent = 'g'+ GroupNumber
                for(let x in obj){
                        if(x.includes(parent)){
                                var category ='', table ='', fieldName ='', aggregate = '', conditionName='', value1='', value2=''
                                obj['g'+GroupNumber]['r'+ruleNumber] = {category, table, fieldName, aggregate, conditionName, value1, value2};
                        } 
                        else if(x.includes('g')){
                                addRuleToObject(obj[x],GroupNumber,ruleNumber);
                        }        
                }
        }

        var deleteGroupFromObject = function(groupNo){
                let groupToFind = 'g' + groupNo
                function deleteGroup(obj){
                        for(let x in obj){
                                if(x.includes(groupToFind)){
                                        delete obj[x];
                                } 
                                else if(x.includes('g')){
                                        deleteGroup(obj[x]);
                                }
                        }
                }

                deleteGroup(queryObj);
        }

        var deleteRuleFromObject = function(ruleNo){
                let ruleToFind = 'r' + ruleNo
                function deleterule(obj){
                        for(let x in obj){
                                if(x.includes(ruleToFind)){
                                        delete obj[x];
                                } 
                                else if(x.includes('g')){
                                        deleterule(obj[x]);
                                }
                        }
                }

                deleterule(queryObj);
        }

        var blockKeyPress = (elem)=>{
                if(elem.attr("type") == "number"){
                        if(event.key == "e" || event.key == "E")
                                event.preventDefault();
                }
                var regex = new RegExp("^[a-zA-Z0-9 ,-.]+$");
                if(!regex.test(event.key))
                        event.preventDefault();
        }

        var registerGroupChange = function(group){
                var conditionLogic = $('#cbox' + group).attr('checked')? 'OR': 'AND';
                var groupToFind = 'g'+group;
                function addChangeToGroup(obj){
                        for(let x in obj){
                                if(x.includes(groupToFind)){
                                        obj[x].condition = conditionLogic;
                                } 
                                else if(x.includes('g')){
                                        addChangeToGroup(obj[x]);
                                }
                        }
                }
                addChangeToGroup(queryObj);
        }

        var registerRuleEdit =function(ruleNo){
                let categoryId = $(`#rule${ruleNo}-category`).attr('sel');
                let fieldId = $(`#rule${ruleNo}-field`).attr('sel');
                let aggregateId = $(`#rule${ruleNo}-aggregate`).attr('sel');
                let conditionId = $(`#rule${ruleNo}-condition`).attr('sel');
                let value1 = $(`#rule${ruleNo}-value1`).attr('value');
                let value2 = $(`#rule${ruleNo}-value2`).attr('value');
                let ruleToFind = 'r' + ruleNo
                let category = fieldCategories.filter((x) => {return x.Id == categoryId})[0]?.Name;
                let field = fieldId ? dataSourceFields.filter((x) => {return x.Id == fieldId})[0] : '';
                if(categoryId == '-1') var table = '';
                else {
                        var tableId = field.TableId;
                        var table = tableId ? dataSourceTables.filter((x) => { return x.Id == tableId})[0]?.Name : '';

                }
                let fieldName = field && field.Name;
                let aggregate = aggregateId ? getAggregate(field && field.DataType, aggregateId) : '';
                let conditionName = conditionId ? getCondition(field && field.DataType, conditionId, aggregate) : '';
                var valid = true;
                var msg = 'The entered value is not compatible with the field datatype'
                if(conditionName.toUpperCase() != 'IS NULL' && conditionName.toUpperCase() != 'IS NOT NULL') {
                        if(conditionName.toUpperCase() == 'IN' && value1) {
                                        var values = value1.split(',');
                                        for(let i=0; i< values.length; i++) {
                                                valid = validateDataType(field && field.DataType, values[i]);
                                                if(!valid) break;
                                        }
                        } else {
                                if(value1) valid = validateDataType(field && field.DataType, value1);
                                if(valid && value1 && value2 && conditionName.toUpperCase() == 'BETWEEN')  {
                                        valid = validateDataType(field && field.DataType, value2);
                                        if(valid && field.DataType == 4){
                                        	let fromDate = new Date(value1)
                                        	let toDate = new Date(value2)
                                        	if(toDate.getTime() <= fromDate.getTime()){
                                        		msg = 'The TO date given for the between condition should be greater than the FROM date';
                                                valid = false;
                                        	}
                                        }
                                        else if(valid && parseFloat(value2) <= parseFloat(value1)) {
                                                msg = 'The second value given for the between condition should be greater than the first Value';
                                                valid = false;
                                        } 
                                }
                        }
                }
                
                showValidation($(`#rule${ruleNo}-value1`), valid)
                showValidation($(`#rule${ruleNo}-value2`), valid)
                
                if(!valid) return {valid, msg}
                
                function addValuesToRule(obj){
                        for(let x in obj){
                                if(x.includes(ruleToFind)){
                                        obj[x].category = category;
                                        obj[x].table = table;
                                        obj[x].fieldName = fieldName;
                                        obj[x].aggregate = aggregate;
                                        obj[x].conditionName = conditionName;
                                        obj[x].value1 = value1;
                                        obj[x].value2 = value2;
                                } 
                                else if(x.includes('g')){
                                        addValuesToRule(obj[x]);
                                }
                        }
                }
                addValuesToRule(queryObj);  
                return {valid};
        }

        var clearFields = (ruleId, clearFrom)=>{
                $(`#rule${ruleId}-value1`).show();
                $(`#rule${ruleId}-value2`).hide();
                var allFields = ['field','aggregate','condition','value1','value2'];
                for(let x in allFields){
                        if(clearFrom.includes(x)){
                                $(`#rule${ruleId}-${allFields[x]}`).val('').attr('disabled','disabled').addClass('validPass') ;
                        }
                }
        }

        function showValidationMsg(msg){
               $('.qg__error__p').html('*  ' + msg);
        }

        function clearValidationMsg(){
                if(!$('.validFail').length)
                        $('.qg__error__p').html('');
        }
        
        function footerButtons() {
        	$('.qg__save-button').off('click').on('click', function (){
        		var isValid = validateRules({changeColor : true });
        		if(!isValid.validRule) {
      				showValidationMsg(isValid.errorMsg? isValid.errorMsg: 'Fill the missing fields before confirming the query');
      				return false;
      			} else {
                                if(confirm('Are you sure you want to confirm the Query')) {
                                        console.log(queryObj);
                                        plugin.results = {
                                                queryToDisplay : returnQuery,
                                                selectQuery : selectQuery,
                                                joinQuery : joinQuery,
                                                whereQuery : query,
                                                queryObj : queryObj
                                        }
                                        plugin.close();
                                        if(plugin.settings.onSaveCallback) plugin.settings.onSaveCallback();
                                } else {
                                        if(plugin.settings.onCancelCallBack) plugin.settings.onCancelCallBack();
                                }
      			}
                });
                $('.qg__reset-button').off('click').on('click', function (){
                        if(confirm('Are you sure you want to reset the query. Any Unsaved changes will be lost')) {
                                $('.qg__groups').empty();
                                groupNo = 0;
                                ruleNo = 0;
                                query = '';
                                returnQuery = '';
                                selectQuery = ''
                                joinQuery = ''
                                queryObj = {};
                                tableObj = {};
                                queryObj['g0'] = { condition: "AND" };
                                $('.qg__result__p').html('');
                                clearValidationMsg();
                                addGroup($(".qg__groups"));
                        }
                });
                $('.qg__close-button').off('click').on('click', function (){
                        if(confirm('Are you sure you want to close this window. Any Unsaved changes will be lost')) {
                                plugin.close();
                        }
                });
        }
        
        plugin.close = () => {
                $('.query-build').hide();
                $('.query-build').empty();
                $('.query-build').remove();
                hideMask();
        }
		
        function getCondition(dataType, condition, agg) {
                if([2,4,7,8,9,10].includes(dataType)) {
                        return conditions[2].filter((x) => {return x.Id == condition})[0].Name;
                } else {
                        if((agg == 'Count' || agg == 'Distinct Count'))
                                return conditions[2].filter((x) => {return x.Id == condition})[0].Name;
                        else
                                return conditions[1].filter((x) => {return x.Id == condition})[0].Name;            
                }
        }

        function getAggregate(dataType, agg) {
                if([2,4,7,8,9,10].includes(dataType)) {
                        return aggregates[1].filter((x) => {return x.Id == agg})[0].Name;
                } else {
                        return aggregates[2].filter((x) => {return x.Id == agg})[0].Name;
                }
        }
        
        function relationalOf(condition) {
                var value = '';
                switch(condition.toUpperCase()) {
                        case 'EQUAL TO': value = '='; break;
                        case 'NOT EQUAL TO': value = '!='; break;
                        case 'GREATER THAN': value = '>'; break;
                        case 'LESS THAN': value = '<'; break;
                        case 'GREATER THAN OR EQUAL TO': value = '>='; break;
                        case 'LESS THAN OR EQUAL TO': value = '<='; break;
                        default: value = condition;
                }
                return value;
        }

        function validateRules(options){
                let validRule = true;
                let errorMsg = '';
                let ruleStatus
                for(i = 0; i <= ruleNo ; i++){
                        if(!$(`#rule-${i}`).length == 0){                       
                                ruleStatus =  verifyRule(i,options)
                                validRule = ruleStatus?.ruleVerified
                                errorMsg = ruleStatus.ruleError
                                if(!validRule) break;                                
                        }        
                }
                if(validRule && !options.add) {
                        validRule = checkRules(queryObj);
                        errorMsg = !validRule? 'A Group should have atleast one rule or one group in it': '';
                }
                return {validRule, errorMsg};
        }

        function verifyRule(index,options){
                let ruleVerified = true;
                let ruleError = ''
                let [c,f,a,cn,v1,v2] = [$(`#rule${index}-category`) ,$(`#rule${index}-field`),$(`#rule${index}-aggregate`),$(`#rule${index}-condition`),$(`#rule${index}-value1`), $(`#rule${index}-value2`)]
                let fields = [c,f,a,cn,v1,v2];
                let fieldValues = [c.attr("sel"),f.attr("sel"),a.attr("value"),cn.attr("value"),v1.attr("value"),v2.attr("value")]
                for(let k in fields){
                        let elemValid = true;
                        let elem  = fields[k]
                        let elemType = elem?.attr("Id")?.split("-")?.pop();
                        let elemValue = elem.attr('value');
                        let disabled = elem.attr("disabled") == "disabled" ? true : false;
                        let visible = elem.is(":visible");
                        if(!disabled && visible){
                                switch(elemType){
                                        case 'category' :
                                        case 'field' :
                                        case 'condition' : {
                                                elemValid =  !elemValue ? false : true;
                                                break;
                                        }
                                        case 'aggregate' : {
                                                let fieldId = fieldValues[1]
                                                let tableId = dataSourceFields.filter((x) => {return x.Id == fieldId})[0]?.TableId;
                                                let isParcelField = dataSourceTables.filter((x) => {return x.Id == tableId})[0]?.IsParcelTable;
                                                elemValid = !isParcelField  && !elemValue ? false : true;
                                                break;
                                        } 
                                        case 'value1' : {
                                                let cond = fieldValues[3];
                                                let needValue = cond == 'IS NULL' || cond == 'IS NOT NULL' ? false : true;
                                                elemValid = needValue  && !elemValue ? false : true;
                                                var validDataType = registerRuleEdit(index);
                                                if(elemValid && !validDataType.valid) {
                                                        elemValid = validDataType.valid;
                                                        ruleError = validDataType.msg;
                                                }
                                                break;
                                        }
                                        case 'value2' : {
                                                let cond = fieldValues[3]
                                                elemValid = cond == 'Between'  && !elemValue ? false : true;
                                                var validDataType = registerRuleEdit(index);
                                                if(elemValid && !validDataType.valid) {
                                                        elemValid = validDataType.valid;
                                                        ruleError = validDataType.msg;
                                                }
                                                break;
                                        } 
                                }
                                if(!elemValid)
                                        ruleVerified = false;
                                options?.changeColor && showValidation(elem,elemValid);
                        }
                }
                return { ruleVerified , ruleError } 
        }

        function showValidation(elem, valid){
                if(!valid)
                        elem.removeClass('validPass').addClass('validFail')
                else
                        elem.removeClass('validFail').addClass('validPass')        
        }
      
        function checkRules(obj) { 
                var valid = true;
                if(Object.keys(obj).length == 1 && Object.keys(obj)[0] == 'condition') { return false};
                for(let x in obj) {
                        if(x.includes('g')) {
                                valid = checkRules(obj[x]);
                        }
                }
                return valid;
	}
        
        function validateDataType(dataType, value) {
                var status = false;
                if([1, 5, 6, 7, 9 , 11, 12, 13].includes(dataType)) {
                        status = value? true: false;
                } else if([2, 3, 8, 10].includes(dataType)) {
                        if(jQuery.isNumeric(value)) {
                        	status = dataType == 3? ((value == 1 || value == 0)? true: false): true;
                        } else {
                        	status = false;
                        }
                } else if(dataType == 4) {
                        value = value.replaceAll(' ', '');
                        var dateReg = /^\d{4}[./-]\d{2}[./-]\d{2}$/;
                        status = value.match(dateReg)? true: false;
                }

                if(status && dataType != 4){
                        var regex = new RegExp("^[a-zA-Z0-9 ,-.]+$");
                        status = regex.test(value)? true: false;
                }
                return status;
	}
        
        function conditionQueryFromObject(obj) {
                for(let x in obj) {
                        if(x.includes('r')) {
                                if(!verifyRule(x.replace('r',''))?.ruleVerified){
                                        return false;  
                                }
                                var qCondition = '';
                                var splFlag = obj[x].category == 'Flag' ? true : false;
                                var logic = returnQuery[returnQuery.length-1] == '('? '': ' ' + obj.condition + ' ';
                                var tableName = obj[x]?.table;
                                var fieldName = obj[x]?.fieldName;
                                var value1 = obj[x]?.value1;
                                var value2 = obj[x]?.value2;
                                var tableInfo = dataSourceTables.filter((x) => {return x.Name == tableName})[0];
                                var fieldInfo = dataSourceFields.filter((x) => {return x.Name == fieldName && x.TableId == tableInfo.Id})[0];
                                
                                if([1,4,5,6].includes(fieldInfo.DataType) && obj[x]?.conditionName.toUpperCase() == 'IN' && value1 ){
                                        let splitValues = value1.split(',')
                                        value1 = ''
                                        for(let x in splitValues) {
                                                value1 += value1!= '' ? `, '${splitValues[x]}'` : `'${splitValues[x]}'`
                                        }
                                }

                                if(obj[x].conditionName.toUpperCase() == 'BETWEEN') qCondition = (jQuery.isNumeric(value1)? value1: "'" + value1 + "'") + ' AND ' + (jQuery.isNumeric(value2)? value2: "'" + value2 + "'");
                                else qCondition = value1 ? obj[x]?.conditionName.toUpperCase() == 'IN'? "(" + value1 + ")": ([2,7,8,9,10].includes(fieldInfo.DataType)? value1 :"'" + value1 + "'"): '';
                                //query to display
                                returnQuery += logic + `${obj[x]?.aggregate}(` + (splFlag ? '': tableName + '.') +  fieldName + ') ' + relationalOf(obj[x]?.conditionName) + ' ' + qCondition;
                                
                                if(splFlag){
                                        if(fieldName == 'SketchFlag'){
                                                query += logic + `sf.Name `
                                                joinQuery +=  `LEFT OUTER JOIN SketchStatusFlags sf on p.SketchFlag = sf.Id `
                                        }
                                        else {
                                                query += logic + `p?.${fieldName} ` 
                                        }
                                } else {
                                        if(!tableObj[tableInfo?.Id])
                                                tableObj[tableInfo?.Id] = {}
                                        tableObj[tableInfo?.Id][x.replace('r','')] =  obj[x]
                                        query +=   logic + `t_${tableInfo?.Id}.${obj[x]?.aggregate == 'Distinct Count' ? 'distinct_count' : obj[x]?.aggregate}_${fieldName} `
                                        //joinQuery += `INNER JOIN (SELECT CC_ParcelId, ${obj[x].aggregate}(${obj[x].fieldName}) As ${obj[x].fieldName} FROM ${tableInfo.CC_TargetTable}  GROUP BY CC_ParcelId) t_${tableInfo.Id} ON p.Id = t_${tableInfo.Id}.CC_ParcelId `
                                }
                                query += relationalOf(obj[x].conditionName) + ' ' + qCondition;
                                //selectQuery += splFlag ? '' :  `t_${tableInfo.Id}.${obj[x].fieldName},`
                        } else if(x.includes('g')) {
                                returnQuery += (returnQuery.length < 1 || returnQuery[returnQuery.length-1] == '('? '': ' ' + obj.condition + ' ') + '(';
                                query += (query.length < 1 || query[query.length-1] == '('? '': ' ' + obj.condition + ' ') + '(';
                                conditionQueryFromObject(obj[x]);
                                returnQuery += ')';
                                query += ')';
                        }
                }
        }

        function selectQueryFromObject(tObj){
                let selectFields = 'SELECT p.Id As ParcelId, p.KeyValue1 As KeyValue1'
                for(let x in tObj){
                        let tId = x
                        let tJoin = ''
                        let tSelect = `SELECT CC_ParcelId`
                        let targetTable = dataSourceTables.filter((x) => {return x.Id == tId})[0].CC_TargetTable;
                        let table = tObj[x]
                        for(let c in table){
                                let field = table[c]
                                let tableId = dataSourceTables.filter((x) => {return x.Name == field.table})[0].Id;
                                let fieldInfo = dataSourceFields.filter((x) => {return x.Name == field.fieldName && x.TableId == tableId})[0];
                                let fieldName = fieldInfo.DataType == 3 ? `CONVERT(int,${field.fieldName})` : field.fieldName
                                if(field?.aggregate != 'Distinct Count'){
                                        if(!tSelect.includes(`${field?.aggregate}(${field?.fieldName})`)){
                                                tSelect += `, ${field?.aggregate}(${fieldName}) As ${field?.aggregate}_${field?.fieldName}`
                                                selectFields += `, t_${x}.${field?.aggregate}_${field?.fieldName}`
                                        }
                                }
                                else {
                                        if(!tSelect.includes(`Count(Distinct(${field?.fieldName}))`)){
                                                tSelect += `, Count(Distinct(${fieldName})) As distinct_count_${field?.fieldName}` 
                                                selectFields += `, t_${x}.distinct_count_${field?.fieldName}`
                                        }         
                                }                  
                        }
                               
                        tJoin += ` INNER JOIN( ${tSelect } FROM ${targetTable} Group by CC_ParcelId) t_${tId} ON t_${tId}.CC_ParcelId = p.Id`
                        joinQuery += tJoin
                }
                selectFields += ` FROM Parcel p `
                selectQuery = selectFields
        }
        
        function getQueryFromObject() {
                returnQuery = '';
                query = '';
                selectQuery = ''
                joinQuery = ''
                tableObj = {};
                conditionQueryFromObject(queryObj);
                selectQueryFromObject(tableObj);
                returnQuery = returnQuery.substring(1, returnQuery.length-1);
                query = ' WHERE ' + query.substring(1, query.length-1);
                $('.qg__result__p').html(returnQuery);
        }
		   
        let objectToNodes = function(qryObj,grpNo){
                var obj = qryObj
                var grp = grpNo ? grpNo : 0;
                if(qryObj && qryObj.condition == "OR") {
                    $(`#cbox${grp}`).attr('checked', 'checked');
                    registerGroupChange(grp);
                }
                for(let x in obj){
                        if(x.includes('g')){
                                let elem  = $(`#group${grp} #qg__group${grp}-rules`);
                                addGroup(elem,false);
                                addGroupToObject(queryObj,grp,groupNo - 1);
                                registerGroupChange(groupNo - 1);
                                objectToNodes(obj[x],groupNo - 1);
                        } 
                        else if(x.includes('r')){
                                addAndFillRule(obj[x],grp);
                        }        
                }
        }

        function addAndFillRule(ruleObj,grpNo){
                let elem  = $(`#group${grpNo} #qg__group${grpNo}-rules`);
                addRuleToObject(queryObj,grpNo,ruleNo);
                addRule(elem,ruleObj);
        }

        function loadFieldCategories(callback){
                $ds('loadfieldcategory', {
                        Query: "SELECT Id, Name, SourceTableId FROM FieldCategory --WHERE ShowCategory = 1"
                    }, function(res) {
                        //fieldCategories = res;
                        res.forEach((x) =>{fieldCategories.push(x)})
                        if(callback) callback();
                    });
        }

        function loadFields(callback){
                $ds('loadfieldcategory', {
                        Query: "SELECT Id, TableId, Name, DisplayLabel, DataType, CategoryId FROM DataSourceField WHERE CategoryId IS NOT NULL"
                    }, function(res) {
                        //dataSourceFields = res;
                        res.forEach((x) =>{dataSourceFields.push(x)})
                        if(callback) callback();
                    });
        }

        function loadTables(callback){
                $ds('loadfieldcategory', {
                        Query: "SELECT t1.Id, t1.Name, t1.CC_TargetTable,CASE WHEN CC_TargetTable = 'ParcelData' THEN 1 ELSE 0 END AS IsParcelTable FROM DataSourceTable As t1 LEFT JOIN Application AS t2 ON t1.Name = t2.ParcelTable"
                    }, function(res) {
                        dataSourceTables = res;
                        if(callback) callback();
                    });
        }

        var init = function(){
                var elem = $('<div>').attr({class : "query-build"});
                elem.appendTo('body');
                if($('.cc-drop-pop').length == 0){
                        var lookupDiv = $('<div>').attr({class : "cc-drop-pop"});
                        var lookupInput = $('<input>').attr({class : "cc-drop-search"});
                        var lookupItems =  $('<div>').attr({class : "cc-drop-items"});
                        lookupDiv.append(lookupInput, lookupItems);
                        lookupDiv.appendTo('body');
                }
                //showMask()// This should be called on the parent page when the button is clicked to invoke the Query Generator        
                createElement(elem);
                if(Object.entries(queryObject).length && queryObject.constructor == Object){
                        addGroup($(".qg__groups"),false);
                        objectToNodes(queryObject.g0);
                        getQueryFromObject(queryObj);
                } 
                else {
                        addGroup($(".qg__groups"));
                }        
        }
        loadFieldCategories(function(){
                loadTables(function(){
                        loadFields(function(){
                                init();
                        }) 
                }) 
        })
        
        
        return plugin;
        
}

const conditions = {
        1 : [
                { Id : '1', Name : 'Equal to'},
                { Id : '2', Name : 'Not Equal to'},
                { Id : '3', Name : 'IN'},
                { Id : '4', Name : 'Is NULL'},
                { Id : '5', Name : 'Is Not NULL'}
        ],
       2 : [
                { Id : '1', Name : 'Equal to'},
                { Id : '2', Name : 'Not Equal to'},
                { Id : '3', Name : 'Greater than'},
                { Id : '4', Name : 'Less than'},
                { Id : '5', Name : 'Greater than or Equal to'},
                { Id : '6', Name : 'Less than or Equal to'},
                { Id : '7', Name : 'IN'},
                { Id : '8', Name : 'Is NULL'},
                { Id : '9', Name : 'Is Not NULL'},
                { Id : '10', Name : 'Between'}
       ]
}

const aggregates = {
        1 : [
                { Id : '1', Name : 'Avg'},
                { Id : '2', Name : 'Sum'},
                { Id : '3', Name : 'Max'},
                { Id : '4', Name : 'Min'}
        ],
        2 : [
                { Id : '1', Name : 'Min'},
                { Id : '2', Name : 'Max'},
                { Id : '3', Name : 'Count'},
                { Id : '4', Name : 'Distinct Count'}
        ]
}
