﻿var dbCustomddl = [];
var targetSource;
var selectedFieldId = '';
var tempsRec = [{},{}];
var tempactiveParcelEdits = [];
function openCustomddl(source, lkVal, _ns) {
    $('.mask').show();
    window.scrollTo(0, 0);
    $('body').css('overflow', 'hidden');
    $('.divCodeFile').show();
    targetSource = lkVal? source: $(source).siblings('.input')[0];
    $('.divin span[legend]').html($(targetSource.parentNode).siblings('.label').children('span:first').html() + ': ');
    getValue = $(targetSource).val();
    selectedText = lkVal? $(targetSource).html(): $('option[value="' + getValue + '"]', $(targetSource))[0].text;
    $('span[selectedcustomddl]').html(selectedText);
    selectedFieldId = $(targetSource).parent().attr('fieldid');
    if (lkVal) {
    	for (x in lkVal) {
    		var nm = lkVal[x].Name? lkVal[x].Name: '', cnm = ((lkVal[x].Name || lkVal[x].Name === 0) ? lkVal[x].Name: (lkVal[x].cId? lkVal[x].cId: '')),val = lkVal[x].Id ? lkVal[x].Id: (lkVal[x].Value? lkVal[x].Value: '')
	    	dbCustomddl.push({ 'label': nm, 'value': val, 'cValue': lkVal[x].cId, 'cLabel': cnm });
	    }
    }
    else {
	    var options = targetSource.options;
	    for (x in options) {
	        if (options[x].tagName && options[x].attributes[0].name != 'nonexist') {
	            dbCustomddl.push({ 'label': options[x].label, 'value': options[x].value, 'cValue': decodeURI(options[x].value), 'cLabel': options[x].label });
	        }
	    }
    }
    
    var splitddl = dbCustomddl.slice(0, 30);
    keyUp(null, false, splitddl);
    
    $('#searchtxt').focus();
    setScreenDimensions();
    return false;
}

function keyUp(s, showAll, ddlVal) {
    setScreenDimensions();
    var v = ddlVal? ddlVal: getMatches($(s).val());
    
    if (!showAll && $('#searchtxt').val() == ''  && $('.lkShowAllRec').css('display') != 'none')
    	return false;
    if (!showAll && $('#searchtxt').val() == '') {
    	var splitddl = dbCustomddl.slice(0, 30);
    	$('span[selectedcustomddl]').html(selectedText);
	    if (dbCustomddl.length < 31) {
	    	$('.lkShowAlllink').hide();
	    	$('.lkShowAllRec').show();
	    }
	    else {
	    	$('.lkShowAlllink').show();
			$('.lkShowAllRec').hide();
	    }
	    v = splitddl;
    }
    else if (!showAll) {
    	if (dbCustomddl.length == v.length) {
	    	$('.lkShowAlllink').hide();
	    	$('.lkShowAllRec').show();
	    }
	    else {
			$('.lkShowAlllink').show();
			$('.lkShowAllRec').hide();
	    }
    }
    
    if (dbCustomddl.length == 0) {
    	$('.lkShowAlllink').hide();
		$('.lkShowAllRec').hide();
    }
    	
    var check_V_Change;
    if(v != check_V_Change) { $('span[selectedcustomddl]').html(selectedText); }
    check_V_Change = v;
    $('.lkShowAllStatus').html('Showing ' + v.length + ' records out of ' + dbCustomddl.length);
    if (v.length == 0) {
        $('.match').html('No matches...');
        $('span[selectedcustomddl]').html(selectedText);
    }
    else {
        $('.match').html('');
        var ul = document.createElement('ul');
        $(ul).attr("class", "customul");
        $('.match')[0].appendChild(ul);
        for (i in v) {
            $('.match ul')[0].appendChild(createList(v[i]));
        }
    }
    /*if (!showAll && $('#searchtxt').val() == '') {
        $('.match').html('No matches...');
        $('span[selectedcustomddl]').html(selectedText);
    }*/
    setScreenDimensions();
    return false;
}

function createList(v) {
	var field = datafields[$(targetSource).parent().attr('fieldid')];
    var li = document.createElement('li');
    $(li).attr('value', v.value);
    $(li).attr('displayValue', v.cLabel);
    if(v.value == '<blank>')
    	v.value = '';
    var temp =  field.LookupQuery && field.LookupQuery != '' ? v.label : v.cValue +'-'+ v.label;   //( v.label != '' && v.cValue != ''? v.cValue +'-'+ v.label: (v.label != '')? v.label: '')  removed added code change to make same as MA.
    if (temp.length > 105)  $(li).css('height','34px') ;
    if (temp.length > 210 ) $(li).css('height','42px');
    li.innerHTML = temp;
    $(li).attr('onclick', 'return listClicked(this);');
    $(li).attr('select', '');
    return li;
}

function listClicked(d) {
    $('ul.customul li').removeAttr('select');
    $(d).attr('select', 'selected');
    var dText = typeof($('ul.customul li[select]').text()) == "string"? $('ul.customul li[select]').text().replace('<', '&lt;').replace('>', '&gt;'): $('ul.customul li[select]').text();
    $('span[selectedcustomddl]').html(dText);
    return;
}

function getMatches(text) {
    var ret = [];
    if(text && typeof(text) == "string") text = text.replace('>','&gt;').replace('<','&lt;');
    var k = dbCustomddl.filter(function (el) { return (el.label.toLowerCase().indexOf(text.toLowerCase()) != -1) || (decodeURI(el.value).toLowerCase().indexOf((text).toLowerCase()) != -1); });
    if (k.length > 100) {
        for (x in k) {
            if (x > 100) {
                break;
            }
            ret.push(k[x]);
        }
    }
    else {
        ret = k;
    }
    return ret;
}

function okClick() {
	relationcycle = [];
    if ($('ul.customul li[select]').length) {
        if($('ul.customul li[select="selected"]')[0]){
        	var selValue = $('ul.customul li[select="selected"]')[0]['attributes'][0].value;
        	var disVal = $('ul.customul li[select="selected"]').attr('displayValue');
        	var flId = $(targetSource).parent().attr("fieldid");
        	var fld = datafields[flId];
        	if (fld && fld.InputType == 5) 
        		$(targetSource).html(disVal);
       	 	$(targetSource).val(selValue);
       	 	if ($(targetSource).hasClass('classCalcAttributes') && $(targetSource).attr('calcField')) {
				var sd = $(targetSource).attr('calcField');
				
				var splitsd = sd.split(',');
				tempsRec[0] = {'cat':splitsd[0],'auxrowuid':splitsd[1]};
				if (splitsd[0] && splitsd[1]) {
					var sRec = activeParcel[splitsd[0]].filter(function(x) {return x.ROWUID == splitsd[1]})[0];
					tempactiveParcelEdits.push({ AuxROWUID: splitsd[1], ChangeId: null, FieldId: parseInt(flId), ParcelId: activeParcel.Id, QCValue: selValue, action: "edit", parentROWUID: (sRec? sRec.ParentROWUID: null) });
	            	tempsRec[1][fld.Name] = selValue;
	            	$(targetSource).parent().addClass('unsaved');
       	 	}}
       	 	else {
	       		selectOwnValue(targetSource, $(targetSource).parent().attr("fieldid"), $(targetSource).attr('auxrowuid'), null);
	        	AuxROWUID: 0
	        	ChangeId: 0
	        	FieldId: 531
	        	ParcelId: 2096
	       	 	QCValue: "003"
	       	 	action = null
       	 	}
        	clearAll();
        	setScreenDimensions();
        }
        else{
            cancelClick();
        }
    }
    else {
        cancelClick();
    }
}

function cancelClick() {
    clearAll();
    $('body').css('overflow-y','auto');
    setScreenDimensions();
}

function clearAll() {
    dbCustomddl = [];
    $('.lkShowAllStatus').html('Showing 0 records out of 0');
	$('.lkShowAlllink').show();
	$('.lkShowAllRec').hide();
    $('#searchtxt').val('');
    $('.divCodeFile').hide();
    $('.mask').hide();
    $('.match').html('');

}

function fillAllLookupList() {
	$('span[selectedcustomddl]').html(selectedText);
	$('.lkShowAlllink').hide();
    $('.lkShowAllRec').show();
    $('#searchtxt').val('');
	keyUp(null, true, dbCustomddl);
}
