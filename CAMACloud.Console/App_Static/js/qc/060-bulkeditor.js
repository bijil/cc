﻿var pageCount = 0, pageSize = 0, totalParcelCount, allParcels = true;
var aad = false
var searchvalidation = false;
function openBulkEditor() {
    closeAdhocCreator();
    isAdhocCreator = false;
    randomValue = parseInt($('.randomizer').val());
    if (randomValue != 100) {
        alert('You cannot perform Bulk Edit job when random selection is used. Set random selection to 100% and search again before using this feature.');
        return false;
    }
    try {
        $('.bulk-alert-message').attr('disabled', 'disabled');
        $('.bulk-priority').val('0');
        $('.bulk-alert-message').val("");
        $('.bulk-dtr-status').val('');
        $('.bulk-editor').show();
        //$('.bulk-editor').css({ 'height': '220px' });
        if (document.getElementById('editNotes').checked) {
            $('.bulk-editor').css({ 'height': '340px' });
        }
        else {
            $('.bulk-editor').css({ 'height': '250px' });
        }
        setScreenDimensions();
    } catch (e) {
        console.error(e);
    }
    return false;
}

function closeBulkEditor() {
    try {
        $('.bulk-alert-message').attr('disabled', 'disabled');
        $('.bulk-priority').val('0');
        $('.bulk-alert-message').val("");
        $('.bulk-editor').hide();
        $('.bulk-editor').css({ 'height': '0px' });
        clearBulkNote();
        setScreenDimensions();
    } catch (e) {
        console.error(e);
    }
    return false;
}

function clearBulkNote() {
    $('.bulk-Notes').val("");
    $('#editNotes').prop('checked', false);
    $('.bulk-Notes-row').hide();
    $('.updateBulk').removeAttr('notes');
}
function searchFilterValidate() {
    randomValue = parseInt($('.randomizer').val());
    var defaultFilter = $('.app-dtr-selected-task').length > 0 ? $('.app-dtr-selected-task').val() : -1;
    var op = $('#myandorswitch').attr('checked') ? 'AND' : 'OR';
    var Fields = $('.hDivBox tr th').map(function () {
        var fields = this.abbr + ',' + this.innerText + ',' + parseInt($(this).children('div').css('width'));
        return fields;
    }).get().join('|');

    var data = {
        query: getSearchParam(),
        searchfields: Fields,
        random: randomValue,
        dtrfilter: defaultFilter,
        export: 'csv',
        PrioTableId: localStorage.getItem("PrioTableId"),
        Operation: op
    };
    if (data.query.contains('LEFT OUTER JOIN'))
        data.CustomSearch = true;
    else
        data.CustomSearch = false;

    var stt = false;
    if (data.query != lastPostData.query || data.random != lastPostData.random || data.Operation != lastPostData.Operation || (__DTR && data.dtrfilter != lastPostData.dtrfilter)) {
        var stt = confirm("Parcel search results have changed! Please search for the parcels you would like to Bulk Edit again.");
        aad = true
        searchvalidation = false
        if (!stt) return false;
    } else {
    searchvalidation = true
    aad = false
    return
    }
}

function bulkUpdateAll() {
    searchFilterValidate()
    showMask();
    if (!aad && searchvalidation) {

        var method = $('#myandorswitch').attr('checked') ? 'AND' : 'OR';
        var customsearch;
        if (getSearchParam().contains('LEFT OUTER JOIN'))
            customsearch = true;
        else
            customsearch = false;
        $qc('search', { query: getSearchParam(), random: 100, checkDownSync: true, dtrfilter: (__DTR ? $('.app-dtr-selected-task').val() : null), PrioTableId: localStorage.getItem("PrioTableId"), Operation: method, CustomSearch: customsearch }, function (result) {
            hideMask();
            if (confirmPendingChanges(result)) {
                try {
                    allParcels = false;
                    if ($('.updateBulk').attr('notes')) {
                        bulkOp('priority-notes', $('.bulk-priority').val(), $('.bulk-alert-message').val(), $('.bulk-Notes').val());
                    }
                    else {
                        bulkOp('priority', $('.bulk-priority').val(), $('.bulk-alert-message').val());
                    }
                } catch (e) {
                    console.error(e);
                }
                return false;
            }
            else return false;
        });
    } else {
        hideMask()
        aad = false
    }
}

function bulkQC(value) {
    searchFilterValidate()
    if (!aad && searchvalidation) {

        var note = '';
        var count = 0, err_count = 0;;
        var warning_msg = '';
        var method = $('#myandorswitch').attr('checked') ? 'AND' : 'OR';
        if (value == 0)
            note = '\n\nNote: When Rejecting a parcel, parcel data changes and photos will be permanently deleted.';
        if (confirmBulkEditAction(note, value)) {
            try {
                allParcels = false;
                showMask();
                var CustomSearch;
                if (getSearchParam().contains('LEFT OUTER JOIN'))
                    CustomSearch = true;
                else
                    CustomSearch = false;
                $qc('search', { query: getSearchParam(), random: 100, checkDownSync: true, dtrfilter: (__DTR ? $('.app-dtr-selected-task').val() : null), PrioTableId: localStorage.getItem("PrioTableId"), Operation: method, CustomSearch: CustomSearch }, function (result) {
                    totalParcelCount = parseInt(result.total);
                    pageSize = 100;
                    if (value == 1) pageSize = 50;
                    pageCount = parseInt(totalParcelCount / pageSize) + (((totalParcelCount / pageSize) % 1 > 0) ? 1 : 0);
                    hideMask();
                    try {
                        var warning_flag = false, error_flag = false;
                        if (result.downSyncCheck.length > 0) {
                            if (result.downSyncCheck[0].notReviewedCount > 0) {
                                warning_flag = true;
                                count = result.downSyncCheck[0].notReviewedCount
                            }
                            if (result.downSyncCheck[0].CC_error_count > 0) {
                                err_count = result.downSyncCheck[0].CC_error_count;
                                error_flag = true;
                            }
                        }
                        warning_msg = "There are " + count + " parcels that have not been marked as complete. If you continue, then YOUR USER and TODAY'S DATE will be set in the Reviewed By and Reviewed On fields within the system, and applicable fields in your CAMA database as well.";
                        err_warnig = err_count + " parcels were not approved due to errors. Do you want to view the list?"
                        var erroredParcels = function (c) {
                            if (value == 1 && error_flag == true) {
                                if (confirm(err_warnig) == true) {
                                    if (currentSearchTemplate.Filters.filter(function (x) { return x.Label == 'CC Error' }).length < 1) {
                                        var err_filter = [new Filter("CC Error", "~CC_Error", "eq", "", null)];
                                        loadFilters(err_filter, true);
                                        $('.qc-tabs').tabs("select", 0);
                                    }
                                    if (c) c();
                                }
                            }
                        }
                        if (value == 1 && warning_flag == true) {
                            if (confirm(warning_msg) == true) {
                                bulkOp('qc', value, null, null, erroredParcels);
                                return true;
                            }
                            else
                                return false;
                        }
                        else {
                            bulkOp('qc', value, null, null, erroredParcels);
                        }
                    } catch (e) {
                        console.error(e);
                    }
                });

            } catch (e) {
                console.error(e);
            }
        }
        else {
            return false;
        }
    } else {
        return false;
    }
}

function bulkReview(value) {
    searchFilterValidate()
    if (searchvalidation) {
        showMask();
        var method = $('#myandorswitch').attr('checked') ? 'AND' : 'OR';
        var CustomSearch;
        if (getSearchParam().contains('LEFT OUTER JOIN'))
            CustomSearch = true;
        else
            CustomSearch = false;
        $qc('search', { query: getSearchParam(), random: 100, checkDownSync: true, dtrfilter: (__DTR ? $('.app-dtr-selected-task').val() : null), PrioTableId: localStorage.getItem("PrioTableId"), Operation: method, CustomSearch: CustomSearch }, function (result) {
            hideMask();

            if (confirmPendingChanges(result, 'BulkReview')) {
                try {
                    allParcels = false;
                    bulkOp('review', value);

                } catch (e) {
                    console.error(e);
                }
            }
        });
    } else {
    return false}
}

function bulkNotes(value) {
    searchFilterValidate()
    if (searchvalidation) {

        if (confirmBulkEditAction()) {
            try {
                allParcels = true;
                bulkOp('note', 0);
            } catch (e) {
                console.error(e);
            }
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}
function bulkFieldAlerts(value) {
    searchFilterValidate()
    if (searchvalidation) {
        if (confirmBulkEditAction()) {
            try {
                allParcels = true;
                bulkOp('fieldAlert', 0);
            } catch (e) {
                console.error(e);
            }
        }
        else {
            return false;
        }
    }else {
        return false;
    }
}

function bulkDTRStatus(value) {
    value = $('.bulk-dtr-status').val();
    if (confirmBulkEditAction()) {
        try {
            allParcels = true;
            bulkOp('dtrstatus', value);
        } catch (e) {
            console.error(e);
        }
    }
    else {

        return false;
    }
}

function bulkOp(op, value1, value2, value3, callback, pageIndex, isIndexing) {
    showMask();
    var method = $('#myandorswitch').attr('checked') ? 'AND' : 'OR';
    var clearAlertFlag = $('#chk_clearAlertMsg').is(':checked') ? 1 : 0;
    $('#progressBar').show();
    if (!pageIndex)
        pageIndex = 1;
    var pripage = "false";
    if (window.location.href.contains("PrioPage=") && op == "priority") {
        pripage = "true";
    }
    var data = {
        query: getSearchParam(),
        operation: op,
        value1: value1,
        value2: value2,
        value3: value3,
        clearAlertFlag: clearAlertFlag,
        page: !allParcels ? (isIndexing ? pageIndex : 1) : 0,
        pagesize: !allParcels ? pageSize : -1,
        PrioTableId: localStorage.getItem("PrioTableId"),
        Method: method,
        pripage: pripage
    };
    if (data.query.contains('LEFT OUTER JOIN'))
        data.CustomSearch = true;
    else
        data.CustomSearch = false;
    if (__DTR) {
        var defaultFilter = $('.app-dtr-selected-task').val();
        data.dtrfilter = defaultFilter;
    }

    $qc('bulkjob', data, function (resp) {
        var isIndexing = (resp.Record == totalParcelCount) ? true : false;
        var progress = pageIndex / pageCount * 100;
        progress = (progress > 100) ? 100 : progress;
        $('#currentProgress').width(progress + '%');
        if (((pageIndex < pageCount) || (!isIndexing && pageCount == 2 && pageIndex == 2)) && !allParcels) {
            pageCount = (!isIndexing && pageCount == 2 && pageIndex == 2) ? 1 : pageCount;
            bulkOp(op, value1, value2, value3, callback, ++pageIndex, isIndexing);
        }
        else {
            $('#currentProgress').width('0%');
            $('#progressBar').hide();
            hideMask();
            notification("Bulk edit process finished successfully");
            $('#chk_clearAlertMsg').prop('checked', false);
            if (($('.bulk-priority').val() != '0') && !$('#chk_clearAlertMsg').is(':checked')) {
                $('.bulk-alert-message').removeAttr('disabled');
            } else {
                $('.bulk-alert-message').attr('disabled', 'disabled');
            }
            if (callback) callback(doSearch);
            $('.parcel-results').Refresh();
        }
    });
}

function confirmBulkEditAction(note, value) {
    var msg = "All parcels in the search results will be updated. Are you sure you want to continue?";
    if (value == 2) msg = "All Approved parcels in the search results will be updated. Are you sure you want to continue?";
    if (note)
        msg += note;
    if (confirm(msg) == true)
        return true;
    else
        return false;
}

function confirmPendingChanges(result, type) {
    totalParcelCount = parseInt(result.total);
    pageSize = 100;
    pageCount = parseInt(totalParcelCount / pageSize) + (((totalParcelCount / pageSize) % 1 > 0) ? 1 : 0);
    var fieldCount = 0, qcCount = 0, tempQC, tempField, message = "", subMessage1 = "", subMessage2 = "";
    fieldCount = result.downSyncCheck.length > 0 ? result.downSyncCheck[0].fieldCount : 0;
    qcCount = result.downSyncCheck.length > 0 ? result.downSyncCheck[0].qcCount : 0; qcCount = qcCount || 0;
    subMessage1 = "The field status of these properties will be reset";
    subMessage2 = "The QC status of these properties will be reset";
    if (type == 'field-review') {
        subMessage1 = subMessage2 = "The priority of these properties will be adjusted";
    }
    if (fieldCount > 0 && type != 'BulkReview') message = fieldCount + " properties in this priority list have pending changes that have not been synced back to " + result.dbName + " database.\n " + subMessage1 + " and the pending changes will remain only in the cloud.\n\n";

    if (qcCount > 0 && type != 'BulkReview') message += qcCount + " properties in this priority list have pending changes that have not been synced back to " + result.dbName + " database.\n " + subMessage2 + " and the pending changes will remain only in the cloud.\n\n";

    if (fieldCount > 0 || qcCount > 0) {
        message += "Are you sure you want to proceed?";
        if (!confirm(message)) {
            hideMask();
            notification("Bulk edit process cancelled by the user", "CAMA Clouds notification");
            return false;
        }
        else return true;
    }
    if (confirmBulkEditAction()) return true;
    else return false;
}