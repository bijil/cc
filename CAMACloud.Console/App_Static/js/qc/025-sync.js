﻿var tlData =
            [
                ['Field', 'tquery', { t: 'Field'}],
                ['FieldSettings', 'tquery', { t: 'FieldSettings'}],
                ['ImportSettings', 'tquery', { t: 'ImportSettings'}],
                ['SketchSettings', 'tquery', { t: 'SketchSettings' }],
                ['LookupValue', 'tquery', { t: 'LookupValue'}],
                ['*ld*', 'lookupdatatables', { pageIndex: 0, tableIndex: 0 }],
                ['Neighborhood', 'tquery', { t: 'Neighborhood'}],
                ['FieldCategory', 'tquery', { t: 'FieldCategory'}],
                ['CategorySettings', 'tquery', { t: 'CategorySettings'}],
                ['ParentChild ', 'tquery', { t: 'ParentChildTable'}],
                ['ClientValidation', 'tquery', { t: 'ClientValidation'}],
                ['TableKeys', 'tquery', { t: 'TableKeys'}]

            ];

function DownloadAppData(callback) {
    clearDatabase(function () {
        ClearTablesFromList(tlData, function () {
            DownloadList(tlData, function () {
                window.localStorage.setItem(localDBName + '-last-cache-time', (new Date()).getTime());
                afterDownloadData(callback);
            }, 'Downloading Application data ...');
        });
    });
}

function updateSqlLiteDataInIndexedDB(callback) {
    if (typeof (openDatabase) == 'undefined') {
        let transaction = indxDB.db.transaction(['sqliteData'], 'readwrite');
        let objectStore = transaction.objectStore('sqliteData');
        let exportedData = db.export();
        let newUint8Array = new Uint8Array(exportedData);
        getFirstIndexedSqlLiteId(objectStore, (ixId) => {
            if (ixId) {
                let putRequest = objectStore.put({ id: ixId, data: newUint8Array });
                putRequest.onsuccess = function () {
                    if (callback) callback();
                };
                putRequest.onerror = function (event) {
                    console.error(`Error updating data in sqliteData object store:`, event.target.error);
                    if (callback) callback();
                };
            }
            else {
                objectStore.add({ data: newUint8Array });
                if (callback) callback();
            }
        });
    }
    else if (callback) callback();
}

function clearSqlLiteDataFromIndexedDB(callback) {
    if (typeof (openDatabase) == 'undefined') {
        let transaction = indxDB.db.transaction(['sqliteData'], 'readwrite');
        let objectStore = transaction.objectStore('sqliteData');

        getFirstIndexedSqlLiteId(objectStore, (ixId) => {
            if (ixId) {
                objectStore.delete(ixId);
                if (callback) callback();
            }
            else if (callback) callback();
        });
    }
    else if (callback) callback();
}

function getFirstIndexedSqlLiteId(objectStore, callback) {
    objectStore.openCursor().onsuccess = function (event) {
        let cursor = event.target.result;
        let firstRowId = cursor ? cursor.primaryKey : null;
        if (callback) callback(firstRowId);
    };
}

function afterDownloadData(callback) {
    updateSqlLiteDataInIndexedDB(() => {
        if (callback) callback();
    });
}

function DownloadList(list, callback, title) {
    syncProgressList = [];
    syncPageCounter = {};
    jobAttemptCounter = {};
    partCounter = 0;
    totalParts = 0;
    noOfPages = 1;
    if (title == null)
        title = 'Downloading ...';
    //    loadScreen('progress');
    //setProgress(0);
    // showProgress();
    // setProgressLabel(title);
    var copylist = list.slice(0);
    syncTables(title, copylist.reverse(), copylist.length, callback);
}

function ClearTablesFromList(list, callback) {
    _clearTablesFromlist(list.slice(0), callback);
}

function _clearTablesFromlist(list, callback) {
    if (list.length == 0) {
        if (callback) callback();
        return;
    }
    var tdef = list.pop();
    var tableName = tdef[0];
    db.transaction(function (tx) {
        log('Dropping table ' + tableName);
        if ((tableName == '') || (tableName == '*') || (tableName == '*ld*')) {
            _clearTablesFromlist(list, callback);
        } else {
            if (tableName == "ParcelChanges") {
                tx.executeSql('DELETE FROM ' + tableName + ' WHERE Synced = 1', [], function (tx, res) {
                    _clearTablesFromlist(list, callback);
                });
            } else {
                tx.executeSql('DROP TABLE IF EXISTS ' + tableName, [], function (tx, res) {
                    _clearTablesFromlist(list, callback);
                });
            }
        }

    });
}

function syncTables(jobtitle, tableDownloadList, downloadCount, callback, tableDef) {
    var tdef = null, skipProgress = false;
    if (tableDef == null) {
        tdef = tableDownloadList.pop();
    }
    else {
        tdef = tableDef;
    }

    if (tdef == null) {
        log('Reached here..');
        if (callback) callback();
    }
    else {

        var params = tdef[2], tableIndex, pageIndex;
        var tableName = tdef[0];
        if (params == null)
            params = {};
        if (tableName != '') {
            if (syncPageCounter[tableName] == null) {
                syncPageCounter[tableName] = 1;
            } else {
                syncPageCounter[tableName] += 1;
            }
            params["page"] = syncPageCounter[tableName];
        } else {
            params["page"] = 1;
        }
        var jobName = 'sync-' + tableName + params["page"];
        if (jobAttemptCounter[jobName] == null) {
            jobAttemptCounter[jobName] = 1;
        } else {
            jobAttemptCounter[jobName] += 1;
        }
		
		if (tableName == "*ld*") {
        	pageIndex = params.pageIndex;
        	tableIndex = params.tableIndex;
        }
		
        $qc(tdef[1], params, function (data) {
            var processor = _syncTable;
            if ((tableName == "*") || (tableName == "*ld*")) {
                processor = _syncSubTables;
            }
            
            if (tableName == "*ld*") {
				if (!data.status) { 
					skipProgress = true;
					if (data.length == 2000) 
						tableDownloadList.push(['*ld*', 'lookupdatatables', { pageIndex: ++pageIndex, tableIndex: tableIndex }])
					else tableDownloadList.push(['*ld*', 'lookupdatatables', { pageIndex: 0, tableIndex: ++tableIndex }])
				}
				else if (data.status == 'completed') { skipProgress = false; partCounter += 1; }
			}
            
            // console.log(data, params, tableName);
            if ((data && data.length != undefined) && (tableName != '')) {
                if (!skipProgress) partCounter += 1;
                if (syncPageCounter[tableName] <= noOfPages || (tableName == '*ld*')) {
                    //setProgressLabel(jobtitle + " (" + partCounter + ")");
                    var paged = false;
                    if ((tableName == "Parcel") || (tableName == "ParcelChanges") || (tableName == "MapPoints") || (tableName == "*") || (tableName == "Images") || (tableName == "Comparables") || (tableName == "ParcelLinks")) {
                        paged = true;
                    } else {
                        tdef = null;
                    }

                    if (data.length == 0) {
                        // calculateProgress(tableDownloadList, downloadCount);
                        syncTables(jobtitle, tableDownloadList, downloadCount, callback, tdef);
                    } else {
                        processor(tableName, data, function () {
                            // calculateProgress(tableDownloadList, downloadCount);
                            syncTables(jobtitle, tableDownloadList, downloadCount, callback, tdef);
                        });
                    }
                } else {
                    //  calculateProgress(tableDownloadList, downloadCount);
                    syncTables(jobtitle, tableDownloadList, downloadCount, callback);
                }
            }
            else {
                if (data.Pages) {
                    noOfPages = data.Pages;
                    totalParts = 7 * (noOfPages + 1) + 2 + 2;
                }
                syncTables(jobtitle, tableDownloadList, downloadCount, callback);
            }

        }, function (req, err, msg) {
            //console.log(req, err, msg);
            if (tableName == "*") {
                syncTables(jobtitle, tableDownloadList, downloadCount, callback);
            } else {
                if (jobAttemptCounter[jobName] < 2) {
                    log('Re-attempting job ' + jobName);
                    if (tableDef == null) {
                        tableDownloadList.push(tdef);
                    }

                    if (tableName != '') {
                        syncPageCounter[tableName] -= 1
                        if (syncPageCounter[tableName] == 0) {
                            syncPageCounter[tableName] = null;
                        }
                    }

                    syncTables(jobtitle, tableDownloadList, downloadCount, callback, tableDef);
                }
                else {
                    console.log(err);

                    alert('Data synchronization failed!');
                    syncNbhd = '';
                }
            }
        });
    }
}


function _getColumnInformation(firstRow, callBack, tableName) {
    var cKeys = [];
    var columns = "";
    var createColumns = "";
    var phs = "";
    for (key in firstRow) {
        var df = getDataField(key, tableName);

        cKeys.push(key);
        var type = "TEXT"

        //        if (key == "Index") {
        //            key = "SortIndex"
        //            type = "INTEGER";
        //        }
        if (key == "Id" || /^.*?Id$/.test(key) || /^.*?_id$/.test(key) || /^.*?UID$/.test(key)) {
            type = "INTEGER"
        }
        if (key == "Synced") {
            type = "INTEGER"
        }
        if (firstRow[key] != null) {
            if ((firstRow[key].toString() == 'true') || (firstRow[key].toString() == 'false')) {
                type = "BOOL";
            }
        }
        if (df) {
            switch (parseInt(df.InputType)) {
                case 1: type = "TEXT"; break;
                case 2: type = "FLOAT"; break;
                case 3: type = "BOOL"; break;
                case 4: type = "DATETIME"; break;
                case 7: type = "FLOAT"; break;
                case 8: type = "INT"; break;
                case 9: type = "INT"; break;
                case 10: type = "INT"; break;
                default: type = "TEXT"; break;
            }
        }
        if (columns == "") {
            columns = key;
            createColumns = "[" + key + "]" + " " + type;
            phs = "?";
        }
        else {
            columns += ", " + key;
            createColumns += ", " + "[" + key + "]" + " " + type;
            phs += ", ?"
        }
    }

    var r = new Object();
    r.Columns = columns;
    r.CreateColumns = createColumns;
    r.PlaceHolders = phs;
    r.ColumnKeys = cKeys;

    if (callBack) callBack(r);
}



function _parseDataForInsert(parcel, firstRow, cinfo) {      
    var pa = [];
    for (key in parcel) {
        if (cinfo.ColumnKeys.indexOf(key) > -1) {                        
            var dat;
            dat = parcel[key];
            //console.log(TrueFalseInPCI);
            if (TrueFalseInPCI=="False") {
                if (dat == "True" || dat == "true") dat = true;
                if (dat == "False" || dat == 'false') dat = false;
            }
            
            
            if (key == "Index") {
                dat = parseInt(dat);
            }
            dat = dat === null ? dat : dat.toString();
            pa.push(dat);
        }
    }
    return pa;
}

function _loadParcelColumnInformation(data, callback, tableName) {
    getData('SELECT * FROM Field', [], function (fields) {
        var columns = [];
        var cKeys = [];
        for (x in fields) {
            f = fields[x];
            if ((f.ImportTypeId === undefined) || (f.ImportTypeId == 0)) {
                cKeys.push(f.Name);
                var type = "";
                switch (f.InputType) {
                    case "2":
                        type = " FLOAT";
                        break;
                    case "3":
                        type = " BOOL";
                        break;
                }
                columns["___" + f.Name] = type;
            }
        }
        for (key in data) {
            var dkey = key;
            dkey = "___" + key;
            if (cKeys.indexOf(key) == -1)
                cKeys.push(key);
            if (columns[dkey] === undefined) {
                var type = ""
                if (key == "Id")
                    type == " INTEGER KEY"
                columns[dkey] = type;
            }
        }

        var createColumns = "";
        var colcheck = [];
        for (key in columns) {
            key = key.replace("___", "")
            if (colcheck.indexOf(key.toLowerCase()) == -1) {
                if (createColumns != "") createColumns += ", ";
                createColumns += key + columns["___" + key];
                colcheck.push(key.toLowerCase());
            }
        }

        createColumns += ",DistanceFromMe FLOAT,DistanceIndex INTEGER"
        var r = new Object();
        r.CreateColumns = createColumns;
        r.ColumnKeys = cKeys;

        if (callback) callback(r);
    });
}

var subTables = [];
function _syncSubTables(tableName, data, callback) {
    if (data.length == 0) {
        //log('No data downloaded as sub tables.');
        if (callback) callback();
    }
    var filtered = {};
    for (x in data) {
        var item = data[x];
        var dsName = item.DataSourceName;
        if (filtered[dsName] == null) {
            filtered[dsName] = [];
        }
        filtered[dsName].push(item);
    }
    _syncSubTableFromList(filtered, callback, (tableName == "*ld*" ? true : false));
}

function _syncSubTableFromList(filtered, callback, islkTbl) {
    if (Object.keys(filtered).length == 0) {
        if (callback) callback();
    }
    for (var key in filtered) {
        _syncTable(key, filtered[key], function () {
            delete filtered[key];
            _syncSubTableFromList(filtered, callback, islkTbl);
        }, islkTbl);
        break;
    }
}

var columnDir = {};

function _syncTable(tableName, data, callback, islkTbl) {
    log("Downloaded table: " + tableName + ", " + data.length + " rows.");
    if (data.length == 0) {
        syncProgressList.push(tableName);
        if (callback) callback();
        return;
    }
    if (syncProgressList.indexOf(tableName) > -1) {
        syncProgressList.push(tableName);
        _syncData(tableName, data, columnDir[tableName], callback, islkTbl);
        return;
    }

    if (tableName == "Parcel") {
        parcelCount = data.length;
        localStorage.setItem("pcount", parcelCount);
    }
    if (data.length > 0) {
        db.transaction(function (x) {
            var dropAction = "DROP TABLE IF EXISTS " + tableName;
            var dropConfirm = tableName + " dropped.";
            if (tableName == "ParcelChanges") {
                dropAction = "DELETE FROM ParcelChanges WHERE Synced = 1"; dropConfirm = 'Synced parcel cleared.'
            }
            executeSql(x, dropAction, [], function (x) {
                //console.log(dropConfirm);
                if (data.length > 0) {
                    var firstRow = data[0];
                    var tableRenderer;
                    if (tableName == "Parcel")
                        tableRenderer = _loadParcelColumnInformation;
                    else
                        tableRenderer = _getColumnInformation;
                    tableRenderer(firstRow, function (cInfo) {
                        columnDir[tableName] = cInfo;
                        syncProgressList.push(tableName);
                        _syncData(tableName, data, cInfo, callback, islkTbl);
                    }, tableName);
                }
            });
        });

    }
}

function _syncData(tableName, data, cinfo, callback, islkTbl) {
    var createSql = "CREATE TABLE IF NOT EXISTS " + tableName + " (" + cinfo.CreateColumns + ");";
    db.transaction(function (x) {
        executeSql(x, createSql, [], function (tx) {
            //log("Loading local table " + tableName + ".");
            if (islkTbl && data.length == 1 && data[0].ROWUID == -9999) {
                if (callback) callback();
                return;
            }

            var firstRow = data[0];
            var progress = 0;
            for (i in data) {
                var parcel = data[i];
                var total = data.length;
                var pa = _parseDataForInsert(parcel, firstRow, cinfo);
                var columns = "";
                var phs = "";
                for (key in parcel) {
                    if (cinfo.ColumnKeys.indexOf(key) > -1) {
                        if (key == "Index") key = "SortIndex";
                        if (columns == "") { columns = "[" + key + "]"; phs = "?"; }
                        else { columns += ", " + "[" + key + "]"; phs += ", ?"; }
                    }
                }
                var insertSql = "INSERT INTO " + tableName + " (" + columns + ") VALUES (" + phs + ")";
                executeSql(tx, insertSql, pa, function (tx, res) {
                    progress += 1;
                    if (progress == total) {
                        //log("Finished creating local table " + tableName + ".");
                        if (callback) callback();
                    }
                }, function (e, m) {
                    log(m.message, true);
                });
            }
        });
    });


}
