﻿//    DATA CLOUD SOLUTIONS, LLC ("COMPANY") CONFIDENTIAL 
//    Unpublished Copyright (c) 2010-2013 
//    DATA CLOUD SOLUTIONS, an Ohio Limited Liability Company, All Rights Reserved.

//    NOTICE: All information contained herein is, and remains the property of COMPANY.
//    The intellectual and technical concepts contained herein are proprietary to COMPANY
//    and may be covered by U.S. and Foreign Patents, patents in process, and are protected
//    by trade secret or copyright law. Dissemination of this information or reproduction
//    of this material is strictly forbidden unless prior written permission is obtained
//    from COMPANY. Access to the source code contained herein is hereby forbidden to
//    anyone except current COMPANY employees, managers or contractors who have executed
//    Confidentiality and Non-disclosure agreements explicitly covering such access.</p>

//    The copyright notice above does not evidence any actual or intended publication
//    or disclosure of this source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
//    DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
//    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
//    IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
//    OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
//    TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
//    ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.


var appUpdating = false;
var firstTimeCached = false;
var datafieldsArray = [];

function initApplicationCache() {

    var updateMessage = 'Updating application ...';


    if (window.applicationCache) {
        if (window.applicationCache.status == 0) {
            updateMessage = 'Installing application ...';
        }

        applicationCache.addEventListener('updateready', function () {
            hideCacheProgress();
            if (window.location.hash != '#repairing') {
                messageBox('An update is available for this application. Reload now?', ["OK", "Cancel"], function () {
                    window.location.reload();
                }, function () {
                    appUpdating = false;
                    hideSplash();
                });
            }

        });


        applicationCache.addEventListener('cached', function () {
            hideCacheProgress();
            if (window.location.hash != '#repairing') {
                messageBox('You have successfully completed installation of MobileAssessor. The application will restart now.', function () {
                    window.location.reload();
                });
            }
        });

        applicationCache.addEventListener('error', function (e) {
            hideCacheProgress();
            console.log('Error in application cache ', e);
        });

        applicationCache.addEventListener('noupdate', function (e) {
            hideCacheProgress();
            log('Application Cache: No updates');
        });

        applicationCache.addEventListener('progress', function (e, f, g) {
            appUpdating = true;
            //log('Updating Cache: ' + parseInt(e.loaded / e.total * 100) + '% completed');
            showSplash();
            showCacheProgress(e.loaded / e.total);
            setSplashProgress(updateMessage, (e.loaded / e.total * 100));
        });
    }
}

function hideCacheProgress() {
    $('.cache-update-bar').hide();
    $('.ma-version').show();
    $('.cache-update-progress').css({ 'background-position': '0px 0px' });
}

function showCacheProgress(percent) {
    $('.cache-update-bar').show();
    $('.ma-version').hide();
    var pgwidth = parseInt($('.cache-update-progress').width() * percent);
    $('.cache-update-progress').css({ 'background-position': pgwidth + 'px 0px' });
}

function loadLookup(callback) {
    log('Caching Lookup Table ...');
    lookup = {};
    ccma.Data.LookupMap = {};
    //getData("SELECT CASE WHEN F.Name IS NULL THEN LV.Source ELSE F.Name END As FieldName, LV.Name, LV.Value, LV.Description, LV.Ordinal, F.SortType FROM LookupValue LV LEFT OUTER JOIN Field F ON (F.LookupTable = LV.Source) ORDER BY 1, LV.Ordinal", [], function (lv) {
    getData("SELECT Id, Name, LookupTable FROM Field WHERE InputType = 5 or InputType = 11", [], function (lmap) {
        for (var i in lmap) {
            ccma.Data.LookupMap[lmap[i].Name] = { LookupTable: lmap[i].LookupTable, IdMap: false };
            ccma.Data.LookupMap['#FIELD-' + lmap[i].Id] = { LookupTable: lmap[i].LookupTable, IdMap: true };
        }
        getData("SELECT * FROM LookupValue", [], function (lv) {
            for (var x in lv) {
                var f = lv[x].Source;
                var v = lv[x].Value.toString().trim().length > 0 ? lv[x].Value.toString().trim() : lv[x].Value.toString();
                var n = lv[x].Name;
                var d = lv[x].Description;
                var o = parseInt(lv[x].Ordinal);
                var t = lv[x].SortType;
                if (lookup[f] === undefined) {
                    lookup[f] = {};
                }
                var color = lv[x].Color? lv[x].Color: '';
                //                if (typeof v == "number" || !isNaN(v)) {
                //                    v = parseFloat(v).toString();
                //                }
                var NumericValue = lv[x].NumericValue;
                if (lookup[f][v] === undefined)
                    lookup[f][v] = { Name: n, Description: d, Ordinal: o, SortType: t, Id: v, color: color, NumericValue: NumericValue, AdditionalValue1: lv[x].AdditionalValue1, AdditionalValue2: lv[x].AdditionalValue2};
            }
            log('Loaded ' + lv.length + ' lookup items in memory ...');
            if (callback) callback();
        }, null, true, function () {
            //No Lookup Tables
            if (callback) callback();
        });

        // if (callback) callback();
    }, null, false, null, true);

}

function loadFieldAlertTypes(callback) {
    log('Caching Field Alert Types ...');
    fieldAlerts = [{ Id: 0, Name: 'No Flag' }];
    getData("SELECT * FROM FieldAlerts ORDER BY Id", [], function (vals) {
        for (var x in vals) {
            var v = vals[x].Id;
            var d = vals[x].Name;
            var temp = { Id: v, Name: d };
            fieldAlerts.push(temp);
        }
        log('Loaded ' + fieldAlerts.length + ' Validations in memory ...');

        if (callback) callback();
    }, null, false, function () {
        if (callback) callback();
    });
}

function loadValidations(callback) {
    log('Caching Validation Table ...');
    ccma.UI.Validations = [];
    getData("SELECT * FROM ClientValidation ORDER BY Ordinal", [], function (vals) {
        for (var x in vals) {
            if (isTrue(vals[x].Enabled)) {
                var v = vals[x].Name;
                var n = decodeHTML(vals[x].Condition);
                var d = vals[x].ErrorMessage.replace( new RegExp( '&gt;', 'g' ), '>' ).replace( new RegExp( '&lt;', 'g' ), '<' );
                var o = vals[x].SourceTable;
                var vfo = vals[x].ValidateFirstOnly;
                var temp = { Name: v, Condition: n, ErrorMessage: d, SourceTable: o, FirstOnly: vfo, IsSoftWarning: (vals[x].IsSoftWarning? vals[x].IsSoftWarning: 'false') };
                ccma.UI.Validations.push(temp);
            }
        }
        log('Loaded ' + ccma.UI.Validations.length + ' Validations in memory ...');

        if (callback) callback();
    }, null, true, function () {
        if (callback) callback();
    });
}

function loadTableKeys(callback) {
    log('Caching Table Keys...');
    tableKeys = [];
    getData("SELECT * FROM TableKeys ", [], function (vals) {
        for (var x in vals) {
            var v = vals[x].Name;
            var n = vals[x].SourceTable;
            var temp = { Name: v, SourceTable: n };
            tableKeys.push(temp);
        }
        log('Loaded ' + tableKeys.length + ' tableKeys in memory ...');

        if (callback) callback();
    }, null, false, function () {
        if (callback) callback();
    });
}

function loadImportSettings(callback) {
    log('Caching Settings Table ...');
    getData("SELECT  Name,Value FROM  ImportSettings", [], function (vals) {
        for (var x in vals) {

            var n = vals[x].Name;
            var v = vals[x].Value;
            clientSettings[n.toString()] = v;

        }
        log('Loaded ' + clientSettings.length + ' Import Settings in memory ...');
        if (callback) callback();
    }, null, true, function () {
        if (callback) callback();
    });

}

function loadSketchSettings(callback) {
    log('Caching Settings Table ...');
    getData("SELECT  Name,Value FROM  SketchSettings", [], function (vals) {
        for (var x in vals) {

            var n = vals[x].Name;
            var v = vals[x].Value;
            sketchSettings[n.toString()] = v;

        }
        log('Loaded ' + sketchSettings.length + ' Sketch Settings in memory ...');
        if (callback) callback();
    }, null, true, function () {
        if (callback) callback();
    });

}

function loadAuxTableNames(callback) {
    log('Caching AuxTable Names ...');
    auxTables = [];
    getData("SELECT DISTINCT SourceTable FROM FieldCategory", [], function (auxt) {

        for (var x in auxt) {
            auxTables.push(auxt[x].SourceTable);
        }
        log('Loaded ' + auxTables.length + ' Auxtable Names in memory ...');

        if (callback) callback();
    }, null, false, function () {
        if (callback) callback();
    });
}

function loadParentChild(callback) {
    log('Caching ParentChild Table ...');
    parentChild = [];
    getData("SELECT * FROM ParentChild ORDER BY ParentTable", [], function (vals) {
        for (var x in vals) {
            var v = vals[x].ParentTable;
            var n = vals[x].ChildTable;
            var temp = { ParentTable: v, ChildTable: n };
            parentChild.push(temp);
        }
        log('Loaded ' + parentChild.length + ' Parent Childs in memory ...');

        if (callback) callback();
    }, null, true, function () {
        if (callback) callback();
    });
}

function loadNeighborhoods(callback) {
    log('Caching Neighborhood ...');
    getData("SELECT * FROM Neighborhood", [], function (results) {
        for (x in results) {
            n = results[x];
            if (n.Number)
                neighborhoods[n.Number.toString()] = [n];
        }
        log('Loaded ' + results.length + ' neighborhood items in memory ...');
        if (callback) callback();
    }, null, true, function () {
        if (callback) callback();
    });
}

function loadFields(callback) {
    log('Caching Fields ...');
    getData("SELECT * FROM Field", [], function (results) {
        for (var x in results) {
            var n = results[x];
            var settings={};
            for (var f in n) {
                if (n[f] == 'true') n[f] = true;
                if (n[f] == 'false') n[f] = false;
            }
             datafields[n.Id.toString()] = n;
            var temp = datafieldsettings.filter(function(f){return f.FieldId == n.Id});
            if(temp.length > 0){
            	for(x in temp)
					settings[temp[x]["PropertyName"]] = temp[x]["Value"]
            	datafields[n.Id.toString()].UI_Settings= settings;
            }
        }

        datafieldsArray = Object.keys(datafields).map(function (x) { return datafields[x] });
        if (callback) callback();
        log('Loaded ' + results.length + ' fields in memory ...');
    });
}
var datafieldsettings=[]
function loadFieldsettings(callback) {
    log('Caching Fieldssettings ...');
    getData("SELECT * FROM FieldSettings", [], function (results) {
        for (var x in results) {
            var n = results[x];
            datafieldsettings[n.Id.toString()] = n;
        }
        if (callback) callback();
    });
}
var categorySettings=[];
function loadCategorySettings(callback) {
    log('Caching CategorySettings ...');
    getData("SELECT * FROM CategorySettings", [], function (results) {
        for (var x in results) {
            var n = results[x];
            categorySettings[n.Id.toString()] = n;
        }
        if (callback) callback();
    });
}

function loadCategories(callback){
    getData( "SELECT * FROM FieldCategory", [], function ( fc ) {
        for ( var x in fc ){
            if ( fc[x].HideExpression && fc[x].HideExpression != '' )
                fc[x].HideExpression = fc[x].HideExpression.replace( new RegExp( '&gt;', 'g' ), '>' ).replace( new RegExp( '&lt;', 'g' ), '<' )
            var fci = fc[x];
            var settings={};
             var temp = categorySettings.filter(function(f){return f.CategoryId == fci.Id});
            if(temp.length > 0){
            	for(x in temp)
					settings[temp[x]["PropertyName"]] = temp[x]["Value"]
            	fci.UI_CateogorySettings= settings;
            }
            fieldCategories.push( fci );
        }
        if ( callback ) callback();
    } );
}

function loadValidationTablesForVS8(callBack) {
    let arr = [];
    ccma.FieldValidations = {};
    getData("SELECT COMPONENT_ID CODE, DESCRIPTION, INPUT_DEFINITION_NUMBER, INPUT_TIP_TEXT, MAX_VALUE, MIN_VALUE, INPUT_REQUIREMENT_TYPE_ID, DECIMAL_ALLOWED FROM COMPONENT_INPUT_DEFINITION", [], (results) => {
        for (let x in results) {
            let item = results[x];
            if (parseInt(item.INPUT_DEFINITION_NUMBER) < 4) arr.push({ CODE: item.CODE, FIELD_TO_VALIDATE: 'MVA_INPUT_' + item.INPUT_DEFINITION_NUMBER, DESCRIPTION: item.DESCRIPTION, INPUT_TIP_TEXT: item.INPUT_TIP_TEXT, MIN_VALUE: item.MIN_VALUE, MAX_VALUE: item.MAX_VALUE, DECIMAL_ALLOWED: item.DECIMAL_ALLOWED, IS_REQUIRED: (item.INPUT_REQUIREMENT_TYPE_ID == '1' ? true : false) });
        }

        ccma.FieldValidations['COMPONENT_INPUT_DEFINITION'] = arr;
        arr = [];
        getData("SELECT CODE, PERCENT_REQUIREMENT_TYPE_ID, PERCENT_INPUT_TEXT INPUT_TIP_TEXT, PERCENT_MAXIMUM MAX_VALUE, PERCENT_MINIMUM MIN_VALUE FROM COMPONENT  WHERE PERCENT_REQUIREMENT_TYPE_ID IN (1, 2)", [], (results) => {
            for (let x in results) {
                let item = results[x];
                arr.push({ CODE: item.CODE, FIELD_TO_VALIDATE: 'MVA_PERCENTAGE', INPUT_TIP_TEXT: item.INPUT_TIP_TEXT, MIN_VALUE: item.MIN_VALUE, MAX_VALUE: item.MAX_VALUE, PERCENT_REQUIREMENT_TYPE_ID: item.PERCENT_REQUIREMENT_TYPE_ID });
            }

            ccma.FieldValidations['COMPONENT'] = arr;
            callBack && callBack();
        });
    });
}
