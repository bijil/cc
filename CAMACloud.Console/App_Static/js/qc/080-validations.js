﻿

function showRequiredFields() {
    try {
        var requireFields;
        if (__DTR)
            requireFields = datafieldsArray.filter(function (d) { return (d.IsRequiredDTROnly || d.IsRequired); });
        else
            requireFields = datafieldsArray.filter(function (d) { return (d.IsRequired); });
        if (requireFields.length > 0) {
            for (var x in requireFields) {
                $('.parcel-field-values td[fieldid="' + requireFields[x].Id + '"]').siblings().children('.required').show();
            }
        }
    }
    catch (e) {
        console.log(e.message);
    }
}
var failedObj=[];
function checkRequiredSum(currentParcel){
	if(!currentParcel) return;
	var sumFailedCatRec=[];
	var msg ="";
	var requiredSumFields = Object.keys(datafields).map(function (x) { return datafields[x] }).filter(function (x) { return (x['UI_Settings'] && x['UI_Settings'].RequiredSum && x['UI_Settings'].RequiredSum !=null) });
	
	if(requiredSumFields.length > 0){
		for (var r in requiredSumFields){
			var catId=requiredSumFields[r].CategoryId
			var sumField = requiredSumFields[r];
			var sourceTable = sumField.SourceTable;
			var requiredSumExp = requiredSumFields[r]['UI_Settings'].RequiredSum;
			var sumValue = requiredSumExp.split('/')[0];
			var fields = requiredSumExp.indexOf('/') > -1 ? requiredSumExp.substring(requiredSumExp.indexOf('/') + 1).split(',') : null;
			var groupedData = {};
			if(sourceTable && activeParcel[sourceTable]){
				var sourceData = activeParcel[sourceTable].filter(function(a){return a.CC_Deleted !=true});
				groupedData = sourceData.reduce(function(l, r) {
					var keys = fields ? r[fields[0]] + (fields[1] ? '~'+ r[fields[1]]:'')+ '~' + r.ParentROWUID : r.ParentROWUID;
		
					if (typeof l[keys] === "undefined") {
						l[keys] = {
								sum: 0,
								ParentRowuid:r['ParentROWUID'],
							CatId:catId
						};
					}
					l[keys].sum += !r[sumField.Name] ? 0 : parseFloat( r[sumField.Name]);
					return l;
				}, {});			
			}
			
			if(Object.keys(groupedData).length > 0) {
				var menuID = getParentCategories(requiredSumFields[r].CategoryId);
 				var sumFailed = Object.keys(groupedData).map(function (x) { return groupedData[x] }).filter(function (x) { return x.sum != sumValue });
				if(sumFailed.length > 0){ 
					sumFailedCatRec.push(sumFailed[0])
				}
			}
		}
	}	

	if (sumFailedCatRec.length > 0){
		var msg ='';
		var failedCats=[]
		$.each(sumFailedCatRec, function(i, el){
			 if($.inArray(el, failedCats) === -1) failedCats.push(el);
		});
		for(i= 0;i < failedCats.length;i++){
			var sumFailedParent = failedCats[0].ParentRowuid;
 			if(!getParentCategories(failedCats[i]['CatId']) || !sumFailedParent){ menuID = failedCats[i]['CatId']; }
 			msg += '\n'+getCategoryName(failedCats[i]['CatId']) 		
 		}
	}
	return msg;
	
}
/*function checkRequiredFields() {//commented by JJ
try {
var requireFields = dataSourceFields.filter(function (d) { return d.IsRequired; });
if (requireFields.length > 0) {
for (var x in requireFields) {
var Content = $('.parcel-field-values td[fieldid="' + requireFields[x].Id + '"] .input').val();
if (Content)
Content = Content.trim();
var IsDeleted = $('.parcel-field-values td[fieldid="' + requireFields[x].Id + '"] .input').attr('recovery');
//if (((Content == '') || (Content == null) || (Content == undefined)) && !(IsDeleted) && (eval('activeParcel.' + requireFields[x].SourceTable).length > 0)) { 
if (((Content == '') || (Content == null) || (Content == undefined)) && !(IsDeleted) && (eval('activeParcel.' + (requireFields[x].SourceTable == '_photo_meta_data' ? 'Photos' : requireFields[x].SourceTable)).length > 0)) {  
alert('All required fields are not filled in.');
return false;
}
// console.log(requireFields[x].Id + '-----' + requireFields[x].SourceTable + '----' + requireFields[x].Name + '----' + Content);
}
}
return true;
}
catch (e) {
console.log(e.message);
}
}*/
function checkRequiredIfEditsFieldsInForm(catId, index) {
    var flag = 0;
    var auxform = $('.category-page[categoryid="' + catId + '"]');
    var requiredFields = Object.keys(datafields).map(function (x) { return datafields[x] }).filter(function (x) { return x.CategoryId == catId }).filter(function (y) { return y.RequiredIfRecordEdited == 1 });
    $('.check', auxform).each(function (i, el) {
        if ($(el).attr('style') == "display: inline-block;")
            flag = 1;
    });
    $('.passed', auxform).each(function (i, el) {
        if ($(el).attr('style') == "display: inline-block;")
            flag = 1;
    });
    if (requiredFields && requiredFields.length > 0) {
        for (var x in requiredFields) {
            if (flag == 1)
                $('.parcel-field-values td[fieldid="' + requiredFields[x].Id + '"]').siblings().children('.required').show();
            else
                $('.parcel-field-values td[fieldid="' + requiredFields[x].Id + '"]').siblings().children('.required').hide();
        }
    }
}

function checkRequiredFields() {
    try {
        var requireFields = Object.keys(datafields).map(function (x) { return datafields[x] }).filter(function (x) { return x.IsRequired });
        if (requireFields.length > 0) {
            for (var x in requireFields) {
                var r = requireFields[x];
                var id = r.Id;
                if (activeParcelEdits.length > 0) {
                    for (var x in activeParcelEdits) {
                        var fid = activeParcelEdits[x].FieldId;
                        if (id == fid) {
                            var Content = $('.parcel-field-values td[fieldid="' + fid + '"] .input').val();
                            if (Content)
                                Content = Content.trim();
                            var IsDeleted = $('.parcel-field-values td[fieldid="' + fid + '"] .input').attr('recovery');
                            if (((Content == '') || (Content == null) || (Content == undefined)) && !(IsDeleted) && (eval('activeParcel.' + r.SourceTable).length > 0)) {
                                alert('All required fields are not filled in.');
                                return false;
                            }
                        }
                    }
                }
            }
        }
        return true;
    }
    catch (e) {
        console.log(e.message);
    }
}

function htmlDecode(input) {
    var e = document.createElement('div');
    e.innerHTML = input;
    if (e.childNodes[0]) {
        input = e.childNodes[0].nodeValue;
        //console.log(input+'     '+e.childNodes[0].nodeValue);
    }
    return input;
}

function IsUniqueInSiblingsValid(category, fields) {
    var sourceTable = getSourceTable(category), ret = true;
    if (sourceTable) {
        var t = activeParcel[sourceTable].filter(function (f) { return f.CC_Deleted != true });
        var kk = t.length;
        for (var i = 0; i < kk; i++) {
            var siblings = getSiblings(category, i);
            siblings.forEach(function (d) {
                if (ret == false) return;
                ty = (fields).split(','); var s = 0;
                ty.forEach(function (u) {
                    u = u.trim();
                    var value = t[i][u];
                    if (d != i && t[d]) {
                        var curVal = t[d][u];
                        if (curVal == value)
                        	if((t[i].CC_YearStatus != "F" && t[d].CC_YearStatus != "F") || t[i].CC_YearStatus == t[d].CC_YearStatus)
                            	s = s + 1;
                    }

                });
                if (s == ty.length) ret = false;
            });
        }
    }
    return ret
}
function getSiblings(category, index) {
	//if(activeParcel[getSourceTable(category)][index] && !activeParcel[getSourceTable(category)][index].parentRecord)
    //return [];
    var predTotal = getAllPredecessor(category, activeParcel, index);
    var parentIndex = (predTotal) ? predTotal[0].Index : null;
    var parentCategory = (predTotal) ? predTotal[0].Id : null;
    var source = [];
    if (predTotal) {
        var FilterFields = fieldCategories.filter(function (d) { return d.ParentCategoryId == parentCategory && d.Id == category; })[0].FilterFields;
        var parentSpec = { index: parentIndex, source: getSourceTable(parentCategory) };
        var FilterValues = getFilterValues(FilterFields, parentSpec.source, parentIndex, activeParcel);
        source = getFilteredIndexesTemp( activeParcel[getSourceTable( category )].filter( function ( k ) { return k.CC_Deleted != true } ), FilterFields, FilterValues, parentSpec, activeParcel );
    }
    else {
        // if there is no parent parcel is the parent
        var st = getSourceTable(category);
        for (var ind in activeParcel[st]) {
            source.push(ind);
        }
    }
    return source;
}

function getAllPredecessor(categoryId, sp, index) {
    // console.log('----------getAllPredecessor');
    var preD = [], parent = getParentCategories(categoryId), preCatid = categoryId, preIndex = index;
    while (parent) {
        if (sp && checkIsNull(index)) {
            var parentKeys = getTableKey(getSourceTable(parent));
            //            var parentRowuid = activeParcel[getSourceTable(preCatid)][preIndex][ParentROWUID];

            var parentRowuid = activeParcel[getSourceTable(preCatid)].filter(function (k) { return k.CC_Deleted != true })[preIndex] ? activeParcel[getSourceTable(preCatid)].filter(function (k) { return k.CC_Deleted != true })[preIndex].ParentROWUID : null;
            var filterCondition = '';
            if (parentKeys) {
                parentKeys.forEach(function (d) {
                    var fcv = sp[getSourceTable(preCatid)][preIndex][d];
                    filterCondition += '&&k.' + d + '==' + ((fcv == null || fcv == undefined) ? 'null' : '\'' + fcv + '\'');
                });
            }
            (parentKeys.length > 1) ? filterCondition = filterCondition.substring(2, filterCondition.length) : '';
            if (sp[getSourceTable(parent)]) {
                sp[getSourceTable(parent)].forEach(function (k) {
                    if ((!parentRowuid && eval(filterCondition)) || (parentRowuid && (k.ROWUID == parentRowuid))) {
                        preIndex = sp[getSourceTable(parent)].indexOf(k);
                    }
                });
            }
            preD.push({ Id: parent, Index: preIndex });
        }
        else {
            preD.push(parent);
        }
        preCatid = parent;
        parent = getParentCategories(parent);
    }
    return preD.length > 0 ? preD : null;
}
function getParentCategories(categoryId) {
    return (fieldCategories.some(function (d) { return d.Id == categoryId; })) ? fieldCategories.filter(function (d) { return d.Id == categoryId; })[0].ParentCategoryId : null;
}

function getTopParentCategory(categoryId){
    var parents = getAllPredecessor(categoryId);
    if (parents)
		return parents[parents.length - 1];
    else
		return categoryId;
}

function getFilterValues(filterFilter, sourceTable, index, sParcel) {
    //    try {
    if (!filterFilter) {
        return null;
    }
    var filterFieldJson = filterFilter.join ? filterFilter : [(filterFilter.indexOf(',') >= 0) ? filterFilter.split(',') : { 0: filterFilter }], filterValues;
    for (x in filterFieldJson[0]) {
        //        filterValues = (filterValues ? filterValues + ',' : '') + (eval('activeParcel.' + sourceTable + '[' + index + ']' + '.' + filterFieldJson[0][x]));
        filterValues = (filterValues ? filterValues + ',' : '') + (eval('sParcel.' + sourceTable + '[' + index + ']' + '.' + filterFieldJson[0][x]));
    }
    return filterValues;
    //    }
    //    catch (e) {
    //        console.warn(e.message);
    //    }
}
function getSourceTable(categoryId) {
    return (fieldCategories.some(function (d) { return d.Id == categoryId; })) ? fieldCategories.filter(function (d) { return d.Id == categoryId; })[0].SourceTable : null;
}

function getCategoryName(categoryId) {
    return (fieldCategories.some(function (d) { return d.Id == categoryId; })) ? fieldCategories.filter(function (d) { return d.Id == categoryId; })[0].Name : null;
}
function getParentSpec(categoryId, index, sp) {
    var parentSpec;
    var predecessor = getAllPredecessor(categoryId, (sp) ? sp : activeParcel, index);
    parentSpec = (predecessor) ? { source: getSourceTable(predecessor[0].Id), index: predecessor[0].Index } : null;
    return parentSpec;
}

function checkIsNull(num) {
    var type = typeof num, returnType = true;
    switch (type) {
        case 'string': returnType = (num == '') ? false : true; break;
        case 'number': (num == 0) ? returnType = true : ''; break;
        case 'object': returnType = (num) ? true : false; break;
        case 'undefined': returnType = (num) ? true : false; break;
        case 'boolean': returnType = (num) ? true : false; break;
    }
    return returnType;
}

function getTableKey(sourceTable) {
    try {
        if (sourceTable) {
            var FieldRequired = [];
            for (x in tableKeys) {
                (tableKeys[x].SourceTable == sourceTable) ? FieldRequired.push(tableKeys[x].Name) : '';
            }
            return FieldRequired;
        }
        else {
            return null;
        }
    }
    catch (e) {
        console.warn(e.message);

    }
}

function getFilteredIndexesTemp(source, filter_name, filterValue, parentSpec, sParcel) {
    //    try {
    var filterfJson = [], filterVJason = [];
    filterfJson = filter_name.join ? filter_name : [(filter_name.indexOf(',') >= 0) ? filter_name.split(',') : [filter_name]];
    filterVJson = filterValue.join ? filterValue : [(filterValue.indexOf(',') >= 0) ? filterValue.split(',') : [filterValue]];
    var filterCondition = '';
    var iter_value = 0;
    var d = source;
    while (iter_value < filterfJson[0].length) {
        filterCondition += 'd[x].' + filterfJson[0][iter_value] + ' == ' + filterVJson[0][iter_value];
        iter_value += 1;
        if (iter_value < filterfJson[0].length) {
            filterCondition += '&&';
        }
    }

    // var pRowuid = (parentSpec) ? eval('activeParcel.' + parentSpec.source + '[' + parentSpec.index + '].ROWUID') : null;
    var pRowuid = (parentSpec) ? eval('sParcel.' + parentSpec.source + '[' + parentSpec.index + '].ROWUID') : null;
    var newArray = [];
    for (x in d) {
        if ((!d[x].ParentROWUID && eval(filterCondition)) || (d[x].ParentROWUID && (d[x].ParentROWUID == pRowuid))) {
            newArray.push(x);
        }
    }
    return newArray;
    //    }
    //    catch (e) {
    //        console.warn(e.message);
    //    }
}
function decodeHTML(encoded) {
    var div = document.createElement('div');
    div.innerHTML = encoded;
    var decoded = div.firstChild.nodeValue;
    return decoded;
}
function checkMinRecordExist(currentParcel) {
    var resp = [];
    var catExist;
    if (!isBPPParcel)
     catExist = fieldCategories.filter(function (u) { if (u.MinRecords == null) u.MinRecords = 0; return u.MinRecords > 0 && u.Hidden != true && u.Type != 1});
    else
     catExist = fieldCategories.filter(function (u) { if (u.MinRecords == null) u.MinRecords = 0; return u.MinRecords > 0 && u.Hidden != true && (u.Type == 1 || u.Type == 2 ) });
    if (catExist) {
        catExist.forEach(function (ex) {
            var st = ex.SourceTable;
            var recordsLen = ex.MinRecords;
            var targetTable = currentParcel[st];
            if (!st) {
            }
            else if (ex.ParentCategoryId == null) {
                if (targetTable.length < recordsLen) resp.push(ex);
            }
            else {
                var parent = parentChild.filter(function (pt) { return pt.ChildTable == st })[0].ParentTable;
                if (parent) {
                    if (parent == 'Parcel') { if (targetTable.length < recordsLen) resp.push(ex); }
                    else {
                        var ExistingRecords = currentParcel[parent].filter(function (p) { return p.CC_Deleted == false });
                        if (ExistingRecords.length == 0) return;
                        if (currentParcel[parent].length > 0) {
                            var t = 0;
                            ExistingRecords.forEach(function (cp, i) {
                                var isExp = false;
                                var exChildRecords = cp[st] ? cp[st].filter(function (c) { return c.CC_Deleted != true }) : null;
                                    if ((!exChildRecords || (exChildRecords && exChildRecords.length == 0)) && ex.HideExpression && ex.HideExpression != '') {
                               // if ((!cp[st] || (cp[st] && cp[st].length == 0)) && ex.HideExpression && ex.HideExpression != '') {
                                        isExp = currentParcel.EvalValue(parent + '[' + i + ']', ex.HideExpression.replace(new RegExp('parent.', 'g'), '').replace(new RegExp('&gt;', 'g'), '>').replace(new RegExp('&lt;', 'g'), '<'))
                                    }
                                if (isExp)
                                    return;
                                if (cp[st] && cp[st].filter(function (p) { return p.CC_Temp_Deleted == "true" }).length > 0)
                                    return;
                                if ((!cp[st] || cp[st].filter(function (p) { return p.CC_Deleted == false }).length < recordsLen) && t == 0) {
                                    resp.push(ex);
                                    t = 1;
                                }
                            })
                        }
                        //  else resp.push(ex);

                    }
                }

            }

        });
    }
    return resp;

}

function IsUniqueFieldsInSiblingsValid(category, fields,fieldSourceTable) {
    var sourceTable = getSourceTable(category), flds = "";
    if (sourceTable && activeParcel[sourceTable] && activeParcel[sourceTable].length > 0) {
        var t = activeParcel[sourceTable].filter(function (f) { return f.CC_Deleted != true });
        var kk = t.length;
        var records = [] , rds = [] , nonUniqueCats = [] , r = false,flds = "",rdsExp = ""; 
        for(var j = 0; j < fields.length ; j++){
			if (fields.length == 1){
				flds += "[r." + fields[j] + "]"
			}
			else if (j == 0){
				flds += "[r." + fields[j] + ",";
			}
			else if (j == fields.length-1) {
				flds += "r." + fields[j] + "]";
				}
			else {
				flds += "r." + fields[j] + ",";				
				}
			
			}
        for (var i = 0; i < kk; i++) {
        if (t[i].parentRecord && t[i].parentRecord[fieldSourceTable] && t[i].parentRecord[fieldSourceTable].length > 0){
			records = t[i].parentRecord[fieldSourceTable].filter(function (d) { return d.CC_Deleted != true });
			rds =  records.map(function(r){return eval(flds)})
			var valid = true;
			for(var k = 0; k < rds.length; k++){
				var rds1 = JSON.parse(JSON.stringify(rds));
				rds1.splice(k,1)
				rds1.forEach(function(g,h){
					if (valid == false) return;
 					var s = 0;
					fields.forEach(function (u,n) {
						var value = rds[k][n];
						var curVal = rds1[h][n];
						if ((curVal !== undefined) && (value !== undefined) && curVal == value)
							s = s + 1;
					});
					if (s == fields.length) {
						valid = false;
					}
			});
			}
		if(valid == false) {
				var cat = getCategoryFromSourceTable(fieldSourceTable).Name
				nonUniqueCats.push(cat)
			}	
			}
        }
   }
   return nonUniqueCats;
}

function validateParcel(review, callBack, errorCallback, filterDataCallback, skipQCAlert, skipValidation) {
    if (review == 0 || skipValidation) {
        if (callBack) callBack();
        return false;
    }
    
    if (isBPPParcel && activeParcel.CC_Deleted){
    	callBack();
    	return;
    }
    
    var notSatisfied = [];
    var IsSilbing = true, combination = true;
    var bppCats = fieldCategories.filter(function(f){ if(isBPPParcel){ return (f.Type =='1' || f.Type =='2')} else { return f } });
    var bppCond= ' AND 1==1'
    if(!isBPPParcel) bppCond= ' AND Type != 1'
    else bppCond = ' AND Type = 1 AND Type = 2'
    var res = checkMinRecordExist(activeParcel);
    if (res.length > 0) {
        var reqdList = 'Below mentioned categories should have minimum number of records : \n';
        res.forEach(function (r) {
            reqdList += r.Name + '(' + r.MinRecords + ')' + '\n';
        });
        if(errorCallback) errorCallback(reqdList);
        if (!skipQCAlert) alert(reqdList);
        if (filterDataCallback) filterDataCallback();
        return false;
    }
	getData("SELECT Id, Name, SourceTable, ParentCategoryId, UniqueFieldsInSiblings FROM FieldCategory WHERE UniqueFieldsInSiblings is not NULL "+bppCond, [], function (uniqSibFields) {
		var nonUniques=[],nonUniqueCategories = [],flds = [];
		if(uniqSibFields.length > 0){
			for(u in uniqSibFields){
				var cat = uniqSibFields[u].Id;
				var uniqueFields = uniqSibFields[u].UniqueFieldsInSiblings.split(',');
				var sourceTable = uniqueFields[0].split('.')[0];
				uniqueFields.forEach(function(u){
					flds.push(u.split('.')[1]);
				})	
				nonUniques = IsUniqueFieldsInSiblingsValid(cat,flds,sourceTable);
				for (x in nonUniques){
					nonUniqueCategories.push(nonUniques[x]);
				}
			}
		}
		var nonUniqueCats = [];
		if(nonUniqueCategories && nonUniqueCategories.length > 0) {
			$.each(nonUniqueCategories, function(i, el){
			    if($.inArray(el, nonUniqueCats) === -1) nonUniqueCats.push(el);
				});
		}
       	if(nonUniqueCats && nonUniqueCats.length > 0){
       		if(errorCallback) errorCallback("There are non-unique records in '" + nonUniqueCats.join("','") + "'.");
			if (!skipQCAlert) alert("There are non-unique records in '" + nonUniqueCats.join("','") + "'."); 
            return;
    	}
    	getData("SELECT SourceTable, Name,CategoryId,Label,assignedname FROM Field WHERE IsUniqueInSiblings = 'true'", [], function (fields) {    	
    	 if(!isBPPParcel) 
			 fields = Object.keys(fields).map( function (x) { return fields[x] } ).filter( function (x) { return ( x.CategoryId == -2 || bppCats.map(function(a){return a.Id}).indexOf(x.CategoryId) > -1 )});
    	 else 
			fields = Object.keys(fields).map( function (x) { return fields[x] } ).filter( function (x) { return ( x.CategoryId == -2 || bppCats.map(function(a){return a.Id}).indexOf(x.CategoryId) > -1)});    		
	        for (var i in fields) {
	            var f = fields[i];
	            if (!f.CategoryId) continue;
	            var st = f.SourceTable;
	            var catid = f.CategoryId;
				if(!(st == "_photo_meta_data" && catid == "-2")){
					var yu = IsUniqueInSiblingsValid(f.CategoryId, f.Name);
					if (yu == false) {
						if (!notSatisfied.some(function (d) { return d.MenuName == getCategoryName(f.CategoryId); })) {
							notSatisfied.push({ 'MenuName': getCategoryName(f.CategoryId).trim(), 'count': 1 });
						}
						else {
							for (x in notSatisfied) {
								if (notSatisfied[x].MenuName == getCategoryName(f.CategoryId).trim()) {
									notSatisfied[x].count = notSatisfied[x].count + 1;
								}
							}
						}
						IsSilbing = false;
					}
				}
				else{
					var photouniquefield = true;
                	res = eval('activeParcel.Photos');
                	res = res.filter((im) => { return !im.CC_Deleted });
                	var jj = f.AssignedName.replace('PhotoMetaField', 'MetaData');
					for (var i = 0; i < res.length; i++) {
						var item = res[i];
						photouniquefield = (res.filter(function(x){ return x[jj] == item[jj]})).length == 1
						if(item[jj] === null) photouniquefield = true;
						if(!photouniquefield)
							break;
					}
					if(!photouniquefield){
	                    if (!notSatisfied.some(function (d) { return d.MenuName == 'Photo Properties : '+ f.Label})) {
		                    var item={}; item.fields=[];
		                    item.MenuName = 'Photo Properties : '+ f.Label;
		                    item.fields[0] = f.Label;
		                    item.MenuId = -2;
		                    item.count = 1;
		                    notSatisfied.push(item);
	                    }
		                else {
	                        for (x in notSatisfied) {
	                            if (notSatisfied[x].MenuName == 'Photo Properties : '+f.Label) {
	                                notSatisfied[x].MenuId = -2;
	                                notSatisfied[x].count = notSatisfied[x].count + 1;
	                                if(!notSatisfied[x].fields.some(function(d) {return d == f.Label;}))
	                          			notSatisfied[x].fields[notSatisfied[x].fields.length]= f.Label;
	                            }
	                        }
	                    }
	                    IsSilbing = false;
	
	                }
	        }
	        }
	       var msgTxt = checkRequiredSum(activeParcel);
	       if(msgTxt.trim() !=""){
	       		alert('There are Required Sum failed records in below given categories:'+msgTxt);
	       		return false;
			}
			getData("SELECT SourceTable, Name, CategoryId, Label, Assignedname, ConditionalValidationConfig, InputType FROM Field WHERE ConditionalValidationConfig IS NOT NULL", [], (conditionalValidations) => {
				let conditionalVal = true, failedConditions = [];
				conditionalValidations.forEach((f) => {
					let s = f.SourceTable, d = QC.dataTypes[parseInt(f.InputType)];
					if (d.jsType == 'Number') {
						if ((s == null) || (s == '')) {
							let source = activeParcel, value = activeParcel[f.Name], validations = getConditionalValidationInputs(f, source);
							if (validations.length > 0) {
								validations = validations[0];
								if (value != '' && !isNaN(value) && ((validations.MIN_VALUE != '' && validations.MIN_VALUE > parseFloat(value)) || (validations.MAX_VALUE != '' && validations.MAX_VALUE < parseFloat(value)))) {
									let cat = getCategoryName(f.CategoryId)
									if (failedConditions.indexOf(cat) > -1) failedConditions.push(cat)
									return;
								}
							}

						}
						else if (s != '_photo_meta_data') {
							let source = activeParcel[f.SourceTable].filter((r) => { return (!r.CC_Deleted && r.CC_Deleted != 'true') }), validatedRecs = source, fieldConfig = f.ConditionalValidationConfig.split(',');
							source.forEach((rec) => {
								let value = rec[f.Name], validations = getConditionalValidationInputs(f, rec);
								if (validations.length > 0) {
									//validatedRecs.push(rec);
									validations = validations[0];
									if (value != '' && !isNaN(value) && ((validations.MIN_VALUE != '' && validations.MIN_VALUE > parseFloat(value)) || (validations.MAX_VALUE != '' && validations.MAX_VALUE < parseFloat(value)))) {
										let cat = getCategoryName(f.CategoryId)
										if (failedConditions.indexOf(cat) < 0) failedConditions.push(cat)
										return;
									}
									if ((!value || value == '') && validations.IS_REQUIRED) {
										let cat = getCategoryName(f.CategoryId)
										if (failedConditions.indexOf(cat) < 0) failedConditions.push(cat)
										return;
									}
								}
							});

							if (fieldConfig[2]?.trim() == 'IsV8MaxPercentage' && validatedRecs.length > 0) {
								let groupBy = function (arr, ...keys) {
									return arr.reduce((acc, obj) => {
										if (obj.bid !== null) {
											let key = keys.map(key => obj[key]).join('-');
											let group = acc.find(group => group[0][keys[0]] === obj[keys[0]] && group[0][keys[1]] === obj[keys[1]] && group[0][keys[2]] === obj[keys[2]] && group[0][keys[3]] === obj[keys[3]]);
											if (group) {
												group.push(obj);
											} else {
												acc.push([obj]);
											}
										}
										return acc;
									}, [])
										.filter(group => group.reduce((sum, obj) => parseFloat(sum) + parseFloat(obj.MVA_PERCENTAGE), 0) !== 100);
								}
								validatedRecs = validatedRecs.filter((x) => { return x.MVA_XML_INPUT_TYPE && ['0', '1', '2', '3', '4'].includes(x.MVA_XML_INPUT_TYPE.toString()) });
								let grouped = groupBy(validatedRecs, 'PID', 'BID', 'SECT_ID', 'MVA_XML_INPUT_TYPE');
								if (grouped?.length > 0) {
									let cat = getCategoryName(f.CategoryId)
									if (failedConditions.indexOf(cat) < 0) failedConditions.push(cat)
									return;
								}
							}
						}
					}

				});

				if (failedConditions.length > 0) {
					alert("There is invalid data on " + failedConditions.join("','") + ". Please return to the " + failedConditions.join("','") + " screen and enter the data according to the data entry rules on screen");
					return false;
				}

				getData("SELECT Id,SourceTable,UniqueKeys FROM FieldCategory WHERE UniqueKeys is not NULL " + bppCond, [], function (compfields) {
					for (var i in compfields) {
						var combinationIsSilbing = false;
						var f = compfields[i];
						if (!f.Id) continue;
						var st = f.SourceTable;
						var catid = f.Id;
						var pp = IsUniqueInSiblingsValid(f.Id, f.UniqueKeys);
						if (pp == true) {
							combinationIsSilbing = true;
						}
						if (combinationIsSilbing == false) {
							if (!notSatisfied.some(function (d) { return d.MenuName == getCategoryName(f.Id); })) {
								notSatisfied.push({ 'MenuName': getCategoryName(f.Id).trim(), 'count': 1 });
							}
							else {
								for (x in notSatisfied) {
									if (notSatisfied[x].MenuName == getCategoryName(f.Id).trim()) {
										notSatisfied[x].count = notSatisfied[x].count + 1;
									}
								}
							}
							combination = false;
						}
					}
					var requiredList = 'Please remove or revise any record containing duplicated values on the following screen(s): \n';
					if (notSatisfied.length) {
						for (x in notSatisfied) {
							if (x < 5) {
								var listItem = notSatisfied[x].MenuName + ', ';
								requiredList += listItem;
							}
						}
						requiredList = requiredList.substring(0, requiredList.length - 2);
						(notSatisfied.length > 5) ? requiredList += ' and others.' : '';
						if (errorCallback) errorCallback(requiredList);
						hideMask();
						if (!skipQCAlert) alert(requiredList);
					}

					if (IsSilbing == false || combination == false) {
						hideMask();
						if (filterDataCallback) filterDataCallback();
						return false;
					}
					else {
						var query;
						if (__DTR) {
							query = "SELECT SourceTable, Name,CategoryId,Label,assignedname,IsRequired,RequiredExpression,IsRequiredDTROnly,VisibilityExpression FROM Field WHERE (((IsRequired = 'true' OR RequiredExpression IS NOT NULL) AND DoNotShowOnDTR ='false') OR (IsRequiredDTROnly='true'))AND ReadOnly='false'"
						}
						else {
							query = "SELECT SourceTable, Name,CategoryId,Label,assignedname,IsRequired,RequiredExpression,RequiredIfRecordEdited,VisibilityExpression FROM Field WHERE (IsRequired = 'true' OR RequiredExpression IS NOT NULL OR RequiredIfRecordEdited = 'true') AND ReadOnly='false' AND  DoNotShow='false'"
						}
						getData(query, [], function (rfields) {
							if (!isBPPParcel)
								rfields = Object.keys(rfields).map(function (x) { return rfields[x] }).filter(function (x) { return (x.CategoryId == -2 || bppCats.map(function (a) { return a.Id }).indexOf(x.CategoryId) > -1) });
							else
								rfields = Object.keys(rfields).map(function (x) { return rfields[x] }).filter(function (x) { return (x.CategoryId == -2 || bppCats.map(function (a) { return a.Id }).indexOf(x.CategoryId) > -1) });
							var allRequired = true
							for (var i in rfields) {
								var f = rfields[i];
								var s = f.SourceTable;
								var n = f.Name;
								var reqExp = f.RequiredExpression;
								var hideExp = f.VisibilityExpression;
								var res, checkRequired = true, isHidden = false;;
								if (!f.CategoryId) continue;
								if (f.CategoryId == -2) {
									if (s == '_photo_meta_data') {
										var an = f.assignedname;
										var jj = f.AssignedName.replace('PhotoMetaField', 'MetaData');
										res = eval('activeParcel.Photos');
										res = res.filter((im) => { return !im.CC_Deleted });

										if (res.length) {

											for (var i = 0; i < res.length; i++) {
												var si = 'Photos' + '[' + i + ']';

												var tt = eval('activeParcel.' + si + '.' + jj);
												if (((typeof tt == 'string') && (tt == "")) || tt == null || tt === undefined) {
													if (!notSatisfied.some(function (d) { return d.MenuName == 'Photo Properties' })) {
														// notSatisfied.push({ 'MenuName': 'Photo Properties', 'MenuId': -2, 'count': 1 });
														var item = {}; item.fields = [];
														item.MenuName = 'Photo Properties';
														item.fields[0] = f.Label;
														item.MenuId = -2;
														item.count = 1;
														notSatisfied.push(item);
													}
													else {
														for (x in notSatisfied) {
															if (notSatisfied[x].MenuName == 'Photo Properties') {
																notSatisfied[x].MenuId = -2;
																notSatisfied[x].count = notSatisfied[x].count + 1;
																if (!notSatisfied[x].fields.some(function (d) { return d == f.Label; }))
																	notSatisfied[x].fields[notSatisfied[x].fields.length] = f.Label;
															}
														}
													}
													allRequired = false;

												}
											}
										}
									}
								}
								else if ((s == null) || (s == '')) {
									checkRequired = reqExp ? activeParcel.EvalValue(null, reqExp) : true;
									isHidden = hideExp ? activeParcel.EvalValue(null, hideExp) : false;
									res = eval('activeParcel.' + n);
									if ((((typeof res == 'string') && (res == "")) || res == null || res == "null" || res === undefined) && checkRequired && !isHidden) {
										if (!notSatisfied.some(function (d) { return d.MenuName == getCategoryName(f.CategoryId); })) {
											//notSatisfied.push({ 'MenuName': getCategoryName(f.CategoryId).trim(), 'count': 1 });
											var item = {}; item.fields = [];
											item.MenuName = getCategoryName(f.CategoryId).trim();
											item.fields[0] = f.Label;
											item.MenuId = f.CategoryId;
											item.count = 1;
											notSatisfied.push(item);
										}
										else {
											for (x in notSatisfied) {
												if (notSatisfied[x].MenuName == getCategoryName(f.CategoryId).trim()) {
													notSatisfied[x].MenuId = f.CategoryId;
													notSatisfied[x].count = notSatisfied[x].count + 1;
													if (!notSatisfied[x].fields.some(function (d) { return d == f.Label; }))
														notSatisfied[x].fields[notSatisfied[x].fields.length] = f.Label;
												}
											}
										}
										allRequired = false;
									}
								} else if (s != '_photo_meta_data') {
									//  var a = eval('activeParcel.' + s);
									//  a = a.filter(function (f) { return f.CC_Deleted == false });
									var a = activeParcel[s];
									var rowuid;
									var requiredDtr = false;
									if (a && a.length) {
										for (var i = 0; i < a.length; i++) {
											var si = eval('activeParcel.' + s + '[' + i + ']');
											rowuid = activeParcel[s][i].ROWUID;
											var requiredIfRecordEdited = false, required = false;
											var currentRecordChanges = activeParcel.ParcelChanges ? activeParcel.ParcelChanges.filter(function (x) { return x.AuxROWUID == rowuid }) : null;
											if ((currentRecordChanges && currentRecordChanges.length > 0) && f.RequiredIfRecordEdited == "true") {
												requiredIfRecordEdited = true;
											}
											if (f.IsRequired == "true")
												required = true;
											checkRequired = reqExp ? activeParcel.EvalValue(s + '[' + i + ']', reqExp) : false;
											if (f.IsRequiredDTROnly == "true")
												requiredDtr = true;
											isHidden = hideExp ? activeParcel.EvalValue(s + '[' + i + ']', hideExp) : false;
											var isDeletedRecord = activeParcelEdits.filter(function (dr) { return (dr.action == "delete" && dr.AuxROWUID == rowuid && dr.QCValue == s) });
											if (si.CC_Deleted == true || isDeletedRecord.length > 0)
												continue;
											else if (f.CategoryId) {
												var _pcatId = fieldCategories.filter(function (fcs) { return fcs.Id == f.CategoryId })[0] ? fieldCategories.filter(function (fcs) { return fcs.Id == f.CategoryId })[0].ParentCategoryId : null;
												var _pcat = _pcatId ? fieldCategories.filter(function (fcs) { return fcs.Id == _pcatId })[0] : null;
												if (_pcat && si.parentRecord) {
													var ispdeleted = activeParcelEdits.filter(function (dr) { return (dr.action == "delete" && dr.AuxROWUID == si.parentRecord.ROWUID && dr.QCValue == _pcat.SourceTable) });
													if (si.parentRecord.CC_Deleted == true || ispdeleted.length > 0)
														continue;
													else {
														var _ppcat = _pcat.ParentCategoryId ? fieldCategories.filter(function (fcs) { return fcs.Id == _pcat.ParentCategoryId })[0] : null;
														if (_ppcat && si.parentRecord.parentRecord) {
															var isppdeleted = activeParcelEdits.filter(function (dr) { return (dr.action == "delete" && dr.AuxROWUID == si.parentRecord.parentRecord.ROWUID && dr.QCValue == _ppcat.SourceTable) });
															if (si.parentRecord.parentRecord.CC_Deleted == true || isppdeleted.length > 0)
																continue;
														}
													}
												}
											}

											res = eval('a' + '[' + i + ']' + '.' + n);
											if ((((typeof res == 'string') && (res == "")) || res == null || res == "null" || res === undefined) && (requiredIfRecordEdited == true || required == true || checkRequired == true || requiredDtr == true) && !isHidden) {
												if (!notSatisfied.some(function (d) { return d.MenuName == getCategoryName(f.CategoryId); })) {
													// notSatisfied.push({ 'MenuName': getCategoryName(f.CategoryId).trim(), 'count': 1 });
													var item = {}; item.fields = [];
													item.MenuName = getCategoryName(f.CategoryId).trim();
													item.fields[0] = f.Label;
													item.MenuId = f.CategoryId;
													item.count = 1;
													item.ROWUID = a[i].ROWUID;
													notSatisfied.push(item);
												}
												else {
													for (x in notSatisfied) {
														if (notSatisfied[x].MenuName == getCategoryName(f.CategoryId).trim()) {
															notSatisfied[x].MenuId = f.CategoryId;
															notSatisfied[x].ROWUID = a[i].ROWUID;
															notSatisfied[x].count = notSatisfied[x].count + 1;
															if (!notSatisfied[x].fields.some(function (d) { return d == f.Label; }))
																notSatisfied[x].fields[notSatisfied[x].fields.length] = f.Label;
														}
													}
												}
												allRequired = false;

											}
										}
									}
								}
							}
							if (!allRequired) {
								var requiredList = 'Please complete all required fields within the below mentioned categories:\n';
								if (notSatisfied.length) {
									for (x in notSatisfied) {
										if (x < 5) {
											var fieldDecription;
											if (notSatisfied[x].fields.length > 3) fieldDecription = notSatisfied[x].fields.slice(0, 3).join() + '.. more ';
											else fieldDecription = notSatisfied[x].fields.join();
											var listItem = notSatisfied[x].MenuName + ' : ' + fieldDecription + ' \n';
											requiredList += listItem;
										}
									}
									requiredList = requiredList.substring(0, requiredList.length - 2);
									(notSatisfied.length > 5) ? requiredList += ' and others.' : '';
								}
								if (errorCallback) errorCallback(requiredList);
								hideMask();
								if (!skipQCAlert) alert(requiredList);
								if (filterDataCallback) filterDataCallback();
								return false
							} else {
								var validated = true;
								var errorMessage = "";
								var warningMsg = "";
								var errorCount = 0;
								for (var x in ccma.UI.Validations) {
									var res = true;
									var v = ccma.UI.Validations[x];
									var c = decodeHTML(v.Condition);
									var s = v.SourceTable;
									try {
										if ((s == null) || (s == '')) {
											res = activeParcel.EvalValue(c);
											validated = validated && res;
										}
										else {
											var a = eval('activeParcel.' + s);
											if (a && a.length) {
												for (var i = 0; i < a.length; i++) {
													if (a[i].CC_Deleted == false) {
														var si = s + '[' + i + ']';
														if (si != "") {
															var ind = a[i]["CC_FIndex"] === undefined ? i : a[i]["CC_FIndex"];
															if (v.FirstOnly == 'true' && ind != 0)
																continue;
															if (c.search(childRecCalcPattern) > -1) {
																var fld = c.split('==')[0].trim();
																var validationSplit = c.split('==')[1].trim().split(childRecCalcPattern);
																var childTable = validationSplit[0].split('.')[0];
																var funct = validationSplit[1];
																var field = validationSplit[0].split('.')[1];
																var evalVal = 0;
																var childRecs = (a[i][childTable] ? a[i][childTable].filter(function (rec) { return (!rec.CC_Deleted && rec.CC_Deleted != 'true'); }) : [])
																if (childRecs.length > 0) {
																	childRecs.forEach(function (chRec) {
																		evalVal += parseFloat((chRec[field] != null && chRec[field] != undefined && chRec[field] != '') ? chRec[field] : 0);
																	});
																	if (funct.toLowerCase() == 'avg') evalVal = evalVal / a[i][childTable].length;
																}
																res = res && (evalVal == a[i][fld]);
															}
															else {
																var dvRules = c.match(/[-+*\/]/g) == null ? true : false;
																var parentSource = getSourceTable(getParentCategories(getCategoryFromSourceTable(v.SourceTable).Id))
																res = res && activeParcel.EvalValue(si, c, null, parentSource ? parentSource : null, dvRules);
															}
															validated = validated && res;
														}
													}
												}
											}
										}
									}
									catch (ex) {
										console.error('Error: Invalid Client Validation setup- Source Table: ' + (((s == null) || (s == '')) ? 'ParcelData' : s) + '  Condition: ' + c);
										alert('Error: Invalid Client Validation setup- Source Table: ' + (((s == null) || (s == '')) ? 'ParcelData' : s) + '  Condition: ' + c);
									}

									if (!res) {
										//errorMessage += v.ErrorMessage.listItem();
										if (v.IsSoftWarning == 'true')
											warningMsg += (v.ErrorMessage + "\n");
										else {
											errorCount++;
											errorMessage += errorCount + '. ' + v.ErrorMessage;
											errorMessage += "\n";
										}
										console.log(v.ErrorMessage);
									}
									console.log(c, res ? 'Passed' : 'Failed');
								}
								if (!validated) {
									if (errorMessage != '') {
										if (errorCallback) errorCallback(errorMessage);
										if (!skipQCAlert) alert(errorMessage);
									}
									if ((warningMsg != '') && (errorMessage == '')) {
										if (skipQCAlert) {
											if (callBack) callBack(warningMsg + '\nSelect \'OK\' to continue without updating or \'Cancel\' to make the correction.');
										}
										else if (confirm(warningMsg + '\nSelect \'OK\' to continue without updating or \'Cancel\' to make the correction.')) {
											if (callBack) callBack();
										}
										else hideMask();
										return;
									}
								}
								if (IsSilbing == false || combination == false || allRequired == false || (validated == false || validated === undefined)) {
									hideMask();
									if (filterDataCallback) filterDataCallback();
									return false
								}
								else if (callBack) callBack();
							}
						});
					}
				});
			});
	   });
 });
}
