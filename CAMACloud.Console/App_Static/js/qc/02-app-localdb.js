﻿//    DATA CLOUD SOLUTIONS, LLC ("COMPANY") CONFIDENTIAL 
//    Unpublished Copyright (c) 2010-2013 
//    DATA CLOUD SOLUTIONS, an Ohio Limited Liability Company, All Rights Reserved.

//    NOTICE: All information contained herein is, and remains the property of COMPANY.
//    The intellectual and technical concepts contained herein are proprietary to COMPANY
//    and may be covered by U.S. and Foreign Patents, patents in process, and are protected
//    by trade secret or copyright law. Dissemination of this information or reproduction
//    of this material is strictly forbidden unless prior written permission is obtained
//    from COMPANY. Access to the source code contained herein is hereby forbidden to
//    anyone except current COMPANY employees, managers or contractors who have executed
//    Confidentiality and Non-disclosure agreements explicitly covering such access.</p>
//    The copyright notice above does not evidence any actual or intended publication
//    or disclosure of this source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
//    DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
//    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
//    IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
//    OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
//    TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
//    ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.


var db;
var indxDB;
var _db_create = {
    Images: "CREATE TABLE IF NOT EXISTS Images (Id, ParcelId, Image, Type, LocalId, Accepted, Synced, IsPrimary, MetaData1, MetaData2, MetaData3, MetaData4, MetaData5, MetaData6, MetaData7, MetaData8, MetaData9, MetaData10);",
    Comparables: "CREATE TABLE IF NOT EXISTS Comparables (Id, ParcelId, C1, C2, C3, C4, C5);",
    ParcelChanges: "CREATE TABLE IF NOT EXISTS ParcelChanges (ParcelId, AuxRowId, ParentAuxRowId, Action, Field, FieldId, OldValue, NewValue, ChangedTime, Latitude, Longitude, Synced);",
    CompletedParcels: "CREATE TABLE IF NOT EXISTS CompletedParcels (ParcelId, SelectedAppraisalType, ChangedTime, Latitude, Longitude, Synced);",
    Location: "CREATE TABLE IF NOT EXISTS Location (UpdateTime, Latitude, Longitude, Accuracy, Synced);",
    ClientValidation: "CREATE TABLE IF NOT EXISTS ClientValidation (Name, Condition, Enabled, SourceTable, ErrorMessage, Ordinal);",
    FieldSettings: "CREATE TABLE IF NOT EXISTS FieldSettings(Id,FieldId,PropertyName,Value);",
	CategorySettings: "CREATE TABLE IF NOT EXISTS CategorySettings(Id,CategoryId,PropertyName,Value);",
	LookupValue: "CREATE TABLE IF NOT EXISTS LookupValue (Id, Source, Value, Name, Description, Ordinal,NumericValue);"
}
var _db_drop = {
    Location: "DROP TABLE IF EXISTS Location;",
    ParcelChanges: "DROP TABLE IF EXISTS ParcelChanges;",
    SubjectParcelsView: "DROP VIEW IF EXISTS SubjectParcels;",
    PendingParcelsView: "DROP VIEW IF EXISTS PendingParcels;",
    EmptyImages: "DELETE FROM Images WHERE Image = ''"
}
var _db_alter_table = {
    item1: 'ALTER TABLE FieldCategory ADD Type TEXT'
}
var _db_essentials = [
        _db_create.LookupValue,
        _db_create.FieldSettings,
        _db_create.CategorySettings, _db_alter_table.item1
]

var _db_views = [
    "CREATE VIEW IF NOT EXISTS SubjectParcels AS SELECT p.Id, p.*, n.*, n.Number AS NeighborhoodNo, n.Number AS NBHDNO, p.Id AS SubjectParcelId FROM Parcel p INNER JOIN Neighborhood n ON p.NeighborhoodId = n.Id WHERE IsComparable = 0 AND p.Id <> -99;",
    "CREATE VIEW IF NOT EXISTS PendingParcels AS SELECT p.Id, p.*, n.*, n.Number AS NeighborhoodNo, n.Number AS NBHDNO, p.Id AS SubjectParcelId FROM Parcel p INNER JOIN Neighborhood n ON p.NeighborhoodId = n.Id WHERE Reviewed = 'false' AND IsComparable = 0 AND p.Id <> -99;"
];
var _db_check = {
    ParcelChangesCompatibility: "SELECT ParentAuxRowId FROM ParcelChanges LIMIT 1",
    FieldCompatibility: "SELECT SourceTable, AssignedName FROM Field LIMIT 1"
}

var _db_comp_ParcelChanges = [_db_drop.ParcelChanges, _db_create.ParcelChanges]
var _db_comp_FieldCategory = ['DROP TABLE IF EXISTS FieldCategory', 'DROP TABLE IF EXISTS Field']

var _db_proc_changeLogin = [
    "DROP TABLE IF EXISTS MapPoints",
    "DROP TABLE IF EXISTS Images",
    "DROP TABLE IF EXISTS Parcel",
    "DELETE FROM ParcelChanges"
]

function initLocalDatabase(callback) {
    var dbSize = 500;
    var varCacheUpdate=false;
    if (navigator.platform == 'iPad')
        if (!navigator.standalone)
            dbSize = 50;
    if (!localDBName)
        localDBName = 'CAMACLOUD'
    if(window.localStorage.getItem('varOrganizationID')!=localDBName.split('_')[0]){
    	varCacheUpdate=true;
    	localStorage.setItem('varOrganizationID',localDBName.split('_')[0]);
    }
    var afterCallBack = function ( ){
        db.transaction( function ( tx )        {
            tx.executeSql( 'SELECT * FROM ParentChild', [], function ( tx1, res )            {
                var useCache = true;
                var lastCacheTime = window.localStorage.getItem( localDBName + '-last-cache-time' );
                if ( lastCacheTime )                {
                    lastCacheTime = new Date( lastCacheTime );
                    var now = new Date();
                    if ( ( ( lastCacheTime - now ) / ( 1000 * 60 ) > 180 ) || varCacheUpdate )
                        useCache = false;
                }
                else
                    useCache = false;

                if ( callback ) callback( { useCache: useCache } );
            }, function ()
            {
                if ( callback ) callback( { useCache: false } );
            } );
        } );
    }

    if (typeof(openDatabase) == 'function') {
        db = openDatabase(localDBName, '1.0', 'CAMACloud', dbSize * 1024 * 1024);
        executeQueries(_db_essentials, afterCallBack, afterCallBack);
    }
    else {
        initIndexDbSqllite(localDBName, (recData) => {
            db = recData ? new SQL.Database(recData) : new SQL.Database(); //openDatabase(localDBName, '1.0', 'CAMACloud', dbSize * 1024 * 1024);
            db.transaction = function (callback) {
                var tx = {
                    executeSql: function (query, params, success, failure) {
                        var _tx = this;
                        var resp = {
                            rows: {
                                length: 0,
                                item: function (i) {
                                    return this[i];
                                }
                            }
                        };
                        try {
                            if (/^\s*SELECT\b/i.test(query) && params?.length > 0) {
                                let stmt = db.prepare(query, params)
                                resp = {
                                    rows: {
                                        length: 0,
                                        item: function (i) {
                                            return this[i];
                                        }
                                    }
                                };
                                let cn = 0;
                                while (stmt.step()) {
                                    let row = stmt.getAsObject();
                                    resp.rows[cn] = row;
                                    cn = cn + 1;
                                }
                                resp.rows.length = cn;
                            }
                            else if (params?.length == 0) {
                                var raw = db.exec(query)[0];
                                if (raw) {
                                    var values = raw.values;
                                    resp = {
                                        rows: {
                                            length: values.length,
                                            item: function (i) {
                                                return this[i];
                                            }
                                        }
                                    };

                                    for (var i = 0; i < values.length; i++) {
                                        resp.rows[i] = {};
                                        var k = 0;
                                        for (var c of raw.columns) {
                                            resp.rows[i][c] = values[i][k];
                                            k++;
                                        }
                                    }

                                }
                            }
                            else {
                                db.run(query, params);
                            }
                        }
                        catch (ex) {
                            failure && failure(_tx, ex);
                            return;
                        }

                        setTimeout(function () { success && success(_tx, resp); }, 10);
                    }
                }

                callback && callback(tx);
            }
            executeQueries(_db_essentials, afterCallBack, afterCallBack);
        }, () => {
            if (callback) callback({ useCache: false });
        });
    }
}

function initIndexDbSqllite(dbName, callback, failCallback) {
    indxDB = indexedDB.open(dbName, 1);

    indxDB.onupgradeneeded = function (event) {
        let dbx = indxDB.db = event.target.result;

        if (!dbx.objectStoreNames.contains('sqliteData')) {
            let objectStore = dbx.createObjectStore('sqliteData', { keyPath: 'id', autoIncrement: true });
        }
    };

    indxDB.onsuccess = function (event) {
        let dbx = indxDB.db =  event.target.result;

        if (dbx.objectStoreNames.contains('sqliteData')) {
            let transaction = dbx.transaction(['sqliteData'], 'readonly');
            let objectStore = transaction.objectStore('sqliteData');

            objectStore.openCursor().onsuccess = function (event) {
                let cursor = event.target.result;
                if (cursor?.value) {
                    if (callback) callback(cursor?.value?.data);
                }
                else {
                    if (callback) callback();
                }
            };
        }
        else {
            if (callback) callback();
        }
    };

    indxDB.onerror = function (event) {
        if (callback) callback();
    };
}


function executeQueries(queries, callback, failedCallback, progressCallback) {
    var total = 0, completed = 0;
    var _exec = function (sql, callback) {
        db.transaction(function (tx) {
            tx.executeSql(sql, [], function (tx1, res) {
                completed += 1;
                if (progressCallback) progressCallback(total, completed);
                if (callback) callback();
            }, function (x, e) { console.warn(sql); console.error(e); if (failedCallback) failedCallback(); });
        });
    }

    var _recExec = function (list, callback) {
        if (list.length == 0) {
            if (callback) callback();
        } else {
            var first = list.shift();
            _exec(first, function () {
                _recExec(list, callback);
            });
        }
    }

    var list = queries.join ? queries : [queries];
    total = list.length;
    _recExec(list, callback);
}

function checkIfImageTableExists(callback) {
    executeQueries(_db_create.Images, callback);
}

function checkIfComparablesTableExists(callback) {
    executeQueries(_db_create.Comparables, callback);
}

function createParcelView(callback) {
    executeQueries(_db_views, callback);
}

function deleteEmptyImages(callback) {
    executeQueries(_db_drop.EmptyImages, callback);
}

function checkCompatibility(callback) {
    db.transaction(function (x) {
        executeSql(x, _db_check.FieldCompatibility, [], callback, function () { executeQueries(_db_comp_FieldCategory, callback) });
    });
}

function resetTablesForUserChange(callback) {
    executeQueries(_db_proc_changeLogin, callback);
}

function clearDatabase(callback) {
    var listsql = "SELECT tbl_name FROM sqlite_master WHERE type = 'table' AND name NOT LIKE '#_%' ESCAPE '#'";
    getData(listsql, [], function (names) {
        var sqls = names.map(function (x) { return "DROP TABLE " + x.tbl_name + ";" })
        executeQueries(sqls, function () {
            console.log('All tables cleared.');
            if (callback) callback();
        });
    });
}


var lastQueryOutput;
function getData(query, params, callback, otherData, ignoreError, errorCallback, _cmParm) {
    if (params == null)
        params = [];
    var data = [];
    if (query) {
        db.transaction(function (x) {
            x.executeSql(query, params, function (x, results) {
                var len = results.rows.length, i;
                for (i = 0; i < len; i++) {
                    data.push(results.rows.item(i));
                }
                if (callback) callback(data, otherData, results); else { lastQueryOutput = data; if (data.length > 0) console.log(data[0]); };
            }, function (x, e) {
                if (errorCallback) errorCallback();
                if (!ignoreError && !query.toLowerCase().includes('parcel')) {
                    console.error(e.message);
                    console.error(query);
                    if( _cmParm ){
                    	var _rlVal = confirm(e.message.replace('could not prepare statement', '').replace('(1', '').replace(')', '') + '\n ' + query + '\n ' + 'Click "OK" to clear cached data and download fresh content from server')
                    	if( _rlVal )
                    		resetApplication();
                    }
                    else
                    	alert(e.message.replace('could not prepare statement', '').replace('(1', '').replace(')', '') + '\n ' + query);
                    log(e.message);
                    log(query);
                }
                else {
                    console.error(e.message);
                    console.error(query);
                }
            });
        });
    }
    else
        if (callback) callback();
}

function executeSql(tx, query, params, success, failure) {
    tx.executeSql(query, params, function (x, results) { if (success) success(x, results) }, function (x, e) { if (failure) failure(x, e); else console.error(query, e.message) });
}
var datafields = {};

function getDataField(key, table) {
    var res = Object.keys(datafields).filter(function (x) { return table ? ((datafields[x].Name == key) && (datafields[x].SourceTable == table)) : (((datafields[x].Name == key) && (datafields[x].CategoryId || (datafields[x].QuickReview == "true")) && datafields[x].SourceTable == null)) }).map(function (x) { return datafields[x] });
    if (!table && res.length == 0) {
        res = Object.keys(datafields).filter(function (x) { return (datafields[x].Name == key) }).map(function (x) { return datafields[x] });
    }
    if (res.length > 0)
        return res[0];
    else
        return null;
}