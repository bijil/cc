﻿var currentPhotoIndex;
var currentPhotoPath;
var currentPhotoId = -1;
var __PushToLinkedSettingsName = "linked";   //linked is the metadata field name of Push To Linked Properties
function showPhoto(index) {
    if (activeParcel.Photos.length > 0) {
        var img = activeParcel.Photos[index];
        currentPhotoPath = img.ImagePath;
        currentPhotoId = img.Id;
        $('.photo-frame').attr('PhotoIndex', index);
        $('.photo-frame').css({ 'background-image': 'url("' + img.ImagePath + '")', 'background-size': 'contain' });
        //$('.photo-download-button').attr('href', img.ImagePath);
       // $('.photo-download-button').attr('download', $.now());
       	$('.photo-download-button').attr('onclick','downloadCurrentPhoto("'+img.ImagePath+'")');
        $('.photo-frame').attr('photo', 1);
        currentPhotoIndex = parseInt(index);
        var currentPhoto = activeParcel.Photos[currentPhotoIndex]
        var photoReferenceId = currentPhoto.ReferenceId;
        var imageId = currentPhoto.Id;
        $('.photo-all-parcels').removeClass('border');
        $('.photo-all-parcels').eq(index).addClass('border');
        if (imageId) {
            $('.ccimageId').text('CCImageId:' + imageId);
            $('.ccimageId').show();
        } else {
            $('.ccimageId').hide();
        }
        
        if (currentPhoto.CC_Deleted == "1" || currentPhoto.CC_Deleted == true) {
            $('.photo-recover-button').removeClass('hidden');
        }
        else $('.photo-recover-button').addClass('hidden');
        
        if (currentPhoto.IsPrimary == "1" || currentPhoto.IsPrimary == true) {
            $('.setAsMain-button').addClass('hidden');
            $('.mainImage-button').removeClass('hidden');           
        }
        else {
            $('.mainImage-button').addClass('hidden');
            $('.setAsMain-button').removeClass('hidden');            
        }

        if (photoReferenceId) {
            $('.referenceId').text('RefId:' + photoReferenceId);
            $('.referenceId').show();
        }
        else {
            $('.referenceId').hide();
        }
        if (activeParcel.Photos[index].DownSynced == false || activeParcel.Photos[index].DownSynced == null)
            $('.downsycset').hide();
        else
            $('.downsycset').show();
        $('.photo-DownSynced-button').prop('checked', activeParcel.Photos[index].DownSynced == true ? false : true);
        if (activeParcelPhotoEdits.length > 0) {
            for (x in activeParcelPhotoEdits) {
                var ce = activeParcelPhotoEdits[x];
                if (ce.PhotoID == currentPhotoId && ce.DownSynced !== undefined) {
                    $('.photo-DownSynced-button').attr('checked', ce.DownSynced == true ? false : true);
                }
            }
        }                     //        $('.photo-DownSynced-button').removeClass().addClass("photo-DownSynced-button");
        //        if ($('.photo-DownSynced-button').attr('downsynced') == 'true')
        //            $('.photo-DownSynced-button').addClass('downsynced');
        //        else
        //            $('.photo-DownSynced-button').addClass('Nodownsynced');
    } else {
        $('.downsycset').hide();
        currentPhotoIndex = -1;
        currentPhotoPath = '';
        currentPhotoId = -1;
        $('.photo-frame').attr('photo', 0);
        $('.photo-frame').css({ 'background-image': 'url("/App_Static/images/parcel-no-photo.png")', 'background-size': 'auto' });
    }
    if (currentPhotoIndex > -1) {
        var total = activeParcel.Photos.length;
        var c = currentPhotoIndex;
        c++;
        $('.parcel-data-title').html('Parcel Photos - showing ' + c + ' of ' + activeParcel.Photos.length);
        $('#ImageCount').html('Parcel Photos - showing ' + c + ' of ' + activeParcel.Photos.length);
        $('.photo-Manager').show();
    } else {
        $('.parcel-data-title').html('Parcel Photos - No photos available.');
        $('#ImageCount').html('');
    }
}

function downloadCurrentPhoto(imgPath) {
    let isReadOnlyUserDTR = __DTR && DTRRoles && DTRRoles.ReadOnly ? true : false;
    if (isReadOnlyUserDTR) {
        alert("You don't have permission to download photos ")
        return false;
    }
    var img=new Image();
    img.onload=imageFound;
    img.onerror=imageNotFound;
    img.src=imgPath;
	function imageFound() {
        var tag = document.createElement('a');
        tag.href = imgPath;
        tag.download = $.now();
        document.body.appendChild(tag);
        tag.click();
        document.body.removeChild(tag);
    	}
	function imageNotFound() {
    	alert('Photo could not found.');
		}
	}
function showPhotoPreview(index){
$('.photo-preview').show();
$('.photo-preview').attr('index',index);
 var img = activeParcel.Photos[index];
$('.photo-preview').css({ 'background-image': 'url("' + img.ImagePath + '")', 'background-size': 'contain' });
$('.photo-preview').unbind('click');
$('.photo-preview').click(function () {
	 var i = $(this).attr('index');
	 if ($('#photo-properties-frame').is(':visible') == true) {
        $('#photo-properties-frame').hide();
        $('.photo-frame-allPhotos').hide();
        $('.photo-frame').show();
        showPhoto(i);
        $('.photo-Manager').hide();
        $('.photo-Manager-Back').show();
    }
    else if ($('.photo-frame').is(':visible') == true) {
        getPhotoUpdates();
        $('.photo-frame-allPhotos').show();
        $('.photo-frame').hide();
        $('.photo-Manager').show();
        $('.photo-Manager-Back').hide();
        $('#photo-properties-frame').hide();
        $('.downsycset').hide();
        $('.referenceId').hide();
        $('.ccimageId').hide();
    }
});
}

function nextPhoto(disableHide) {
    if (currentPhotoIndex > -1 && currentPhotoIndex < activeParcel.Photos.length - 1) {
        showPhoto(currentPhotoIndex + 1);
        if (disableHide != true)
            $('.photo-Manager').hide(); if (currentPhotoIndex >= 7) {
                var rightScroll = $('.photo-frame-allPhotos').scrollLeft();
                $('.photo-frame-allPhotos').animate({ scrollLeft: rightScroll +152 });
            }
    }
}


function prevPhoto(disableHide) {
    if (currentPhotoIndex > 0) {
        showPhoto(currentPhotoIndex - 1);
        if (disableHide != true)
            $('.photo-Manager').hide(); if (currentPhotoIndex <= 7) {
                var leftScroll = $('.photo-frame-allPhotos').scrollLeft();
                $('.photo-frame-allPhotos').animate({ scrollLeft: leftScroll - 152 });
            }
    }
}


function photoUploadStarted() {
}


function photoUploadError() {
}


function photoUploadCompleted() {
}

function acceptNewPhoto(id, path) {
    var newImageIndex = activeParcel.Photos.length;
    let pimg = activeParcel.Photos.filter(function (i) { return (i.IsPrimary && !i.CC_CC_Deleted) }); 
    var img = { 'Id': id, 'ParcelId': activeParcel.Id, 'IsSketch': false, 'ImagePath': path };
    activeParcel.Photos.push(img);
    openCategory("photo-viewer", "Parcel Photos");
    savePhotoDefaultProperties(img);
    showPhoto(newImageIndex);
    if (pimg && pimg.length == 0) {  
		let imgx = activeParcel.Photos.findIndex((x) => { return x.Id == id });  
		activeParcel.Photos[imgx].IsPrimary = true;
		$qc('updateprimaryphoto', { ImageId: id });
	}
    showPhotoThumbnail();
    getParcelDataUpdates(true,'newPhoto');    
    showAllPhotos();
    Thumnailphotos();
    $('#DeletePh').removeAttr('disabled');   	
}

function showPhotoThumbnail() {
    if (activeParcel.Photos) {
    	let ndelPhotos = activeParcel.Photos.filter(function (d) { return !d.CC_Deleted });
        var phl = ndelPhotos.length
        var primaryPhoto = ndelPhotos.filter(function (d) { return checkAsBoolean(d.IsPrimary); });
        if (phl > 0) {
            if (primaryPhoto.length) {
                $('.photo-thumb').css({ 'background-image': 'url("' + primaryPhoto[0].ImagePath + '")' });
                activeParcel.FirstPhoto = primaryPhoto[0].ImagePath;
            }
            else {
                $('.photo-thumb').css({ 'background-image': 'url("' + activeParcel.Photos[0].ImagePath + '")' });
                activeParcel.FirstPhoto = ndelPhotos[0].ImagePath;
            }
        } else {
            $('.photo-thumb').css({ 'background-image': 'url("/App_Static/images/residential.png")' });
            activeParcel.FirstPhoto = "/App_Static/images/residential.png";
        }
    }
}

function makeDownSynced(photo) {
    var ds;
    if (!$(photo).is(":checked")) {
        ds = 'true';
    } else {
        ds = 'false';
    }
    if (activeParcelPhotoEdits.length > 0) {
        for (x in activeParcelPhotoEdits) {
            var ce = activeParcelPhotoEdits[x];
            if (ce.PhotoID == currentPhotoId) {
                activeParcelPhotoEdits.splice(x, 1);
            }
        }
    }
    if (activeParcel.Photos[currentPhotoIndex].DownSynced != getBool(ds)) {
        console.log(activeParcel.Photos[currentPhotoIndex].DownSynced, ds);
        var Photoedit = {
            ParcelId: activeParcel.Id,
            PhotoID: currentPhotoId,
            DownSynced: ds
        };
        activeParcelPhotoEdits.push(Photoedit);
    }
}


function downloadPhoto() {
    if (currentPhotoPath != '') {
        window.open(currentPhotoPath, '_blank');
    }
}


function deleteCurrentPhoto() {
    if (window.confirm('You are about to delete a photo permanently. Are you sure you want to continue?')) {
        if (currentPhotoIndex > -1) {
            $qc('deleteimage', { ImageId: currentPhotoId }, function (data) {
                if (data.status == "OK") {
                    activeParcel.Photos.splice(currentPhotoIndex, 1);
                    showPhoto(0);
                    showPhotoThumbnail();
                    getParcelDataUpdates(true,'deletePhoto');
                }
            });
        }
    }
}


//Created on 2015.03.10
function showAllPhotos() {
	    var photos = activeParcel.Photos;
	    var currentPhotos = activeParcel.Photos.length;
	    var photosLimit = getClientSettingValue("MaxPhotosPerParcel");
	    var imageIDs = "";
	    var images = photos.filter(function (p) { return p.IsPrimary != true });
	    var photosToBeDeleted = currentPhotos - photosLimit ;
	    if (photosToBeDeleted > 0 && !(photosLimit == 1 && activeParcel.Photos.length == 1) && images.length > 0) {
	            for (var j = 0 ; j < photosToBeDeleted ; j++) {
	                imageIDs = (imageIDs && imageIDs.length != 0) ? imageIDs + '$' + images[j].Id.toString() : images[j].Id.toString();
	                for (var k = 0; k < photos.length; k++) {
	                    if (photos[k].Id == images[j].Id)
	                        photos.splice(k, 1);
	                }
	            }
	            $qc('deleteimage', { ImageId: imageIDs }, function (data) { });                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
	    }
	    if (activeParcel.Photos.length > 0) {
	        var i;
	        var imgCnt = activeParcel.Photos.length;
	        $(".photo-all-parcels").remove();
	        for (i = 0; i < imgCnt; i++) {
	            var img = activeParcel.Photos[i];
	            currentPhotoPath = img.ImagePath;
	            currentPhotoId = img.Id;
	            var $newbaseDiv = $("<div></div>");
	            $newbaseDiv
	                    .addClass("photo-all-parcels");
	            var $newDiv = $("<div></div>");
	            $newDiv
	                    .addClass("photo-all-parcels-item")
	                    .attr("onClick", "showPh(this)")
	                    .attr("divId", i)
	                    .css({
	                        'background-image': 'linear-gradient(to right, rgb(247 247 247) 0%, rgb(242 242 242) 20%, rgb(237 237 237) 80%, rgb(255 255 255) 100%)',
	                        'animation': 'photoloading 1s linear infinite'
	                    });
	            var image = new Image(118, 98);
	            image.src =  img.ImagePath;
		     image.id = i;
	            image.onload = function(){
	               $('.photo-all-parcels-item[divid="'+$(this).attr('id')+'"]').css('animation','')
	            	 $('.photo-all-parcels-item[divid="'+$(this).attr('id')+'"]').css({'background-image': 'url("'+$(this).attr('src')+'")'});
	            }
		     image.onerror = function(){
		       $('.photo-all-parcels-item[divid="'+$(this).attr('id')+'"]').css('animation','')
	            	$('.photo-all-parcels-item[divid="'+$(this).attr('id')+'"]').css({'background-image': 'none'});
	            }
	            var $newcheckbox = $("<input type='checkbox'>");
	            $newcheckbox
	                    .attr("id", "chkbox" + i)
	                    .attr("imgid", currentPhotoId)
	                    .attr("imgPath", currentPhotoPath)
	                    .attr("onClick", "btncheckAll(this)")
	                    .addClass("ChkSelPhoto")
	                    .css({
	                        'float': 'right', 'width': '14px', 'height': '15px', 'margin-left': '-3px', 'margin-top': '-3px'
	                    });
	            $newcheckbox.appendTo($newbaseDiv);
	            $newDiv.appendTo($newbaseDiv);
	            $newbaseDiv.appendTo($('.photo-frame-allPhotos'));
	            if (img.IsPrimary) {
	                $('<a class="tabPrimary" ></a>').insertBefore($('.ChkSelPhoto[imgid="' + currentPhotoId + '"]'));
	            }
                if(img.CC_Deleted == 1 || img.CC_Deleted == true) {
                    $('.ChkSelPhoto[imgid="' + currentPhotoId + '"]').prop('disabled',true);
                    $('<a class="tabDeleted" ></a>').insertBefore($('.ChkSelPhoto[imgid="' + currentPhotoId + '"]'));
                } 

	            $('.photo-frame-allPhotos').show();
	            $('.photo-Manager').show();
	            $('.photo-frame').hide();
	            $('#comploader').hide();
	        }
	        if ($('.tabPrimary').length == 0) {
	            let nonDelPhotos = activeParcel.Photos.filter((x) => { return !x.CC_Deleted });
				if (nonDelPhotos.length > 0) {
		            $('<a class="tabPrimary" ></a>').insertBefore($('.ChkSelPhoto[imgid="' + nonDelPhotos[0].Id + '"]'));
		            nonDelPhotos[0].IsPrimary = true;
				}
	        }
	
	    } else {
	        $('.photo-frame-allPhotos').html('');
	        $('.photo-Manager').hide();
	        $('.photo-frame-allPhotos').hide();
	        $('.photo-frame').show();
	        $('.photo-frame').attr('photo', 0);
	        $('.photo-frame').css({ 'background-image': 'url("/App_Static/images/parcel-no-photo.png")', 'background-size': 'auto' });
	        $('#comploader').hide();
	        currentPhotoIndex = -1;
	        currentPhotoPath = '';
	        currentPhotoId = -1;
	        $('.add-photo-frame').show();
	    }
	    $('iframe[id=photouploader]').contents().find('.btnCncl').trigger('click');
	    $('.downsycset').hide();
	    $('#photo-properties-frame').hide();
	    $('.referenceId').hide();
	    $('.ccimageId').hide();
	    $('#DownloadSelectedPh').parent('td').hide();
	    btncheckAll($('.SelectAllPh'));
	    if (isBPPParcel && activeParcel.CC_Deleted && $('.tabs-main li.selected').attr('category') == 'photo-viewer'){
			$('.add-photo-frame').hide();
			$('.photo-Manager').hide();
			$('#comploader').hide();
			$('.parcel-data-title').html('Photos cannot be taken on a property that is flagged to be deleted.');
			//alert('Photos cannot be added to a property that is deleted. Recovering this parcel will allow you to add photos.');
		}
}
function Thumnailphotos() {
     if (window.location.href.toString().indexOf("images.aspx") == -1) return;
    if (activeParcel.Photos.length > 0) {
        var i;
        var width = 152;
        var left = 45;
        var imgCnt = activeParcel.Photos.length;
        $(".photo-all-parcels").remove();
        for (i = 0; i < imgCnt; i++) {
            var img = activeParcel.Photos[i];
            currentPhotoPath = img.ImagePath;
            currentPhotoId = img.Id;
            var $newbaseDiv = $("<div></div>");
            $('.photo-frame-allPhotos').css({ 'position': 'relative', 'height': '150px', 'width': width + 'px', 'display': 'inline-flex', 'bottom': '5px' });
            if (i < 6) {
                width = width + 151;
                left = left - 6
            }
            if (i == 7)
                $('.photo-frame-allPhotos').css({ 'overflow-x': 'auto', 'overflow-y': 'hidden' });
            $newbaseDiv
                    .addClass("photo-all-parcels");
            var $newDiv = $("<div></div>");
            $newDiv
                    .addClass("photo-all-parcels-item")
                    .attr("onClick", "showPh(this)")
                    .attr("divId", i)
                    .css({
                        'background-image': 'url("' + img.ImagePath + '")'
                    });
            var $newcheckbox = $("<input type='checkbox'>");
            $newcheckbox
                    .attr("id", "chkbox" + i)
                    .attr("imgid", currentPhotoId)
                    .attr("imgPath", currentPhotoPath)
                    .attr("onClick", "btncheckAll(this)")
                    .addClass("ChkSelPhoto")
                    .css({
                        'float': 'right', 'width': '14px', 'height': '15px', 'margin-left': '-3px', 'margin-top': '-3px'
                    });
            $newcheckbox.appendTo($newbaseDiv);
            $newDiv.appendTo($newbaseDiv);
            $newbaseDiv.appendTo($('.photo-frame-allPhotos'));
            if (img.IsPrimary) {
                $('<a class="tabPrimary" ></a>').insertBefore($('.ChkSelPhoto[imgid="' + currentPhotoId + '"]'));
                showPh($newDiv);
            }
            if(img.CC_Deleted == 1 || img.CC_Deleted == true) {
            	$('.ChkSelPhoto[imgid="' + currentPhotoId + '"]').prop('disabled', true);
                $('<a class="tabDeleted" ></a>').insertBefore($('.ChkSelPhoto[imgid="' + currentPhotoId + '"]'));
            }
            $('.photo-frame-allPhotos').show();
            $('.photo-Manager').show();
            //  $('.photo-frame').hide();
            $('#comploader').hide();
          
        }
        if ($('.tabPrimary').length == 0) {  
			let nonDelPhotos = activeParcel.Photos.filter((x) => { return !x.CC_Deleted });        
			if (nonDelPhotos.length > 0) {
				showPh($('.photo-frame-allPhotos .photo-all-parcels .photo-all-parcels-item')[0]);
	            $('<a class="tabPrimary" ></a>').insertBefore($('.ChkSelPhoto[imgid="' + nonDelPhotos[0].Id + '"]'));
	            nonDelPhotos[0].IsPrimary = true;
	        }	        
        }

    } else {
        $('.photo-frame-allPhotos').html('');
        $('.photo-Manager').hide();
        $('.photo-frame-allPhotos').hide();
        $('.photo-frame').show();
        $('.photo-frame').attr('photo', 0);
        $('.photo-frame').css({ 'background-image': 'url("/App_Static/images/parcel-no-photo.png")', 'background-size': 'auto' });
        $('#comploader').hide();
        $('.ccimageId').hide();
        currentPhotoIndex = -1;
        currentPhotoPath = '';
        currentPhotoId = -1;
        $('.add-photo-frame').show();
    }
    $('.downsycset').hide();
    $('#photo-properties-frame').hide();
}
//Created on 15/4/2015
function getPhotoUpdates() {
    if (currentPhotoIndex > -1) {
        $('.parcel-data-title').html('Displaying ' + activeParcel.Photos.length + ' Parcel Photos ');
        $('#ImageCount').html('Displaying ' + +activeParcel.Photos.length + ' Parcel Photos ');
        $('.photo-Manager').show();
    } else {
        $('.parcel-data-title').html('Parcel Photos - No photos available.');
        $('#ImageCount').html('');
    }
}


function deleteAllSelectedPhoto(enablephotoframe) {
    let isReadOnlyUserDTR = __DTR && DTRRoles && DTRRoles.ReadOnly ? true : false;
    if (isReadOnlyUserDTR) {
        alert("You don't have permission to delete photos ")
        return false;
    }
    if (activeParcel.CC_Deleted == true || (ccma.Session.RealDataReadOnly == true && activeParcel.IsRPProperty)){
            alert ('This option is available for the selected parcel.');
            return false;
        }
    $('.add-photo-frame').hide();
    var cntCheck = $('.ChkSelPhoto:checked').length;
    if (cntCheck > 0) {
        if (confirm('Are you want to delete selected photos permanently?')) {
            $('#comploader').show();
            var imageIDs = $('.ChkSelPhoto:checked').map(function () { return $(this).attr('imgid') }).get().join('$');
            let imgIds = imageIDs.split('$');
            var images = activeParcel.Photos.filter(function (i) { return ((i.IsPrimary == null || i.IsPrimary == false) && !i.CC_Deleted && (imgIds.indexOf(i.Id.toString()) == -1)) });
            var newPrimaryImageId = "";
            if (images.length > 0) {
                imageIDs.split('$').forEach(function (im) {
                    let image = activeParcel.Photos.filter(function (i) { return i.Id == im });
                    if (newPrimaryImageId == "")
                        newPrimaryImageId = image.length > 0 && image[0].IsPrimary == true ? images[0].Id.toString() : "";
                });
            }
            let ps = checkLinkedProperties(imageIDs);

            $qc('deleteimage', { ImageId: imageIDs, newPrimary: newPrimaryImageId, UpdatePushLink: (ps ? '1' : '0') }, function (data) {
                if (data.status == "OK") {
                    var selectedIds = $('.ChkSelPhoto:checked').map(function () { return $(this).attr('id').slice(6, $(this).attr('id').length); }).get().join('$');
                    var cntCheck = $('.ChkSelPhoto:checked').length;
                    var ty = [];
                    ty = selectedIds.split('$');
                    let clientSettingVal = null;
                    for (var i = ty.length - 1; i >= 0; i--) {
                        if (clientSettings && clientSettings.PhotoDeleteAndRecover) clientSettingVal = clientSettings.PhotoDeleteAndRecover;
                        else if (window.opener && window.opener.clientSettings) clientSettingVal = window.opener.clientSettings['PhotoDeleteAndRecover'];
                        if (clientSettingVal == "1") {
                            activeParcel.Photos[ty[i]].CC_Deleted = true;
                            activeParcel.Photos[ty[i]].IsPrimary = false;
                            if (ps) {
                                let getMetaDataField = window.opener && window.opener.getDataField ? window.opener.getDataField : getDataField;
                                let field = getMetaDataField(__PushToLinkedSettingsName);
                                if (field && field.AssignedName) {
                                    let metaField = field.AssignedName.replace('PhotoMetaField', 'MetaData');
                                    activeParcel.Photos[ty[i]][metaField] = "false";
                                }
                            }
                        }
                        else {
                            activeParcel.Photos.splice(ty[i], 1); //ty.splice(i, 1);
                        }

                        if (enablephotoframe) {
                            Thumnailphotos();
                            showPhotoThumbnail();
                            getParcelDataUpdates(true, 'deletePhoto');
                            getPhotoUpdates();
                        }
                        else {
                            showAllPhotos();
                            showPhotoThumbnail();
                            getParcelDataUpdates(true, 'deletePhoto');
                            getPhotoUpdates();
                        }

                    }
                    if (activeParcel.Photos.length < 1) {
                        $('.photo-Manager').hide();
                        $('.add-photo-frame').show();

                    }
                    else {
                        $('.photo-Manager').show();
                    }
                    if (localStorage.getItem('PhotosPopupSatus') == "true") {
                        window.opener.showPhotoThumbnail();
                        let ix = $('.photo-frame-popup').attr('photoindex');
                        if (clientSettingVal == "1" && activeParcel.Photos[ix].CC_Deleted)
                            $('.photo-recover-button').removeClass('hidden');
                    }
                    $('#SelectAllPh').html('Select All');

                }
            });
        }
        else {
            return false;
        }
    } else {
        alert('Select at least one photo to delete.');
    }

}
function downloadSelectedPhotos() {

    let isReadOnlyUserDTR = __DTR && DTRRoles && DTRRoles.ReadOnly ? true : false;
    if (isReadOnlyUserDTR) {
        alert("You don't have permission to delete photos ")
        return false;
    }
	//$('.ChkSelPhoto:checked').each(function(i,c){
     // 	var path = $(c).attr('imgpath');
     // 	$('#DownloadSelectedPhhid').attr('href', path);
     // 	$('#DownloadSelectedPhhid').attr('download', path);
     // 	$('#DownloadSelectedPhhid').trigger('click');
  	//});   

    try {

        var path = '';
        $('.ChkSelPhoto:checked').each(function (i, c) {
            path += $(c).attr('imgpath') + ",";
        });

        var urls = path.split(",");
        urls.pop();
        var images = [];
        var counter = 0;
        var noImgcounter = 0;


        function convertImgToBase64URL(url, callback) {
            var img = new Image();
            img.crossOrigin = 'Anonymous';
            img.onload = function () {
                var canvas = document.createElement('CANVAS'),
                    ctx = canvas.getContext('2d'), dataURL;
                canvas.height = this.height;
                canvas.width = this.width;
                ctx.drawImage(this, 0, 0);
                dataURL = canvas.toDataURL();

                callback(dataURL, url);
                canvas = null;
            };
            img.onerror = function () { callback(null, null, true); };
            img.src = url;
        }

        function createArchive(images) {
            // Use jszip
            var zip = new JSZip();
            var img = zip.folder(activeParcel.KeyValue1);
            var imgname;
            for (var i = 0; i < images.length; i++) {
                //$('#DownloadSelectedPh').attr('download', $.now());
                imgname = $.now();
                var commaIdx = images[i].data.indexOf(",");
                img.file(imgname + i + ".jpg", images[i].data.slice(commaIdx + 1), { base64: true });
            }
            var file = zip.generate({ type: "blob" });
            saveAs(file, activeParcel.KeyValue1 + ".zip");
            alert(images.length + " " + "Photo(s) downloaded successfully!");
        }

        for (var i = 0; i < urls.length; i++) {
            convertImgToBase64URL(urls[i], function (base64Img, url, flag) {
                if (!flag) {
                    images.push({
                        url: url,
                        data: base64Img
                    });
                    counter++;
                }
                else {
                    noImgcounter++;
                }
                if ((noImgcounter + counter) == urls.length) {
                    if (noImgcounter > 0 && counter > 0) {
                        if (confirm("Some of the selected photos are not available. Do you want to continue download with the remaining photos?")) {
                            createArchive(images);
                        }
                        else {
                        }
                    }
                    else if (noImgcounter == 0 && counter > 0) {
                        createArchive(images);
                    }
                    else if (noImgcounter > 0 && counter == 0) {
                        alert("The selected photos are not available.");
                    }
                }
            });
        }
    }
    catch (e) {
        alert("Photos download failed")
    }
}
function hideParcelPhotos() {
    localStorage.setItem('PhotosPopupSatus', true);
    $(".tabs li[category='photo-viewer']").css("display", "none");
    openCategory('dashboard', 'Dashboard');
    $('.tabs li:first-child').addClass('selected');
}
function onunloadPoupUp() {
    localStorage.setItem('PhotosPopupSatus', false);
    $(".tabs li[category='photo-viewer']").css("display", "inline");
}

//Created on 25/11/2015

function showPhotoProperties() {

    if (currentPhotoIndex > -1) {
    	$('#comploader').show();
    	$('.photo-preview').hide();
    	var currentPhoto = activeParcel.Photos[currentPhotoIndex];
        var cpi = currentPhotoIndex;
        showPhotoPreview(cpi);
        cpi++;
        $('.parcel-data-title').html('Parcel Photos Properties - showing ' + cpi + ' of ' + activeParcel.Photos.length);
        if(((currentPhoto.DownSynced && (currentPhoto.DownSynced == '1' || currentPhoto.DownSynced == 'true')) &&
         (clientSettings["PhotoNoDownsyncOnFlagging"] == '1' || clientSettings["PhotoNoDownsyncOnFlagging"] == 'true')) || 
         (clientSettings['PhotoDeleteAndRecover'] && currentPhoto.CC_Deleted == true)){
	        $('#photo-properties-frame').find('input').prop('readonly', true);
	        $('#photo-properties-frame').find('input[type="text"], textarea').attr("disabled", true);
	        $('#photo-properties-frame').find('select').attr("disabled", true);
	        $('#photo-properties-frame').find('.LoadGeolocationIcon').hide();
	    }
	    else{
	    	$('#photo-properties-frame').find('input').removeAttr('readonly');
	        $('#photo-properties-frame').find('input[type="text"], textarea').removeAttr("disabled");
	        $('#photo-properties-frame').find('select').removeAttr("disabled");
	    }

        //if ($('.photo-viewer').find('.downsycset').is(':visible'))
        loadPhotoProperties(function(){
        	initRadioButtonChangeTracker();
	        $('#comploader').hide(); 
	        $('.photo-frame').hide(); 
	        $('#photo-properties-frame').show(); 
	    });

        if (ccma.Session.RealDataReadOnly == true && activeParcel.IsRPProperty) {
            $('#photo-properties-frame').find('input').prop('readonly', true);
            $('#photo-properties-frame').find('input[type="text"], textarea').attr("disabled", true);
            $('#photo-properties-frame').find('select').attr("disabled", true);
        }

        //Disable the PushToLinkedProperties to restrict manual user changes
        if(clientSettings && clientSettings['PhotoDeleteAndRecover'] == 1 && clientSettings['DeletePhotoPromptForLinkedProperties'] == 1) {
            let field = getDataField(__PushToLinkedSettingsName);
            $(`td[fieldid="${field.Id}"]`).find('select').attr("disabled", true);
        }

    }
}

function setAsMainImage() {
    let isReadOnlyUserDTRs = __DTR && DTRRoles && DTRRoles.ReadOnly ? true : false;
    if (isReadOnlyUserDTRs) {
        alert("You don't have permission to do this operation ")
        return false;
    }
    showMask();
    $qc('setasmainimage', { ImageId: currentPhotoId, ParcelId: activeParcel.Id }, (data) => {
        if (data.status == "OK") {
            $('.setAsMain-button').addClass('hidden');
            $('.mainImage-button').removeClass('hidden');
            let imgx = activeParcel.Photos.findIndex((x) => { return x.Id == currentPhotoId }), cpImgx = activeParcel.Photos.findIndex((x) => { return x.IsPrimary == '1' || x.IsPrimary == true });
            activeParcel.Photos[cpImgx].IsPrimary = false;
            activeParcel.Photos[imgx].IsPrimary = true;
            $('.tabPrimary').remove();
            $('<a class="tabPrimary" ></a>').insertBefore($('.ChkSelPhoto[imgid="' + currentPhotoId + '"]'));
            if (window.opener) window.opener.showPhotoThumbnail();
            else showPhotoThumbnail();
            if (data.AuditTrail) activeParcel.AuditTrail = data.AuditTrail;
            updateRecentAuditTrail();
            hideMask();
        }
        else {
            console.log(data);
            hideMask();
        }
    }, (er) => {
        console.log(er.message);
        hideMask();
    });
}


function recoverDeletedPhoto() {
    if(confirm('Are you sure you want to restore this deleted photo?')) {
        let getMetaDataField = window.opener && window.opener.getDataField ? window.opener.getDataField : getDataField ;
        let field = getMetaDataField(__PushToLinkedSettingsName), metaField;
        if(field && field.AssignedName) {
            metaField = field.AssignedName.replace('PhotoMetaField', 'MetaData');
        }
        
        let images = activeParcel.Photos.filter(function (i) { return (i.IsPrimary && i.Id != currentPhotoId) }); 
        let isPrimary = images.length > 0? '0':'1';
        
        $qc('recoverimage', { ImageId: currentPhotoId, MetaField: metaField, isPrimary: isPrimary }, function (data) {
            if (data.status == "OK") {

                var image = activeParcel.Images.find(function (i) { return i.Id == currentPhotoId });
                var delX = -1;
                for ( x in activeParcelPhotoEdits ) {
                    var ce = activeParcelPhotoEdits[x];
                    if ( ce.ParcelId == image.ParcelId)
                        if(ce.PhotoID == image.Id)
                            if ( ce.FieldId == field.Id )
                                delX = x;
                }
                if ( delX > -1 ) activeParcelPhotoEdits.splice( delX, 1 );
                
				let imgx = activeParcel.Photos.findIndex((x) => { return x.Id == currentPhotoId });
                activeParcel.Photos[imgx].CC_Deleted= false;
                if(metaField) activeParcel.Photos[imgx][metaField]= "true";
                $('.photo-recover-button').addClass('hidden');
                $(`.ChkSelPhoto[imgid=${currentPhotoId}]`).prop('disabled',false);
                $(`.ChkSelPhoto[imgid=${currentPhotoId}]`).siblings('.tabDeleted').remove();
                if (isPrimary == '1') { 
	                activeParcel.Photos[imgx].IsPrimary = true;
	                $('<a class="tabPrimary" ></a>').insertBefore($('.ChkSelPhoto[imgid="' + currentPhotoId + '"]'));
					if (window.opener) window.opener.showPhotoThumbnail();
					else showPhotoThumbnail();
				}
            } else console.log(data)
        });
    }
}

function loadPhotoProperties(callback) {
    var pfdiv = $('#photo-properties-frame');
	var fieldLength = $('td.value', pfdiv).length;
	var count = 0;
	$('td.value .input', pfdiv).removeAttr('fvalue')
    $('td.value', pfdiv).each(function (i, fs) {
        setPhotoFieldValue(fs,function(){
			count++;  
			if(fieldLength == count)
				if(callback) callback();
		});
    });
	$('#photo-properties-frame .parcel-field-values td.value').removeClass('unsaved');
	for(x in activeParcelPhotoEdits){
    	var ed = activeParcelPhotoEdits[x];
		if(ed.MetaData && ed.PhotoID == activeParcel.Photos[currentPhotoIndex].Id){
			$( 'td.value[fieldid="' + ed.FieldId + '"]' ).addClass( 'unsaved' );
        }
	}
}

function setPhotoFieldValue(fs, callback, callbackdata) {
    var fieldProcessor = function (callback) { if (callback) callback(); }
    var assignedName = $(fs).attr('assignedname');
    var metaField = assignedName.replace('PhotoMetaField', 'MetaData');
    var fval = "";
    var fname = $(fs).attr('fieldname');
    var ftype = $(fs).attr('datatype');
    var fieldId = $(fs).attr('fieldid');
    var field = datafields[fieldId];
    if (field.InfoContent && field.InfoContent.toString().trim() != '') {
    	var label = $(fs).siblings('.label');
    	$(label).find('.infoButton').show();
    	var w = $('span', $(label)).eq(0).width();
    	if (w > 155) $(label).width(w + 12);
	}
    if (fname != null) {
        fieldProcessor(function () {
            var currentPhoto = activeParcel.Photos[currentPhotoIndex]
            var v = currentPhoto[metaField];
            var OriginalValue = null;
            if(activeParcel.Images && activeParcel.Images.findIndex(function(ph){return ph.Id == currentPhoto.Id}) > -1){
            	var curPhotoIndex = activeParcel.Images.findIndex(function(ph){return ph.Id == currentPhoto.Id});
				OriginalValue = activeParcel.Images[curPhotoIndex][metaField];
			}
            if (field.InputType == 5) {
            	var cat = { Id: -2 }
               	//$('select', $(fs)).empty();
                getLookupData(field, { category: cat, source: currentPhoto, currentField: field, metaFieldCopy: metaField, ftype: ftype, fs: fs  }, function(data, dsource, doptions) {
					//$('select', $(fs)).html('<option value="${Id}">${Name}</option>');
					//$('select', $(fs)).fillTemplate(data);
					//var optionsCount = clientSettings.LookUpOptionsCount || 25
                    //if(data.length > optionsCount)
                      // $(".customddlspan",$(fs)).css("display","inline-block");
                   // else
                      // $(".customddlspan",$(fs)).css("display","none");
                    var cpfs = doptions && doptions.fs ? doptions.fs: fs;
                    var cpField = doptions && doptions.currentField ? doptions.currentField: field;
                    var cpMetaField =  doptions && doptions.metaFieldCopy ? doptions.metaFieldCopy: metaField;
                    var cpftype = doptions && doptions.ftype ? doptions.ftype: ftype;
                    currentPhoto = dsource;
                    
                    var v = { Value: currentPhoto[cpMetaField] || null };
                	var lkVal = v.Value? (data.filter(function(x) { return x.Id == v.Value.toString(); })[0]? data.filter(function(x) { return x.Id == v.Value.toString(); })[0]: null): null;
					var nonexists = !lkVal? true: false;
					var disVal = lkVal && lkVal.Name ? lkVal.Name: (lkVal? v.Value: '');
					// $('.cusDpdownSpan', $(cpfs)).siblings('.cusDpdown-arrow').css('display', 'inline-block');
                    
                    $('.cc-drop', $(cpfs))[0].getLookup.setData(data);
                	$('.cusDpdownSpan', $(cpfs)).html(disVal);
                	
					if(cpField && cpField.IsUniqueInSiblings == true) {
                        
                        let lp = $('.cc-drop', $(cpfs))[0].getLookup;
                        for (var k of Object.keys(lp.rows)) {
                            lp.rows[k]._disabled = false;
                        }
                        activeParcel.Photos.forEach(function(opt){
                            let sid = opt[metaField];
                            for (var k of Object.keys(lp.rows)) {
                                if(lp.rows[k].Id == sid) lp.rows[k]._disabled = true;
                            }
                        });
		       		}
		       		
					refreshRelatedPhotoFields(cpField, savePhotoProperty);
					setElementValue('.input', cpfs, v.Value, cpftype, cpField.DefaultValue, cpField.AutoSelectFirstitem, function () {
                    	if (callback) callback(callbackdata);
                	}, data, nonexists);
                });
            }
            else if (field.InputType == 13) {
	        	var radioOptions = [];
				if (field.RadioFieldValues && field.RadioFieldValues != "") {
            		var values = []; 
            		values = field.RadioFieldValues.trim().split(',');
            		radioOptions = [{ name : field.Id, text: 'Yes', value: values[0] }, { name : field.Id, text: 'No', value: values[1] }];
        		}
        		else
	        		radioOptions = [{ name : field.Id, text: 'Yes', value: 'true' }, { name : field.Id, text: 'No', value: 'false' }];
	    		if(field.AllowRadioNull == "true"){
	    			var nullOption = { name : field.Id, text: 'Blank', value: '' }
	    			radioOptions.push(nullOption);
	    		}
	    		var ielem =$('.parcel-field-values td[fieldid = "'+field.Id+'"] .input');
	    		$(ielem).html('');
				$(ielem).html('<input type="radio" id=${name}${value}  class="radioButton" name=${name} value=${value}><label for =${name}${value} >${text}</label></input>');
				$(ielem).fillTemplate(radioOptions);
				$(ielem).css('border','0px');
				$('input', $(ielem)).css('height','13px');
				var v = { Value: currentPhoto[metaField] || null };
				if((currentPhoto.DownSynced && (currentPhoto.DownSynced == '1' || currentPhoto.DownSynced == 'true')) && (clientSettings["PhotoNoDownsyncOnFlagging"] == '1' || clientSettings["PhotoNoDownsyncOnFlagging"] == 'true'))
					$('#photo-properties-frame').find('input').attr("disabled", true);
				else
                    $('#photo-properties-frame').find('input').removeAttr("disabled");
                    $('td.value[fieldid="' + fieldId + '"] .input').attr('fvalue', OriginalValue);
	        	setElementValue('.input', fs, v.Value, ftype, field.DefaultValue, field.AutoSelectFirstitem, function () {
                    if (callback) callback(callbackdata);
                });
        	}
			else{
            	setElementValue('.input', fs, v, ftype, field.DefaultValue, field.AutoSelectFirstitem, function () {
                    if (callback) callback(callbackdata);
                });
			}
            if (field.InputType != 13) {
                $('td.value[fieldid="' + fieldId + '"] .input').attr('fvalue', OriginalValue);
            }
            //FD_26439
            const $inputField = $('td.value[fieldid="' + fieldId + '"] .input');
            const isDropdownOrSelect = $inputField.hasClass('cc-drop') || $inputField.prop('type') === 'select-one';
            if ((field.ReadOnly || (field.IsReadOnlyDTR && __DTR) && isDropdownOrSelect)) {
                $inputField.attr('disabled', 'disabled');
            } else if (field.ReadOnly || (field.IsReadOnlyDTR && __DTR)) {
                $inputField.attr('readonly', 'readonly');
            }
            //FD_26439
			if (ftype == '12') {	
				$('input', fs).attr('readonly', 'readonly'); 			
				if (field.ReadOnly == "true" || ((currentPhoto.DownSynced && (currentPhoto.DownSynced == '1' || currentPhoto.DownSynced == 'true')) && (clientSettings["PhotoNoDownsyncOnFlagging"] == '1' || clientSettings["PhotoNoDownsyncOnFlagging"] == 'true')))
					$('.LoadGeolocationIcon', fs).hide();
				else
					$('.LoadGeolocationIcon', fs).show();
			}
        });
    }
}

//check/uncheck photos in QC/DTR
function checkAll(btn) {
    $('.add-photo-frame').hide();
    var cntChk = $('.ChkSelPhoto:checked:not([disabled])').length,
    	totChk = $('.ChkSelPhoto:not([disabled])').length;
    //             if ($('#SelectAllPh').hasClass('selected') && (cntChk == totChk)) {
    if (cntChk == totChk) {
        $('.ChkSelPhoto:not([disabled])').each(function () { $(this).prop("checked", false); });
        $('#SelectAllPh').html('Select All');
        //                $('#SelectAllPh').removeClass('selected');
    } 
    else {
        $('.ChkSelPhoto:not([disabled])').each(function () { $(this).prop("checked", true); });
        //                $('#SelectAllPh').addClass('selected');
        $('#SelectAllPh').html('Deselect All');
    }
    
    if ($('.ChkSelPhoto:checked').length > 0) {
    	$('#DownloadSelectedPh').parent('td').show();
    }
    else {
    	$('#DownloadSelectedPh').parent('td').hide();
    }
}

function btncheckAll(chk) {
    if ($(chk).is(':checked')) {
        var cntChk = $('.ChkSelPhoto:checked').length;
        var totChk = $('.ChkSelPhoto').length;
        if (cntChk == totChk) {
            $('#SelectAllPh').html('Deselect All');
            //  $('#SelectAllPh').addClass('selected');
        }


    } else {
        $('#SelectAllPh').html('Select All');
        //     $('#SelectAllPh').removeClass('selected');
    }
if($('.ChkSelPhoto:checked').length > 0){
    $('#DownloadSelectedPh').parent('td').show();
    }
    else{
    $('#DownloadSelectedPh').parent('td').hide();}
}

function showPh(divId) {
	if (!(isBPPParcel && activeParcel.CC_Deleted)){
    getData('SELECT * FROM Field WHERE SourceTable = \'_photo_meta_data\' AND DoNotShow = \'false\' ORDER BY AssignedName', [], function (fields) {
        if (fields.length == 0) {
            $('.photo-properties-button').hide();
        }
    });
    var d = $(divId).attr("divId");
    $('.photo-frame-allPhotos').hide();
    $('.add-photo-frame').hide();
    $('.photo-Manager-Back').show();
    $('.photo-frame').show();
    showPhoto(d);
    $('.photo-Manager').hide();
    }
    else {
    	alert('Photos cannot be added to a property that is deleted. Recovering this parcel will allow you to add photos.');
    }
}

function backPhotoMang() {
    if ($('#photo-properties-frame').is(':visible') == true) {
        $('#photo-properties-frame').hide();
        $('.photo-frame-allPhotos').hide();
        $('.photo-frame').show();
        showPhoto(currentPhotoIndex);
        $('.photo-Manager').hide();
        $('.photo-Manager-Back').show();
        $('#comploader').hide(); 

    }
    else if ($('.photo-frame').is(':visible') == true) {
        getPhotoUpdates();
        $('.photo-frame-allPhotos').show();
        $('.photo-frame').hide();
        $('.photo-Manager').show();
        $('.photo-Manager-Back').hide();
        $('#photo-properties-frame').hide();
        $('.downsycset').hide();
        $('.referenceId').hide();
        $('.ccimageId').hide();
        $('#comploader').hide(); 
    }
}

function photUploader() {
    let isReadOnlyUserDTR = __DTR && DTRRoles && DTRRoles.ReadOnly ? true : false;
    if (isReadOnlyUserDTR) {
        alert("You don't have permission to add photos ")
        return false;
    }
	if(activeParcel.CC_Deleted == true || (ccma.Session.RealDataReadOnly == true && activeParcel.IsRPProperty)){
		var msg = 'This option is available for the selected parcel.'
		if(activeParcel.CC_Deleted == true)
			msg = 'Photos cannot be added to a property that is deleted. Recovering this parcel will allow you to add photos.'
		alert(msg)
		return;
	}
   $('.add-photo-frame').show();
}
function showNoTabs(divId) {
	if ( !( isBPPParcel && activeParcel.CC_Deleted)){
	    if (activeParcel.Photos.length == 0) {
	        var d = $(divId).attr("divId");
	        $('.photo-frame-allPhotos').hide();
	        $('.add-photo-frame').hide();
	        $('.add-photo-frame').show();
	        showPhoto(d);
	        $('.photo-Manager').hide();
	
	    }
	}
}
function deSelectAll() {
	if ( !( isBPPParcel && activeParcel.CC_Deleted))
    	$('#SelectAllPh').html('Select All');
}
function nextPhotoProperties() {
    if (currentPhotoIndex > -1 && currentPhotoIndex < activeParcel.Photos.length - 1) {
        showPhoto(currentPhotoIndex + 1);
        $('.photo-Manager').hide();
        showPhotoProperties();
       // showPhotoPreview(currentPhotoIndex + 1)
    }
}


function prevPhotoProperties() {
    if (currentPhotoIndex > 0) {
        showPhoto(currentPhotoIndex - 1);
        $('.photo-Manager').hide();
        showPhotoProperties();
        //showPhotoPreview(currentPhotoIndex - 1);

    }
}

function refreshRelatedPhotoFields(field, saveMethod, newChange){
	var rfields = ccma.UI.RelatedFields[field.Id];
    if ((rfields == undefined) || (rfields == null)) {
        return false;
    }
    for (var x in rfields) {
        var sfield = datafields[rfields[x]];
        $('td[fieldid ="' + sfield.Id + '"]').each(function () {
            var el = $('.input', this);
            setPhotoFieldValue(this, function (sfield) {
            	if(newChange){
                	if (el.length > 0) {
                    	saveMethod(el[0], function () {
                    	    refreshRelatedPhotoFields(sfield, saveMethod);
                    	});
                	}
                } else {
                    refreshRelatedPhotoFields(sfield, saveMethod);
                }
            }, sfield);
        });

    }

}

function savePhotoProperty(el, callback) {
    var enqueue = true;
    var fieldId = $(el).parent().attr('fieldid');
    var field = datafields[fieldId];
    var value = $(el).val();
    if(field.InputType == 13)
    	var value = $('input:checked',el).val();
    if(field.InputType == "5"){
	     try {
	        value = decodeURI(value);
	     }
	     catch {  }
    }
    var assignedName = field.AssignedName;
    var metaField = assignedName.replace('PhotoMetaField', 'MetaData');
    var currentPhoto = activeParcel.Photos[currentPhotoIndex]
	var currentValue = $( 'td.value[fieldid="' + fieldId + '"] .input' ).attr('fvalue');  
	if (currentValue == value || (currentValue == null && value === "")) {
        enqueue = false;
        $( 'td.value[fieldid="' + fieldId + '"]' ).removeClass( 'unsaved' );
    }
    if(activeParcel.Photos.length) 
		activeParcel.Photos[currentPhotoIndex][metaField] = value;
	var delX = -1;
	for ( x in activeParcelPhotoEdits ) {
        var ce = activeParcelPhotoEdits[x];
        if ( ce.ParcelId == currentPhoto.ParcelId)
			if(ce.PhotoID == currentPhoto.Id)
            	if ( ce.FieldId == fieldId )
                    delX = x;
    }
    if ( delX > -1 )
        activeParcelPhotoEdits.splice( delX, 1 );
    if (enqueue) {
        var Photoedit = {
			ParcelId: currentPhoto.ParcelId,
			PhotoID: currentPhoto.Id,
			MetaData: metaField,
			MetaValue: value,
			FieldId: fieldId
		};
		activeParcelPhotoEdits.push(Photoedit);
		$( 'td.value[fieldid="' + fieldId + '"]' ).addClass( 'unsaved' );
    }
	if(field && field.IsUniqueInSiblings == true){
        if(field.InputType == "5") {
            let lp = $(el)[0].getLookup;
            for (var k of Object.keys(lp.rows)) {
                lp.rows[k]._disabled = false;
            }
            activeParcel.Photos.forEach(function(opt){
                let sid = opt[metaField];
                for (var k of Object.keys(lp.rows)) {
                    if(lp.rows[k].Id == sid) lp.rows[k]._disabled = true;
                }
            });            
        } else {
            $('option',el).removeAttr('disabled');
            $('option',el).css('background','')
                activeParcel.Photos.forEach(function(opt){
                    $('option[value = "'+opt[metaField]+'"]',el).attr('disabled','disabled');
                    $('option[value = "'+opt[metaField]+'"]',el).css('background','#DDD')
            });
        }
    }
    refreshRelatedPhotoFields(field, savePhotoProperty, true);
    if (callback) callback();
}

function setElementValue(selector, field, value, ftype, defaultValue, AutoSelectFirstitem, callback, lkdVals, nonexists) {
    var el = $(selector, field)[0];
    if (parseInt(ftype) == 11) {
        ccma.Data.Evaluable.setLookupList(el, ((v.Value) ? v.Value : '').split(','));
    } 
    else {
    	if (parseInt(ftype) == 5 && nonexists)
    		$(el).val('');
    	else
        	$(el).val(value);
    }
    if (parseInt(ftype) == 11 || parseInt(ftype) == 5 || parseInt(ftype) == 3) {
        if (( (value == null) || ( ( parseInt(ftype) == 5 && lkdVals && lkdVals.findIndex(element => element.Id == value) === -1 ) || ( parseInt(ftype) != 5 && $(el).html().indexOf('value="' + value + '"') == -1 ) ) ) && defaultValue == null && (AutoSelectFirstitem=='true' || AutoSelectFirstitem==true)) {
            if (parseInt(ftype) == 5 && lkdVals) {
            	if(!lkdVals[0]) {
            		$(el).val(''); $(el).html('');
            	}
            	else {
	            	var lkv = ( (lkdVals[0].Id == '' || lkdVals[0].Id == '<blank>' || lkdVals[0].Value == '<blank>') && lkdVals[1])? lkdVals[1]: lkdVals[0];
	            	var lId = !lkv.Id && lkv.Value ? (lkv.Value == '<blank>'? '': lkv.Value): lkv.Id;
	            	$(el).val(lId);
	            	var disNm = lkv.Name? lkv.Name : (lkv.Id? lkv.Id: '');
	            	$(el).html(disNm);
            	}
            }
            else {
	            if ($(el).find('option:first-child').val() == '')
	                $(el)[0].selectedIndex = 1;
	            else
	                $(el)[0].selectedIndex = 0;
            }
            savePhotoProperty(el, callback);
        }
    }
    if (parseInt(ftype) == 13) { 
    	$('input', $(el)).removeAttr('checked');
        $('input[value = "'+ value +'"]', $(el)).prop( "checked", true );
        savePhotoProperty(el, callback);
    }
    if (value == null && defaultValue != null) {
    	let fieldId = $(el).parent().attr('fieldid');
        let field = datafields[fieldId];
        if (field)
        	defaultValue = getDefaultValue(field, defaultValue);
        if (parseInt(ftype) == 3) defaultValue = defaultValue.toString(); 
        $(el).val(defaultValue);
        if (parseInt(ftype) == 5 && lkdVals ) {
        	var lkv = lkdVals.filter(function(ld) { return ld.Id == defaultValue })[0];
        	var disNm = lkv && lkv.Name ? lkv.Name: (lkv? defaultValue: '');
        	var cval = lkv ? defaultValue: '';
        	$(el).val(cval);
        	$(el).html(disNm);
        }
        else if(parseInt(ftype) == 5) {
        	$(el).val('');
        	$(el).html('');
        }
        savePhotoProperty(el, callback);
    } else{
        if (callback) callback();
    }
}

function savePhotoDefaultProperties(img){
	var imageIndex = activeParcel.Photos.length - 1;
	var pfdiv = $('#photo-properties-frame .parcel-field-values');
	let cloc = null;
	var defaultValuePhotoUpdate =  function() {
	    $('td.value',pfdiv).each(function (fs) {
	        var fieldId = $($('.value',pfdiv)[fs]).attr('fieldid');
	        var field = datafields[fieldId];
	        var value = field.DefaultValue;
	        var assignedName = field.AssignedName;
	        var metaField = assignedName.replace('PhotoMetaField', 'MetaData');
	        if (field.DefaultValue == null && field.AutoSelectFirstitem && field.InputType == "5") {
	            getLookupData(field, { category: -2, source: img, metaField: metaField }, function (ld, img, o) {
	                var metaField = o.metaField;
	                if (ld.length == 0) { if (callback) callback(); else return false; }
	                value = ld[1]? ld[1].Id : ld[0].Id;
	                activeParcel.Photos[imageIndex][metaField] = value;
	                var Photoedit = {
		            	ParcelId: img.ParcelId,
		            	PhotoID: img.Id,
		           		MetaData: metaField,
		           		MetaValue: value,
		           		FieldId: fieldId
	        		};
	        		activeParcelPhotoEdits.push(Photoedit);
	        	});
	        }
	        else {
	        	value = getDefaultValue(field,value);
	        	if (!value && field.InputType == '12' && cloc) {
	        		value = cloc.lat().toFixed(8)  + "," +  cloc.lng().toFixed(8);
	        	}
	            activeParcel.Photos[imageIndex][metaField] = value;
	            var Photoedit = {
	            	ParcelId: img.ParcelId,
	            	PhotoID: img.Id,
	           		MetaData: metaField,
	           		MetaValue: value,
	           		FieldId: fieldId
	        	};
	        	activeParcelPhotoEdits.push(Photoedit);
	        }
	    });
    }
    if (navigator && navigator.geolocation) {
    	navigator.geolocation.getCurrentPosition((position) => {
       		cloc = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
       		defaultValuePhotoUpdate();
		}, () => { defaultValuePhotoUpdate(); }); 
    }
    else defaultValuePhotoUpdate();
}

function getDefaultValue(field,defaultValue){
	if(defaultValue == undefined||defaultValue == null)
   		return null;
    switch (defaultValue.toString().toUpperCase()) {
    	case "CURRENT_DATE":
     	case "CURRENTDATE":
	        var dt = (new Date()).format("yyyy-MM-dd");
	        defaultValue = checkForCustomFormat(field.Id) == '' ? dt : deFormatvalue(dt, field.Id);
	        break;
        case "CURRENT_USER":
        case "CURRENTUSER":
            defaultValue = ccma.CurrentUser;
            break;
        case "CURRENT_YEAR":
     	case "CURRENTYEAR":
            defaultValue = ccma.CurrentYear;
            break;
        case "CURRENT_YEAR":
        case "CURRENTYEAR":
             case "ACTIVE_YEAR":
        case "ACTIVEYEAR":
            defaultValue = clientSettings.CurrentYearValue
            break;
        case "CURRENT_DAY":
        case "CURRENTDAY":
            defaultValue = ccma.CurrentDay;
            break;
        case "CURRENT_MONTH":
        case "CURRENTMONTH":
            defaultValue = ccma.CurrentMonth;
            break;
        case "FUTUREYEARSTATUS":
        case "FUTUREYEARSTATUS":
            defaultValue = ccma.FutureYearStatus;
            break;
        case "BLANK":
            defaultValue = ' ';
            break;
    }
    return defaultValue;
}

function checkLinkedProperties(selImages){
    if(!(clientSettings && clientSettings['PhotoDeleteAndRecover'] == 1 && clientSettings['DeletePhotoPromptForLinkedProperties'] == 1)) return false;
    let linkTable = clientSettings['DeletePhotoPromptTableCheck'];
    if(!(linkTable && activeParcel && activeParcel[linkTable])) return false;
    let count = activeParcel[linkTable]?.length;
    let field = getDataField(__PushToLinkedSettingsName);
    if(!(field && field.AssignedName)) return false;
    let metaField = field.AssignedName.replace('PhotoMetaField', 'MetaData');
    if(count > 0) {
        if(confirm(`This image is linked to ${count} other properties, do you want to delete the photo from all other linked properties as well?`)) {
            let value = false;  // Setting dropdown as No(0) as default value is Yes(1)
            selImages.split('$').forEach(function(im){
                var image = activeParcel.Images.find(function (i) { return i.Id == im });
                var delX = -1;
                for ( x in activeParcelPhotoEdits ) {
                    var ce = activeParcelPhotoEdits[x];
                    if ( ce.ParcelId == image.ParcelId)
                        if(ce.PhotoID == image.Id)
                            if ( ce.FieldId == field.Id )
                                delX = x;
                }
                if ( delX > -1 ) activeParcelPhotoEdits.splice( delX, 1 );
                if(activeParcel.Photos.length) 
                    activeParcel.Photos.find(img => img.Id == image.Id)[metaField] = value;
            });
            return true;
        } 
        else 
            return false;
    }
}