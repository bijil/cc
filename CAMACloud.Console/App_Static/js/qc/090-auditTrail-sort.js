﻿function checkSort(){
	if(auditSorted == false){
		resetSort();
		auditSort();
		$('#groupByRowUid')[0].innerText = "Chronological"
	}
	else if(auditSorted == true){
		resetSort();
		$('#groupByRowUid')[0].innerText = "GroupBy RowUID"
		auditSorted = false;
		$('#recent-audit-trail-table thead td').each(function(col) {
    	
			if($(this).html()==("Date Time\n                                            "))
			{		 
				sortOrder = -1;	      
				$(this).siblings().removeClass('asc sort-selected');
				$(this).siblings().removeClass('desc sort-selected');
				var arrData = $('#recent-audit-trail-table').find('tbody >tr:has(td)').get();
		
				arrData.sort(function(a, b) {
					var val1 = $(a).children('td').eq(col).text();
					var val2 = $(b).children('td').eq(col).text();
					if ($.isNumeric(val1) && $.isNumeric(val2))
					     return sortOrder == 1 ? val1 - val2 : val2 - val1;
					else
					     return (val1 < val2) ? -sortOrder : (val1 > val2) ? sortOrder : 0;
					});
			  
		     	$.each(arrData, function(index, row) {
		        			$('#recent-audit-trail-table tbody').append(row);
		      		});
				}
			});
		
			}
}
function resetSort() {
	
	$('#recent-audit-trail-table thead td').each(function(col) {
    $(this).removeClass("sort-selected");
    $(this).removeClass("desc");
    $(this).removeClass("asc");
    $(this).removeClass("focus");
    $('#recent-audit-trail-table *').off(); 
    $('#recent-audit-trail-table *').unbind(); 
    });
}
function auditSort() {
	auditSorted = true;
	var tempField = new Array();
	var regex = new RegExp("(#[0-9]+\.)");// matches aux rec in the format '#3864364.'
	var regex1 = new RegExp("(#[0-9]+\)");// matches aux rec in the format '#3864364'
	var regex2 = new RegExp("(Aux Record [0-9]+)");// matches aux rec in the format 'Aux Record 3864364.'
	var arrData = $('#recent-audit-trail-table').find('tbody >tr:has(td)').get();
	var i=0;
	$.each(arrData, function(index, row) {
        //console.log($(this));
		if(regex.test($(this).children().get(2).innerText)){
			var auxNum = $(this).children().get(2).innerText.match(regex)[0];
			auxNum = auxNum.replace(/#/g, '');
			auxNum = parseInt(auxNum.replace(/\./g, ''));
			tempField.push(auxNum);
		}
		else if(regex2.test($(this).children().get(2).innerText)){
			var auxNum = $(this).children().get(2).innerText.match(regex2)[0];
			auxNum = auxNum.replace(/[a-zA-Z]+/g, '');
			auxNum = parseInt(auxNum.replace(/\./g, ''));
			tempField.push(auxNum);
		}
		else{
			auxNum = 0;
			tempField.push(auxNum);
		}
		$(this).children(".auxNum").remove();
		var node = "<td style='display:none;' class='auxNum'>"+ auxNum +" </td>";       
		$(this).append(node);   
		i++;
		
	});
	$('#recent-audit-trail-table thead td').each(function(col) {
    	
	if($(this).html()==("Description\n                                            "))
	{		 
		$(this).addClass('desc sort-selected');
		$(this).removeClass('asc');
		sortOrder = -1;
			      
		$(this).siblings().removeClass('asc sort-selected');
		$(this).siblings().removeClass('desc sort-selected');
		var arrData = $('#recent-audit-trail-table').find('tbody >tr:has(td)').get();

		arrData.sort(function(a, b) {
			var val1 = $(a).children('td').eq(col+1).text();
			var val2 = $(b).children('td').eq(col+1).text();
			if ($.isNumeric(val1) && $.isNumeric(val2))
			     return sortOrder == 1 ? val1 - val2 : val2 - val1;
			else
			     return (val1 < val2) ? -sortOrder : (val1 > val2) ? sortOrder : 0;
			});
	  
     	$.each(arrData, function(index, row) {
        			$('#recent-audit-trail-table tbody').append(row);
      		});
		}
	});

		
  	$('#recent-audit-trail-table thead td').each(function(col) {
    	$(this).hover(
      		  function() {
		        $(this).addClass('focus');
		      },
		      function() {
		        $(this).removeClass('focus');
		      }
    	);
	
    	$(this).click(function() {
			if($(this).html()==("Description\n                                            "))
			{
	
				 if ($(this).is('.asc')) {
			        $(this).removeClass('asc');
			        $(this).addClass('desc sort-selected');
			        sortOrder = -1;
			      } else {
			        $(this).addClass('asc sort-selected');
			        $(this).removeClass('desc');
			        sortOrder = 1;
			      }
			      $(this).siblings().removeClass('asc sort-selected');
			      $(this).siblings().removeClass('desc sort-selected');
			      var arrData = $('#recent-audit-trail-table').find('tbody >tr:has(td)').get();

			      arrData.sort(function(a, b) {
			        var val1 = $(a).children('td').eq(col+1).text();
			        var val2 = $(b).children('td').eq(col+1).text();
			        if ($.isNumeric(val1) && $.isNumeric(val2))
			          return sortOrder == 1 ? val1 - val2 : val2 - val1;
			        else
			          return (val1 < val2) ? -sortOrder : (val1 > val2) ? sortOrder : 0;
			      });
	  
     			 $.each(arrData, function(index, row) {
        			$('#recent-audit-trail-table tbody').append(row);
      			});
	
	
	
			}
			else{
		    if ($(this).is('.asc')) {
		        $(this).removeClass('asc');
		        $(this).addClass('desc sort-selected');
		        sortOrder = -1;
		      } else {
		        $(this).addClass('asc sort-selected');
		        $(this).removeClass('desc');
		        sortOrder = 1;
		      }
		      $(this).siblings().removeClass('asc sort-selected');
		      $(this).siblings().removeClass('desc sort-selected');
		      var arrData = $('#recent-audit-trail-table').find('tbody >tr:has(td)').get();
		
		      arrData.sort(function(a, b) {
		        var val1 = $(a).children('td').eq(col).text().toUpperCase();
		        var val2 = $(b).children('td').eq(col).text().toUpperCase();
		        if ($.isNumeric(val1) && $.isNumeric(val2))
		          return sortOrder == 1 ? val1 - val2 : val2 - val1;
		        else
		          return (val1 < val2) ? -sortOrder : (val1 > val2) ? sortOrder : 0;
		      });
			  
		      $.each(arrData, function(index, row) {
		        $('#recent-audit-trail-table tbody').append(row);
		      });
		    }});
		  });
		}