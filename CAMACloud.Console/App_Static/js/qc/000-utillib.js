﻿String.prototype.trim = function () { return this.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); };
String.prototype.ltrim = function () { return this.replace(/^\s+/, ''); }
String.prototype.rtrim = function () { return this.replace(/\s+$/, ''); }
String.prototype.fulltrim = function () { return this.replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g, '').replace(/\s+/g, ' '); }


Date.fromString = function (str) { return new Date(Date.parse(str)); }
Date.from = function (input) { return new Date(Date.parse($(input).val())); }
Date.prototype.toUSString = function () { return (this.getMonth() + 1).padLeft(2, '0') + "/" + this.getDate().padLeft(2, '0') + '/' + this.getFullYear(); }

Number.prototype.padLeft = function (width, ch) {
    if (!ch) {
        ch = " ";
    }

    if (("" + this).length >= width) {
        return "" + this;
    }
    else {
        return arguments.callee.call(
				  ch + this,
				  width,
				  ch
				);
    }
};
String.prototype.padLeft = function (width, ch) {
    if (!ch) {
        ch = " ";
    }

    if (("" + this).length >= width) {
        return "" + this;
    }
    else {
        return arguments.callee.call(
      ch + this,
      width,
      ch
    );
    }
};

String.prototype.padRight = function (width, ch) {
    if (!ch) {
        ch = " ";
    }

    if (("" + this).length >= width) {
        return "" + this;
    }
    else {
        return arguments.callee.call(
      this + ch,
      width,
      ch
    );
    }
};


function copyDataRows(a) {
    var copy = [];
    for (x in a) {
        copy.push(copyDataObject(a[x]));
    }
    return copy;
}

function copyDataObject(o) {
    var copy = new Object();
    for (x in o) {
        copy[x] = o[x];
    }
    return copy;
}


function isTrue(v) {
    return v == "true" || v == true;
}