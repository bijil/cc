// QC
// For hiding popup on outside click
let eventString = "click touchmove touchstart touchend mousedown keydown";
$(document)
    //.off(eventString)       // Uncommented due to impact where all previous document listeners are unbinded.File should be loaded only once(FD13798).
    .on(eventString, (e) => {
        // mscroll is set from bindFocusForInputs() in static/js/004-app.js to check whether to close the popup or rePosision it
        let el = $(".cc-drop.cc-drop-focus"),
            scroll = el.attr("mscroll");
        if (!(e.type == "keydown" && $(e.target).hasClass("cc-drop-customtext"))) {
            if (scroll != "0") {
                if (
                    (e.type == "keydown" && $(e.target).closest("div.cc-drop-pop .cc-drop-search").length <= 0) ||
                    ($(e.target).closest(".cc-drop.cc-drop-focus").length <= 0 &&
                        $(e.target).closest(".cc-drop-pop").length <= 0)
                ) {
                    //if (e.type == "keydown") el.blur(); //Pressing Arrow keys will not focus out from the input field
                    $(".cc-drop").removeClass("cc-drop-focus");
                    $(".cc-drop-pop").hide();
                }
            } else {
                el[0]?.getLookup?.rePositionLookup();
            }
        }
    });
// In case event gets focused without click event fired
$(document)
    .off("focusin", ".cc-drop")
    .on("focusin", ".cc-drop", (e) => {
        if (!$(e.target).attr("readonly")) $(e.target).attr({ readonly: true, mfocus: true });
    });
$(document)
    .off("focusout", ".cc-drop")
    .on("focusout", ".cc-drop", (e) => {
        if ($(e.target).attr("mfocus")) $(e.target).removeAttr("mfocus readonly");
    });

//For hiding popup on scroll event
$(window)
    //.off("wheel")       // Uncommented due to impact where all previous document listeners are unbinded.File should be loaded only once.
    .on("wheel", function (e) {
        if ($(e.target).closest(".cc-drop-pop").length <= 0) {
            $(".cc-drop.cc-drop-focus").removeAttr("readonly").blur();
            $(".cc-drop-pop").hide();
            $(".cc-drop").removeClass("cc-drop-focus");
        }
    });

// 1. Used for reposition the lookup when keyboard becomes active
// 2. Will hide the popup when orientation is changed
function reCalculateLookupPosition(landscape, orientationChanged) {
    let el = $(".cc-drop.cc-drop-focus")[0];
    if (el) {
        let lp = el?.getLookup;
        if (!lp) return false;
        if (keyboardActive) lp.rePositionLookup();
        if (orientationChanged) {
            lp.hide();
            return false;
        }
    }
}
/**
 *  lookup plugin definition
 */
// (function ($) {
$.lookup = function (element, options, data) {
    /**
     * lookup plugin defaults
     * @type {Object}
     */
    var defaults = {
        popupView: true,
        width: 252,
        height: 30,
        popupHeight: 250,
        searchActive: false,
        searchActiveAbove: 3,
        searchAutoFocus: false,
        onSave: null,
        customText: false
    };

    var plugin = this;

    var popupDiv = "div.cc-drop-pop",
        $popupDiv = $("div.cc-drop-pop"),
        popupList = "div.cc-drop-pop .cc-drop-items",
        $popupList = $("div.cc-drop-pop .cc-drop-items"),
        search = "div.cc-drop-pop .cc-drop-search",
        $search = $("div.cc-drop-pop .cc-drop-search"),
        customText = "div.cc-drop-pop .cc-drop-customtext",
        $customText = $("div.cc-drop-pop .cc-drop-customtext");

    plugin.settings = {};

    var $element = $(element),
        element = element;

    plugin.init = function () {
        plugin.settings = $.extend({}, defaults, options);

        //Encrypting Id Values
        plugin.rows = _formatData(data);
        plugin.popupBelow = true;
        $element[0].getLookup = {
            settings: plugin.settings,
            rows: plugin.rows,
            setData: (d) => {
                plugin.setData(d);
            },
            rePositionLookup: () => {
                let m = _calculatePosition();
                $popupList.css(m.height);
                $popupDiv.css({ ...m.position });
                $(element).removeAttr("mscroll");
            },
            hide: plugin.hide,
            onSave: (f) => {
                plugin.settings.onSave = f;
            },
            addNonExist: (d, cinput) => {
                plugin.addNonExist(d, cinput);
            },
        };

        // $element[0].rows = plugin.rows;
        // $element[0].setRow = setData;

        $element.addClass("cc-drop");
        $search.hide();
        $customText.hide();
        _initDropdownElement();
    };

    plugin.setData = function (d) {
        let tData = _formatData(d);
        plugin.rows = tData;
        $element[0].getLookup.rows = tData;
    };

    plugin.hide = function () {
        $(".cc-drop.cc-drop-focus").removeAttr("readonly").blur();
        $(".cc-drop").removeClass("cc-drop-focus");
        $(".cc-drop-pop").hide();
        $search.blur();
    };

    plugin.onSave = function (e) {
        if (plugin.settings.onSave) plugin.settings.onSave(e);
    };

    plugin.addNonExist = function (val, cinput) {
        let tData = $element[0].getLookup.rows;
        let nKey = Object.keys(tData).length; // Since object keys returns an array/object whose index starts from 0
        tData[nKey] = { Id: encodeURI(val), Name: val, nonexist: true, _disabled: true, cinput: cinput };
        plugin.rows = tData;
        $element[0].getLookup.rows = tData;
    }
    var _formatData = function (d) {

        let arr = [];
        for (var k of Object.keys(d)) {
            let td = _.clone(d[k]);
            if (td.Name) td.Name = __replaceChars(td.Name);
            if (td.Id) {
                if (typeof td.Id === 'string') td.Id = __replaceChars(__decodeURI(td.Id));
                td.Id = encodeURI(td.Id);
            }
            arr.push(td);
        }
        return arr;
    }

    var _initDropdownElement = function () {
        $popupDiv
            .css({ width: plugin.settings.width })
            .removeClass("cc-drop-full cc-drop-mini")
            .addClass(plugin.settings.popupView ? "cc-drop-mini" : "cc-drop-full");

        $element.off("click").on("click", function (e) {
            // $element.attr("readonly", true); // To disable editing on the dropdown input field
            $(".cc-drop").removeClass("cc-drop-focus");
            $(e.target).addClass("cc-drop-focus");
            _populateDropdown();
            let wt = $(e.target).attr('customWidth') && $(e.target).attr('customWidth') != '' ? (parseInt($(e.target).attr('customWidth')) + 20) : plugin.settings.width;
            $popupDiv.css({ opacity: 0, width: wt, display: "flex" });
            $popupList.css({ "max-height": "none" });
            let m = _calculatePosition();
            $popupList.css(m.height);
            $popupDiv.css({ ...m.position, opacity: 1 });
            if (plugin.settings.searchActive) {
                $search.val("");
                plugin.settings.searchAutoFocus && $search.focus();
            }
            if (plugin.settings.customText) {
                let nv = plugin.rows.filter((x) => { return x.nonexist })[0]?.Id;
                if (nv) $customText.val(nv);
                else $customText.val("");
            }
        });
    };

    var _populateDropdown = function () {
        _clearDropdown();
        let data = $element[0].getLookup.rows;
        for (var k of Object.keys(data)) {
            // Changed .text() method to html() as there might be some html entities in the Name sometimes
            let ex = $("<span>").attr({ val: data[k].Id, disabled: data[k]._disabled }).html(data[k].Name);
            if (data[k].nonexist) ex.attr("nonexist", "");
            // .onclick(_setSelected($(this)));
            if (data[k].Id == $element.attr("sel") || data[k].Id == $element.val()) ex.attr("selected", true);
            $popupList.append(ex);
        }
        $popupList.append(
            $("<p>")
                .attr({ class: "drop-empty" })
                .css({
                    display: "none",
                    margin: 0,
                    padding: "4px 8px",
                    "font-style": "italic",
                    "font-weight": 500,
                    color: "#000",
                })
                .text("No results found")
        );
        if (plugin.settings.searchActive && plugin.settings.searchActiveAbove < Object.keys(plugin.rows).length) {
            $search
                .show()
                .off("keyup")
                .on("keyup", function (e) {
                    let qs = $(e.target).val();
                    _searchElement(qs);
                });
        } else $search.hide();

        if (plugin.settings.customText) {
            $customText
                .show()
                .off("blur")
                .on("blur", function (e) {
                    let qs = $(e.target).val();
                    _customTextChange(e.target);
                    //_searchElement(qs);
                });
        } else $customText.hide();

        $("span", popupList).on("click", function (e) {
            let disabled = $(e.target).attr("disabled");
            if (disabled == "true" || disabled == "disabled") e.preventDefault();
            else _dropdownItemClick($(e.target));
        });
    };

    var _customTextChange = function (item) {
        $("span", popupList).removeAttr("selected");
        let val = $(item).val();
        $element.val(val, true).html(val).change();
        $popupDiv.hide();
        __addNonExists($element, val);
        plugin.onSave(element);
    };

    var _dropdownItemClick = function (item) {
        $("span", popupList).removeAttr("selected");
        let val = item.attr("val"),
            text = item.text();
        item.attr({ selected: true });
        $element.val(val).html(text).change();
        // $element.trigger();
        // saveInputValue(element);
        $popupDiv.hide();
        plugin.onSave(element);
    };

    var _clearDropdown = function () {
        $popupList.html("");
    };

    var _searchElement = function (q) {
        let c = $("span", popupList).length;
        $("span", popupList).each(function (i, x) {
            if ($(x).text().toUpperCase().indexOf(q.toUpperCase()) < 0) {
                c--;
                $(x).hide();
            } else {
                c++;
                $(x).show();
            }
        });
        $(".drop-empty", popupList)[c <= 0 ? "show" : "hide"]();

        if (!plugin.popupBelow) {
            let offset = $element.offset();
            let ph = $popupDiv.height();
            $popupDiv.css({ top: offset.top - ph - 15 });
        }
    };

    var _calculatePosition = function () {
        let offset = $element.offset();
        let left = Math.min(offset.left, window.innerWidth - plugin.settings.width);
        let top = offset.top + plugin.settings.height;
        let h = $popupDiv.height() || plugin.settings.popupHeight;
        let ph = $popupDiv.height(),
            phOffset = 10;
        let wh = window.innerHeight;
        if (plugin.settings.searchActive && plugin.settings.searchActiveAbove < Object.keys(plugin.rows).length)
            phOffset = 60;

        //To determine whether the space is greater above or below clicked element
        let popBelow = wh / 2 > offset.top ? true : false;
        //For forcing popup below when popupHeight is lesser than half of window height even when the element is nearer to bottom
        if (!popBelow && ph < wh - top - 15) popBelow = true;
        plugin.popupBelow = popBelow;
        if (popBelow) {
            h = wh - top - phOffset;
            $popupDiv.removeClass("cc-drop-above");
        } else {
            $popupDiv.addClass("cc-drop-above");
            if (ph > offset.top) {
                h = offset.top - phOffset;
                top = 10;
            } else top = offset.top - ph - 15;
        }

        //Height is set for the options listing div and top left to the popup div
        return { position: { top, left }, height: { "max-height": h } };
    };

    plugin.init();
};

$.fn.lookup = function (options, data) {
    return this.each(function () {
        if (undefined == $(this).lookupData) {
            var plugin = new $.lookup(this, { ...options }, { ...data });
            /**
             * Intended initially to use .data() -which is better than current one- but was forced to change to [0].getLookup because
             * MA uses zepto instead of jQuery and zepto .data() method is very much different than jQuery
             */
            // $(this).data("lookup", plugin);
            Object.defineProperty($(this)[0], "getLookup", {});
            return false;
        }
    });
};

// Needed if element is input
// Input fields doesn't support html() calls. It has only val() for getting and setting values.
// But we are using html() to set display value and val() for choosing value/Id of options
// So we need to override the html() calls so that if the field is a cc-drop lookup field
// then we calls the val() instead with arguments passed.

var __valCall = $.fn.val; // We are saving the intial method so that we can invoke it for other field types.
$.fn.val = function (value, ctext) {
    //If the field is only the new lookup type we need to call overriden methods otherise traditional html() call is invoked
    if (this.hasClass("cc-drop")) {
        if (arguments.length >= 1) {
            let tValue;
            // Used incase the encoded value hasn't been decoded before calling the setter
            // If the value contains a % symbol the decode URI might fail, in that case we'll use the default value
            if (typeof value == "number") tValue = value;
            else {
                tValue = __replaceChars(__decodeURI(value));
                tValue = encodeURI(tValue);
            }
            // CC_3187 forced a change to make the lookup value setting to work like normal select
            // ie Setting a selected item using .val() method will set the display text itself
            // Note that can be overrided by using .html() as it'll work, BUT WILL NOT CHANGE THE SELECTED VALUE
            if (ctext) {
                this.attr("sel", value);
                return this;
            }
            let lp = this[0] && this[0].getLookup;
            let rows = lp && lp.rows;
            let idNotFound = true; // If id is not matched it's better to set a blank value as default
            for (var k of Object.keys(rows)) {
                if (rows[k].Id === '') {
                    if (rows[k].Id === tValue || (typeof rows[k].Id === 'undefined' && tValue == "" && rows[k].Value=='<blank>')){
                        this.html(rows[k].Name).attr("sel", rows[k].Id);
                        idNotFound = false;
                        break;
                    }
                } else if (rows[k].Id == tValue) {
                    this.html(rows[k].Name).attr("sel", rows[k].Id);
                    idNotFound = false;
                    break;
                }                
            }
            if(idNotFound) this.html("").attr("sel", "");
            return this;
        } else {
            let id = __decodeURI(this.attr("sel"));
            return id;
        }
    }
    return __valCall.apply(this, arguments);
};

var __htmlCall = $.fn.html; // We are saving the intial method so that we can invoke it for other field types.
$.fn.html = function (value) {
    //If the field is only the new lookup type we need to call overriden methods otherise traditional html() call is invoked
    if (this.hasClass("cc-drop")) {
        if (arguments.length >= 1) {
            if (arguments[0] == null) {
                arguments[0] = "";
            }
            __valCall.apply(this, arguments);
            return this;
        } else return __valCall.apply(this, arguments);
    }
    return __htmlCall.apply(this, arguments);
};

//Replace any html entity codes coming to corresponding character
const __replaceChars = (s) => {
    if (s && typeof s === 'string') {
        s = s.replaceAll('&lt;', '<')?.replaceAll('&gt;', '>')?.replaceAll('&amp;', '&')?.replaceAll('&quot;', '"')?.replaceAll('&apos;', "'");
        return s;
    } else return s;
}

//For decodeURI without try catch
const __decodeURI = (v) => {
    v = (v === null || v === undefined) ? '' : v;
    try {
        return decodeURI(v);
    } catch {
        return v;
    }
}

var __addNonExists = (el, val) => {
    let lp = $(el)[0] && $(el)[0].getLookup;
    if (lp) {
        let rows = { ...lp.rows }, isExists = false;
        for (var k of Object.keys(rows)) {
            let tId = __decodeURI(rows[k].Id);
            if (rows[k].Id === '') {
                if (val == null || val === '') {
                    isExists = true;
                    break;
                }
            } else if (tId == val) {
                isExists = true;
                break;
            }
        }
        if (!isExists) {
            lp.addNonExist(val, true);
        }
    }
}
// })(jQuery);
