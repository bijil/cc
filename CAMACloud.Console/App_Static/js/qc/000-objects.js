﻿//    DATA CLOUD SOLUTIONS, LLC ("COMPANY") CONFIDENTIAL 
//    Unpublished Copyright (c) 2010-2013 
//    DATA CLOUD SOLUTIONS, an Ohio Limited Liability Company, All Rights Reserved.

//    NOTICE: All information contained herein is, and remains the property of COMPANY.
//    The intellectual and technical concepts contained herein are proprietary to COMPANY
//    and may be covered by U.S. and Foreign Patents, patents in process, and are protected
//    by trade secret or copyright law. Dissemination of this information or reproduction
//    of this material is strictly forbidden unless prior written permission is obtained
//    from COMPANY. Access to the source code contained herein is hereby forbidden to
//    anyone except current COMPANY employees, managers or contractors who have executed
//    Confidentiality and Non-disclosure agreements explicitly covering such access.</p>

//    The copyright notice above does not evidence any actual or intended publication
//    or disclosure of this source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of COMPANY. ANY REPRODUCTION, MODIFICATION,
//    DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
//    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND
//    IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
//    OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
//    TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL
//    ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

ccma.Data.Evaluable = {};

var childRecCalcPattern = /\\.|(sum|avg)|\(\)/i;


ccma.Data.Evaluable.__eval = function (source, expression, outType, parentSource, dvRules) {
    // Assumptions: Calculations done only on number data types
    // For all other types, exit after the first variable
    if (source && source.indexOf('[') > -1) {
        var t = eval('this.' + source);
        if (t == undefined)
            return false;
        else
            if (!t && expression.indexOf('parent.') > -1) {
                source = parentSource;
                expression = expression.replace(/parent./g, '')
            }
    }
    if (expression === undefined && (!!source)) {
        outType = null;
        expression = source;
        source = null;
    }

    var tableName, ParentTable, actualTable;
    if (source) {
        if (source.indexOf('[') > -1)
            actualTable = source.substr(0, source.indexOf('['));
        else
            actualTable = source;
    }

    if (outType) {
        if ((typeof outType == "string") || (typeof outType == "number"))
            outType = QC.dataTypes[outType];
        else if (typeof outType == "object")
        { } else (outType = null);
    }

    var result = ccma.Data.Evaluable.__getEvalResult();

    var originalExp = expression.toString();
    var field = getDataField(expression, actualTable);
	var calcField = false;
    if (field) if ((field.CalculationExpression != null) && (field.CalculationExpression != "")) {
        if ((field.CalculationOverrideExpression != null) && (field.CalculationOverrideExpression != "")) {
            if (!activeParcel.EvalValue(source, decodeHTML(field.CalculationOverrideExpression))) {
                expression = field.CalculationExpression;
    			calcField = true;
            }
        } else if (field.CalculationExpression.search(childRecCalcPattern) == -1) {
        	expression = field.CalculationExpression;
        	calcField = true;
    	}
    }

    var isMIFieldTrue = (field?.['UI_Settings']?.MIFields ?? "") === "true";
    var fdt;
    var orgExpression = '';
    var testExp = expression;
    var ifcondition = /.*?([a-zA-Z][a-zA-Z0-9_#.]+)(?=[^']*(?:'[^']*'[^']*)*$).*?/i.exec(testExp);
    if (ifcondition && ifcondition[1].toUpperCase() == "IF" && isMIFieldTrue && clientSettings?.LoadMethodAfterAppLoad == "loadValidationTablesForMI") {
        try {
            var value = 0;
            var original = 0;
            var exp = expression.toString();
            var mfieldid = exp.match(/([a-zA-Z_][a-zA-Z0-9_]+)/g)
            if (mfieldid != null) {
                var ufieldid = mfieldid.filter((item, index) => mfieldid.indexOf(item) === index);
                var type = '';                
                var iforiginalexp;
                var ifvvalue = exp.replace(/if/g, 'IF');
                ifvvalue = ifvvalue.replace(/If/g, 'IF');
                ifvvalue = ifvvalue.replace(/([^<>=])=([^<>=])/g, '$1==$2');
                for (var v in ufieldid)
                    if (ufieldid[v].toUpperCase() != "IF") {
                        result.Value = eval('this' + (source ? '.' + source : '') + type + "['" + ufieldid[v] + "']");
                        result.Original = eval('this' + (source ? '.' + source : '') + '.Original' + "['" + ufieldid[v] + "']");
                        result.Value = result.Value === null || result.Value === "" ? 0 : result.Value;
                        result.Original = result.Original === null || result.Original === "" ? 0 : result.Original;
                        var v1 = ufieldid[v];
                        ifvvalue = ifvvalue.replaceAll(v1, result.Value);
                        iforiginalexp = ifvvalue.replaceAll(v1, result.Original);
                    }
                var vvalue = ifvvalue;
                var originalvvalue = iforiginalexp;
                value = eval(vvalue);
                original = eval(originalvvalue);
            }
            else {
                value = expression;
            }

        } catch (e) {
            console.warn('Error while evaluating ' + expression, e);
            return ccma.Data.Evaluable.__getEvalResult(null, "****", "****");
        }
    }
    else {
        while ((match = /.*?([a-zA-Z][a-zA-Z0-9_#.]+)(?=[^']*(?:'[^']*'[^']*)*$).*?/i.exec(testExp)) != null) {
            var v = match[1];
            var fieldExp = v;
            tableName = actualTable;
            var input = match.input;
            var type = ''
            while (v.indexOf('parent.') > -1) {
                tableName = parentSource;
                v = v.replace('parent.', '');
                if ((source && source != null) && eval('this.' + source + '.parentRecord') != null && eval('this.' + source + '.parentRecord'))
                    type += '.parentRecord'
                else
                    source = null
            }
            //if(v) v = v.toLocaleLowerCase();
            if (v.indexOf('parcel.') > -1) {
                tableName = null;
                v = v.replace('parcel.', '');
                source = null;
            }
            if (v == 'ccma.CurrentYear' || v == 'CurrentYear' || v == 'Current_Year') {
                var regex = new RegExp(v, "g");
                testExp = testExp.replace(regex, ccma.CurrentYear);
                expression = expression.replace(/CurrentYear/g, 'ccma.CurrentYear').replace(/Current_Year/g, 'ccma.CurrentYear')
                continue;
            }
            else if (v == 'Current_User' || v == 'CurrentUser' || v == 'CURRENT_USER') {
                var regex = new RegExp(v, "g");
                testExp = testExp.replace(regex, "'" + ccma.CurrentUser + "'")
                expression = expression.replace(/CurrentUser/g, 'ccma.CurrentUser').replace(/Current_User/ig, 'ccma.CurrentUser').replace(/CURRENT_USER/g, 'ccma.CurrentUser');;
                continue;
            }
            else if (v == 'Current_Date' || v == 'CurrentDate') {
                var regex = new RegExp(v, "g");
                testExp = testExp.replace(regex, "'" + ccma.CurrentDate + "'")
                expression = expression.replace(/CurrentDate/g, 'ccma.CurrentDate').replace(/Current_Date/g, 'ccma.CurrentDate')
                continue;
            }
            else if (v == 'Today') {
                var regex = new RegExp(v, "g");
                testExp = testExp.replace(regex, "'" + ccma.Today + "'")
                expression = expression.replace(/Today/g, 'ccma.Today');
                continue;
            }
            else if (v == 'Tomorrow') {
                var regex = new RegExp(v, "g");
                testExp = testExp.replace(regex, "'" + ccma.Tomorrow + "'")
                expression = expression.replace(/Tomorrow/g, 'ccma.Tomorrow');
                continue;
            }
            else if (v == 'CurrentDay' || v == 'Current_Day') {
                var regex = new RegExp(v, "g");
                testExp = testExp.replace(regex, ccma.CurrentDay);
                expression = expression.replace(/CurrentDay/g, 'ccma.CurrentDay').replace(/Current_Day/g, 'ccma.CurrentDay')
                continue;
            }
            else if (v == 'Current_Month' || v == 'CurrentMonth') {
                var regex = new RegExp(v, "g");
                testExp = testExp.replace(regex, ccma.CurrentYear);
                expression = expression.replace(/CurrentMonth/g, 'ccma.CurrentMonth').replace(/Current_Month/g, 'ccma.CurrentMonth')
                continue;
            }
            else if (v == 'FutureYearStatus') {
                var regex = new RegExp(v, "g");
                testExp = testExp.replace(regex, "'" + ccma.ShowFutureYearStatus + "'")
                expression = expression.replace(/FutureYearStatus/g, 'ccma.FutureYearStatus');
                continue;
            }
            if (v && v.contains('.')) {
                split = v.split('.')
                tbName = split[0];
                fName = split[1];
                if (false && tableListGlobal.indexOf(tbName) > -1 && ['any', 'all', 'max', 'min', 'count', 'sum', 'avg', 'first', 'last'].indexOf(split[split.length - 1]) > -1) {
                    type += '.' + tbName;
                    v = v.replace(tbName + '.', '');
                    type += '.map(function(i){ return i.' + fName + '})';
                    v = v.replace(fName + '.', '');
                }
            }
            var tf = getDataField(v, tableName);
            if (v == 'null') {
                break;
            }
            result.Value = eval('this' + (source ? '.' + source : '') + type + "['" + v + "']");
            if ((result.Value == null) && !source)
                result.Value = activeParcel[v];
            if (result.Value === undefined && !type.contains('map')) {
                //console.warn('Undefined property found while evaluating expression - ' + v)
                if (activeParcel) {
                    if (source) {
                        result.Value = (/\.IN\./g.test(v)) ? isSiblingsTrue(source, v) : activeParcel[actualTable][source.substring(source.indexOf('[') + 1, source.length - 1)][input];
                        return result;
                    }
                }
                else {
                    return ccma.Data.Evaluable.__getEvalResult(null, "***", "***");
                }
            }

            var dt = {};

            if (tf) {
                dt = QC.dataTypes[parseInt(tf.InputType)];
            } else if (v.startsWith('SUM') || v.startsWith('AVG') || v.startsWith('COUNT') || v.startsWith('MAX') || v.startsWith('MIN') || v.startsWith('FIRST') || v.startsWith('LAST')) {
                dt = getDatatypeOfAggregateFields(v);
                var aggrfield = true;
                //dt = ccma.Data.Types[2];
            } else {
                dt = QC.dataTypes[1];
            }
            if (!dt)
                dt = QC.dataTypes[2];
            if (!fdt)
                fdt = dt;

            result.Value = getLargeDecToRound(result.Value, dt);

            if ((dt.jsType != 'Number' && dt.jsType != 'String' && dt.jsType != 'Date') || (v == expression)) {
                result.Original = eval('this.Original' + (source ? '.' + source : '') + "['" + v + "']");
                if (field) {
                    var NumericScale = field.NumericScale;
                    var NumPrecision = field.NumericPrecision;
                    var NumericTextValue = result.Value ? result.Value.toString().replace('.', '').length : 0;
                    var NumericTextOrgValue = result.Original ? result.Original.toString().replace('.', '').length : 0;

                    if (dt.jsType == 'Number' && result.Value && isNaN(result.Value) == false && result.Value.toString().indexOf('.') > -1 && NumPrecision && NumericTextValue > parseInt(NumPrecision)) {
                        result.Value = result.Value.toString().substring(0, NumPrecision + 1);
                    }
                    if (dt.jsType == 'Number' && result.Original && isNaN(result.Original) == false && result.Original.toString().indexOf('.') > -1 && NumPrecision && NumericTextOrgValue > parseInt(NumPrecision)) {
                        result.Original = result.Original.toString().substring(0, NumPrecision + 1);
                    }
                    if (result.Value && NumericScale && result.Value.toString().indexOf('.') > -1 && dt.jsType == 'Number') {
                        if (NumericScale == 0)
                            result.Value = result.Value.toString().substring(0, result.Value.toString().indexOf('.'))
                        else
                            result.Value = result.Value.toString().substring(0, result.Value.toString().indexOf('.') + parseInt(NumericScale) + 1)
                    }
                    if (result.Original && NumericScale && result.Original.toString().indexOf('.') > -1 && dt.jsType == 'Number') {
                        if (NumericScale == 0)
                            result.Original = result.Original.toString().substring(0, result.Original.toString().indexOf('.'))
                        else
                            result.Original = result.Original.toString().substring(0, result.Original.toString().indexOf('.') + parseInt(NumericScale) + 1)
                    }
                }
                if (result.Value && result.Value.trim)
                    result.Value = result.Value.trim();
                if (result.Original && result.Original.trim)
                    result.Original = result.Original.trim();

                if (dt.dataType == "Lookup" && typeof result.Value == "number") {
                    result.Value = parseFloat(result.Value);
                    result.Original = parseFloat(result.Original);
                }

                var displayOptions = {};
                if (field && field.LookupQueryFilter && activeParcel) {
                    displayOptions.referenceFilter = field.LookupQueryFilter;
                    displayOptions.referenceObject = eval('activeParcel.' + source);
                }

                let rv = result.Value, rov = result.Original;
                if (field && field.Id && field.UIProperties && field.UIProperties != null) {
                    rv = customFormatValue(rv, field.Id);
                    rov = customFormatValue(rov, field.Id);
                }

                result.DisplayValue = (dt.dataSource == 'ccma.YesNo') ? evalYesNo(rv) : (dt.dataSource == "field.LookupTable") && (!aggrfield) ? evalLookup(v, rv, null, field, null, displayOptions) : ((rv != null) && (rv !== '')) ? rv.formatout((outType || dt).formatter) : '';
                result.DisplayOriginal = (dt.dataSource == 'ccma.YesNo') ? evalYesNo(rov) : (dt.dataSource == "field.LookupTable") && (!aggrfield) ? evalLookup(v, rov, null, field, null, displayOptions) : ((result.Original != null) && (result.Value !== '')) ? rov.formatout((outType || dt).formatter) : '&lt;blank&gt;';
                result.ChangedValue = (result.DisplayOriginal == result.DisplayValue || typeof (result.DisplayValue) != "string" || (field && (field.ReadOnly == "true" || field.ReadOnly == true)) || (tf && tf.Category && (tf.Category.IsReadOnly == true || tf.Category.IsReadOnly == "true"))) ? result.DisplayValue : (result.DisplayValue ? (result.DisplayValue.substr(0, 10) == "data:image" ? result.DisplayValue : '<span class="EditedValue">' + result.DisplayValue + '</span>') : '');
                result.ChangedValue = (result.ChangedValue == NaN || result.ChangedValue == 'NaN') ? '' : result.ChangedValue;
                return result;
            }

            var valueEx = '';
            if (type.contains('map'))
                valueEx = 'thisACTIVEDATASOURCE' + (source ? '.' + source : '') + type + "." + v + "";
            else
                valueEx = 'thisACTIVEDATASOURCE' + (source ? '.' + source : '') + type + "['" + v + "']";

            if (dt.jsType == 'Number' || (calcField && dt.jsType == 'Number')) {
                var tex = 'safeParseFloat(' + valueEx + ',true,' + calcField + ',' + dvRules + ')';
            }
            else if ((dt.jsType == 'String' || dt.jsType == 'Date') && !calcField && !type.contains('map')) {
                var tex = 'safeParseString(' + valueEx + ')';
            }
            else {
                var tex = valueEx;
            }
            var rr = new RegExp("(?=\\b)(" + fieldExp + ")(?=\\b)", "g");
            if (fieldExp.indexOf('.') == -1) expression = expression.replace(/\./g, '###_');
            expression = expression.replace(rr, tex);
            expression = expression.replace(/###_/g, '.');
            if (fieldExp.indexOf('.') == -1) testExp = testExp.replace(/\./g, '###_');
            testExp = testExp.replace(rr, '');
            testExp = testExp.replace(/###_/g, '.');
            if (testExp.indexOf('"') > -1) {
                var k = testExp.substring(testExp.indexOf('"') + 1, testExp.length);
                testExp = k.substring(k.indexOf('"') + 1, k.length);
            }
            orgExpression = expression.contains('return i.') ? expression.replace(/return i./g, 'return i.Original.') : (expression.contains('parentRecord') ? expression.slice(0, expression.lastIndexOf('parentRecord')) + expression.slice(expression.lastIndexOf('parentRecord')).replace('parentRecord', 'parentRecord.Original') : expression.replace(/ACTIVEDATASOURCE/g, '.Original'));

        }

        var value, original;
        try {
            (fdt) ? ((fdt.jsType == 'String') || (fdt.jsType == 'Number') ? expression = replaceLikeOperators(expression, fdt.jsType) : '') : '';
            (fdt) ? ((fdt.jsType == 'String') || (fdt.jsType == 'Number') ? orgExpression = replaceLikeOperators(orgExpression, fdt.jsType) : '') : '';
            value = eval(expression.replace(/ACTIVEDATASOURCE/g, ''));

            try {   // in dtr if create new record then that values is not inserted in to original.So error occur when evaluating original expression.
                original = eval(orgExpression.replace(/ACTIVEDATASOURCE/g, ''));
            } catch (ex) {
                console.warn('Error while evaluating ' + orgExpression, ex);
                fdt = (calcField && field) ? QC.dataTypes[field.InputType] : fdt;
                fdt = value === true || value === false ? null : fdt
                outType = outType || fdt;
                if (field && (field.InputType == '8' || field.InputType == '10'))
                    value ? value = Math.round(value) : value;
                if (expression.match(/.*?lookup\[.*?\].*?/))
                    return ccma.Data.Evaluable.__getEvalResult(outType, value, "-", field);
                else
                    return ccma.Data.Evaluable.__getEvalResult(outType, value, "****", field);
            }

        } catch (e) {
            console.warn('Error while evaluating ' + expression, e);
            if (expression.match(/.*?lookup\[.*?\].*?/)) {
                return ccma.Data.Evaluable.__getEvalResult(null, "-", "-");
            }
            else {
                return ccma.Data.Evaluable.__getEvalResult(null, "****", "****");
            }
        }

    }
   
           
	fdt = (calcField && field)? QC.dataTypes[field.InputType]: fdt;
    fdt = value === true || value === false ? null : fdt
    outType = outType || fdt;
    if(field && (field.InputType == '8' || field.InputType == '10'))
		value ? value = Math.round(value) : value;
    return ccma.Data.Evaluable.__getEvalResult(outType, value, original,field);
};

ccma.Data.Evaluable.__getEvalResult = function (type, value, original, field) {
    if (value && value.trim)
        value = value.trim();
    if (original && original.trim)
        original = original.trim();
    if (!type)
        type = QC.dataTypes[1];

    let rv = value, rov = original;
    if (field && field.Id && field.UIProperties && field.UIProperties != null) {
        rv = customFormatValue(rv, field.Id);
        rov = customFormatValue(rov, field.Id);
    }

    var DisplayValue = rv ? rv.formatout(type.formatter) : rv == 0 ? 0 : '';
    var DisplayOriginal = rov ? rov.formatout(type.formatter) : rov == 0 ? 0 : '&lt;blank&gt;';
    var result = {
        Value: value,
        DisplayValue: DisplayValue,
        Original: DisplayOriginal,
        DisplayOriginal: original != null ? original.formatout(type.formatter) : '&lt;blank&gt;',
        ChangedValue: (DisplayOriginal == DisplayValue || typeof (DisplayValue) != "string") ? DisplayValue : (DisplayValue ? (DisplayValue.substr(0, 10) == "data:image" ? DisplayValue : '<span class="EditedValue">' + DisplayValue + '</span>') : ''),
        toString: function () { return this.Value; }
    };

    if (field){
            var NumericScale = field.NumericScale;
            var NumPrecision = field.NumericPrecision;
            var NumericTextValue = result.Value ? result.Value.toString().replace('.','').length : 0;
            var NumericTextOrgValue = result.Original ? result.Original.toString().replace('.','').length : 0;

            result.Value = getLargeDecToRound(result.Value, type);
            
            if (type.jsType == 'Number' && result.Value && isNaN(result.Value) == false && result.Value.toString().indexOf('.') > -1 && NumPrecision && NumericTextValue > parseInt(NumPrecision)) {
				result.Value = result.Value.toString().substring( 0, NumPrecision + 1);
			}
			if (type.jsType == 'Number' && result.Original && isNaN( result.Original ) == false && result.Original.toString().indexOf( '.' ) > -1 && NumPrecision && NumericTextOrgValue > parseInt(NumPrecision )) {
				result.Original = result.Original.toString().substring( 0, NumPrecision + 1);
			}
            if (type.jsType == 'Number' && result.Value &&  result.Value.toString().indexOf( '.' ) > -1 &&  NumericScale){
                if ( NumericScale == 0 )
                    result.Value = result.Value.toString().substring(0, result.Value.toString().indexOf('.') )
                else
                    result.Value = result.Value.toString().substring( 0, result.Value.toString().indexOf( '.' ) + parseInt( NumericScale ) + 1 )
            }
            if (type.jsType == 'Number' && result.Original && result.Original.toString().indexOf( '.' ) > -1 && NumericScale){
                if ( NumericScale == 0 )
                    result.Original = result.Original.toString().substring(0, result.Original.toString().indexOf('.') )
                else
                    result.Original = result.Original.toString().substring( 0, result.Original.toString().indexOf( '.' ) + parseInt( NumericScale ) + 1 ) 
            }
			if (type.jsType == 'Number' && isNaN( result.Value ) == true && (field.CalculationExpression != null) && (field.CalculationExpression != "")){ 				
                                result.Value = value ? value.formatout(type.formatter) : value == 0 ? 0 : ''
			}
     } 
    return result;
}


ccma.Data.Evaluable.OrigEval = function (source, expression) {
    var result = this.__eval(source, expression);
    return result.Original;
};

ccma.Data.Evaluable.EvalValue = function (source, expression, datatype, parentSource, dvRules) {
    var result = this.__eval(source, expression, null, parentSource, dvRules);
    return result.Value;
};

ccma.Data.Evaluable.Test = function (source, expression) {
    if (expression === undefined && (!!source)) {
        expression = source;
        source = null;
    }
    return this.__eval(source, expression, 3).Value;
}


ccma.Data.Evaluable.EvalText = function (source, expression, datatype) {
    var result = this.__eval(source, expression);
    return result.ChangedValue;
};

ccma.Data.Evaluable.Eval = function (source, expression) {
    return this.__eval(source, expression);
};

ccma.Data.Evaluable.Lookup = function (field, rvalue) {
    if (rvalue == null)
        rvalue = "n";
    var value = "";
    value = eval("this." + field);
    var lv;
    try {
        lv = getFieldLookup(field)[value];
    } catch (e) {
        console.warn("Error while accessing lookup table: ", field, value, getFieldLookup(field), true);
        return "ERR";
    }
    if (value == '') {
        return value;
    }
    if (lv === undefined) {
        //console.warn('No lookup available for ' + value + ' in ' + field);
        return "-"; //NULL Value Lookup
        //return value + " (N/L)";
    }
    switch (rvalue.toLowerCase()) {
        case "n":
        case "name":
            return lv.Name;
            break;
        case "d":
        case "desc":
        case "description":
            return lv.Description;
            break;
        default:
            return lv.Name;
            break;
    }
}

ccma.Data.Evaluable.setLookupList = function (select, value) {

    try {
        for (var i = 0; i < select.options.length; i++) {
            o = select.options[i];
            o.selected = false;
            if (value.indexOf(o.value) != -1) {
                o.selected = true;
            }
        }
    }
    catch (e) {
        console.error('error in object.js/ccma.Data.Evaluable.setLookupList :' + e.message);
    }
}

ccma.Data.Evaluable.sortArray = function (Array, prop, asc) {
    var k = [];
    k = Array.sort(function (a, b) {
        if (asc) return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
        else return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
    });
    // k.forEach(function (e) { console.log(e[prop]); });
    return k;
}
ccma.Data.Evaluable.EvalObjectPRC = function (object, expression, f,contextName) {
    if (expression == "")
        return '';
    var aggreField = false;
    var originalExp = expression.toString();
    var tblname = contextName ? contextName : object.DataSourceName;
    if(window.opener)
    	var field = window.opener.getDataField(expression, tblname), calcFieldvalues;
    else 
    	var field = getDataField(expression, tblname), calcFieldvalues;
    if (field && field.CalculationExpression && field.CalculationExpression.trim() != '' && field.CalculationExpression.search(childRecCalcPattern) > -1) {
          var tempVal = calculateChildRecords(field, null, object);
          calcFieldvalues = tempVal.toString();
    }
   
    var result = ccma.Data.Evaluable.__getEvalResult();
    if (expression.startsWith('SUM_') || expression.startsWith('AVG_') || expression.startsWith('COUNT_') || expression.startsWith('MAX_') || expression.startsWith('MIN_') || expression.startsWith('FIRST_') || expression.startsWith('LAST_') || expression.startsWith('MEDIAN_')){
		aggreField = true;
		var tempObject = _.clone(object);
		if(activeParcel)
			object = activeParcel;
    }

    var Value = eval('object.' + expression);
    var orgValue = object["Original"] ? eval('object.Original.' + expression) : null;
    if (Value && Value != undefined && object["Original"]) {
        result.Value = Value;
        if (f != null) { result.Value = eval(f + '(result.Value);'); }
        result.Original = orgValue;
        if (f != null) { result.Original = eval(f + '(orgValue);'); }
        
        if (calcFieldvalues && calcFieldvalues != NaN && calcFieldvalues != "") {
            result.Value = calcFieldvalues.toString();
        }

        if (field) {
            let ndt = QC.dataTypes[parseInt(field.InputType)];
            result.Value = getLargeDecToRound(result.Value, ndt);
        }

        if (field && field.Id && field.UIProperties && field.UIProperties != null) {
            result.Value = customFormatValue(result.Value, field.Id);
            result.Original = customFormatValue(result.Original, field.Id);
        }

        result.ChangedValue = ( result.Value == result.Original  || ( field && (field.ReadOnly=="true"|| field.ReadOnly == true) ) ) ? result.Value : ( result.Value != null ? ( ( typeof (result.Value) == "string" && result.Value.substr(0, 10) == "data:image" ) ? result.Value : '<span class="EditedValue">' + result.Value + '</span>' ) : '' );  
    }
    else {
        if (field && field.Id && field.UIProperties && field.UIProperties != null) {
            Value = customFormatValue(Value, field.Id);
            orgValue = customFormatValue(orgValue, field.Id);
        }

        result.ChangedValue = Value;
        if (f != null) { result.ChangedValue = eval(f + '(Value);'); }
    }

    if (result.ChangedValue == undefined) { result.ChangedValue = ""; }
    if(aggreField) { object = tempObject; }
    
    return result.ChangedValue;
}

ccma.Data.Evaluable.EvalObject = function (object, expression, tableName, showDescription) {
    var originalExp = expression.toString();
    var field = getDataField(expression, tableName);
    var calcField = false;
    if (field) if ((field.CalculationExpression != null) && (field.CalculationExpression != "")) {
        if ((field.CalculationOverrideExpression != null) && (field.CalculationOverrideExpression != "")) {
            if (!activeParcel.EvalObject(object, decodeHTML(field.CalculationOverrideExpression), tableName).Value){
                expression = field.CalculationExpression;
                calcField = true;
            }
        } else if (field.CalculationExpression.search(childRecCalcPattern) == -1 && field.CalculationExpression.search(/parent/i) == -1) {
        	expression = field.CalculationExpression;
        	calcField = true;
        	}
    }
    var fdt;
    var testExp = expression.toString();

    var result = ccma.Data.Evaluable.__getEvalResult();

    var obj = object

    while ((match = /.*?([a-zA-Z][a-zA-Z0-9_]+).*?/i.exec(testExp)) != null) {
        var v = match[1];
        var tf = getDataField(v, tableName);

        result.Value = eval('obj.' + v);
        // if (result.Value && result.Value.Value) result.Value = result.Value.Value;
        if (result.Value === undefined) {
            //console.warn('Undefined property found while evaluating expression - ' + v)
            return ccma.Data.Evaluable.__getEvalResult(null, "***", "***");
        }

        var dt;
        if (tf) {
            dt = QC.dataTypes[parseInt(tf.InputType)];
        } else {
            dt = QC.dataTypes[1];
        }

        if (!fdt)
            fdt = dt;
        if ((dt.jsType != 'Number' && dt.jsType != 'String') || (v == expression)) {
            result.Original = obj["Original"] ? eval('obj.Original.' + v) : null;
            var displayOptions = {};
            if (field && field.LookupQueryFilter ) {
                displayOptions.referenceFilter = field.LookupQueryFilter;
                displayOptions.referenceObject = obj;
            }

            let rv = result.Value, rov = result.Original;
            if (field && field.Id && field.UIProperties && field.UIProperties != null) {
                rv = customFormatValue(rv, field.Id);
                rov = customFormatValue(rov, field.Id);
            }

            result.Value = getLargeDecToRound(result.Value, dt);

            result.DisplayValue = getDisplayValue(v, rv, dt, field, null, showDescription, displayOptions ); // (dt.dataSource == 'ccma.YesNo') ? evalYesNo(result.Value) : (dt.dataSource == "field.LookupTable") ? evalLookup(v, result.Value, null, field) : result.Value ? result.Value.format(dt.formatter) : '';
            result.DisplayOriginal = getDisplayValue(v, rov, dt, field, null, showDescription, displayOptions );  //(dt.dataSource == 'ccma.YesNo') ? evalYesNo(result.Original) : (dt.dataSource == "field.LookupTable") ? evalLookup(v, result.Original, null, field) : result.Original ? result.Original.format(dt.formatter) : '&lt;blank&gt;';
            var approvedField = activeParcel.ParcelChanges.filter(function (e) { return e.FieldName == expression && e.NewValue == result.Value && e.AuxROWUID == obj.ROWUID && e.QCChecked == false && e.SourceTable == tableName })
            if (result.DisplayValue) {
                  var copyvalue = (result.DisplayValue).toString();
                  copyvalue = copyvalue.slice(0, (copyvalue.indexOf("."))+3);
            }
            var roundValue = (result.DisplayValue && (field && field.InputType == 2) && result.DisplayValue.contains('.')) ? copyvalue : result.DisplayValue;     
            result.ChangedValue = (result.DisplayOriginal == result.DisplayValue || typeof (result.DisplayValue) != "string" || (field && (field.ReadOnly=="true"|| field.ReadOnly == true)) || (tf && tf.Category &&(tf.Category.IsReadOnly == true || tf.Category.IsReadOnly == "true"))) ? roundValue  : (result.DisplayValue ? (result.DisplayValue.substr(0, 10) == "data:image" ? result.DisplayValue : '<span class="EditedValue">' + roundValue  + '</span>') : '');
            return result;
        }

        var valueEx = 'obj.' + v;
        if (dt.jsType == 'Number') {
            var tex = 'safeParseFloat(' + valueEx + ')';
        }
        else if (dt.jsType == 'String' && !calcField) {
            var tex = 'safeParseString(' + valueEx + ')';
        }
        else {
            var tex = valueEx;
        }
        expression = expression.replace(new RegExp(v, "g"), tex);
        testExp = testExp.replace(new RegExp(v, "g"), '');
        if (testExp.indexOf('"') > -1) {
            var k = testExp.substring(testExp.indexOf('"') + 1, testExp.length);
            testExp = k.substring(k.indexOf('"') + 1, k.length);
        }
    }

    var value, original;
    try {
        (fdt) ? ((fdt.jsType == 'String') || (fdt.jsType == 'Number') ? expression = replaceLikeOperators(expression, fdt.jsType) : '') : '';
        value = eval(expression);
        original = eval(expression);
    } catch (e) {
        console.warn('Error while evaluating ' + expression, e);
        if (expression.match(/.*?lookup\[.*?\].*?/)) {
            return ccma.Data.Evaluable.__getEvalResult(null, "-", "-");
        }
        else {
            return ccma.Data.Evaluable.__getEvalResult(null, "****", "****");
        }
    }

    return ccma.Data.Evaluable.__getEvalResult(fdt, value, original);
};


function getDisplayValue( fieldName, value, type, field, blank, getDescriptionForLookup, options ){
    if (!blank) blank = '';
    return ( type.dataSource == 'ccma.YesNo' ) ? evalYesNo( value == null ? false : value ) : ( type.dataSource == "field.LookupTable" ) ? evalLookup( fieldName, value, getDescriptionForLookup, field, null, options ) : (value ||  value === 0) ? value.formatout( type.formatter ) : blank;
}

function evalLookup( group, id, getDescription, field, getNumericValue, options, disText ){
    var fieldId = field ? field.Id : null;
    if (getFieldLookup(group, fieldId) == null && !disText) {
        console.warn('Missing lookup table - ' + group);
        return "???";
    }
    if ( (id == undefined) || (id == null) || (id == "" && id !== 0 && !disText) )  {
        return "";
    }
    id = id.toString().trim();
    var item = (getFieldLookup( group, fieldId, options ) != null)? getFieldLookup( group, fieldId, options )[id]: null;
    
    if (disText) {
    	if (item == null || (!item.Name)) {
    		if (id == '' && isTrue(field.LookupShowNullValue))
    			return  '-- No Value --';
    		else
    			return id;
    	}
    	return item.Name;
    }
    
    if (item == null || !item.Name) 
        return id
    if (getDescription === 'SHORTDESC')
        return item.Name;
    else if (getDescription === 'LONGDESC')
        return item.Description;
    else if (getDescription === 'SHORTDESC, LONGDESC')
        return item.Name + ' - ' + item.Description;
    else if (getDescription)
        return item.Description;
    if (getNumericValue)
            return item.NumericValue;
    return item.Name;
}

function getLargeDecToRound(val, dt) {
    if ((dt.jsType == 'Number' || dt.dataSource == "field.LookupTable") && !isNaN(val) && val?.toString().split('.')[1]?.length > 12) {
        let decPart = val?.toString().split('.')[1];
        if (decPart?.substr(-5).slice(0, -1) == '9999') {
            val = parseFloat(val);
            if (dt.jsType == 'Number') { val = val ? parseFloat(val.toFixed(10)) : val; }
            else { val = val ? parseFloat(val.toFixed(2)) : val; }
        }
    }
    return val;
}