﻿var QC = [];
var ppl;
QC.dataTypes = {
    1: { "cctype": 1, "typename": "Text", "htmltag": "input", "type": "text","pattern": "", "maxlength": 50, "rows": null, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "string", "jsType": "String", "formatter": "s" },
    2: { "cctype": 2, "typename": "Real Number", "htmltag": "input", "type": "number", "pattern": "^[\-]?([0-9]*(.[0-9]{1,10})?)?$", "maxlength": 15, "rows": null, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "float", "jsType": "Number", "formatter": ",." },
    3: { "cctype": 3, "typename": "Yes/No", "htmltag": "select", "type": null,"pattern": "", "maxlength": null, "rows": null, "multiple": null, "dataSource": "ccma.YesNo", "changeEvent": "change", "dataType": "string", "jsType": "String", "formatter": "s" },
    4: { "cctype": 4, "typename": "Date", "htmltag": "input", "type": "date","pattern": "", "maxlength": 10, "rows": null, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "date", "jsType": "Date", "formatter": "d" },
    5: { "cctype": 5, "typename": "Lookup", "htmltag": "select", "type": null,"pattern": "", "maxlength": null, "rows": null, "multiple": null, "dataSource": "field.LookupTable", "changeEvent": "change", "dataType": "string", "jsType": "String", "formatter": "s" },
    6: { "cctype": 6, "typename": "Long Text", "htmltag": "textarea", "type": null,"pattern": "", "maxlength": 200, "rows": 2, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "string", "jsType": "String", "formatter": "s" },
    7: { "cctype": 7, "typename": "Money", "htmltag": "input", "type": "number", "pattern": "^([0-9]*(.[0-9]{1,4})?)?$", "maxlength": 15, "rows": null, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "money", "jsType": "Number", "formatter": "$" },
    8: { "cctype": 8, "typename": "Whole Number", "htmltag": "input", "type": "number", "pattern": "^(?:[1-9][0-9]*|0)$", "maxlength": 15, "rows": null, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "int", "jsType": "Number", "formatter": "," },
    9: { "cctype": 9, "typename": "Cost Value", "htmltag": "input", "type": "number", "pattern": "^[0-9]*$","maxlength": 15, "rows": null, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "value", "jsType": "Number", "formatter": "$$$" },
    10: { "cctype": 10, "typename": "Year", "htmltag": "input", "type": "number", "pattern": "^(0|9999|[12][0987][0-9][0-9])?$", "maxlength": 4, "rows": null, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "year", "jsType": "Number", "formatter": "i" },
    11: { "cctype": 11, "typename": "Lookup List", "htmltag": "select", "type": null,"pattern": "", "maxlength": null, "rows": null, "multiple": "multiple", "dataSource": "field.LookupTable", "changeEvent": "change", "dataType": "string", "jsType": "String", "formatter": "s" },
    12: { "cctype": 12, "typename": "Geo Location", "baseType": "text", "htmltag": "input", "type": "text", "pattern": "", "maxlength": 30, "rows": null, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "string", "jsType": "String", "formatter": "g" },
    13: { "cctype": 13, "typename": "Tri State Radio", "baseType": "text", "htmltag": "radiogroup", "type": null, "pattern": "", "maxlength": null, "rows": null, "multiple": null, "dataSource": null, "changeEvent": "change", "dataType": "string", "jsType": "String", "formatter": "s" }
};
var ccma = [];
 ccma = {
    Constants: {},
    Data: {
        Controller: null,
        Formatters: {},
        FieldCategories: [],
        LookupMap: {},
        Types: {}
    },
    Map: {},
    Session: {
        User: null,
        IsLoggedIn: false
    },
    ClientSettings: {},
    Sync: {},
    UI: {
        ActiveScreenId: null,
        ActivePhoto: null,
        ActiveFormTabId: null,
        Camera: {
            MaxPhotosPerParcel: 10,
            MaxCaptureWidth: 1200,
            MaxCaptureHeight: 900,
            MaxStorageWidth: 500,
            MaxStorageHeight: 375
        },
        FieldProps: {},
        RelatedFields: {},
        ParcelLinks: {
            OpenedFromLink: false,
            LinkMap: {}
        },
        SortScreenFields: [],
        SortOptions: [{ Id: 1, Name: 'Geo Location' }, { Id: 2, Name: 'Parcel ID - Ascending' }, { Id: 3, Name: 'Parcel ID - Descending'}],
        EstimateChartFields: [],
        Events: {
            refreshParcelListAfterDataEntry: false,
            refreshDataCollectionEstimate: false
        },
        Validations: [],
        LookupQueryCustomFields: [],
        Layout: {
            Screen: {
                UsableHeight: 0,
                UsableWidth: 0
            }
        }

    },
    Sketching: {
        SketchFormatter: null,
        Config: null
    }
 };
 

 Date.prototype.addDays = function (days) { var date = new Date(this.valueOf()); date.setDate(date.getDate() + days); return date; }
 Date.prototype.addDay = function (days) { var edate =new Date($('#startdate').val());var date =new Date($('#enddate').val());edate.setDate(edate.getDate() + days);edate.setHours(date.getHours());edate.setMinutes(date.getMinutes());return edate;}
 Date.prototype.toUSStringTime = function () { return (this.getMonth() + 1).padLeft(2, '0') + "/" + this.getDate().padLeft(2, '0') + '/' + this.getFullYear().padLeft(2, '0') + " " + this.getHours().padLeft(2, '0') + ":" + (this.getMinutes()< 10 ? '0' : '') + this.getMinutes(); }
 
 ccma.__defineGetter__( "CurrentDay", function () { return ( new Date() ).getDate() } );
 ccma.__defineGetter__( "CurrentMonth", function () { return ( new Date() ).getMonth() + 1 } );
 ccma.__defineGetter__("CurrentYear", function () { return (new Date()).getFullYear() });
 ccma.__defineGetter__("CurrentDate", function ()  { return ((new Date()).formatout('ymd')) + ' 12:00:00' }); 
 ccma.__defineGetter__("CurrentUser", function ()  { return currentLoginId; });
 ccma.__defineGetter__("Today", function () { return (new Date()).formatout("ymd") });
 ccma.__defineGetter__("Tomorrow", function () { return (new Date()).addDays(1).formatout("ymd") });
 ccma.__defineGetter__("FutureYearStatus", function () {  return (showFutureData ? 'F' : 'A') }); 