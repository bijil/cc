﻿//11159	    MD40,MR54,U85,R20,L20xD85

var sketchApp;
var touchClickEvent = 'click';
var activeParcel = {};
var CAMACloud = {};
var clientSettings = {};
var sketchSettings = {};
var vectorLabelPositions = [];
var copyVector = null;
var _keyElement = {};
var sketchEditorOpenFirstTime = false;
_keyElement.lenth=0;
var msg_sketcheditor_close = 'There are unsaved changes to the sketch. Tap OK to continue exiting the editor and lose the pending changes or CANCEL to return to the sketch editor to save your changes or to continue editing.';

activeParcel.Sketches = [
    {
        rowuid: 593,
        label: "A13",
        vector: "MR6,R16,U9,R6,U40,R11,U11,L35,D9,L4,D42,R6,D9"
    },
    {
        rowuid: 594,
        label: "A11",
        vector: "MR55,R16,U31,L16,D5,L10,D16,R10,D10"
    },
    {
        rowuid: 595,
        label: "A11",
        vector: "MR55,MU40,R28,U18,L28,D18"
    },
    {
        rowuid: 596,
        label: "BLDG",
        vector: "MR80,R20,U24,L20,D24"
    }
]

activeParcel.Sketches = [
    {
        rowuid: 593,
        label: "A13",
        vector: "U26,R5,U20,R30,D15,R30,D31,L65"
    }
]

function onResize() {

    var docHeight = window.innerHeight;
    var docWidth = window.innerWidth;
    console.log(docHeight, docWidth);
    $('.ccse-toolbar').height(40);
    $('.ccse-canvas').height(docHeight - 85);
    $('.ccse-canvas').width(docWidth);
    $('.ccse-canvas').attr('height', (docHeight - 85)  + 'px');
    $('.ccse-canvas').attr('width', docWidth + 'px');
    if( window.opener && window.opener.appType == "sv" && ( window.opener.lightWeight || ( window.opener.clientSettings && window.opener.clientSettings["SketchValidationEditSketch"] != "1" ) ) ) {
    	$('.ccse-canvas').height(docHeight - 45);
    	$('.ccse-canvas').attr('height', (docHeight - 45)  + 'px');
    }
    if (sketchApp) {
    	var _scale = sketchApp.scale? sketchApp.scale: 1;
        sketchApp.refresh(_scale);
        sketchApp.panSketchPosition(true); //Check later CC_3840 case 9
        sketchApp.render();
    }

    if (window.onafterresize)
        window.onafterresize();

}

window.onresize = onResize;

var winTop = screenTop, winLeft = screenLeft;
window.setInterval(function () {
    if (winTop != screenTop || winLeft != screenLeft) {
        if (window.onpositionchange)
            window.onpositionchange();
    }

    winTop = screenTop;
    winLeft = screenLeft;
}, 500);

$(function () {
    onResize();
    var config;
    var formatter;
    if (window.opener != null) {
        clientSettings = window.opener.clientSettings;
        sketchSettings = window.opener.sketchSettings;
        data = window.opener.activeParcel.CCSketchSegments;
       
        if ( clientSettings["SketchConfig"] != null || sketchSettings["SketchConfig"] != null )
            config = eval('CAMACloud.Sketching.Configs.' + ( clientSettings["SketchConfig"] || sketchSettings["SketchConfig"] ) );
        else
            config = CAMACloud.Sketching.Configs.GetConfigFromSettings();

        if (!formatter)
            if (config.formatter)
                formatter = eval('CAMACloud.Sketching.Formatters.' + config.formatter);
        if (!formatter)
            if (window.opener.ccma.Sketching.SketchFormatter)
                formatter = window.opener.ccma.Sketching.SketchFormatter;
        if (!formatter) {
            formatter = CAMACloud.Sketching.Formatters.TASketch;        
        }
        if ( clientSettings["sketchOriginPosition"] || sketchSettings["sketchOriginPosition"] )
        	formatter.originPosition = clientSettings["sketchOriginPosition"] || sketchSettings["sketchOriginPosition"];
        if (sketchSettings["RoundToDecimalPlace"] == 'Half Foot' || sketchSettings["RoundToDecimalPlace"] == 'Quarter Foot' || sketchSettings["RoundToDecimalPlace"] == 'Rounded') {
            sketchSettings.SegmentRoundingFactor = sketchSettings["RoundToDecimalPlace"] == 'Half Foot' ? 2 : (sketchSettings["RoundToDecimalPlace"] == 'Quarter Foot' ? 4 : 1);
        }

       if( window.opener && window.opener.appType=="sv" && ( window.opener.lightWeight || ( clientSettings && clientSettings["SketchValidationEditSketch"] != "1" ) ) ){
        	$('.ccse-mode-buttons').hide();      	
        }

    }
    sketchApp = new CCSketchEditor('.ccse-canvas', formatter);
    sketchPad = new CCSketchPad('.sketch-pad', sketchApp);
	if (config && config.encoder) {
		var encodeName = config.encoder; 
       	var sketchEncoder = CAMACloud.Sketching.Encoders[encodeName];
        if (sketchEncoder) {
            sketchApp.encoder = sketchEncoder;
           // sketchRenderer.encoder = sketchEncoder;
        } else {
            	console.error('Invalid sketch encoder: ')
       	}
    } 
	
    if (formatter.arcMode && formatter.arcMode != 'disable') {
        $('.sp-circle', sketchPad.selector).html(formatter.arcMode);
        $('.sp-circle', sketchPad.selector).attr('cmd', formatter.arcMode);
        
    }

    if (formatter.arcMode == 'ARC') {
        $('.sp-circle', sketchPad.selector).html('ARC');
        $('.sp-circle', sketchPad.selector).attr('cmd', 'ARC');

        $('.sp-rect', sketchPad.selector).html('-ARC');
        $('.sp-rect', sketchPad.selector).attr('cmd', 'ARX');
        $('.sp-rectx', sketchPad.selector).show();
    }
    else if (formatter.arcMode == 'disable') {
        $('.sp-circle', sketchPad.selector).attr("onclick", "return false")
        $('.sp-circle', sketchPad.selector).css("background", "silver")
        $('.sp-rectx', sketchPad.selector).hide();
    } else {
        $('.sp-rectx', sketchPad.selector).hide();
        
 		 $('.toolarccontainer').css("position","absolute");
         $('.toolarccontainer').css("visibility","hidden");
    }

    if (window.opener != null) {
        sketchApp.external.datafields = window.opener.datafields;
        sketchApp.external.lookup = window.opener.lookup;
        sketchApp.external.fieldCategories = window.opener.fieldCategories;
        sketchApp.external.lookupMap = ( window.opener.appType == "sv" ) ? ( window.opener.LookupMap ? window.opener.LookupMap: {} ) : window.opener.ccma.Data.LookupMap;
    }
    cpan = new PanControl('.pan-control', sketchApp);
    czoom = new ZoomControl('.zoom-control');
    ctools = new SketchEditorToolbar('.ccse-sketchtoolbar', sketchApp);
    sketchApp.showStatus = function (message) {
        $('.sketch-label').html(message);
    }

    sketchApp.attachPanControl(cpan);
    sketchApp.attachZoomControl(czoom);
    sketchApp.showStatus('Ready');

    var data = [], isInvalidSketch = false;
    sketchEditorOpenFirstTime = true;
    if (window.opener != null) {
        data = window.opener.activeParcel.CCSketchSegments;
        var CC_beforeSketch=false;
       if (window.opener.activeParcel.CC_beforeSketch == true) CC_beforeSketch=true;
    }
    sketchApp.config = config; sketchApp.isInvalidSketchString = false;

    try {
        sketchApp.open(data, formatter, null, config.AfterSave, config.BeforeSave, CC_beforeSketch);
    }
    catch (e) { console.log(e); isInvalidSketch = true; }

    if (isInvalidSketch || (window.opener?.activeParcel?.isInvalidSketchString)) {
        sketchApp.isInvalidSketchString = true; sketchApp.sketches = []; sketchApp.refreshControlList(); $('.ccse-save').hide(); $('.ccse-currentsketch').hide(); $('.ccse-beforesketch').hide(); alert('Sketch cannot be rendered due to the wrong sketch data.');
    }

    sketchApp.isOpenSketchEditorTrue = true;
    sketchApp.ischangeSketchOriginPosition = false;
    sketchApp.render();

    sketchApp.onClose = function () {
        window.close();
    }
    if ( window.opener && window.opener.copyVector ){
        window.opener.copyVector = null;
        $('.ccse-mode-paste-vector').hide();
    }
    else if ( copyVector )
        copyVector = null;
    if (window.opener && window.opener.sketchMode == 'readOnly' ){
        sketchApp.sketchMode = 'readOnly';
        $( '.ccse-mode-buttons' ).remove();
        $( '.ccse-toolbar a' ).remove();
    }
    sketchApp.onSave = function (data, notes, beforeSave, afterSave, onError, cc_error_msg, auditTrailEntry) {
        console.dir(data);

        try {
            if (beforeSave)
                beforeSave();
            if (data.length == 0 && notes.length == 0) {
                if (afterSave)
                    afterSave();
                return;
            }
            window.onbeforeunload = function(evt) {
			    var message = 'Did you remember to download your form?';
			    if (typeof evt == 'undefined') {
			        evt = window.event;
			    }
			    if (evt) {
			        evt.returnValue = message;
			    }
			
			    return message;
			}
            window.opener.saveSketchChanges(data, notes, cc_error_msg, auditTrailEntry, function () {
                if (afterSave)
                    afterSave(function (callback) { 
                        if ( window.opener && window.opener.getParcel ) {
                            window.opener.getParcel( window.opener.activeParcel.Id, function ()
                            {
                                window.onbeforeunload = null;

                                if ( callback ) callback();
                            }, true );
                        }
                        else
                            if ( callback ) callback();
                        
                	});
            });

        }
        catch (e) {

        }

    }

    //$('.sketch-label').html('OK');
});

window.ongesturechange = function (e) { e.preventDefault(); return false; }
$(document).bind('touchmove', function (e) {
    try {
        e.preventDefault();
    }
    catch (ex) {
        alert(ex);
    }

});

var loadSketchSegments = function (showPrevious, load_prev_lookup) { 
	var prev_lookup = load_prev_lookup? sketchApp.sketches.map(function(sk){ if (sk.lookUp) return {sid: sk.sid, lk:_.clone(sk.lookUp)}; else return []; }): null;
	return window.opener && window.opener.getSketchSegments(showPrevious, prev_lookup); 
}

var reopen = function () { sketchApp.resetPan(); sketchApp.zoom(1); sketchApp.open(window.opener.activeParcel.CCSketchSegments, null, null, sketchApp.afterSave); sketchApp.render() }

var saveParcelChange = function ( callback ) {
    if ( window.opener.__DTR )
        window.opener.activeParcel.Reviewed = 0;
    var skipValidation = sketchApp.config && sketchApp.config.ParcelValidationInBeforeSave? true: false;
    window.opener.saveParcelChanges(function (warningMsg, pcicallback) {
    	if (warningMsg) {
    		$('.ccse-canvas').removeClass('dimmer');
			$('.dimmer').hide();
			messageBox(warningMsg, ["OK", "Cancel"], function () {
				$('.ccse-canvas').addClass('dimmer');
            	$('.dimmer').show();
				if (pcicallback) {
					pcicallback(function() {
						if (callback) callback(window.opener.hideMask);
					});
				}
				else if (callback) callback(window.opener.hideMask);
			}, function() {
	        	$('.ccse-canvas').removeClass('dimmer');
				$('.dimmer').hide(); 
				sketchApp.saving = false;
        	});
		}
        else if (callback) callback(window.opener.hideMask);
    }, function(alertMsg){ 
    	$('.ccse-canvas').removeClass('dimmer');
		$('.dimmer').hide(); 
		messageBox(alertMsg);
		sketchApp.saving = false;
   }, skipValidation, true);
}

var validateParcelChange = function (vcallback) {
	if (sketchApp.config && sketchApp.config.ParcelValidationInBeforeSave) {
		if ( ( window.opener.__DTR || !(window.opener.activeParcel.Reviewed) ) && vcallback ) {
			vcallback(); return; 
		}
		
		window.opener.validateParcel((window.opener.activeParcel.Reviewed? 1: 0), function (warningMsg) {
			if (warningMsg) {
				messageBox(warningMsg, ["OK", "Cancel"], function() {
					if (vcallback) vcallback();
				}, function() {
		        	$('.ccse-canvas').removeClass('dimmer');
					$('.dimmer').hide(); 
					sketchApp.saving = false;
	        	});
			}
	        else if (vcallback) vcallback();
	    }, function (alertMsg) { 
	    	$('.ccse-canvas').removeClass('dimmer');
			$('.dimmer').hide(); 
			messageBox(alertMsg);
			sketchApp.saving = false;
	    }, null, true);
	}
	else if (vcallback) vcallback();       
}

var afterSketchProval = function (callback) {
	if ( window.opener && window.opener.getParcel ) {
    	window.opener.getParcel( window.opener.activeParcel.Id, function ()
        {
        	window.onbeforeunload = null;
         	if ( callback ) callback();
            }, true );
    }
    else
    	if ( callback ) callback();                                        	
}

function openKeyPad() {
    sketchApp.showKeypad();
    var sp_Text = $('.sp-text-box');
    $(sp_Text[0]).trigger('click')
    $('.box-length span').text('0');
    $('.box-angle span').text('0');
    $('.box-lengthR span').text('0');
    $('.box-width span').text('0');	
}

///keyboard events Start//

$(document).keydown(function (event) {
    var key = event.which;
    if ($(".mask").is(":visible"))
    	return;
    if ((key != '37' & key != '38' && key != '39' && key != '40') || (!sketchApp.currentNode && !sketchApp.currentVector) || $('.mask').is(':visible') || $('.dimmer').is(':visible')) // condition added to prevent sketch move in sketch save or mask showing
        return;
    var dist = 1;
    switch (key) {
        case 37:
            // Key left.
            if (sketchApp.currentNode) {
                if (_keyElement.lenth) {
                    sketchApp.processCmd('L', parseFloat($('.box-length span', sketchPad.selector).text()), null);
                    sketchPad.reset();
                } else
                    sketchApp.currentNode.p.moveBy(-1 * dist, 0);
            }
            else if (sketchApp.currentVector)
                sketchApp.currentVector.moveBy(new PointX(1 * dist, 0), true);
            _keyElement.lenth = 0;
            break;

        case 38:
            // Key up.
            if (sketchApp.currentNode) {
                if (_keyElement.lenth) {
                    sketchApp.processCmd('U', parseFloat($('.box-length span', sketchPad.selector).text()), null);
                    sketchPad.reset();
                } else
                    sketchApp.currentNode.p.moveBy(0, -1 * dist);
            }
            else if (sketchApp.currentVector)
                sketchApp.currentVector.moveBy(new PointX(0, -1 * dist), true);
            _keyElement.lenth = 0;
            break;

        case 39:
            // Key right.
            if (sketchApp.currentNode) {
                if (_keyElement.lenth) {
                    sketchApp.processCmd('R', parseFloat($('.box-length span', sketchPad.selector).text()), null);
                    sketchPad.reset();
                } else
                    sketchApp.currentNode.p.moveBy(1 * dist, 0);
            }
            else if (sketchApp.currentVector)
                sketchApp.currentVector.moveBy(new PointX(-1 * dist, 0), true);
            _keyElement.lenth = 0;
            break;

        case 40:
            // Key down.
            if (sketchApp.currentNode) {
                if (_keyElement.lenth) {
                    sketchApp.processCmd('D', parseFloat($('.box-length span', sketchPad.selector).text()), null);
                    sketchPad.reset();
                } else
                    sketchApp.currentNode.p.moveBy(0, 1 * dist);
            }
            else if (sketchApp.currentVector)
                sketchApp.currentVector.moveBy(new PointX(0, 1 * dist), true);
            _keyElement.lenth = 0;
            break;

    }
    if (sketchApp.currentNode) {
        sketchApp.currentNode.recalculateAll();
        sketchApp.currentVector.isModified = true;
    }
    sketchApp.render();
});


Mousetrap.bind('del', function (e) { //delete vector, node 
    e.preventDefault();
    if (sketchApp.CC_beforeSketch || $(".mask").is(":visible"))
    return;
    try {
        if (sketchApp.currentNode)
            sketchApp.deleteCurrentNode();
        else if (sketchApp.currentVector)
            sketchApp.deleteCurrentVector()
        else
            return false
    } catch (e) {
        alert(e);
    }
    return false;
});


Mousetrap.bind('b', function (e) { //create box
    e.preventDefault();
    if (sketchApp.CC_beforeSketch || $(".mask").is(":visible"))
    return;
    if (sketchApp.currentVector && (sketchApp.mode == CAMACloud.Sketching.MODE_NEW)) {
        if (sketchApp.currentNode) {
            messageBox('You are not allowed to draw polygon after creating first node')
            return;
        }
        else {
            if (sketchPad.locked)
                return;
            _quickShape = {}
            _quickShape.shape = "POLY";
            _quickShape.edges = 4;
            _drawshape = true;
        }
    }
    return false;
});

Mousetrap.bind('f h', function (e) { // horizontal flip
    e.preventDefault();
    if (sketchApp.CC_beforeSketch || $(".mask").is(":visible"))
    return;
    try {
        if (sketchApp && sketchApp.mode == CAMACloud.Sketching.MODE_DEFAULT) {
            sketchApp.resetSketchMovable();
            if (sketchApp.currentSketch)
                sketchApp.currentSketch.flipVertical(sketchApp);
        }
    } catch (e) {
        alert(e);
    }
    return false;
});


Mousetrap.bind('f v', function (e) { // vertical flip
    e.preventDefault();
    if (sketchApp.CC_beforeSketch || $(".mask").is(":visible"))
    return;
    try {
        if (sketchApp && sketchApp.mode == CAMACloud.Sketching.MODE_DEFAULT) {
            sketchApp.resetSketchMovable();
            if (sketchApp.currentSketch)
                sketchApp.currentSketch.flipHorizontal(sketchApp);
        }
    } catch (e) {
        alert(e);
    }
    return false;
});

Mousetrap.bind('k', function (e) { // showing keys
    e.preventDefault();
    if (sketchApp.CC_beforeSketch || $(".mask").is(":visible"))
    return;
    if (sketchApp.currentVector) {
        if (clientSettings && clientSettings["EnableRotation"] == "1" && sketchApp && sketchApp.rotationButtonEnabled) {
            $('.sketch-pad .sp-left span').css("background-image", "url(/images/sp-left.png)");
            $('.sketch-pad .sp-right span').css("background-image", "url(/images/sp-right.png)");
            sketchApp.rotationButtonEnabled = false;
        }
        sketchApp.showKeypad();
        var sp_Text = $('.sp-text-box');
        $(sp_Text[0]).trigger('click')
        $('.box-length span').text('0');
        $('.box-angle span').text('0');
        $('.box-lengthR span').text('0');
        $('.box-width span').text('0');
    }
    return false;

});



Mousetrap.bind('m', function (e) {  //enable move function
    e.preventDefault();
    if (sketchApp.CC_beforeSketch || $(".mask").is(":visible"))
    return;
    try {
        if (sketchApp) {
            if (!sketchApp.sketchMovable) {
                sketchApp.toggleSketchMovable();
                $('.ccse-sketch-move').html('Release');
            }
        }
    } catch (e) {
        alert(e);
    }
    return false;
});


Mousetrap.bind('enter', function (e) {
    e.preventDefault();
    if (sketchApp.CC_beforeSketch || $(".mask").is(":visible"))
    return;
    try {
        if (sketchApp) {
            if ((sketchApp.mode == CAMACloud.Sketching.MODE_NEW) && sketchApp.currentNode)
                sketchApp.processCmd('OK', null, null); //autocomplete vector
            if (sketchApp.sketchMovable) {
                sketchApp.toggleSketchMovable();    //release move function
                $('.ccse-sketch-move').html('Move');
            }
        }
    } catch (e) {
        alert(e);
    }
    return false;
});


Mousetrap.bind('space', function (e) {
    e.preventDefault();
    if (sketchApp.CC_beforeSketch || $(".mask").is(":visible"))
    return;
    try {
        if (sketchApp) {
            if (sketchApp.sketchMovable) {
                sketchApp.toggleSketchMovable();  //release move function
                $('.ccse-sketch-move').html('Move');
            }
        }
    } catch (e) {
        alert(e);
    }
    return false;
});

Mousetrap.bind('r', function (e) { //rotate btn focus
    e.preventDefault();
    if (sketchApp.CC_beforeSketch || $(".mask").is(":visible"))
    return;
    if (sketchApp.currentVector && sketchApp.currentVector.isClosed)
        $(".ccse-rotate-sketch").focus();
    return false;
});


Mousetrap.bind('ctrl+s', function (e) { //save sketch
    e.preventDefault();
    if (sketchApp.CC_beforeSketch || $(".mask").is(":visible"))
    return;
    try {
        if (sketchApp && !sketchApp.saving) {
            sketchApp.save(sketchApp.afterSave, sketchApp.beforeSave);
        }
    } catch (e) {
        alert(e);
    }
    return false;
});


Mousetrap.bind('ctrl+c', function (e) { //copy vector
    e.preventDefault();
    if (sketchApp.CC_beforeSketch || $(".mask").is(":visible"))
    return;
    try {
        if (sketchApp) {
            sketchApp.copyCurrentVector();
        }
    } catch (e) {
        alert(e);
    }
    return false;
});


Mousetrap.bind('ctrl+x', function (e) { //cut vector
    e.preventDefault();
    if (sketchApp.CC_beforeSketch || $(".mask").is(":visible"))
    return;
    try {
        if (sketchApp) {
            sketchApp.cutCurrentVector();
        }
    } catch (e) {
        alert(e);
    }
    return false;
});


   Mousetrap.bind('ctrl+v', function (e) { //paste 
            e.preventDefault();
    if (sketchApp.CC_beforeSketch || $(".mask").is(":visible"))
    return;
            try {
                e.preventDefault();
                if (sketchApp && window.opener.copyVector) {
                    if (sketchApp.currentVector) {
                        $(sketchApp.vectorSelector).val(''); //   While mid-Sketching
                        sketchApp.currentVector = null;
                        sketchApp.raiseModeChange();
                        sketchApp.renderAll();
                        sketchApp.mode = CAMACloud.Sketching.MODE_DEFAULT;
                        sketchApp.currentNode = null;
                    }
                    sketchApp.pasteVector();
                }
            } catch (e) {
                alert(e);
            }
            return false;
        });


Mousetrap.bind('shift+r', function (e) { //create rectangle

    e.preventDefault();
    if (sketchApp.CC_beforeSketch || $(".mask").is(":visible"))
    return;
    if (sketchApp.currentVector && (sketchApp.mode == CAMACloud.Sketching.MODE_NEW)) {
        if (sketchApp.currentNode) {
            quicktoolbarUpdate();
            messageBox('You are not allowed to draw rectangle after creating first node')
            return;
        }
        else {
            _quickShape = {}
            _quickShape.shape = "RECT";
            _drawshape = true;
        }
    }
    return false;
});



Mousetrap.bind('p', function (e) { //polygon drawing enable
    e.preventDefault();
    if (sketchApp.CC_beforeSketch || $(".mask").is(":visible"))
    return;
    try {
        if (sketchApp.mode == CAMACloud.Sketching.MODE_NEW) {
            if (sketchApp.currentNode) {
                messageBox('You are not allowed to draw polygon after creating first node')
                return;
            }
            else {
                _quickShape = {}
                _quickShape.shape = "POLY";
                _quickShape.edges = $('#polysides').find(":selected").text();
                _drawshape = true;
                $('.hexbtn').removeClass('toolbtn-inactive').addClass('toolbtn-active');
                $('#polysides').prop('disabled', false);
                $(".toolbox-control").css("visibility", "visible");
                $("#polysides").attr("size", 8);
                $("#polysides").css("position", "fixed");
            }
        }

    } catch (e) {
        alert(e);
    }
    return false;
});


Mousetrap.bind('+', function (e) {
    e.preventDefault();
    if (sketchApp.CC_beforeSketch || $(".mask").is(":visible"))
    return;
    if (sketchApp.currentNode && (_keyElement.fun == 'ARC')) {
        sketchApp.processCmd('ARC', _keyElement.lenth, null);  //for concave arc
        _keyElement = {};
    }
    else if (sketchApp.currentNode) {
        sketchApp.processCmd("+", null, null); //new node point
        _keyElement.lenth=0;
    }
});



Mousetrap.bind('-', function (e) { //for concave arc
    e.preventDefault();
    if (sketchApp.CC_beforeSketch || $(".mask").is(":visible"))
    return;
    if (sketchApp.currentNode && (_keyElement.fun == 'ARC')) {
        sketchApp.processCmd('ARX', _keyElement.lenth, null);
        _keyElement = {};
    }
});

Mousetrap.bind('a', function (e) {// saving arc event
    e.preventDefault();
    if (sketchApp.CC_beforeSketch || $(".mask").is(":visible"))
    return;
    if (sketchApp) {
        _keyElement.lenth = 0;
        _keyElement.fun = 'ARC'
    }
});


$(document).keypress(function (e) { //saving number entered
    if (sketchApp.CC_beforeSketch || $(".mask").is(":visible"))
    return;
    var key = e.keyCode || e.charCode;
    if (sketchApp.currentNode) {
        if (key >= 48 && key <= 57) {
            if (!($('.sketch-pad').is(":visible"))) {
                openKeyPad();
            }
            var tbx = $('.sp-text-box[selected]', sketchPad.selector).hasClass('box-length')? true: false;
            sketchPad.sketchPadNumber(key - 48, tbx);
        }
    }
});



Mousetrap.bind('ctrl+z', function (e) { //undo
    e.preventDefault();
    if (sketchApp.CC_beforeSketch || $(".mask").is(":visible"))
    return;
    try {
        
        if (sketchApp && sketchApp.undo && sketchApp.mode == CAMACloud.Sketching.MODE_NEW) sketchApp.undo();
    } catch (e) {
        alert(e);
    }
    return false;
});

Mousetrap.bind('ctrl+y', function (e) { //redo
    e.preventDefault();
    if (sketchApp.CC_beforeSketch || $(".mask").is(":visible"))
    return;
    try {

        if (sketchApp && sketchApp.redo && sketchApp.mode == CAMACloud.Sketching.MODE_NEW) sketchApp.redo();
    } catch (e) {
        alert(e);
    }
    return false;
});

Mousetrap.bind('esc', function (e) {
    e.preventDefault();
    if (sketchApp.CC_beforeSketch || $(".mask").is(":visible"))
    return;
    try {
        if (sketchApp) {

            if (clientSettings && clientSettings["EnableRotation"] == "1" && sketchApp.rotationButtonEnabled) {
                $('.sketch-pad .sp-left span').css("background-image", "url(/images/sp-left.png)");
                $('.sketch-pad .sp-right span').css("background-image", "url(/images/p-right.png)");
                sketchApp.rotationButtonEnabled = false;
            }
            if (sketchApp) sketchApp.hideKeypad();
            else $('.sketch-pad').hide();
        }
    } catch (e) {
        alert(e);
    }
    return false;
});



///keyboard events ends//

function okClick() {
    if ($('ul.customul li[select]').length) {
        if($('ul.customul li[select="selected"]')[0]){
	        var pval =$('ul.customul li[select="selected"]')[0]['attributes'][0].value;
	    	//$('span[lookup="' + SketchLookup + '"]').siblings('select').val(pval);
	    	$(currentLookupValue).val(pval);
	        $( '.control_div select' ).trigger("change");
	        currentLookupValue = null;
	        SketchLookup = null;
	        clearAll();
	        $('.mask').show();
	        if(window.opener && window.opener.appType != 'sv')
    			window.opener.setScreenDimensions();
	    }
	    else{
	        cancelClick();
	    }
	}
	else {
	    cancelClick();
	}
}
function cancelClick() {
    clearAll();
    $('.mask').show();
    if(window.opener && window.opener.appType != 'sv')
    	window.opener.setScreenDimensions();
}
function clearAll() {
    $('#searchtxt').val('');
    $('.divCodeFile').hide();
    $('.mask').hide();
    $('.match').html('');
}

function clearVectorlabelPosition() {
	if (window.opener)
		window.opener.vectorLabelPositions = []; 
	vectorLabelPositions = [];
}
