﻿function MFCDSketchDefinition(data, isChanged, isOriginal) {

	var vector = '', vectorStart = true, vectorSource, vectorCounter = 0,sketches = [] ,startPos = '';
	var vector_orginal = '',dOrginal = [],Orgin_vectorStart = true, org_startPos;
	if(data.length > 0){
		var Skt_table = data[0].CASKLCONS == undefined ? 'CAM_OUTSK' : 'CAM_SKETCH';
		var skt_fields = {};
		if(Skt_table == 'CAM_SKETCH'){
			skt_fields = {RECNUM:'CASKLORDNUM', DIAG1:'CASKLDIAG1', DIAG2:'CASKLDIAG2',CASKLDU:'CASKLDU', CASKLRL:'CASKLRL', SQFT:'CASKLSQFT', CONS:'CASKLCONS' ,SHB:'CASKLSHB', TYPE: 'CASKLTYPE'}
			vectorSource = ccma.Sketching.Config.sources[0].VectorSource[0];
		}
		else{
			skt_fields = {RECNUM:'CAOSLORDNUM', DIAG1:'CAOSLDIAG1', DIAG2:'CAOSLDIAG2',CASKLDU:'CAOSLDU', CASKLRL:'CAOSLRL', SQFT:'CAOSLSQFT', CONS:'CAOSLCONS' ,SHB:'CAOSLSHB', TYPE: 'CAOSLTYPE'}
			vectorSource = ccma.Sketching.Config.sources[1].VectorSource[0];
		}
		var convertToSketch = function(d, type, isAngled, isInverted) {
			var updownData = isInverted? parseInt(d[skt_fields.CASKLDU + type]) * -1: parseInt(d[skt_fields.CASKLDU + type]);
			var leftrightData = isInverted? parseInt(d[skt_fields.CASKLRL + type]) * -1: parseInt(d[skt_fields.CASKLRL + type]);
		
			return ' ' + (leftrightData > 0? 'R' + leftrightData: 'L' + leftrightData * -1) + (isAngled? '/': ' ') + (updownData > 0? 'D' + updownData: 'U' + updownData * -1);
		}
		
		data = (!isOriginal) ? data.filter(function(x){ return x.CC_Deleted != true }) : data;
		data.map(function(d){ if (d[skt_fields.RECNUM]) d[skt_fields.RECNUM] = parseInt(d[skt_fields.RECNUM]); })
		var trowuid ='';
		data = _.sortBy(data, skt_fields.RECNUM)
		data.forEach(function(d){
			if(isOriginal && d[skt_fields.CONS] == null)
				return;
			++vectorCounter;
			dOrginal = d.Original;
			if (vectorStart) {
				if (d[skt_fields.DIAG1] != '+' && d[skt_fields.DIAG1] != '-') 
					vector += convertToSketch(d, 1);
				else 
					vector += convertToSketch(d, 1, true);
				startPos = convertToSketch(d, 1);
				vector += ':' + startPos + ' S '
				vectorStart = false;
				if(d[skt_fields.DIAG2] != '+' && d[skt_fields.DIAG2] != '-')
					vector += convertToSketch(d, 2);
				else
					vector += convertToSketch(d, 2, true);		
			}
			else{
				if (d[skt_fields.DIAG1] != '+' && d[skt_fields.DIAG1] != '-') 
					vector += convertToSketch(d, 1);
				else 
					vector += convertToSketch(d, 1, true);
				if(d[skt_fields.DIAG2] != '+' && d[skt_fields.DIAG2] != '-' )
					vector += convertToSketch(d, 2);
				else
					vector += convertToSketch(d, 2, true);		
			}
			if(!isOriginal){
				if (Orgin_vectorStart) {
					if (dOrginal[skt_fields.DIAG1] != '+' && dOrginal[skt_fields.DIAG1] != '-') 
						vector_orginal += convertToSketch(dOrginal, 1);
					else 
						vector_orginal += convertToSketch(dOrginal, 1, true);
					org_startPos = convertToSketch(dOrginal, 1);
					vector_orginal += ':' + org_startPos + ' S '
					Orgin_vectorStart = false;
					if(dOrginal[skt_fields.DIAG2] != '+' && dOrginal[skt_fields.DIAG2] != '-')
						vector_orginal += convertToSketch(dOrginal, 2);
						else
					vector_orginal += convertToSketch(dOrginal, 2, true);		
				}
				else{
					if (dOrginal[skt_fields.DIAG1] != '+' && dOrginal[skt_fields.DIAG1] != '-') 
						vector_orginal += convertToSketch(dOrginal, 1);
					else 
						vector_orginal += convertToSketch(dOrginal, 1, true);
					if(dOrginal[skt_fields.DIAG2] != '+' && dOrginal[skt_fields.DIAG2] != '-' )
						vector_orginal += convertToSketch(dOrginal, 2);
					else
						vector_orginal += convertToSketch(dOrginal, 2, true);		
				}	
			}
			if(trowuid == '')
				trowuid = d.ROWUID;
			else
				trowuid = trowuid + ',' + d.ROWUID;
			if ((d[skt_fields.CONS] != null && d[skt_fields.CONS] != ' ' && d[skt_fields.CONS].trim() != '') || (d[skt_fields.SHB] != null && d[skt_fields.SHB] != ' ' && d[skt_fields.SHB].trim() != '')) {
				if (vectorCounter == 1 ) {
					vector += convertToSketch(d, 2, false, true);
					if(!isOriginal)
						vector_orginal += convertToSketch(dOrginal, 2, false, true);
				}
				if(!isOriginal && (vector != vector_orginal)) isChanged = true;
				vector = vector.replace(new RegExp(/L0|U0/, 'g'), '');
				var SHB = d[skt_fields.SHB] != null? d[skt_fields.SHB]: '', CONS = d[skt_fields.CONS] != null? d[skt_fields.CONS]: '', TP = (d[skt_fields.TYPE] != null? d[skt_fields.TYPE]: ''); 
				var lblField = { details: { SHB: SHB, CONS: CONS, TYPE: TP } }, lbl = '';
				lbl = lbl + SHB; lbl = lbl + TP; lbl = lbl + (CONS != ''? '/' + CONS: ''); 
	     		sketches.push({
		        	uid: (trowuid),
		        	label: [{ Field: skt_fields.CONS, Value: lbl, Description: null, IsEdited: isChanged, hiddenFromEdit: vectorSource.false, hiddenFromShow: false, lookup: null, Caption: vectorSource.labelCaption, ShowCurrentValueOnly: vectorSource.ShowCurrentValueOnly, lookUpQuery: null, splitByDelimiter: null, Target: vectorSource.LabelTarget, UseLookUpNameAsValue: null, IsLargeLookup: false, IsRequired: true, labelDetails: lblField }],
		        	vector: d[skt_fields.CONS] + '[-1,-1,' + startPos + ']' + vector,
		        	labelPosition: null,
		        	isChanged: isChanged,
		        	vectorConfig: vectorSource
		    	});
				vectorStart = true;
				vector = '';
				vector_orginal = '';
				startPos = '';
				org_startPos = '';
				vectorCounter = 0;
				trowuid = '';
				isChanged = false;
				Orgin_vectorStart = true;
	    	}
		});
		console.log(sketches);
		return sketches;
	}
	else
		return sketches;
}

function MFCDLabelConfig(head, items, lookups, callback, editor, type) {
	var html = '', shb = '', tp = '', cons = '', skt_table = editor.currentSketch.config.VectorSource[0].Table, skt_fields = [];
	var getDataField = window.opener? window.opener.getDataField: getDataField;
	$('.mask').show(); 
	$('.Current_vector_details .head').html(head);
    $('.dynamic_prop').html('');

	if (type == 'edit' && items[0].labelDetails.details) {
		var lblDet = items[0].labelDetails.details;
		shb = lblDet.SHB; cons = lblDet.CONS; tp = lblDet.TYPE;
	}
	
	if(skt_table == 'CAM_SKETCH')
		skt_fields = [{ Name: 'CASKLSHB', Default: 4, Title: 'Shb +', class: 'SHB', value: shb }, { Name: 'CASKLCONS', Default: 5, Title: 'Const.', class: 'CONST', value: cons }, { Name: 'CASKLTYPE', Default: 1, Title: 'Type', class: 'Typ', value: tp }];
	else
		skt_fields = [{ Name: 'CAOSLSHB', Default: 4, Title: 'Shb +', class: 'SHB', value: shb }, { Name: 'CAOSLCONS', Default: 5, Title: 'Const.', class: 'CONST', value: cons }, { Name:'CAOSLTYPE', Default: 1, Title: 'Type', class: 'Typ', value: tp }];
	
  	var container = $("<div />", {
	  	class: "lblclass",
	    style: 'display:flex;'
  	});
	skt_fields.forEach(function(item, i) {
	  	var title = item.Title;
	  	var field = getDataField(item.Name, skt_table);
	  	var maxlength = (field && field.MaxLength)? field.MaxLength: item.Default;
	  	var val = typeof(item.value) == "string" && item.value.length > maxlength? item.value.substring(0, maxlength): item.value;
	    var itemContainer = $("<div />", {
	      	class: "",
	      	style: "margin:10px;text-align:center;",
	    });
	    var itemSpan = $("<label />", {
	      	class: "",
	      	text: title,
	      	style: "width:100px;display:block;font-weight:700;",
	    });
	    var itemInput = $("<input />", {
	    	type: "Text",
	        id: "input_" + i,
	        class: item.class,
	        style: "width:100px;margin-top:8px;",
	        maxlength: maxlength,
	        val: val
	    });
	    $(itemContainer).append(itemSpan).append(itemInput);
	    $(container).append(itemContainer);
	});
  	$('.dynamic_prop').append(container);
  	$('.dynamic_prop').append('<span class="validateLabel" style="height: 12px; display: none; width:450px; color:Red; margin-left: 100px;">Please enter any value in shb + or const.</span>');
  	$('.Current_vector_details').css('width', '500px');
  	$('.Current_vector_details').show();
  	
  	$('.SHB, .CONST, .Typ').keyup(function () {
  		$('.validateLabel').hide();
  	});
  	
  	$('.Current_vector_details #Btn_Save_vector_properties').unbind(touchClickEvent);
    $('.Current_vector_details #Btn_Save_vector_properties').bind(touchClickEvent, function () {
    	var shbVal = $('.SHB').val(), consVal = $('.CONST').val(), typeVal = $('.Typ').val();
    	if (shbVal == '' && consVal == '') {
    		$('.validateLabel').show();
    		return false;
    	}
    	items[0].Value = shbVal + typeVal + (consVal? '/' + consVal :  '');
    	items[0].labelDetails = {};
    	items[0].labelDetails.details = { SHB: shbVal, CONS: consVal, TYPE: typeVal };
    	if (callback) callback(items, {});
    	$('.Current_vector_details').hide();
        if (!sketchApp.isScreenLocked) $('.mask').hide();
        return false;
    });
    
    $('.Current_vector_details #Btn_cancel_vector_properties').unbind(touchClickEvent)
    $('.Current_vector_details #Btn_cancel_vector_properties').bind(touchClickEvent, function() {
		$('.Current_vector_details').hide();
        if (!sketchApp.isScreenLocked) $('.mask').hide();
        return false;
	});
}

function MFCDonSketchSave(sketchDataArray, noteData, sketchSaveCallBack, onError) {
	if (sketchDataArray == null || sketchDataArray == undefined || sketchDataArray.length == 0) {
        if (sketchSaveCallBack) sketchSaveCallBack();
        return;
    }
    var parcel = window.opener? window.opener.activeParcel: activeParcel;
    var getDataField = window.opener? window.opener.getDataField: getDataField;
    var getCategoryFromSourceTable = window.opener? window.opener.getCategoryFromSourceTable: getCategoryFromSourceTable;
    var saveData = '';
    var s_count = 0, out_count = 0, delete_vect=[];
    sketchDataArray = sketchDataArray.reverse();
	function fun1() {
		var isdtr = window.opener ? window.opener.__DTR : (typeof (__DTR) !== 'undefined' ? __DTR : false);
		let isSvApp = window.opener && window.opener.appType == "sv" ? true : (typeof (appType) !== 'undefined' && appType == "sv" ? true : false);
		var _appType = isdtr ? 'DTR' : (isSvApp ? 'SV' : 'QC');
    	var saveRequestData = {
        	ParcelId: parcel.Id,
        	Priority: '',
        	AlertMessage: '',
        	Data: '',
        	recoveryData: '',
        	AppraisalType: '',
			activeParcelPhotoEditsdata: '',
			appType: _appType
    	};
    
    	if (saveData != '') {
    		console.log(saveData);
        	saveRequestData.Data = saveData;
        	$.ajax({
            	url: '/quality/saveparcelchanges.jrq',
            	type: "POST",
            	dataType: 'json',
            	data: saveRequestData,
            	success: function (resp) {
                	if (resp.status == "OK")
                    	if (sketchSaveCallBack) sketchSaveCallBack(afterSketchProval);
            	}
        	});
    	}
    	else {
        	if (sketchSaveCallBack) sketchSaveCallBack(afterSketchProval);
        	return;
    	} 
    }    
    function sketchDataArraySave(sketchData,sketchSaveCallBack){
		var sktData = sketchData.pop();
		var p_record = sktData.parentRow;
		var source_Name = sktData.sourceName;
		var skout_Fields = [];
		var catId = getCategoryFromSourceTable(source_Name).Id;
		if(source_Name == 'CAM_SKETCH'){
			var skt_fields = ['CASKLCONS','CASKLORDNUM', 'CASKLRL1', 'CASKLDU1', 'CASKLRL2', 'CASKLDU2', 'CASKLDIAG1', 'CASKLDIAG2', 'CASKLSHB', 'CASKLTYPE', 'CASKLSQFT'];
			skt_fields.forEach(function(sk_flds){ skout_Fields.push(getDataField(sk_flds, source_Name)); });
		}
		else{
			var out_fields = ['CAOSLCONS', 'CAOSLORDNUM', 'CAOSLRL1', 'CAOSLDU1', 'CAOSLRL2', 'CAOSLDU2', 'CAOSLDIAG1', 'CAOSLDIAG2', 'CAOSLSHB', 'CAOSLTYPE', 'CAOSLSQFT'];
			out_fields.forEach(function(out_flds){ skout_Fields.push(getDataField(out_flds, source_Name)); });
		}
		if(sktData.vectorString == '' || sktData.vectorString == undefined){
			if(sketchData.length > 0) 
				sketchDataArraySave(sketchData,sketchSaveCallBack)
			else 
				fun1();			
		}
		var skVector_String = sktData.vectorString.split(":")[1];
		var start_point = skVector_String.split(" S ")[0].split(" ");
		if(start_point[0]=='')
			start_point = start_point.splice(0,1)
		var vect_string = skVector_String.split(" S ")[1].split(" ");
		if(vect_string[0]=='')
			vect_string = vect_string.splice(0,1)
		var startNode = sktData.startNode;
		var skout_Label = sktData.label.split("-");
		var sketch_label = '', sketch_SHB = '', sketch_Type = '', sk_Area = sktData.area;
		if(sktData.labelDetails && sktData.labelDetails[0]) {
			sketch_label = sktData.labelDetails[0].CONS;
			sketch_SHB = sktData.labelDetails[0].SHB;
			sketch_Type = sktData.labelDetails[0].TYPE;
		}
		else if(skout_Label.length > 1) {
			sketch_label = skout_Label[0];
			sketch_Type = skout_Label[1];
		}
		else {
			sketch_label = skout_Label[0];
		}	
		var LRL1 = '',LRL2 = '',LDU1 = '', LDU2= '',v = 0 ,diag1 ='',diag2 = '',rec_creat = 0;
		var rec_sketch = [];
		
		function create_records_sketch(){
			var rowId = ccTicks()
			rec_sketch.forEach(function(rec){
				rowId= rowId +10 ;
				saveData += '^^^' + parcel.Id + '|' + rowId + '|' + p_record.ROWUID + '||' + source_Name + '|new|\n';
				if(source_Name == 'CAM_SKETCH' ){
					for(var i=0; i<skout_Fields.length; i++)
						saveData += '^^^' + parcel.Id + '|' + rowId + '|' + skout_Fields[i].Id + '||' + rec[skout_Fields[i].Name] + '|edit|' + p_record.ROWUID + '\n';							
				}
				else{
					for(var i=0; i<skout_Fields.length; i++)
						saveData += '^^^' + parcel.Id + '|' + rowId + '|' + skout_Fields[i].Id + '||' + rec[skout_Fields[i].Name] + '|edit|' + p_record.ROWUID + '\n';							
				}
			});
			if(sketchData && sketchData.length > 0) 
				sketchDataArraySave(sketchData,sketchSaveCallBack)
			else
				fun1();			
		}			
		function vect_record_creation(){
			var points = [];
			while(startNode != null){
				var s = '';
				var x = startNode.sdx == 'R' ? startNode.dx : (-1 * startNode.dx);
				var y = startNode.sdy == 'D' ? startNode.dy : (-1 * startNode.dy);
				if(startNode.isVeer){
					if((startNode.sdx == 'R' && startNode.sdy == 'D') || (startNode.sdx == 'L' && startNode.sdy == 'U'))
						s = '+';
					else((startNode.sdx == 'R' && startNode.sdy == 'U') || (startNode.sdx == 'L' && startNode.sdy == 'D'))
						s = '-';
				}
				points.push({x: x, y: y, s:s});
				startNode = startNode.nextNode;
			}
			
			for(var i = points.length-1; i>=0; i--){
				 var p = points[i]; var pp = points[i-1]; 
				 if(p.x == 0 && pp && !pp.s && (pp.y == 0 || pp.y == -0)) { 
				 	pp.y = p.y; points.splice(i, 1);
				 } 
			}
			var traversal = [];
			
			for(var i = 0; i < points.length; i += 2){ 
				var p1 = points[i], p2 = points[i+1]; 
				if(!p2) p2 = {x: 0, y: 0, s: ''}
				traversal.push({du1: p1.y, du2: p2.y, rl1: p1.x, rl2: p2.x, diag1: p1.s, diag2: p2.s})
			}
			
			for(var i = 0; i < traversal.length; i++){
				if(i != (traversal.length-1)) {
					if(source_Name == 'CAM_SKETCH') {
						s_count = s_count + 1;
						rec_sketch.push({CASKLCONS: ' ',CASKLORDNUM: s_count, CASKLRL1: traversal[i].rl1, CASKLDU1: traversal[i].du1, CASKLRL2: traversal[i].rl2, CASKLDU2: traversal[i].du2, CASKLDIAG1: traversal[i].diag1, CASKLDIAG2: traversal[i].diag2, CASKLSHB: ' ', CASKLTYPE: ' ', CASKLSQFT: '0',Source_Name: source_Name })
					}
					else {
						out_count = out_count + 1;
						rec_sketch.push({CAOSLCONS: ' ', CAOSLORDNUM: out_count, CAOSLRL1: traversal[i].rl1, CAOSLDU1: traversal[i].du1, CAOSLRL2: traversal[i].rl2, CAOSLDU2: traversal[i].du2, CAOSLDIAG1: traversal[i].diag1, CAOSLDIAG2: traversal[i].diag2, CAOSLSHB: ' ', CAOSLTYPE: ' ', CAOSLSQFT: '0', Source_Name: source_Name })
					}
				}
				else {
					if(source_Name == 'CAM_SKETCH') {
						s_count = s_count + 1;
						rec_sketch.push({CASKLCONS: sketch_label,CASKLORDNUM: s_count, CASKLRL1: traversal[i].rl1, CASKLDU1: traversal[i].du1, CASKLRL2: traversal[i].rl2, CASKLDU2: traversal[i].du2, CASKLDIAG1: traversal[i].diag1, CASKLDIAG2: traversal[i].diag2, CASKLSHB: sketch_SHB, CASKLTYPE: sketch_Type, CASKLSQFT: sk_Area,Source_Name: source_Name })
					}
					else {
						out_count = out_count + 1;
						rec_sketch.push({CAOSLCONS: sketch_label, CAOSLORDNUM: out_count, CAOSLRL1: traversal[i].rl1, CAOSLDU1: traversal[i].du1, CAOSLRL2: traversal[i].rl2, CAOSLDU2: traversal[i].du2, CAOSLDIAG1: traversal[i].diag1, CAOSLDIAG2: traversal[i].diag2, CAOSLSHB: sketch_SHB, CAOSLTYPE: sketch_Type, CAOSLSQFT: sk_Area, Source_Name: source_Name })
					}
				}
			}
			
			if(rec_sketch.length > 0)
				rec_sketch = rec_sketch.reverse();
			create_records_sketch();
		} 
		//delete modified segments records
		function deleteRecord(delete_rowid){
			var d_rowuid = delete_rowid.pop();
			if(d_rowuid && d_rowuid.length > 0) { 				
 				saveData += '^^^' + parcel.Id + '|' + d_rowuid + '|||' + source_Name + '|delete|\n';
 				deleteRecord(delete_rowid);
 			}
 			else
 				vect_record_creation(); 				
 			
 			
		}
		//update skechrecord num if no change in segments
		function updateNumber(update_rowid) {
			var u_rowuid = update_rowid.pop();
			var update_field,count = 0 ,sk_re = [];
			if(source_Name == 'CAM_SKETCH' ){
				sk_re = parcel[source_Name].filter(function(re){return re.ROWUID == u_rowuid })[0];
				update_field = getDataField('CASKLORDNUM',source_Name);
				count = ++s_count;
            }
			else{
				sk_re = parcel[source_Name].filter(function(re){return re.ROWUID == u_rowuid })[0];
				count = ++out_count;
				update_field = getDataField('CAOSLORDNUM',source_Name);
			}
			saveData += '^^^' + parcel.Id + '|' + sk_re.ROWUID + '|' + update_field.Id + '||' + count + '||' + sk_re.ParentROWUID + '\n';
			if(update_rowid.length > 0)
				updateNumber(update_rowid);
			else if(sketchData.length > 0) 
				sketchDataArraySave(sketchData,sketchSaveCallBack)
			else {
				fun1();
			}	
		}
		
		function update_data(field, sourceRecord, callback) {
			if(field.SourceTable == 'CAM_SKETCH' ){
				for(var i=0; i<skout_Fields.length; i++)
					saveData += '^^^' + parcel.Id + '|' + sourceRecord.ROWUID + '|' + skout_Fields[i].Id + '||' + sourceRecord[skout_Fields[i].Name] + '||' + p_record.ROWUID + '\n';							
				if (callback) callback(true);
			}
			else if(field.SourceTable == 'CAM_OUTB'){
					for(var i=0; i<skout_Fields.length; i++)
						saveData += '^^^' + parcel.Id + '|' + sourceRecord.ROWUID + '|' + skout_Fields[i].Id + '||' + sourceRecord[skout_Fields[i].Name] + '||' + p_record.ROWUID + '\n';							
					if (callback) callback(true);
			}			
			else if (callback) callback(false); 
		}
		//condition checked new ,modified,nochange
		if(sktData.newRecord)
			vect_record_creation();
		else if(!sktData.newRecord && sktData.isModified){
			var sketch_uid = sktData.sid;
			var delete_rowid= [];
			sketch_uid.split(',').forEach(function(roid){
				delete_rowid.push(roid);
			})
			if(delete_rowid.length > 0)
				deleteRecord(delete_rowid);
			else if(sketchData.length > 0) 
				sketchDataArraySave(sketchData,sketchSaveCallBack)
			else {
				fun1();
			}
			
		}
		else{
			var sketch_uid = sktData.sid;
			var update_rowid= [];
			sketch_uid.split(',').forEach(function(roid){
				update_rowid.push(roid);
			})
			update_rowid = update_rowid.reverse();
			if(update_rowid.length > 0)
				updateNumber(update_rowid);
			else if(sketchData.length > 0) 
				sketchDataArraySave(sketchData,sketchSaveCallBack)
			else {
				fun1();
			}
			
		}

	}
	//deletevector function
	function deleteVectfunction(delete_vect_row){
		var delete_vectr = delete_vect_row.pop();
		var sourceData = parcel[delete_vectr.Source].filter( function ( x ) { return x.ROWUID == delete_vectr.ROWUID } )[0];
        sourceData = !sourceData.CC_Deleted? sourceData: [];
 		saveData += '^^^' + parcel.Id + '|' + sourceData.ROWUID + '|||' + delete_vectr.Source + '|delete|\n';
		if(delete_vect_row.length > 0)
		 	deleteVectfunction(delete_vect_row);		
		else
			sketchDataArraySave(sketchDataArray,sketchSaveCallBack)	
	}
	delete_vect = sketchApp.deletedVectors;
	if(delete_vect.length > 0){
		var delete_vect_row = [];
		delete_vect.forEach(function(d_vect){
			if(!d_vect.clientId){
				var sketh_source = d_vect.vectorConfig.Table;
				d_vect.uid.split(',').forEach(function(u){
					delete_vect_row.push({ROWUID: u, Source: sketh_source})
				})
			}
			else
				sketchDataArraySave(sketchDataArray,sketchSaveCallBack);
		});
		if(delete_vect_row.length > 0)
			deleteVectfunction(delete_vect_row);
		else
			sketchDataArraySave(sketchDataArray,sketchSaveCallBack)
	}
	else
		sketchDataArraySave(sketchDataArray,sketchSaveCallBack)
	
}