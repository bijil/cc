﻿function testAreaFields() {
    getData('SELECT main_fn_area, uppr_fn_area, addl_fn_area, unfin_area, bsmt_area, fn_bsmt_area, att_storg_sf, carport_sqft, att_gar_sqft, bltin_garage, breezeway_sf, cov_porch_sf, enc_porch_sf, opn_porch_sf, scr_porch_sf, wood_deck_sf, gls_porch_sf, tot_sqf_l_area FROM residence WHERE CC_ParcelId = ?', [activeParcel ? activeParcel.Id : 0], function (result) {
        if (result.length > 0) {
            var res = result[0];
            var message = '\n';
            for (var x in res) {
                message += x + ": " + (res[x] || 0) + "\n";
            }
            console.log(message);
        } else console.warn('Open a parcel and call this function.')
    });    ;
}