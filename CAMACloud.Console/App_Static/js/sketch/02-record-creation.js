﻿var defaultFields = ["CURRENT_DATE", "CURRENTDATE", "CURRENT_USER", "CURRENTUSER", "CURRENT_YEAR", "CURRENTYEAR", "CURRENT_YEAR", "CURRENTYEAR", "ACTIVE_YEAR", "ACTIVEYEAR", "CURRENT_DAY", "CURRENTDAY", "CURRENT_MONTH", "CURRENTMONTH", "FUTUREYEARSTATUS", "BLANK"]; 
function insertNewAuxRecordSketch(parcelIds, pAuxRecords, catIds, newAuxRowIds, otherDatas, callbacks) {
    // otherData  data format {fieldName:Value}
    // new Record creation format saveData += '^^^' + parcel.Id + '|' + newAuxRowuid + '|' + parentRowuid + '||' + sourceTable + '|new|' + CC_YearStatus + '\n';
    // field updation format saveData += '^^^' + parcel.Id + '|' + newAuxRowuid + '|' + field Id + '||' + Value + '|edit|' + parcentRowuid + '\n';
    
    var parcel = window.opener.activeParcel? window.opener.activeParcel: activeParcel;
    var fieldCategoriess = window.opener.fieldCategories? window.opener.fieldCategories: fieldCategories;
    var datafieldss = window.opener.datafields? window.opener.datafields: datafields;
    var getCategoryFromSourceTables = window.opener.getCategoryFromSourceTable? window.opener.getCategoryFromSourceTable: getCategoryFromSourceTable;
    var getCategorys = window.opener.getCategory? window.opener.getCategory: getCategory;
    var getDataFields = window.opener.getDataField? window.opener.getDataField: getDataField;
    var saveNewRecordData = '';
    
    function insertNewRecordSketch(parcelId, pAuxRecord, catId, newAuxRowId, otherData, callback) {
        var thisF, parentF;
        thisF = getCategorys(catId);
        parentF = getCategorys(thisF.ParentCategoryId);
        var FYvalue = '0';
        if (clientSettings.FutureYearValue) {
            FYvalue = clientSettings.FutureYearValue;
        }
        if(!parcelId)
        	parcelId = parcel.Id;
        if (!newAuxRowId)
            newAuxRowId = ccTicks();
        var newRecord = { ROWUID: newAuxRowId, ParentROWUID: null, CC_ParcelId: parcelId, CC_Deleted: false, CC_RecordStatus: "I", CC_LinkedROWUID: null, CC_YearStatus: null, Original: {}}
        var newParentAuxRowId = pAuxRecord ? pAuxRecord.ROWUID: '0';
        var childCats = fieldCategoriess.filter(function(x){return x.ParentCategoryId == catId});
        var CC_YearStatus = "";
        var yearFieldValue;
        
        if ( thisF.YearPartitionField && (clientSettings['FutureYearEnabled'] == '1') )  {
        	var yearField = getDataFields(thisF.YearPartitionField, thisF.SourceTable);
        	var ShowFutureData = (window.opener? window.opener.showFutureData : showFutureData);
            var YearFieldVal = (ShowFutureData ? FYvalue : clientSettings["CurrentYearValue"] );
			CC_YearStatus = ( ShowFutureData ? 'F' : 'A'  );
			if(yearField)
				yearFieldValue = '^^^' + parcelId + '|' + newAuxRowId + '|' + yearField.Id + '||' + YearFieldVal + '|edit|' + newParentAuxRowId + '\n';
    	}
        
        if(pAuxRecord){
        	newRecord.ParentROWUID = newParentAuxRowId;
            newRecord.parentRecord = pAuxRecord;
        }
        childCats.forEach(function(chCat){
            newRecord[chCat.SourceTable] = [];
        });
        newRecord.CC_YearStatus = CC_YearStatus != '' ? CC_YearStatus : null;

        saveNewRecordData += '^^^' + parcelId + '|' + newAuxRowId + '|' + newParentAuxRowId + '||' + thisF.SourceTable + '|new||' + CC_YearStatus + '\n';
		if(yearFieldValue)
			saveNewRecordData += yearFieldValue;
        var defaults = Object.keys(datafieldss).filter(function (k) { return datafieldss[k].SourceTable && (datafieldss[k].SourceTable == thisF.SourceTableName && datafieldss[k].DefaultValue != null) }).map(function (k) { var df = datafieldss[k]; return { Id: df.Id, Name: df.Name, SourceTable: df.SourceTable, DefaultValue: df.DefaultValue } });
        var data = [];

        if (otherData) {
             for (f in otherData) {
                 var fv = otherData[f];
                 var df = Object.keys(datafieldss).filter(function (k) { return datafieldss[k].SourceTable && (datafieldss[k].SourceTable == thisF.SourceTableName && datafieldss[k].Name == f) }).map(function (k) { var df = datafieldss[k]; return { Id: df.Id, Name: df.Name, SourceTable: df.SourceTable, DefaultValue: df.DefaultValue } });
                 if (df.length > 0) {
                      df[0].DefaultValue = fv;
                      df[0].otherDataVal = true;
                      defaults.push(df[0]);
                 }
            }
        }

        var pFields = [];
        if (thisF.IncludeParentFieldsOnInsert) {
             var parentSource = pAuxRecord ? pAuxRecord : parcel;
             thisF.IncludeParentFieldsOnInsert.split(',').forEach(function (f) {
                 f=f.trim(); 
                 pFields.push(datafieldss(f, thisF.SourceTable))
             })
        }
        
    	function insertDefaultFieldValues(pid, nrid, prid, fields, pAuxRecord, callback) {
		    var defaultDataVal = '';
		    
		    var _exec = function (f, _recExec_callback) {
		        var defaultValue = f.DefaultValue, specialDefaults = false;
		        var t = f.DefaultValue.toString().split('.')
		        var l = t.length;
		        t = t[l - 1]
		        
		        if (f.DefaultValue.toString().contains('parent.parent.') && pAuxRecord) {
		            specialDefaults = true;
		            if (pAuxRecord.parentRecord)
		                defaultValue = pAuxRecord.parentRecord[t];
		        } else if (f.DefaultValue.toString().contains('parent.') && pAuxRecord) {
		            specialDefaults = true;
		            defaultValue = pAuxRecord[t];
		        } else if ((f.DefaultValue.toString().contains('parent.') && !pAuxRecord )|| f.DefaultValue.toString().contains('parcel.')    ) {
		           specialDefaults = true;
		           defaultValue = parcel[t];
		           if(!defaultValue || defaultValue===undefined) defaultValue = null;
		       	}
		        if (defaultValue && defaultValue.toString().contains('parent.'))
		            defaultValue = "";
		        if( defaultValue && defaultFields.includes( defaultValue.toString().toUpperCase() ) )
            		specialDefaults = true;
            		
            	defaultValue = getDefaultValue(f,defaultValue);	
            	if( f.otherDataVal )
        			specialDefaults = true;	
        		if (!specialDefaults ) {
			    	var tblKeys = ( (window.opener && window.opener.tableKeys)? window.opener.tableKeys: tableKeys );
			    	if( tblKeys ) {
			    		var keyfield1 = tblKeys.filter(function (k){ return (k.Name == f.Name && k.SourceTable == f.SourceTable)});
						if (keyfield1[0])
							specialDefaults = true;
			    	}
				}
		        if( specialDefaults )
		       		defaultDataVal += '^^^' + pid + '|' + nrid + '|' + f.Id + '||' + defaultValue + '|edit|' + prid + '\n';
			    newRecord[f.Name] = defaultValue;
		        if (_recExec_callback) _recExec_callback();
		    }
		
		    var _recExec = function (list, callback) {
		        if (list.length == 0) {
		            if (callback) callback( defaultDataVal);
		        } else {
		               var first = list.shift();
		            _exec(first, function () {
		                _recExec(list, callback);
		            });
		        }
		    }
		
		    var list = fields.join ? fields : [fields];
		    _recExec(list, callback);
 		}

		function insertParentFieldValuesPCI(pid, nrid, prid, pFields, pAuxRecord, callback) {
		    var parentFieldData = '';
		    
		    var _exec = function (f, _recExec_callback) {
		        var value = pAuxRecord[f.Name];
		        parentFieldData += '^^^' + pid + '|' + nrid + '|' + f.Id + '||' + value + '|edit|' + prid + '\n';
				newRecord[f.Name] = value; 
		        if (callback) callback( parentFieldData );
		    }
		    
		    var _recExec = function (list, callback) {
		        if (list.length == 0) {
		            if (callback) callback();
		        } else {
		           	var first = list.shift();
		            _exec(first, function () {
		                _recExec(list, callback);
		            });
		        }
		    }
		    
		    var list = pFields.join ? pFields : [pFields];
		    _recExec(list, callback);
		}

        insertDefaultFieldValues(parcelId, newAuxRowId, (newParentAuxRowId || ''), defaults, pAuxRecord, function ( defaultData )  {
            if(defaultData)
            	saveNewRecordData += defaultData;
            insertParentFieldValuesPCI(parcelId, newAuxRowId, ( newParentAuxRowId || '' ), pFields, parentSource, function (parentFieldData){
                if(parentFieldData)
                	saveNewRecordData += parentFieldData;
                var autoInsert = thisF.AutoInsertProperties && JSON.parse('[' + thisF.AutoInsertProperties.replace(/}{/g, '},{') + ']');
                if(autoInsert)
                    var autoInsertItems = JSON.parse('[' + thisF.AutoInsertProperties.replace(/}{/g, '},{') + ']');
                if(pAuxRecord && parentF){
                    var pareRecord = parcel[parentF.SourceTable] && parcel[parentF.SourceTable].filter(function (pRec) {return pRec.ROWUID == pAuxRecord.ROWUID})[0];
                    if(pareRecord)
                        pareRecord[thisF.SourceTable].push(newRecord);
                }
         
                parcel[thisF.SourceTable].push(newRecord);
                
                var proc = function(Items) {
                    if(autoInsert != null){
                        var insertItem = autoInsertItems.pop();
                        if (insertItem && insertItem.table != null) {
                             var copyfields = insertItem.fieldsToCopy.split(',');
                             var insertCat = getCategoryFromSourceTables(insertItem.table)
                             var insertCatId = insertCat.Id;
                             var parentSourceData = insertItem.relation.toLowerCase() == 'child' ? newRecord : (insertItem.relation.toLowerCase() == 'parcel'? null: newRecord.parentRecord);// 2 relation -- 1,child  2.silbing
                             var newRecordData = {};
                             copyfields.forEach(function (f) {
                                 if (f && f.trim() != '' && newRecord[f]) newRecordData[f] = newRecord[f];
                             });
                             insertNewRecordSketch( null, parentSourceData, insertCatId, null, newRecordData, function () {
                                 if(autoInsertItems.length > 0) proc(autoInsertItems);
                                 else if (callback) callback(newAuxRowId, newParentAuxRowId);
                             })
                        }
                   }
                   else if (callback) callback(newAuxRowId, newParentAuxRowId);
                }	
                
                proc(autoInsertItems);	
            });
        });

    }
    
    insertNewRecordSketch(parcelIds, pAuxRecords, catIds, newAuxRowIds, otherDatas, function(nAuxRowId, nParentAuxRowId) {
        if(callbacks) callbacks(saveNewRecordData, nAuxRowId, nParentAuxRowId)
    });
    
}

function getDefaultValue(field,defaultValue){
	if(defaultValue == undefined||defaultValue == null)
   		return null;
    var ccmas = window.opener? window.opener.ccma: ccma;
    switch (defaultValue.toString().toUpperCase()) {
    	case "CURRENT_DATE":
     	case "CURRENTDATE":
	        var dt = toSQLDateString(new Date()) // (new Date()).format("yyyy-MM-dd");
	        defaultValue = checkForCustomFormat(field.Id) == '' ? dt : deFormatvalue(dt, field.Id);
	        break;
        case "CURRENT_USER":
        case "CURRENTUSER":
            defaultValue = ccmas.CurrentUser;
            break;
        case "CURRENT_YEAR":
     	case "CURRENTYEAR":
            defaultValue = ccmas.CurrentYear;
            break;
        case "CURRENT_YEAR":
        case "CURRENTYEAR":
        case "ACTIVE_YEAR":
        case "ACTIVEYEAR":
            defaultValue = clientSettings.CurrentYearValue
            break;
        case "CURRENT_DAY":
        case "CURRENTDAY":
            defaultValue = ccmas.CurrentDay;
            break;
        case "CURRENT_MONTH":
        case "CURRENTMONTH":
            defaultValue = ccmas.CurrentMonth;
            break;
        case "FUTUREYEARSTATUS":
        case "FUTUREYEARSTATUS":
            defaultValue = ccmas.FutureYearStatus;
            break;
        case "BLANK":
            defaultValue = ' ';
            break;
    }
    return defaultValue;
}

function checkForCustomFormat(fieldid) {
    var fmt;
    var datafieldss = window.opener.datafields? window.opener.datafields: datafields;
    if (datafieldss[fieldid].UIProperties)
        var o = JSON.parse(datafields[fieldid].UIProperties.replace(/'/g, '"'));
    else
        return '';
    if (o.CustomFormat && o.CustomFormat != '')
        fmt = o.CustomFormat;
    return ((fmt && fmt != '') ? fmt : '');
}

function deFormatvalue(value, fieldid) {
    var fmt;
    if (value == "" || value == null)
        return value;
    fmt = checkForCustomFormat(fieldid)
    if (fmt != '') {
        switch (fmt) {
            case 'YYYYMMDD':
                val = value.split(" ")[0].split("-");
                return (val[0] + val[1] + val[2]);
        }
    }
    else
        return value;

}

String.prototype.contains = function (infix) {
    return this.indexOf(infix) != -1;
}

String.prototype.padLeft = function (width, ch) {
    if (!ch) {
        ch = " ";
    }

    if (("" + this).length >= width) {
        return "" + this;
    }
    else {
        return arguments.callee.call(
      ch + this,
      width,
      ch
    );
    }
};

function toSQLDateString(str) {
    var noDate = "";

    if (!str) return noDate;
    var typeName = str.constructor.name;

    if ((str == null) || (str == "")) {
        return noDate;
    }
    var d;
    if (str instanceof Date) {
        d = str;
    } else {
        d = parseDate(str.substr(0, 10));
        if (isNaN(d)) return str;
    }

    if (d.getMonth() == NaN) { return ""; }
    var ds = d.getFullYear() + '-' + (d.getMonth() + 1).toString().padLeft(2, '0') + '-' + d.getDate().toString().padLeft(2, '0');
    if (ds.startsWith('NaN')) {
        var dt = str.split(" ")[0].split("-");
        var dd = dt[2];
        var mm = dt[1];
        var yyyy = dt[0];
        return yyyy + "-" + mm + "-" + dd;
    }

    return ds;
}