﻿
CAMACloud.Sketching.Formatters.WarrenOH = {
    name: "Warren OH",
    saveAllSegments: false,
    allowSegmentAddition: false,
    allowSegmentDeletion: false,
    preventSavingOpenSegments: false,
    allowSegmentAdditionOnMidsketch: true,
    nodeToString: function (n) {
        var str = "";
        if (n.arcLength > 0) {
            str += "A" + sRound(n.arcLength);
        }
        if (n.arcLength < 0) {
            str += "B" + Math.abs(sRound(n.arcLength));
        }
        if (n.dy > 0) {
            if (n.isStart) {
                if (str != "") str += " ";
            } else {
                if (str != "") str += "/";
            }
            str += n.sdy + sRound(n.dy);
        }
        if (n.dx > 0) {
            if (n.isStart) {
                if (str != "") str += " ";
            } else {
                if (str != "") str += "/";
            }
            str += n.sdx + sRound(n.dx);
        }
        if (n.hideDimensions) {
            if (str != "") str += "/";
            str += "#";
        }
        if (n.isStart) {
            str += "C";
        }
        return str;
    },
    vectorToString: function (v) {
        var str = "";
        if (v.startNode != null) {
            str += v.startNode.vectorString();
            var nn = v.startNode.nextNode;
            while (nn != null) {
                str += nn.vectorString();
                nn = nn.nextNode;
            }
            str += "H"
        }
        return str;
    },
    labelPositionFromString: function (editor, l) {
        if (!l)
            return null
        var parts = l.split(',');
        return new PointX(parseFloat(parts[0]), -(parseFloat(parts[1])))
    },
    vectorFromString: function (editor, s) {
        if (s == null) {
            var nv = new Vector(editor);
            nv.isClosed = false;
            nv.label = "Blank"
            nv.uid = -1
            return nv;
        }

        var o = editor.origin;
        var p = o.copy();

        var v = new Vector(editor);

        var hi = s.split(':');
        var head = hi[0];
        var lineString = s

        v.header = "";

        var isStart = false;
        var hasStarted = false;
        var isVeer = false;
        var veerSteps = 0;
        var ins = lineString.match(/[C]|[UDLR][0-9]*/g);
        for (var i in ins) {
            var cmd = ins[i];
            var distance = 0;
            if (cmd == "C") {
                isStart = true;
            }
                //else if (cmd == "V") {
                //    isVeer = true;
                //    veerSteps = 2;
                //    continue;
                //}
            else {
                var cp = cmd.match(/[UDLR]|[0-9]*/g);
                var dir = cp[0];
                distance = cp[1];
                var pix = parseFloat(distance) * DEFAULT_PPF;
                switch (dir) {
                    case "U": p.moveBy(0, pix); break;
                    case "D": p.moveBy(0, -pix); break;
                    case "L": p.moveBy(-pix, 0); break;
                    case "R": p.moveBy(pix, 0); break;
                }
                if (!isVeer)
                    p = p.copy();
            }

            if (isVeer) {
                veerSteps--;
                if (veerSteps == 0) {
                    isVeer = false;
                } else
                    continue;
            }

            if (isStart) {
                v.start(p.copy());
                hasStarted = true;
                isStart = false;
            } else if (hasStarted) {
                var q = p.copy().alignToGrid(editor);
                if (!v.startNode.overlaps(q))
                    v.connect(p.copy());
                else
                    v.terminateNode(v.startNode);
            }
            if (v.endNode)
                if (cmd) v.endNode.commands.push(cmd);


        }

        if (v.startNode)
            if (v.startNode.overlaps(v.endNode.p.copy()))
                v.isClosed = true;
        return v;

    },
    open: function (editor, data) {
        console.log(data)
        editor.vectors = [];
        editor.sketches = [];
        for (var x in data) {
            var sketch = new Sketch(editor);
            var s = data[x];
            sketch.parentRow = s.parentRow;
            sketch.uid = s.uid;
            sketch.label = s.label;
            sketch.vectors = [];
            sketch.config = s.config || {};
            sketch.sid = s.sid;
            var counter = 0;
            for (var i in s.sketches) {
                counter += 1;
                var sk = s.sketches[i];
                var v = this.vectorFromString(editor, sk.vector);
                v.sketch = sketch;
                v.uid = sk.uid;
                v.index = counter;
                v.name = sk.label;
                var lbdesc = sk.labelDescription ? '-' + sk.labelDescription.Name : ''
                v.label = '[' + counter + '] ' + sk.label + lbdesc;
                v.vectorString = sk.vector;
                v.isChanged = sk.isChanged;
                v.labelPosition = this.labelPositionFromString(editor, sk.labelPosition);
                editor.vectors.pop();
                sketch.vectors.push(v);
            }
            sketch.isModified = false;
            editor.loadNotesForSketch(sketch, s.notes);
            editor.sketches.push(sketch);
        }
    }
}
