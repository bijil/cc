﻿
CAMACloud.Sketching.Formatters.Sigma2 = {
    name: "SIGMA Sketch II Format (Jackson)",
    saveAllSegments: true,
    allowSegmentAddition: true,
    allowSegmentDeletion: true,
    allowLabelEdit: true,
    newVectorHeaderSuffix: '{0}[R0U0]:',
    nodeToString: function (n) {
        var str = "";
        if (!n.isStart) {
            if ((n.dy > 0) && (n.dx > 0)) {
                str += "(";
            }
        }
        if (n.dy > 0) {
            str += n.sdy + sRound(n.dy);
        }
        if (n.dx > 0) {
            str += n.sdx + sRound(n.dx);
        }
        if (!n.isStart) {
            if ((n.dy > 0) && (n.dx > 0)) {
                str += ")";
            }
        }
        if (n.isStart) {
            str += "S";
        }
        return str;
    },
    vectorToString: function (v) {
        var str = "";
        if (v.startNode != null) {
            if (!v.labelEdited)
                str += v.header;
            else {
                str += v.name + "{" + v.level + "}[" + this.labelPositionToString(v.labelPosition) + "]:";
            }
            str += v.startNode.vectorString();
            var nn = v.startNode.nextNode;
            while (nn != null) {
                str += nn.vectorString();
                nn = nn.nextNode;
            }
        }
        return str;
    },
    vectorFromString: function (editor, s) {
        if (s == null) {
            var nv = new Vector(editor);
            nv.isClosed = false;
            nv.label = "Blank"
            nv.uid = -1
            return nv;
        }

        var o = editor.origin;
        var p = o.copy();

        var v = new Vector(editor);

        var hi = s.split(':');
        var head = hi[0];
        var lineString = hi[1];

        v.header = head + ":";

        var isStart = false;
        var hasStarted = false;
        var isVeer = false;
        var veerSteps = 0;
        var ins = lineString.match(/[S]|[(]|[UDLR][0-9]*/g);
        for (var i in ins) {
            var cmd = ins[i];
            var distance = 0;
            if (cmd == "S") {
                isStart = true;
            } else if (cmd == "(") {
                isVeer = true;
                veerSteps = 2;
                continue;
            }
            else {
                var cp = cmd.match(/[UDLR]|[0-9]*/g);
                var dir = cp[0];
                distance = cp[1] || 0;
                var pix = parseFloat(distance) * DEFAULT_PPF;
                switch (dir) {
                    case "U": p.moveBy(0, pix); break;
                    case "D": p.moveBy(0, -pix); break;
                    case "L": p.moveBy(-pix, 0); break;
                    case "R": p.moveBy(pix, 0); break;
                }
                if (!isVeer)
                    p = p.copy();
            }

            if (isVeer) {
                veerSteps--;
                if (veerSteps == 0) {
                    isVeer = false;
                } else
                    continue;
            }

            if (isStart) {
                v.start(p.copy());
                hasStarted = true;
                isStart = false;
            } else if (hasStarted) {
                var q = p.copy().alignToGrid(editor);
                if (!v.startNode.overlaps(q))
                    v.connect(p.copy());
                else
                    v.terminateNode(v.startNode);
            }
            if (v.endNode)
                if (cmd) v.endNode.commands.push(cmd);


        }

        v.isModified = false;
        if (v.startNode)
            if (v.startNode.overlaps(v.endNode.p.copy()))
                v.isClosed = true;
        return v;

    },
    labelPositionFromString: function (editor, l) {
        var ins = l.match(/[UDLR]|[0-9.]*/g);
        if (ins.length > 3) {
            var x = ins[0] == "R" ? ins[1] : -ins[1];
            var y = ins[2] == "U" ? ins[3] : -ins[3];
            return new PointX(parseFloat(x), (parseFloat(y)))
        }

    },
    labelPositionToString: function (p) {
        var x = p.x > 0 ? "R" + p.x : "L" + -p.x;
        var y = p.y > 0 ? "U" + p.y : "D" + -p.y;
        return x + y
    },
    open: function (editor, data) {
        editor.vectors = [];
        editor.sketches = [];
        for (var x in data) {
            var sketch = new Sketch(editor);
            var s = data[x];
            sketch.parentRow = s.parentRow;
            sketch.uid = s.uid;
            sketch.label = s.label;
            sketch.vectors = [];
            sketch.config = s.config || {};
            sketch.sid = s.sid;
            var counter = 0;
            for (var i in s.sketches) {
                counter += 1;
                var sk = s.sketches[i];
                var v = this.vectorFromString(editor, sk.vector);
                v.sketch = sketch;
                v.uid = sk.uid;
                v.index = counter;
                v.name = sk.label;
                v.label = '[' + counter + '] ' + sk.label;
                v.labelPosition = this.labelPositionFromString(editor, sk.labelPosition);
                v.vectorString = sk.vector;
                v.isChanged = sk.isChanged;
                editor.vectors.pop();
                sketch.vectors.push(v);
            }
            sketch.isModified = false;
            editor.loadNotesForSketch(sketch, s.notes);
            editor.sketches.push(sketch);
        }
    },
    getParts: function (vectorString) {
        var p = [];
        var parts = (vectorString || '').split(",");
        for (var i in parts) {
            var part = parts[i];
            if (/(.*?):(.*?)/.test(part)) {
                var hi = part.split(':');
                var head = hi[0];
                var info = hi[1];
                var labels, posString;

                var r1 = /(.*?)\[(.*?)\]/;
                var hparts = r1.exec(head);
                if (hparts == null) {
                    labels = "";
                } else {
                    labels = hparts[1].replace(/\{(.*?)\}/g, '');
                    posString = hparts[2];
                }

                p.push({
                    label: labels,
                    vector: part,
                    labelPosition: posString
                })

            }
        }

        return p;
    },
    addNewVector: function () {

    }
}
