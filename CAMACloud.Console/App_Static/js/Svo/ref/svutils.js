﻿Object.defineProperties(window, {
    lightWeight: {
        get: function () { return localStorage.getItem('sv-lightweight') == "1"; },
        set: function (v) { localStorage.setItem('sv-lightweight', v ? "1" : "0"); }
    },
    autoForward: {
        get: function () { return localStorage.getItem('sv-autoforward') == "1"; },
        set: function (v) { localStorage.setItem('sv-autoforward', v ? "1" : "0"); }
    },
    autoSaveEnabled: {
        get: function () { return localStorage.getItem('sv-autosave') == "1"; },
        set: function (v) { localStorage.setItem('sv-autosave', v ? "1" : "0"); }
    },
    autoSaveBatch: {
        get: function () { var v = localStorage.getItem('sv-autosavebatch'); return v ? parseInt(v) : 20; },
        set: function (v) { if (isNaN(parseInt(v))) v = 20; if (v <= 0) v = 20; if (v > 100) v = 100; localStorage.setItem('sv-autosavebatch', parseInt(v)); autoSaveCount = 0; }
    },
    testMode: {
        get: function () { return localStorage.getItem('sv-testmode') == "1"; },
        set: function (v) { localStorage.setItem('sv-testmode', v ? "1" : "0"); }
    }
})

function getCenterOfOverlay() {
    if (overlay.container) {
        var img_pos = overlay.container.getBoundingClientRect();
        var blockCenter = { x: img_pos.left + img_pos.width / 2, y: img_pos.top + img_pos.height / 2 };
        return PointToLatLng(blockCenter);
    }
}

function getAdjHandlePosition() {
    if (overlay.container) {
        var img_pos = overlay.container.getBoundingClientRect();
        var blockCenter = { x: img_pos.left - 30, y: img_pos.top - 30 };
        return PointToLatLng(blockCenter);
    }

}