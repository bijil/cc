﻿var mapFlag;

function sigmaSketch(color, vector, target, showLabels, showLengths) {
    var ox, oy;
    ox = 0; oy = 0;
    var x, y;
    var max, may, mix, miy;
    max = 0; may = 0; mix = 0; miy = 0;

    var scale = 1;
    var fontSize;

    var canvas = document.getElementById(target);
    var ctx;

    if (!canvas.getContext) {
        alert('Incompatible browser!');
        return;
    }

    ctx = canvas.getContext("2d");

    canvas.width = canvas.width;

    var i;

    var parts = vector.split(',');
    for (i in parts) {
        var part = parts[i];
        if (/(.*?):(.*?)/.test(part)) {
            var hi = part.split(':');
            var head = hi[0];
            var info = hi[1];
            drawLines(ctx, info, true, true);
        }
    }

    var sHeight = may - miy;
    var sWidth = max - mix;
    if (sHeight * 1.1 > canvas.height) {
        scale = canvas.height / sHeight * 0.9;
    }

    var maxHeight = canvas.height * 0.9;
    var maxWidth = canvas.width * 0.9;
    var aspectRatio = parseFloat(sWidth) / parseFloat(sHeight);
    var width, height;

    if ((sWidth < maxWidth) && (sHeight < maxHeight)) {
        scale = 1;
    } else if (aspectRatio > 1) {
        width = maxWidth;
        height = (width / aspectRatio);
        if (height > maxHeight) {
            height = maxHeight;
            width = (height * aspectRatio);
        }
    } else {
        height = maxHeight;
        width = (height * aspectRatio);
        if (width > maxWidth) {
            width = maxWidth;
            height = (width / aspectRatio);
        }
    }

    scale = width / sWidth;

    ctx.lineWidth = 2 / scale;
    ctx.strokeStyle = color;
    fontSize = parseInt(12 / scale);
    if (fontSize < 8)
        fontSize = 8;
    if (fontSize > 18)
        fontSize = 18;
    ctx.font = 'normal ' + fontSize + 'pt Arial';
    ctx.scale(scale, scale);
    oy = (canvas.height / scale - (sHeight)) / 2 - miy;
    ox = (canvas.width / scale - (sWidth)) / 2 - mix;
    for (i in parts) {
        var part = parts[i];
        if (/(.*?):(.*?)/.test(part)) {
            var hi = part.split(':');
            var head = hi[0];
            var info = hi[1];
            if (showLabels)
                drawLabel(ctx, head);
            drawSketch(ctx, info, showLengths);
        }
    }

    function drawLabel(ctx, head) {
        var posString, labels;
        var r1 = /(.*?)\[(.*?)\]/;
        var parts = r1.exec(head);
        labels = parts[1].replace(/\{(.*?)\}/g, '');
        posString = parts[2];
        drawLines(ctx, posString);
        ctx.fillText(labels, x, y);

    }
    function drawSketch(ctx, vector, showLengths) {
        drawLines(ctx, vector, false, false, showLengths);
    }

    function drawLines(ctx, lineString, penOff, detectBounds, showLengths) {
        var penOn = false;
        x = ox; y = oy;
        var cx, cy;
        var ins = lineString.match(/[S]|[UDLR][0-9]*/g);
        ctx.beginPath();
        ctx.moveTo(x, y);
        for (var i in ins) {
            cx = x;
            cy = y;
            var cmd = ins[i];
            var distance = 0;
            if (cmd == "S") {
                penOn = true;
            }
            else {
                var cp = cmd.match(/[UDLR]|[0-9]*/g);
                var dir = cp[0];
                distance = cp[1];
                var dis = parseInt(distance) * 12;
                switch (dir) {
                    case "U":
                        y -= dis;
                        break;
                    case "D":
                        y += dis;
                        break;
                    case "L":
                        x -= dis;
                        break;
                    case "R":
                        x += dis;
                        break;
                }
            }
            if (!penOff && penOn) {
                if (showLengths) {
                    if (distance > 0)
                        ctx.fillText(distance, (x + cx) / 2, (y + cy) / 2);
                }
                ctx.moveTo(cx, cy);
                ctx.lineTo(x, y);
                ctx.stroke();
            } else {
                ctx.moveTo(x, y);
            }

            if (detectBounds) {
                if (x < mix) mix = x;
                if (x > max) max = x;
                if (y < miy) miy = y;
                if (y > may) may = y;
            }

        }
        ctx.stroke();
    }

    return canvas.toDataURL();
}
var ccma;
var getDataField = top.getDataField;
var evalLookup = top.evalLookup;
var getData = (top && top.getData)? top.getData: function(x){};
function getSketchSegments(showPrevious, previousLookup, errorThrow) {
    if ( !clientSettings.SketchConfig && !sketchSettings["SketchConfig"] ) {
        alert("Please add a sketch config to load sketches");
        return;
    }
    if (!ccma && top)
        ccma = top.ccma
    if ( clientSettings["SketchFormat"] || sketchSettings["SketchFormat"] )
        ccma.Sketching.SketchFormatter = eval( 'CAMACloud.Sketching.Formatters.' + ( clientSettings["SketchFormat"] || sketchSettings["SketchFormat"] ) );
    ccma.Sketching.Config = CAMACloud.Sketching.Configs.GetConfigFromSettings();
    if (!ccma.Sketching.SketchFormatter)
        if (ccma.Sketching.Config.formatter)
            ccma.Sketching.SketchFormatter = eval('CAMACloud.Sketching.Formatters.' + ccma.Sketching.Config.formatter);
    var sketchedited = false;
    var IsSketchApproved = false;
    var processor = ccma.Sketching.SketchFormatter;
    var config = ccma.Sketching.Config;
    if (!config) return;
    if (!processor)
        processor = CAMACloud.Sketching.Formatters.TASketch;
	var isPages = false;
    var sketches = [];
    var _sktLookupArray = ( sketchSettings["sketchLookupQuery"] && sketchSettings["sketchLookupQuery"] != '' )? sketchSettings["sketchLookupQuery"].split('$'): [];
    var sk_count = -1;
    for (var ski in config.sources) {
    	var sk_count = sk_count + 1;
        var sksource = config.sources[ski];
        var keyPrefix = sksource.Key ? (sksource.Key + '--') : '';
        var sketchTable = sksource.SketchSource.Table ? eval('activeParcel.' + sksource.SketchSource.Table) : [activeParcel];
        sketchTable = !showPrevious? (sketchTable && sketchTable.filter ? sketchTable.filter(function (st) { return st.CC_Deleted != true && eval( sksource.SketchSource.sketchSourceFilter || true ); }) : sketchTable): (sketchTable && sketchTable.filter ? sketchTable.filter(function (st) { return eval( sksource.SketchSource.sketchSourceFilter || true ); }) : sketchTable);
        var sketchTableOriginal =  sksource.SketchSource.Table ? ( activeParcel.Original[sksource.SketchSource.Table] ? ( !showPrevious ?  ( activeParcel.Original[sksource.SketchSource.Table].filter(function(st) { return st.CC_Deleted != true &&  eval( sksource.SketchSource.sketchSourceFilter || true ); }) ) : ( activeParcel.Original[sksource.SketchSource.Table].filter(function(st) { return eval( sksource.SketchSource.sketchSourceFilter || true ); }) ) ) : [] ) : [activeParcel.Original];
        var notesTable = sksource.NotesSource && sksource.NotesSource.Table ? eval('activeParcel.' + sksource.NotesSource.Table) : [];
        notesTable = notesTable && notesTable.filter ? notesTable.filter(function (x) { return x.CC_Deleted != true }) : notesTable;
        var notesTableOriginal = sksource.NotesSource && sksource.NotesSource.Table ? eval('activeParcel.Original' + sksource.NotesSource.Table) : [];
        if (showPrevious)
            notesTable = notesTableOriginal;
        var sketchKeyField = sksource.SketchSource.KeyField || 'ROWUID';
        
        if (sksource.SketchSource.sketchSourceSorting) {
           sketchTable = sketchSorting(sketchTable, sksource.SketchSource.sketchSourceSorting, (sksource.SketchSource.Table? sksource.SketchSource.Table: null));
           var orgSortOrder = sketchTable.map(function(st) { return st.ROWUID });
           var orgmap = orgSortOrder.reduce((r, v, i) => ((r[v] = i), r), {});
           sketchTableOriginal = sketchTableOriginal.sort((a, b) => orgmap[a['ROWUID']] - orgmap[b['ROWUID']]);
       	}
         
        for (var i in sketchTable) {
            var pi = sketchTable[i];
            var orgTable = sketchTableOriginal[i];
            var originalTab = sketchTable[i].Original;
            if (showPrevious && originalTab)
                pi = originalTab;
            var sketchKey = keyPrefix + pi[sketchKeyField];
            var labelField = sksource.SketchSource.LabelField ? sksource.SketchSource.LabelField.split( '/' ) : [];
            if (sketchSettings && sketchSettings.SketchDDLOptions)
            	labelField = labelField.concat(sketchSettings.SketchDDLOptions.split(','));
            if(showPrevious) {
            	var labelLength = labelField.length;
            	var l;
            	for(l = 0 ; l < labelLength ; l++){
					if(labelField[l].indexOf("parentRecord.") > -1)
						labelField[l] = labelField[l].split("parentRecord.")[1];
            	}
            }
            var labelFieldValue = '', labelFieldDelimter = sksource.SketchSource.LabelFieldDelimter? sksource.SketchSource.LabelFieldDelimter: '/';
            
            for(var k = 0; k < labelField.length; k++){
            	var splitField = labelField[k].split(' %# ');
            	for ( var sf = 0; sf < splitField.length; sf++ ) {
            		var lblField = splitField[sf].trim(), field, desc = '', bracketlblField = splitField[sf].trim(), bracketExists = false;
	                if (bracketlblField.indexOf('{') > -1) {
            			lblField = bracketlblField.match(/\{.*?\}/g)[0].match(/\{.*?\}/g)[0].replace( '{', '' ).replace( '}', '' );
            			bracketExists = true;
                    }

                    if (lblField == 'KeyValue1' || lblField == 'AlternateKey') {
                        if (clientSettings.ShowAlternateField == "1") desc = activeParcel.alternateFieldvalue;
                        if (!desc || desc == '') desc = eval('activeParcel.KeyValue1');
                    }
                    else {
                        var value = (lblField.indexOf('parentRecord.') > -1 && !pi.parentRecord) ? '' : (eval('pi.' + lblField) || '');
                        var tbl = sksource.SketchSource.Table ? sksource.SketchSource.Table : null;
                        if (lblField.indexOf('parentRecord.') > -1) {
                            lblField = lblField.replace('parentRecord.', '');
                            tbl = window?.parent?.parentChild ? (window?.parent?.parentChild.filter(function (pc) { return pc.ChildTable == tbl })[0] ? window?.parent?.parentChild.filter(function (pc) { return pc.ChildTable == tbl })[0].ParentTable : tbl) : tbl;
                        }
                        if (getDataField)
                            field = getDataField(lblField, tbl);
                        if (field && field.InputType == '5') {
                            if (evalLookup)
                                desc = evalLookup(lblField, value, null, field);
                            desc = ((desc == '???') || (desc == '') || (desc == null)) ? value : desc;
                        }
                        else
                            desc = value;
                        if (config.isPVDConfig)
                            desc = ((lblField == 'ResidenceType' || lblField == 'CommercialType2') ? (desc == 'None' ? '' : desc) : (lblField == 'YearConstrcuted' ? (desc == '0' || desc == 0 ? '' : desc) : desc));
                    }

                    if (bracketExists) {
                		bracketlblField = bracketlblField.replace('{' + lblField + '}', desc);
                		desc = bracketlblField;
                    }
                    
	                labelFieldValue = labelFieldValue + desc;
	                if( sf != (splitField.length -1) && desc && desc != '' )
	                	labelFieldValue = labelFieldValue + labelFieldDelimter;
                }
                if(k != (labelField.length -1) && labelFieldValue[labelFieldValue.length - 1] != labelFieldDelimter)
                	labelFieldValue = labelFieldValue + labelFieldDelimter;
            }
            var sketch = {
                uid: sketchKey.toString(),
                sid: pi[sketchKeyField],
                label: (sksource.SketchLabelPrefix ? (sksource.SketchLabelPrefix + ' ') : '') + (labelFieldValue) + (eval('pi.' + sksource.SketchSource.KeyFields[0]) ? (' [' + eval('pi.' + sksource.SketchSource.KeyFields[0]) + ']') : ''),
                sketches: [],
                notes: [],
                config: sksource,
                parentRow: sksource.SketchSource.Table ? pi : null
            }
            
            if(sksource.GroupingField && sksource.GroupingField == 'ParcelId')
                	sketch.GroupingValue = activeParcel.Id;
            else if(sksource.GroupingField && sksource.DGroupingField)
                sketch.GroupingValue = pi[sksource.GroupingField] || pi['ROWUID'];
            else if(sksource.GroupingField && sksource.OGroupingField)
                sketch.GroupingValue = pi[sksource.GroupingField] || pi['ParentROWUID'];
            else if ( sksource.GroupingField && sksource.SketchSource.Table )
                sketch.GroupingValue = pi[sksource.GroupingField] || pi['ParentROWUID'];
                
            if(config.ShowSectNum){
                	var isBristol = (config.isBristolTable ? true : false);
        			var sectTables = {}, isShowSectNum = false;
        			if (isBristol)
            			sectTables = { Residential: 'CCV_CONSTR_RESDEP', Commercial: 'CCV_CONSTR_COMDEP', CondoMain: 'CCV_CONSTR_CDMDEP', CondoUnit: 'CCV_CONSTR_CDUDEP' }
        			else
            			sectTables = { Residential: 'CCV_CONSTRSECTION_RES', Commercial: 'CCV_CONSTRSECTION_COM', CondoMain: 'CCV_CONSTRSECTION_CDM', CondoUnit: 'CCV_CONSTRSECTION_CDU' }
            		if( !showPrevious ){	
        				if (pi[sectTables.Residential].filter(function (res) { return res.CC_Deleted != true }).length > 0){
            				if(pi[sectTables.Residential].filter(function (res) { return res.CC_Deleted != true }).length > 1)
            					isShowSectNum = true;
            			}
        				else if (pi[sectTables.Commercial].filter(function (comm) { return comm.CC_Deleted != true }).length > 0){
            				if (pi[sectTables.Commercial].filter(function (comm) { return comm.CC_Deleted != true }).length > 1)
            					isShowSectNum = true;
            			}
        				else if (pi[sectTables.CondoMain].filter(function (comn) { return comn.CC_Deleted != true }).length > 0){
            				if (pi[sectTables.CondoMain].filter(function (comn) { return comn.CC_Deleted != true }).length > 1)
            					isShowSectNum = true;
            			}
        				else if (pi[sectTables.CondoUnit].filter(function (conut) { return conut.CC_Deleted != true }).length > 0){
            				if (pi[sectTables.CondoUnit].filter(function (conut) { return conut.CC_Deleted != true }).length > 1)
            					isShowSectNum = true;
            			}
            		}
            		if(isShowSectNum)
            			sketch.isShowSectNum = true;
            }
            
            var query = sketchSettings["sketchLookupQuery"]? _sktLookupArray[sk_count]: ( sksource.LookUpQuery? sksource.LookUpQuery: '');
            if ( query && query != '' ) {
				var testField;
                if ( query.search( /\{.*?\}/g ) > -1 ){
                    query.match( /\{.*?\}/g ).forEach( function ( fn ) {
                        var testFieldName = fn.replace( '{', '' ).replace( '}', '' );
                        if ( testFieldName.contains('parcel.') )
                        	testField = activeParcel[testFieldName]? activeParcel[testFieldName]: null;
                        else if ( testFieldName.contains('parent.parent.') )
                        	testField = ( pi['parentRecord']? ( pi['parentRecord']['parentRecord']? pi['parentRecord']['parentRecord'][testFieldName]: null ): null );  
						else if ( testFieldName.contains('parent.') )
							testField = ( pi['parentRecord']? pi['parentRecord'][testFieldName]: null);
						else
							testField = pi[testFieldName];
                        query = query.replace( fn, pi[testFieldName] )
                    } );
                }
                sketch.lookUp = [];
				if (previousLookup && previousLookup.length > 0) { 
					var prelk = previousLookup.filter(function(x){return x.sid == pi[sketchKeyField]})[0]? previousLookup.filter(function(x){return x.sid == pi[sketchKeyField]})[0].lk: [];
			    	sketch.lookUp = prelk;
			   	}
				else{
                	getData( query, [], function ( ld, res, skdata ) {
                		var lookup = {};
                    	if (ld.length > 0) {
                            var props = Object.keys(ld[0]);
                            var IdField = props[0]; var NameField = props[1]; var DescField = props[1]
                            if (!NameField) NameField = IdField;
                            if (!DescField) DescField = NameField;
                            for (var x in ld) {
                                var d = ld[x];
                                var v = d[IdField];
                                if(v && typeof(v) == "string") v = v.replace('&gt;','>').replace('&lt;','<');
                                if (lookup[v] === undefined)
                                    lookup[v] = { Name: d[NameField], Description: d[DescField], Ordinal: x, SortType: null, Id: v, Object: d };
                            }

                            skdata.lookUp = lookup;
                        }
                        else
                    		skdata.lookUp = ld;
                	}, null, sketch )
				}
            }
            
            for (var vs in sksource.VectorSource) {
                var vectorSource = sksource.VectorSource[vs];
                var sketchVectorTable = vectorSource.Table ? eval('activeParcel.' + vectorSource.Table) : [activeParcel];
                let sketchVectorTableAll = sketchVectorTable;// index is wrong if deleted record is exists.Readonly property checking based on index of all vector table record.
                sketchVectorTable = !showPrevious ? (sketchVectorTable && sketchVectorTable.filter ? sketchVectorTable.filter(function (x) { return x.CC_Deleted != true }) : sketchVectorTable) : sketchVectorTable;
                //sketchVectorTable = sketchVectorTable && sketchVectorTable.filter ? sketchVectorTable.filter(function (x) { return x.CC_Deleted != true }) : sketchVectorTable;
                var sketchVectorTableOrginal = vectorSource.Table ? eval('activeParcel.Original.' + vectorSource.Table) : [activeParcel.Original];
                var sketchVectorOrig = sketchVectorTable ? sketchVectorTable.Original : [];
                var sketchVectorKeyField = vectorSource.KeyField || 'ROWUID';
                var vectorCategory = window.parent.getCategoryFromSourceTable?  window.parent.getCategoryFromSourceTable(vectorSource.Table): ( ( window.parent.opener && window.parent.opener.getCategoryFromSourceTable )? window.parent.opener.getCategoryFromSourceTable: null );
                vectorCategory = vectorCategory? vectorCategory: [];
                vectorCategory = ( Object.keys( vectorCategory ).length == 0 ) ? null : vectorCategory;
                var doNotAllowDeleteFirstVector = null, maxVectorLimit = null;
                if ( vectorCategory )
                {
                    doNotAllowDeleteFirstVector = vectorCategory.DoNotAllowDeleteFirstRecord;
				    maxVectorLimit = vectorCategory.MaxRecords
                }
                doNotAllowDeleteFirstVector = ( doNotAllowDeleteFirstVector == null || doNotAllowDeleteFirstVector == 'false' || doNotAllowDeleteFirstVector == false ? false : true );
                maxVectorLimit = ( maxVectorLimit == null ? 0 : parseInt( maxVectorLimit ) );
                var sketchChanges = activeParcel.ParcelChanges ? activeParcel.ParcelChanges.filter(function (ch) { if (ch.FieldName == vectorSource.CommandField || ch.FieldName == vectorSource.LabelField) return ch }) : [];
                var approvedSketchChanges = activeParcel.ParcelChanges ? activeParcel.ParcelChanges.filter(function (ch) { if ((ch.QCChecked) && (ch.FieldName == vectorSource.CommandField || ch.FieldName == vectorSource.LabelField)) return ch }) : [];
                if (sketchChanges && approvedSketchChanges && sketchChanges.length > 0 && (sketchChanges.length == approvedSketchChanges.length))
                    IsSketchApproved = true;
                var labelRequiredTrue = vectorSource.IsLabelRequired == undefined ? true : vectorSource.IsLabelRequired;
                
                if (sksource.SketchSource.Table == vectorSource.Table) {
                    var fullVector = eval('pi.' + vectorSource.CommandField);
                    var fullVectorOrg = orgTable ? eval('orgTable.' + vectorSource.CommandField) : null;
                    if (!processor) {
                        processor = CAMACloud.Sketching.Formatters.Sigma;
                    }
                    if (config.encoder) {
                        var encodeName = config.encoder;
                        fullVector = CAMACloud.Sketching.Encoders[encodeName].decode(fullVector);
                        fullVectorOrg = CAMACloud.Sketching.Encoders[encodeName].decode(fullVectorOrg);
                    }

                    let customParameters = {};

                    if (config.isHelionRS) {
                        customParameters.isHelionRS = true;
                    }

                    try {
                        var parts = processor.getParts ? processor.getParts(fullVector, config.doNotDecodeEncode, null, customParameters) : [];
                        var orgParts = fullVectorOrg ? processor.getParts(fullVectorOrg, config.doNotDecodeEncode, null, customParameters) : null;
                    }
                    catch (err) {
                        if (errorThrow) throw "Sketch cannot be rendered due to the Vector sketch data entered by the user.";
                        else return [];
                    }
                    var pcount = 0, boundaryScale = 0;
                    var filteredFreeFormTextLabels = processor.getFreeFormTextLabels ? processor.getFreeFormTextLabels(fullVector) : [];
                    var filteredfreeFormLineEntries = processor.getLineEntries ? processor.getLineEntries(fullVector) : [];
                    
                    if (config.IsJsonFormat) {
                       var jString = (fullVector && fullVector != '') ? JSON.parse(fullVector) : "";
                      	if(jString == ""){
                       		jString = {
                       			JSON: {
                       				GlobalSettings: {SketchScale: 12, AreaSizeDecimals: 1, AreaSizeSuffix: "sf", DimensionDecimals: 2, DimensionSuffix: "'", DimensionsMatchLineColor: false},
                       				LabelEntries: [],
                       				LineEntries: []
                       			},
                       			DCS: ""
                       		}
                       	}
                       	sketch.jsonString = jString;
                    }
                    var mvpncount = 0;
                    
                    for (var x in parts) {
                        pcount++;
                        var part = parts[x];
                        var orgPart = orgParts ? orgParts[x] : null;
                        var partSplitfirst = null, orgPartSplitfirst = null;
                        
                        if (part.isMVPNote) {
                        	var mData = part.mvpData, isNoteChanged = false;
                        	
                        	if((!orgPart || (orgPart && !orgPart.vector)) && part.vector)
                                isNoteChanged =  true;
                            else if (orgPart && orgPart.vector && part.vector)
                                isNoteChanged = part.vector.replace(/ /g, '') != orgPart.vector.replace(/ /g, '');
                             
                        	for(var nt in part.mvpNotes) {
                        		var note = part.mvpNotes[nt];
                        		mvpncount++;
                            	var n = {
	                            	uid: sketchKey.toString() + "/" + mvpncount,
	                           	 	text: ( note['noteText'] || '' ).replace( /&lt;/g, '' ).replace( /&gt;/g, '' ),
	                            	x: parseFloat( note['xPosition'] || 0 ) * ( 1 ),
	                            	y: parseFloat( note['yPosition'] || 0 ) * ( 1 ),
									fontSize: note['fontSize'],
									mvpData: mData
	                        	}
	                        	sketch.notes.push( n );
                        	}
                        	if (isNoteChanged && !showPrevious)
                            	sketchedited = true;
                        	continue;
                        }

                        if (part.isVisionNote || part.isHelionNote) {
                            part.note.uid = sketchKey.toString() + "/" + part.note.uid;
                            sketch.notes.push(part.note);
                            continue;
                        }
                        
                        if (config.SkipRowuidInChange) {
                        	var vstring = part.vector;
                        	var partsplit = vstring.split('[')[1].split(']')[0].split(/\,/);
                        	if (partsplit[partsplit.length - 1].indexOf('{') > -1 && partsplit[partsplit.length - 1].indexOf('}') > -1)
                            	partSplitfirst = partsplit.pop();
                        	part.vector = vstring.split('[')[0] + '[' + partsplit.toString() + ']' + vstring.split(']')[1];
                        	if (orgPart) {
                        		var ostring = orgPart.vector;
                        		var opartsplit = ostring.split('[')[1].split(']')[0].split(/\,/);
                        		if (opartsplit[opartsplit.length - 1].indexOf('{') > -1 && opartsplit[opartsplit.length - 1].indexOf('}') > -1)
                            		orgPartSplitfirst = opartsplit.pop();
                        		orgPart.vector = ostring.split('[')[0] + '[' + opartsplit.toString() + ']' + ostring.split(']')[1];
                        	}
                        }
                        var vectorDeleted = orgParts && parts ? orgParts.length > parts.length : false;
                        var isChanged = false;
                        
                        if (part.brightModified) {
                        	isChanged = part.brightModified.isModified? true: false;
                        }
                        else {                        
	                        if (orgPart && orgPart.vector)
	                            isChanged = !vectorDeleted && part.vector.replace(/ /g, '') != orgPart.vector.replace(/ /g, '');
	                        else isChanged = true;
                        }
                        
                        if (config.EnableBoundary)
                            boundaryScale = (part.boundScale > boundaryScale) ? part.boundScale : boundaryScale;
                        
                        if (config.isMVP) {
                        	var partFirst = part.vector.split(':')[0], partSecond = part.vector.split(':')[1];
                            var hPartFirst = partFirst.replace(/[\[\]']+/g,'');
                            hPartFirst = hPartFirst.split(",");
                            hPartFirst[41] = part.bbScale;
                            part.vector = '[' + hPartFirst.toString() + ']:' + partSecond; 
                        }
                        
                        if (config.SkipRowuidInChange) {
							if (partSplitfirst) {
								var vstring = part.vector;
                        		var partsplit = vstring.split('[')[1].split(']')[0].split(/\,/);
                        		partsplit.push(partSplitfirst);
                        		part.vector = vstring.split('[')[0] + '[' + partsplit.toString() + ']' + vstring.split(']')[1];
							}
                        	if (orgPartSplitfirst) {
                        		var ostring = orgPart.vector;
                        		var opartsplit = ostring.split('[')[1].split(']')[0].split(/\,/);
                        		opartsplit.push(orgPartSplitfirst);
                        		orgPart.vector = ostring.split('[')[0] + '[' + opartsplit.toString() + ']' + ostring.split(']')[1];
                        	}
                        }
                        
                        var label = [], swapLookupIdName = false, swapName;
                        if(sksource.EnableSwapLookupIdName){
                        	swapLookupIdName = true;
                           	swapName = part.name;
                        }
                        var lkName = ( sketchSettings["sketchLookupQuery"] && sketchSettings["sketchLookupQuery"] != '' && query != '')? null : ( clientSettings['SketchLabelLookup'] || sketchSettings['SketchLabelLookup'] || vectorSource.LabelLookup );

                        if (lkName?.contains("/")) {
                            let slashlist = lkName.split("/");
                            for (slt in slashlist) {
                                if (lookup[slashlist[slt]]) {
                                    lkName = slashlist[slt]; break;
                                }
                            }
                        }

                        var fLookUp = (window.parent && window.parent.LookupMap) ? window.parent.LookupMap[vectorSource.LabelLookup] : null;
	                    var isLookUpQueryField = fLookUp ? fLookUp.LookupTable == '$QUERY$' : false;
                        var labelValue = vectorSource.GetLabelValueFromField ? pi[vectorSource.LabelField] : ( sksource.AssignLookUpNameAsLabel ? part[vectorSource.LabelTarget] : ( (config.ApexEditSketchLabel && sketchSettings['ApexEditSketchLabel'] == '1' && part.code) ? part.code: part.label ) );
                        var useLookUpNameAsValue = ( sksource.AssignLookUpNameAsLabel || (config.ApexEditSketchLabel && sketchSettings['ApexEditSketchLabel'] == '1') ) ? true: false;
                        var labelNameValue = ( useLookUpNameAsValue ) ? part.label : part[vectorSource.LabelTarget];
                        var desc = lkName && lookup[lkName] ? (swapLookupIdName ? lookup[lkName][swapName] : lookup[lkName][labelValue]) : null
                        var clrLabelValue = sksource.splitByDelimiter && labelValue.indexOf(sksource.splitByDelimiter) > -1? labelValue.split(sksource.splitByDelimiter)[0]: labelValue;
                        var clrdesc = lkName && lookup[lkName] ? (swapLookupIdName ? lookup[lkName][swapName] : lookup[lkName][clrLabelValue]) : null;
                        var lookDesc = ( !sksource.DoNotShowLabelDescription && sketchSettings["DoNotShowLabelDescriptionSketch"] != '1' && clientSettings["DoNotShowLabelDescriptionSketch"] != "1") ? desc : null
                        var labelDetails = {}, BasementFunctionality = false, mvpDisplayLabel = '';
                        labelDetails = part.mvpLabelDetails;
                        if(part.sketchType == 'outBuilding'){
                        	labelValue = labelValue.replace('&lt;','<');
                    		labelValue = labelValue.replace('&gt;','>');
                    		mvpDisplayLabel = part.mvpDisplayLabel;
                    	}
                    	var _hlfs = vectorSource.HideLabelFromShow? vectorSource.HideLabelFromShow: ( (config.ApexEditSketchLabel && sketchSettings['ApexEditSketchLabel'] == '1') ? true: false );
                        label.push({ Field: vectorSource.LabelField, Value: labelValue, Description: lookDesc, IsEdited: isChanged, hiddenFromEdit: vectorSource.HideLabelFromEdit, hiddenFromShow: _hlfs, lookup: lkName, Caption: vectorSource.labelCaption, NameValue: labelNameValue, ShowCurrentValueOnly: vectorSource.ShowCurrentValueOnly, lookUpQuery: sksource.LookUpQuery, splitByDelimiter: sksource.splitByDelimiter, Target: vectorSource.LabelTarget, UseLookUpNameAsValue: useLookUpNameAsValue, IsLargeLookup: sksource.IsLargeLookup, IsRequired: labelRequiredTrue, colorCode: ( clrdesc && clrdesc.color ? clrdesc.color : null ), labelDetails: labelDetails, IsSwapLookupIdName: swapLookupIdName, showLabelDescriptionOnly: isLookUpQueryField, EnableFieldValidation: vectorSource.EnableFieldValidation  })
                        if ( vectorSource.ExtraLabelFields && ( !config.ApexEditSketchLabel || ( config.ApexEditSketchLabel && sketchSettings['ApexEditSketchLabel'] == '1' ) ) ) {
                            vectorSource.ExtraLabelFields.forEach(function (item) {
                                var value = part[item.Target]
                                if (item.ValueRegx) {
                                    value = value.match(item.ValueRegx)
                                    if (value && value[0]) value = value[0]
                                }
                                let lknm = item.LookUp;
                                if (lknm?.contains("/")) {
                                    let slashlist = lknm.split("/");
                                    for (slt in slashlist) {
                                        if (lookup[slashlist[slt]]) {
                                            lknm = slashlist[slt]; break;
                                        }
                                    }
                                }

                                fLookUp = (window.parent && window.parent.LookupMap) ? window.parent.LookupMap[lknm]: null;
                                isLookUpQueryField = fLookUp ? fLookUp.LookupTable == '$QUERY$' : false;
                                label.push({ Field: item.Name, Value: value, lookup: lknm, Target: item.Target, Caption: item.Caption, hiddenFromShow: item.HideLabelFromShow, DoNotShowLabelDescription: item.DoNotShowLabelDescription, IsLargeLookup: item.IsLargeLookup, ValidationRegx: item.ValidationRegx, requiredAny: item.requiredAny, showLabelDescriptionOnly: isLookUpQueryField, EnableFieldValidation: item.EnableFieldValidation })
                            })
                        }
                        if (isChanged)
                            sketchedited = true;
                        var hideArea_value = false;
						if(part.isUnSketchedArea) 
							hideArea_value = true;
                        var sk = {
                            uid: sketchKey.toString() + "/" + pcount,
                            label: label,
                            labelPosition: part.labelPosition,
                            referenceIds: part.referenceIds,
                            isChanged: showPrevious ? false : isChanged,
                            vector: part.vector,
                            mvpData: part.mvpData,
                            AreaLabelPosition: part.AreaLabelPosition,
                            sketchType: part.sketchType,
                            hideAreaValue:hideArea_value,
                            isUnSketchedArea: part.isUnSketchedArea,
                            Disable_CopyVector_OI: part.Disable_CopyVector_OI,
                            otherValues: part.otherValues,
                            LabelDelimiter: sksource.LabelDelimiter === undefined ? '/' : '',
                            vectorConfig: vectorSource,
                            BasementFunctionality: part.BasementFunctionality,
                            doNotAllowDeleteFirstVector: doNotAllowDeleteFirstVector,
                            maxVectorLimit: maxVectorLimit,
                            mvpDisplayLabel: mvpDisplayLabel
                        }

                        var hideSketch = (sksource.HideNullSketches && !sk.vector) || false;
                        if (!hideSketch) sketch.sketches.push(sk);
                    }
                    if (config.EnableBoundary) {
                        sketch.boundaryScale = (boundaryScale != 0 && boundaryScale != '') ? boundaryScale : 80;
                    }
                    
                    if(config.NoteMaxLength) {
	                	sketch.maxNotelen = config.NoteMaxLength;
	                }
                    
                    for (var x in filteredfreeFormLineEntries) {
                        var LineEntries = filteredfreeFormLineEntries[x];
                        var label = [], Line_Type = false;
                        if (LineEntries['LinePattern'].contains('Dashed'))
                            Line_Type = true;
                        var sk = {
                            uid: LineEntries['KeyCode'],
                            label: label,
                            otherValues: [],
                            vector: '[-1,-1]:' + LineEntries['Path'],
                            referenceIds: '-1,-1',
                            isChanged: false,
                            isFreeFormLineEntries: true,
                            vectorConfig: vectorSource,
                            PageNum: LineEntries['PageNum'],
                            Width: LineEntries['Width'],
                            Color: LineEntries['Color'],
                            LinePattern: LineEntries['LinePattern'],
                            Line_Type: Line_Type,
                            KeyCode: LineEntries['KeyCode'],
                            hideAreaValue: true

                        }
                        var hideSketch = (sksource.HideNullSketches && !sk.vector) || false;
                        if (!hideSketch) sketch.sketches.push(sk);
                    }
                    for (var x in filteredFreeFormTextLabels) {
                        var note = filteredFreeFormTextLabels[x];
                        var notePosition = note["Position"].split(",");
                        var n = {
                            uid: note['KeyCode'],
                            text: (note['Text'] || '').replace(/&lt;/g, '').replace(/&gt;/g, ''),
                            x: parseInt(notePosition[0] || 0) * (1),
                            y: parseInt(notePosition[1] || 0) * (1),
                            //lx: parseInt( note[sksource.NotesSource.LineXField] || 0 ) * ( sksource.NotesSource.ScaleFactor || 1 ),
                            //ly: parseInt( note[sksource.NotesSource.LineYField] || 0 ) * ( sksource.NotesSource.ScaleFactor || 1 ),
                            Bold: note['Bold'],
                            Color: note['Color'],
                            FontFace: note['FontFace'],
                            FontSize: note['FontSize'],
                            Italic: note['Italic'],
                            PageNum: note['PageNum'],
                            Rotation: note['Rotation'],
                            KeyCode: note['KeyCode']
                        }
                        sketch.notes.push(n);
                    }

                } else {

                    var filteredDetails = sketchVectorTable && sketchVectorTable.filter(function (ivx) { if (ivx.ParentROWUID) return (ivx.ParentROWUID == pi.ROWUID && eval(vectorSource.filter || true)); else if (pi && pi.constructor.name == "Parcel") { return (ivx.CC_ParcelId == pi.Id); } else return (eval('ivx.' + vectorSource.ConnectingFields[0]) == eval('pi.' + sksource.SketchSource.KeyFields[0]) && eval(vectorSource.filter || true)); });
                    if( config.IsProvalConfig && ( vectorSource.Table == 'sktsegment' || vectorSource.Table == 'ccv_SktSegment' ) )
                        filteredDetails = filteredDetails.sort(function(x,y){ return x.CC_segment_id - y.CC_segment_id});
                    if (config.EnableBoundary && config.IsProvalConfig && config.ConfigTables) {
                        var ConfigTables = config.ConfigTables;
                        var boundaryScales = ( showPrevious ? 250 : ( ( pi[ConfigTables.SktHeader] && pi[ConfigTables.SktHeader][0] && pi[ConfigTables.SktHeader][0]['scale'] )? pi[ConfigTables.SktHeader][0]['scale'] : 0 ) );
                        sketch.boundaryScale = (boundaryScales != 0 && boundaryScales != '') ? boundaryScales : 100;
                    }
                    if (config.OutBuildingDefinition && vectorSource.type == 'outBuilding' ) {
                    	for (var x in filteredDetails) {
                        	var id = filteredDetails[x];
                        	//if(showPrevious) id = filteredDetails[x].Original;
                            var outB = config.OutBuildingDefinition( id, vectorSource, {sksource}, null, showPrevious);
                            if(!_.isEmpty(outB)) {
                            	var outBd = outB.pop();
                            	if (outBd.isChanged)
                            		sketchedited = true;
                            	sketch.sketches.push(outBd);
                            }
                        }
                    }
                    else if (config.SketchDefinition) {
                        var sks = config.SketchDefinition(filteredDetails, isChanged);
                        sks.forEach(function(sk){
                        	if (sk.isChanged && !showPrevious)
                            	sketchedited = true; 
                        	sketch.sketches.push(sk);
                        });
                    }
                    else
                        for (var x in filteredDetails) {
                            var id = filteredDetails[x];
                            if(!config.VectorDefinition){
								if(showPrevious) id = filteredDetails[x].Original;
                        	}
                            var org = id && id.Original ? id.Original : [];
                            var readonly = false
                            var vectorKey = id[sketchVectorKeyField];
                            var vector = '';
                            var isChanged = false;
                            var labelValue, field, index, vector, vectorDetail, labelPosition, isChanged, areaFieldValue = null, noVectorSegment = false, areaUnit = null, Line_Type = false, isUnSketchedTrueArea = false, perimeterFieldValue = null, BasementFunctionality = false, yearValueToDisplay;
                            if (config.VectorDefinition) {
                                if(showPrevious){
									if(id.CC_RecordStatus && id.CC_RecordStatus == 'I')
										continue;
								}
                            	vector = config.VectorDefinition( id, vectorSource, processor,showPrevious );
                           		vectorDetail = config.VectorDefinition( id, vectorSource, null, showPrevious);
                                vector = vectorDetail.vector_string;
                                isChanged = vectorDetail.isChanged;
                            	noVectorSegment = vectorDetail.noVectorSegment;
                            	Line_Type = vectorDetail.Line_Type;
	                        	isUnSketchedTrueArea = vectorDetail.isUnSketchedTrueArea;
	                        	perimeterFieldValue = vectorDetail.perimeterFieldValue ? vectorDetail.perimeterFieldValue: null
                            	areaFieldValue = vectorDetail.areaFieldValue ? vectorDetail.areaFieldValue : null;
                            	areaUnit = vectorDetail.areaUnit ? vectorDetail.areaUnit : null;
                            }
                            else {
                                vector = eval('id.' + vectorSource.CommandField);
                                labelPosition = eval('id.' + vectorSource.LabelCommandField);
                                if (sksource.EnableMarkedAreaDrawing) 
                                 	areaFieldValue = eval( 'id.' + vectorSource.AreaField); 
                            	if(org && config.IsVectorStartCommandField)
	                                isChanged= (eval('id.' + vectorSource.CommandField) != eval('org.' + vectorSource.CommandField)) || (eval('id.' + vectorSource.LabelField) != eval('org.' + vectorSource.LabelField)) || (eval('id.' + vectorSource.VectorStartCommandField) != eval('org.' + vectorSource.VectorStartCommandField))
                            	else if ( org )
                                	isChanged = eval( 'id.' + vectorSource.CommandField ) != eval( 'org.' + vectorSource.CommandField ) || eval( 'id.' + vectorSource.LabelField ) != eval( 'org.' + vectorSource.LabelField );
                            }
                            if (isChanged && !showPrevious)
                                sketchedited = true;
                            if (getDataField)
                                var field = getDataField(vectorSource.LabelField, vectorSource.Table)
                            var index = sketchVectorTableAll.indexOf(id)
                            if (field && field.ReadOnlyExpression)
                                readonly = activeParcel.EvalValue(vectorSource.Table + '[' + index + ']', field.ReadOnlyExpression)
                            
                            var labelValue
                            if ( config.LabelPositionDefinition )
                            	labelPosition = config.LabelPositionDefinition( id, vectorSource, showPrevious );
                        	var labelDetails = {};
                        	if ( config.LabelDefinition )
                        	{
                            	labelDetails = config.LabelDefinition( id, vectorSource, showPrevious );
                            	labelValue = labelDetails.labelValue;
                            	BasementFunctionality = labelDetails.BasementFunctionality ? labelDetails.BasementFunctionality : false
                            	isChanged = isChanged || labelDetails.isChanged;
                            	//if ( labelValue == '' ) labelPosition = null;
                        	}
                            else labelValue = eval('id.' + vectorSource.LabelField)
                            
                            if(sksource.ShowYearBuilt && clientSettings['ShowYearValue'] == "1" && clientSettings['YearValueToDisplay']){
	                            var yearFieldValue = clientSettings['YearValueToDisplay'];
	                            var yearField = getDataField? getDataField( yearFieldValue, vectorSource.Table): null;
	                            if(yearField)
	                            	yearValueToDisplay = eval( 'id.' + yearFieldValue);
	                        }
                            
                            let lblLookup = vectorSource.LabelLookup;
	                        if (vectorSource.LabelLookupSelector && getDataField) {
	                        	let lblkField = getDataField(vectorSource.LabelLookupSelector.FieldName, vectorSource.LabelLookupSelector.Table);
	                        	lblLookup = lblkField? (lookup['#FIELD-'+ lblkField.Id]? '#FIELD-'+ lblkField.Id: lblLookup): lblLookup;
	                        }

                            if (lblLookup?.contains("/")) {
                                let slashlist = lblLookup.split("/");
                                for (slt in slashlist) {
                                    if (lookup[slashlist[slt]]) {
                                        lblLookup = slashlist[slt]; break;
                                    }
                                }
                            }

                            var fLookUp = ( window.parent && window.parent.LookupMap )? window.parent.LookupMap[lblLookup]: null;
                        	var isLookUpQueryField = fLookUp ? fLookUp.LookupTable == '$QUERY$' : false;
                        	var desc = ( sketchSettings["sketchLookupQuery"] && sketchSettings["sketchLookupQuery"] != '' && query != '')? null: (lblLookup && lookup[lblLookup] ? lookup[lblLookup][labelValue] : null);
                        	var lookDesc = ( !sksource.DoNotShowLabelDescription && sketchSettings["DoNotShowLabelDescriptionSketch"] != '1' && clientSettings["DoNotShowLabelDescriptionSketch"] != "1" )? desc : null;
                        	var label = []; 
                        	var _hlfs = vectorSource.HideLabelFromShow? vectorSource.HideLabelFromShow: ( (config.ApexEditSketchLabel && sketchSettings['ApexEditSketchLabel'] == '1') ? true: false );
                        	label.push( { Field: vectorSource.LabelField, Value: labelValue, Description: lookDesc, IsEdited: false, lookup: lblLookup, labelDetails:labelDetails, ReadOnly: readonly, Caption: vectorSource.labelCaption, IsLargeLookup: sksource.IsLargeLookup, IsRequired: labelRequiredTrue, showLabelDescriptionOnly: isLookUpQueryField, hiddenFromShow: _hlfs, colorCode: ( desc && desc.color ? desc.color : null ), EnableFieldValidation: vectorSource.EnableFieldValidation } )
                            if ( vectorSource.ExtraLabelFields && ( !config.ApexEditSketchLabel || ( config.ApexEditSketchLabel && sketchSettings['ApexEditSketchLabel'] == '1' ) ) ) {
                                vectorSource.ExtraLabelFields.forEach(function (item) {
                                    if (getDataField) field = getDataField(item.Name, vectorSource.Table);
                                    var val = eval('id.' + item.Name)
                                    let lknm = item.LookUp;
                                    if (lknm?.contains("/")) {
                                        let slashlist = lknm.split("/");
                                        for (slt in slashlist) {
                                            if (lookup[slashlist[slt]]) {
                                                lknm = slashlist[slt]; break;
                                            }
                                        }
                                    }
                                    var t = lknm && lookup[lknm] ? lookup[lknm][val] : val
                                    if (field && field.ReadOnlyExpression)
                                        readonly = activeParcel.EvalValue(vectorSource.Table + '[' + index + ']', field.ReadOnlyExpression)
                                    fLookUp = (window.parent && window.parent.LookupMap) ? window.parent.LookupMap[lknm]: null;
                                	isLookUpQueryField = fLookUp ? fLookUp.LookupTable == '$QUERY$' : false;
                                    label.push({ Field: item.Name, Value: val, Description: t, lookup: lknm, ReadOnly: readonly, Caption: item.Caption, showLabelDescriptionOnly: isLookUpQueryField, hiddenFromShow: item.HideLabelFromShow, EnableFieldValidation: item.EnableFieldValidation })
                                })
                            }
                            var sk = {
                                uid: vectorKey.toString(),
                                label: label,
                                labelPosition: labelPosition,
                                vector: vector,
                                isChanged: showPrevious ? false : isChanged,
                                vectorConfig: vectorSource,
                                noVectorSegment: noVectorSegment,
                                Line_Type: Line_Type,
                                areaFieldValue: areaFieldValue,
                                isUnSketchedTrueArea: isUnSketchedTrueArea,
                                perimeterFieldValue: perimeterFieldValue,
                                BasementFunctionality: BasementFunctionality,
                                yearValueToDisplay: yearValueToDisplay,
                                doNotAllowDeleteFirstVector: doNotAllowDeleteFirstVector,
                                maxVectorLimit: maxVectorLimit,
                                vectorStart: eval('id.' + vectorSource.VectorStartCommandField)
                            }
							
							if ( vectorSource.FootModeField ) {
                            	var footMode = id[vectorSource.FootModeField]
                            	if ( footMode == true || footMode == 'true' )
                                	sk.footMode = footMode;
                       		}
                       		
                            if (vectorSource.IdField) {
                                sk.rowId = eval('id.' + vectorSource.IdField);
                            }

                            if (processor.decodeSketch) {
                                sk.vector = processor.decodeSketch(sk.vector);
                            }
                            sketch.index = parseInt( i );
                        	if ( vectorSource.vectorPagingField ){
                            	var page = id[vectorSource.vectorPagingField]
                            	if ( page == true || page == 'true' ){
                                	sk.page = page;
                                	var s = _.clone( sketch )
                                	s.sketches = [];
                                	s.sketches.push( sk )
                                	sketches.push( s );
                                	isPages = true;
                                	s.isNewPage = true;
                                	s.index = ( sketches.length )
                                	continue;
                            	}
                        	}
                            var hideSketch = (sksource.HideNullSketches && !sk.vector) || false;
                            if (!hideSketch) sketch.sketches.push(sk);
                        }
                }
            }
            
            if(config.IsNoteMaxLength){
               if(sksource.NotesSource){
                	var maxNotelen, noteField = sksource.NotesSource.TextField, sNoteTable = sksource.NotesSource.Table;
                	maxNotelen = (getDataField && getDataField(noteField,sNoteTable) && getDataField(noteField,sNoteTable).MaxLength && getDataField(noteField,sNoteTable).MaxLength != "" && getDataField(noteField,sNoteTable).MaxLength != '0') ? getDataField(noteField,sNoteTable).MaxLength: "34";
                	maxNotelen = maxNotelen.toString();
                	sketch.maxNotelen = maxNotelen;
                }
            }
            else if (sksource.NotesSource && sksource.NotesSource.Table && sksource.NotesSource.TextField ) {
				let noteFld = getDataField && getDataField(sksource.NotesSource.TextField, sksource.NotesSource.Table)
				if ( noteFld && noteFld.MaxLength)
					sketch.maxNotelen = noteFld.MaxLength;
			}
            
            if (config.NoteDefinition)
                sketch.notes = config.NoteDefinition(pi);
            else if (notesTable && notesTable.length > 0) {
                var cf = sksource.NotesSource.ConnectingFields;
                var filterCondition = "1==1";
                cf.forEach(function (f) {
                    filterCondition += " && ivx." + f + " == pi." + f
                });
                var filteredNotes = notesTable.filter(function (ivx) { if (ivx.ParentROWUID) return (ivx.ParentROWUID == pi.ROWUID); else return (eval(filterCondition)) });
                for (var x in filteredNotes) {
                    var note = filteredNotes[x];
                    var n = {
                        uid: note[sksource.NotesSource.KeyField || 'ROWUID'],
                        text: ( note[sksource.NotesSource.TextField] || '' ).replace(/&lt;/g,'').replace(/&gt;/g,''),
                        x: parseInt(note[sksource.NotesSource.PositionXField]) * (sksource.NotesSource.ScaleFactor || 1),
                        y: parseInt(note[sksource.NotesSource.PositionYField]) * (sksource.NotesSource.ScaleFactor || 1),
                        lx: parseInt(note[sksource.NotesSource.LineXField]) * (sksource.NotesSource.ScaleFactor || 1),
                        ly: parseInt(note[sksource.NotesSource.LineYField]) * (sksource.NotesSource.ScaleFactor || 1)
                    }
                    sketch.notes.push(n);
                }
            }

            if ((sketch.config.AllowSegmentAddition || sketch.config.AllowSegmentAdditionByCustomButtons) || sketch.sketches.length > 0) {
                sketches.push(sketch);
            }
        }
    }
    
	if ( isPages && sketches.length > 1 ) {
        sketches = sketches.sort( function ( x, y ) { return x.index - y.index } )
        sketches.forEach( function ( sk, i )
        {
            sk.label = sk.label + ' - ' + ( i + 1 );
        } )
    }
    
    return sketches;
}

var clr = 'white';
var selectedSketch = null, selectedSection = null;
top.getSketchSegments = getSketchSegments
function previewSketch(colorCode, type, callback, selSketch, selSection, UpdateSketchDropDown) {
    //console.log(type);
    if (!colorCode) colorCode = clr; clr = colorCode;
    sketchRenderer.scale = 1
    sketchRenderer.resetOrigin();
    var sketches = [];
    if (type == 1) sketches = getSketchSegments();
    sketches = sketches? sketches : [];
    sketchRenderer.formatter = ccma.Sketching.SketchFormatter
    if(!sketchRenderer.formatter)
        sketchRenderer.formatter = CAMACloud.Sketching.Formatters.TASketch; // if config or formatter not set then set TASKetch as default
        
    if( clientSettings["sketchOriginPosition"] || sketchSettings["sketchOriginPosition"] ) //set sketchOriginPosition based on clientsettings
    	sketchRenderer.formatter.originPosition = clientSettings["sketchOriginPosition"] || sketchSettings["sketchOriginPosition"];
    if( sketchSettings["RoundToDecimalPlace"] == 'Half Foot' || sketchSettings["RoundToDecimalPlace"] == 'Quarter Foot' )
		sketchSettings.SegmentRoundingFactor = sketchSettings["RoundToDecimalPlace"] == 'Half Foot' ? 2 : 4;
    sketchRenderer.showOrigin = false;
    sketchRenderer.viewOnly = true;
    if (type == 1) {
        try {
            sketchRenderer.open(sketches, null, true, null, null, null, colorCode, "black");
        } catch (ex) { sketchRenderer.sketches = []; }
        top.$('.sketch-select,.section-select').empty().append('<option></option>').empty()
    }
    var sk = sketchRenderer.sketches
    if (type == 1) {
    	top.$('#showSketchOptions').empty();
        sk.forEach(function (s, i) {
            if (s.vectors.length > 0) {
            	if(clientSettings.SVSketchHide){
                    var condition = clientSettings.SVSketchHide;
           		    var tab = s.config.SketchSource.Table;
            	    var si = tab + '[' + i + ']';
            	    if ((activeParcel.EvalValue(si,condition,null,tab,true))==true) {
            	        var skRowuid = (s.parentRow && s.parentRow.ROWUID)? s.parentRow.ROWUID: s.sid;
            	        var skPrefix = (s.config && s.config.SketchLabelPrefixInSv)? s.config.SketchLabelPrefixInSv: '';
                        top.$('.sketch-select').append($('<option>', { value: i, text: s.label, skrowuid: skRowuid, skprefix: skPrefix }));
                        top.$('#showSketchOptions').append($('<span>', { value: i, text: s.label, skrowuid: skRowuid, skprefix: skPrefix }));
                        if( UpdateSketchDropDown ) {
                	        top.sketchDropDownCount = top.sketchDropDownCount + 1;
                	        top.sketchDropDownChange.push( {Id: (i.toString()), Name: s.label} );
                        }
                    }
            }
            else{
            	var skRowuid = (s.parentRow && s.parentRow.ROWUID)? s.parentRow.ROWUID: s.sid;
				var skPrefix = (s.config && s.config.SketchLabelPrefixInSv)? s.config.SketchLabelPrefixInSv: '';
                top.$('.sketch-select').append($('<option>', { value: i, text: s.label, skrowuid: skRowuid, skprefix: skPrefix }));
				top.$('#showSketchOptions').append($('<span>', { value: i, text: s.label, skrowuid: skRowuid, skprefix: skPrefix }));
				if( UpdateSketchDropDown ) {
						top.sketchDropDownCount = top.sketchDropDownCount + 1;
						top.sketchDropDownChange.push( {Id: (i.toString()), Name: s.label} );
				}
             }
            }
        })
        top.$('.sketch-select').val(top.$('#sketchid').val());
        if( UpdateSketchDropDown )
        	top.sketchDropDownChange = $.grep(top.sketchDropDownChange, function(data, index) { return data.Id != top.$('#sketchid').val() });
    }
    if (selSketch !== undefined) {
        selectedSketch = selSketch;
    } else {
        selectedSketch = (mapFlag) ? parent.window.opener.$('.sketch-select').val() : top.$('.sketch-select').val();
    }
	
	var _ah = false, _pExists = false;
	if( selSketch || selSection )
		_pExists = true;
		
    if (selectedSketch > -1 && type == 1 && selectedSketch)
        for (var key in sk[selectedSketch].sections) {
            top.$('.section-select').append($('<option>', { value: key, text: key }));
        }
    selectedSection = selSection !== undefined ? selSection : (mapFlag) ? parent.window.opener.$('.section-select').val() : top.$('.section-select').val();
    var selectedSectionIndex = selectedSketch && sk[selectedSketch].sections ? sk[selectedSketch].sections[selectedSection] : null;
    var ab = selectedSectionIndex ? sectionbound(selectedSectionIndex) : { xmax: 0, ymax: 0, xmin: 0, ymin: 0 };

    //newchange start
    var url2;
    sketchRenderer.labelColorCode = 'black'
    sketchRenderer.lineColor = 'black'
    var scale = .75
    sketchRenderer.scale = scale
    sketchRenderer.resizeCanvas((ab.xmax - ab.xmin) * 10 + 400, (ab.ymax - ab.ymin) * 10 + 95);
    sketchRenderer.pan(sketchRenderer.formatter.originPosition == "topRight" ? applyRoundToOrigin1(-ab.xmax * 10 * scale + 15) : applyRoundToOrigin1(-ab.xmin * 10 * scale + 15), (sketchRenderer.formatter.originPosition == "topRight" || sketchRenderer.formatter.originPosition == "topLeft") ? applyRoundToOrigin1(-ab.ymax * 10 * scale + 15) : applyRoundToOrigin1(-ab.ymin * 10 * scale + 15));
    sketchRenderer.clearCanvas();
    if (selectedSketch && selectedSection && selectedSketch > -1 && selectedSection > -1 && sk[selectedSketch].sections[selectedSection]) {
        sk[selectedSketch].sections[selectedSection].forEach(function (vt) {
            if (!vt.isClosed && vt.isChanged)
                sketchRenderer.lineColor = '#FFA62F' ;  //currently all segments linecolor set to black but if closed and changed then only change color to yellow.If not closed segment but changed segment then color shows black.Actually need yellow in this case.
            else
                sketchRenderer.lineColor = 'black';
            if( (!_pExists) && (!_ah) && (vt.startNode) )
            	_ah = true;
            vt.render(true);
        })
    } else
        url2 = ""
	if(sketchRenderer.displayNotes) sketchRenderer && sketchRenderer.sketches[selectedSketch] && sketchRenderer.sketches[selectedSketch].renderNotes();
    url2 = sketchRenderer.toDataURL();
    activeParcel.SketchPreview = url2;
    top.$('.sketch-big').css({ 'background-image': 'url(' + url2 + ')' });
    // end
    
    if( !_pExists ) {
    	if( typeof adjustHandle != 'undefined' )
			adjustHandle.visible = _ah;
	    else if( mapFrame  && mapFrame.contentWindow && mapFrame.contentWindow.adjustHandle )
	        mapFrame.contentWindow.adjustHandle.visible = _ah; 
    }
    
    scale = 1
    var sHeight = (ab.ymax - ab.ymin) * 10;
    var sWidth = (ab.xmax - ab.xmin) * 10;
    if (sHeight * 1.1 > 300) {
        scale = 3000 / sHeight * 0.9;
    }

    var maxHeight = 300 * 0.9;
    var maxWidth = 300 * 0.9;
    var aspectRatio = parseFloat(sWidth) / parseFloat(sHeight);
    var width, height, sWidthHeight = false;

    if ((sWidth < maxWidth) && (sHeight < maxHeight)) {
        scale = 1;
        width = maxWidth;
        sWidthHeight = true; //sketch height and width less than max height width then set scale = 1. SV_322
    } else if (aspectRatio > 1) {
        width = maxWidth;
        height = (width / aspectRatio);
        if (height > maxHeight) {
            height = maxHeight;
            width = (height * aspectRatio);
        }
    } else {
        height = maxHeight;
        width = (height * aspectRatio);
        if (width > maxWidth) {
            width = maxWidth;
            height = (width / aspectRatio);
        }
    }
	if( !sWidthHeight )
    	scale = parseFloat((width / sWidth).toFixed(2))
    sketchRenderer.scale = scale
    sketchRenderer.resizeCanvas(400, 400);
    sketchRenderer.pan(sketchRenderer.formatter.originPosition == "topRight" ? applyRoundToOrigin1(-ab.xmax * 10 * scale - 45) : applyRoundToOrigin1(-ab.xmin * 10 * scale - 20), (sketchRenderer.formatter.originPosition == "topRight" || sketchRenderer.formatter.originPosition == "topLeft") ? applyRoundToOrigin1(-ab.ymax * 10 * scale - 45) : applyRoundToOrigin1(-ab.ymin * 10 * scale - 20));
    sketchRenderer.clearCanvas();
    if (selectedSketch && selectedSection && selectedSketch > -1 && selectedSection > -1 && sk[selectedSketch].sections[selectedSection]) {
        sk[selectedSketch].sections[selectedSection].forEach(function (vt) {
            if (!vt.isClosed && vt.isChanged)
                sketchRenderer.lineColor = '#FFA62F'
            else
                sketchRenderer.lineColor = 'black'
            vt.render();
        })
    } else
        var url = ""
    url = sketchRenderer.toDataURL();
    if (type != 1)
        $('#sketch').attr('src', url);

    if (!type)
        setPositionOverlayIfDataExist(true)


    if (callback) callback(url);

}

function applyRoundToOrigin1(v) {
    if (sketchRenderer?.config?.formatter == 'Vision') v = Math.ceil(v);
    return v;
}

function sectionbound(sk) {
    var x1 = 0, x2 = 0, y1 = 0, y2 = 0;
    for (x in sk) {
        var b = new PointX(0, 0);
        var v = sk[x];
        var sn = v.startNode;
        while (sn != null) {
            x1 = Math.min(x1, isNaN(sn.p.x) ? 0 : sn.p.x);
            x2 = Math.max(x2, isNaN(sn.p.x) ? 0 : sn.p.x);
            y1 = Math.min(y1, isNaN(sn.p.y) ? 0 : sn.p.y);
            y2 = Math.max(y2, isNaN(sn.p.y) ? 0 : sn.p.y);

            if (sn.isArc) {
                var mp = sn.midpoint;
                x1 = Math.min(x1, isNaN(mp.x) ? 0 : mp.x);
                x2 = Math.max(x2, isNaN(mp.x) ? 0 : mp.x);
                y1 = Math.min(y1, isNaN(mp.y) ? 0 : mp.y);
                y2 = Math.max(y2, isNaN(mp.y) ? 0 : mp.y);
            }

            sn = sn.nextNode;
        }
    }
    return { xmax: x2, ymax: y2, xmin: x1, ymin: y1 };
}

var WyandottVectorDefinition = function (data, isChanged) {
    var vector = '', vectorStart = true, sketches = [], vectorSource = ccma.Sketching.Config.sources[0].VectorSource[0], vectorCounter = 0, startPos = '';

    var convertToSketch = function (d, type, isAngled, isInverted) {
        var updownData = isInverted ? parseInt(d['CASKLDU' + type]) * -1 : parseInt(d['CASKLDU' + type]);
        var leftrightData = isInverted ? parseInt(d['CASKLRL' + type]) * -1 : parseInt(d['CASKLRL' + type]);

        return ' ' + (leftrightData > 0 ? 'R' + leftrightData : 'L' + leftrightData * -1) + (isAngled ? '/' : ' ') + (updownData > 0 ? 'D' + updownData : 'U' + updownData * -1);
    }

    data.map(function (d) { if (d.CASKLRECNUM) d.CASKLRECNUM = parseInt(d.CASKLRECNUM); })

    data = _.sortBy(data, 'CASKLRECNUM')
    data.forEach(function (d) {
        ++vectorCounter;
        if (d.CASKLDIAG1 != '+' && d.CASKLDIAG1 != '-') {
            if (vectorStart) {
                startPos = convertToSketch(d, 1);
                vector += ':' + startPos + ' S '
                vectorStart = false;
            }
            else vector += convertToSketch(d, 1);
            vector += convertToSketch(d, 2);
        }
        else {
            vector += convertToSketch(d, 1, true);
            vector += convertToSketch(d, 2);
        }
        if (d.CASKLSQFT != '0') {

            if (vectorCounter == 1) vector += convertToSketch(d, 2, false, true);

            sketches.push({
                uid: (d.CASKLPARCEL ? d.CASKLPARCEL.toString() : ''),
                label: d.CASKLCONS,//[{ Field: 'CASKLCONS', Value: d.CASKLCONS, Description: null, IsEdited: isChanged, hiddenFromEdit: vectorSource.false, hiddenFromShow: false, lookup: null, Caption: vectorSource.labelCaption, ShowCurrentValueOnly: vectorSource.ShowCurrentValueOnly, lookUpQuery: null, splitByDelimiter: null, Target: vectorSource.LabelTarget, UseLookUpNameAsValue: null, IsLargeLookup: false }],
                vector: d.CASKLCONS + '[-1,-1,' + startPos + ']' + vector,
                labelPosition: null,
                isChanged: isChanged,
                vectorConfig: vectorSource
            });
            vectorStart = true;
            vector = '';
            startPos = '';
            vectorCounter = 0;
        }
    });
    console.log(sketches);
    return sketches;
}
function clearVectorlabelPosition() {
    if (window.opener)
        window.opener.vectorLabelPositions = [];
    vectorLabelPositions = [];
}

function mapSave() {
	var newCurrentOverlayPosition = windmapFrame.contentWindow.isNewSketchDataFormat? windmapFrame.contentWindow.currentOverlayPosition: {};
    if (!windmapFrame.contentWindow.isNewSketchDataFormat) {
    	parent.window.opener.$('.sketch-select option').each(function(index, opt) {
    		var optval = (opt.value || opt.value == 0)? opt.value.toString(): opt.value;
    		var skRowId = $(opt).attr('skrowuid');
			var skPrefix = $(opt).attr('skprefix');
			skPrefix = skPrefix? skPrefix: '';
    		if (windmapFrame.contentWindow.currentOverlayPosition["sketch-" + optval]) { 
    			newCurrentOverlayPosition["sketch-" + skRowId + skPrefix] = windmapFrame.contentWindow.currentOverlayPosition["sketch-" + optval];
    		}
		});
    }
    
    var mapData = {
        Description: 1,
        pid: parent.window.opener.activeParcel.Id,
        notes: parent.window.opener.$('#txtRevNote').val(),
        flag: '',
        szoom: windmapFrame.contentWindow.scale,
        mzoom: windmapFrame.contentWindow.mapZoom,
        rotate: windmapFrame.contentWindow.degree,
        salat: windmapFrame.contentWindow.getParcelCenter().lat(),
        salng: windmapFrame.contentWindow.getParcelCenter().lng(),
        status: '',
        sketchData: JSON.stringify(newCurrentOverlayPosition),
        sketchLatLng: windmapFrame.contentWindow.sketchLatLng,
        IsSketchChanged: windmapFrame.contentWindow.isSketchChanged
    };
    if (parent.window.opener.activeParcel.SketchRotate != windmapFrame.contentWindow.degree || windmapFrame.contentWindow.isSketchChanged) {
        parent.window.opener.$('#sketchid').val(parent.window.opener.$('.sketch-select').val());
        parent.window.opener.saveReview("Details Saved Sucessfully.", 1, mapData);
        windmapFrame.contentWindow.isSketchChanged = false;
    }
}

var sketchSorting = function (skRecord, stExps, stable) {
    if (!stExps || !skRecord || skRecord && skRecord.length == 0)
        return skRecord;
    var splitstExp = stExps.split(',');
    var sortResultRecord = [];
    splitstExp.forEach(function(splitExp) {
    	var stExp = splitExp.trim();
    	var stField = stExp.split(" ")[0];
	    var fld = getDataField? getDataField(stField, stable): null;
	    var fldType = fld? fld.InputType: (stField == 'ROWUID'? '2': null);
	    var stOrder = stExp.split(" ")[1] &&  stExp.split(" ")[1] == 'DESC' ? 'DESC': 'ASC';
	    sortResultRecord = skRecord.sort(function(a, b) { 
	                                var val1 = a[stField], val2 = b[stField];
	                                if (fldType == "2" || fldType == "7" || fldType == "8" || fldType == "9") {
	                                    val1 = val1? parseFloat(val1): val1;
	                                    val2 = val2? parseFloat(val2): val2;
	                                }
	                                if (val1 == val2) {
	                                    return 0;
	                                }
	                                else if ( (val1 == null || val1 == '' || val1 == ' ') && val1 !== 0) {
	                                    return 1;
	                                }
	                                else if ( (val2 == null || val2 == '' || val2 == ' ') && val2 !== 0 ) {
	                                    return -1;
	                                }
	                                else if (stOrder == 'DESC') {
	                                    return val1 < val2 ? 1 : -1;   
	                                }
	                                else { 
	                                	if (stField == 'ROWUID' && val2 > 0 && val1 < 0) 
                                            return 0;
                                        else
	                                    	return val1 < val2 ? -1 : 1;
	                                }
	                           }); 	
    });
    
    return (sortResultRecord ? sortResultRecord : []);
}

var sketchPreviewGenerations = (imgType, index) => {
    let url = "data:image/png;base64,R0lGODlhFAAUAIAAAP///wAAACH5BAEAAAAALAAAAAAUABQAAAIRhI+py+0Po5y02ouz3rz7rxUAOw==",
        type = (imgType == 'png') ? 'image/jpeg' : 'image/png';
    let skRender = typeof (sketchRenderer) != 'undefined' ? sketchRenderer: ((mapFrame?.contentWindow?.sketchRenderer) ? mapFrame.contentWindow.sketchRenderer : null);
    if (!skRender) return url;

    skRender.scale = 1;
    skRender.showOrigin = false;
    skRender.currentSketch = skRender.sketches[index];
    let ab = skRender.sketchBounds(true);
    skRender.resizeCanvas((ab.xmax - ab.xmin) * 10 + 400, (ab.ymax - ab.ymin) * 10 + 95);
    skRender.pan(skRender.formatter.originPosition == "topRight" ? -ab.xmax * 10 + 15 : -ab.xmin * 10 + 15, (skRender.formatter.originPosition == "topRight" || skRender.formatter.originPosition == "topLeft") ? -ab.ymax * 10 + 15 : -ab.ymin * 10 + 15);
    skRender.clearCanvas();
    clearVectorlabelPosition();
    for (var x in skRender.sketches[index].vectors) {
        skRender.sketches[index].vectors[x].render();
    }

    skRender.sketches[index].renderNotes();
    if (skRender.width < 25000 && skRender.width < 25000) {
        url = skRender.toDataURL(type, 1.0);
    }
    return url;
}
