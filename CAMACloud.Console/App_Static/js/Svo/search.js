﻿var hasNbhdBounds = false;
var pagefirstload = false;
function doSearch(searchstrng, optns, sval) {
        clearMarkers();
    $('.masklayer').show();
    var b = $(window).width() - $('#divSearchCntnr').width();
    $('#search-map').width(b);
    $('.resultframe').hide();
    $('.searchframe').show();
    $('#divRghtCntnr').hide();
    $('#divFootCntnr').hide();

    var fieldName = "";
    var fieldValue = "";
    if (optns == undefined) optns = "0";
    if (optns == "0" || optns == "1" || optns == "2")
        var Optn = optns;
    else
        var Optn = optns.split('/'); // To retrieve datatype of field along with fieldname.
    var srchOptn = Optn[0].trim();
    var fieldDataType = Optn[1];
    var schemaType = Optn[2];
    var cntnOprtr = $('#MainContent_Operator').val();

    if (schemaType) schemaType = schemaType.toUpperCase();



    if (fieldDataType != undefined) {
        var numericlength = Optn[2].split('(')[1];
    }

    if (srchOptn) {
        fieldName = srchOptn;
        fieldValue = sval;
        if (fieldValue == undefined) fieldValue = "0";
        var val = optns.split('/');
        if (val[3]) {
            if (datafields[val[3]].LookupTable)
                fieldValue = sval;
        }
        var str = fieldValue;
    }
    if (fieldValue == '' && fieldDataType == "8") {
        fieldValue = "1";
    }
    if (schemaType != undefined) {
        if (schemaType.contains('INT') && isNaN(fieldValue)) {
            clearAlert();
            return false;
        }
        else if (schemaType.contains('INT') && (fieldValue.includes('.'))) {
            clearAlert();
            return false;
        }
    }

    if ((fieldName == 0 || fieldName == 1 || fieldName == 2) && (fieldValue.includes('%'))) {
        alert('Invalid Special character')
        clearSearch()
        closeParcel();
        $('.masklayer').hide();
        return false;
    }

    if (fieldDataType == 1 || fieldDataType == 2 || fieldDataType == 3 || fieldDataType == 5 || fieldDataType == 6 || fieldDataType == 7 || fieldDataType == 8 || fieldDataType == 9 || fieldDataType == 10 || fieldDataType == 11 || fieldDataType == 12 || fieldDataType == 13) {
        var pattern = new RegExp(QC.dataTypes[fieldDataType].pattern);
        var validate1 = (pattern.test(fieldValue));
        if (!validate1 && cntnOprtr != ' IS NULL ' && cntnOprtr != ' IS NOT NULL ') {
            clearAlert();
            return false;
        }
    }

    if (fieldDataType != undefined) {
        if (schemaType.contains('numeric')) {
            var pattern = /^([a-zA-Z]*$)|[ !@#$%^&*()_+\-=\[\]{};':"\\|,<>\/?]/;
            var validate = (pattern.test(fieldValue));
            if (validate || str.length > numericlength) {
                clearAlert();
                $('.masklayer').hide();
                return false;
            }
        } else if (schemaType.contains('BIT')) {
            if (!(fieldValue == 0 || fieldValue == 1)) {
                clearAlert();
                $('.masklayer').hide();
                return false;
            }

        }


    }

    var flag = $('.SrchFlag').val();
    var user = $(".ddlUsers").val();

    if (fieldDataType == 4 && schemaType.contains('DATE')) {
        //var regex = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
        //var regex1 = /^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$/;
        //var regex = new RegExp("^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$");
        var regex = (schemaType == 'DATETIME') ? /((\b1[8-9]\d{2}|\b2\d{3})[-/.](0[1-9]|1[0-2])[-/.](0[1-9]|[12]\d|3[01]))/ : /(\b[12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;
        var validdate = (regex.test(fieldValue));
        //var leapyrvalidate = (regex1.test(fieldValue));
        if (!validdate) {
            alert("Please enter a valid Date Format")
            clearSearch()
            closeParcel();
            $('.masklayer').hide();
            return false;
        }
    }

    if (fieldName != "" && fieldValue == "" && cntnOprtr != "IS NOT NULL" && cntnOprtr != "IS NULL") {
        var alrtMsg = 'You need to provide the Search options';
        if (fieldName != "" && fieldValue == "") {
            alrtMsg = 'You need to provide either the ID or Value to search.';
            if (fieldName == 1) alrtMsg = 'You need to provide the Assignment Group to search.';
            else if (fieldName == 2) alrtMsg = 'You need to provide the CC Tag to search.';
            clearSearch();
            closeParcel();
            $('.masklayer').hide();
            $('#divRghtCntnr').hide('fast');
            $('#divFootCntnr').hide();
        }
        alert(alrtMsg);
        $('.masklayer').hide();
        window.location.hash = "";
        return false;
    }


        $.ajax({
            url: '/sv/search.jrq',
            data: {
                fieldName: fieldName,
                fieldValue: fieldValue,
                flag: flag,
                user: user,
                searchstrng: searchstrng
            },
            dataType: 'json',
            async: false,
            success: function (resp) {
                displaySearchResults(resp)               
                //showpagination();
            }
        });
    
        return false;
}

    function clearSearch() {
        hasNbhdBounds = false;

        if (nbhdpolygon != null) {
            nbhdpolygon.setMap(null);
            delete nbhdpolygon;
        }

        for (x in markers) {
            markers[x].setMap(null);
            delete markers[x];
        }

        for (x in parcels) {
            delete parcels[x];
        }

        $('#lblCntParcl').html("0");

        var htmlCntnts = "<tr>";
        htmlCntnts += "<td colspan=\"3\" class=\"TdNodata\">No Data Available</td>";
        htmlCntnts += "</tr>";


        $('#TabSearchContents tbody tr').remove();
        $('#TabSearchContents tbody').append(htmlCntnts);
        if (!window.location.href.includes("SearchParcelsSV.aspx")) {
            closeParcel();
            loadSearchMap();
        }

    }

    var nbhdpolygon;
    var parcels;
    var parcelindex;

function displaySearchResults(results) {
    if (results.status == 'Error') {
    	if(results.message.contains('aggfield') && results.message.contains('Invalid column'))
            alert('Please check the aggregate condition given. Column name is invalid.');
        else
        	alert(results.message);
        hideMask();
        return;
    }
    parcels = results.Parcels;
    //SortSearchResults("Parcel Id",true)
    parcelindex = 0;
    hasNbhdBounds = results.HasNeighborhood;
    //showpagination();
    if (parcels.length > 0) {
        pagefirstload = true;
        showpagination();
        $('#lblCntParcl').html(parcels.length);
    }
    //var htmlCntnts = "";
    //if (parcels.length > 0) {
    //    $('#lblCntParcl').html(parcels.length);
    //    for (x in parcels) {
    //        var ParcelAddr = "";
    //        if (parcels[x].StreetAddress != null)
    //            ParcelAddr = parcels[x].StreetAddress;
    //        if (parcels[x].KeyValue1 == '0' || parcels[x].ClientRowuid)
    //            parcels[x].KeyValue1 = parcels[x].ClientRowuid;
    //        var ParcelId = parcels[x].KeyValue1;
    //        var ColorCode = parcels[x].ColorCode;
    //        htmlCntnts += "<tr id=\"tr-" + parcels[x].ID + "\">";
    //        htmlCntnts += "<td width=\"10%\" ><span id=\"divSq-" + parcels[x].ID + "\" class=\"divSquare\" style=\"opacity: 0.7;background-color:" + ColorCode + ";\"></span></td>";
    //        htmlCntnts += "<td width=\"45%\"><span onclick=\"if(mapWindow)mapWindow.close();openParcel(" + parcels[x].ID +" , null,  null, true )\" class=\"PrclId\">" + ParcelId + " </span></td>";
    //        htmlCntnts += "<td width=\"45%\"><span onclick=\"if(mapWindow)mapWindow.close();openParcel(" + parcels[x].ID +" , null, null, true )\" class=\"PrclAdr\">" + ParcelAddr + "</span></td>";
    //        htmlCntnts += "</tr>";
    //    }
    //} else {
    //    $('#lblCntParcl').html("0");
    //    htmlCntnts += "<tr>";
    //    htmlCntnts += "<td colspan=\"3\" class=\"TdNodata\">No Data Available</td>";
    //    htmlCntnts += "</tr>";
    //}

    //$('#TabSearchContents tbody tr').remove();
    //$('#TabSearchContents tbody').append(htmlCntnts);

    if (results.NeighborhoodPoints) {
        var points = [];
        var bounds = new google.maps.LatLngBounds();

        for (x in results.NeighborhoodPoints) {
            var pt = results.NeighborhoodPoints[x];
            latlng = new google.maps.LatLng(pt.Latitude, pt.Longitude);
            points.push(latlng);
            bounds.extend(latlng);
        }

        if (map) {
            var polyOptions = {
                path: points,
                strokeColor: "#FF0000",
                strokeOpacity: 0.8,
                strokeWeight: 4,
                fillOpacity: 0,
                visible: true,
                clickable: false
            }
            if (nbhdpolygon == null) {
                nbhdpolygon = new google.maps.Polygon(polyOptions);
            } else {
                if (nbhdpolygon.setOptions)
                    nbhdpolygon.setOptions(polyOptions);
            }
            nbhdpolygon.setMap(map);
            map.setCenter(bounds.getCenter());
            map.fitBounds(bounds);
        }
    }
	var addMsg ='';
	if(results.TotalNoGisParcels[0].ParcelwithNoGis > 0)
		addMsg = results.TotalNoGisParcels[0].ParcelwithNoGis +' parcel(s) are excluded from the result due to unavailability of GIS points. '
    if (parcels.length == 0) {
        alert('Your search did not produce any results.\n'+ addMsg);
        clearSearch()
        closeParcel();
        $('#divRghtCntnr').hide('fast');
        $('#divFootCntnr').hide();
    } else if (parcels.length == 1){
    	if(results.TotalNoGisParcels[0].ParcelwithNoGis > 0){
    		additionalMsg = 'Search result produced '+parcels[0].ActualResCount+' parcel.\n'
        	alert(additionalMsg+addMsg+'Zoom down map to appropriate location to view the parcels.');
    	}
        openParcel(parcels[0].ID, null, null, true);
        $('#divprclcnt').show();
    } else {
        $('#divprclcnt').show();
        //showResultsOnMap(true);
        //if ((map.getZoom() < 19) && (parcels.length > 30)) {
        var additionalMsg = ''
        if(parcels[0].ActualResCount > 500)
            //additionalMsg = 'Search result produced ' + parcels[0].ActualResCount + ' parcels. Displaying the top 500 results below.\n'
        additionalMsg = 'Search result produced ' + parcels[0].ActualResCount + ' parcels.\n'
        alert(additionalMsg+addMsg+'Zoom down map to appropriate location to view the parcels.');
        // }
    }
    hideMask();
    return false;
}


var svg = '<?xml version="1.0" standalone="no"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg width="4cm" height="4cm" viewBox="0 0 400 400" xmlns="http://www.w3.org/2000/svg" version="1.1"><title>Example triangle01- simple example of a "path"</title><desc>A path that draws a triangle</desc><rect x="1" y="1" width="398" height="398" fill="none" stroke="blue" /><path d="M 100 100 L 300 100 L 200 300 z" fill="red" stroke="blue" stroke-width="3" /></svg>'

var markers = [];
var infoWindow;

var loadingResults = false;

function showResultsOnMap(onsearch) {

    let pageIndex = $('#pageSelector').val();
    let recordsPerPage = parseInt($('#recordsPerPageSelector').val(), 10);

    let start = (pageIndex - 1) * recordsPerPage;
    let end = start + recordsPerPage;

loadingResults = true;
if (parcels == null) {
loadingResults = false;
return;
}
clearMarkers();
var mz = map.getZoom();
if (hasNbhdBounds) {
if ((mz < 19) && (parcels.length > 30)) {
loadingResults = false;
return;
}
}
   var parcelperpage = parcels.slice(start, end)
if (onsearch && !hasNbhdBounds) {
var pbounds = new google.maps.LatLngBounds();
    for (x in parcelperpage) {
        var p = parcelperpage[x];
        if (parcelperpage[x].Latitude && parcels[x].Longitude) {
var marker = getMarker(p);
pbounds.extend(marker.position);
}
}
map.setCenter(pbounds.getCenter());
map.fitBounds(pbounds);

} else {

var bounds = map.getBounds();
var sw = bounds.getSouthWest();
var ne = bounds.getNorthEast();


var lat1 = sw.lat();
var lon1 = sw.lng();
var lat2 = ne.lat();
var lon2 = ne.lng();

    for (x in parcelperpage) {
        var p = parcelperpage[x];
if ((p.Latitude > lat1) && (p.Latitude < lat2) && (p.Longitude > lon1) && (p.Longitude < lon2)) {
var marker = getMarker(p);
}
}
}

showMarkersInMap();
}

function clearMarkers() {
    console.error('Clearing map')
    for (x in markers) {
        markers[x].setMap(null);
        delete markers[x];
    }
}

function showMarkersInMap() {
for (x in markers) {
markers[x].setMap(map);
}
loadingResults = false;
}

function getMarker(p) {
var marker = new google.maps.Marker({
position: new google.maps.LatLng(p.Latitude, p.Longitude),
icon: {
path: google.maps.SymbolPath.CIRCLE,
scale: 7,
fillColor: p.ColorCode,
strokeColor: p.ColorCode,
fillOpacity: 0.3,
strokeWeight: 2
}
//        ,
//        map: map
});


marker.ParcelId = p.ID;

google.maps.event.addListener(marker, "click", function () {
if(mapWindow)mapWindow.close();
openParcel(this.ParcelId);
});

markers.push(marker);

return marker;
}

function clearAlert(){
            alert("Your search does not produce any results")
            clearSearch()
            closeParcel();
            $('.masklayer').hide();
            
}
function showpagination() {
    if (parcels) {
        var table = parcels;
        var recordsPerPage = parseInt($('#recordsPerPageSelector').val(), 10);

        var totalPages = Math.ceil(table.length / recordsPerPage);

        var pageSelector = document.getElementById('pageSelector');


        if (pagefirstload) {
            pageSelector.innerHTML = '';
            for (var i = 1; i <= totalPages; i++) {
                var option = document.createElement('option');
                option.value = i;
                option.text = i;
                pageSelector.appendChild(option);
            }
        }

        var pageIndex = $('#pageSelector').val();
        var start = (pageIndex - 1) * recordsPerPage;
        var end = start + recordsPerPage;

        var htmlCntnts = "";
        if (parcels.length > 0) {
            let pageparcel = parcels.slice(start, end);
            for (x in pageparcel) {
                var ParcelAddr = "";
                if (pageparcel[x].StreetAddress != null)
                    ParcelAddr = pageparcel[x].StreetAddress;
                if (pageparcel[x].KeyValue1 == '0' || pageparcel[x].ClientRowuid)
                    pageparcel[x].KeyValue1 = pageparcel[x].ClientRowuid;
                var ParcelId = pageparcel[x].KeyValue1;
                var ColorCode = pageparcel[x].ColorCode;
                htmlCntnts += "<tr id=\"tr-" + pageparcel[x].ID + "\">";
                htmlCntnts += "<td width=\"10%\" ><span id=\"divSq-" + pageparcel[x].ID + "\" class=\"divSquare\" style=\"opacity: 0.7;background-color:" + ColorCode + ";\"></span></td>";
                htmlCntnts += "<td width=\"45%\"><span onclick=\"if(mapWindow)mapWindow.close();openParcel(" + pageparcel[x].ID + " , null,  null, true, true)\" class=\"PrclId\">" + ParcelId + " </span></td>";
                htmlCntnts += "<td width=\"45%\"><span onclick=\"if(mapWindow)mapWindow.close();openParcel(" + pageparcel[x].ID + " , null, null, true, true)\" class=\"PrclAdr\">" + ParcelAddr + "</span></td>";
                htmlCntnts += "</tr>";
            }
        } else {
            $('#lblCntParcl').html("0");
            htmlCntnts += "<tr>";
            htmlCntnts += "<td colspan=\"3\" class=\"TdNodata\">No Data Available</td>";
            htmlCntnts += "</tr>";
        }

        $('#TabSearchContents tbody tr').remove();
        $('#TabSearchContents tbody').append(htmlCntnts);
        
        closeParcel();
        showResultsOnMap(true);
        var btnPrevGrayOut = pageIndex == 1;
        $('#btnPreviousPage').toggleClass('grayed-out', btnPrevGrayOut);
        $('.search-container').removeAttr('style');
       // var btnNextGrayOut = pageIndex == totalPages;
       // $('#btnNextPage').toggleClass('grayed-out', btnNextGrayOut);      
    }
}

$('#recordsPerPageSelector').on('change', function () {
    svPromptAction(() => {
        pagefirstload = true;
        showpagination();
    }, (!activeParcel ? true : false));
});

$('#btnNextPage').on("click", function () {
    var currentPage = parseInt($('#pageSelector').val(), 10);
    $('#pageSelector').val(currentPage + 1);
    $('#pageSelector').change();
});

$('#btnPreviousPage').on("click", function () {
    var currentPage = parseInt($('#pageSelector').val(), 10);
    $('#pageSelector').val(currentPage - 1);
    $('#pageSelector').change();
});

$('#pageSelector').on('change', function () {
    svPromptAction(() => {
        pagefirstload = false;
        showpagination();
    }, (!activeParcel ? true : false));
});