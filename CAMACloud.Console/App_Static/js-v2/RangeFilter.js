﻿function RangeFilter(s, o) {
    o = o || {};
    function ec(va, r) {
        if (!Array.isArray(va) || va.length != 2 || !isFinite(va[0]) || !isFinite(va[1]))
            throw 'Invalid parameters - expecting numeric array of length 2.';
        if (va[1] < va[0])
            throw 'Invalid parameters - values not in order.';
        if (r) {
            if (va[0] < r[0] || va[1] > r[1])
                throw 'Invalid value - not within range.';
        }
    }
    var span = '<span />'
    var ni = '<input type="number" class="rf-ni" step="0.1" />'

    $(s).html('');
    $(s).append($(span).html(o.label || 'Select Range:'))
        .append($('<div class="rf-slider"></div>'))
        .append($(span).html('Min:'))
        .append($(ni).attr('oper', 'min'))
        .append($(span).html('Max:'))
        .append($(ni).attr('oper', 'max'));
    var resline = $(span);
    resline.append($(span).addClass('rf-result-count').html(0)).append('&nbsp;');
    resline.append($(span).addClass('rf-result-label').html(o.resultCounterText || 'items'));
    $(s).append(resline);

    var sel = s;
    var slider = $('.rf-slider', s), minVal = $('.rf-ni[oper="min"]', s), maxVal = $('.rf-ni[oper="max"]', s);
    var _range = [-4, 5], maxLength = 5, mirrored = false, inversed = false;
    var onchange, calculate;
    slider.slider({
        range: true,
        min: -1,
        max: 1,
        step: 0.1,
        values: [0, 0],
        slide: function (event, ui) {
            var oper = $(ui.handle).index() == 1 ? 'min' : 'max';
            var v1 = ui.values[0], v2 = ui.values[1];
            minVal.val(v1);
            maxVal.val(v2);
            updateResult(oper, v1, v2);

        }
    });

    function updateResult(oper, v1, v2) {
        if (v1 === undefined) {
            v1 = _rf.values[0];
            v2 = _rf.values[1];
        }
        if (oper) {
            if (mirrored) {
                if (oper == "min") {
                    v2 = Math.round((_range[1] - (v1 - _range[0])) * 10) / 10;
                } else if (oper == "max") {
                    v1 = Math.round((_range[0] + (_range[1] - v2)) * 10) / 10;
                }
            }

            try {
                _rf.values = [v1, v2];
            } catch (e) {
                console.error(e, v1, v2);
                _rf.values = [_range[0], _range[1]];
            }
        }


        if (calculate) {
            var v = calculate(_rf.values);
            if (v !== undefined && isFinite(v)) _rf.result = v
        }
        onchange && onchange(_rf.values)
    }

    Object.defineProperties(this,
        {
            onchange: { get: () => { return onchange }, set: (v) => { if (typeof v == "function") onchange = v; } },
            calculate: { get: () => { return calculate }, set: (v) => { if (typeof v == "function") calculate = v; } },
            mirrored: { get: () => { return mirrored }, set: (v) => { if (typeof v == "boolean") { mirrored = v; updateResult('min'); } } },
            range: {
                set: function (v) {
                    ec(v);
                    _range = v;
                    slider.slider({ min: v[0], max: v[1] });
                    slider.slider({ values: [v[0], v[1]] });
                    maxLength = Math.max(v.map((x) => { x.toFixed(1).length }));
                    [minVal, maxVal].forEach((x, i) => { x.val(v[i]); x.prop({ 'min': v[0], 'max': v[1] }).attr({ 'maxlength': maxLength, 'dval': v[i] }) });
                    updateResult();
                },
                get: function (v) { return _range; }
            },
            values: {
                set: function (v) {
                    ec(v, _range);
                    slider.slider({ values: [v[0], v[1]] });
                    minVal.val(v[0]);
                    maxVal.val(v[1]);
                    minVal.prop('max', v[1]);
                    maxVal.prop('min', v[0]);
                    updateResult();
                },
                get: function () { return slider.slider('values'); }
            },
            result: {
                set: function (v) {
                    $('.rf-result-count', sel).html(v);
                }
            },
            inversed: {
                get: () => { return inversed },
                set: (v) => {
                    if (typeof v == "boolean") {
                        inversed = v;
                        if (v) {
                            $(slider).removeClass('ui-widget-content').addClass('ui-widget-header');
                            $('.ui-slider-range', slider).removeClass('ui-widget-header').addClass('ui-widget-content');
                        } else {
                            $('.ui-slider-range', slider).removeClass('ui-widget-content').addClass('ui-widget-header');
                            $(slider).removeClass('ui-widget-header').addClass('ui-widget-content');
                        }
                        updateResult('min');
                    }
                }
            },
            applied: {
                get: () => { 
                    var v = _rf.values;
                    return inversed || _range[0] != v[0] || _range[1] != v[1];
                }
            }
        }
     );

    $('.rf-ni', s).on('mouseup keyup change', (e) => {
        var c = $(e.currentTarget);
        var oper = c.attr('oper');
        var changeKeys = [9, 13, 38, 40]
        if (e.type == "keyup" && changeKeys.indexOf(e.keyCode) == -1) {
            return;
        }
        var minmax;
        try {
            minmax = $('.rf-ni').map(function (i, x) { return parseFloat($(x).val()) }).toArray()
        } catch (ex) {
            console.error(ex);
            minmax = [_range[0], _range[1]];
        }
        updateResult(oper, minmax[0], minmax[1]);
        if (e.type == "mouseup")
            $(e.currentTarget).blur();
        e.preventDefault();

        return false;
    })

    $('.rf-ni', s).on('keypress', (e) => { e.keyCode == 13 && e.preventDefault(); });

    var _rf = this;
    Object.freeze(this);
}