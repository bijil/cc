const loading = {
    show: (msg) => {
        msg && $(".masklayer .mask-new .mask-msg").text(msg);
        loading._sel = loading._mn;
        showMask();
    },
    hide: () => {
        hideMask();
        loading._sel = loading._md;
        $(".masklayer .mask-new .mask-msg").text("Loading items, please wait.");
    },
    showAsk: (msg, t) => {
        let l = loading;
        l._sel = l._a;
        $(".mask-q", l._a).text(msg);
        if (t == 1) $(".ask-btn", l._a).show();
        else if (t == 0) $(".ask-cancel", l._a).hide();
        $(".ask-ok ", l._a).text("Okay");
        showMask();
    },
    hideAsk: () => {
        let l = loading;
        hideMask();
        l._sel = l._md;
    },
    showError: (msg, desc) => {
        let l = loading;
        l._sel = l._e;
        $(".mask-q", l._e).text(msg);
        let de = $(".mask-desc", l._e);
        if (desc && desc.trim().length) {
            de.show();
            $("span", de).hide().text(desc);
            $(document)
                .off("click", ".masklayer .mask-error .mask-desc p")
                .on("click", ".masklayer .mask-error .mask-desc p", () => {
                    $("span", de).toggle();
                });
        } else {
            de.hide();
            $("span", de).text("");
        }
        showMask();
    },

    get _mn() {
        return $(".masklayer .mask-new");
    },
    get _md() {
        return $(".masklayer .mask-default");
    },
    get _a() {
        return $(".masklayer .mask-ask");
    },
    get _e() {
        return $(".masklayer .mask-error");
    },
    set _sel(el) {
        let l = loading;
        l._md.hide();
        l._mn.hide();
        l._a.hide();
        l._e.hide();
        el.show();
    },
};

const ask = function (msg, succ, fail, parms) {
    loading.showAsk(msg, 1);
    $(".masklayer .mask-default, .masklayer .mask-new").hide();
    $(".masklayer .mask-ask").show();

    $(document)
        .off("click", ".masklayer .mask-ask .ask-ok")
        .on("click", ".masklayer .mask-ask .ask-ok", () => {
            loading.hideAsk();
            if (succ && typeof succ === "function") succ(parms);
        });
    $(document)
        .off("click", ".masklayer .mask-ask .ask-cancel")
        .on("click", ".masklayer .mask-ask .ask-cancel", () => {
            loading.hideAsk();
            if (fail && typeof fail === "function") fail();
        });
};

const say = function (msg, bText) {
    var ok = ".masklayer .mask-ask .ask-ok";
    $(ok).text(bText || "Close");
    loading.showAsk(msg, 0);

    $(document)
        .off("click", ok)
        .on("click", ok, () => {
            loading.hideAsk();
            closeSay();
        });

    function closeSay() {
        bText && $(ok).text("Yes");
        $(".masklayer .mask-ask .ask-cancel").show();
    }
};

const sayError = (msg, desc, bText) => {
    var ok = ".masklayer .mask-error .ask-ok";
    $(ok).text(bText && bText.trim().length ? bText : "Okay");
    loading.showError(msg, desc, bText);

    $(document)
        .off("click", ok)
        .on("click", ok, () => {
            loading.hideAsk();
            $(ok).text("Okay");
        });
};

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

const SnackBar = function (msg, options) {
    const sb = {};
    const def = {
        position: 'top-left',
        autoHide: true,
        autoHideAfter: 3000,
        autoShow: true,
        loading: false,
    }
    const posEnum = { "top-left": 1, "top-center": 2, "top-right": 3, "bottom-left": 4, "bottom-center": 5, "bottom-right": 6 };

    sb.msg = msg || 'This is a SnackBar';
    sb.settings = $.extend(def, options);
    sb.sbId = Math.floor(Math.random() * 1000000);

    function init(sb) {
        $('.snackbar[sb]').remove();
        const p = $('<p>').text(sb.msg);
        let sbClass = 'snackbar ';
        if (!posEnum[sb.settings.position]) sb.settings.position = 'bottom-left'
        sbClass += 'snackbar-' + sb.settings.position;
        const sbc = $('<div>').attr('class', 'snackbar__container').html(p)
        const div = $('<div>').attr({ class: sbClass, sb: sb.sbId, style: "display:none" }).html(sbc);
        $('.masklayer').before(div);

        if (sb.settings.loading) {
            const sbl = $('<div>').attr('class', 'snackbar__loading').html('<div></div><div></div>');
            sbc.prepend(sbl);
        }
        if (sb.settings.autoShow) div.fadeIn(250);
        if (sb.settings.autoHide) {
            setTimeout(() => { sb.hide(); }, sb.settings.autoHideAfter)
        }
    }

    sb.show = () => {
        let d = $(`.snackbar[sb=${sb.sbId}]`);
        if (d && d.length) d.fadeIn(250);
        else init(sb);
    }

    sb.hide = () => {
        let d = $(`.snackbar[sb=${sb.sbId}]`);
        d.fadeOut(250, () => {
            d.remove();
        })
    }

    sb.setMessage = (msg) => {
        if (msg) {
            let d = $(`.snackbar[sb=${sb.sbId}]`);
            sb.msg = msg;
            $('p', d).text(msg);
        }
    }

    init(sb)
    return sb;
}