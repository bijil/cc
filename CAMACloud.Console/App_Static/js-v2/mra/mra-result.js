﻿var ratioDistribution = {},
    graphData = [];
var chm = new ChartManager();
var rrange;
var sortF = false;
var ratioSorted = false;
var filter = {
    min: null,
    max: null,
    inversed: false,
    applied: false,
    set: function () {
        this.applied = rrange.applied;
        var rv = rrange.values;
        this.min = rv[0];
        this.max = rv[1];
        this.inversed = rrange.inversed;
    },
    reset: function () {
        this.applied = false;
        this.inversed = false;
        this.min = null;
        this.max = null;
    },
};

const VALUE_TITLE = "Predicted Value";
var templateFields = {};

function resizeWindow() {
    var H = $(".main-content-area").height();
    $(".d1001").height(H);
    $("#chart1").width($(".chart1-container").box().width - 10);
    $(".result-scroller").height(H - $(".result-scroller").box().top);
    $(".templates-list-div").height(H - $(".templates-list-div").box().top);
}

function enableLayout() {
    $("body").css({ cursor: null });
    $(".d1001").removeAttr("disabled");
    $(".shade").hide();
}

function disableLayout() {
    $(".d1001").attr("disabled", "disabled");
    $(".shade").show();
}

function fillFields(c, d) {
    $("[field]", c).each((i, x) => {
        var fld = $(x).attr("field"),
            fmt = $(x).attr("format");
        var v = d[fld];
        if (fmt == "f") {
            v = formatNumber(v);
        }
        $(x).html(v);
    });
}

function loadGraph(fieldId) {
    selectedFieldId = fieldId;
    loading.show("Getting Results");
    disableLayout();
    $(".select-field").selectOne("fid", fieldId, "selected");
    PageMethods.GetChartData(
        template.ID,
        fieldId,
        filter.applied,
        filter.min || 0,
        filter.max || 0,
        filter.inversed,
        function (d) {
            checkError(d, () => {
                var cd = d.Data;
                var gd = (graphData = cd.GraphData);
                var actuals = gd.map(function (o) {
                    return { x: o.i, y: (o.a)?(o.a).toFixed(2): o.a };
                });
                var predicted = gd.map(function (o) {
                    return { x: o.i, y: (o.p)?(o.p).toFixed(2): o.p, pointRadius: 2 };
                });
                var actualsHeading =
                    VALUE_TITLE + " vs. " + templateFields[fieldId].Label;
                regchart.clear();
                regchart
                    .addData(VALUE_TITLE, predicted, {
                        backgroundColor: ["#0033ff"],
                        borderColor: ["#0066ff"],
                    })
                    .addData(actualsHeading, actuals, {
                        backgroundColor: ["#9E9E9E88"],
                        borderColor: ["#9E9E9E"],
                    });
                regchart.refresh();
            });
            enableLayout();
            loading.hide();
        }
    );
}

function openTemplate(tid) {
    disableLayout();
    templateId = tid;
    template = templates[tid];

    outputType = template.DataType;
    fieldTypes[template.OutputField] = outputType;

    $(".template-name").html(
        `${template.Name} <span title="${template.OutputFieldTable}.${template.OutputFieldName}">${template.DisplayLabel}</span>`
    );
    $(".output-data").show();
    $(".mra-links .button-action").show();
    $(".mra-item").selectOne("tid", tid, "selected");

    PageMethods.GetTemplate(tid, function (d) {
        checkError(d, () => {
            var data = d.Data;
            fillFields(".reg-results", data.Result);
            fillFields(".reg-anova", data.Result);

            templateFields = {};
            var html = "",
                linksHtml = "",
                firstField = 0,
                bias = 0;
            for (var i in data.Coefficients) {
                with ((x = data.Coefficients[i])) {
                    x.Coefficient = numberWithCommas(
                        Math.round(formatNumber(x.Coefficient) * 100) / 100
                    );
                    x.StandardError = numberWithCommas(
                        Math.round(formatNumber(x.StandardError) * 100) / 100
                    );
                    x.tStat = numberWithCommas(
                        Math.round(formatNumber(x.tStat) * 100) / 100
                    );
                    x.pValue = numberWithCommas(
                        Math.round(formatNumber(x.pValue) * 100) / 100
                    );
                    var link;
                    if (x.FieldId != -1) {
                        linksHtml += `<a fid="${FieldId}" class="select-field">${Head}</a>`;
                        link = `<a fid="${FieldId}" class="select-field">${Head}</a>`;
                        if (!firstField) firstField = x.FieldId;
                    } else {
                        link = x.Head;
                    }
                    html += `<tr><td fid="${FieldId}">${link}</td><td>${Coefficient}</td><td>${StandardError}</td><td>${tStat}</td><td>${pValue}</td></tr>`;
                    if(x.FieldId != -1) {
	                    var field = data.InputFields[i-bias];
	                    templateFields[x.FieldId] = { Id: x.FieldId, Label: x.Head, Aggregate: field.Aggregate?field.Aggregate:"MAX", Expression: field.Expression, FilterOperator: field.FilterOperator, FilterValue: field.FilterValue, FilterValue2: field.FilterValue2, LookupTable: field.LookupTable};
	                } else {
	                	bias++;
	                	templateFields[x.FieldId] = { Id: x.FieldId, Label: x.Head};
	                }
                }
            }
            $(".field-list").html(linksHtml);
            $(".coefficients-list").html(html);

            $(".select-field").bindEvent("click", function () {
                var fid = $(this).attr("fid");
                loadGraph(fid);
            });

            ratioDistribution = data.RatioDistribution;
            if (ratioDistribution.length > 0) {
                var rr = {},
                    rd2 = [];
                ratioDistribution
                    .map((x) => {
                        return {
                            r: x.r < 0.5 ? 0.5 : x.r > 1.5 ? 1.5 : x.r,
                            p: x.p,
                        };
                    })
                    .map((x) => {
                        rr[x.r] = (rr[x.r] || 0) + (x.p || 0);
                    });
                for (var x in rr) {
                    rd2.push({ r: parseFloat(x), p: parseFloat(rr[x].toFixed(1)) });
                }
                rd2 = rd2.sort((x, y) => x.r - y.r);

                var fx = rd2.map(function (i) {
                    return i.r;
                });
                var fy = rd2.map(function (i) {
                    return i.p;
                });
                chm.charts["chart2"].data.labels = fx;
                chm.setData("chart2", "Ratio Frequency", fy);
                var rf = ratioDistribution.map((x) => x.r);
                rrange.range = [
                    rf.sort((x, y) => x - y)[0],
                    rf.sort((x, y) => y - x)[0],
                ];
            }

            loadResidualData(0);

            loading.hide();
            enableLayout();

            if (firstField) loadGraph(firstField);

            resizeWindow();
        });
    });
}

function sortResidualDatabyRatio() {
    ratioSorted? loadResidualData(0, 1) : loadResidualData(0, 2);
    ratioSorted = !ratioSorted;
    sortF = true;
}

function sortResidualDatabyId() {
	if(sortF) {
		loadResidualData(0, 0);
        ratioSorted = false;
		sortF = false;
	}
}

function loadResidualData(pageIndex, RSort) {
    var pageSize = 50;
    RSort = RSort ? RSort : 0;
    var spans = $(".residual-table th").length;
    var loading = `<tr><td colspan="${spans}" class='residuals-loading'>Loading residuals ...</td></tr>`;
    $(".residual-list").html(loading);
    PageMethods.GetResidualsData(
        templateId,
        pageIndex,
        pageSize,
        filter.applied,
        filter.min || 0,
        filter.max || 0,
        filter.inversed,
        RSort,
        function (d) {
            checkError(d, () => {
                var c = d.FullCount;
                var ctx = c + " parcels.";
                if (c > 500) ctx = "Displaying top 500 of " + ctx;
                $(".residuals-count").html(ctx);

                var html = "";

                if (RSort) d.data = d.Data.sort((a, b) => { return b.Ratio - a.Ratio });

                for (var i in d.Data) {
                    with ((dx = d.Data[i])) {
                        if (outputType != 8) {
                            Actual = numberWithCommas(
                                Math.round($out(Actual) * 100) / 100
                            );
                            Predicted = numberWithCommas(
                                Math.round($out(Predicted) * 100) / 100
                            );
                        } else {
                            Actual = $out(Actual);
                            Predicted = $out(Predicted);
                        }
                        if (Ratio) Ratio = Ratio.toFixed(2);
                        Residual = numberWithCommas(
                            Math.round(Residual * 100) / 100
                        );
                        html += `<tr><td>${ObservationNo}</td><td><a target="_blank" href="/protected/quality/#/parcel/${ParcelId}">${Parcel}</a></td><td>${Actual}</td><td>${Predicted}</td><td>${Residual}</td><td>${Ratio}</td></tr>`;
                    }
                }

                $(".residual-list").html(html);

                var numPages = Math.min(Math.ceil(d.FullCount / pageSize), 10);
                var numButtons = Array.apply(0, Array(numPages))
                    .map(function (x, i) {
                        var sel = i == pageIndex ? ' selected=""' : "";
                        return `<a pi="${i}"${sel}>${i + 1}</a>`;
                    })
                    .reduce(function (x, y) {
                        return x + y;
                    }, "");
                $(".data-page-links").html(numButtons);

                $(".rt-container").height($(".residual-table").box().height);

                $("a[pi]", ".data-page-links").bindEvent("click", function () {
                    var pageIndex = parseInt($(this).attr("pi"));
                    sortF? !ratioSorted? loadResidualData(pageIndex, 1): loadResidualData(pageIndex, 2): loadResidualData(pageIndex, 0);
                });
            });
        }
    );
    enableLayout();
}

function toggleRegressionInfo(ib) {
    var visible = $(ib).attr("status") == "1";
    $(".template-result")[visible ? "hide" : "show"]();
    $(ib).attr("status", visible ? "0" : "1");
    resizeWindow();
}

function downloadReport(type) {
    var query = "?templateId=" + templateId;
    if (filter.applied) {
        query += "&rmin=" + filter.min;
        query += "&rmax=" + filter.max;
        query += "&rinv=" + (filter.inversed ? "1" : "0");
    }
    if(type == 1){
    	//query += "&cData=" + $('.corr-result-table').html();
    	var url = location.pathname + "/exportCorrelation" + query;
    } else var url = location.pathname + "/exportdata" + query;
    
    //alert(query);
    location.href = url;
}

$(window).on("resize", resizeWindow);

var regchart;

$(() => {
    var qd = QueryData;
    resizeWindow();

    if (!qd.templateId) {
        $(".output-data").hide();
    }

    rrange = new RangeFilter(".range-fields", {
        label: "Prediction Ratio Range:",
        resultCounterText: "parcels (approx)",
    });

    regchart = chm.createChart("chart1", "chart1", "scatter", {
        labels: [100, 400],
        tooltips: {
            callbacks: {
                //title: (ti, d) => { console.log('title', ti, d); return 'title' },
                //label: (ti, d) => { console.log('label', ti, d); return 'title' },
            },
            displayColors: false,
        },
        scales: {
            yAxes: [
                {
                    ticks: {
                        callback: function (dataLabel, index, labels) {
                            return outputType ? $out(dataLabel, 1) : dataLabel;
                        },
                    },
                },
            ],
        },
    });

    regchart.onclick = (x, c) => {
        if (x.length > 0) {
            var ce = x[0];
            var pd = graphData[ce._index];
            showCPInfo(pd);
        }
        $(document)
            .off("click")
            .on("click", (e) => {
                var container = $(".chart-info");
                if (
                    !container.is(e.target) &&
                    container.has(e.target).length === 0
                ) {
                    if (
                        !$(c.canvas).is(e.target) ||
                        !($(c.canvas).is(e.target) && x.length)
                    ) {
                        $(regchart.chart.canvas).removeAttr("disabled");
                        container.hide();
                    }
                }
            });
    };

    chm.createChart("chart2", "chart2", "line", {
        labels: ["Min", "Max", "Avg"],
        elements: { point: { radius: 1 } },
        scales: {
            yAxes: [
                {
                    ticks: {
                        max: 100,
                        min: 0,
                    },
                },
            ],
            xAxes: [
                {
                    ticks: {
                        callback: function (dataLabel, index, labels) {
                            if (index == 0) {
                                return "<" + dataLabel;
                            }
                            if (index == labels.length - 1) return ">" + dataLabel;
                            return (parseFloat(dataLabel) * 10) % 5 === 0
                                ? dataLabel
                                : "";
                        },
                    },
                },
            ],
        },
    });

    PageMethods.GetLoadInfo(function (d) {
        loading.show("Getting Results");
        checkError(d, () => {
            var px = d.Data.GIS;
            defaultExtent = [px.lg1, px.lt1, px.lg2, px.lt2];
            defaultCenter = [(px.lg1 + px.lg2) / 2, (px.lt1 + px.lt2) / 2];

            datasets = d.Data.Datasets;
            templates = {};

            d.Data.Templates.forEach(function (x, i) {
                templates[x.ID] = x;
            });

            var html = "";
            for (var i in d.Data.Templates) {
                with ((tx = d.Data.Templates[i])) {
                    tx.state = tx.Compiled ? 1 : 0;
                    tx.draft = !tx.Compiled ? "<span>Draft</span>" : "";
                    html += `<tr><td class="left-list-item mra-item" tid="${ID}" c="${state}"><a style="width: 220px;display: inline-flex;"><div title="${Name}" style="white-space: pre;text-overflow: ellipsis; overflow: hidden;width: auto; margin-right:5px">${Name}</div> ${draft}</a> <span d="1"></span><span e="1"></span></td></tr>`;
                }
            }
            $(".templates-list").html(html);

            $("a", ".templates-list")
                .off("click")
                .on("click", function () {
                    loading.show("Opening Model");
                    var tid = $(this).parent().attr("tid");
                    var state = $(this).parent().attr("c");
                    if (state == "1") {
                        openTemplate(tid);
                    } else {
                        window.location.href =
                            "mra.aspx?" + URLPack({ templateId: tid });
                    }
                });

            $("span[e]", ".templates-list")
                .off("click")
                .on("click", function () {
                    var tid = $(this).parent().attr("tid");
                    window.location.href =
                        "mra.aspx?" + URLPack({ templateId: tid });
                });

            $("span[d]", ".templates-list")
                .off("click")
                .on("click", function () {
                    var tid = $(this).parent().attr("tid");
                    let title = $(this).parent().find("a>div[title]").text();
                    ask(
                        "Are you sure you want to delete Regression Model - " +
                            title +
                            " permanently?",
                        () => {
                            loading.show("Deleting " + title);
                            PageMethods.DeleteTemplate(tid, function (d) {
                                checkError(d, () => {
                                    window.location.href = window.location.pathname;
                                });
                                loading.hide();
                            });
                        }
                    );
                });

            if (qd.templateId) {
                openTemplate(qd.templateId);
            }
        });
        loading.hide();
    });

    rrange.calculate = function (v) {
        var fn = !rrange.inversed
            ? (x) => x.r >= v[0] && x.r <= v[1]
            : (x) => !(x.r >= v[0] && x.r <= v[1]);
        var sum = ratioDistribution
            .filter(fn)
            .map((x) => x.c)
            .reduce((x, y) => x + y, 0);
        return sum;
    };

    $("#chk_range_mirror").on("change", (e, t) => {
        rrange.mirrored = $(e.currentTarget).prop("checked");
    });
    $("#chk_range_inverse").on("change", (e, t) => {
        rrange.inversed = $(e.currentTarget).prop("checked");
    });

    $(".b-t-v").each((i, x) => {
        var vs = $(x).attr("viewselector");
        $(vs)[$(x).attr("status") == "0" ? "hide" : "show"]();

        $(x).on("click", () => {
            var status = $(x).attr("status") == "0" ? "1" : "0";
            $(vs)[status == "0" ? "hide" : "show"]();
            $(x).attr("status", status);
        });

        $("[cancel]", vs).on("click", (e) => {
            $(vs).hide();
            $(x).attr("status", "0");
            e.preventDefault();
            return false;
        });
    });
});

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function saveAsJPEG() {
	var divToDownload = $('.corr-result').clone();
	divToDownload.removeClass('corr-result');
	divToDownload.removeClass('popup-dg');
	divToDownload.addClass('download');
	$('#divContentArea').append(divToDownload);
	$('.download .filter-title').html(template.Name +" Correlation Matrix");
	$('.download .matrix-button-panel').hide();
	$('.download').css('max-width','1000%');
	$('.download').css('max-height','1000%');
	$('.download').css('z-index', '-1');
	$('.download').css('overflow', 'hidden');
	$('.download').css('position', 'absolute');
	$('.download').css('top', '0%');
	$('.download').css('left', '0%');
	$('.download').css('padding-right', '1%');
	$('.download').css('background-color', 'white');
	domtoimage.toBlob(divToDownload[0])
    .then(function(blob) {
      window.saveAs(blob, template.Name.trim() + "-Correlation-Matrix.png");
      $('.download').remove();
    });
}

function unAbbr(op) {
	switch(op) {
		case 'EQ': op = 'Equal to'; break;
		case 'NE': op = 'Not Equal to'; break;
		case 'GT': op = 'Greater than'; break;
		case 'LT': op = 'Less than'; break;
		case 'GE': op = 'Greater/Equal to'; break;
		case 'LE': op = 'Less/Equal to'; break;
		case 'NL': op = 'is NULL'; break;
		case 'NN': op = 'is Not NULL'; break;
		default: op = 'None';
	}
	return op;
}

function getFieldInfo(field) {
	var title = "";
	if(field.Id && field.Label) {
		title = 'Field: ' + field.Label + (!field.LookupTable? '\nAggregate: ' + field.Aggregate: '');
		field.Expression? title += '\nExpression: ' + field.Expression: !field.LookupTable? title += '\nExpression: None': title += '';
		field.FilterOperator? field.FilterOperator == 'BW'? title += '\nFilter: Between ' + field.FilterValue + ' and ' + field.FilterValue2: title += '\nFilter: ' + unAbbr(field.FilterOperator) + ' ' + ((field.FilterOperator == 'NL' || field.FilterOperator == 'NN')? '': field.FilterValue): !field.LookupTable? title+= '\nFilter: None': title += '';
		field.LookupTable? title += '\nLookupTable: ' + field.LookupTable: title += '';
		return title;
	}
	return " ";
}

function getCorrelationMatrix(){
    var correlationFields = {...templateFields};
    correlationFields[template.OutputField]={
        Aggregate : template.OutputAggregate,
        Expression :  template.OutputFieldExpression,
       FilterOperator :  template.OutputFilterOperator,
        FilterValue : template.OutputFilterValue,
        FilterValue2 : template.OutputFilterValue2,
        Id : template.OutputField,
        Label : template.OutputFieldName,
        LookupTable : ''
    }
	if(Object.keys(correlationFields).length + (Object.keys(correlationFields).includes('-1')?-1:0) < 2) {
		say('At least two input Fields are required to create a Correlation Matrix');
		return false;
	}
	disableLayout();
	loading.show('Opening Correlation Matrix');
 	var templateId = template.ID;
 	$('.corr-result-table').html('');
 	$('.corr-result-matrix').html('');
 	PageMethods.LoadCorrelation( templateId, (d)=>{
 			 if(d.Success == false) {
 			 	sayError("There was an error while creating the Correlation Matrix", d.ErrorMessage);
 			 	$('.corr-result').hide();
 			 	enableLayout();
 			 	return false;
 			 }
             var data = d.Data;
             var headerLabel = '<tr><th colspan="2" style="background-color: #d4dadc; border: 0px;"></th>';
			 var headerHtml = '<tr><th colspan="2" style="background-color: #d4dadc;"></th>';
			 var headerMatrix = '<tr><th style="background-color: #d4dadc;"></th>';
			 var rowHtml = '';
			 var rowMatrix = '';
			 var i = 0;
			 for (h in correlationFields){
			 	if(h!= -1) {
			 		var field = correlationFields[h];
			 		var fieldInfo = getFieldInfo(field);
			 		headerLabel += '<th style="text-align: center;"><b>' + String.fromCharCode(i+65) + '</b></th>';
			 		headerHtml += '<th class="corr-table-head" title="' + fieldInfo + '">' + field.Label + '</th>';
			 		headerMatrix += '<th title="' + fieldInfo + '" style="width:30px; max-width:30px; min-width:30px;"><b>' + String.fromCharCode(i+65) + '</b></th>';
			 		i++;
			 	}
			 }
			 headerLabel += "</tr>";
			 headerHtml += "</tr>";
			 headerMatrix += "</tr>";
			 $('.corr-result-table').append(headerLabel);
			$('.corr-result-table').append(headerHtml);
			$('.corr-result-matrix').append(headerMatrix);
			var i = 0;
			for(x in correlationFields){
				var j = 0;
				if(x != -1){
					var fieldInfo = getFieldInfo(correlationFields[x]);
					rowHtml += '<tr><th><b>' + String.fromCharCode(i+65) + '</b></th><th class="corr-table-head corr-table-left" title="' + fieldInfo + '" style="min-width: 70px;">' + correlationFields[x].Label + '</th>';
					rowMatrix += '<tr><th title="' + fieldInfo + '" style="height:30px; min-height: 30px; max-height: 30px;"><b>' + String.fromCharCode(i+65) + '</b></th>';
					var rowField = data.filter((i)=>{ return i.Id1 == x });
					for(row in rowField){
						rowHtml += '<td>' + (j<=i?rowField[row].r: '') + '</td>';
						var per = Math.floor(Math.abs(rowField[row].r*255)) + 10;
						var rgb = "";
						var s = 255;
						if(rowField[row].r >= 0) rgb = (s-per*0.8) + ',' + (s-per*0.8) + ',' + (s-per*0.2);
						else rgb = (s-per*0.2) + ',' + (s-per) + ',' + (s-per);
						if(j<=i) {
							rowMatrix += '<td class="corr-table-color" title="' + rowField[row].r + '" style="background-color:rgb(' + rgb + ');"></td>';
						} else {
							rowMatrix += '<td class="corr-table-color"></td>';
						}
						j++;
					}
					rowHtml += '</tr>';
					rowMatrix += '</tr>';
				}
				i++;
			}
			$('.wui-layout').hide();
			$('.corr-result-table tbody').append(rowHtml);
			$('.corr-result-matrix tbody').append(rowMatrix);
			loading.hide();
			$('.corr-result').show();
			$('.corr-gradient').css('height', ($('.corr-result-matrix').height()-30)+'px');
			$('.corr-table-left').height((((i-2)*30)/(i-1)) + 'px');
 			$('.wui-layout').show();
 			corrReportScale = 1;
 			var xScale = window.innerWidth/($('.corr-result').width() + 32), yScale = window.innerHeight/(popupHeight = $('.corr-result').height() + 16);
 			if(xScale < 1 || yScale < 1) {
 				corrReportScale = xScale>yScale? yScale: xScale;
 			}
 			$('.wui-layout').show();
 	});
}