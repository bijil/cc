﻿
var defaultExtent, defaultCenter;
var pageQuery, datasets, templates, datasetId, templateId, template, compiled, outputType, fieldTypes = {}, selectedFieldId;

function checkError(d, callback) {
    if (d.Success) {
        callback && callback(d);
    } else {
        alert('Operation failed - ' + (d.ErrorMessage || 'No details.'));
    }
}

((a) => {
    var formatter = a.formatter = new (function () {
        function formatNumber(n) {
            if (isNaN(n)) return "∞";
            return (n || 0).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
        }

        function _formatCost(n) {
            return formatNumber(n).replace(/\..*/, '');
        }

        function formatMoney(n) {
            return "$" + formatNumber(n);
        }

        function formatCost(n) {
            if(n < 0)
                return "<span class='neg-value'>(" + "$" + _formatCost(-n) + ")</span>";
            else
                return "$" + _formatCost(n);
        }

        function formatCostAbbr(n) {
            var abbr = '';
            if (n > 1E9) { n = parseInt(n / 1E9); abbr = 'B'; }
            else if (n > 1E6) { n = parseInt(n / 1E6); abbr = 'M'; }
            else if (n > 1E3) { n = parseInt(n / 1E3); abbr = 'K'; }

            return "$" + formatNumber(n).replace(/\..*/, '') + abbr;
        }

        this.format = (v, t, a) => {
            switch (t) {
                case 8:
                    return a ? formatCostAbbr(v) : formatCost(v);
                    break;
                default:
                    return v;
            }
        }
    });

    a._$ = formatter.format;
})(window)


function $out(v, a) {
    return _$(v, outputType || 8, a);
}
