﻿var sortF = false;
var linkOpenedInQc = false;
var chm = new ChartManager();
var calculations = {},
    activeCalculation = {},
    calculationResults = {},
    calculationResultsTotalCount = 0,
    ratioDistribution = {},
    regChart,
    ratioChart,
    dataSets = {},
    templates = {},
    cMap,
    mapVisible = false;

//var chm = new ChartManager();
$(window).on("resize", resizeWindow);
function resizeWindow() {
    var H = $(".main-content-area").height();
    $(".d1001").height(H);
    $("#chart1").width($(".chart1-container").box().width - 10);
    $(".result-scroller").height(H - $(".result-scroller").box().top);
    $(".result-scroller #mapbox").height(H - $(".result-scroller").box().top);
    $(".calc-list-div").height(H - $(".calc-list-div").box().top - 1);
}

function enableLayout() {
    $("body").css({ cursor: null });
    $(".d1001").removeAttr("disabled");
    $(".shade").hide();
}

function disableLayout() {
    $(".d1001").attr("disabled", "disabled");
    $(".shade").show();
}

$(() => {
    $(".switch-button-checkbox").on("change", (e) => {
        if ($(e.target).prop("checked")) {
            mapVisible = true;
            $(".result-scroller.output-map").show();
            $(".result-scroller.output-data").hide();
            loadMapView();
        } else {
            $(".result-scroller.output-data").show();
            $(".result-scroller.output-map").hide();
            mapVisible = false;
        }
    });

    loading.show("Loading Calculations");
    var qd = QueryData;
    resizeWindow();

    if (!qd.templateId) {
        $(".output-data").hide();
    }

    initCharts();

    $('.cv-selector').on('change',function(){
        let val = $(this).val();
        $(".c-expression")[val? 'show' : 'hide']();
        
    });

    PageMethods.GetLoadInfo(function (d) {
        $(".calc-info-parent").parents(".ca4-bg").hide();
        $();
        if (d.Success) {
            loading.hide();
            d.Data.Calculations.forEach(function (x, i) {
                calculations[x.ID] = x;
            });
            d.Data.Datasets.forEach(function (x, i) {
                dataSets[x.ID] = x;
            });
            d.Data.Templates.forEach(function (x, i) {
                templates[x.ID] = x;
            });
            fillDropdown(".ds-selector", d.Data.Datasets, true);
            fillDropdown(".rm-selector", d.Data.Templates, false);
            fillDropdown(".cv-selector", d.Data.CompareValues, false);
            showCalculationList(d.Data.Calculations);
        } else say("Error Occured : " + d.ErrorMessage);
    });
});

function openCalcDialog(id) {
    disableLayout();
    $(".calc-dialog").show();
    if (id) {
        var calc = calculations[id];
        $(".calc-row input[class='c-name']").val(calc.Name);
        $(".calc-row .rm-selector").val(calc.TemplateID);
        $(".calc-row .ds-selector").val(calc.DataSetID);
        $(".calc-row .cv-selector").val(calc.ComparisonField);
        $(".calc-row input[class='c-exp']").val(calc.CustomExpression);
        $(".c-expression")[calc.ComparisonField? 'show' : 'hide']();
        $(".calc-save").attr("create", 0).attr("calcid", id);
    } else {
        $(".calc-row input,.calc-row select").val("");
        $(".calc-save").attr("create", 1).attr("calcid", "");
        $(".c-expression")['hide']();
    }
}

function hideCalcDialog() {
    $(".calc-dialog").hide();
    enableLayout();
}

function reloadCalculationList() {
    PageMethods.GetCalculationList((d) => {
        if (d.Success) {
            calculations = {};
            activeCalculation = {};
            d.Data.Calculations.forEach(function (x, i) {
                calculations[x.ID] = x;
            });
            showCalculationList(d.Data.Calculations);
        } else say(d.ErrorMessage);
    });
}

function reCalculate(id, msg, f) {
    id = id ? id : activeCalculation.ID;
    msg = msg ? msg : "Recalculating based on the changes";
    loading.show(msg);
    PageMethods.RunCalculation(id, (d) => {
        checkError(d, () => {
            $(".output-data").show();
            calculationResults = d.Data.CalculationResult;
            calculationResultsTotalCount = d.Data.CalculationResult;
            loading.hide();
            openCalculation(id);
        });
        loading.hide();
    });
}

function recalcButton() {
    var currentId = activeCalculation && activeCalculation.ID;
    reCalculate(currentId, "Calculating values based on your settings");
}

function saveCalcInfo() {
    $(".calc-save").attr("disabled", "disabled");
    var type = parseInt($(".calc-save").attr("create"));
    var name = $(".calc-row input[class='c-name']").val();
    var rmid = $(".calc-row .rm-selector").val();
    var dsid = $(".calc-row .ds-selector").val();
    var fId = $('.calc-row .cv-selector').val() || 0;
    var cExp = $(".calc-row input[class='c-exp']").val();
    if (type) {
        var nameRepeat = false;
        for (var x in calculations) {
            if (calculations[x].Name == name) {
                nameRepeat = true;
                say("A Calculation with the same name already exists. Please type in a different name");
                enableLayout();
                $(".calc-save").removeAttr("disabled");
                return false;
            }
        }
    }
    cExp = fId ? cExp : '';
    if (name.trim().length == 0) {
        say("Enter a valid name");
        $(".calc-save").removeAttr("disabled");
    } else if (name && rmid && dsid)
        if (type) {
            PageMethods.CreateNewCalculation(name, rmid, dsid, fId, cExp, (d) => {
                if (d.Success && d.Data) {
                    reloadCalculationList();
                    hideCalcDialog();
                    reCalculate(d.Data, "Calculating values based on your settings");
                    say("New calculation model is created.");
                } else say(d.ErrorMessage);
                $(".calc-save").removeAttr("disabled");
            });
        } else {
            var calcid = $(".calc-save").attr("calcid");
            var oldCalc = calculations[calcid];
            PageMethods.UpdateCalculation(calcid, name, rmid, dsid, fId, cExp, (d) => {
                if (d.Success) {
                    calculations[calcid] = d.Data.Calculations[0];
                    reloadCalculationList();
                    hideCalcDialog();
                    if (oldCalc.TemplateID != rmid || oldCalc.DataSetID != dsid || oldCalc.ComparisonField != fId || oldCalc.CustomExpression != cExp) {
                        reCalculate(calcid);
                    } else openCalculation(calcid);
                } else say(d.ErrorMessage);
                $(".calc-save").removeAttr("disabled");
            });
        }
    else {
        if(!rmid) say("Please select a regression model from the list.");
        else say("Please select a dataset from the list.");
        $(".calc-save").removeAttr("disabled");
    }
}

function showCalculationList(cdata) {
    var html = "";
    cdata.forEach(function (x) {
        with (x) {
            html += `<tr><td class="left-list-item calc-item" calcId="${ID}"><a calcId="${ID}" style="width: 220px;display: inline-flex;"><div title=${Name} style="white-space: pre;text-overflow: ellipsis; overflow: hidden;width: auto; margin-right:0px">${Name}</div></a> <span d="1"></span><span e="1"></span></td></tr>`;
        }
    });
    $(".calculations-list").html(html);
    $("a", ".calculations-list")
        .off("click")
        .on("click", function () {
            var calcId = $(this).parent().attr("calcId");
            openCalculation(calcId);
        });

    $("span[e]", ".calculations-list")
        .off("click")
        .on("click", function () {
            var calcid = $(this).parent().attr("calcid");
            openCalcDialog(calcid);
        });

    $("span[d]", ".calculations-list")
        .off("click")
        .on("click", function () {
            var cid = $(this).parent().attr("calcid");
            let title = $(this).parent().find("a>div[title]").text();
            ask("Are you sure you want to delete Calculation model " + title + " permanently?", () => {
                loading.show("Deleting " + title);
                PageMethods.DeleteCalculation(cid, function (d) {
                    if (d.Success) {
                        reloadCalculationList();
                        setCalculationHeader();
                    } else say(d.ErrorMessage);
                    loading.hide();
                });
            });
        });
}

function fillDropdown(s, da, f) {
    if (s && da) {
        var html = '<option value="">-- Select --</option>';
        da.forEach(function (x) {
            with (x) {
                html += `<option value="${ID}">${Name} ` + (f ? `(${PoolSize} parcels)` : "") + `</option>`;
            }
        });
        $(s).html(html);
    }
}

function setCalculationHeader(calcId) {
    if (calcId) {
        activeCalculation = calculations[calcId];
        $(".calculation-name").html(activeCalculation.Name);
        $(".calculation-action .switch-button-checkbox").prop("checked", false);
        $(".calculation-action").show();
        $(".calc-item").selectOne("calcId", calcId, "selected");
        var CurrentDataSet = dataSets[activeCalculation.DataSetID];
        var CurrentTemplate = templates[activeCalculation.TemplateID];
        outputType = templates[activeCalculation.TemplateID].DataType;
        if (!CurrentDataSet || !CurrentTemplate) {
            $(".calc-info-parent").parents(".ca4-bg").hide();
            if (
                confirm(
                    "Dataset or Regression model for the given model does not exists. Do you want to edit model settings?"
                )
            ) {
                openCalcDialog(calcId);
                return false;
            } else {
                return false;
            }
        }
        $(".calc-info-model").html(CurrentTemplate.Name).attr("title", CurrentTemplate.Name);
        $(".calc-info-dataset").html(CurrentDataSet.Name).attr("title", CurrentDataSet.Name);
        $(".calc-info-count")
            .html(numberWithCommas(calculationResultsTotalCount))
            .attr("title", calculationResultsTotalCount);
        $(".calc-info-parent").parents(".ca4-bg").show();
    } else {
        activeCalculation = {};
        $(".calculation-name").html("&nbsp;");
        $(".calculation-action").hide();
        $(".output-data").hide();
        $(".calc-item").removeClass("selected");
        $(".calc-info-parent").parents(".ca4-bg").hide();
        $(".calc-info-model,.calc-info-dataset,.calc-info-count").html("").attr("title", "");
        $(".result-scroller.output-map").hide();
        mapVisible = false;
    }
}

function showResults(calcId) {
    setCalculationHeader(calcId);
    loadResidualData(0);
}

function openCalculation(calcId) {
    loading.show("Loading Calculation Results");
    $(".result-scroller.output-data").show();
    $(".result-scroller.output-map").hide();
    cMap = null;
    let isCalcValid =
        calcId &&
        calculations &&
        calculations[calcId] &&
        calculations[calcId].TemplateID &&
        templates[calculations[calcId].TemplateID];
    if (isCalcValid) {
        PageMethods.GetCalculation(calcId, (d) => {
            checkError(d, () => {
                $(".output-data").show();
                calculationResults = d.Data.CalculationResult;
                calculationResultsTotalCount = d.FullCount;
                ratioDistribution = d.Data.RatioDistribution;
                loading.hide();
                showResults(calcId);
                loadGraph();
            });
            loading.hide();
        });
    } else {
        say("Selected Regression Model has been edited. You need to run regression and calculate again to continue");
    }
}

function sortDatabyRatio() {
    sortF ? loadResidualData(0, 0) : loadResidualData(0, 1);
    sortF = !sortF;
}

function loadResidualData(pageIndex, RSort) {
    var pageSize = 50;
    RSort = RSort ? 1 : 0;
    var spans = $(".residual-table th").length;
    var loading = `<tr><td colspan="${spans}" class='residuals-loading'>Loading residuals ...</td></tr>`;
    $(".residual-list").html(loading);
    if(!activeCalculation.ComparisonField)
        $('.output-table tr th.compared').hide();
    else    
        $('.output-table tr th.compared').show();
    PageMethods.GetGridData(activeCalculation.ID, pageIndex, pageSize, RSort, function (d) {
        checkError(d, () => {
            let ctx = "Displaying ";
            ctx +=
                calculationResultsTotalCount > 500
                    ? "top 500 of " + calculationResultsTotalCount
                    : calculationResultsTotalCount + " parcels";
            $(".rt-count").html(ctx);
            var html = "";
            for (var i in d.Data) {
                with ((dx = d.Data[i])) {
                    if (outputType != 8) {
                        Actual = numberWithCommas(Math.round($out(Actual) * 100) / 100);
                        Predicted = numberWithCommas(Math.round($out(Predicted) * 100) / 100);
                    } else {
                        Actual = $out(Actual);
                        Predicted = $out(Predicted);
                    }
                    if (Ratio) Ratio = Ratio.toFixed(2);
                    Residual = numberWithCommas(Math.round(Residual * 100) / 100);
                    html += `<tr><td class="open-in-qc" style="text-decoration: none;" pid="${ParcelId}">${Parcel}</a></td><td>${Actual}</td><td>${Predicted}</td><td>${Residual}</td><td>${Ratio}</td>` 
                    html += activeCalculation?.ComparisonField ? `<td>${ComparedValue}</td><td>${ComparedRatio}</td>` : ``
                    html += `</tr>`;
                }
            }

            $(".residual-list").html(html);
            openInQc();
            var numPages = Math.min(Math.ceil(calculationResultsTotalCount / pageSize), 10);
            var numButtons = Array.apply(0, Array(numPages))
                .map(function (x, i) {
                    var sel = i == pageIndex ? ' selected=""' : "";
                    return `<a pi="${i}"${sel}>${i + 1}</a>`;
                })
                .reduce(function (x, y) {
                    return x + y;
                }, "");
            $(".data-page-links").html(numButtons);

            $(".rt-container").height($(".output-table").box().height);

            $("a[pi]", ".data-page-links").bindEvent("click", function () {
                var pageIndex = parseInt($(this).attr("pi"));
                sortF ? loadResidualData(pageIndex, 1) : loadResidualData(pageIndex, 0);
            });
        });
    });
}

function loadGraph() {
    var actuals = calculationResults.map(function (o) {
        return { x: o.pid, y: o.a ? o.a.toFixed(2) : o.a };
    });
    var predicted = calculationResults.map(function (o) {
        return { x: o.pid, y: o.p ? o.p.toFixed(2) : o.p, pointRadius: 2 };
    });
    // var compared = calculationResults.map(function (o) {
    //     return { x: o.pid, y: o.c ? o.c.toFixed(2) : o.c, pointRadius: 2 };
    // });
    regChart.clear();
    regChart
        .addData("Predicted Value", predicted, {
            backgroundColor: ["#0033ff"],
            borderColor: ["#0066ff"],
        })
        .addData("Actual Value", actuals, {
            backgroundColor: ["#9E9E9E88"],
            borderColor: ["#9E9E9E"],
        });
        // .addData("Compared Value", compared, {
        //     backgroundColor: ["#8200de"],
        //     borderColor: ["#9E9E9E"],
        // });
    regChart.refresh();

    //Ratio Chart
    if (ratioDistribution && ratioDistribution.length > 0) {
        var rr = {},
            rd2 = [];
        ratioDistribution
            .map((x) => {
                return { r: x.r < 0.5 ? 0.5 : x.r > 1.5 ? 1.5 : x.r, p: x.p };
            })
            .map((x) => {
                rr[x.r] = (rr[x.r] || 0) + (x.p || 0);
            });
        for (var x in rr) {
            rd2.push({ r: parseFloat(x), p: parseFloat(rr[x].toFixed(1)) });
        }
        rd2 = rd2.sort((x, y) => x.r - y.r);

        var fx = rd2.map(function (i) {
            return i.r;
        });
        var fy = rd2.map(function (i) {
            return i.p;
        });
        chm.charts["chart2"].data.labels = fx;
        chm.setData("chart2", "Ratio Frequency", fy);
        var rf = ratioDistribution.map((x) => x.r);
    } else {
        chm.setData("chart2", "Ratio Frequency", []);
    }
}

function showCPInfo(pd) {
    var cpi = $(".chart-info");
    $(".iv-parcel", cpi).html(pd.pk);
    if (outputType != 8) {
        $(".iv-actual", cpi).html("$" + numberWithCommas(Math.round($out(pd.a) * 100) / 100));
        $(".iv-predicted", cpi).html("$" + numberWithCommas(Math.round($out(pd.p) * 100) / 100));
    } else {
        $(".iv-actual", cpi).html($out(pd.a));
        $(".iv-predicted", cpi).html($out(pd.p));
    }

    $(".iv-parcel", cpi).html(pd.pk);
    var cb = $(regChart.chart.canvas).box();
    var pb = cpi.box();
    var t = -cb.height + (cb.height - pb.height) / 2,
        l = (cb.width - pb.width) / 2;
    cpi.css({ "margin-top": t, "margin-left": l });
    cpi.show();

    var qcurl = "/protected/quality/#/parcel/" + pd.pid;
    $(".iv-qc-link", cpi).attr("href", qcurl);
    $(".iv-qc-link", cpi).attr("target", "_parcel_" + pd.pid);
    $(".iv-qc-link", cpi).attr("rel", "noopener");

    $(regChart.chart.canvas).attr("disabled", "disabled");

    $(".close", cpi)
        .off("click")
        .on("click", () => {
            $(regChart.chart.canvas).removeAttr("disabled");
            cpi.hide();
        });
}

function initCharts() {
    regChart = chm.createChart("chart1", "chart1", "scatter", {
        labels: [100, 400],
        tooltips: {
            callbacks: {
                //title: (ti, d) => { console.log('title', ti, d); return 'title' },
                //label: (ti, d) => { console.log('label', ti, d); return 'title' },
            },
            displayColors: false,
        },
        responsive: true,
        maintainAspectRatio: false,
        options: {},
        scales: {
            yAxes: [
                {
                    ticks: {
                        callback: function (dataLabel, index, labels) {
                            return outputType ? $out(dataLabel, 1) : dataLabel;
                        },
                    },
                },
            ],
        },
    });
    regChart.onclick = (x, c) => {
        if (x.length > 0) {
            var ce = x[0];
            var pd = calculationResults[ce._index];
            showCPInfo(pd);
        }
        $(document)
            .off("click")
            .on("click", (e) => {
                var container = $(".chart-info");
                if (!container.is(e.target) && container.has(e.target).length === 0) {
                    if (!$(c.canvas).is(e.target) || !($(c.canvas).is(e.target) && x.length)) {
                        $(regChart.chart.canvas).removeAttr("disabled");
                        container.hide();
                    }
                }
            });
    };

    ratioChart = chm.createChart("chart2", "chart2", "line", {
        labels: ["Min", "Max", "Avg"],
        elements: { point: { radius: 1 } },
        scales: {
            yAxes: [
                {
                    ticks: {
                        max: 100,
                        min: 0,
                    },
                },
            ],
            xAxes: [
                {
                    ticks: {
                        callback: function (dataLabel, index, labels) {
                            if (index == 0) {
                                return "<" + dataLabel;
                            }
                            if (index == labels.length - 1) return ">" + dataLabel;
                            return (parseFloat(dataLabel) * 10) % 5 === 0 ? dataLabel : "";
                        },
                    },
                },
            ],
        },
    });
}

function downloadReport() {
    var query = "?calculationId=" + activeCalculation.ID;
    var url = location.pathname + "/exportdata" + query;
    location.href = url;
}

function resetView() {
    console.log("resetview");
    cMap.clear();
}

function initMap() {
    PageMethods.GetGISConfig(function (d) {
        if (d.Success) {
            var px = d.Data;
            defaultExtent = [px.lg1, px.lt1, px.lg2, px.lt2];
            defaultCenter = [(px.lg1 + px.lg2) / 2, (px.lt1 + px.lt2) / 2];
            cMap = new MapView("mapbox", defaultExtent, defaultCenter);
            cMap.viewChangeStart = (e) => {
                hideMapPopup();
            };
            cMap.viewChanged = function (e) {
                loadPointsInMap(e);
            };
            cMap.mapClick = (e, event) => {
                let left = event && event.pointerEvent && event.pointerEvent.clientX;
                let top = event && event.pointerEvent && event.pointerEvent.clientY;
                let pw = $(".popup-dg.map-popup").outerWidth(),
                    ph = $(".popup-dg.map-popup").outerHeight();
                left = left > $(window).innerWidth() - pw ? left - pw - 5 : left + 5;
                top = top > $(window).innerHeight() - ph - 30 ? top - ph - 5 : top + 5;
                if (e && e.length) {
                    showMapPopup(e[0].values_, left, top - 75);
                } else hideMapPopup();
            };
            resetView();
            loadPointsInMap(defaultExtent, function (p, c) {
                cMap && cMap.resetView(p);
                enableLayout();
            });
        } else {
            alert(d.ErrorMessage || "GIS Configuration unavailable!");
        }
    });
}

function loadMapView() {
    cMap = null;
    $("#mapbox").html("");
    initMap();
}

function loadPointsInMap(extent, callback) {
    if (mapVisible && cMap && activeCalculation && activeCalculation.ID && extent) {
        var resolution = cMap.getResolution();
        PageMethods.GetMapData(activeCalculation.ID, extent, function (d) {
            cMap && cMap.clear();
            if (d.Count > 0) {
                var slat = 0,
                    slng = 0;
                var pcount = 0;
                for (var i in d.Data) {
                    var p = d.Data[i];
                    cMap && cMap.addDataPoint(
                        [p.Longitude, p.Latitude],
                        resolution * p.Range,
                        p.AvgRatio,
                        p.ParcelId,
                        p.MaxRatio,
                        p.MinRatio,
                        p.Volume,
                        p.AvgComparedRatio
                    );
                    slng += p.Longitude;
                    slat += p.Latitude;
                    pcount += p.Volume;
                }

                var clng = slng / d.Count,
                    clat = slat / d.Count;
                if (callback) callback([clng, clat], pcount);
                syncLock = false;
            } else {
                if (callback) callback(null, 0);
                syncLock = false;
            }
        });
    }
}
function showMapPopup(val, left, top) {
    let mp = $(".map-popup").show();
    let dr = ["div[ar]", "div[max]", "div[min]", "div[count]", "div[pId]","div[cvr]"];
    let drData = [val.AvgRatio, val.MaxRatio, val.MinRatio, val.Count, val.ParcelId, val.AvgComparedRatio];
    dr.forEach((name, i) => {
        if (i == 0) $(name + " span", mp).text(drData[3] == 1 ? "Ratio:" : "Avg Ratio:");
        if (i == 5) $(name + " span", mp).text(drData[3] == 1 ? "CompRatio:" : "Avg CompRatio:");
        $(name).hide();
        if (i == 4 && drData[3] == 1) {
            $(name + " p", mp).html(
                '<span class="open-in-qc" pid="'+ drData[i] + '">' + drData[i] + "</span>"
            );
            $(name).show();
        } else if (i != 4 && i != 5) {
            $(name + " p", mp).text(drData[i]);
            $(name)[drData[3] == 1 && [1, 2, 3].indexOf(i) >= 0 ? "hide" : "show"]();
        } else if (i ==5){
            $(name + " p", mp).text(drData[i]);
            $(name)[!activeCalculation.ComparisonField ? "hide" : "show"]();
        }

    });
    openInQc();
    mp.css({
        left,
        top,
    });
    $(document)
    .off("click")
    .on("click", (e) => {
        var mapViewDiv = $(".ol-unselectable");
        if (mapVisible) {
            if (!mapViewDiv.is(e.target)) {
                if (!e.target.closest(".map-popup")) hideMapPopup();
            }
        }
    });
}
function hideMapPopup() {
    $(".map-popup").hide();
}
function openInQc(){
    $('.open-in-qc').off('click').on('click', function(e) {
	    e.preventDefault();
        let parcelId = $(this).attr("pid")
	    function openInNewTab(href) {
		  Object.assign(document.createElement('a'), {
			target: '_blank',
			href: href,
		  }).click();
		}
		if(!linkOpenedInQc){
			linkOpenedInQc = !linkOpenedInQc;
			openInNewTab("/protected/quality/#/parcel/" + parcelId);
			setTimeout(function(){linkOpenedInQc = false},60000)
		}
		else {
			say("Opening in QC is disabled for multiple clicks in short time. Try again after completely loading the opened one.")
		}
		
	});
}

