﻿Partial Class App_MasterPages_CAMACloud
	Inherits System.Web.UI.MasterPage
    Public EnabledInternalView As String

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Membership.ApplicationName <> HttpContext.Current.GetCAMASession.TenantKey Then
            CAMASession.Logout(HttpContext.Current)
            Exit Sub
		End If

		Page.Title = HttpContext.Current.GetCAMASession.OrganizationName + " :: CAMA Cloud&#174;"
		Dim organizationId As Integer = HttpContext.Current.GetCAMASession.OrganizationId
        EnabledInternalView = Database.System.GetStringValue("SELECT EnableInternalView FROM OrganizationSettings WHERE OrganizationId = " + organizationId.ToString())

        If Not IsPostBack Then
            Dim allowedRoles As String = ConfigurationManager.AppSettings("AllowedRole")
            Dim Username = Membership.GetUser().ToString
            Dim role = Roles.GetRolesForUser(Username)
            Dim copyAllowedRoles As String = allowedRoles
            Dim curl As String = Request.Url.ToString()
            Dim cpDefaultUser As Boolean = False

            Dim maintenanceURL As String = ApplicationSettings.MaintenanceURL
            If (maintenanceURL <> "False") Then
                FormsAuthentication.SignOut()
                Response.Redirect(maintenanceURL)
            End If

            If role.Length >= 1 Then
                If role.Contains("BasicSettings") AndAlso (allowedRoles.Contains("DataSetup") Or allowedRoles.Contains("Support") Or curl.Contains("autopriorityrules")) Then 'Added DataSetup condition to prevent user from access dtr, sv or other modules when directly using link.
                    allowedRoles = "BasicSettings"
                    'ElseIf role(0) = "DataSetup" And Username = "dcs-support" Then
                    '    allowedRoles = "datasetup"
                    'check in 'dcs-support'"DataSetup"
                End If
            End If

            Dim default_ManagerUser As String() = {"admin", "dcs-support", "vendor-cds", "dcs-qa", "System", "dcs-rd", "malite_admin", "dcs-integration", "dcs-ps"}
            If ClientOrganization.HasModule(organizationId, "DTR") AndAlso Roles.RoleExists("DTR-Manager") Then
	            For Each un In default_ManagerUser
		    		If Not (Roles.IsUserInRole(un, "DTR-ReadOnly") Or Roles.IsUserInRole(un, "DTR-Editor") Or Roles.IsUserInRole(un, "DTR-Manager")) Then
		    			Roles.AddUserToRole(un, "DTR-Manager")
		    		End If
	            Next
	            Dim DTRNoNUser As DataTable = Database.System.GetDataTable("SELECT au.UserName FROM  aspnet_Users au INNER JOIN aspnet_Applications a On a.ApplicationId=au.ApplicationId WHERE  a.ApplicationName='" & Membership.ApplicationName & "'")
	            For Each row As DataRow In DTRNoNUser.Rows
	            	Dim un = row("UserName")
	            	If Roles.IsUserInRole(un, "BasicSettings") AndAlso Roles.IsUserInRole(un, "QC") AndAlso (Not default_ManagerUser.Contains(un)) AndAlso (Not (Roles.IsUserInRole(un, "DTR-ReadOnly") Or Roles.IsUserInRole(un, "DTR-Editor") Or Roles.IsUserInRole(un, "DTR-Manager"))) Then
	            		Roles.AddUserToRole(un, "DTR-Manager")
	            	End If
	            Next
            End If
            
            If copyAllowedRoles.Contains("Support") Then 'Added to handle support links correctly. "NotSupported" is given to restricted user.
            	If allowedRoles.Contains("BasicSettings") Then
            		If (Username = "vendor-cds" Or Username = "malite_admin") AndAlso (curl.Contains("clientsettingseditor") Or curl.Contains("sketchsettingseditor") Or (Username = "malite_admin" AndAlso curl.Contains("prioritylistbackups"))) Then
            			allowedRoles = "NotSupported" 
            		Else
            			If Not (Username = "vendor-cds" Or Username = "malite_admin") AndAlso Not (curl.Contains("photocleanup")) Then
            				allowedRoles = "NotSupported"
            			End If
            		End If
            	Else
            		If default_ManagerUser.Contains(Username) Then
            			allowedRoles = "DataSetup"
            			If (Username = "vendor-cds" Or Username = "malite_admin") AndAlso (curl.Contains("clientsettingseditor") Or curl.Contains("sketchsettingseditor") Or (Username = "malite_admin" AndAlso curl.Contains("prioritylistbackups"))) Then
	            			allowedRoles = "NotSupported"
            			End If
            		Else
            			If Not (Roles.IsUserInRole(Username, "DataSetup")) Then
            				allowedRoles = "NotSupported"
            			Else If Roles.IsUserInRole(Username, "DataSetup") And curl.Contains("photocleanup") Then 'photocleanup under client data setup not under advanced settings
            				allowedRoles = "DataSetup"
            			End If
            		End If
            	End If
            End If
            
            If allowedRoles.Contains("BasicSettings") AndAlso Not ((Username = "vendor-cds" Or Username = "malite_admin") AndAlso copyAllowedRoles.Contains("Support"))  Then 'basic settings rols having limited access , added special conditional access to basic settings users
            	Dim basicSettings_links As String () = {"datasetup", "users", "fieldalerttypes", "heatmap", "License", "photocleanup", "searchOptions", "userroles", "parcelstatuslist", "autopriorityrules", "annualprocesses"}
            	Dim IsBsl As Boolean = False
            	For Each bsl In basicSettings_links
            		If curl.Contains(bsl) Then
            			IsBsl = True
            			Exit For
            		End If
            	Next
            	If Not IsBsl Then
            		allowedRoles = "NotSupported"
            	End If
            End If
            
            If allowedRoles <> "" Then
                Dim restricted As Boolean = True
                For Each allowedRole In allowedRoles.Split(",")
                    allowedRole = allowedRole.Trim
                    If allowedRole <> "" Then
                        If Roles.IsUserInRole(allowedRole) Then
                            restricted = False
                            Exit For
                        End If
                    End If
                Next

                If restricted AndAlso default_ManagerUser.Contains(Username) AndAlso allowedRoles.Contains("CompSales") Then
                    For Each bsl In {"cse/rules", "/cse/rlookup", "/cse/reportfields", "/cse/Settings"}
                        If curl.Contains(bsl) Then
                            cpDefaultUser = True
                            Exit For
                        End If
                    Next
                End If

                If restricted AndAlso Not cpDefaultUser Then
                    Response.Redirect("~/web/403.aspx")
                End If
            End If
            'End If


            'If allowedRole <> "" Then
            '    If Not Roles.IsUserInRole(allowedRole) Then

            '    End If
            'End If\
            

			Dim nylDay  = Database.Tenant.GetStringValue("SELECT NYLDate FROM APPLICATION WHERE ROWID = 1")
			Dim currentDate As Date = DateTime.Now
			If nylDay.ToString <> "" Then
				'Dim nylDt As Date = nylDay
'				Dim datediff = nylDt - currentDate
'				Dim daysToNyl = ""
				Dim dt1 As DateTime = Convert.ToDateTime(nylDay)
	            Dim dt2 As DateTime = Convert.ToDateTime(currentDate)
	            Dim ts As TimeSpan = dt1.Subtract(dt2)
	            Dim days = ts.Days
	           ' days = days.ToString.Replace("-", "")
	            Dim nylDate = nylDay.ToString.Split(" "c)(0)
				If  convert.ToInt32(days) <= 30 AndAlso convert.ToInt32(days) > 0 Then
	               nylPanel.Style("Display") = ""
	               lblNyl.Text = "Rollover process is estimated for " + days.ToString + " days from now on " + nylDate + ". For more information, "
'	            Else If convert.ToInt32(days) < 0
'	                nylPanel.Style("Display") = ""
'	                lblNyl.Text = "Estimated date (" + nylDate + ") for rollover process expired.For more information,"
	            End If
	            
	            
	            
'				If datediff.ToString.IndexOf(".") > 2 Then
'					daysToNyl = datediff.ToString.Substring(0, datediff.ToString.IndexOf(":"))
'				Else
'					daysToNyl = datediff.ToString.Substring(0, datediff.ToString.IndexOf("."))
'				End If

'				Dim nylDate = nylDt.ToString.Split(" "c)(0)
'				If  convert.ToInt32(daysToNyl) <= 30 AndAlso convert.ToInt32(daysToNyl) > 0 Then
'	               nylPanel.Style("Display") = ""
'	               lblNyl.Text = "Rollover process is estimated for " + daysToNyl + " days from now on " + nylDate + ".For more information,"
'	            Else If convert.ToInt32(daysToNyl) < 0
'	                nylPanel.Style("Display") = ""
'	                lblNyl.Text = "Estimated date (" + nylDate + ") for rollover process expired.For more information,"
'	            End If
	            
			End If

        End If
    End Sub
    
    Private Sub lbLogout_Click(sender As Object, e As System.EventArgs) Handles lbLogout.Click
        CAMASession.Logout(HttpContext.Current)
    End Sub

    'Sub LogoutFromApplication()
    '    FormsAuthentication.SignOut()

    '    Dim ccHost As CAMACloudHost = CAMACloudHost.GetWorkingHost()
    '    Dim fac As New HttpCookie(FormsAuthentication.FormsCookieName, "")
    '    fac.Domain = ccHost.HostRoot
    '    fac.Expires = DateTime.Now.AddYears(-1)
    '    Response.Cookies.Add(fac)

    '    Dim ans As New HttpCookie("ASP.NET_SessionId", "")
    '    ans.Expires = DateTime.Now.AddYears(-1)
    '    Response.Cookies.Add(ans)

    '    Session.RemoveAll()
    '    Session.Abandon()
    '    Response.Redirect("~/")
    'End Sub
End Class

