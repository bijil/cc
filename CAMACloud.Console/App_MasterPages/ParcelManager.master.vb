﻿Imports System.Web.UI

Partial Class App_MasterPages_ParcelManager
    Inherits System.Web.UI.MasterPage

    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            Dim organizationId As Integer = HttpContext.Current.GetCAMASession.OrganizationId
            phMobileTasks.Visible = ClientOrganization.HasModule(organizationId, "MobileAssessor")
            phDTR.Visible = ClientOrganization.HasModule(organizationId, "DTR")

            hlNeighborhood.Text = CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName() & "s Status"
            hlRelinkNeighborhoods.Text = "Relink " & CAMACloud.BusinessLogic.Neighborhood.getNeighborhoodName() & "s"
           
       	
        End If
    End Sub

   
End Class

